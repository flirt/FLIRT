// ========================================================================= //
// Copyright 2016-2018 SUPAMONKS_STUDIO                                      //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
//                                                                           //
// ========================================================================= //
#pragma once

#include <memory>

#include <smks/xchg/ParmEvent.hpp>
#include <smks/ClientBase.hpp>
#include <smks/rndr/AbstractDevice.hpp>
#include "smks/sys/configure_rndr_client.hpp"

namespace smks { namespace rndr
{
    class FLIRT_RNDR_CLIENT_EXPORT Client:
        public ClientBase
    {
        friend class Binding;
        friend class XformBinding;
    public:
        typedef std::shared_ptr<Client> Ptr;
        typedef std::weak_ptr<Client>   WPtr;
    private:
        typedef std::shared_ptr<scn::Scene>         ScenePtr;
        typedef std::shared_ptr<ClientBindingBase>  ClientBindingBasePtr;
        typedef std::weak_ptr<scn::TreeNode>        TreeNodeWPtr;

    private:
        class PImpl;
    public:
        ///////////////
        // class Config
        ///////////////
        class FLIRT_RNDR_CLIENT_EXPORT Config
        {
        public:
            const char*  deviceModule() const;
            unsigned int numThreads() const;
            const char*  rtCoreConfig() const;
        private:
            struct PImpl;
        private:
            PImpl* _pImpl;
        public:
            Config();
            Config(const Config&);
            explicit Config(const char*);
            Config& operator=(const Config&);
            ~Config();
        private:
            void setDefaults();
        };
        ////////////////
    private:
        PImpl *_pImpl;

    public:
        static inline
        Ptr
        create(const char* jsonCfg = "")  // invokes SmksRndrDevice module by default
        {
            Ptr ptr(new Client(jsonCfg));
            ptr->build(ptr, jsonCfg);
            return ptr;
        }

        static inline
        void
        destroy(Ptr& ptr)
        {
            if (ptr)
                ptr->dispose();
            ptr = nullptr;
        }

    protected:
        explicit
        Client(const char* jsonCfg);
    private:
        // non-copyable
        Client(const Client&);
        Client& operator=(const Client&);

    protected:
        //<! creates device
        virtual
        void
        build(Ptr const&,
              const char* jsonCfg);

        //<! destroys device
        virtual
        void
        dispose();

    public:
        ~Client();

    private:
        // synchronize device's handles (Binding)
        unsigned int setBool        (AbstractDevice::Handle, const char*, bool);
        unsigned int setChar        (AbstractDevice::Handle, const char*, char);
        unsigned int setUInt        (AbstractDevice::Handle, const char*, unsigned int);
        unsigned int setUInt64      (AbstractDevice::Handle, const char*, size_t);
        unsigned int setInt1        (AbstractDevice::Handle, const char*, int);
        unsigned int setInt2        (AbstractDevice::Handle, const char*, const math::Vector2i&);
        unsigned int setInt3        (AbstractDevice::Handle, const char*, const math::Vector3i&);
        unsigned int setInt4        (AbstractDevice::Handle, const char*, const math::Vector4i&);
        unsigned int setFloat1      (AbstractDevice::Handle, const char*, float);
        unsigned int setFloat2      (AbstractDevice::Handle, const char*, const math::Vector2f&);
        unsigned int setFloat4      (AbstractDevice::Handle, const char*, const math::Vector4f&);
        unsigned int setTransform   (AbstractDevice::Handle, const char*, const math::Affine3f&);
        unsigned int setString      (AbstractDevice::Handle, const char*, const std::string&);
        unsigned int setIdSet       (AbstractDevice::Handle, const char*, const xchg::IdSet&);
        unsigned int setBounds      (AbstractDevice::Handle, const char*, const xchg::BoundingBox&);
        unsigned int setDataStream  (AbstractDevice::Handle, const char*, const xchg::DataStream&);
        unsigned int setData        (AbstractDevice::Handle, const char*, std::shared_ptr<xchg::Data> const&);
        unsigned int setObjectPtr   (AbstractDevice::Handle, const char*, xchg::ObjectPtr const&);
        void         unset          (AbstractDevice::Handle, unsigned int);

        void commitFrameState       (AbstractDevice::Handle);
        void beginSubframeCommits   (AbstractDevice::Handle, size_t numSubframes);
        void commitSubframeState    (AbstractDevice::Handle, size_t subframe);
        void endSubframeCommits     (AbstractDevice::Handle);
        bool perSubframeParameter   (AbstractDevice::Handle, unsigned int) const;

        //bool rtLink       (unsigned int parmId, AbstractDevice::Handle target, xchg::NodeClass, AbstractDevice::Handle source, xchg::NodeClass);
        //bool rtUnlink (unsigned int parmId, AbstractDevice::Handle target, xchg::NodeClass, AbstractDevice::Handle source, xchg::NodeClass);

    public:
        void
        render(unsigned int renderActionId);

    protected:
        virtual
        void
        handleSceneEvent(scn::Scene&, const xchg::SceneEvent&);

        virtual
        void
        handleSceneParmEvent(const xchg::ParmEvent&);

    private:
        //void
        //handleNodeParmEvent(unsigned int nodeId, const xchg::ParmEvent&);
        void
        handleRtHandleTypeChanged(unsigned int);

    protected:
        virtual
        std::shared_ptr<ClientBindingBase>
        createBinding(TreeNodeWPtr const&);

        virtual
        void
        destroyBinding(ClientBindingBasePtr const&);
    };
}
}
