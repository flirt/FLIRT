// ========================================================================= //
// Copyright 2016-2018 SUPAMONKS_STUDIO                                      //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
//                                                                           //
// ========================================================================= //
#include <sstream>
#include <json/json.h>

#include <smks/sys/Exception.hpp>
#include <smks/xchg/cfg.hpp>
#include <smks/math/math.hpp>

#include "smks/rndr/Client.hpp"

namespace smks { namespace rndr
{
    struct Client::Config::PImpl
    {
        std::string deviceModule;
    };
}
}

using namespace smks;

void
rndr::Client::Config::setDefaults()
{
    _pImpl->deviceModule = "SmksRndrDevice";
}

rndr::Client::Config::Config():
    _pImpl(new PImpl())
{
    setDefaults();
}

rndr::Client::Config::Config(const Config& other):
    _pImpl(new PImpl())
{
    *_pImpl = *other._pImpl;
}

rndr::Client::Config&
rndr::Client::Config::operator=(const Config& other)
{
    *_pImpl = *other._pImpl;
    return *this;
}

// explicit
rndr::Client::Config::Config(const char* json):
    _pImpl(new PImpl())
{
    setDefaults();

    if (strlen(json) == 0)
        return;

    Json::Value  root;
    Json::Reader reader;

    if (!reader.parse(json, root))
    {
        std::stringstream sstr;
        sstr
            << "Failed to parse Json configuration: " << reader.getFormatedErrorMessages();
        throw sys::Exception(sstr.str().c_str(), "Rendering Client Configuration");
    }

    _pImpl->deviceModule = root.get(xchg::cfg::rendering_device, _pImpl->deviceModule).asString();
}

rndr::Client::Config::~Config()
{
    delete _pImpl;
}

const char* rndr::Client::Config::deviceModule() const { return _pImpl->deviceModule.c_str(); }
