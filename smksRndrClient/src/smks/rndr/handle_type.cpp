// ========================================================================= //
// Copyright 2016-2018 SUPAMONKS_STUDIO                                      //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
//                                                                           //
// ========================================================================= //
#include <smks/parm/BuiltIn.hpp>
#include <smks/parm/BuiltIns.hpp>
#include <smks/xchg/NodeCode.hpp>
#include <smks/xchg/NodeCodes.hpp>
#include <smks/ClientBase.hpp>
#include "handle_type.hpp"

using namespace smks;

bool
rndr::parameterDictatesHandleType(unsigned int      parm,
                                  xchg::NodeClass   nodeClass)
{
    if (nodeClass & xchg::NodeClass::MESH)
        return
            parm == parm::subdivisionLevel().id() ||
            parm == parm::rtSubdivisionMethod().id();

    else if (nodeClass & xchg::NodeClass::CAMERA)
        return
            parm == parm::lensRadius().id() ||
            parm == parm::focalDistance().id();

    else if (nodeClass & (xchg::NodeClass::LIGHT | xchg::NodeClass::SHADER | xchg::NodeClass::RTNODE))
        return parm == parm::code().id();

    else
        return false;
}

void
rndr::getHandleType(const ClientBase&   client,
                    unsigned int        nodeId,
                    std::string&        type)
{
    const xchg::NodeClass nodeClass = client.getNodeClass(nodeId);

    if (nodeClass == xchg::NodeClass::ANY)
        type.clear();

    else if (nodeClass & xchg::NodeClass::MESH)
    {
        unsigned int    subdivisionLevel    = 0;
        std::string     rtSubdivisionMethod = "osd";

        client.getNodeParameter<unsigned int>   (parm::subdivisionLevel(),      subdivisionLevel,       nodeId, client.sceneId());
        client.getNodeParameter<std::string>    (parm::rtSubdivisionMethod(),   rtSubdivisionMethod,    nodeId, client.sceneId());
        type = subdivisionLevel > 0
            ? (rtSubdivisionMethod == "embree" ? "subdivisionMesh" : "triangleMesh")
            : "triangleMesh";
    }

    else if (nodeClass & xchg::NodeClass::CURVES)
        type = "hairGeometry";

    else if (nodeClass & xchg::NodeClass::CAMERA)
    {
        float lensRadius    = 0.0f;
        float focalDistance = 0.0f;

        client.getNodeParameter<float>(parm::lensRadius(),      lensRadius,     nodeId, client.sceneId());
        client.getNodeParameter<float>(parm::focalDistance(),   focalDistance,  nodeId, client.sceneId());
        type = lensRadius > 0.0f && focalDistance > 0.0f
            ? xchg::code::depthOfField().c_str()
            : xchg::code::pinhole().c_str();
    }

    else if (nodeClass & (xchg::NodeClass::LIGHT | xchg::NodeClass::SHADER | xchg::NodeClass::RTNODE))
    {
        client.getNodeParameter<std::string>(parm::code(), type, nodeId);
    }

    else
        type.clear();
}
