// ========================================================================= //
// Copyright 2016-2018 SUPAMONKS_STUDIO                                      //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
//                                                                           //
// ========================================================================= //
#pragma once

#include <boost/unordered_set.hpp>
#include <smks/sys/Exception.hpp>
#include <smks/ClientBindingBase.hpp>
#include <smks/rndr/AbstractDevice.hpp>
#include "RefCount.hpp"

namespace smks
{
    class ClientBase;

    namespace rndr
    {
        class Client;

        class Binding:
            public ClientBindingBase
        {
        public:
            typedef std::shared_ptr<Binding>    Ptr;
            typedef std::weak_ptr<Binding>      WPtr;
        protected:
            typedef std::weak_ptr<ClientBase>       ClientWPtr;
            typedef std::weak_ptr<scn::TreeNode>    TreeNodeWPtr;
        private:
            typedef boost::unordered_map<unsigned int, xchg::ParmEvent> ParmEventMap;

        private:
            RefCount<AbstractDevice::Handle> _handle;

            // whenever a parameter event is caught, store it for applying updates
            ParmEventMap    _perFrameParmUpdates;
            ParmEventMap    _perSubframeParmUpdates;

        public:
            EIGEN_MAKE_ALIGNED_OPERATOR_NEW

            static
            Ptr
            create(ClientWPtr const&,
                   TreeNodeWPtr const&,
                   AbstractDevice*);

            static
            void
            destroy(Ptr&);

        protected:
            Binding(ClientWPtr const&,
                    TreeNodeWPtr const&,
                    AbstractDevice*);
            // non-copyable
            Binding(const Binding&);
            Binding& operator=(const Binding&);

            virtual
            void
            erase();

        public:
            void
            getHandle(const RefCount<AbstractDevice::Handle>&);

            inline
            AbstractDevice::Handle
            getHandle() const
            {
                return static_cast<AbstractDevice::Handle>(_handle);
            }

            template <typename HandleType> inline
            HandleType
            castHandle() const
            {
                scn::TreeNode::Ptr const& dataNode = this->dataNode().lock();

                return dataNode && compatible<HandleType>(*dataNode)
                    ? reinterpret_cast<HandleType>(getHandle())
                    : nullptr;
            }

            void
            commitFrameState();
            void
            beginSubframeCommits(size_t numSubframes);
            void
            commitSubframeState(size_t subframe);
            void
            endSubframeCommits();

        protected:
            virtual
            void
            handle(const xchg::ParmEvent&);

        protected:
            template <typename HandleType> static inline
            bool
            compatible(const scn::TreeNode&)
            {
                throw sys::Exception(
                    "Not implemented for requested Handle type.",
                    "TreeNode Compatibility Test");
            }

            template<> static inline bool compatible<AbstractDevice::SceneHandle>           (const scn::TreeNode& n){ return n.is(xchg::NodeClass::SCENE); }
            template<> static inline bool compatible<AbstractDevice::ShapeHandle>           (const scn::TreeNode& n){ return n.is(xchg::NodeClass::DRAWABLE); }
            template<> static inline bool compatible<AbstractDevice::LightHandle>           (const scn::TreeNode& n){ return n.is(xchg::NodeClass::LIGHT); }
            template<> static inline bool compatible<AbstractDevice::CameraHandle>          (const scn::TreeNode& n){ return n.is(xchg::NodeClass::CAMERA); }
            template<> static inline bool compatible<AbstractDevice::MaterialHandle>        (const scn::TreeNode& n){ return n.is(xchg::NodeClass::MATERIAL); }
            template<> static inline bool compatible<AbstractDevice::SubSurfaceHandle>      (const scn::TreeNode& n){ return n.is(xchg::NodeClass::SUBSURFACE); }
            template<> static inline bool compatible<AbstractDevice::ToneMapperHandle>      (const scn::TreeNode& n){ return n.is(xchg::NodeClass::TONEMAPPER); }
            template<> static inline bool compatible<AbstractDevice::RenderPassHandle>      (const scn::TreeNode& n){ return n.is(xchg::NodeClass::RENDERPASS); }
            template<> static inline bool compatible<AbstractDevice::RendererHandle>        (const scn::TreeNode& n){ return n.is(xchg::NodeClass::RENDERER); }
            template<> static inline bool compatible<AbstractDevice::FrameBufferHandle>     (const scn::TreeNode& n){ return n.is(xchg::NodeClass::FRAMEBUFFER); }
            template<> static inline bool compatible<AbstractDevice::IntegratorHandle>      (const scn::TreeNode& n){ return n.is(xchg::NodeClass::INTEGRATOR); }
            template<> static inline bool compatible<AbstractDevice::SamplerFactoryHandle>  (const scn::TreeNode& n){ return n.is(xchg::NodeClass::SAMPLER_FACTORY); }
            template<> static inline bool compatible<AbstractDevice::PixelFilterHandle>     (const scn::TreeNode& n){ return n.is(xchg::NodeClass::PIXEL_FILTER); }
            template<> static inline bool compatible<AbstractDevice::DenoiserHandle>        (const scn::TreeNode& n){ return n.is(xchg::NodeClass::DENOISER); }
        private:
            void
            clearParmUpdates();
            void
            addParmUpdate(const Client&, const xchg::ParmEvent&);
            void
            applyAndFlushParmUpdates(Client&, ParmEventMap&) const;
        };
    }
}
