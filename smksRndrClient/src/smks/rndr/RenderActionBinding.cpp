// ========================================================================= //
// Copyright 2016-2018 SUPAMONKS_STUDIO                                      //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
//                                                                           //
// ========================================================================= //
#include <smks/sys/Log.hpp>

#include <smks/parm/BuiltIn.hpp>
#include <smks/parm/BuiltIns.hpp>
#include <smks/scn/TreeNode.hpp>

#include "smks/rndr/Client.hpp"
#include "RenderActionBinding.hpp"

using namespace smks;

// static
rndr::RenderActionBinding::Ptr
rndr::RenderActionBinding::create(ClientBase::WPtr const&   client,
                                   TreeNodeWPtr const&              node)
{
    Ptr ptr(new RenderActionBinding(client, node));
    ptr->build(ptr);
    return ptr;
}

rndr::RenderActionBinding::RenderActionBinding(ClientBase::WPtr const&  client,
                                                TreeNodeWPtr const&             node):
    ClientBindingBase(client, node)
{ }

void
rndr::RenderActionBinding::handle(const xchg::ParmEvent& evt)
{
    scn::TreeNode::Ptr const&   node    = dataNode().lock();
    Client::Ptr const&      client  = std::dynamic_pointer_cast<Client>(
        this->client().lock());

    if (!node || !client)
        return;

    if (evt.parm() == parm::rendering().id())
    {
        char state = xchg::INACTIVE;

        node->getParameter<char>(parm::rendering(), state);
        if (state == xchg::ACTIVE)
            client->render(node->id());
    }
}
