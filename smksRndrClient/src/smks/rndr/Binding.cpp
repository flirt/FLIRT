// ========================================================================= //
// Copyright 2016-2018 SUPAMONKS_STUDIO                                      //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
//                                                                           //
// ========================================================================= //
#include <smks/sys/Log.hpp>

#include <smks/parm/BuiltIn.hpp>
#include <smks/parm/BuiltIns.hpp>
#include <smks/xchg/ParmEvent.hpp>
#include <smks/xchg/Data.hpp>
#include <smks/xchg/DataStream.hpp>
#include <smks/xchg/BoundingBox.hpp>
#include <smks/xchg/ObjectPtr.hpp>
#include <smks/scn/TreeNode.hpp>
#include <smks/scn/Scene.hpp>
#include <smks/scn/Drawable.hpp>
#include <smks/scn/Shader.hpp>
#include <smks/scn/RtNode.hpp>

#include "Binding.hpp"
#include "handle_type.hpp"
#include "smks/rndr/Client.hpp"

using namespace smks;

////////////////////////
// class Binding
////////////////////////
// static
rndr::Binding::Ptr
rndr::Binding::create(ClientBase::WPtr const&       client,
                      scn::TreeNode::WPtr const&    dataNode,
                      AbstractDevice*               device)
{
    Ptr ptr(new Binding(client, dataNode, device));
    ptr->build(ptr);
    return ptr;
}

rndr::Binding::Binding(ClientBase::WPtr const&      client,
                       scn::TreeNode::WPtr const&   dataNode,
                       AbstractDevice*              device):
    ClientBindingBase       (client, dataNode),
    _handle             (sys::null, device),
    _perFrameParmUpdates    (),
    _perSubframeParmUpdates ()
{ }

// static
void
rndr::Binding::destroy(Ptr& ptr)
{
    if (ptr)
    {
        ptr->erase();
        ptr = nullptr;
    }
}

// virtual
void
rndr::Binding::erase()
{
    _handle = sys::null;
    clearParmUpdates();
    //---
    ClientBindingBase::erase();
}

void
rndr::Binding::clearParmUpdates()
{
    _perFrameParmUpdates.clear();
    _perSubframeParmUpdates.clear();
}

void
rndr::Binding::addParmUpdate(const Client&      client,
                             const xchg::ParmEvent& evt)
{
    if (!_handle)
        return;

    _perFrameParmUpdates[evt.parm()] = evt;
    // if the event is about a parameter whose changes must be tracked across
    // subframes, then it must ALSO be stored in order to perform a relevant
    // per subframe commit.
    if (client.perSubframeParameter(_handle, evt.parm()))
        _perSubframeParmUpdates[evt.parm()] = evt;
}

void
rndr::Binding::getHandle(const RefCount<AbstractDevice::Handle>& value)
{
    _handle = value;

    scn::TreeNode::Ptr const&   node    = dataNode().lock();
    Client::Ptr const&      client  = std::dynamic_pointer_cast<Client>(
        this->client().lock());

    if (!node || !client)
        return;

    // trigger updates for all user-defined parameters
    // and relevant parameters of the node
    xchg::IdSet relevantParms, currentParms, parms;

    node->getRelevantParameters(relevantParms);
    node->getCurrentParameters(currentParms);
    xchg::IdSet::set_union(relevantParms, currentParms, parms);

    clearParmUpdates();
    for (xchg::IdSet::const_iterator id = parms.begin(); id != parms.end(); ++id)
    {
        parm::BuiltIn::Ptr const&   parm = parm::builtIns().find(*id);
        xchg::ParmEvent             evt (
            node->parameterExists(*id) ? xchg::ParmEvent::PARM_ADDED : xchg::ParmEvent::PARM_REMOVED,
            *id,
            node->id(),
            parm ? parm->flags() : parm::NO_PARM_FLAG);
        addParmUpdate(*client, evt);
    }
}

// virtual
void
rndr::Binding::handle(const xchg::ParmEvent& evt)
{
    if (!evt)
        throw sys::Exception(
            "Invalid parameter event.",
            "Rt Binding's Parameter Event Handler");

    scn::TreeNode::Ptr const&   node    = dataNode().lock();
    Client::Ptr const&          client  = std::dynamic_pointer_cast<Client>(
        this->client().lock());

    if (!node || !client)
        return;

    //-----------------------------
    // RECORD PARAMETER VALUE CHANGE
    //-----------------------------
    if (evt.node() == node->id() ||
        node->isParameterRelevant(evt.parm()) ||
        parm::isUserAttribute(evt.parm()))
        addParmUpdate(*client, evt);

    //---------------------------------------
    // NOTIFY CLIENT HANDLE'S TYPE MAY CHANGE
    //---------------------------------------
    if (evt.node() == node->id() &&
        parameterDictatesHandleType(evt.parm(), node->nodeClass()))
    {
        client->handleRtHandleTypeChanged(node->id());
    }
}

void
rndr::Binding::commitFrameState()
{
    Client::Ptr const& client = std::dynamic_pointer_cast<Client>(
        this->client().lock());

    if (!_handle || !client)
        return;
    //---
    applyAndFlushParmUpdates(*client, _perFrameParmUpdates);
    client->commitFrameState(_handle);
}

void
rndr::Binding::beginSubframeCommits(size_t numSubframes)
{
    Client::Ptr const& client = std::dynamic_pointer_cast<Client>(
        this->client().lock());

    if (!_handle || !client)
        return;
    //---
    client->beginSubframeCommits(_handle, numSubframes);
}

void
rndr::Binding::commitSubframeState(size_t subframe)
{
    Client::Ptr const& client = std::dynamic_pointer_cast<Client>(
        this->client().lock());

    if (!_handle || !client)
        return;
    //---
    applyAndFlushParmUpdates(*client, _perSubframeParmUpdates);
    client->commitSubframeState(_handle, subframe);
}

void
rndr::Binding::endSubframeCommits()
{
    Client::Ptr const& client = std::dynamic_pointer_cast<Client>(
        this->client().lock());

    if (!_handle || !client)
        return;
    //---
    client->endSubframeCommits(_handle);
}

void
rndr::Binding::applyAndFlushParmUpdates(Client&         client,
                                        ParmEventMap&   updates) const
{
    if (!_handle)
        return;

#if defined(FLIRT_LOG_ENABLED)
    {
        if (!updates.empty())
        {
            scn::TreeNode::Ptr const& node = dataNode().lock();
            if (node)
            {
                std::stringstream sstr;
                sstr
                    << updates.size() << " unprocessed parameter event(s) "
                    << "for node '" << node->name() << "'";
                for (ParmEventMap::const_iterator it = updates.begin(); it != updates.end(); ++it)
                {
                    const xchg::ParmEvent&      evt     = it->second;
                    parm::BuiltIn::Ptr const&   builtIn = parm::builtIns().find(evt.parm());
                    xchg::ParameterInfo         parmInfo;

                    sstr << "\n- '" << client.getNodeName(evt.node()) << "'.";
                    if (builtIn)
                        sstr << "'" << builtIn->c_str() << "'";
                    else if (node->getParameterInfo(evt.parm(), parmInfo))
                        sstr << "'" << parmInfo.name << "' (user-defined)";
                    else
                        sstr << evt.parm() << " (unknown)";
                    sstr << "      " << evt;
                }
                LOG(DEBUG) << "\n" << sstr.str();
            }
        }
    }
#endif // defined(FLIRT_LOG_ENABLED)

    //-----------------------------------------
    // process all uncommitted parameter events
    //-----------------------------------------
    for (ParmEventMap::const_iterator it = updates.begin(); it != updates.end(); ++it)
    {
        const xchg::ParmEvent& evt = it->second;

        //-----------------------------------------
        // parameter is removed
        //-----------------------------------------
        if (evt.type() == xchg::ParmEvent::PARM_REMOVED)
        {
            if (evt.node() == id() ||
                !client.nodeParameterExists(evt.parm(), id()))
                client.unset(_handle, evt.parm());
        }

        //-----------------------------------------
        // parameter is either added or updated
        //-----------------------------------------
        else
        {
            xchg::ParameterInfo         parmInfo;
            parm::BuiltIn::Ptr const&   builtIn = parm::builtIns().find(evt.parm());

            if (builtIn)
            {
//----------------------------------------------------------------------------------
#define RT_SET_BUILTIN_PARM(_CTYPE_, _RT_SET_)                                      \
    else if (builtIn->compatibleWith<_CTYPE_>())                                    \
    {                                                                               \
        unsigned int node = evt.node();                                             \
        if (evt.node() != id())                                                     \
        {                                                                           \
            /* if updated parameter is defined at scene scale,          */          \
            /* then it must always be queried by the node               */          \
            /* and not the node associated with caught event.           */          \
            if (builtIn->flags() & parm::ParmFlags::NEEDS_SCENE)                    \
                node = id();                                                        \
            /* also, prevent updates caused by other nodes              */          \
            /* whenever parameter is directly defined on node itself    */          \
            else if (client.nodeParameterExistsAs<_CTYPE_>(evt.parm(), id()))       \
                node = id();                                                        \
        }                                                                           \
        _CTYPE_ parmValue;                                                          \
        client.getNodeParameter(*builtIn, parmValue, node);                         \
        client._RT_SET_(_handle, builtIn->c_str(), parmValue);                  \
    }
//----------------------------------------------------------------------------------

                if (false) {} // left on purpose
                RT_SET_BUILTIN_PARM(bool,               setBool)
                RT_SET_BUILTIN_PARM(char,               setChar)
                RT_SET_BUILTIN_PARM(unsigned int,       setUInt)
                RT_SET_BUILTIN_PARM(size_t,             setUInt64)
                RT_SET_BUILTIN_PARM(int,                setInt1)
                RT_SET_BUILTIN_PARM(math::Vector2i,     setInt2)
                RT_SET_BUILTIN_PARM(math::Vector3i,     setInt3)
                RT_SET_BUILTIN_PARM(math::Vector4i,     setInt4)
                RT_SET_BUILTIN_PARM(float,              setFloat1)
                RT_SET_BUILTIN_PARM(math::Vector2f,     setFloat2)
                RT_SET_BUILTIN_PARM(math::Vector4f,     setFloat4)
                RT_SET_BUILTIN_PARM(math::Affine3f,     setTransform)
                RT_SET_BUILTIN_PARM(std::string,        setString)
                RT_SET_BUILTIN_PARM(xchg::IdSet,        setIdSet)
                RT_SET_BUILTIN_PARM(xchg::BoundingBox,  setBounds)
                RT_SET_BUILTIN_PARM(xchg::DataStream,   setDataStream)
                RT_SET_BUILTIN_PARM(xchg::Data::Ptr,    setData)
                RT_SET_BUILTIN_PARM(xchg::ObjectPtr,    setObjectPtr)
                else
                {
                    std::stringstream sstr;
                    sstr
                        << "Unsupported type for updating built-in parameter '" << builtIn->c_str() << "' "
                        << "on FLIRT handle of node '" << client.getNodeName(id()) << "'.";
                    FLIRT_LOG(LOG(DEBUG) << sstr.str();)
                    std::cerr << sstr.str();
                    //throw sys::Exception(sstr.str().c_str(), "Commit RT Handle Binding");
                }
            }

            else if (client.getNodeParameterInfo(evt.parm(), parmInfo, evt.node()))
            {
//----------------------------------------------------------------------------------
#define RT_SET_PARM(_CTYPE_, _RT_SET_)                                              \
    else if (*parmInfo.typeinfo == typeid(_CTYPE_))                                 \
    {                                                                               \
        unsigned int node = evt.node();                                             \
        if (evt.node() != id())                                                     \
        {                                                                           \
            /* if updated parameter is defined at scene scale,          */          \
            /* then it must always be queried by the node               */          \
            /* and not the node associated with caught event.           */          \
            if (parmInfo.flags & parm::ParmFlags::NEEDS_SCENE)                      \
                node = id();                                                        \
            /* also, prevent updates caused by other nodes              */          \
            /* whenever parameter is directly defined on node itself    */          \
            else if (client.nodeParameterExistsAs<_CTYPE_>(evt.parm(), id()))       \
                node = id();                                                        \
        }                                                                           \
        _CTYPE_ parmValue;                                                          \
        client.getNodeParameter<_CTYPE_>(evt.parm(), parmValue, _CTYPE_(), node);   \
        client._RT_SET_(_handle, parmInfo.name, parmValue);                     \
    }
//----------------------------------------------------------------------------------

                // if updated parameter is defined at scene scale,
                // then it must always be queried by the node and
                // not the node associated with caught event.
                const bool isGlobal = parmInfo.flags & parm::ParmFlags::NEEDS_SCENE;

                if (false) {} // left on purpose
                RT_SET_PARM(bool,               setBool)
                RT_SET_PARM(char,               setChar)
                RT_SET_PARM(unsigned int,       setUInt)
                RT_SET_PARM(size_t,             setUInt64)
                RT_SET_PARM(int,                setInt1)
                RT_SET_PARM(math::Vector2i,     setInt2)
                RT_SET_PARM(math::Vector3i,     setInt3)
                RT_SET_PARM(math::Vector4i,     setInt4)
                RT_SET_PARM(float,              setFloat1)
                RT_SET_PARM(math::Vector2f,     setFloat2)
                RT_SET_PARM(math::Vector4f,     setFloat4)
                RT_SET_PARM(math::Affine3f,     setTransform)
                RT_SET_PARM(std::string,        setString)
                RT_SET_PARM(xchg::IdSet,        setIdSet)
                RT_SET_PARM(xchg::BoundingBox,  setBounds)
                RT_SET_PARM(xchg::DataStream,   setDataStream)
                RT_SET_PARM(xchg::Data::Ptr,    setData)
                RT_SET_PARM(xchg::ObjectPtr,    setObjectPtr)
                else
                {
                    std::stringstream sstr;
                    sstr
                        << "Unsupported type for updating custom parameter '" << parmInfo.name << "' "
                        << "on FLIRT handle of node '" << client.getNodeName(id()) << "'.";
                    FLIRT_LOG(LOG(DEBUG) << sstr.str();)
                    std::cerr << sstr.str();
                    //throw sys::Exception(sstr.str().c_str(), "Commit RT Handle Binding");
                }
            }
            else
            {
                std::stringstream sstr;
                sstr
                    << "Failed to get type of parameter ID = " << evt.parm()
                    << " on node '" << client.getNodeName(id()) << "' "
                    << "for updating FLIRT handle.";
                FLIRT_LOG(LOG(DEBUG) << sstr.str();)
                std::cerr << sstr.str();
                //---
                client.unset(_handle, evt.parm());
            }
        }
    }
    updates.clear();
}
