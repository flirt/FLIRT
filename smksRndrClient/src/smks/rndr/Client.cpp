// ========================================================================= //
// Copyright 2016-2018 SUPAMONKS_STUDIO                                      //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
//                                                                           //
// ========================================================================= //
#include <set>
#include <boost/unordered_set.hpp>

#include <json/value.h>
#include <json/writer.h>

#include <smks/sys/version.hpp>
#include <smks/sys/compile_info.hpp>

#include <std/to_string_xchg.hpp>
#include <smks/xchg/SceneEvent.hpp>
#include <smks/util/string/path.hpp>
#include <smks/sys/Log.hpp>
#include <smks/img/Image.hpp>
#include <smks/img/OutputImage.hpp>
#include <smks/math/math.hpp>

#include <smks/parm/BuiltIns.hpp>
#include <smks/parm/BuiltInMap.hpp>
#include <smks/xchg/NodeCode.hpp>
#include <smks/xchg/NodeCodes.hpp>
#include <smks/xchg/Data.hpp>
#include <smks/xchg/ObjectPtr.hpp>
#include <smks/xchg/Vector.hpp>

#include <smks/scn/Scene.hpp>
#include <smks/scn/PolyMesh.hpp>
#include <smks/scn/Curves.hpp>
#include <smks/scn/Light.hpp>
#include <smks/scn/Material.hpp>
#include <smks/scn/SubSurface.hpp>
#include <smks/scn/Texture.hpp>
#include <smks/scn/Camera.hpp>
#include <smks/scn/Xform.hpp>
#include <smks/scn/Denoiser.hpp>
#include <smks/scn/ToneMapper.hpp>
#include <smks/scn/Integrator.hpp>
#include <smks/scn/SamplerFactory.hpp>
#include <smks/scn/PixelFilter.hpp>
#include <smks/scn/RenderPass.hpp>
#include <smks/scn/Renderer.hpp>
#include <smks/scn/RenderAction.hpp>
#include <smks/scn/FrameBuffer.hpp>
#include <smks/scn/TreeNodeSelector.hpp>
#include <smks/scn/TreeNodeVisitor.hpp>
#include <smks/scn/TreeNodeFilter.hpp>

#include <smks/client_node_filters.hpp>

#include <smks/rndr/AbstractDevice.hpp>

#include "RefCount.hpp"
#include "Binding.hpp"
#include "RenderActionBinding.hpp"
#include "handle_type.hpp"
#include "../sys/device_helpers.hpp"
#include "smks/rndr/Client.hpp"

namespace smks { namespace rndr
{
    ///////////////////////////
    // class Client::PImpl
    ///////////////////////////
    class Client::PImpl
    {
    private:
        typedef boost::unordered_map<unsigned int, std::string>                     NodeToHandleType;
        typedef boost::unordered_map<unsigned int, AbstractDevice::PrimitiveHandle> PrimitiveHandles;
    private:
        Client&             _self;
        const Config        _cfg;
        AbstractDevice*     _device;
        NodeToHandleType    _handleType;
        PrimitiveHandles    _primitives;

    public:
        PImpl(Client&     self,
              const char* jsonCfg):
            _self       (self),
            _cfg        (jsonCfg),
            _device     (nullptr),
            _handleType (),
            _primitives ()
        { }

        inline
        ~PImpl()
        {
            dispose();
        }

        inline
        void
        build(Client::Ptr const&,
              const char* jsonCfg);

        inline
        void
        dispose();

        inline
        ClientBindingBase::Ptr
        createBinding(scn::TreeNode::WPtr const&);

        inline
        void
        destroyBinding(ClientBindingBase::Ptr const&);

        inline
        void
        handleSceneEvent(scn::Scene&, const xchg::SceneEvent&);

        inline
        void
        handleSceneParmEvent(const xchg::ParmEvent&);

        inline
        void
        handleRtHandleTypeChanged(unsigned int nodeId);

        template<typename Type> inline
        RefCount<Type>
        createHandle() const;

        template<typename Type> inline
        RefCount<Type>
        createHandle(const Type) const;

        inline unsigned int setBool         (AbstractDevice::Handle, const char*, bool);
        inline unsigned int setChar         (AbstractDevice::Handle, const char*, char);
        inline unsigned int setUInt         (AbstractDevice::Handle, const char*, unsigned int);
        inline unsigned int setUInt64       (AbstractDevice::Handle, const char*, size_t);
        inline unsigned int setInt1         (AbstractDevice::Handle, const char*, int);
        inline unsigned int setInt2         (AbstractDevice::Handle, const char*, const math::Vector2i&);
        inline unsigned int setInt3         (AbstractDevice::Handle, const char*, const math::Vector3i&);
        inline unsigned int setInt4         (AbstractDevice::Handle, const char*, const math::Vector4i&);
        inline unsigned int setFloat1       (AbstractDevice::Handle, const char*, float);
        inline unsigned int setFloat2       (AbstractDevice::Handle, const char*, const math::Vector2f&);
        inline unsigned int setFloat4       (AbstractDevice::Handle, const char*, const math::Vector4f&);
        inline unsigned int setTransform    (AbstractDevice::Handle, const char*, const math::Affine3f&);
        inline unsigned int setString       (AbstractDevice::Handle, const char*, const std::string&);
        inline unsigned int setIdSet        (AbstractDevice::Handle, const char*, xchg::IdSet const&);
        inline unsigned int setBounds       (AbstractDevice::Handle, const char*, const xchg::BoundingBox&);
        inline unsigned int setDataStream   (AbstractDevice::Handle, const char*, const xchg::DataStream&);
        inline unsigned int setData         (AbstractDevice::Handle, const char*, xchg::Data::Ptr const&);
        inline unsigned int setObjectPtr    (AbstractDevice::Handle, const char*, xchg::ObjectPtr const&);
        inline void         unset           (AbstractDevice::Handle, unsigned int);

        inline bool link    (unsigned int parmId, const ClientBindingBase& target, const ClientBindingBase& source);
        inline bool unlink  (unsigned int parmId, const ClientBindingBase& target, const ClientBindingBase& source);

        //inline void add   (AbstractDevice::FrameBufferHandle, AbstractDevice::RenderPassHandle);
        //inline void remove(AbstractDevice::FrameBufferHandle, AbstractDevice::RenderPassHandle);

        inline void commitFrameState    (AbstractDevice::Handle);
        inline void beginSubframeCommits(AbstractDevice::Handle, size_t numSubframes);
        inline void commitSubframeState (AbstractDevice::Handle, size_t subframe);
        inline void endSubframeCommits  (AbstractDevice::Handle);
        inline bool perSubframeParameter(AbstractDevice::Handle, unsigned int) const;
    private:
        inline
        bool
        supportsMultipleSubframes(AbstractDevice::Handle) const;

    public:
        void
        render(unsigned int renderActionId);

    private:
        inline
        AbstractDevice::PrimitiveHandle
        createPrimitiveHandle(const scn::TreeNode&);

        inline
        void
        destroyPrimitiveHandle(unsigned int);

        inline
        AbstractDevice::Handle
        createHandle(const scn::TreeNode&, std::string& type);

        template<typename HandleType> inline
        HandleType
        castHandle(unsigned int) const;

        inline
        Binding::Ptr
        createHandleBinding(const scn::TreeNode&) const;

        inline
        unsigned int
        getTypedAscendant(unsigned int, xchg::NodeClass, bool allowSelf) const;

        inline
        void
        accumulateNodeAndSources(unsigned int, xchg::IdSet&) const;

        inline
        void
        getRenderingTimeSteps(unsigned int rendererId,
                              unsigned int sceneId,
                              std::vector<float>&) const;

        inline
        void
        getSampleTimesInRange(float, float, std::vector<float>&) const;

        inline
        void
        commitBindingFrameStates(const xchg::IdSet&);
        inline
        void
        commitBindingSubframeStates(const xchg::IdSet&, size_t subframe);

    private:
        //<! returns the ID->fullname mapping for the specified node types as a JSON value
        inline
        void
        getNodeManifest(xchg::NodeClass, Json::Value&) const;

        inline
        void
        mapDrawableToShaders(Json::Value&) const;

        inline
        void
        mapDrawableToSelections(Json::Value&) const;

        inline
        void
        processDeviceLinkBatch(bool                 destroyLink,
                               const Binding&       target,
                               const xchg::IdSet&   sources);
        inline
        void
        processDeviceLinkBatch(bool                 destroyLink,
                               const xchg::IdSet&   targets,
                               const Binding&       source);

    private:
        static inline
        void
        logLink(bool            destroyLink,
                unsigned int    parmId,
                const ClientBindingBase&,
                const ClientBindingBase&);

    private:
        //////////////////////////////////
        // struct AbstractBindingOperation
        //////////////////////////////////
        struct AbstractBindingOperation
        {
        public:
            virtual
            ~AbstractBindingOperation()
            { }

            virtual
            void
            apply(rndr::Binding&) const = 0;
        };
        //////////////////////////////////
        inline
        void
        forEachBinding(const xchg::IdSet&, const AbstractBindingOperation*);
    };
}
}

using namespace smks;

//////////////////////////
// class Client::PImpl
//////////////////////////
void
rndr::Client::PImpl::build(Client::Ptr const&   ptr,
                           const char*          jsonCfg)
{
    dispose();

    assert(ptr);
    _device = sys::createDeviceHelper(
        _cfg.deviceModule(),
        jsonCfg,
        ptr);

    if (_device == nullptr)
        throw sys::Exception(
            "Failed to create rendering device.",
            "Render Client Building");
}

void
rndr::Client::PImpl::dispose()
{
    _self.setScene(nullptr);

    while (!_primitives.empty())
        destroyPrimitiveHandle(_primitives.begin()->first);

    sys::destroyDeviceHelper(
        _cfg.deviceModule(),
        _device);
    _device = nullptr;
}

rndr::AbstractDevice::PrimitiveHandle
rndr::Client::PImpl::createPrimitiveHandle(const scn::TreeNode& n)
{
    assert(_device);

    AbstractDevice::PrimitiveHandle primHdl = nullptr;

    // create primitive (but does not set its base RT handle yet:
    // it will be set by createHandle() once the primitive has
    // been registered)
    if (n.nodeClass() == xchg::MESH ||
        n.nodeClass() == xchg::CURVES)
        primHdl = _device->newPrimitive("shape");
    else if (n.nodeClass() == xchg::LIGHT)
        primHdl = _device->newPrimitive("light");

    // add primitive to scene
    if (primHdl)
    {
        assert(n.root().lock() && n.root().lock()->id() == _self.sceneId());
        AbstractDevice::SceneHandle sceneHdl = castHandle<AbstractDevice::SceneHandle>(_self.sceneId());
        if (sceneHdl)
            _device->add(sceneHdl, primHdl);
    }

    return primHdl;
}

void
rndr::Client::PImpl::destroyPrimitiveHandle(unsigned int nodeId)
{
    assert(_device);

    PrimitiveHandles::iterator const& it = _primitives.find(nodeId);
    if (it == _primitives.end())
        return;

    AbstractDevice::PrimitiveHandle primHdl = it->second;
    assert(primHdl);

    // remove primitive from scene
    AbstractDevice::SceneHandle sceneHdl = castHandle<AbstractDevice::SceneHandle>(_self.sceneId());
    if (sceneHdl)
        _device->remove(sceneHdl, primHdl);
    // destroy primitive
    _device->destroy(primHdl);

    _primitives.erase(it);
}

template<typename Type> inline
rndr::RefCount<Type>
rndr::Client::PImpl::createHandle() const
{
    assert(_device);
    return RefCount<Type>(_device);
}

template<typename Type> inline
rndr::RefCount<Type>
rndr::Client::PImpl::createHandle(const Type t) const
{
    assert(_device);
    return RefCount<Type>(t, _device);
}

ClientBindingBase::Ptr
rndr::Client::PImpl::createBinding(scn::TreeNode::WPtr const& n)
{
    scn::TreeNode::Ptr const& node = n.lock();
    if (!node)
        return nullptr;

    // register node for local-to-world matrix computation
    if (node->asGraphAction() &&
        node->asGraphAction()->asRenderAction())
        return RenderActionBinding::create(_self.self(), n);

    else
    {
        // create RT primitive if node requires it
        AbstractDevice::PrimitiveHandle primHdl = createPrimitiveHandle(*node);
        if (primHdl)
            _primitives[node->id()] = primHdl;  // register RT primitive prior RT handle creation

        std::string             type;
        AbstractDevice::Handle  getHandle = createHandle(*node, type);

        if (!getHandle)
            return nullptr;
        _handleType[node->id()] = type;

        Binding::Ptr getHandleBind = createHandleBinding(*node);

        getHandleBind->getHandle(RefCount<AbstractDevice::Handle>(getHandle, _device));

        return getHandleBind;
    }
}

rndr::Binding::Ptr
rndr::Client::PImpl::createHandleBinding(const scn::TreeNode& node) const
{
    return Binding::create(_self.self(), node.self(), _device);
}

rndr::AbstractDevice::Handle
rndr::Client::PImpl::createHandle(const scn::TreeNode&  node,
                                  std::string&          type)
{
    assert(_device);

    AbstractDevice::Handle getHandle = nullptr;

    // deternmine the 'type' argument for the RT handle creator
    getHandleType(_self, node.id(), type);
    // call RT handle creator depending on node's type
    if (node.asScene())
        getHandle = _device->newScene(type.c_str(), node.id());
    else if (node.asSceneObject())
    {
        const scn::SceneObject* scnObj = node.asSceneObject();
        if (scnObj->asDrawable())
        {
            const scn::Drawable* drw = scnObj->asDrawable();
            if (drw->asPolyMesh() ||
                drw->asCurves())
                getHandle = _device->newShape(type.c_str(), node.id());
        }
        else if (scnObj->asLight())
            getHandle = _device->newLight(type.c_str(), node.id());
        else if (scnObj->asCamera())
            getHandle = _device->newCamera(type.c_str(), node.id());
    }
    else if (node.asShader())
    {
        const scn::Shader* shader = node.asShader();
        if (shader->asMaterial())
            getHandle = _device->newMaterial(type.c_str(), node.id());
        else if (shader->asSubSurface())
            getHandle = _device->newSubSurface(type.c_str(), node.id());
        else if (shader->asTexture())
            getHandle = _device->newTexture(type.c_str(), node.id());
    }
    else if (node.asGraphAction())
    {
        if (node.asGraphAction()->asTreeNodeSelector())
            getHandle = _device->newSelection(type.c_str(), node.id());
    }
    else if (node.asRtNode())
    {
        const scn::RtNode* rtNode = node.asRtNode();
        if (rtNode->asToneMapper())
            getHandle = _device->newToneMapper(type.c_str(), node.id());
        else if (rtNode->asDenoiser())
            getHandle = _device->newDenoiser(type.c_str(), node.id());
        else if (rtNode->asRenderPass())
            getHandle = _device->newRenderPass(type.c_str(), node.id());
        else if (rtNode->asRenderer())
            getHandle = _device->newRenderer(type.c_str(), node.id());
        else if (rtNode->asFrameBuffer())
            getHandle = _device->newFrameBuffer(type.c_str(), node.id());
        else if (rtNode->asIntegrator())
            getHandle = _device->newIntegrator(type.c_str(), node.id());
        else if (rtNode->asSamplerFactory())
            getHandle = _device->newSamplerFactory(type.c_str(), node.id());
        else if (rtNode->asPixelFilter())
            getHandle = _device->newPixelFilter(type.c_str(), node.id());
    }

#if defined (FLIRT_LOG_ENABLED)
    {
        std::stringstream sstr;
        if (getHandle)
            sstr
                << "Handle of type '" << type << "' "
                << "created (ptr= " << (void*)getHandle << ") "
                << "for node '" << node.name() << "' "
                << "(ID = " << node.id() << ", " << std::to_string(node.nodeClass()) << ")";
        else
            sstr
                << "no Handle created "
                << "for node '" << node.name() << "' "
                << "(ID = " << node.id() << ", " << std::to_string(node.nodeClass()) << ")";
        LOG(DEBUG) << sstr.str();
    }
#endif // defined (FLIRT_LOG_ENABLED)

    if (getHandle)
    {
        // if RT primitive associated to node, must set RT handle as part of it.
        PrimitiveHandles::iterator const& it = _primitives.find(node.id());
        if (it != _primitives.end())
        {
            if (node.nodeClass() == xchg::MESH ||
                node.nodeClass() == xchg::CURVES)
                _device->setShape(it->second, reinterpret_cast<AbstractDevice::ShapeHandle>(getHandle));
            else if (node.nodeClass() == xchg::LIGHT)
                _device->setLight(it->second, reinterpret_cast<AbstractDevice::LightHandle>(getHandle));
        }
    }

    return getHandle;
}

void
rndr::Client::PImpl::destroyBinding(ClientBindingBase::Ptr const& bind)
{
    if (!bind)
        return;
    scn::TreeNode::Ptr const&   dataNode    = bind->dataNode().lock();
    const unsigned int          nodeId      = dataNode ? dataNode->id() : 0;
    FLIRT_LOG(LOG(DEBUG)
        << "destroying binding from node '" << dataNode->name() << "' "
        << "(ID = " << nodeId << ").";)

    // destroy associated primitive if any
    destroyPrimitiveHandle(nodeId);

    Binding::Ptr getHandleBind = std::dynamic_pointer_cast<Binding>(bind);
    if (getHandleBind)
    {
        _device->destroy(getHandleBind->getHandle());
        Binding::destroy(getHandleBind);
    }
    _handleType.erase(nodeId);
}

void
rndr::Client::PImpl::handleSceneEvent(scn::Scene&               scene,
                                      const xchg::SceneEvent&   evt)
{
    if (evt.type() == xchg::SceneEvent::LINK_ATTACHED ||
        evt.type() == xchg::SceneEvent::LINK_DETACHED)
    {
        ClientBindingBase::Ptr const& sourceBind = _self.getBinding(evt.source());
        ClientBindingBase::Ptr const& targetBind = _self.getBinding(evt.target());
        if (sourceBind &&
            targetBind)
        {
            if (evt.type() == xchg::SceneEvent::LINK_ATTACHED)
            {
                link(evt.parm(), *targetBind, *sourceBind);
            }
            else
            {
                unlink(evt.parm(), *targetBind, *sourceBind);
            }
        }
    }
}

void
rndr::Client::PImpl::handleSceneParmEvent(const xchg::ParmEvent& evt)
{ }

void
rndr::Client::PImpl::handleRtHandleTypeChanged(unsigned int nodeId)
{
    NodeToHandleType::const_iterator const& it = _handleType.find(nodeId);
    bool recreateRtHandle = false;
    if (it == _handleType.end())
        recreateRtHandle = true;    // RT handle has not been recorded anyway
    else
    {
        std::string type;

        getHandleType(_self, nodeId, type);
        recreateRtHandle = type != it->second;  // RT handle must be updated since its type changed
    }

    if (!recreateRtHandle)
        return;

    scn::TreeNode::Ptr const&       node        = _self.getNode(nodeId);
    ClientBindingBase::Ptr const&   bindbase    = _self.getBinding(nodeId);

    if (!node)
    {
        std::stringstream sstr;
        sstr << "Failed to find node with ID = " << nodeId << ".";
        throw sys::Exception(sstr.str().c_str(), "RT Handle Type Changed Handler");
    }
    if (!bindbase)
    {
        std::stringstream sstr;
        sstr << "Failed to find binding associated with node '" << node->name() << "'.";
        throw sys::Exception(sstr.str().c_str(), "RT Handle Type Changed Handler");
    }

    Binding::Ptr const& bind = std::dynamic_pointer_cast<Binding>(bindbase);
    if (!bind)
        return;
    //----------------------------------------------------------------------------
    // change of RT handle triggers the re-connection of all outgoing device links
    FLIRT_LOG(LOG(DEBUG) << "changing node '"<< node->name() << "' 's Handle type...";)
    /////////////////////////
    // struct DeviceLinkBatch
    /////////////////////////
    struct DeviceLinkBatch
    {
        xchg::IdSet     links;
        Binding::Ptr    bind;
    };
    typedef std::vector<DeviceLinkBatch> DeviceLinkBatches;
    /////////////////////////

    // 1. gather currently active linking info about the node whose RT handle is about to be replaced.
    xchg::IdSet         ids;
    DeviceLinkBatches   targets, sources;

    node->getLinkTargets(ids);
    targets.reserve(ids.size());
    for (xchg::IdSet::const_iterator id = ids.begin(); id != ids.end(); ++id)
    {
        Binding::Ptr bind = std::dynamic_pointer_cast<Binding>(
            _self.getBinding(*id));

        if (!bind)
            continue;
        targets.push_back(DeviceLinkBatch());
        targets.back().bind = bind;
        _self.getLinksBetweenNodes(*id, node->id(), targets.back().links);
    }

    node->getLinkSources(ids);
    sources.reserve(ids.size());
    for (xchg::IdSet::const_iterator id = ids.begin(); id != ids.end(); ++id)
    {
        Binding::Ptr bind = std::dynamic_pointer_cast<Binding>(
            _self.getBinding(*id));

        if (!bind)
            continue;
        sources.push_back(DeviceLinkBatch());
        sources.back().bind = bind;
        _self.getLinksBetweenNodes(node->id(), *id, sources.back().links);
    }

    // 2. disconnect all device links involving obsolete RT handle
    for (size_t i = 0; i < targets.size(); ++i)
    {
        const xchg::IdSet& links = targets[i].links;
        for (xchg::IdSet::const_iterator lnk = links.begin(); lnk != links.end(); ++lnk)
            unlink(*lnk, *targets[i].bind, *bind);
    }
    for (size_t i = 0; i < sources.size(); ++i)
    {
        const xchg::IdSet& links = sources[i].links;
        for (xchg::IdSet::const_iterator lnk = links.begin(); lnk != links.end(); ++lnk)
            unlink(*lnk, *bind, *sources[i].bind);
    }

    // 3. destroy obsolete RT handle
    _device->destroy(bind->getHandle());
    _handleType.erase(nodeId);

    // 4. create new RT handle and update node's RT binding
    {
        std::string                 type;
        AbstractDevice::Handle  newRTHandle = createHandle(*node, type);

        if (!newRTHandle)
            return;
        _handleType.insert(std::pair<unsigned int, std::string>(nodeId, type));
        bind->getHandle(RefCount<AbstractDevice::Handle>(newRTHandle, _device));
        //<! cause all relevant parameters and user-defined parameters be flagged as dirty
    }

    // 5. reconnect all device links to new RT handle
    for (size_t i = 0; i < sources.size(); ++i)
    {
        const xchg::IdSet& links = sources[i].links;
        for (xchg::IdSet::const_iterator lnk = links.begin(); lnk != links.end(); ++lnk)
            link(*lnk, *bind, *sources[i].bind);
    }
    for (size_t i = 0; i < targets.size(); ++i)
    {
        const xchg::IdSet& links = targets[i].links;
        for (xchg::IdSet::const_iterator lnk = links.begin(); lnk != links.end(); ++lnk)
            link(*lnk, *targets[i].bind, *bind);
    }
    FLIRT_LOG(LOG(DEBUG) << "node '"<< node->name() << "' 's Handle type changed.";)
    //----------------------------------------------------------------------------
}

inline
void
rndr::Client::PImpl::processDeviceLinkBatch(bool                destroyLink,
                                            const Binding&      target,
                                            const xchg::IdSet&  sources)
{ }

inline
void
rndr::Client::PImpl::processDeviceLinkBatch(bool                destroyLink,
                                            const xchg::IdSet&  targets,
                                            const Binding&      source)
{ }

unsigned int
rndr::Client::PImpl::setBool(AbstractDevice::Handle getHandle, const char* name, bool value)
{
    return _device->setBool(getHandle, name, value);
}

unsigned int
rndr::Client::PImpl::setChar(AbstractDevice::Handle getHandle, const char* name, char value)
{
    return _device->setChar(getHandle, name, value);
}

unsigned int
rndr::Client::PImpl::setUInt(AbstractDevice::Handle getHandle, const char* name, unsigned int value)
{
    return _device->setUInt(getHandle, name, value);
}

unsigned int
rndr::Client::PImpl::setUInt64(AbstractDevice::Handle getHandle, const char* name, size_t value)
{
    return _device->setUInt64(getHandle, name, value);
}

unsigned int
rndr::Client::PImpl::setInt1(AbstractDevice::Handle getHandle, const char* name, int value)
{
    return _device->setInt1(getHandle, name, value);
}

unsigned int
rndr::Client::PImpl::setInt2(AbstractDevice::Handle getHandle, const char* name, const math::Vector2i& value)
{
    return _device->setInt2(getHandle, name, value);
}

unsigned int
rndr::Client::PImpl::setInt3(AbstractDevice::Handle getHandle, const char* name, const math::Vector3i& value)
{
    return _device->setInt3(getHandle, name, value);
}

unsigned int
rndr::Client::PImpl::setInt4(AbstractDevice::Handle getHandle, const char* name, const math::Vector4i& value)
{
    return _device->setInt4(getHandle, name, value);
}

unsigned int
rndr::Client::PImpl::setFloat1(AbstractDevice::Handle getHandle, const char* name, float value)
{
    return _device->setFloat1(getHandle, name, value);
}

unsigned int
rndr::Client::PImpl::setFloat2(AbstractDevice::Handle getHandle, const char* name, const math::Vector2f&  value)
{
    return _device->setFloat2(getHandle, name, value);
}

unsigned int
rndr::Client::PImpl::setFloat4(AbstractDevice::Handle getHandle, const char* name, const math::Vector4f& value)
{
    return _device->setFloat4(getHandle, name, value);
}

unsigned int
rndr::Client::PImpl::setTransform(AbstractDevice::Handle getHandle, const char* name, const math::Affine3f& value)
{
    return _device->setTransform(getHandle, name, value);
}

unsigned int
rndr::Client::PImpl::setString(AbstractDevice::Handle getHandle, const char* name, const std::string& value)
{
    return _device->setString(getHandle, name, value.c_str());
}

unsigned int
rndr::Client::PImpl::setIdSet(AbstractDevice::Handle getHandle, const char* name, const xchg::IdSet& value)
{
    return _device->setIdSet(getHandle, name, value);
}

unsigned int
rndr::Client::PImpl::setBounds(AbstractDevice::Handle getHandle, const char* name, const xchg::BoundingBox& value)
{
    return _device->setBounds(getHandle, name, value);
}

unsigned int
rndr::Client::PImpl::setDataStream(AbstractDevice::Handle getHandle, const char* name, const xchg::DataStream& value)
{
    return _device->setDataStream(getHandle, name, value);
}

unsigned int
rndr::Client::PImpl::setData(AbstractDevice::Handle getHandle, const char* name, xchg::Data::Ptr const& value)
{
    return _device->setData(getHandle, name, value);
}

unsigned int
rndr::Client::PImpl::setObjectPtr(AbstractDevice::Handle getHandle, const char* name, xchg::ObjectPtr const& value)
{
    return _device->setObjectPtr(getHandle, name, value);
}

void
rndr::Client::PImpl::unset(AbstractDevice::Handle getHandle, unsigned int parmId)
{
    _device->unset(getHandle, parmId);
}

void
rndr::Client::PImpl::commitFrameState(AbstractDevice::Handle getHandle)
{
    _device->commitFrameState(getHandle);
}

void
rndr::Client::PImpl::beginSubframeCommits(AbstractDevice::Handle    getHandle,
                                          size_t                    numSubframes)
{
    _device->beginSubframeCommits(
        getHandle,
        numSubframes == 1 || supportsMultipleSubframes(getHandle)
            ? numSubframes
            : 1);
}

void
rndr::Client::PImpl::commitSubframeState(AbstractDevice::Handle getHandle,
                                         size_t                 subframe)
{
    if (subframe == 0 || supportsMultipleSubframes(getHandle))
        _device->commitSubframeState(getHandle, subframe);
}

void
rndr::Client::PImpl::endSubframeCommits(AbstractDevice::Handle getHandle)
{
    _device->endSubframeCommits(getHandle);
}

bool
rndr::Client::PImpl::supportsMultipleSubframes(AbstractDevice::Handle getHandle) const
{
    const unsigned int id = _device->getScnId(getHandle);
    if (id == 0)
        return false;

    size_t parm1l = 0;

    // if node does not carry stored times, then it is useless to duplicate the same information
    // for mulitple subframes.
    _self.getNodeParameter<size_t>(parm::globalNumSamples(), parm1l, id);
    if (parm1l <= 1)
        return false;

    // multiple subframe handling breaks for shapes with heterogeneous topology.
    const xchg::NodeClass nodeClass = _self.getNodeClass(id);
    if ((nodeClass & xchg::DRAWABLE) && (nodeClass & ~xchg::DRAWABLE) == 0)
    {
        _self.getNodeParameter<size_t>(parm::numIndexBufferSamples(), parm1l, id);
        if (parm1l > 1)
            return false;
    }

    return true;
}

bool
rndr::Client::PImpl::perSubframeParameter(AbstractDevice::Handle    getHandle,
                                          unsigned int              parmId) const
{
    return _device->perSubframeParameter(getHandle, parmId);
}

void
rndr::Client::PImpl::logLink(bool                       destroyLink,
                             unsigned int               parmId,
                             const ClientBindingBase&   targetBind,
                             const ClientBindingBase&   sourceBind)
{
#if defined(FLIRT_LOG_ENABLED)
    {
        scn::TreeNode::Ptr const& target    = targetBind.dataNode().lock();
        scn::TreeNode::Ptr const& source    = sourceBind.dataNode().lock();
        if (!target ||
            !source)
            return;

        parm::BuiltIn::Ptr const& builtIn   = parm::builtIns().find(parmId);
        std::stringstream sstr;

        sstr
            << (!destroyLink ? "link" : "unlink") << " "
            << "'" << target->name() << "' (ID = " << target->id() << ", " << std::to_string(target->nodeClass()) << ")"
            << "\t" << (!destroyLink ? "--<--" : "--x--") << "[";
        if (builtIn)
            sstr << builtIn->c_str();
        else
            sstr << parmId;
        sstr
            << "]" << (!destroyLink ? "--<--" : "--x--") << "\t"
            << "'" << source->name() << "' (ID = " << source->id() << ", " << std::to_string(source->nodeClass()) << ")";
        LOG(DEBUG) << sstr.str();
    }
#endif // defined(FLIRT_LOG_ENABLED)
}


bool
rndr::Client::PImpl::link(unsigned int              parmId,
                          const ClientBindingBase&  target,
                          const ClientBindingBase&  source)
{
    logLink(false, parmId, target, source);

    const Binding* targetBind = dynamic_cast<const Binding*>(&target);
    const Binding* sourceBind = dynamic_cast<const Binding*>(&source);
    if (!targetBind ||
        !sourceBind)
        return false;

    AbstractDevice::Handle targetHdl = targetBind->getHandle();
    AbstractDevice::Handle sourceHdl = sourceBind->getHandle();

    if (targetHdl == nullptr ||
        sourceHdl == nullptr)
        return false;

    const xchg::NodeClass targetClass = targetBind->nodeClass();
    const xchg::NodeClass sourceClass = sourceBind->nodeClass();
    //--- ### -> DRAWABLE --------------------------------------
    if (targetClass & xchg::NodeClass::DRAWABLE)
    {
        if (sourceClass == xchg::NodeClass::MATERIAL &&
            parmId == parm::materialId().id())
        {
            PrimitiveHandles::iterator const& it = _primitives.find(target.id());
            if (it != _primitives.end() &&
                it->second)
                _device->setMaterial(
                    it->second,
                    castHandle<AbstractDevice::MaterialHandle>(source.id()));
        }
        else if (sourceClass == xchg::NodeClass::SUBSURFACE &&
            parmId == parm::subsurfaceId().id())
        {
            PrimitiveHandles::iterator const& it = _primitives.find(target.id());
            if (it != _primitives.end() &&
                it->second)
                _device->setSubSurface(
                    it->second,
                    castHandle<AbstractDevice::SubSurfaceHandle>(source.id()));
        }
    }
    //--- ### -> LIGHT -----------------------------------------
    else if (targetClass & xchg::NodeClass::LIGHT)
    {
        if (sourceClass == xchg::NodeClass::MESH &&
            parmId == parm::shapeId().id())
        {
            PrimitiveHandles::iterator const& it = _primitives.find(target.id());
            if (it != _primitives.end() &&
                it->second)
                _device->setShape(
                    it->second,
                    castHandle<AbstractDevice::ShapeHandle>(source.id()));
        }
    }
    //--- ### -> FRAMEBUFFER -----------------------------------
    else if (targetClass == xchg::NodeClass::FRAMEBUFFER)
    {
        if (sourceClass == xchg::NodeClass::RENDERPASS &&
            parmId == parm::renderPassIds().id())
        {
            _device->add(
                reinterpret_cast<AbstractDevice::FrameBufferHandle>(targetHdl),
                reinterpret_cast<AbstractDevice::RenderPassHandle>(sourceHdl));
            return true;
        }
        else if (sourceClass == xchg::NodeClass::DENOISER &&
            parmId == parm::denoiserId().id())
        {
            _device->setDenoiser(
                reinterpret_cast<AbstractDevice::FrameBufferHandle>(targetHdl),
                reinterpret_cast<AbstractDevice::DenoiserHandle>(sourceHdl));
            return true;
        }
        else if (sourceClass == xchg::NodeClass::TONEMAPPER &&
            parmId == parm::toneMapperId().id())
        {
            _device->setToneMapper(
                reinterpret_cast<AbstractDevice::FrameBufferHandle>(targetHdl),
                reinterpret_cast<AbstractDevice::ToneMapperHandle>(sourceHdl));
            return true;
        }
    }
    //--- ### -> RENDERER --------------------------------------
    else if (targetClass == xchg::NodeClass::RENDERER)
    {
        if (sourceClass == xchg::NodeClass::INTEGRATOR &&
            parmId == parm::integratorId().id())
        {
            _device->setIntegrator(
                reinterpret_cast<AbstractDevice::RendererHandle>(targetHdl),
                reinterpret_cast<AbstractDevice::IntegratorHandle>(sourceHdl));
            return true;
        }
        else if (sourceClass == xchg::NodeClass::SAMPLER_FACTORY &&
            parmId == parm::samplerFactoryId().id())
        {
            _device->setSamplerFactory(
                reinterpret_cast<AbstractDevice::RendererHandle>(targetHdl),
                reinterpret_cast<AbstractDevice::SamplerFactoryHandle>(sourceHdl));
            return true;
        }
        else if (sourceClass == xchg::NodeClass::PIXEL_FILTER &&
            parmId == parm::pixelFilterId().id())
        {
            _device->setFilter(
                reinterpret_cast<AbstractDevice::RendererHandle>(targetHdl),
                reinterpret_cast<AbstractDevice::PixelFilterHandle>(sourceHdl));
            return true;
        }
    }
    //--- ### -> MATERIAL --------------------------------------
    else if (targetClass == xchg::NodeClass::MATERIAL)
    {
        if (sourceClass == xchg::NodeClass::TEXTURE)
        {
            _device->setupMaterial(
                reinterpret_cast<AbstractDevice::MaterialHandle>(targetHdl),
                parmId,
                reinterpret_cast<AbstractDevice::TextureHandle>(sourceHdl));
            return true;
        }
        else if (sourceClass == xchg::NodeClass::MATERIAL &&
            (parmId == parm::inputId0().id() ||
             parmId == parm::inputId1().id()))
        {
            size_t idx = parmId == parm::inputId0().id()
                ? 0
                : 1;
            _device->setInput(
                reinterpret_cast<AbstractDevice::MaterialHandle>(targetHdl),
                reinterpret_cast<AbstractDevice::MaterialHandle>(sourceHdl),
                idx);
            return true;
        }
    }
    //--- ### -> SUBSURFACE ------------------------------------
    else if (targetClass == xchg::NodeClass::SUBSURFACE)
    {
        if (sourceClass == xchg::NodeClass::TEXTURE)
        {
            _device->setupSubSurface(
                reinterpret_cast<AbstractDevice::SubSurfaceHandle>(targetHdl),
                parmId,
                reinterpret_cast<AbstractDevice::TextureHandle>(sourceHdl));
            return true;
        }
    }
    //--- if not handled until now -----------------------------
    if (_device->link(parmId, targetHdl, sourceHdl))
        return true;

    return false;
}

bool
rndr::Client::PImpl::unlink(unsigned int                parmId,
                            const ClientBindingBase&    target,
                            const ClientBindingBase&    source)
{
    logLink(true, parmId, target, source);

    const Binding* targetBind = dynamic_cast<const Binding*>(&target);
    const Binding* sourceBind = dynamic_cast<const Binding*>(&source);
    if (!targetBind ||
        !sourceBind)
        return false;

    AbstractDevice::Handle targetHdl = targetBind->getHandle();
    AbstractDevice::Handle sourceHdl = sourceBind->getHandle();
    if (targetHdl == nullptr ||
        sourceHdl == nullptr)
        return false;

    const xchg::NodeClass targetClass = targetBind->nodeClass();
    const xchg::NodeClass sourceClass = sourceBind->nodeClass();
    //--- ### -> DRAWABLE --------------------------------------
    if (targetClass & xchg::NodeClass::DRAWABLE)
    {
        if (sourceClass == xchg::NodeClass::MATERIAL &&
            parmId == parm::materialId().id())
        {
            PrimitiveHandles::iterator const& it = _primitives.find(target.id());
            if (it != _primitives.end() &&
                it->second)
                _device->setMaterial(
                    it->second,
                    nullptr);
        }
        else if (sourceClass == xchg::NodeClass::SUBSURFACE &&
            parmId == parm::subsurfaceId().id())
        {
            PrimitiveHandles::iterator const& it = _primitives.find(target.id());
            if (it != _primitives.end() &&
                it->second)
                _device->setSubSurface(
                    it->second,
                    nullptr);
        }
    }
    //--- ### -> LIGHT -----------------------------------------
    else if (targetClass & xchg::NodeClass::LIGHT)
    {
        if (sourceClass == xchg::NodeClass::MESH &&
            parmId == parm::shapeId().id())
        {
            PrimitiveHandles::iterator const& it = _primitives.find(target.id());
            if (it != _primitives.end() &&
                it->second)
                _device->setShape(
                    it->second,
                    nullptr);
        }
    }
    //--- ### -> FRAMEBUFFER -----------------------------------
    else if (targetClass == xchg::NodeClass::FRAMEBUFFER)
    {
        if (sourceClass == xchg::NodeClass::RENDERPASS &&
            parmId == parm::renderPassIds().id())
        {
            _device->remove(
                reinterpret_cast<AbstractDevice::FrameBufferHandle>(targetHdl),
                reinterpret_cast<AbstractDevice::RenderPassHandle>(sourceHdl));
            return true;
        }
        else if (sourceClass == xchg::NodeClass::DENOISER &&
            parmId == parm::denoiserId().id())
        {
            _device->setDenoiser(
                reinterpret_cast<AbstractDevice::FrameBufferHandle>(targetHdl),
                nullptr);
            return true;
        }
        else if (sourceClass == xchg::NodeClass::TONEMAPPER &&
            parmId == parm::toneMapperId().id())
        {
            _device->setToneMapper(
                reinterpret_cast<AbstractDevice::FrameBufferHandle>(targetHdl),
                nullptr);
            return true;
        }
    }
    //--- ### -> RENDERER --------------------------------------
    else if (targetClass == xchg::NodeClass::RENDERER)
    {
        if (sourceClass == xchg::NodeClass::INTEGRATOR &&
            parmId == parm::integratorId().id())
        {
            _device->setIntegrator(
                reinterpret_cast<AbstractDevice::RendererHandle>(targetHdl),
                nullptr);
            return true;
        }
        else if (sourceClass == xchg::NodeClass::SAMPLER_FACTORY &&
            parmId == parm::samplerFactoryId().id())
        {
            _device->setSamplerFactory(
                reinterpret_cast<AbstractDevice::RendererHandle>(targetHdl),
                nullptr);
            return true;
        }
        else if (sourceClass == xchg::NodeClass::PIXEL_FILTER &&
            parmId == parm::pixelFilterId().id())
        {
            _device->setFilter(
                reinterpret_cast<AbstractDevice::RendererHandle>(targetHdl),
                nullptr);
            return true;
        }
    }
    //--- ### -> MATERIAL --------------------------------------
    else if (targetClass == xchg::NodeClass::MATERIAL)
    {
        if (sourceClass == xchg::NodeClass::TEXTURE)
        {
            _device->setupMaterial(
                reinterpret_cast<AbstractDevice::MaterialHandle>(targetHdl),
                parmId,
                nullptr);
            return true;
        }
        else if (sourceClass == xchg::NodeClass::MATERIAL &&
            (parmId == parm::inputId0().id() ||
             parmId == parm::inputId1().id()))
        {
            size_t idx = parmId == parm::inputId0().id()
                ? 0
                : 1;
            _device->setInput(
                reinterpret_cast<AbstractDevice::MaterialHandle>(targetHdl),
                nullptr,
                idx);
            return true;
        }
    }
    //--- ### -> SUBSURFACE ------------------------------------
    else if (targetClass == xchg::NodeClass::SUBSURFACE)
    {
        if (sourceClass == xchg::NodeClass::TEXTURE)
        {
            _device->setupSubSurface(
                reinterpret_cast<AbstractDevice::SubSurfaceHandle>(targetHdl),
                parmId,
                nullptr);
            return true;
        }
    }
    //--- if not handled until now -----------------------------
    if (_device->unlink(parmId, targetHdl, sourceHdl))
        return true;

    return false;
}

void
rndr::Client::PImpl::getNodeManifest(xchg::NodeClass    cl,
                                     Json::Value&       root) const
{
    root.clear();

    xchg::IdSet ids;

    _self.getIds(ids, false, FilterClientNodeByClass::create(cl));
    for (xchg::IdSet::const_iterator id = ids.begin(); id != ids.end(); ++id)
        root[std::to_string(*id)] = _self.getNodeFullName(*id);
}

void
rndr::Client::PImpl::mapDrawableToShaders(Json::Value& root) const
{
    root.clear();

    xchg::IdSet drawables;

    _self.getIds(drawables, false, FilterClientNodeByClass::create(xchg::NodeClass::DRAWABLE));
    for (xchg::IdSet::const_iterator drawable = drawables.begin(); drawable != drawables.end(); ++drawable)
    {
        unsigned int    materialId      = 0;
        unsigned int    subsurfaceId    = 0;
        size_t          numIds          = 0;

        if (_self.getNodeParameter(parm::materialId(), materialId, *drawable))
            ++numIds;
        if (_self.getNodeParameter(parm::subsurfaceId(), subsurfaceId, *drawable))
            ++numIds;
        if (numIds > 1)
        {
            Json::Value shaders;

            shaders.append(materialId);
            shaders.append(subsurfaceId);
            root[std::to_string(*drawable)] = shaders;
        }
        else if (numIds == 1)
            root[std::to_string(*drawable)] = materialId > 0 ? materialId : subsurfaceId;
    }
}

void
rndr::Client::PImpl::mapDrawableToSelections(Json::Value& root) const
{
    root.clear();

    xchg::IdSet selectors, selection;
    boost::unordered_map<unsigned int, xchg::IdSet> drawableToSelections;

    _self.getIds(selectors, false, FilterClientNodeByClass::create(xchg::NodeClass::SELECTOR));
    for (xchg::IdSet::const_iterator selector = selectors.begin(); selector != selectors.end(); ++selector)
    {
        if (!_self.getNodeParameter(parm::selection(), selection, *selector))
            continue;
        for (xchg::IdSet::const_iterator selected = selection.begin(); selected != selection.end(); ++selected)
            if (_self.getNodeClass(*selected) & xchg::NodeClass::DRAWABLE)
                drawableToSelections[*selected].insert(*selector);
    }
    for (boost::unordered_map<unsigned int, xchg::IdSet>::const_iterator it = drawableToSelections.begin();
        it != drawableToSelections.end();
        ++it)
        if (it->second.size() == 1)
            root[std::to_string(it->first)] = *it->second.begin();
        else if (it->second.size() > 1)
        {
            Json::Value selections;

            for (xchg::IdSet::const_iterator id = it->second.begin(); id != it->second.end(); ++id)
                selections.append(*id);
            root[std::to_string(it->first)] = selections;
        }
}

void
rndr::Client::PImpl::render(unsigned int renderActionId)
{
    const size_t    BUFFER_SIZE = 512;
    char            BUFFER      [BUFFER_SIZE];

    FLIRT_LOG(LOG(DEBUG) << "render action ID = " << renderActionId;)

    scn::Scene::Ptr const& scene = _self.scene();
    if (!scene)
        return;

    scn::TreeNode::Ptr const& renderActionNode = scene->descendantById(renderActionId).lock();
    if (!renderActionNode ||
        !renderActionNode->is(xchg::NodeClass::RENDER_ACTION))
        return;

    // retrieve IDs of the nodes involved with rendering
    unsigned int rendererId     = 0;
    unsigned int cameraId       = 0;
    unsigned int frameBufferId  = 0;

    const scn::RenderAction* renderAction = renderActionNode->asGraphAction()->asRenderAction();
    assert(renderAction);

    // make sure users specified an output destination when not in progressive mode
    // and that we can write to it
    bool        isProgressive = false;
    std::string filePath;

    renderAction->getParameter<bool>(parm::isProgressive(), isProgressive);
    if (!isProgressive)
    {
        if (!renderAction->getParameter<std::string>(parm::filePath(), filePath) ||
            filePath.empty())
        {
            std::stringstream sstr;
            sstr
                << "Abort rendering: no valid '" << parm::filePath().c_str() << "' value "
                << "set on render action ID = " << renderActionId << ".";
            FLIRT_LOG(LOG(DEBUG) << sstr.str();)
            std::cerr << sstr.str() << std::endl;

            return;
        }
        {
            if (util::path::dirname(filePath.c_str(), BUFFER, BUFFER_SIZE) &&
                !util::path::allows(util::path::permissions(BUFFER), util::path::WRITE, util::path::USER))
            {
                std::stringstream sstr;
                sstr
                    << "Abort rendering: output directory \'" << BUFFER << "\' "
                    << "either does not exist, or is not writable.";
                FLIRT_LOG(LOG(DEBUG) << sstr.str();)
                std::cerr << sstr.str() << std::endl;

                return;
            }
        }
    }

    bool status =
        renderAction->getParameter<unsigned int>(parm::rendererId   (), rendererId)     &&
        renderAction->getParameter<unsigned int>(parm::cameraId     (), cameraId)       &&
        renderAction->getParameter<unsigned int>(parm::frameBufferId(), frameBufferId)  &&
        rendererId      > 0 &&
        cameraId        > 0 &&
        frameBufferId   > 0;
    if (!status)
    {
        std::stringstream sstr;
        sstr
            << "Failed to retrieve information relative to rendering action"
            << "\n\t-> renderer ID = "      << rendererId
            << "\n\t-> camera ID = "        << cameraId
            << "\n\t-> framebuffer ID = "   << frameBufferId;
        FLIRT_LOG(LOG(DEBUG) << sstr.str();)
        std::cerr << sstr.str() << std::endl;

        return;
    }

    FLIRT_LOG(LOG(DEBUG)
        << "executing RENDER ACTION (ID = " << renderActionId << ")\n"
        << "\t-> renderer ID = "    << rendererId
        << "\t-> camera ID = "      << cameraId
        << "\t-> framebuffer ID = " << frameBufferId;)

    //-----------------------
    // ACTIVATION STATE CHECK
    //-----------------------------------------------------------------------------------
#define RETURNS_IF_INACTIVE(_NODE_)                                                 \
    {                                                                               \
        char parm1c = xchg::INACTIVE;                                               \
        _self.getNodeParameter<char>(parm::globalInitialization(), parm1c, _NODE_); \
        if (parm1c != xchg::ACTIVE)                                                 \
        {                                                                           \
            std::stringstream sstr;                                                 \
            sstr                                                                    \
                << "Node '" << _self.getNodeName(_NODE_) << "' "                    \
                << "of type " << std::to_string(_self.getNodeClass(_NODE_))         \
                << " (or its ancestors) is not initialized. "                       \
                << "Abort rendering.";                                              \
            FLIRT_LOG(LOG(DEBUG) << sstr.str();)                                     \
            std::cerr << sstr.str() << std::endl;                                   \
                                                                                    \
            return;                                                                 \
        }                                                                           \
    }
    //-----------------------------------------------------------------------------------
#define RETURNS_IF_INVISIBLE(_NODE_)                                            \
    {                                                                           \
        char parm1c = 0;                                                        \
        _self.getNodeParameter<char>(parm::globallyVisible(), parm1c, _NODE_);  \
        if (parm1c != 1)                                                        \
        {                                                                       \
            std::stringstream sstr;                                             \
            sstr                                                                \
                << "Node '" << _self.getNodeName(_NODE_) << "' "                \
                << "of type " << std::to_string(_self.getNodeClass(_NODE_))     \
                << " (or its ancestors) is not visible. "                       \
                << "Abort rendering.";                                          \
            FLIRT_LOG(LOG(DEBUG) << sstr.str();)                                 \
            std::cerr << sstr.str() << std::endl;                               \
                                                                                \
            return;                                                             \
        }                                                                       \
    }
    //-----------------------------------------------------------------------------------
    {
        // warns users that some drawables are not initialized
        xchg::IdSet                     ids;
        CompoundClientNodeFilter::Ptr   filter = CompoundClientNodeFilter::create();

        filter->add(FilterClientNodeByBuiltIn<char>::create(parm::initialization(), xchg::INACTIVE));
        filter->add(FilterClientNodeByClass::create(xchg::NodeClass::MESH | xchg::NodeClass::CURVES));
        _self.getIds(ids, false, filter);
        if (!ids.empty())
        {
            std::stringstream sstr;
            sstr << "WARNING: " << ids.size() << " drawable(s) are not initialized yet.";
            FLIRT_LOG(LOG(DEBUG) << sstr.str();)
            std::cerr << sstr.str() << std::endl;
        }
        // prevents rendering if a render action's link is not initialized
        RETURNS_IF_INACTIVE(rendererId)
        RETURNS_IF_INACTIVE(cameraId)
        RETURNS_IF_INVISIBLE(cameraId)
        RETURNS_IF_INACTIVE(frameBufferId)
    }
    {
        // warns users when camera ratio and framebuffer ratio mismatch (horizontal ratios)
        float   cameraRatio = 0.0f;
        size_t  frameWidth  = 0;
        size_t  frameHeight = 0;
        bool    ratiosEqual = _self.getNodeParameter<float>(parm::xAspectRatio(), cameraRatio, cameraId) &&
            _self.getNodeParameter<size_t>(parm::width(), frameWidth, frameBufferId) &&
            _self.getNodeParameter<size_t>(parm::height(), frameHeight, frameBufferId) &&
            frameHeight > 0 &&
            math::abs(cameraRatio - static_cast<float>(frameWidth)/static_cast<float>(frameHeight)) < 1e-2f;

        if (!ratiosEqual)
            std::cerr
                << "WARNING: camera ratio (= " << cameraRatio << ") "
                << "and frame buffer ratio (= " << static_cast<float>(frameWidth)/static_cast<float>(frameHeight) << ") "
                << "mismatch."
                << std::endl;
    }

    //------------------------------------------
    // GATHER ALL BINDINGS INVOLVED IN RENDERING
    //------------------------------------------
    xchg::IdSet toCommit;

    accumulateNodeAndSources(renderActionId, toCommit);
    accumulateNodeAndSources(scene->id(), toCommit);
    for (PrimitiveHandles::const_iterator it = _primitives.begin(); it != _primitives.end(); ++it)
        accumulateNodeAndSources(it->first, toCommit);

    //--------------------------
    // DETERMINE SUB-FRAME TIMES
    //--------------------------
    std::vector<float> subframeTimes;

    getRenderingTimeSteps(rendererId, scene->id(), subframeTimes);
    assert(!subframeTimes.empty());

    //-------------------------------------------------
    // COMMIT FRAME'S AND SUB-FRAMES' PARAMETER CHANGES
    //-----------------------------------------------------------------------------------
#define FORALL_BINDING_CALL(_FUNC_)                                                     \
    for (xchg::IdSet::const_iterator id = toCommit.begin(); id != toCommit.end(); ++id) \
    {                                                                                   \
        Binding::Ptr const& bind =                                                      \
            std::dynamic_pointer_cast<Binding>(_self.getBinding(*id));                  \
        if (bind == nullptr)                                                            \
            continue;                                                                   \
        bind->_FUNC_;                                                                   \
    }
    //-----------------------------------------------------------------------------------
    float referenceTime;
    scene->getParameter<float>(parm::currentTime(), referenceTime);

    // - commit all subframes
    FLIRT_LOG(LOG(DEBUG)
        << "\n\n---  begin(subframes)  --------------------------------------\n\n";)
    FORALL_BINDING_CALL (beginSubframeCommits(subframeTimes.size()))
    for (size_t i = 0; i < subframeTimes.size(); ++i)
    {
        FLIRT_LOG(LOG(DEBUG)
            << "\n\n---  subframe #" << i << "  (t= " << subframeTimes[i] << ")  -------------------------------------\n\n";)
        scene->setParameter<float>(parm::currentTime(), subframeTimes[i]);
        FORALL_BINDING_CALL (commitSubframeState(i))
    }
    FLIRT_LOG(LOG(DEBUG)
        << "\n\n---  end(subframes)  ----------------------------------------\n\n";)
    FORALL_BINDING_CALL (endSubframeCommits())
    // - commit frame
    FLIRT_LOG(LOG(DEBUG)
        << "\n\n---  frame (t= " << referenceTime<< ")  -------------------------------------------\n\n";)
    scene->setParameter<float>(parm::currentTime(), referenceTime);
    FORALL_BINDING_CALL (commitFrameState())

    //std::cout << "\n\n\n-------------\n rendering deactived\n-------------\n";
    //return;

    //---------------------------
    // PROCEED TO FRAME RENDERING
    //---------------------------
    AbstractDevice::RendererHandle      rendererHdl     = castHandle<AbstractDevice::RendererHandle>    (rendererId);
    AbstractDevice::SceneHandle         sceneHdl        = castHandle<AbstractDevice::SceneHandle>       (scene->id());
    AbstractDevice::CameraHandle        cameraHdl       = castHandle<AbstractDevice::CameraHandle>      (cameraId);
    AbstractDevice::FrameBufferHandle   frameBufferHdl  = castHandle<AbstractDevice::FrameBufferHandle> (frameBufferId);

    // lock framebuffer data visible by the scene
    _self.setNodeParameter<xchg::Data::Ptr>(parm::frameData(), nullptr, frameBufferId);

    //#####################
    _device->renderFrame(rendererHdl,
                         sceneHdl,
                         cameraHdl,
                         frameBufferHdl,
                         subframeTimes.size());
    //#####################

    // update the framebuffer data visible by the scene
    size_t          frameWidth  = 0;
    size_t          frameHeight = 0;
    const float*    rgba        = _device->getFrameData(frameBufferHdl, frameWidth, frameHeight);
    xchg::Data::Ptr frameData   = rgba
        ? xchg::Data::create<float>(rgba, (frameWidth * frameHeight) << 2, false)
        : nullptr;

    _self.setNodeParameter<size_t>          (parm::width(),     frameWidth,     frameBufferId);
    _self.setNodeParameter<size_t>          (parm::height(),    frameHeight,    frameBufferId);
    _self.setNodeParameter<xchg::Data::Ptr> (parm::frameData(), frameData,      frameBufferId);

    // write framebuffer content on disk
    assert(rendererHdl);
    assert(sceneHdl);
    assert(cameraHdl);
    assert(frameBufferHdl);

    img::OutputImage::Ptr frame = img::OutputImage::create();
    char frameBufferVerbosity   = 1;

    _device->getFrame(frameBufferHdl, *frame);
    _self.getNodeParameter<char>(parm::verbosityLevel(), frameBufferVerbosity, frameBufferId);
    if (frameBufferVerbosity > 0)
    {
        _device->getMetadata(frameBufferHdl,    *frame);
        _device->getMetadata(rendererHdl,       *frame);
        _device->getMetadata(sceneHdl,          *frame);
        _device->getMetadata(cameraHdl,         *frame);
        //_device->getMetadata(rtToneMapper,    *frame);
        // embed module information in output image metadata
        {
            std::stringstream sstr;

            sstr << sys::FLIRT_MAJOR << "." << sys::FLIRT_MINOR << "." << sys::FLIRT_TWEAK;
            frame->setMetadata("version",   sstr.str());
            frame->setMetadata("date",      sys::BUILD_DATE);
            frame->setMetadata("repo",      sys::BUILD_REPO);
            frame->setMetadata("commit",    sys::BUILD_COMMIT);
            frame->setMetadata("compiler",  sys::BUILD_COMPILER);
        }

        Json::Value         json;
        Json::FastWriter    writer;

        getNodeManifest(xchg::DRAWABLE, json);
        if (!json.empty())
            frame->setMetadata("drawableManifest", writer.write(json));
        getNodeManifest(xchg::MATERIAL | xchg::SUBSURFACE, json);
        if (!json.empty())
            frame->setMetadata("shaderManifest", writer.write(json));
        getNodeManifest(xchg::SELECTOR, json);
        if (!json.empty())
            frame->setMetadata("selectionManifest", writer.write(json));
        mapDrawableToShaders(json);
        if (!json.empty())
            frame->setMetadata("drawableToShaders", writer.write(json));
        mapDrawableToSelections(json);
        if (!json.empty())
            frame->setMetadata("drawableToSelections", writer.write(json));
    }
    if (!filePath.empty())
    {
        frame->saveToFile(filePath.c_str());
        //---
        std::stringstream sstr;
        sstr << "Saved rendering in '" << filePath << "'" << std::endl;
        FLIRT_LOG(LOG(DEBUG) << sstr.str();)
        std::cout << sstr.str() << std::endl;
    }
}

void
rndr::Client::PImpl::forEachBinding(const xchg::IdSet&              ids,
                                    const AbstractBindingOperation* op)
{
    if (!op)
        return;

    for (xchg::IdSet::const_iterator id = ids.begin(); id != ids.end(); ++id)
    {
        Binding::Ptr const& bind = std::dynamic_pointer_cast<Binding>(
            _self.getBinding(*id));
        if (bind)
            op->apply(*bind);
    }
}

void
rndr::Client::PImpl::getRenderingTimeSteps(unsigned int         rendererId,
                                           unsigned int         sceneId,
                                           std::vector<float>&  timeSteps) const
{
    timeSteps.clear();

    assert(_self.getNodeClass(rendererId) == xchg::RENDERER);
    assert(_self.getNodeClass(sceneId) == xchg::SCENE);

    float               referenceTime   = 0.0f;
    std::string         shutterTypeName;
    xchg::ShutterType   shutterType     = xchg::NO_SHUTTER;
    float               shutter         = 0.0f;
    size_t              maxTimeSteps    = 0;
    float               framerate       = 0.0f;

    _self.getNodeParameter<float>       (parm::currentTime(),   referenceTime,      sceneId);
    _self.getNodeParameter<std::string> (parm::shutterType(),   shutterTypeName,    rendererId);
    _self.getNodeParameter<size_t>      (parm::maxTimeSteps(),  maxTimeSteps,   rendererId);
    shutterType = xchg::getShutterType(shutterTypeName.c_str());

    if (shutterType != xchg::NO_SHUTTER &&
        maxTimeSteps > 1)
    {
        float shutterOpen   = referenceTime;
        float shutterClose  = referenceTime;

        _self.getNodeParameter<float>(parm::shutter(), shutter, rendererId);
        if (shutter > 0.0f)
        {
            _self.getNodeParameter<float>(parm::framerate(), framerate, sceneId);

            FLIRT_LOG(LOG(DEBUG)
                << "rendering time step computation\n"
                << "\t- current time = " << referenceTime << "\n"
                << "\t- shutter type = " << std::to_string(shutterType) << "\n"
                << "\t- shutter = " << shutter << "\n"
                << "\t- framerate = " << framerate << "\n";)
            if (math::abs(framerate) < 1e-6f)
                throw sys::Exception(
                    "Framerate must be positive.",
                    "Rendering Time Computation");

            xchg::getShutterOpenAndClose(referenceTime, framerate, shutterType, shutter, shutterOpen, shutterClose);
            FLIRT_LOG(LOG(DEBUG) << "shutter:\topen = " << shutterOpen << "\t-> close = " << shutterClose;)
            if (shutterOpen < shutterClose)
            {
                // gather times of all samples lying in the specified range
                getSampleTimesInRange(shutterOpen, shutterClose, timeSteps);
#if defined(FLIRT_LOG_ENABLED)
                {
                    std::stringstream sstr;
                    sstr
                        << "rendering time step computation\n"
                        << "sample times = [ ";
                    for (size_t i = 0; i < timeSteps.size(); ++i)
                        sstr << timeSteps[i] << " ";
                    sstr << "]";
                }
#endif // defined(FLIRT_LOG_ENABLED)

                // resample time uniformly (Embree-related constraint)
                if (timeSteps.size() >= 2)
                {
                    shutterOpen     = timeSteps.front();
                    float duration  = timeSteps.back() - shutterOpen;
                    if (duration > 0.0f)
                    {
                        float minStep  = FLT_MAX;
                        for (size_t i = 1; i < timeSteps.size(); ++i)
                            minStep = std::min(minStep, timeSteps[i] - timeSteps[i-1]);

                        const float     step        = std::max(minStep, duration / static_cast<float>(maxTimeSteps - 1));
                        assert(step > 1e-6f);
                        const size_t    numSteps    = std::min(maxTimeSteps, static_cast<size_t>(math::ceil(duration / step)) + 1);

                        timeSteps.resize(numSteps);
                        float t = shutterOpen;
                        for (size_t i = 0; i < numSteps; ++i, t+= step)
                            timeSteps[i] = t;
                    }
                }
            }
        }
    }

    if (timeSteps.empty())
        timeSteps.resize(1, referenceTime);

#if defined(FLIRT_LOG_ENABLED)
    {
        std::stringstream sstr;
        sstr << "uniform rendering times = [ ";
        for (size_t i = 0; i < timeSteps.size(); ++i)
            sstr << timeSteps[i] << " ";
        sstr << "]";
        LOG(DEBUG) << sstr.str();
    }
#endif // defined(FLIRT_LOG_ENABLED)
}

void
rndr::Client::PImpl::getSampleTimesInRange(float                shutterOpen,
                                           float                shutterClose,
                                           std::vector<float>&  ret) const
{
    ret.clear();

    if (!_self.scene() ||
        shutterOpen < 0.0f || shutterClose < 0.0f || shutterOpen > shutterClose)
        return;

    // first, select only drawables that may take part in the actual rendering.
    ////////////////////////////////
    // class DrawableSelectingFilter
    ////////////////////////////////
    class DrawableSelectingFilter:
        public scn::AbstractTreeNodeFilter
    {
    public:
        typedef std::shared_ptr<DrawableSelectingFilter> Ptr;
    public:
        static inline
        Ptr
        create()
        {
            Ptr ptr(new DrawableSelectingFilter());
            return ptr;
        }
    public:
        inline
        bool
        operator()(const scn::TreeNode& n) const
        {
            if (n.nodeClass() != xchg::MESH &&
                n.nodeClass() != xchg::CURVES)
                return false;

            char parm1c = 0;

            n.getParameter<char>(parm::globalInitialization(), parm1c);
            if (parm1c == 0)
                return false;
            n.getParameter<char>(parm::globallyVisible(), parm1c);
            if (parm1c == 0)
                return false;

            return true;
        }
    };
    ////////////////////////////////

    xchg::IdSet drawables;
    scn::Scene::IdGetterOptions opts;

    opts.filter(DrawableSelectingFilter::create());
    _self.scene()->getIds(drawables, &opts);

#if defined(FLIRT_LOG_ENABLED)
    {
        std::stringstream sstr;

        sstr
            << "stored time aggregation\n"
            << drawables.size() << " drawable(s)\n";
        for (xchg::IdSet::const_iterator id = drawables.begin(); id != drawables.end(); ++id)
            sstr << "\t'" << _self.getNodeName(*id) << "'\n";
        LOG(DEBUG) << sstr.str();
    }
#endif // defined(FLIRT_LOG_ENABLED)

    if (drawables.empty())
        return;

    // second, from each selected drawable, go up the hierarchy in order to gather
    // stored time samples lying in requested time range.
    ///////////////////////////
    // class TimeGettingVisitor
    ///////////////////////////
    class TimeGettingVisitor:
        public scn::TreeNodeVisitor
    {
    private:
        const int       _start; // in milliseconds
        const int       _end;   // in milliseconds
        std::set<int>&  _milliseconds;
        xchg::IdSet&    _visited;
    public:
        inline
            TimeGettingVisitor(float            startInSeconds,
                               float            endInSeconds,
                               std::set<int>&   milliseconds,
                               xchg::IdSet&     visited):
            scn::TreeNodeVisitor(scn::TreeNodeVisitor::ASCENDING),
            _start              (math::max(0, static_cast<int>(math::floor(1000.0f * startInSeconds)))),
            _end                (math::max(0, static_cast<int>(math::floor(1000.0f * endInSeconds)) + 1)),
            _milliseconds       (milliseconds),
            _visited            (visited)
        { }

        virtual inline
        void
        apply(scn::TreeNode& n)
        {
            if (_visited.find(n.id()) == _visited.end())
            {
                _visited.insert(n.id());

                xchg::Vector<float> timesInSeconds;

                n.getStoredTimes(timesInSeconds);
                for (size_t i = 0; i < timesInSeconds.size(); ++i)
                {
                    const int t = math::max(0, static_cast<int>(math::floor(1000.0f * timesInSeconds[i])));
                    if (t < _start || _end < t)
                        continue;
                    _milliseconds.insert(t);
                }
            }
            //---
            traverse(n);
        }
    };
    ///////////////////////////////

    std::set<int>   milliseconds;
    xchg::IdSet     visited;

    TimeGettingVisitor visitor(shutterOpen, shutterClose, milliseconds, visited);
    for (xchg::IdSet::const_iterator id = drawables.begin(); id != drawables.end(); ++id)
    {
        scn::TreeNode::Ptr const& drw = _self.getNode(*id);
        if (drw)
            drw->accept(visitor);
    }
    for (std::set<int>::const_iterator it = milliseconds.begin(); it != milliseconds.end(); ++it)
        ret.push_back(static_cast<float>(*it) * 1e-3f); // milliseconds -> seconds

#if defined(FLIRT_LOG_ENABLED)
    {
        std::stringstream sstr;
        sstr << "stored time aggregation\n";
        sstr << "\t- visited: " << visited << "\n";
        sstr << "\t- stored times in [" << shutterOpen << ", " << shutterClose << "]: [ ";
        for (size_t i = 0; i < ret.size(); ++i)
            sstr << ret[i] << " ";
        sstr << "]";
        LOG(DEBUG) << sstr.str();
    }
#endif //defined(FLIRT_LOG_ENABLED)
}

unsigned int
rndr::Client::PImpl::getTypedAscendant(unsigned int     nodeId,
                                       xchg::NodeClass  cl,
                                       bool             allowSelf) const
{
    // starts from either the primitive's shape or light instance
    unsigned int id = nodeId;
    do
    {
        if (id > 0 && (_self.getNodeClass(id) & cl))
        {
            if (id != nodeId || allowSelf)
                return id;
        }
        id = _self.getNodeParent(id);
    }
    while (id > 0);

    return 0;
}

template<typename HandleType>
HandleType
rndr::Client::PImpl::castHandle(unsigned int nodeId) const
{
    Binding::Ptr const& bind = std::dynamic_pointer_cast<Binding>(
        _self.getBinding(nodeId));

    return bind
        ? bind->castHandle<HandleType>()
        : nullptr;
}

void
rndr::Client::PImpl::accumulateNodeAndSources(unsigned int  node,
                                              xchg::IdSet&  ret) const
{
    if (ret.find(node) != ret.end())
        return;

    ret.insert(node);

    xchg::IdSet sources;
    _self.getNodeLinkSources(node, sources);
    for (xchg::IdSet::const_iterator id = sources.begin(); id != sources.end(); ++id)
        accumulateNodeAndSources(*id, ret);
}

///////////////////
// class Client
///////////////////
// explicit
rndr::Client::Client(const char* jsonCfg):
    ClientBase  (),
    _pImpl      (new PImpl(*this, jsonCfg))
{ }

rndr::Client::~Client()
{
    dispose();
    delete _pImpl;
}

// virtual
void
rndr::Client::build(Client::Ptr const&  ptr,
                    const char*         jsonCfg)
{
    ClientBase::build(ptr);
    //---
    _pImpl->build(ptr, jsonCfg);
}

// virtual
void
rndr::Client::dispose()
{
    _pImpl->dispose();
    //---
    ClientBase::dispose();
}

// virtual
ClientBindingBase::Ptr
rndr::Client::createBinding(scn::TreeNode::WPtr const& dataNode)
{
    return _pImpl->createBinding(dataNode);
}

// virtual
void
rndr::Client::destroyBinding(ClientBindingBase::Ptr const& bind)
{
    _pImpl->destroyBinding(bind);
}

// virtual
void
rndr::Client::handleSceneEvent(scn::Scene&              scene,
                               const xchg::SceneEvent&  evt)
{
    ClientBase::handleSceneEvent(scene, evt);
    //---
    _pImpl->handleSceneEvent(scene, evt);
}

// virtual
void
rndr::Client::handleSceneParmEvent(const xchg::ParmEvent& evt)
{
    ClientBase::handleSceneParmEvent(evt);
    //---
    _pImpl->handleSceneParmEvent(evt);
}

//void
//rndr::Client::handleNodeParmEvent(unsigned int nodeId, const xchg::ParmEvent& evt)
//{
//  _pImpl->handleNodeParmEvent(nodeId, evt);
//}

void
rndr::Client::handleRtHandleTypeChanged(unsigned int nodeId)
{
    _pImpl->handleRtHandleTypeChanged(nodeId);
}

unsigned int
rndr::Client::setBool(AbstractDevice::Handle getHandle, const char* name, bool value)
{
    return _pImpl->setBool(getHandle, name, value);
}

unsigned int
rndr::Client::setChar(AbstractDevice::Handle getHandle, const char* name, char value)
{
    return _pImpl->setChar(getHandle, name, value);
}

unsigned int
rndr::Client::setUInt(AbstractDevice::Handle getHandle, const char* name, unsigned int value)
{
    return _pImpl->setUInt(getHandle, name, value);
}

unsigned int
rndr::Client::setUInt64(AbstractDevice::Handle getHandle, const char* name, size_t value)
{
    return _pImpl->setUInt64(getHandle, name, value);
}

unsigned int
rndr::Client::setInt1(AbstractDevice::Handle getHandle, const char* name, int value)
{
    return _pImpl->setInt1(getHandle, name, value);
}

unsigned int
rndr::Client::setInt2(AbstractDevice::Handle getHandle, const char* name, const math::Vector2i& value)
{
    return _pImpl->setInt2(getHandle, name, value);
}

unsigned int
rndr::Client::setInt3(AbstractDevice::Handle getHandle, const char* name, const math::Vector3i& value)
{
    return _pImpl->setInt3(getHandle, name, value);
}

unsigned int
rndr::Client::setInt4(AbstractDevice::Handle getHandle, const char* name, const math::Vector4i& value)
{
    return _pImpl->setInt4(getHandle, name, value);
}

unsigned int
rndr::Client::setFloat1(AbstractDevice::Handle getHandle, const char* name, float value)
{
    return _pImpl->setFloat1(getHandle, name, value);
}

unsigned int
rndr::Client::setFloat2(AbstractDevice::Handle getHandle, const char* name, const math::Vector2f& value)
{
    return _pImpl->setFloat2(getHandle, name, value);
}

unsigned int
rndr::Client::setFloat4(AbstractDevice::Handle getHandle, const char* name, const math::Vector4f& value)
{
    return _pImpl->setFloat4(getHandle, name, value);
}

unsigned int
rndr::Client::setTransform(AbstractDevice::Handle getHandle, const char* name, const math::Affine3f& value)
{
    return _pImpl->setTransform(getHandle, name, value);
}

unsigned int
rndr::Client::setString(AbstractDevice::Handle getHandle, const char* name, const std::string& value)
{
    return _pImpl->setString(getHandle, name, value);
}

unsigned int
rndr::Client::setIdSet(AbstractDevice::Handle getHandle, const char* name, const xchg::IdSet& value)
{
    return _pImpl->setIdSet(getHandle, name, value);
}

unsigned int
rndr::Client::setBounds(AbstractDevice::Handle getHandle, const char* name, const xchg::BoundingBox& value)
{
    return _pImpl->setBounds(getHandle, name, value);
}

unsigned int
rndr::Client::setDataStream(AbstractDevice::Handle getHandle, const char* name, const xchg::DataStream& value)
{
    return _pImpl->setDataStream(getHandle, name, value);
}

unsigned int
rndr::Client::setData(AbstractDevice::Handle getHandle, const char* name, xchg::Data::Ptr const& value)
{
    return _pImpl->setData(getHandle, name, value);
}

unsigned int
rndr::Client::setObjectPtr(AbstractDevice::Handle getHandle, const char* name, xchg::ObjectPtr const& value)
{
    return _pImpl->setObjectPtr(getHandle, name, value);
}

void
rndr::Client::unset(AbstractDevice::Handle getHandle, unsigned int parmId)
{
    _pImpl->unset(getHandle, parmId);
}

void
rndr::Client::commitFrameState(AbstractDevice::Handle getHandle)
{
    _pImpl->commitFrameState(getHandle);
}

void
rndr::Client::beginSubframeCommits(AbstractDevice::Handle getHandle, size_t numSubframes)
{
    _pImpl->beginSubframeCommits(getHandle, numSubframes);
}

void
rndr::Client::commitSubframeState(AbstractDevice::Handle getHandle, size_t subframe)
{
    _pImpl->commitSubframeState(getHandle, subframe);
}

void
rndr::Client::endSubframeCommits(AbstractDevice::Handle getHandle)
{
    _pImpl->endSubframeCommits(getHandle);
}

bool
rndr::Client::perSubframeParameter(AbstractDevice::Handle getHandle, unsigned int parmId) const
{
    return _pImpl->perSubframeParameter(getHandle, parmId);
}

//bool
//rndr::Client::link(unsigned int           parmId,
//                         AbstractDevice::Handle       targetHdl,
//                         xchg::NodeClass      targetClass,
//                         AbstractDevice::Handle       sourceHdl,
//                         xchg::NodeClass      sourceClass)
//{
//  return _pImpl->link(parmId, targetHdl, targetClass, sourceHdl, sourceClass);
//}
//
//bool
//rndr::Client::unlink(unsigned int     parmId,
//                           AbstractDevice::Handle targetHdl,
//                           xchg::NodeClass    targetClass,
//                           AbstractDevice::Handle sourceHdl,
//                           xchg::NodeClass    sourceClass)
//{
//  return _pImpl->unlink(parmId, targetHdl, targetClass, sourceHdl, sourceClass);
//}

//void
//rndr::Client::add(AbstractDevice::FrameBufferHandle frameBufferHdl, AbstractDevice::RenderPassHandle rtRenderPass)
//{
//  _pImpl->add(frameBufferHdl, rtRenderPass);
//}
//
//void
//rndr::Client::remove(AbstractDevice::FrameBufferHandle frameBufferHdl, AbstractDevice::RenderPassHandle rtRenderPass)
//{
//  _pImpl->remove(frameBufferHdl, rtRenderPass);
//}

void
rndr::Client::render(unsigned int renderActionId)
{
    return _pImpl->render(renderActionId);
}
