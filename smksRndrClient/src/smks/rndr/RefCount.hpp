// ========================================================================= //
// Copyright 2016-2018 SUPAMONKS_STUDIO                                      //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
//                                                                           //
// ========================================================================= //
#pragma once

#include <smks/sys/Exception.hpp>

#include "smks/rndr/AbstractDevice.hpp"
#include <smks/sys/constants.hpp>
#include <smks/sys/Exception.hpp>

namespace smks { namespace rndr
{
    //////////////////////////////////////////////////
    /// Automatic reference counting for Flirt Handles
    //////////////////////////////////////////////////
    template<typename ObjType>
    class RefCount
    {
    private:
        ObjType                 _object;
        AbstractDevice *const   _device;

    public:
        __forceinline explicit
        RefCount(AbstractDevice* device) :
            _object(nullptr),
            _device(device)
        {
            if (_device == nullptr)
                throw sys::Exception(
                    "Invalid rendering device.",
                    "Reference Count Creation");
        }

        __forceinline
        RefCount(sys::NullTy, AbstractDevice* device):
            _object(nullptr),
            _device(device)
        {
            if (_device == nullptr)
                throw sys::Exception(
                    "Invalid rendering device.",
                    "Reference Count Creation");
        }

        __forceinline
        RefCount(ObjType const obj, AbstractDevice* device):
            _object(obj),
            _device(device)
        {
            if (_device == nullptr)
                throw sys::Exception(
                    "Invalid rendering device.",
                    "Reference Count Creation");
        }

        __forceinline
        RefCount(const RefCount& other):
            _object(other._object),
            _device(other._device)
        {
            if (_object && _device)
                _device->incRef(_object);
        }

        __forceinline
        ~RefCount(void)
        {
            if (_object && _device)
                _device->decRef(_object);
        }

        __forceinline
        RefCount&
        operator=(const RefCount& other)
        {
            if (_device != other._device)
                throw sys::Exception(
                    "Cannot assign reference counted handles from different rendering devices.",
                    "Reference Count Assignment");

            if (_device)
            {
                if (other._object)
                    _device->incRef(other._object);
                if (_object)
                    _device->decRef(_object);
            }
            _object = other._object;

            return *this;
        }

        __forceinline
        RefCount&
        operator=(sys::NullTy)
        {
            if (_object && _device)
                _device->decRef(_object);
            _object = nullptr;

            return *this;
        }

        __forceinline
        operator bool() const
        {
            return _object != nullptr;
        }

        __forceinline
        operator ObjType() const
        {
            return _object;
        }
    };
}
}
