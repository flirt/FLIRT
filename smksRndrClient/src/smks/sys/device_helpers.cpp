// ========================================================================= //
// Copyright 2016-2018 SUPAMONKS_STUDIO                                      //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
//                                                                           //
// ========================================================================= //
#include <sstream>
#include <smks/sys/library.hpp>
#include <smks/sys/Log.hpp>

#include "device_helpers.hpp"

#define WIN32_LEAN_AND_MEAN
#include <windows.h>

namespace smks { namespace sys
{
    static const char* g_createSymbol   = "create";
    static const char* g_destroySymbol  = "destroy";

    typedef rndr::AbstractDevice* (*DeviceCreator) (const char*, std::shared_ptr<ClientBase> const&);
    typedef void (*DeviceDestructor) (rndr::AbstractDevice*);

    static
    void*
    getSymbolFromModule(const std::string& moduleFile,
                        const std::string& symbol);

    static
    bool
    getModuleBinaryPath(std::string&);
}
}

using namespace smks;

// static
rndr::AbstractDevice*
sys::createDeviceHelper(
    const char*                         moduleFile,
    const char*                         jsonCfg,
    std::shared_ptr<ClientBase> const&  client)
{
    DeviceCreator create = reinterpret_cast<DeviceCreator>(
        getSymbolFromModule(moduleFile, g_createSymbol));

    if (!create)
        return nullptr;

    //----------------------------------------------------
    rndr::AbstractDevice* device = create(jsonCfg, client);
    //----------------------------------------------------
    if (device == nullptr)
    {
        std::stringstream sstr;
        sstr
            << "Symbol '" << g_createSymbol << "' (from '" << moduleFile << "') "
            << "failed to create rendering device.";
        std::cerr << sstr.str();
        FLIRT_LOG(LOG(DEBUG) << sstr.str();)
    }

    return device;
}

// static
void
sys::destroyDeviceHelper(const char*            moduleFile,
                         rndr::AbstractDevice*  device)
{
    DeviceDestructor destroy = reinterpret_cast<DeviceDestructor>(
        getSymbolFromModule(moduleFile, g_destroySymbol));

    if (!destroy)
        return;

    //-------------
    destroy(device);
    //-------------
}

// static
void*
sys::getSymbolFromModule(const std::string& moduleFile,
                         const std::string& symbolName)
{
    std::string bindir;
    getModuleBinaryPath(bindir);  // get the binary directory in order to look for device-implementing module

    sys::lib_t lib = openLibrary(moduleFile.c_str(), bindir.c_str());
    if (lib == nullptr)
    {
        std::stringstream sstr;
        sstr
            << "Failed to load shared library '" << moduleFile << "'.";
        std::cerr << sstr.str();
        FLIRT_LOG(LOG(DEBUG) << sstr.str();)
    }

    void* symbol = getSymbol(lib, symbolName.c_str());
    if (symbol == nullptr)
    {
        std::stringstream sstr;
        sstr
            << "Failed to find symbol '" << symbolName << "' "
            << "from load shared library '" << moduleFile << "'.";
        std::cerr << sstr.str();
        FLIRT_LOG(LOG(DEBUG) << sstr.str();)
    }

    return symbol;
}

// static
bool
sys::getModuleBinaryPath(std::string& dir)
{
    dir.clear();

    char path[512];
    HMODULE hm = NULL;

    if (!GetModuleHandleExA(
            GET_MODULE_HANDLE_EX_FLAG_FROM_ADDRESS |
            GET_MODULE_HANDLE_EX_FLAG_UNCHANGED_REFCOUNT,
            (LPCSTR) &getModuleBinaryPath, &hm))
        return false;

    GetModuleFileNameA(hm, path, sizeof(path));
    dir = std::string(path);

    return true;
}
