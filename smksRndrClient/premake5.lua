-- ========================================================================= --
-- Copyright 2016-2018 SUPAMONKS_STUDIO                                      --
-- Author: Pierre-Edouard Landes <pel@supamonks.com>                         --
--                                                                           --
-- This program is free software: you can redistribute it and/or modify      --
-- it under the terms of the GNU Lesser General Public License as            --
-- published by the Free Software Foundation, either version 3 of the        --
-- License, or (at your option) any later version.                           --
--                                                                           --
-- This program is distributed in the hope that it will be useful,           --
-- but WITHOUT ANY WARRANTY; without even the implied warranty of            --
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             --
-- GNU Lesser General Public License for more details.                       --
--                                                                           --
-- You should have received a copy of the GNU Lesser General Public License  --
-- along with this program.  If not, see <https://www.gnu.org/licenses/>.    --
--                                                                           --
-- ========================================================================= --


smks.module.uses_rndr_client = function ()

    smks.module.links(
        smks.module.name.rndr_client,
        smks.module.kind.rndr_client)

    smks.dep.eigen.includes()

    smks.module.includes(smks.module.name.math)
    smks.module.includes(smks.module.name.xchg)
    smks.module.includes(smks.module.name.clientbase)
    smks.module.includes(smks.module.name.rndr_device_api)

end


smks.module.copy_rndr_client_to_targetdir = function()

    smks.module.copy_to_targetdir(
        smks.module.name.rndr_client)

    smks.module.copy_clientbase_to_targetdir()

end


project(smks.module.name.rndr_client)

    language 'C++'
    smks.module.set_kind(smks.module.kind.rndr_client)

    files
    {
        'include/**.hpp',
        'src/**.hpp',
        'src/**.cpp'
    }
    defines
    {
        'FLIRT_CLIENTBASE_DLL',
        'FLIRT_RNDR_CLIENT_DLL',
        'FLIRT_IMG_DLL',
        'FLIRT_UTIL_DLL',
    }
    includedirs
    {
        'include',
    }

    smks.dep.boost.includes()
    smks.dep.json.links()

    smks.compile_info.links()

    smks.module.uses_log()
    smks.module.uses_strid()
    smks.module.uses_sys()
    smks.module.uses_util()
    smks.module.uses_img()
    smks.module.uses_xchg()
    smks.module.uses_scn()
    smks.module.uses_clientbase()
    smks.module.includes(smks.module.name.rndr_device_api)
