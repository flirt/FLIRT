// ========================================================================= //
// Copyright 2016-2018 SUPAMONKS_STUDIO                                      //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
//                                                                           //
// ========================================================================= //

#pragma once

#include <string>
#include <sstream>

#include <osg/Matrixf>
#include <osg/Matrixd>
#include <osg/Vec2i>
#include <osg/Vec4i>
#include <osg/Vec4f>
#include <osg/Vec4b>

#include <smks/sys/Exception.hpp>
#include "smks/util/osg/key.hpp"
#include "smks/sys/configure_util_osg.hpp"

namespace std
{
    inline
    string
    to_string(smks::util::osg::BindingSection);

    inline
    string
    to_string(const osg::Matrixf&);

    inline
    string
    to_string(const osg::Matrixd&);

    inline
    string
    to_string(const osg::Vec2i&);

    inline
    string
    to_string(const osg::Vec4i&);

    inline
    string
    to_string(const osg::Vec3f&);

    inline
    string
    to_string(const osg::Vec3d&);

    inline
    string
    to_string(const osg::Vec4b&);

    inline
    string
    to_string(const osg::Vec4f&);

    inline
    string
    to_string(const osg::Vec4d&);

    FLIRT_UTIL_OSG_EXPORT
    ostream&
    to_string(ostream&, const osg::Node*);
}
#include "to_string_osg-inl.hpp"
