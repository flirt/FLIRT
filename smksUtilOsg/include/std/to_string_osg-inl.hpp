// ========================================================================= //
// Copyright 2016-2018 SUPAMONKS_STUDIO                                      //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
//                                                                           //
// ========================================================================= //

inline
std::string
std::to_string(smks::util::osg::BindingSection section)
{
    switch (section)
    {
    case smks::util::osg::BINDINGS_DEFAULT:         return "";
    case smks::util::osg::BINDINGS_ANIMATION:       return "Animation";
    case smks::util::osg::BINDINGS_VIEWPORT:        return "Viewport";
    case smks::util::osg::BINDINGS_RENDERING:       return "Rendering";
    case smks::util::osg::BINDINGS_MANIPULATION:    return "Manipulation";
    case smks::util::osg::BINDINGS_SCENE_EDITING:   return "Scene Editing";
    case smks::util::osg::BINDINGS_DEBUGGING:       return "Debugging";
    default:
        throw smks::sys::Exception("Unsupported binding section.", "Binding Section To String");
    }
}

inline
std::string
std::to_string(const osg::Matrixf& m)
{
    std::stringstream stream;

    for (unsigned int row = 0; row < 4; ++row)
    {
        stream << "[ ";
        for (unsigned int col = 0; col < 4; ++col)
            stream << m(row, col) << "\t";
        stream << " ]\n";
    }

    return stream.str();
}

inline
std::string
std::to_string(const osg::Matrixd& m)
{
    std::stringstream stream;

    for (unsigned int row = 0; row < 4; ++row)
    {
        stream << "[ ";
        for (unsigned int col = 0; col < 4; ++col)
            stream << m(row, col) << "\t";
        stream << " ]\n";
    }

    return stream.str();
}

inline
std::string
std::to_string(const osg::Vec2i& v)
{
    std::stringstream stream;

    stream << "( " << v.x() << ",\t" << v.y() << " )";

    return stream.str();
}

inline
std::string
std::to_string(const osg::Vec3f& v)
{
    std::stringstream stream;

    stream << "( " << v.x() << ",\t" << v.y() << ",\t" << v.z() << " )";

    return stream.str();
}

inline
std::string
std::to_string(const osg::Vec3d& v)
{
    std::stringstream stream;

    stream << "( " << v.x() << ",\t" << v.y() << ",\t" << v.z() << " )";

    return stream.str();
}

inline
std::string
std::to_string(const osg::Vec4b& v)
{
    std::stringstream stream;

    stream << "( " << int(v.x()) << ",\t" << int(v.y()) << ",\t" << int(v.z()) << ",\t" << int(v.w()) << " )";

    return stream.str();
}

inline
std::string
std::to_string(const osg::Vec4f& v)
{
    std::stringstream stream;

    stream << "( " << v.x() << ",\t" << v.y() << ",\t" << v.z() << ",\t" << v.w() << " )";

    return stream.str();
}

inline
std::string
std::to_string(const osg::Vec4i& v)
{
    std::stringstream stream;

    stream << "( " << v.x() << ",\t" << v.y() << ",\t" << v.z() << ",\t" << v.w() << " )";

    return stream.str();
}

inline
std::string
std::to_string(const osg::Vec4d& v)
{
    std::stringstream stream;

    stream << "( " << v.x() << ",\t" << v.y() << ",\t" << v.z() << ",\t" << v.w() << " )";

    return stream.str();
}
