// ========================================================================= //
// Copyright 2016-2018 SUPAMONKS_STUDIO                                      //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
//                                                                           //
// ========================================================================= //
#pragma once

#include <osg/Object>
#include <osg/CopyOp>
#include "smks/sys/configure_util_osg.hpp"

namespace smks { namespace util { namespace osg
{
    class FLIRT_UTIL_OSG_EXPORT UserData:
        public ::osg::Object
    {
    private:
        class PImpl;
    private:
        PImpl* _pImpl;
    public:
        explicit
        UserData(unsigned int);

        UserData(const UserData&, const ::osg::CopyOp&);

        ~UserData();

        unsigned int
        id() const;

        ::osg::Object*
        cloneType() const;

        ::osg::Object*
        clone(const ::osg::CopyOp&) const;

        const char*
        libraryName() const;

        const char*
        className() const;
    };
}
}
}
