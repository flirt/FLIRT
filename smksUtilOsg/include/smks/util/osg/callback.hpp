// ========================================================================= //
// Copyright 2016-2018 SUPAMONKS_STUDIO                                      //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
//                                                                           //
// ========================================================================= //
#pragma once

#include <osg/Node>
#include <osg/Callback>

#include <osgViewer/View>


namespace smks { namespace util { namespace osg
{
    template<class CallbackType> inline
    const CallbackType*
    getUpdateCallback(const ::osg::Node*);

    template<class CallbackType> inline
    CallbackType*
    getUpdateCallback(::osg::Node*);

    template<class CallbackType> inline
    const CallbackType*
    getCallback(const ::osg::Callback*);

    template<class CallbackType> inline
    CallbackType*
    getCallback(::osg::Callback*);

    template<class HandlerType> inline
    const HandlerType*
    getEventHandler(const osgViewer::View*);

    template<class HandlerType> inline
    HandlerType*
    getEventHandler(osgViewer::View*);
}
}
}



template<class CallbackType>
inline
const CallbackType*
smks::util::osg::getUpdateCallback(const ::osg::Node* n)
{
    return n ?
        getCallback<CallbackType>(n->getUpdateCallback())
        : nullptr;
}

template<class CallbackType>
inline
CallbackType*
smks::util::osg::getUpdateCallback(::osg::Node* n)
{
    return n ?
        getCallback<CallbackType>(n->getUpdateCallback())
        : nullptr;
}

template<class CallbackType>
inline
const CallbackType*
smks::util::osg::getCallback(const ::osg::Callback* callback)
{
    if (callback)
        do
        {
            if (dynamic_cast<const CallbackType*>(callback))
                return dynamic_cast<const CallbackType*>(callback);

            callback = callback->getNestedCallback();
        }
        while(callback);

    return nullptr;
}

template<class CallbackType>
inline
CallbackType*
smks::util::osg::getCallback(::osg::Callback* callback)
{
    if (callback)
        do
        {
            if (dynamic_cast<CallbackType*>(callback))
                return dynamic_cast<CallbackType*>(callback);

            callback = callback->getNestedCallback();
        }
        while(callback);

    return nullptr;
}

template<class HandlerType>
inline
const HandlerType*
smks::util::osg::getEventHandler(const osgViewer::View* view)
{
    if (view == nullptr)
        return nullptr;

    const osgViewer::View::EventHandlers& handlers = view->getEventHandlers();

    for (osgViewer::View::EventHandlers::const_iterator it = handlers.begin(); it != handlers.end(); ++it)
        if (dynamic_cast<const HandlerType*>(it->get()))
            return dynamic_cast<const HandlerType*>(it->get());

    return nullptr;
}

template<class HandlerType>
inline
HandlerType*
smks::util::osg::getEventHandler(osgViewer::View* view)
{
    if (view == nullptr)
        return nullptr;

    const osgViewer::View::EventHandlers& handlers = view->getEventHandlers();

    for (osgViewer::View::EventHandlers::const_iterator it = handlers.begin(); it != handlers.end(); ++it)
        if (dynamic_cast<HandlerType*>(it->get()))
            return dynamic_cast<HandlerType*>(it->get());

    return nullptr;
}
