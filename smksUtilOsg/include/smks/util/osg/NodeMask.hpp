// ========================================================================= //
// Copyright 2016-2018 SUPAMONKS_STUDIO                                      //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
//                                                                           //
// ========================================================================= //
#pragma once

#include <osg/Node>

namespace smks { namespace util { namespace osg
{
    enum NodeMask
    {
        INVISIBLE_TO_ALL        = 0,
        VISIBLE_TO_ALL          = -1,
        //----------------------
        VISIBLE_SCENE_ENTITY    = 0x00000001,
        VISIBLE_RT_ENTITY       = 0x00010000,
        //----------------------
        CAMERA_FULL_MASK        = VISIBLE_TO_ALL,
        PRE_CAMERA_MASK         = 0x0000ffff,
        POST_CAMERA_MASK        = 0xffff0000
    };
}
}
}
