// ========================================================================= //
// Copyright 2016-2018 SUPAMONKS_STUDIO                                      //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
//                                                                           //
// ========================================================================= //
#pragma once

#include <vector>

#include <osg/ref_ptr>
#include <osg/BoundingBox>

#include "smks/sys/configure_util_osg.hpp"

namespace osg
{
    class Geometry;
}

namespace smks { namespace util { namespace osg
{
    FLIRT_UTIL_OSG_EXPORT
    ::osg::ref_ptr<::osg::Geometry>
    createTexturedUnitQuad(unsigned int textureUnit = 0);

    FLIRT_UTIL_OSG_EXPORT
    ::osg::BoundingBoxf
    computeBounds(const std::vector<float>&, size_t stride);

    FLIRT_UTIL_OSG_EXPORT
    ::osg::BoundingBoxf
    computeBounds(const float*, size_t size, size_t stride);

    FLIRT_UTIL_OSG_EXPORT
    void
    mapCenterToOrigin(float*, size_t size, size_t stride);

    // extends bounds so that its center coincides with the origin
    FLIRT_UTIL_OSG_EXPORT
    void
    symmetrizeBounds(::osg::BoundingBoxf&);
}
}
}
