// ========================================================================= //
// Copyright 2016-2018 SUPAMONKS_STUDIO                                      //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
//                                                                           //
// ========================================================================= //
#pragma once

#include <smks/math/types.hpp>

#include "smks/util/osg/BasicShape.hpp"

namespace smks { namespace util { namespace osg
{
    //<! creates a shape resembling an arrow indicating the default forward direction (negative Z-axis) and starting at the origin
    class FLIRT_UTIL_OSG_EXPORT ArrowShape:
        public BasicShape
    {
    private:
        static std::vector<unsigned int>    _strokeLines;
        static std::vector<unsigned int>    _fillTriangles;
        std::vector<float>                  _positions3d;

    public:
        ArrowShape(ResourcesPtr const&,
                   float length             = 5.0f,
                   float tip                = 0.2f,
                   float section            = 0.2f,
                   const math::Affine3f&    = math::Affine3f::Identity());

    protected:
        inline
        const char*
        name() const
        {
            return "arrow";
        }

        const float*
        getVertexData(size_t&, size_t& stride);

        const unsigned int*
        getIndexData(ComponentType, size_t&, GLenum&);
    };
}
}
}
