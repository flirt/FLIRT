// ========================================================================= //
// Copyright 2016-2018 SUPAMONKS_STUDIO                                      //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
//                                                                           //
// ========================================================================= //
#pragma once

#include <vector>

#include <smks/math/types.hpp>
#include "smks/sys/configure_util_osg.hpp"


namespace smks { namespace util { namespace osg
{
    FLIRT_UTIL_OSG_EXPORT
    void
    appendUnitDiskXY(std::vector<float>&        positions3d,
                 std::vector<unsigned int>& strokeLines,                // GL_LINES
                 std::vector<unsigned int>* fillTriangles = nullptr,    // GL_TRIANGLES
                 size_t numSegments     = 16,
                 const math::Affine3f&  = math::Affine3f::Identity());

    FLIRT_UTIL_OSG_EXPORT
    void
    appendUnitSquareXY(std::vector<float>&          positions3d,
                   std::vector<unsigned int>&   strokeLines,                // GL_LINES
                   std::vector<unsigned int>*   fillTriangles = nullptr,    // GL_TRIANGLES
                   const math::Affine3f&    = math::Affine3f::Identity());

    FLIRT_UTIL_OSG_EXPORT
    void
    appendForwardArrow(std::vector<float>&          positions3d,
                       std::vector<unsigned int>&   strokeLines,                // GL_LINES
                       std::vector<unsigned int>*   fillTriangles = nullptr,    // GL_TRIANGLES
                       float length             = 5.0f,
                       float tip                = 0.2f,
                       float section            = 0.2f,
                       const math::Affine3f&    = math::Affine3f::Identity());

    FLIRT_UTIL_OSG_EXPORT
    void
    appendRaysXY(std::vector<float>&        positions3d,
                 std::vector<unsigned int>& lines,  // GL_LINES
                 size_t numRays         = 4,
                 float  rmin            = 0.25f,
                 float  rmax            = 0.5f,
                 const math::Affine3f&  = math::Affine3f::Identity());
}
}
}
