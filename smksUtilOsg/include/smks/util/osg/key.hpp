// ========================================================================= //
// Copyright 2016-2018 SUPAMONKS_STUDIO                                      //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
//                                                                           //
// ========================================================================= //
#pragma once

#include <iosfwd>
#include <osgGA/GUIEventAdapter>

#include "smks/sys/configure_util_osg.hpp"

namespace osg
{
    class ApplicationUsage;
}
namespace osgGA
{
    class GUIEventAdapter;
}

namespace smks { namespace util { namespace osg
{
    ////////////////////////
    // class KeyControl
    ////////////////////////
    class FLIRT_UTIL_OSG_EXPORT KeyControl
    {
    private:
        struct PImpl;
    private:
        PImpl* _pImpl;
    public:
        explicit
        KeyControl(osgGA::GUIEventAdapter::KeySymbol);

        KeyControl(osgGA::GUIEventAdapter::ModKeyMask, osgGA::GUIEventAdapter::KeySymbol);

        KeyControl(int, osgGA::GUIEventAdapter::KeySymbol);

        KeyControl(const KeyControl&);

        ~KeyControl();

        KeyControl&
        operator=(const KeyControl&);

        bool
        compatibleWith(const osgGA::GUIEventAdapter&) const;

        friend
        std::ostream&
        operator<<(std::ostream& out, const KeyControl& _this)
        {
            return _this.print(out);
        }
    private:
        std::ostream&
        print(std::ostream&) const;
    };

    //////////////////////
    // enum BindingSection
    //////////////////////
    enum BindingSection
    {
        BINDINGS_DEFAULT = 0,
        BINDINGS_ANIMATION,
        BINDINGS_VIEWPORT,
        BINDINGS_RENDERING,
        BINDINGS_MANIPULATION,
        BINDINGS_SCENE_EDITING,
        BINDINGS_DEBUGGING,
        NUM_BINDING_SECTIONS
    };

    FLIRT_UTIL_OSG_EXPORT
    void
    addMouseBinding(::osg::ApplicationUsage&,
                    BindingSection,
                    size_t idx,
                    const char*,
                    osgGA::GUIEventAdapter::MouseButtonMask,
                    osgGA::GUIEventAdapter::ModKeyMask = (osgGA::GUIEventAdapter::ModKeyMask)0);
    FLIRT_UTIL_OSG_EXPORT
    void
    addKeyboardBinding(::osg::ApplicationUsage&,
                       BindingSection,
                       size_t idx,
                       const char*,
                       const KeyControl&);
    FLIRT_UTIL_OSG_EXPORT
    void
    addKeyboardBinding(::osg::ApplicationUsage&,
                       BindingSection,
                       size_t idx,
                       const char*,
                        const KeyControl&,
                        const KeyControl&);

    FLIRT_UTIL_OSG_EXPORT
    const char*
    splitSectionFromKeys(const char*, BindingSection&, size_t&);

    //FLIRT_UTIL_OSG_EXPORT
    //size_t //<! number of used characters
    //getKeyName(osgGA::GUIEventAdapter::KeySymbol, char* buffer, size_t bufferSize);
}
}
}
