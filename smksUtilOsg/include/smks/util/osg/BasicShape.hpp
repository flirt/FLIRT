// ========================================================================= //
// Copyright 2016-2018 SUPAMONKS_STUDIO                                      //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
//                                                                           //
// ========================================================================= //
#pragma once

#include <memory>
#include <vector>

#include <osg/ref_ptr>
#include <osg/observer_ptr>
#include <osg/Array>
#include <osg/Program>
#include <osg/Uniform>
#include <osg/Geode>

#include "smks/sys/configure_util_osg.hpp"

namespace osg
{
    class Group;
}

namespace smks
{
    namespace sys
    {
        class ResourceDatabase;
    }

    namespace util { namespace osg
    {
        //<! basic shapes are invisible by default. one needs to specify either stroke colors, fill
        // colors, or both to make them appear.
        class FLIRT_UTIL_OSG_EXPORT BasicShape:
            public ::osg::Geode
        {
        private:
            class Geometry;
            class StrokeGeometry;
            class FillGeometry;
        public:
            typedef std::shared_ptr<BasicShape> Ptr;
        protected:
            typedef std::shared_ptr<sys::ResourceDatabase>  ResourcesPtr;
            typedef std::weak_ptr<sys::ResourceDatabase>    ResourcesWPtr;
            typedef ::osg::ref_ptr<Geometry>                GeometryPtr;
        protected:
            ///////////////////////
            // struct ComponentType
            ///////////////////////
            enum ComponentType
            {
                COMPONENT_STROKE = 0,
                COMPONENT_FILL,
                NUM_COMPONENTS
            };
            ///////////////////////
        protected:
            ResourcesWPtr   _resources;
            GeometryPtr     _geometries[NUM_COMPONENTS];

        public:
            explicit
            BasicShape(ResourcesPtr const&);
        private:
            // non-copyable
            BasicShape(const BasicShape&);
            BasicShape& operator=(const BasicShape&);

        public:
            void
            strokeColor(unsigned int);

            void
            fillColor(unsigned int);

        protected:
            virtual
            const char*
            name() const = 0;

            virtual
            const float*
            getVertexData(size_t& size, size_t& stride) = 0;

            virtual
            const unsigned int*
            getIndexData(ComponentType, size_t& size, GLenum&) = 0;

        private:
            void
            setComponentColor(ComponentType, unsigned int);
        };
    }
}
}
