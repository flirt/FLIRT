// ========================================================================= //
// Copyright 2016-2018 SUPAMONKS_STUDIO                                      //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
//                                                                           //
// ========================================================================= //
inline
osg::ref_ptr<osg::Texture2D>
util::osg::createRGBATexture(bool                       useFloat,
                             ::osg::Texture::FilterMode minFilter,
                             ::osg::Texture::FilterMode magFilter)
{
    ::osg::ref_ptr<::osg::Texture2D> texture = new ::osg::Texture2D();

    texture->setSourceFormat(GL_RGBA);
    if (useFloat)
    {
        texture->setInternalFormat(GL_RGBA32F_ARB);
        texture->setSourceType(GL_FLOAT);
    }
    else
    {
        texture->setInternalFormat(GL_RGBA);
        texture->setSourceType(GL_UNSIGNED_BYTE);
    }
    texture->setFilter(::osg::Texture2D::MIN_FILTER, minFilter);
    texture->setFilter(::osg::Texture2D::MAG_FILTER, magFilter);
    texture->setResizeNonPowerOfTwoHint(false);

    return texture;
}

inline
GLenum
util::osg::getGLenum(::osg::Camera::BufferComponent component)
{
    GLenum ret = GL_COLOR_ATTACHMENT0_EXT;

    switch (component)
    {
    case ::osg::Camera::DEPTH_BUFFER:
        ret = GL_DEPTH_ATTACHMENT_EXT;
        break;
    case ::osg::Camera::STENCIL_BUFFER:
        ret = GL_STENCIL_ATTACHMENT_EXT;
        break;
    case ::osg::Camera::PACKED_DEPTH_STENCIL_BUFFER:
        throw; // no corresponding value in FrameBufferObject header
        break;
    case ::osg::Camera::COLOR_BUFFER:
    case ::osg::Camera::COLOR_BUFFER0:
        ret = GL_COLOR_ATTACHMENT0_EXT;
        break;
    case ::osg::Camera::COLOR_BUFFER1:
        ret = GL_COLOR_ATTACHMENT1_EXT;
        break;
    case ::osg::Camera::COLOR_BUFFER2:
        ret = GL_COLOR_ATTACHMENT2_EXT;
        break;
    case ::osg::Camera::COLOR_BUFFER3:
        ret = GL_COLOR_ATTACHMENT3_EXT;
        break;
    case ::osg::Camera::COLOR_BUFFER4:
        ret = GL_COLOR_ATTACHMENT4_EXT;
        break;
    case ::osg::Camera::COLOR_BUFFER5:
        ret = GL_COLOR_ATTACHMENT5_EXT;
        break;
    case ::osg::Camera::COLOR_BUFFER6:
        ret = GL_COLOR_ATTACHMENT6_EXT;
        break;
    case ::osg::Camera::COLOR_BUFFER7:
        ret = GL_COLOR_ATTACHMENT7_EXT;
        break;
    case ::osg::Camera::COLOR_BUFFER8:
        ret = GL_COLOR_ATTACHMENT8_EXT;
        break;
    case ::osg::Camera::COLOR_BUFFER9:
        ret = GL_COLOR_ATTACHMENT9_EXT;
        break;
    case ::osg::Camera::COLOR_BUFFER10:
        ret = GL_COLOR_ATTACHMENT10_EXT;
        break;
    case ::osg::Camera::COLOR_BUFFER11:
        ret = GL_COLOR_ATTACHMENT11_EXT;
        break;
    case ::osg::Camera::COLOR_BUFFER12:
        ret = GL_COLOR_ATTACHMENT12_EXT;
        break;
    case ::osg::Camera::COLOR_BUFFER13:
        ret = GL_COLOR_ATTACHMENT13_EXT;
        break;
    case ::osg::Camera::COLOR_BUFFER14:
        ret = GL_COLOR_ATTACHMENT14_EXT;
        break;
    case ::osg::Camera::COLOR_BUFFER15:
        ret = GL_COLOR_ATTACHMENT15_EXT;
        break;
    }

    return ret;
}
