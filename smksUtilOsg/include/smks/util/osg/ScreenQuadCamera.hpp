// ========================================================================= //
// Copyright 2016-2018 SUPAMONKS_STUDIO                                      //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
//                                                                           //
// ========================================================================= //
#pragma once

#include <memory>
#include <osg/Camera>

#include "smks/sys/configure_util_osg.hpp"

namespace osg
{
    class GraphicsContext;
    class MatrixTransform;
    class Geometry;
    class Uniform;
    class Texture2D;
}

namespace smks { namespace util { namespace osg
{
    class FLIRT_UTIL_OSG_EXPORT ScreenQuadCamera:
        public ::osg::Camera
    {
    public:
        typedef ::osg::ref_ptr<ScreenQuadCamera> Ptr;
    private:
        typedef ::osg::ref_ptr<::osg::MatrixTransform>  MatrixTransformPtr;
        typedef ::osg::ref_ptr<::osg::Geometry>         GeometryPtr;
        typedef ::osg::ref_ptr<::osg::Uniform>          UniformPtr;
    private:
        struct EventCallback;
    private:
        const unsigned int  _textureUnit;
        MatrixTransformPtr  _xfm;
        GeometryPtr         _quad;

    public:
        static
        Ptr
        create(unsigned int textureUnit = 0);

    protected:
        explicit
        ScreenQuadCamera(unsigned int);
    private:
        // non-copyable
        ScreenQuadCamera(const ScreenQuadCamera&);
        ScreenQuadCamera& operator=(const ScreenQuadCamera&);

    public:
        inline
        unsigned int
        textureUnit() const
        {
            return _textureUnit;
        }

        ::osg::Geometry*
        quad();

        const ::osg::Geometry*
        quad() const;

    protected:
        virtual
        void
        handleResize(int, int, int, int);
    };
}
}
}
