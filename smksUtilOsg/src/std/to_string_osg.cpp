// ========================================================================= //
// Copyright 2016-2018 SUPAMONKS_STUDIO                                      //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
//                                                                           //
// ========================================================================= //
#include <ostream>
#include <bitset>

#include <osg/Node>
#include <osg/Camera>
#include <osg/Group>

#include "std/to_string_osg.hpp"

namespace smks { namespace util { namespace osg
{
    static inline
    std::ostream&
    to_string(std::ostream& out, const ::osg::Node& node, const std::string& indent, bool lastChild);
}
}
}

using namespace smks;

// static
std::ostream&
util::osg::to_string(std::ostream& out, const ::osg::Node& node, const std::string& indent, bool lastChild)
{
    out << indent;

    std::string str = indent;
    if (lastChild)
    {
        out << "`--";
        str += "  ";
    }
    else
    {
        out << "|--";
        str += "|  ";
    }
    out << " " << node.getName();
    out << std::hex << "\t// " << node.getNodeMask() << " ";

    const ::osg::Camera* cam = dynamic_cast<const ::osg::Camera*>(&node);
    if (cam)
    {
        out << "\t// CAM ";
        switch (cam->getRenderOrder())
        {
        case ::osg::Camera::PRE_RENDER      : out << "pre-render ";     break;
        case ::osg::Camera::NESTED_RENDER   : out << "nested-render ";  break;
        case ::osg::Camera::POST_RENDER     : out << "post-render ";    break;
        default: break;
        }
        out << "#" << cam->getRenderOrderNum() << " (cullmask = " << cam->getCullMask() << ", ptr = " << (void*)(cam) << ")";
    }
    out << "\n";

    const ::osg::Group* group = dynamic_cast<const ::osg::Group*>(&node);
    if (group)
    {
        for (size_t i = 0; i < group->getNumChildren(); ++i)
            if (group->getChild(i))
                to_string(out, *group->getChild(i), str, i == group->getNumChildren() - 1);
    }
    return out;
}

std::ostream&
std::to_string(std::ostream& out, const osg::Node* node)
{
    return node
        ? util::osg::to_string(out, *node, "", true)
        : out;
}
