// ========================================================================= //
// Copyright 2016-2018 SUPAMONKS_STUDIO                                      //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
//                                                                           //
// ========================================================================= //
#include <cassert>
#include <algorithm>
#include <osg/Geometry>

#include "smks/util/osg/geometry.hpp"

using namespace smks;

::osg::ref_ptr<::osg::Geometry>
util::osg::createTexturedUnitQuad(unsigned int textureUnit)
{
    const ::osg::Vec3f ZERO     (0.0f, 0.0f, 0.0f);
    const ::osg::Vec3f UNIT_X   (1.0f, 0.0f, 0.0f);
    const ::osg::Vec3f UNIT_Y   (0.0f, 1.0f, 0.0f);

    ::osg::Vec3Array*           positions   = new ::osg::Vec3Array(4);
    ::osg::Vec4Array*           texcoords   = new ::osg::Vec4Array(4);
    ::osg::Vec4Array*           colors      = new ::osg::Vec4Array(4);
    ::osg::DrawElementsUShort*  triangles   = new ::osg::DrawElementsUShort(::osg::PrimitiveSet::TRIANGLES, 6);

    ::osg::Vec3f*   posPtr      = &positions->front();
    ::osg::Vec4f*   texPtr      = &texcoords->front();
    ::osg::Vec4f*   colorPtr    = &colors   ->front();
    GLushort*       indicesPtr  = &triangles->front();

    posPtr->set(ZERO);                      ++posPtr;
    posPtr->set(UNIT_X);                    ++posPtr;
    posPtr->set(UNIT_X + UNIT_Y);           ++posPtr;
    posPtr->set(UNIT_Y);                    ++posPtr;

    texPtr->set(0.0f, 0.0f, 0.0f, 0.0f);    ++texPtr;
    texPtr->set(1.0f, 0.0f, 0.0f, 0.0f);    ++texPtr;
    texPtr->set(1.0f, 1.0f, 0.0f, 0.0f);    ++texPtr;
    texPtr->set(0.0f, 1.0f, 0.0f, 0.0f);    ++texPtr;

    colorPtr->set(1.0f, 1.0f, 1.0f, 1.0f);  ++colorPtr;
    colorPtr->set(1.0f, 1.0f, 1.0f, 1.0f);  ++colorPtr;
    colorPtr->set(1.0f, 1.0f, 1.0f, 1.0f);  ++colorPtr;
    colorPtr->set(1.0f, 1.0f, 1.0f, 1.0f);  ++colorPtr;

    *indicesPtr = 0;                        ++indicesPtr;
    *indicesPtr = 2;                        ++indicesPtr;
    *indicesPtr = 1;                        ++indicesPtr;

    *indicesPtr = 0;                        ++indicesPtr;
    *indicesPtr = 3;                        ++indicesPtr;
    *indicesPtr = 2;                        ++indicesPtr;

    ::osg::ref_ptr<::osg::Geometry> geometry = new ::osg::Geometry();

    geometry->setUseDisplayList(false);
    geometry->setUseVertexArrayObject(true);

    geometry->setVertexArray (positions);
    geometry->setTexCoordArray (textureUnit, texcoords, ::osg::Array::BIND_PER_VERTEX);
    geometry->setColorArray (colors, ::osg::Array::BIND_PER_VERTEX);

    geometry->addPrimitiveSet(triangles);

    return geometry;
}

::osg::BoundingBoxf
util::osg::computeBounds(const std::vector<float>& positions, size_t stride)
{
    return computeBounds(!positions.empty() ? &positions.front() : nullptr, positions.size(), stride);
}

::osg::BoundingBoxf
util::osg::computeBounds(const float* positions, size_t size, size_t stride)
{
    ::osg::BoundingBoxf bounds;

    if (positions == nullptr ||
        size == 0 ||
        stride == 0)
    {
        bounds.init();
        return bounds;
    }

    const size_t numPositions = size / 3;
    if (numPositions > 0)
    {
        float minPos[3]     = { FLT_MAX, FLT_MAX, FLT_MAX };
        float maxPos[3]     = { -FLT_MAX, -FLT_MAX, -FLT_MAX };
        const float* ptr    = positions;
        for (size_t i = 0; i < numPositions; ++i)
        {
            for (size_t k = 0; k < 3; ++k)
            {
                minPos[k] = ptr[k] < minPos[k] ? ptr[k] : minPos[k];
                maxPos[k] = maxPos[k] < ptr[k] ? ptr[k] : maxPos[k];
            }
            ptr += stride;
        }
        bounds.set(
            minPos[0], minPos[1], minPos[2],
            maxPos[0], maxPos[1], maxPos[2]);
    }

    return bounds;
}

void
util::osg::mapCenterToOrigin(float* positions, size_t size, size_t stride)
{
    if (positions == nullptr ||
        size == 0 ||
        stride == 0)
        return;

    assert(size % 3 == 0);

    float           center[]        = { 0.0f, 0.0f, 0.0f };
    const size_t    numPositions    = size / 3;
    for (size_t n = 0, i = 0; n < numPositions; ++n)
    {
        for (size_t k = 0; k < 3; ++k)
            center[k] += positions[i+k];
        i += stride;
    }
    const float rNumPositions = 1.0f / static_cast<float>(numPositions);
    for (size_t k = 0; k < 3; ++k)
        center[k] *= rNumPositions;
    for (size_t n = 0, i = 0; n < numPositions; ++n)
    {
        for (size_t k = 0; k < 3; ++k)
            positions[i+k] -= center[k];
        i += stride;
    }
}

void
util::osg::symmetrizeBounds(::osg::BoundingBoxf& bounds)
{
    if (!bounds.valid())
        return;

    const float absx = std::max(fabsf(bounds.xMin()), fabsf(bounds.xMax()));
    const float absy = std::max(fabsf(bounds.yMin()), fabsf(bounds.yMax()));
    const float absz = std::max(fabsf(bounds.zMin()), fabsf(bounds.zMax()));

    bounds.set(-absx, -absy, -absz, absx, absy, absz);
}
