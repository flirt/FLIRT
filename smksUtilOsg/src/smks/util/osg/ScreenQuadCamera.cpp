// ========================================================================= //
// Copyright 2016-2018 SUPAMONKS_STUDIO                                      //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
//                                                                           //
// ========================================================================= //
#include <cassert>

#include <osg/NodeCallback>
#include <osg/MatrixTransform>
#include <osg/Geometry>
#include <osgGA/EventVisitor>

#include <smks/sys/Exception.hpp>
#include <smks/util/osg/geometry.hpp>

#include "smks/util/osg/ScreenQuadCamera.hpp"

namespace smks { namespace util { namespace osg
{
    ///////////////////////
    // struct EventCallback
    ///////////////////////
    struct ScreenQuadCamera::EventCallback:
        public ::osg::NodeCallback
    {
    public:
        inline
        void
        operator() (::osg::Node* node, ::osg::NodeVisitor* nv)
        {
            ScreenQuadCamera*       quadCam = static_cast<ScreenQuadCamera*>(node);
            osgGA::EventVisitor*    ev  = static_cast<osgGA::EventVisitor*> (nv);

            assert(quadCam);
            assert(ev);

            const osgGA::EventQueue::Events&    events  = ev->getEvents();
            for (osgGA::EventQueue::Events::const_iterator it = events.begin();
                it != events.end();
                ++it)
            {
                const osgGA::GUIEventAdapter* ea = (*it)->asGUIEventAdapter();

                if (ea->getEventType() != osgGA::GUIEventAdapter::RESIZE)
                    continue;

                quadCam->handleResize(ea->getWindowX(),
                                      ea->getWindowY(),
                                      ea->getWindowWidth(),
                                      ea->getWindowHeight());
                break;
            }
            traverse(node, nv);
        }
    };
}
}
}

using namespace smks;

// static
util::osg::ScreenQuadCamera::Ptr
util::osg::ScreenQuadCamera::create(unsigned int textureUnit)
{
    Ptr ptr(new ScreenQuadCamera(textureUnit));
    return ptr;
}

util::osg::ScreenQuadCamera::ScreenQuadCamera(unsigned int textureUnit):
    ::osg::Camera   (),
    _textureUnit    (textureUnit),
    _xfm            (new ::osg::MatrixTransform()),
    _quad           (util::osg::createTexturedUnitQuad(textureUnit))
{
    setRenderOrder(::osg::Camera::NESTED_RENDER);

    setReferenceFrame(::osg::Transform::ABSOLUTE_RF);
    setViewMatrix(::osg::Matrix::identity());

    setClearMask(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    getOrCreateStateSet()->setMode(GL_BLEND, ::osg::StateAttribute::ON | ::osg::StateAttribute::OVERRIDE);

    addEventCallback(new EventCallback());

    _quad->setName("__screenQuad.geometry");
    _xfm->setName("__screenQuad.xform");
    _xfm->addChild(_quad.get());
    addChild(_xfm.get());
}

::osg::Geometry*
util::osg::ScreenQuadCamera::quad()
{
    return _quad.get();
}

const ::osg::Geometry*
util::osg::ScreenQuadCamera::quad() const
{
    return _quad.get();
}

// virtual
void
util::osg::ScreenQuadCamera::handleResize(int x, int y, int width, int height)
{
    setProjectionMatrixAsOrtho2D(0, width, 0, height);
    if (_xfm)
        _xfm->setMatrix(
            ::osg::Matrixf::scale(static_cast<float>(width), static_cast<float>(height), 1.0f));
}
