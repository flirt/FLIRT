// ========================================================================= //
// Copyright 2016-2018 SUPAMONKS_STUDIO                                      //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
//                                                                           //
// ========================================================================= //
#include "smks/util/osg/UserData.hpp"

namespace smks { namespace util { namespace osg
{
    class UserData::PImpl
    {
    private:
        unsigned int _id;
    public:
        explicit inline
        PImpl(unsigned int id):
            _id(id)
        { }

        inline
        PImpl(const PImpl& other):
            _id()
        {
            copy(other);
        }

        inline
        PImpl&
        operator=(const PImpl& other)
        {
            copy(other);
            return *this;
        }

        inline
        unsigned int
        id() const
        {
            return _id;
        }
    private:
        void
        copy(const PImpl& other)
        {
            _id = other._id;
        }
    };
}
}
}


using namespace smks;

// explicit
util::osg::UserData::UserData(unsigned int id):
    _pImpl(new PImpl(id))
{ }

util::osg::UserData::UserData(const UserData& other,
                              const ::osg::CopyOp&):
    _pImpl(new PImpl(*other._pImpl))
{ }

util::osg::UserData::~UserData()
{
    delete _pImpl;
}

unsigned int
util::osg::UserData::id() const
{
    return _pImpl->id();
}

::osg::Object*
util::osg::UserData::cloneType() const
{
    return new UserData(_pImpl->id());
}

::osg::Object*
util::osg::UserData::clone(const ::osg::CopyOp& op) const
{
    return new UserData(*this, op);
}

const char*
util::osg::UserData::libraryName() const
{
    return "SmksUtilOsg";
}

const char*
util::osg::UserData::className() const
{
    return "UserData";
}
