// ========================================================================= //
// Copyright 2016-2018 SUPAMONKS_STUDIO                                      //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
//                                                                           //
// ========================================================================= //
#include <cassert>
#include <osg/Drawable>
#include <iostream>
#include "smks/util/osg/drawable.hpp"

using namespace smks;

void
util::osg::setupRender(::osg::Drawable& drw)
{
    // both use of display list and vertex buffer objects must be explicitly turned off.
    // otherwise, VBO unbinding performed by OSG ripples through drawcalls and may
    // cause the application to crash.
    drw.setUseDisplayList(false);
    drw.setUseVertexBufferObjects(false);
    drw.setUseVertexArrayObject(true);
}
