// ========================================================================= //
// Copyright 2016-2018 SUPAMONKS_STUDIO                                      //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
//                                                                           //
// ========================================================================= //
#include <smks/sys/Exception.hpp>
#include <smks/math/matrixAlgo.hpp>

#include "smks/util/osg/basic_shapes.hpp"

using namespace smks;

void
util::osg::appendUnitSquareXY(std::vector<float>&           positions3d,
                              std::vector<unsigned int>&    strokeLines,
                              std::vector<unsigned int>*    fillTriangles,
                              const math::Affine3f&         xfm)
{
    if (positions3d.size() % 3 != 0)
        throw sys::Exception("Vertex buffer should only contain 3D floating-point positions.", "Square Shape Buffers Initialization");
    if (strokeLines.size() % 2 != 0)
        throw sys::Exception("Stroke index buffer should only contain lines.", "Square Shape Buffers Initialization");
    if (fillTriangles && fillTriangles->size() % 2 != 0)
        throw sys::Exception("Fill index buffer should only contain triangles.", "Square Shape Buffers Initialization");

    const size_t v0 = positions3d.size() / 3;

    // vertex buffer
    //--------------
    const size_t numNewVertices = 4;

    positions3d.reserve(positions3d.size() + 3 * numNewVertices);
    for (size_t i = 0; i < numNewVertices; ++i)
    {
        const float x = i < 2 ? -0.5f : 0.5f;
        const float y = i % 2 == 0 ? -0.5f : 0.5f;
        const math::Vector4f& pos = math::transformPoint(xfm, math::Vector4f(x, y, 0.0f, 1.0f));

        positions3d.push_back(pos.x());
        positions3d.push_back(pos.y());
        positions3d.push_back(pos.z());
    }

    // stroke index buffer
    //--------------------
    strokeLines.reserve(strokeLines.size() + 8);

    strokeLines.push_back(v0 + 0);
    strokeLines.push_back(v0 + 1);

    strokeLines.push_back(v0 + 1);
    strokeLines.push_back(v0 + 3);

    strokeLines.push_back(v0 + 3);
    strokeLines.push_back(v0 + 2);

    strokeLines.push_back(v0 + 2);
    strokeLines.push_back(v0 + 0);

    if (fillTriangles)
    {
        // fill index buffer
        //------------------
        fillTriangles->reserve(fillTriangles->size() + numNewVertices + 1);

        fillTriangles->push_back(v0 + 0);
        fillTriangles->push_back(v0 + 1);
        fillTriangles->push_back(v0 + 2);

        fillTriangles->push_back(v0 + 1);
        fillTriangles->push_back(v0 + 3);
        fillTriangles->push_back(v0 + 2);
    }
}

void
util::osg::appendUnitDiskXY(std::vector<float>&         positions3d,
                            std::vector<unsigned int>&  strokeLines,
                            std::vector<unsigned int>*  fillTriangles,
                            size_t                      numSegments,
                            const math::Affine3f&       xfm)
{
    if (positions3d.size() % 3 != 0)
        throw sys::Exception("Vertex buffer should only contain 3D floating-point positions.", "Disk Shape Buffers Initialization");
    if (strokeLines.size() % 2 != 0)
        throw sys::Exception("Stroke index buffer should only contain lines.", "Disk Shape Buffers Initialization");
    if (fillTriangles && fillTriangles->size() % 2 != 0)
        throw sys::Exception("Fill index buffer should only contain triangles.", "Disk Shape Buffers Initialization");

    const size_t v0 = positions3d.size() / 3;

    // vertex buffer
    //--------------
    const size_t    N               = std::max(static_cast<size_t>(3), numSegments);
    const float     step            = 360.0f / static_cast<float>(N) * static_cast<float>(sys::degrees_to_radians);
    const float     cStep           = cosf(step);
    const float     sStep           = sinf(step);
    const size_t    numNewVertices  = N + 1;

    float cAng = 1.0f;
    float sAng = 0.0f;

    positions3d.reserve(positions3d.size() + 3 * numNewVertices);
    for (size_t i = 0; i < numNewVertices; ++i)
    {
        math::Vector4f pos(0.0f, 0.0f, 0.0f, 1.0f);
        if (i > 0)
        {
            const float cNew = cAng * cStep - sAng * sStep;
            const float sNew = cAng * sStep + sAng * cStep;

            pos.x() = 0.5f * cNew;
            pos.y() = 0.5f * sNew;
            cAng    = cNew;
            sAng    = sNew;
        }
        pos = math::transformPoint(xfm, pos);
        positions3d.push_back(pos.x());
        positions3d.push_back(pos.y());
        positions3d.push_back(pos.z());
    }

    // stroke index buffer
    //--------------------
    strokeLines.reserve(strokeLines.size() + (N << 1));
    for (size_t i = 0; i < N; ++i)
    {
        const size_t iNext = (i + 1) % N;

        strokeLines.push_back(v0 + 1 + i);
        strokeLines.push_back(v0 + 1 + iNext);
    }

    if (fillTriangles)
    {
        // fill index buffer
        //------------------
        fillTriangles->reserve(fillTriangles->size() + numNewVertices + 1);
        for (size_t i = 0; i < N; ++i)
        {
            const size_t iNext = (i + 1) % N;

            fillTriangles->push_back(v0);
            fillTriangles->push_back(v0 + 1 + i);
            fillTriangles->push_back(v0 + 1 + iNext);
        }
    }
}

void
util::osg::appendForwardArrow(std::vector<float>&           positions3d,
                              std::vector<unsigned int>&    strokeLines,    // GL_LINES
                              std::vector<unsigned int>*    fillTriangles,  // GL_TRIANGLES
                              float                         length,
                              float                         tip,
                              float                         section,
                              const math::Affine3f&         xfm)
{
    if (positions3d.size() % 3 != 0)
        throw sys::Exception("Vertex buffer should only contain 3D floating-point positions.", "Arrow Shape Buffers Initialization");
    if (strokeLines.size() % 2 != 0)
        throw sys::Exception("Stroke index buffer should only contain lines.", "Arrow Shape Buffers Initialization");
    if (fillTriangles && fillTriangles->size() % 2 != 0)
        throw sys::Exception("Fill index buffer should only contain triangles.", "Arrow Shape Buffers Initialization");

    const size_t v0 = positions3d.size() / 3;

    // vertex buffer
    //--------------
    const float l0  = std::max(0.0f, length);
    const float l1  = l0 * (1.0f - std::max(0.0f, std::min(1.0f, tip * 0.7f)));
    const float l2  = l0 * (1.0f - std::max(0.0f, std::min(1.0f, tip)));
    const float s   = l0 * std::max(0.0f, std::min(1.0f, section)) * 0.5f;

    //       [3]
    //  [1]-[2]----------------------[0]
    //       [4]
    const float vertexData[] = {
        0.0f,   0.0f,   0.0f,
        0.0f,   0.0f,   - l0,
        0.0f,   0.0f,   - l1,
        s,      0.0f,   - l2,
        - s,    0.0f,   - l2,
    };
    const float* ptr = vertexData;

    positions3d.reserve(positions3d.size() + 15);
    for (size_t i = 0; i < 5; ++i)
    {
        const math::Vector4f& pos = math::transformPoint(xfm, math::Vector4f(ptr[0], ptr[1], ptr[2], 1.0f));
        positions3d.push_back(pos.x());
        positions3d.push_back(pos.y());
        positions3d.push_back(pos.z());
        ptr += 3;
    }

    // stroke index buffer
    //--------------------
    strokeLines.reserve(strokeLines.size() + 10);

    strokeLines.push_back(v0 + 0);
    strokeLines.push_back(v0 + 2);

    strokeLines.push_back(v0 + 2);
    strokeLines.push_back(v0 + 3);

    strokeLines.push_back(v0 + 3);
    strokeLines.push_back(v0 + 1);

    strokeLines.push_back(v0 + 1);
    strokeLines.push_back(v0 + 4);

    strokeLines.push_back(v0 + 4);
    strokeLines.push_back(v0 + 2);

    if (fillTriangles)
    {
        // fill index buffer
        //------------------
        fillTriangles->reserve(fillTriangles->size() + 6);

        fillTriangles->push_back(v0 + 1);
        fillTriangles->push_back(v0 + 2);
        fillTriangles->push_back(v0 + 3);

        fillTriangles->push_back(v0 + 1);
        fillTriangles->push_back(v0 + 4);
        fillTriangles->push_back(v0 + 2);
    }
}

void
util::osg::appendRaysXY(std::vector<float>&         positions3d,
                        std::vector<unsigned int>&  lines,              // GL_LINES
                        size_t                      numRays,
                        float                       rmin,
                        float                       rmax,
                        const math::Affine3f&       xfm)
{
    if (numRays == 0)
        return;

    if (positions3d.size() % 3 != 0)
        throw sys::Exception("Vertex buffer should only contain 3D floating-point positions.", "Ray Shape Buffers Initialization");
    if (lines.size() % 2 != 0)
        throw sys::Exception("Index buffer should only contain lines.", "Ray Shape Buffers Initialization");

    size_t      numVertices = positions3d.size() / 3;
    const float step        = 360.0f / static_cast<float>(numRays) * static_cast<float>(sys::degrees_to_radians);
    const float cStep       = cosf(step);
    const float sStep       = sinf(step);
    float       cAng        = 1.0f;
    float       sAng        = 0.0f;

    positions3d .reserve(positions3d.size() + 3 * (numRays << 1));
    lines       .reserve(lines.size() + (numRays << 1));

    for (size_t i = 0; i < numRays; ++i)
    {
        const float cNew = cAng * cStep - sAng * sStep;
        const float sNew = cAng * sStep + sAng * cStep;

        math::Vector4f pos = math::transformPoint(xfm, math::Vector4f(rmin * cNew, rmin * sNew, 0.0f, 1.0f));
        lines.push_back(numVertices);
        positions3d.push_back(pos.x());
        positions3d.push_back(pos.y());
        positions3d.push_back(pos.z());
        ++numVertices;

        pos = math::transformPoint(xfm, math::Vector4f(rmax * cNew, rmax * sNew, 0.0f, 1.0f));
        lines.push_back(numVertices);
        positions3d.push_back(pos.x());
        positions3d.push_back(pos.y());
        positions3d.push_back(pos.z());
        ++numVertices;

        cAng = cNew;
        sAng = sNew;
    }
}
