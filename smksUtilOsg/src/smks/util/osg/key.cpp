// ========================================================================= //
// Copyright 2016-2018 SUPAMONKS_STUDIO                                      //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
//                                                                           //
// ========================================================================= //
#include <iostream>
#include <sstream>
#include <boost/unordered_map.hpp>

#include <osg/ApplicationUsage>
#include <osgGA/GUIEventAdapter>

#include "std/to_string_osg.hpp"
#include "smks/util/osg/key.hpp"

namespace smks { namespace util { namespace osg
{
    typedef boost::unordered_map<int, std::string> NameMap;

    static const char SEP_SECTION   = '|';  //<! character used to separate section infos from keys in binding string
    static const char SEP_IDX       = ':';  //<! character used to separate section name from section index in section infos

    static inline
    NameMap
    initializeKeyNames();

    static inline
    std::string
    keyName(osgGA::GUIEventAdapter::KeySymbol);

    static inline
    std::string
    buttonNames(osgGA::GUIEventAdapter::MouseButtonMask);

    static inline
    std::string
    modNames(osgGA::GUIEventAdapter::ModKeyMask);

    static NameMap _keys = initializeKeyNames();

    /////////////////////////////
    // struct KeyControl::PImpl
    /////////////////////////////
    struct KeyControl::PImpl
    {
        osgGA::GUIEventAdapter::ModKeyMask  mods;
        osgGA::GUIEventAdapter::KeySymbol   key;
    public:
        explicit inline
        PImpl(osgGA::GUIEventAdapter::KeySymbol key):
            mods((osgGA::GUIEventAdapter::ModKeyMask)0), key(key)
        { }

        inline
        PImpl(osgGA::GUIEventAdapter::ModKeyMask mods, osgGA::GUIEventAdapter::KeySymbol key):
            mods(mods), key(key)
        { }

        inline
        PImpl(int mods, osgGA::GUIEventAdapter::KeySymbol key):
            mods((osgGA::GUIEventAdapter::ModKeyMask)mods), key(key)
        { }

        inline
        PImpl(const PImpl& other):
            mods(other.mods), key(other.key)
        { }

        inline
        PImpl&
        operator=(const PImpl& other)
        {
            mods = other.mods;
            key = other.key;
            return *this;
        }

        inline
        bool
        compatibleWith(const osgGA::GUIEventAdapter& ea) const
        {
            const osgGA::GUIEventAdapter::KeySymbol k = (osgGA::GUIEventAdapter::KeySymbol)ea.getKey();
            if (k != key)
                return false;
            //-------------------------------------------------------
            //--- hack for key events coming from _pyflirt module ---
            const osgGA::GUIEventAdapter::KeySymbol unmodk = (osgGA::GUIEventAdapter::KeySymbol)ea.getUnmodifiedKey();
            if (k == key && unmodk == key)
                return true;    // necessary because unmodk always equal to k when coming from _pyflirt
            //-------------------------------------------------------

            if (ea.getUnmodifiedKey() != ea.getKey())
                return true;
            else
                return mods == 0
                ? ea.getModKeyMask() == 0
                : (ea.getModKeyMask() & mods) != 0;
        }

        inline
        std::ostream&
        print(std::ostream& out) const
        {
            return out << modNames(mods) << keyName(key);
        }
    };
}
}
}

using namespace smks;

///////////////////////
// class  KeyControl
///////////////////////
// explicit
util::osg::KeyControl::KeyControl(osgGA::GUIEventAdapter::KeySymbol key):
    _pImpl(new PImpl(key))
{ }

util::osg::KeyControl::KeyControl(osgGA::GUIEventAdapter::ModKeyMask mods, osgGA::GUIEventAdapter::KeySymbol key):
    _pImpl(new PImpl(mods, key))
{ }

util::osg::KeyControl::KeyControl(int mods, osgGA::GUIEventAdapter::KeySymbol key):
    _pImpl(new PImpl(mods, key))
{ }

util::osg::KeyControl::KeyControl(const KeyControl& other):
    _pImpl(new PImpl(*other._pImpl))
{ }

util::osg::KeyControl::~KeyControl()
{
    delete _pImpl;
}

util::osg::KeyControl&
util::osg::KeyControl::operator=(const KeyControl& other)
{
    *_pImpl = *other._pImpl;
    return *this;
}

bool
util::osg::KeyControl::compatibleWith(const osgGA::GUIEventAdapter& ea) const
{
    return _pImpl->compatibleWith(ea);
}

std::ostream&
util::osg::KeyControl::print(std::ostream& out) const
{
    return _pImpl->print(out);
}

///////////////////////

void
util::osg::addMouseBinding(::osg::ApplicationUsage&                 usage,
                           BindingSection                           section,
                           size_t                                   idx,
                           const char*                              explanation,
                           osgGA::GUIEventAdapter::MouseButtonMask  buttons,
                           osgGA::GUIEventAdapter::ModKeyMask       mods)
{
    std::stringstream sstr;
    sstr
        << static_cast<int>(section) << SEP_IDX << idx
        << SEP_SECTION
        << modNames(mods) << buttonNames(buttons);
    usage.addKeyboardMouseBinding(sstr.str(), explanation);
}
void
util::osg::addKeyboardBinding(::osg::ApplicationUsage&  usage,
                              BindingSection            section,
                              size_t                    idx,
                              const char*               explanation,
                              const KeyControl&         key)
{
    std::stringstream sstr;
    sstr
        << static_cast<int>(section) << SEP_IDX << idx
        << SEP_SECTION
        << key;
    usage.addKeyboardMouseBinding(sstr.str(), explanation);
}
void
util::osg::addKeyboardBinding(::osg::ApplicationUsage&  usage,
                              BindingSection            section,
                              size_t                    idx,
                              const char*               explanation,
                              const KeyControl&         key1,
                              const KeyControl&         key2)
{
    std::stringstream sstr;
    sstr
        << static_cast<int>(section) << SEP_IDX << idx
        << SEP_SECTION
        << key1 << "/" << key2;
    usage.addKeyboardMouseBinding(sstr.str(), explanation);
}

const char*
util::osg::splitSectionFromKeys(const char*         input,
                                BindingSection& section,
                                size_t&             idx)
{
    std::string str(input);
    section     = BINDINGS_DEFAULT;
    idx         = 0;

    size_t sepSectionPos = str.find_first_of(SEP_SECTION);
    if (sepSectionPos == std::string::npos)
        return input;

    std::string sectionInfo = str.substr(0, sepSectionPos);
    size_t      sepIdxPos   = sectionInfo.find_first_of(SEP_IDX);
    if (sepIdxPos != std::string::npos)
    {
        std::string sectionEnum = sectionInfo.substr(0, sepIdxPos);
        std::string sectionIdx  = sectionInfo.substr(sepIdxPos + 1);

        section = static_cast<BindingSection>(atoi(sectionEnum.c_str()));
        idx     = atol(sectionIdx.c_str());
    }

    return input + sepSectionPos + 1;
}

// static
util::osg::NameMap
util::osg::initializeKeyNames()
{
    NameMap m;

    m[osgGA::GUIEventAdapter::KEY_Space           ]= "Space";
    m[osgGA::GUIEventAdapter::KEY_0               ]= "0";
    m[osgGA::GUIEventAdapter::KEY_1               ]= "1";
    m[osgGA::GUIEventAdapter::KEY_2               ]= "2";
    m[osgGA::GUIEventAdapter::KEY_3               ]= "3";
    m[osgGA::GUIEventAdapter::KEY_4               ]= "4";
    m[osgGA::GUIEventAdapter::KEY_5               ]= "5";
    m[osgGA::GUIEventAdapter::KEY_6               ]= "6";
    m[osgGA::GUIEventAdapter::KEY_7               ]= "7";
    m[osgGA::GUIEventAdapter::KEY_8               ]= "8";
    m[osgGA::GUIEventAdapter::KEY_9               ]= "9";
    m[osgGA::GUIEventAdapter::KEY_A               ]= "A";
    m[osgGA::GUIEventAdapter::KEY_B               ]= "B";
    m[osgGA::GUIEventAdapter::KEY_C               ]= "C";
    m[osgGA::GUIEventAdapter::KEY_D               ]= "D";
    m[osgGA::GUIEventAdapter::KEY_E               ]= "E";
    m[osgGA::GUIEventAdapter::KEY_F               ]= "F";
    m[osgGA::GUIEventAdapter::KEY_G               ]= "G";
    m[osgGA::GUIEventAdapter::KEY_H               ]= "H";
    m[osgGA::GUIEventAdapter::KEY_I               ]= "I";
    m[osgGA::GUIEventAdapter::KEY_J               ]= "J";
    m[osgGA::GUIEventAdapter::KEY_K               ]= "K";
    m[osgGA::GUIEventAdapter::KEY_L               ]= "L";
    m[osgGA::GUIEventAdapter::KEY_M               ]= "M";
    m[osgGA::GUIEventAdapter::KEY_N               ]= "N";
    m[osgGA::GUIEventAdapter::KEY_O               ]= "O";
    m[osgGA::GUIEventAdapter::KEY_P               ]= "P";
    m[osgGA::GUIEventAdapter::KEY_Q               ]= "Q";
    m[osgGA::GUIEventAdapter::KEY_R               ]= "R";
    m[osgGA::GUIEventAdapter::KEY_S               ]= "S";
    m[osgGA::GUIEventAdapter::KEY_T               ]= "T";
    m[osgGA::GUIEventAdapter::KEY_U               ]= "U";
    m[osgGA::GUIEventAdapter::KEY_V               ]= "V";
    m[osgGA::GUIEventAdapter::KEY_W               ]= "W";
    m[osgGA::GUIEventAdapter::KEY_X               ]= "X";
    m[osgGA::GUIEventAdapter::KEY_Y               ]= "Y";
    m[osgGA::GUIEventAdapter::KEY_Z               ]= "Z";

    m[osgGA::GUIEventAdapter::KEY_Exclaim         ]= "!";
    m[osgGA::GUIEventAdapter::KEY_Quotedbl        ]= "\"";
    m[osgGA::GUIEventAdapter::KEY_Hash            ]= "|";
    m[osgGA::GUIEventAdapter::KEY_Dollar          ]= "$";
    m[osgGA::GUIEventAdapter::KEY_Ampersand       ]= "&";
    m[osgGA::GUIEventAdapter::KEY_Quote           ]= "\'";
    m[osgGA::GUIEventAdapter::KEY_Leftparen       ]= "(";
    m[osgGA::GUIEventAdapter::KEY_Rightparen      ]= ")";
    m[osgGA::GUIEventAdapter::KEY_Asterisk        ]= "*";
    m[osgGA::GUIEventAdapter::KEY_Plus            ]= "+";
    m[osgGA::GUIEventAdapter::KEY_Comma           ]= ",";
    m[osgGA::GUIEventAdapter::KEY_Minus           ]= "-";
    m[osgGA::GUIEventAdapter::KEY_Period          ]= ".";
    m[osgGA::GUIEventAdapter::KEY_Slash           ]= "/";
    m[osgGA::GUIEventAdapter::KEY_Colon           ]= ":";
    m[osgGA::GUIEventAdapter::KEY_Semicolon       ]= ";";
    m[osgGA::GUIEventAdapter::KEY_Less            ]= "<";
    m[osgGA::GUIEventAdapter::KEY_Equals          ]= "=";
    m[osgGA::GUIEventAdapter::KEY_Greater         ]= ">";
    m[osgGA::GUIEventAdapter::KEY_Question        ]= "?";
    m[osgGA::GUIEventAdapter::KEY_At              ]= "@";
    m[osgGA::GUIEventAdapter::KEY_Leftbracket     ]= "[";
    m[osgGA::GUIEventAdapter::KEY_Backslash       ]= "\\";
    m[osgGA::GUIEventAdapter::KEY_Rightbracket    ]= "]";
    m[osgGA::GUIEventAdapter::KEY_Caret           ]= "^";
    m[osgGA::GUIEventAdapter::KEY_Underscore      ]= "_";
    m[osgGA::GUIEventAdapter::KEY_Backquote       ]= "`";

    m[osgGA::GUIEventAdapter::KEY_BackSpace       ]= "Backspace";        /* back space; back char */
    m[osgGA::GUIEventAdapter::KEY_Tab             ]= "Tab";
    m[osgGA::GUIEventAdapter::KEY_Linefeed        ]= "Linefeed";        /* Linefeed; LF */
    m[osgGA::GUIEventAdapter::KEY_Clear           ]= "Clear";
    m[osgGA::GUIEventAdapter::KEY_Return          ]= "Return";        /* Return; enter */
    m[osgGA::GUIEventAdapter::KEY_Pause           ]= "Pause";        /* Pause; hold */
    m[osgGA::GUIEventAdapter::KEY_Scroll_Lock     ]= "Scroll-down";
    m[osgGA::GUIEventAdapter::KEY_Sys_Req         ]= "Sys-req";
    m[osgGA::GUIEventAdapter::KEY_Escape          ]= "Escape";
    m[osgGA::GUIEventAdapter::KEY_Delete          ]= "Delete";        /* Delete; rubout */


    m[osgGA::GUIEventAdapter::KEY_Home            ]= "Home";
    m[osgGA::GUIEventAdapter::KEY_Left            ]= "Left";        /* Move left; left arrow */
    m[osgGA::GUIEventAdapter::KEY_Up              ]= "Up";        /* Move up; up arrow */
    m[osgGA::GUIEventAdapter::KEY_Right           ]= "Right";        /* Move right; right arrow */
    m[osgGA::GUIEventAdapter::KEY_Down            ]= "Down";        /* Move down; down arrow */
    m[osgGA::GUIEventAdapter::KEY_Prior           ]= "Prior";        /* Prior; previous */
    m[osgGA::GUIEventAdapter::KEY_Page_Up         ]= "Page-up";
    m[osgGA::GUIEventAdapter::KEY_Next            ]= "Next";        /* Next */
    m[osgGA::GUIEventAdapter::KEY_Page_Down       ]= "Nage-down";
    m[osgGA::GUIEventAdapter::KEY_End             ]= "End";        /* EOL */
    m[osgGA::GUIEventAdapter::KEY_Begin           ]= "Begin";        /* BOL */


    m[osgGA::GUIEventAdapter::KEY_Select          ]= "0xFF60";        /* Select"; mark */
    m[osgGA::GUIEventAdapter::KEY_Print           ]= "0xFF61";
    m[osgGA::GUIEventAdapter::KEY_Execute         ]= "0xFF62";        /* Execute"; run"; do */
    m[osgGA::GUIEventAdapter::KEY_Insert          ]= "0xFF63";        /* Insert"; insert here */
    m[osgGA::GUIEventAdapter::KEY_Undo            ]= "0xFF65";        /* Undo"; oops */
    m[osgGA::GUIEventAdapter::KEY_Redo            ]= "0xFF66";        /* redo"; again */
    m[osgGA::GUIEventAdapter::KEY_Menu            ]= "0xFF67";        /* On Windows"; this is VK_APPS"; the context-menu key */
    m[osgGA::GUIEventAdapter::KEY_Find            ]= "0xFF68";        /* Find"; search */
    m[osgGA::GUIEventAdapter::KEY_Cancel          ]= "0xFF69";        /* Cancel"; stop"; abort"; exit */
    m[osgGA::GUIEventAdapter::KEY_Help            ]= "0xFF6A";        /* Help */
    m[osgGA::GUIEventAdapter::KEY_Break           ]= "0xFF6B";
    m[osgGA::GUIEventAdapter::KEY_Mode_switch     ]= "0xFF7E";        /* Character set switch */
    m[osgGA::GUIEventAdapter::KEY_Script_switch   ]= "0xFF7E";        /* Alias for mode_switch */
    m[osgGA::GUIEventAdapter::KEY_Num_Lock        ]= "0xFF7F";


    m[osgGA::GUIEventAdapter::KEY_KP_Space        ]= "0xFF80";        /* space */
    m[osgGA::GUIEventAdapter::KEY_KP_Tab          ]= "0xFF89";
    m[osgGA::GUIEventAdapter::KEY_KP_Enter        ]= "0xFF8D";        /* enter */
    m[osgGA::GUIEventAdapter::KEY_KP_F1           ]= "0xFF91";        /* PF1"; KP_A"; ... */
    m[osgGA::GUIEventAdapter::KEY_KP_F2           ]= "0xFF92";
    m[osgGA::GUIEventAdapter::KEY_KP_F3           ]= "0xFF93";
    m[osgGA::GUIEventAdapter::KEY_KP_F4           ]= "0xFF94";
    m[osgGA::GUIEventAdapter::KEY_KP_Home         ]= "0xFF95";
    m[osgGA::GUIEventAdapter::KEY_KP_Left         ]= "0xFF96";
    m[osgGA::GUIEventAdapter::KEY_KP_Up           ]= "0xFF97";
    m[osgGA::GUIEventAdapter::KEY_KP_Right        ]= "0xFF98";
    m[osgGA::GUIEventAdapter::KEY_KP_Down         ]= "0xFF99";
    m[osgGA::GUIEventAdapter::KEY_KP_Prior        ]= "0xFF9A";
    m[osgGA::GUIEventAdapter::KEY_KP_Page_Up      ]= "0xFF9A";
    m[osgGA::GUIEventAdapter::KEY_KP_Next         ]= "0xFF9B";
    m[osgGA::GUIEventAdapter::KEY_KP_Page_Down    ]= "0xFF9B";
    m[osgGA::GUIEventAdapter::KEY_KP_End          ]= "0xFF9C";
    m[osgGA::GUIEventAdapter::KEY_KP_Begin        ]= "0xFF9D";
    m[osgGA::GUIEventAdapter::KEY_KP_Insert       ]= "0xFF9E";
    m[osgGA::GUIEventAdapter::KEY_KP_Delete       ]= "0xFF9F";
    m[osgGA::GUIEventAdapter::KEY_KP_Equal        ]= "0xFFBD";        /* equals */
    m[osgGA::GUIEventAdapter::KEY_KP_Multiply     ]= "0xFFAA";
    m[osgGA::GUIEventAdapter::KEY_KP_Add          ]= "0xFFAB";
    m[osgGA::GUIEventAdapter::KEY_KP_Separator    ]= "0xFFAC";       /* separator"; often comma */
    m[osgGA::GUIEventAdapter::KEY_KP_Subtract     ]= "0xFFAD";
    m[osgGA::GUIEventAdapter::KEY_KP_Decimal      ]= "0xFFAE";
    m[osgGA::GUIEventAdapter::KEY_KP_Divide       ]= "0xFFAF";

    m[osgGA::GUIEventAdapter::KEY_KP_0            ]= "0xFFB0";
    m[osgGA::GUIEventAdapter::KEY_KP_1            ]= "0xFFB1";
    m[osgGA::GUIEventAdapter::KEY_KP_2            ]= "0xFFB2";
    m[osgGA::GUIEventAdapter::KEY_KP_3            ]= "0xFFB3";
    m[osgGA::GUIEventAdapter::KEY_KP_4            ]= "0xFFB4";
    m[osgGA::GUIEventAdapter::KEY_KP_5            ]= "0xFFB5";
    m[osgGA::GUIEventAdapter::KEY_KP_6            ]= "0xFFB6";
    m[osgGA::GUIEventAdapter::KEY_KP_7            ]= "0xFFB7";
    m[osgGA::GUIEventAdapter::KEY_KP_8            ]= "0xFFB8";
    m[osgGA::GUIEventAdapter::KEY_KP_9            ]= "0xFFB9";


    m[osgGA::GUIEventAdapter::KEY_F1              ]= "0xFFBE";
    m[osgGA::GUIEventAdapter::KEY_F2              ]= "0xFFBF";
    m[osgGA::GUIEventAdapter::KEY_F3              ]= "0xFFC0";
    m[osgGA::GUIEventAdapter::KEY_F4              ]= "0xFFC1";
    m[osgGA::GUIEventAdapter::KEY_F5              ]= "0xFFC2";
    m[osgGA::GUIEventAdapter::KEY_F6              ]= "0xFFC3";
    m[osgGA::GUIEventAdapter::KEY_F7              ]= "0xFFC4";
    m[osgGA::GUIEventAdapter::KEY_F8              ]= "0xFFC5";
    m[osgGA::GUIEventAdapter::KEY_F9              ]= "0xFFC6";
    m[osgGA::GUIEventAdapter::KEY_F10             ]= "0xFFC7";
    m[osgGA::GUIEventAdapter::KEY_F11             ]= "0xFFC8";
    m[osgGA::GUIEventAdapter::KEY_F12             ]= "0xFFC9";
    m[osgGA::GUIEventAdapter::KEY_F13             ]= "0xFFCA";
    m[osgGA::GUIEventAdapter::KEY_F14             ]= "0xFFCB";
    m[osgGA::GUIEventAdapter::KEY_F15             ]= "0xFFCC";
    m[osgGA::GUIEventAdapter::KEY_F16             ]= "0xFFCD";
    m[osgGA::GUIEventAdapter::KEY_F17             ]= "0xFFCE";
    m[osgGA::GUIEventAdapter::KEY_F18             ]= "0xFFCF";
    m[osgGA::GUIEventAdapter::KEY_F19             ]= "0xFFD0";
    m[osgGA::GUIEventAdapter::KEY_F20             ]= "0xFFD1";
    m[osgGA::GUIEventAdapter::KEY_F21             ]= "0xFFD2";
    m[osgGA::GUIEventAdapter::KEY_F22             ]= "0xFFD3";
    m[osgGA::GUIEventAdapter::KEY_F23             ]= "0xFFD4";
    m[osgGA::GUIEventAdapter::KEY_F24             ]= "0xFFD5";
    m[osgGA::GUIEventAdapter::KEY_F25             ]= "0xFFD6";
    m[osgGA::GUIEventAdapter::KEY_F26             ]= "0xFFD7";
    m[osgGA::GUIEventAdapter::KEY_F27             ]= "0xFFD8";
    m[osgGA::GUIEventAdapter::KEY_F28             ]= "0xFFD9";
    m[osgGA::GUIEventAdapter::KEY_F29             ]= "0xFFDA";
    m[osgGA::GUIEventAdapter::KEY_F30             ]= "0xFFDB";
    m[osgGA::GUIEventAdapter::KEY_F31             ]= "0xFFDC";
    m[osgGA::GUIEventAdapter::KEY_F32             ]= "0xFFDD";
    m[osgGA::GUIEventAdapter::KEY_F33             ]= "0xFFDE";
    m[osgGA::GUIEventAdapter::KEY_F34             ]= "0xFFDF";
    m[osgGA::GUIEventAdapter::KEY_F35             ]= "0xFFE0";


    m[osgGA::GUIEventAdapter::KEY_Shift_L         ]= "0xFFE1";        /* Left shift */
    m[osgGA::GUIEventAdapter::KEY_Shift_R         ]= "0xFFE2";        /* Right shift */
    m[osgGA::GUIEventAdapter::KEY_Control_L       ]= "0xFFE3";        /* Left control */
    m[osgGA::GUIEventAdapter::KEY_Control_R       ]= "0xFFE4";        /* Right control */
    m[osgGA::GUIEventAdapter::KEY_Caps_Lock       ]= "0xFFE5";        /* Caps lock */
    m[osgGA::GUIEventAdapter::KEY_Shift_Lock      ]= "0xFFE6";        /* Shift lock */

    m[osgGA::GUIEventAdapter::KEY_Meta_L          ]= "0xFFE7";        /* Left meta */
    m[osgGA::GUIEventAdapter::KEY_Meta_R          ]= "0xFFE8";        /* Right meta */
    m[osgGA::GUIEventAdapter::KEY_Alt_L           ]= "0xFFE9";        /* Left alt */
    m[osgGA::GUIEventAdapter::KEY_Alt_R           ]= "0xFFEA";        /* Right alt */
    m[osgGA::GUIEventAdapter::KEY_Super_L         ]= "0xFFEB";        /* Left super */
    m[osgGA::GUIEventAdapter::KEY_Super_R         ]= "0xFFEC";        /* Right super */
    m[osgGA::GUIEventAdapter::KEY_Hyper_L         ]= "0xFFED";        /* Left hyper */
    m[osgGA::GUIEventAdapter::KEY_Hyper_R         ]= "0xFFEE";         /* Right hyper */

    return m;
}

// static
std::string
util::osg::keyName(osgGA::GUIEventAdapter::KeySymbol key)
{
    NameMap::const_iterator const& it = _keys.find(key);
    return it != _keys.end()
        ? it->second
        : std::string();
}

//size_t //<! number of used characters
//util::osg::getKeyName(osgGA::GUIEventAdapter::KeySymbol key, char* buffer, size_t bufferSize)
//{
//  if (bufferSize == 0 || buffer == nullptr)
//      return false;
//
//  const std::string& ret = keyName(key);
//  size_t num = ret.size() + 1;
//  if (bufferSize < num)
//      num = bufferSize;
//  memcpy(buffer, ret.c_str(), sizeof(char) * num);
//
//  return num;
//}

// static inline
std::string
util::osg::modNames(osgGA::GUIEventAdapter::ModKeyMask mods)
{
    std::stringstream sstr;
    if (mods & osgGA::GUIEventAdapter::MODKEY_CTRL)
        sstr << "Ctrl+";
    if (mods & osgGA::GUIEventAdapter::MODKEY_SHIFT)
        sstr << "Shift+";
    if (mods & osgGA::GUIEventAdapter::MODKEY_ALT)
        sstr << "Alt+";
    return sstr.str();
}

// static inline
std::string
util::osg::buttonNames(osgGA::GUIEventAdapter::MouseButtonMask buttons)
{
    std::stringstream sstr;
    size_t n = 0;
    if (buttons & osgGA::GUIEventAdapter::LEFT_MOUSE_BUTTON)
    {
        sstr << "LMB"; ++n;
    }
    if (buttons & osgGA::GUIEventAdapter::MIDDLE_MOUSE_BUTTON)
    {
        sstr << (sstr.str().empty() ? "" : ", ") << "MMB"; ++n;
    }
    if (buttons & osgGA::GUIEventAdapter::RIGHT_MOUSE_BUTTON)
    {
        sstr << (sstr.str().empty() ? "" : ", ") << "RMB"; ++n;
    }
    return n > 1
        ? "(" + sstr.str() + ")"
        : sstr.str();
}
