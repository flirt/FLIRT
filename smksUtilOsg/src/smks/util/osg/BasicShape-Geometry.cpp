// ========================================================================= //
// Copyright 2016-2018 SUPAMONKS_STUDIO                                      //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
//                                                                           //
// ========================================================================= //
#include <osg/Callback>

#include <smks/sys/ResourceDatabase.hpp>
#include <smks/view/gl/ContextExtensions.hpp>
#include <smks/view/gl/uniform.hpp>

#include "smks/util/osg/drawable.hpp"
#include "smks/util/osg/geometry.hpp"
#include "BasicShape-Geometry.hpp"

namespace smks { namespace util { namespace osg
{
    ///////////////////////////////////////////////
    // class BasicShape::Geometry::UpdateCallback
    ///////////////////////////////////////////////
    class BasicShape::Geometry::UpdateCallback:
        public ::osg::Callback
    {
    public:
        virtual inline
        bool
        run(::osg::Object* object, ::osg::Object* data)
        {
            Geometry* geo = reinterpret_cast<Geometry*>(object);
            if (geo)
                geo->loadProgram();
            return traverse(object, data);
        }
    };
}
}
}

using namespace smks;

// explicit
util::osg::BasicShape::Geometry::Geometry(sys::ResourceDatabase::WPtr const& resources):
    ::osg::Geometry     (),
    _resources          (resources),
    _uColor             (nullptr),
    _precomputedBounds  (),
    _vertexData         (nullptr),
    _vertexDataSize     (0),
    _stride             (0),
    _indexData          (nullptr),
    _numIndices         (0),
    _mode               (0),
    //--------------
    _bufferObjects      ()
{
    if (!_resources.lock())
        throw sys::Exception("Invalid resource database.", "Basic Shape Geometry Construction");
    util::osg::setupRender(*this);

    ::osg::StateSet* stateSet = getOrCreateStateSet();
    stateSet->setDataVariance(::osg::Object::DYNAMIC);  // prevents update phase to commence before all draw phases finish
    addUpdateCallback(new UpdateCallback());            // assign program at update phase
}

void
util::osg::BasicShape::Geometry::build()
{
    _uColor = getColorUniform(-1);
    assert(_uColor);
    getOrCreateStateSet()->addUniform(_uColor->data());
}

// virtual
util::osg::BasicShape::Geometry::~Geometry()
{
    dispose();
}

void
util::osg::BasicShape::Geometry::dispose()
{
    sys::ResourceDatabase::Ptr const&   resources   = _resources.lock();
    view::gl::ContextExtensions*        extensions  = resources ? view::gl::getContextExtensions(*resources) : nullptr;

    _precomputedBounds.init();
    _vertexData     = nullptr;
    _vertexDataSize = 0;
    _stride         = 0;
    _indexData      = nullptr;
    _numIndices     = 0;
    _mode           = 0;
    if (extensions)
        for (size_t contextId = 0; contextId < _bufferObjects.size(); ++contextId)
        {
            BufferObjects& buffers = _bufferObjects.get(contextId);
            if (buffers)
                extensions->getOrCreateGLExtension(contextId)->glDeleteBuffers(2, &buffers.vbo);
        }
}

void
util::osg::BasicShape::Geometry::setVertexData(const float* vertexData,
                                               size_t       vertexDataSize,
                                               size_t       stride)
{
    _vertexData         = vertexData;
    _vertexDataSize     = vertexDataSize;
    _stride             = stride;
    _precomputedBounds  = util::osg::computeBounds(_vertexData, _vertexDataSize, _stride);
    dirtyBound();
}

void
util::osg::BasicShape::Geometry::setIndexData(const unsigned int*   indexData,
                                              size_t                numIndices,
                                              GLenum                mode)
{
    _indexData  = indexData;
    _numIndices = numIndices;
    _mode       = mode;
}

void
util::osg::BasicShape::Geometry::setColor(unsigned int color)
{
    if (_uColor && _uColor->data())
        view::gl::setUniformui(*_uColor->data(), color);
}

void
util::osg::BasicShape::Geometry::drawImplementation(::osg::RenderInfo& renderInfo) const
{
    if (!isProgramUpToDate())
        return;

    if (!_vertexData || _vertexDataSize == 0 || _stride == 0 ||
        !_indexData || _numIndices == 0 || _mode == 0)
        return;
    //---
    sys::ResourceDatabase::Ptr const&   resources   = _resources.lock();
    const view::gl::ContextExtensions*  extensions  = resources ? view::gl::getContextExtensions(*resources) : nullptr;
    if (!extensions)
        return;

    assert(renderInfo.getState());

    ::osg::State&               state       = *renderInfo.getState();
    const unsigned int          contextId   = state.getContextID();
    BufferObjects&              buffers     = _bufferObjects.get(contextId);
    const ::osg::GLExtensions*  ext         = const_cast<view::gl::ContextExtensions*>(extensions)
        ->getOrCreateGLExtension(contextId);

    assert(ext);

    const bool      generateBuffers = !buffers;
    const GLuint    locPosition     = state.getVertexAlias()._location;

    if (generateBuffers)
        ext->glGenBuffers(2, &buffers.vbo);

    ext->glBindBuffer(GL_ARRAY_BUFFER,          buffers.vbo);
    ext->glBindBuffer(GL_ELEMENT_ARRAY_BUFFER,  buffers.ebo);

    if (generateBuffers)
        ext->glVertexAttribPointer(locPosition, 3, GL_FLOAT, false, _stride * sizeof(float), 0);

    if (buffers.dirty)
    {
        ext->glBufferData(
            GL_ARRAY_BUFFER,
            _vertexDataSize * sizeof(float),
            _vertexData,
            GL_DYNAMIC_DRAW);
        ext->glBufferData(
            GL_ELEMENT_ARRAY_BUFFER,
            _numIndices * sizeof(unsigned int),
            _indexData,
            GL_DYNAMIC_DRAW);
        buffers.dirty = false;
    }

    ext->glEnableVertexAttribArray(locPosition);
    //----------------------------------------------------
    glDrawElements(_mode, _numIndices, GL_UNSIGNED_INT, 0);
    //----------------------------------------------------
    ext->glDisableVertexAttribArray(locPosition);
    ext->glBindBuffer(GL_ARRAY_BUFFER,          0);
    ext->glBindBuffer(GL_ELEMENT_ARRAY_BUFFER,  0);
}

bool
util::osg::BasicShape::Geometry::isProgramUpToDate() const
{
    return getStateSet() &&
        getStateSet()->getAttribute(::osg::StateAttribute::PROGRAM);
}

void
util::osg::BasicShape::Geometry::loadProgram()
{
    if (isProgramUpToDate())
        return;

    sys::ResourceDatabase::Ptr const& resources = _resources.lock();
    if (!resources)
        return;

    view::gl::getOrCreateContextExtensions(*resources);
    ::osg::ref_ptr<::osg::Program> const& program = getProgram();
    if (!program)
        return;

    assert(getStateSet());
    getStateSet()->setAttribute(program.get(), ::osg::StateAttribute::OVERRIDE);
}
