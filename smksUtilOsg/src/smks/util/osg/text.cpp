// ========================================================================= //
// Copyright 2016-2018 SUPAMONKS_STUDIO                                      //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
//                                                                           //
// ========================================================================= //
#include <osgText/Text>
#include "smks/util/osg/text.hpp"

using namespace smks;

::osg::ref_ptr<osgText::Text>
util::osg::createLabel(const char*  text,
                       int          px,
                       int          py,
                       float        characterSize,
                       const char*  font)
{
    ::osg::ref_ptr<osgText::Text> ret = new osgText::Text();

    //ret->setUseDisplayList(false);
    //ret->setUseVertexArrayObject(true);

    //if (strlen(osgName) > 0)
    //  ret->setName(osgName);
    ret->setPosition(::osg::Vec3f(
        static_cast<float>(px),
        static_cast<float>(py),
        0.0f));
    ret->setCharacterSize(characterSize);
    ret->setFont(font);
    ret->setText(text);

    return ret;
}
