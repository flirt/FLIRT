// ========================================================================= //
// Copyright 2016-2018 SUPAMONKS_STUDIO                                      //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
//                                                                           //
// ========================================================================= //
#include <cassert>
#include <iostream>

#include <osg/Geometry>

#include <smks/util/color/rgba8.hpp>
#include "smks/util/osg/BasicShape.hpp"
#include "BasicShape-Geometry.hpp"

using namespace smks;

// explicit
util::osg::BasicShape::BasicShape(ResourcesPtr const& resources):
    ::osg::Geode(),
    _resources(resources)
{
    for (size_t typ = 0; typ < NUM_COMPONENTS; ++typ)
        _geometries[typ] = nullptr;
}

void
util::osg::BasicShape::fillColor(unsigned int color)
{
    setComponentColor(COMPONENT_FILL, color);
}

void
util::osg::BasicShape::strokeColor(unsigned int color)
{
    setComponentColor(COMPONENT_STROKE, color);
}

void
util::osg::BasicShape::setComponentColor(ComponentType  typ,
                                         unsigned int   color)
{
    std::string compName = "";
    switch (typ)
    {
    case COMPONENT_STROKE:  compName = "stroke";    break;
    case COMPONENT_FILL:    compName = "fill";      break;
    default:                                        break;
    }

    unsigned char rgba[4] = { 0, 0, 0, 0 };

    util::color::uintToUchar4(color, rgba);
    if (rgba[3] > 0)
    {
        if (!_geometries[typ])
        {
            size_t vertexDataSize   = 0;
            size_t stride           = 0;
            size_t numIndices       = 0;
            GLenum mode             = 0;

            const float*        vertexData  = getVertexData(vertexDataSize, stride);
            const unsigned int* indexData   = getIndexData(static_cast<ComponentType>(typ), numIndices, mode);

            switch (typ)
            {
            case COMPONENT_STROKE:  _geometries[typ] = StrokeGeometry::create(_resources);  break;
            case COMPONENT_FILL:    _geometries[typ] = FillGeometry ::create(_resources);   break;
            default:                                                                        break;
            }
            _geometries[typ]->setVertexData(vertexData, vertexDataSize, stride);
            _geometries[typ]->setIndexData(indexData, numIndices, mode);
            _geometries[typ]->setName("__" + std::string(name()) + "." + compName);
            addDrawable(_geometries[typ].get());
        }

        ::osg::StateSet* stateSet = _geometries[typ]->getOrCreateStateSet();

        _geometries[typ]->setColor(color);
        if (rgba[3] < 255)
        {
            stateSet->setMode(GL_BLEND, ::osg::StateAttribute::ON);
            stateSet->setRenderingHint(::osg::StateSet::TRANSPARENT_BIN);
        }
        else
        {
            stateSet->setMode(GL_BLEND, ::osg::StateAttribute::OFF);
            stateSet->setRenderingHint(::osg::StateSet::OPAQUE_BIN);
        }
    }
    else
    {
        if (_geometries[typ])
        {
            removeDrawable(_geometries[typ].get());
            _geometries[typ] = nullptr;
        }
    }
}
