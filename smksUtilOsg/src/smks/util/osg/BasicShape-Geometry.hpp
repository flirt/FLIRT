// ========================================================================= //
// Copyright 2016-2018 SUPAMONKS_STUDIO                                      //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
//                                                                           //
// ========================================================================= //
#pragma once

#include <cassert>
#include <osg/Geometry>

#include <smks/sys/Exception.hpp>
#include <smks/xchg/Buffered.hpp>
#include <smks/view/gl/ProgramInputDeclarations.hpp>
#include <smks/view/gl/ProgramTemplate.hpp>
#include <smks/view/gl/ProgramTemplates.hpp>

#include "smks/util/osg/BasicShape.hpp"

namespace smks
{
    namespace sys
    {
        class ResourceDatabase;
    }

    namespace util { namespace osg
    {
        class BasicShape::Geometry:
            public ::osg::Geometry
        {
            friend class UpdateCallback;
        protected:
            typedef std::weak_ptr<sys::ResourceDatabase> ResourcesWPtr;
        private:
            //////////////////////
            // struct BufferObjects
            //////////////////////
            struct BufferObjects
            {
                GLuint  vbo, ebo;
                bool    dirty;
            public:
                inline BufferObjects(): vbo(0), ebo(0), dirty(true) { }
                inline operator bool() const { return vbo != 0 && ebo != 0; }
            };
            //////////////////////
            typedef xchg::Buffered<BufferObjects> PerContextBuffers;
            class UpdateCallback;
        protected:
            const ResourcesWPtr         _resources;
        private:
            view::gl::UniformInput::Ptr _uColor;
            ::osg::BoundingBoxf         _precomputedBounds;
            const float*                _vertexData;
            size_t                      _vertexDataSize;
            size_t                      _stride;
            const unsigned int*         _indexData;
            size_t                      _numIndices;
            GLenum                      _mode;
            //------------------
            mutable PerContextBuffers   _bufferObjects;

        protected:
            explicit
            Geometry(ResourcesWPtr const&);

            void
            build();
        protected:
            virtual
            ::osg::ref_ptr<::osg::Program>
            getProgram() const = 0;

            virtual
            view::gl::UniformInput::Ptr
            getColorUniform(unsigned int) = 0;

        private:
            // non-copyable
            Geometry(const Geometry&);
            Geometry* operator=(const Geometry&);

        public:
            virtual
            ~Geometry();

            void
            dispose();

            void
            setVertexData(const float*, size_t vertexDataSize, size_t stride);

            void
            setIndexData(const unsigned int*, size_t, GLenum );

            void
            setColor(unsigned int);

            virtual inline
            ::osg::BoundingBox
            computeBoundingBox() const
            {
                return _precomputedBounds;
            }

            virtual
            void
            drawImplementation(::osg::RenderInfo&) const;
        private:
            bool
            isProgramUpToDate() const;
            void
            loadProgram();
        };

        ///////////////////////////////////
        // class BasicShape::StrokeGeometry
        ///////////////////////////////////
        class BasicShape::StrokeGeometry:
            public BasicShape::Geometry
        {
        public:
            typedef ::osg::ref_ptr<StrokeGeometry> Ptr;
        public:
            static inline
            Ptr
            create(ResourcesWPtr const& resources)
            {
                Ptr ptr(new StrokeGeometry(resources));
                ptr->build();
                return ptr;
            }
        protected:
            explicit inline
            StrokeGeometry(ResourcesWPtr const& resources):
                Geometry(resources)
            { }

            virtual inline
            ::osg::ref_ptr<::osg::Program>
            getProgram() const
            {
                ResourcesPtr const& resources = _resources.lock();
                return resources
                    ? view::gl::getOrCreateProgram(view::gl::get_LineProgram(), resources)
                    : nullptr;
            }

            virtual inline
            view::gl::UniformInput::Ptr
            getColorUniform(unsigned int color)
            {
                return view::gl::get_LineProgram().createUniform(view::gl::uColor(), color);
            }
        };

        /////////////////////////////////
        // class BasicShape::FillGeometry
        /////////////////////////////////
        class BasicShape::FillGeometry:
            public BasicShape::Geometry
        {
        public:
            typedef ::osg::ref_ptr<FillGeometry> Ptr;
        public:
            static inline
            Ptr
            create(ResourcesWPtr const& resources)
            {
                Ptr ptr(new FillGeometry(resources));
                ptr->build();
                return ptr;
            }
        protected:
            explicit inline
            FillGeometry(ResourcesWPtr const& resources):
                Geometry(resources)
            { }

            virtual inline
            ::osg::ref_ptr<::osg::Program>
            getProgram() const
            {
                ResourcesPtr const& resources = _resources.lock();
                return resources
                    ? view::gl::getOrCreateProgram(view::gl::get_LineProgram(), resources)
                    : nullptr;
            }

            virtual inline
            view::gl::UniformInput::Ptr
            getColorUniform(unsigned int color)
            {
                return view::gl::get_LineProgram().createUniform(view::gl::uColor(), color);
            }
        };
    }
}
}
