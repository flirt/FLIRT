// ========================================================================= //
// Copyright 2016-2018 SUPAMONKS_STUDIO                                      //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
//                                                                           //
// ========================================================================= //
#include <osg/Geode>
#include <osg/Geometry>

#include "smks/util/osg/basic_shapes.hpp"
#include "smks/util/osg/UnitDiskXY.hpp"
#include "BasicShape-Geometry.hpp"

using namespace smks;

util::osg::UnitDiskXY::UnitDiskXY(ResourcesPtr const&   resources,
                                  size_t                numSegments,
                                  const math::Affine3f& xfm):
    BasicShape      (resources),
    _strokeLines    (),
    _fillTriangles  (),
    _positions3d    ()
{
    appendUnitDiskXY(_positions3d, _strokeLines, &_fillTriangles, numSegments, xfm);
}

const float*
util::osg::UnitDiskXY::getVertexData(size_t& size, size_t& stride)
{
    size    = _positions3d.size();
    stride  = 3;
    return !_positions3d.empty() ? &_positions3d.front() : nullptr;
}

const unsigned int*
util::osg::UnitDiskXY::getIndexData(ComponentType type, size_t& size, GLenum& mode)
{
    if (type == BasicShape::COMPONENT_FILL)
    {
        size = _fillTriangles.size();
        mode = GL_TRIANGLES;
        return !_fillTriangles.empty() ? &_fillTriangles.front() : nullptr;
    }
    else
    {
        size = _strokeLines.size();
        mode = GL_LINES;
        return !_strokeLines.empty() ? &_strokeLines.front() : nullptr;
    }
}
