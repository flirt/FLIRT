-- ========================================================================= --
-- Copyright 2016-2018 SUPAMONKS_STUDIO                                      --
-- Author: Pierre-Edouard Landes <pel@supamonks.com>                         --
--                                                                           --
-- This program is free software: you can redistribute it and/or modify      --
-- it under the terms of the GNU Lesser General Public License as            --
-- published by the Free Software Foundation, either version 3 of the        --
-- License, or (at your option) any later version.                           --
--                                                                           --
-- This program is distributed in the hope that it will be useful,           --
-- but WITHOUT ANY WARRANTY; without even the implied warranty of            --
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             --
-- GNU Lesser General Public License for more details.                       --
--                                                                           --
-- You should have received a copy of the GNU Lesser General Public License  --
-- along with this program.  If not, see <https://www.gnu.org/licenses/>.    --
--                                                                           --
-- ========================================================================= --


smks.module.uses_util_osg = function()

    smks.module.links(
        smks.module.name.util_osg,
        smks.module.kind.util_osg)

    smks.dep.osg.includes()
    smks.dep.eigen.includes()

    smks.module.includes(smks.module.name.math)

end


smks.module.copy_util_osg_to_targetdir = function()

    smks.module.copy_to_targetdir(
        smks.module.name.util_osg)

    smks.module.copy_view_gl_to_targetdir()

end


project(smks.module.name.util_osg)

    language 'C++'
    smks.module.set_kind(smks.module.kind.util_osg)

    files
    {
        'include/**.hpp',
        'src/**.hpp',
        'src/**.cpp',
    }
    defines
    {
        'FLIRT_UTIL_OSG_DLL',
    }
    includedirs
    {
        'include',
    }

    smks.dep.boost.includes()
    smks.dep.osg.links()
    smks.dep.opengl.links()

    smks.module.includes(smks.module.name.sys)
    smks.module.includes(smks.module.name.util)
    smks.module.includes(smks.module.name.asset)
    smks.module.uses_view_gl()
