# Building FLIRT

FLIRT's build system relies on **premake5** and once its dependencies are properly configurated, only requires users to run the `premake5 [action]` command at the root of the repository in order to generate the solution and its projects.

FLIRT comes with a range of dependencies, which are all listed below:

* Boost C++ Libraries (with regex, system, and filesystem as static libraries),
* Eigen,
* zlib,
* HDF5,
* Alembic,
* OpenSubdiv,
* OpenSceneGraph (compiled with core OpenGL 3.1),
* Embree,
* Intel Threading Building Blocks,
* IlmBase,
* OpenEXR,
* libpng,
* libjpeg,
* LibTIFF,
* OpenColorIO,
* jsoncpp,
* CPython 2.7,
* Easylogging++,
* Templatized C++ Command Line Parser Library (only used by examples),
* googletest (only used by tests).

Most of the build system's scripts are located in the build/ folder.

By default, all paths used to find dependencies' headers and binaries are expressed relatively to the directory pointed by the `FLIRT_DEPENDS_DIR` environment variable.
If needed, one can edit the `build/smks.dep.*.lua` script files in order to change each dependency's include paths, library paths, and binary names.

## Downloads

* **Prebuilt dependency package**
    * Visual Studio 2015 (VC14) for 64bit: [flirt-dep-msvc14.7z](https://spaces.hightail.com/space/DoUj5cczz9)
