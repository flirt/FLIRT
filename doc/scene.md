# Scene Representation and Management

All components from the FLIRT framework ultimately refer to the concept of its scene, which stores all necessary information for the tasks at hand, may it be its real-time inspection or rendering.

## Structure

The FLIRT **scene** is at its core a directed acyclic graph:

* with nodes representing any entity or action necessary for preview and rendering (from geometries, to materials, to lighting conditions, to rendering configurations),

* and the edges of which are used to accumulate specific attributes as one goes down the hierarchy (typically 3D objects' local transforms and visibility statuses).

At a finer level, **nodes** are nothing but plain parameter containers. Parameters map a name to a value which may come in many types: numbers, integers, vectors, matrices, strings and so on. Alongside the system, users can freely add arbitrary parameters to any scene node, and while node instances track the value changes of some specifically named parameters to be properly defined or trigger actions, close to no restriction is put upon any node's parameter set. In any case, users can query any node instance's *"relevant"* set of parameters to avoid clashing names with reserved parameters. Refer to the [BuiltIns.hpp](../smksXchg/include/smks/parm/BuiltIns.hpp) file to find the complete list of *"built-in"*parameters recognized by the FLIRT framework.

## Update Propagation

Upon any change in terms of layout or content, the FLIRT scene propagates editing information via **events**. They synchronously traverse the scene hierarchy and framework components, and enable the dynamic update of the whole system. Tracking and reacting to such events is thus crucial at each level of the system. To this end :

* Nodes can be *"linked"* together: the link's target node receives events relative to the changes of the parameter values from the link's source node;

* Parameters can be *"connected"* together: whenever the value of the connection's master parameter changes, the value of the connection's slave parameter is appropriately updated (connections can be assigned priority values to disambiguate cases when slave parameters receive multiple incoming connections).

Scene-scale events include: node creation, node deletion, parameter connection, parameter disconnection, node linking, node unlinking, link attachment, and link detachment. Parameter-related events include: parameter addition, update, and removal.

## Content Initialization

The FLIRT scene can load and embed the content of **Alembic archives** into its hierarchy. At the time of the writing, Alembic imports only support xforms, cameras, polygonal meshes, curve sets and point sets. Users are free to manually create any node that is not carrying geometric information. Such nodes includes lights and materials which, while mandatory to rendering, are not currently automatically recognized at the Alembic import stage. Polygonal meshes can be dynamically subdivided using **OpenSubdiv**.

## Scene Rendering

In order to render the scene, users must create a **RenderAction** node, configure it, and switch its *"rendering"* state parameter to *active*.

The configuration of a rendering boils down to the creation of a small node network around the render action:

* the render action is to be linked to a renderer, a framebuffer, and the camera through which the scene must be rendered;

* the renderer is to be linked to an integrator, a sampler factory, and a pixel filter;

* the framebuffer may be linked to a denoiser, a tonemapper, and one or more render passes.

Each of the aforementioned nodes partially controls the configuration of the render action's rendering. All of them can have their parameters independently tuned and shared by several render actions throughout the scene.
