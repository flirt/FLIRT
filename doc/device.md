# Visualizing the Scene

Scene visualizations are provided by **devices**. Clients are in charge of creating devices and constantly maintaining their states up-to-date with the scene's. At the time of writing, the FLIRT framework defines two distinct device interfaces and provides each of them with one implementation:

* [`smks::rndr::AbstractDevice`](../smksRndrDeviceAPI/include/smks/rndr/AbstractDevice.hpp) is the interface for rendering devices,
* [`smks::view::AbstractDevice`](../smksViewDeviceAPI/include/smks/view/AbstractDevice.hpp) is the interface for viewport devices.

## Rendering Devices

Implementations of rendering devices are expected to define functions for declaring, parameterizing, and associating internal objects. Those functions are then directly invoked by the rendering client, and actual images are produced through calls to the `renderFrame` method.

Beside the implementation of the `smks::rndr::AbstractDevice` interface, modules implementing rendering devices must also define and expose the following:

`smks::rndr::AbstractDevice* create(const char* jsonCfg, std::shared_ptr<smks::ClientBase> const&);`
`void destroy(smks::rndr::AbstractDevice*);`

The rendering device implementation we propose is a **physically-based path tracer** that relies on Embree for fast ray tracing.

## Viewport Devices

Implementations of viewport devices may appear more concise, but are intrinsically bound to the **OpenSceneGraph** toolkit (used for OpenGL rendering). Indeed, `smks::view::AbstractDevice` inherits from `osgViewer::Viewer`, so its derived classes only need to define a handful of methods.

Modules implementing rendering devices must finally define and expose the following:

`smks::view::AbstractDevice* createDevice(const char* jsonCfg, std::shared_ptr<smks::ClientBase> const&, osg::Group*, osg::GraphicsContext*);`
`void destroyDevice(smks::view::AbstractDevice* device);`

Split into several complementary modules, the code of the provided viewport device proposes the following features:

* mouse-based picking,
* camera controls: switch between cameras, free orbit camera, zoom to object;
* animation controls,
* light controls,
* framebuffer display and progressive rendering,
* head-up display and onscreen help.

Mouse-based, key-based controls and scene editing are all developed in C++ and can be entirely deactivated depending on how the framework gets configured at startup (by setting most `viewport_*` options to `false`, see [cfg.hpp](../smksXchg/include/smks/xchg/cfg.hpp) for details). It may come in handy when using the Python module and redefining the GUI and user interactions in that language.
