FLIRT Module Documentation
==========================

.. toctree::
   :maxdepth: 2

Introduction
------------

.. automodule:: _pyflirt

Functions
---------
.. autofunction:: _pyflirt.add_include_directory

.. autofunction:: _pyflirt.connect_id_node_parameters

.. autofunction:: _pyflirt.connect_node_parameters

.. autofunction:: _pyflirt.create_archive

.. autofunction:: _pyflirt.create_assign

.. autofunction:: _pyflirt.create_camera

.. autofunction:: _pyflirt.create_data_node

.. autofunction:: _pyflirt.create_denoiser

.. autofunction:: _pyflirt.create_flat_render_pass

.. autofunction:: _pyflirt.create_framebuffer

.. autofunction:: _pyflirt.create_id_assign

.. autofunction:: _pyflirt.create_integrator

.. autofunction:: _pyflirt.create_light

.. autofunction:: _pyflirt.create_lightpath_expression_pass

.. autofunction:: _pyflirt.create_material

.. autofunction:: _pyflirt.create_pixel_filter

.. autofunction:: _pyflirt.create_render_action

.. autofunction:: _pyflirt.create_renderer

.. autofunction:: _pyflirt.create_sampler_factory

.. autofunction:: _pyflirt.create_selector

.. autofunction:: _pyflirt.create_shape_id_coverage_pass

.. autofunction:: _pyflirt.create_subsurface

.. autofunction:: _pyflirt.create_texture

.. autofunction:: _pyflirt.create_tonemapper

.. autofunction:: _pyflirt.create_xform

.. autofunction:: _pyflirt.deregister_observer

.. autofunction:: _pyflirt.deregister_progress_callbacks

.. autofunction:: _pyflirt.destroy_data_node

.. autofunction:: _pyflirt.disconnect_node_parameters

.. autofunction:: _pyflirt.finalize

.. autofunction:: _pyflirt.get_class_codes

.. autofunction:: _pyflirt.get_connection_info

.. autofunction:: _pyflirt.get_current_node_parameters

.. autofunction:: _pyflirt.get_links_between_nodes

.. autofunction:: _pyflirt.get_module_info

.. autofunction:: _pyflirt.get_node_child

.. autofunction:: _pyflirt.get_node_class

.. autofunction:: _pyflirt.get_node_full_name

.. autofunction:: _pyflirt.get_node_ids

.. autofunction:: _pyflirt.get_node_link_sources

.. autofunction:: _pyflirt.get_node_link_targets

.. autofunction:: _pyflirt.get_node_name

.. autofunction:: _pyflirt.get_node_num_children

.. autofunction:: _pyflirt.get_node_parameter

.. autofunction:: _pyflirt.get_node_parameter_info

.. autofunction:: _pyflirt.get_node_parent

.. autofunction:: _pyflirt.get_relevant_node_parameters

.. autofunction:: _pyflirt.get_scene

.. autofunction:: _pyflirt.initialize

.. container:: toggle

    .. container:: header

        Show/hide configuration options

    * **cache_anim**: enable/disable animation caching
    * **default_curve_width**
    * **default_point_width**
    * **gl_texture_size**: viewport's maximal texture resolution
    * **gl_udim_tile_size**: viewport's maximal UDIM tile resolution
    * **log_file**
    * **num_threads**
    * **rendering_device**: path used by the rendering client to find its device's module
    * **rt_core_config**: configuration string passed to the rendering device
    * **screen_height**
    * **screen_ratio**
    * **screen_width**
    * **viewport_animation**: enable/disable viewport's animation controls
    * **viewport_auto_play**: enable/disable viewport's animation auto-play
    * **viewport_camera_focus**: enable/disable viewport camera's zoom-to-object
    * **viewport_camera_switch**: enable/disable viewport's camera switch
    * **viewport_device**: path used by the viewport client to find its device's module
    * **viewport_fbuffer_switch**: enable/disable viewport's switch on framebuffer display
    * **viewport_hud**: enable/disable viewport's head-up display
    * **viewport_light_focus**: enable/disable viewport light's align-to-view
    * **viewport_manipulator**: enable/disable selection manipulation in viewport
    * **viewport_obj_loader**: enable/disable viewport's prompt to load geometric content
    * **viewport_orbit_camera**: enable/disable orbit controls on viewport's camera
    * **viewport_picking**: enable/disable object picking in viewport
    * **viewport_snapshot**
    * **window_title**
    * **with_rendering**: active/deactivate the creation of the scene's rendering client and device
    * **with_viewport**: active/deactivate the creation of the scene's viewport client and device


.. autofunction:: _pyflirt.is_node_parameter_readable

.. autofunction:: _pyflirt.is_node_parameter_writable

.. autofunction:: _pyflirt.key_press_event

.. autofunction:: _pyflirt.key_release_event

.. autofunction:: _pyflirt.mouse_double_click_event

.. autofunction:: _pyflirt.mouse_move_event

.. autofunction:: _pyflirt.mouse_press_event

.. autofunction:: _pyflirt.mouse_release_event

.. autofunction:: _pyflirt.paint_gl

.. autofunction:: _pyflirt.register_node_parameter_observer

.. autofunction:: _pyflirt.register_progress_callbacks

.. autofunction:: _pyflirt.register_scene_observer

.. autofunction:: _pyflirt.resize_event

.. autofunction:: _pyflirt.run

.. autofunction:: _pyflirt.set_connection_priority

.. autofunction:: _pyflirt.set_id_node_parameter_by_name

.. autofunction:: _pyflirt.set_node_parameter

.. autofunction:: _pyflirt.set_node_parameter_by_name

.. autofunction:: _pyflirt.set_switch_node_parameter_by_name

.. autofunction:: _pyflirt.set_trigger_node_parameter_by_name

.. autofunction:: _pyflirt.unset_node_parameter

.. autofunction:: _pyflirt.wheel_event

Enumerations
------------
.. autoclass:: _pyflirt.ActionState
    :members:
.. container:: toggle

    .. container:: header

        Show/hide action state values

    * ACTIVATING
    * ACTIVE
    * DEACTIVATING
    * INACTIVE


.. autoclass:: _pyflirt.BuiltIns
    :members:
.. container:: toggle

    .. container:: header

        Show/hide built-in parameters

    * **accumulate** (bool)
    * **backplate** (object)
    * **backplatePath** (string)
    * **backScattering** (float)
    * **backScatteringMap** (unsigned int)
    * **boundingBox** (bounding box)
    * **cameraId** (unsigned int)
    * **castsShadows** (bool)
    * **center** (4-float vector)
    * **code** (string)
    * **config** (string)
    * **contrastLevel** (char)
    * **currentlyVisible** (char)
    * **currentSampleIdx** (int)
    * **currentTime** (float)
    * **denoiserId** (unsigned int)
    * **diffuseReflectance** (4-float vector)
    * **diffuseReflectanceMap** (unsigned int)
    * **diffuseTransmission** (4-float vector)
    * **diffuseTransmissionMap** (unsigned int)
    * **displayName** (string)
    * **displayViewName** (string)
    * **exposureFstops** (float)
    * **extIOR** (float)
    * **extTransmission** (4-float vector)
    * **eyeCorneaIOR** (float)
    * **eyeGlobeColor** (4-float vector)
    * **eyeHighlightAngSizeD** (float)
    * **eyeHighlightColor** (4-float vector)
    * **eyeHighlightCoordsD** (2-float vector)
    * **eyeHighlightSmoothness** (float)
    * **eyeIrisAngSizeD** (float)
    * **eyeIrisColor** (4-float vector)
    * **eyeIrisDepth** (float)
    * **eyeIrisDimensions** (2-float vector)
    * **eyeIrisEdgeBlur** (float)
    * **eyeIrisEdgeOffset** (float)
    * **eyeIrisNormalSmoothness** (float)
    * **eyeIrisRotationD** (float)
    * **eyePupilBlur** (float)
    * **eyePupilDimensions** (2-float vector)
    * **eyePupilSize** (float)
    * **eyeRadius** (float)
    * **falloff** (float)
    * **falloffMap** (unsigned int)
    * **filePath** (string)
    * **focalDistance** (float)
    * **forceInitialization** (bool)
    * **forciblyVisible** (char)
    * **frameBufferId** (unsigned int)
    * **frameData** (data)
    * **framerate** (float)
    * **fresnelName** (string)
    * **fresnelValue** (4-float vector)
    * **fresnelWeight** (float)
    * **fresnelWeightMap** (unsigned int)
    * **front** (4-float vector)
    * **gamma** (float)
    * **glCount** (size)
    * **glIndices** (data)
    * **glitterColor** (4-float vector)
    * **glitterColorMap** (unsigned int)
    * **glitterSpread** (float)
    * **glitterSpreadMap** (unsigned int)
    * **glNormals** (data stream)
    * **globalInitialization** (char)
    * **globallyVisible** (char)
    * **globalNumSamples** (size)
    * **glossyReflectance** (4-float vector)
    * **glossyReflectanceMap** (unsigned int)
    * **glPositions** (data stream)
    * **glTexcoords** (data stream)
    * **glVertices** (data)
    * **glWidths** (data stream)
    * **glWireIndices** (data)
    * **halfAngleD** (float)
    * **halfWidth** (float)
    * **hdriMap** (unsigned int)
    * **height** (size)
    * **horizonScatteringColor** (4-float vector)
    * **horizonScatteringColorMap** (unsigned int)
    * **horizonScatteringFalloff** (float)
    * **horizonScatteringFalloffMap** (unsigned int)
    * **id** (unsigned int)
    * **illumMask** (int)
    * **image** (object)
    * **inheritsTransforms** (bool)
    * **initialization** (char)
    * **inputColorspace** (string)
    * **inputId0** (unsigned int)
    * **inputId1** (unsigned int)
    * **integratorId** (unsigned int)
    * **intensity** (4-float vector)
    * **intIOR** (float)
    * **intTransmission** (4-float vector)
    * **IOR** (float)
    * **IORimag** (4-float vector)
    * **IORreal** (4-float vector)
    * **irradiance** (4-float vector)
    * **isProgressive** (bool)
    * **iteration** (size)
    * **layerColor** (4-float vector)
    * **layerColorMap** (unsigned int)
    * **layerIOR** (float)
    * **lensRadius** (float)
    * **manipulatorFlags** (char)
    * **materialId** (unsigned int)
    * **maxAngleD** (float)
    * **maxCount** (size)
    * **maxDistance** (float)
    * **maxPatchDistance** (float)
    * **maxRayDepth** (size)
    * **maxRayIntensity** (float)
    * **maxTimeSteps** (size)
    * **minAngleD** (float)
    * **minPatchMatchCount** (size)
    * **minRayContribution** (float)
    * **mouseFlags** (char)
    * **NDFName** (string)
    * **normalMap** (unsigned int)
    * **numHistoBins** (size)
    * **numIndexBufferSamples** (size)
    * **numLightsForDirect** (size)
    * **numMipmapLevels** (size)
    * **numSamples** (size)
    * **numSampleSets** (size)
    * **numVertexBufferSamples** (size)
    * **opacity** (float)
    * **opacityMap** (unsigned int)
    * **parmName** (string)
    * **parmValue**
    * **patchHalfSize** (size)
    * **pixelFilterId** (unsigned int)
    * **positions** (data stream)
    * **primaryVisibility** (bool)
    * **priority** (int)
    * **radiance** (4-float vector)
    * **receivesShadows** (bool)
    * **reflectance** (4-float vector)
    * **reflectanceMap** (unsigned int)
    * **reflectivity** (float)
    * **regex** (string)
    * **rendererId** (unsigned int)
    * **rendering** (char)
    * **renderPassIds** (ID set)
    * **rotateD** (4-float vector)
    * **rotateOrder** (char)
    * **roughness** (float)
    * **roughnessMap** (unsigned int)
    * **roughnessX** (float)
    * **roughnessXMap** (unsigned int)
    * **roughnessY** (float)
    * **roughnessYMap** (unsigned int)
    * **rtCount** (size)
    * **rtCounts** (data)
    * **rtFaceVertices** (data)
    * **rtIndices** (data)
    * **rtNormals** (data stream)
    * **rtPathIds** (data)
    * **rtPositions** (data stream)
    * **rtScalps** (data)
    * **rtSubdivisionMethod** (string)
    * **rtTangentsX** (data stream)
    * **rtTangentsY** (data stream)
    * **rtTesselationRate** (float)
    * **rtTexcoords** (data stream)
    * **rtVertices** (data)
    * **rtWidths** (data stream)
    * **samplerFactoryId** (unsigned int)
    * **scale** (4-float vector)
    * **searchWindowHalfSize** (size)
    * **selected** (bool)
    * **selection** (ID set)
    * **selectorId** (unsigned int)
    * **selectorIds** (ID set)
    * **shadowBias** (float)
    * **shapeId** (unsigned int)
    * **showProgress** (bool)
    * **shutter** (float)
    * **shutterType** (string)
    * **sourceId** (unsigned int)
    * **specularExponent** (float)
    * **specularExponentMap** (unsigned int)
    * **specularReflectance** (4-float vector)
    * **specularReflectanceMap** (unsigned int)
    * **storeAsHalf** (bool)
    * **storeRawRgba** (bool)
    * **storeRgbaAsHalf** (bool)
    * **subdivisionLevel** (unsigned int)
    * **subsurfaceId** (unsigned int)
    * **thickness** (float)
    * **thicknessMap** (unsigned int)
    * **tileImages** (object)
    * **timeBounds** (2-float vector)
    * **toneMapperId** (unsigned int)
    * **transform** (3d affine transform)
    * **transformUV** (4-float vector)
    * **translate** (4-float vector)
    * **transmission** (4-float vector)
    * **transmissionMap** (unsigned int)
    * **transmissivity** (float)
    * **transmissivityMap** (unsigned int)
    * **transparentShadows** (bool)
    * **up** (4-float vector)
    * **upVector** (4-float vector)
    * **usedAsPoint** (bool)
    * **userAttribInitialization** (char)
    * **userColorName** (string)
    * **verbosityLevel** (char)
    * **vertices** (data)
    * **vignetting** (bool)
    * **visibleInReflections** (bool)
    * **visibleInRefractions** (bool)
    * **weight** (float)
    * **weightMap** (unsigned int)
    * **width** (size)
    * **widths** (data stream)
    * **worldTransform** (3d affine transform)
    * **xAspectRatio** (float)
    * **xMax** (int)
    * **xMin** (int)
    * **yFieldOfViewD** (float)
    * **yMax** (int)
    * **yMin** (int)
    * **zFar** (float)
    * **zNear** (float)


.. autoclass:: _pyflirt.ManipulatorFlags
    :members:
.. container:: toggle

    .. container:: header

        Show/hide manipulator flags

    * MANIP_OBJECT_MODE
    * MANIP_ROTATE
    * MANIP_SCALE
    * MANIP_TRANSLATE
    * NO_MANIP


.. autoclass:: _pyflirt.NodeClass
    :members:
.. container:: toggle

    .. container:: header

        Show/hide node classes

    * ANY
    * ARCHIVE
    * ASSIGN
    * CAMERA
    * CURVES
    * DENOISER
    * DRAWABLE
    * FRAMEBUFFER
    * GRAPH_ACTION
    * INTEGRATOR
    * LIGHT
    * MATERIAL
    * MESH
    * NONE
    * PIXEL_FILTER
    * POINTS
    * RENDER_ACTION
    * RENDERER
    * RENDERPASS
    * RTNODE
    * SAMPLER_FACTORY
    * SCENE
    * SCENE_OBJECT
    * SELECTOR
    * SHADER
    * SUBSURFACE
    * TEXTURE
    * TONEMAPPER
    * XFORM


.. autoclass:: _pyflirt.ParmEvent
    :members:
.. container:: toggle

    .. container:: header

        Show/hide parameter events

    * ADDED
    * CHANGED
    * REMOVED
    * UNKNOWN


.. autoclass:: _pyflirt.ParmType
    :members:
.. container:: toggle

    .. container:: header

        Show/hide parameter types

    * AFFINE_3
    * BOOL
    * BOUNDINGBOX
    * CHAR
    * DATA
    * DATASTREAM
    * FLOAT
    * FLOAT_2
    * FLOAT_4
    * IDSET
    * INT
    * INT_2
    * INT_3
    * INT_4
    * OBJECT
    * SIZE
    * STRING
    * UINT
    * UNKNOWN


.. autoclass:: _pyflirt.SceneEvent
    :members:
.. container:: toggle

    .. container:: header

        Show/hide scene events

    * LINK_ATTACHED
    * LINK_DETACHED
    * NODE_CREATED
    * NODE_DELETED
    * NODES_LINKED
    * NODES_UNLINKED
    * PARM_CONNECTED
    * PARM_DISCONNECTED
    * UNKNOWN


Exceptions
----------
.. autoclass:: _pyflirt.Error

Indices and Tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
