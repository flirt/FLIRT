# Controlling the Scene

Clients hold a reference to the scene they act upon. Their primary role is to edit its content while observing its changes and dispatching update events. The main takeaway here is that users are not supposed to directly interact with the scene and its nodes, but instead to resort to clients attached to it.

When used to control a visualization of the scene, clients are associated a **device** and are responsible for the construction, the update, and destruction of the nodes' **bindings**. Bindings are intermediary, visualization-specific objects needed by the client's device to achieve scene visualization.

The FLIRT framework currently proposes a renderer and a real-time viewport. It thus proposes a rendering and viewport clients. Clients' devices are created by invoking the creator function from a dynamically loaded external device module, which acts pretty much as a plug-in. Clients can point to different device modules. Check the [cfg.hpp](../smksXchg/include/smks/xchg/cfg.hpp) file in order to get to the framework's complete list of configuration options (most notably, the *"rendering_device"* and *"viewport_device"* options).
