// ========================================================================= //
// Copyright 2016-2018 SUPAMONKS_STUDIO                                      //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
//                                                                           //
// ========================================================================= //
#pragma once

#include <memory>
#include "smks/sys/configure_util.hpp"

namespace smks { namespace util { namespace string
{
    class FLIRT_UTIL_EXPORT Regex
    {
    public:
        typedef std::shared_ptr<Regex>  Ptr;
        typedef std::weak_ptr<Regex>    WPtr;

    private:
        struct PImpl;
    private:
        PImpl   *_pImpl;

    public:
        static inline
        Ptr
        create(const char* regex)
        {
            Ptr ptr(new Regex(regex));
            return ptr;
        }

    protected:
        explicit
        Regex(const char*);

    private:
        // non-copyable
        Regex(const Regex&);
        Regex& operator=(const Regex&);

    public:
        ~Regex();

        const char*
        c_str() const;

        bool
        match(const char*) const;
    };
}
}
}
