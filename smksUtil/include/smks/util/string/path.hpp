// ========================================================================= //
// Copyright 2016-2018 SUPAMONKS_STUDIO                                      //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
//                                                                           //
// ========================================================================= //
#pragma once

#include <iostream>
#include <string>

#include "smks/sys/configure_util.hpp"

namespace smks { namespace util { namespace path
{
    enum FilePermsAction    { NO_ACTION = 0, READ = 1 << 0, WRITE = 1 << 1, EXEC = 1 << 2 };
    enum FilePermsActor     { NO_ACTOR = 0, USER = 1 << 0, GROUP = 1 << 1, OTHERS = 1 << 2 };

    FLIRT_UTIL_EXPORT
    size_t
    abspath(const char* path, char* buffer, size_t bufferSize);

    FLIRT_UTIL_EXPORT
    size_t
    basename(const char* path, char* buffer, size_t bufferSize);

    FLIRT_UTIL_EXPORT
    size_t
    dirname(const char* path, char* buffer, size_t bufferSize);

    FLIRT_UTIL_EXPORT
    uint32_t
    permissions(const char*);

    FLIRT_UTIL_EXPORT
    bool
    allows(uint32_t, FilePermsAction, FilePermsActor);

    FLIRT_UTIL_EXPORT
    size_t
    getPathRelativelyToFile(const char* __file__, const char* rpath, char* buffer, size_t bufferSize);

    FLIRT_UTIL_EXPORT
    size_t
    getRepositoryRoot(const char*, char* buffer, size_t bufferSize);
}
}
}
