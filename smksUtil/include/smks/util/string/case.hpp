// ========================================================================= //
// Copyright 2016-2018 SUPAMONKS_STUDIO                                      //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
//                                                                           //
// ========================================================================= //
#pragma once

#include <ctype.h>
#include <string>
#include <algorithm>

namespace smks { namespace util { namespace string
{
    inline
    char
    to_lower(char c)
    {
        return char(tolower(int(c)));
    }

    inline
    char
    to_upper(char c)
    {
        return char(toupper(int(c)));
    }

    inline
    std::string
    strlwr(const std::string& s)
    {
        std::string dst(s);
        std::transform(dst.begin(), dst.end(), dst.begin(), to_lower);
        return dst;
    }

    inline
    std::string
    strupr(const std::string& s)
    {
        std::string dst(s);
        std::transform(dst.begin(), dst.end(), dst.begin(), to_upper);
        return dst;
    }
}
} }
