// ========================================================================= //
// Copyright 2016-2018 SUPAMONKS_STUDIO                                      //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
//                                                                           //
// ========================================================================= //
#pragma once

#include <iostream>
#include <string>

#include "smks/sys/configure_util.hpp"

namespace smks { namespace util { namespace string
{
    FLIRT_UTIL_EXPORT
    bool
    parseResolution2d(const char*, size_t& width, size_t& height, size_t = 800, size_t = 600);

    FLIRT_UTIL_EXPORT
    float
    parseRatio2d(const char*, float = 800.0f / 600.0f);

    FLIRT_UTIL_EXPORT
    size_t
    parseIntegers(const char*, int* buffer, size_t bufferSize);
}
}
}
