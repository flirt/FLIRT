// ========================================================================= //
// Copyright 2016-2018 SUPAMONKS_STUDIO                                      //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
//                                                                           //
// ========================================================================= //
#pragma once

namespace smks { namespace util { namespace color
{
    inline
    unsigned int
    uchar4ToUint(unsigned char r, unsigned char g, unsigned char b, unsigned char a)
    {
        return r + (g << 8) + (b << 16) + (a << 24);
    }

    inline
    unsigned int
    uchar4ToUint(const unsigned char rgba[4])
    {
        return uchar4ToUint(rgba[0], rgba[1], rgba[2], rgba[3]);
    }

    inline
    void
    uintToUchar4(unsigned int color, unsigned char& r, unsigned char& g, unsigned char& b, unsigned char& a)
    {
        r = static_cast<unsigned char>(  color          & 0xff);
        g = static_cast<unsigned char>( (color >> 8)    & 0xff);
        b = static_cast<unsigned char>( (color >> 16)   & 0xff);
        a = static_cast<unsigned char>( (color >> 24)   & 0xff);
    }

    inline
    void
    uintToUchar4(unsigned int color, unsigned char rgba[4])
    {
        uintToUchar4(color, rgba[0], rgba[1], rgba[2], rgba[3]);
    }
}
}
}
