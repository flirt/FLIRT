// ========================================================================= //
// Copyright 2016-2018 SUPAMONKS_STUDIO                                      //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
//                                                                           //
// ========================================================================= //
#pragma once

#include <string>
#include <iostream>
#include <sstream>

namespace smks { namespace util { namespace io
{
    inline
    std::string
    getLine()
    {
        std::string result;

        getline(std::cin, result);
        return result;
    }

    inline
    int
    getInteger(int minValue = -INT_MAX, int maxValue = INT_MAX)
    {
        int         ret;
        const int   upperBound = minValue < maxValue ? maxValue : minValue;
        const int   lowerBound = minValue < maxValue ? minValue : maxValue;

        while (true)
        {
            std::stringstream sstr;

            sstr << getLine();
            if (sstr >> ret)
            {
                char remaining;
                if (sstr >> remaining)
                    std::cout << "Unexpected character: " << remaining << std::endl;
                else
                    return ret < lowerBound
                        ? lowerBound
                        : (upperBound < ret ? upperBound : ret);
            }
            else if (lowerBound == -INT_MAX && upperBound == INT_MAX)
                std::cout << "Please enter an integer." << std::endl;
            else
                std::cout << "Please enter an integer in [ " << lowerBound << ", " << upperBound << " ]." << std::endl;
            std::cout << "Retry: \n";
        }
    }

    inline
    size_t
    getUInt64(size_t minValue = 0, size_t maxValue = SIZE_MAX)
    {
        int             ret;
        const size_t    upperBound = minValue < maxValue ? maxValue : minValue;
        const size_t    lowerBound = minValue < maxValue ? minValue : maxValue;

        while (true)
        {
            std::stringstream sstr;

            sstr << getLine();
            if (sstr >> ret)
            {
                char remaining;
                if (sstr >> remaining)
                    std::cout << "Unexpected character: " << remaining << std::endl;
                else
                    return ret < lowerBound
                        ? lowerBound
                        : (upperBound < ret ? upperBound : ret);
            }
            else if (lowerBound == 0 && upperBound == SIZE_MAX)
                std::cout << "Please enter an unsigned integer." << std::endl;
            else
                std::cout << "Please enter an unsigned integer in [ " << lowerBound << ", " << upperBound << " ]." << std::endl;
            std::cout << "Retry: \n";
        }
    }

    inline
    double
    getNumber(double minValue = -DBL_MAX, double maxValue = DBL_MAX)
    {
        double          ret;
        const double    upperBound = minValue < maxValue ? maxValue : minValue;
        const double    lowerBound = minValue < maxValue ? minValue : maxValue;

        while (true)
        {
            std::stringstream sstr;

            sstr << getLine();
            if (sstr >> ret)
            {
                char remaining;
                if (sstr >> remaining)
                    std::cout << "Unexpected character: " << remaining << std::endl;
                else
                    return ret < lowerBound
                        ? lowerBound
                        : (upperBound < ret ? upperBound : ret);
            }
            else if (abs(lowerBound - (-DBL_MAX)) < 1e-6 && abs(upperBound - DBL_MAX) < 1e-6 )
                std::cout << "Please enter a number." << std::endl;
            else
                std::cout << "Please enter a number in [ " << lowerBound << ", " << upperBound << " ]." << std::endl;
            std::cout << "Retry: \n";
        }
    }

    inline
    bool
    getBoolean(bool defaultValue = false)
    {
        while (true)
        {
            std::stringstream   sstr;
            std::string         ret;

            sstr << getLine();
            if (!(sstr >> ret) ||
                ret.empty())
                return defaultValue;
            if (ret.size() == 1)
            {
                const char c = ret.front();
                if (c == 'y' || c == 'Y')
                    return true;
                else if (c == 'n' || c == 'N')
                    return false;
            }
            std::cout << "Please enter either Y or N.\nRetry: \n";
        }
    }
}
} }
