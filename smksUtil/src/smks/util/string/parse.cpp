// ========================================================================= //
// Copyright 2016-2018 SUPAMONKS_STUDIO                                      //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
//                                                                           //
// ========================================================================= //

#include <string>
#include <sstream>
#include <boost/regex.hpp>
#include "smks/util/string/parse.hpp"

namespace smks { namespace util { namespace string
{
    struct NotDigitPredicate
    {
        inline
        bool
        operator()(const char c)
        {
            return c != ' ' && !std::isdigit(c);
        }
    };
}
}
}

using namespace smks;

float
util::string::parseRatio2d(const char*  str,
                           float        defaultValue)
{
    if (str == nullptr ||
        strlen(str) == 0)
        return defaultValue;

    // FIXME: should use regex
    char* ptr;
    float ratio = static_cast<float>(strtod(str, &ptr));

    if (*ptr == 0)
        return ratio;
    else
    {
        size_t width, height;

        if (parseResolution2d(str, width, height))
            return height > 0
                ? static_cast<float>(width) / static_cast<float>(height)
                : defaultValue;
        else
            return defaultValue;
    }
}

bool
util::string::parseResolution2d(const char* str,
                                size_t&     width,
                                size_t&     height,
                                size_t      defaultWidth,
                                size_t      defaultHeight)
{
    if (str == nullptr ||
        strlen(str) == 0)
        return false;

    const boost::regex REGEX("(\\d+)x(\\d+)");

    width   = defaultWidth;
    height  = defaultHeight;

    boost::cmatch match;
    if (boost::regex_match(str, match, REGEX))
    {
        char*   ptr1;
        char*   ptr2;

        width   = static_cast<size_t>(strtol(match[1].str().c_str(), &ptr1, 10));
        height  = static_cast<size_t>(strtol(match[2].str().c_str(), &ptr2, 10));

        assert(*ptr1 == 0 && *ptr2 == 0);

        return true;
    }
    else
        return false;
}

size_t
util::string::parseIntegers(const char* str_,
                            int*        buffer,
                            size_t      bufferSize)
{
    if (str_ == nullptr ||
        strlen(str_) == 0 ||
        buffer == nullptr ||
        bufferSize == 0)
        return 0;

    memset(buffer, 0, sizeof(int) * bufferSize);

    std::string             str(str_);
    NotDigitPredicate       notADigit;
    std::string::iterator   end         = std::remove_if(str.begin(), str.end(), notADigit);
    std::string             allNumbers  (str.begin(), end);
    std::stringstream       sstr        (allNumbers);

    size_t n = 0;
    for(int i = 0; sstr >> i; )
    {
        if (n < bufferSize)
            buffer[n++] = i;
    }
    return n;
}
