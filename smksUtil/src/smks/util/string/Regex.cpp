// ========================================================================= //
// Copyright 2016-2018 SUPAMONKS_STUDIO                                      //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
//                                                                           //
// ========================================================================= //
#include <iostream>
#include <boost/regex.hpp>
#include "smks/util/string/Regex.hpp"

namespace smks { namespace util { namespace string
{
    struct Regex::PImpl
    {
    public:
        boost::regex    _regex;
        std::string     _str;
    public:
        explicit inline
        PImpl(const char* regex):
            _regex  (),
            _str    ()
        {
            try
            {
                _regex = boost::regex(regex);
            }
            catch (const std::exception& e)
            {
                _regex = boost::regex("");
                std::cerr
                    << "Failed to initialize boost regex with '"
                    << (regex ? std::string(regex) : std::string())
                    << "': " << e.what()
                    << std::endl;
            }
            _str = _regex.str();
        }

        inline
        const char*
        c_str() const
        {
            return _str.c_str();
        }

        inline
        bool
        match(const char* str) const
        {
            return boost::regex_match(str, _regex);
        }
    };
}
} }

using namespace smks;

// explicit
util::string::Regex::Regex(const char* regex):
    _pImpl(new PImpl(regex))
{
}

util::string::Regex::~Regex()
{
    delete _pImpl;
}

const char*
util::string::Regex::c_str() const
{
    return _pImpl->c_str();
}

bool
util::string::Regex::match(const char* str) const
{
    return _pImpl->match(str);
}
