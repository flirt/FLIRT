// ========================================================================= //
// Copyright 2016-2018 SUPAMONKS_STUDIO                                      //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
//                                                                           //
// ========================================================================= //
#include <sstream>
#include <boost/filesystem.hpp>
#include <boost/regex.hpp>

#include <smks/sys/Exception.hpp>

#include "smks/util/string/path.hpp"

using namespace smks;
using namespace boost;

size_t
util::path::abspath(const char* path, char* buffer, size_t bufferSize)
{
    if (buffer == nullptr || bufferSize == 0)
        return 0;

    std::string path_ = std::string();
    if (path && strlen(path) > 0)
        path_ = std::string(path);

    filesystem::path fpath(path_);

    if (!filesystem::exists(fpath))
    {
        std::stringstream sstr;
        sstr << "Location '" << path_ << "' does not exist.";
        throw sys::Exception(sstr.str().c_str(), "Absolute Path Getter");
    }
    if (!filesystem::is_regular_file(fpath))
    {
        std::stringstream sstr;
        sstr << "Location '" << path_ << "' is not a valid file.";
        throw sys::Exception(sstr.str().c_str(), "Absolute Path Getter");
    }

    const std::string& ret = fpath.string();
    size_t num = ret.size() + 1;
    if (bufferSize < num)
        num = bufferSize;
    memcpy(buffer, ret.c_str(), sizeof(char) * num);

    return num;
}

size_t
util::path::basename(const char* path, char* buffer, size_t bufferSize)
{
    if (buffer == nullptr || bufferSize == 0)
        return 0;

    std::string path_ = std::string();
    if (path && strlen(path) > 0)
        path_ = std::string(path);

    filesystem::path fpath(path_);

    if (!filesystem::exists(fpath))
    {
        std::stringstream sstr;
        sstr << "Location '" << path_ << "' does not exist.";
        throw sys::Exception(sstr.str().c_str(), "Base Path Getter");
    }
    if (!filesystem::is_regular_file(fpath))
    {
        std::stringstream sstr;
        sstr << "Location '" << path_ << "' is not a valid file.";
        throw sys::Exception(sstr.str().c_str(), "Base Path Getter");
    }

    const std::string& ret = fpath.leaf().replace_extension().string();
    size_t num = ret.size() + 1;
    if (bufferSize < num)
        num = bufferSize;
    memcpy(buffer, ret.c_str(), sizeof(char) * num);

    return num;
}

size_t
util::path::dirname(const char* path, char* buffer, size_t bufferSize)
{
    if (buffer == nullptr || bufferSize == 0)
        return 0;

    std::string path_ = std::string();
    if (path && strlen(path) > 0)
        path_ = std::string(path);

    filesystem::path fpath(path_);

    std::string ret = fpath.parent_path().string();
    if (ret.empty())
        ret = ".";
    size_t num = ret.size() + 1;
    if (bufferSize < num)
        num = bufferSize;
    memcpy(buffer, ret.c_str(), sizeof(char) * num);

    return num;
}

uint32_t
util::path::permissions(const char* path)
{
    std::string path_ = std::string();
    if (path && strlen(path) > 0)
        path_ = std::string(path);

    filesystem::path                fpath(path_);
    const filesystem::file_status&  status = filesystem::status(fpath);

    return static_cast<uint32_t>(status.permissions());
}

bool
util::path::allows(uint32_t perms, FilePermsAction action, FilePermsActor actor)
{
    uint32_t flags = 0;
    if (action & FilePermsAction::READ)     flags |= (1 << 2);
    if (action & FilePermsAction::WRITE)    flags |= (1 << 1);
    if (action & FilePermsAction::EXEC)     flags |= (1 << 0);

    uint32_t req = 0;
    if (actor & FilePermsActor::USER)   req |= (flags << 6);
    if (actor & FilePermsActor::GROUP)  req |= (flags << 3);
    if (actor & FilePermsActor::OTHERS) req |= (flags << 0);

    return (perms & req) != 0;
}

size_t
util::path::getPathRelativelyToFile(const char* __file__,
                                    const char* rpath,
                                    char*       buffer,
                                    size_t      bufferSize)
{
    if (__file__ == nullptr || rpath == nullptr ||
        buffer == nullptr || bufferSize == 0)
        return 0;

    boost::filesystem::path basedir = boost::filesystem::path(__file__).branch_path();

    if (!boost::filesystem::exists(basedir) ||
        !boost::filesystem::is_directory(basedir))
        return 0;

    boost::filesystem::path p = basedir /  boost::filesystem::path(rpath);
    p = boost::filesystem::canonical(p);
    if (!boost::filesystem::exists(p))
        return 0;

    std::string ret = p.string();
    size_t num = ret.size() + 1;
    if (bufferSize < num)
        num = bufferSize;
    memcpy(buffer, ret.c_str(), sizeof(char) * num);
    return num;
}

size_t
util::path::getRepositoryRoot(const char*  path,
                              char*        buffer,
                              size_t       bufferSize)
{
    if (path == nullptr ||
        buffer == nullptr ||
        bufferSize == 0)
        return 0;

    // HACK:
    // current file is expected to be : ${REPO_ROOT}/smksUtil/src/smks/util/string/path.cpp
    boost::filesystem::path p = boost::filesystem::path(__FILE__).branch_path();
    for (int i = 0; i < 5; ++i)
        p /= "..";

    if (!boost::filesystem::exists(p))
        throw sys::Exception(
            "Failed to get repository root.",
            "Repository Path Getter");

    p = boost::filesystem::canonical(
        boost::filesystem::absolute(p));

    std::string ret = p.string();

    if (strlen(path))
    {
        p /= boost::filesystem::path(path);
        if (!boost::filesystem::exists(p))
        {
            ret = p.string();
            std::replace(ret.begin(), ret.end(), '\\', '/');
            std::cerr << "Failed to find location '" << ret << "'.";

            return 0;
        }
        p = boost::filesystem::canonical(
            boost::filesystem::absolute(p));
        ret = p.string();
    }

    std::replace(ret.begin(), ret.end(), '\\', '/');

    size_t num = ret.size() + 1;
    if (bufferSize < num)
        num = bufferSize;
    memcpy(buffer, ret.c_str(), sizeof(char) * num);

    return num;
}
