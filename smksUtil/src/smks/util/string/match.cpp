// ========================================================================= //
// Copyright 2016-2018 SUPAMONKS_STUDIO                                      //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
//                                                                           //
// ========================================================================= //
#include "smks/util/string/match.hpp"

#include <boost/algorithm/string/replace.hpp>
#include <boost/filesystem.hpp>
#include <boost/regex.hpp>

#include <smks/sys/Exception.hpp>

using namespace smks;
using namespace boost;

bool
util::string::matchWithRegex(const char* rgx_, const char* candidate)
{
    if (rgx_ == nullptr ||
        candidate == nullptr)
        return false;

    const regex rgx(rgx_, regex::normal);
    cmatch      what;

    return boost::regex_match(candidate, what, rgx);
}

void
util::string::matchWithRegex(const std::string&                                 rgx_,
                             boost::unordered_map<unsigned int, std::string>&   candidates)
{
    const regex rgx(rgx_, regex::normal);
    cmatch      what;

    for (boost::unordered_map<unsigned int, std::string>::iterator it = candidates.begin();
        it != candidates.end(); )
        if (!boost::regex_match(it->second.c_str(), what, rgx))
            it = candidates.erase(it);
        else
            ++it;
}

std::string
util::string::getRegexFromPattern(const std::string& pattern)
{
    std::string expr = pattern;

    replace_all(expr, "\\", "\\\\");
    replace_all(expr, "^", "\\^");
    replace_all(expr, ".", "\\.");
    replace_all(expr, "$", "\\$");
    replace_all(expr, "|", "\\|");
    replace_all(expr, "(", "\\(");
    replace_all(expr, ")", "\\)");
    replace_all(expr, "[", "\\[");
    replace_all(expr, "]", "\\]");
    replace_all(expr, "*", "\\*");
    replace_all(expr, "+", "\\+");
    replace_all(expr, "?", "\\?");
    replace_all(expr, "/", "\\/");

    // Convert chars '*?' back to their regex equivalents
    replace_all(expr, "\\?", ".");
    replace_all(expr, "\\*", ".*");

    return expr;
}
