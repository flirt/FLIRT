-- ========================================================================= --
-- Copyright 2016-2018 SUPAMONKS_STUDIO                                      --
-- Author: Pierre-Edouard Landes <pel@supamonks.com>                         --
--                                                                           --
-- This program is free software: you can redistribute it and/or modify      --
-- it under the terms of the GNU Lesser General Public License as            --
-- published by the Free Software Foundation, either version 3 of the        --
-- License, or (at your option) any later version.                           --
--                                                                           --
-- This program is distributed in the hope that it will be useful,           --
-- but WITHOUT ANY WARRANTY; without even the implied warranty of            --
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             --
-- GNU Lesser General Public License for more details.                       --
--                                                                           --
-- You should have received a copy of the GNU Lesser General Public License  --
-- along with this program.  If not, see <https://www.gnu.org/licenses/>.    --
--                                                                           --
-- ========================================================================= --
smks.dep.boost = {}


smks.dep.boost.path = function(...)

    ---------------------------------------------------------------------------
    return smks.dep.path(
        'boost_1_67_0',
        'boost_1_67_0',
        smks.compiler(),
        ...)
    ---------------------------------------------------------------------------

end


smks.dep.boost.includes = function()

    filter {}
    includedirs
    {
        smks.dep.boost.path('include'),
    }

end


smks.dep.boost.links_filesystem = function()

    filter {}
    defines
    {
        'BOOST_SYSTEM_STATIC_LINK',
        'BOOST_SYSTEM_NO_LIB',
        'BOOST_FILESYSTEM_STATIC_LINK',
        'BOOST_FILESYSTEM_NO_LIB',
    }
    libdirs
    {
        smks.dep.boost.path('lib'),
    }
    filter 'configurations:Debug'
        links
        {
            'libboost_system-mt-gd',
            'libboost_filesystem-mt-gd',
        }
    filter 'configurations:Release'
        links
        {
            'libboost_system-mt',
            'libboost_filesystem-mt',
        }
    filter{}

end


smks.dep.boost.links_regex = function()

    filter {}
    defines
    {
        'BOOST_REGEX_STATIC_LINK',
        'BOOST_REGEX_NO_LIB',
    }
    libdirs
    {
        smks.dep.boost.path('lib'),
    }
    filter 'configurations:Debug'
        links
        {
            'libboost_regex-mt-gd',
        }
    filter 'configurations:Release'
        links
        {
            'libboost_regex-mt',
        }
    filter{}

end
