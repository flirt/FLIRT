-- ========================================================================= --
-- Copyright 2016-2018 SUPAMONKS_STUDIO                                      --
-- Author: Pierre-Edouard Landes <pel@supamonks.com>                         --
--                                                                           --
-- This program is free software: you can redistribute it and/or modify      --
-- it under the terms of the GNU Lesser General Public License as            --
-- published by the Free Software Foundation, either version 3 of the        --
-- License, or (at your option) any later version.                           --
--                                                                           --
-- This program is distributed in the hope that it will be useful,           --
-- but WITHOUT ANY WARRANTY; without even the implied warranty of            --
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             --
-- GNU Lesser General Public License for more details.                       --
--                                                                           --
-- You should have received a copy of the GNU Lesser General Public License  --
-- along with this program.  If not, see <https://www.gnu.org/licenses/>.    --
--                                                                           --
-- ========================================================================= --


smks.module.kind = {}

local STATIC_LIB = 'StaticLib'
local SHARED_LIB = 'SharedLib'
local HEADER_ONLY = 'StaticLib' -- 'None' (only supported for visual studio)

smks.module.kind.log = SHARED_LIB
smks.module.kind.strid = SHARED_LIB
smks.module.kind.sys = SHARED_LIB
smks.module.kind.util = SHARED_LIB
smks.module.kind.math = SHARED_LIB
smks.module.kind.img = SHARED_LIB
smks.module.kind.asset = SHARED_LIB
smks.module.kind.xchg = SHARED_LIB
smks.module.kind.parm = HEADER_ONLY
smks.module.kind.scn = SHARED_LIB
smks.module.kind.clientbase = SHARED_LIB
smks.module.kind.rndr_device_api = HEADER_ONLY
smks.module.kind.rndr_client = SHARED_LIB
smks.module.kind.rndr_device = SHARED_LIB
smks.module.kind.view_gl = SHARED_LIB
smks.module.kind.util_osg = SHARED_LIB
smks.module.kind.view_client = SHARED_LIB
smks.module.kind.view_device_api = HEADER_ONLY
smks.module.kind.view_device = SHARED_LIB
smks.module.kind.view_device_ctrl = SHARED_LIB
smks.module.kind.view_debug = SHARED_LIB
