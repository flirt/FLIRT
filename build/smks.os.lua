-- ========================================================================= --
-- Copyright 2016-2018 SUPAMONKS_STUDIO                                      --
-- Author: Pierre-Edouard Landes <pel@supamonks.com>                         --
--                                                                           --
-- This program is free software: you can redistribute it and/or modify      --
-- it under the terms of the GNU Lesser General Public License as            --
-- published by the Free Software Foundation, either version 3 of the        --
-- License, or (at your option) any later version.                           --
--                                                                           --
-- This program is distributed in the hope that it will be useful,           --
-- but WITHOUT ANY WARRANTY; without even the implied warranty of            --
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             --
-- GNU Lesser General Public License for more details.                       --
--                                                                           --
-- You should have received a copy of the GNU Lesser General Public License  --
-- along with this program.  If not, see <https://www.gnu.org/licenses/>.    --
--                                                                           --
-- ========================================================================= --
smks.os = {}


smks.os.translate = function(p)

    local sep = '/'
    if os.is('windows') then
        sep = '\\'
    end
    return string.format('"%s"', path.translate(p, sep))

end


smks.os.get_targetdir = function(subdir)

    if not os.is('windows') then
        error(string.format('get_targetdir not implemented for %q.', os.get()))
    end

    ret = '$(TARGETDIR)'

    if subdir and string.len(subdir) > 0 then
        ret = path.join(ret, subdir)
    end
    return smks.os.translate(ret)

end


smks.os.copy_to_dir = function(p, dir)

    if not os.is('windows') then
        error(string.format('copy_to_dir not implemented for %q.', os.get()))
    end

    return string.format(
        'xcopy /y %s %s',
        smks.os.translate(p),
        smks.os.translate(dir))

end


smks.os.copy_to_targetdir = function(p, subdir)

    return smks.os.copy_to_dir(
        p,
        smks.os.get_targetdir(subdir))

end


smks.os.make_dir = function(dir)

    if not os.is('windows') then
        error(string.format('make_dir not implemented for %q.', os.get()))
    end

    return string.format(
        'if not exist %s mkdir %s',
        smks.os.translate(dir),
        smks.os.translate(dir))

end


smks.os.make_sub_targetdir = function(subdir)

    return smks.os.make_dir(
        smks.os.get_targetdir(subdir))

end


smks.os.as_sharedlib = function(p)
    -- appends to specified path the extension dedicated to shared obj

    local ext = nil
    if os.is('windows') then
        ext = 'dll'
    end
    if not ext then
        error(
            string.format(
                'set_sharedlib_extension not implemented for %q', os.get()))
    end

    return path.replaceextension(p, ext)

end


smks.os.export_symbol = function(symbol_name)

    -- visual studio specifics
    if _ACTION:match('vs%d+') then
        filter {}
        linkoptions
        {
            string.format('/export:%s', symbol_name),
        }
    end

end


smks.os.specific_settings = function()

    filter {}

    if os.is('windows') then
        defines
        {
            'WIN32',
            '_WINDOWS',
            '_USE_MATH_DEFINES',
            '_CRT_SECURE_NO_WARNINGS',
            'NOMINMAX'
        }
    end

    -- visual studio specifics
    if _ACTION:match('vs%d+') then
        buildoptions
        {
            '/MP4',
        }
    end

end
