-- ========================================================================= --
-- Copyright 2016-2018 SUPAMONKS_STUDIO                                      --
-- Author: Pierre-Edouard Landes <pel@supamonks.com>                         --
--                                                                           --
-- This program is free software: you can redistribute it and/or modify      --
-- it under the terms of the GNU Lesser General Public License as            --
-- published by the Free Software Foundation, either version 3 of the        --
-- License, or (at your option) any later version.                           --
--                                                                           --
-- This program is distributed in the hope that it will be useful,           --
-- but WITHOUT ANY WARRANTY; without even the implied warranty of            --
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             --
-- GNU Lesser General Public License for more details.                       --
--                                                                           --
-- You should have received a copy of the GNU Lesser General Public License  --
-- along with this program.  If not, see <https://www.gnu.org/licenses/>.    --
--                                                                           --
-- ========================================================================= --


smks.module = {}


smks.module.path = function(module_name, ...)

    return smks.repo_path(
        module_name:gsub('^%u', string.lower), -- first letter in lowercase
        ...)

end


smks.module.outdir = function(module_kind)

    local dir = nil
    if module_kind == 'StaticLib' then
        dir = 'lib'
    elseif module_kind == 'SharedLib' or module_kind == 'ConsoleApp' then
        dir = 'bin'
    end
    if not dir then
        return nil  -- module has unsupported kind
    end

    return path.translate(
        path.join(smks.compiler(), dir, '%{cfg.buildcfg}'),
        '/')

end


smks.module.set_kind = function(module_kind)

    local outdir = smks.module.outdir(module_kind)
    if not outdir then
        return
    end

    filter {}
    kind(module_kind)
    targetdir(outdir)

end


smks.module.includes = function(module_name)

    filter {}
    includedirs
    {
        smks.module.path(module_name, 'include'),
    }

end


smks.module.links = function(module_name, module_kind)

    smks.module.includes(module_name)

    local outdir = smks.module.outdir(module_kind)
    if outdir then
        filter {}
        libdirs
        {
            smks.module.path(module_name, outdir),
        }
        links
        {
            module_name,
        }
    end

end


smks.module.copy_to_targetdir = function(module_name)

    local bin_path = smks.module.path(
        module_name,
        smks.module.outdir('SharedLib'), smks.os.as_sharedlib(module_name))
    local cmd = smks.os.copy_to_targetdir(bin_path)

    filter {}
    postbuildcommands
    {
        smks.os.copy_to_targetdir(bin_path)
    }

end

