-- ========================================================================= --
-- Copyright 2016-2018 SUPAMONKS_STUDIO                                      --
-- Author: Pierre-Edouard Landes <pel@supamonks.com>                         --
--                                                                           --
-- This program is free software: you can redistribute it and/or modify      --
-- it under the terms of the GNU Lesser General Public License as            --
-- published by the Free Software Foundation, either version 3 of the        --
-- License, or (at your option) any later version.                           --
--                                                                           --
-- This program is distributed in the hope that it will be useful,           --
-- but WITHOUT ANY WARRANTY; without even the implied warranty of            --
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             --
-- GNU Lesser General Public License for more details.                       --
--                                                                           --
-- You should have received a copy of the GNU Lesser General Public License  --
-- along with this program.  If not, see <https://www.gnu.org/licenses/>.    --
--                                                                           --
-- ========================================================================= --
smks.dep.python = {}


smks.dep.python.path = function(...)

    ---------------------------------------------------------------------------
    return smks.dep.path(
        string.format('cpython-2.7-%s', smks.compiler()),
        ...)
    ---------------------------------------------------------------------------
end


smks.dep.python.links = function()

    filter {}

    includedirs
    {
        smks.dep.python.path('Include'),
        smks.dep.python.path('PC'),
        smks.dep.python.path('libs'),
    }
    libdirs
    {
        smks.dep.python.path('libs'),
    }
    filter 'configurations:Release'
        links
        {
            'python27',
        }
    filter 'configurations:Debug'
        links
        {
            'python27_d',
        }
    filter {}

end
