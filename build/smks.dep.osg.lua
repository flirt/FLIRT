-- ========================================================================= --
-- Copyright 2016-2018 SUPAMONKS_STUDIO                                      --
-- Author: Pierre-Edouard Landes <pel@supamonks.com>                         --
--                                                                           --
-- This program is free software: you can redistribute it and/or modify      --
-- it under the terms of the GNU Lesser General Public License as            --
-- published by the Free Software Foundation, either version 3 of the        --
-- License, or (at your option) any later version.                           --
--                                                                           --
-- This program is distributed in the hope that it will be useful,           --
-- but WITHOUT ANY WARRANTY; without even the implied warranty of            --
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             --
-- GNU Lesser General Public License for more details.                       --
--                                                                           --
-- You should have received a copy of the GNU Lesser General Public License  --
-- along with this program.  If not, see <https://www.gnu.org/licenses/>.    --
--                                                                           --
-- ========================================================================= --
smks.dep.osg = {}


smks.dep.osg.path = function(...)

    ---------------------------------------------------------------------------
    return smks.dep.path(
        string.format('osg-%s', smks.compiler()),
        '%{cfg.buildcfg}',
        ...)
    ---------------------------------------------------------------------------

end


smks.dep.osg.includes = function()

    filter {}
    includedirs
    {
        smks.dep.osg.path('include'),
    }

end


smks.dep.osg.links = function()

    smks.dep.osg.includes()

    filter {}
    libdirs
    {
        smks.dep.osg.path('lib'),
    }
    filter 'configurations:Debug'
        links
        {
            'OpenThreadsd',
            --
            'osgd',
            'osgGAd',
            'osgAnimationd',
            'osgViewerd',
            'osgUtild',
            'osgTextd',
        }
    filter 'configurations:Release'
        links
        {
            'OpenThreads',
            --
            'osg',
            'osgGA',
            'osgAnimation',
            'osgViewer',
            'osgUtil',
            'osgText',
        }
    filter {}

end


smks.dep.osg.copy_to_targetdir = function()

    local OT_VER = '21'
    local OSG_VER = '160'
    local OSG_PLUGIN_VER = '3.7.0'
    local OSG_PLUGIN_SUBDIR = string.format('osgPlugins-%s', OSG_PLUGIN_VER)

    local get_openthread_bin = function(module_name)

        return smks.dep.osg.path(
            'bin', smks.os.as_sharedlib(
                string.format('ot%s-%s', OT_VER, module_name)))

    end

    local get_osg_bin = function(module_name)

        return smks.dep.osg.path(
            'bin', smks.os.as_sharedlib(
                string.format('osg%s-%s', OSG_VER, module_name)))

    end

    local get_osg_plugin = function(module_name)

        return smks.dep.osg.path(
            'bin', OSG_PLUGIN_SUBDIR, smks.os.as_sharedlib(module_name))

    end

    filter {}
    postbuildcommands
    {
        smks.os.make_sub_targetdir(
            OSG_PLUGIN_SUBDIR),
    }
    filter 'configurations:Release'
        postbuildcommands
        {
            smks.os.copy_to_targetdir(
                get_openthread_bin('OpenThreads')),
            smks.os.copy_to_targetdir(
                get_osg_bin('osg')),
            smks.os.copy_to_targetdir(
                get_osg_bin('osgGA')),
            smks.os.copy_to_targetdir(
                get_osg_bin('osgDB')),
            smks.os.copy_to_targetdir(
                get_osg_bin('osgAnimation')),
            smks.os.copy_to_targetdir(
                get_osg_bin('osgViewer')),
            smks.os.copy_to_targetdir(
                get_osg_bin('osgUtil')),
            smks.os.copy_to_targetdir(
                get_osg_bin('osgText')),
            smks.os.copy_to_targetdir(
                get_osg_plugin('osgdb_freetype'), OSG_PLUGIN_SUBDIR),
        }
    filter 'configurations:Debug'
        postbuildcommands
        {
            smks.os.copy_to_targetdir(
                get_openthread_bin('OpenThreadsd')),
            smks.os.copy_to_targetdir(
                get_osg_bin('osgd')),
            smks.os.copy_to_targetdir(
                get_osg_bin('osgGAd')),
            smks.os.copy_to_targetdir(
                get_osg_bin('osgDBd')),
            smks.os.copy_to_targetdir(
                get_osg_bin('osgAnimationd')),
            smks.os.copy_to_targetdir(
                get_osg_bin('osgViewerd')),
            smks.os.copy_to_targetdir(
                get_osg_bin('osgUtild')),
            smks.os.copy_to_targetdir(
                get_osg_bin('osgTextd')),
            smks.os.copy_to_targetdir(
                get_osg_plugin('osgdb_freetyped'), OSG_PLUGIN_SUBDIR),
        }
    filter {}

end
