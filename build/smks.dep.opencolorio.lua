-- ========================================================================= --
-- Copyright 2016-2018 SUPAMONKS_STUDIO                                      --
-- Author: Pierre-Edouard Landes <pel@supamonks.com>                         --
--                                                                           --
-- This program is free software: you can redistribute it and/or modify      --
-- it under the terms of the GNU Lesser General Public License as            --
-- published by the Free Software Foundation, either version 3 of the        --
-- License, or (at your option) any later version.                           --
--                                                                           --
-- This program is distributed in the hope that it will be useful,           --
-- but WITHOUT ANY WARRANTY; without even the implied warranty of            --
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             --
-- GNU Lesser General Public License for more details.                       --
--                                                                           --
-- You should have received a copy of the GNU Lesser General Public License  --
-- along with this program.  If not, see <https://www.gnu.org/licenses/>.    --
--                                                                           --
-- ========================================================================= --
smks.dep.opencolorio = {}


smks.dep.opencolorio.path = function(...)

    ---------------------------------------------------------------------------
    return smks.dep.path(
        'opencolorio',
        smks.compiler(),
        ...)
    ---------------------------------------------------------------------------

end


smks.dep.opencolorio.links = function()

    filter {}
    includedirs
    {
        smks.dep.opencolorio.path('include'),
    }
    libdirs
    {
        smks.dep.opencolorio.path('bin', '%{cfg.buildcfg}'),
    }
    links
    {
        'OpenColorIO',
    }

end


smks.dep.opencolorio.copy_to_targetdir = function()

    filter {}
    postbuildcommands
    {
        smks.os.copy_to_targetdir(
            smks.dep.opencolorio.path(
                'bin', '%{cfg.buildcfg}', smks.os.as_sharedlib('OpenColorIO'))),
    }

    -- create subdirectory in target dir and copy additional data

    local src_dir = smks.repo_path('asset', 'ocio', 'filmic-blender-master')
    local dst_dir = path.translate(
        path.join('ocio', 'filmic-blender-master'),
        '/')

    postbuildcommands
    {
        smks.os.make_sub_targetdir('ocio'),
        smks.os.make_sub_targetdir(dst_dir),
        smks.os.copy_to_targetdir(src_dir, dst_dir),
    }

end
