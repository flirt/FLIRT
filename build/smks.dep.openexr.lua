-- ========================================================================= --
-- Copyright 2016-2018 SUPAMONKS_STUDIO                                      --
-- Author: Pierre-Edouard Landes <pel@supamonks.com>                         --
--                                                                           --
-- This program is free software: you can redistribute it and/or modify      --
-- it under the terms of the GNU Lesser General Public License as            --
-- published by the Free Software Foundation, either version 3 of the        --
-- License, or (at your option) any later version.                           --
--                                                                           --
-- This program is distributed in the hope that it will be useful,           --
-- but WITHOUT ANY WARRANTY; without even the implied warranty of            --
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             --
-- GNU Lesser General Public License for more details.                       --
--                                                                           --
-- You should have received a copy of the GNU Lesser General Public License  --
-- along with this program.  If not, see <https://www.gnu.org/licenses/>.    --
--                                                                           --
-- ========================================================================= --
smks.dep.openexr = {}


smks.dep.openexr.path = function(...)

    ---------------------------------------------------------------------------
    return smks.dep.path(
        'openexr',
        'OpenEXR',
        smks.compiler(),
        '%{cfg.buildcfg}',
        ...)
    ---------------------------------------------------------------------------
end


smks.dep.openexr.links = function()

    filter {}

    smks.dep.ilmbase.links()

    includedirs
    {
        smks.dep.openexr.path('include', 'OpenEXR'),
    }
    libdirs
    {
        smks.dep.openexr.path('lib'),
    }
    links
    {
        'IlmImf',
        'IlmImfUtil',
    }

end
