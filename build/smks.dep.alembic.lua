-- ========================================================================= --
-- Copyright 2016-2018 SUPAMONKS_STUDIO                                      --
-- Author: Pierre-Edouard Landes <pel@supamonks.com>                         --
--                                                                           --
-- This program is free software: you can redistribute it and/or modify      --
-- it under the terms of the GNU Lesser General Public License as            --
-- published by the Free Software Foundation, either version 3 of the        --
-- License, or (at your option) any later version.                           --
--                                                                           --
-- This program is distributed in the hope that it will be useful,           --
-- but WITHOUT ANY WARRANTY; without even the implied warranty of            --
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             --
-- GNU Lesser General Public License for more details.                       --
--                                                                           --
-- You should have received a copy of the GNU Lesser General Public License  --
-- along with this program.  If not, see <https://www.gnu.org/licenses/>.    --
--                                                                           --
-- ========================================================================= --
smks.dep.alembic = {}


smks.dep.alembic.path = function(...)

    ---------------------------------------------------------------------------
    return smks.dep.path(
        'alembic',
        ...)
    ---------------------------------------------------------------------------
end


smks.dep.alembic.links = function()

    filter {}

    smks.dep.hdf5.links()

    defines
    {
        'ALEMBIC_VERSION_NS=v7',
    }
    includedirs
    {
        smks.dep.alembic.path('lib'),
    }
    libdirs
    {
        smks.dep.alembic.path(smks.compiler(), 'lib', '%{cfg.buildcfg}'),
    }
    links
    {
        'AlembicAbc',
        'AlembicAbcCollection',
        'AlembicAbcCoreAbstract',
        'AlembicAbcCoreFactory',
        'AlembicAbcCoreHDF5',
        'AlembicAbcCoreOgawa',
        'AlembicAbcGeom',
        'AlembicOgawa',
        'AlembicUtil',
    }

end
