-- ========================================================================= --
-- Copyright 2016-2018 SUPAMONKS_STUDIO                                      --
-- Author: Pierre-Edouard Landes <pel@supamonks.com>                         --
--                                                                           --
-- This program is free software: you can redistribute it and/or modify      --
-- it under the terms of the GNU Lesser General Public License as            --
-- published by the Free Software Foundation, either version 3 of the        --
-- License, or (at your option) any later version.                           --
--                                                                           --
-- This program is distributed in the hope that it will be useful,           --
-- but WITHOUT ANY WARRANTY; without even the implied warranty of            --
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             --
-- GNU Lesser General Public License for more details.                       --
--                                                                           --
-- You should have received a copy of the GNU Lesser General Public License  --
-- along with this program.  If not, see <https://www.gnu.org/licenses/>.    --
--                                                                           --
-- ========================================================================= --
smks.dep.zlib = {}


smks.dep.zlib.path = function(...)

    ---------------------------------------------------------------------------
    return smks.dep.path(
        'zlib-1.2.8',
        ...)
    ---------------------------------------------------------------------------

end


smks.dep.zlib.outdir_path = function(...)

    ---------------------------------------------------------------------------
    return smks.dep.zlib.path(smks.compiler(), 'bin', '%{cfg.buildcfg}', ...)
    ---------------------------------------------------------------------------

end


smks.dep.zlib.links = function()

    filter {}
    libdirs
    {
        smks.dep.zlib.outdir_path(),
    }
    links
    {
        'zlib',
    }

end


smks.dep.zlib.copy_to_targetdir = function()

    filter {}
    postbuildcommands
    {
        smks.os.copy_to_targetdir(
            smks.dep.zlib.outdir_path(smks.os.as_sharedlib('zlib')))
    }

end
