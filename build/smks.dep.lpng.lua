-- ========================================================================= --
-- Copyright 2016-2018 SUPAMONKS_STUDIO                                      --
-- Author: Pierre-Edouard Landes <pel@supamonks.com>                         --
--                                                                           --
-- This program is free software: you can redistribute it and/or modify      --
-- it under the terms of the GNU Lesser General Public License as            --
-- published by the Free Software Foundation, either version 3 of the        --
-- License, or (at your option) any later version.                           --
--                                                                           --
-- This program is distributed in the hope that it will be useful,           --
-- but WITHOUT ANY WARRANTY; without even the implied warranty of            --
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             --
-- GNU Lesser General Public License for more details.                       --
--                                                                           --
-- You should have received a copy of the GNU Lesser General Public License  --
-- along with this program.  If not, see <https://www.gnu.org/licenses/>.    --
--                                                                           --
-- ========================================================================= --
smks.dep.lpng = {}


smks.dep.lpng.path = function(...)

    ---------------------------------------------------------------------------
    return smks.dep.path(
        'lpng1614',
        smks.compiler(),
        '%{cfg.buildcfg}',
        ...)
    ---------------------------------------------------------------------------

end

smks.dep.lpng.links = function()

    filter {}
    includedirs
    {
        smks.dep.lpng.path('include'),
    }
    libdirs
    {
        smks.dep.lpng.path('lib'),
    }
    filter 'configurations:Debug'
        links
        {
            'libpng16d',
        }
    filter 'configurations:Release'
        links
        {
            'libpng16',
        }
    filter {}

end


smks.dep.lpng.copy_to_targetdir = function()

    filter 'configurations:Debug'
        postbuildcommands
        {
            smks.os.copy_to_targetdir(
                smks.dep.lpng.path('bin', smks.os.as_sharedlib('libpng16d')))
        }
    filter 'configurations:Release'
        postbuildcommands
        {
            smks.os.copy_to_targetdir(
                smks.dep.lpng.path('bin', smks.os.as_sharedlib('libpng16')))
        }
    filter {}

end
