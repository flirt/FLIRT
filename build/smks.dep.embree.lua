-- ========================================================================= --
-- Copyright 2016-2018 SUPAMONKS_STUDIO                                      --
-- Author: Pierre-Edouard Landes <pel@supamonks.com>                         --
--                                                                           --
-- This program is free software: you can redistribute it and/or modify      --
-- it under the terms of the GNU Lesser General Public License as            --
-- published by the Free Software Foundation, either version 3 of the        --
-- License, or (at your option) any later version.                           --
--                                                                           --
-- This program is distributed in the hope that it will be useful,           --
-- but WITHOUT ANY WARRANTY; without even the implied warranty of            --
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             --
-- GNU Lesser General Public License for more details.                       --
--                                                                           --
-- You should have received a copy of the GNU Lesser General Public License  --
-- along with this program.  If not, see <https://www.gnu.org/licenses/>.    --
--                                                                           --
-- ========================================================================= --


smks.dep.embree = {}


smks.dep.embree.path = function(...)

    ---------------------------------------------------------------------------
    if _ACTION == 'vs2012' then
        return smks.dep.path(
            'embree_msvc11',
            'msvc11',
            '%{cfg.buildcfg}',
            ...)
    else
        return smks.dep.path(
            'embree',
            smks.compiler(),
            '%{cfg.buildcfg}',
            ...)
    end
    ---------------------------------------------------------------------------

end


smks.dep.embree.links = function()

    filter {}
    includedirs
    {
        smks.dep.embree.path('include'),
    }
    libdirs
    {
        smks.dep.embree.path('lib'),
    }
    links
    {
        'embree',
    }

end


smks.dep.embree.copy_to_targetdir = function()

    filter {}
    postbuildcommands
    {
        smks.os.copy_to_targetdir(
            smks.dep.embree.path('bin', smks.os.as_sharedlib('embree'))),
    }

end
