-- ========================================================================= --
-- Copyright 2016-2018 SUPAMONKS_STUDIO                                      --
-- Author: Pierre-Edouard Landes <pel@supamonks.com>                         --
--                                                                           --
-- This program is free software: you can redistribute it and/or modify      --
-- it under the terms of the GNU Lesser General Public License as            --
-- published by the Free Software Foundation, either version 3 of the        --
-- License, or (at your option) any later version.                           --
--                                                                           --
-- This program is distributed in the hope that it will be useful,           --
-- but WITHOUT ANY WARRANTY; without even the implied warranty of            --
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             --
-- GNU Lesser General Public License for more details.                       --
--                                                                           --
-- You should have received a copy of the GNU Lesser General Public License  --
-- along with this program.  If not, see <https://www.gnu.org/licenses/>.    --
--                                                                           --
-- ========================================================================= --


smks.dep.googletest = {}


smks.dep.googletest.path = function(...)

    ---------------------------------------------------------------------------
    return smks.dep.path(
        'googletest',
        smks.compiler(),
        '%{cfg.buildcfg}',
        ...)
    ---------------------------------------------------------------------------

end


smks.dep.googletest.use = function()

    filter {}
    defines
    {
        '_VARIADIC_MAX=10',
    }
    includedirs
    {
        smks.dep.googletest.path('include'),
    }
    libdirs
    {
        smks.dep.googletest.path('lib'),
    }
    links
    {
        'gtest',
    }
    postbuildcommands
    {
        smks.os.copy_to_targetdir(
            smks.dep.googletest.path('lib', smks.os.as_sharedlib('gtest'))),
    }

end
