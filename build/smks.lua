-- ========================================================================= --
-- Copyright 2016-2018 SUPAMONKS_STUDIO                                      --
-- Author: Pierre-Edouard Landes <pel@supamonks.com>                         --
--                                                                           --
-- This program is free software: you can redistribute it and/or modify      --
-- it under the terms of the GNU Lesser General Public License as            --
-- published by the Free Software Foundation, either version 3 of the        --
-- License, or (at your option) any later version.                           --
--                                                                           --
-- This program is distributed in the hope that it will be useful,           --
-- but WITHOUT ANY WARRANTY; without even the implied warranty of            --
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             --
-- GNU Lesser General Public License for more details.                       --
--                                                                           --
-- You should have received a copy of the GNU Lesser General Public License  --
-- along with this program.  If not, see <https://www.gnu.org/licenses/>.    --
--                                                                           --
-- ========================================================================= --


smks = {}

smks.compiler = function()

    if _ACTION == 'vs2012' then
        return 'msvc11'
    elseif _ACTION == 'vs2015' then
        return 'msvc14'
    else
        error(
            string.format(
                'no compiler name associated with action %q.', _ACTION))
    end

end


smks.repo_path = function(...)

    -- local function used to get directory storing smks.lua script file. -----
    local get_dirname = function()
        local dirname = debug.getinfo(2, "S").source:sub(2)
        return dirname:match("(.*/)")
    end
    ---------------------------------------------------------------------------
    return path.translate(
        path.join(get_dirname(), '..', ...),
        '/')

end


smks.envvar_based_path = function(envvar, ...)

    local root = os.getenv(envvar)
    if not root then
        error(
            string.format(
                'please define the following environment variable %q.',
                envvar))
    end
    if not os.isdir(root) then
        error(
            string.format(
                'value of environment variable %q does not point ' +
                'towards valid directory (%q).',
                envvar,
                root))
    end

    return path.translate(
        path.join(root, ...),
        '/')

end

dofile 'smks.os.lua'
dofile 'smks.dep.lua'
dofile 'smks.module.lua'
dofile 'smks.module.name.lua'
dofile 'smks.module.kind.lua'
