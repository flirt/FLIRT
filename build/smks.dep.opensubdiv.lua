-- ========================================================================= --
-- Copyright 2016-2018 SUPAMONKS_STUDIO                                      --
-- Author: Pierre-Edouard Landes <pel@supamonks.com>                         --
--                                                                           --
-- This program is free software: you can redistribute it and/or modify      --
-- it under the terms of the GNU Lesser General Public License as            --
-- published by the Free Software Foundation, either version 3 of the        --
-- License, or (at your option) any later version.                           --
--                                                                           --
-- This program is distributed in the hope that it will be useful,           --
-- but WITHOUT ANY WARRANTY; without even the implied warranty of            --
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             --
-- GNU Lesser General Public License for more details.                       --
--                                                                           --
-- You should have received a copy of the GNU Lesser General Public License  --
-- along with this program.  If not, see <https://www.gnu.org/licenses/>.    --
--                                                                           --
-- ========================================================================= --
smks.dep.opensubdiv = {}


smks.dep.opensubdiv.path = function(...)

    ---------------------------------------------------------------------------
    return smks.dep.path(
        'opensubdiv',
        smks.compiler(),
        '%{cfg.buildcfg}',
        ...)
    ---------------------------------------------------------------------------
end


smks.dep.opensubdiv.links = function()

    filter {}

    smks.dep.tbb.links()

    defines
    {
        'DEFINE_OSD_MACROS',
        -- 'and=&&',
        -- 'and_eq=&=',
        -- 'bitand=&',
        -- 'bitor=|',
        -- 'compl=~',
        -- 'not=!',
        -- 'not_eq=!=',
        -- 'or=||',
        -- 'or_eq=|=',
        -- 'xor=^/**/',
        -- 'xor_eq=^=',
        'OPENSUBDIV_HAS_TBB',
        '__TBB_NO_IMPLICIT_LINKAGE',
    }
    includedirs
    {
        smks.dep.opensubdiv.path('include'),
    }
    libdirs
    {
        smks.dep.opensubdiv.path('lib'),
    }
    links
    {
        'osdCPU',
        -- 'osdGPU',
    }

end
