-- ========================================================================= --
-- Copyright 2016-2018 SUPAMONKS_STUDIO                                      --
-- Author: Pierre-Edouard Landes <pel@supamonks.com>                         --
--                                                                           --
-- This program is free software: you can redistribute it and/or modify      --
-- it under the terms of the GNU Lesser General Public License as            --
-- published by the Free Software Foundation, either version 3 of the        --
-- License, or (at your option) any later version.                           --
--                                                                           --
-- This program is distributed in the hope that it will be useful,           --
-- but WITHOUT ANY WARRANTY; without even the implied warranty of            --
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             --
-- GNU Lesser General Public License for more details.                       --
--                                                                           --
-- You should have received a copy of the GNU Lesser General Public License  --
-- along with this program.  If not, see <https://www.gnu.org/licenses/>.    --
--                                                                           --
-- ========================================================================= --
smks.dep = {}


smks.dep.path = function(...)

    ---------------------------------------------------------------------------
    return smks.envvar_based_path('FLIRT_DEPENDS_DIR', ...)
    ---------------------------------------------------------------------------

end

dofile 'smks.dep.easylogging.lua'
dofile 'smks.dep.eigen.lua'
dofile 'smks.dep.tbb.lua'
dofile 'smks.dep.boost.lua'
dofile 'smks.dep.zlib.lua'
dofile 'smks.dep.ilmbase.lua'
dofile 'smks.dep.openexr.lua'
dofile 'smks.dep.hdf5.lua'
dofile 'smks.dep.alembic.lua'
dofile 'smks.dep.opensubdiv.lua'
dofile 'smks.dep.osg.lua'
dofile 'smks.dep.lpng.lua'
if os.isfile('smks.dep.lodepng.lua') then
    dofile 'smks.dep.lodepng.lua'
end
dofile 'smks.dep.jpeg.lua'
dofile 'smks.dep.tiff.lua'
dofile 'smks.dep.opengl.lua'
dofile 'smks.dep.opencolorio.lua'
dofile 'smks.dep.json.lua'
dofile 'smks.dep.embree.lua'
dofile 'smks.dep.python.lua'
if os.isfile('smks.dep.qt.lua') then
    dofile 'smks.dep.qt.lua'
end
dofile 'smks.dep.tclap.lua'
dofile 'smks.dep.googletest.lua'
