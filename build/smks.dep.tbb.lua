-- ========================================================================= --
-- Copyright 2016-2018 SUPAMONKS_STUDIO                                      --
-- Author: Pierre-Edouard Landes <pel@supamonks.com>                         --
--                                                                           --
-- This program is free software: you can redistribute it and/or modify      --
-- it under the terms of the GNU Lesser General Public License as            --
-- published by the Free Software Foundation, either version 3 of the        --
-- License, or (at your option) any later version.                           --
--                                                                           --
-- This program is distributed in the hope that it will be useful,           --
-- but WITHOUT ANY WARRANTY; without even the implied warranty of            --
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             --
-- GNU Lesser General Public License for more details.                       --
--                                                                           --
-- You should have received a copy of the GNU Lesser General Public License  --
-- along with this program.  If not, see <https://www.gnu.org/licenses/>.    --
--                                                                           --
-- ========================================================================= --
smks.dep.tbb = {}


smks.dep.tbb.path = function(...)

    ---------------------------------------------------------------------------
    return smks.dep.path(
        'tbb-2017_U7',
        'tbb-2017_U7',
        ...)
    ---------------------------------------------------------------------------

end


smks.dep.tbb.outdir_path = function(...)

    ---------------------------------------------------------------------------
    return smks.dep.tbb.path(
        'build', smks.compiler(), 'x64', '%{cfg.buildcfg}-MT', ...)
    ---------------------------------------------------------------------------

end


smks.dep.tbb.links = function()

    filter {}
    includedirs
    {
        smks.dep.tbb.path('include'),
    }
    libdirs
    {
        smks.dep.tbb.outdir_path()
    }
    filter 'configurations:Release'
        links
        {
            'tbb',
            'tbbmalloc',
            'tbbmalloc_proxy'
        }
    filter 'configurations:Debug'
        links
        {
            'tbb_debug',
            'tbbmalloc_debug',
            'tbbmalloc_proxy_debug'
        }
    filter{}

end


smks.dep.tbb.copy_to_targetdir = function()

    filter 'configurations:Release'
        postbuildcommands
        {
            smks.os.copy_to_targetdir(
                smks.dep.tbb.outdir_path(
                    smks.os.as_sharedlib('tbb'))),
            smks.os.copy_to_targetdir(
                smks.dep.tbb.outdir_path(
                    smks.os.as_sharedlib('tbbmalloc'))),
            smks.os.copy_to_targetdir(
                smks.dep.tbb.outdir_path(
                    smks.os.as_sharedlib('tbbmalloc_proxy'))),
        }
    filter 'configurations:Debug'
        postbuildcommands
        {
            smks.os.copy_to_targetdir(
                smks.dep.tbb.outdir_path(
                    smks.os.as_sharedlib('tbb_debug'))),
            smks.os.copy_to_targetdir(
                smks.dep.tbb.outdir_path(
                    smks.os.as_sharedlib('tbbmalloc_debug'))),
            smks.os.copy_to_targetdir(
                smks.dep.tbb.outdir_path(
                    smks.os.as_sharedlib('tbbmalloc_proxy_debug'))),
        }
    filter{}

end
