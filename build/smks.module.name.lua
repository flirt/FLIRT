-- ========================================================================= --
-- Copyright 2016-2018 SUPAMONKS_STUDIO                                      --
-- Author: Pierre-Edouard Landes <pel@supamonks.com>                         --
--                                                                           --
-- This program is free software: you can redistribute it and/or modify      --
-- it under the terms of the GNU Lesser General Public License as            --
-- published by the Free Software Foundation, either version 3 of the        --
-- License, or (at your option) any later version.                           --
--                                                                           --
-- This program is distributed in the hope that it will be useful,           --
-- but WITHOUT ANY WARRANTY; without even the implied warranty of            --
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             --
-- GNU Lesser General Public License for more details.                       --
--                                                                           --
-- You should have received a copy of the GNU Lesser General Public License  --
-- along with this program.  If not, see <https://www.gnu.org/licenses/>.    --
--                                                                           --
-- ========================================================================= --


smks.module.name = {}





smks.module.name.log = 'SmksLog'
smks.module.name.strid = 'SmksStrId'
smks.module.name.sys = 'SmksSys'
smks.module.name.util = 'SmksUtil'
smks.module.name.math = 'SmksMath'
smks.module.name.img = 'SmksImg'
smks.module.name.asset = 'SmksAsset'
smks.module.name.xchg = 'SmksXchg'
smks.module.name.parm = 'SmksParm'
smks.module.name.scn = 'SmksScn'
smks.module.name.clientbase = 'SmksClientBase'
smks.module.name.rndr_device_api = 'SmksRndrDeviceAPI'
smks.module.name.rndr_client = 'SmksRndrClient'
smks.module.name.rndr_device = 'SmksRndrDevice'
smks.module.name.view_gl = 'SmksViewGL'
smks.module.name.util_osg = 'SmksUtilOsg'
smks.module.name.view_client = 'SmksViewClient'
smks.module.name.view_device_api = 'SmksViewDeviceAPI'
smks.module.name.view_device = 'SmksViewDevice'
smks.module.name.view_device_ctrl = 'SmksViewDeviceCtrl'
smks.module.name.view_debug = 'SmksViewDebug'
