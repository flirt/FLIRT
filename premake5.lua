-- ========================================================================= --
-- Copyright 2016-2018 SUPAMONKS_STUDIO                                      --
-- Author: Pierre-Edouard Landes <pel@supamonks.com>                         --
--                                                                           --
-- This program is free software: you can redistribute it and/or modify      --
-- it under the terms of the GNU Lesser General Public License as            --
-- published by the Free Software Foundation, either version 3 of the        --
-- License, or (at your option) any later version.                           --
--                                                                           --
-- This program is distributed in the hope that it will be useful,           --
-- but WITHOUT ANY WARRANTY; without even the implied warranty of            --
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             --
-- GNU Lesser General Public License for more details.                       --
--                                                                           --
-- You should have received a copy of the GNU Lesser General Public License  --
-- along with this program.  If not, see <https://www.gnu.org/licenses/>.    --
--                                                                           --
-- ========================================================================= --

dofile 'build/smks.lua'

smks.module.copy_default_abc_to_targetdir = function()

    filter {}
    local subdir = 'abc'
    postbuildcommands
    {
        smks.os.make_sub_targetdir(subdir),
        smks.os.copy_to_targetdir(
            smks.repo_path('asset', 'abc', 'default.abc'), subdir),
    }

end

-------------------------------------------------------------------------------

workspace('Flirt')

    platforms {'x64'}
    configurations {'Release', 'Debug'}
    location(string.format('buildFlirt-%s', smks.compiler()))

    -- global settings ------------
    filter 'configurations:Release'
        optimize 'On'
        defines
        {
            'NDEBUG',
        }
    filter 'configurations:Debug'
        symbols 'On'
        defines
        {
            'DEBUG',
            '_DEBUG',
        }
    filter {}
    flags {'NoMinimalRebuild'}

    smks.os.specific_settings()
    -------------------------------

    include 'contrib'
    include 'compile_info'
    include 'smksLog'
    include 'smksStrId'
    include 'smksSys'
    include 'smksUtil'
    include 'smksMath'
    include 'smksImg'
    include 'smksAsset'
    include 'smksXchg'
    include 'smksParm'
    include 'smksScn'
    include 'smksClientBase'
    include 'smksRndrDeviceAPI'
    include 'smksRndrClient'
    include 'smksRndrDevice'
    include 'smksViewGL'
    include 'smksUtilOsg'
    include 'smksViewDeviceAPI'
    include 'smksViewClient'
    include 'smksViewDevice'
    include 'smksViewDeviceCtrl'
    -------------------------------
    if os.isdir(smks.repo_path('smksViewDebug')) then
        include 'smksViewDebug'
    end
    -------------------------------
    include '_pyflirt'
    include 'example'
    include 'testsuite'
