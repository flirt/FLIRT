// ========================================================================= //
// Copyright 2016-2018 SUPAMONKS_STUDIO                                      //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
//                                                                           //
// ========================================================================= //
#pragma once

#include <gtest/gtest.h>

#include <smks/math/types.hpp>
#include <smks/math/matrixAlgo.hpp>
#include <smks/math/test/util.hpp>
#include <smks/math/hash.hpp>

TEST(HashTest, Affine3fHash)
{
    using namespace smks::math;

    for (size_t n = 0; n < 20; ++n)
    {
        Affine3f mat;
        Affine3f addMat;
        Affine3f multMat;

        mat.matrix().setRandom();
        const float factor =  pow<float>(10.0f, n % 5);
        mat.matrix() *= factor;
        mat.makeAffine();

        Affine3f refmat = mat;
        //--------------------------
        size_t refSeed = 0;
        hash_combine(refSeed, mat);
        //--------------------------

        addMat.matrix().setRandom();

        multMat.matrix().setRandom(); // [-1, 1]
        for (size_t r = 0; r < 4; ++r)
            for (size_t c = 0; c < 4; ++c)
                multMat.matrix()(r,c) = 1.1f + multMat.matrix()(r,c); // avoid zeros

        mat.matrix() = (mat.matrix() + addMat.matrix()).eval();
        mat.matrix() = (mat.matrix().cwiseProduct(multMat.matrix())).eval();
        mat.matrix() = (mat.matrix().cwiseQuotient(multMat.matrix())).eval();
        mat.matrix() = (mat.matrix() - addMat.matrix()).eval();

        //--------------------------
        size_t seed = 0;
        hash_combine(seed, mat);
        //--------------------------
        EXPECT_EQ(seed, refSeed);
    }
}
