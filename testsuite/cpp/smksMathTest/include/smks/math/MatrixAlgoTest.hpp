// ========================================================================= //
// Copyright 2016-2018 SUPAMONKS_STUDIO                                      //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
//                                                                           //
// ========================================================================= //
#pragma once

#include <algorithm>
#include <vector>

#include <gtest/gtest.h>

#include <smks/math/types.hpp>
#include <smks/math/matrixAlgo.hpp>
#include <smks/math/test/util.hpp>

TEST(MatrixAlgoTest, NormalMatrixTest)
{
    using namespace smks::math;

    for (size_t n = 0; n < 10; ++n)
    {
        Affine3f xfm;

        xfm.matrix().setRandom();
        xfm.makeAffine();

        Vector4f nrml = Vector4f::Random();
        nrml[3] = 0.0f;
        nrml.normalize();

        Vector4f        nrml2       = transformVector(normalMatrix(xfm),    nrml)               .normalized();
        Eigen::Vector3f refNrml2    = (xfm.linear().inverse().transpose()   * nrml.head<3>())   .normalized();

        EXPECT_TRUE(equal<3>(nrml2.data(), refNrml2.data()));
    }
}

TEST(MatrixAlgoTest, InterpTranslationTest)
{
    using namespace smks::math;

    Eigen::Vector3f trl0(2.0f, 3.0f, 4.0f);
    Eigen::Vector3f trl1(1.0f, 2.0f, 3.0f);
    Affine3f        xfm0(Eigen::Translation3f(trl0[0], trl0[1], trl0[2]));
    Affine3f        xfm1(Eigen::Translation3f(trl1[0], trl1[1], trl1[2]));

    std::vector<float> samples(5, 0.0f);
    const float dt = 1.0f/static_cast<float>(samples.size()-1);
    for (size_t i = 1; i < samples.size(); ++i)
        samples[i] = samples[i-1] + dt;

    Affine3fv xfms;

    interpolateXform(xfm0, xfm1, samples, xfms);

    for (size_t i = 0; i < samples.size(); ++i)
    {
        const float w1 = static_cast<float>(i) * dt;
        const float w0 = 1.0f - w1;

        Eigen::Vector3f refTrl = w0 * trl0 + w1 * trl1;

        EXPECT_TRUE(equal<3>(xfms[i].translation().data(), refTrl.data()));
    }
}

TEST(XformTest, DefaultComposeParms)
{
    using namespace smks::math;

    Affine3f xfm;

    compose(xfm);
    EXPECT_TRUE(smks::xchg::equal(xfm, Affine3f::Identity()));
}

TEST(XformTest, DefaultComposeEulerParms)
{
    using namespace smks::math;

    Affine3f xfm;

    composeEuler(xfm);
    EXPECT_TRUE(smks::xchg::equal(xfm, Affine3f::Identity()));
}

TEST(XformTest, DefaultLookAtParms)
{
    using namespace smks::math;

    Affine3f xfm;

    lookAtPoint(xfm);
    EXPECT_TRUE(smks::xchg::equal(xfm, Affine3f::Identity()));
}

TEST(MatrixAlgoTest, DecomposeComposition)
{
    using namespace smks::math;

    for (size_t i = 0; i < 10; ++i)
    {
        Affine3f    ref_xfm;
        Vector4f    ref_translate   = Vector4f::Random() * 100.0f;
        Quaternionf ref_rotate      = test::getRandomRotation();
        Vector4f    ref_scale       = Vector4f::Random() * 2.5f;

        ref_translate[3] = 1.0f;
        ref_scale[3] = 1.0f;

        compose(ref_xfm , ref_translate, ref_rotate, ref_scale);

        Vector4f    translate, scale;
        Quaternionf rotate;

        decompose(ref_xfm, translate, rotate, scale);

        Affine3f xfm;
        compose(xfm, translate, rotate, scale);

        EXPECT_TRUE(smks::xchg::equal(xfm, ref_xfm));
    }
}

TEST(MatrixAlgoTest, DecomposeCompositionEuler)
{
    using namespace smks::math;

    Eigen::Vector3f axis[3] = {
        Eigen::Vector3f::UnitX(),
        Eigen::Vector3f::UnitY(),
        Eigen::Vector3f::UnitZ() };

    for (size_t o = 0; o < 6; ++o)
    {
        RotateOrder     order = static_cast<RotateOrder>(o);
        unsigned int    r[3];

        getRotationOrderShuffle(order, r);

        for (size_t i = 0; i < 10; ++i)
        {
            Vector4f ref_translate, ref_rotateD, ref_scale;
            Affine3f ref_xfm;

            ref_translate       = Vector4f::Random() * 100.0f;
            ref_rotateD         = Vector4f::Random() * 180.0f;  // in degrees
            ref_scale           = Vector4f::Random() * 2.5f;

            ref_translate[3]    = 1.0f;
            ref_rotateD[3]      = 1.0f;
            ref_scale[3]        = 1.0f;

            composeEuler(ref_xfm, ref_translate, order, ref_rotateD, ref_scale);

            Vector4f translate, rotateD, scale;
            decompose(ref_xfm, translate, order, rotateD, scale);

            Affine3f xfm;
            composeEuler(xfm, translate, order, rotateD, scale);

            EXPECT_TRUE(smks::xchg::equal(xfm, ref_xfm));
        }
    }
}

TEST(MatrixAlgoTest, DecomposeLookAtPoint)
{
    using namespace smks::math;

    for (size_t i = 0; i < 10; ++i)
    {
        Vector4f ref_eyePosition, ref_targetPosition, ref_upVector;
        Affine3f ref_xfm;

        ref_eyePosition     = Vector4f::Random() * 100.0f;
        ref_targetPosition  = Vector4f::Random() * 100.0f;  // in degrees
        ref_upVector        = Vector4f::Random();

        ref_eyePosition[3]      = 1.0f;
        ref_targetPosition[3]   = 1.0f;
        ref_upVector[3]         = 0.0f;
        ref_upVector.normalized();

        lookAtPoint(ref_xfm, ref_eyePosition, ref_targetPosition, ref_upVector);

        Vector4f translate, rotateD, scale, forwardVector, upVector;
        Affine3f xfm;

        decompose(ref_xfm, translate, RotateOrder::XYZ, rotateD, scale, &forwardVector, &upVector);
        lookAtPoint(xfm, translate, translate + forwardVector, upVector);

        EXPECT_TRUE(smks::xchg::equal(xfm, ref_xfm));
    }
}
