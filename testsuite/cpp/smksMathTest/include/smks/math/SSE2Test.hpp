// ========================================================================= //
// Copyright 2016-2018 SUPAMONKS_STUDIO                                      //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
//                                                                           //
// ========================================================================= //
#pragma once

#include <algorithm>
#include <vector>

#include <gtest/gtest.h>

#include <smks/math/types.hpp>
#include <smks/math/matrixAlgo.hpp>
#include <smks/math/test/util.hpp>

TEST(SSE2Test, SSE2Transform4x4Point)
{
    using namespace smks::math;

    for (size_t n = 0; n < 10; ++n)
    {
        Affine3f mat;

        mat.matrix().setRandom();
        mat.makeAffine();

        const Vector4f& p = Vector4f::Random();
        Vector4f        q;

        transform4x4Point(mat.matrix().data(), p.data(), q.data());

        const Vector4f& qref = mat * p;

        EXPECT_TRUE(smks::xchg::equal(q, qref));
    }
}

TEST(SSE2Test, SSE2TransformPoint)
{
    using namespace smks::math;

    for (size_t n = 0; n < 10; ++n)
    {
        Affine3f xfm;
        Vector4f p = Vector4f::Random();

        xfm.matrix().setRandom();
        xfm.makeAffine();
        p[3] = 1.0f;    // important !

        const Vector4f& q = transformPoint(xfm, p);
        const Vector4f& qref = xfm * p;

        EXPECT_TRUE(smks::xchg::equal(q, qref));
    }
}

TEST(SSE2Test, SSE2TransformVector)
{
    using namespace smks::math;

    for (size_t n = 0; n < 10; ++n)
    {
        Affine3f xfm;
        Vector4f p = Vector4f::Random();

        xfm.matrix().setRandom();
        xfm.makeAffine();
        p[3] = 42.0f;   // irrelevant value

        const Vector4f& q = transformVector(xfm, p);
        const Eigen::Vector3f& qref = xfm.linear() * p.head<3>();

        EXPECT_TRUE(smks::math::equal<3>(q.data(), qref.data()));
        EXPECT_FLOAT_EQ(q[3], 0.0f);
    }
}
