// ========================================================================= //
// Copyright 2016-2018 SUPAMONKS_STUDIO                                      //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
//                                                                           //
// ========================================================================= //
#pragma once

#include <cmath>
#include <gtest/gtest.h>

#include <Eigen/Dense>
#include <smks/parm/Container.hpp>
#include <smks/xchg/BoundingBox.hpp>

TEST(ParmTypeTest, BoundingBoxTest)
{
    smks::parm::Container::Ptr  parms = smks::parm::Container::create();
    smks::xchg::BoundingBox     box;

    for (size_t i = 0; i < 10; ++i)
    {
        float p[3];
        for (size_t j = 0; j < 3; ++j)
            p[j] = static_cast<float>(rand() % 100);
        box += p;
    }

    std::string     parmName    = "my_parm";
    unsigned int    parmId      = parms->set<smks::xchg::BoundingBox>(parmName.c_str(), box);

    EXPECT_FALSE(parms->existsAs<bool>(parmName));
    EXPECT_FALSE(parms->existsAs<bool>(parmId));
    EXPECT_FALSE(parms->existsAs<int>(parmName));
    EXPECT_FALSE(parms->existsAs<int>(parmId));
    EXPECT_FALSE(parms->existsAs<float>(parmName));
    EXPECT_FALSE(parms->existsAs<float>(parmId));
    EXPECT_FALSE(parms->existsAs<std::string>(parmName));
    EXPECT_FALSE(parms->existsAs<std::string>(parmId));
    EXPECT_TRUE(parms->existsAs<smks::xchg::BoundingBox>(parmId));
    EXPECT_TRUE(parms->existsAs<smks::xchg::BoundingBox>(parmId));

    EXPECT_TRUE(parms->get<smks::xchg::BoundingBox>(parmName) == box);
    EXPECT_TRUE(parms->get<smks::xchg::BoundingBox>(parmId) == box);
}

TEST(ParmTypeTest, EigenMatrixTest)
{
    smks::parm::Container::Ptr  parms = smks::parm::Container::create();
    Eigen::Matrix4f             mat;

    mat.setRandom();

    std::string     parmName    = "my_parm";
    unsigned int    parmId      = parms->set<Eigen::Matrix4f>(parmName.c_str(), mat);

    EXPECT_FALSE(parms->existsAs<bool>(parmName));
    EXPECT_FALSE(parms->existsAs<bool>(parmId));
    EXPECT_FALSE(parms->existsAs<int>(parmName));
    EXPECT_FALSE(parms->existsAs<int>(parmId));
    EXPECT_FALSE(parms->existsAs<float>(parmName));
    EXPECT_FALSE(parms->existsAs<float>(parmId));
    EXPECT_FALSE(parms->existsAs<std::string>(parmName));
    EXPECT_FALSE(parms->existsAs<std::string>(parmId));
    EXPECT_TRUE(parms->existsAs<Eigen::Matrix4f>(parmId));
    EXPECT_TRUE(parms->existsAs<Eigen::Matrix4f>(parmId));

    EXPECT_TRUE(parms->get<Eigen::Matrix4f>(parmName) == mat);
    EXPECT_TRUE(parms->get<Eigen::Matrix4f>(parmId) == mat);
}
