// ========================================================================= //
// Copyright 2016-2018 SUPAMONKS_STUDIO                                      //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
//                                                                           //
// ========================================================================= //
#pragma once

#include <gtest/gtest.h>

#include <smks/parm/Container.hpp>
#include <smks/xchg/Data.hpp>
#include <smks/xchg/DataStream.hpp>

TEST(ParmContainerTest, BooleanTest)
{
    smks::parm::Container::Ptr parms = smks::parm::Container::create();

    std::string     parmName    = "my_parm";
    bool            parmValue   = true;
    unsigned int    parmId      = parms->set<bool>(parmName.c_str(), parmValue);

    EXPECT_TRUE(parms->existsAs<bool>(parmName));
    EXPECT_TRUE(parms->existsAs<bool>(parmId));
    EXPECT_FALSE(parms->existsAs<int>(parmName));
    EXPECT_FALSE(parms->existsAs<int>(parmId));
    EXPECT_FALSE(parms->existsAs<float>(parmName));
    EXPECT_FALSE(parms->existsAs<float>(parmId));
    EXPECT_FALSE(parms->existsAs<std::string>(parmName));
    EXPECT_FALSE(parms->existsAs<std::string>(parmId));

    EXPECT_TRUE(parms->get<bool>(parmName) == parmValue);
    EXPECT_TRUE(parms->get<bool>(parmId) == parmValue);
}

TEST(ParmContainerTest, IntTest)
{
    smks::parm::Container::Ptr parms = smks::parm::Container::create();

    std::string     parmName    = "my_parm";
    int             parmValue   = 42;
    unsigned int    parmId      = parms->set<int>(parmName.c_str(), parmValue);

    EXPECT_FALSE(parms->existsAs<bool>(parmName));
    EXPECT_FALSE(parms->existsAs<bool>(parmId));
    EXPECT_TRUE(parms->existsAs<int>(parmName));
    EXPECT_TRUE(parms->existsAs<int>(parmId));
    EXPECT_FALSE(parms->existsAs<float>(parmName));
    EXPECT_FALSE(parms->existsAs<float>(parmId));
    EXPECT_FALSE(parms->existsAs<std::string>(parmName));
    EXPECT_FALSE(parms->existsAs<std::string>(parmId));

    EXPECT_EQ(parms->get<int>(parmName), parmValue);
    EXPECT_EQ(parms->get<int>(parmId), parmValue);
}

TEST(ParmContainerTest, FloatTest)
{
    smks::parm::Container::Ptr parms = smks::parm::Container::create();

    std::string     parmName    = "my_parm";
    float           parmValue   = 42.0f;
    unsigned int    parmId      = parms->set<float>(parmName.c_str(), parmValue);

    EXPECT_FALSE(parms->existsAs<bool>(parmName));
    EXPECT_FALSE(parms->existsAs<bool>(parmId));
    EXPECT_FALSE(parms->existsAs<int>(parmName));
    EXPECT_FALSE(parms->existsAs<int>(parmId));
    EXPECT_TRUE(parms->existsAs<float>(parmName));
    EXPECT_TRUE(parms->existsAs<float>(parmId));
    EXPECT_FALSE(parms->existsAs<std::string>(parmName));
    EXPECT_FALSE(parms->existsAs<std::string>(parmId));

    EXPECT_FLOAT_EQ(parms->get<float>(parmName), parmValue);
    EXPECT_FLOAT_EQ(parms->get<float>(parmId), parmValue);
}

TEST(ParmContainerTest, StringTest)
{
    smks::parm::Container::Ptr parms = smks::parm::Container::create();

    std::string     parmName    = "my_parm";
    std::string     parmValue   = "42.0f";
    unsigned int    parmId      = parms->set<std::string>(parmName.c_str(), parmValue);

    EXPECT_FALSE(parms->existsAs<bool>(parmName));
    EXPECT_FALSE(parms->existsAs<bool>(parmId));
    EXPECT_FALSE(parms->existsAs<int>(parmName));
    EXPECT_FALSE(parms->existsAs<int>(parmId));
    EXPECT_FALSE(parms->existsAs<float>(parmName));
    EXPECT_FALSE(parms->existsAs<float>(parmId));
    EXPECT_TRUE(parms->existsAs<std::string>(parmName));
    EXPECT_TRUE(parms->existsAs<std::string>(parmId));

    EXPECT_TRUE(parms->get<std::string>(parmName) == parmValue);
    EXPECT_TRUE(parms->get<std::string>(parmId) == parmValue);
}

TEST(ParmContainerTest, ObjectTest)
{
    ////////////////
    // struct MyObject
    ////////////////
    struct MyObject
    {
        int         my_int;
        double      my_double;
        std::string my_string;

    public:
        inline
        bool
        operator==(const MyObject&) const
        {
            return false;
        }
    };

    smks::parm::Container::Ptr parms = smks::parm::Container::create();

    std::string     parmName    = "my_parm";
    MyObject        parmValue;

    parmValue.my_int    = 18;
    parmValue.my_double = 36.0;
    parmValue.my_string = "seventy-two";

    unsigned int    parmId      = parms->set<MyObject>(parmName.c_str(), parmValue);

    // change local values
    parmValue.my_int    = 36;
    parmValue.my_double = 72.0;
    parmValue.my_string = "eighteen";

    EXPECT_FALSE(parms->existsAs<bool>(parmName));
    EXPECT_FALSE(parms->existsAs<bool>(parmId));
    EXPECT_FALSE(parms->existsAs<int>(parmName));
    EXPECT_FALSE(parms->existsAs<int>(parmId));
    EXPECT_FALSE(parms->existsAs<float>(parmName));
    EXPECT_FALSE(parms->existsAs<float>(parmId));
    EXPECT_FALSE(parms->existsAs<std::string>(parmName));
    EXPECT_FALSE(parms->existsAs<std::string>(parmId));

    EXPECT_TRUE(parms->existsAs<MyObject>(parmName));
    EXPECT_TRUE(parms->existsAs<MyObject>(parmId));

    const MyObject& storedValue = parms->get<MyObject>(parmName);

    EXPECT_EQ(storedValue.my_int, 18);
    EXPECT_DOUBLE_EQ(storedValue.my_double, 36.0);
    EXPECT_STREQ(storedValue.my_string.c_str(), "seventy-two");
}

TEST(ParmContainerTest, SeveralParmsTest)
{
    smks::parm::Container::Ptr parms = smks::parm::Container::create();

    std::string     parmName1   = "my_parm_1";
    std::string     parmValue1  = "str";
    unsigned int    parmId1     = parms->set<std::string>(parmName1.c_str(), parmValue1);

    std::string     parmName2   = "my_parm_2";
    int             parmValue2  = 42;
    unsigned int    parmId2     = parms->set<int>(parmName2.c_str(), parmValue2);

    EXPECT_TRUE(parms->existsAs<std::string>(parmName1));
    EXPECT_TRUE(parms->existsAs<std::string>(parmId1));

    EXPECT_TRUE(parms->existsAs<int>(parmName2));
    EXPECT_TRUE(parms->existsAs<int>(parmId2));
}

TEST(ParmContainerTest, RewriteTest)
{
    smks::parm::Container::Ptr parms = smks::parm::Container::create();

    std::string     parmName    = "my_parm";
    int             parmValue1  = 42;
    int             parmValue2  = -24;

    unsigned int    parmId      = parms->set<int>(parmName.c_str(), parmValue1);

    EXPECT_TRUE(parms->existsAs<int>(parmName));
    EXPECT_TRUE(parms->existsAs<int>(parmId));
    EXPECT_EQ(parms->get<int>(parmName), parmValue1);

    parms->set<int>(parmName.c_str(), parmValue2);

    EXPECT_TRUE(parms->existsAs<int>(parmName));
    EXPECT_TRUE(parms->existsAs<int>(parmId));
    EXPECT_EQ(parms->get<int>(parmName), parmValue2);
}

TEST(ParmContainerTest, TypeChangeTest)
{
    smks::parm::Container::Ptr parms = smks::parm::Container::create();

    std::string     parmName    = "my_parm";
    int             parmValue1  = 42;
    std::string     parmValue2  = "-24";

    unsigned int    parmId      = parms->set<int>(parmName.c_str(), parmValue1);

    EXPECT_TRUE(parms->existsAs<int>(parmName));
    EXPECT_TRUE(parms->existsAs<int>(parmId));
    EXPECT_EQ(parms->get<int>(parmName), parmValue1);

    parms->set<std::string>(parmName.c_str(), parmValue2);

    EXPECT_TRUE(parms->existsAs<std::string>(parmName));
    EXPECT_TRUE(parms->existsAs<std::string>(parmId));
    EXPECT_TRUE(parms->get<std::string>(parmName) == parmValue2);
}
