// ========================================================================= //
// Copyright 2016-2018 SUPAMONKS_STUDIO                                      //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
//                                                                           //
// ========================================================================= //
#pragma once

#include "smks/scn/ArchiveTest.hpp"
#include "smks/scn/SceneTest.hpp"
#include "smks/scn/TreeNodeTest.hpp"
#include "smks/scn/ParmConnectionTest.hpp"
#include "smks/scn/TreeNodeSelectorTest.hpp"
#include "smks/scn/ParmAssignTest.hpp"
#include "smks/scn/NodeLinkingTest.hpp"
#include "smks/scn/LightTest.hpp"
#include "smks/scn/XformTest.hpp"
#include "smks/scn/CameraTest.hpp"
#include "smks/scn/TextureTest.hpp"
#include "smks/scn/UserAttribTest.hpp"
#include "smks/scn/LinkEventTest.hpp"
#include "smks/scn/ChainedParmEventTest.hpp"
#include "smks/scn/GlobalParmTest.hpp"
