// ========================================================================= //
// Copyright 2016-2018 SUPAMONKS_STUDIO                                      //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
//                                                                           //
// ========================================================================= //
#pragma once

#include <gtest/gtest.h>

#include <smks/scn/Scene.hpp>
#include <smks/scn/Light.hpp>

#include "smks/scn/test/util.hpp"
#include <smks/math/matrixAlgo.hpp>

TEST(LightTest, Creation)
{
    using namespace smks::scn;

    Scene::Ptr scene = Scene::create();
    Light::Ptr light = Light::create("my_light_code", "my_light", scene);

    EXPECT_TRUE(scene->hasDescendant(light->id()));

    Scene::destroy(scene);
}
