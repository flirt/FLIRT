// ========================================================================= //
// Copyright 2016-2018 SUPAMONKS_STUDIO                                      //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
//                                                                           //
// ========================================================================= //
#pragma once

#include <gtest/gtest.h>

#include "smks/scn/test/util.hpp"
#include <smks/parm/builtins.hpp>
#include <smks/scn/TreeNodeSelector.hpp>
#include "smks/scn/test/FindNodesWithParm.hpp"
#include "smks/scn/test/FilterByNameLength.hpp"

TEST(ParmInheritanceTest, NodeCreation)
{
    smks::scn::test::FindNodesWithParm<int> finder;
    smks::scn::test::SceneContent1          all;

    const std::string   parmName1   = "my_parm1";
    const int           parmValue1  = 42;

    smks::scn::TreeNodeSelector::Ptr    select  = smks::scn::TreeNodeSelector::create("/a.*", "__select", all.scene);

    smks::scn::ParmAssign<int>::Ptr     assign1 = smks::scn::ParmAssign<int>::create(
        parmName1.c_str(), parmValue1, "__assign1", all.scene);

    unsigned int id = select->id();
    assign1->setParameter<unsigned int>(smks::parm::selectorId(), id);

    // check correctness
    smks::xchg::IdSet expected;
    expected.insert(all.node_a->id());
    expected.insert(all.node_a_b->id());
    expected.insert(all.node_a_aa->id());
    expected.insert(all.node_a_ab->id());
    expected.insert(all.node_a_aa_a->id());

    finder.initialize(parmName1);
    all.scene->accept(finder);
    //smks::scn::test::printNodes(std::cout << "nodes : ", finder.found(), all.scene);
    EXPECT_TRUE(smks::xchg::equal(finder.found(), expected));

    //---
    assign1->unsetParameter(smks::parm::selectorId().id());
    //---

    // check correctness
    finder.initialize(parmName1);
    all.scene->accept(finder);
    EXPECT_TRUE(finder.found().empty());
}
