// ========================================================================= //
// Copyright 2016-2018 SUPAMONKS_STUDIO                                      //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
//                                                                           //
// ========================================================================= //
#pragma once

#include <gtest/gtest.h>
#include <smks/scn/Scene.hpp>
#include <smks/scn/TreeNode.hpp>
#include "smks/scn/test/TestNode.hpp"

TEST(LinkEventTest, LinkThenCreateSource)
{
    using namespace smks;

    std::vector<std::string>    linkNames;
    std::vector<unsigned int>   linkId;
    for (size_t i = 0; i < 3; ++i)
    {
        linkNames.push_back ("__link_" + std::to_string(i) + "__");
        linkId.push_back    (util::string::getId(linkNames[i].c_str()));
    }

    scn::test::EventHistory evts;
    std::vector<size_t>     idx;
    const std::string       sourceName  = "__source__";
    const unsigned int      sourceId    = util::string::getId(("/" + sourceName).c_str());
    scn::Scene::Ptr         scene       = scn::Scene::create();
    //######################

    scn::test::TestNode::Ptr target = scn::test::TestNode::create("__target__", scene, &evts);
    scn::TreeNode::Ptr otherSource  = scn::TreeNode::create("__other_source__", scene);
    target->setParameterId(linkNames[0].c_str(), sourceId);
    target->setParameterId(linkNames[1].c_str(), sourceId);
    //----------
    evts.clear();
    //---
    scn::TreeNode::Ptr source = scn::TreeNode::create(sourceName.c_str(), scene);
    //---
    idx = evts.getSceneEvents(xchg::SceneEvent::LINK_ATTACHED);
    EXPECT_EQ(idx.size(), 2);
    for (size_t i = 0; i < idx.size(); ++i)
    {
        const xchg::SceneEvent* sceneEvt = evts[idx[i]].asSceneEvent();
        EXPECT_EQ(sceneEvt->target(), target->id());
        EXPECT_EQ(sceneEvt->source(), source->id());
        EXPECT_TRUE(sceneEvt->parm() == linkId[0] || sceneEvt->parm() == linkId[1]);
    }
    EXPECT_NE(evts[idx[0]].asSceneEvent()->parm(), evts[idx[1]].asSceneEvent()->parm());
    //----------
    target->setParameterId(linkNames[2].c_str(), sourceId);

    target->unsetParameter(linkNames[0].c_str());
    target->setParameterId(linkNames[1].c_str(), otherSource->id());
    scn::TreeNode::destroy(source);
    scn::TreeNode::destroy(target);

    //######################
    scn::Scene::destroy(scene);
}

TEST(LinkEventTest, CreateSourceThenLink)
{
    using namespace smks;

    std::vector<std::string>    linkNames;
    std::vector<unsigned int>   linkId;
    for (size_t i = 0; i < 3; ++i)
    {
        linkNames.push_back ("__link_" + std::to_string(i) + "__");
        linkId.push_back    (util::string::getId(linkNames[i].c_str()));
    }

    scn::test::EventHistory evts;
    std::vector<size_t>     idx;
    const std::string       sourceName  = "__source__";
    const unsigned int      sourceId    = util::string::getId(("/" + sourceName).c_str());
    scn::Scene::Ptr         scene       = scn::Scene::create();
    //######################

    scn::test::TestNode::Ptr target = scn::test::TestNode::create("__target__", scene, &evts);
    scn::TreeNode::Ptr otherSource  = scn::TreeNode::create("__other_source__", scene);
    target->setParameterId(linkNames[0].c_str(), sourceId);
    target->setParameterId(linkNames[1].c_str(), sourceId);
    scn::TreeNode::Ptr source = scn::TreeNode::create(sourceName.c_str(), scene);
    //----------
    evts.clear();
    //---
    target->setParameterId(linkNames[2].c_str(), sourceId);
    //---
    idx = evts.getSceneEvents(xchg::SceneEvent::LINK_ATTACHED);
    EXPECT_EQ(idx.size(), 1);
    const xchg::SceneEvent* sceneEvt = evts[idx.front()].asSceneEvent();
    EXPECT_EQ(sceneEvt->target(), target->id());
    EXPECT_EQ(sceneEvt->source(), source->id());
    EXPECT_TRUE(sceneEvt->parm() == linkId[2]);
    //----------
    target->unsetParameter(linkNames[0].c_str());
    target->setParameterId(linkNames[1].c_str(), otherSource->id());
    scn::TreeNode::destroy(source);
    scn::TreeNode::destroy(target);

    //######################
    scn::Scene::destroy(scene);
}

TEST(LinkEventTest, UnlinkThenDestroySource)
{
    using namespace smks;

    std::vector<std::string>    linkNames;
    std::vector<unsigned int>   linkId;
    for (size_t i = 0; i < 3; ++i)
    {
        linkNames.push_back ("__link_" + std::to_string(i) + "__");
        linkId.push_back    (util::string::getId(linkNames[i].c_str()));
    }

    scn::test::EventHistory evts;
    std::vector<size_t>     idx;
    const std::string       sourceName  = "__source__";
    const unsigned int      sourceId    = util::string::getId(("/" + sourceName).c_str());
    scn::Scene::Ptr         scene       = scn::Scene::create();
    //######################

    scn::test::TestNode::Ptr target = scn::test::TestNode::create("__target__", scene, &evts);
    scn::TreeNode::Ptr otherSource  = scn::TreeNode::create("__other_source__", scene);
    target->setParameterId(linkNames[0].c_str(), sourceId);
    target->setParameterId(linkNames[1].c_str(), sourceId);
    scn::TreeNode::Ptr source = scn::TreeNode::create(sourceName.c_str(), scene);
    target->setParameterId(linkNames[2].c_str(), sourceId);
    //---------
    evts.clear();
    //---
    target->unsetParameter(linkNames[0].c_str());
    //---
    idx = evts.getSceneEvents(xchg::SceneEvent::LINK_DETACHED);
    EXPECT_EQ(idx.size(), 1);
    const xchg::SceneEvent* sceneEvt = evts[idx.front()].asSceneEvent();
    EXPECT_EQ(sceneEvt->target(), target->id());
    EXPECT_EQ(sceneEvt->source(), source->id());
    EXPECT_TRUE(sceneEvt->parm() == linkId[0]);
    //---------
    target->setParameterId(linkNames[1].c_str(), otherSource->id());
    scn::TreeNode::destroy(source);
    scn::TreeNode::destroy(target);

    //######################
    scn::Scene::destroy(scene);
}

TEST(LinkEventTest, DestroySourceThenUnlink)
{
    using namespace smks;

    std::vector<std::string>    linkNames;
    std::vector<unsigned int>   linkId;
    for (size_t i = 0; i < 3; ++i)
    {
        linkNames.push_back ("__link_" + std::to_string(i) + "__");
        linkId.push_back    (util::string::getId(linkNames[i].c_str()));
    }

    scn::test::EventHistory evts;
    std::vector<size_t>     idx;
    const std::string       sourceName  = "__source__";
    const unsigned int      sourceId    = util::string::getId(("/" + sourceName).c_str());
    scn::Scene::Ptr         scene       = scn::Scene::create();
    //######################

    scn::test::TestNode::Ptr target = scn::test::TestNode::create("__target__", scene, &evts);
    scn::TreeNode::Ptr otherSource  = scn::TreeNode::create("__other_source__", scene);
    target->setParameterId(linkNames[0].c_str(), sourceId);
    target->setParameterId(linkNames[1].c_str(), sourceId);
    scn::TreeNode::Ptr source = scn::TreeNode::create(sourceName.c_str(), scene);
    target->setParameterId(linkNames[2].c_str(), sourceId);

    target->unsetParameter(linkNames[0].c_str());
    target->setParameterId(linkNames[1].c_str(), otherSource->id());
    //---------
    evts.clear();
    //---
    scn::TreeNode::destroy(source);
    //---
    idx = evts.getSceneEvents(xchg::SceneEvent::LINK_DETACHED);
    EXPECT_EQ(idx.size(), 1);
    const xchg::SceneEvent* sceneEvt = evts[idx.front()].asSceneEvent();
    EXPECT_EQ(sceneEvt->target(), target->id());
    EXPECT_EQ(sceneEvt->source(), sourceId);
    EXPECT_TRUE(sceneEvt->parm() == linkId[2]);
    //---------
    scn::TreeNode::destroy(target);

    //######################
    scn::Scene::destroy(scene);
}

TEST(LinkEventTest, DestroyTargetThenUnlink)
{
    using namespace smks;

    std::vector<std::string>    linkNames;
    std::vector<unsigned int>   linkId;
    for (size_t i = 0; i < 3; ++i)
    {
        linkNames.push_back ("__link_" + std::to_string(i) + "__");
        linkId.push_back    (util::string::getId(linkNames[i].c_str()));
    }

    scn::test::EventHistory evts;
    std::vector<size_t>     idx;
    const std::string       sourceName  = "__source__";
    const unsigned int      sourceId    = util::string::getId(("/" + sourceName).c_str());
    scn::Scene::Ptr         scene       = scn::Scene::create();
    //######################

    scn::test::TestNode::Ptr target = scn::test::TestNode::create("__target__", scene, &evts);
    scn::TreeNode::Ptr otherSource  = scn::TreeNode::create("__other_source__", scene);
    target->setParameterId(linkNames[0].c_str(), sourceId);
    target->setParameterId(linkNames[1].c_str(), sourceId);
    scn::TreeNode::Ptr source = scn::TreeNode::create(sourceName.c_str(), scene);
    target->setParameterId(linkNames[2].c_str(), sourceId);

    target->unsetParameter(linkNames[0].c_str());
    target->setParameterId(linkNames[1].c_str(), otherSource->id());    // ---->
    scn::TreeNode::destroy(source);
    //---------
    evts.clear();
    //---
    const unsigned int targetId = target->id();
    scn::TreeNode::destroy(target); // <-----
    //---
    idx = evts.getSceneEvents(xchg::SceneEvent::LINK_DETACHED);
    EXPECT_EQ(idx.size(), 1);
    const xchg::SceneEvent* sceneEvt = evts[idx.front()].asSceneEvent();
    EXPECT_EQ(sceneEvt->target(), targetId);
    EXPECT_EQ(sceneEvt->source(), otherSource->id());
    EXPECT_TRUE(sceneEvt->parm() == linkId[1]);
    //---------

    //######################
    scn::Scene::destroy(scene);
}

TEST(LinkEventTest, SwapLinkSource)
{
    using namespace smks;

    std::vector<std::string>    linkNames;
    std::vector<unsigned int>   linkId;
    for (size_t i = 0; i < 3; ++i)
    {
        linkNames.push_back ("__link_" + std::to_string(i) + "__");
        linkId.push_back    (util::string::getId(linkNames[i].c_str()));
    }

    scn::test::EventHistory evts;
    std::vector<size_t>     idx;
    const std::string       sourceName  = "__source__";
    const unsigned int      sourceId    = util::string::getId(("/" + sourceName).c_str());
    scn::Scene::Ptr         scene       = scn::Scene::create();
    //######################

    scn::test::TestNode::Ptr target = scn::test::TestNode::create("__target__", scene, &evts);
    scn::TreeNode::Ptr otherSource  = scn::TreeNode::create("__other_source__", scene);
    target->setParameterId(linkNames[0].c_str(), sourceId);
    target->setParameterId(linkNames[1].c_str(), sourceId);
    scn::TreeNode::Ptr source = scn::TreeNode::create(sourceName.c_str(), scene);
    target->setParameterId(linkNames[2].c_str(), sourceId);

    target->unsetParameter(linkNames[0].c_str());
    //---------
    evts.clear();
    //---
    target->setParameterId(linkNames[1].c_str(), otherSource->id());
    //---
    idx = evts.getSceneEvents(xchg::SceneEvent::LINK_DETACHED);
    EXPECT_EQ(idx.size(), 1);
    const xchg::SceneEvent* sceneEvt = evts[idx.front()].asSceneEvent();
    EXPECT_EQ(sceneEvt->target(), target->id());
    EXPECT_EQ(sceneEvt->source(), sourceId);
    EXPECT_TRUE(sceneEvt->parm() == linkId[1]);

    idx = evts.getSceneEvents(xchg::SceneEvent::LINK_ATTACHED);
    EXPECT_EQ(idx.size(), 1);
    sceneEvt = evts[idx.front()].asSceneEvent();
    EXPECT_EQ(sceneEvt->target(), target->id());
    EXPECT_EQ(sceneEvt->source(), otherSource->id());
    EXPECT_TRUE(sceneEvt->parm() == linkId[1]);
    //---------
    scn::TreeNode::destroy(source);
    scn::TreeNode::destroy(target);

    //######################
    scn::Scene::destroy(scene);
}
