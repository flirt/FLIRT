// ========================================================================= //
// Copyright 2016-2018 SUPAMONKS_STUDIO                                      //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
//                                                                           //
// ========================================================================= //
#pragma once

#include <gtest/gtest.h>

#include <smks/scn/Scene.hpp>
#include <smks/scn/Camera.hpp>
#include <smks/math/matrixAlgo.hpp>

#include "smks/scn/test/util.hpp"
#include "smks/scn/test/ParmObserver.hpp"

TEST(CameraTest, CustomCameraRegistration)
{
    using namespace smks::scn;

    Scene::Ptr  scene   = Scene::create();
    Camera::Ptr camera  = Camera::create("my_camera", scene);

    EXPECT_TRUE(scene->hasDescendant(camera->id()));

    Scene::destroy(scene);
}

TEST(CameraTest, CustomCameraDefaultValue)
{
    using namespace smks::scn;

    Scene::Ptr  scene   = Scene::create();
    Camera::Ptr camera  = Camera::create("my_camera", scene);

    float yFieldOfViewD, xAspectRatio, zNear, zFar;

    bool status = camera->getParameter<float>(smks::parm::yFieldOfViewD(), yFieldOfViewD);
    EXPECT_FALSE(status);
    EXPECT_EQ(yFieldOfViewD, 37.85f);

    status = camera->getParameter<float>(smks::parm::xAspectRatio(), xAspectRatio);
    EXPECT_FALSE(status);
    EXPECT_EQ(xAspectRatio, 1.5f);

    status = camera->getParameter<float>(smks::parm::zNear(), zNear);
    EXPECT_FALSE(status);
    EXPECT_EQ(zNear, 0.1f);

    status = camera->getParameter<float>(smks::parm::zFar(), zFar);
    EXPECT_FALSE(status);
    EXPECT_EQ(zFar, 1e+5f);

    Scene::destroy(scene);
}

TEST(CameraTest, CustomCameraSetters)
{
    using namespace smks::scn;

    Scene::Ptr  scene   = Scene::create();
    Camera::Ptr camera  = Camera::create("my_camera", scene);

    float ref_yFieldOfViewD = 45.0f;
    float ref_xAspectRatio  = 2.0f;
    float ref_zNear         = 1.5f;
    float ref_zFar          = 150.0f;

    camera->setParameter<float>(smks::parm::yFieldOfViewD   (), ref_yFieldOfViewD);
    camera->setParameter<float>(smks::parm::xAspectRatio    (), ref_xAspectRatio);
    camera->setParameter<float>(smks::parm::zNear           (), ref_zNear);
    camera->setParameter<float>(smks::parm::zFar            (), ref_zFar);

    float yFieldOfViewD, xAspectRatio, zNear, zFar;

    bool status = camera->getParameter<float>(smks::parm::yFieldOfViewD(), yFieldOfViewD);
    EXPECT_TRUE(status);
    EXPECT_EQ(yFieldOfViewD, ref_yFieldOfViewD);

    status = camera->getParameter<float>(smks::parm::xAspectRatio(), xAspectRatio);
    EXPECT_TRUE(status);
    EXPECT_EQ(xAspectRatio, ref_xAspectRatio);

    status = camera->getParameter<float>(smks::parm::zNear(), zNear);
    EXPECT_TRUE(status);
    EXPECT_EQ(zNear, ref_zNear);

    status = camera->getParameter<float>(smks::parm::zFar(), zFar);
    EXPECT_TRUE(status);
    EXPECT_EQ(zFar, ref_zFar);

    Scene::destroy(scene);
}

TEST(CameraTest, CameraHandlesWorldTransformChanged)
{
    using namespace smks;

    scn::test::EventHistory     evts;
    std::vector<size_t>         idx;
    const xchg::ParmEvent*      parmEvt;

    scn::Scene::Ptr     scene       = scn::Scene::create();
    scn::Xform::Ptr     ancestor    = scn::Xform::create("_ancestor_", scene);
    scn::Xform::Ptr     parent      = scn::Xform::create("_parent_", ancestor);
    scn::Camera::Ptr    camera      = scn::Camera::create("_camera_", parent);
    //---
    scn::test::ParmObserver::Ptr obs = scn::test::ParmObserver::create();
    camera->registerObserver(obs);

    //---------
    obs->flushHistory();
    //---
    parent->setParameter<math::Affine3f>(parm::transform(), math::test::getRandomTransform());
    //---
    obs->flushHistory(&evts);
    idx = evts.getParmEvents(parm::worldTransform().id());
    EXPECT_FALSE(idx.empty());
    for (size_t i = 0; i < idx.size(); ++i)
    {
        parmEvt = evts[idx[i]].asParmEvent();
        EXPECT_EQ(parmEvt->node(), parent->id());
        EXPECT_TRUE(parmEvt->comesFromAncestor());
    }
    //---
    ancestor->setParameter<math::Affine3f>(parm::transform(), math::test::getRandomTransform());
    //---
    obs->flushHistory(&evts);
    idx = evts.getParmEvents(parm::worldTransform().id());
    EXPECT_FALSE(idx.empty());
    for (size_t i = 0; i < idx.size(); ++i)
    {
        parmEvt = evts[idx[i]].asParmEvent();
        EXPECT_EQ(parmEvt->node(), ancestor->id());
        EXPECT_TRUE(parmEvt->comesFromAncestor());
    }
    //---------

    scn::Scene::destroy(scene);
}
