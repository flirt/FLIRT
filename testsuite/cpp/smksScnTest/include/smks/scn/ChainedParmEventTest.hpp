// ========================================================================= //
// Copyright 2016-2018 SUPAMONKS_STUDIO                                      //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
//                                                                           //
// ========================================================================= //
#pragma once

#include <gtest/gtest.h>

#include <smks/scn/Scene.hpp>
#include <smks/scn/TreeNode.hpp>
#include <smks/scn/AbstractParmObserver.hpp>

#include "smks/scn/test/TestNode.hpp"

TEST(ChainedParmEventTest, ParmHandlerChangesAnotherParm)
{
    using namespace smks;

    scn::test::EventHistory     evts;
    scn::Scene::Ptr             scene   = scn::Scene::create();
    scn::test::TestNode::Ptr    node    = scn::test::TestNode::create("_my_node_", scene);

    ///////////////////////////////////
    // class HandlerChangesAnotherParm
    ///////////////////////////////////
    class MyNodeObserver:
        public scn::AbstractParmObserver
    {
    public:
        typedef std::shared_ptr<MyNodeObserver> Ptr;
    private:
        const float _base;
    public:
        static
        Ptr
        create(float base)
        {
            Ptr ptr(new MyNodeObserver(base));
            return ptr;
        }
    protected:
        explicit
        MyNodeObserver(float base):
            _base(base)
        { }
    public:
        void
        handle(scn::TreeNode& node, const xchg::ParmEvent& evt)
        {
            float value_in_node = 0.0f;
            node.getParameter<float>("my_other_parm", value_in_node, -1.0f);

            std::cout
                << "handler (" << (void*)(this) << "): "
                << "'my_other_parm' (value_in_node) = " << value_in_node
                << std::endl;

            // set another parameter to the calling node when 'my_parm' is encountered
            if (evt.parm() == util::string::getId("my_parm"))
            {
                std::cout
                    << "handler (" << (void*)(this) << ") "
                    << "will set 'my_other_parm' to " << _base
                    << std::endl;

                node.setParameter<float>("my_other_parm", _base);
            }
        }
    };
    ///////////////////////////////////

    //---
    typedef std::vector<scn::AbstractParmObserver::Ptr> NodeObservers;

    NodeObservers observers;

    observers.push_back(MyNodeObserver::create(16.0f));
    observers.push_back(MyNodeObserver::create(32.0f));
    for (size_t i = 0; i < observers.size(); ++i)
        node->registerObserver(observers[i]);

    std::cout
        << "####\nmy_parm = " << util::string::getId("my_parm")
        << std::endl;

    node->flushHistory();
    node->setParameter<unsigned int>("my_parm", 42);
    node->flushHistory(&evts);
    std::cout << evts << std::endl;
    for (size_t i = 0; i < observers.size(); ++i)
        node->deregisterObserver(observers[i]);
    //---
    scn::Scene::destroy(scene);
}
