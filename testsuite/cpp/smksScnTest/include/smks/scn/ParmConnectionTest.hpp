// ========================================================================= //
// Copyright 2016-2018 SUPAMONKS_STUDIO                                      //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
//                                                                           //
// ========================================================================= //
#pragma once

#include <gtest/gtest.h>

#include <smks/parm/builtins.hpp>

#include <smks/scn/Scene.hpp>
#include <smks/scn/ParmConnection.hpp>

// - self connection detection
// - parameter connection cycle detection

TEST(ParmConnectionTest, PostConnection)
{
    smks::scn::Scene::Ptr scene = smks::scn::Scene::create();
    smks::scn::TreeNode::Ptr node1 = smks::scn::TreeNode::create("node1", scene);
    smks::scn::TreeNode::Ptr node2 = smks::scn::TreeNode::create("node2", scene);

    std::string parmName1 = "parm1";
    std::string parmName2 = "parm2";

    unsigned int cnxId = scene->connect<int>(node1->id(), parmName1.c_str(), node2->id(), parmName2.c_str());

    EXPECT_TRUE(scene->hasConnection(cnxId));

    smks::scn::ParmConnectionBase::Ptr cnx = scene->connectionById(cnxId);

    EXPECT_TRUE(cnx->established());

    int parmValue = 42;

    unsigned int parmId1 = node1->setParameter<int>(parmName1.c_str(), parmValue);

    EXPECT_TRUE(node1->parameterExistsAs<int>(parmName1.c_str()));
    EXPECT_TRUE(node2->parameterExistsAs<int>(parmName2.c_str()));

    int ival = 0;

    node1->getParameter<int>(parmName1.c_str(), ival);
    EXPECT_EQ(ival, parmValue);
    node2->getParameter<int>(parmName2.c_str(), ival);
    EXPECT_EQ(ival, parmValue);

    parmValue = -24;
    node1->updateParameter<int>(parmId1, parmValue);

    EXPECT_TRUE(node1->parameterExistsAs<int>(parmName1.c_str()));
    EXPECT_TRUE(node2->parameterExistsAs<int>(parmName2.c_str()));

    node1->getParameter<int>(parmName1.c_str(), ival);
    EXPECT_EQ(ival, parmValue);
    node2->getParameter<int>(parmName2.c_str(), ival);
    EXPECT_EQ(ival, parmValue);

    node1->unsetParameter(parmId1);

    EXPECT_FALSE(node1->parameterExistsAs<int>(parmName1.c_str()));
    EXPECT_FALSE(node2->parameterExistsAs<int>(parmName2.c_str()));

    smks::scn::Scene::destroy(scene);
}

TEST(ParmConnectionTest, PreConnection)
{
    smks::scn::Scene::Ptr scene = smks::scn::Scene::create();

    char nodeName1[] = "/node1";
    char nodeName2[] = "/node2";

    unsigned int nodeId1 = smks::util::string::getId(nodeName1);
    unsigned int nodeId2 = smks::util::string::getId(nodeName2);
    std::string parmName1 = "parm1";
    std::string parmName2 = "parm2";

    unsigned int cnxId = scene->connect<int>(nodeId1, parmName1.c_str(), nodeId2, parmName2.c_str());

    EXPECT_TRUE(scene->hasConnection(cnxId));

    smks::scn::ParmConnectionBase::Ptr cnx = scene->connectionById(cnxId);

    EXPECT_FALSE(cnx->established());

    smks::scn::TreeNode::Ptr node1 = smks::scn::TreeNode::create("node1", scene);
    smks::scn::TreeNode::Ptr node2 = smks::scn::TreeNode::create("node2", scene);

    EXPECT_TRUE(cnx->established());

    ASSERT_EQ(node1->id(), nodeId1);
    ASSERT_EQ(node2->id(), nodeId2);

    int parmValue = 42;
    int ival = 0;

    unsigned int parmId1 = node1->setParameter<int>(parmName1.c_str(), parmValue);

    EXPECT_TRUE(node1->parameterExistsAs<int>(parmName1.c_str()));
    EXPECT_TRUE(node2->parameterExistsAs<int>(parmName2.c_str()));

    node1->getParameter<int>(parmName1.c_str(), ival);
    EXPECT_EQ(ival, parmValue);
    node2->getParameter<int>(parmName2.c_str(), ival);
    EXPECT_EQ(ival, parmValue);

    parmValue = -24;
    node1->updateParameter<int>(parmId1, parmValue);

    EXPECT_TRUE(node1->parameterExistsAs<int>(parmName1.c_str()));
    EXPECT_TRUE(node2->parameterExistsAs<int>(parmName2.c_str()));

    node1->getParameter<int>(parmName1.c_str(), ival);
    EXPECT_EQ(ival, parmValue);
    node2->getParameter<int>(parmName2.c_str(), ival);
    EXPECT_EQ(ival, parmValue);

    node1->unsetParameter(parmId1);

    EXPECT_FALSE(node1->parameterExistsAs<int>(parmName1.c_str()));
    EXPECT_FALSE(node2->parameterExistsAs<int>(parmName2.c_str()));

    smks::scn::Scene::destroy(scene);
}

TEST(ParmConnectionTest, Priority)
{
    smks::scn::Scene::Ptr       scene   = smks::scn::Scene::create();
    smks::scn::TreeNode::Ptr    inNode1 = smks::scn::TreeNode::create("inNode1", scene);
    smks::scn::TreeNode::Ptr    inNode2 = smks::scn::TreeNode::create("inNode2", scene);
    smks::scn::TreeNode::Ptr    outNode = smks::scn::TreeNode::create("outNode", scene);

    std::string inParmName1 = "inParmName1";
    std::string inParmName2 = "inParmName2";
    std::string outParmName = "outParmName";

    unsigned int cnxId1 = scene->connect<float>(inNode1->id(), inParmName1.c_str(), outNode->id(), outParmName.c_str());
    unsigned int cnxId2 = scene->connect<float>(inNode2->id(), inParmName2.c_str(), outNode->id(), outParmName.c_str());

    EXPECT_TRUE(scene->hasConnection(cnxId1));
    EXPECT_TRUE(scene->hasConnection(cnxId2));

    smks::scn::ParmConnectionBase::Ptr cnx1 = scene->connectionById(cnxId1);
    smks::scn::ParmConnectionBase::Ptr cnx2 = scene->connectionById(cnxId2);

    cnx1->priority(100);
    cnx2->priority(1);

    float parmValue1 = 11.0f;
    float parmValue2 = 22.0f;

    inNode1->setParameter<float>(inParmName1.c_str(), parmValue1);
    inNode2->setParameter<float>(inParmName2.c_str(), parmValue2);

    float fval = 0.0f;

    outNode->getParameter<float>(outParmName.c_str(), fval);
    EXPECT_FLOAT_EQ(fval, parmValue1);

    cnx2->priority(200);

    outNode->getParameter<float>(outParmName.c_str(), fval);
    EXPECT_FLOAT_EQ(fval, parmValue2);

    parmValue1 = 33.0f;
    inNode1->setParameter<float>(inParmName1.c_str(), parmValue1);

    outNode->getParameter<float>(outParmName.c_str(), fval);
    EXPECT_FLOAT_EQ(fval, parmValue2);

    smks::scn::Scene::destroy(scene);
}

TEST(ParmConnectionTest, ConnectionChain)
{
    static const size_t N = 4;

    smks::scn::Scene::Ptr       scene = smks::scn::Scene::create();
    smks::scn::TreeNode::Ptr    nodes[N];
    std::string                 parmNames[N];
    assert(N > 1);
    unsigned int                cnxId[N-1];

    // create node sequence
    for (size_t n = 0; n < N; ++n)
    {
        const std::string name = "node_" + std::to_string(n);
        nodes[n] = smks::scn::TreeNode::create(name.c_str(), scene);
        parmNames[n] = "parm_" + std::to_string(n);
    }

    // create parameter connection chain
    int priority = 1;
    for (size_t n = 0; n < N-1; ++n)
        cnxId[n] = scene->connect<int>(nodes[n]->id(), parmNames[n].c_str(), nodes[n+1]->id(), parmNames[n+1].c_str(), priority);

    for (size_t n = 0; n < N-1; ++n)
        EXPECT_TRUE(scene->hasConnection(cnxId[n]));

    // set parameter to the fist node
    const int value = 42;
    nodes[0]->setParameter<int>(parmNames[0].c_str(), value);

    // check that the parameter value has been properly propagated
    for (size_t n = 0; n < N; ++n)
    {
        int ival = 0;

        EXPECT_TRUE(nodes[n]->parameterExistsAs<int>(parmNames[n].c_str()));
        nodes[n]->getParameter<int>(parmNames[n].c_str(), ival);
        EXPECT_EQ(ival, value);
    }

    smks::scn::Scene::destroy(scene);
}

TEST(ParmConnectionTest, SelfConnectPrevention)
{
    smks::scn::Scene::Ptr       scene       = smks::scn::Scene::create();
    smks::scn::TreeNode::Ptr    node        = smks::scn::TreeNode::create("node", scene);
    std::string                 parmName    = "my_parmName";

    EXPECT_TRUE(scene->hasDescendant(node->id()));
    unsigned int cnxId = scene->connect<float>(node->id(), parmName.c_str(), node->id(), parmName.c_str());
    EXPECT_EQ(cnxId, 0);
    EXPECT_EQ(scene->numConnections(), 0);

    smks::scn::Scene::destroy(scene);
}

TEST(ParmConnectionTest, CyclePrevention)
{
    static const size_t N = 4;

    smks::scn::Scene::Ptr       scene   = smks::scn::Scene::create();
    smks::scn::TreeNode::Ptr    nodes[N];

    for (size_t n = 0; n < N; ++n)
    {
        const std::string name = "node_" + std::to_string(n);
        nodes[n] = smks::scn::TreeNode::create(name.c_str(), scene);
        EXPECT_TRUE(scene->hasDescendant(nodes[n]->id()));
    }

    for (size_t n = 0; n < N; ++n)
    {
        size_t m = (n + 1) % N;
        const std::string parmName_n = "parm_" + std::to_string(n);
        const std::string parmName_m = "parm_" + std::to_string(m);

        unsigned int cnxId = scene->connect<float>(nodes[n]->id(), parmName_n.c_str(), nodes[m]->id(), parmName_m.c_str());

        if (n == N - 1)
            EXPECT_EQ(cnxId, 0);
        else
            EXPECT_TRUE(cnxId > 0);
    }
    EXPECT_EQ(scene->numConnections(), N-1);

    smks::scn::Scene::destroy(scene);
}

TEST(ParmConnectionTest, UserOverwrite)
{
    static const size_t N = 2;

    smks::scn::Scene::Ptr       scene       = smks::scn::Scene::create();
    smks::scn::TreeNode::Ptr    source      = smks::scn::TreeNode::create("source", scene);
    smks::scn::TreeNode::Ptr    target      = smks::scn::TreeNode::create("target", scene);
    const std::string           parmName    = "my_parmName";

    // directly set value to target
    target->setParameter<double>(parmName.c_str(), 24.0);

    double dval = 0.0;

    target->getParameter<double>(parmName.c_str(), dval);
    EXPECT_DOUBLE_EQ(dval, 24.0);

    // connected value takes precedence now
    source->setParameter<double>(parmName.c_str(), 42.0);

    const unsigned int cnxId = scene->connect<double>(source->id(), parmName.c_str(), target->id(), INT_MAX);

    EXPECT_TRUE(scene->hasConnection(cnxId));
    target->getParameter<double>(parmName.c_str(), dval);
    EXPECT_DOUBLE_EQ(dval, 42.0);

    // again, set parameter value directly to target. connection should die.
    target->setParameter<double>(parmName.c_str(), 100.0);

    EXPECT_FALSE(scene->hasConnection(cnxId));
    scene->disconnect(cnxId);   // should do nothing
    target->getParameter<double>(parmName.c_str(), dval);
    EXPECT_DOUBLE_EQ(dval, 100.0);

    smks::scn::Scene::destroy(scene);
}

TEST(ParmConnectionTest, UserMultiPriorityOverwrite)
{
    static const size_t N = 4;  // number of source nodes

    smks::scn::Scene::Ptr       scene       = smks::scn::Scene::create();
    smks::scn::TreeNode::Ptr    target      = smks::scn::TreeNode::create("target", scene);
    smks::scn::TreeNode::Ptr    source[N];

    for (size_t n = 0; n < N; ++n)
    {
        const std::string name = "source_" + std::to_string(n);
        source[n] = smks::scn::TreeNode::create(name.c_str(), scene);
    }

    const std::string parmName = "my_parmName";

    // connected all source nodes to target
    unsigned int cnxId[N];
    for (size_t n = 0; n < N; ++n)
        cnxId[n] = scene->connect<float>(source[n]->id(), parmName.c_str(), target->id(), static_cast<int>(10 * (N - n)));

    for (size_t n = 0; n < N; ++n)
        EXPECT_TRUE(scene->hasConnection(cnxId[n]));

    // set values on source nodes
    for (size_t n = 0; n < N; ++n)
        source[n]->setParameter<float>(parmName.c_str(), static_cast<float>(n));

    float fval = 0.0f;

    EXPECT_TRUE(target->parameterExistsAs<float>(parmName.c_str()));
    target->getParameter<float>(parmName.c_str(), fval);
    EXPECT_FLOAT_EQ(fval, 0.0f);

    // break the connection set with a user-defined value
    target->setParameter<float>(parmName.c_str(), -1.0f);

    for (size_t n = 0; n < N; ++n)
        EXPECT_FALSE(scene->hasConnection(cnxId[n]));


    smks::scn::Scene::destroy(scene);
}

TEST(ParmConnectionTest, ConnectionChainBreak)
{
    static const size_t N = 3;

    smks::scn::Scene::Ptr       scene       = smks::scn::Scene::create();
    smks::scn::TreeNode::Ptr    nodes[N];

    for (size_t i = 0; i < N; ++i)
    {
        const std::string name = "node_" + std::to_string(i);
        nodes[i] = smks::scn::TreeNode::create(name.c_str(), scene);
    }

    for (size_t i = 0; i < N; ++i)
        EXPECT_TRUE(scene->hasDescendant(nodes[i]->id()));

    // create a connection chain
    const std::string   parmName = "my_parmName";
    unsigned int        cnxId[N-1];
    for (size_t i = 0; i < N-1; ++i)
        cnxId[i] = scene->connect<int>(nodes[i]->id(), parmName.c_str(), nodes[i+1]->id());

    for (size_t i = 0; i < N-1; ++i)
        EXPECT_TRUE(scene->hasConnection(cnxId[i]));

    int ival = 0;

    // set parameter at the beginning of the chain
    int valueFromChain = 42;
    nodes[0]->setParameter<int>(parmName.c_str(), valueFromChain);
    EXPECT_TRUE(nodes[N-1]->parameterExistsAs<int>(parmName.c_str()));
    nodes[N-1]->getParameter<int>(parmName.c_str(), ival);
    EXPECT_EQ(ival, valueFromChain);

    // break the chain by having the user explicitly set value on 2nd node
    int valueFromUser = 21;
    nodes[1]->setParameter<int>(parmName.c_str(), valueFromUser);

    EXPECT_FALSE(scene->hasConnection(cnxId[0]));
    for (size_t i = 1; i < N-1; ++i)
        EXPECT_TRUE(scene->hasConnection(cnxId[i]));

    EXPECT_TRUE(nodes[N-1]->parameterExistsAs<int>(parmName.c_str()));
    nodes[N-1]->getParameter<int>(parmName.c_str(), ival);
    EXPECT_EQ(ival, valueFromUser);

    // restore broken chain
    cnxId[0] = scene->connect<int>(nodes[0]->id(), parmName.c_str(), nodes[1]->id());

    for (size_t i = 0; i < N-1; ++i)
        EXPECT_TRUE(scene->hasConnection(cnxId[i]));

    EXPECT_TRUE(nodes[N-1]->parameterExistsAs<int>(parmName.c_str()));
    nodes[N-1]->getParameter<int>(parmName.c_str(), ival);
    EXPECT_EQ(ival, valueFromChain);

    smks::scn::Scene::destroy(scene);
}

TEST(ParmConnectionTest, BasicConnectionBehavior)
{
    using namespace smks;

    scn::Scene::Ptr     scene   = scn::Scene::create();
    scn::TreeNode::Ptr  master  = scn::TreeNode::create("_master_", scene);
    scn::TreeNode::Ptr  slave   = scn::TreeNode::create("_slave_", scene);

    const char* masterParmName  = "master_parm";
    const char* slaveParmName   = "slave_parm";
    const unsigned int masterParm   = util::string::getId(masterParmName);
    const unsigned int slaveParm    = util::string::getId(slaveParmName);

    std::string parmValue;

    //---
    const unsigned int cnx = scene->connect<std::string>(master->id(), masterParmName, slave->id(), slaveParmName);
    //---
    EXPECT_TRUE(scene->hasConnection(cnx));
    EXPECT_FALSE(slave->parameterExists(slaveParmName));

    master->setParameter<std::string>(masterParmName, "foo");

    EXPECT_TRUE(slave->parameterExistsAs<std::string>(slaveParmName));
    slave->getParameter<std::string>(slaveParm, parmValue);
    EXPECT_STREQ(parmValue.c_str(), "foo");
    //---
    scn::TreeNode::destroy(slave);
    // connection is expected to survive even though only one node remains
    //---
    scn::Scene::destroy(scene);
}

TEST(ParmConnectionTest, ConnectionSurvivesSlaveDeletion)
{
    using namespace smks;

    scn::Scene::Ptr     scene   = scn::Scene::create();
    scn::TreeNode::Ptr  master  = scn::TreeNode::create("_master_", scene);
    scn::TreeNode::Ptr  slave   = scn::TreeNode::create("_slave_", scene);

    const char* masterParmName  = "master_parm";
    const char* slaveParmName   = "slave_parm";
    const unsigned int masterParm   = util::string::getId(masterParmName);
    const unsigned int slaveParm    = util::string::getId(slaveParmName);

    std::string parmValue;

    //---
    const unsigned int cnx = scene->connect<std::string>(master->id(), masterParmName, slave->id(), slaveParmName);
    //---
    EXPECT_TRUE(scene->hasConnection(cnx));
    //---
    scn::TreeNode::destroy(slave);
    // connection is expected to survive even though only one node remains ...
    //---
    EXPECT_TRUE(scene->hasConnection(cnx));
    //---
    // ... and to be functional again once node is recreated.
    scn::TreeNode::Ptr new_slave = scn::TreeNode::create("_slave_", scene);
    EXPECT_FALSE(new_slave->parameterExists(slaveParmName));

    master->setParameter<std::string>(masterParmName, "foo");

    EXPECT_TRUE(new_slave->parameterExistsAs<std::string>(slaveParmName));
    new_slave->getParameter<std::string>(slaveParm, parmValue);
    EXPECT_STREQ(parmValue.c_str(), "foo");

    scn::Scene::destroy(scene);
}

TEST(ParmConnectionTest, ConnectionSurvivesMasterDeletion)
{
    using namespace smks;

    scn::Scene::Ptr     scene   = scn::Scene::create();
    scn::TreeNode::Ptr  master  = scn::TreeNode::create("_master_", scene);
    scn::TreeNode::Ptr  slave   = scn::TreeNode::create("_slave_", scene);

    const char* masterParmName  = "master_parm";
    const char* slaveParmName   = "slave_parm";
    const unsigned int masterParm   = util::string::getId(masterParmName);
    const unsigned int slaveParm    = util::string::getId(slaveParmName);

    std::string parmValue;

    //---
    const unsigned int cnx = scene->connect<std::string>(master->id(), masterParmName, slave->id(), slaveParmName);
    //---
    EXPECT_TRUE(scene->hasConnection(cnx));
    //---
    scn::TreeNode::destroy(master);
    // connection is expected to survive even though only one node remains ...
    //---
    EXPECT_TRUE(scene->hasConnection(cnx));
    //---
    // ... and to be functional again once node is recreated.
    scn::TreeNode::Ptr new_master = scn::TreeNode::create("_master_", scene);
    EXPECT_FALSE(slave->parameterExists(slaveParmName));

    new_master->setParameter<std::string>(masterParmName, "foo");

    EXPECT_TRUE(slave->parameterExistsAs<std::string>(slaveParmName));
    slave->getParameter<std::string>(slaveParm, parmValue);
    EXPECT_STREQ(parmValue.c_str(), "foo");

    scn::Scene::destroy(scene);
}

TEST(ParmConnectionTest, ConnectionSurvivesDeletionOfBothEnds)
{
    using namespace smks;

    scn::Scene::Ptr     scene   = scn::Scene::create();
    scn::TreeNode::Ptr  master  = scn::TreeNode::create("_master_", scene);
    scn::TreeNode::Ptr  slave   = scn::TreeNode::create("_slave_", scene);

    const char* masterParmName  = "master_parm";
    const char* slaveParmName   = "slave_parm";
    const unsigned int masterParm   = util::string::getId(masterParmName);
    const unsigned int slaveParm    = util::string::getId(slaveParmName);

    std::string parmValue;

    //---
    const unsigned int cnx = scene->connect<std::string>(master->id(), masterParmName, slave->id(), slaveParmName);
    //---
    EXPECT_TRUE(scene->hasConnection(cnx));
    //---
    scn::TreeNode::destroy(master);
    scn::TreeNode::destroy(slave);
    // connection is expected to survive even though only one node remains ...
    //---
    EXPECT_TRUE(scene->hasConnection(cnx));
    //---
    // ... and to be functional again once node is recreated.
    scn::TreeNode::Ptr new_master = scn::TreeNode::create("_master_", scene);
    scn::TreeNode::Ptr new_slave = scn::TreeNode::create("_slave_", scene);
    EXPECT_FALSE(new_slave->parameterExists(slaveParmName));

    new_master->setParameter<std::string>(masterParmName, "foo");

    EXPECT_TRUE(new_slave->parameterExistsAs<std::string>(slaveParmName));
    new_slave->getParameter<std::string>(slaveParm, parmValue);
    EXPECT_STREQ(parmValue.c_str(), "foo");

    scn::Scene::destroy(scene);
}
