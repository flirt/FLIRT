// ========================================================================= //
// Copyright 2016-2018 SUPAMONKS_STUDIO                                      //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
//                                                                           //
// ========================================================================= //
#pragma once

#include <gtest/gtest.h>

#include <smks/scn/Scene.hpp>
#include <smks/scn/Xform.hpp>

#include "smks/scn/test/util.hpp"
#include <smks/math/matrixAlgo.hpp>

TEST(XformTest, CustomXformRegistration)
{
    using namespace smks::scn;

    Scene::Ptr scene = Scene::create();
    Xform::Ptr xform = Xform::create("my_xform", scene);

    EXPECT_TRUE(scene->hasDescendant(xform->id()));

    Scene::destroy(scene);
}

TEST(XformTest, CustomXformDefaultValue)
{
    using namespace smks::scn;

    Scene::Ptr scene = Scene::create();
    Xform::Ptr xform = Xform::create("my_xform", scene);

    smks::math::Affine3f xfm;

    const bool status = xform->getParameter<smks::math::Affine3f>(smks::parm::transform(), xfm);
    EXPECT_FALSE(status);
    EXPECT_TRUE(smks::xchg::equal(xfm, smks::math::Affine3f::Identity()));

    Scene::destroy(scene);
}

TEST(XformTest, TranslateRotateScale)
{
    using namespace smks::scn;

    Scene::Ptr scene = Scene::create();
    Xform::Ptr xform = Xform::create("my_xform", scene);

    smks::math::Vector4f    translate   (1.25f, 2.25f, 3.25f, 1.0f);
    smks::math::Vector4f    rotateD     (12.0f, 53.0f, 64.0f, 0.0f);
    smks::math::Vector4f    scale       (3.5f, 2.5f, 1.5f, 1.0f);

    smks::math::Affine3f    xfm;
    smks::math::Affine3f    mayaMatrix;
    bool                    status;

    xform->setParameter<smks::math::Vector4f>(smks::parm::translate (), translate);
    xform->setParameter<smks::math::Vector4f>(smks::parm::scale     (), scale);

    //---
    char order = static_cast<char>(smks::math::RotateOrder::XYZ);

    xform->setParameter<char>                   (smks::parm::rotateOrder(), order);
    xform->setParameter<smks::math::Vector4f>   (smks::parm::rotateD    (), rotateD);   // must reset angle values (will change to minimize rotations)
    //---
    status = xform->getParameter<smks::math::Affine3f>(smks::parm::transform(), xfm);
    mayaMatrix.matrix().col(0) << 0.9233641964892052f, 1.8931771592385975f, -2.795224285165525f, 0.0f;
    mayaMatrix.matrix().col(1) << -2.015909034701295f, 1.4450813385058163f, 0.3128109475576785f, 0.0f;
    mayaMatrix.matrix().col(2) << 0.7939770844297613f, 0.9164707647834902f, 0.8829958814726035f, 0.0f;
    mayaMatrix.matrix().col(3) << 1.25f, 2.25f, 3.25f, 1.0f;

    EXPECT_TRUE(status);
    EXPECT_TRUE(smks::xchg::equal(xfm, mayaMatrix));
    //---
    order = static_cast<char>(smks::math::RotateOrder::YZX);

    xform->setParameter<char>                   (smks::parm::rotateOrder(), order);
    xform->setParameter<smks::math::Vector4f>   (smks::parm::rotateD    (), rotateD);   // must reset angle values (will change to minimize rotations)
    //---
    //****
    smks::math::Affine3f testXfm;
    smks::math::composeEuler(testXfm, translate, static_cast<smks::math::RotateOrder>(order), rotateD, scale);
    //****
    status = xform->getParameter<smks::math::Affine3f>(smks::parm::transform(), xfm);
    mayaMatrix.matrix().col(0) << 0.9233641964892052f, 2.432966503416903f, -2.340528263852666f, 0.0f;
    mayaMatrix.matrix().col(1) << -2.2469851157479175f, 1.0719792136566575f, 0.22785621583659305f, 0.0f;
    mayaMatrix.matrix().col(2) << 0.5251481466088673f, 0.8654979257508392f, 1.106857156382099f, 0.0f;
    mayaMatrix.matrix().col(3) << 1.25f, 2.25f, 3.25f, 1.0f;

    EXPECT_TRUE(status);
    //****
    EXPECT_TRUE(smks::xchg::equal(testXfm, mayaMatrix));
    //****
    EXPECT_TRUE(smks::xchg::equal(xfm, mayaMatrix));
    //---
    order = static_cast<char>(smks::math::RotateOrder::ZXY);

    xform->setParameter<char>                   (smks::parm::rotateOrder(), order);
    xform->setParameter<smks::math::Vector4f>   (smks::parm::rotateD    (), rotateD);   // must reset angle values (will change to minimize rotations)
    //---
    status = xform->getParameter<smks::math::Affine3f>(smks::parm::transform(), xfm);
    mayaMatrix.matrix().col(0) << 1.4457071712780276f, 3.0770363397947573f, -0.8317320112258313f, 0.0f;
    mayaMatrix.matrix().col(1) << -1.1702953343040376f, 1.0719792136566575f, 1.9316493977930511f, 0.0f;
    mayaMatrix.matrix().col(2) << 1.1717751120203679f, -0.311867536226639f, 0.8829958814726035f, 0.0f;
    mayaMatrix.matrix().col(3) << 1.25f, 2.25f, 3.25f, 1.0f;

    EXPECT_TRUE(status);
    EXPECT_TRUE(smks::xchg::equal(xfm, mayaMatrix));
    //---
    order = static_cast<char>(smks::math::RotateOrder::XZY);

    xform->setParameter<char>                   (smks::parm::rotateOrder(), order);
    xform->setParameter<smks::math::Vector4f>   (smks::parm::rotateD    (), rotateD);   // must reset angle values (will change to minimize rotations)
    //---
    status = xform->getParameter<smks::math::Affine3f>(smks::parm::transform(), xfm);
    mayaMatrix.matrix().col(0) << 0.9233641964892052f, 3.1457791620470847f, -1.2253456754206904f, 0.0f;
    mayaMatrix.matrix().col(1) << -0.9076049205211778f, 1.0719792136566575f, 2.068118438033422f, 0.0f;
    mayaMatrix.matrix().col(2) << 1.3404666823895934f, -0.13671372950195596f, 0.6591346065631082f, 0.0f;
    mayaMatrix.matrix().col(3) << 1.25f, 2.25f, 3.25f, 1.0f;

    EXPECT_TRUE(status);
    EXPECT_TRUE(smks::xchg::equal(xfm, mayaMatrix));
    //---
    order = static_cast<char>(smks::math::RotateOrder::YXZ);

    xform->setParameter<char>                   (smks::parm::rotateOrder(), order);
    xform->setParameter<smks::math::Vector4f>   (smks::parm::rotateD    (), rotateD);   // must reset angle values (will change to minimize rotations)
    //---
    status = xform->getParameter<smks::math::Affine3f>(smks::parm::transform(), xfm);
    mayaMatrix.matrix().col(0) << 0.40102122170038224f, 2.1479408504515423f, -2.734141928047525f, 0.0f;
    mayaMatrix.matrix().col(1) << -2.1978830998533985f, 1.0719792136566577f, 0.5197792270443984f, 0.0f;
    mayaMatrix.matrix().col(2) << 0.6938397169780928f, 0.9944368861049855f, 0.8829958814726035f, 0.0f;
    mayaMatrix.matrix().col(3) << 1.25f, 2.25f, 3.25f, 1.0f;

    EXPECT_TRUE(status);
    EXPECT_TRUE(smks::xchg::equal(xfm, mayaMatrix));
    //---
    order = static_cast<char>(smks::math::RotateOrder::ZYX);

    xform->setParameter<char>                   (smks::parm::rotateOrder(), order);
    xform->setParameter<smks::math::Vector4f>   (smks::parm::rotateD    (), rotateD);   // must reset angle values (will change to minimize rotations)
    //---
    status = xform->getParameter<smks::math::Affine3f>(smks::parm::transform(), xfm);
    mayaMatrix.matrix().col(0) << 0.9233641964892052f, 3.3318000310077025f, -0.5445246679618095f, 0.0f;
    mayaMatrix.matrix().col(1) << -1.352269399456141f, 0.6988770888074985f, 1.9831637063123368f, 0.0f;
    mayaMatrix.matrix().col(2) << 1.1979532650709392f, -0.18768656853460713f, 0.8829958814726035f, 0.0f;
    mayaMatrix.matrix().col(3) << 1.25f, 2.25f, 3.25f, 1.0f;

    EXPECT_TRUE(status);
    EXPECT_TRUE(smks::xchg::equal(xfm, mayaMatrix));
    //---

    Scene::destroy(scene);
}

TEST(XformTest, LookAtPoint)
{
    using namespace smks::scn;

    Scene::Ptr scene = Scene::create();
    Xform::Ptr xform = Xform::create("my_xform", scene);

    smks::math::Vector4f    eye     (1.5f, 2.5f, 3.5f, 1.0f);
    smks::math::Vector4f    target  (0.5f, 1.5f, -6.5f, 1.0f);
    smks::math::Vector4f    up      (0.3f, 2.0f, -0.25f, 0.0f);

    smks::math::Affine3f    xfm;
    smks::math::Affine3f    mayaMatrix;
    bool                    status;

    //---
    xform->setParameter<smks::math::Vector4f>(smks::parm::center    (), eye);
    xform->setParameter<smks::math::Vector4f>(smks::parm::front(), target);
    xform->setParameter<smks::math::Vector4f>(smks::parm::upVector      (), up);
    //---
    status = xform->getParameter<smks::math::Affine3f>(smks::parm::transform(), xfm);
    mayaMatrix.matrix().col(0) << 0.983989873891948f,   -0.1579243007480904f,   -0.08260655731438532f,  0.0f;
    mayaMatrix.matrix().col(1) << 0.1481890903861711f,  0.9824744239239f,       -0.11306635143100666f,  0.0f;
    mayaMatrix.matrix().col(2) << 0.09901475429766692f, 0.09901475429766704f,   0.9901475429766744f,    0.0f;
    mayaMatrix.matrix().col(3) << 1.5f, 2.5f, 3.5f, 1.0f;

    EXPECT_TRUE(status);
    EXPECT_TRUE(smks::xchg::equal(xfm, mayaMatrix));

    Scene::destroy(scene);
}

TEST(XformTest, XformParmSynchro)
{
    using namespace smks;

    scn::Scene::Ptr scene = scn::Scene::create();
    scn::Xform::Ptr xform = scn::Xform::create("my_xform", scene);

    math::Vector4f      ref_translate, ref_rotateD, ref_scale;
    math::RotateOrder   order = math::RotateOrder::XYZ;
    math::Affine3f      ref_xfm;

    ref_translate   = math::Vector4f::Random() * 100.0f;
    ref_rotateD     = math::Vector4f::Random() * 180.0f;
    ref_scale       = math::Vector4f::Random() * 2.0f;

    ref_translate[3]    = 1.0f;
    ref_rotateD[3]      = 1.0f;
    ref_scale[3]        = 1.0f;

    math::composeEuler(ref_xfm, ref_translate, order, ref_rotateD, ref_scale);

    //---
    xform->setParameter<math::Affine3f>(parm::transform().c_str(), ref_xfm);
    //---
    EXPECT_TRUE(xform->parameterExistsAs<math::Affine3f>(parm::transform    ().id()));
    EXPECT_TRUE(xform->parameterExistsAs<math::Vector4f>(parm::translate    ().id()));
    EXPECT_TRUE(xform->parameterExistsAs<math::Vector4f>(parm::rotateD      ().id()));
    EXPECT_TRUE(xform->parameterExistsAs<math::Vector4f>(parm::scale        ().id()));
    EXPECT_TRUE(xform->parameterExistsAs<math::Vector4f>(parm::center       ().id()));
    EXPECT_TRUE(xform->parameterExistsAs<math::Vector4f>(parm::front        ().id()));
    EXPECT_TRUE(xform->parameterExistsAs<math::Vector4f>(parm::upVector     ().id()));

    math::Vector4f translate, rotateD, scale, eyePosition, targetPosition, upVector;

    xform->getParameter<math::Vector4f>(parm::translate (), translate);
    xform->getParameter<math::Vector4f>(parm::rotateD   (), rotateD);
    xform->getParameter<math::Vector4f>(parm::scale     (), scale);
    xform->getParameter<math::Vector4f>(parm::center    (), eyePosition);
    xform->getParameter<math::Vector4f>(parm::front     (), targetPosition);
    xform->getParameter<math::Vector4f>(parm::upVector  (), upVector);

    math::Affine3f xfm;

    math::composeEuler(xfm, translate, order, rotateD, scale);
    EXPECT_TRUE(smks::xchg::equal(xfm, ref_xfm));

    math::lookAtPoint(xfm, eyePosition, targetPosition, upVector, scale);
    EXPECT_TRUE(smks::xchg::equal(xfm, ref_xfm));

    //---
    // change orientation via new rotation angles
    math::Vector4f new_rotateD = math::Vector4f::Random() * 180.0f;
    new_rotateD[3] = 1.0f;

    xform->setParameter<math::Vector4f>(parm::rotateD().c_str(), new_rotateD);
    //---
    math::composeEuler(ref_xfm, translate, order, new_rotateD, scale);
    xform->getParameter<math::Affine3f>(parm::transform(), xfm);
    EXPECT_TRUE(smks::xchg::equal(xfm, ref_xfm));

    //---
    // change orientation via new target position
    math::Vector4f new_targetPosition = math::Vector4f::Random() * 100.0f;
    new_targetPosition[3] = 1.0f;

    xform->setParameter<math::Vector4f>(parm::front().c_str(), new_targetPosition);
    //---
    xform->getParameter<math::Vector4f>(parm::center    (), eyePosition);
    xform->getParameter<math::Vector4f>(parm::front     (), targetPosition);
    xform->getParameter<math::Vector4f>(parm::upVector  (), upVector);

    math::lookAtPoint(ref_xfm, eyePosition, targetPosition, upVector, scale);
    xform->getParameter<math::Affine3f>(parm::transform(), xfm);
    EXPECT_TRUE(smks::xchg::equal(xfm, ref_xfm));

    scn::Scene::destroy(scene);
}

TEST(XformTest, NonWritableWorldTransform)
{
    using namespace smks;

    scn::test::EventHistory     evts;
    scn::Scene::Ptr             scene   = scn::Scene::create();
    scn::Xform::Ptr             xform   = scn::Xform::create("my_xform", scene);
    scn::test::TestNode::Ptr    node    = scn::test::TestNode::create("my_node", xform);

    math::Affine3f xfm;
    math::Affine3f ref_xfm = math::test::getRandomTransform();

    xform->setParameter<math::Affine3f>(parm::transform(), ref_xfm);
    node->getParameter<math::Affine3f>(parm::worldTransform(), xfm);
    EXPECT_TRUE(smks::xchg::equal(xfm, ref_xfm));

    EXPECT_FALSE(node->parameterExists(parm::worldTransform().id()));
    EXPECT_FALSE(node->parameterExistsAs<math::Affine3f>(parm::worldTransform().id()));
    const unsigned int parmId = node->setParameter<math::Affine3f>(parm::worldTransform(), math::test::getRandomTransform());
    EXPECT_EQ(parmId, 0);
    EXPECT_FALSE(node->parameterExists(parm::worldTransform().id()));
    EXPECT_FALSE(node->parameterExistsAs<math::Affine3f>(parm::worldTransform().id()));
    node->updateParameter<math::Affine3f>(parm::worldTransform().id(), math::test::getRandomTransform());
    EXPECT_FALSE(node->parameterExists(parm::worldTransform().id()));
    EXPECT_FALSE(node->parameterExistsAs<math::Affine3f>(parm::worldTransform().id()));

    node->flushHistory();
    xform->setParameter<math::Affine3f>(parm::transform(), math::test::getRandomTransform());
    node->flushHistory(&evts);
    for (size_t i = 0; i < evts.size(); ++i)
        std::cout << "[" << i << "] : " << parm::test::getParameterName(evts[i].asParmEvent()->parm()) << std::endl;
    std::cout << evts << std::endl;


    scn::Scene::destroy(scene);
}

TEST(XformTest, InheritsTrfsGetters)
{
    using namespace smks;

    scn::Scene::Ptr scene = scn::Scene::create();
    scene->setSharedResourceDatabase(g_resources);

    scn::Archive::Ptr archive = scn::Archive::create("abc/inherits_xforms.abc", scene);
    scn::test::initializeDescendants(true, *archive);
    // scene
    // - A              - B             - C (*)
    // -- AA            -- BB (*)       -- CC
    // --- objA (*)     --- objB        ---objC
    // ---- objAShape   ---- objBShape  ---- objCShape
    //
    // * does NOT inherit transforms
    const char* names[] = {
        "A", "AA", "objA", "objAShape",
        "B", "BB", "objB", "objBShape",
        "C", "CC", "objC", "objCShape"
    };

    const int num = sizeof(names) / sizeof(const char*);
    for (int i = 0; i < num; ++i)
    {
        const char*         name = names[i];
        scn::TreeNode::Ptr  node = scn::test::getNodeByName(*scene, name, false);
        EXPECT_NE(node, nullptr);

        bool inherits;
        node->getParameter<bool>(parm::inheritsTransforms(), inherits);

        if (strcmp(name, "objA") == 0 ||
            strcmp(name, "BB") == 0 ||
            strcmp(name, "C") == 0)
            EXPECT_FALSE(inherits);
        else
            EXPECT_TRUE(inherits);

        if (strcmp(name, "objAShape") != 0 &&
            strcmp(name, "objBShape") != 0 &&
            strcmp(name, "objCShape") != 0)
            EXPECT_FALSE(node->isParameterWritable(parm::inheritsTransforms().id()));
    }

    scn::Scene::destroy(scene);
}

TEST(XformTest, InheritsTrfsResetWorld)
{
    using namespace smks;

    scn::Scene::Ptr scene = scn::Scene::create();
    scene->setSharedResourceDatabase(g_resources);

    scn::Archive::Ptr archive = scn::Archive::create("abc/inherits_xforms.abc", scene);
    scn::test::initializeDescendants(true, *archive);
    // scene
    // - A              - B             - C (*)
    // -- AA            -- BB (*)       -- CC
    // --- objA (*)     --- objB        ---objC
    // ---- objAShape   ---- objBShape  ---- objCShape
    //
    // * does NOT inherit transforms
    const char* names[] = { "objA", "BB", "C" };
    const int   num     = sizeof(names) / sizeof(const char*);
    for (int i = 0; i < num; ++i)
    {
        const char*         name = names[i];
        scn::TreeNode::Ptr  node = scn::test::getNodeByName(*scene, name, false);
        EXPECT_NE(node, nullptr);

        // world xfm == local xfm even though the parent's world xfm != identity
        math::Affine3f localXfm, worldXfm;
        node->getParameter<math::Affine3f>(parm::transform(), localXfm);
        node->getParameter<math::Affine3f>(parm::worldTransform(), worldXfm);
        EXPECT_TRUE(xchg::equal<math::Affine3f>(localXfm, worldXfm));

        //
        scn::TreeNode::Ptr parent = node->parent().lock();
        EXPECT_NE(parent, nullptr);

        math::Affine3f parentXfm;
        parent->getParameter<math::Affine3f>(parm::worldTransform(), parentXfm);
        EXPECT_FALSE(xchg::equal<math::Affine3f>(parentXfm, math::Affine3f::Identity()));
    }

    scn::Scene::destroy(scene);
}

TEST(XformTest, InheritsTrfsDescWorld)
{
    using namespace smks;

    scn::Scene::Ptr scene = scn::Scene::create();
    scene->setSharedResourceDatabase(g_resources);

    scn::Archive::Ptr archive = scn::Archive::create("abc/inherits_xforms.abc", scene);
    scn::test::initializeDescendants(true, *archive);
    // scene
    // - A              - B             - C (*)
    // -- AA            -- BB (*)       -- CC
    // --- objA (*)     --- objB        ---objC
    // ---- objAShape   ---- objBShape  ---- objCShape
    //
    // * does NOT inherit transforms

    const char* names[] = {
        "A", "AA", "objA", "objAShape",
        "B", "BB", "objB", "objBShape",
        "C", "CC", "objC", "objCShape"
    };

    const math::Affine3f& xfmObjA = scn::test::getNodeParameter<math::Affine3f>(parm::transform(), *scene, "objA", false);
    const math::Affine3f& xfmBB   = scn::test::getNodeParameter<math::Affine3f>(parm::transform(), *scene, "BB", false);
    const math::Affine3f& xfmObjB = scn::test::getNodeParameter<math::Affine3f>(parm::transform(), *scene, "objB", false);
    const math::Affine3f& xfmC    = scn::test::getNodeParameter<math::Affine3f>(parm::transform(), *scene, "C", false);
    const math::Affine3f& xfmCC   = scn::test::getNodeParameter<math::Affine3f>(parm::transform(), *scene, "CC", false);
    const math::Affine3f& xfmObjC = scn::test::getNodeParameter<math::Affine3f>(parm::transform(), *scene, "objC", false);

    math::Affine3f worldXfm, xfm;

    worldXfm    = scn::test::getNodeParameter<math::Affine3f>(parm::worldTransform(), *scene, "objAShape", false);
    xfm         = xfmObjA;
    EXPECT_TRUE(xchg::equal<math::Affine3f>(xfm, worldXfm));

    worldXfm    = scn::test::getNodeParameter<math::Affine3f>(parm::worldTransform(), *scene, "objB", false);
    xfm         = xfmBB * xfmObjB;
    EXPECT_TRUE(xchg::equal<math::Affine3f>(xfm, worldXfm));

    worldXfm    = scn::test::getNodeParameter<math::Affine3f>(parm::worldTransform(), *scene, "objBShape", false);
    xfm         = xfmBB * xfmObjB;
    EXPECT_TRUE(xchg::equal<math::Affine3f>(xfm, worldXfm));

    worldXfm    = scn::test::getNodeParameter<math::Affine3f>(parm::worldTransform(), *scene, "CC", false);
    xfm         = xfmC * xfmCC;
    EXPECT_TRUE(xchg::equal<math::Affine3f>(xfm, worldXfm));

    worldXfm    = scn::test::getNodeParameter<math::Affine3f>(parm::worldTransform(), *scene, "objC", false);
    xfm         = xfmC * xfmCC * xfmObjC;
    EXPECT_TRUE(xchg::equal<math::Affine3f>(xfm, worldXfm));

    worldXfm    = scn::test::getNodeParameter<math::Affine3f>(parm::worldTransform(), *scene, "objCShape", false);
    xfm         = xfmC * xfmCC * xfmObjC;
    EXPECT_TRUE(xchg::equal<math::Affine3f>(xfm, worldXfm));

    scn::Scene::destroy(scene);
}

TEST(XformTest, UsersInheritsTrfsGetters)
{
    using namespace smks;

    scn::Scene::Ptr scene = scn::Scene::create();
    std::vector<scn::TreeNode::Ptr> nodes;
    bool inherits;

    scn::test::createXformBranchBelowNode(scene, 3, nodes);

    for (size_t i = 0; i < nodes.size(); ++i)
    {
        nodes[i]->getParameter<bool>(parm::inheritsTransforms(), inherits);
        EXPECT_TRUE(inherits);
        EXPECT_TRUE(nodes[i]->isParameterWritable(parm::inheritsTransforms().id()));
    }
    scn::TreeNode& node = *nodes[1];
    node.setParameter<bool>(parm::inheritsTransforms(), false);
    node.getParameter<bool>(parm::inheritsTransforms(), inherits);
    EXPECT_FALSE(inherits);
    node.setParameter<bool>(parm::inheritsTransforms(), true);
    node.getParameter<bool>(parm::inheritsTransforms(), inherits);
    EXPECT_TRUE(inherits);

    scn::Scene::destroy(scene);
}

TEST(XformTest, UsersInheritsTrfsResetWorld)
{
    using namespace smks;

    scn::Scene::Ptr scene = scn::Scene::create();
    std::vector<scn::TreeNode::Ptr> nodes;
    math::Affine3f xfm, refWorldXfm, worldXfm;

    scn::test::createXformBranchBelowNode(scene, 3, nodes);

    scn::TreeNode& node = *nodes[1];
    node.getParameter<math::Affine3f>(parm::transform(), xfm);
    node.getParameter<math::Affine3f>(parm::worldTransform(), refWorldXfm);
    EXPECT_FALSE(xchg::equal<math::Affine3f>(xfm, refWorldXfm));
    //---
    node.setParameter<bool>(parm::inheritsTransforms(), false);
    //---
    node.getParameter<math::Affine3f>(parm::worldTransform(), worldXfm);
    EXPECT_TRUE(xchg::equal<math::Affine3f>(xfm, worldXfm));
    //---
    node.setParameter<bool>(parm::inheritsTransforms(), true);
    //---
    node.getParameter<math::Affine3f>(parm::worldTransform(), worldXfm);
    EXPECT_TRUE(xchg::equal<math::Affine3f>(refWorldXfm, worldXfm));

    scn::Scene::destroy(scene);
}

TEST(XformTest, UsersInheritsTrfsDescWorld)
{
    using namespace smks;

    scn::Scene::Ptr scene = scn::Scene::create();
    std::vector<scn::TreeNode::Ptr> nodes;
    math::Affine3f xfm_1, xfm_2, worldXfm_2;

    scn::test::createXformBranchBelowNode(scene, 3, nodes);

    //---
    nodes[1]->setParameter<bool>(parm::inheritsTransforms(), false);
    //---
    nodes[1]->getParameter<math::Affine3f>(parm::transform(), xfm_1);
    nodes[2]->getParameter<math::Affine3f>(parm::transform(), xfm_2);
    nodes[2]->getParameter<math::Affine3f>(parm::worldTransform(), worldXfm_2);

    EXPECT_TRUE(xchg::equal<math::Affine3f>(worldXfm_2, xfm_1 * xfm_2));

    scn::Scene::destroy(scene);
}
