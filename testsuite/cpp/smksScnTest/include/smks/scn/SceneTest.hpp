// ========================================================================= //
// Copyright 2016-2018 SUPAMONKS_STUDIO                                      //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
//                                                                           //
// ========================================================================= //
#pragma once

#include <gtest/gtest.h>

#include <boost/unordered_map.hpp>

#include "smks/scn/test/util.hpp"
#include <smks/scn/Material.hpp>
#include <smks/scn/SubSurface.hpp>
#include <smks/scn/Integrator.hpp>
#include <smks/scn/Camera.hpp>
#include <smks/scn/Light.hpp>
#include <smks/scn/Xform.hpp>
#include <smks/scn/Archive.hpp>
#include "smks/scn/test/TestScene.hpp"
#include "smks/scn/test/FilterByNameLength.hpp"

TEST(SceneTest, IdGetterByType)
{
    using namespace smks::scn;

    Scene::Ptr      scene       = Scene::create();
    TreeNode::Ptr   node1       = TreeNode::create("_node1_", scene);
    TreeNode::Ptr   node2       = TreeNode::create("_node2_", scene);
    Material::Ptr   material    = Material::create("matte", "_matte_", scene);
    SubSurface::Ptr subsurface  = SubSurface::create("default", "_subsurface_", scene);
    Integrator::Ptr integrator  = Integrator::create("default", "_integrator_", scene);

    smks::xchg::IdSet       ids, refs;
    Scene::IdGetterOptions  opts;

    //---
    scene->getIds(ids, &opts);
    //---
    refs.clear();
    refs.insert(scene->id());
    refs.insert(node1->id());
    refs.insert(node2->id());
    refs.insert(material->id());
    refs.insert(subsurface->id());
    refs.insert(integrator->id());
    EXPECT_TRUE(smks::xchg::equal(ids, refs));

    //---
    opts.filter(FilterTreeNodeByClass::create(smks::xchg::NodeClass::MATERIAL));
    scene->getIds(ids, &opts);
    //---
    refs.clear();
    refs.insert(material->id());
    EXPECT_TRUE(smks::xchg::equal(ids, refs));

    //---
    opts.filter(FilterTreeNodeByClass::create(smks::xchg::NodeClass::SHADER));
    scene->getIds(ids, &opts);
    //---
    refs.clear();
    refs.insert(material->id());
    refs.insert(subsurface->id());
    EXPECT_TRUE(smks::xchg::equal(ids, refs));

    //---
    opts.filter(FilterTreeNodeByClass::create(smks::xchg::NodeClass::SUBSURFACE | smks::xchg::NodeClass::INTEGRATOR));
    scene->getIds(ids, &opts);
    //---
    refs.clear();
    refs.insert(integrator->id());
    refs.insert(subsurface->id());
    EXPECT_TRUE(smks::xchg::equal(ids, refs));

    Scene::destroy(scene);
}

TEST(SceneTest, NodeCreatedEvent)
{
    using namespace smks::scn;

    test::EventHistory      evts;
    std::vector<size_t>     evtIds;

    test::TestScene::Ptr    scene   = test::TestScene::create("");
    TreeNode::Ptr           parent  = TreeNode::create("parent", scene);
    TreeNode::Ptr           node    = TreeNode::create("node", parent);

    scene->flushHistory(&evts);

    evtIds = evts.getSceneEvents(smks::xchg::SceneEvent::NODE_CREATED);
    EXPECT_EQ(evtIds.size(), 2);

    const smks::xchg::SceneEvent* evt = evts[evtIds[0]].asSceneEvent();
    assert(evt);
    EXPECT_EQ(evt->node(),      parent->id());
    EXPECT_EQ(evt->parent(),    scene->id());

    evt = evts[evtIds[1]].asSceneEvent();
    assert(evt);
    EXPECT_EQ(evt->node(),      node->id());
    EXPECT_EQ(evt->parent(),    parent->id());

    test::TestScene::destroy(scene);
}

TEST(SceneTest, NodeDeletedEvent)
{
    using namespace smks::scn;

    test::EventHistory      evts;
    std::vector<size_t>     evtIds;

    test::TestScene::Ptr    scene       = test::TestScene::create("");
    TreeNode::Ptr           parent      = TreeNode::create("parent", scene);
    TreeNode::Ptr           node        = TreeNode::create("node", parent);

    const unsigned int      nodeId      = node->id();
    const unsigned int      parentId    = parent->id();
    //---
    TreeNode::destroy(parent);
    //---
    scene->flushHistory(&evts);

    evtIds = evts.getSceneEvents(smks::xchg::SceneEvent::NODE_DELETED);
    EXPECT_EQ(evtIds.size(), 2);

    const smks::xchg::SceneEvent* evt = evts[evtIds[0]].asSceneEvent();
    assert(evt);
    EXPECT_EQ(evt->node(),      nodeId);
    EXPECT_EQ(evt->parent(),    parentId);

    evt = evts[evtIds[1]].asSceneEvent();
    assert(evt);
    EXPECT_EQ(evt->node(),      parentId);
    EXPECT_EQ(evt->parent(),    scene->id());

    test::TestScene::destroy(scene);
}

TEST(SceneTest, NodeConnectedEvent)
{
    using namespace smks::scn;

    test::EventHistory      evts;
    std::vector<size_t>     evtIds;

    const std::string srcName       = "src";
    const std::string srcParmName   = "srcParm";
    const std::string dstName       = "dst";
    const std::string dstParmName   = "dstParm";
    const unsigned int srcId        = smks::util::string::getId("/src");
    const unsigned int srcParmId    = smks::util::string::getId(srcParmName.c_str());
    const unsigned int dstId        = smks::util::string::getId("/dst");
    const unsigned int dstParmId    = smks::util::string::getId(dstParmName.c_str());

    test::TestScene::Ptr    scene   = test::TestScene::create("");
    TreeNode::Ptr           src     = TreeNode::create(srcName.c_str(), scene);
    TreeNode::Ptr           dst     = TreeNode::create(dstName.c_str(), scene);
    //---
    const unsigned int      cnxId   = scene->connect<int>(srcId, srcParmName.c_str(), dstId, dstParmName.c_str());
    //---
    scene->flushHistory(&evts);

    evtIds = evts.getSceneEvents(smks::xchg::SceneEvent::PARM_CONNECTED);
    EXPECT_EQ(evtIds.size(), 1);

    const smks::xchg::SceneEvent* evt = evts[evtIds[0]].asSceneEvent();
    assert(evt);
    EXPECT_EQ(evt->connection(),    cnxId);
    EXPECT_EQ(evt->master(),        srcId);
    EXPECT_EQ(evt->masterParm(),    srcParmId);
    EXPECT_EQ(evt->slave(),         dstId);
    EXPECT_EQ(evt->slaveParm(),     dstParmId);

    test::TestScene::destroy(scene);
}

TEST(SceneTest, NodeConnectedEvent_LateNodeCreation)
{
    using namespace smks::scn;

    test::EventHistory      evts;
    std::vector<size_t>     evtIds;

    const std::string srcName       = "src";
    const std::string srcParmName   = "srcParm";
    const std::string dstName       = "dst";
    const std::string dstParmName   = "dstParm";
    const unsigned int srcId        = smks::util::string::getId("/src");
    const unsigned int srcParmId    = smks::util::string::getId(srcParmName.c_str());
    const unsigned int dstId        = smks::util::string::getId("/dst");
    const unsigned int dstParmId    = smks::util::string::getId(dstParmName.c_str());

    test::TestScene::Ptr    scene   = test::TestScene::create("");
    const unsigned int      cnxId   = scene->connect<int>(srcId, srcParmName.c_str(), dstId, dstParmName.c_str());
    //---
    TreeNode::Ptr           src     = TreeNode::create(srcName.c_str(), scene);
    TreeNode::Ptr           dst     = TreeNode::create(dstName.c_str(), scene);
    //---
    scene->flushHistory(&evts);

    evtIds = evts.getSceneEvents(smks::xchg::SceneEvent::PARM_CONNECTED);
    EXPECT_EQ(evtIds.size(), 1);

    const smks::xchg::SceneEvent* evt = evts[evtIds[0]].asSceneEvent();
    assert(evt);
    EXPECT_EQ(evt->connection(),    cnxId);
    EXPECT_EQ(evt->master(),        srcId);
    EXPECT_EQ(evt->masterParm(),    srcParmId);
    EXPECT_EQ(evt->slave(),         dstId);
    EXPECT_EQ(evt->slaveParm(),     dstParmId);

    test::TestScene::destroy(scene);
}

TEST(SceneTest, NodeConnectedEvent_SourceRecreation)
{
    using namespace smks::scn;

    test::EventHistory      evts;
    std::vector<size_t>     evtIds;

    const std::string srcName       = "src";
    const std::string srcParmName   = "srcParm";
    const std::string dstName       = "dst";
    const std::string dstParmName   = "dstParm";
    const unsigned int srcId        = smks::util::string::getId("/src");
    const unsigned int srcParmId    = smks::util::string::getId(srcParmName.c_str());
    const unsigned int dstId        = smks::util::string::getId("/dst");
    const unsigned int dstParmId    = smks::util::string::getId(dstParmName.c_str());

    test::TestScene::Ptr    scene   = test::TestScene::create("");
    TreeNode::Ptr           src     = TreeNode::create(srcName.c_str(), scene);
    TreeNode::Ptr           dst     = TreeNode::create(dstName.c_str(), scene);

    const unsigned int      cnxId   = scene->connect<int>(srcId, srcParmName.c_str(), dstId, dstParmName.c_str());
    //---
    scene->disconnect(cnxId);
    //---
    scene->flushHistory(&evts);

    evtIds = evts.getSceneEvents(smks::xchg::SceneEvent::PARM_DISCONNECTED);
    EXPECT_EQ(evtIds.size(), 1);

    const smks::xchg::SceneEvent* evt = evts[evtIds[0]].asSceneEvent();
    assert(evt);
    EXPECT_EQ(evt->connection(),    cnxId);
    EXPECT_EQ(evt->master(),        srcId);
    EXPECT_EQ(evt->masterParm(),    srcParmId);
    EXPECT_EQ(evt->slave(),         dstId);
    EXPECT_EQ(evt->slaveParm(),     dstParmId);

    test::TestScene::destroy(scene);
}

TEST(SceneTest, NodeDisconnectedEvent)
{
    using namespace smks::scn;

    test::EventHistory      evts;
    std::vector<size_t>     evtIds;

    const std::string srcName       = "src";
    const std::string srcParmName   = "srcParm";
    const std::string dstName       = "dst";
    const std::string dstParmName   = "dstParm";
    const unsigned int srcId        = smks::util::string::getId("/src");
    const unsigned int srcParmId    = smks::util::string::getId(srcParmName.c_str());
    const unsigned int dstId        = smks::util::string::getId("/dst");
    const unsigned int dstParmId    = smks::util::string::getId(dstParmName.c_str());

    test::TestScene::Ptr    scene   = test::TestScene::create("");
    TreeNode::Ptr           src     = TreeNode::create(srcName.c_str(), scene);
    TreeNode::Ptr           dst     = TreeNode::create(dstName.c_str(), scene);

    const unsigned int      cnxId   = scene->connect<int>(srcId, srcParmName.c_str(), dstId, dstParmName.c_str());
    //---
    scene->disconnect(cnxId);
    //---
    scene->flushHistory(&evts);

    evtIds = evts.getSceneEvents(smks::xchg::SceneEvent::PARM_DISCONNECTED);
    EXPECT_EQ(evtIds.size(), 1);

    const smks::xchg::SceneEvent* evt = evts[evtIds[0]].asSceneEvent();
    assert(evt);
    EXPECT_EQ(evt->connection(),    cnxId);
    EXPECT_EQ(evt->master(),        srcId);
    EXPECT_EQ(evt->masterParm(),    srcParmId);
    EXPECT_EQ(evt->slave(),         dstId);
    EXPECT_EQ(evt->slaveParm(),     dstParmId);

    test::TestScene::destroy(scene);
}

TEST(SceneTest, NodeDisconnectedEvent_NodeDeletion)
{
    using namespace smks::scn;

    test::EventHistory      evts;
    std::vector<size_t>     evtIds;

    const std::string srcName       = "src";
    const std::string srcParmName   = "srcParm";
    const std::string dstName       = "dst";
    const std::string dstParmName   = "dstParm";
    const unsigned int srcId        = smks::util::string::getId("/src");
    const unsigned int srcParmId    = smks::util::string::getId(srcParmName.c_str());
    const unsigned int dstId        = smks::util::string::getId("/dst");
    const unsigned int dstParmId    = smks::util::string::getId(dstParmName.c_str());

    test::TestScene::Ptr    scene   = test::TestScene::create("");
    TreeNode::Ptr           src     = TreeNode::create(srcName.c_str(), scene);
    TreeNode::Ptr           dst     = TreeNode::create(dstName.c_str(), scene);

    const unsigned int      cnxId   = scene->connect<int>(srcId, srcParmName.c_str(), dstId, dstParmName.c_str());
    //---
    TreeNode::destroy(src);
    //---
    scene->flushHistory(&evts);

    evtIds = evts.getSceneEvents(smks::xchg::SceneEvent::PARM_DISCONNECTED);
    EXPECT_EQ(evtIds.size(), 1);

    const smks::xchg::SceneEvent* evt = evts[evtIds[0]].asSceneEvent();
    assert(evt);
    EXPECT_EQ(evt->connection(),    cnxId);
    EXPECT_EQ(evt->master(),        srcId);
    EXPECT_EQ(evt->masterParm(),    srcParmId);
    EXPECT_EQ(evt->slave(),         dstId);
    EXPECT_EQ(evt->slaveParm(),     dstParmId);

    scene->disconnect(cnxId);   // should do nothing since the connection is already down
    test::TestScene::destroy(scene);
}

TEST(SceneTest, NodeLinkedEvent)
{
    using namespace smks::scn;

    test::EventHistory      evts;
    std::vector<size_t>     evtIds;

    const std::string targetName    = "target";
    const std::string linkedName    = "linked";
    const std::string parmName      = "srcParm";
    const unsigned int linkedId     = smks::util::string::getId("/linked");

    test::TestScene::Ptr    scene   = test::TestScene::create("");
    TreeNode::Ptr           target  = TreeNode::create(targetName.c_str(), scene);
    TreeNode::Ptr           linked  = TreeNode::create(linkedName.c_str(), scene);

    //---
    const unsigned int parmId = target->setParameterId(parmName.c_str(), linkedId);
    //---
    scene->flushHistory(&evts);

    evtIds = evts.getSceneEvents(smks::xchg::SceneEvent::NODES_LINKED);
    EXPECT_EQ(evtIds.size(), 1);

    const smks::xchg::SceneEvent* evt = evts[evtIds[0]].asSceneEvent();
    assert(evt);
    EXPECT_EQ(evt->target(),    target->id());
    EXPECT_EQ(evt->source(),    linked->id());

    test::TestScene::destroy(scene);
}

TEST(SceneTest, NodeLinkedEvent_LateLinkedCreation)
{
    using namespace smks::scn;

    test::EventHistory      evts;
    std::vector<size_t>     evtIds;

    const std::string targetName    = "target";
    const std::string linkedName    = "linked";
    const std::string parmName      = "srcParm";
    const unsigned int targetId     = smks::util::string::getId("/target");
    const unsigned int linkedId     = smks::util::string::getId("/linked");

    test::TestScene::Ptr    scene   = test::TestScene::create("");
    TreeNode::Ptr   target  = TreeNode::create(targetName.c_str(), scene);

    const unsigned int  parmId = target->setParameterId(parmName.c_str(), linkedId);
    //---
    TreeNode::Ptr   linked  = TreeNode::create(linkedName.c_str(), scene);
    //---

    scene->flushHistory(&evts);

    evtIds = evts.getSceneEvents(smks::xchg::SceneEvent::NODES_LINKED);
    EXPECT_EQ(evtIds.size(), 1);

    const smks::xchg::SceneEvent* evt = evts[evtIds[0]].asSceneEvent();
    assert(evt);
    EXPECT_EQ(evt->target(),    target->id());
    EXPECT_EQ(evt->source(),    linked->id());

    test::TestScene::destroy(scene);
}

TEST(SceneTest, NodeLinkedEvent_LinkedRecreation)
{
    using namespace smks::scn;

    test::EventHistory      evts;
    std::vector<size_t>     evtIds;

    const std::string targetName    = "target";
    const std::string linkedName    = "linked";
    const std::string parmName      = "srcParm";
    const unsigned int targetId     = smks::util::string::getId("/target");
    const unsigned int linkedId     = smks::util::string::getId("/linked");

    test::TestScene::Ptr    scene   = test::TestScene::create("");
    TreeNode::Ptr           target  = TreeNode::create(targetName.c_str(), scene);
    TreeNode::Ptr           linked  = TreeNode::create(linkedName.c_str(), scene);

    const unsigned int  parmId = target->setParameterId(parmName.c_str(), linkedId);

    TreeNode::destroy(linked);
    linked = nullptr;
    scene->flushHistory(&evts);
    //---
    linked  = TreeNode::create(linkedName.c_str(), scene);
    //---
    scene->flushHistory(&evts);
    evtIds = evts.getSceneEvents(smks::xchg::SceneEvent::NODES_LINKED);
    EXPECT_EQ(evtIds.size(), 1);

    const smks::xchg::SceneEvent* evt = evts[evtIds[0]].asSceneEvent();
    assert(evt);
    EXPECT_EQ(evt->target(),    targetId);
    EXPECT_EQ(evt->source(),    linkedId);

    test::TestScene::destroy(scene);
}

TEST(SceneTest, NodeUnlinkedEvent)
{
    using namespace smks::scn;

    test::EventHistory      evts;
    std::vector<size_t>     evtIds;

    const std::string targetName    = "target";
    const std::string linkedName    = "linked";
    const std::string parmName      = "srcParm";
    const unsigned int targetId     = smks::util::string::getId("/target");
    const unsigned int linkedId     = smks::util::string::getId("/linked");

    test::TestScene::Ptr    scene   = test::TestScene::create("");
    TreeNode::Ptr           target  = TreeNode::create(targetName.c_str(), scene);
    TreeNode::Ptr           linked  = TreeNode::create(linkedName.c_str(), scene);

    const unsigned int  parmId = target->setParameterId(parmName.c_str(), linkedId);
    //---
    target->unsetParameter(parmName.c_str());
    //---
    scene->flushHistory(&evts);

    evtIds = evts.getSceneEvents(smks::xchg::SceneEvent::NODES_UNLINKED);
    EXPECT_EQ(evtIds.size(), 1);

    const smks::xchg::SceneEvent* evt = evts[evtIds[0]].asSceneEvent();
    assert(evt);
    EXPECT_EQ(evt->target(),    targetId);
    EXPECT_EQ(evt->source(),    linkedId);

    test::TestScene::destroy(scene);
}

TEST(SceneTest, NodeUnlinkedEvent_LinkedDeletion)
{
    using namespace smks::scn;

    test::EventHistory      evts;
    std::vector<size_t>     evtIds;

    const std::string targetName    = "target";
    const std::string linkedName    = "linked";
    const std::string parmName      = "srcParm";
    const unsigned int targetId     = smks::util::string::getId("/target");
    const unsigned int linkedId     = smks::util::string::getId("/linked");

    test::TestScene::Ptr    scene   = test::TestScene::create("");
    TreeNode::Ptr           target  = TreeNode::create(targetName.c_str(), scene);
    TreeNode::Ptr           linked  = TreeNode::create(linkedName.c_str(), scene);

    const unsigned int  parmId = target->setParameterId(parmName.c_str(), linkedId);
    //---
    TreeNode::destroy(linked);
    //---
    scene->flushHistory(&evts);

    evtIds = evts.getSceneEvents(smks::xchg::SceneEvent::NODES_UNLINKED);
    EXPECT_EQ(evtIds.size(), 1);

    const smks::xchg::SceneEvent* evt = evts[evtIds[0]].asSceneEvent();
    assert(evt);
    EXPECT_EQ(evt->target(),    targetId);
    EXPECT_EQ(evt->source(),    linkedId);

    test::TestScene::destroy(scene);
}

TEST(SceneTest, NodeUnlinkedEvent_TargetDeletion)
{
    using namespace smks::scn;

    test::EventHistory      evts;
    std::vector<size_t>     evtIds;

    const std::string targetName    = "target";
    const std::string linkedName    = "linked";
    const std::string parmName      = "srcParm";
    const unsigned int targetId     = smks::util::string::getId("/target");
    const unsigned int linkedId     = smks::util::string::getId("/linked");

    test::TestScene::Ptr    scene   = test::TestScene::create("");
    TreeNode::Ptr           target  = TreeNode::create(targetName.c_str(), scene);
    TreeNode::Ptr           linked  = TreeNode::create(linkedName.c_str(), scene);

    const unsigned int  parmId = target->setParameterId(parmName.c_str(), linkedId);
    //---
    TreeNode::destroy(target);
    //---
    scene->flushHistory(&evts);

    evtIds = evts.getSceneEvents(smks::xchg::SceneEvent::NODES_UNLINKED);
    EXPECT_EQ(evtIds.size(), 1);

    const smks::xchg::SceneEvent* evt = evts[evtIds[0]].asSceneEvent();
    assert(evt);
    EXPECT_EQ(evt->target(),    targetId);
    EXPECT_EQ(evt->source(),    linkedId);

    test::TestScene::destroy(scene);
}

TEST(SceneTest, WorldTransformComputation)
{
    using namespace smks;
    typedef boost::unordered_map<unsigned int, math::Affine3f> Transforms;

    scn::TreeNode::Ptr  scene       = scn::Scene    ::create();
    scn::TreeNode::Ptr  xfm1        = scn::Xform    ::create("xfm1",    scene);
    scn::TreeNode::Ptr  node1       = scn::TreeNode ::create("node1",   scene);
    scn::TreeNode::Ptr  xfm2        = scn::Xform    ::create("xfm2",    xfm1);
    scn::TreeNode::Ptr  node2       = scn::TreeNode ::create("node2",   xfm1);
    scn::TreeNode::Ptr  xfm3        = scn::Xform    ::create("xfm2",    xfm2);
    scn::TreeNode::Ptr  node3       = scn::TreeNode ::create("node3",   xfm2);
    scn::TreeNode::Ptr  node4       = scn::TreeNode ::create("node3",   xfm3);
    scn::TreeNode::Ptr  xfm4        = scn::Xform    ::create("xfm4",    node3);
    scn::TreeNode::Ptr  node5       = scn::TreeNode ::create("node5",   xfm4);

    scn::TreeNode::Ptr  anyNodes[]  = { node1, node2, node3, node4, node5 };
    scn::TreeNode::Ptr  xforms[]    = { xfm1, xfm2, xfm3, xfm4 };
    const size_t        numAnyNodes = sizeof(anyNodes) / sizeof(scn::TreeNode::Ptr);
    const size_t        numXforms   = sizeof(xforms) / sizeof(scn::TreeNode::Ptr);
    Transforms          transforms;

    // initialize xforms
    for (size_t i = 0; i < numXforms; ++i)
    {
        const math::Affine3f& xfm = math::test::getRandomTransform();

        transforms[xforms[i]->id()] = xfm;
        xforms[i]->setParameter<math::Affine3f>(parm::transform(), xfm);
    }

    std::vector<size_t> pickedXfm(10);
    for (size_t n = 0; n < pickedXfm.size(); ++n)
        pickedXfm[n] = n % numXforms;
    std::random_shuffle(pickedXfm.begin(), pickedXfm.end());

    size_t n = 0;
    do
    {
        // compute reference world transforms from local transforms
        math::Affine3f  worldXfm1   = transforms[xfm1->id()];
        math::Affine3f  worldXfm2   = worldXfm1 * transforms[xfm2->id()];
        math::Affine3f  worldXfm3   = worldXfm2 * transforms[xfm3->id()];
        math::Affine3f  worldXfm4   = worldXfm2 * transforms[xfm4->id()];

        math::Affine3f  worldNode1  = math::Affine3f::Identity();
        math::Affine3f  worldNode2  = worldXfm1;
        math::Affine3f  worldNode3  = worldXfm2;
        math::Affine3f  worldNode4  = worldXfm3;
        math::Affine3f  worldNode5  = worldXfm4;

//      std::cout << "xfm:\n" << worldXfm.matrix() << "\ncam:\n" << worldCam.matrix() << "\nlight1:\n" << worldLight1.matrix() << "\nlight2:\n" << worldLight2.matrix() << std::endl;

        //-------
        math::Affine3f mat;

        scene->getParameter<math::Affine3f>(parm::worldTransform(), mat);
        EXPECT_TRUE(xchg::equal<math::Affine3f>(mat, math::Affine3f::Identity()));

        xfm1->getParameter<math::Affine3f>(parm::worldTransform(), mat);
        EXPECT_TRUE(xchg::equal<math::Affine3f>(mat, worldXfm1));

        xfm2->getParameter<math::Affine3f>(parm::worldTransform(), mat);
        EXPECT_TRUE(xchg::equal<math::Affine3f>(mat, worldXfm2));

        xfm3->getParameter<math::Affine3f>(parm::worldTransform(), mat);
        EXPECT_TRUE(xchg::equal<math::Affine3f>(mat, worldXfm3));

        xfm4->getParameter<math::Affine3f>(parm::worldTransform(), mat);
        EXPECT_TRUE(xchg::equal<math::Affine3f>(mat, worldXfm4));

        node1->getParameter<math::Affine3f>(parm::worldTransform(), mat);
        EXPECT_TRUE(xchg::equal<math::Affine3f>(mat, worldNode1));

        node2->getParameter<math::Affine3f>(parm::worldTransform(), mat);
        EXPECT_TRUE(xchg::equal<math::Affine3f>(mat, worldNode2));

        node3->getParameter<math::Affine3f>(parm::worldTransform(), mat);
        EXPECT_TRUE(xchg::equal<math::Affine3f>(mat, worldNode3));

        node4->getParameter<math::Affine3f>(parm::worldTransform(), mat);
        EXPECT_TRUE(xchg::equal<math::Affine3f>(mat, worldNode4));

        node5->getParameter<math::Affine3f>(parm::worldTransform(), mat);
        EXPECT_TRUE(xchg::equal<math::Affine3f>(mat, worldNode5));
        //-------
        if (n >= pickedXfm.size())
            break;

        // update one of the transforms in the hierarchy
        const size_t            randIdx     = pickedXfm[n];
        scn::TreeNode::Ptr      randNode    = xforms[randIdx];
        const math::Affine3f&   xfm         = math::test::getRandomTransform();

        transforms[randNode->id()] = xfm;
        //---
        randNode->setParameter<math::Affine3f>(parm::transform(), xfm);
        //---
        ++n;
    }
    while (n < 10);

    scn::Scene::destroy(scene);
}

//////////////////////////
// Global Num Samples Test
//////////////////////////
namespace smks { namespace scn { namespace test
{
    typedef boost::unordered_map<unsigned int, std::pair<size_t, size_t>> GlobalNumSamplesMap;

    void
    GlobalNumSamplesComputation_test(const TreeNode& node, size_t prvGlobalNumSamples, GlobalNumSamplesMap& results)
    {
        size_t numSamples = 0;
        size_t globalNumSamples = 0;

        node.getParameter<size_t>(parm::numSamples(),       numSamples);
        node.getParameter<size_t>(parm::globalNumSamples(), globalNumSamples);

        size_t refGlobalNumSamples  = std::max(prvGlobalNumSamples, numSamples);
        results[node.id()].first    = globalNumSamples;
        results[node.id()].second   = refGlobalNumSamples;

        for (size_t i = 0; i < node.numChildren(); ++i)
            if (node.child(i))
                GlobalNumSamplesComputation_test(*node.child(i), refGlobalNumSamples, results);
    }
}
}
}

TEST(SceneTest, ComputeGlobalNumSamples_01)
{
    using namespace smks;

    scn::Scene::Ptr scene = scn::Scene::create();
    scene->setSharedResourceDatabase(g_resources);

    scn::Archive::Ptr archive = scn::Archive::create("abc/global_num_samples.abc", scene);
    scn::test::initializeDescendants(true, *archive);

    scn::test::GlobalNumSamplesMap results;

    scn::test::GlobalNumSamplesComputation_test(*scene, 0, results);
    for (scn::test::GlobalNumSamplesMap::const_iterator it = results.begin(); it != results.end(); ++it)
        EXPECT_EQ(it->second.first, it->second.second);

    scn::Scene::destroy(scene);
}

TEST(SceneTest, ComputeGlobalNumSamples_02)
{
    using namespace smks;

    scn::Scene::Ptr scene = scn::Scene::create();
    scene->setSharedResourceDatabase(g_resources);

    scn::Archive::Ptr archive = scn::Archive::create("abc/groups_only.abc", scene);
    scn::test::initializeDescendants(true, *archive);

    scn::TreeNode::Ptr const& group_static  = scn::test::getNodeByName(*scene, "group_static", false);
    scn::TreeNode::Ptr const& group_moved   = scn::test::getNodeByName(*scene, "group_moved", false);
    assert(group_static);
    assert(group_moved);

    // create static user xforms
    scn::TreeNode::Ptr const& group_static_child = scn::Xform::create("group_static_child", group_static);
    scn::TreeNode::Ptr const& group_moved_child = scn::Xform::create("group_moved_child", group_moved);

    // add all kinds of geometry directly below those static user xforms
    scn::TreeNode::Ptr  parents[]   = { group_static_child, group_moved_child };
    const std::string   baseNames[] = {
        "abc/geo_static.abc",
        "abc/geo_transformed.abc",
        "abc/geo_deformed.abc",
        "abc/geo_dynamic.abc"
    };
    for (int j = sizeof(parents)/sizeof(scn::TreeNode::Ptr) - 1; j >= 0; --j)
        for (int i = sizeof(baseNames) / sizeof(std::string) - 1; i >= 0; --i)
        {
            scn::Archive::Ptr geo_abc = scn::Archive::create(baseNames[i].c_str(), parents[j]);
            scn::test::initializeDescendants(true, *geo_abc);
        }

    struct ExpectedResult{ std::string xformQuery; bool isXformAnimated; bool isShapeAnimated; };
    std::vector<ExpectedResult> results;
    //###################################
    results.push_back(ExpectedResult());
    results.back().xformQuery = ".*/group_static_child/geo_static/geo_static";
    results.back().isXformAnimated = false;
    results.back().isShapeAnimated = false;

    results.push_back(ExpectedResult());
    results.back().xformQuery = ".*/group_static_child/geo_transformed/geo_transformed";
    results.back().isXformAnimated = true;
    results.back().isShapeAnimated = true;

    results.push_back(ExpectedResult());
    results.back().xformQuery = ".*/group_static_child/geo_deformed/geo_deformed";
    results.back().isXformAnimated = false;
    results.back().isShapeAnimated = true;

    results.push_back(ExpectedResult());
    results.back().xformQuery = ".*/group_static_child/geo_dynamic/geo_dynamic";
    results.back().isXformAnimated = false;
    results.back().isShapeAnimated = true;
    //-----------------------------------
    results.push_back(ExpectedResult());
    results.back().xformQuery = ".*/group_moved_child/geo_static/geo_static";
    results.back().isXformAnimated = true;
    results.back().isShapeAnimated = true;

    results.push_back(ExpectedResult());
    results.back().xformQuery = ".*/group_moved_child/geo_transformed/geo_transformed";
    results.back().isXformAnimated = true;
    results.back().isShapeAnimated = true;

    results.push_back(ExpectedResult());
    results.back().xformQuery = ".*/group_moved_child/geo_deformed/geo_deformed";
    results.back().isXformAnimated = true;
    results.back().isShapeAnimated = true;

    results.push_back(ExpectedResult());
    results.back().xformQuery = ".*/group_moved_child/geo_dynamic/geo_dynamic";
    results.back().isXformAnimated = true;
    results.back().isShapeAnimated = true;
    //###################################
    for (size_t i = 0; i < results.size(); ++i)
    {
        scn::TreeNode::Ptr      node    = nullptr;
        size_t                  parm1s  = 0;
        const ExpectedResult&   result  = results[i];

        // xform
        node = scn::test::getNodeByName(*scene, result.xformQuery.c_str(), true, true);
        assert(node);
        EXPECT_EQ(node->nodeClass(), xchg::XFORM);
        node->getParameter<size_t>(parm::globalNumSamples(), parm1s);
        if (result.isXformAnimated) EXPECT_GT(parm1s, 1);
        else                        EXPECT_EQ(parm1s, 1);
        // shape
        node = node->child(0);
        assert(node);
        EXPECT_EQ(node->nodeClass(), xchg::MESH);
        node->getParameter<size_t>(parm::globalNumSamples(), parm1s);
        if (result.isShapeAnimated) EXPECT_GT(parm1s, 1);
        else                        EXPECT_EQ(parm1s, 1);
    }

    scn::Scene::destroy(scene);
}

/////////////////////////
// Global Visibility Test
/////////////////////////
namespace smks { namespace scn { namespace test
{
    typedef boost::unordered_map<unsigned int, std::pair<char, char>> GloballyVisibleMap;

    void
    GloballyVisibleComputation_test(const TreeNode& node, char prvGloballyVisible, GloballyVisibleMap& results)
    {
        char visible = 0;
        char globallyVisible = 0;

        node.getParameter<char>(parm::currentlyVisible(),   visible);
        node.getParameter<char>(parm::globallyVisible(),    globallyVisible);

        char refGloballyVisible     = (prvGloballyVisible != 0) && (visible != 0);
        results[node.id()].first    = globallyVisible;
        results[node.id()].second   = refGloballyVisible;

        for (size_t i = 0; i < node.numChildren(); ++i)
            if (node.child(i))
                GloballyVisibleComputation_test(*node.child(i), refGloballyVisible, results);
    }
}
}
}

TEST(SceneTest, ComputeGloballyVisible)
{
    using namespace smks;

    scn::Scene::Ptr scene = scn::Scene::create();
    scene->setSharedResourceDatabase(g_resources);

    scn::Archive::Ptr archive = scn::Archive::create("abc/global_visibility.abc", scene);
    scn::test::initializeDescendants(true, *archive);

    scn::test::GloballyVisibleMap results;

    scn::test::GloballyVisibleComputation_test(*scene, 1, results);
    for (scn::test::GloballyVisibleMap::const_iterator it = results.begin(); it != results.end(); ++it)
        EXPECT_EQ(it->second.first, it->second.second);

    scn::Scene::destroy(scene);
}

TEST(SceneTest, ComputeGloballyVisibleWithOverride)
{
    using namespace smks;

    scn::Scene::Ptr scene = scn::Scene::create();
    scene->setSharedResourceDatabase(g_resources);

    scn::Archive::Ptr archive = scn::Archive::create("abc/visibility_override.abc", scene);
    scn::test::initializeDescendants(true, *archive);

    scn::TreeNode::Ptr group1       = scn::test::getNodeByName(*scene, "group1", false);
    scn::TreeNode::Ptr group2       = scn::test::getNodeByName(*scene, "group2", false);
    scn::TreeNode::Ptr cube         = scn::test::getNodeByName(*scene, "cube", false);
    scn::TreeNode::Ptr cubeShape    = scn::test::getNodeByName(*scene, "cubeShape", false);
    assert(group1);
    assert(group2);
    assert(cube);
    assert(cubeShape);

    char parm1c = 0;

    group1->getParameter<char>(parm::currentlyVisible(), parm1c);
    EXPECT_EQ(parm1c, 0);   // invisible in archive
    group2->getParameter<char>(parm::currentlyVisible(), parm1c);
    EXPECT_NE(parm1c, 0);   // visible in archive
    cube->getParameter<char>(parm::currentlyVisible(), parm1c);
    EXPECT_EQ(parm1c, 0);   // invisible in archive
    cubeShape->getParameter<char>(parm::currentlyVisible(), parm1c);
    EXPECT_NE(parm1c, 0);   // visible in archive

    group2->getParameter<char>(parm::globallyVisible(), parm1c);
    EXPECT_EQ(parm1c, 0);
    //---
    group1->setParameter<char>(parm::forciblyVisible(), 1);
    //---
    group2->getParameter<char>(parm::globallyVisible(), parm1c);
    EXPECT_NE(parm1c, 0);   // now globally visible

    cubeShape->getParameter<char>(parm::globallyVisible(), parm1c);
    EXPECT_EQ(parm1c, 0);
    //---
    cube->setParameter<char>(parm::forciblyVisible(), 1);
    //---
    cubeShape->getParameter<char>(parm::globallyVisible(), parm1c);
    EXPECT_NE(parm1c, 0);   // now globally visible
    //---
    group2->setParameter<char>(parm::forciblyVisible(), 0);
    //---
    cube->getParameter<char>(parm::globallyVisible(), parm1c);
    EXPECT_EQ(parm1c, 0);   // now globally invisible
    cubeShape->getParameter<char>(parm::globallyVisible(), parm1c);
    EXPECT_EQ(parm1c, 0);   // now globally invisible

    scn::Scene::destroy(scene);
}

TEST(SceneTest, GlobalParameterEvent)
{
    using namespace smks;

    scn::test::EventHistory                     evts;
    scn::test::TestScene::Ptr                   scene = scn::test::TestScene::create();
    std::vector<scn::TreeNode::Ptr>             nodes;
    std::vector<scn::test::ParmObserver::Ptr>   obs;

    nodes.push_back(scn::Xform::create("xfm1", scene)); // 0
    nodes.push_back(scn::Xform::create("xfm2", nodes[0]));  // 1
    nodes.push_back(scn::test::TestNode::create("node1", nodes[1]));    // 2
    nodes.push_back(scn::test::TestNode::create("node2", scene));   // 3

    std::cout << "transform      = " << parm::transform().id() << std::endl;
    std::cout << "worldTransform = " << parm::worldTransform().id() << std::endl;

    for (size_t i = 0; i < nodes.size(); ++i)
    {
        obs.push_back(scn::test::ParmObserver::create());
        nodes[i]->registerObserver(obs.back());
    }

    //for (size_t i = 0; i < obs.size(); ++i)
    //  obs[i]->flushHistory();
    nodes[0]->setParameter<math::Affine3f>(parm::transform(), math::test::getRandomTransform());
    for (size_t i = 0; i < obs.size(); ++i)
    {
        obs[i]->flushHistory(&evts);
        std::cout << "node '" << nodes[i]->name() << "' :\n" << evts << "\n\n";
        if (std::dynamic_pointer_cast<scn::test::TestNode>(nodes[i]))
        {
            std::dynamic_pointer_cast<scn::test::TestNode>(nodes[i])->flushHistory(&evts);
            std::cout << "--\n" << evts << std::endl;
        }
    }


    for (size_t i = 0; i < nodes.size(); ++i)
        nodes[i]->deregisterObserver(obs[i]);

    scn::Scene::destroy(scene);
}
