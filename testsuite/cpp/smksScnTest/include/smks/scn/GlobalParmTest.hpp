// ========================================================================= //
// Copyright 2016-2018 SUPAMONKS_STUDIO                                      //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
//                                                                           //
// ========================================================================= //
#pragma once

#include <gtest/gtest.h>

#include <smks/scn/Scene.hpp>
#include <smks/scn/Archive.hpp>

#include "smks/scn/test/util.hpp"

namespace smks { namespace scn { namespace test
{
    class TestNodeParmListener:
        public scn::AbstractParmObserver
    {
    public:
        typedef std::shared_ptr<TestNodeParmListener> Ptr;
    private:
        const Scene&        _scene;
        const unsigned int  _node;
    public:
        static
        Ptr
        create(const Scene& scene, unsigned int node)
        {
            Ptr ptr(new TestNodeParmListener(scene, node));
            return ptr;
        }
    protected:
        TestNodeParmListener(const Scene& scene, unsigned int node):
            _scene  (scene),
            _node   (node)
        { }
    public:
        void
        handle(scn::TreeNode& node, const xchg::ParmEvent& evt)
        {
            if (evt.parm() == parm::globalInitialization().id())
            {
                std::vector<std::pair<unsigned int, char>> parms;
                TreeNode::Ptr const& emitter = _scene.descendantById(evt.node()).lock();

                test::getParentParameters<char>(
                    parm::initialization(),
                    node,
                    parms);

                std::cout
                    << "evnt: " << evt << " "
                    << "(" << parm::test::getParameterName(evt.parm()) << ")"
                    << std::endl;
                std::cout
                    << "'" << emitter->name() << "' ----> '" << node.name() << "'"
                    << std::endl;
                for (int i = 0; i < parms.size(); ++i)
                    std::cout
                        << "[" << parms[i].first << "] " << (int)parms[i].second
                        << std::endl;

                char active = 0;
                emitter->getParameter<char>(parm::globalInitialization(), active);
                node.getParameter<char>(parm::globalInitialization(), active);
                std::cout << "node: " << node.name() << " active ? " << (int)active << std::endl;
            }
        }
    };
}
}
}

TEST(GlobalParmTest, EventGoDownHierarchy)
{
    using namespace smks;

    scn::Scene::Ptr scene = scn::Scene::create();
    scene->setSharedResourceDatabase(g_resources);

    scn::Archive::Ptr  archive = scn::Archive::create("abc/event_traversal.abc", scene);
    scn::TreeNode::Ptr root = scn::test::getNodeByName(*scene, "root", false);

    scn::TreeNode::Ptr  group[] = {
        scn::test::getNodeByName(*scene, "group1", false),
        scn::test::getNodeByName(*scene, "group2", false)
    };
    scn::TreeNode::Ptr  pCube[] = {
        scn::test::getNodeByName(*scene, "pCube1", false),
        scn::test::getNodeByName(*scene, "pCube2", false),
    };
    scn::TreeNode::Ptr  pCubeShape[] = {
        scn::test::getNodeByName(*scene, "pCubeShape1", false),
        scn::test::getNodeByName(*scene, "pCubeShape2", false),
    };

    scn::TreeNode::Ptr node = nullptr; // used for intermediary nodes
    scn::test::TestNodeParmListener::Ptr handler = scn::test::TestNodeParmListener::create(*scene, pCubeShape[0]->id());
    pCubeShape[0]->registerObserver(handler);

    char active = 0;
    pCubeShape[0]->getParameter<char>(parm::globalInitialization(), active);
    EXPECT_EQ(active, xchg::INACTIVE);

    root->getParameter<char>(parm::initialization(), active);
    EXPECT_EQ(active, xchg::INACTIVE);
    group[0]->getParameter<char>(parm::initialization(), active);
    EXPECT_EQ(active, xchg::INACTIVE);
    pCube[0]->getParameter<char>(parm::initialization(), active);
    EXPECT_EQ(active, xchg::INACTIVE);
    pCubeShape[0]->getParameter<char>(parm::initialization(), active);
    EXPECT_EQ(active, xchg::INACTIVE);

    pCube[0]->setParameter<char>(parm::initialization(), xchg::ACTIVE);
    pCubeShape[0]->setParameter<char>(parm::initialization(), xchg::ACTIVE);
    root->setParameter<char>(parm::initialization(), xchg::ACTIVE);
    group[0]->setParameter<char>(parm::initialization(), xchg::ACTIVE);

    std::cout << std::endl;

    node = pCubeShape[0];
    while (node)
    {
        node->getParameter<char>(parm::initialization(), active);
        std::cout << "'" << node->name() << "' active ? " << (int)active << std::endl;
        node = node->parent().lock();
    }

    pCubeShape[0]->getParameter<char>(parm::globalInitialization(), active);
    std::cout << "\n\nglobal initialization = " << (int)active << std::endl;

    scn::Scene::destroy(scene);
}
