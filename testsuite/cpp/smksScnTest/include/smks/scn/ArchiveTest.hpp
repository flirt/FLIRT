// ========================================================================= //
// Copyright 2016-2018 SUPAMONKS_STUDIO                                      //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
//                                                                           //
// ========================================================================= //
#pragma once

#include <gtest/gtest.h>

#include <smks/scn/Scene.hpp>
#include <smks/scn/Archive.hpp>

#include <smks/math/matrixAlgo.hpp>
#include <std/to_string_xchg.hpp>

#include "smks/scn/test/util.hpp"
#include "smks/scn/test/TestScene.hpp"

TEST(ArchiveTest, GlobalInitStateDefault)
{
    using namespace smks;

    scn::Scene::Ptr     scene   = scn::Scene::create();
    scene->setSharedResourceDatabase(g_resources);

    scn::Archive::Ptr   archive = scn::Archive::create("abc/basic_scene.abc", scene);

    scn::TreeNode::Ptr xform = scn::test::getNodeByName(*scene, "scene", false);
    scn::TreeNode::Ptr mesh = scn::test::getNodeByName(*scene, "sphereShape", false);
    scn::TreeNode::Ptr curves = scn::test::getNodeByName(*scene, "curveShape", false);
    scn::TreeNode::Ptr camera = scn::test::getNodeByName(*scene, "cameraShape1", false);
    scn::TreeNode::Ptr node = nullptr; // used for intermediary nodes

    ASSERT_NE(xform, nullptr);
    ASSERT_NE(mesh, nullptr);
    ASSERT_NE(curves, nullptr);
    ASSERT_NE(camera, nullptr);

    char state;
    bool status;

    // by default, all should be globally inactive
    status = xform->getParameter<char>(parm::globalInitialization(), state);
    EXPECT_TRUE(status);
    EXPECT_EQ(state, xchg::INACTIVE);

    status = mesh->getParameter<char>(parm::globalInitialization(), state);
    EXPECT_TRUE(status);
    EXPECT_EQ(state, xchg::INACTIVE);

    status = curves->getParameter<char>(parm::globalInitialization(), state);
    EXPECT_TRUE(status);
    EXPECT_EQ(state, xchg::INACTIVE);

    status = camera->getParameter<char>(parm::globalInitialization(), state);
    EXPECT_TRUE(status);
    EXPECT_EQ(state, xchg::INACTIVE);

    // progressively activate them alongside their ancestors
    xform->setParameter<char>(parm::initialization(), xchg::ACTIVE);
    xform->getParameter<char>(parm::globalInitialization(), state);
    EXPECT_EQ(state, xchg::ACTIVE);

    if (node = scn::test::getNodeByName(*scene, "sphere", false))
        node->setParameter<char>(parm::initialization(), xchg::ACTIVE);
    mesh->getParameter<char>(parm::globalInitialization(), state);
    EXPECT_EQ(state, xchg::INACTIVE);
    mesh->setParameter<char>(parm::initialization(), xchg::ACTIVE);
    mesh->getParameter<char>(parm::globalInitialization(), state);
    EXPECT_EQ(state, xchg::ACTIVE);

    if (node = scn::test::getNodeByName(*scene, "camera_test", false))
        node->setParameter<char>(parm::initialization(), xchg::ACTIVE);
    if (node = scn::test::getNodeByName(*scene, "camera1", false))
        node->setParameter<char>(parm::initialization(), xchg::ACTIVE);
    camera->getParameter<char>(parm::globalInitialization(), state);
    EXPECT_EQ(state, xchg::INACTIVE);
    camera->setParameter<char>(parm::initialization(), xchg::ACTIVE);
    camera->getParameter<char>(parm::globalInitialization(), state);
    EXPECT_EQ(state, xchg::ACTIVE);

    if (node = scn::test::getNodeByName(*scene, "curve", false))
        node->setParameter<char>(parm::initialization(), xchg::ACTIVE);
    curves->getParameter<char>(parm::globalInitialization(), state);
    EXPECT_EQ(state, xchg::INACTIVE);
    curves->setParameter<char>(parm::initialization(), xchg::ACTIVE);
    curves->getParameter<char>(parm::globalInitialization(), state);
    EXPECT_EQ(state, xchg::ACTIVE);

    scn::Scene::destroy(scene);
}

TEST(ArchiveTest, InitializationParmEventsToScene)
{
    using namespace smks;

    scn::test::EventHistory     evts;
    std::vector<size_t>         idx;
    scn::test::TestScene::Ptr   scene   = scn::test::TestScene::create();

    scene->setSharedResourceDatabase(g_resources);

    scn::Archive::Ptr  archive = scn::Archive::create("abc/single_xform.abc", scene);
    scn::TreeNode::Ptr xform   = scn::test::getNodeByName(*scene, "scene", false);
    ASSERT_NE(xform, nullptr);

    char state;
    bool status;

    std::cout << "parm::numSamples() = " << parm::numSamples().id() << std::endl;
    std::cout << "parm::currentlyVisible() = " << parm::currentlyVisible().id() << std::endl;
    std::cout << "parm::initialization() = " << parm::initialization().id() << std::endl;
    std::cout << "parm::transform() = " << parm::transform().id() << std::endl;
    std::cout << "parm::globalInitialization() = " << parm::globalInitialization().id() << std::endl;
    // by default, all should be globally inactive
    //---------
    scene->flushHistory();
    //---
    status = xform->setParameter<char>(parm::initialization(), xchg::ACTIVE);
    //---
    scene->flushHistory(&evts);
    std::cout << evts << std::endl;
    //---------

    ////---------
    //scene->flushHistory();
    ////---
    //status = xform->setParameter<char>(parm::initialization(), xchg::INACTIVE);
    ////---
    //scene->flushHistory(&evts);
    //std::cout << evts << std::endl;
    ////---------

    //---------
    scene->flushHistory();
    //---
    scn::TreeNode::destroy(xform);
    //---
    scene->flushHistory(&evts);
    for (size_t i = 0; i < evts.size(); ++i)
        if (evts[i].asParmEvent() &&
            parm::builtIns().find(evts[i].asParmEvent()->parm()))
            std::cout
                << "[" << i << "] = " << parm::builtIns().find(evts[i].asParmEvent()->parm())->c_str()
                << " " << std::to_string(evts[i].asParmEvent()->type())
                << std::endl;
    std::cout << evts << std::endl;
    //---------

    //EXPECT_TRUE(status);
    //EXPECT_EQ(state, xchg::INACTIVE);
    //
    //// progressively activate them alongside their ancestors
    //xform->setParameter<char>(parm::initialization(), xchg::ACTIVE);
    //xform->getParameter<char>(parm::globalInitialization(), state);
    //EXPECT_EQ(state, xchg::ACTIVE);

    //if (node = scn::test::getNodeByName(*scene, "sphere"))
    //  node->setParameter<char>(parm::initialization(), xchg::ACTIVE);
    //mesh->getParameter<char>(parm::globalInitialization(), state);
    //EXPECT_EQ(state, xchg::INACTIVE);
    //mesh->setParameter<char>(parm::initialization(), xchg::ACTIVE);
    //mesh->getParameter<char>(parm::globalInitialization(), state);
    //EXPECT_EQ(state, xchg::ACTIVE);

    //if (node = scn::test::getNodeByName(*scene, "camera_test"))
    //  node->setParameter<char>(parm::initialization(), xchg::ACTIVE);
    //if (node = scn::test::getNodeByName(*scene, "camera1"))
    //  node->setParameter<char>(parm::initialization(), xchg::ACTIVE);
    //camera->getParameter<char>(parm::globalInitialization(), state);
    //EXPECT_EQ(state, xchg::INACTIVE);
    //camera->setParameter<char>(parm::initialization(), xchg::ACTIVE);
    //camera->getParameter<char>(parm::globalInitialization(), state);
    //EXPECT_EQ(state, xchg::ACTIVE);

    //if (node = scn::test::getNodeByName(*scene, "curve"))
    //  node->setParameter<char>(parm::initialization(), xchg::ACTIVE);
    //curves->getParameter<char>(parm::globalInitialization(), state);
    //EXPECT_EQ(state, xchg::INACTIVE);
    //curves->setParameter<char>(parm::initialization(), xchg::ACTIVE);
    //curves->getParameter<char>(parm::globalInitialization(), state);
    //EXPECT_EQ(state, xchg::ACTIVE);

    scn::Scene::destroy(scene);
}

TEST(ArchiveTest, ConstantVisibility)
{
    using namespace smks;

    scn::test::TestScene::Ptr scene = scn::test::TestScene::create();
    scene->setSharedResourceDatabase(g_resources);

    scn::Archive::Ptr archive = scn::Archive::create("abc/constant_visibility.abc", scene);
    scn::test::initializeDescendants(true, *archive);

    scn::TreeNode::Ptr const& always_shown = scn::test::getNodeByName(*scene, "always_shown", false);
    scn::TreeNode::Ptr const& always_hidden = scn::test::getNodeByName(*scene, "always_hidden", false);
    assert(always_shown);
    assert(always_hidden);

    char parm1c;

    always_shown->getParameter<char>(parm::currentlyVisible(), parm1c);
    EXPECT_NE(parm1c, 0);
    always_hidden->getParameter<char>(parm::currentlyVisible(), parm1c);
    EXPECT_EQ(parm1c, 0);
}

TEST(ArchiveTest, OverrideConstantVisibilityPriorInit)
{
    using namespace smks;

    scn::test::TestScene::Ptr scene = scn::test::TestScene::create();
    scene->setSharedResourceDatabase(g_resources);

    scn::Archive::Ptr archive = scn::Archive::create("abc/constant_visibility.abc", scene);

    scn::TreeNode::Ptr const& always_shown = scn::test::getNodeByName(*scene, "always_shown", false);
    scn::TreeNode::Ptr const& always_hidden = scn::test::getNodeByName(*scene, "always_hidden", false);
    assert(always_shown);
    assert(always_hidden);

    //-------
    always_shown->setParameter<char>(parm::forciblyVisible(), 0);
    always_hidden->setParameter<char>(parm::forciblyVisible(), 1);
    //-------
    scn::test::initializeDescendants(true, *archive);

    char parm1c;

    always_shown->getParameter<char>(parm::currentlyVisible(), parm1c);
    EXPECT_EQ(parm1c, 0);
    always_hidden->getParameter<char>(parm::currentlyVisible(), parm1c);
    EXPECT_NE(parm1c, 0);
}

TEST(ArchiveTest, OverrideConstantVisibilityPostInit)
{
    using namespace smks;

    scn::test::TestScene::Ptr scene = scn::test::TestScene::create();
    scene->setSharedResourceDatabase(g_resources);

    scn::Archive::Ptr archive = scn::Archive::create("abc/constant_visibility.abc", scene);

    scn::TreeNode::Ptr const& always_shown = scn::test::getNodeByName(*scene, "always_shown", false);
    scn::TreeNode::Ptr const& always_hidden = scn::test::getNodeByName(*scene, "always_hidden", false);
    assert(always_shown);
    assert(always_hidden);

    scn::test::initializeDescendants(true, *archive);
    //-------
    always_shown->setParameter<char>(parm::forciblyVisible(), 0);
    always_hidden->setParameter<char>(parm::forciblyVisible(), 1);
    //-------

    char parm1c;

    always_shown->getParameter<char>(parm::currentlyVisible(), parm1c);
    EXPECT_EQ(parm1c, 0);
    always_hidden->getParameter<char>(parm::currentlyVisible(), parm1c);
    EXPECT_NE(parm1c, 0);
}

TEST(ArchiveTest, RemoveVisibilityOverride)
{
    using namespace smks;

    scn::test::TestScene::Ptr scene = scn::test::TestScene::create();
    scene->setSharedResourceDatabase(g_resources);

    scn::Archive::Ptr archive = scn::Archive::create("abc/constant_visibility.abc", scene);

    scn::TreeNode::Ptr const& always_shown = scn::test::getNodeByName(*scene, "always_shown", false);
    scn::TreeNode::Ptr const& always_hidden = scn::test::getNodeByName(*scene, "always_hidden", false);
    assert(always_shown);
    assert(always_hidden);

    scn::test::initializeDescendants(true, *archive);

    char parm1c;

    always_shown->setParameter<char>(parm::forciblyVisible(), 0);
    always_hidden->setParameter<char>(parm::forciblyVisible(), 1);

    always_shown->getParameter<char>(parm::currentlyVisible(), parm1c);
    EXPECT_EQ(parm1c, 0);
    always_hidden->getParameter<char>(parm::currentlyVisible(), parm1c);
    EXPECT_NE(parm1c, 0);
    //--- set irrelevant visibility override value
    always_shown->setParameter<char>(parm::forciblyVisible(), -1);
    always_hidden->setParameter<char>(parm::forciblyVisible(), -1);
    //---
    always_shown->getParameter<char>(parm::currentlyVisible(), parm1c);
    EXPECT_NE(parm1c, 0);
    always_hidden->getParameter<char>(parm::currentlyVisible(), parm1c);
    EXPECT_EQ(parm1c, 0);

    always_shown->setParameter<char>(parm::forciblyVisible(), 0);
    always_hidden->setParameter<char>(parm::forciblyVisible(), 1);

    always_shown->getParameter<char>(parm::currentlyVisible(), parm1c);
    EXPECT_EQ(parm1c, 0);
    always_hidden->getParameter<char>(parm::currentlyVisible(), parm1c);
    EXPECT_NE(parm1c, 0);
    //--- unset visibility override value
    always_shown->unsetParameter(parm::forciblyVisible());
    always_hidden->unsetParameter(parm::forciblyVisible());
    //---
    always_shown->getParameter<char>(parm::currentlyVisible(), parm1c);
    EXPECT_NE(parm1c, 0);
    always_hidden->getParameter<char>(parm::currentlyVisible(), parm1c);
    EXPECT_EQ(parm1c, 0);
}

TEST(ArchiveTest, AnimatedVisibility)
{
    using namespace smks;

    scn::test::TestScene::Ptr scene = scn::test::TestScene::create();
    scene->setSharedResourceDatabase(g_resources);

    scn::Archive::Ptr archive = scn::Archive::create("abc/constant_visibility.abc", scene);

    scn::test::initializeDescendants(true, *archive);

    scn::TreeNode::Ptr const& shown_then_hidden = scn::test::getNodeByName(*scene, "shown_then_hidden", false);
    scn::TreeNode::Ptr const& hidden_then_shown = scn::test::getNodeByName(*scene, "hidden_then_shown", false);
    assert(shown_then_hidden);
    assert(hidden_then_shown);

    const float fps = 25.0f;
    const float time1 = 1.0f / fps;
    const float time2 = 5.0f / fps;
    char        parm1c;

    scene->setParameter<float>(parm::currentTime(), time1);
    shown_then_hidden->getParameter<char>(parm::currentlyVisible(), parm1c);
    EXPECT_NE(parm1c, 0);
    hidden_then_shown->getParameter<char>(parm::currentlyVisible(), parm1c);
    EXPECT_EQ(parm1c, 0);


    scene->setParameter<float>(parm::currentTime(), time2);
    shown_then_hidden->getParameter<char>(parm::currentlyVisible(), parm1c);
    EXPECT_EQ(parm1c, 0);
    hidden_then_shown->getParameter<char>(parm::currentlyVisible(), parm1c);
    EXPECT_NE(parm1c, 0);
}

TEST(ArchiveTest, OverrideAnimatedVisibility)
{
    using namespace smks;

    scn::test::TestScene::Ptr scene = scn::test::TestScene::create();
    scene->setSharedResourceDatabase(g_resources);

    scn::Archive::Ptr archive = scn::Archive::create("abc/constant_visibility.abc", scene);

    scn::test::initializeDescendants(true, *archive);

    scn::TreeNode::Ptr const& shown_then_hidden = scn::test::getNodeByName(*scene, "shown_then_hidden", false);
    scn::TreeNode::Ptr const& hidden_then_shown = scn::test::getNodeByName(*scene, "hidden_then_shown", false);
    assert(shown_then_hidden);
    assert(hidden_then_shown);

    const float fps = 25.0f;
    const float time1 = 1.0f / fps;
    const float time2 = 5.0f / fps;
    char        parm1c;

    //-------
    shown_then_hidden->setParameter<char>(parm::forciblyVisible(), 0);
    hidden_then_shown->setParameter<char>(parm::forciblyVisible(), 1);
    //-------
    scene->setParameter<float>(parm::currentTime(), time1);
    shown_then_hidden->getParameter<char>(parm::currentlyVisible(), parm1c);
    EXPECT_EQ(parm1c, 0);
    hidden_then_shown->getParameter<char>(parm::currentlyVisible(), parm1c);
    EXPECT_NE(parm1c, 0);

    //-------
    shown_then_hidden->setParameter<char>(parm::forciblyVisible(), 1);
    hidden_then_shown->setParameter<char>(parm::forciblyVisible(), 0);
    //-------
    scene->setParameter<float>(parm::currentTime(), time2);
    shown_then_hidden->getParameter<char>(parm::currentlyVisible(), parm1c);
    EXPECT_NE(parm1c, 0);
    hidden_then_shown->getParameter<char>(parm::currentlyVisible(), parm1c);
    EXPECT_EQ(parm1c, 0);
}
