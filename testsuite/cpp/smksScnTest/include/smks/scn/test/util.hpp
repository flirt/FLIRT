// ========================================================================= //
// Copyright 2016-2018 SUPAMONKS_STUDIO                                      //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
//                                                                           //
// ========================================================================= //
#pragma once

#include <sstream>
#include <vector>
#include <boost/unordered_set.hpp>

#include <smks/xchg/IdSet.hpp>
#include <smks/xchg/SceneEvent.hpp>
#include <smks/xchg/ParmEvent.hpp>

#include <smks/util/string/getId.hpp>
#include <smks/util/string/path.hpp>
#include <smks/math/matrixAlgo.hpp>
#include <smks/scn/Scene.hpp>
#include <smks/scn/Archive.hpp>
#include <smks/scn/TreeNodeVisitor.hpp>
#include <smks/scn/TreeNodeFilter.hpp>

#include <smks/parm/BuiltIns.hpp>

#include "smks/scn/test/TestNode.hpp"
#include "smks/scn/test/ParmObserver.hpp"
#include "g_resources.hpp"

namespace smks { namespace parm { namespace test
{
    std::string
    getParameterName(unsigned int);
}
}
}

namespace smks { namespace xchg { namespace test
{
    inline
    bool
    equal(const xchg::IdSet& a, unsigned int b)
    {
        xchg::IdSet b_;
        b_.insert(b);
        return xchg::equal<xchg::IdSet>(a, b_);
    }
    inline
    bool
    equal(const xchg::IdSet& a, unsigned int b1, unsigned int b2)
    {
        xchg::IdSet b;
        b.insert(b1);
        b.insert(b2);
        return xchg::equal<xchg::IdSet>(a, b);
    }
    template<unsigned int ArraySize> inline
    bool
    equal(const xchg::IdSet& a, const unsigned int* b)
    {
        xchg::IdSet b_;
        for (unsigned int i = 0; i < ArraySize; ++i)
            b_.insert(b[i]);
        return xchg::equal<xchg::IdSet>(a, b_);
    }
}
}
}

namespace smks { namespace math { namespace test
{
    inline
    math::Vector4f
    getRandomPoint(float factor, bool zeroAllowed)
    {
        math::Vector4f ret(math::Vector4f::Random() * factor);
        ret[3] = 1.0f;
        if (!zeroAllowed)
        {
            for (size_t i = 0; i < 3; ++i)
                if (abs(ret[i]) < 1e-6f)
                    ret[i] += 0.1f;
        }
        return ret;
    }

    inline
    math::Vector4f
    getRandomDirection()
    {
        math::Vector4f ret(math::Vector4f::Random());
        ret[3] = 0.0f;
        ret.normalize();
        return ret;
    }

    inline
    math::Affine3f
    getRandomTransform()
    {
        math::Affine3f xfm;
        math::lookAt(xfm,
            getRandomPoint(5.0f, true),
            getRandomDirection(),
            getRandomDirection(),
            getRandomPoint(5.0f, false));
        return xfm;
    }
}
}
}

namespace smks { namespace scn { namespace test
{
    inline
    TreeNode::Ptr
    getNodeByName(const Scene&  scene,
                  const char*   query,
                  bool          isQueryRegex,
                  bool          useFullName = false,
                  unsigned int  fromId = 0)
    {
        xchg::IdSet ids;
        Scene::IdGetterOptions opts;

        opts.accumulate(false);
        if (fromId)
            opts.fromId(fromId);
        opts.filter(FilterTreeNodeByName::create(query, isQueryRegex, useFullName));
        scene.getIds(ids, &opts);
        if (ids.size() > 1)
        {
            std::stringstream sstr;
            sstr
                << "Query '" << query << "' "
                << (isQueryRegex ? "(as regex) " : "")
                << "did not return unique result.";
            throw sys::Exception(sstr.str().c_str(), "Node Getter By Name");
        }
        return ids.empty()
            ? nullptr
            : scene.descendantById(*ids.begin()).lock();
    }

    template<typename ValueType> inline
    ValueType
    getNodeParameter(const parm::BuiltIn&   parm,
                     const Scene&           scene,
                     const char*            nodeName,
                     bool                   useFullName = false)
    {
        if (!parm.compatibleWith<ValueType>())
        {
            std::stringstream sstr;
            sstr
                << "Incompatible built-in parameter "
                << "'" << parm::test::getParameterName(parm.id()) << "'."
                << std::endl;
            throw sys::Exception(sstr.str().c_str(), "Node Parameter Getter");
        }

        TreeNode::Ptr const& node = getNodeByName(scene, nodeName, false, useFullName);
        if (!node)
        {
            std::stringstream sstr;
            sstr
                << "Failed to find node '" << nodeName << "' in scene."
                << std::endl;
            throw sys::Exception(sstr.str().c_str(), "Node Parameter Getter");
        }

        ValueType ret;
        if (!node->getParameter<ValueType>(parm, ret))
            std::cerr
                << "Failed to find parameter "
                << "'" << parm::test::getParameterName(parm.id()) << "'"
                << " on node '" << nodeName << "'."
                << std::endl;
        return ret;
    }

    inline
    void
    findFileRelativelyToSourceFile(const char*          _FILE_,
                                   const std::string&   relPath,
                                   std::string&         oPath)
    {
        oPath.clear();
        if (_FILE_ == nullptr ||
            strlen(_FILE_) == 0)
            return;

        oPath = std::string(_FILE_);
        std::replace(oPath.begin(), oPath.end(), '\\', '/');
        size_t lastSep = oPath.find_last_of('/');
        if (lastSep != std::string::npos)
            oPath = oPath.substr(0, lastSep);
        oPath += '/' + relPath;
        std::replace(oPath.begin(), oPath.end(), '\\', '/');
    }

    inline
    bool
    isChildOf(TreeNode::Ptr const& n, TreeNode::Ptr const& parent)
    {
        if (n && parent)
            for (size_t i = 0; i < parent->numChildren(); ++i)
                if (parent->child(i)->id() == n->id())
                    return true;
        return false;
    }

    inline
    bool
    isParentOf(TreeNode::Ptr const& n, TreeNode::Ptr const& child)
    {
        TreeNode::Ptr const& parent = child->parent().lock();
        return n && parent
            ? parent->id() == n->id()
            : false;
    }

    inline
    bool
    retrievableFromScene(TreeNode::Ptr const& n, TreeNode::Ptr const& scene)
    {
        if (!n || !scene || !scene->asScene() || !scene->asScene()->hasDescendant(n->id()))
            return false;

        TreeNode::Ptr const& desc = scene->asScene()->descendantById(n->id()).lock();

        return n.get() == desc.get();
    }

    inline
    std::ostream&
    printNodes(std::ostream& out, const xchg::IdSet& ids, Scene::Ptr const& scene)
    {
        for (xchg::IdSet::const_iterator id = ids.begin(); id != ids.end(); ++id)
        {
            out << "[" << *id << "]";
            if (scene &&
                scene->hasDescendant(*id))
            {
                TreeNode::Ptr const& desc = scene->descendantById(*id).lock();
                if (desc)
                    out << " = '" << desc->fullName() << "'";
            }
            out << "\t";
        }
        return out;
    }

    inline
    void
    initializeDescendants(bool initialize, TreeNode& node)
    {
        node.setParameter<char>(
            parm::initialization(),
            initialize ? xchg::ACTIVE : xchg::INACTIVE);

        for (size_t i = 0; i < node.numChildren(); ++i)
            if (node.child(i))
                initializeDescendants(initialize, *node.child(i));
    }

    template<typename ValueType> inline
    void
    getParentParameters(
        const parm::BuiltIn&    parm,
        const TreeNode&         node,
        std::vector<std::pair<unsigned int, ValueType>>& ret)
    {
        ret.clear();

        const TreeNode* n = &node;
        while (n)
        {
            ValueType value;
            n->getParameter<ValueType>(parm, value);
            ret.push_back(std::pair<unsigned int, ValueType>(n->id(), value));
            n = n->parent().lock().get();
        }
    }

    void
    createXformBranchBelowNode(TreeNode::Ptr const&,
                               size_t,
                               std::vector<TreeNode::Ptr>&);

    ///////////////////////
    // struct SceneContent1
    ///////////////////////
    struct SceneContent1
    {
        Scene::Ptr scene;
        TestNode::Ptr
            node_a,         node_b,
            node_a_b,       node_a_aa,      node_a_ab, node_a_aa_a,
            node_b_a,       node_b_ba,      node_b_bb,
            node_b_bb_bb1,  node_b_bb_bb2;

        /*  scene
        *   - a                         - b
        *   -- b    -- aa   --ab        -- ba   -- bb               -- a
        *           --- a                       --- bb1 --- bb2
        */

        explicit inline
        SceneContent1(const xchg::IdSet& relevantBuiltins = xchg::IdSet()):
            scene        (Scene::create()),
            node_a       (TestNode::create("a",     scene,      relevantBuiltins)),
            node_b       (TestNode::create("b",     scene,      relevantBuiltins)),
            node_a_b     (TestNode::create("b",     node_a,     relevantBuiltins)),
            node_a_aa    (TestNode::create("aa",    node_a,     relevantBuiltins)),
            node_a_ab    (TestNode::create("ab",    node_a,     relevantBuiltins)),
            node_a_aa_a  (TestNode::create("a",     node_a_aa,  relevantBuiltins)),
            node_b_a     (TestNode::create("a",     node_b,     relevantBuiltins)),
            node_b_ba    (TestNode::create("ba",    node_b,     relevantBuiltins)),
            node_b_bb    (TestNode::create("bb",    node_b,     relevantBuiltins)),
            node_b_bb_bb1(TestNode::create("bb1",   node_b_bb,  relevantBuiltins)),
            node_b_bb_bb2(TestNode::create("bb2",   node_b_bb,  relevantBuiltins))
        { }

        inline
        ~SceneContent1()
        {
            TreeNode::destroy(scene);
        }
    };
    ///////////////////////

    ///////////////////////
    // struct SceneContent2
    ///////////////////////
    struct SceneContent2
    {
        Scene::Ptr      scene;
        TestNode::Ptr   node_a, node_a_b1, node_a_b2, node_a_b1_c;
        /*  scene
        *   - a
        *   -- b1   -- b2
        *   --- c
        */
        TestNodes   nodes;

        explicit inline
        SceneContent2(const xchg::IdSet& relevantBuiltins = xchg::IdSet()):
            scene        (Scene::create()),
            node_a       (TestNode::create("a",     scene,      relevantBuiltins)),
            node_a_b1    (TestNode::create("b1",    node_a,     relevantBuiltins)),
            node_a_b2    (TestNode::create("b2",    node_a,     relevantBuiltins)),
            node_a_b1_c  (TestNode::create("c",     node_a_b1,  relevantBuiltins))
        {
            nodes.push_back(node_a);
            nodes.push_back(node_a_b1);
            nodes.push_back(node_a_b2);
            nodes.push_back(node_a_b1_c);
        }

        inline
        ~SceneContent2()
        {
            TreeNode::destroy(scene);
        }
    };
    ///////////////////////

    ///////////////////////
    // struct SceneContent3
    ///////////////////////
    struct SceneContent3
    {
        Scene::Ptr      scene;
        TestNode::Ptr   node1, node2, node3_a, node4_a, node3_b, node4_b;
        /*  scene
        *   - node1
        *   -- node2
        *   --- node3_a     --- node3_b
        *   ---- node4_a    ---- node4_b
        */
        TestNodes   nodes;

        explicit inline
        SceneContent3(const xchg::IdSet& relevantBuiltins = xchg::IdSet()):
            scene   (Scene::create()),
            node1   (test::TestNode::create("node1",    scene,      relevantBuiltins)),
            node2   (test::TestNode::create("node2",    node1,      relevantBuiltins)),
            node3_a (test::TestNode::create("node3_a",  node2,      relevantBuiltins)),
            node4_a (test::TestNode::create("node4_a",  node3_a,    relevantBuiltins)),
            node3_b (test::TestNode::create("node3_b",  node2,      relevantBuiltins)),
            node4_b (test::TestNode::create("node4_b",  node3_b,    relevantBuiltins))
        {
            nodes.push_back(node1);
            nodes.push_back(node2);
            nodes.push_back(node3_a);
            nodes.push_back(node4_a);
            nodes.push_back(node3_b);
            nodes.push_back(node4_b);
        }

        inline
        void
        flushAllEvents()
        {
            for (size_t i = 0; i < nodes.size(); ++i)
                nodes[i]->flushHistory();
        }

        inline
        ~SceneContent3()
        {
            TreeNode::destroy(scene);
        }
    };
    ///////////////////////

    ///////////////////////
    // struct SceneContent4
    ///////////////////////
    struct SceneContent4
    {
        Scene::Ptr      scene;
        TestNode::Ptr   node_a, node_aa, node_aaa, node_b, node_bb, node_c, node_d, node_e;
        /*  scene
        *   - node_a        - node_b    - node_c    - node_d    - node_e
        *   -- node_aa      -- node_bb
        *   --- node_aaa
        */
        TestNodes   nodes;

        explicit inline
        SceneContent4(const xchg::IdSet& relevantBuiltins = xchg::IdSet()):
            scene   (Scene::create()),
            node_a  (test::TestNode::create("node_a",   scene,      relevantBuiltins)),
            node_aa (test::TestNode::create("node_aa",  node_a,     relevantBuiltins)),
            node_aaa(test::TestNode::create("node_aaa", node_aa,    relevantBuiltins)),
            node_b  (test::TestNode::create("node_b",   scene,      relevantBuiltins)),
            node_bb (test::TestNode::create("node_bb",  node_b,     relevantBuiltins)),
            node_c  (test::TestNode::create("node_c",   scene,      relevantBuiltins)),
            node_d  (test::TestNode::create("node_d",   scene,      relevantBuiltins)),
            node_e  (test::TestNode::create("node_e",   scene,      relevantBuiltins))
        {
            nodes.push_back(node_a);
            nodes.push_back(node_aa);
            nodes.push_back(node_aaa);
            nodes.push_back(node_b);
            nodes.push_back(node_bb);
            nodes.push_back(node_c);
            nodes.push_back(node_d);
            nodes.push_back(node_e);

            // links: node_b <- node_a, node_c <- node_b, node_aaa <- node_c, node_d <- node_aaa, node_e <- node_aa
            const char* link = "link";
            node_b->setParameterId(link, node_a->id());
            node_c->setParameterId(link, node_b->id());
            node_aaa->setParameterId(link, node_c->id());
            node_d->setParameterId(link, node_aaa->id());
            node_e->setParameterId(link, node_aa->id());
        }

        inline
        void
        flushAllEvents()
        {
            for (size_t i = 0; i < nodes.size(); ++i)
                nodes[i]->flushHistory();
        }

        inline
        ~SceneContent4()
        {
            TreeNode::destroy(scene);
        }
    };
    ///////////////////////

    /////////////////////////
    // struct UserAttribScene
    /////////////////////////
    struct UserAttribScene
    {
        Scene::Ptr      scene;
        Archive::Ptr    abc;
        TreeNode::Ptr   pCube1;
    public:
        inline
        UserAttribScene():
            scene   (Scene::create()),
            abc     (nullptr),
            pCube1  (nullptr)
        {
            scene->setSharedResourceDatabase(g_resources);

            abc = Archive::create("abc/user_attribs.abc", scene);
            xchg::IdSet                 ids;
            scn::Scene::IdGetterOptions opts;

            opts.accumulate(false);
            opts.filter(scn::FilterTreeNodeByName::create("pCube1", false));
            scene->getIds(ids, &opts);
            if (ids.size() == 1 &&
                scene->hasDescendant(*ids.begin()))
                pCube1 = scene->descendantById(*ids.begin()).lock();
            assert(pCube1);
        }

        inline
        ~UserAttribScene()
        {
            TreeNode::destroy(scene);
        }
    };
    ///////////////////////

    //////////////////////////////
    // struct InheritedAttribScene
    //////////////////////////////
    struct InheritedAttribScene
    {
        Scene::Ptr      scene;
    public:
        inline
        InheritedAttribScene():
            scene   (Scene::create())
        {
        }

        inline
        ~InheritedAttribScene()
        {
            TreeNode::destroy(scene);
        }
    };
    ///////////////////////

    inline
    std::ostream&
    dumpLocalGlobalInitializationState(std::ostream& out, TreeNode::Ptr const& node, size_t depth = 0)
    {
        if (!node)
            return out;

        char state, globalState;
        node->getParameter<char>(parm::initialization(), state);
        node->getParameter<char>(parm::globalInitialization(), globalState);
        for (size_t d = 0; d < depth; ++d)
            out << "  ";
        out << node->name() << ": " << std::to_string(state) << " " << std::to_string(globalState) << std::endl;

        for (size_t i = 0; i < node->numChildren(); ++i)
            dumpLocalGlobalInitializationState(out, node->child(i), depth + 1);

        return out;
    }
} } }
