// ========================================================================= //
// Copyright 2016-2018 SUPAMONKS_STUDIO                                      //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
//                                                                           //
// ========================================================================= //
#pragma once

#include <functional>
#include <memory>
#include <smks/xchg/equal.hpp>

namespace smks { namespace scn { namespace test
{
    ////////////////////////
    // struct CompareParm<T>
    ////////////////////////
    template <typename ValueType>
    struct CompareParm:
        public std::binary_function<const ValueType&, const ValueType&, bool>
    {
    public:
        virtual
        ~CompareParm()
        { }

        virtual
        bool
        operator()(const ValueType&, const ValueType&) = 0;
    };
    ////////////////////////////

    /////////////////////
    // struct EqualParm<T>
    /////////////////////
    template <typename ValueType>
    struct EqualParm:
        public CompareParm<ValueType>
    {
    public:
        typedef std::shared_ptr<EqualParm> Ptr;
    public:
        static inline
        Ptr
        create()
        {
            Ptr ptr(new EqualParm());
            return ptr;
        }
    protected:
        inline
        EqualParm()
        { }
    public:
        inline
        bool
        operator()(const ValueType& a, const ValueType& b)
        {
            return xchg::equal<ValueType>(a, b);
        }
    };
    /////////////////////
}
}
}
