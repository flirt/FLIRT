// ========================================================================= //
// Copyright 2016-2018 SUPAMONKS_STUDIO                                      //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
//                                                                           //
// ========================================================================= //
#pragma once

#include <functional>
#include <smks/parm/Any.hpp>

namespace smks { namespace scn { namespace test
{
    ////////////////////////
    // class DefaultOverride
    ////////////////////////
    class DefaultOverride:
        public std::binary_function<unsigned int, parm::Any&, bool>
    {
    public:
        typedef std::shared_ptr<DefaultOverride> Ptr;
    public:
        virtual
        ~DefaultOverride()
        { }

        virtual
        bool
        operator()(unsigned int, parm::Any&) = 0;
    };

    ////////////////////////////////////////
    // class TypedDefaultOverride<ValueType>
    ////////////////////////////////////////
    template<typename ValueType>
    class TypedDefaultOverride:
        public DefaultOverride
    {
    public:
        typedef std::shared_ptr<TypedDefaultOverride> Ptr;
    private:
        const unsigned int  _parmId;
        const parm::Any     _parmDefault;
    public:
        static inline
        Ptr
        create(unsigned int     parmId,
               const ValueType& parmDefault)
        {
            Ptr ptr(new TypedDefaultOverride(parmId, parmDefault));
            return ptr;
        }
    protected:
        inline
        TypedDefaultOverride(unsigned int       parmId,
                             const ValueType&   parmDefault):
            _parmId     (parmId),
            _parmDefault(parmDefault)
        { }
    public:
        inline
        bool
        operator()(unsigned int parmId, parm::Any& any)
        {
            if (parmId == _parmId)
            {
                any = _parmDefault;
                return true;
            }
            return false;
        }
    };
}
}
}
