// ========================================================================= //
// Copyright 2016-2018 SUPAMONKS_STUDIO                                      //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
//                                                                           //
// ========================================================================= //
#pragma once

#include <vector>
#include "smks/scn/test/ParmPreprocessor.hpp"

namespace smks { namespace scn { namespace test
{
    //////////////////////
    // class TestProcessor
    //////////////////////
    class TestPreprocessor:
        public ParmPreprocessor
    {
    public:
        typedef std::shared_ptr<TestPreprocessor> Ptr;
    private:
        std::vector<std::pair<unsigned int, parm::ParmFlags>> _parmFlags;
    public:
        static inline Ptr create()
        {
            Ptr ptr(new TestPreprocessor());
            return ptr;
        }
    private:
        TestPreprocessor(): _parmFlags() { }
    public:
        inline
        void
        flush()
        {
            _parmFlags.clear();
        }
        inline
        bool
        operator()(const char*, unsigned int parmId, parm::ParmFlags f, const parm::Any&)
        {
            _parmFlags.push_back(std::pair<unsigned int, parm::ParmFlags>(parmId, f));
            return false;   // does not skip anything
        }
        inline
        size_t
        numParmFlags() const
        {
            return _parmFlags.size();
        }
        inline
        unsigned int
        parm(size_t idx) const
        {
            assert(idx < _parmFlags.size());
            return _parmFlags[idx].first;
        }
        inline
        parm::ParmFlags
        flags(size_t idx) const
        {
            assert(idx < _parmFlags.size());
            return _parmFlags[idx].second;
        }
    };
}
}
}
