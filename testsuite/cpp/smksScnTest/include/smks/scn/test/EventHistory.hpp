// ========================================================================= //
// Copyright 2016-2018 SUPAMONKS_STUDIO                                      //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
//                                                                           //
// ========================================================================= //
#pragma once

#include <vector>
#include <smks/util/string/getId.hpp>
#include "smks/scn/test/EventEntry.hpp"
#include "smks/scn/test/EventHistory.hpp"

namespace smks { namespace scn { namespace test
{
    /////////////////////
    // class EventHistory
    /////////////////////
    class EventHistory:
        public std::vector<EventEntry>
    {
    public:
        inline
        EventHistory():
            std::vector<EventEntry>()
        {
            reserve(128);
        }

        inline
        std::vector<size_t>
        getSceneEvents(xchg::SceneEvent::Type typ = xchg::SceneEvent::UNKNOWN) const
        {
            std::vector<size_t> ids;
            for (size_t i = 0; i < size(); ++i)
                if ((*this)[i].asSceneEvent() &&
                    (typ == xchg::SceneEvent::UNKNOWN || (*this)[i].asSceneEvent()->type() == typ))
                    ids.push_back(i);
            return ids;
        }

        inline
        std::vector<size_t>
        getParmEvents(const std::string&    parmName,
                      unsigned int          node    = 0,
                      xchg::ParmEvent::Type typ     = xchg::ParmEvent::UNKNOWN) const
        {
            return getParmEvents(
                util::string::getId(parmName.c_str()),
                node,
                typ);
        }

        inline
        std::vector<size_t>
        getParmEvents(xchg::ParmEvent::Type typ) const
        {
            return getParmEvents(0, 0, typ);
        }

        inline
        std::vector<size_t>
        getParmEvents(unsigned int          parm    = 0,
                      unsigned int          node    = 0,
                      xchg::ParmEvent::Type typ     = xchg::ParmEvent::UNKNOWN) const
        {
            std::vector<size_t> ids;
            for (size_t i = 0; i < (*this).size(); ++i)
                if ((*this)[i].asParmEvent() &&
                    (parm   == 0                        || (*this)[i].asParmEvent()->parm() == parm) &&
                    (node   == 0                        || (*this)[i].asParmEvent()->node() == node) &&
                    (typ    == xchg::ParmEvent::UNKNOWN || (*this)[i].asParmEvent()->type() == typ))
                    ids.push_back(i);
            return ids;
        }

        friend inline
        std::ostream&
        operator<<(std::ostream& out, const EventHistory& h)
        {
            for (size_t i = 0; i < h.size(); ++i)
                out << h[i] << "\n";
            return out;
        }
    };
    ///////////////////////////
}
}
}
