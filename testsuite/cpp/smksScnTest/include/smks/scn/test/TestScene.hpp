// ========================================================================= //
// Copyright 2016-2018 SUPAMONKS_STUDIO                                      //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
//                                                                           //
// ========================================================================= //
#pragma once

#include <smks/scn/Scene.hpp>
#include "smks/scn/test/EventHistory.hpp"

namespace smks { namespace scn { namespace test
{
    ////////////////////////
    // class EventHistoryScene
    ////////////////////////
    class TestScene:
        public Scene
    {
    public:
        typedef std::shared_ptr<TestScene> Ptr;
    private:
        EventHistory _history;

    public:
        static inline
        Ptr
        create(const char* json = "")
        {
            Ptr ptr(new TestScene(json));
            ptr->build(ptr);
            return ptr;
        }

        static inline
        void
        destroy(Ptr& ptr)
        {
            TreeNode::destroy(std::static_pointer_cast<TreeNode>(ptr));
        }

        inline
        void
        flushHistory(EventHistory* history = nullptr)
        {
            if (history)
                *history = _history;
            _history.clear();
        }

        inline
        const EventEntry&
        operator[](size_t i) const
        {
            assert(i < _history.size());
            return _history[i];
        }

    protected:
        inline
        TestScene(const char* json):
            Scene   (json),
            _history()
        { }

        inline
        void
        handleSceneEvent(const xchg::SceneEvent& evt)
        {
            Scene::handleSceneEvent(evt);
            //---
            _history.push_back(EventEntry::create(evt));
        }

        inline
        void
        handleParmEvent(const xchg::ParmEvent& evt)
        {
            Scene::handleParmEvent(evt);
            //---
            _history.push_back(EventEntry::create(evt));
        }
    };
    //////////////////
}
}
}
