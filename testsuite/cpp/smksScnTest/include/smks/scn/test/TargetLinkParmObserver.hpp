// ========================================================================= //
// Copyright 2016-2018 SUPAMONKS_STUDIO                                      //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
//                                                                           //
// ========================================================================= //
#pragma once

#include <smks/scn/Scene.hpp>
#include <smks/xchg/SceneEvent.hpp>
#include "smks/scn/test/SceneObserver.hpp"

namespace smks { namespace scn { namespace test
{
    ///////////////////////////////
    // class TargetLinkParmObserver
    ///////////////////////////////
    class TargetLinkParmObserver:
        public SceneObserver
    {
    public:
        typedef std::shared_ptr<TargetLinkParmObserver> Ptr;
    private:
        const Scene::Ptr                _scene;
        const std::string               _linkName;
        const xchg::SceneEvent::Type    _eventFilter;
        const bool                      _shouldParmExist;
        //------------------------------
        bool _testOK;
    public:
        static inline
        Ptr
        create(const char*              linkName,
               xchg::SceneEvent::Type   eventFilter,
               bool                     shouldParmExist)
        {
            Ptr ptr(new TargetLinkParmObserver(linkName, eventFilter, shouldParmExist));
            return ptr;
        }
    private:
        inline
        TargetLinkParmObserver(const std::string&       linkName,
                               xchg::SceneEvent::Type   eventFilter,
                               bool                     shouldParmExist):
            SceneObserver   (),
            _linkName       (linkName),
            _eventFilter    (eventFilter),
            _shouldParmExist(shouldParmExist),
            _testOK         (false)
        {
            if (eventFilter != xchg::SceneEvent::NODES_LINKED &&
                eventFilter != xchg::SceneEvent::NODES_UNLINKED)
                throw sys::Exception("Unsupported scene event filter.", "Target Link Observer");
        }

        inline
        void
        handle(Scene& scene, const xchg::SceneEvent& evt)
        {
            SceneObserver::handle(scene, evt);
            //---
            if (evt.type() != _eventFilter)
                return;
            assert(evt.type() == xchg::SceneEvent::NODES_LINKED ||
                evt.type() == xchg::SceneEvent::NODES_UNLINKED);

            TreeNode::Ptr const& target = scene.hasDescendant(evt.target())
                ? scene.descendantById(evt.target()).lock()
                : nullptr;

            if (!target || evt.type() != _eventFilter)
                return;

            _testOK = (target->parameterExistsAsId(_linkName.c_str()) == _shouldParmExist);
        }
    public:
        inline
        bool
        ok() const
        {
            return _testOK;
        }
    };
}
}
}
