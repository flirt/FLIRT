// ========================================================================= //
// Copyright 2016-2018 SUPAMONKS_STUDIO                                      //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
//                                                                           //
// ========================================================================= //
#pragma once

#include <smks/scn/AbstractSceneObserver.hpp>
#include "smks/scn/test/EventHistory.hpp"

namespace smks { namespace scn { namespace test
{
    class SceneObserver:
        public AbstractSceneObserver
    {
    public:
        typedef std::shared_ptr<SceneObserver> Ptr;
    private:
        EventHistory _history;
    public:
        static
        Ptr
        create()
        {
            Ptr ptr(new SceneObserver);
            return ptr;
        }
    protected:
        SceneObserver():
            _history()
        { }

        virtual
        void
        handle(scn::Scene&, const xchg::SceneEvent& evt)
        {
            _history.push_back(EventEntry::create(evt));
        }
    public:
        void
        flushHistory(EventHistory* history = nullptr)
        {
            if (history)
                *history = _history;
            _history.clear();
        }
    };
}
}
}
