// ========================================================================= //
// Copyright 2016-2018 SUPAMONKS_STUDIO                                      //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
//                                                                           //
// ========================================================================= //
#pragma once

#include <smks/xchg/SceneEvent.hpp>
#include <smks/xchg/ParmEvent.hpp>

namespace smks { namespace scn { namespace test
{
    ////////////////////
    // struct EventEntry
    ////////////////////
    struct EventEntry
    {
    public:
        xchg::SceneEvent*   _sceneEvent;
        xchg::ParmEvent*    _parmEvent;

    public:
        static inline
        EventEntry
        create(const xchg::ParmEvent& evt)
        {
            return EventEntry(evt);
        }

        static inline
        EventEntry
        create(const xchg::SceneEvent& evt)
        {
            return EventEntry(evt);
        }

    protected:
        explicit inline
        EventEntry(const xchg::ParmEvent& evt):
            _sceneEvent(nullptr),
            _parmEvent(new xchg::ParmEvent(evt))
        { }

        explicit inline
        EventEntry(const xchg::SceneEvent& evt):
            _sceneEvent(new xchg::SceneEvent(evt)),
            _parmEvent(nullptr)
        { }

    public:
        inline
        EventEntry():
            _sceneEvent(nullptr),
            _parmEvent(nullptr)
        { }

        inline
        EventEntry(const EventEntry& other):
            _sceneEvent(nullptr),
            _parmEvent(nullptr)
        {
            copy(other);
        }

        inline
        EventEntry&
        operator=(const EventEntry& other)
        {
            copy(other);
            return *this;
        }

        inline
        ~EventEntry()
        {
            clear();
        }

        inline
        const xchg::SceneEvent*
        asSceneEvent() const
        {
            return _sceneEvent;
        }

        inline
        const xchg::ParmEvent*
        asParmEvent() const
        {
            return _parmEvent;
        }

        friend inline
        std::ostream&
        operator<<(std::ostream& out, const EventEntry& e)
        {
            if (e.asSceneEvent())
                return out << *e.asSceneEvent();
            else if (e.asParmEvent())
                return out << *e.asParmEvent();
            else
                return out;
        }

    private:
        inline
        void
        clear()
        {
            if (_sceneEvent)
                delete _sceneEvent;
            _sceneEvent = nullptr;
            if (_parmEvent)
                delete _parmEvent;
            _parmEvent = nullptr;
        }

        inline
        void
        copy(const EventEntry& other)
        {
            clear();

            if (other._sceneEvent)
                _sceneEvent = new xchg::SceneEvent(*other._sceneEvent);
            if (other._parmEvent)
                _parmEvent = new xchg::ParmEvent(*other._parmEvent);
        }
    };
    ///////////////////////////
}
}
}
