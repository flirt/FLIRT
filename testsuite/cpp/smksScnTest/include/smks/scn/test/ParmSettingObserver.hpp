// ========================================================================= //
// Copyright 2016-2018 SUPAMONKS_STUDIO                                      //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
//                                                                           //
// ========================================================================= //
#pragma once

#include "smks/scn/test/ParmEditingObserverBase.hpp"

namespace smks { namespace scn { namespace test
{
    //-----------------------------
    // class ParmSettingObserver<T>
    //-----------------------------
    template<typename ValueType>
    class ParmSettingObserver:
        public ParmEditingObserverBase
    {
    public:
        typedef std::shared_ptr<ParmSettingObserver> Ptr;
    private:
        const std::string   _curParm;
        const std::string   _newParm;
        const ValueType     _curParmValue;
        const ValueType     _newParmValue;
        const bool          _testParmValue;
    public:
        static inline Ptr create(const char*        curParm,
                                 const char*        newParm,
                                 const ValueType&   newParmValue)
        {
            Ptr ptr(new ParmSettingObserver(
                curParm,
                ValueType(),
                newParm,
                newParmValue,
                false));
            return ptr;
        }
        static inline Ptr create(const char*        curParm,
                                 const ValueType&   curParmValue,
                                 const char*        newParm,
                                 const ValueType&   newParmValue)
        {
            Ptr ptr(new ParmSettingObserver(
                curParm,
                curParmValue,
                newParm,
                newParmValue,
                true));
            return ptr;
        }
    private:
        inline
        ParmSettingObserver(const char*         curParm,
                            const ValueType&    curParmValue,
                            const char*         newParm,
                            const ValueType&    newParmValue,
                            bool                testParmValue):
            ParmEditingObserverBase (),
            _curParm                (curParm),
            _curParmValue           (curParmValue),
            _newParm                (newParm),
            _newParmValue           (newParmValue),
            _testParmValue          (testParmValue)
        { }
        inline
        void
        handle(scn::TreeNode& node, const xchg::ParmEvent& evt)
        {
            ParmEditingObserverBase::handle(node, evt);
            //-----------------------
            const unsigned int curParmId = util::string::getId(_curParm.c_str());
            const unsigned int newParmId = util::string::getId(_newParm.c_str());

            bool doEdit = evt.parm() == curParmId;
            if (_testParmValue)
            {
                ValueType curParmValue;

                node.getParameter<ValueType>(_curParm.c_str(), curParmValue);
                doEdit = xchg::equal<ValueType>(curParmValue, _curParmValue);
            }
            if (!doEdit)
                return;
            //---
            if (_useDelayedEdits)
            {
                test::TestNode* testNode = dynamic_cast<test::TestNode*>(&node);
                assert(testNode);
                ParmEditingObserverBase::delay(
                    *testNode,
                    scn::ParmSetEdit<ValueType>::create(
                        _newParm.c_str(),
                        newParmId,
                        _newParmValue,
                        parm::NO_PARM_FLAG));
            }
            else
                node.setParameter<ValueType>(_newParm.c_str(), _newParmValue);
        }
    };
    //---------------------
}
}
}
