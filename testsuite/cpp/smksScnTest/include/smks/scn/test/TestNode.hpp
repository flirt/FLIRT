// ========================================================================= //
// Copyright 2016-2018 SUPAMONKS_STUDIO                                      //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
//                                                                           //
// ========================================================================= //
#pragma once

#include <smks/xchg/IdSet.hpp>
#include <smks/scn/TreeNode.hpp>
#include "smks/scn/test/EventHistory.hpp"

namespace smks { namespace scn { namespace test
{
    class DefaultOverride;
    class CustomParmHandler;
    class ParmPreprocessor;

    /////////////////////////
    // class TestNode
    /////////////////////////
    class TestNode:
        public TreeNode
    {
        friend class ParmEditingObserverBase;
    public:
        typedef std::shared_ptr<TestNode> Ptr;
    private:
        typedef std::shared_ptr<DefaultOverride>                        DefaultOverridePtr;
        typedef boost::unordered_map<unsigned int, DefaultOverridePtr>  DefaultOverrideMap;
        typedef std::shared_ptr<ParmPreprocessor>                       PreprocessorPtr;
        typedef std::vector<PreprocessorPtr>                            PreprocessorPtrs;
    private:
        //--------------
        EventHistory*       _history;
        bool                _ownsHistory;
        //--------------
        xchg::IdSet         _relevantBuiltins;  //<! necessary to declare additional *built-in* parameters as relevant
        xchg::IdSet         _readonlyParms;     //<! parameters flagged as read-only
        xchg::IdSet         _redirectToRoot;    //<! parameters that are redirected to the root
        DefaultOverrideMap  _defaultOverrides;
        PreprocessorPtrs    _preprocessors;
    public:
        static
        Ptr
        create(const char*, TreeNode::WPtr const&, EventHistory* = nullptr);

        static
        Ptr
        create(const char*, TreeNode::WPtr const&, const xchg::IdSet& relevant, EventHistory* = nullptr);

        static
        Ptr
        create(const char*, TreeNode::WPtr const&, unsigned int relevant, EventHistory* = nullptr);

        static
        Ptr
        create(const char*, TreeNode::WPtr const&, const std::string& relevant, EventHistory* = nullptr);

        static
        Ptr
        create(const char*, TreeNode::WPtr const&, const std::vector<std::string>& relevant, EventHistory* = nullptr);

    protected:
        TestNode(const char*, TreeNode::WPtr const&, EventHistory*);
    public:
        ~TestNode();
    protected:
        virtual
        void
        build(Ptr const&, const xchg::IdSet& relevantParms = xchg::IdSet());

    public:
        virtual
        bool
        isParameterRelevant(unsigned int) const;

        virtual
        bool
        isParameterWritable(unsigned int) const;

    protected:
        virtual
        bool
        preprocessParameterEdit(const char*, unsigned int, parm::ParmFlags, const parm::Any&);
        virtual
        bool
        overrideParameterDefaultValue(unsigned int, parm::Any&) const;

    private:
        bool
        isParameterRelevantForClass(unsigned int) const;

    public:
        void makeReadonly(unsigned int);
        void makeReadonly(const std::string&);
        void makeReadonly(const std::vector<std::string>&);
        void makeRedirectToRoot(unsigned int);
        void makeRedirectToRoot(const std::string&);
        void makeRedirectToRoot(const std::vector<std::string>&);

        void setDefaultOverride (unsigned int, DefaultOverridePtr const&);
        void setCustomGetter    (unsigned int, scn::AbstractParmGetter::Ptr const&);
        void addPreprocessor    (PreprocessorPtr const&);

        void
        flushHistory(EventHistory* = nullptr);

    protected:
        void
        handleSceneEvent(const xchg::SceneEvent&);

        void
        handleParmEvent(const xchg::ParmEvent&);

        bool
        mustSendToScene(const xchg::ParmEvent&) const;
    private:
        void
        throwErrorIfBuiltInNotRelevant(unsigned int parmId, const char*) const;
    };
    /////////////////////////
    typedef std::vector<TestNode::Ptr> TestNodes;
}
}
}
