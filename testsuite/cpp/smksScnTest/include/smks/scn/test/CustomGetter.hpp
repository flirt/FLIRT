// ========================================================================= //
// Copyright 2016-2018 SUPAMONKS_STUDIO                                      //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
//                                                                           //
// ========================================================================= //
#pragma once

#include <smks/scn/ParmGetter.hpp>

namespace smks { namespace scn { namespace test
{
    /////////////////////////
    // struct CustomGetter<T>
    /////////////////////////
    template<typename ValueType>
    struct CustomGetter:
        public TypedParmGetter<ValueType>
    {
    public:
        typedef std::shared_ptr<CustomGetter> Ptr;
    private:
        const bool      _ret;
        const ValueType _val;
    public:
        static inline
        Ptr
        create()
        {
            Ptr ptr(new CustomGetter());
            return ptr;
        }
        static inline
        Ptr
        create(ValueType val)
        {
            Ptr ptr(new CustomGetter(val));
            return ptr;
        }
    public:
        inline
        CustomGetter():
            TypedParmGetter<ValueType>(),
            _ret(false),
            _val()
        { }

        explicit inline
        CustomGetter(float val):
            TypedParmGetter<ValueType>(),
            _ret(true),
            _val(val)
        { }

        inline
        const ValueType&
        value() const
        {
            return _val;
        }

        inline
        bool
        operator()(const parm::BuiltIn&, float& val, unsigned int)
        {
            val = _val;
            return _ret;
        }
    };
    //////////////////////
}
}
}
