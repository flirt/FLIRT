// ========================================================================= //
// Copyright 2016-2018 SUPAMONKS_STUDIO                                      //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
//                                                                           //
// ========================================================================= //
#pragma once

#include <smks/scn/TreeNode.hpp>
#include "smks/scn/test/ParmObserver.hpp"

namespace smks { namespace scn { namespace test
{
    //-------------------------------
    // class ParmValueQueryObserver<T>
    //-------------------------------
    template<typename ValueType>
    class ParmValueQueryObserver:
        public ParmObserver
    {
    public:
        typedef std::shared_ptr<ParmValueQueryObserver> Ptr;
    private:
        typedef std::pair<xchg::ParmEvent::Type, ValueType> EventTypeAndParmValue;

        const std::string                   _parmName;
        std::vector<EventTypeAndParmValue>  _typesAndValues;
    public:
        static inline
        Ptr
        create(const char* parmName)
        {
            Ptr ptr(new ParmValueQueryObserver(parmName));
            return ptr;
        }
    protected:
        inline
        ParmValueQueryObserver(const char* parmName):
            ParmObserver    (),
            _parmName       (parmName),
            _typesAndValues ()
        { }
    public:
        inline
        void
        flushTypesAndValues()
        {
            _typesAndValues.clear();
        }
        inline
        size_t
        numTypesAndValues() const
        {
            return _typesAndValues.size();
        }
        inline
        xchg::ParmEvent::Type
        eventType(size_t i) const
        {
            assert(i < _typesAndValues.size());
            return _typesAndValues[i].first;
        }
        inline
        const ValueType&
        parmValue(size_t i) const
        {
            assert(i < _typesAndValues.size());
            return _typesAndValues[i].second;
        }
    protected:
        inline
        void handle(TreeNode& node, const xchg::ParmEvent& evt)
        {
            ParmObserver::handle(node, evt);
            //---
            if (evt.node() != node.id())
                return;

            assert(!node.parameterExists(_parmName.c_str()) ||
                node.parameterExistsAs<ValueType>(_parmName.c_str()) );

            EventTypeAndParmValue entry;

            entry.first = evt.type();
            node.getParameter<ValueType>(_parmName.c_str(), entry.second);
            _typesAndValues.push_back(entry);
        }
    };
    //---------------------
}
}
}
