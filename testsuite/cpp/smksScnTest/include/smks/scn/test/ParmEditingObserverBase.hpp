// ========================================================================= //
// Copyright 2016-2018 SUPAMONKS_STUDIO                                      //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
//                                                                           //
// ========================================================================= //
#pragma once

#include <smks/scn/ParmEdit.hpp>
#include "smks/scn/test/TestNode.hpp"
#include "smks/scn/test/ParmObserver.hpp"

namespace smks { namespace scn { namespace test
{
    class TestNode;

    //-------------------------------
    // class ParmEditingObserverBase
    //-------------------------------
    class ParmEditingObserverBase:
        public ParmObserver
    {
    public:
        typedef std::shared_ptr<ParmEditingObserverBase> Ptr;
    protected:
        bool _useDelayedEdits;
    protected:
        inline
        ParmEditingObserverBase():
            ParmObserver    (),
            _useDelayedEdits(false)
        { }
    public:
        inline bool useDelayedEdits() const     { return _useDelayedEdits; }
        inline void useDelayedEdits(bool value) { _useDelayedEdits = value; }
    protected:
        inline
        void
        delay(TestNode& n, ParmEdit::Ptr const& edit) // used for inherited friendship
        {
            assert(_useDelayedEdits);
            n.delay(edit);
        }
    };
    //---------------------
}
}
}
