// ========================================================================= //
// Copyright 2016-2018 SUPAMONKS_STUDIO                                      //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
//                                                                           //
// ========================================================================= //
#pragma once

#include <smks/xchg/IdSet.hpp>
#include <smks/scn/TreeNodeVisitor.hpp>
#include "smks/scn/test/CompareParm.hpp"

namespace smks { namespace scn { namespace test
{
    ////////////////////////////
    // class FindNodesWithParm<T>
    ////////////////////////////
    template <typename ValueType>
    class FindNodesWithParm:
        public TreeNodeVisitor
    {
    private:
        typedef std::shared_ptr<CompareParm<ValueType>> ComparePtr;
    private:
        std::string _parmName;
        ValueType   _parmValue;
        ComparePtr  _compare;
        xchg::IdSet _found;

    public:
        inline
        FindNodesWithParm():
            TreeNodeVisitor (Direction::DESCENDING),
            _parmName       (),
            _parmValue      (),
            _compare        (nullptr),
            _found          ()
        { }

        explicit inline
        FindNodesWithParm(const std::string& parmName):
            TreeNodeVisitor (Direction::DESCENDING),
            _parmName       (parmName),
            _parmValue      (),
            _compare        (nullptr),
            _found          ()
        { }

        inline
        const xchg::IdSet&
        found() const
        {
            return _found;
        }

        inline
        void
        clear()
        {
            _found.clear();
        }

        inline
        void
        initialize(const std::string& parmName)
        {
            clear();
            _parmName   = parmName;
            _parmValue  = ValueType();
            _compare    = nullptr;
        }

        inline
        void
        initialize(const std::string&   parmName,
                   const ValueType&     parmValue,
                   ComparePtr const&    valueFilter)
        {
            clear();
            _parmName   = parmName;
            _parmValue  = parmValue;
            _compare    = valueFilter;
        }

        virtual inline
        void
        apply(TreeNode& n)
        {
            if (!_parmName.empty() &&
                n.parameterExistsAs<ValueType>(_parmName.c_str()))
            {
                bool record = false;

                if (_compare)
                {
                    ValueType val;
                    n.getParameter<ValueType>(_parmName.c_str(), val);
                    record = (*_compare)(val, _parmValue);
                }
                else
                    record = true;

                if (record)
                    _found.insert(n.id());
            }

            traverse(n);
        }
    };
    ////////////////////////////
}
}
}
