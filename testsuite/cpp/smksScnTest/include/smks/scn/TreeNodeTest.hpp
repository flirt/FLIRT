// ========================================================================= //
// Copyright 2016-2018 SUPAMONKS_STUDIO                                      //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
//                                                                           //
// ========================================================================= //
#pragma once

#include <gtest/gtest.h>

#include <smks/parm/builtins.hpp>
#include <smks/xchg/Data.hpp>
#include <smks/xchg/DataStream.hpp>
#include <smks/scn/ParmAssign.hpp>
#include <smks/scn/TreeNodeSelector.hpp>
#include "smks/scn/test/util.hpp"
#include "smks/scn/test/CustomGetter.hpp"
#include "smks/scn/test/DefaultOverride.hpp"
#include "smks/scn/test/ParmSettingObserver.hpp"
#include "smks/scn/test/ParmValueQueryObserver.hpp"
#include "smks/scn/test/TestPreprocessor.hpp"



TEST(TreeNodeTest, ParmExistsAsId)
{
    using namespace smks;

    const parm::BuiltIn&        builtIn = parm::materialId();
    scn::Scene::Ptr             scene   = scn::Scene::create();
    scn::test::TestNode::Ptr    node    = scn::test::TestNode::create("_node_", scene, xchg::IdSet(builtIn.id()));
    scn::test::TestNode::Ptr    source  = scn::test::TestNode::create("_source_", scene);
    //---
    node->setParameter<unsigned int>(builtIn, source->id());
    EXPECT_TRUE(node->parameterExistsAsId(builtIn.c_str()));
    EXPECT_TRUE(node->parameterExistsAsId(builtIn.id()));
    //---
    const char* parmName = "parm";
    const unsigned int parm = node->setParameterId(parmName, source->id());
    EXPECT_TRUE(node->parameterExistsAsId(parmName));
    EXPECT_TRUE(node->parameterExistsAsId(parm));
    //---
    const char* otherParmName = "other_parm";
    const unsigned int otherParm = node->setParameter<unsigned int>(otherParmName, source->id());
    EXPECT_FALSE(node->parameterExistsAsId(otherParmName));
    EXPECT_FALSE(node->parameterExistsAsId(otherParm));
    //---
    scn::Scene::destroy(scene);
}

TEST(TreeNodeTest, ParmExistsAsSwitch)
{
    using namespace smks;

    const parm::BuiltIn&        builtIn = parm::initialization();
    scn::Scene::Ptr             scene   = scn::Scene::create();
    scn::test::TestNode::Ptr    node    = scn::test::TestNode::create("_node_", scene, xchg::IdSet(builtIn.id()));
    //---
    node->setParameter<char>(builtIn, xchg::ACTIVE);
    EXPECT_TRUE(node->parameterExistsAsSwitch(builtIn.c_str()));
    EXPECT_TRUE(node->parameterExistsAsSwitch(builtIn.id()));
    //---
    const char* parmName = "parm";
    const unsigned int parm = node->setParameterSwitch(parmName, true);
    EXPECT_TRUE(node->parameterExistsAsSwitch(parmName));
    EXPECT_TRUE(node->parameterExistsAsSwitch(parm));
    //---
    const char* otherParmName = "other_parm";
    const unsigned int otherParm = node->setParameter<char>(otherParmName, xchg::ACTIVE);
    EXPECT_FALSE(node->parameterExistsAsId(otherParmName));
    EXPECT_FALSE(node->parameterExistsAsId(otherParm));
    //---
    scn::Scene::destroy(scene);
}


TEST(TreeNodeTest, ParmUpdateErr_UnknownParm)
{
    using namespace smks;

    scn::Scene::Ptr             scene   = scn::Scene::create();
    scn::test::TestNode::Ptr    node    = scn::test::TestNode::create("_node_", scene);

    bool caught = false;
    try
    {
        node->updateParameter<std::string>(rand(), "foo");
    }
    catch(const sys::Exception&)
    {
        caught = true;
    }
    EXPECT_TRUE(caught);

    scn::Scene::destroy(scene);
}

TEST(TreeNodeTest, ParmUpdateErr_BadParmType)
{
    using namespace smks;

    scn::Scene::Ptr             scene   = scn::Scene::create();
    scn::test::TestNode::Ptr    node    = scn::test::TestNode::create("_node_", scene);

    bool caught = false;
    try
    {
        const unsigned int parm = node->setParameter<int>("parm", 42);
        node->updateParameter<std::string>(parm, "foo");
    }
    catch(const sys::Exception&)
    {
        caught = true;
    }
    EXPECT_TRUE(caught);

    scn::Scene::destroy(scene);
}

TEST(TreeNodeTest, ParmUpdateErr_BadBuiltInType)
{
    using namespace smks;

    scn::Scene::Ptr             scene   = scn::Scene::create();
    scn::test::TestNode::Ptr    node    = scn::test::TestNode::create("_node_", scene);

    bool caught = false;
    try
    {
        node->updateParameter<std::string>(parm::numSamples().id(), "foo");
    }
    catch(const sys::Exception&)
    {
        caught = true;
    }
    EXPECT_TRUE(caught);

    scn::Scene::destroy(scene);
}

TEST(TreeNodeTest, PreprocessEditSeesFlagId)
{
    using namespace smks;

    const parm::BuiltIn&        builtIn     = parm::materialId();
    const char*                 parmName    = "parm";
    scn::Scene::Ptr             scene   = scn::Scene::create();
    scn::test::TestNode::Ptr    node    = scn::test::TestNode::create("_node_", scene, xchg::IdSet(builtIn.id()));
    scn::test::TestNode::Ptr    source1 = scn::test::TestNode::create("_source1_", scene);
    scn::test::TestNode::Ptr    source2 = scn::test::TestNode::create("_source2_", scene);

    scn::test::TestPreprocessor::Ptr preproc = scn::test::TestPreprocessor::create();
    node->addPreprocessor(preproc);
    //---
    preproc->flush();
    node->setParameter<unsigned int>(builtIn, source1->id());
    EXPECT_EQ(preproc->numParmFlags(), 1);
    EXPECT_EQ(preproc->parm(0), builtIn.id());
    EXPECT_EQ(preproc->flags(0) & parm::PARM_ROLE_FLAGS, parm::CREATES_LINK);
    //---
    preproc->flush();
    node->updateParameter<unsigned int>(builtIn.id(), source2->id());
    EXPECT_EQ(preproc->numParmFlags(), 1);
    EXPECT_EQ(preproc->parm(0), builtIn.id());
    EXPECT_EQ(preproc->flags(0) & parm::PARM_ROLE_FLAGS, parm::CREATES_LINK);
    //---
    preproc->flush();
    const unsigned int parm = node->setParameterId(parmName, source1->id());
    EXPECT_EQ(preproc->numParmFlags(), 1);
    EXPECT_EQ(preproc->parm(0), parm);
    EXPECT_EQ(preproc->flags(0) & parm::PARM_ROLE_FLAGS, parm::CREATES_LINK);
    //---
    preproc->flush();
    node->updateParameter<unsigned int>(parm, source2->id());
    EXPECT_EQ(preproc->numParmFlags(), 1);
    EXPECT_EQ(preproc->parm(0), parm);
    EXPECT_EQ(preproc->flags(0) & parm::PARM_ROLE_FLAGS, parm::CREATES_LINK);
    //---
    scn::Scene::destroy(scene);
}

TEST(TreeNodeTest, PreprocessEditSeesFlagSwitch)
{
    using namespace smks;

    const parm::BuiltIn&        builtIn     = parm::initialization();
    const char*                 parmName    = "parm";
    scn::Scene::Ptr             scene   = scn::Scene::create();
    scn::test::TestNode::Ptr    node    = scn::test::TestNode::create("_node_", scene, xchg::IdSet(builtIn.id()));

    scn::test::TestPreprocessor::Ptr preproc = scn::test::TestPreprocessor::create();
    node->addPreprocessor(preproc);
    //---
    preproc->flush();
    node->setParameter<char>(builtIn, xchg::ACTIVE);
    EXPECT_EQ(preproc->numParmFlags(), 1);
    EXPECT_EQ(preproc->parm(0), builtIn.id());
    EXPECT_EQ(preproc->flags(0) & parm::PARM_ROLE_FLAGS, parm::SWITCHES_STATE);
    //---
    preproc->flush();
    node->updateParameter<char>(builtIn.id(), xchg::INACTIVE);
    EXPECT_EQ(preproc->numParmFlags(), 1);
    EXPECT_EQ(preproc->parm(0), builtIn.id());
    EXPECT_EQ(preproc->flags(0) & parm::PARM_ROLE_FLAGS, parm::SWITCHES_STATE);
    //---
    preproc->flush();
    const unsigned int parm = node->setParameterSwitch(parmName, true);
    EXPECT_EQ(preproc->numParmFlags(), 1);
    EXPECT_EQ(preproc->parm(0), parm);
    EXPECT_EQ(preproc->flags(0) & parm::PARM_ROLE_FLAGS, parm::SWITCHES_STATE);
    //---
    preproc->flush();
    node->updateParameter<char>(parm, xchg::INACTIVE);
    EXPECT_EQ(preproc->numParmFlags(), 1);
    EXPECT_EQ(preproc->parm(0), parm);
    EXPECT_EQ(preproc->flags(0) & parm::PARM_ROLE_FLAGS, parm::SWITCHES_STATE);
    //---
    scn::Scene::destroy(scene);
}

TEST(TreeNodeTest, PreprocessEditSeesFlagTrigger)
{
    using namespace smks;

    const parm::BuiltIn&        builtIn     = parm::rendering();
    const char*                 parmName    = "parm";
    scn::Scene::Ptr             scene   = scn::Scene::create();
    scn::test::TestNode::Ptr    node    = scn::test::TestNode::create("_node_", scene, xchg::IdSet(builtIn.id()));

    scn::test::TestPreprocessor::Ptr preproc = scn::test::TestPreprocessor::create();
    node->addPreprocessor(preproc);
    //---
    preproc->flush();
    node->setParameter<char>(builtIn, xchg::ACTIVE);
    EXPECT_EQ(preproc->numParmFlags(), 1);
    EXPECT_EQ(preproc->parm(0), builtIn.id());
    EXPECT_EQ(preproc->flags(0) & parm::PARM_ROLE_FLAGS, parm::TRIGGERS_ACTION);
    //---
    preproc->flush();
    node->updateParameter<char>(builtIn.id(), xchg::ACTIVE);
    EXPECT_EQ(preproc->numParmFlags(), 1);
    EXPECT_EQ(preproc->parm(0), builtIn.id());
    EXPECT_EQ(preproc->flags(0) & parm::PARM_ROLE_FLAGS, parm::TRIGGERS_ACTION);
    //---
    preproc->flush();
    const unsigned int parm = node->setParameterTrigger(parmName);
    EXPECT_EQ(preproc->numParmFlags(), 1);
    EXPECT_EQ(preproc->parm(0), parm);
    EXPECT_EQ(preproc->flags(0) & parm::PARM_ROLE_FLAGS, parm::TRIGGERS_ACTION);
    //---
    scn::Scene::destroy(scene);
}


TEST(TreeNodeTest, LinkSourceGetter)
{
    using namespace smks;

    const unsigned int source1Id = util::string::getId("/_source1_");

    scn::Scene::Ptr     scene   = scn::Scene::create();
    scn::TreeNode::Ptr  target  = scn::TreeNode::create("_target_", scene);
    scn::TreeNode::Ptr  source2 = scn::TreeNode::create("_source2_", scene);
    scn::TreeNode::Ptr  source3 = scn::TreeNode::create("_source3_", scene);

    xchg::IdSet parmValue2;
    parmValue2.insert(source1Id);
    parmValue2.insert(source2->id());

    xchg::IdSet ids, refIds;

    //---
    const unsigned int parm1    = target->setParameterId            ("parm1", source1Id);
    const unsigned int parm2    = target->setParameter<xchg::IdSet> ("parm2", parmValue2);
    //---
    target->getLinkSources(ids);    // ids = { source2 } (source1 not created yet)
    EXPECT_TRUE(xchg::equal<xchg::IdSet>(ids, xchg::IdSet(source2->id())));
    //---
    scn::TreeNode::Ptr  source1 = scn::TreeNode::create("_source1_", scene);
    //---
    target->getLinkSources(ids);    // ids = { source1, source2 }
    refIds.clear(); refIds.insert(source1->id()); refIds.insert(source2->id());
    EXPECT_TRUE(xchg::equal<xchg::IdSet>(ids, refIds));
    //---
    scn::TreeNode::destroy(source1);
    //---
    target->getLinkSources(ids);    // ids = { source2 } (source1 has been destroyed)
    EXPECT_TRUE(xchg::equal<xchg::IdSet>(ids, xchg::IdSet(source2->id())));
    //---
    target->updateParameter(parm1, source3->id());
    //---
    target->getLinkSources(ids);    // ids = { source2, source3 }
    refIds.clear(); refIds.insert(source2->id()); refIds.insert(source3->id());
    EXPECT_TRUE(xchg::equal<xchg::IdSet>(ids, refIds));
    //---

    scn::Scene::destroy(scene);
}

TEST(TreeNodeTest, CyclicSelfLinking)
{
    using namespace smks;

    scn::Scene::Ptr             scene   = scn::Scene::create();
    scn::test::TestNode::Ptr    node_a  = scn::test::TestNode::create("_node_a_", scene);
    scn::test::EventHistory     evts;
    std::vector<size_t>         idx;

    //---
    node_a->setParameterId("link", node_a->id());   // self-link
    //---
    xchg::IdSet ids;

    node_a->getLinkSources(ids);
    EXPECT_TRUE(ids.empty());
    node_a->getLinkTargets(ids);
    EXPECT_TRUE(ids.empty());

    //---------
    node_a->flushHistory();
    //---
    const unsigned int parm = node_a->setParameter<std::string>("parm", "foo"); // only ADDED event, not linked one
    //---
    node_a->flushHistory(&evts);
    idx = evts.getParmEvents(parm);
    EXPECT_EQ(idx.size(), 1);
    const xchg::ParmEvent* parmEvt = evts[idx.front()].asParmEvent();
    EXPECT_EQ(parmEvt->node(), node_a->id());
    EXPECT_FALSE(parmEvt->comesFromLink());
    //---------

    scn::Scene::destroy(scene);
}

TEST(TreeNodeTest, CyclicLinkingViaSingleId)
{
    using namespace smks;

    scn::Scene::Ptr scene = scn::Scene::create();

    std::vector<scn::test::TestNode::Ptr> nodes;
    for (size_t i = 0; i < 3; ++i)
    {
        const std::string name = "_node_" + std::to_string(i) + "_";
        nodes.push_back(scn::test::TestNode::create(name.c_str(), scene));
    }

    const char* my_parm = "my_parm";

    //--------------------------------------------------
    // [0] <- [1], [1] <- [2], [2] <- [0]
    nodes[0]->setParameterId(my_parm, nodes[1]->id());
    nodes[1]->setParameterId(my_parm, nodes[2]->id());
    nodes[2]->setParameterId(my_parm, nodes[0]->id());  // would cause cycle
    //--------------------------------------------------

    xchg::IdSet ids;
    nodes[0]->getLinkSources(ids);
    EXPECT_TRUE(xchg::test::equal(ids, nodes[1]->id()));
    nodes[0]->getLinkTargets(ids);
    EXPECT_TRUE(ids.empty());   // link did not occur

    nodes[1]->getLinkSources(ids);
    EXPECT_TRUE(xchg::test::equal(ids, nodes[2]->id()));
    nodes[1]->getLinkTargets(ids);
    EXPECT_TRUE(xchg::test::equal(ids, nodes[0]->id()));

    nodes[2]->getLinkSources(ids);
    EXPECT_TRUE(ids.empty());   // link did not occur
    nodes[2]->getLinkTargets(ids);
    EXPECT_TRUE(xchg::test::equal(ids, nodes[1]->id()));

    scn::Scene::destroy(scene);
}

TEST(TreeNodeTest, AlmostCyclicLinkingViaIdSet)
{
    using namespace smks;

    scn::Scene::Ptr scene = scn::Scene::create();

    std::vector<scn::test::TestNode::Ptr> nodes;
    for (size_t i = 0; i < 3; ++i)
    {
        const std::string name = "_node_" + std::to_string(i) + "_";
        nodes.push_back(scn::test::TestNode::create(name.c_str(), scene));
    }

    const char* my_parm = "my_parm";

    //--------------------------------------------------
    // [1] <- [0], [1] <- [2], [2] <- [0],
    xchg::IdSet sources;
    sources.insert(nodes[0]->id());
    sources.insert(nodes[2]->id());
    nodes[1]->setParameter<xchg::IdSet>(my_parm, sources);
    nodes[2]->setParameterId(my_parm, nodes[0]->id());
    //--------------------------------------------------
    xchg::IdSet ids;
    nodes[0]->getLinkSources(ids);
    EXPECT_TRUE(ids.empty());
    nodes[0]->getLinkTargets(ids);
    EXPECT_TRUE(xchg::test::equal(ids, nodes[1]->id(), nodes[2]->id()));

    nodes[1]->getLinkSources(ids);
    EXPECT_TRUE(xchg::test::equal(ids, nodes[0]->id(), nodes[2]->id()));
    nodes[1]->getLinkTargets(ids);
    EXPECT_TRUE(ids.empty());

    nodes[2]->getLinkSources(ids);
    EXPECT_TRUE(xchg::test::equal(ids, nodes[0]->id()));
    nodes[2]->getLinkTargets(ids);
    EXPECT_TRUE(xchg::test::equal(ids, nodes[1]->id()));

    scn::Scene::destroy(scene);
}

TEST(TreeNodeTest, CyclicLinkingViaIdSet)
{
    using namespace smks;

    scn::Scene::Ptr scene = scn::Scene::create();

    std::vector<scn::test::TestNode::Ptr> nodes;
    for (size_t i = 0; i < 4; ++i)
    {
        const std::string name = "_node_" + std::to_string(i) + "_";
        nodes.push_back(scn::test::TestNode::create(name.c_str(), scene));
    }

    const char* my_parm = "my_parm";

    //--------------------------------------------------
    // [2] <- [0,1], [3] <- [2], [0] <- [3] (cycle: [0] -> [2] -> [3] -> [0])
    nodes[3]->setParameterId(my_parm, nodes[2]->id());
    nodes[0]->setParameterId(my_parm, nodes[3]->id());
    xchg::IdSet sources;
    sources.insert(nodes[0]->id());
    sources.insert(nodes[1]->id());
    nodes[2]->setParameter<xchg::IdSet>(my_parm, sources);  // note that while [2] <- [0] fails, [2] <- [1] succeeds.
    //--------------------------------------------------
    xchg::IdSet ids;

    nodes[0]->getLinkSources(ids);
    EXPECT_TRUE(xchg::test::equal(ids, nodes[3]->id()));
    nodes[0]->getLinkTargets(ids);
    EXPECT_TRUE(ids.empty());

    nodes[1]->getLinkSources(ids);
    EXPECT_TRUE(ids.empty());
    nodes[1]->getLinkTargets(ids);
    EXPECT_TRUE(xchg::test::equal(ids, nodes[2]->id()));

    nodes[2]->getLinkSources(ids);
    EXPECT_TRUE(xchg::test::equal(ids, nodes[1]->id()));
    nodes[2]->getLinkTargets(ids);
    EXPECT_TRUE(xchg::test::equal(ids, nodes[3]->id()));

    nodes[3]->getLinkSources(ids);
    EXPECT_TRUE(xchg::test::equal(ids, nodes[2]->id()));
    nodes[3]->getLinkTargets(ids);
    EXPECT_TRUE(xchg::test::equal(ids, nodes[0]->id()));

    scn::Scene::destroy(scene);
}

TEST(TreeNodeTest, CreatesLinkFromParmEvent)
{
    using namespace smks;

    std::vector<size_t> idx;
    scn::Scene::Ptr scene = scn::Scene::create();
    //---
    scn::test::EventHistory evts;
    scn::test::TestNode::Ptr target = scn::test::TestNode::create("target", scene);
    scn::TreeNode::Ptr source = scn::TreeNode::create("source", scene);

    const char* my_uint = "my_uint";
    //--
    target->setParameter<unsigned int>(my_uint, rand());
    //--
    target->flushHistory(&evts);
    idx = evts.getParmEvents(my_uint, target->id(), xchg::ParmEvent::PARM_ADDED);
    EXPECT_EQ(idx.size(), 1);
    EXPECT_FALSE(evts[idx.front()].asParmEvent()->isActiveLink());
    //--
    target->setParameter<unsigned int>(my_uint, source->id());
    //--
    target->flushHistory(&evts);
    idx = evts.getParmEvents(my_uint, target->id(), xchg::ParmEvent::PARM_CHANGED);
    EXPECT_EQ(idx.size(), 1);
    EXPECT_FALSE(evts[idx.front()].asParmEvent()->isActiveLink());
    //--
    target->unsetParameter(my_uint);
    //--
    target->flushHistory(&evts);
    idx = evts.getParmEvents(my_uint, target->id(), xchg::ParmEvent::PARM_REMOVED);
    EXPECT_EQ(idx.size(), 1);
    EXPECT_FALSE(evts[idx.front()].asParmEvent()->isActiveLink());

    const char* my_id = "my_id";
    //---
    target->setParameterId(my_id, rand());
    //---
    target->flushHistory(&evts);
    idx = evts.getParmEvents(my_id, target->id(), xchg::ParmEvent::PARM_ADDED);
    EXPECT_EQ(idx.size(), 1);
    EXPECT_TRUE(evts[idx.front()].asParmEvent()->isActiveLink());
    //---
    target->setParameterId(my_id, source->id());
    //---
    target->flushHistory(&evts);
    idx = evts.getParmEvents(my_id, target->id(), xchg::ParmEvent::PARM_CHANGED);
    EXPECT_EQ(idx.size(), 1);
    EXPECT_TRUE(evts[idx.front()].asParmEvent()->isActiveLink());
    //---
    target->unsetParameter(my_id);
    //---
    target->flushHistory(&evts);
    idx = evts.getParmEvents(my_id, target->id(), xchg::ParmEvent::PARM_REMOVED);
    EXPECT_EQ(idx.size(), 1);
    EXPECT_TRUE(evts[idx.front()].asParmEvent()->isActiveLink());

    const char* my_ids = "my_ids";
    xchg::IdSet ids;
    //---
    ids.clear(); ids.insert(rand());
    target->setParameter<xchg::IdSet>(my_ids, ids);
    //---
    target->flushHistory(&evts);
    idx = evts.getParmEvents(my_ids, target->id(), xchg::ParmEvent::PARM_ADDED);
    EXPECT_EQ(idx.size(), 1);
    EXPECT_TRUE(evts[idx.front()].asParmEvent()->isActiveLink());
    //---
    ids.clear(); ids.insert(source->id());
    target->setParameter<xchg::IdSet>(my_ids, ids);
    //---
    target->flushHistory(&evts);
    idx = evts.getParmEvents(my_ids, target->id(), xchg::ParmEvent::PARM_CHANGED);
    EXPECT_EQ(idx.size(), 1);
    EXPECT_TRUE(evts[idx.front()].asParmEvent()->isActiveLink());
    //---
    target->unsetParameter(my_ids);
    //---
    target->flushHistory(&evts);
    idx = evts.getParmEvents(my_ids, target->id(), xchg::ParmEvent::PARM_REMOVED);
    EXPECT_EQ(idx.size(), 1);
    EXPECT_TRUE(evts[idx.front()].asParmEvent()->isActiveLink());

    //---
    scn::Scene::destroy(scene);
}

TEST(TreeNodeTest, CreatesLinkFromLinkedParmEvent)
{
    using namespace smks;

    std::vector<size_t> idx;
    scn::Scene::Ptr scene = scn::Scene::create();
    //---
    scn::test::EventHistory evts;
    scn::test::TestNode::Ptr target = scn::test::TestNode::create("target", scene);
    scn::TreeNode::Ptr source = scn::TreeNode::create("source", scene);
    scn::TreeNode::Ptr other = scn::TreeNode::create("other", scene);

    target->setParameterId("my_link", source->id());

    const char* my_uint = "my_uint";
    //--
    source->setParameter<unsigned int>(my_uint, rand());
    //--
    target->flushHistory(&evts);
    idx = evts.getParmEvents(my_uint, source->id(), xchg::ParmEvent::PARM_ADDED);
    EXPECT_EQ(idx.size(), 1);
    EXPECT_FALSE(evts[idx.front()].asParmEvent()->isActiveLink());
    //--
    source->setParameter<unsigned int>(my_uint, source->id());
    //--
    target->flushHistory(&evts);
    idx = evts.getParmEvents(my_uint, source->id(), xchg::ParmEvent::PARM_CHANGED);
    EXPECT_EQ(idx.size(), 1);
    EXPECT_FALSE(evts[idx.front()].asParmEvent()->isActiveLink());
    //--
    source->unsetParameter(my_uint);
    //--
    target->flushHistory(&evts);
    idx = evts.getParmEvents(my_uint, source->id(), xchg::ParmEvent::PARM_REMOVED);
    EXPECT_EQ(idx.size(), 1);
    EXPECT_FALSE(evts[idx.front()].asParmEvent()->isActiveLink());

    const char* my_id = "my_id";
    //---
    source->setParameterId(my_id, rand());
    //---
    target->flushHistory(&evts);
    idx = evts.getParmEvents(my_id, source->id(), xchg::ParmEvent::PARM_ADDED);
    EXPECT_EQ(idx.size(), 1);
    EXPECT_TRUE(evts[idx.front()].asParmEvent()->isActiveLink());
    //---
    source->setParameterId(my_id, other->id());
    //---
    target->flushHistory(&evts);
    idx = evts.getParmEvents(my_id, source->id(), xchg::ParmEvent::PARM_CHANGED);
    EXPECT_EQ(idx.size(), 1);
    EXPECT_TRUE(evts[idx.front()].asParmEvent()->isActiveLink());
    //---
    source->unsetParameter(my_id);
    //---
    target->flushHistory(&evts);
    idx = evts.getParmEvents(my_id, source->id(), xchg::ParmEvent::PARM_REMOVED);
    EXPECT_EQ(idx.size(), 1);
    EXPECT_TRUE(evts[idx.front()].asParmEvent()->isActiveLink());

    const char* my_ids = "my_ids";
    xchg::IdSet ids;
    //---
    ids.clear(); ids.insert(rand());
    source->setParameter<xchg::IdSet>(my_ids, ids);
    //---
    target->flushHistory(&evts);
    idx = evts.getParmEvents(my_ids, source->id(), xchg::ParmEvent::PARM_ADDED);
    EXPECT_EQ(idx.size(), 1);
    EXPECT_TRUE(evts[idx.front()].asParmEvent()->isActiveLink());
    //---
    ids.clear(); ids.insert(other->id());
    source->setParameter<xchg::IdSet>(my_ids, ids);
    //---
    target->flushHistory(&evts);
    idx = evts.getParmEvents(my_ids, source->id(), xchg::ParmEvent::PARM_CHANGED);
    EXPECT_EQ(idx.size(), 1);
    EXPECT_TRUE(evts[idx.front()].asParmEvent()->isActiveLink());
    //---
    source->unsetParameter(my_ids);
    //---
    target->flushHistory(&evts);
    idx = evts.getParmEvents(my_ids, source->id(), xchg::ParmEvent::PARM_REMOVED);
    EXPECT_EQ(idx.size(), 1);
    EXPECT_TRUE(evts[idx.front()].asParmEvent()->isActiveLink());

    //---
    scn::Scene::destroy(scene);
}

TEST(TreeNodeTest, InvalidParentAndRoot)
{
    using namespace smks::scn;

    TreeNode::Ptr node1 = TreeNode::create("node1", TreeNode::WPtr());

    EXPECT_TRUE(!node1->parent().lock());
    EXPECT_EQ(node1->id(), node1->root().lock()->id());

    TreeNode::Ptr node2 = TreeNode::create("node2", node1); // root is not a proper scene

    EXPECT_TRUE(!node2->parent().lock());
    EXPECT_EQ(node2->id(), node2->root().lock()->id());

    TreeNode::destroy(node1);
    TreeNode::destroy(node2);
}

TEST(TreeNodeTest, NodeIDCollision)
{
    using namespace smks::scn;

    Scene::Ptr scene = Scene::create();

    const std::string name = "node";

    //---
    TreeNode::Ptr valid_node    = TreeNode::create(name.c_str(), scene);    // added to scene
    TreeNode::Ptr invalid_node  = TreeNode::create(name.c_str(), scene);    // outside of the scene
    //---

    EXPECT_EQ(valid_node->parent().lock()->id(),    scene->id());
    EXPECT_EQ(valid_node->root().lock()->id(),      scene->id());

    EXPECT_TRUE(!invalid_node->parent().lock());
    EXPECT_EQ(invalid_node->root().lock()->id(),    invalid_node->id());

    Scene   ::destroy(scene);
    TreeNode::destroy(invalid_node);
}

TEST(TreeNodeTest, AddNode)
{
    smks::scn::Scene::Ptr       scene = smks::scn::Scene::create();
    smks::scn::TreeNode::Ptr    node1 = smks::scn::TreeNode::create("node1", scene);
    smks::scn::TreeNode::Ptr    node2 = smks::scn::TreeNode::create("node2", node1);

    EXPECT_TRUE(smks::scn::test::isChildOf(node1, scene));
    EXPECT_TRUE(smks::scn::test::isChildOf(node2, node1));
    EXPECT_FALSE(smks::scn::test::isChildOf(node2, scene));

    EXPECT_TRUE(smks::scn::test::isParentOf(scene, node1));
    EXPECT_TRUE(smks::scn::test::isParentOf(node1, node2));
    EXPECT_FALSE(smks::scn::test::isParentOf(scene, node2));

    smks::scn::Scene::destroy(scene);
}

TEST(TreeNodeTest, RegisterNode)
{
    smks::scn::Scene::Ptr       scene = smks::scn::Scene::create();
    smks::scn::TreeNode::Ptr    node1 = smks::scn::TreeNode::create("node1", scene);
    smks::scn::TreeNode::Ptr    node2 = smks::scn::TreeNode::create("node2", node1);

    EXPECT_TRUE(scene->hasDescendant(scene->id()));
    EXPECT_TRUE(scene->hasDescendant(node1->id()));
    EXPECT_TRUE(scene->hasDescendant(node2->id()));

    EXPECT_TRUE(smks::scn::test::retrievableFromScene(scene, scene));
    EXPECT_TRUE(smks::scn::test::retrievableFromScene(node1, scene));
    EXPECT_TRUE(smks::scn::test::retrievableFromScene(node2, scene));

    smks::scn::Scene::destroy(scene);
}

TEST(TreeNodeTest, RemoveNode)
{
    smks::scn::Scene::Ptr       scene = smks::scn::Scene::create();
    smks::scn::TreeNode::Ptr    node1 = smks::scn::TreeNode::create("node1", scene);
    smks::scn::TreeNode::Ptr    node2 = smks::scn::TreeNode::create("node2", node1);
    smks::scn::TreeNode::Ptr    node3 = smks::scn::TreeNode::create("node3", node2);

    EXPECT_TRUE(smks::scn::test::isChildOf(node1, scene));
    EXPECT_TRUE(smks::scn::test::isChildOf(node2, node1));
    EXPECT_FALSE(smks::scn::test::isChildOf(node2, scene));

    EXPECT_TRUE(smks::scn::test::retrievableFromScene(scene, scene));
    EXPECT_TRUE(smks::scn::test::retrievableFromScene(node1, scene));
    EXPECT_TRUE(smks::scn::test::retrievableFromScene(node2, scene));

    smks::scn::TreeNode::destroy(node1);

    EXPECT_TRUE(smks::scn::test::retrievableFromScene(scene, scene));
    EXPECT_FALSE(smks::scn::test::retrievableFromScene(node1, scene));
    EXPECT_FALSE(smks::scn::test::retrievableFromScene(node2, scene));

    smks::scn::Scene::destroy(scene);
}

// test the correct notification of parameter-related events
TEST(TreeNodeTest, ParmAddedEvent)
{
    using namespace smks::scn;

    const std::string   parmName    = "my_parm";
    const unsigned int  parmId      = smks::util::string::getId(parmName.c_str());

    test::SceneContent2 all;

    //---
    all.node_a->setParameter<int>(parmName.c_str(), 42);
    //---

    test::TestNode::Ptr descA[] = { all.node_a, all.node_a_b1, all.node_a_b2, all.node_a_b1_c };
    const size_t numDesc = sizeof(descA) / sizeof(test::TestNode::Ptr);

    for (size_t i = 0; i < numDesc; ++i)
    {
        test::EventHistory evts;

        descA[i]->flushHistory(&evts);
        EXPECT_EQ(evts.getParmEvents(parmId).size(), 1);
        EXPECT_EQ(evts.getParmEvents(parmId, all.node_a->id(), smks::xchg::ParmEvent::PARM_ADDED).size(), 1);
    }
}

TEST(TreeNodeTest, AddBuiltInWithParmID)
{
    using namespace smks;

    const parm::BuiltIn&        builtin = parm::priority();
    scn::test::SceneContent2    all     (xchg::IdSet(builtin.id()));

    //---
    all.node_a->updateParameter<int>(builtin.id(), 42);
    //---

    scn::test::TestNode::Ptr descA[] = { all.node_a, all.node_a_b1, all.node_a_b2, all.node_a_b1_c };
    const size_t numDesc = sizeof(descA) / sizeof(scn::test::TestNode::Ptr);

    for (size_t i = 0; i < numDesc; ++i)
    {
        scn::test::EventHistory evts;

        descA[i]->flushHistory(&evts);
        EXPECT_EQ(evts.getParmEvents(builtin.id()).size(), 1);
        EXPECT_EQ(evts.getParmEvents(builtin.id(), all.node_a->id(), smks::xchg::ParmEvent::PARM_ADDED).size(), 1);
    }
}

TEST(TreeNodeTest, AddLinkingBuiltInWithParmID)
{
    using namespace smks;

    const parm::BuiltIn&        builtin = parm::materialId();   // must create node link
    scn::test::SceneContent2    all     (xchg::IdSet(builtin.id()));
    scn::TreeNode::Ptr          source  = scn::TreeNode::create("source", all.scene);

    // get all descendants of 'node_a'
    scn::test::TestNode::Ptr    desc_a[]    = { all.node_a, all.node_a_b1, all.node_a_b2, all.node_a_b1_c };
    const size_t                num_desc_a  = sizeof(desc_a) / sizeof(scn::test::TestNode::Ptr);

    //---------
    for (size_t i = 0; i < num_desc_a; ++i)
        desc_a[i]->flushHistory();
    //---
    all.node_a->updateParameter<unsigned int>(builtin.id(), source->id());
    unsigned int sourceParmId = source->setParameter<int>("parm_on_source", 42);
    //---
    scn::test::EventHistory evts;
    std::vector<size_t>     idx;
    const xchg::ParmEvent*  parmEvt;
    for (size_t i = 0; i < num_desc_a; ++i)
    {
        desc_a[i]->flushHistory(&evts);
        idx = evts.getParmEvents();
        if (desc_a[i] == all.node_a)
        {
            // node_a is expected to have recieved the builtin update event, and source change event
            EXPECT_EQ(idx.size(), 2);

            parmEvt = evts[idx[0]].asParmEvent();
            EXPECT_EQ(parmEvt->parm(), builtin.id());
            EXPECT_EQ(parmEvt->node(), all.node_a->id());

            parmEvt = evts[idx[1]].asParmEvent();
            EXPECT_EQ(parmEvt->parm(), sourceParmId);
            EXPECT_EQ(parmEvt->node(), source->id());
        }
        else
        {
            // node_a's children are expected to have recieved only the builtin update event.
            EXPECT_EQ(idx.size(), 1);

            parmEvt = evts[idx[0]].asParmEvent();
            EXPECT_EQ(parmEvt->parm(), builtin.id());
            EXPECT_EQ(parmEvt->node(), all.node_a->id());
            //EXPECT_FALSE(parmEvt->emitter(), all.node_a->id());
        }
    }
}

TEST(TreeNodeTest, ParmChangedEvent)
{
    using namespace smks::scn;

    const std::string   parmName    = "my_parm";
    const unsigned int  parmId      = smks::util::string::getId(parmName.c_str());

    test::SceneContent2 all;

    //---
    all.node_a->setParameter<int>(parmName.c_str(), 42);
    all.node_a->updateParameter<int>(parmId, 43);
    //---

    test::TestNode::Ptr descA[] = { all.node_a, all.node_a_b1, all.node_a_b2, all.node_a_b1_c };
    const size_t numDesc = sizeof(descA) / sizeof(test::TestNode::Ptr);

    for (size_t i = 0; i < numDesc; ++i)
    {
        test::EventHistory  evts;

        descA[i]->flushHistory(&evts);
        EXPECT_EQ(evts.getParmEvents(parmId).size(), 2);
        EXPECT_EQ(evts.getParmEvents(parmId, all.node_a->id(), smks::xchg::ParmEvent::PARM_CHANGED).size(), 1);
    }
}

TEST(TreeNodeTest, ParmDeletedEvent)
{
    using namespace smks::scn;

    const std::string   parmName    = "my_parm";
    const unsigned int  parmId      = smks::util::string::getId(parmName.c_str());

    test::SceneContent2 all;

    //---
    all.node_a->setParameter<int>(parmName.c_str(), 42);
    all.node_a->unsetParameter(parmName.c_str());
    //---

    test::TestNode::Ptr descA[] = { all.node_a, all.node_a_b1, all.node_a_b2, all.node_a_b1_c };
    const size_t numDesc = sizeof(descA) / sizeof(test::TestNode::Ptr);

    for (size_t i = 0; i < numDesc; ++i)
    {
        test::EventHistory  evts;

        descA[i]->flushHistory(&evts);
        EXPECT_EQ(evts.getParmEvents(parmId).size(), 2);
        EXPECT_EQ(evts.getParmEvents(parmId, all.node_a->id(), smks::xchg::ParmEvent::PARM_REMOVED).size(), 1);
    }
}

TEST(TreeNodeTest, GetBuiltInValue)
{
    using namespace smks;

    const parm::BuiltIn&        builtin = parm::yFieldOfViewD();
    scn::test::SceneContent2    all     (xchg::IdSet(builtin.id()));

    float               builtin_default;
    const float         ancestor_value      = 45.0f;
    const float         irrelevant_value    = -1.0f;

    builtin.defaultValue(builtin_default);

    //---
    all.node_a->setParameter<float>(builtin.c_str(), ancestor_value);
    //---
    float   value = 0.0f;
    bool    found = all.node_a_b1->getParameter<float>(builtin.id(), value);
    EXPECT_FALSE(found);
    EXPECT_FLOAT_EQ(value, builtin_default);

    found = all.node_a_b1->getParameter<float>(builtin.id(), value, irrelevant_value, all.scene->id());
    EXPECT_TRUE(found);
    EXPECT_FLOAT_EQ(value, ancestor_value);
}

TEST(TreeNodeTest, GetParmFromAncestor)
{
    using namespace smks::scn;

    const std::string   parmName    = "my_parm";
    const unsigned int  parmId      = smks::util::string::getId(parmName.c_str());

    test::SceneContent2 all;

    //---
    all.node_a->setParameter<int>(parmName.c_str(), 42);
    //---

    EXPECT_FALSE(all.node_a_b1_c->parameterExistsAs<int>(parmName.c_str()));
    EXPECT_TRUE(all.node_a_b1_c->parameterExistsAs<int>(parmName.c_str(), all.node_a->id()));

    int ival = -1;

    all.node_a_b1_c->getParameter<int>(parmName.c_str(), ival, 0);
    EXPECT_EQ(ival, 0);

    all.node_a_b1_c->getParameter<int>(parmName.c_str(), ival, 0, all.node_a->id());
    EXPECT_EQ(ival, 42);
}

TEST(TreeNodeTest, GetParmFromAncestor_Collision)
{
    using namespace smks::scn;

    const std::string   parmName    = "my_parm";
    const unsigned int  parmId      = smks::util::string::getId(parmName.c_str());

    test::SceneContent2 all;

    //---
    all.node_a->setParameter<int>(parmName.c_str(), 42);
    all.node_a_b1->setParameter<std::string>(parmName.c_str(), "fourtytwo");
    //---

    EXPECT_TRUE(all.node_a_b1_c ->parameterExistsAs<std::string>(parmName.c_str(), all.node_a_b1->id()));
    EXPECT_TRUE(all.node_a_b1_c ->parameterExistsAs<int>        (parmName.c_str(), all.scene->id()));
    EXPECT_FALSE(all.node_a_b1_c->parameterExistsAs<int>        (parmName.c_str(), all.node_a_b1->id()));

    std::string sval;
    int         ival = -1;

    all.node_a_b1_c->getParameter<std::string>(parmName.c_str(), sval, std::string(), all.node_a_b1->id());
    EXPECT_EQ(strcmp(sval.c_str(), "fourtytwo"), 0);
    all.node_a_b1_c->getParameter<int>(parmName.c_str(), ival, 0, all.scene->id());
    EXPECT_EQ(ival, 42);
}

TEST(TreeNodeTest, ReadOnlyParm)
{
    using namespace smks::scn;

    std::vector<std::string> parmNames;
    parmNames.push_back("parm1"); // read-only
    parmNames.push_back("parm2"); // read-only
    parmNames.push_back("parm3");

    Scene::Ptr                  scene   = Scene::create();
    test::TestNode::Ptr const&  node    = test::TestNode::create("node", scene);
    //---
    node->makeReadonly(parmNames[0]);
    node->makeReadonly(parmNames[1]);
    //---
    unsigned int parmId = 0;

    parmId = node->setParameter<int>(parmNames[0].c_str(), 42);
    EXPECT_EQ(parmId, 0);
    EXPECT_FALSE(node->parameterExistsAs<int>(parmNames[0].c_str()));

    parmId = node->setParameterId(parmNames[1].c_str(), 42);
    EXPECT_EQ(parmId, 0);
    EXPECT_FALSE(node->parameterExistsAsId(parmNames[1].c_str()));

    parmId = node->setParameter<int>(parmNames[2].c_str(), 42);
    EXPECT_NE(parmId, 0);
    EXPECT_TRUE(node->parameterExistsAs<int>(parmNames[2].c_str()));
    //---

    Scene::destroy(scene);
}

TEST(TreeNodeTest, ReadOnlyParm_Connection)
{
    using namespace smks::scn;

    const float         parmValue   = 42.0f;
    const std::string   parmSrc     = "parmSrc";
    const std::string   parmDst1    = "parmDst1";
    const std::string   parmDst2    = "parmDst2";   // read-only

    Scene::Ptr                  scene   = Scene::create();
    TreeNode::Ptr const&        src     = TreeNode::create("source", scene);
    test::TestNode::Ptr const&  dst     = test::TestNode::create("destination", scene);
    //---
    dst->makeReadonly(parmDst2.c_str());
    //---
    unsigned int cnx1   = scene->connect<float>(src->id(), parmSrc.c_str(), dst->id(), parmDst1.c_str());
    unsigned int cnx2   = scene->connect<float>(src->id(), parmSrc.c_str(), dst->id(), parmDst2.c_str());
    unsigned int parmId = src->setParameter<float>(parmSrc.c_str(), parmValue);

    EXPECT_NE(parmId, 0);
    EXPECT_TRUE(src->parameterExistsAs<float>(parmSrc.c_str()));
    EXPECT_TRUE(dst->parameterExistsAs<float>(parmDst1.c_str()));

    float fval = 0.0f;

    dst->getParameter<float>(parmDst1.c_str(), fval);
    EXPECT_FLOAT_EQ(fval, parmValue);
    EXPECT_FALSE(dst->parameterExistsAs<float>(parmDst2.c_str()));

    scene->disconnect(cnx1);
    scene->disconnect(cnx2);
    //---

    Scene::destroy(scene);
}

TEST(TreeNodeTest, ReadOnlyParm_Assign)
{
    using namespace smks::scn;

    const std::string   readonly_a  = "readonly_a";
    const std::string   readonly_b  = "readonly_b";

    Scene::Ptr                  scene       = Scene::create();
    test::TestNode::Ptr const&  target_a    = test::TestNode::create("target_a", scene);
    test::TestNode::Ptr const&  target_b    = test::TestNode::create("target_b", scene);
    test::TestNode::Ptr const&  target_c    = test::TestNode::create("target_c", scene);

    //---
    target_a->makeReadonly(readonly_a);
    target_b->makeReadonly(readonly_b);
    //---
    TreeNodeSelector::Ptr select = TreeNodeSelector::create(".*target.*", "select", scene);

    smks::xchg::IdSet selection, expected;
    bool status = select->getParameter<smks::xchg::IdSet>(smks::parm::selection(), selection);

    expected.insert(target_a->id());
    expected.insert(target_b->id());
    expected.insert(target_c->id());
    EXPECT_TRUE(status);
    EXPECT_TRUE(smks::xchg::equal<smks::xchg::IdSet>(selection, expected));

    ParmAssign<double>::Ptr assign_1 = ParmAssign<double>::create(readonly_a.c_str(), 1.5, "assign_1", scene);
    ParmAssign<double>::Ptr assign_2 = ParmAssign<double>::create(readonly_b.c_str(), 2.5, "assign_2", scene);

    //---
    unsigned int selectorId = select->id();
    assign_1->setParameter<unsigned int>(smks::parm::selectorId(), selectorId);
    assign_2->setParameter<unsigned int>(smks::parm::selectorId(), selectorId);

    EXPECT_FALSE(target_a->parameterExistsAs<double>(readonly_a.c_str()));
    EXPECT_TRUE (target_a->parameterExistsAs<double>(readonly_b.c_str()));

    EXPECT_TRUE (target_b->parameterExistsAs<double>(readonly_a.c_str()));
    EXPECT_FALSE(target_b->parameterExistsAs<double>(readonly_b.c_str()));

    EXPECT_TRUE(target_c->parameterExistsAs<double>(readonly_a.c_str()));
    EXPECT_TRUE(target_c->parameterExistsAs<double>(readonly_b.c_str()));
    //---

    Scene::destroy(scene);
}

TEST(TreeNodeTest, DataStreamChangeEvents)
{
    using namespace smks::scn;

    Scene::Ptr                  scene   = Scene::create();
    test::TestNode::Ptr node    = test::TestNode::create("node", scene);
    test::EventHistory          evts;

    const size_t numSamples     = 4;
    const size_t numVertices    = 5;
    const size_t vertexStride   = 5;
    const size_t numAttribs     = 2;
    const size_t attrSize[]     = { 3, 2 };

    std::vector<std::vector<float>> vData(
        numSamples,
        std::vector<float>(numVertices * vertexStride, 0.0f));

    const float kcoeff[] = { 2.0f, 3.0f, 7.0f, 11.0f };

    for (size_t i = 0; i < numSamples; ++i)
    {
        size_t num = 0;
        for (size_t j = 0; j < numVertices; ++j)
            for (size_t k = 0; k < numAttribs; ++k)
                for (size_t l = 0; l < attrSize[k]; ++l)
                    vData[i][num++] = kcoeff[0] * i + kcoeff[1] * j + kcoeff[2] * k + kcoeff[3] * l;
    }

    node->flushHistory();

    const std::string parmNameData      = "data";
    const std::string parmNameStream0   = "stream0";
    const std::string parmNameStream1   = "stream1";

    const unsigned int parmIds[] = {
        smks::util::string::getId(parmNameData.c_str()),
        smks::util::string::getId(parmNameStream0.c_str()),
        smks::util::string::getId(parmNameStream1.c_str())
    };

    smks::xchg::Data::Ptr   data = nullptr;
    smks::xchg::DataStream  stream0;
    smks::xchg::DataStream  stream1;

    std::vector<size_t> evtIdx;

    //---
    for (size_t i = 0; i < numSamples; ++i)
    {
        data    = smks::xchg::Data::create<float>(&vData[i].front(), numVertices * vertexStride, false);
        stream0 = smks::xchg::DataStream(data, numVertices, vertexStride, 0);
        stream1 = smks::xchg::DataStream(data, numVertices, vertexStride, attrSize[0]);

        node->setParameter<smks::xchg::Data::Ptr>(parmNameData.c_str(), data);
        node->setParameter<smks::xchg::DataStream>(parmNameStream0.c_str(), stream0);
        node->setParameter<smks::xchg::DataStream>(parmNameStream1.c_str(), stream1);

        node->flushHistory(&evts);

        for (size_t n = 0; n < 3; ++n)
        {
            evtIdx = evts.getParmEvents(parmIds[n]);
            EXPECT_EQ   (evtIdx.size(), 1);
            if (i > 0)
                EXPECT_TRUE (evts[evtIdx[0]].asParmEvent()->type() == smks::xchg::ParmEvent::PARM_CHANGED );
            else
                EXPECT_TRUE (evts[evtIdx[0]].asParmEvent()->type() == smks::xchg::ParmEvent::PARM_ADDED );
        }

        smks::xchg::Data::Ptr   nodeData        = nullptr;
        smks::xchg::DataStream  nodeStreams[]   = { smks::xchg::DataStream(), smks::xchg::DataStream() };

        node->getParameter<smks::xchg::Data::Ptr>(parmNameData.c_str(), nodeData, nullptr);
        node->getParameter<smks::xchg::DataStream>(parmNameStream0.c_str(), nodeStreams[0], smks::xchg::DataStream());
        node->getParameter<smks::xchg::DataStream>(parmNameStream1.c_str(), nodeStreams[1], smks::xchg::DataStream());

        EXPECT_TRUE(nodeData != nullptr);
        EXPECT_EQ(nodeData->size(), numVertices * vertexStride);
        EXPECT_EQ(nodeData->type(), typeid(float));
        for (size_t k = 0; k < 2; ++k)
        {
            EXPECT_EQ(nodeStreams[k].numElements(), numVertices);
            EXPECT_EQ(nodeStreams[k].stride(),      vertexStride);

            for (size_t j = 0; j < numVertices; ++j)
            {
                const float* ptr = nodeStreams[k].getPtr<float>(j);
                for (size_t l = 0; l < attrSize[k]; ++l)
                {
                    EXPECT_FLOAT_EQ(ptr[l], kcoeff[0] * i + kcoeff[1] * j + kcoeff[2] * k + kcoeff[3] * l);
                }
            }
        }

    }
    //---


    Scene::destroy(scene);
}

TEST(TreeNodeTest, ParmRemovalWhenNodeDeleted)
{
    using namespace smks::scn;

    std::vector<size_t>     evtIdx;
    test::EventHistory      evts1, evts2;
    Scene::Ptr              scene   = Scene::create();
    test::TestNode::Ptr node1   = test::TestNode::create("node1", scene, &evts1);
    test::TestNode::Ptr node2   = test::TestNode::create("node2", scene, &evts2);

    const std::string parmName = "my_parm";
    const unsigned int parmId = smks::util::string::getId(parmName.c_str());

    node1->setParameter<int>(parmName.c_str(), 42);
    node2->setParameter<double>(parmName.c_str(), 42.0);

    //---
    node2->flushHistory();
    EXPECT_TRUE(evts2.empty());
    test::TestNode::destroy(node2);
    evtIdx = evts2.getParmEvents(parmId);
    EXPECT_EQ(evtIdx.size(), 1);
    EXPECT_EQ(evts2[evtIdx.front()].asParmEvent()->type(), smks::xchg::ParmEvent::PARM_REMOVED);
    //---

    //---
    node1->flushHistory();
    EXPECT_TRUE(evts1.empty());
    Scene::destroy(scene);
    evtIdx = evts1.getParmEvents(parmId);
    EXPECT_EQ(evtIdx.size(), 1);
    EXPECT_EQ(evts1[evtIdx.front()].asParmEvent()->type(), smks::xchg::ParmEvent::PARM_REMOVED);
    //---
}

TEST(TreeNodeTest, ParmOverrideAlongHierarchy_ParentAddedFirst)
{
    using namespace smks::scn;

    test::SceneContent3 all;

    const std::string   parmName    = "parm";
    const unsigned int  parmId      = smks::util::string::getId(parmName.c_str());

    test::TestNode::Ptr parent                          = all.node1;
    test::TestNode::Ptr child                           = all.node3_a;
    test::TestNode::Ptr nodes_influenced_by_parent[]    = { all.node1, all.node2, all.node3_b, all.node4_b };
    test::TestNode::Ptr nodes_influenced_by_child[]     = { all.node3_a, all.node4_a };

    //---
    all.flushAllEvents();
    parent  ->setParameter<std::string>(parmName.c_str(), "toto");
    child   ->setParameter<int>(parmName.c_str(), 42);
    //---

    for (size_t i = 0; i < sizeof(nodes_influenced_by_parent)/sizeof(test::TestNode::Ptr); ++i)
    {
        test::EventHistory evts;
        nodes_influenced_by_parent[i]->flushHistory(&evts);
        const std::vector<size_t>& idx = evts.getParmEvents(parmId);

        EXPECT_EQ(idx.size(), 1);
        EXPECT_EQ(evts[idx[0]].asParmEvent()->node(), parent->id());
    }

    for (size_t i = 0; i < sizeof(nodes_influenced_by_child)/sizeof(test::TestNode::Ptr); ++i)
    {
        test::EventHistory evts;
        nodes_influenced_by_child[i]->flushHistory(&evts);
        const std::vector<size_t>& idx = evts.getParmEvents(parmId);

        EXPECT_EQ(idx.size(), 2);
        EXPECT_EQ(evts[idx[0]].asParmEvent()->node(), parent->id());
        EXPECT_EQ(evts[idx[1]].asParmEvent()->node(), child->id());
    }
}

TEST(TreeNodeTest, ParmOverrideAlongHierarchy_ChildAddedFirst)
{
    using namespace smks::scn;

    test::SceneContent3 all;

    const std::string   parmName    = "parm";
    const unsigned int  parmId      = smks::util::string::getId(parmName.c_str());

    test::TestNode::Ptr parent                          = all.node1;
    test::TestNode::Ptr child                           = all.node3_a;
    test::TestNode::Ptr nodes_influenced_by_parent[]    = { all.node1, all.node2, all.node3_b, all.node4_b };
    test::TestNode::Ptr nodes_influenced_by_child[]     = { all.node3_a, all.node4_a };

    //---
    all.flushAllEvents();
    child   ->setParameter<int>(parmName.c_str(), 42);
    parent  ->setParameter<std::string>(parmName.c_str(), "toto");
    //---

    for (size_t i = 0; i < sizeof(nodes_influenced_by_parent)/sizeof(test::TestNode::Ptr); ++i)
    {
        test::EventHistory evts;
        nodes_influenced_by_parent[i]->flushHistory(&evts);
        const std::vector<size_t>& idx = evts.getParmEvents(parmId);

        EXPECT_EQ(idx.size(), 1);
        EXPECT_EQ(evts[idx[0]].asParmEvent()->node(), parent->id());
    }

    for (size_t i = 0; i < sizeof(nodes_influenced_by_child)/sizeof(test::TestNode::Ptr); ++i)
    {
        test::EventHistory evts;
        nodes_influenced_by_child[i]->flushHistory(&evts);
        const std::vector<size_t>& idx = evts.getParmEvents(parmId);

        EXPECT_EQ(idx.size(), 1);
        EXPECT_EQ(evts[idx[0]].asParmEvent()->node(), child->id());
    }
}

TEST(TreeNodeTest, ParmOverrideAlongHierarchy_ParentRemovedFirst)
{
    using namespace smks::scn;

    test::SceneContent3 all;

    const std::string   parmName    = "parm";
    const unsigned int  parmId      = smks::util::string::getId(parmName.c_str());

    test::TestNode::Ptr parent                          = all.node1;
    test::TestNode::Ptr child                           = all.node3_a;
    test::TestNode::Ptr nodes_influenced_by_parent[]    = { all.node1, all.node2, all.node3_b, all.node4_b };
    test::TestNode::Ptr nodes_influenced_by_child[]     = { all.node3_a, all.node4_a };

    parent  ->setParameter<std::string>(parmName.c_str(), "toto");
    child   ->setParameter<int>(parmName.c_str(), 42);
    //---
    all.flushAllEvents();
    parent  ->unsetParameter(parmName.c_str());
    child   ->unsetParameter(parmName.c_str());
    //---

    for (size_t i = 0; i < sizeof(nodes_influenced_by_parent)/sizeof(test::TestNode::Ptr); ++i)
    {
        test::EventHistory evts;
        nodes_influenced_by_parent[i]->flushHistory(&evts);
        const std::vector<size_t>& idx = evts.getParmEvents(parmId);

        EXPECT_EQ(idx.size(), 1);
        EXPECT_EQ(evts[idx[0]].asParmEvent()->node(), parent->id());
    }

    for (size_t i = 0; i < sizeof(nodes_influenced_by_child)/sizeof(test::TestNode::Ptr); ++i)
    {
        test::EventHistory evts;
        nodes_influenced_by_child[i]->flushHistory(&evts);
        const std::vector<size_t>& idx = evts.getParmEvents(parmId);

        EXPECT_EQ(idx.size(), 1);
        EXPECT_EQ(evts[idx[0]].asParmEvent()->node(), child->id());
    }
}

TEST(TreeNodeTest, ParmOverrideAlongHierarchy_ChildRemovedFirst)
{
    using namespace smks::scn;

    test::SceneContent3 all;

    const std::string   parmName    = "parm";
    const unsigned int  parmId      = smks::util::string::getId(parmName.c_str());

    test::TestNode::Ptr parent                          = all.node1;
    test::TestNode::Ptr child                           = all.node3_a;
    test::TestNode::Ptr nodes_influenced_by_parent[]    = { all.node1, all.node2, all.node3_b, all.node4_b };
    test::TestNode::Ptr nodes_influenced_by_child[]     = { all.node3_a, all.node4_a };

    parent  ->setParameter<std::string>(parmName.c_str(), "toto");
    child   ->setParameter<int>(parmName.c_str(), 42);
    //---
    all.flushAllEvents();
    child   ->unsetParameter(parmName.c_str());
    parent  ->unsetParameter(parmName.c_str());
    //---

    for (size_t i = 0; i < sizeof(nodes_influenced_by_parent)/sizeof(test::TestNode::Ptr); ++i)
    {
        test::EventHistory evts;
        nodes_influenced_by_parent[i]->flushHistory(&evts);
        const std::vector<size_t>& idx = evts.getParmEvents(parmId);

        EXPECT_EQ(idx.size(), 1);
        EXPECT_EQ(evts[idx[0]].asParmEvent()->node(), parent->id());
    }

    for (size_t i = 0; i < sizeof(nodes_influenced_by_child)/sizeof(test::TestNode::Ptr); ++i)
    {
        test::EventHistory evts;
        nodes_influenced_by_child[i]->flushHistory(&evts);
        const std::vector<size_t>& idx = evts.getParmEvents(parmId);

        EXPECT_EQ(idx.size(), 2);
        EXPECT_EQ(evts[idx[0]].asParmEvent()->node(), child->id());
        EXPECT_EQ(evts[idx[1]].asParmEvent()->node(), parent->id());
    }
}

TEST(TreeNodeTest, HierarchicalParmExistTest)
{
    using namespace smks::scn;

    static const size_t N = 3;
    const std::string   parmName = "my_parm";

    Scene::Ptr      scene       = Scene::create();
    TreeNode::Ptr   linked      = TreeNode::create("linked", scene);
    TreeNode::Ptr   containing  = nullptr;
    TreeNode::Ptr   nodes[N];
    for (size_t n = 0; n < N; ++n)
    {
        const std::string name = "node_" + std::to_string(n);
        nodes[n] = TreeNode::create(name.c_str(), n > 0 ? nodes[n-1] : scene);
    }

    //---
    nodes[0]    ->setParameter<std::string> (parmName.c_str(), "my_string");
    nodes[N-1]  ->setParameterId            (parmName.c_str(), linked->id());
    //---

    EXPECT_FALSE(nodes[N-1]->parameterExistsAs<std::string> (parmName.c_str()));
    EXPECT_TRUE (nodes[N-1]->parameterExistsAsId            (parmName.c_str()));
    EXPECT_TRUE (nodes[N-1]->parameterExistsAs<std::string> (parmName.c_str(), nodes[0]->id(), &containing));
    EXPECT_EQ   (containing->id(), nodes[0]->id());

    //---
    nodes[N-1]->unsetParameter(parmName.c_str());
    //---

    EXPECT_TRUE (nodes[N-1]->parameterExistsAs<std::string> (parmName.c_str(), scene->id()));
    EXPECT_FALSE(nodes[N-1]->parameterExistsAsId            (parmName.c_str(), scene->id()));

    Scene::destroy(scene);
}

TEST(TreeNodeTest, HierarchicalParmGetterTest)
{
    using namespace smks::scn;

    static const size_t N = 3;
    const std::string   parmName = "my_parm";

    Scene::Ptr      scene       = Scene::create();
    TreeNode::Ptr   linked      = TreeNode::create("linked", scene);
    TreeNode::Ptr   nodes[N];
    for (size_t n = 0; n < N; ++n)
    {
        const std::string name = "node_" + std::to_string(n);
        nodes[n] = TreeNode::create(name.c_str(), n > 0 ? nodes[n-1] : scene);
    }

    //---
    nodes[0]    ->setParameter<std::string> (parmName.c_str(), "my_string");
    nodes[N-1]  ->setParameterId            (parmName.c_str(), linked->id());
    //---

    std::string     pStr;
    unsigned int    pId;

    EXPECT_FALSE(nodes[N-1]->getParameter<std::string>  (parmName.c_str(), pStr));
    EXPECT_TRUE (nodes[N-1]->getParameter<unsigned int> (parmName.c_str(), pId));
    EXPECT_EQ   (pId, linked->id());
    EXPECT_TRUE (nodes[N-1]->getParameter<std::string>  (parmName.c_str(), pStr, "default", nodes[0]->id()));
    EXPECT_EQ   (pStr, "my_string");

    //---
    nodes[N-1]->unsetParameter(parmName.c_str());
    //---

    EXPECT_TRUE (nodes[N-1]->getParameter<std::string>  (parmName.c_str(), pStr, "default", scene->id()));
    EXPECT_FALSE(nodes[N-1]->getParameter<unsigned int> (parmName.c_str(), pId, 0, scene->id()));

    Scene::destroy(scene);
}

TEST(TreeNodeTest, HierarchicalParmInfoGetterTest)
{
    using namespace smks;

    static const size_t N = 3;
    const char*         parmName    = "parm";
    const unsigned int  parm        = smks::util::string::getId(parmName);

    scn::Scene::Ptr     scene   = scn::Scene::create();
    scn::TreeNode::Ptr  source  = scn::TreeNode::create("source", scene);
    scn::TreeNode::Ptr  nodes[N];
    for (size_t n = 0; n < N; ++n)
    {
        const std::string name = "node_" + std::to_string(n);
        nodes[n] = scn::TreeNode::create(name.c_str(), n > 0 ? nodes[n-1] : scene);
    }

    //---
    nodes[0]    ->setParameter<std::string> (parmName, "foo");
    nodes[N-1]  ->setParameterId            (parmName, source->id());
    //---

    scn::TreeNode::Ptr  containing  = nullptr;
    xchg::ParameterInfo infos;
    bool                found = false;

    found = nodes[N-1]->getParameterInfo(parm, infos);
    EXPECT_TRUE(found);
    EXPECT_EQ(strcmp(infos.name, parmName), 0);
    EXPECT_EQ(*infos.typeinfo, typeid(unsigned int));
    EXPECT_EQ(infos.flags & parm::CREATES_LINK, parm::CREATES_LINK);

    found = nodes[N-1]->getParameterInfo(parm, infos, scene->id());
    EXPECT_TRUE(found);
    EXPECT_EQ(strcmp(infos.name, parmName), 0);
    EXPECT_EQ(*infos.typeinfo, typeid(unsigned int));
    EXPECT_EQ(infos.flags & parm::CREATES_LINK, parm::CREATES_LINK);

    //---
    nodes[N-1]->unsetParameter(parmName);
    //---

    found = nodes[N-1]->getParameterInfo(parm, infos);
    EXPECT_FALSE(found);
    EXPECT_EQ(infos.name, nullptr);
    EXPECT_EQ(infos.typeinfo, nullptr);
    EXPECT_EQ(infos.flags, parm::NO_PARM_FLAG);

    found = nodes[N-1]->getParameterInfo(parm, infos, nodes[0]->id(), &containing);
    EXPECT_TRUE(found);
    EXPECT_EQ(strcmp(infos.name, parmName), 0);
    EXPECT_EQ(*infos.typeinfo, typeid(std::string));
    EXPECT_EQ(infos.flags, parm::NO_PARM_FLAG);
    EXPECT_NE(containing.get(), nullptr);
    EXPECT_EQ(containing->id(), nodes[0]->id());

    scn::Scene::destroy(scene);
}

TEST(TreeNodeTest, BuiltInInfoGetterTest)
{
    using namespace smks;

    const parm::BuiltIn&        builtIn = parm::materialId();
    scn::Scene::Ptr             scene   = scn::Scene::create();
    scn::test::TestNode::Ptr    node    = scn::test::TestNode::create("node", scene, xchg::IdSet(builtIn.id()));

    bool                    found   = false;
    xchg::ParameterInfo     info;

    // built-in not set yet, expected to get no parameter information from node
    found = node->getParameterInfo(builtIn.id(), info);
    EXPECT_FALSE(found);
    EXPECT_EQ(info.name,        nullptr);
    EXPECT_EQ(info.typeinfo,    nullptr);
    EXPECT_EQ(info.flags,       parm::NO_PARM_FLAG);

    // now sets an actual value to the built-in parameter
    node->setParameter<unsigned int>(parm::materialId(), rand());

    found = node->getParameterInfo(builtIn.id(), info);
    EXPECT_TRUE(found);
    EXPECT_EQ(strcmp(info.name,                 builtIn.c_str()), 0);
    EXPECT_EQ(*info.typeinfo,                   typeid(unsigned int));
    EXPECT_EQ(info.flags & parm::CREATES_LINK,  parm::CREATES_LINK);

    scn::Scene::destroy(scene);
}

TEST(TreeNodeTest, ParmEventsAlwaysGoDownHierarchy)
{
    using namespace smks;

    scn::Scene::Ptr scene = scn::Scene::create();
    scn::test::TestNode::Ptr nodes[4];

    for (size_t i = 0; i < 4; ++i)
    {
        scn::TreeNode::Ptr parent = nullptr;
        if (i > 0)  parent = nodes[i-1];
        else        parent = scene;
        nodes[i] = scn::test::TestNode::create(("node_" + std::to_string(i)).c_str(), parent);
    }

    const char*         parmName    = "my_parm";
    const unsigned int  parmId      = util::string::getId(parmName);

    for (size_t i = 0; i < 4; ++i)
        nodes[i]->flushHistory();
    //---
    unsigned int idx = 1;
    nodes[idx]->setParameter<float>(parmName, 42.0f);
    //---
    for (size_t i = 0; i < 4; ++i)
    {
        scn::test::EventHistory evts;

        nodes[i]->flushHistory(&evts);
        if (i < idx)
            EXPECT_TRUE(evts.empty());
        else
        {
            EXPECT_TRUE(evts.size() == 1);
            EXPECT_EQ(evts.front().asParmEvent()->parm(),           parmId);
            EXPECT_EQ(evts.front().asParmEvent()->node(),   nodes[idx]->id());
        }
    }
    //---
    idx = 3;
    nodes[idx]->setParameter<float>(parmName, 43.0f);
    //---
    for (size_t i = 0; i < 4; ++i)
    {
        scn::test::EventHistory evts;

        nodes[i]->flushHistory(&evts);
        if (i < idx)
            EXPECT_TRUE(evts.empty());
        else
        {
            EXPECT_TRUE(evts.size() == 1);
            EXPECT_EQ(evts.front().asParmEvent()->parm(),           parmId);
            EXPECT_EQ(evts.front().asParmEvent()->node(),   nodes[idx]->id());
        }
    }

    scn::Scene::destroy(scene);
}

TEST(TreeNodeTest, ParmEventsGoDownHierarchyUntilOverride)
{
    using namespace smks;

    scn::Scene::Ptr scene = scn::Scene::create();
    scn::test::TestNode::Ptr nodes[4];

    for (size_t i = 0; i < 4; ++i)
    {
        scn::TreeNode::Ptr parent = nullptr;
        if (i > 0)  parent = nodes[i-1];
        else        parent = scene;
        nodes[i] = scn::test::TestNode::create(("node_" + std::to_string(i)).c_str(), parent);
    }

    const char*         parmName    = "my_parm";
    const unsigned int  parmId      = util::string::getId(parmName);
    //---
    const unsigned int bottom = 2;
    nodes[bottom]->setParameter<float>(parmName, 42.0f);
    //---
    for (size_t i = 0; i < 4; ++i)
        nodes[i]->flushHistory();
    //---
    const unsigned int top = 0;
    nodes[top]->setParameter<float>(parmName, 24.0f);
    //---
    for (size_t i = 0; i < 4; ++i)
    {
        scn::test::EventHistory evts;

        nodes[i]->flushHistory(&evts);
        if (bottom <= i)
            EXPECT_TRUE(evts.empty());  // event descent stops once parameter is redefined!
        else
        {
            EXPECT_TRUE(evts.size() == 1);
            EXPECT_EQ(evts.front().asParmEvent()->parm(),           parmId);
            EXPECT_EQ(evts.front().asParmEvent()->node(),   nodes[top]->id());
        }
    }

    scn::Scene::destroy(scene);
}

TEST(TreeNodeTest, ParmEventsGoThroughLinks)
{
    using namespace smks;

    scn::test::SceneContent4    scene;
    scn::test::EventHistory     evts;
    std::vector<size_t>         idx;
    const xchg::ParmEvent*      parmEvt;

    scene.flushAllEvents();
    //---
    const unsigned int parm = scene.node_a->setParameter<int>("parm", 42);
    //---
    // event is expected to reach :
    // - node_b, node_c, node_d (via links)
    // - node_aaa (via links even if descendant)
    // - node_aa (via hierarchy)
    xchg::IdSet visited;

    visited.insert(scene.node_a->id());
    visited.insert(scene.node_aa->id());
    visited.insert(scene.node_aaa->id());
    visited.insert(scene.node_b->id());
    visited.insert(scene.node_c->id());
    visited.insert(scene.node_d->id());

    for (size_t i = 0; i < scene.nodes.size(); ++i)
    {
        scene.nodes[i]->flushHistory(&evts);
        if (visited.find(scene.nodes[i]->id()) != visited.end())
        {
            idx = evts.getParmEvents();
            EXPECT_EQ(idx.size(), 1);
            parmEvt = evts[idx[0]].asParmEvent();
            EXPECT_EQ(parmEvt->parm(), parm);
            EXPECT_EQ(parmEvt->node(), scene.node_a->id());
        }
        else
        {
            EXPECT_TRUE(evts.empty());
        }
    }
}

TEST(TreeNodeTest, ParmEventFlagsThroughLinks)
{
    using namespace smks;

    scn::test::SceneContent4    scene;
    scn::test::EventHistory     evts;
    std::vector<size_t>         idx;
    const xchg::ParmEvent*      parmEvt;

    scene.flushAllEvents();
    //---
    const unsigned int parm = scene.node_a->setParameter<int>("parm", 42);
    //---
    // event is expected to reach :
    // - node_b, node_c, node_d (via links)
    // - node_aaa (via links even if descendant)
    // - node_aa (via hierarchy)

    // node_a : emitter
    scene.node_a->flushHistory(&evts);
    idx = evts.getParmEvents();
    EXPECT_EQ(idx.size(), 1);
    parmEvt = evts[idx[0]].asParmEvent();
    EXPECT_TRUE (parmEvt->emittedByNode());
    EXPECT_FALSE(parmEvt->comesFromAncestor());
    EXPECT_FALSE(parmEvt->comesFromLink());

    // node_b, node_c, node_aaa, node_d : link targets
    const scn::test::TestNode::Ptr targets[] = {
        scene.node_b, scene.node_c, scene.node_aaa, scene.node_d
    };
    const size_t numTargets = sizeof(targets) / sizeof(scn::test::TestNode::Ptr);
    for (size_t i = 0; i < numTargets; ++i)
    {
        targets[i]->flushHistory(&evts);
        idx = evts.getParmEvents();
        EXPECT_EQ(idx.size(), 1);
        parmEvt = evts[idx[0]].asParmEvent();
        EXPECT_FALSE(parmEvt->emittedByNode());
        EXPECT_FALSE(parmEvt->comesFromAncestor());
        EXPECT_TRUE (parmEvt->comesFromLink());
    }

    // node_aa : descendant of emitter
    scene.node_aa->flushHistory(&evts);
    idx = evts.getParmEvents();
    EXPECT_EQ(idx.size(), 1);
    parmEvt = evts[idx[0]].asParmEvent();
    EXPECT_FALSE(parmEvt->emittedByNode());
    EXPECT_TRUE (parmEvt->comesFromAncestor());
    EXPECT_FALSE(parmEvt->comesFromLink());
}

TEST(TreeNodeTest, ParmEventsGoThroughLinksUntilOverride)
{
    using namespace smks;

    scn::Scene::Ptr         scene = scn::Scene::create();
    scn::test::TestNodes    nodes(5);

    scn::test::EventHistory evts;
    std::vector<size_t>     idx;
    const xchg::ParmEvent*  parmEvt;

    for (size_t i = 0; i < nodes.size(); ++i)
    {
        const std::string name = "node_" + std::to_string(i);
        nodes[i] = scn::test::TestNode::create(name.c_str(), scene);
    }
    const char* link = "link";
    for (size_t i = 1; i < nodes.size(); ++i)
        nodes[i]->setParameterId(link, nodes[i-1]->id());   // links: [0] -> [1] -> ... -> [4]

    for (size_t i = 0; i < nodes.size(); ++i)
        nodes[i]->flushHistory();
    //---
    const unsigned int parm_a = nodes[0]->setParameter<int>("parm_a", 42);
    //---
    for (size_t i = 0; i < nodes.size(); ++i)
    {
        nodes[i]->flushHistory(&evts);
        idx = evts.getParmEvents();
        EXPECT_EQ(idx.size(), 1);
        parmEvt = evts[idx[0]].asParmEvent();
        EXPECT_EQ(parmEvt->parm(), parm_a);
        EXPECT_EQ(parmEvt->node(), nodes[0]->id());
    }

    // nodes[2] defines 'parm_b', spreads event to its subsequent targets
    //---
    const char*         parm_b_name = "parm_b";
    const unsigned int  parm_b      = nodes[2]->setParameter<std::string>(parm_b_name, "foo");
    //---
    for (size_t i = 0; i < nodes.size(); ++i)
    {
        nodes[i]->flushHistory(&evts);
        if (i < 2)
        {
            EXPECT_TRUE(evts.empty());
        }
        else
        {
            idx = evts.getParmEvents();
            EXPECT_EQ(idx.size(), 1);
            parmEvt = evts[idx[0]].asParmEvent();
            EXPECT_EQ(parmEvt->parm(), parm_b);
            EXPECT_EQ(parmEvt->node(), nodes[2]->id());
        }
    }

    // nodes[0] defines 'parm_b' too, spreads event to its subsequent targets unti override
    //---
    nodes[0]->setParameter<int>(parm_b_name, 24);
    //---
    for (size_t i = 0; i < nodes.size(); ++i)
    {
        nodes[i]->flushHistory(&evts);
        if (i < 2)
        {
            idx = evts.getParmEvents();
            EXPECT_EQ(idx.size(), 1);
            parmEvt = evts[idx[0]].asParmEvent();
            EXPECT_EQ(parmEvt->parm(), parm_b);
            EXPECT_EQ(parmEvt->node(), nodes[0]->id());
        }
        else
        {
            EXPECT_TRUE(evts.empty());
        }
    }
    scn::Scene::destroy(scene);
}

TEST(TreeNodeTest, ParmEventRedirectedToRoot)
{
    using namespace smks;

    scn::test::TestScene::Ptr   scene   = scn::test::TestScene::create();
    scn::test::TestNodes        nodes;

    scn::test::EventHistory     evts;
    std::vector<size_t>         idx;
    const xchg::ParmEvent*      parmEvt;

    nodes.push_back(scn::test::TestNode::create("node_0", scene));
    nodes.push_back(scn::test::TestNode::create("node_1", nodes[0]));
    nodes.push_back(scn::test::TestNode::create("node_2", scene));
    //---
    nodes[2]->setParameterId("link", scene->id());
    //---
    const char* parmName = "parm";
    //---
    nodes[1]->makeRedirectToRoot(parmName);
    //---------
    scene->flushHistory();
    for (size_t i = 0; i < nodes.size(); ++i)
        nodes[i]->flushHistory();
    //---
    const int parm = nodes[1]->setParameter<int>(parmName, 42);
    //---
    scene->flushHistory(&evts);
    idx = evts.getParmEvents();
    EXPECT_EQ(idx.size(), 1);
    parmEvt = evts[idx[0]].asParmEvent();
    EXPECT_EQ(parmEvt->parm(), parm);
    EXPECT_EQ(parmEvt->node(), nodes[1]->id());
    EXPECT_FALSE(parmEvt->emittedByNode());
    EXPECT_FALSE(parmEvt->comesFromAncestor());
    EXPECT_FALSE(parmEvt->comesFromLink());

    // neither node[0] or node[2] are supposed to have received the parameter event
    nodes[0]->flushHistory(&evts);
    EXPECT_TRUE(evts.empty());
    nodes[2]->flushHistory(&evts);
    EXPECT_TRUE(evts.empty());
    //---------
    scn::test::TestScene::destroy(scene);
}

TEST(TreeNodeTest, ParmDefaultValueByName)
{
    using namespace smks;

    scn::Scene::Ptr     scene       = scn::Scene::create();
    scn::TreeNode::Ptr  parent      = scn::TreeNode::create("parent", scene);
    scn::TreeNode::Ptr  node        = scn::TreeNode::create("node", parent);

    const char*         parmName        = "my_parm";
    const float         actualValue     = 42.5f;
    const float         defaultValue    = 11.25f;
    const unsigned int  parmId          = parent->setParameter<float>(parmName, actualValue);
    float               retValue;

    //---
    // no default value specified
    bool status = node->getParameter<float>(parmName, retValue);
    EXPECT_FALSE(status);
    EXPECT_FLOAT_EQ(retValue, 0.0f);

    status = node->getParameter<float>(parmId, retValue);
    EXPECT_FALSE(status);
    EXPECT_FLOAT_EQ(retValue, 0.0f);
    //---
    // default value specified
    status = node->getParameter<float>(parmName, retValue, defaultValue);
    EXPECT_FALSE(status);
    EXPECT_FLOAT_EQ(retValue, defaultValue);

    status = node->getParameter<float>(parmId, retValue, defaultValue);
    EXPECT_FALSE(status);
    EXPECT_FLOAT_EQ(retValue, defaultValue);
    //---
    // found actual value in parent
    status = node->getParameter<float>(parmName, retValue, defaultValue, parent->id());
    EXPECT_TRUE(status);
    EXPECT_FLOAT_EQ(retValue, actualValue);

    status = node->getParameter<float>(parmId, retValue, defaultValue, parent->id());
    EXPECT_TRUE(status);
    EXPECT_FLOAT_EQ(retValue, actualValue);
    //---
    scn::Scene::destroy(scene);
}

TEST(TreeNodeTest, BuiltInDefaultValue)
{
    using namespace smks;

    const parm::BuiltIn&        builtIn = parm::yFieldOfViewD();
    scn::Scene::Ptr             scene   = scn::Scene::create();
    scn::test::TestNode::Ptr    parent  = scn::test::TestNode::create("parent", scene,  builtIn.id());
    scn::test::TestNode::Ptr    node    = scn::test::TestNode::create("node",   parent, builtIn.id());

    float       defaultValue;
    const float randomValue = 100.0f * rand() / static_cast<float>(RAND_MAX);

    if (!builtIn.defaultValue(defaultValue))
        throw sys::Exception("Use incorrect built-in parameter.", "TreeNodeTest.BuiltInDefaultValue");

    const float actualValue = 2.0f * (1.0f + defaultValue);

    const unsigned int  parmId = parent->setParameter<float>(builtIn, actualValue);
    float               retValue;

    EXPECT_EQ(parmId, builtIn.id());

    //---
    // built-in default value > no user default value
    bool status = node->getParameter<float>(builtIn, retValue);
    EXPECT_FALSE(status);
    EXPECT_FLOAT_EQ(retValue, defaultValue);

    status = node->getParameter<float>(builtIn.c_str(), retValue);
    EXPECT_FALSE(status);
    EXPECT_FLOAT_EQ(retValue, defaultValue);

    status = node->getParameter<float>(builtIn.id(), retValue);
    EXPECT_FALSE(status);
    EXPECT_FLOAT_EQ(retValue, defaultValue);
    //---
    // built-in default value > user default value
    status = node->getParameter<float>(builtIn.c_str(), retValue, randomValue);
    EXPECT_FALSE(status);
    EXPECT_FLOAT_EQ(retValue, defaultValue);

    status = node->getParameter<float>(builtIn.id(), retValue, randomValue);
    EXPECT_FALSE(status);
    EXPECT_FLOAT_EQ(retValue, defaultValue);
    //---
    // built-in user value > built-in default value
    status = node->getParameter<float>(builtIn, retValue, parent->id());
    EXPECT_TRUE(status);
    EXPECT_FLOAT_EQ(retValue, actualValue);

    status = node->getParameter<float>(builtIn.c_str(), retValue, randomValue, parent->id());
    EXPECT_TRUE(status);
    EXPECT_FLOAT_EQ(retValue, actualValue);

    status = node->getParameter<float>(builtIn.id(), retValue, randomValue, parent->id());
    EXPECT_TRUE(status);
    EXPECT_FLOAT_EQ(retValue, actualValue);
    //---

    scn::Scene::destroy(scene);
}

TEST(TreeNodeTest, BuiltInDefaultValueOverrideByNode)
{
    using namespace smks;

    const parm::BuiltIn&    builtIn     = parm::yFieldOfViewD();
    float                   oldDefault;
    builtIn.defaultValue(oldDefault);
    const float             newDefault  = (10.0f + oldDefault) * 0.5f;

    scn::Scene::Ptr             scene       = scn::Scene::create();
    scn::test::TestNode::Ptr    parent      = scn::test::TestNode::create("parent",     scene,  builtIn.id());
    scn::test::TestNode::Ptr    overriding  = scn::test::TestNode::create("overriding", parent, builtIn.id());
    scn::test::TestNode::Ptr    child       = scn::test::TestNode::create("child",      scene,  builtIn.id());

    const float actualValue = 20.5f;

    //---
    overriding->setDefaultOverride(
        builtIn.id(),
        scn::test::TypedDefaultOverride<float>::create(builtIn.id(), newDefault));
    //---

    parent->setParameter<float>(builtIn, actualValue);

    bool        status;
    float       retValue    = 0.0f;
    const float randomValue = 100.0f * rand() / static_cast<float>(RAND_MAX);
    //---
    status = overriding->getParameter<float>(builtIn.c_str(), retValue, randomValue);
    EXPECT_FALSE(status);
    EXPECT_FLOAT_EQ(retValue, newDefault);
    //---
    status = overriding->getParameter<float>(builtIn.c_str(), retValue, randomValue, parent->id());
    EXPECT_TRUE(status);
    EXPECT_FLOAT_EQ(retValue, actualValue);
    //---
    status = child->getParameter<float>(builtIn.c_str(), retValue, randomValue, overriding->id());
    EXPECT_FALSE(status);
    EXPECT_FLOAT_EQ(retValue, oldDefault);
    //---
    scn::Scene::destroy(scene);
}

TEST(TreeNodeTest, CustomGetter)
{
    using namespace smks;

    const parm::BuiltIn&                builtIn = parm::yFieldOfViewD();
    scn::Scene::Ptr                     scene   = scn::Scene::create();
    scn::test::TestNode::Ptr            node    = scn::test::TestNode::create("node", scene, builtIn.id());
    scn::test::CustomGetter<float>::Ptr getter  = scn::test::CustomGetter<float>::create(42.0f);

    const float storedValue = 25.0f;
    float       defaultValue = 0.0f;
    builtIn.defaultValue(defaultValue);
    float       parmValue   = 0.0f;

    //---
    node->setParameter<float>(builtIn, storedValue);
    //---
    node->getParameter<float>(builtIn, parmValue);  // get stored value
    EXPECT_FLOAT_EQ(parmValue, storedValue);
    //---
    node->setCustomGetter(builtIn.id(), getter);
    //---
    node->getParameter<float>(builtIn, parmValue);  // get value imposed from getter
    EXPECT_FLOAT_EQ(parmValue, getter->value());
    //---
    node->unsetParameter(builtIn);
    //---
    node->getParameter<float>(builtIn, parmValue);  // still get value imposed from getter
    EXPECT_FLOAT_EQ(parmValue, getter->value());
    //---
    node->setCustomGetter(builtIn.id(), nullptr);
    //---
    node->getParameter<float>(builtIn, parmValue);  // get built-in's default value
    EXPECT_FLOAT_EQ(parmValue, defaultValue);

    scn::Scene::destroy(scene);
}

TEST(TreeNodeTest, CustomGetterDefaultValue)
{
    using namespace smks;

    const parm::BuiltIn&                builtIn = parm::yFieldOfViewD();
    scn::Scene::Ptr                     scene   = scn::Scene::create();
    scn::test::TestNode::Ptr            node    = scn::test::TestNode::create("node", scene, builtIn.id());
    scn::test::CustomGetter<float>::Ptr getter  = scn::test::CustomGetter<float>::create();

    const float storedValue = 25.0f;
    float       defaultValue = 0.0f;
    builtIn.defaultValue(defaultValue);
    float       parmValue   = 0.0f;

    //---
    node->setCustomGetter(builtIn.id(), getter);
    //---
    node->setParameter<float>(builtIn, storedValue);
    //---
    node->getParameter<float>(builtIn, parmValue);  // get stored value despite getter
    EXPECT_FLOAT_EQ(parmValue, storedValue);
    //---
    node->unsetParameter(builtIn);
    //---
    node->getParameter<float>(builtIn, parmValue);  // get default value despite getter
    EXPECT_FLOAT_EQ(parmValue, defaultValue);

    scn::Scene::destroy(scene);
}

TEST(TreeNodeTest, NestedParmEventsHandledInRightOrderIfDelayed)
{
    using namespace smks;

    const unsigned int      parm_a = util::string::getId("parm_a");
    const unsigned int      parm_b = util::string::getId("parm_b");
    const unsigned int      parm_c = util::string::getId("parm_c");
    scn::test::EventHistory evts;
    std::vector<size_t>     idx;
    const xchg::ParmEvent*  parmEvt;

    scn::test::TestScene::Ptr   scene = scn::test::TestScene::create();
    scn::test::TestNode::Ptr    node = scn::test::TestNode::create("node", scene);

    node->makeRedirectToRoot("parm_a");
    node->makeRedirectToRoot("parm_b");
    node->makeRedirectToRoot("parm_c");

    scn::test::ParmObserver::Ptr                            listener = scn::test::ParmObserver::create();
    std::vector<scn::test::ParmEditingObserverBase::Ptr>    editObservers;

    editObservers.push_back(scn::test::ParmSettingObserver<int>         ::create("parm_a", "parm_b", 42));
    editObservers.push_back(scn::test::ParmSettingObserver<std::string> ::create("parm_b", "parm_c", "foo"));
    for (size_t i = 0; i < editObservers.size(); ++i)
        node->registerObserver(editObservers[i]);
    node->registerObserver(listener);
    //----------
    listener->flushHistory();
    //---
    for (size_t i = 0; i < editObservers.size(); ++i)
        editObservers[i]->useDelayedEdits(false);

    node->setParameter<float>("parm_a", 24.0f);
    //---
    listener->flushHistory(&evts);
    idx = evts.getParmEvents();
    EXPECT_EQ(idx.size(), 3);
    for (size_t i = 0; i < idx.size(); ++i)
    {
        parmEvt = evts[idx[i]].asParmEvent();
        EXPECT_EQ(parmEvt->type(), xchg::ParmEvent::PARM_ADDED);
        EXPECT_EQ(parmEvt->node(), node->id());
    }
    EXPECT_EQ(evts[idx[0]].asParmEvent()->parm(), parm_c);  // nested reverse order by default
    EXPECT_EQ(evts[idx[1]].asParmEvent()->parm(), parm_b);
    EXPECT_EQ(evts[idx[2]].asParmEvent()->parm(), parm_a);
    //---
    node->unsetParameter(parm_a);
    node->unsetParameter(parm_b);
    node->unsetParameter(parm_c);
    listener->flushHistory();
    //---
    for (size_t i = 0; i < editObservers.size(); ++i)
        editObservers[i]->useDelayedEdits(true);

    node->setParameter<float>("parm_a", 24.0f);
    //---
    listener->flushHistory(&evts);
    idx = evts.getParmEvents();
    EXPECT_EQ(idx.size(), 3);
    for (size_t i = 0; i < idx.size(); ++i)
    {
        parmEvt = evts[idx[i]].asParmEvent();
        EXPECT_EQ(parmEvt->type(), xchg::ParmEvent::PARM_ADDED);
        EXPECT_EQ(parmEvt->node(), node->id());
    }
    EXPECT_EQ(evts[idx[0]].asParmEvent()->parm(), parm_a);  // correct order when edits are delayed
    EXPECT_EQ(evts[idx[1]].asParmEvent()->parm(), parm_b);
    EXPECT_EQ(evts[idx[2]].asParmEvent()->parm(), parm_c);
    //----------
    scn::test::TestScene::destroy(scene);
}

TEST(TreeNodeTest, NestedParmEventsRedirectedToRootInRightOrder)
{
    using namespace smks;

    const unsigned int      parm_a = util::string::getId("parm_a");
    const unsigned int      parm_b = util::string::getId("parm_b");
    const unsigned int      parm_c = util::string::getId("parm_c");
    scn::test::EventHistory evts;
    std::vector<size_t>     idx;
    const xchg::ParmEvent*  parmEvt;

    scn::test::TestScene::Ptr   scene = scn::test::TestScene::create();
    scn::test::TestNode::Ptr    node = scn::test::TestNode::create("node", scene);

    node->makeRedirectToRoot("parm_a");
    node->makeRedirectToRoot("parm_b");
    node->makeRedirectToRoot("parm_c");

    std::vector<scn::test::ParmEditingObserverBase::Ptr> editObservers;

    editObservers.push_back(scn::test::ParmSettingObserver<int>         ::create("parm_a", "parm_b", 42));
    editObservers.push_back(scn::test::ParmSettingObserver<std::string> ::create("parm_b", "parm_c", "foo"));
    for (size_t i = 0; i < editObservers.size(); ++i)
        node->registerObserver(editObservers[i]);
    //----------
    scene->flushHistory();
    //---
    // should work whether delayed edits are used or no
    node->setParameter<float>("parm_a", 24.0f);
    //---
    scene->flushHistory(&evts);
    idx = evts.getParmEvents();
    EXPECT_EQ(idx.size(), 3);
    for (size_t i = 0; i < idx.size(); ++i)
    {
        parmEvt = evts[idx[i]].asParmEvent();
        EXPECT_EQ(parmEvt->type(), xchg::ParmEvent::PARM_ADDED);
        EXPECT_EQ(parmEvt->node(), node->id());
    }
    EXPECT_EQ(evts[idx[0]].asParmEvent()->parm(), parm_a);
    EXPECT_EQ(evts[idx[1]].asParmEvent()->parm(), parm_b);
    EXPECT_EQ(evts[idx[2]].asParmEvent()->parm(), parm_c);
    //----------
    scn::test::TestScene::destroy(scene);
}

TEST(TreeNodeTest, DelayedNestedParmEditsOnSameParameter)
{
    using namespace smks;

    scn::Scene::Ptr             scene   = scn::Scene::create();
    scn::test::TestNode::Ptr    node    = scn::test::TestNode::create("node", scene);

    const char*             parm = "parm";
    scn::test::EventHistory evts;
    std::vector<size_t>     idx;
    const xchg::ParmEvent*  parmEvt;

    scn::test::ParmValueQueryObserver<int>::Ptr             listener = scn::test::ParmValueQueryObserver<int>::create(parm);
    std::vector<scn::test::ParmEditingObserverBase::Ptr>    editObservers;

    editObservers.push_back(scn::test::ParmSettingObserver<int>::create(parm, 41, parm, 42));
    editObservers.push_back(scn::test::ParmSettingObserver<int>::create(parm, 42, parm, 43));
    for (size_t i = 0; i < editObservers.size(); ++i)
    {
        node->registerObserver(editObservers[i]);
        editObservers[i]->useDelayedEdits(true);    // use delayed parameter edits
    }
    node->registerObserver(listener);
    //---------
    node->flushHistory();
    //---
    node->setParameter<int>(parm, 41);
    //---
    node->flushHistory(&evts);
    EXPECT_EQ(listener->numTypesAndValues(), 3);
    EXPECT_EQ(listener->parmValue(0), 41);
    EXPECT_EQ(listener->parmValue(1), 42);
    EXPECT_EQ(listener->parmValue(2), 43);
    //---------
    scn::Scene::destroy(scene);
}

void
Test_ParmSwitchBehavior(char dftValue)
{
    using namespace smks;

    // default, dftValue == INACTIVE
    char changingValue  = xchg::ACTIVATING;
    char changeValue    = xchg::ACTIVE;
    char revertingValue = xchg::DEACTIVATING;
    if (dftValue == xchg::ACTIVE)
    {
        changingValue   = xchg::DEACTIVATING;
        changeValue     = xchg::INACTIVE;
        revertingValue  = xchg::ACTIVATING;
    }

    const parm::BuiltIn&    builtIn     = parm::initialization();
    const char*             parmName    = "parm";
    const unsigned int      parm        = util::string::getId(parmName);

    scn::Scene::Ptr             scene   = scn::Scene::create();
    scn::test::TestNode::Ptr    node    = scn::test::TestNode::create("_node_", scene, xchg::IdSet(builtIn.id()));

    scn::test::TypedDefaultOverride<char>::Ptr      default_builtin     = scn::test::TypedDefaultOverride<char>     ::create(builtIn.id(), dftValue);
    scn::test::TypedDefaultOverride<char>::Ptr      default_parm        = scn::test::TypedDefaultOverride<char>     ::create(parm, dftValue);
    scn::test::ParmValueQueryObserver<char>::Ptr    listener_builtIn    = scn::test::ParmValueQueryObserver<char>   ::create(builtIn.c_str());
    scn::test::ParmValueQueryObserver<char>::Ptr    listener_parm       = scn::test::ParmValueQueryObserver<char>   ::create(parmName);
    scn::test::ParmValueQueryObserver<char>* current_listener = nullptr;

    node->setDefaultOverride(builtIn.id(), default_builtin);
    node->setDefaultOverride(parm, default_parm);
    node->registerObserver(listener_builtIn);
    node->registerObserver(listener_parm);
    //---
    bool hasParm = false;
    char parmValue = 0;

    hasParm = node->getParameter<char>(builtIn.id(), parmValue);
    EXPECT_FALSE(hasParm);
    EXPECT_EQ(parmValue, dftValue);
    hasParm = node->getParameter<char>(parm, parmValue);
    EXPECT_FALSE(hasParm);
    EXPECT_EQ(parmValue, dftValue);
    //---

    // switch via built-in parameter
    //---------
    current_listener = listener_builtIn.get();
    current_listener->flushTypesAndValues();
    //---
    node->setParameter<char>(builtIn, changeValue);
    //---
    EXPECT_EQ(current_listener->numTypesAndValues(), 2);
    EXPECT_EQ(current_listener->eventType(0), xchg::ParmEvent::PARM_ADDED);
    EXPECT_EQ(current_listener->parmValue(0), changingValue);
    EXPECT_EQ(current_listener->eventType(1), xchg::ParmEvent::PARM_CHANGED);
    EXPECT_EQ(current_listener->parmValue(1), changeValue);
    EXPECT_TRUE(node->parameterExists(builtIn.id()));
    //---------
    current_listener->flushTypesAndValues();
    //---
    node->updateParameter<char>(builtIn.id(), dftValue);
    //---
    EXPECT_EQ(current_listener->numTypesAndValues(), 2);
    EXPECT_EQ(current_listener->eventType(0), xchg::ParmEvent::PARM_CHANGED);
    EXPECT_EQ(current_listener->parmValue(0), revertingValue);
    EXPECT_EQ(current_listener->eventType(1), xchg::ParmEvent::PARM_REMOVED);
    EXPECT_EQ(current_listener->parmValue(1), dftValue);
    EXPECT_FALSE(node->parameterExists(builtIn.id()));
    //---------

    // switch via user-defined parameter
    //---------
    current_listener = listener_parm.get();
    current_listener->flushTypesAndValues();
    //---
    node->setParameterSwitch(parmName, changeValue);
    //---
    EXPECT_EQ(current_listener->numTypesAndValues(), 2);
    EXPECT_EQ(current_listener->eventType(0), xchg::ParmEvent::PARM_ADDED);
    EXPECT_EQ(current_listener->parmValue(0), changingValue);
    EXPECT_EQ(current_listener->eventType(1), xchg::ParmEvent::PARM_CHANGED);
    EXPECT_EQ(current_listener->parmValue(1), changeValue);
    EXPECT_TRUE(node->parameterExists(parmName));
    //---------
    current_listener->flushTypesAndValues();
    //---
    node->updateParameter<char>(parm, dftValue);
    //---
    EXPECT_EQ(current_listener->numTypesAndValues(), 2);
    EXPECT_EQ(current_listener->eventType(0), xchg::ParmEvent::PARM_CHANGED);
    EXPECT_EQ(current_listener->parmValue(0), revertingValue);
    EXPECT_EQ(current_listener->eventType(1), xchg::ParmEvent::PARM_REMOVED);
    EXPECT_EQ(current_listener->parmValue(1), dftValue);
    EXPECT_FALSE(node->parameterExists(parmName));
    //---------

    scn::Scene::destroy(scene);
}

TEST(TreeNodeTest, ParmSwitchBehavior_NodeActiveByDefault)
{
    Test_ParmSwitchBehavior(smks::xchg::ACTIVE);
}
TEST(TreeNodeTest, ParmSwitchBehavior_NodeInactiveByDefault)
{
    Test_ParmSwitchBehavior(smks::xchg::INACTIVE);
}

TEST(TreeNodeTest, ParmTriggerBehavior)
{
    using namespace smks;

    const parm::BuiltIn&    builtIn     = parm::rendering();
    const char*             parmName    = "parm";
    const unsigned int      parm        = util::string::getId(parmName);

    scn::Scene::Ptr             scene   = scn::Scene::create();
    scn::test::TestNode::Ptr    node    = scn::test::TestNode::create("_node_", scene, xchg::IdSet(builtIn.id()));

    scn::test::ParmValueQueryObserver<char>::Ptr listener_builtIn   = scn::test::ParmValueQueryObserver<char>::create(builtIn.c_str());
    scn::test::ParmValueQueryObserver<char>::Ptr listener_parm      = scn::test::ParmValueQueryObserver<char>::create(parmName);
    scn::test::ParmValueQueryObserver<char>* current_listener = nullptr;

    node->registerObserver(listener_builtIn);
    node->registerObserver(listener_parm);
    //---------
    current_listener = listener_builtIn.get();
    current_listener->flushTypesAndValues();
    //---
    node->setParameter<char>(builtIn, xchg::ACTIVE);
    //---
    EXPECT_EQ(current_listener->numTypesAndValues(), 2);
    EXPECT_EQ(current_listener->eventType(0), xchg::ParmEvent::PARM_ADDED);
    EXPECT_EQ(current_listener->parmValue(0), xchg::ACTIVE);
    EXPECT_EQ(current_listener->eventType(1), xchg::ParmEvent::PARM_REMOVED);
    EXPECT_EQ(current_listener->parmValue(1), xchg::INACTIVE);
    EXPECT_FALSE(node->parameterExists(builtIn.id()));
    //---------
    current_listener = listener_parm.get();
    current_listener->flushTypesAndValues();
    //---
    node->setParameterTrigger(parmName);
    //---
    EXPECT_EQ(current_listener->numTypesAndValues(), 2);
    EXPECT_EQ(current_listener->eventType(0), xchg::ParmEvent::PARM_ADDED);
    EXPECT_EQ(current_listener->parmValue(0), xchg::ACTIVE);
    EXPECT_EQ(current_listener->eventType(1), xchg::ParmEvent::PARM_REMOVED);
    EXPECT_EQ(current_listener->parmValue(1), xchg::INACTIVE);
    EXPECT_FALSE(node->parameterExists(parmName));
    //---------

    scn::Scene::destroy(scene);
}
