// ========================================================================= //
// Copyright 2016-2018 SUPAMONKS_STUDIO                                      //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
//                                                                           //
// ========================================================================= //

#pragma once

#include <gtest/gtest.h>

#include <smks/parm/builtins.hpp>

#include "smks/scn/test/util.hpp"
#include <smks/scn/Scene.hpp>
#include <smks/scn/TreeNodeFilter.hpp>
#include <smks/scn/Archive.hpp>
#include <smks/scn/ParmConnection.hpp>

TEST(UserAttribTest, LoadUnload)
{
    smks::scn::test::UserAttribScene scene;

    const char* parmName = "my_anim_float";

    EXPECT_FALSE(scene.pCube1->parameterExists(parmName));

    scene.pCube1->setParameter<char>(smks::parm::userAttribInitialization(), smks::xchg::ACTIVE);

    EXPECT_TRUE(scene.pCube1->parameterExists(parmName));
    EXPECT_TRUE(scene.pCube1->parameterExistsAs<float>(parmName));

    scene.pCube1->setParameter<char>(smks::parm::userAttribInitialization(), smks::xchg::INACTIVE);

    EXPECT_FALSE(scene.pCube1->parameterExists(parmName));
}

TEST(UserAttribTest, OverrideExisting)
{
    smks::scn::test::UserAttribScene scene;

    const char* parmName = "my_anim_float";

    scene.pCube1->setParameter<std::string>(parmName, "i will be overriden");

    EXPECT_TRUE(scene.pCube1->parameterExists(parmName));
    EXPECT_TRUE(scene.pCube1->parameterExistsAs<std::string>(parmName));

    scene.pCube1->setParameter<char>(smks::parm::userAttribInitialization(), smks::xchg::ACTIVE);

    EXPECT_TRUE(scene.pCube1->parameterExists(parmName));
    EXPECT_TRUE(scene.pCube1->parameterExistsAs<float>(parmName));

    scene.pCube1->setParameter<char>(smks::parm::userAttribInitialization(), smks::xchg::INACTIVE);

    EXPECT_FALSE(scene.pCube1->parameterExists(parmName));
}

TEST(UserAttribTest, LoadBool1)
{
    smks::scn::test::UserAttribScene scene;

    const char* parmName = "my_bool";
    const bool  refValue = true;
    bool        parmValue;

    EXPECT_FALSE(scene.pCube1->parameterExists(parmName));
    scene.pCube1->setParameter<char>(smks::parm::userAttribInitialization(), smks::xchg::ACTIVE);
    EXPECT_TRUE(scene.pCube1->parameterExists(parmName));
    EXPECT_TRUE(scene.pCube1->parameterExistsAs<bool>(parmName));
    scene.pCube1->getParameter<bool>(parmName, parmValue);
    EXPECT_EQ(parmValue, refValue);
}

TEST(UserAttribTest, LoadLong1)
{
    smks::scn::test::UserAttribScene scene;

    const char* parmName = "my_long";
    const int   refValue = 123456789;
    int         parmValue;

    EXPECT_FALSE(scene.pCube1->parameterExists(parmName));
    scene.pCube1->setParameter<char>(smks::parm::userAttribInitialization(), smks::xchg::ACTIVE);
    EXPECT_TRUE(scene.pCube1->parameterExists(parmName));
    EXPECT_TRUE(scene.pCube1->parameterExistsAs<int>(parmName));
    scene.pCube1->getParameter<int>(parmName, parmValue);
    EXPECT_EQ(parmValue, refValue);
}

TEST(UserAttribTest, LoadShort1)
{
    smks::scn::test::UserAttribScene scene;

    const char* parmName = "my_short";
    const int   refValue = 123;
    int         parmValue;

    EXPECT_FALSE(scene.pCube1->parameterExists(parmName));
    scene.pCube1->setParameter<char>(smks::parm::userAttribInitialization(), smks::xchg::ACTIVE);
    EXPECT_TRUE(scene.pCube1->parameterExists(parmName));
    EXPECT_TRUE(scene.pCube1->parameterExistsAs<int>(parmName));
    scene.pCube1->getParameter<int>(parmName, parmValue);
    EXPECT_EQ(parmValue, refValue);
}

TEST(UserAttribTest, LoadByte)
{
    smks::scn::test::UserAttribScene scene;

    const char* parmName = "my_byte";
    const char  refValue = 255;
    char        parmValue;

    EXPECT_FALSE(scene.pCube1->parameterExists(parmName));
    scene.pCube1->setParameter<char>(smks::parm::userAttribInitialization(), smks::xchg::ACTIVE);
    EXPECT_TRUE(scene.pCube1->parameterExists(parmName));
    EXPECT_TRUE(scene.pCube1->parameterExistsAs<char>(parmName));
    scene.pCube1->getParameter<char>(parmName, parmValue);
    EXPECT_EQ(parmValue, refValue);
}

TEST(UserAttribTest, LoadChar)
{
    smks::scn::test::UserAttribScene scene;

    const char* parmName = "my_char";
    const char  refValue = -1;
    char        parmValue;

    EXPECT_FALSE(scene.pCube1->parameterExists(parmName));
    scene.pCube1->setParameter<char>(smks::parm::userAttribInitialization(), smks::xchg::ACTIVE);
    EXPECT_TRUE(scene.pCube1->parameterExists(parmName));
    EXPECT_TRUE(scene.pCube1->parameterExistsAs<char>(parmName));
    scene.pCube1->getParameter<char>(parmName, parmValue);
    EXPECT_EQ(parmValue, refValue);
}

TEST(UserAttribTest, LoadEnum)
{
    smks::scn::test::UserAttribScene scene;

    const char* parmName = "my_enum";
    const int   refValue = 0;
    int         parmValue;

    EXPECT_FALSE(scene.pCube1->parameterExists(parmName));
    scene.pCube1->setParameter<char>(smks::parm::userAttribInitialization(), smks::xchg::ACTIVE);
    EXPECT_TRUE(scene.pCube1->parameterExists(parmName));
    EXPECT_TRUE(scene.pCube1->parameterExistsAs<int>(parmName));
    scene.pCube1->getParameter<int>(parmName, parmValue);
    EXPECT_EQ(parmValue, refValue);
}

TEST(UserAttribTest, LoadFloat1)
{
    smks::scn::test::UserAttribScene scene;

    const char* parmName = "my_float";
    const float refValue = 3.140f;
    float       parmValue;

    EXPECT_FALSE(scene.pCube1->parameterExists(parmName));
    scene.pCube1->setParameter<char>(smks::parm::userAttribInitialization(), smks::xchg::ACTIVE);
    EXPECT_TRUE(scene.pCube1->parameterExists(parmName));
    EXPECT_TRUE(scene.pCube1->parameterExistsAs<float>(parmName));
    scene.pCube1->getParameter<float>(parmName, parmValue);
    EXPECT_EQ(parmValue, refValue);
}

TEST(UserAttribTest, LoadDouble)
{
    smks::scn::test::UserAttribScene scene;

    const char* parmName = "my_double";
    const float refValue = -15.500f;
    float       parmValue;

    EXPECT_FALSE(scene.pCube1->parameterExists(parmName));
    scene.pCube1->setParameter<char>(smks::parm::userAttribInitialization(), smks::xchg::ACTIVE);
    EXPECT_TRUE(scene.pCube1->parameterExists(parmName));
    EXPECT_TRUE(scene.pCube1->parameterExistsAs<float>(parmName));
    scene.pCube1->getParameter<float>(parmName, parmValue);
    EXPECT_EQ(parmValue, refValue);
}

TEST(UserAttribTest, LoadStringAttr)
{
    smks::scn::test::UserAttribScene scene;

    const char*         parmName = "my_string";
    const std::string   refValue = "this is my string";
    std::string         parmValue;

    EXPECT_FALSE(scene.pCube1->parameterExists(parmName));
    scene.pCube1->setParameter<char>(smks::parm::userAttribInitialization(), smks::xchg::ACTIVE);
    EXPECT_TRUE(scene.pCube1->parameterExists(parmName));
    EXPECT_TRUE(scene.pCube1->parameterExistsAs<std::string>(parmName));
    scene.pCube1->getParameter<std::string>(parmName, parmValue);
    EXPECT_EQ(strcmp(parmValue.c_str(), refValue.c_str()), 0);
}

TEST(UserAttribTest, LoadRGB)
{
    smks::scn::test::UserAttribScene scene;

    const char*                 parmName = "my_rgb";
    const smks::math::Vector4f  refValue(0.2f, 0.5f, 0.7f, 0.0f);
    smks::math::Vector4f        parmValue;

    EXPECT_FALSE(scene.pCube1->parameterExists(parmName));
    scene.pCube1->setParameter<char>(smks::parm::userAttribInitialization(), smks::xchg::ACTIVE);
    EXPECT_TRUE(scene.pCube1->parameterExists(parmName));
    EXPECT_TRUE(scene.pCube1->parameterExistsAs<smks::math::Vector4f>(parmName));
    scene.pCube1->getParameter<smks::math::Vector4f>(parmName, parmValue);
    EXPECT_TRUE(parmValue.isApprox(refValue));
}

TEST(UserAttribTest, LoadFloat2)
{
    smks::scn::test::UserAttribScene scene;

    const char*                 parmName = "my_float2";
    const smks::math::Vector2f  refValue(1.5f, -1.8f);
    smks::math::Vector2f        parmValue;

    EXPECT_FALSE(scene.pCube1->parameterExists(parmName));
    scene.pCube1->setParameter<char>(smks::parm::userAttribInitialization(), smks::xchg::ACTIVE);
    EXPECT_TRUE(scene.pCube1->parameterExists(parmName));
    EXPECT_TRUE(scene.pCube1->parameterExistsAs<smks::math::Vector2f>(parmName));
    scene.pCube1->getParameter<smks::math::Vector2f>(parmName, parmValue);
    EXPECT_TRUE(parmValue.isApprox(refValue));
}

TEST(UserAttribTest, LoadFloat3)
{
    smks::scn::test::UserAttribScene scene;

    const char*                 parmName = "my_float3";
    const smks::math::Vector4f  refValue(1.5f, -1.8f, 1.95f, 0.0f);
    smks::math::Vector4f        parmValue;

    EXPECT_FALSE(scene.pCube1->parameterExists(parmName));
    scene.pCube1->setParameter<char>(smks::parm::userAttribInitialization(), smks::xchg::ACTIVE);
    EXPECT_TRUE(scene.pCube1->parameterExists(parmName));
    EXPECT_TRUE(scene.pCube1->parameterExistsAs<smks::math::Vector4f>(parmName));
    scene.pCube1->getParameter<smks::math::Vector4f>(parmName, parmValue);
    EXPECT_TRUE(parmValue.isApprox(refValue));
}

TEST(UserAttribTest, LoadDouble2)
{
    smks::scn::test::UserAttribScene scene;

    const char*                 parmName = "my_double2";
    const smks::math::Vector2f  refValue(2.5f, -2.8f);
    smks::math::Vector2f        parmValue;

    EXPECT_FALSE(scene.pCube1->parameterExists(parmName));
    scene.pCube1->setParameter<char>(smks::parm::userAttribInitialization(), smks::xchg::ACTIVE);
    EXPECT_TRUE(scene.pCube1->parameterExists(parmName));
    EXPECT_TRUE(scene.pCube1->parameterExistsAs<smks::math::Vector2f>(parmName));
    scene.pCube1->getParameter<smks::math::Vector2f>(parmName, parmValue);
    EXPECT_TRUE(parmValue.isApprox(refValue));
}

TEST(UserAttribTest, LoadDouble3)
{
    smks::scn::test::UserAttribScene scene;

    const char*                 parmName = "my_double3";
    const smks::math::Vector4f  refValue(2.5f, -2.8f, 2.95f, 0.0f);
    smks::math::Vector4f        parmValue;

    EXPECT_FALSE(scene.pCube1->parameterExists(parmName));
    scene.pCube1->setParameter<char>(smks::parm::userAttribInitialization(), smks::xchg::ACTIVE);
    EXPECT_TRUE(scene.pCube1->parameterExists(parmName));
    EXPECT_TRUE(scene.pCube1->parameterExistsAs<smks::math::Vector4f>(parmName));
    scene.pCube1->getParameter<smks::math::Vector4f>(parmName, parmValue);
    EXPECT_TRUE(parmValue.isApprox(refValue));
}

TEST(UserAttribTest, LoadLong2)
{
    smks::scn::test::UserAttribScene scene;

    const char*                 parmName = "my_long2";
    const smks::math::Vector2i  refValue(-2, 3);
    smks::math::Vector2i        parmValue;

    EXPECT_FALSE(scene.pCube1->parameterExists(parmName));
    scene.pCube1->setParameter<char>(smks::parm::userAttribInitialization(), smks::xchg::ACTIVE);
    EXPECT_TRUE(scene.pCube1->parameterExists(parmName));
    EXPECT_TRUE(scene.pCube1->parameterExistsAs<smks::math::Vector2i>(parmName));
    scene.pCube1->getParameter<smks::math::Vector2i>(parmName, parmValue);
    EXPECT_TRUE(parmValue.cwiseEqual(refValue).all());
}

TEST(UserAttribTest, LoadLong3)
{
    smks::scn::test::UserAttribScene scene;

    const char*                 parmName = "my_long3";
    const smks::math::Vector3i  refValue(-2, 3, -4);
    smks::math::Vector3i        parmValue;

    EXPECT_FALSE(scene.pCube1->parameterExists(parmName));
    scene.pCube1->setParameter<char>(smks::parm::userAttribInitialization(), smks::xchg::ACTIVE);
    EXPECT_TRUE(scene.pCube1->parameterExists(parmName));
    EXPECT_TRUE(scene.pCube1->parameterExistsAs<smks::math::Vector3i>(parmName));
    scene.pCube1->getParameter<smks::math::Vector3i>(parmName, parmValue);
    EXPECT_TRUE(parmValue.cwiseEqual(refValue).all());
}

TEST(UserAttribTest, LoadShort2)
{
    smks::scn::test::UserAttribScene scene;

    const char*                 parmName = "my_short2";
    const smks::math::Vector2i  refValue(12, 13);
    smks::math::Vector2i        parmValue;

    EXPECT_FALSE(scene.pCube1->parameterExists(parmName));
    scene.pCube1->setParameter<char>(smks::parm::userAttribInitialization(), smks::xchg::ACTIVE);
    EXPECT_TRUE(scene.pCube1->parameterExists(parmName));
    EXPECT_TRUE(scene.pCube1->parameterExistsAs<smks::math::Vector2i>(parmName));
    scene.pCube1->getParameter<smks::math::Vector2i>(parmName, parmValue);
    EXPECT_TRUE(parmValue.cwiseEqual(refValue).all());
}

TEST(UserAttribTest, LoadShort3)
{
    smks::scn::test::UserAttribScene scene;

    const char*                 parmName = "my_short3";
    const smks::math::Vector3i  refValue(12, 13, 14);
    smks::math::Vector3i        parmValue;

    EXPECT_FALSE(scene.pCube1->parameterExists(parmName));
    scene.pCube1->setParameter<char>(smks::parm::userAttribInitialization(), smks::xchg::ACTIVE);
    EXPECT_TRUE(scene.pCube1->parameterExists(parmName));
    EXPECT_TRUE(scene.pCube1->parameterExistsAs<smks::math::Vector3i>(parmName));
    scene.pCube1->getParameter<smks::math::Vector3i>(parmName, parmValue);
    EXPECT_TRUE(parmValue.cwiseEqual(refValue).all());
}

TEST(UserAttribTest, LoadAnimAttr)
{
    smks::scn::test::UserAttribScene scene;

    const char* parmName = "my_anim_float";
    float       parmValue = 0.0f;
    const float rFramerate = 1.0f / 25.0f;

    EXPECT_FALSE(scene.pCube1->parameterExists(parmName));

    scene.scene->setParameter<float>(smks::parm::currentTime(), 5.0f * rFramerate); // set current time bfr initializing custom attributes
    scene.pCube1->setParameter<char>(smks::parm::userAttribInitialization(), smks::xchg::ACTIVE);

    EXPECT_TRUE(scene.pCube1->parameterExistsAs<float>(parmName));
    scene.pCube1->getParameter<float>(parmName, parmValue);
    EXPECT_FLOAT_EQ(parmValue, 8.5f);

    scene.scene->setParameter<float>(smks::parm::currentTime(), 4.0f * rFramerate); // change current time now custom attributes are loaded
    scene.pCube1->getParameter<float>(parmName, parmValue);
    EXPECT_FLOAT_EQ(parmValue, 7.5625f);

    scene.scene->setParameter<float>(smks::parm::currentTime(), 3.0f * rFramerate); // change current time now custom attributes are loaded
    scene.pCube1->getParameter<float>(parmName, parmValue);
    EXPECT_FLOAT_EQ(parmValue, 5.5f);

    scene.scene->setParameter<float>(smks::parm::currentTime(), 2.0f * rFramerate); // change current time now custom attributes are loaded
    scene.pCube1->getParameter<float>(parmName, parmValue);
    EXPECT_FLOAT_EQ(parmValue, 3.4375f);

    scene.scene->setParameter<float>(smks::parm::currentTime(), 1.0f * rFramerate); // change current time now custom attributes are loaded
    scene.pCube1->getParameter<float>(parmName, parmValue);
    EXPECT_FLOAT_EQ(parmValue, 2.5f);
}

TEST(UserAttribTest, ParmEvents)
{
    using namespace smks;

    const float rFramerate = 1.0f / 25.0f;
    scn::test::UserAttribScene      scene;
    scn::test::EventHistory         history;
    scn::test::ParmObserver::Ptr    obs = scn::test::ParmObserver::create();
    std::vector<size_t> idx;

    const char* parmName = "my_anim_float";

    scene.pCube1->registerObserver(obs);

    //---
    obs->flushHistory();
    scene.pCube1->setParameter<char>(smks::parm::userAttribInitialization(), smks::xchg::ACTIVE);
    //---

    obs->flushHistory(&history);
    idx = history.getParmEvents(parmName, scene.pCube1->id(), xchg::ParmEvent::PARM_ADDED);
    EXPECT_EQ(idx.size(), 1);

    //---
    obs->flushHistory();
    scene.scene->setParameter<float>(smks::parm::currentTime(), 5.0f * rFramerate); // set current time bfr initializing custom attributes
    //---

    obs->flushHistory(&history);
    idx = history.getParmEvents(parmName, scene.pCube1->id(), xchg::ParmEvent::PARM_CHANGED);
    EXPECT_EQ(idx.size(), 1);

    //---
    obs->flushHistory();
    scene.pCube1->setParameter<char>(smks::parm::userAttribInitialization(), smks::xchg::INACTIVE);
    //---

    obs->flushHistory(&history);
    idx = history.getParmEvents(parmName, scene.pCube1->id(), xchg::ParmEvent::PARM_REMOVED);
    EXPECT_EQ(idx.size(), 1);
}

TEST(UserAttribTest, ParmEventsThruConnection_ConnectThenInit)
{
    using namespace smks;

    const float                 rFramerate = 1.0f / 25.0f;
    scn::test::UserAttribScene  scene;
    scn::test::EventHistory     history;
    scn::test::TestNode::Ptr    targetNode = scn::test::TestNode::create("targetNode", scene.scene);
    std::vector<size_t>         idx;

    const char* parmName        = "my_anim_float";
    const char* targetParmName  = "my_connected_anim_float";

    // connection: pCube1.my_anim_float -> targetNode.my_connected_anim_float
    scene.scene->connect<float>(scene.pCube1->id(), parmName, targetNode->id(), targetParmName);

    //---
    targetNode->flushHistory();
    scene.pCube1->setParameter<char>(smks::parm::userAttribInitialization(), smks::xchg::ACTIVE);
    //---

    targetNode->flushHistory(&history);
    idx = history.getParmEvents(targetParmName, targetNode->id(), xchg::ParmEvent::PARM_ADDED);
    EXPECT_EQ(idx.size(), 1);

    //---
    targetNode->flushHistory();
    scene.scene->setParameter<float>(smks::parm::currentTime(), 5.0f * rFramerate); // set current time bfr initializing custom attributes
    //---

    targetNode->flushHistory(&history);
    idx = history.getParmEvents(targetParmName, targetNode->id(), xchg::ParmEvent::PARM_CHANGED);
    EXPECT_EQ(idx.size(), 1);

    //---
    targetNode->flushHistory();
    scene.pCube1->setParameter<char>(smks::parm::userAttribInitialization(), smks::xchg::INACTIVE);
    //---

    targetNode->flushHistory(&history);
    idx = history.getParmEvents(targetParmName, targetNode->id(), xchg::ParmEvent::PARM_REMOVED);
    EXPECT_EQ(idx.size(), 1);
}

TEST(UserAttribTest, ParmEventsThruConnection_InitThenConnect)
{
    using namespace smks;

    const float                 rFramerate = 1.0f / 25.0f;
    scn::test::UserAttribScene  scene;
    scn::test::EventHistory     history;
    scn::test::TestNode::Ptr    targetNode = scn::test::TestNode::create("targetNode", scene.scene);
    std::vector<size_t>         idx;

    const char* parmName        = "my_anim_float";
    const char* targetParmName  = "my_connected_anim_float";

    scene.pCube1->setParameter<char>(smks::parm::userAttribInitialization(), smks::xchg::ACTIVE);

    // connection: pCube1.my_anim_float -> targetNode.my_connected_anim_float
    //---
    targetNode->flushHistory();
    scene.scene->connect<float>(scene.pCube1->id(), parmName, targetNode->id(), targetParmName);
    //---

    targetNode->flushHistory(&history);
    idx = history.getParmEvents(targetParmName, targetNode->id(), xchg::ParmEvent::PARM_ADDED);
    EXPECT_EQ(idx.size(), 1);

    //---
    targetNode->flushHistory();
    scene.scene->setParameter<float>(smks::parm::currentTime(), 5.0f * rFramerate); // set current time bfr initializing custom attributes
    //---

    targetNode->flushHistory(&history);
    idx = history.getParmEvents(targetParmName, targetNode->id(), xchg::ParmEvent::PARM_CHANGED);
    EXPECT_EQ(idx.size(), 1);

    //---
    targetNode->flushHistory();
    scene.pCube1->setParameter<char>(smks::parm::userAttribInitialization(), smks::xchg::INACTIVE);
    //---

    targetNode->flushHistory(&history);
    idx = history.getParmEvents(targetParmName, targetNode->id(), xchg::ParmEvent::PARM_REMOVED);
    EXPECT_EQ(idx.size(), 1);
}
