// ========================================================================= //
// Copyright 2016-2018 SUPAMONKS_STUDIO                                      //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
//                                                                           //
// ========================================================================= //
#pragma once

#include <gtest/gtest.h>

#include "smks/scn/test/util.hpp"
#include <smks/parm/builtins.hpp>
#include <smks/scn/ParmAssign.hpp>
#include <smks/scn/IdAssign.hpp>
#include <smks/scn/TreeNodeSelector.hpp>
#include "smks/scn/test/FindNodesWithParm.hpp"
#include "smks/scn/test/FilterByNameLength.hpp"

// - direct assignation explicit selection
// - direct assignation implicit selection
// - filtered assignation explicit selection
// - filtered assignation implicit selection
// - parameter value change
// - parameter name change
// - priority change
// - matched node addition
// - matched node deletion
// - assign deletion
// - multiple assign addition / single assign deletion

TEST(ParmAssignTest, SelectorDeletion0)
{
    smks::scn::test::FindNodesWithParm<int> finder;
    smks::scn::test::SceneContent1          all;

    const std::string   parmName1   = "my_parm1";
    const int           parmValue1  = 42;

    smks::scn::TreeNodeSelector::Ptr    select  = smks::scn::TreeNodeSelector::create("/a.*", "__select", all.scene);

    smks::scn::ParmAssign<int>::Ptr     assign1 = smks::scn::ParmAssign<int>::create(
        parmName1.c_str(), parmValue1, "__assign1", all.scene);

    unsigned int id = select->id();
    assign1->setParameter<unsigned int>(smks::parm::selectorId(), id);

    // check correctness
    smks::xchg::IdSet expected;
    expected.insert(all.node_a->id());
    expected.insert(all.node_a_b->id());
    expected.insert(all.node_a_aa->id());
    expected.insert(all.node_a_ab->id());
    expected.insert(all.node_a_aa_a->id());

    finder.initialize(parmName1);
    all.scene->accept(finder);
    //smks::scn::test::printNodes(std::cout << "nodes : ", finder.found(), all.scene);
    EXPECT_TRUE(smks::xchg::equal(finder.found(), expected));

    //---
    assign1->unsetParameter(smks::parm::selectorId().id());
    //---

    // check correctness
    finder.initialize(parmName1);
    all.scene->accept(finder);
    EXPECT_TRUE(finder.found().empty());
}


TEST(ParmAssignTest, DirectAssignFromExplicitSelection)
{
    smks::scn::test::SceneContent1  all;

    const std::string   parmName    = "my_parm";
    const int           parmValue   = 42;

    smks::scn::test::FindNodesWithParm<int> finder;

    smks::scn::ParmAssign<int>::Ptr assign = smks::scn::ParmAssign<int>::create(parmName.c_str(), parmValue, "__assign", all.scene);

    // explicit selection
    smks::xchg::IdSet selection;
    selection.insert(all.node_b_bb->id());
    assign->setParameter<smks::xchg::IdSet>(smks::parm::selection(), selection);

    // check correctness
    finder.initialize(parmName);
    all.scene->accept(finder);
    EXPECT_TRUE(smks::xchg::equal(finder.found(), selection));
}

TEST(ParmAssignTest, FilteredAssignFromExplicitSelection)
{
    smks::scn::test::SceneContent1  all;

    const std::string   parmName    = "my_parm";
    const int           parmValue   = 42;

    smks::scn::test::FindNodesWithParm<int> finder;

    smks::scn::test::FilterByNameLength::Ptr    filter  = smks::scn::test::FilterByNameLength::create(3);
    smks::scn::ParmAssign<int>::Ptr             assign  = smks::scn::ParmAssign<int>::create(
        parmName.c_str(), parmValue, smks::scn::TreeNodeVisitor::DESCENDING, filter, "__assign", all.scene);

    // explicit selection
    smks::xchg::IdSet selection;
    selection.insert(all.node_b_bb->id());
    assign->setParameter<smks::xchg::IdSet>(smks::parm::selection(), selection);

    // check correctness
    finder.initialize(parmName);
    all.scene->accept(finder);

    smks::xchg::IdSet expected;
    expected.insert(all.node_b_bb_bb1->id());
    expected.insert(all.node_b_bb_bb2->id());
    EXPECT_TRUE(smks::xchg::equal(finder.found(), expected));
}

TEST(ParmAssignTest, DirectAssignFromRegexSelection)
{
    smks::scn::test::SceneContent1  all;

    const std::string   parmName    = "my_parm";
    const int           parmValue   = 42;

    smks::scn::test::FindNodesWithParm<int> finder;

    smks::scn::ParmAssign<int>::Ptr     assign  = smks::scn::ParmAssign<int>::create(parmName.c_str(), parmValue, "__assign", all.scene);
    // regex selection
    smks::scn::TreeNodeSelector::Ptr    select  = smks::scn::TreeNodeSelector::create(".*/bb", "__select", all.scene);

    unsigned int selectorId = select->id();
    assign->setParameter<unsigned int>(smks::parm::selectorId(), selectorId);

    // check correctness
    finder.initialize(parmName);
    all.scene->accept(finder);

    smks::xchg::IdSet expected;
    expected.insert(all.node_b_bb->id());
    EXPECT_TRUE(smks::xchg::equal(finder.found(), expected));
}

TEST(ParmAssignTest, FilterAssignFromRegexSelection)
{
    smks::scn::test::SceneContent1  all;

    const std::string   parmName    = "my_parm";
    const int           parmValue   = 42;

    smks::scn::test::FindNodesWithParm<int> finder;

    smks::scn::ParmAssign<int>::Ptr     assign  = smks::scn::ParmAssign<int>::create(
        parmName.c_str(), parmValue, smks::scn::TreeNodeVisitor::ASCENDING, nullptr, "__assign", all.scene);
    // regex selection
    smks::scn::TreeNodeSelector::Ptr    select  = smks::scn::TreeNodeSelector::create(".*/bb", "__select", all.scene);

    unsigned int selectorId = select->id();
    assign->setParameter<unsigned int>(smks::parm::selectorId(), selectorId);

    // check correctness
    finder.initialize(parmName);
    all.scene->accept(finder);

    smks::xchg::IdSet expected;
    expected.insert(all.scene->id());
    expected.insert(all.node_b->id());
    expected.insert(all.node_b_bb->id());
    EXPECT_TRUE(smks::xchg::equal(finder.found(), expected));
}

TEST(ParmAssignTest, ParameterValueChange)
{
    smks::scn::test::SceneContent1  all;

    const std::string   parmName    = "my_parm";
    const int           parmValue   = 42;

    smks::scn::test::EqualParm<int>::Ptr    eq = smks::scn::test::EqualParm<int>::create();
    smks::scn::test::FindNodesWithParm<int> finder;

    smks::scn::ParmAssign<int>::Ptr         assign  = smks::scn::ParmAssign<int>::create(
        parmName.c_str(), parmValue, smks::scn::TreeNodeVisitor::DESCENDING, nullptr, "__assign", all.scene);

    // regex selection
    smks::scn::TreeNodeSelector::Ptr    select  = smks::scn::TreeNodeSelector::create(".*/bb", "__select", all.scene);

    unsigned int selectorId = select->id();
    assign->setParameter<unsigned int>(smks::parm::selectorId(), selectorId);

    // check correctness
    finder.initialize(parmName, parmValue, eq);
    all.scene->accept(finder);

    smks::xchg::IdSet expected;
    int ival = 0;

    expected.insert(all.node_b_bb->id());
    expected.insert(all.node_b_bb_bb1->id());
    expected.insert(all.node_b_bb_bb2->id());
    EXPECT_TRUE(smks::xchg::equal(finder.found(), expected));
    all.node_b_bb->getParameter<int>(parmName.c_str(), ival);
    EXPECT_EQ(ival, parmValue);

    // change parameter value
    const int newParmValue = 43;
    assign->setParameter<int>(smks::parm::parmValue(), newParmValue);

    // check previous parm value is nowhere to be found
    finder.initialize(parmName, parmValue, eq);
    all.scene->accept(finder);
    EXPECT_TRUE(finder.found().empty());
    // check new parm value is found everywhere it needs to be
    finder.initialize(parmName, newParmValue, eq);
    all.scene->accept(finder);
    EXPECT_TRUE(smks::xchg::equal(finder.found(), expected));
    all.node_b_bb->getParameter<int>(parmName.c_str(), ival);
    EXPECT_EQ(ival, newParmValue);
}

TEST(ParmAssignTest, ParameterNameChange)
{
    smks::scn::test::SceneContent1  all;

    const std::string   parmName    = "my_parm";
    const int           parmValue   = 42;

    smks::scn::test::FindNodesWithParm<int> finder;

    smks::scn::ParmAssign<int>::Ptr     assign  = smks::scn::ParmAssign<int>::create(
        parmName.c_str(), parmValue, smks::scn::TreeNodeVisitor::DESCENDING, nullptr, "__assign", all.scene);

    // regex selection
    smks::scn::TreeNodeSelector::Ptr    select  = smks::scn::TreeNodeSelector::create(".*/bb", "__select", all.scene);

    unsigned int selectorId = select->id();
    assign->setParameter<unsigned int>(smks::parm::selectorId(), selectorId);

    // check correctness
    finder.initialize(parmName);
    all.scene->accept(finder);

    smks::xchg::IdSet expected;
    expected.insert(all.node_b_bb->id());
    expected.insert(all.node_b_bb_bb1->id());
    expected.insert(all.node_b_bb_bb2->id());
    EXPECT_TRUE(smks::xchg::equal(finder.found(), expected));

    // change parameter name
    const std::string newParmName = "my_new_parm";
    assign->setParameter<std::string>(smks::parm::parmName(), newParmName);

    // check previous parm name is nowhere to be found
    finder.initialize(parmName);
    all.scene->accept(finder);
    EXPECT_TRUE(finder.found().empty());
    // check new parm name is found everywhere it needs to be
    finder.initialize(newParmName);
    all.scene->accept(finder);
    EXPECT_TRUE(smks::xchg::equal(finder.found(), expected));

    int ival = 0;

    all.node_b_bb->getParameter<int>(newParmName.c_str(), ival);
    EXPECT_EQ(ival, parmValue);
}

TEST(ParmAssignTest, MatchedDeletionAndAddition)
{
    smks::scn::test::SceneContent1  all;

    const std::string   parmName    = "my_parm";
    const int           parmValue   = 42;

    smks::scn::test::FindNodesWithParm<int> finder(parmName);

    smks::scn::TreeNodeSelector::Ptr    select  = smks::scn::TreeNodeSelector::create("/a.*", "__select", all.scene);
    smks::scn::ParmAssign<int>::Ptr     assign  = smks::scn::ParmAssign<int>::create(parmName.c_str(), parmValue, "__assign", all.scene);

    unsigned int selectorId = select->id();
    assign->setParameter<unsigned int>(smks::parm::selectorId(), selectorId);

    // check correctness
    smks::xchg::IdSet expected;
    expected.insert(all.node_a->id());
    expected.insert(all.node_a_b->id());
    expected.insert(all.node_a_aa->id());
    expected.insert(all.node_a_ab->id());
    expected.insert(all.node_a_aa_a->id());

    finder.initialize(parmName);
    all.scene->accept(finder);
    EXPECT_TRUE(smks::xchg::equal(finder.found(), expected));

    const unsigned int ID_a_aa = all.node_a_aa->id();
    //---
    smks::scn::TreeNode::destroy(all.node_a_aa);
    //---

    expected.erase(ID_a_aa);
    expected.erase(all.node_a_aa_a->id());

    finder.initialize(parmName);
    all.scene->accept(finder);
    EXPECT_TRUE(smks::xchg::equal(finder.found(), expected));

    //---
    smks::scn::TreeNode::Ptr node_a_c = smks::scn::TreeNode::create("c", all.node_a);
    //---

    expected.insert(node_a_c->id());

    finder.initialize(parmName);
    all.scene->accept(finder);
    EXPECT_TRUE(smks::xchg::equal(finder.found(), expected));
}

TEST(ParmAssignTest, AssignDeletion)
{
    smks::scn::test::SceneContent1  all;

    const std::string   parmName    = "my_parm";
    const int           parmValue   = 42;

    smks::scn::test::FindNodesWithParm<int> finder(parmName);

    smks::scn::ParmAssign<int>::Ptr assign = smks::scn::ParmAssign<int>::create(parmName.c_str(), parmValue, "__assign", all.scene);
    smks::xchg::IdSet               selection;

    selection.insert(all.node_b_bb->id());
    assign->setParameter<smks::xchg::IdSet>(smks::parm::selection(), selection);

    finder.clear();
    all.scene->accept(finder);
    EXPECT_TRUE(smks::xchg::equal(finder.found(), selection));

    smks::scn::TreeNode::destroy(assign);

    // check correctness
    finder.clear();
    all.scene->accept(finder);
    EXPECT_TRUE(finder.found().empty());
}

TEST(ParmAssignTest, ParmRemovalEvent_Unassignment)
{
    using namespace smks::scn;

    test::EventHistory  evts;
    std::vector<size_t> idx;

    const std::string   parmName    = "my_id";
    const unsigned int  parmId      = smks::util::string::getId(parmName.c_str());

    Scene::Ptr                  scene   = Scene::create();
    test::TestNode::Ptr target  = test::TestNode::create("target", scene);
    test::TestNode::Ptr source  = test::TestNode::create("source", scene);
    IdAssign::Ptr               assign  = IdAssign::create(parmName.c_str(), source->id(), "assign", scene);

    smks::xchg::IdSet selection;

    selection.insert(target->id());
    assign->setParameter<smks::xchg::IdSet>(smks::parm::selection(), selection);

    target->flushHistory(&evts);
    idx = evts.getParmEvents(parmId);
    EXPECT_EQ(idx.size(), 1);
    EXPECT_EQ(evts[idx[0]].asParmEvent()->type(), smks::xchg::ParmEvent::PARM_ADDED);

    source->flushHistory(&evts);
    idx = evts.getParmEvents(parmId);
    EXPECT_TRUE(idx.empty());

    //---
    assign->unsetParameter(smks::parm::selection().id());
    //---

    target->flushHistory(&evts);
    idx = evts.getParmEvents(parmId);
    EXPECT_EQ(idx.size(), 1);
    EXPECT_EQ(evts[idx[0]].asParmEvent()->type(), smks::xchg::ParmEvent::PARM_REMOVED);

    Scene::destroy(scene);
}

TEST(ParmAssignTest, ParmRemovalEvent_AssignDeletion)
{
    using namespace smks::scn;

    test::EventHistory  evts;
    std::vector<size_t> idx;

    const std::string   parmName    = "my_id";
    const unsigned int  parmId      = smks::util::string::getId(parmName.c_str());

    Scene::Ptr                  scene   = Scene::create();
    test::TestNode::Ptr target  = test::TestNode::create("target", scene);
    test::TestNode::Ptr source  = test::TestNode::create("source", scene);
    IdAssign::Ptr               assign  = IdAssign::create(parmName.c_str(), source->id(), "assign", scene);

    smks::xchg::IdSet selection;

    selection.insert(target->id());
    assign->setParameter<smks::xchg::IdSet>(smks::parm::selection(), selection);

    target->flushHistory(&evts);
    idx = evts.getParmEvents(parmId);
    EXPECT_EQ(idx.size(), 1);
    EXPECT_EQ(evts[idx[0]].asParmEvent()->type(), smks::xchg::ParmEvent::PARM_ADDED);

    source->flushHistory(&evts);
    idx = evts.getParmEvents(parmId);
    EXPECT_TRUE(idx.empty());

    //---
    IdAssign::destroy(assign);
    //---

    target->flushHistory(&evts);
    idx = evts.getParmEvents(parmId);
    EXPECT_EQ(idx.size(), 1);
    EXPECT_EQ(evts[idx[0]].asParmEvent()->type(), smks::xchg::ParmEvent::PARM_REMOVED);

    Scene::destroy(scene);
}

TEST(ParmAssignTest, SelectorDeletion)
{
    smks::scn::test::FindNodesWithParm<int> finder;
    smks::scn::test::SceneContent1          all;

    const std::string   parmName1   = "my_parm1";
    const std::string   parmName2   = "my_parm2";
    const int           parmValue1  = 42;
    const int           parmValue2  = 43;

    smks::scn::TreeNodeSelector::Ptr    select  = smks::scn::TreeNodeSelector::create("/a.*", "__select", all.scene);

    smks::scn::ParmAssign<int>::Ptr     assign1 = smks::scn::ParmAssign<int>::create(
        parmName1.c_str(), parmValue1, "__assign1", all.scene);
    smks::scn::ParmAssign<int>::Ptr     assign2 = smks::scn::ParmAssign<int>::create(
        parmName2.c_str(), parmValue2, "__assign2", all.scene);

    unsigned int selectorId = select->id();
    assign1->setParameter<unsigned int>(smks::parm::selectorId(), selectorId);
    assign2->setParameter<unsigned int>(smks::parm::selectorId(), selectorId);

    // check correctness
    smks::xchg::IdSet expected;
    expected.insert(all.node_a->id());
    expected.insert(all.node_a_b->id());
    expected.insert(all.node_a_aa->id());
    expected.insert(all.node_a_ab->id());
    expected.insert(all.node_a_aa_a->id());

    finder.initialize(parmName1);
    all.scene->accept(finder);
    //smks::scn::test::printNodes(std::cout << "nodes : ", finder.found(), all.scene);
    EXPECT_TRUE(smks::xchg::equal(finder.found(), expected));

    finder.initialize(parmName2);
    all.scene->accept(finder);
    EXPECT_TRUE(smks::xchg::equal(finder.found(), expected));

    //---
    assign1->unsetParameter(smks::parm::selectorId().id());
    //---

    // check correctness
    finder.initialize(parmName1);
    all.scene->accept(finder);
    EXPECT_TRUE(finder.found().empty());

    finder.initialize(parmName2);
    all.scene->accept(finder);
    EXPECT_TRUE(smks::xchg::equal(finder.found(), expected));
}

TEST(ParmAssignTest, PriorityChange)
{
    smks::scn::test::EqualParm<int>::Ptr    eq = smks::scn::test::EqualParm<int>::create();
    smks::scn::test::FindNodesWithParm<int> finder;
    smks::scn::test::SceneContent1          all;

    const std::string   parmName        = "my_parm";
    const int           parmValue_p3    = 24;
    const int           parmValue_p5    = 42;
    const int           parmValue_p8    = 100;

    smks::scn::TreeNodeSelector::Ptr    select_p5   = smks::scn::TreeNodeSelector::create(".*/.*/a", "__select_p5", all.scene);
    smks::scn::ParmAssign<int>::Ptr     assign_p5   = smks::scn::ParmAssign<int>::create(
        parmName.c_str(), parmValue_p5, "__assign_p5", all.scene);
    smks::scn::TreeNodeSelector::Ptr    select_p3   = smks::scn::TreeNodeSelector::create("/b.*", "__select_p3", all.scene);
    smks::scn::ParmAssign<int>::Ptr     assign_p3   = smks::scn::ParmAssign<int>::create(
        parmName.c_str(), parmValue_p3, "__assign_p3", all.scene);
    smks::scn::TreeNodeSelector::Ptr    select_p8   = smks::scn::TreeNodeSelector::create("/a.*", "__select_p8", all.scene);
    smks::scn::ParmAssign<int>::Ptr     assign_p8   = smks::scn::ParmAssign<int>::create(
        parmName.c_str(), parmValue_p8, "__assign_p8", all.scene);

    unsigned int    selectorId  = select_p5->id();
    int             priority    = 5;
    assign_p5->setParameter<int>(smks::parm::priority(), priority);
    assign_p5->setParameter<unsigned int>(smks::parm::selectorId(), selectorId);

    //smks::scn::test::printNodes(std::cout << "p5 -> ", select_p5->getParameter<smks::xchg::IdSet>(smks::parm::selection.str()), all.scene) << std::endl;

    selectorId  = select_p3->id();
    priority    = 3;
    assign_p3->setParameter<int>(smks::parm::priority(), priority);
    assign_p3->setParameter<unsigned int>(smks::parm::selectorId(), selectorId);

    //smks::scn::test::printNodes(std::cout << "p3 -> ", select_p3->getParameter<smks::xchg::IdSet>(smks::parm::selection.str()), all.scene) << std::endl;

    selectorId  = select_p8->id();
    priority    = 8;
    assign_p8->setParameter<int>(smks::parm::priority(), priority);
    assign_p8->setParameter<unsigned int>(smks::parm::selectorId(), selectorId);

    //smks::scn::test::printNodes(std::cout << "p8 -> ", select_p8->getParameter<smks::xchg::IdSet>(smks::parm::selection.str()), all.scene) << std::endl;
    int ival = 0;

    all.node_a          ->getParameter<int>(parmName.c_str(), ival);        EXPECT_EQ(ival, parmValue_p8);
    all.node_a_b        ->getParameter<int>(parmName.c_str(), ival);        EXPECT_EQ(ival, parmValue_p8);
    all.node_a_aa       ->getParameter<int>(parmName.c_str(), ival);        EXPECT_EQ(ival, parmValue_p8);
    all.node_a_ab       ->getParameter<int>(parmName.c_str(), ival);        EXPECT_EQ(ival, parmValue_p8);
    all.node_a_aa_a     ->getParameter<int>(parmName.c_str(), ival);        EXPECT_EQ(ival, parmValue_p8);
    all.node_b          ->getParameter<int>(parmName.c_str(), ival);        EXPECT_EQ(ival, parmValue_p3);
    all.node_b_ba       ->getParameter<int>(parmName.c_str(), ival);        EXPECT_EQ(ival, parmValue_p3);
    all.node_b_bb       ->getParameter<int>(parmName.c_str(), ival);        EXPECT_EQ(ival, parmValue_p3);
    all.node_b_bb_bb1   ->getParameter<int>(parmName.c_str(), ival);        EXPECT_EQ(ival, parmValue_p3);
    all.node_b_bb_bb2   ->getParameter<int>(parmName.c_str(), ival);        EXPECT_EQ(ival, parmValue_p3);
    all.node_b_a        ->getParameter<int>(parmName.c_str(), ival);        EXPECT_EQ(ival, parmValue_p5);

    smks::scn::TreeNode::destroy(assign_p5);

    all.node_a_aa_a     ->getParameter<int>(parmName.c_str(), ival);        EXPECT_EQ(ival, parmValue_p8);
    all.node_b_a        ->getParameter<int>(parmName.c_str(), ival);        EXPECT_EQ(ival, parmValue_p3);
    finder.initialize(parmName, parmValue_p5, eq);
    all.scene->accept(finder);
    EXPECT_TRUE(finder.found().empty());

    smks::scn::TreeNode::destroy(assign_p8);

    finder.initialize(parmName, parmValue_p8, eq);
    all.scene->accept(finder);
    EXPECT_TRUE(finder.found().empty());

    finder.initialize(parmName, parmValue_p3, eq);
    all.scene->accept(finder);

    smks::xchg::IdSet expected;
    expected.insert(all.node_b          ->id());
    expected.insert(all.node_b_ba       ->id());
    expected.insert(all.node_b_bb       ->id());
    expected.insert(all.node_b_bb_bb1   ->id());
    expected.insert(all.node_b_bb_bb2   ->id());
    expected.insert(all.node_b_a        ->id());
    EXPECT_TRUE(smks::xchg::equal(finder.found(), expected));

    smks::scn::TreeNode::destroy(assign_p3);

    finder.initialize(parmName);
    all.scene->accept(finder);
    EXPECT_TRUE(finder.found().empty());
}
