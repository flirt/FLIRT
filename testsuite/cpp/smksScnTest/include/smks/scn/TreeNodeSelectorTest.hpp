// ========================================================================= //
// Copyright 2016-2018 SUPAMONKS_STUDIO                                      //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
//                                                                           //
// ========================================================================= //
#pragma once

#include <gtest/gtest.h>

#include <smks/parm/builtins.hpp>
#include <smks/scn/TreeNodeSelector.hpp>
#include "smks/scn/test/util.hpp"


TEST(TreeNodeSelectorTest, Matching)
{
    smks::xchg::IdSet   refSelection;
    smks::xchg::IdSet   selection;
    std::string         regex = ".*";

    smks::scn::test::SceneContent1      all;
    smks::scn::TreeNodeSelector::Ptr    select = smks::scn::TreeNodeSelector::create(regex.c_str(), "select", all.node_b_bb_bb2);

    select->getParameter<smks::xchg::IdSet>(smks::parm::selection(), selection);

    refSelection.clear();
    refSelection.insert(all.scene           ->id());
    refSelection.insert(all.node_a          ->id());
    refSelection.insert(all.node_b          ->id());
    refSelection.insert(all.node_a_b        ->id());
    refSelection.insert(all.node_a_aa       ->id());
    refSelection.insert(all.node_a_ab       ->id());
    refSelection.insert(all.node_a_aa_a     ->id());
    refSelection.insert(all.node_b_a        ->id());
    refSelection.insert(all.node_b_ba       ->id());
    refSelection.insert(all.node_b_bb       ->id());
    refSelection.insert(all.node_b_bb_bb1   ->id());
    refSelection.insert(all.node_b_bb_bb2   ->id());
    refSelection.insert(select              ->id());

    EXPECT_TRUE(smks::xchg::equal(selection, refSelection));
    //---
    regex = "a";
    select->setParameter<std::string>       (smks::parm::regex(),       regex);
    select->getParameter<smks::xchg::IdSet> (smks::parm::selection(),   selection);

    EXPECT_TRUE(selection.empty());
    //---
    regex = "/a";
    select->setParameter<std::string>       (smks::parm::regex(),       regex);
    select->getParameter<smks::xchg::IdSet> (smks::parm::selection(),   selection);

    refSelection.clear();
    refSelection.insert(all.node_a->id());
    EXPECT_TRUE(smks::xchg::equal(selection, refSelection));
    //---
    regex = "/a.*";
    select->setParameter<std::string>       (smks::parm::regex(),       regex);
    select->getParameter<smks::xchg::IdSet> (smks::parm::selection(),   selection);

    refSelection.clear();
    refSelection.insert(all.node_a      ->id());
    refSelection.insert(all.node_a_aa   ->id());
    refSelection.insert(all.node_a_ab   ->id());
    refSelection.insert(all.node_a_aa_a ->id());
    refSelection.insert(all.node_a_b    ->id());
    EXPECT_TRUE(smks::xchg::equal(selection, refSelection));
    //---
    regex = ".*bb.*";
    select->setParameter<std::string>       (smks::parm::regex(),       regex);
    select->getParameter<smks::xchg::IdSet> (smks::parm::selection(),   selection);

    refSelection.clear();
    refSelection.insert(all.node_b_bb       ->id());
    refSelection.insert(all.node_b_bb_bb1   ->id());
    refSelection.insert(all.node_b_bb_bb2   ->id());
    refSelection.insert(select              ->id());
    EXPECT_TRUE(smks::xchg::equal(selection, refSelection));
    //---
}

TEST(TreeNodeSelectorTest, RegexDeletion)
{
    smks::xchg::IdSet   refSelection;
    smks::xchg::IdSet   selection;
    std::string         regex = "/a.*";

    smks::scn::test::SceneContent1      all;
    smks::scn::TreeNodeSelector::Ptr    select = smks::scn::TreeNodeSelector::create(regex.c_str(), "select", all.node_b_bb_bb2);

    select->getParameter<smks::xchg::IdSet> (smks::parm::selection(),   selection);

    refSelection.clear();
    refSelection.insert(all.node_a      ->id());
    refSelection.insert(all.node_a_aa   ->id());
    refSelection.insert(all.node_a_ab   ->id());
    refSelection.insert(all.node_a_aa_a ->id());
    refSelection.insert(all.node_a_b    ->id());
    EXPECT_TRUE(smks::xchg::equal(selection, refSelection));
    //---
    select->unsetParameter(smks::parm::regex().id());
    select->getParameter<smks::xchg::IdSet> (smks::parm::selection(),   selection);

    EXPECT_TRUE(selection.empty());
}

TEST(TreeNodeSelectorTest, MatchedNodeAddition)
{
    smks::xchg::IdSet   refSelection;
    smks::xchg::IdSet   selection;
    std::string         regex = "/a.*";

    smks::scn::test::SceneContent1      all;
    smks::scn::TreeNodeSelector::Ptr    select = smks::scn::TreeNodeSelector::create(regex.c_str(), "select", all.node_b_bb_bb2);

    select->getParameter<smks::xchg::IdSet>(smks::parm::selection(), selection);

    refSelection.clear();
    refSelection.insert(all.node_a      ->id());
    refSelection.insert(all.node_a_aa   ->id());
    refSelection.insert(all.node_a_ab   ->id());
    refSelection.insert(all.node_a_aa_a ->id());
    refSelection.insert(all.node_a_b    ->id());
    EXPECT_TRUE(smks::xchg::equal(selection, refSelection));
    //---
    smks::scn::TreeNode::Ptr const node_a_ab_c = smks::scn::TreeNode::create("c", all.node_a_ab);
    select->getParameter<smks::xchg::IdSet>(smks::parm::selection(), selection);

    refSelection.insert(node_a_ab_c->id());
    EXPECT_TRUE(smks::xchg::equal(selection, refSelection));
}

TEST(TreeNodeSelectorTest, MatchedNodeDeletion)
{
    smks::xchg::IdSet   refSelection;
    smks::xchg::IdSet   selection;
    std::string         regex = "/a.*";

    smks::scn::test::SceneContent1      all;
    smks::scn::TreeNodeSelector::Ptr    select = smks::scn::TreeNodeSelector::create(regex.c_str(), "select", all.node_b_bb_bb2);

    select->getParameter<smks::xchg::IdSet>(smks::parm::selection(), selection);

    refSelection.clear();
    refSelection.insert(all.node_a      ->id());
    refSelection.insert(all.node_a_aa   ->id());
    refSelection.insert(all.node_a_ab   ->id());
    refSelection.insert(all.node_a_aa_a ->id());
    refSelection.insert(all.node_a_b    ->id());
    EXPECT_TRUE(smks::xchg::equal(selection, refSelection));
    //---
    const unsigned int ID_a_aa = all.node_a_aa->id();

    smks::scn::TreeNode::destroy(all.node_a_aa);
    select->getParameter<smks::xchg::IdSet>(smks::parm::selection(), selection);

    refSelection.erase(ID_a_aa);
    refSelection.erase(all.node_a_aa_a  ->id());
    EXPECT_TRUE(smks::xchg::equal(selection, refSelection));
}
