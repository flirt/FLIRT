// ========================================================================= //
// Copyright 2016-2018 SUPAMONKS_STUDIO                                      //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
//                                                                           //
// ========================================================================= //
#pragma once

#include <gtest/gtest.h>

#include <smks/parm/builtins.hpp>

#include <smks/scn/Scene.hpp>
#include <smks/scn/ParmConnection.hpp>
#include <smks/scn/TreeNodeSelector.hpp>
#include <smks/scn/IdAssign.hpp>
#include "smks/scn/test/util.hpp"
#include "smks/scn/test/TargetLinkParmObserver.hpp"

// - id parameter direct setting/unsetting
// - simple id connection on several nodes -> test presence + parm event handling
// - multiple id connections on single node
// - node links tracking

TEST(NodeLinkingTest, LinkingParameterTracking)
{
    smks::xchg::IdSet links, expected;

    smks::scn::Scene::Ptr       scene   = smks::scn::Scene::create();

    smks::scn::TreeNode::Ptr    target      = smks::scn::TreeNode::create("target", scene);
    smks::scn::TreeNode::Ptr    linked[2]   =
    {
        smks::scn::TreeNode::create("linked_0", scene),
        smks::scn::TreeNode::create("linked_1", scene)
    };

    std::string     parmName[3] =
    {
        "link_1",
        "link_2",
        "link_3"
    };

    unsigned int    parmId[3];
    for (size_t i = 0; i < 3; ++i)
        parmId[i] = smks::util::string::getId(parmName[i].c_str());

    // add first linking parameter
    target->setParameterId(parmName[0].c_str(), linked[0]->id());

    target->getLinksFrom(linked[0]->id(), links);
    expected.clear(); expected.insert(parmId[0]);
    EXPECT_TRUE(smks::xchg::equal(links, expected));
    target->getLinksFrom(linked[1]->id(), links);
    EXPECT_TRUE(links.empty());

    // add second redundant parameter
    target->setParameterId(parmName[1].c_str(), linked[0]->id());

    target->getLinksFrom(linked[0]->id(), links);
    expected.clear(); expected.insert(parmId[0]); expected.insert(parmId[1]);
    EXPECT_TRUE(smks::xchg::equal(links, expected));
    target->getLinksFrom(linked[1]->id(), links);
    EXPECT_TRUE(links.empty());

    // add linking parameter to another linked node
    target->setParameterId(parmName[2].c_str(), linked[1]->id());

    target->getLinksFrom(linked[0]->id(), links);
    expected.clear(); expected.insert(parmId[0]); expected.insert(parmId[1]);
    EXPECT_TRUE(smks::xchg::equal(links, expected));
    target->getLinksFrom(linked[1]->id(), links);
    expected.clear(); expected.insert(parmId[2]);
    EXPECT_TRUE(smks::xchg::equal(links, expected));

    // remove redundant linking parameter
    target->unsetParameter(parmName[0].c_str());

    target->getLinksFrom(linked[0]->id(), links);
    expected.clear(); expected.insert(parmId[1]);
    EXPECT_TRUE(smks::xchg::equal(links, expected));

    // keep on unsetting links
    target->unsetParameter(parmName[1].c_str());

    target->getLinksFrom(linked[0]->id(), links);
    EXPECT_TRUE(links.empty());

    // keep on unsetting links
    target->unsetParameter(parmName[2].c_str());

    target->getLinksFrom(linked[1]->id(), links);
    EXPECT_TRUE(links.empty());

    smks::scn::Scene::destroy(scene);
}

TEST(NodeLinkingTest, AddRemoveParameterId)
{
    smks::scn::test::EventHistory   evts;

    smks::scn::Scene::Ptr           scene   = smks::scn::Scene::create();
    smks::scn::test::TestNode::Ptr  target  = smks::scn::test::TestNode::create("target", scene);
    smks::scn::TreeNode::Ptr        linked  = smks::scn::TreeNode::create("linked", scene);

    const std::string linkedParmName    = "linked_parm";
    const std::string parmName          = "my_link";

    linked->setParameter<std::string>(linkedParmName.c_str(), "created");

    target->setParameterId(parmName.c_str(), linked->id());
    target->flushHistory(&evts);
    EXPECT_EQ(evts.getParmEvents(linkedParmName, linked->id()).size(), 1);

    linked->setParameter<std::string>(linkedParmName.c_str(), "updated");
    target->flushHistory(&evts);
    EXPECT_EQ(evts.getParmEvents(linkedParmName, linked->id()).size(), 1);

    target->unsetParameter(parmName.c_str());
    target->flushHistory(&evts);
    EXPECT_EQ(evts.getParmEvents(linkedParmName, linked->id()).size(), 1);

    smks::scn::Scene::destroy(scene);
}

TEST(NodeLinkingTest, IdParameterSetter)
{
    smks::scn::test::SceneContent1          all;
    smks::scn::test::TestNode::Ptr const&   target = all.node_b_bb;

    const std::string   parmName    = "my_id"; // on target
    const unsigned int  parmId      = smks::util::string::getId(parmName.c_str());

    // _linked_ will be the node the ID parameter points to (and whose parm changes should be caught)
    const std::string   linkedName  = "_linked_";
    const unsigned int  linkedId    = smks::util::string::getId("/_linked_");

    smks::scn::TreeNode::Ptr linked = smks::scn::TreeNode::create(linkedName.c_str(), all.scene);

    // set parameter as an ID pointing to _linked_
    assert(linked->id() == linkedId);

    //---
    target->setParameterId(parmName.c_str(), linkedId);
    //---

    EXPECT_TRUE(target->parameterExistsAs<unsigned int>(parmName.c_str()));
    EXPECT_TRUE(target->parameterExistsAsId(parmName.c_str()));
    EXPECT_FALSE(target->parameterExistsAs<std::string>(parmName.c_str()));

    // change _linked_'s parameter set
    const std::string   linkedParmName  = "parm_from_link";
    const unsigned int  linkedParmId    = smks::util::string::getId(linkedParmName.c_str());
    linked->setParameter<std::string>(linkedParmName.c_str(), "my_value_from_linked");

    // check _linked_'s parameter change has been caught by target(s)
    smks::scn::test::EventHistory evts;

    target->flushHistory(&evts);
    EXPECT_EQ(evts.getParmEvents(linkedParmId, linked->id()).size(), 1);

    // make the parameter point to another node
    smks::scn::TreeNode::Ptr otherLinked    = smks::scn::TreeNode::create("_other_linked_", all.scene);

    //---
    target->updateParameter(parmId, otherLinked->id());
    //---

    otherLinked->setParameter<std::string>(linkedParmName.c_str(), "my_value_from_linked");

    // check _other_linked_'s parameter change has been caught by target(s)
    target->flushHistory(&evts);
    EXPECT_EQ(evts.getParmEvents(linkedParmId, otherLinked->id()).size(), 1);

    // remove parameter
    //---
    target->unsetParameter(parmName.c_str());
    //---

    EXPECT_FALSE(target->parameterExistsAs<unsigned int>(parmName.c_str()));
    EXPECT_FALSE(target->parameterExistsAsId(parmName.c_str()));
}

TEST(NodeLinkingTest, IdParameterSetter_LinkedNodeLateCreation)
{
    smks::scn::test::SceneContent1          all;
    smks::scn::test::TestNode::Ptr const&   target = all.node_b_bb;

    const std::string   parmName    = "my_id"; // on target
    const unsigned int  parmId      = smks::util::string::getId(parmName.c_str());

    // _linked_ will be the node the ID parameter points to (and whose parm changes should be caught)
    const std::string   linkedName  = "_linked_";
    const unsigned int  linkedId    = smks::util::string::getId("/_linked_");

    // set parameter as an ID pointing to _linked_
    target->setParameterId(parmName.c_str(), linkedId);

    EXPECT_TRUE(target->parameterExistsAs<unsigned int>(parmName.c_str()));
    EXPECT_TRUE(target->parameterExistsAsId(parmName.c_str()));
    EXPECT_FALSE(target->parameterExistsAs<std::string>(parmName.c_str()));

    // create linked node and change its parameter set
    const std::string           linkedParmName  = "parm_from_link";
    const unsigned int          linkedParmId    = smks::util::string::getId(linkedParmName.c_str());
    //---
    smks::scn::TreeNode::Ptr    linked          = smks::scn::TreeNode::create(linkedName.c_str(), all.scene);
    //---
    linked->setParameter<std::string>(linkedParmName.c_str(), "my_value_from_linked");

    // check _linked_'s parameter change has been caught by target(s)
    smks::scn::test::EventHistory evts;

    target->flushHistory(&evts);
    EXPECT_EQ(evts.getParmEvents(linkedParmId, linked->id()).size(), 1);

    // remove parameter
    target->unsetParameter(parmName.c_str());

    EXPECT_FALSE(target->parameterExistsAs<unsigned int>(parmName.c_str()));
    EXPECT_FALSE(target->parameterExistsAsId(parmName.c_str()));
}

TEST(NodeLinkingTest, IdConnection_MultipleTargets)
{
    static const size_t NUM = 2;

    smks::scn::test::SceneContent1  all;
    smks::scn::test::TestNode::Ptr  targets[NUM] = { all.node_a, all.node_b_a };

    // _connected_ will transfer its ID parameter to target nodes via connection
    const std::string   connectedName   = "_connected_";
    const unsigned int  connectedId     = smks::util::string::getId("/_connected_");
    // _linked_ will be the node the ID parameter points to (and whose parm changes should be caught)
    const std::string   linkedName      = "_linked_";
    const unsigned int  linkedId        = smks::util::string::getId("/_linked_");

    smks::scn::TreeNode::Ptr    connected   = smks::scn::TreeNode::create(connectedName.c_str(),    all.scene);
    smks::scn::TreeNode::Ptr    linked      = smks::scn::TreeNode::create(linkedName.c_str(),       all.scene);

    assert(connected->id() == connectedId);
    assert(linked->id() == linkedId);

    const std::string   parmName        = "my_id";  // on _connected_
    std::string         targetParmNames[NUM];
    for (size_t i = 0; i < NUM; ++i)
        targetParmNames[i] = parmName + "_on_target_" + std::to_string(i);

    // establish parameter connection
    unsigned int cnxId[NUM];
    for (size_t i = 0; i < NUM; ++i)
        cnxId[i] = all.scene->connectId(connectedId, parmName.c_str(), targets[i]->id(), targetParmNames[i].c_str());

    for (size_t i = 0; i < NUM; ++i)
        EXPECT_TRUE(all.scene->hasConnection(cnxId[i]));

    // make _connected_'s tracked parameter points to _linked_
    connected->setParameterId(parmName.c_str(), linkedId);

    // check connectivity
    for (size_t i = 0; i < NUM; ++i)
    {
        EXPECT_TRUE(targets[i]->parameterExistsAs<unsigned int>(targetParmNames[i].c_str()));
        EXPECT_TRUE(targets[i]->parameterExistsAsId(targetParmNames[i].c_str()));

        unsigned int uval = 0;
        targets[i]->getParameter<unsigned int>(targetParmNames[i].c_str(), uval);
        EXPECT_EQ(uval, linkedId);
    }

    // change _linked_'s parameter set
    const std::string   linkedParmName  = "parm_from_link";
    const unsigned int  linkedParmId    = smks::util::string::getId(linkedParmName.c_str());
    linked->setParameter<std::string>(linkedParmName.c_str(), "my_value_from_linked");

    // check _linked_'s parameter change has been caught by target(s)
    for (size_t i = 0; i < NUM; ++i)
    {
        smks::scn::test::EventHistory evts;

        targets[i]->flushHistory(&evts);
        EXPECT_EQ(evts.getParmEvents(linkedParmId, linked->id()).size(), 1);
    }

    // disconnect
    for (size_t i = 0; i < NUM; ++i)
        all.scene->disconnectId(cnxId[i]);

    for (size_t i = 0; i < NUM; ++i)
        EXPECT_FALSE(all.scene->hasConnection(cnxId[i]));
}

TEST(NodeLinkingTest, IdConnection_ConnectedNodeLateCreation)
{
    smks::scn::test::SceneContent1          all;
    smks::scn::test::TestNode::Ptr const&   target = all.node_b_bb;

    // _connected_ will transfer its ID parameter to target nodes via connection
    const std::string   connectedName   = "_connected_";
    const unsigned int  connectedId     = smks::util::string::getId("/_connected_");
    // _linked_ will be the node the ID parameter points to (and whose parm changes should be caught)
    const std::string   linkedName      = "_linked_";
    const unsigned int  linkedId        = smks::util::string::getId("/_linked_");

    smks::scn::TreeNode::Ptr linked     = smks::scn::TreeNode::create(linkedName.c_str(), all.scene);

    const std::string   parmName        = "my_id";  // on _connected_
    const std::string   targetParmName  = "my_id_on_target";    // on target
    const unsigned int  targetParmId    = smks::util::string::getId(targetParmName.c_str());

    // establish parameter connection
    const unsigned int cnx  = all.scene->connectId(connectedId, parmName.c_str(), target->id(), targetParmName.c_str());

    EXPECT_TRUE(all.scene->hasConnection(cnx));

    // create _connected_ and make _connected_'s tracked parameter points to _linked_
    //---
    smks::scn::TreeNode::Ptr connected = smks::scn::TreeNode::create(connectedName.c_str(), all.scene);
    //---
    connected->setParameterId(parmName.c_str(), linkedId);

    // check connectivity
    EXPECT_TRUE(target->parameterExistsAs<unsigned int>(targetParmName.c_str()));
    EXPECT_TRUE(target->parameterExistsAsId(targetParmName.c_str()));

    unsigned int uval = 0;
    target->getParameter<unsigned int>(targetParmName.c_str(), uval);
    EXPECT_EQ(uval, linkedId);

    // change _linked_'s parameter set
    const std::string   linkedParmName  = "parm_from_link";
    const unsigned int  linkedParmId    = smks::util::string::getId(linkedParmName.c_str());
    linked->setParameter<std::string>(linkedParmName.c_str(), "my_value_from_linked");

    // check _linked_'s parameter change has been caught by target(s)
    smks::scn::test::EventHistory evts;

    target->flushHistory(&evts);
    EXPECT_EQ(evts.getParmEvents(linkedParmId, linked->id()).size(), 1);

    // disconnect
    all.scene->disconnectId(cnx);

    EXPECT_FALSE(all.scene->hasConnection(cnx));
}

TEST(NodeLinkingTest, IdConnection_LinkedNodeLateCreation)
{
    smks::scn::test::SceneContent1          all;
    smks::scn::test::TestNode::Ptr const&   target = all.node_b_bb;

    // _connected_ will transfer its ID parameter to target nodes via connection
    const std::string   connectedName   = "_connected_";
    const unsigned int  connectedId     = smks::util::string::getId("/_connected_");
    // _linked_ will be the node the ID parameter points to (and whose parm changes should be caught)
    const std::string   linkedName      = "_linked_";
    const unsigned int  linkedId        = smks::util::string::getId("/_linked_");

    const std::string   parmName        = "my_id";  // on _connected_
    const std::string   targetParmName  = "my_id_on_target";    // on target
    const unsigned int  targetParmId    = smks::util::string::getId(targetParmName.c_str());

    // establish parameter connection
    const unsigned int cnx  = all.scene->connectId(connectedId, parmName.c_str(), target->id(), targetParmName.c_str());

    EXPECT_TRUE(all.scene->hasConnection(cnx));

    // create _connected_ and make _connected_'s tracked parameter points to _linked_
    smks::scn::TreeNode::Ptr connected = smks::scn::TreeNode::create(connectedName.c_str(), all.scene);

    connected->setParameterId(parmName.c_str(), linkedId);

    // check connectivity
    EXPECT_TRUE(target->parameterExistsAs<unsigned int>(targetParmName.c_str()));
    EXPECT_TRUE(target->parameterExistsAsId(targetParmName.c_str()));

    unsigned int uval = 0;
    target->getParameter<unsigned int>(targetParmName.c_str(), uval);
    EXPECT_EQ(uval, linkedId);

    // change _linked_'s parameter set
    const std::string   linkedParmName  = "parm_from_link";
    const unsigned int  linkedParmId    = smks::util::string::getId(linkedParmName.c_str());
    //---
    smks::scn::TreeNode::Ptr linked     = smks::scn::TreeNode::create(linkedName.c_str(), all.scene);
    //---
    linked->setParameter<std::string>(linkedParmName.c_str(), "my_value_from_linked");

    // check _linked_'s parameter change has been caught by target(s)
    smks::scn::test::EventHistory evts;

    target->flushHistory(&evts);
    EXPECT_EQ(evts.getParmEvents(linkedParmId, linked->id()).size(), 1);

    // disconnect
    all.scene->disconnectId(cnx);

    EXPECT_FALSE(all.scene->hasConnection(cnx));
}

TEST(NodeLinkingTest, IdConnection_SingleLinkToMultipleNodes)
{
    smks::scn::test::EventHistory                   evts;
    smks::scn::test::SceneContent1                  all;
    smks::scn::test::TestNode::Ptr const&   target = all.node_b_bb;

    // _connected_ will transfer its ID parameter to target nodes via connection
    const std::string   connectedName   = "_connected_";
    const unsigned int  connectedId     = smks::util::string::getId("/_connected_");

    // _linked_ will be the node the ID parameter points to (and whose parm changes should be caught)
    static const size_t NUM = 2;
    std::string                 linkedName      [NUM];
    smks::scn::TreeNode::Ptr    linked          [NUM];
    std::string                 linkedParmName  [NUM];
    unsigned int                linkedParmId    [NUM];
    std::string                 parmName        [NUM];

    for (size_t i = 0; i < NUM; ++i)
    {
        linkedName[i]       = "_linked_" + std::to_string(i);
        linked[i]           = smks::scn::TreeNode::create(linkedName[i].c_str(), all.scene);
        linkedParmName[i]   = "my_parm_on_linked_" + std::to_string(i);
        linkedParmId[i]     = smks::util::string::getId(linkedParmName[i].c_str());

        parmName[i]         = "my_parm_" + std::to_string(i);   // on _connected_

        linked[i]->setParameter<std::string>(linkedParmName[i].c_str(), "my_value_from_linked_" + std::to_string(i));
    }

    const std::string targetParmName = "my_parm_on_target"; // single node link on target

    // establish parameter connection
    unsigned int cnx[NUM];
    //---
    for (size_t i = 0; i < NUM; ++i)
        cnx[i] = all.scene->connectId(connectedId, parmName[i].c_str(), target->id(), targetParmName.c_str(), static_cast<int>(i) * 100);
    //---
    for (size_t i = 0; i < NUM; ++i)
        EXPECT_TRUE(all.scene->hasConnection(cnx[i]));

    // create _connected_ and make _connected_'s tracked parameter points to _linked_
    smks::scn::TreeNode::Ptr connected = smks::scn::TreeNode::create(connectedName.c_str(), all.scene);
    //---
    for (size_t i = 0; i < NUM; ++i)
    {
        const unsigned int parmId___ = connected->setParameterId(parmName[i].c_str(), linked[i]->id());
    }
    //---

    // check connectivity
    EXPECT_TRUE(target->parameterExistsAs<unsigned int>(targetParmName.c_str()));
    assert(NUM > 0);

    unsigned int uval = 0;
    target->getParameter<unsigned int>(targetParmName.c_str(), uval);
    EXPECT_EQ(uval, linked[NUM-1]->id());

    // change not prioritary linked node's parameters -> should have no effect on target
    linked[0]->setParameter<std::string>(linkedParmName[0].c_str(), "useless_value_since_low_priority");

    target->flushHistory(&evts);
    EXPECT_EQ   (evts.getParmEvents(0, linked[NUM-1]->id()).size(), 1);
    EXPECT_TRUE (evts.getParmEvents(0, linked[0]->id()).empty());

    // change parameter from highest priority connection
    linked[NUM-1]->setParameter<std::string>(linkedParmName[NUM-1].c_str(), "new_value_from_highest_priority");

    target->flushHistory(&evts);
    EXPECT_EQ   (evts.getParmEvents(0, linked[NUM-1]->id()).size(), 1);

    // change weakest connection's priority to highest
    all.scene->connectionById(cnx[0])->priority((NUM + 1) * 100);
    target->flushHistory(&evts);
    EXPECT_EQ(evts.getParmEvents(linkedParmId[NUM-1],   linked[NUM-1]->id()).size(),    1); // deletion
    EXPECT_EQ(evts.getParmEvents(linkedParmId[0],       linked[0]->id()).size(),        1); // addition

    linked[0]->setParameter<std::string>(linkedParmName[0].c_str(), "new_value_from_new_highest_priority");
    target->flushHistory(&evts);
    EXPECT_EQ(evts.getParmEvents(linkedParmId[0], linked[0]->id()).size(), 1);  // update

    // remove strongest connection
    all.scene->disconnectId(cnx[0]);
    linked[NUM-1]->setParameter<std::string>(linkedParmName[NUM-1].c_str(), "final_value_from_highest_priority");
    target->flushHistory(&evts);
    EXPECT_EQ(evts.getParmEvents(linkedParmId[NUM-1], linked[NUM-1]->id()).size(), 2); // addition due to priority change + update

    // disconnect remaining connections
    for (size_t i = 1; i < NUM; ++i)
        all.scene->disconnectId(cnx[i]);

    for (size_t i = 0; i < NUM; ++i)
        EXPECT_FALSE(all.scene->hasConnection(cnx[i]));
}

TEST(NodeLinkingTest, IdAssign)
{
    smks::scn::test::SceneContent1  all;

    // _linked_ will be the node the ID parameter points to (and whose parm changes should be caught)
    const std::string   linkedName  = "_linked_";
    const unsigned int  linkedId    = smks::util::string::getId("/_linked_");

    const std::string   parmName    = "my_parm";

    smks::scn::test::FindNodesWithParm<unsigned int> finder(parmName);

    smks::scn::TreeNodeSelector::Ptr    select  = smks::scn::TreeNodeSelector::create(".*/.*/a", "__select_p5", all.scene);
    smks::scn::IdAssign::Ptr            assign  = smks::scn::IdAssign::create(parmName.c_str(), linkedId, "__assign", all.scene);

    assign->setParameterId(smks::parm::selectorId().c_str(), select->id());

    // check selection correctness
    smks::xchg::IdSet expected;
    expected.insert(all.node_b_a->id());
    expected.insert(all.node_a_aa_a->id());

    finder.initialize(parmName, linkedId, nullptr);
    all.scene->accept(finder);
    EXPECT_TRUE(smks::xchg::equal(finder.found(), expected));

    // create linked node and set a parameter
    const std::string           linkedParmName  = "my_linked_parm";
    const unsigned int          linkedParmId    = smks::util::string::getId(linkedParmName.c_str());
    smks::scn::TreeNode::Ptr    linked          = smks::scn::TreeNode::create(linkedName.c_str(), all.scene);
    linked->setParameter<std::string>(linkedParmName.c_str(), "my_value");

    // check all targets got the information
    static const size_t             NUM = 2;
    smks::scn::test::TestNode::Ptr  targets[NUM] = { all.node_b_a, all.node_a_aa_a };

    for (size_t i = 0; i < NUM; ++i)
    {
        smks::scn::test::EventHistory evts;

        targets[i]->flushHistory(&evts);
        EXPECT_EQ(evts.getParmEvents(linkedParmId, linked->id()).size(), 1);
    }

    // remove assignment
    smks::scn::TreeNode::destroy(assign);

    finder.initialize(parmName);
    all.scene->accept(finder);
    EXPECT_TRUE(finder.found().empty());
}

TEST(NodeLinkingTest, IdConnection_UserOverwrite)
{
    static const size_t N = 2;  // num linked nodes

    smks::scn::test::EventHistory history;

    smks::scn::Scene::Ptr       scene = smks::scn::Scene::create();
    smks::scn::TreeNode::Ptr    linked          [N];
    std::string                 linkedParmName  [N];
    for (size_t n = 0; n < N; ++n)
    {
        const std::string name = "_linked_" + std::to_string(n);
        linked[n] = smks::scn::TreeNode::create(name.c_str(), scene);
        linkedParmName[n] = "linked_parm_" + std::to_string(n);
    }

    smks::scn::TreeNode::Ptr        source  = smks::scn::TreeNode::create("source", scene);
    smks::scn::test::TestNode::Ptr  target  = smks::scn::test::TestNode::create("target", scene);

    // connect source -> target
    const std::string parmName = "link";

    unsigned int cnxId = scene->connectId(source->id(), parmName.c_str(), target->id());
    source->setParameterId(parmName.c_str(), linked[0]->id());

    EXPECT_TRUE(scene->hasConnection(cnxId));
    EXPECT_TRUE(target->parameterExistsAs<unsigned int>(parmName.c_str()));
    EXPECT_TRUE(target->parameterExistsAsId(parmName.c_str()));

    unsigned int uval = 0;
    target->getParameter<unsigned int>(parmName.c_str(), uval);
    EXPECT_EQ(uval, linked[0]->id());

    // touch linked node in test link
    linked[0]->setParameter<std::string>(linkedParmName[0].c_str(), "my_linked_value");

    target->flushHistory(&history);
    for (size_t n = 0; n < N; ++n)
        if (n == 0)
            EXPECT_FALSE(history.getParmEvents(linkedParmName[n], linked[n]->id()).empty());
        else
            EXPECT_TRUE(history.getParmEvents(linkedParmName[n], linked[n]->id()).empty());

    // break ID connection by having the user explicitly set another ID value
    linked[1]->setParameter<char>(linkedParmName[1].c_str(), 'a');
    target->setParameterId(parmName.c_str(), linked[1]->id());

    EXPECT_FALSE(scene->hasConnection(cnxId));
    scene->disconnectId(cnxId); // should do nothing

    target->flushHistory();

    linked[0]->setParameter<std::string>(linkedParmName[0].c_str(), "my_other_linked_value");
    linked[1]->setParameter<char>(linkedParmName[1].c_str(), 'b');

    target->flushHistory(&history);
    for (size_t n = 0; n < N; ++n)
        if (n == 1)
            EXPECT_FALSE(history.getParmEvents(linkedParmName[n], linked[n]->id()).empty());
        else
            EXPECT_TRUE(history.getParmEvents(linkedParmName[n], linked[n]->id()).empty());

    // restore ID connection
    cnxId = scene->connectId(source->id(), parmName.c_str(), target->id());
    EXPECT_TRUE(scene->hasConnection(cnxId));

    target->flushHistory();

    linked[0]->setParameter<std::string>(linkedParmName[0].c_str(), "my_final_linked_value");
    linked[1]->setParameter<char>(linkedParmName[1].c_str(), 'c');

    target->flushHistory(&history);
    for (size_t n = 0; n < N; ++n)
        if (n == 0)
            EXPECT_FALSE(history.getParmEvents(linkedParmName[n], linked[n]->id()).empty());
        else
            EXPECT_TRUE(history.getParmEvents(linkedParmName[n], linked[n]->id()).empty());

    // disconnect ID connection
    scene->disconnectId(cnxId);
    EXPECT_FALSE(scene->hasConnection(cnxId));

    smks::scn::Scene::destroy(scene);
}

TEST(NodeLinkingTest, ParmEventsAlongHierarchy)
{
    using namespace smks;

    scn::Scene::Ptr                         scene   = scn::Scene::create();
    std::vector<scn::test::TestNode::Ptr>   nodes;

    nodes.push_back(scn::test::TestNode::create("a", scene));       // [0]
    nodes.push_back(scn::test::TestNode::create("b", scene));       // [1]
    nodes.push_back(scn::test::TestNode::create("c", nodes[1]));    // [2]

    const char*         linkParmName    = "link_parm";
    const unsigned int  linkParmId      = smks::util::string::getId(linkParmName);
    const char*         parmName        = "other_parm";
    const unsigned int  parmId          = smks::util::string::getId(parmName);

    //---
    nodes[1]->setParameterId(linkParmName, nodes[0]->id()); // [1] <- [0]
    //---
    for (size_t i = 0; i < nodes.size(); ++i)
        nodes[i]->flushHistory();
    //---
    nodes[0]->setParameter<int>(parmName, 42);
    //---

    EXPECT_TRUE (nodes[0]->parameterExistsAs<int>(parmName));
    EXPECT_FALSE(nodes[1]->parameterExistsAs<int>(parmName));
    EXPECT_FALSE(nodes[2]->parameterExistsAs<int>(parmName));

    // parameter event is expected to be directly sent to node_a, indirectly to node_b, and not to node_c
    scn::test::EventHistory evts;
    std::vector<size_t>     idx;
    const xchg::ParmEvent*  parmEvt;

    nodes[0]->flushHistory(&evts);
    idx = evts.getParmEvents();
    EXPECT_EQ(idx.size(), 1);
    parmEvt = evts[0].asParmEvent();
    EXPECT_EQ(parmEvt->parm(),  parmId);
    EXPECT_EQ(parmEvt->node(),  nodes[0]->id());
    EXPECT_FALSE(parmEvt->comesFromLink());

    nodes[1]->flushHistory(&evts);
    idx = evts.getParmEvents();
    EXPECT_EQ(idx.size(), 1);
    parmEvt = evts[0].asParmEvent();
    EXPECT_EQ(parmEvt->parm(),  parmId);
    EXPECT_EQ(parmEvt->node(),  nodes[0]->id());
    EXPECT_TRUE(parmEvt->comesFromLink());

    nodes[2]->flushHistory(&evts);
    EXPECT_TRUE(evts.empty());

    scn::Scene::destroy(scene);
}

TEST(NodeLinkingTest, BackLinkAlongHierarchy)
{
    using namespace smks;

    scn::Scene::Ptr                 scene   = scn::Scene::create();
    scn::test::TestNode::Ptr const& node_a  = scn::test::TestNode::create("a", scene);
    scn::test::TestNode::Ptr const& node_b  = scn::test::TestNode::create("b", node_a);
    scn::test::TestNode::Ptr const& node_c  = scn::test::TestNode::create("c", node_b);

    scn::test::TestNode::Ptr    nodes[]     = { node_a, node_b, node_c };
    const size_t                numNodes    = sizeof(nodes) / sizeof(scn::test::TestNode::Ptr);

    scn::test::EventHistory     evts;
    std::vector<size_t>         idx;

    const std::string   linkParmName    = "link_parm";
    const unsigned int  linkParmId      = smks::util::string::getId(linkParmName.c_str());
    const std::string   parmName        = "other_parm";
    const unsigned int  parmId          = smks::util::string::getId(parmName.c_str());

    node_a->setParameterId(linkParmName.c_str(), node_b->id());
    //---------
    for (size_t i = 0; i < numNodes; ++i)
        nodes[i]->flushHistory();
    //---
    node_b->setParameter<int>(parmName.c_str(), 42);
    //---
    const bool hasParm[] = { false, true, false };
    for (size_t i = 0; i < numNodes; ++i)
    {
        EXPECT_EQ(nodes[i]->parameterExistsAs<int>(parmName.c_str()), hasParm[i]);

        nodes[i]->flushHistory(&evts);
        idx = evts.getParmEvents();
        EXPECT_EQ(idx.size(), 1);
        const smks::xchg::ParmEvent* parmEvt = evts[idx[0]].asParmEvent();
        EXPECT_EQ(parmEvt->parm(), parmId);
        EXPECT_EQ(parmEvt->node(), node_b->id());
    }

    scn::Scene::destroy(scene);
}

TEST(NodeLinkingTest, SelfLinking)
{
    using namespace smks::scn;

    Scene::Ptr                  scene   = Scene::create();
    test::TestNode::Ptr const&  node_a  = test::TestNode::create("a", scene);

    const std::string   linkParmName    = "link_parm";
    const unsigned int  linkParmId      = smks::util::string::getId(linkParmName.c_str());
    const std::string   parmName        = "other_parm";
    const unsigned int  parmId          = smks::util::string::getId(parmName.c_str());

    //---
    const unsigned int cnx = node_a->setParameterId(linkParmName.c_str(), node_a->id());    // adds the ID parameter, but does not trigger actual linking
    //---
    node_a->flushHistory();

    //---
    node_a->setParameter<int>(parmName.c_str(), 42);    // will add the parameter, but no linking event will be recieved
    //---

    test::EventHistory evts;
    node_a->flushHistory(&evts);

    EXPECT_TRUE (node_a->parameterExistsAs<int>(parmName.c_str()));
    EXPECT_EQ(evts.size(), 1);
    EXPECT_TRUE(evts.front().asParmEvent() != nullptr);
    EXPECT_EQ(evts.front().asParmEvent()->node(), node_a->id());
    EXPECT_EQ(evts.front().asParmEvent()->parm(), parmId);

    Scene::destroy(scene);
}

TEST(NodeLinkingTest, LinkLoop)
{
    using namespace smks;

    scn::Scene::Ptr                 scene   = scn::Scene::create();
    scn::test::TestNode::Ptr const& node_a  = scn::test::TestNode::create("a", scene);
    scn::test::TestNode::Ptr const& node_b  = scn::test::TestNode::create("b", scene);
    scn::test::TestNode::Ptr const& node_c  = scn::test::TestNode::create("c", scene);

    scn::test::TestNode::Ptr    nodes[]     = { node_a, node_b, node_c };
    const size_t                numNodes    = sizeof(nodes) / sizeof(scn::test::TestNode::Ptr);

    const std::string   linkParmName    = "link_parm";
    const unsigned int  linkParmId      = smks::util::string::getId(linkParmName.c_str());
    const std::string   parmName        = "other_parm";
    const unsigned int  parmId          = smks::util::string::getId(parmName.c_str());

    //---
    node_a->setParameterId(linkParmName.c_str(), node_c->id());
    node_b->setParameterId(linkParmName.c_str(), node_a->id());
    node_c->setParameterId(linkParmName.c_str(), node_b->id());
    //---
    for (size_t i = 0; i < numNodes; ++i)
        nodes[i]->flushHistory();
    //---
    node_a->setParameter<int>(parmName.c_str(), 42);
    //---
    EXPECT_TRUE (node_a->parameterExistsAs<int>(parmName.c_str()));
    EXPECT_FALSE(node_b->parameterExistsAs<int>(parmName.c_str()));
    EXPECT_FALSE(node_c->parameterExistsAs<int>(parmName.c_str()));

    scn::test::EventHistory evts;
    std::vector<size_t>     idx;
    const xchg::ParmEvent*  parmEvt;

    node_a->flushHistory(&evts);
    idx = evts.getParmEvents();
    EXPECT_EQ(idx.size(), 1);
    parmEvt = evts[idx[0]].asParmEvent();
    EXPECT_EQ(parmEvt->parm(), parmId);
    EXPECT_EQ(parmEvt->node(), node_a->id());

    node_b->flushHistory(&evts);
    idx = evts.getParmEvents();
    EXPECT_EQ(idx.size(), 1);
    parmEvt = evts[idx[0]].asParmEvent();
    EXPECT_EQ(parmEvt->parm(), parmId);
    EXPECT_EQ(parmEvt->node(), node_a->id());

    node_c->flushHistory(&evts);
    EXPECT_TRUE(evts.getParmEvents().empty());

    scn::Scene::destroy(scene);
}

TEST(NodeLinkingTest, ParmRemovalEvent_LinkDeletion)
{
    using namespace smks;

    scn::Scene::Ptr                 scene       = scn::Scene::create();
    scn::TreeNode::Ptr              node_src    = scn::TreeNode::create("source", scene);
    scn::test::TestNode::Ptr const& node_trg    = scn::test::TestNode::create("target", scene);

    const std::string   parmName    = "my_id";
    const unsigned int  parmId      = smks::util::string::getId(parmName.c_str());

    node_trg->setParameterId(parmName.c_str(), node_src->id());
    node_trg->flushHistory();

    scn::test::EventHistory evts;
    std::vector<size_t> idx;

    //---
    node_trg->unsetParameter(parmId);
    node_trg->flushHistory(&evts);
    //---
    EXPECT_FALSE(node_trg->parameterExistsAs<unsigned int>(parmId));

    idx = evts.getParmEvents(xchg::ParmEvent::PARM_REMOVED);
    EXPECT_EQ(idx.size(), 1);
    const xchg::ParmEvent* parmEvt = evts[idx.front()].asParmEvent();
    EXPECT_EQ(parmEvt->type(),  xchg::ParmEvent::PARM_REMOVED);
    EXPECT_EQ(parmEvt->parm(),  parmId);
    EXPECT_EQ(parmEvt->node(),  node_trg->id());

    idx = evts.getSceneEvents(xchg::SceneEvent::NODES_UNLINKED);
    EXPECT_EQ(idx.size(), 1);
    const xchg::SceneEvent* sceneEvt = evts[idx.front()].asSceneEvent();
    EXPECT_EQ(sceneEvt->type(),     xchg::SceneEvent::NODES_UNLINKED);
    EXPECT_EQ(sceneEvt->target(),   node_trg->id());
    EXPECT_EQ(sceneEvt->source(),   node_src->id());

    scn::Scene::destroy(scene);
}

TEST(NodeLinkingTest, MultiLinking)
{
    using namespace smks::scn;

    Scene::Ptr                  scene       = Scene::create();
    TreeNode::Ptr               node_src1   = TreeNode::create("source_1", scene);
    TreeNode::Ptr               node_src2   = TreeNode::create("source_2", scene);
    test::TestNode::Ptr const&  node_trg    = test::TestNode::create("target", scene);

    const std::string   parmName    = "my_id";
    const unsigned int  parmId      = smks::util::string::getId(parmName.c_str());

    smks::xchg::IdSet sources;

    sources.insert(node_src1->id());
    sources.insert(node_src2->id());
    node_trg->setParameter<smks::xchg::IdSet>(parmName.c_str(), sources);

    test::EventHistory  evts;
    std::vector<size_t> idx;
    const std::string   parmSrcNames[2] = { "parm_src1", "parm_src2" };
    const unsigned int  parmSrcIds[2]   = { smks::util::string::getId(parmSrcNames[0].c_str()), smks::util::string::getId(parmSrcNames[1].c_str()) };

    //---
    node_trg->flushHistory();
    node_src1->setParameter<std::string>(parmSrcNames[0].c_str(), "toto");
    node_src2->setParameter<int>        (parmSrcNames[1].c_str(), 42);
    node_trg->flushHistory(&evts);
    //---

    idx = evts.getParmEvents();
    EXPECT_EQ(idx.size(), 2);
    EXPECT_EQ(evts[idx[0]].asParmEvent()->parm(),       parmSrcIds[0]);
    EXPECT_EQ(evts[idx[0]].asParmEvent()->node(),   node_src1->id());
    EXPECT_EQ(evts[idx[1]].asParmEvent()->parm(),       parmSrcIds[1]);
    EXPECT_EQ(evts[idx[1]].asParmEvent()->node(),   node_src2->id());

    Scene::destroy(scene);
}

TEST(NodeLinkingTest, CreationEvent_Then_LinkEvent)
{
    using namespace smks::scn;

    test::EventHistory          evts;
    test::SceneObserver::Ptr    obs     = test::SceneObserver::create();

    Scene::Ptr                  scene   = Scene::create();

    const unsigned int          srcId   = smks::util::string::getId("/source");
    TreeNode::Ptr const&        trgNode = test::TestNode::create("target", scene);

    scene->registerObserver(obs);

    const std::string   linkName    = "link";
    const unsigned int  linkId      = smks::util::string::getId(linkName.c_str());

    //---
    trgNode->setParameterId(linkName.c_str(), srcId);           //<! link is established first

    TreeNode::Ptr srcNode = TreeNode::create("source", scene);  //<! then the source node is created
    //---
    assert(srcNode->id() == srcId);

    obs->flushHistory(&evts);

    const std::vector<size_t>& idxCreated   = evts.getSceneEvents(smks::xchg::SceneEvent::NODE_CREATED);
    const std::vector<size_t>& idxLinked    = evts.getSceneEvents(smks::xchg::SceneEvent::NODES_LINKED);

    EXPECT_EQ(idxCreated.size(), 1);
    EXPECT_EQ(idxLinked.size(), 1);
    EXPECT_LT(idxCreated.front(), idxLinked.front());   //<! CREATED event is received before LINKED event

    scene->deregisterObserver(obs);

    Scene::destroy(scene);
}

TEST(NodeLinkingTest, BidirectionalQueryTest)
{
    using namespace smks;

    static const size_t NUM_SRC = 3;
    static const size_t NUM_TRG = 2;

    scn::Scene::Ptr scene = scn::Scene::create();

    std::vector<scn::TreeNode::Ptr> srcNodes(NUM_SRC, nullptr), trgNodes(NUM_TRG, nullptr);

    for (size_t i = 0; i < NUM_SRC; ++i)
    {
        const std::string name = "source_" + std::to_string(i);
        srcNodes[i] = scn::TreeNode::create(name.c_str(), scene);
    }
    for (size_t i = 0; i < NUM_TRG; ++i)
    {
        const std::string name = "target_" + std::to_string(i);
        trgNodes[i] = scn::TreeNode::create(name.c_str(), scene);
    }

    // /source_0    -> 3529679015
    // /source_1    -> 599245648
    // /source_2    -> 921037228
    //---------------------------
    // /target_0    -> 2570149893
    // /target_1    -> 1810859394

    struct { std::string name; unsigned int id; } p00, p00b, p10, p20, p21;

    p00.name    = "p00";    p00.id  = smks::util::string::getId(p00.name.c_str());
    p00b.name   = "p00b";   p00b.id = smks::util::string::getId(p00b.name.c_str());
    p10.name    = "p10";    p10.id  = smks::util::string::getId(p10.name.c_str());
    p20.name    = "p20";    p20.id  = smks::util::string::getId(p20.name.c_str());
    p21.name    = "p21";    p21.id  = smks::util::string::getId(p21.name.c_str());

    //---
    trgNodes[0]->setParameterId(p00.name.c_str(),   srcNodes[0]->id());
    trgNodes[0]->setParameterId(p00b.name.c_str(),  srcNodes[0]->id());
    trgNodes[0]->setParameterId(p10.name.c_str(),   srcNodes[1]->id());
    trgNodes[0]->setParameterId(p20.name.c_str(),   srcNodes[2]->id());
    trgNodes[1]->setParameterId(p21.name.c_str(),   srcNodes[2]->id());
    //---

    xchg::IdSet ids, refIds;

    srcNodes[0]->getLinkTargets(ids);
    refIds.clear(); refIds.insert(trgNodes[0]->id());
    EXPECT_TRUE(ids == refIds);
    EXPECT_EQ(srcNodes[0]->getNumLinksTo(trgNodes[0]->id()), 2);
    EXPECT_EQ(srcNodes[0]->getNumLinksTo(trgNodes[1]->id()), 0);
    srcNodes[0]->getLinksTo(trgNodes[0]->id(), ids);
    refIds.clear(); refIds.insert(p00.id); refIds.insert(p00b.id);
    EXPECT_TRUE(ids == refIds);

    srcNodes[1]->getLinkTargets(ids);
    refIds.clear(); refIds.insert(trgNodes[0]->id());
    EXPECT_TRUE(ids == refIds);
    EXPECT_EQ(srcNodes[1]->getNumLinksTo(trgNodes[0]->id()), 1);
    EXPECT_EQ(srcNodes[1]->getNumLinksTo(trgNodes[1]->id()), 0);
    srcNodes[1]->getLinksTo(trgNodes[0]->id(), ids);
    refIds.clear(); refIds.insert(p10.id);
    EXPECT_TRUE(ids == refIds);

    srcNodes[2]->getLinkTargets(ids);
    refIds.clear(); refIds.insert(trgNodes[0]->id()); refIds.insert(trgNodes[1]->id());
    EXPECT_TRUE(ids == refIds);
    EXPECT_EQ(srcNodes[2]->getNumLinksTo(trgNodes[0]->id()), 1);
    EXPECT_EQ(srcNodes[2]->getNumLinksTo(trgNodes[1]->id()), 1);
    srcNodes[2]->getLinksTo(trgNodes[0]->id(), ids);
    refIds.clear(); refIds.insert(p20.id);
    EXPECT_TRUE(ids == refIds);
    srcNodes[2]->getLinksTo(trgNodes[1]->id(), ids);
    refIds.clear(); refIds.insert(p21.id);
    EXPECT_TRUE(ids == refIds);

    //*******************

    trgNodes[0]->getLinkSources(ids);
    refIds.clear(); refIds.insert(srcNodes[0]->id()); refIds.insert(srcNodes[1]->id()); refIds.insert(srcNodes[2]->id());
    EXPECT_TRUE(ids == refIds);
    EXPECT_EQ(trgNodes[0]->getNumLinksFrom(srcNodes[0]->id()), 2);
    EXPECT_EQ(trgNodes[0]->getNumLinksFrom(srcNodes[1]->id()), 1);
    EXPECT_EQ(trgNodes[0]->getNumLinksFrom(srcNodes[2]->id()), 1);
    trgNodes[0]->getLinksFrom(srcNodes[0]->id(), ids);
    refIds.clear(); refIds.insert(p00.id); refIds.insert(p00b.id);
    EXPECT_TRUE(ids == refIds);
    trgNodes[0]->getLinksFrom(srcNodes[1]->id(), ids);
    refIds.clear(); refIds.insert(p10.id);
    EXPECT_TRUE(ids == refIds);
    trgNodes[0]->getLinksFrom(srcNodes[2]->id(), ids);
    refIds.clear(); refIds.insert(p20.id);
    EXPECT_TRUE(ids == refIds);

    trgNodes[1]->getLinkSources(ids);
    refIds.clear(); refIds.insert(srcNodes[2]->id());
    EXPECT_TRUE(ids == refIds);
    EXPECT_EQ(trgNodes[1]->getNumLinksFrom(srcNodes[0]->id()), 0);
    EXPECT_EQ(trgNodes[1]->getNumLinksFrom(srcNodes[1]->id()), 0);
    EXPECT_EQ(trgNodes[1]->getNumLinksFrom(srcNodes[2]->id()), 1);
    trgNodes[1]->getLinksFrom(srcNodes[2]->id(), ids);
    refIds.clear(); refIds.insert(p21.id);
    EXPECT_TRUE(ids == refIds);

    scn::Scene::destroy(scene);
}


TEST(NodeLinkingTest, LinkTargetQueryTest)
{
    using namespace smks;

    static const size_t NUM_SRC = 3;
    static const size_t NUM_TRG = 2;

    scn::Scene::Ptr     scene   = scn::Scene::create();
    scn::TreeNode::Ptr  src     = scn::TreeNode::create("source_0", scene);
    scn::TreeNode::Ptr  trg0    = scn::TreeNode::create("target_0", scene);
    scn::TreeNode::Ptr  trg1    = scn::TreeNode::create("target_1", scene);

    struct { std::string name; unsigned int id; } pa, pb, pc;

    pa.name = "pa"; pa.id   = smks::util::string::getId(pa.name.c_str());
    pb.name = "pb"; pb.id   = smks::util::string::getId(pb.name.c_str());
    pc.name = "pc"; pc.id   = smks::util::string::getId(pc.name.c_str());

    xchg::IdSet ids, refIds;

    src->getLinkTargets(ids);
    EXPECT_TRUE(ids.empty());

    //---
    trg0->setParameterId(pa.name.c_str(), src->id());
    //---

    src->getLinkTargets(ids);
    refIds.clear(); refIds.insert(trg0->id());
    EXPECT_TRUE(ids == refIds);
    src->getLinksTo(trg0->id(), ids);
    refIds.clear(); refIds.insert(pa.id);
    EXPECT_TRUE(ids == refIds);

    //---
    trg0->setParameterId(pb.name.c_str(), src->id());
    //---

    src->getLinkTargets(ids);
    refIds.clear(); refIds.insert(trg0->id());
    EXPECT_TRUE(ids == refIds);
    src->getLinksTo(trg0->id(), ids);
    refIds.clear(); refIds.insert(pa.id); refIds.insert(pb.id);
    EXPECT_TRUE(ids == refIds);

    //---
    trg1->setParameterId(pc.name.c_str(), src->id());
    //---

    src->getLinkTargets(ids);
    refIds.clear(); refIds.insert(trg0->id()); refIds.insert(trg1->id());
    EXPECT_TRUE(ids == refIds);
    src->getLinksTo(trg1->id(), ids);
    refIds.clear(); refIds.insert(pc.id);
    EXPECT_TRUE(ids == refIds);

    //---
    const unsigned int trg1Id = trg1->id();
    scn::TreeNode::destroy(trg1);
    //---

    src->getLinkTargets(ids);
    refIds.clear(); refIds.insert(trg0->id());
    EXPECT_TRUE(ids == refIds);
    src->getLinksTo(trg1Id, ids);
    EXPECT_TRUE(ids.empty());

    //---
    trg0->unsetParameter(pb.id);
    //---

    src->getLinkTargets(ids);
    refIds.clear(); refIds.insert(trg0->id());
    EXPECT_TRUE(ids == refIds);
    src->getLinksTo(trg0->id(), ids);
    refIds.clear(); refIds.insert(pa.id);
    EXPECT_TRUE(ids == refIds);

    scn::Scene::destroy(scene);
}

TEST(NodeLinkingTest, UnlinkedEvent_Then_DeletedEvent)
{
    using namespace smks;

    scn::test::EventHistory         evts;
    scn::test::SceneObserver::Ptr   obs = scn::test::SceneObserver::create();

    scn::Scene::Ptr     scene   = scn::Scene::create();
    scn::TreeNode::Ptr  src1    = scn::test::TestNode::create("source_1", scene);
    scn::TreeNode::Ptr  src2    = scn::test::TestNode::create("source_2", scene);
    scn::TreeNode::Ptr  src3    = scn::test::TestNode::create("source_3", scene);
    scn::TreeNode::Ptr  trg     =    scn::test::TestNode::create("target", scene);

    scene->registerObserver(obs);

    // node links are set
    unsigned int link1  = trg->setParameterId("link1",  src1->id());
    unsigned int link1b = trg->setParameterId("link1b", src1->id());
    unsigned int link2  = trg->setParameterId("link2",  src2->id());
    unsigned int link3  = trg->setParameterId("link3",  src3->id());

    obs->flushHistory();

    std::vector<size_t> idxDeleted, idxUnlinked;

    //---
    // destroy a redundant link
    trg->unsetParameter(link1b);
    //---
    obs->flushHistory(&evts);
    idxUnlinked = evts.getSceneEvents(xchg::SceneEvent::NODES_UNLINKED);
    EXPECT_TRUE(idxUnlinked.empty());

    //---
    // destroy a active link
    trg->unsetParameter(link1);
    //---
    obs->flushHistory(&evts);
    idxUnlinked = evts.getSceneEvents(xchg::SceneEvent::NODES_UNLINKED);
    EXPECT_EQ(idxUnlinked.size(), 1);

    //---
    // destroy source node
    scn::TreeNode::destroy(src2);
    //---
    obs->flushHistory(&evts);
    idxUnlinked = evts.getSceneEvents(xchg::SceneEvent::NODES_UNLINKED);
    idxDeleted  = evts.getSceneEvents(xchg::SceneEvent::NODE_DELETED);
    EXPECT_EQ(idxUnlinked.size(), 1);
    EXPECT_EQ(idxDeleted.size(), 1);
    EXPECT_LT(idxUnlinked.front(), idxDeleted.front()); //<! UNLINKED event is received before DELETED event

    //---
    // destroy target node
    scn::TreeNode::destroy(trg);
    //---
    obs->flushHistory(&evts);
    idxUnlinked = evts.getSceneEvents(xchg::SceneEvent::NODES_UNLINKED);
    idxDeleted  = evts.getSceneEvents(xchg::SceneEvent::NODE_DELETED);
    EXPECT_EQ(idxUnlinked.size(), 1);
    EXPECT_EQ(idxDeleted.size(), 1);
    EXPECT_LT(idxUnlinked.front(), idxDeleted.front()); //<! UNLINKED event is received before DELETED event

    scene->deregisterObserver(obs);

    scn::Scene::destroy(scene);
}

TEST(NodeLinkingTest, TargetParmsUpToDateDuringLinking)
{
    using namespace smks;

    scn::Scene::Ptr     scene   = scn::Scene::create();
    scn::TreeNode::Ptr  src     = scn::test::TestNode::create("source", scene);
    scn::TreeNode::Ptr  trg     = scn::test::TestNode::create("target", scene);
    const char* link            = "link";
    const bool  shouldParmExist = true;
    scn::test::TargetLinkParmObserver::Ptr obs = scn::test::TargetLinkParmObserver::create(link, smks::xchg::SceneEvent::NODES_LINKED, shouldParmExist);

    scene->registerObserver(obs);
    //---
    const unsigned int linkId = trg->setParameterId(link, src->id());
    EXPECT_TRUE(obs->ok());
    //---
    scene->deregisterObserver(obs);

    scn::Scene::destroy(scene);
}

TEST(NodeLinkingTest, TargetParmsUpToDateDuringUnlinking)
{
    using namespace smks;

    scn::Scene::Ptr     scene   = scn::Scene::create();
    scn::TreeNode::Ptr  src     = scn::test::TestNode::create("source", scene);
    scn::TreeNode::Ptr  trg     = scn::test::TestNode::create("target", scene);
    const char* link            = "link";
    const bool  shouldParmExist = false;
    scn::test::TargetLinkParmObserver::Ptr obs = scn::test::TargetLinkParmObserver::create(link, smks::xchg::SceneEvent::NODES_UNLINKED, shouldParmExist);

    scene->registerObserver(obs);
    const unsigned int linkId = trg->setParameterId(link, src->id());
    //---
    trg->unsetParameter(linkId);
    EXPECT_TRUE(obs->ok());
    //---
    scene->deregisterObserver(obs);

    scn::Scene::destroy(scene);
}

TEST(NodeLinkingTest, GetUnsetEventsFromDeletedSource)
{
    using namespace smks;

    scn::test::EventHistory evts;
    std::vector<size_t>     idx;
    scn::Scene::Ptr             scene   = scn::Scene::create();
    scn::TreeNode::Ptr          src     = scn::TreeNode::create("source", scene);
    scn::test::TestNode::Ptr    trg     = scn::test::TestNode::create("target", scene, &evts);
    const std::string   link    = "link";
    const std::string   parm    = "my_parm";

    const unsigned int linkId = trg->setParameterId     (link.c_str(), src->id());
    const unsigned int parmId = src->setParameter<int>  (parm.c_str(), 42);
    //---------
    evts.clear();
    //---
    const unsigned int srcId = src->id();
    scn::TreeNode::destroy(src);
    //---
    idx = evts.getParmEvents();
    EXPECT_EQ(idx.size(), 1);
    const xchg::ParmEvent* parmEvt = evts[idx.front()].asParmEvent();
    EXPECT_EQ(parmEvt->parm(),  parmId);
    EXPECT_EQ(parmEvt->node(),  srcId);
    EXPECT_EQ(parmEvt->type(),  xchg::ParmEvent::PARM_REMOVED);
    //---------

    scn::Scene::destroy(scene);
}
