// ========================================================================= //
// Copyright 2016-2018 SUPAMONKS_STUDIO                                      //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
//                                                                           //
// ========================================================================= //
#pragma once

#include <gtest/gtest.h>

#include <smks/scn/Scene.hpp>
#include <smks/scn/Texture.hpp>
#include <smks/parm/BuiltIns.hpp>
#include <smks/xchg/ObjectPtr.hpp>
#include <smks/xchg/Image.hpp>
#include <smks/xchg/TileImages.hpp>
#include "smks/scn/test/util.hpp"

TEST(TextureTest, Creator)
{
    using namespace smks;

    const std::string   refCode = "nearest";
    scn::Scene::Ptr     scene   = scn::Scene::create();
    //---
    scn::Texture::Ptr   tex     = scn::Texture::create(refCode.c_str(), "my_texture", scene);
    //---
    EXPECT_TRUE(scene->hasDescendant(tex->id()));

    EXPECT_TRUE(tex->parameterExists(parm::code().id()));
    EXPECT_TRUE(tex->parameterExistsAs<std::string>(parm::code().id()));

    std::string code;
    tex->getParameter<std::string>(parm::code(), code);
    EXPECT_STREQ(code.c_str(), refCode.c_str());
}

TEST(TextureTest, FilePathSetter)
{
    using namespace smks;

    scn::Scene::Ptr     scene   = scn::Scene::create();
    scn::Texture::Ptr   tex     = scn::Texture::create("my_tex_code", "my_texture", scene);

    const std::string   refFilePath = "path/to/something.exr";
    //---
    tex->setParameter<std::string>(parm::filePath(), refFilePath);
    //---

    EXPECT_TRUE(tex->parameterExists(parm::filePath().id()));
    EXPECT_TRUE(tex->parameterExistsAs<std::string>(parm::filePath().id()));

    std::string filePath;
    tex->getParameter<std::string>(parm::filePath(), filePath);
    EXPECT_STREQ(filePath.c_str(), refFilePath.c_str());
}

TEST(TextureTest, ImageVsTiles)
{
    using namespace smks;

    scn::Scene::Ptr         scene       = scn::Scene::create();
    scn::Texture::Ptr       tex         = scn::Texture::create("my_tex_code", "my_texture", scene);
    xchg::ObjectPtr         obj;
    xchg::Image::Ptr        image       = nullptr;
    xchg::TileImages::Ptr   tileImages  = nullptr;

    //---
    tex->setParameter<std::string>(parm::filePath(), "path/to/single_image.exr");
    //---
    EXPECT_TRUE (tex->parameterExists                   (parm::image()      .id()));
    EXPECT_FALSE(tex->parameterExists                   (parm::tileImages() .id()));
    EXPECT_TRUE (tex->parameterExistsAs<xchg::ObjectPtr>(parm::image()      .id()));
    EXPECT_FALSE(tex->parameterExistsAs<xchg::ObjectPtr>(parm::tileImages() .id()));

    tex->getParameter<xchg::ObjectPtr>(parm::image(), obj);
    EXPECT_FALSE(obj.empty());
    image = obj.shared_ptr<xchg::Image>();
    EXPECT_TRUE(static_cast<bool>(image));

    tex->getParameter<xchg::ObjectPtr>(parm::tileImages(), obj);
    EXPECT_TRUE(obj.empty());
    tileImages = obj.shared_ptr<xchg::TileImages>();
    EXPECT_FALSE(static_cast<bool>(tileImages));

    //---
    tex->setParameter<std::string>(parm::filePath(), "path/to/images_tiles_<UDIM>.exr");
    //---
    EXPECT_TRUE (tex->parameterExists                   (parm::tileImages() .id()));
    EXPECT_FALSE(tex->parameterExists                   (parm::image()      .id()));
    EXPECT_TRUE (tex->parameterExistsAs<xchg::ObjectPtr>(parm::tileImages() .id()));
    EXPECT_FALSE(tex->parameterExistsAs<xchg::ObjectPtr>(parm::image()      .id()));

    tex->getParameter<xchg::ObjectPtr>(parm::image(), obj);
    EXPECT_TRUE(obj.empty());
    image = obj.shared_ptr<xchg::Image>();
    EXPECT_FALSE(static_cast<bool>(image));

    tex->getParameter<xchg::ObjectPtr>(parm::tileImages(), obj);
    EXPECT_FALSE(obj.empty());
    tileImages = obj.shared_ptr<xchg::TileImages>();
    EXPECT_TRUE(static_cast<bool>(tileImages));
}
