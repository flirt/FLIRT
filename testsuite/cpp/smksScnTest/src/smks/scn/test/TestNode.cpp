// ========================================================================= //
// Copyright 2016-2018 SUPAMONKS_STUDIO                                      //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
//                                                                           //
// ========================================================================= //
#include <smks/util/string/getId.hpp>
#include <smks/parm/BuiltIn.hpp>
#include <smks/parm/BuiltIns.hpp>
#include "smks/scn/test/TestNode.hpp"
#include "smks/scn/test/DefaultOverride.hpp"
#include "smks/scn/test/ParmPreprocessor.hpp"

using namespace smks;

// static
scn::test::TestNode::Ptr
scn::test::TestNode::create(const char*             name,
                            TreeNode::WPtr const&   parent,
                            EventHistory*           history)
{
    Ptr ptr(new TestNode(name, parent, history));
    ptr->build(ptr);
    return ptr;
}

// static
scn::test::TestNode::Ptr
scn::test::TestNode::create(const char*             name,
                            TreeNode::WPtr const&   parent,
                            const xchg::IdSet&      relevant,
                            EventHistory*           history)
{
    Ptr ptr(new TestNode(name, parent, history));
    ptr->build(ptr, relevant);
    return ptr;
}

// static
scn::test::TestNode::Ptr
scn::test::TestNode::create(const char*             name,
                            TreeNode::WPtr const&   parent,
                            unsigned int            relevant,
                            EventHistory*           history)
{
    xchg::IdSet relevantIds;

    Ptr ptr(new TestNode(name, parent, history));
    relevantIds.insert(relevant);
    ptr->build(ptr, relevantIds);
    return ptr;
}

// static
scn::test::TestNode::Ptr
scn::test::TestNode::create(const char*             name,
                            TreeNode::WPtr const&   parent,
                            const std::string&      relevant,
                            EventHistory*           history)
{
    xchg::IdSet relevantIds;

    Ptr ptr(new TestNode(name, parent, history));
    relevantIds.insert(util::string::getId(relevant.c_str()));
    ptr->build(ptr, relevantIds);
    return ptr;
}

// static
scn::test::TestNode::Ptr
scn::test::TestNode::create(const char*                     name,
                            TreeNode::WPtr const&           parent,
                            const std::vector<std::string>& relevant,
                            EventHistory*                   history)
{
    xchg::IdSet relevantIds;

    Ptr ptr(new TestNode(name, parent, history));
    for (size_t i = 0; i < relevant.size(); ++i)
        relevantIds.insert(util::string::getId(relevant[i].c_str()));
    ptr->build(ptr, relevantIds);
    return ptr;
}


scn::test::TestNode::TestNode(const char*           name,
                              TreeNode::WPtr const& parent,
                              EventHistory*         history):
    TreeNode            (name, parent),
    _history            (history ? history  : new EventHistory()),
    _ownsHistory        (history ? false    : true),
    //-------------------
    _relevantBuiltins   (),
    _readonlyParms      (),
    _redirectToRoot     (),
    _defaultOverrides   (),
    _preprocessors      ()
{
    assert(_history);
}

scn::test::TestNode::~TestNode()
{
    if (_ownsHistory)
        delete _history;
}

void
scn::test::TestNode::flushHistory(EventHistory* history)
{
    if (history)
        *history = *_history;
    _history->clear();
}

//virtual
void
scn::test::TestNode::build(Ptr const&           ptr,
                           const xchg::IdSet&   relevantIds)
{
    TreeNode::build(ptr);
    //---
    _relevantBuiltins.clear();
    for (xchg::IdSet::const_iterator id = relevantIds.begin(); id != relevantIds.end(); ++id)
    {
        if (*id == 0) continue;
        if (TreeNode::isParameterRelevant(*id))
        {
            std::stringstream sstr;
            sstr
                << "Parameter ID = " << *id << " is already relevant wrt base class of node '" << name() << "'. "
                << "May cause conflicts when testing.";
            throw sys::Exception(sstr.str().c_str(), "Test Node");
        }
        if (!parm::builtIns().find(*id))
        {
            std::cerr << "Parameter with ID = " << *id << " is NOT recognized as a built-in parameter." << std::endl;
            continue;
        }
        _relevantBuiltins.insert(*id);
    }
}

// virtual
bool
scn::test::TestNode::isParameterRelevant(unsigned int parmId) const
{
    return isParameterRelevantForClass(parmId) || TreeNode::isParameterRelevant(parmId);
}

// virtual
bool
scn::test::TestNode::isParameterWritable(unsigned int parmId) const
{
    if (_readonlyParms.find(parmId) != _readonlyParms.end())
        return false;
    if (isParameterRelevantForClass(parmId))
        return true;
    return TreeNode::isParameterWritable(parmId);
}

// virtual
bool
scn::test::TestNode::preprocessParameterEdit(const char* parmName, unsigned int parmId, parm::ParmFlags f, const parm::Any& any)
{
    bool ret = false;
    for (size_t i = 0; i < _preprocessors.size(); ++i)
        ret = (*_preprocessors[i])(parmName, parmId, f, any) || ret;
    //---
    return TreeNode::preprocessParameterEdit(parmName, parmId, f, any) || ret;
}

// virtual
bool
scn::test::TestNode::overrideParameterDefaultValue(unsigned int parmId, parm::Any& any) const
{
    DefaultOverrideMap::const_iterator const& it = _defaultOverrides.find(parmId);
    if (it != _defaultOverrides.end())
        return (*it->second)(parmId, any);
    return TreeNode::overrideParameterDefaultValue(parmId, any);
}

bool
scn::test::TestNode::isParameterRelevantForClass(unsigned int parmId) const
{
    return _relevantBuiltins.find(parmId) != _relevantBuiltins.end();
}

void
scn::test::TestNode::makeReadonly(unsigned int parmId)
{
    throwErrorIfBuiltInNotRelevant(parmId, "Make Built-in Parameter Read-Only");
    _readonlyParms.insert(parmId);
}

void
scn::test::TestNode::makeReadonly(const std::string& parmName)
{
    makeReadonly(util::string::getId(parmName.c_str()));
}

void
scn::test::TestNode::makeReadonly(const std::vector<std::string>& parmNames)
{
    for (size_t i = 0; i < parmNames.size(); ++i)
        makeReadonly(parmNames[i]);
}

void
scn::test::TestNode::makeRedirectToRoot(unsigned int parmId)
{
    throwErrorIfBuiltInNotRelevant(parmId, "Make Built-in Parameter Redirected to Root");
    _redirectToRoot.insert(parmId);
}
void
scn::test::TestNode::makeRedirectToRoot(const std::string& parmName)
{
    makeRedirectToRoot(util::string::getId(parmName.c_str()));
}
void
scn::test::TestNode::makeRedirectToRoot(const std::vector<std::string>& parmNames)
{
    for (size_t i = 0; i < parmNames.size(); ++i)
        makeRedirectToRoot(parmNames[i]);
}

void
scn::test::TestNode::setCustomGetter(unsigned int                           parmId,
                                     scn::AbstractParmGetter::Ptr const&    func)
{
    throwErrorIfBuiltInNotRelevant(parmId, "Register Built-in Parameter Custom Getter");
    TreeNode::setCustomGetter(parmId, func);    // simply change function's visibility
}

void
scn::test::TestNode::setDefaultOverride(unsigned int                parmId,
                                        DefaultOverride::Ptr const& func)
{
    throwErrorIfBuiltInNotRelevant(parmId, "Register Built-in Parameter Default Override");
    if (func)
        _defaultOverrides[parmId] = func;
    else
        _defaultOverrides.erase(parmId);
}

void
scn::test::TestNode::addPreprocessor(PreprocessorPtr const& func)
{
    if (func)
        _preprocessors.push_back(func);
}

void
scn::test::TestNode::throwErrorIfBuiltInNotRelevant(unsigned int    parmId,
                                                    const char*     msg) const
{
    parm::BuiltIn::Ptr const& parm = parm::builtIns().find(parmId);
    if (!parm || isParameterRelevantForClass(parm->id()))
        return;

    std::stringstream sstr;
    sstr
        << "Built-in parameter '" << parm->c_str() << "' "
        << "must be made relevant to test node '" << name() << "'.";
    throw sys::Exception(sstr.str().c_str(), msg);
}

void
scn::test::TestNode::handleSceneEvent(const xchg::SceneEvent& evt)
{
    TreeNode::handleSceneEvent(evt);
    //---
    _history->push_back(EventEntry::create(evt));
}

void
scn::test::TestNode::handleParmEvent(const xchg::ParmEvent& evt)
{
    TreeNode::handleParmEvent(evt);
    //---
    _history->push_back(EventEntry::create(evt));
}

bool
scn::test::TestNode::mustSendToScene(const xchg::ParmEvent& evt) const
{
    if (_redirectToRoot.find(evt.parm()) != _redirectToRoot.end())
        return true;
    return TreeNode::mustSendToScene(evt);
}
