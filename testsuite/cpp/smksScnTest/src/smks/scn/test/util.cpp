// ========================================================================= //
// Copyright 2016-2018 SUPAMONKS_STUDIO                                      //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
//                                                                           //
// ========================================================================= //
#include <sstream>
#include <smks/parm/BuiltIn.hpp>
#include <smks/parm/BuiltIns.hpp>
#include <smks/scn/Xform.hpp>
#include "smks/scn/test/util.hpp"

using namespace smks;

std::string
parm::test::getParameterName(unsigned int parmId)
{
    parm::BuiltIn::Ptr const& builtIn = parm::builtIns().find(parmId);
    return builtIn
        ? builtIn->c_str()
        : "?";
}

void
scn::test::createXformBranchBelowNode(
    TreeNode::Ptr const&        root,
    size_t                      numXfms,
    std::vector<TreeNode::Ptr>& xfms)
{
    xfms.clear();
    if (numXfms == 0)
        return;
    xfms.resize(numXfms);
    for (size_t n = 0; n < numXfms; ++n)
    {
        std::stringstream sstr;
        sstr << "xfm_" << n;

        xfms[n] = Xform::create(
            sstr.str().c_str(),
            n > 0 ? xfms[n-1] : root);
        xfms[n]->setParameter<math::Affine3f>(
            parm::transform(),
            math::test::getRandomTransform());
    }
}
