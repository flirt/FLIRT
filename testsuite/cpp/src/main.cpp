// ========================================================================= //
// Copyright 2016-2018 SUPAMONKS_STUDIO                                      //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
//                                                                           //
// ========================================================================= //
#include <smks/sys/Log.hpp>

#include "g_resources.hpp"

#include <smks/sys/initialize_asset.hpp>
#include <smks/sys/initialize_scn.hpp>

#include <smks/sys/ResourcePathFinder.hpp>
#include <smks/sys/ResourceDatabase.hpp>

#include "smks/smksScnTest_all.hpp"
#include "smks/smksMathTest_all.hpp"
#include "smks/smksParmTest_all.hpp"
#include "smks/smksSysTest_all.hpp"
#include "smks/smksXchgTest_all.hpp"

#if defined(FLIRT_LOG_ENABLED)
INITIALIZE_NULL_EASYLOGGINGPP;
#endif // defined(FLIRT_LOG_ENABLED)

using namespace smks;

sys::ResourceDatabase::Ptr g_resources = nullptr;

int
main(int argc, char** argv)
{
    ::testing::InitGoogleTest(&argc, argv);

    sys::initializeSmksAsset();
    sys::initializeSmksScn(1);

#if defined(FLIRT_LOG_ENABLED)
    el::Helpers::setStorage(smks::sys::sharedLoggingRepository());
    el::Loggers::reconfigureAllLoggers(el::Level::Global, el::ConfigurationType::ToStandardOutput, "false");
    el::Loggers::reconfigureAllLoggers(el::Level::Global, el::ConfigurationType::Format,           "%datetime [%fbase:%line]: %msg");
    el::Loggers::reconfigureAllLoggers(el::Level::Global, el::ConfigurationType::ToFile,           "false");
#endif // defined(FLIRT_LOG_ENABLED)

    g_resources = sys::ResourceDatabase::create("g_resources");
    sys::ResourcePathFinder::Ptr const& pathFinder = sys::ResourcePathFinder::create(argv[0]);
    g_resources->setSharedPathFinder(pathFinder);

    int ret = RUN_ALL_TESTS();

    sys::finalizeSmksScn();
    sys::finalizeSmksAsset();

    return ret;
}
