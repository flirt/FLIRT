// ========================================================================= //
// Copyright 2016-2018 SUPAMONKS_STUDIO                                      //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
//                                                                           //
// ========================================================================= //
#pragma once

#include <algorithm>
#include <vector>

#include <gtest/gtest.h>

#include <smks/xchg/Data.hpp>

namespace smks { namespace xchg { namespace test
{
    void
    testStringData(size_t N, bool copy)
    {
        std::vector<std::string> values(N);

        for (size_t i = 0; i < N; ++i)
            values[i] = std::to_string(rand());

        Data::Ptr                   data    = Data::create<std::string>(&values.front(), values.size(), copy);
        const std::string *const    dataPtr = data->getPtr<std::string>();

        EXPECT_EQ(data->size(), values.size());
        for (size_t i = 0; i < data->size(); ++i)
        {
            const std::string& v1 = data->get<std::string>(i);
            const std::string& v2 = dataPtr[i];

            EXPECT_EQ(v1, values[i]);
            EXPECT_EQ(v2, values[i]);
        }
    }
}
}
}

//TEST(DataTest, WithoutDataCopy)
//{
//  smks::xchg::test::testStringData(15, false);
//}

TEST(DataTest, WithDataCopy)
{
    smks::xchg::test::testStringData(10, true);
}

//TEST(DataTest, Equality)
//{
//  using namespace smks::xchg;
//
//  std::vector<float> values(15);
//
//  for (size_t i = 0; i < values.size(); ++i)
//      values[i] = static_cast<float>(rand()) / static_cast<float>(RAND_MAX);
//
//  Data::Ptr const& data1 = Data::create<float>(&values.front(), values.size(), false);
//  Data::Ptr const& data2 = Data::create<float>(&values.front(), values.size(), false);
//  Data::Ptr const& data3 = Data::create<float>(&values.front(), values.size(), true);
//  Data::Ptr const& data4 = Data::create<float>(&values.front(), values.size() - 1, false);
//
//  EXPECT_TRUE(*data1 == *data2);
//  EXPECT_TRUE(*data1 == *data3);
//  EXPECT_FALSE(*data1 == *data4);
//}
