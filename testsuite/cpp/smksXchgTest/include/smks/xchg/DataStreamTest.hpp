// ========================================================================= //
// Copyright 2016-2018 SUPAMONKS_STUDIO                                      //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
//                                                                           //
// ========================================================================= //
#pragma once

#include <vector>
#include <gtest/gtest.h>

#include <smks/xchg/DataStream.hpp>
#include <smks/xchg/Data.hpp>
#include <smks/parm/Container.hpp>

TEST(DataStreamTest, AccessTest)
{
    using namespace smks::xchg;

    const size_t numElements    = 5 + (rand() % 5);
    const size_t numStreams     = 2 + (rand() % 2);

    size_t* offsets = new size_t[numStreams+1];

    offsets[0] = 0;
    for (size_t i = 0; i < numStreams; ++i)
        offsets[i+1] = offsets[i] + (1 + (rand() % 4));

    const size_t        stride  = offsets[numStreams];
    std::vector<float>  values  (stride * numElements);

    const float kA = 3.0f;
    const float kB = 7.0f;
    const float kC = 11.0f;

    for (size_t i = 0; i < numStreams; ++i)
    {
        const size_t dim_i = offsets[i+1] - offsets[i];
        for (size_t j = 0; j < numElements; ++j)
            for (size_t d = 0; d < dim_i; ++d)
                values[offsets[i] + stride * j + d] = kA * i + kB * j + kC;// << std::endl;
    }

    Data::Ptr   data    = Data::create<float>(&values.front(), values.size(), true);
    DataStream* streams = new DataStream[numStreams];

    for (size_t i = 0; i < numStreams; ++i)
        streams[i].initialize(data, numElements, stride, offsets[i]);

    values.clear();
    values.shrink_to_fit();

    for (size_t j = 0; j < numElements; ++j)
        for (size_t i = 0; i < numStreams; ++i)
        {
            const size_t    dim_i   = offsets[i+1] - offsets[i];
            const float*    ptr     = streams[i].getPtr<float>(j);
            for (size_t d = 0; d < dim_i; ++d)
            {
                EXPECT_FLOAT_EQ(ptr[d], kA * i + kB * j + kC);
            }
        }

    delete[] streams;
    delete[] offsets;
}

TEST(DataStreamTest, Equality)
{
    using namespace smks::xchg;

    const size_t numElements    = 4;
    const size_t numStreams     = 2;
    const size_t offsets[]      = { 0, 3, 5 };
    const size_t stride         = offsets[numStreams];
    std::vector<int> values     (stride * numElements);

    for (size_t i = 0; i < values.size(); ++i)
        values[i] = static_cast<int>(rand() % 10000);

    Data::Ptr   data1   = Data::create<int>(&values.front(), values.size(), false);
    Data::Ptr   data2   = Data::create<int>(&values.front(), values.size(), false);
    Data::Ptr   data3   = Data::create<int>(&values.front(), values.size(), true);

    DataStream  str1;
    DataStream  str2;

    str1.initialize(data1, numElements, stride, offsets[0]);

    str2.initialize(data1, numElements, stride, offsets[0]);
    EXPECT_EQ(str1, str2);

    str2.initialize(data2, numElements, stride, offsets[0]);
    EXPECT_EQ(str1, str2);

    str2.initialize(data3, numElements, stride, offsets[0]);
    EXPECT_EQ(str1, str2);
}

TEST(DataStreamTest, StorageAsParm)
{
    using namespace smks;

    std::vector<float> data(5);
    for (size_t i = 0; i < data.size(); ++i)
        data[i] = static_cast<float>(i);

    const std::string       parmName    = "my_parm";
    parm::Container::Ptr    parms       = parm::Container::create();

    xchg::Data::Ptr         data1       = xchg::Data::create<float>(&data.front(), data.size(), false);
    xchg::DataStream        datastream  (data1, data1->size(), 1, 0);
    xchg::Data::Ptr         data2       = xchg::Data::create<float>(&data.front(), data.size(), false);

    parms->set<xchg::Data::Ptr>(parmName.c_str(), data1);
    data1.reset();
    parms->set<xchg::Data::Ptr>(parmName.c_str(), data2);
    data2.reset();

    if (parms->existsAs<xchg::Data::Ptr>(parmName.c_str()))
    {
        xchg::Data::Ptr const& storedData = parms->get<xchg::Data::Ptr>(parmName.c_str());
        EXPECT_EQ(storedData->getPtr<float>(), &data.front());
    }
    EXPECT_TRUE(datastream);
    EXPECT_EQ(datastream.getPtr<float>(0), &data.front());
}
