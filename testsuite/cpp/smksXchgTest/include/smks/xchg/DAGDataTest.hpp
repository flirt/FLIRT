// ========================================================================= //
// Copyright 2016-2018 SUPAMONKS_STUDIO                                      //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
//                                                                           //
// ========================================================================= //
#pragma once

#include <gtest/gtest.h>

#include <smks/sys/Exception.hpp>
#include "smks/xchg/test/util.hpp"

TEST(DAGDataTest, SingleNode)
{
    using namespace smks::xchg;

    test::DAGLayoutPublic::Ptr L = test::DAGLayoutPublic::create();
    unsigned int id = test::genId();
    //---
    L->add(id);
    //---
    EXPECT_EQ(L->size(),                1);
    EXPECT_EQ(L->id (L->index(id)),     id);
    EXPECT_EQ(L->parent(id),            0);
    EXPECT_EQ(L->firstChild (id),       0);
    EXPECT_EQ(L->previousSibling(id),   0);
    EXPECT_EQ(L->nextSibling(id),       0);
}

TEST(DAGDataTest, ChildAddition)
{
    using namespace smks::xchg;

    test::DAGLayoutPublic::Ptr L = test::DAGLayoutPublic::create();
    unsigned int    id[] = { test::genId(), test::genId() };
    L->add(id[0]);
    //---
    L->add(id[1], id[0]);
    //---
    EXPECT_EQ(L->size(),                2);
    EXPECT_EQ(L->id (L->index(id[1])),  id[1]);
    EXPECT_EQ(L->parent(id[1]),         id[0]);
    EXPECT_EQ(L->firstChild(id[0]),     id[1]);
}

TEST(DAGDataTest, ErrChildAddition)
{
    using namespace smks::xchg;

    bool            caught = false;
    test::DAGLayoutPublic::Ptr L = test::DAGLayoutPublic::create();
    unsigned int    id[] = { test::genId(), test::genId() };

    //---
    try { L->add(id[1], id[0]); }
    catch(const smks::sys::Exception& e) { std::cerr << e.what() << std::endl; caught = true; }
    //---
    EXPECT_TRUE(caught);
    EXPECT_EQ(L->size(), 0);
}

TEST(DAGDataTest, SiblingAddition)
{
    using namespace smks::xchg;

    test::DAGLayoutPublic::Ptr L = test::DAGLayoutPublic::create();
    unsigned int    parentId = test::genId();
    unsigned int    childId[] = { test::genId(), test::genId() };

    L->add(parentId);
    //---
    L->add(childId[0], parentId);
    L->add(childId[1], parentId);
    //---
    EXPECT_EQ(L->size(),                3);
    EXPECT_EQ(L->parent(childId[0]),    parentId);
    EXPECT_EQ(L->parent(childId[1]),    parentId);
    EXPECT_EQ(L->firstChild(parentId),  childId[0]);

    EXPECT_EQ(L->nextSibling(childId[0]),   childId[1]);
    EXPECT_EQ(L->nextSibling(childId[1]),   0);

    EXPECT_EQ(L->previousSibling(childId[0]),   0);
    EXPECT_EQ(L->previousSibling(childId[1]),   childId[0]);
}

TEST(DAGDataTest, FirstChildDeletion)
{
    using namespace smks::xchg;

    test::DAGLayoutPublic::Ptr L = test::DAGLayoutPublic::create();
    unsigned int    parentId    = test::genId();
    unsigned int    childId[]   = { test::genId(), test::genId(), test::genId() };

    L->add(parentId);
    L->add(childId[0], parentId);
    L->add(childId[1], parentId);
    L->add(childId[2], parentId);
    EXPECT_EQ(L->firstChild(parentId),  childId[0]);
    //---
    L->remove(childId[0]);
    //---
    EXPECT_EQ(L->firstChild (parentId),     childId[1]);
    EXPECT_EQ(L->nextSibling(childId[1]),   childId[2]);
    EXPECT_EQ(L->nextSibling(childId[2]),   0);

    EXPECT_EQ(L->previousSibling(childId[1]),   0);
    EXPECT_EQ(L->previousSibling(childId[2]),   childId[1]);
}

TEST(DAGDataTest, SiblingDeletion)
{
    using namespace smks::xchg;

    test::DAGLayoutPublic::Ptr L = test::DAGLayoutPublic::create();
    unsigned int    parentId    = test::genId();
    unsigned int    childId[]   = { test::genId(), test::genId(), test::genId() };

    L->add(parentId);
    L->add(childId[0], parentId);
    L->add(childId[1], parentId);
    L->add(childId[2], parentId);
    //---
    L->remove(childId[1]);
    //---
    EXPECT_EQ(L->size(),                    3);
    EXPECT_EQ(L->firstChild (parentId),     childId[0]);
    EXPECT_EQ(L->nextSibling(childId[0]),   childId[2]);
    EXPECT_EQ(L->nextSibling(childId[2]),   0);

    EXPECT_EQ(L->previousSibling(childId[0]),   0);
    EXPECT_EQ(L->previousSibling(childId[2]),   childId[0]);
}

TEST(DAGDataTest, BranchDeletion)
{
    using namespace smks::xchg;

    test::DAGLayoutPublic::Ptr L = test::DAGLayoutPublic::create();
    unsigned int    parentId        = test::genId();
    unsigned int    childId[]       = { test::genId(), test::genId() };
    unsigned int    grdChildId[]    = { test::genId(), test::genId() };

    L->add(parentId);
    L->add(childId[0],      parentId);
    L->add(grdChildId[0],   childId[0]);
    L->add(grdChildId[1],   childId[0]);
    L->add(childId[1],      parentId);
    EXPECT_EQ(L->firstChild(parentId),  childId[0]);
    //---
    L->remove(childId[0]);
    //---
    EXPECT_EQ(L->firstChild(parentId),          childId[1]);
    EXPECT_EQ(L->id (L->index(childId[0])),     0);
    EXPECT_EQ(L->id (L->index(grdChildId[0])),  0);
    EXPECT_EQ(L->id (L->index(grdChildId[1])),  0);
    EXPECT_EQ(L->id (L->index(parentId)),       parentId);
    EXPECT_EQ(L->id (L->index(childId[1])),     childId[1]);
}

TEST(DAGDataTest, SwapEntries)
{
    using namespace smks::xchg;

    test::DAGLayoutPublic::Ptr L = test::DAGLayoutPublic::create();
    unsigned int    rootId          = test::genId();
    unsigned int    parentId        = test::genId();
    unsigned int    childId[]       = { test::genId(), test::genId() };
    unsigned int    grdChildId[]    = { test::genId(), test::genId(), test::genId() };

    L->add(rootId);
    L->add(parentId,        rootId);
    L->add(childId[0],      parentId);
    L->add(grdChildId[0],   childId[0]);
    L->add(grdChildId[1],   childId[0]);
    L->add(childId[1],      parentId);
    L->add(grdChildId[2],   childId[1]);

    EXPECT_EQ(L->firstChild     (parentId),         childId[0]);
    EXPECT_EQ(L->nextSibling    (childId[0]),       childId[1]);
    EXPECT_EQ(L->previousSibling(childId[1]),       childId[0]);
    EXPECT_EQ(L->parent         (grdChildId[0]),    childId[0]);
    EXPECT_EQ(L->parent         (grdChildId[2]),    childId[1]);

    //---
    const size_t a = L->index(childId[0]);
    const size_t b = L->index(childId[1]);
    L->swapIndices(a,b);
    //---
    EXPECT_EQ(L->index(childId[0]), b);
    EXPECT_EQ(L->index(childId[1]), a);

    // DAG's structure must be left untouched
    EXPECT_EQ(L->firstChild     (parentId),         childId[0]);
    EXPECT_EQ(L->nextSibling    (childId[0]),       childId[1]);
    EXPECT_EQ(L->previousSibling(childId[1]),       childId[0]);
    EXPECT_EQ(L->parent         (grdChildId[0]),    childId[0]);
    EXPECT_EQ(L->parent         (grdChildId[2]),    childId[1]);
}

TEST(DAGDataTest, SingleEntryDirtyTest)
{
    using namespace smks::xchg;

    test::DAGLayoutPublic::Ptr L    = test::DAGLayoutPublic::create();
    unsigned int    rootId          = test::genId();

    L->add(rootId);
    EXPECT_TRUE(L->isDirty(rootId));

    L->clean();
    EXPECT_FALSE(L->isDirty(rootId));

    L->dirty(rootId);
    EXPECT_TRUE(L->isDirty(rootId));
}

TEST(DAGDataTest, DirtySwapTest)
{
    using namespace smks::xchg;

    test::DAGLayoutPublic::Ptr L    = test::DAGLayoutPublic::create();
    unsigned int    rootId          = test::genId();
    unsigned int    parentId[]      = { test::genId(), test::genId() };
    unsigned int    childId[]       = { test::genId(), test::genId() };
    unsigned int    grandChildId    = test::genId();

    L->add(rootId);
    L->add(parentId[0],     rootId);
    L->add(childId[0],      parentId[0]);
    L->add(grandChildId,    childId[0]);
    L->add(childId[1],      parentId[0]);
    L->add(parentId[1],     rootId);

    EXPECT_TRUE(L->isDirty(rootId));
    EXPECT_TRUE(L->isDirty(grandChildId));
    for (size_t i = 0; i < 2; ++i)
    {
        EXPECT_TRUE(L->isDirty(parentId[i]));
        EXPECT_TRUE(L->isDirty(childId[i]));
    }

    L->clean();

    EXPECT_FALSE(L->isDirty(rootId));
    EXPECT_FALSE(L->isDirty(grandChildId));
    for (size_t i = 0; i < 2; ++i)
    {
        EXPECT_FALSE(L->isDirty(parentId[i]));
        EXPECT_FALSE(L->isDirty(childId[i]));
    }

    //---
    L->dirty(parentId[0]);
    //---

    EXPECT_FALSE(L->isDirty(rootId));
    EXPECT_FALSE(L->isDirty(parentId[1]));

    EXPECT_TRUE(L->isDirty(parentId[0]));
    for (size_t i = 0; i < 2; ++i)
        EXPECT_TRUE(L->isDirty(childId[i]));
    EXPECT_TRUE(L->isDirty(grandChildId));
}

TEST(DAGDataTest, AttachedDataCapacityUpdate)
{
    using namespace smks::xchg;

    test::HeldData::Ptr data(new test::HeldData);
    test::DAGLayoutPublic::Ptr L = test::DAGLayoutPublic::create(data);

    unsigned int id[] = { test::genId(), test::genId(), test::genId(), test::genId() };

    //--
    L->add(id[0]);
    L->add(id[1], id[0]);
    L->add(id[2], id[1]);
    L->add(id[3], id[2]);
    //--
    EXPECT_EQ(data->currentCapacity, L->capacity());
}

TEST(DAGDataTest, AttachedDataRemoveUpdate)
{
    using namespace smks::xchg;

    test::HeldData::Ptr data(new test::HeldData);
    test::DAGLayoutPublic::Ptr L = test::DAGLayoutPublic::create(data);

    unsigned int id[] = { test::genId(), test::genId(), test::genId(), test::genId(), test::genId(), test::genId() };
    const size_t N = sizeof(id) / sizeof(unsigned int);

    L->add(id[0]);
    for (size_t n = 1; n < N-1; ++n)
        L->add(id[n], id[n-1]);
    L->add(id[N-1], id[0]); // necessary to force swapping when removing

    //--
    L->remove(id[1]);
    //--
    EXPECT_EQ(data->numMoves, 3 * (N-2));   // 3 moves per swap ([0] and [N-1] should not swap)
}

TEST(DAGDataTest, AttachedDataCleanUpdate)
{
    using namespace smks::xchg;

    test::HeldData::Ptr data(new test::HeldData);
    test::DAGLayoutPublic::Ptr L = test::DAGLayoutPublic::create(data);
    unsigned int    rootId          = test::genId();
    unsigned int    parentId[]      = { test::genId(), test::genId() };
    unsigned int    childId[]       = { test::genId(), test::genId() };
    unsigned int    grandChildId    = test::genId();

    L->add(rootId);
    L->add(parentId[0],     rootId);
    L->add(childId[0],      parentId[0]);
    L->add(grandChildId,    childId[0]);
    L->add(childId[1],      parentId[0]);
    L->add(parentId[1],     rootId);

    L->clean();
    EXPECT_EQ(data->numUpdatesOK, 6);   // all nodes should be updated
    EXPECT_EQ(data->numUpdatesErr, 0);
    //---
    data->numUpdatesOK  = 0;
    data->numUpdatesErr = 0;
    L->dirty(parentId[0]);
    L->clean();
    //---
    EXPECT_EQ(data->numUpdatesOK, 4);   // only root and parentId[1] should not be updated
    EXPECT_EQ(data->numUpdatesErr, 0);
}

TEST(DAGDataTest, DisposalTest)
{
    using namespace smks::xchg;

    test::DAGLayoutPublic::Ptr L = test::DAGLayoutPublic::create();
    unsigned int    rootId          = test::genId();
    unsigned int    parentId        = test::genId();
    unsigned int    childId[]       = { test::genId(), test::genId() };
    unsigned int    grandChildId[]  = { test::genId(), test::genId() };

    L->add(rootId);
    L->add(parentId,        rootId);
    L->add(childId[0],      parentId);
    L->add(childId[1],      parentId);
    L->add(grandChildId[0], childId[1]);
    L->add(grandChildId[1], childId[1]);

    L->swapIndices(L->index(rootId), L->index(childId[1]));

    //---
    L->dispose();
    //---
    EXPECT_EQ(L->size(), 0);
    EXPECT_EQ(L->capacity(), 0);
}

TEST(DAGDataTest, AttachedDataDisposalTest)
{
    using namespace smks::xchg;

    test::HeldData::Ptr data(new test::HeldData);
    test::DAGLayoutPublic::Ptr L = test::DAGLayoutPublic::create(data);
    unsigned int    rootId          = test::genId();
    unsigned int    parentId        = test::genId();
    unsigned int    childId[]       = { test::genId(), test::genId() };
    unsigned int    grandChildId[]  = { test::genId(), test::genId() };

    L->add(rootId);
    L->add(parentId,        rootId);
    L->add(childId[0],      parentId);
    L->add(childId[1],      parentId);
    L->add(grandChildId[0], childId[1]);
    L->add(grandChildId[1], childId[1]);

    L->swapIndices(L->index(rootId), L->index(childId[1]));

    //---
    L->dispose();
    //---
    EXPECT_EQ(data->currentCapacity, 0);
}

TEST(DAGDataTest, DisposalDebug)
{
    using namespace smks::xchg;

    test::HeldData::Ptr data(new test::HeldData);
    test::DAGLayoutPublic::Ptr L = test::DAGLayoutPublic::create(data);

    L->add(1000);
    L->add(1001,    1000);
    L->add(1002,    1000);
    L->add(1003,    1002);
    L->add(1004);
    L->add(1005,    1004);
    L->add(1006,    1004);
    L->add(1007,    1006);
    L->add(1008);
    L->add(1009);
    L->add(1010);

    //---
    L->dispose();
    //---
    EXPECT_EQ(data->currentCapacity, 0);
}

TEST(DAGDataTest, DirtinessPropagation)
{
    using namespace smks::xchg;

    test::DAGLayoutPublic::Ptr L = test::DAGLayoutPublic::create();

    const unsigned int root = test::genId();
    const unsigned int a    = test::genId();
    const unsigned int aa   = test::genId();
    const unsigned int aaa  = test::genId();
    const unsigned int b    = test::genId();
    const unsigned int bb   = test::genId();
    const unsigned int bbb  = test::genId();
    const unsigned int c    = test::genId();
    const unsigned int cc   = test::genId();
    const unsigned int ccc  = test::genId();

    const unsigned int ids[] = {
        root,
        a, aa, aaa,
        b, bb, bbb,
        c, cc, ccc
    };
    const int num = sizeof(ids) / sizeof(const unsigned int);
    //---
    L->add(root);
    L->add(a, root);
    L->add(aa, a);
    L->add(b, root);
    L->add(bb, b);
    L->add(c, root);
    L->add(cc, c);
    L->add(aaa, aa);
    L->add(ccc, cc);
    L->add(bbb, bb);

    L->clean();
    //---
    for (int i = 0; i < num; ++i)
        EXPECT_FALSE(L->isDirty(ids[i]));
    //---
    L->dirty(b);
    //---
    for (int i = 0; i < num; ++i)
        if (ids[i] == b || ids[i] == bb || ids[i] == bbb)
            EXPECT_TRUE(L->isDirty(ids[i]));
        else
            EXPECT_FALSE(L->isDirty(ids[i]));
}
