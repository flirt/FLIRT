// ========================================================================= //
// Copyright 2016-2018 SUPAMONKS_STUDIO                                      //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
//                                                                           //
// ========================================================================= //
#pragma once

#include <memory>
#include <gtest/gtest.h>
#include <smks/xchg/Vector.hpp>

namespace smks { namespace xchg { namespace test
{
    /////////////////
    // struct Element
    /////////////////
    struct Element
    {
        int idx;
        xchg::Vector<float> floats;

    public:
        Element(): idx(-1), floats() {}
        explicit Element(int idx): idx(idx), floats() {}
    };

    ///////////////////////
    // class DeallocTracker
    ///////////////////////
    class DeallocTracker
    {
    public:
        typedef std::shared_ptr<DeallocTracker> Ptr;
    private:
        bool& _deallocated;
    public:
        static inline Ptr create(bool& deallocated)
        {
            Ptr ptr(new DeallocTracker(deallocated)); return ptr;
        }
    private:
        explicit DeallocTracker(bool& deallocated): _deallocated(deallocated)
        {
            _deallocated = false;
        }
        // non-copyable
        DeallocTracker();
        DeallocTracker(const DeallocTracker&);
        DeallocTracker& operator=(const DeallocTracker&);
    public:
        ~DeallocTracker()
        {
            _deallocated = true;
        }
    };
}
}
}

TEST(VectorTest, EmptyCreator)
{
    using namespace smks::xchg;
    //---
    Vector<test::Element> vec;
    //---
    EXPECT_TRUE(vec.empty());
    EXPECT_EQ(vec.size(), 0);
    EXPECT_EQ(vec.capacity(), 0);
}

TEST(VectorTest, SizeCreationTest)
{
    using namespace smks::xchg;

    const size_t sz = 5;
    //---
    Vector<test::Element> vec(sz);
    //---
    EXPECT_TRUE(!vec.empty());
    EXPECT_EQ(vec.size(), sz);
    EXPECT_EQ(vec.capacity(), sz);
}

TEST(VectorTest, PushTest)
{
    using namespace smks::xchg;

    Vector<test::Element> vec;
    //---
    vec.push_back(test::Element(101));
    vec.push_back(test::Element(102));
    vec.push_back(test::Element(103));
    //---
    EXPECT_TRUE(!vec.empty());
    EXPECT_EQ(vec[0].idx, 101);
    EXPECT_EQ(vec[1].idx, 102);
    EXPECT_EQ(vec[2].idx, 103);
    EXPECT_EQ(vec.size(), 3);
    EXPECT_EQ(vec.capacity(), 4);
}

TEST(VectorTest, CopyCreationTest)
{
    using namespace smks::xchg;

    Vector<test::Element> vecOrig;

    vecOrig.push_back(test::Element(101));
    vecOrig.push_back(test::Element(102));
    vecOrig.push_back(test::Element(103));

    //---
    Vector<test::Element> vec(vecOrig);
    //---
    EXPECT_TRUE(!vec.empty());
    EXPECT_EQ(vec[0].idx, 101);
    EXPECT_EQ(vec[1].idx, 102);
    EXPECT_EQ(vec[2].idx, 103);
    EXPECT_EQ(vec.size(), 3);
    EXPECT_GE(vec.capacity(), vec.size());
}

TEST(VectorTest, AssignTest)
{
    using namespace smks::xchg;

    Vector<test::Element> vecOrig, vec;

    vecOrig.push_back(test::Element(101));
    vecOrig.push_back(test::Element(102));
    vecOrig.push_back(test::Element(103));

    //---
    vec = vecOrig;
    //---
    EXPECT_TRUE(!vec.empty());
    EXPECT_EQ(vec[0].idx, 101);
    EXPECT_EQ(vec[1].idx, 102);
    EXPECT_EQ(vec[2].idx, 103);
    EXPECT_EQ(vec.size(), 3);
    EXPECT_GE(vec.capacity(), vec.size());
}

TEST(VectorTest, BracketTest)
{
    using namespace smks::xchg;

    size_t sz = 3;
    Vector<test::Element> vec(sz);

    //---
    for (size_t i = 0; i < vec.size(); ++i)
        vec[i] = test::Element(100 + static_cast<int>(i));
    //---

    EXPECT_TRUE(!vec.empty());
    EXPECT_EQ(vec.size(), sz);
    for (size_t i = 0; i < vec.size(); ++i)
        EXPECT_EQ(vec[i].idx, 100 + static_cast<int>(i));
    EXPECT_GE(vec.capacity(), vec.size());
}

TEST(VectorTest, GreaterResizeTest)
{
    using namespace smks::xchg;

    size_t sz = 3;
    size_t new_sz = 5;
    Vector<test::Element> vec(sz);

    for (size_t i = 0; i < sz; ++i)
        vec[i] = test::Element(100 + static_cast<int>(i));
    //---
    vec.resize(new_sz);
    //---
    EXPECT_TRUE(!vec.empty());
    EXPECT_EQ(vec.size(), new_sz);
    for (size_t i = 0; i < sz; ++i)
        EXPECT_EQ(vec[i].idx, 100 + static_cast<int>(i));
    EXPECT_GE(vec.capacity(), vec.size());
}

TEST(VectorTest, LesserResizeTest)
{
    using namespace smks::xchg;

    size_t sz = 5;
    size_t new_sz = 3;
    Vector<test::Element> vec(sz);

    for (size_t i = 0; i < sz; ++i)
        vec[i] = test::Element(100 + static_cast<int>(i));
    //---
    vec.resize(new_sz);
    //---
    EXPECT_TRUE(!vec.empty());
    EXPECT_EQ(vec.size(), new_sz);
    for (size_t i = 0; i < new_sz; ++i)
        EXPECT_EQ(vec[i].idx, 100 + static_cast<int>(i));
    EXPECT_GE(vec.capacity(), vec.size());
}

TEST(VectorTest, ReserveOnEmptyTest)
{
    using namespace smks::xchg;

    size_t cap = 32;
    Vector<test::Element> vec;

    EXPECT_TRUE(vec.empty());
    EXPECT_EQ(vec.capacity(), 0);
    //---
    vec.reserve(cap);
    //---
    EXPECT_EQ(vec.capacity(), cap);
}

TEST(VectorTest, GreaterReserveTest)
{
    using namespace smks::xchg;

    size_t cap = 32;
    size_t sz = 3;
    Vector<test::Element> vec(sz);

    for (size_t i = 0; i < vec.size(); ++i)
        vec[i] = test::Element(100 + static_cast<int>(i));

    //---
    vec.reserve(cap);

    size_t actual_cap = sz;
    while (actual_cap < cap)
        actual_cap *= 2;
    //---
    EXPECT_EQ(vec.size(), sz);
    for (size_t i = 0; i < sz; ++i)
        EXPECT_EQ(vec[i].idx, 100 + static_cast<int>(i));
    EXPECT_EQ(vec.capacity(), actual_cap);
}

TEST(VectorTest, LesserReserveTest)
{
    using namespace smks::xchg;

    size_t cap = 2;
    size_t sz = 5;
    Vector<test::Element> vec(sz);

    for (size_t i = 0; i < vec.size(); ++i)
        vec[i] = test::Element(100 + static_cast<int>(i));

    size_t old_cap = vec.capacity();
    //---
    vec.reserve(cap);
    //---
    EXPECT_EQ(vec.size(), sz);
    for (size_t i = 0; i < sz; ++i)
        EXPECT_EQ(vec[i].idx, 100 + static_cast<int>(i));
    EXPECT_EQ(vec.capacity(), old_cap);
}

TEST(VectorTest, PopBackTest)
{
    using namespace smks::xchg;

    bool deallocated[2];
    Vector<std::shared_ptr<test::DeallocTracker>> vec;

    vec.push_back(test::DeallocTracker::create(deallocated[0]));
    vec.push_back(test::DeallocTracker::create(deallocated[1]));

    EXPECT_FALSE(deallocated[0]);
    EXPECT_FALSE(deallocated[1]);
    EXPECT_EQ(vec.size(), 2);
    //---
    vec.pop_back();
    //---
    EXPECT_EQ(vec.size(), 1);
    EXPECT_TRUE(deallocated[1]);
    EXPECT_FALSE(deallocated[0]);
}

TEST(VectorTest, ClearTest)
{
    using namespace smks::xchg;

    static const size_t N = 5;
    bool deallocated[N];
    Vector<std::shared_ptr<test::DeallocTracker>> vec;

    for (size_t i = 0; i < N; ++i)
        vec.push_back(test::DeallocTracker::create(deallocated[i]));

    for (size_t i = 0; i < N; ++i)
        EXPECT_FALSE(deallocated[i]);
    //---
    vec.clear();
    //---
    EXPECT_TRUE(vec.empty());
    EXPECT_EQ(vec.size(), 0);
    EXPECT_EQ(vec.capacity(), 0);
    for (size_t i = 0; i < N; ++i)
        EXPECT_TRUE(deallocated[i]);
}

TEST(VectorTest, DeallocationTest)
{
    using namespace smks::xchg;

    static const size_t N = 5;
    bool deallocated[N];
    Vector<std::shared_ptr<test::DeallocTracker>> *vec = new Vector<std::shared_ptr<test::DeallocTracker>>;

    for (size_t i = 0; i < N; ++i)
        vec->push_back(test::DeallocTracker::create(deallocated[i]));

    for (size_t i = 0; i < N; ++i)
        EXPECT_FALSE(deallocated[i]);
    //---
    delete vec;
    //---
    for (size_t i = 0; i < N; ++i)
        EXPECT_TRUE(deallocated[i]);
}
