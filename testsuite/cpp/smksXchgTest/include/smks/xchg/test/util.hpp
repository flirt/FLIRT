// ========================================================================= //
// Copyright 2016-2018 SUPAMONKS_STUDIO                                      //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
//                                                                           //
// ========================================================================= //
#pragma once

#include <cmath>

#include <smks/xchg/DAGLayout.hpp>

namespace smks { namespace xchg { namespace test
{
    inline
    unsigned int
    genId()
    {
        return 1 + (rand() % RAND_MAX);
    }

    ////////////////////////
    // class DAGLayoutPublic
    ////////////////////////
    class DAGLayoutPublic:
        public DAGLayout
    {
    public:
        typedef std::shared_ptr<DAGLayoutPublic> Ptr;

    public:
        static inline
        Ptr
        create(AbstractData::Ptr const& data = nullptr)
        {
            Ptr ptr(new DAGLayoutPublic);
            if (data)
                ptr->attach(data);
            return ptr;
        }

    protected:
        DAGLayoutPublic():
            DAGLayout()
        { }

    public:
        // exposes all protected functions and getters
        inline
        size_t
        index(unsigned int node) const
        {
            return DAGLayout::index(node);
        }

        inline
        unsigned int
        id(size_t idx) const
        {
            return DAGLayout::id(idx);
        }

        inline
        size_t
        size() const
        {
            return DAGLayout::size();
        }

        inline
        size_t
        capacity() const
        {
            return DAGLayout::capacity();
        }

        inline
        void
        swapIndices(size_t idx1, size_t idx2)
        {
            DAGLayout::swapIndices(idx1, idx2);
        }
    };
    ////////////////////////

    //////////////////
    // struct HeldData
    //////////////////
    struct HeldData:
        public DAGLayout::AbstractData
    {
    public:
        typedef std::shared_ptr<HeldData> Ptr;
    public:
        size_t currentCapacity, numMoves, numUpdatesOK, numUpdatesErr;

    public:
        HeldData():
            currentCapacity(0),
            numMoves(0),
            numUpdatesOK(0),
            numUpdatesErr(0)
        { }
        virtual
        void
        resize(size_t capacity)
        {
            currentCapacity = capacity;
        }
        virtual
        bool
        inherits(size_t idx) const
        {
            return true;
        }
        virtual
        void
        inherits(size_t idx, bool)
        { }
        virtual
        void
        moveEntry(size_t dst, size_t src)
        {
            ++numMoves;
        }
        virtual
        void
        recomputeEntry(size_t current)
        {
            ++numUpdatesOK;
        }
        virtual
        void
        accumulateEntries(size_t current, size_t parent)
        {
            if (parent < current)
                ++numUpdatesOK;
            else
                ++numUpdatesErr;
        }
    };
    //////////////////

}
} }
