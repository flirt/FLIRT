// ========================================================================= //
// Copyright 2016-2018 SUPAMONKS_STUDIO                                      //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
//                                                                           //
// ========================================================================= //
#pragma once

#include <algorithm>
#include <vector>

#include <gtest/gtest.h>

#include <smks/xchg/IdSet.hpp>

TEST(IdSetTest, EqualityTest)
{
    using namespace smks::xchg;

    const size_t                N = 10;
    std::vector<size_t>         indices (N);
    std::vector<unsigned int>   ids     (N);
    IdSet                       sets    [2];
    size_t                      rdm     = rand() % N;

    for (size_t i = 0; i < 10; ++i)
    {
        indices[i]  = i;
        ids[i]      = static_cast<unsigned int>(rand());
    }

    for (size_t j = 0; j < 2; ++j)
    {
        std::random_shuffle(indices.begin(), indices.end());
        for (size_t i = 0; i < indices.size(); ++i)
            sets[j].insert(ids[i]);
    }

    EXPECT_EQ(sets[0], sets[1]);

    sets[1].erase(ids[rdm]);
    EXPECT_NE(sets[0], sets[1]);

    sets[1].insert(ids[rdm]);
    EXPECT_EQ(sets[0], sets[1]);
}
