// ========================================================================= //
// Copyright 2016-2018 SUPAMONKS_STUDIO                                      //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
//                                                                           //
// ========================================================================= //
#pragma once

#include <gtest/gtest.h>
#include <smks/xchg/DynamicBitset.hpp>

TEST(DynamicBitsetTest, EmptyBitset)
{
    using namespace smks;
    xchg::DynamicBitset bset;
    EXPECT_EQ(bset.size(), 0);
    EXPECT_EQ(bset.capacity(), 0);
}

TEST(DynamicBitsetTest, ReserveBitset)
{
    using namespace smks;
    xchg::DynamicBitset bset;

    bset.resize(63);    // -> 1 word == 64 bits
    EXPECT_EQ(bset.size(), 63);
    EXPECT_EQ(bset.capacity(), 64);

    bset.resize(64);
    EXPECT_EQ(bset.size(), 64);
    EXPECT_EQ(bset.capacity(), 64);

    bset.resize(65);    // -> 2 words == 128 bits
    EXPECT_EQ(bset.size(), 65);
    EXPECT_EQ(bset.capacity(), 128);

    bset.resize(63);    // -> 1 word == 64 bits
    EXPECT_EQ(bset.size(), 63);
    EXPECT_EQ(bset.capacity(), 128);    // will not decrease

    bset.resize(0); // actually clears
    EXPECT_EQ(bset.size(), 0);
    EXPECT_EQ(bset.capacity(), 0);
}

TEST(DynamicBitsetTest, ClearBitset)
{
    using namespace smks;
    xchg::DynamicBitset bset;

    bset.resize(63);    // -> 1 word == 64 bits
    EXPECT_EQ(bset.size(), 63);
    EXPECT_EQ(bset.capacity(), 64);

    bset.clear();
    EXPECT_EQ(bset.size(), 0);
    EXPECT_EQ(bset.capacity(), 0);
}

TEST(DynamicBitsetTest, GetSetBitset)
{
    using namespace smks;
    xchg::DynamicBitset bset;

    size_t size = 48;
    bool* values = new bool[size];
    for (size_t i = 0; i < size; ++i)
        values[i] = rand() % 2 == 0;

    bset.resize(size);
    for (size_t i = 0; i < size; ++i)
        bset.set(i, values[i]);

    for (size_t i = 0; i < size; ++i)
        EXPECT_EQ(bset.get(i), values[i]);

    delete[] values;
}

TEST(DynamicBitsetTest, CopyBitset)
{
    using namespace smks;
    xchg::DynamicBitset bset;

    size_t size = 48;
    bool* values = new bool[size];
    for (size_t i = 0; i < size; ++i)
        values[i] = rand() % 2 == 0;

    bset.resize(size);
    for (size_t i = 0; i < size; ++i)
        bset.set(i, values[i]);
    //---
    xchg::DynamicBitset bset1(bset);
    //---
    for (size_t i = 0; i < size; ++i)
        EXPECT_EQ(bset1.get(i), values[i]);

    //---
    xchg::DynamicBitset bset2 = bset;
    //---
    for (size_t i = 0; i < size; ++i)
        EXPECT_EQ(bset2.get(i), values[i]);

    delete[] values;
}

TEST(DynamicBitsetTest, ValuesKeptUponCapacityChange)
{
    using namespace smks;
    xchg::DynamicBitset bset;

    size_t size = 48;
    bool* values = new bool[size];
    for (size_t i = 0; i < size; ++i)
        values[i] = rand() % 2 == 0;

    bset.resize(size);
    for (size_t i = 0; i < size; ++i)
        bset.set(i, values[i]);
    //---
    bset.resize(size << 1);
    //---
    for (size_t i = 0; i < size; ++i)
        EXPECT_EQ(bset.get(i), values[i]);

    delete[] values;
}

TEST(DynamicBitsetTest, ValuesSetByResize)
{
    using namespace smks;
    xchg::DynamicBitset bset;

    //---
    bset.resize(50, true);  // -> 1 word
    //---
    for (size_t i = 0; i < 50; ++i)
        EXPECT_TRUE(bset.get(i));

    //---
    bset.resize(60, false);
    //---
    for (size_t i = 0; i < 50; ++i)
        EXPECT_TRUE(bset.get(i));
    for (size_t i = 50; i < 60; ++i)
        EXPECT_FALSE(bset.get(i));

    //---
    bset.resize(70, true);  // -> 2 words
    //---
    for (size_t i = 0; i < 50; ++i)
        EXPECT_TRUE(bset.get(i));
    for (size_t i = 50; i < 60; ++i)
        EXPECT_FALSE(bset.get(i));
    for (size_t i = 60; i < 70; ++i)
        EXPECT_TRUE(bset.get(i));
}

TEST(DynamicBitsetTest, ResetBitset)
{
    using namespace smks;
    xchg::DynamicBitset bset;

    size_t size = 48;
    bool* values = new bool[size];
    for (size_t i = 0; i < size; ++i)
        values[i] = rand() % 2 == 0;

    bset.resize(size);

    for (size_t i = 0; i < size; ++i)
        bset.set(i, values[i]);
    for (size_t i = 0; i < size; ++i)
        EXPECT_EQ(bset.get(i), values[i]);

    //---
    bset.reset(true);
    //---
    EXPECT_EQ(bset.size(), size);
    for (size_t i = 0; i < size; ++i)
        EXPECT_TRUE(bset.get(i));

    for (size_t i = 0; i < size; ++i)
        bset.set(i, values[i]);
    for (size_t i = 0; i < size; ++i)
        EXPECT_EQ(bset.get(i), values[i]);

    //---
    bset.reset(false);
    //---
    EXPECT_EQ(bset.size(), size);
    for (size_t i = 0; i < size; ++i)
        EXPECT_FALSE(bset.get(i));

    delete[] values;
}
