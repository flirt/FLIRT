// ========================================================================= //
// Copyright 2016-2018 SUPAMONKS_STUDIO                                      //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
//                                                                           //
// ========================================================================= //
#pragma once

#include <smks/ClientBindingBase.hpp>

namespace smks { namespace view { namespace test
{
    class Binding:
        public ClientBindingBase
    {
    public:
        typedef std::shared_ptr<Binding> Ptr;
    protected:
        typedef std::weak_ptr<ClientBase>       ClientWPtr;
        typedef std::weak_ptr<scn::TreeNode>    DataNodeWPtr;
    public:
        static inline
        Ptr
        create(ClientWPtr const& client, DataNodeWPtr const& dataNode)
        {
            Ptr ptr(new Binding(client, dataNode));
            ptr->build(ptr);
            return ptr;
        }
    protected:
        Binding(ClientWPtr const&, DataNodeWPtr const&);
    private:
        // non-copyable
        Binding(const Binding&);
        Binding& operator=(const Binding&);

    protected:
        void
        handle(const xchg::ParmEvent&);
    };
}
}
}
