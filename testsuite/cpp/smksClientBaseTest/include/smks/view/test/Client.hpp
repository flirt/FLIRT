// ========================================================================= //
// Copyright 2016-2018 SUPAMONKS_STUDIO                                      //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
//                                                                           //
// ========================================================================= //
#pragma once

#include <smks/ClientBase.hpp>

namespace smks
{
    namespace scn
    {
        class TreeNode;
    }
    namespace view
    {
        namespace test
        {
            class ClientObserver;
            class Client:
                public ClientBase
            {
            public:
                typedef std::shared_ptr<Client> Ptr;
                typedef std::weak_ptr<Client>   WPtr;
            private:
                typedef std::weak_ptr<scn::TreeNode>       TreeNodeWPtr;
                typedef std::shared_ptr<ClientBindingBase> ClientBindingBasePtr;
                typedef std::weak_ptr<Client>              ClientWPtr;
                typedef std::shared_ptr<ClientObserver>    ObserverPtr;
            private:
                WPtr        _self;
                ObserverPtr _observer;
            public:
                static inline
                Ptr
                create()
                {
                    Ptr ptr(new Client());
                    ptr->build(ptr);
                    return ptr;
                }
            protected:
                Client();
            private:
                // non-copyable
                Client(const Client&);
                Client& operator=(const Client&);

            protected:
                void
                build(ClientBase::Ptr const&);
                void
                dispose();

                ClientBindingBasePtr
                createBinding(TreeNodeWPtr const&);

                void
                destroyBinding(ClientBindingBasePtr const&);
            };
        }
    }
}
