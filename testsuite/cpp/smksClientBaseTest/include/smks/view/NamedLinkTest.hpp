// ========================================================================= //
// Copyright 2016-2018 SUPAMONKS_STUDIO                                      //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
//                                                                           //
// ========================================================================= //
#pragma once

#include <iostream>
#include <gtest/gtest.h>

#include <smks/scn/Scene.hpp>
#include "smks/view/test/Client.hpp"
#include <smks/view/ClientBindingBase.hpp>
#include <std/to_string_xchg.hpp>


TEST(NamedLinkTest, SourceCreatedThenParmAdded)
{
    using namespace smks;

    const char*         linkName    = "__link__";
    const std::string   sourceName  = "__source__";
    const unsigned int  linkId      = util::string::getId(linkName);
    const unsigned int  sourceId    = util::string::getId(("/" + sourceName).c_str());

    scn::Scene::Ptr scene = scn::Scene::create();
    view::test::Client::Ptr client = view::test::Client::create();
    client->setScene(scene);
    //######################

    scn::TreeNode::Ptr target = scn::TreeNode::create("__target__", scene);
    scn::TreeNode::Ptr source = scn::TreeNode::create(sourceName.c_str(), scene);
    target->setParameterId(linkName, sourceId);

    std::cout << "_._._._._._._._._._._._._._._._._" << std::endl;

    target->unsetParameter(linkId);
    scn::TreeNode::destroy(source);

    //######################
    client->setScene(nullptr);
    scn::Scene::destroy(scene);
}

TEST(NamedLinkTest, ParmAddedThenSourceCreated)
{
    using namespace smks;

    const char*         linkName    = "__link__";
    const std::string   sourceName  = "__source__";
    const unsigned int  linkId      = util::string::getId(linkName);
    const unsigned int  sourceId    = util::string::getId(("/" + sourceName).c_str());

    scn::Scene::Ptr scene = scn::Scene::create();
    view::test::Client::Ptr client = view::test::Client::create();
    client->setScene(scene);
    //######################

    scn::TreeNode::Ptr target = scn::TreeNode::create("__target__", scene);
    std::cout << "TARGET = " << target->id() << "\tSOURCE = " << sourceId << std::endl;
    target->setParameterId(linkName, sourceId);
    scn::TreeNode::Ptr source = scn::TreeNode::create(sourceName.c_str(), scene);

    std::cout << "_._._._._._._._._._._._._._._._._" << std::endl;

    scn::TreeNode::destroy(source);
    target->unsetParameter(linkId);

    //######################
    client->setScene(nullptr);
    scn::Scene::destroy(scene);
}
