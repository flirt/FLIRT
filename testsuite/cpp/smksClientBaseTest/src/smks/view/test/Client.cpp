// ========================================================================= //
// Copyright 2016-2018 SUPAMONKS_STUDIO                                      //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
//                                                                           //
// ========================================================================= //
#include "smks/view/test/Binding.hpp"
#include "smks/view/test/Client.hpp"
#include "smks/view/test/ClientObserver.hpp"

using namespace smks;

view::test::Client::Client():
    ClientBase(),
    _self     (),
    _observer (ClientObserver::create(*this))
{ }

void
view::test::Client::build(ClientBase::Ptr const& ptr)
{
    ClientBase::build(ptr);
    _self = std::static_pointer_cast<Client>(ptr);
    registerObserver(_observer);
}

void
view::test::Client::dispose()
{
    deregisterObserver(_observer);
}

ClientBindingBase::Ptr
view::test::Client::createBinding(TreeNodeWPtr const& node)
{
    return Binding::create(_self, node);
}

void
view::test::Client::destroyBinding(ClientBindingBase::Ptr const& bind)
{ }
