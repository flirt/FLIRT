// ========================================================================= //
// Copyright 2016-2018 SUPAMONKS_STUDIO                                      //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
//                                                                           //
// ========================================================================= //
#include <std/to_string_xchg.hpp>

#include <smks/scn/TreeNode.hpp>
#include <smks/ClientBase.hpp>

#include "smks/view/test/Binding.hpp"

using namespace smks;

view::test::Binding::Binding(ClientBase::WPtr const&    client,
                             scn::TreeNode::WPtr const& node):
    ClientBindingBase(client, node)
{ }

void
view::test::Binding::handle(const xchg::ParmEvent& evt)
{
    ClientBase::Ptr const& cl = client().lock();
    if (!cl)
        return;

    unsigned int sourceId = 0;
    cl->getNodeParameter<unsigned int>(evt.parm(), sourceId, 0, evt.node());
    xchg::IdSet links;
    if (sourceId)
        cl->getLinksBetweenNodes(evt.node(), sourceId, links);
    std::cout
        << "parm evt: " << evt << "\n"
        << "\tsourceId = " << sourceId << "\n"
        << "\tlinks = " << links
        << std::endl;
}
