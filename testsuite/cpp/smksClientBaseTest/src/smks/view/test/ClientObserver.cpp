// ========================================================================= //
// Copyright 2016-2018 SUPAMONKS_STUDIO                                      //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
//                                                                           //
// ========================================================================= //
#include <iostream>

#include <smks/xchg/IdSet.hpp>
#include <smks/xchg/ParmEvent.hpp>
#include <smks/xchg/SceneEvent.hpp>
#include <smks/xchg/ClientEvent.hpp>
#include <smks/ClientBase.hpp>

#include "smks/view/test/ClientObserver.hpp"

using namespace smks;

void
view::test::ClientObserver::handle(ClientBase&, const xchg::ClientEvent& evt)
{
    std::cout << "client evt: " << evt << std::endl;
}

void
view::test::ClientObserver::handle(ClientBase&, const xchg::SceneEvent& evt)
{
    std::cout << "scene evt: " << evt << std::endl;
    if (evt.type() == xchg::SceneEvent::NODES_LINKED ||
        evt.type() == xchg::SceneEvent::NODES_UNLINKED)
    {
        xchg::IdSet links;
        _client.getLinksBetweenNodes(evt.target(), evt.source(), links);
        std::cout << "\tlinks = " << links << std::endl;
    }
}

void
view::test::ClientObserver::handleSceneParmEvent(ClientBase&, const xchg::ParmEvent& evt)
{
    std::cout << "parm evt: " << evt << std::endl;
}
