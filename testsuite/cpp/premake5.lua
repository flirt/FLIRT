-- ========================================================================= --
-- Copyright 2016-2018 SUPAMONKS_STUDIO                                      --
-- Author: Pierre-Edouard Landes <pel@supamonks.com>                         --
--                                                                           --
-- This program is free software: you can redistribute it and/or modify      --
-- it under the terms of the GNU Lesser General Public License as            --
-- published by the Free Software Foundation, either version 3 of the        --
-- License, or (at your option) any later version.                           --
--                                                                           --
-- This program is distributed in the hope that it will be useful,           --
-- but WITHOUT ANY WARRANTY; without even the implied warranty of            --
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             --
-- GNU Lesser General Public License for more details.                       --
--                                                                           --
-- You should have received a copy of the GNU Lesser General Public License  --
-- along with this program.  If not, see <https://www.gnu.org/licenses/>.    --
--                                                                           --
-- ========================================================================= --


project 'testsuite'

    language 'C++'
    smks.module.set_kind('ConsoleApp')

    files
    {
        '**.hpp',
        '**.cpp',
    }
    includedirs
    {
        'include',
        'smksSysTest/include',
        'smksMathTest/include',
        'smksXchgTest/include',
        'smksParmTest/include',
        'smksScnTest/include',
        'smksClientBaseTest/include',
    }

    if _ACTION:match('vs%d+') then
        buildoptions
        {
            '/bigobj',
        }
    end

    smks.dep.boost.includes()
    smks.dep.googletest.use()

    smks.module.uses_log()
    smks.module.uses_strid()
    smks.module.uses_util()
    smks.module.uses_xchg()
    smks.module.uses_asset()
    smks.module.uses_math()
    smks.module.uses_scn()
    smks.module.uses_clientbase()

    smks.module.copy_clientbase_to_targetdir()

    local subdir = 'abc'

    filter {}
    postbuildcommands
    {
        smks.os.make_sub_targetdir(subdir),
        smks.os.copy_to_targetdir(
            smks.repo_path('asset', 'abc', 'testsuite', '*.abc'),
            subdir)
    }
