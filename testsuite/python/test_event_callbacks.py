# =========================================================================== #
# Copyright 2016-2018 SUPAMONKS_STUDIO                                        #
# Author: Pierre-Edouard Landes <pel@supamonks.com>                           #
#                                                                             #
# This program is free software: you can redistribute it and/or modify        #
# it under the terms of the GNU Lesser General Public License as              #
# published by the Free Software Foundation, either version 3 of the          #
# License, or (at your option) any later version.                             #
#                                                                             #
# This program is distributed in the hope that it will be useful,             #
# but WITHOUT ANY WARRANTY; without even the implied warranty of              #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               #
# GNU Lesser General Public License for more details.                         #
#                                                                             #
# You should have received a copy of the GNU Lesser General Public License    #
# along with this program.  If not, see <https://www.gnu.org/licenses/>.      #
#                                                                             #
# =========================================================================== #
import unittest
import json
from import_pyflirt import import_pyflirt


class _EventRecorder:

    def __init__(self):
        self.history = []

    def handle(self, type, dict_info):
        all_info = dict_info
        all_info['event'] = type
        self.history.append(all_info)

    def flush(self):
        ret = self.history
        self.history = []
        return ret


class TestEventCallbacks(unittest.TestCase):

    def __init__(self, compiler, methodName='runTest'):
        super(TestEventCallbacks, self).__init__(methodName)
        self._compiler = compiler

    def setUp(self):
        module_cfg = json.dumps({
            'with_viewport': False,
            'cache_anim': False,
            'num_threads': 1
        })
        _pyflirt = import_pyflirt(compiler=self._compiler)
        _pyflirt.initialize(cfg=module_cfg)

        self.target = _pyflirt.create_data_node(
            name='target', parent=_pyflirt.get_scene())
        self.source_1 = _pyflirt.create_data_node(
            name='source_1', parent=_pyflirt.get_scene())
        self.source_2 = _pyflirt.create_data_node(
            name='source_2', parent=_pyflirt.get_scene())
        self.recorder = _EventRecorder()

    def tearDown(self):
        _pyflirt = import_pyflirt(compiler=self._compiler)
        self.recorder.flush()
        _pyflirt.destroy_data_node(node=self.source_2)
        _pyflirt.destroy_data_node(node=self.source_1)
        _pyflirt.destroy_data_node(node=self.target)
        _pyflirt.finalize()

    def test_node_creation_event_handling(self):
        _pyflirt = import_pyflirt(compiler=self._compiler)
        self.assertEquals(self.recorder.flush(), [])
        ###
        obs = _pyflirt.register_scene_observer(func=self.recorder.handle)
        ###
        new_target = _pyflirt.create_data_node(
            name='new_target', parent=_pyflirt.get_scene())

        history = self.recorder.flush()
        self.assertEquals(len(history), 1)  # node creation
        self.assertTrue(
            history[0],
            {
                'event': _pyflirt.SceneEvent.NODE_CREATED,
                'node': self.target,
                'parent': _pyflirt.get_scene()
            })
        ###
        _pyflirt.destroy_data_node(node=new_target)

        history = self.recorder.flush()
        self.assertEquals(len(history), 1)  # node creation
        self.assertTrue(
            history[0],
            {
                'event': _pyflirt.SceneEvent.NODE_DELETED,
                'node': self.target,
                'parent': _pyflirt.get_scene()
            })

        _pyflirt.deregister_observer(obs=obs)

    def test_node_linking_event_handling(self):
        _pyflirt = import_pyflirt(compiler=self._compiler)
        self.assertEquals(self.recorder.flush(), [])
        obs = _pyflirt.register_scene_observer(func=self.recorder.handle)
        # ---
        parm = _pyflirt.set_id_node_parameter_by_name(
            parm_name='my_parm',
            parm_value=self.source_1, node=self.target)
        # ---
        history = self.recorder.flush()
        self.assertEquals(len(history), 2)  # node linking
        self.assertTrue(
            history[0],
            {
                'event': _pyflirt.SceneEvent.NODES_LINKED,
                'target': self.target,
                'source': self.source_1
            })
        self.assertTrue(
            history[1],
            {
                'event': _pyflirt.SceneEvent.LINK_ATTACHED,
                'target': self.target,
                'source': self.source_1,
                'parm': parm
            })
        # ---
        _pyflirt.unset_node_parameter(parm=parm, node=self.target)
        # ---
        history = self.recorder.flush()
        self.assertEquals(len(history), 2)  # node unlinking
        self.assertTrue(
            history[0],
            {
                'event': _pyflirt.SceneEvent.LINK_DETACHED,
                'target': self.target,
                'source': self.source_1,
                'parm': parm
            })
        self.assertTrue(
            history[1],
            {
                'event': _pyflirt.SceneEvent.NODES_UNLINKED,
                'target': self.target,
                'source': self.source_1
            })

        _pyflirt.deregister_observer(obs=obs)

    def test_parm_connection_event_handling(self):
        _pyflirt = import_pyflirt(compiler=self._compiler)
        self.assertEquals(self.recorder.flush(), [])
        # ---
        obs = _pyflirt.register_scene_observer(func=self.recorder.handle)
        # ---
        parm_name = 'my_connected_parm'
        cnx = _pyflirt.connect_node_parameters(
            parm_type=_pyflirt.ParmType.STRING,
            master=self.target,
            slave=self.source_1,
            master_parm_name=parm_name)
        cnx_info = _pyflirt.get_connection_info(cnx=cnx)

        self.assertEquals(
            cnx_info['master_parm'],
            cnx_info['slave_parm'])
        self.assertEquals(
            cnx_info['master_parm_name'],
            cnx_info['slave_parm_name'])

        history = self.recorder.flush()
        self.assertEquals(len(history), 1)  # parm connection
        self.assertTrue(
            history[0],
            {
                'event': _pyflirt.SceneEvent.PARM_CONNECTED,
                'master': cnx_info['master'],
                'slave': cnx_info['slave'],
                'master_parm': cnx_info['master_parm'],
                'slave_parm': cnx_info['slave_parm'],
                'cnx': cnx
            })
        # ---
        _pyflirt.disconnect_node_parameters(cnx=cnx)

        history = self.recorder.flush()
        self.assertEquals(len(history), 1)  # parm disconnection
        self.assertTrue(
            history[0],
            {
                'event': _pyflirt.SceneEvent.PARM_DISCONNECTED,
                'master': cnx_info['master'],
                'slave': cnx_info['slave'],
                'master_parm': cnx_info['master_parm'],
                'slave_parm': cnx_info['slave_parm'],
                'cnx': cnx
            })
        # ---
        _pyflirt.deregister_observer(obs=obs)

    def test_parm_event_handling(self):
        _pyflirt = import_pyflirt(compiler=self._compiler)
        self.assertEquals(self.recorder.flush(), [])
        obs = _pyflirt.register_node_parameter_observer(
            func=self.recorder.handle, node=self.target)
        # ---
        parm = _pyflirt.set_node_parameter_by_name(
            parm_name='my_parm', parm_type=_pyflirt.ParmType.STRING,
            node=self.target, parm_value='a string')
        # ---
        history = self.recorder.flush()
        self.assertEquals(len(history), 1)
        self.assertEquals(
            history[0],
            {
                'event': _pyflirt.ParmEvent.ADDED,
                'parm': parm,
                'node': self.target,
                'comes_from_ancestor': False,
                'comes_from_link': False,
                'emitted_by_node': True
            })
        # ---
        _pyflirt.set_node_parameter(
            parm=parm, node=self.target, parm_value='a string')
        # ---
        self.assertEquals(self.recorder.flush(), [])
        # ---
        _pyflirt.set_node_parameter(
            parm=parm, node=self.target, parm_value='a really new string')
        # ---
        history = self.recorder.flush()
        self.assertEquals(len(history), 1)
        self.assertEquals(
            history[0],
            {
                'event': _pyflirt.ParmEvent.CHANGED,
                'parm': parm,
                'node': self.target,
                'comes_from_ancestor': False,
                'comes_from_link': False,
                'emitted_by_node': True
            })
        # ---
        _pyflirt.unset_node_parameter(parm=parm, node=self.target)
        # ---
        history = self.recorder.flush()
        self.assertEquals(len(history), 1)
        self.assertEquals(
            history[0],
            {
                'event': _pyflirt.ParmEvent.REMOVED,
                'parm': parm,
                'node': self.target,
                'comes_from_ancestor': False,
                'comes_from_link': False,
                'emitted_by_node': True
            })
        _pyflirt.deregister_observer(obs=obs)

    def test_parm_event_deregistration(self):
        _pyflirt = import_pyflirt(compiler=self._compiler)
        obs = _pyflirt.register_node_parameter_observer(
            func=self.recorder.handle, node=self.target)

        parm = _pyflirt.set_node_parameter_by_name(
            parm_type=_pyflirt.ParmType.STRING, node=self.target,
            parm_name='my_parm', parm_value='a string')
        _pyflirt.set_node_parameter(
            parm=parm, node=self.target, parm_value='a string')
        _pyflirt.set_node_parameter(
            parm=parm, node=self.target, parm_value='a really new string')
        _pyflirt.unset_node_parameter(parm=parm, node=self.target)

        history = self.recorder.flush()
        self.assertGreater(len(history), 0)
        history = self.recorder.flush()
        self.assertEquals(history, [])

        ###
        _pyflirt.deregister_observer(obs=obs)
        ###
        parm = _pyflirt.set_node_parameter_by_name(
            parm_name='my_second_parm', parm_type=_pyflirt.ParmType.INT,
            node=self.target, parm_value=42)
        _pyflirt.unset_node_parameter(parm=parm, node=self.target)

        history = self.recorder.flush()
        self.assertEquals(history, [])

    # -- Insert tests in suite ------------------------------------------------
    @staticmethod
    def get_suite(compiler):
        suite = unittest.TestSuite()
        for member in dir(TestEventCallbacks):
            if member.startswith('test_'):
                suite.addTest(
                    TestEventCallbacks(compiler=compiler, methodName=member))
        return suite
    # -------------------------------------------------------------------------
