# =========================================================================== #
# Copyright 2016-2018 SUPAMONKS_STUDIO                                        #
# Author: Pierre-Edouard Landes <pel@supamonks.com>                           #
#                                                                             #
# This program is free software: you can redistribute it and/or modify        #
# it under the terms of the GNU Lesser General Public License as              #
# published by the Free Software Foundation, either version 3 of the          #
# License, or (at your option) any later version.                             #
#                                                                             #
# This program is distributed in the hope that it will be useful,             #
# but WITHOUT ANY WARRANTY; without even the implied warranty of              #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               #
# GNU Lesser General Public License for more details.                         #
#                                                                             #
# You should have received a copy of the GNU Lesser General Public License    #
# along with this program.  If not, see <https://www.gnu.org/licenses/>.      #
#                                                                             #
# =========================================================================== #
import unittest
import json
from import_pyflirt import import_pyflirt


class TestParameterMethods(unittest.TestCase):

    def __init__(self, compiler, methodName='runTest'):
        super(TestParameterMethods, self).__init__(methodName)
        self._compiler = compiler

    def setUp(self):
        module_cfg = json.dumps({
            'with_viewport': False,
            'cache_anim': False,
            'num_threads': 1
        })
        _pyflirt = import_pyflirt(compiler=self._compiler)
        _pyflirt.initialize(cfg=module_cfg)

        self.target = _pyflirt.create_data_node(
            name='target', parent=_pyflirt.get_scene())
        self.source_1 = _pyflirt.create_data_node(
            name='source_1', parent=_pyflirt.get_scene())
        self.source_2 = _pyflirt.create_data_node(
            name='source_2', parent=_pyflirt.get_scene())

    def tearDown(self):
        _pyflirt = import_pyflirt(compiler=self._compiler)
        _pyflirt.destroy_data_node(node=self.source_2)
        _pyflirt.destroy_data_node(node=self.source_1)
        _pyflirt.destroy_data_node(node=self.target)
        _pyflirt.finalize()

    def set_parm(self, parm_type, parm_name, parm_value, expected_flags=[]):
        _pyflirt = import_pyflirt(compiler=self._compiler)
        ret_parms = _pyflirt.get_current_node_parameters(node=self.target)
        self.assertEquals(ret_parms, [])

        # ---
        parm = _pyflirt.set_node_parameter_by_name(
            parm_name=parm_name, parm_type=parm_type, parm_value=parm_value,
            node=self.target)
        # ---
        ret_parms = _pyflirt.get_current_node_parameters(node=self.target)
        ret_parm_info = _pyflirt.get_node_parameter_info(
            parm=parm, node=self.target)
        ret_parm_value = _pyflirt.get_node_parameter(
            parm=parm, node=self.target)

        self.assertEquals(ret_parms, [parm])
        self.assertTrue('type' in ret_parm_info)
        self.assertEquals(ret_parm_info['type'], parm_type)
        self.assertTrue('name' in ret_parm_info)
        self.assertEquals(ret_parm_info['name'], parm_name)
        for flag in self._get_descr_parm_flags():
            self.assertTrue(flag in ret_parm_info)
            self.assertEquals(
                ret_parm_info[flag],
                True if flag in expected_flags else False)

        if type(ret_parm_value) is list:
            for i in range(len(ret_parm_value)):
                self.assertAlmostEqual(
                    ret_parm_value[i],
                    parm_value[i], places=5)
        else:
            self.assertAlmostEqual(ret_parm_value, parm_value)

        # ---
        _pyflirt.unset_node_parameter(parm=parm, node=self.target)
        # ---
        ret_parms = _pyflirt.get_current_node_parameters(node=self.target)
        ret_parm_info = _pyflirt.get_node_parameter_info(
            parm=parm,
            node=self.target)
        ret_parm_value = _pyflirt.get_node_parameter(
            parm=parm,
            node=self.target)

        self.assertEquals(ret_parms, [])
        self.assertDictEqual(ret_parm_info, {})
        self.assertIsNone(ret_parm_value)

    def upd_parm(self, parm_type, parm_name, parm_value_1,
                 parm_value_2, expected_flags=[]):
        _pyflirt = import_pyflirt(compiler=self._compiler)
        ret_parms = _pyflirt.get_current_node_parameters(node=self.target)
        self.assertEquals(ret_parms, [])
        # ---
        parm = _pyflirt.set_node_parameter_by_name(
            parm_name=parm_name, parm_type=parm_type, node=self.target,
            parm_value=parm_value_1)
        # ---
        ret_parm_value = _pyflirt.get_node_parameter(
            parm=parm, node=self.target)

        if type(ret_parm_value) is list:
            for i in range(len(ret_parm_value)):
                self.assertAlmostEqual(
                    ret_parm_value[i], parm_value_1[i], places=5)
        else:
            self.assertAlmostEqual(ret_parm_value, parm_value_1)

        # ---
        _pyflirt.set_node_parameter(
            parm=parm, node=self.target, parm_value=parm_value_2)
        # ---
        ret_parm_value = _pyflirt.get_node_parameter(
            parm=parm, node=self.target)
        ret_parm_info = _pyflirt.get_node_parameter_info(
            parm=parm, node=self.target)

        self.assertTrue('type' in ret_parm_info)
        self.assertEquals(ret_parm_info['type'], parm_type)
        self.assertTrue('name' in ret_parm_info)
        self.assertEquals(ret_parm_info['name'], parm_name)
        for flag in self._get_descr_parm_flags():
            self.assertTrue(flag in ret_parm_info)
            self.assertEquals(
                ret_parm_info[flag], True if flag in expected_flags else False)
        if type(ret_parm_value) is list:
            for i in range(len(ret_parm_value)):
                self.assertAlmostEqual(
                    ret_parm_value[i], parm_value_2[i], places=5)
        else:
            self.assertAlmostEqual(ret_parm_value, parm_value_2)

        # ---
        _pyflirt.unset_node_parameter(parm=parm, node=self.target)
        # ---
        ret_parms = _pyflirt.get_current_node_parameters(node=self.target)
        ret_parm_value = _pyflirt.get_node_parameter(
            parm=parm, node=self.target)

        self.assertEquals(ret_parms, [])
        self.assertIsNone(ret_parm_value)

    def set_and_update_id_parm(self, parm_name, parm_value_1, parm_value_2):
        _pyflirt = import_pyflirt(compiler=self._compiler)
        ret_parms = _pyflirt.get_current_node_parameters(node=self.target)
        self.assertEquals(ret_parms, [])
        # ---
        parm = _pyflirt.set_id_node_parameter_by_name(parm_name=parm_name,
                                                      parm_value=parm_value_1,
                                                      node=self.target)
        # ---
        ret_parms = _pyflirt.get_current_node_parameters(node=self.target)
        ret_parm_info = _pyflirt.get_node_parameter_info(
            parm=parm, node=self.target)
        ret_parm_value = _pyflirt.get_node_parameter(
            parm=parm, node=self.target)

        self.assertEquals(ret_parms, [parm])
        self.assertTrue('type' in ret_parm_info)
        self.assertEquals(ret_parm_info['type'], _pyflirt.ParmType.UINT)
        self.assertTrue('name' in ret_parm_info)
        self.assertEquals(ret_parm_info['name'], parm_name)
        for flag in self._get_descr_parm_flags():
            self.assertTrue(flag in ret_parm_info)
            self.assertEquals(
                ret_parm_info[flag],
                True if flag == 'creates_link' else False)
        self.assertEquals(ret_parm_value, parm_value_1)

        # ---
        _pyflirt.set_node_parameter(
            parm=parm, parm_value=parm_value_2, node=self.target)
        # ---
        ret_parm_value = _pyflirt.get_node_parameter(
            parm=parm, node=self.target)
        self.assertEquals(ret_parms, [parm])
        self.assertTrue('type' in ret_parm_info)
        self.assertEquals(ret_parm_info['type'], _pyflirt.ParmType.UINT)
        self.assertTrue('name' in ret_parm_info)
        self.assertEquals(ret_parm_info['name'], parm_name)
        for flag in self._get_descr_parm_flags():
            self.assertTrue(flag in ret_parm_info)
            self.assertEquals(
                ret_parm_info[flag], True if flag == 'creates_link' else False)
        self.assertEquals(ret_parm_value, parm_value_2)

        # ---
        _pyflirt.unset_node_parameter(parm=parm, node=self.target)
        # ---
        ret_parms = _pyflirt.get_current_node_parameters(node=self.target)
        ret_parm_info = _pyflirt.get_node_parameter_info(
            parm=parm, node=self.target)
        ret_parm_value = _pyflirt.get_node_parameter(
            parm=parm, node=self.target)

        self.assertEquals(ret_parms, [])
        self.assertDictEqual(ret_parm_info, {})
        self.assertIsNone(ret_parm_value)

    def set_and_update_switch_parm(self, parm_name):
        _pyflirt = import_pyflirt(compiler=self._compiler)
        ret_parms = _pyflirt.get_current_node_parameters(node=self.target)
        self.assertEquals(ret_parms, [])
        # ---
        parm = _pyflirt.set_switch_node_parameter_by_name(
            parm_name=parm_name, parm_value=True, node=self.target)
        # ---
        ret_parms = _pyflirt.get_current_node_parameters(node=self.target)
        ret_parm_info = _pyflirt.get_node_parameter_info(
            parm=parm, node=self.target)
        ret_parm_value = _pyflirt.get_node_parameter(
            parm=parm, node=self.target)

        self.assertEquals(ret_parms, [parm])
        self.assertTrue('type' in ret_parm_info)
        self.assertEquals(ret_parm_info['type'], _pyflirt.ParmType.CHAR)
        self.assertTrue('name' in ret_parm_info)
        self.assertEquals(ret_parm_info['name'], parm_name)
        for flag in self._get_descr_parm_flags():
            self.assertTrue(flag in ret_parm_info)
            self.assertEquals(
                ret_parm_info[flag],
                True if flag == 'switches_state' else False)
        self.assertEquals(ret_parm_value, _pyflirt.ActionState.ACTIVE)

        # ---
        _pyflirt.unset_node_parameter(parm=parm, node=self.target)
        # ---
        ret_parms = _pyflirt.get_current_node_parameters(node=self.target)
        ret_parm_info = _pyflirt.get_node_parameter_info(
            parm=parm, node=self.target)
        ret_parm_value = _pyflirt.get_node_parameter(
            parm=parm, node=self.target)

        self.assertEquals(ret_parms, [])
        self.assertDictEqual(ret_parm_info, {})
        self.assertIsNone(ret_parm_value)

    def test_missing_parm_queries(self):
        _pyflirt = import_pyflirt(compiler=self._compiler)
        ret_parms = _pyflirt.get_current_node_parameters(node=self.target)
        ret_parm_info = _pyflirt.get_node_parameter_info(
            parm=424242424242, node=self.target)
        ret_parm_value = _pyflirt.get_node_parameter(
            parm=424242424242, node=self.target)

        self.assertEquals(ret_parms, [])
        self.assertDictEqual(ret_parm_info, {})
        self.assertIsNone(ret_parm_value)

    def test_set_bool(self):
        _pyflirt = import_pyflirt(compiler=self._compiler)
        self.set_parm(parm_type=_pyflirt.ParmType.BOOL,
                      parm_name='my_parm',
                      parm_value=True)

    def test_upd_bool(self):
        _pyflirt = import_pyflirt(compiler=self._compiler)
        self.upd_parm(parm_type=_pyflirt.ParmType.BOOL,
                      parm_name='my_parm',
                      parm_value_1=True,
                      parm_value_2=False)

    def test_set_int(self):
        _pyflirt = import_pyflirt(compiler=self._compiler)
        self.set_parm(parm_type=_pyflirt.ParmType.INT,
                      parm_name='my_parm',
                      parm_value=-2147483648)

    def test_upd_int(self):
        _pyflirt = import_pyflirt(compiler=self._compiler)
        self.upd_parm(parm_type=_pyflirt.ParmType.INT,
                      parm_name='my_parm',
                      parm_value_1=-2147483648,
                      parm_value_2=2147483647)

    def test_set_uint(self):
        _pyflirt = import_pyflirt(compiler=self._compiler)
        self.set_parm(parm_type=_pyflirt.ParmType.UINT,
                      parm_name='my_parm',
                      parm_value=4294967294)

    def test_upd_uint(self):
        _pyflirt = import_pyflirt(compiler=self._compiler)
        self.upd_parm(parm_type=_pyflirt.ParmType.UINT,
                      parm_name='my_parm',
                      parm_value_1=4294967294,
                      parm_value_2=4294967295)

    def test_set_char(self):
        _pyflirt = import_pyflirt(compiler=self._compiler)
        self.set_parm(parm_type=_pyflirt.ParmType.CHAR,
                      parm_name='my_parm',
                      parm_value=-128)

    def test_upd_char(self):
        _pyflirt = import_pyflirt(compiler=self._compiler)
        self.upd_parm(parm_type=_pyflirt.ParmType.CHAR,
                      parm_name='my_parm',
                      parm_value_1=-128,
                      parm_value_2=127)

    def test_set_size(self):
        _pyflirt = import_pyflirt(compiler=self._compiler)
        self.set_parm(parm_type=_pyflirt.ParmType.SIZE,
                      parm_name='my_parm',
                      parm_value=18446744073709551614)

    def test_upd_size(self):
        _pyflirt = import_pyflirt(compiler=self._compiler)
        self.upd_parm(parm_type=_pyflirt.ParmType.SIZE,
                      parm_name='my_parm',
                      parm_value_1=18446744073709551614,
                      parm_value_2=18446744073709551615)

    def test_set_int2(self):
        _pyflirt = import_pyflirt(compiler=self._compiler)
        self.set_parm(parm_type=_pyflirt.ParmType.INT_2,
                      parm_name='my_parm',
                      parm_value=[-2147483648, 2147483647])

    def test_upd_int2(self):
        _pyflirt = import_pyflirt(compiler=self._compiler)
        self.upd_parm(parm_type=_pyflirt.ParmType.INT_2,
                      parm_name='my_parm',
                      parm_value_1=[-2147483648, 2147483647],
                      parm_value_2=[2147483647, -2147483648])

    def test_set_int3(self):
        _pyflirt = import_pyflirt(compiler=self._compiler)
        self.set_parm(parm_type=_pyflirt.ParmType.INT_3,
                      parm_name='my_parm',
                      parm_value=[-2147483648, 2147483647, 42])

    def test_upd_int3(self):
        _pyflirt = import_pyflirt(compiler=self._compiler)
        self.upd_parm(parm_type=_pyflirt.ParmType.INT_3,
                      parm_name='my_parm',
                      parm_value_1=[-2147483648, 2147483647, 42],
                      parm_value_2=[42, -2147483648, 2147483647])

    def test_set_int4(self):
        _pyflirt = import_pyflirt(compiler=self._compiler)
        self.set_parm(parm_type=_pyflirt.ParmType.INT_4,
                      parm_name='my_parm',
                      parm_value=[-2147483648, 2147483647, 42, -42])

    def test_upd_int4(self):
        _pyflirt = import_pyflirt(compiler=self._compiler)
        self.upd_parm(parm_type=_pyflirt.ParmType.INT_4,
                      parm_name='my_parm',
                      parm_value_1=[-2147483648, 2147483647, 42, -42],
                      parm_value_2=[-42, -2147483648, 2147483647, 42])

    def test_set_float(self):
        _pyflirt = import_pyflirt(compiler=self._compiler)
        self.set_parm(parm_type=_pyflirt.ParmType.FLOAT,
                      parm_name='my_parm',
                      parm_value=-42.5)

    def test_upd_float(self):
        _pyflirt = import_pyflirt(compiler=self._compiler)
        self.upd_parm(parm_type=_pyflirt.ParmType.FLOAT,
                      parm_name='my_parm',
                      parm_value_1=-42.5,
                      parm_value_2=42.5)

    def test_set_float2(self):
        _pyflirt = import_pyflirt(compiler=self._compiler)
        self.set_parm(parm_type=_pyflirt.ParmType.FLOAT_2,
                      parm_name='my_parm',
                      parm_value=[-42.5, 24.5])

    def test_upd_float2(self):
        _pyflirt = import_pyflirt(compiler=self._compiler)
        self.upd_parm(parm_type=_pyflirt.ParmType.FLOAT_2,
                      parm_name='my_parm',
                      parm_value_1=[-42.5, 24.5],
                      parm_value_2=[42.5, -24.5])

    def test_set_float4(self):
        _pyflirt = import_pyflirt(compiler=self._compiler)
        self.set_parm(parm_type=_pyflirt.ParmType.FLOAT_4,
                      parm_name='my_parm',
                      parm_value=[-42.5, 24.5, 12.5, -12.5])

    def test_upd_float4(self):
        _pyflirt = import_pyflirt(compiler=self._compiler)
        self.upd_parm(parm_type=_pyflirt.ParmType.FLOAT_4,
                      parm_name='my_parm',
                      parm_value_1=[-42.5, 24.5, 12.5, -12.5],
                      parm_value_2=[42.5, -24.5, -12.5, 12.5])

    def test_set_affine(self):
        _pyflirt = import_pyflirt(compiler=self._compiler)
        self.set_parm(parm_type=_pyflirt.ParmType.AFFINE_3,
                      parm_name='my_parm',
                      parm_value=[2.5, 3.5, 4.5, 0.0, 2.6, 3.6, 4.6, 0.0, 2.7,
                                  3.7, 4.7, 0.0, 0.0, 0.0, 0.0, 1.0])

    def test_upd_affine(self):
        _pyflirt = import_pyflirt(compiler=self._compiler)
        self.upd_parm(parm_type=_pyflirt.ParmType.AFFINE_3,
                      parm_name='my_parm',
                      parm_value_1=[2.5, 3.5, 4.5, 0.0, 2.6, 3.6, 4.6, 0.0,
                                    2.7, 3.7, 4.7, 0.0, 0.0, 0.0, 0.0, 1.0],
                      parm_value_2=[12.5, 13.5, 14.5, 0.0, 12.6, 13.6, 14.6,
                                    0.0, 12.7, 13.7, 14.7, 0.0, 0.0, 0.0,
                                    0.0, 1.0])

    def test_set_string(self):
        _pyflirt = import_pyflirt(compiler=self._compiler)
        self.set_parm(parm_type=_pyflirt.ParmType.STRING,
                      parm_name='my_parm',
                      parm_value='my value is 42')

    def test_upd_string(self):
        _pyflirt = import_pyflirt(compiler=self._compiler)
        self.upd_parm(parm_type=_pyflirt.ParmType.STRING,
                      parm_name='my_parm',
                      parm_value_1='my value is 42',
                      parm_value_2='my value is 43')

    def test_set_ids(self):
        _pyflirt = import_pyflirt(compiler=self._compiler)
        self.set_parm(parm_type=_pyflirt.ParmType.IDSET,
                      parm_name='my_parm',
                      parm_value=[self.source_1, self.source_2],
                      expected_flags=['creates_link'])

    def test_upd_ids(self):
        _pyflirt = import_pyflirt(compiler=self._compiler)
        self.upd_parm(parm_type=_pyflirt.ParmType.IDSET,
                      parm_name='my_parm',
                      parm_value_1=[self.source_1, self.source_2],
                      parm_value_2=[self.source_1],
                      expected_flags=['creates_link'])

    def test_set_and_update_id(self):
        self.set_and_update_id_parm(parm_name='my_parm',
                                    parm_value_1=self.source_1,
                                    parm_value_2=self.source_2)

    def test_set_and_update_switch(self):
        self.set_and_update_switch_parm(parm_name='my_parm')

    def test_change_parm_type(self):
        _pyflirt = import_pyflirt(compiler=self._compiler)

        parm_name = 'my_parm'
        parm_type_1 = _pyflirt.ParmType.INT
        parm_value_1 = 42
        parm_type_2 = _pyflirt.ParmType.STRING
        parm_value_2 = 'fourty-two'

        # ---
        parm = _pyflirt.set_node_parameter_by_name(
            parm_name=parm_name, node=self.target, parm_type=parm_type_1,
            parm_value=parm_value_1)
        # ---
        ret_parm_info = _pyflirt.get_node_parameter_info(
            parm=parm, node=self.target)
        ret_parm_value = _pyflirt.get_node_parameter(
            parm=parm, node=self.target)
        self.assertTrue('type' in ret_parm_info)
        self.assertEquals(ret_parm_info['type'], parm_type_1)
        self.assertAlmostEquals(ret_parm_value, parm_value_1)

        # ---
        parm = _pyflirt.set_node_parameter_by_name(
            parm_name=parm_name, node=self.target, parm_type=parm_type_2,
            parm_value=parm_value_2)
        # ---
        ret_parm_info = _pyflirt.get_node_parameter_info(
            parm=parm, node=self.target)
        ret_parm_value = _pyflirt.get_node_parameter(
            parm=parm, node=self.target)
        self.assertTrue('type' in ret_parm_info)
        self.assertEquals(ret_parm_info['type'], parm_type_2)
        self.assertAlmostEquals(ret_parm_value, parm_value_2)

        _pyflirt.unset_node_parameter(parm=parm, node=self.target)

    def test_upd_buggy(self):
        _pyflirt = import_pyflirt(compiler=self._compiler)
        error_thrown = False
        try:
            self.upd_parm(parm_type=_pyflirt.ParmType.IDSET,
                          parm_name='my_parm',
                          parm_value_1=[self.source_1, self.source_2],
                          parm_value_2='incompatible value')
        except _pyflirt.Error:
            error_thrown = True
        self.assertTrue(error_thrown)

    def test_forbidden_switch_value(self):
        _pyflirt = import_pyflirt(compiler=self._compiler)
        # ---
        parm = _pyflirt.set_switch_node_parameter_by_name(
            parm_name='my_parm', node=self.target, parm_value=False)
        # ---
        ret_parm_value = _pyflirt.get_node_parameter(
            parm=parm, node=self.target)
        self.assertEquals(ret_parm_value, _pyflirt.ActionState.INACTIVE)

        # ---
        _pyflirt.set_node_parameter(
            parm=parm, node=self.target,
            parm_value=_pyflirt.ActionState.ACTIVATING)
        # ---
        ret_parm_value = _pyflirt.get_node_parameter(
            parm=parm, node=self.target)
        self.assertEquals(ret_parm_value, _pyflirt.ActionState.ACTIVE)

        # ---
        _pyflirt.set_node_parameter(
            parm=parm, node=self.target,
            parm_value=_pyflirt.ActionState.DEACTIVATING)
        # ---
        ret_parm_value = _pyflirt.get_node_parameter(
            parm=parm, node=self.target)
        self.assertIsNone(ret_parm_value)

    @staticmethod
    def _get_descr_parm_flags():
        return [
            'switches_state',
            'triggers_action',
            'needs_scene',
            'creates_link']

    # -- Insert tests in suite ------------------------------------------------
    @staticmethod
    def get_suite(compiler):
        suite = unittest.TestSuite()
        for member in dir(TestParameterMethods):
            if member.startswith('test_'):
                suite.addTest(
                    TestParameterMethods(compiler=compiler, methodName=member))
        return suite
    # -------------------------------------------------------------------------
