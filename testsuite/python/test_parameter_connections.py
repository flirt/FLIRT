# =========================================================================== #
# Copyright 2016-2018 SUPAMONKS_STUDIO                                        #
# Author: Pierre-Edouard Landes <pel@supamonks.com>                           #
#                                                                             #
# This program is free software: you can redistribute it and/or modify        #
# it under the terms of the GNU Lesser General Public License as              #
# published by the Free Software Foundation, either version 3 of the          #
# License, or (at your option) any later version.                             #
#                                                                             #
# This program is distributed in the hope that it will be useful,             #
# but WITHOUT ANY WARRANTY; without even the implied warranty of              #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               #
# GNU Lesser General Public License for more details.                         #
#                                                                             #
# You should have received a copy of the GNU Lesser General Public License    #
# along with this program.  If not, see <https://www.gnu.org/licenses/>.      #
#                                                                             #
# =========================================================================== #
import unittest
import json
from import_pyflirt import import_pyflirt


class TestParameterConnections(unittest.TestCase):

    def __init__(self, compiler, methodName='runTest'):
        super(TestParameterConnections, self).__init__(methodName)
        self._compiler = compiler

    def setUp(self):
        module_cfg = json.dumps({
            'with_viewport': False,
            'cache_anim': False,
            'num_threads': 1
        })
        _pyflirt = import_pyflirt(compiler=self._compiler)
        _pyflirt.initialize(cfg=module_cfg)

        self.master = _pyflirt.create_data_node(
            name='master', parent=_pyflirt.get_scene())
        self.slave = _pyflirt.create_data_node(
            name='slave', parent=_pyflirt.get_scene())
        self.source_1 = _pyflirt.create_data_node(
            name='source_1', parent=_pyflirt.get_scene())
        self.source_2 = _pyflirt.create_data_node(
            name='source_2', parent=_pyflirt.get_scene())

    def tearDown(self):
        _pyflirt = import_pyflirt(compiler=self._compiler)
        _pyflirt.destroy_data_node(node=self.source_2)
        _pyflirt.destroy_data_node(node=self.source_1)
        _pyflirt.destroy_data_node(node=self.slave)
        _pyflirt.destroy_data_node(node=self.master)
        _pyflirt.finalize()

    def _connect_parm(
            self, parm_type, master_parm_name, slave_parm_name,
            parm_value_1, parm_value_2):
        # ---
        _pyflirt = import_pyflirt(compiler=self._compiler)
        cnx = _pyflirt.connect_node_parameters(
            parm_type=parm_type, master_parm_name=master_parm_name,
            master=self.master, slave_parm_name=slave_parm_name,
            slave=self.slave)

        self.assertGreater(cnx, 0)
        cnx_info = _pyflirt.get_connection_info(cnx=cnx)
        self.assertTrue(
            isinstance(cnx_info, dict) and 'slave_parm' in cnx_info)
        slave_parm = cnx_info['slave_parm']
        # ---
        _pyflirt.set_node_parameter_by_name(
            parm_name=master_parm_name, node=self.master,
            parm_value=parm_value_1, parm_type=parm_type)

        ret_parm_value = _pyflirt.get_node_parameter(
            parm=slave_parm, node=self.slave)
        if type(ret_parm_value) is list:
            for i in range(len(ret_parm_value)):
                self.assertAlmostEqual(
                    ret_parm_value[i], parm_value_1[i], places=5)
        else:
            self.assertAlmostEqual(ret_parm_value, parm_value_1)
        # ---
        _pyflirt.set_node_parameter_by_name(
            parm_name=master_parm_name, node=self.master,
            parm_value=parm_value_2, parm_type=parm_type)

        ret_parm_value = _pyflirt.get_node_parameter(
            parm=slave_parm, node=self.slave)
        if type(ret_parm_value) is list:
            for i in range(len(ret_parm_value)):
                self.assertAlmostEqual(
                    ret_parm_value[i], parm_value_2[i], places=5)
        else:
            self.assertAlmostEqual(ret_parm_value, parm_value_2)
        # ---

    def _connect_id_parm(
            self, master_parm_name, slave_parm_name, parm_value_1,
            parm_value_2):
        _pyflirt = import_pyflirt(compiler=self._compiler)
        # ---
        cnx = _pyflirt.connect_id_node_parameters(
            master_parm_name=master_parm_name, master=self.master,
            slave_parm_name=slave_parm_name, slave=self.slave)

        self.assertGreater(cnx, 0)
        cnx_info = _pyflirt.get_connection_info(cnx=cnx)
        self.assertTrue(
            isinstance(cnx_info, dict) and 'slave_parm' in cnx_info)
        slave_parm = cnx_info['slave_parm']
        # ---
        _pyflirt.set_id_node_parameter_by_name(
            parm_name=master_parm_name, node=self.master,
            parm_value=parm_value_1)

        ret_parm_value = _pyflirt.get_node_parameter(
            parm=slave_parm, node=self.slave)
        if type(ret_parm_value) is list:
            for i in range(len(ret_parm_value)):
                self.assertAlmostEqual(
                    ret_parm_value[i], parm_value_1[i], places=5)
        else:
            self.assertAlmostEqual(ret_parm_value, parm_value_1)
        # ---
        _pyflirt.set_id_node_parameter_by_name(
            parm_name=master_parm_name, node=self.master,
            parm_value=parm_value_2)

        ret_parm_value = _pyflirt.get_node_parameter(
            parm=slave_parm, node=self.slave)
        if type(ret_parm_value) is list:
            for i in range(len(ret_parm_value)):
                self.assertAlmostEqual(
                    ret_parm_value[i], parm_value_2[i], places=5)
        else:
            self.assertAlmostEqual(ret_parm_value, parm_value_2)
        # ---

    def test_connect_bool(self):
        _pyflirt = import_pyflirt(compiler=self._compiler)
        self._connect_parm(
            parm_type=_pyflirt.ParmType.BOOL,
            master_parm_name='my_master_parm',
            slave_parm_name='my_slave_parm',
            parm_value_1=True,
            parm_value_2=False)

    def test_connect_int(self):
        _pyflirt = import_pyflirt(compiler=self._compiler)
        self._connect_parm(
            parm_type=_pyflirt.ParmType.INT,
            master_parm_name='my_master_parm',
            slave_parm_name='my_slave_parm',
            parm_value_1=-2147483648,
            parm_value_2=2147483647)

    def test_connect_uint(self):
        _pyflirt = import_pyflirt(compiler=self._compiler)
        self._connect_parm(
            parm_type=_pyflirt.ParmType.UINT,
            master_parm_name='my_master_parm',
            slave_parm_name='my_slave_parm',
            parm_value_1=4294967294,
            parm_value_2=4294967295)

    def test_connect_char(self):
        _pyflirt = import_pyflirt(compiler=self._compiler)
        self._connect_parm(
            parm_type=_pyflirt.ParmType.CHAR,
            master_parm_name='my_master_parm',
            slave_parm_name='my_slave_parm',
            parm_value_1=-128,
            parm_value_2=127)

    def test_connect_size(self):
        _pyflirt = import_pyflirt(compiler=self._compiler)
        self._connect_parm(
            parm_type=_pyflirt.ParmType.SIZE,
            master_parm_name='my_master_parm',
            slave_parm_name='my_slave_parm',
            parm_value_1=18446744073709551614,
            parm_value_2=18446744073709551615)

    def test_connect_int2(self):
        _pyflirt = import_pyflirt(compiler=self._compiler)
        self._connect_parm(
            parm_type=_pyflirt.ParmType.INT_2,
            master_parm_name='my_master_parm',
            slave_parm_name='my_slave_parm',
            parm_value_1=[-2147483648, 2147483647],
            parm_value_2=[2147483647, -2147483648])

    def test_connect_int3(self):
        _pyflirt = import_pyflirt(compiler=self._compiler)
        self._connect_parm(
            parm_type=_pyflirt.ParmType.INT_3,
            master_parm_name='my_master_parm',
            slave_parm_name='my_slave_parm',
            parm_value_1=[-2147483648, 2147483647, 42],
            parm_value_2=[42, -2147483648, 2147483647])

    def test_connect_int4(self):
        _pyflirt = import_pyflirt(compiler=self._compiler)
        self._connect_parm(
            parm_type=_pyflirt.ParmType.INT_4,
            master_parm_name='my_master_parm',
            slave_parm_name='my_slave_parm',
            parm_value_1=[-2147483648, 2147483647, 42, -42],
            parm_value_2=[-42, -2147483648, 2147483647, 42])

    def test_connect_float(self):
        _pyflirt = import_pyflirt(compiler=self._compiler)
        self._connect_parm(
            parm_type=_pyflirt.ParmType.FLOAT,
            master_parm_name='my_master_parm',
            slave_parm_name='my_slave_parm',
            parm_value_1=-42.5,
            parm_value_2=42.5)

    def test_connect_float2(self):
        _pyflirt = import_pyflirt(compiler=self._compiler)
        self._connect_parm(
            parm_type=_pyflirt.ParmType.FLOAT_2,
            master_parm_name='my_master_parm',
            slave_parm_name='my_slave_parm',
            parm_value_1=[-42.5, 24.5],
            parm_value_2=[42.5, -24.5])

    def test_connect_float4(self):
        _pyflirt = import_pyflirt(compiler=self._compiler)
        self._connect_parm(
            parm_type=_pyflirt.ParmType.FLOAT_4,
            master_parm_name='my_master_parm',
            slave_parm_name='my_slave_parm',
            parm_value_1=[-42.5, 24.5, 12.5, -12.5],
            parm_value_2=[42.5, -24.5, -12.5, 12.5])

    def test_connect_affine(self):
        _pyflirt = import_pyflirt(compiler=self._compiler)
        self._connect_parm(
            parm_type=_pyflirt.ParmType.AFFINE_3,
            master_parm_name='my_master_parm',
            slave_parm_name='my_slave_parm',
            parm_value_1=[
                2.5, 3.5, 4.5, 0.0, 2.6, 3.6, 4.6, 0.0,
                2.7, 3.7, 4.7, 0.0, 0.0, 0.0, 0.0, 1.0],
            parm_value_2=[
                12.5, 13.5, 14.5, 0.0, 12.6, 13.6, 14.6,
                0.0, 12.7, 13.7, 14.7, 0.0, 0.0, 0.0, 0.0, 1.0])

    def test_connect_string(self):
        _pyflirt = import_pyflirt(compiler=self._compiler)
        self._connect_parm(
            parm_type=_pyflirt.ParmType.STRING,
            master_parm_name='my_master_parm',
            slave_parm_name='my_slave_parm',
            parm_value_1='my value is 42',
            parm_value_2='my value is 43')

    def test_connect_id(self):
        # _pyflirt = import_pyflirt(compiler=self._compiler)
        self._connect_id_parm(
            master_parm_name='my_master_parm',
            slave_parm_name='my_slave_parm',
            parm_value_1=self.source_1,
            parm_value_2=self.source_2)

    def test_connect_idset(self):
        _pyflirt = import_pyflirt(compiler=self._compiler)
        self._connect_parm(
            parm_type=_pyflirt.ParmType.IDSET,
            master_parm_name='my_master_parm',
            slave_parm_name='my_slave_parm',
            parm_value_1=[self.source_1, self.source_2],
            parm_value_2=[self.source_1])

    def test_connect_info(self):
        _pyflirt = import_pyflirt(compiler=self._compiler)
        master_parm_name = 'my_master_parm'
        slave_parm_name = 'my_slave_parm'
        priority = 3
        ###
        cnx = _pyflirt.connect_node_parameters(
            parm_type=_pyflirt.ParmType.INT,
            master_parm_name=master_parm_name,
            master=self.master,
            slave_parm_name=slave_parm_name,
            slave=self.slave,
            priority=priority)
        ###
        cnx_info = _pyflirt.get_connection_info(cnx=cnx)
        self.assertEquals(cnx_info['priority'], priority)
        self.assertEquals(cnx_info['master'], self.master)
        self.assertEquals(cnx_info['slave'], self.slave)
        self.assertEquals(cnx_info['master_parm_name'], master_parm_name)
        self.assertEquals(cnx_info['slave_parm_name'], slave_parm_name)
        ###
        _pyflirt.disconnect_node_parameters(cnx=cnx)
        ###
        cnx_info = _pyflirt.get_connection_info(cnx=cnx)
        self.assertEquals(cnx_info, {})

    def test_connect_priority(self):
        _pyflirt = import_pyflirt(compiler=self._compiler)
        parm_name = 'my_connected_parm'
        low_priority = 5
        high_priority = 50

        cnx = _pyflirt.connect_node_parameters(
            parm_type=_pyflirt.ParmType.INT,
            master_parm_name=parm_name,
            master=self.master,
            slave=self.slave,
            priority=low_priority)

        cnx_info = _pyflirt.get_connection_info(cnx=cnx)
        self.assertEquals(cnx_info['priority'], low_priority)
        ###
        _pyflirt.set_connection_priority(cnx=cnx, priority=high_priority)
        ###
        cnx_info = _pyflirt.get_connection_info(cnx=cnx)
        self.assertEquals(cnx_info['priority'], high_priority)

        _pyflirt.disconnect_node_parameters(cnx=cnx)
        ###
        _pyflirt.set_connection_priority(cnx=cnx, priority=low_priority)
        # should not crash, just do nothing
        ###

    # -- Insert tests in suite ------------------------------------------------
    @staticmethod
    def get_suite(compiler):
        suite = unittest.TestSuite()
        for member in dir(TestParameterConnections):
            if member.startswith('test_'):
                suite.addTest(
                    TestParameterConnections(
                        compiler=compiler, methodName=member))
        return suite
    # -------------------------------------------------------------------------
