# =========================================================================== #
# Copyright 2016-2018 SUPAMONKS_STUDIO                                        #
# Author: Pierre-Edouard Landes <pel@supamonks.com>                           #
#                                                                             #
# This program is free software: you can redistribute it and/or modify        #
# it under the terms of the GNU Lesser General Public License as              #
# published by the Free Software Foundation, either version 3 of the          #
# License, or (at your option) any later version.                             #
#                                                                             #
# This program is distributed in the hope that it will be useful,             #
# but WITHOUT ANY WARRANTY; without even the implied warranty of              #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               #
# GNU Lesser General Public License for more details.                         #
#                                                                             #
# You should have received a copy of the GNU Lesser General Public License    #
# along with this program.  If not, see <https://www.gnu.org/licenses/>.      #
#                                                                             #
# =========================================================================== #
import unittest
import json
from import_pyflirt import import_pyflirt


class _EventRecorder:
    def __init__(self):
        self.history = []

    def handle(self, type, dict_info):
        all_info = dict_info
        all_info['event'] = type
        self.history.append(all_info)

    def flush(self):
        ret = self.history
        self.history = []
        return ret


class TestNodeMethods(unittest.TestCase):

    def __init__(self, compiler, methodName='runTest'):
        super(TestNodeMethods, self).__init__(methodName)
        self._compiler = compiler

    def setUp(self):
        module_cfg = json.dumps({
            'with_viewport': False,
            'cache_anim': False,
            'num_threads': 1
        })
        _pyflirt = import_pyflirt(compiler=self._compiler)
        _pyflirt.initialize(cfg=module_cfg)

        self.parent = _pyflirt.create_data_node(
            name='parent', parent=_pyflirt.get_scene())
        self.recorder = _EventRecorder()
        self.obs = _pyflirt.register_scene_observer(func=self.recorder.handle)

    def tearDown(self):
        _pyflirt = import_pyflirt(compiler=self._compiler)
        self.recorder.flush()
        _pyflirt.deregister_observer(obs=self.obs)
        _pyflirt.destroy_data_node(node=self.parent)
        _pyflirt.finalize()

    def test_data_node_creation(self):
        _pyflirt = import_pyflirt(compiler=self._compiler)
        node_name = 'my_node'
        node_full_name = '/parent/%s' % node_name
        ###
        node = _pyflirt.create_data_node(name=node_name, parent=self.parent)
        ###
        self.assertGreater(node, 0)

        ret_name = _pyflirt.get_node_name(node=node)
        ret_full_name = _pyflirt.get_node_full_name(node=node)
        ret_parent = _pyflirt.get_node_parent(node=node)
        ret_class = _pyflirt.get_node_class(node=node)
        self.assertEquals(ret_name, node_name)
        self.assertEquals(ret_full_name, node_full_name)
        self.assertEquals(ret_class, _pyflirt.NodeClass.ANY)
        self.assertEquals(ret_parent, self.parent)

        parent_num_children = _pyflirt.get_node_num_children(node=ret_parent)
        self.assertEquals(parent_num_children, 1)
        parent_only_child = _pyflirt.get_node_child(node=ret_parent, idx=0)
        self.assertEquals(parent_only_child, node)
        ###
        _pyflirt.destroy_data_node(node=node)
        ###

        self.assertIsNone(_pyflirt.get_node_name(node=node))
        self.assertIsNone(_pyflirt.get_node_full_name(node=node))
        self.assertEquals(_pyflirt.get_node_parent(node=node), 0)
        self.assertEquals(
            _pyflirt.get_node_class(node=node),
            _pyflirt.NodeClass.NONE)
        self.assertEquals(_pyflirt.get_node_num_children(node=self.parent), 0)
        self.assertEquals(_pyflirt.get_node_child(node=self.parent, idx=0), 0)

    def test_data_node_creation_event(self):
        _pyflirt = import_pyflirt(compiler=self._compiler)
        self.recorder.flush()
        ###
        node_name = 'my_node'
        node = _pyflirt.create_data_node(name=node_name, parent=self.parent)
        ###
        history = self.recorder.flush()
        self.assertEquals(len(history), 1)
        self.assertEquals(
            history[0],
            {
                'event': _pyflirt.SceneEvent.NODE_CREATED,
                'node': node,
                'parent': self.parent
            })

        ###
        _pyflirt.destroy_data_node(node=node)
        ###
        history = self.recorder.flush()
        self.assertEquals(len(history), 1)
        self.assertEquals(
            history[0],
            {
                'event': _pyflirt.SceneEvent.NODE_DELETED,
                'node': node,
                'parent': self.parent
            })

    # -- Insert tests in suite ------------------------------------------------
    @staticmethod
    def get_suite(compiler):
        suite = unittest.TestSuite()
        for member in dir(TestNodeMethods):
            if member.startswith('test_'):
                suite.addTest(
                    TestNodeMethods(compiler=compiler, methodName=member))
        return suite
    # -------------------------------------------------------------------------
