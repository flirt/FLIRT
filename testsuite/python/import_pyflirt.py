# =========================================================================== #
# Copyright 2016-2018 SUPAMONKS_STUDIO                                        #
# Author: Pierre-Edouard Landes <pel@supamonks.com>                           #
#                                                                             #
# This program is free software: you can redistribute it and/or modify        #
# it under the terms of the GNU Lesser General Public License as              #
# published by the Free Software Foundation, either version 3 of the          #
# License, or (at your option) any later version.                             #
#                                                                             #
# This program is distributed in the hope that it will be useful,             #
# but WITHOUT ANY WARRANTY; without even the implied warranty of              #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               #
# GNU Lesser General Public License for more details.                         #
#                                                                             #
# You should have received a copy of the GNU Lesser General Public License    #
# along with this program.  If not, see <https://www.gnu.org/licenses/>.      #
#                                                                             #
# =========================================================================== #
import sys
import os


def import_pyflirt(compiler):
    if '_pyflirt' in sys.modules:
        return sys.modules['_pyflirt']

    cfg = 'Debug' if hasattr(sys, 'gettotalrefcount') else 'Release'
    path = os.path.join(
        os.path.dirname(__file__),
        '..',
        '..',
        '_pyflirt',
        compiler,
        'bin',
        cfg)
    path = os.path.abspath(path)
    sys.path.insert(0, path)

    print '-- import _pyflirt from "{:s}"'.format(path)
    import _pyflirt
    return sys.modules['_pyflirt']
