# =========================================================================== #
# Copyright 2016-2018 SUPAMONKS_STUDIO                                        #
# Author: Pierre-Edouard Landes <pel@supamonks.com>                           #
#                                                                             #
# This program is free software: you can redistribute it and/or modify        #
# it under the terms of the GNU Lesser General Public License as              #
# published by the Free Software Foundation, either version 3 of the          #
# License, or (at your option) any later version.                             #
#                                                                             #
# This program is distributed in the hope that it will be useful,             #
# but WITHOUT ANY WARRANTY; without even the implied warranty of              #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               #
# GNU Lesser General Public License for more details.                         #
#                                                                             #
# You should have received a copy of the GNU Lesser General Public License    #
# along with this program.  If not, see <https://www.gnu.org/licenses/>.      #
#                                                                             #
# =========================================================================== #
import sys
import argparse
import unittest
from test_node_methods import TestNodeMethods
from test_parameter_methods import TestParameterMethods
from test_event_callbacks import TestEventCallbacks
from test_parameter_connections import TestParameterConnections


def run_tests(compiler):
    suites = [
        TestNodeMethods.get_suite(compiler=compiler),
        TestParameterMethods.get_suite(compiler=compiler),
        TestEventCallbacks.get_suite(compiler=compiler),
        TestParameterConnections.get_suite(compiler=compiler)
    ]
    all_tests = unittest.TestSuite(suites)
    return unittest.TextTestRunner(verbosity=2).run(all_tests)  # return report


def _run_tests(argv):
    # parse arguments and pass them to main function
    parser = argparse.ArgumentParser(description='FLIRT Module Testing')

    parser.add_argument(
        '-c', '--compiler',
        required=True, type=str,
        help='compiler used to build FLIRT module')
    parser.add_argument(
        '--pause',
        dest='pause', action='store_true',
        help='pause before running tests (useful for attaching debugger)')
    parser.set_defaults(pause=False)

    args = parser.parse_args()

    if args.pause:
        raw_input('-- attach debugger if you care...')
    run_tests(compiler=args.compiler)


if __name__ == '__main__':
    try:
        _run_tests(argv=sys.argv)
    except Exception as e:
        print 'ERROR\n{:s}'.format(e.message)
        raise
