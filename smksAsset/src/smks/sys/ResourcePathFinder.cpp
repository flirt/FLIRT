// ========================================================================= //
// Copyright 2016-2018 SUPAMONKS_STUDIO                                      //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
//                                                                           //
// ========================================================================= //
#include <iostream>
#include <string>
#include <vector>
#include <fstream>
#include <sstream>

#include <boost/filesystem.hpp>
#include <smks/sys/Log.hpp>
#include <smks/util/string/getId.hpp>
#include "smks/sys/ResourcePathFinder.hpp"

namespace smks { namespace sys
{
    //////////////////////////////////
    // class ResourcePathFinder::PImpl
    //////////////////////////////////
    class ResourcePathFinder::PImpl
    {
    private:
        std::vector<boost::filesystem::path> _includes;

    public:
        inline
        PImpl(const std::string& argv0):
            _includes()
        {
            _includes.reserve(8);
            if (!argv0.empty())
            {
                const boost::filesystem::path& path = boost::filesystem::system_complete(argv0).branch_path();
                addIncludeDirectory(path.string());
            }
        }

        inline
        bool
        addIncludeDirectory(const std::string&);

        inline
        bool
        resolveFileName(const std::string&,
                        std::string& resolved,
                        std::string& dir,
                        std::string& baseName,
                        std::string& ext) const;
    };
}
}

using namespace smks;

//////////////////////////////////
// class ResourcePathFinder::Pimpl
//////////////////////////////////
bool
sys::ResourcePathFinder::PImpl::addIncludeDirectory(const std::string& dir)
{
    bool validDir = false;
    boost::filesystem::path d(dir);

    try
    {
        validDir = boost::filesystem::exists(d) &&
            boost::filesystem::is_directory(d);
    }
    catch(const std::exception& e)
    {
        std::stringstream sstr;
        sstr
            << "Failed to assess validity of directory '" << dir << "' "
            << "with boost::filesystem.\n\n" << e.what();
        std::cerr << sstr.str() << std::endl;
        FLIRT_LOG(LOG(DEBUG) << sstr.str();)
        validDir = false;
    }

    if (validDir)
        _includes.push_back(d);

    return validDir;
}

bool
sys::ResourcePathFinder::PImpl::resolveFileName(const std::string&  fileName,
                                                std::string&        resolved,
                                                std::string&        directory,
                                                std::string&        baseName,
                                                std::string&        ext) const
{
    resolved    .clear();
    directory   .clear();
    baseName    .clear();
    ext         .clear();

    if (fileName.empty())
        return false;

    boost::filesystem::path f(fileName);

    if (boost::filesystem::exists(f) &&
        boost::filesystem::is_regular_file(f))
    {
        boost::filesystem::path refPath = boost::filesystem::canonical(f);
        refPath.make_preferred();

        boost::filesystem::path path = refPath;

        resolved    = refPath.string();
        ext         = refPath.extension().string();
        baseName    = path.leaf().replace_extension(boost::filesystem::path()).string();
        path        = refPath;
        directory   = path.remove_filename().string();

        return true;
    }

    for (size_t i = 0; i < _includes.size(); ++i)
    {
        f = _includes[i] / boost::filesystem::path(fileName);
        if (boost::filesystem::exists(f) &&
            boost::filesystem::is_regular_file(f))
        {
            boost::filesystem::path refPath = boost::filesystem::canonical(f);
            refPath.make_preferred();

            boost::filesystem::path path = refPath;

            resolved    = refPath.string();
            ext         = refPath.extension().string();
            baseName    = path.leaf().replace_extension(boost::filesystem::path()).string();
            path        = refPath;
            directory   = path.remove_filename().string();

            return true;
        }
    }

    return false;
}

///////////////////////////
// class ResourcePathFinder
///////////////////////////
sys::ResourcePathFinder::ResourcePathFinder(const char* argv0):
    _pImpl(new PImpl(argv0))
{ }

sys::ResourcePathFinder::~ResourcePathFinder()
{
    delete _pImpl;
}

bool
sys::ResourcePathFinder::addIncludeDirectory(const char* dir)
{
    return dir
        ? _pImpl->addIncludeDirectory(dir)
        : false;
}

size_t
sys::ResourcePathFinder::resolveFileName(const char* fileName,
                                         char*       buffer,
                                         size_t      bufferSize) const
{
    size_t size = 0;

    if (fileName    == nullptr ||
        buffer      == nullptr ||
        bufferSize  == 0)
        return size;

    std::string resolved, directory, baseName, ext;

    if (_pImpl->resolveFileName(std::string(fileName), resolved, directory, baseName, ext))
    {
        size = resolved.size();
        if (size > bufferSize - 1)  // must include null termination character
            size = bufferSize - 1;
        if (size > 0)
            memcpy(buffer, resolved.c_str(), sizeof(char) * (size + 1));
    }

    return size;
}
