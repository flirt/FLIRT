// ========================================================================= //
// Copyright 2016-2018 SUPAMONKS_STUDIO                                      //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
//                                                                           //
// ========================================================================= //
#include <string>
#include <fstream>
#include <sstream>

#include <smks/sys/Log.hpp>
#include "smks/sys/TextResource.hpp"

namespace smks { namespace sys
{
    class TextResource::PImpl
    {
    private:
        std::string _text;

    public:
        inline
        PImpl(const char* filePath, void*):
            _text()
        {
            FLIRT_LOG(LOG(DEBUG) << "creating new text resource with resolved filename = '" << filePath << "'.";)
            std::ifstream   stream(filePath);
            std::string     line;

            while(stream.good())
            {
                std::getline(stream, line);
                _text += line + "\n";
            }
            stream.close();
        }

        inline
        const char*
        text() const
        {
            return _text.c_str();
        }
    };
}
}

using namespace smks;

sys::TextResource::TextResource(const char* filePath, void* args):
    ResourceBase(filePath, args),
    _pImpl      (new PImpl(filePath, args))
{ }

sys::TextResource::~TextResource()
{
    delete _pImpl;
}

const char*
sys::TextResource::text() const
{
    return _pImpl->text();
}
