// ========================================================================= //
// Copyright 2016-2018 SUPAMONKS_STUDIO                                      //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
//                                                                           //
// ========================================================================= //
#include <smks/sys/Log.hpp>
#include <smks/img/Image.hpp>
#include "smks/sys/ImageResource.hpp"

namespace smks { namespace sys
{
    class ImageResource::PImpl
    {
    private:
        img::AbstractImage::Ptr _image;
    public:
        inline
        PImpl(const char* filePath, void*):
            _image(img::Image::create(filePath))
        {
            FLIRT_LOG(LOG(DEBUG)
                << "creating new image resource "
                << "with resolved filename '" << filePath << "'.";)
        }

        inline
        img::AbstractImage::Ptr const&
        image() const
        {
            return _image;
        }
    };
}
}

using namespace smks;

sys::ImageResource::ImageResource(const char* filePath, void* args):
    ResourceBase(filePath, args),
    _pImpl      (new PImpl(filePath, args))
{ }

sys::ImageResource::~ImageResource()
{
    delete _pImpl;
}

const img::AbstractImage*
sys::ImageResource::image() const
{
    return _pImpl->image().get();
}
