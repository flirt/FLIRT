// ========================================================================= //
// Copyright 2016-2018 SUPAMONKS_STUDIO                                      //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
//                                                                           //
// ========================================================================= //
#include <string>
#include <boost/unordered_map.hpp>

#include <smks/sys/Log.hpp>
#include <smks/sys/sync/Mutex.hpp>
#include <smks/sys/Exception.hpp>
#include <smks/sys/Log.hpp>

#include <smks/util/string/getId.hpp>

#include "smks/sys/ResourceDatabase.hpp"
#include "smks/sys/ResourcePathFinder.hpp"

namespace smks { namespace sys
{
    class ResourceDatabase::PImpl
    {
    private:

    private:
        typedef boost::unordered_map<unsigned int, DatabaseResourceEntry*>  ResourceEntryMap;
        typedef boost::unordered_map<unsigned int, DatabaseGenericEntry*>   GenericEntryMap;

    private:
        const std::string           _name;
        ResourcePathFinder::Ptr     _pathFinder;
        ResourceEntryMap            _resourceEntries;
        GenericEntryMap             _genericEntries;
        mutable sys::sync::MutexSys _mutex;

    public:
        explicit inline
        PImpl(const char* name):
            _name           (name),
            _pathFinder     (nullptr),
            _resourceEntries(),
            _genericEntries ()
        { }

        inline
        ~PImpl()
        {
            releaseAll();
        }

    private:
        // non-copyable
        PImpl(const PImpl&);
        PImpl& operator=(const PImpl&);

    public:
        inline
        bool
        unload(unsigned int id)
        {
            FLIRT_LOG(LOG(DEBUG) << "unloading resource ID = " << id;)
            //---------------------------------
            sys::sync::Lock<sys::sync::MutexSys> lock(_mutex); // unlocked when out of scope
            //---------------------------------
            ResourceEntryMap::iterator it = _resourceEntries.find(id);

            if (it != _resourceEntries.end())
            {
                if (it->second == nullptr ||
                    it->second->resourceBase() == nullptr)
                    _resourceEntries.erase(it);

                it->second->resourceBase()->decrReferenceCount();
                if (it->second->resourceBase()->referenceCount())
                {
                    delete it->second;
                    _resourceEntries.erase(it);
                }

                return true;
            }

            return false;
        }

        inline
        const std::string&
        name() const
        {
            return _name;
        }

        inline
        void
        setSharedPathFinder(ResourcePathFinder::Ptr const& value)
        {
            _pathFinder = value;
        }

        ResourcePathFinder::Ptr const&
        getOrCreatePathFinder()
        {
            if (!_pathFinder)
                _pathFinder = ResourcePathFinder::create();
            return _pathFinder;
        }

        inline
        DatabaseResourceEntry*
        findResourceEntry(const char* resolved)
        {
            const unsigned int id = getResourceId(resolved);
            if (id == 0)
                return nullptr;

            //---------------------------------
            sys::sync::Lock<sys::sync::MutexSys> lock(_mutex); // unlocked when out of scope
            //---------------------------------
            ResourceEntryMap::iterator const& it = _resourceEntries.find(id);

            if (it == _resourceEntries.end())
                return nullptr;
            if (it->second == nullptr ||
                it->second->resourceBase() == nullptr)
                throw Exception("Invalid entry.", "Resource Manager Entry Finder");

            const char* entryFileName = it->second->resourceBase()->fileName();
            if (strcmp(entryFileName, resolved) != 0)
            {
                std::stringstream sstr;
                sstr
                    << "ID collision in manager '" << _name << "' "
                    << "between entries '" << entryFileName << "' "
                    << "and '" << resolved << "'.",

                throw Exception(sstr.str().c_str(), "Resource Manager Entry Finder");
            }

            return it->second;
        }

        inline
        void
        storeResourceEntry(const char* resolved, DatabaseResourceEntry* entry)
        {
            if (entry)
            {
                //---------------------------------
                sys::sync::Lock<sys::sync::MutexSys> lock(_mutex); // unlocked when out of scope
                //---------------------------------
                _resourceEntries.insert(std::pair<unsigned int, DatabaseResourceEntry*>(getResourceId(resolved), entry));
            }
        }

        inline
        const sys::DatabaseGenericEntry*
        findGenericEntry(unsigned int id) const
        {
            GenericEntryMap::const_iterator const& it = _genericEntries.find(id);

            return it != _genericEntries.end()
                ? it->second
                : nullptr;
        }

        inline
        sys::DatabaseGenericEntry*
        findGenericEntry(unsigned int id)
        {
            GenericEntryMap::iterator const& it = _genericEntries.find(id);

            return it != _genericEntries.end()
                ? it->second
                : nullptr;
        }

        inline
        unsigned int
        storeGenericEntry(const char* alias, DatabaseGenericEntry* obj)
        {
            if (alias == nullptr || strlen(alias) == 0 || obj == nullptr)
                return 0;

            const unsigned int id = getResourceId(alias);

            drop(id);
            _genericEntries.insert(std::pair<unsigned int, DatabaseGenericEntry*>(id, obj));

            return id;
        }

        inline
        void
        drop(unsigned int id)
        {
            GenericEntryMap::iterator const& it = _genericEntries.find(id);

            if (it != _genericEntries.end())
            {
                delete it->second;
                _genericEntries.erase(it);
            }
        }

    private:
        inline
        void
        releaseAll()
        {
            //---------------------------------
            sys::sync::Lock<sys::sync::MutexSys> lock(_mutex); // unlocked when out of scope
            //---------------------------------
            ResourceEntryMap::iterator it = _resourceEntries.begin();

            while (it != _resourceEntries.end())
            {
                delete it->second;
                it = _resourceEntries.erase(it);
            }
        }
    };
}
}

using namespace smks;

sys::ResourceDatabase::ResourceDatabase(const char* name):
    _pImpl(new PImpl(name))
{ }

sys::ResourceDatabase::~ResourceDatabase()
{
    delete _pImpl;
}

bool
sys::ResourceDatabase::unload(unsigned int id)
{
    return _pImpl->unload(id);
}

const char*
sys::ResourceDatabase::name() const
{
    return _pImpl->name().c_str();
}

void
sys::ResourceDatabase::setSharedPathFinder(ResourcePathFinder::Ptr const& value)
{
    _pImpl->setSharedPathFinder(value);
}

sys::ResourcePathFinder::Ptr const&
sys::ResourceDatabase::getOrCreatePathFinder()
{
    return _pImpl->getOrCreatePathFinder();
}

sys::DatabaseResourceEntry*
sys::ResourceDatabase::findResourceEntry(const char* resolved)
{
    return _pImpl->findResourceEntry(resolved);
}

void
sys::ResourceDatabase::storeResourceEntry(const char* resolved, DatabaseResourceEntry* resource)
{
    _pImpl->storeResourceEntry(resolved, resource);
}

const sys::DatabaseGenericEntry*
sys::ResourceDatabase::findGenericEntry(unsigned int id) const
{
    return _pImpl->findGenericEntry(id);
}

sys::DatabaseGenericEntry*
sys::ResourceDatabase::findGenericEntry(unsigned int id)
{
    return _pImpl->findGenericEntry(id);
}

unsigned int
sys::ResourceDatabase::storeGenericEntry(const char* alias, DatabaseGenericEntry* obj)
{
    return _pImpl->storeGenericEntry(alias, obj);
}

void
sys::ResourceDatabase::drop(unsigned int id)
{
    _pImpl->drop(id);
}

// static
unsigned int
sys::ResourceDatabase::getResourceId(const char* str)
{
    return util::string::getId(str);
}
