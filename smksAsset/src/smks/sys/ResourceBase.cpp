// ========================================================================= //
// Copyright 2016-2018 SUPAMONKS_STUDIO                                      //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
//                                                                           //
// ========================================================================= //
#include <string>
#include <smks/util/string/getId.hpp>
#include "smks/sys/ResourceBase.hpp"

namespace smks
{
    namespace sys
    {
        ////////////////////////////
        // class ResourceBase::PImpl
        ////////////////////////////
        class ResourceBase::PImpl
        {
        private:
            size_t              _referenceCount;
            const std::string   _fileName;
            const unsigned int  _id;

        public:
            PImpl(const std::string& fileName):
                _referenceCount (0),
                _fileName       (fileName),
                _id             (util::string::getId(_fileName.c_str()))
            { }

            inline
            const char*
            fileName() const
            {
                return _fileName.c_str();
            }

            inline
            unsigned int
            id() const
            {
                return _id;
            }

            inline
            size_t
            referenceCount() const
            {
                return _referenceCount;
            }

            inline
            void
            incrReferenceCount()
            {
                ++_referenceCount;
            }

            inline
            void
            decrReferenceCount()
            {
                --_referenceCount;
            }
        };
    }
}

using namespace smks;


/////////////////////
// class ResourceBase
/////////////////////
sys::ResourceBase::ResourceBase(const char* fileName, void* args):
    _pImpl(new PImpl(fileName))
{ }

// virtual
sys::ResourceBase::~ResourceBase()
{
    delete _pImpl;
}

const char*
sys::ResourceBase::fileName() const
{
    return _pImpl->fileName();
}

unsigned int
sys::ResourceBase::id() const
{
    return _pImpl->id();
}

size_t
sys::ResourceBase::referenceCount() const
{
    return _pImpl->referenceCount();
}

void
sys::ResourceBase::incrReferenceCount()
{
    _pImpl->incrReferenceCount();
}

void
sys::ResourceBase::decrReferenceCount()
{
    _pImpl->decrReferenceCount();
}
