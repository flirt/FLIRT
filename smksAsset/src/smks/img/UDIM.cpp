// ========================================================================= //
// Copyright 2016-2018 SUPAMONKS_STUDIO                                      //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
//                                                                           //
// ========================================================================= //
#include <cassert>
#include <string>
#include <smks/math/math.hpp>
#include <smks/math/matrixAlgo.hpp>
#include "smks/img/UDIM.hpp"

namespace smks { namespace img
{
    static const std::string UDIM_TAG = "<UDIM>";
}
}

using namespace smks;

bool
img::isUDIMFilePath(const char* filePath)
{
    if (filePath == nullptr || strlen(filePath) == 0)
        return false;

    std::string                     filePath_   (filePath);
    const std::string::size_type    posUDIM     = filePath_.find(UDIM_TAG);

    return posUDIM != std::string::npos;
}

size_t
img::getUDIMFilePath(const char* filePath, char* buffer, size_t bufferSize)
{
    if (filePath == nullptr || strlen(filePath) == 0 ||
        bufferSize == 0 || buffer == nullptr)
        return false;

    std::string                     ret     (filePath);
    const std::string::size_type    posUDIM = ret.find(UDIM_TAG);

    if (posUDIM == std::string::npos)
        return 0;

    ret.replace(posUDIM, UDIM_TAG.size(), "%04d", strlen("%04d"));
    size_t num = ret.size() + 1;
    if (bufferSize < num)
        num = bufferSize;
    memcpy(buffer, ret.c_str(), sizeof(char) * num);

    return num;
}

size_t
img::getUDIMIndex(const math::Vector2f& UV, math::Vector2f& localUV)
{
    const math::Vector2f& iUV = math::floor(UV);
    localUV = UV - iUV;

    return getUDIMIndex(math::Vector2i(static_cast<int>(iUV.x()), static_cast<int>(iUV.y())));
}

size_t
img::getUDIMIndex(const math::Vector2i& uv)
{
    assert(uv.x() >= 0 && uv.y() >= 0 && uv.x() < MAX_UDIM);
    return 1000 + uv.x() + 1 + MAX_UDIM * uv.y();
}
