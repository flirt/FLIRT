// ========================================================================= //
// Copyright 2016-2018 SUPAMONKS_STUDIO                                      //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
//                                                                           //
// ========================================================================= //
#include <smks/sys/sync/Mutex.hpp>
#include <smks/img/AbstractImage.hpp>
#include "smks/sys/ResourceDatabase.hpp"
#include "smks/sys/ImageResource.hpp"
#include "smks/xchg/Image.hpp"

namespace smks { namespace xchg
{
    class Image::PImpl
    {
    private:
        sys::sync::MutexSys         _mutex;
        const std::string           _filePath;
        sys::ResourceDatabase::WPtr _resources;
        const sys::ImageResource*   _resource;  // owned by resource database
    public:
        inline
        PImpl(const std::string& filePath, sys::ResourceDatabase::WPtr const& rsrc):
            _filePath   (filePath),
            _resources  (rsrc),
            _resource   (nullptr)
        {
            if (_filePath.empty())
                throw sys::Exception(
                    "Empty file path.",
                    "Exchangeable Image Construction");

            sys::ResourceDatabase::Ptr const& resources = _resources.lock();
            if (!resources)
            {
                std::stringstream sstr;
                sstr
                    << "Invalid resources when creating '" << filePath << "' image.";
                throw sys::Exception(sstr.str().c_str(), "Exchangeable Image Construction");
            }
        }

        inline
        const std::string&
        filePath() const
        {
            return _filePath;
        }

        inline
        const img::AbstractImage*
        getImage()
        {
            if (_resource && _resource->image())
                return _resource->image();

            // image needs to be loaded
            sys::ResourceDatabase::Ptr const& resources = _resources.lock();
            if (!resources)
                return nullptr;
            //------------------------------------------------
            sys::sync::Lock<sys::sync::MutexSys> lock(_mutex);
            //------------------------------------------------
            _resource = resources->load<sys::ImageResource>(_filePath.c_str(), nullptr);

            if (_resource &&
                _resource->image() &&
                _resource->image()->numBytes() > 0)
                return _resource->image();
            else
            {
                _resource = nullptr;
                return nullptr;
            }
        }
    };
}
}

using namespace smks;

xchg::Image::Image(const char* filePath, sys::ResourceDatabase::WPtr const& resources):
    _pImpl(new PImpl(filePath, resources))
{ }

xchg::Image::~Image()
{
    delete _pImpl;
}

const char*
xchg::Image::filePath() const
{
    return _pImpl->filePath().c_str();
}

const img::AbstractImage*
xchg::Image::getImage()
{
    return _pImpl->getImage();
}
