// ========================================================================= //
// Copyright 2016-2018 SUPAMONKS_STUDIO                                      //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
//                                                                           //
// ========================================================================= //
#include <boost/unordered_map.hpp>

#include <smks/sys/sync/Mutex.hpp>
#include <smks/sys/Log.hpp>
#include <smks/sys/Exception.hpp>
#include <smks/sys/ResourceDatabase.hpp>
#include <smks/sys/ResourceDatabase.hpp>

#include "smks/img/UDIM.hpp"
#include "smks/xchg/Image.hpp"
#include "smks/xchg/TileImages.hpp"

namespace smks { namespace xchg
{
    class TileImages::PImpl
    {
    private:
        typedef std::weak_ptr<sys::ResourceDatabase>        ResourceDatabaseWPtr;
        typedef boost::unordered_map<size_t, Image::Ptr>    UDimToTile;
    private:
        std::string                 _formattedFilePath;
        const ResourceDatabaseWPtr  _resources;
        UDimToTile                  _tiles; //<! UDIM -> image

        sys::sync::MutexSys         _mutex;
    public:
        inline
        PImpl(const char*                   filePath,
              ResourceDatabasePtr const&    resources):
            _formattedFilePath  (),
            _resources          (resources),
            _tiles              ()
        {
            const size_t    BUFFER_SIZE = 512;
            char            BUFFER      [BUFFER_SIZE];

            if (filePath == nullptr)
                throw sys::Exception(
                    "Invalid file path.",
                    "Tile Image Map Construction");

            if (img::getUDIMFilePath(filePath, BUFFER, BUFFER_SIZE))
                _formattedFilePath = std::string(BUFFER);
            else
            {
                std::stringstream sstr;
                sstr
                    << "'" << filePath << "' is not UDIM compatible.";
                throw sys::Exception(sstr.str().c_str(), "Tile Image Map Construction");
            }

            FLIRT_LOG(LOG(DEBUG)
                << "tile map created with formatted file path '" << _formattedFilePath << "' "
                << "from '" << filePath << "'";)
        }

        inline
        const std::string&
        formattedFilePath() const
        {
            return _formattedFilePath;
        }

        inline
        const img::AbstractImage*
        getImage(const math::Vector2f& UV, math::Vector2f& localUV)
        {
            return getImage(img::getUDIMIndex(UV, localUV));
        }

        inline
        const img::AbstractImage*
        getImage(const math::Vector2i& udim)
        {
            return getImage(img::getUDIMIndex(udim));
        }

        inline
        bool
        tileExists(const math::Vector2i& udim) const
        {
            sys::ResourceDatabase::Ptr const& resources = _resources.lock();
            if (!resources)
                return false;

            std::string     fileName;

            formatFileName(img::getUDIMIndex(udim), fileName);

            const size_t    BUFFER_SIZE = 512;
            char            BUFFER      [BUFFER_SIZE];

            return resources->getOrCreatePathFinder()->resolveFileName(fileName.c_str(), BUFFER, BUFFER_SIZE);
        }

    private:
        inline
        const img::AbstractImage*
        getImage(const size_t udim)
        {
            UDimToTile::const_iterator it = _tiles.find(udim);
            if (it != _tiles.end())
                return it->second->getImage();

            // need to load image from UDIM collection
            //------------------------------------------------
            sys::sync::Lock<sys::sync::MutexSys> lock(_mutex);  // unlocked when out of scope
            //------------------------------------------------
            if (_tiles.find(udim) == _tiles.end()) // double check locking
            {
                std::string fileName;

                formatFileName(udim, fileName);
                _tiles.insert(std::pair<size_t, Image::Ptr>(udim, Image::create(fileName.c_str(), _resources)));
            }
            return _tiles.find(udim)->second->getImage();
        }

        inline
        void
        formatFileName(const size_t udim, std::string& ret) const
        {
            ret.clear();
            if (_formattedFilePath.empty())
                return;

            char* str = new char[_formattedFilePath.size() + 1];

            sprintf(str, _formattedFilePath.c_str(), udim);
            ret = std::string(str);
            delete[] str;
        }
    };
}
}

using namespace smks;

xchg::TileImages::TileImages(const char* filePath, ResourceDatabasePtr const& resources):
    _pImpl(new PImpl(filePath, resources))
{ }

xchg::TileImages::~TileImages()
{
    delete _pImpl;
}

const char* //<! resolved, formatted file path template used to find texture image tile files
xchg::TileImages::formattedFilePath() const
{
    return _pImpl->formattedFilePath().c_str();
}

const img::AbstractImage*
xchg::TileImages::getImage(const math::Vector2i& udim)
{
    return _pImpl->getImage(udim);
}

const img::AbstractImage*
xchg::TileImages::getImage(const math::Vector2f& UV, math::Vector2f& localUV)
{
    return _pImpl->getImage(UV, localUV);
}

bool
xchg::TileImages::tileExists(const math::Vector2i& udim) const
{
    return _pImpl->tileExists(udim);
}
