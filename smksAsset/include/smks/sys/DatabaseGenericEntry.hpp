// ========================================================================= //
// Copyright 2016-2018 SUPAMONKS_STUDIO                                      //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
//                                                                           //
// ========================================================================= //
#pragma once

#include <typeinfo>
#include "smks/sys/ResourceBase.hpp"

namespace smks { namespace sys
{
    class DatabaseGenericEntry
    {
        ///////////////////////
        // class AbstractHolder
        ///////////////////////
    private:
        class AbstractHolder
        {
        public:
            virtual inline
            ~AbstractHolder()
            { }

            virtual
            const std::type_info&
            type() const = 0;
        };
        ///////////////////////

    private:
        AbstractHolder* _content;

        //////////////////////////////
        // class Holder <ObjectType>
        //////////////////////////////
    private:
        template<typename ObjectType>
        class Holder:
            public AbstractHolder
        {
        public:
            ObjectType held;    //<! owns it
        public:
            explicit inline
            Holder(const ObjectType& held):
                held(held)
            { }

            inline
            const std::type_info&
            type() const
            {
                return typeid(ObjectType);
            }
        };
        //////////////////////////////

    public:
        template<typename ObjectType>
        DatabaseGenericEntry(const ObjectType& held):
            _content(new Holder<ObjectType>(held))
        { }
    private:
        // non-copyable
        DatabaseGenericEntry(const DatabaseGenericEntry&);
        DatabaseGenericEntry& operator=(const DatabaseGenericEntry&);

    public:
        inline
        ~DatabaseGenericEntry()
        {
            delete _content;
        }

        inline
        const std::type_info&
        type() const
        {
            return _content->type();
        }

        template<typename ObjectType> static inline
        const ObjectType*
        cast(const DatabaseGenericEntry* entry)
        {
            return entry->type() == typeid(ObjectType)
                ? &static_cast<Holder<ObjectType> *>(entry->_content)->held
                : nullptr;
        }

        template<typename ObjectType> static inline
        ObjectType*
        cast(DatabaseGenericEntry* entry)
        {
            return entry->type() == typeid(ObjectType)
                ? &static_cast<Holder<ObjectType> *>(entry->_content)->held
                : nullptr;
        }
    };
}
}
