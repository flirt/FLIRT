// ========================================================================= //
// Copyright 2016-2018 SUPAMONKS_STUDIO                                      //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
//                                                                           //
// ========================================================================= //
#pragma once

#include <memory>
#include "smks/sys/configure_asset.hpp"

namespace smks { namespace sys
{
    class FLIRT_ASSET_EXPORT ResourcePathFinder
    {
    public:
        typedef std::shared_ptr<ResourcePathFinder> Ptr;

    private:
        class PImpl;
    private:
        PImpl* _pImpl;

    public:
        static inline
        Ptr
        create(const char* argv0="")
        {
            Ptr ptr(new ResourcePathFinder(argv0));
            return ptr;
        }

    public:
        explicit
        ResourcePathFinder(const char*);
    public:
        ~ResourcePathFinder();

    private:
        // non-copyable
        ResourcePathFinder(const ResourcePathFinder&);
        ResourcePathFinder& operator=(const ResourcePathFinder&);

    public:
        bool
        addIncludeDirectory(const char*);

        size_t  // returns the number of characters of the resolved filename (= 0 if failed)
        resolveFileName(const char*, char* buffer, size_t bufferSize) const;
    };
}
}
