// ========================================================================= //
// Copyright 2016-2018 SUPAMONKS_STUDIO                                      //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
//                                                                           //
// ========================================================================= //
#pragma once

#include "smks/sys/configure_asset.hpp"

namespace smks { namespace sys
{
    class FLIRT_ASSET_EXPORT ResourceBase
    {
        friend class ResourceDatabase;  // for incr/decr reference count
    private:
        class PImpl;
    private:
        PImpl* _pImpl;

    public:
        ResourceBase(const char*, void* args);

        virtual
        ~ResourceBase();

    private:
        // non-copyable
        ResourceBase(const ResourceBase&);
        ResourceBase& operator=(const ResourceBase&);

    public:
        const char*
        fileName() const;

        size_t
        referenceCount() const;

        unsigned int
        id() const;

    private:
        void
        incrReferenceCount();

        void
        decrReferenceCount();
    };
}
}
