// ========================================================================= //
// Copyright 2016-2018 SUPAMONKS_STUDIO                                      //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
//                                                                           //
// ========================================================================= //
#pragma once

#include <typeinfo>
#include "smks/sys/ResourceBase.hpp"

namespace smks { namespace sys
{
    class DatabaseResourceEntry
    {
        ///////////////////////
        // class AbstractHolder
        ///////////////////////
    private:
        class AbstractHolder
        {
        public:
            virtual inline
            ~AbstractHolder()
            { }

            virtual
            const std::type_info&
            type() const = 0;

            virtual
            ResourceBase*
            base() = 0;
        };
        ///////////////////////

    private:
        AbstractHolder* _content;

        //////////////////////////////
        // class Holder <ResourceType>
        //////////////////////////////
    private:
        template<typename ResourceType>
        class Holder:
            public AbstractHolder
        {
        public:
            ResourceType* held; //<! owns it
        public:
            explicit inline
            Holder(ResourceType* held):
                held(held)
            {
                if (held == nullptr)
                    throw sys::Exception("Invalid resource.", "Resource Manager Holder Construction");
            }

            inline
            const std::type_info&
            type() const
            {
                return typeid(ResourceType);
            }

            inline
            ResourceBase*
            base()
            {
                return static_cast<ResourceBase*>(held);
            }
        };
        //////////////////////////////

    public:
        template<typename ResourceType>
        DatabaseResourceEntry(ResourceType* held):
            _content(new Holder<ResourceType>(held))
        { }
    private:
        // non-copyable
        DatabaseResourceEntry(const DatabaseResourceEntry&);
        DatabaseResourceEntry& operator=(const DatabaseResourceEntry&);

    public:
        inline
        ~DatabaseResourceEntry()
        {
            delete _content;
        }

        inline
        const std::type_info&
        type() const
        {
            return _content->type();
        }

        ResourceBase*
        resourceBase()
        {
            return _content->base();
        }

        template<typename ResourceType> static inline
        ResourceType*
        cast(DatabaseResourceEntry* entry)
        {
            return entry->type() == typeid(ResourceType)
                ? static_cast<Holder<ResourceType> *>(entry->_content)->held
                : nullptr;
        }
    };
}
}
