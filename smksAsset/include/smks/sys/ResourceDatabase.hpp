// ========================================================================= //
// Copyright 2016-2018 SUPAMONKS_STUDIO                                      //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
//                                                                           //
// ========================================================================= //
#pragma once

#include <iostream>
#include <sstream>
#include <cassert>

#include <smks/sys/Exception.hpp>

#include "smks/sys/ResourcePathFinder.hpp"
#include "smks/sys/DatabaseResourceEntry.hpp"
#include "smks/sys/DatabaseGenericEntry.hpp"

namespace smks { namespace sys
{
    class ResourcePathFinder;

    class FLIRT_ASSET_EXPORT ResourceDatabase
    {
    public:
        typedef std::shared_ptr<ResourceDatabase>   Ptr;
        typedef std::weak_ptr<ResourceDatabase>     WPtr;
    private:
        class PImpl;
    private:
        PImpl* _pImpl;

    public:
        static inline
        Ptr
        create(const char* name)
        {
            Ptr ptr (new ResourceDatabase(name));
            return ptr;
        }

    public:
        explicit
        ResourceDatabase(const char*);
    public:
        ~ResourceDatabase();

    private:
        // non-copyable
        ResourceDatabase(const ResourceDatabase&);
        ResourceDatabase& operator=(const ResourceDatabase&);

    public:
        const char*
        name() const;

        void
        setSharedPathFinder(ResourcePathFinder::Ptr const&);

        ResourcePathFinder::Ptr const&
        getOrCreatePathFinder();

        //---------------------
        // file-based resources
        //---------------------
        template<class ResourceType> inline
        const ResourceType*
        load(const char* fileName,
             void*       args = nullptr)
        {
            if (strlen(fileName) == 0)
                throw Exception(
                    "No valid filename specified.",
                    "Resource Loading");

            char resolved[512];

            if (getOrCreatePathFinder()->resolveFileName(fileName, resolved, 512) == 0)
            {
                std::cerr
                    << "Failed to resolve filename "
                    << "for file '" << fileName << "'."
                    << std::endl;
                return nullptr;
            }

            DatabaseResourceEntry*  entry       = findResourceEntry(resolved);
            ResourceType*           resource    = nullptr;

            if (entry) // already loaded
            {
                if (entry->type() != typeid(ResourceType))
                {
                    std::stringstream sstr;
                    sstr
                        << "Resource '" << resolved << "' "
                        << "already loaded with another resource type.";
                    throw sys::Exception(sstr.str().c_str(), "Resource Loading");
                }
                resource = DatabaseResourceEntry::cast<ResourceType>(entry);
            }
            else
            {
                resource = new ResourceType(resolved, args);
                entry    = new DatabaseResourceEntry(resource);
                storeResourceEntry(resolved, entry);
            }
            assert(resource);
            resource->incrReferenceCount();

            return resource;
        }

        bool
        unload(unsigned int);
        //---------------------

        //-----------------------
        // generic object storage
        //-----------------------
        template <typename ObjectType> inline
        unsigned int
        store(const char* alias, const ObjectType& obj)
        {
            return storeGenericEntry(alias, new DatabaseGenericEntry(obj));
        }

        template <typename ObjectType> inline
        const ObjectType&
        take(const char* alias) const
        {
            return take<ObjectType>(getResourceId(alias));
        }

        template <typename ObjectType> inline
        ObjectType&
        take(const char* alias)
        {
            return take<ObjectType>(getResourceId(alias));
        }

        template <typename ObjectType> inline
        const ObjectType&
        take(unsigned int id) const
        {
            const DatabaseGenericEntry* entry = findGenericEntry(id);

            if (entry == nullptr)
            {
                std::stringstream sstr;
                sstr
                    << "Failed to find entry "
                    << "with ID = " << id << ".";
                throw sys::Exception(sstr.str().c_str(), "Database Object Getter");
            }
            if (entry->type() != typeid(ObjectType))
            {
                std::stringstream sstr;
                sstr
                    << "Entry with ID = " << id << " is of a different type.";
                throw sys::Exception(sstr.str().c_str(), "Database Object Getter");
            }

            const ObjectType* obj = DatabaseGenericEntry::cast<ObjectType>(entry);
            assert(obj);

            return *obj;
        }

        template <typename ObjectType> inline
        ObjectType&
        take(unsigned int id)
        {
            DatabaseGenericEntry* entry = findGenericEntry(id);

            if (entry == nullptr)
            {
                std::stringstream sstr;
                sstr
                    << "Failed to find entry "
                    << "with ID = " << id << ".";
                throw sys::Exception(sstr.str().c_str(), "Database Object Getter");
            }
            if (entry->type() != typeid(ObjectType))
            {
                std::stringstream sstr;
                sstr
                    << "Entry with ID = " << id << " is of a different type.";
                throw sys::Exception(sstr.str().c_str(), "Database Object Getter");
            }

            ObjectType* obj = DatabaseGenericEntry::cast<ObjectType>(entry);
            assert(obj);

            return *obj;
        }

        template <typename ObjectType> inline
        bool
        stored(const char* alias) const
        {
            return stored<ObjectType>(getResourceId(alias));
        }

        template <typename ObjectType> inline
        bool
        stored(unsigned int id) const
        {
            const DatabaseGenericEntry* entry = findGenericEntry(id);
            return entry && entry->type() == typeid(ObjectType);
        }

        inline
        void
        drop(const char* alias)
        {
            drop(getResourceId(alias));
        }

        void
        drop(unsigned int);
        //-----------------------

    private:
        static
        unsigned int
        getResourceId(const char*);

        DatabaseResourceEntry*
        findResourceEntry(const char*);

        void
        storeResourceEntry(const char*, DatabaseResourceEntry*);

        const DatabaseGenericEntry*
        findGenericEntry(unsigned int) const;

        DatabaseGenericEntry*
        findGenericEntry(unsigned int);

        unsigned int
        storeGenericEntry(const char*, DatabaseGenericEntry*);
    };
}
}
