// ========================================================================= //
// Copyright 2016-2018 SUPAMONKS_STUDIO                                      //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
//                                                                           //
// ========================================================================= //
#pragma once

#include <memory>
#include <smks/math/types.hpp>
#include "smks/sys/configure_asset.hpp"

namespace smks
{
    namespace img
    {
        class AbstractImage;
    }
    namespace sys
    {
        class ResourceDatabase;
    }

    namespace xchg
    {
        class FLIRT_ASSET_EXPORT TileImages
        {
        public:
            typedef std::shared_ptr<TileImages> Ptr;
            typedef std::weak_ptr<TileImages>   WPtr;
        private:
            typedef std::shared_ptr<sys::ResourceDatabase> ResourceDatabasePtr;

        private:
            class PImpl;
        private:
            PImpl *_pImpl;

        public:
            static inline
            Ptr
            create(const char* filePath, ResourceDatabasePtr const& resources)
            {
                Ptr ptr(new TileImages(filePath, resources));
                return ptr;
            }

        protected:
            TileImages(const char* filePath, ResourceDatabasePtr const&);
        private:
            TileImages(const TileImages&);
            TileImages& operator=(const TileImages&);
        public:
            ~TileImages();

            const char* //<! resolved, formatted file path template used to find texture image tile files
            formattedFilePath() const;

            bool
            tileExists(const math::Vector2i&) const;

            const img::AbstractImage*
            getImage(const math::Vector2i&);

            //<! selects the texture tile depending on the texture coordinates,
            // also returns the texture coordinates in the local tile space
            const img::AbstractImage*
            getImage(const math::Vector2f&, math::Vector2f&);
        };
    }
}
