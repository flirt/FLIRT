// ========================================================================= //
// Copyright 2016-2018 SUPAMONKS_STUDIO                                      //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
//                                                                           //
// ========================================================================= //
#pragma once

#include <memory>
#include "smks/sys/configure_asset.hpp"

namespace smks
{
    namespace img
    {
        class AbstractImage;
    }
    namespace sys
    {
        class ResourceDatabase;
    }

    namespace xchg
    {
        class FLIRT_ASSET_EXPORT Image
        {
        public:
            typedef std::shared_ptr<Image>  Ptr;
            typedef std::weak_ptr<Image>    WPtr;
        private:
            typedef std::weak_ptr<sys::ResourceDatabase> ResourceDatabaseWPtr;

        private:
            class PImpl;
        private:
            PImpl *_pImpl;

        public:
            static inline
            Ptr
            create(const char* filePath, ResourceDatabaseWPtr const& resources)
            {
                Ptr ptr(new Image(filePath, resources));
                return ptr;
            }

        protected:
            Image(const char* filePath, ResourceDatabaseWPtr const&);
        private:
            Image(const Image&);
            Image& operator=(const Image&);
        public:
            ~Image();

            const char*
            filePath() const;

            const img::AbstractImage*
            getImage();
        };
    }
}
