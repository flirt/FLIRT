// ========================================================================= //
// Copyright 2016-2018 SUPAMONKS_STUDIO                                      //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
//                                                                           //
// ========================================================================= //
#include <string>

#include <smks/math/types.hpp>
#include <smks/math/RotateOrder.hpp>
#include <smks/sys/constants.hpp>
#include <smks/xchg/Image.hpp>
#include <smks/xchg/TileImages.hpp>
#include "smks/xchg/BoundingBox.hpp"
#include "smks/xchg/IdSet.hpp"
#include "smks/xchg/Data.hpp"
#include "smks/xchg/DataStream.hpp"
#include "smks/xchg/ObjectPtr.hpp"
#include "smks/xchg/ManipulatorFlags.hpp"
#include "smks/xchg/MouseFlags.hpp"

#include "smks/parm/BuiltIns.hpp"
#include "smks/parm/BuiltIn.hpp"
#include "smks/parm/BuiltInMap.hpp"
#include "initializeAllBuiltIns.hpp"

namespace smks { namespace parm
{
    typedef const BuiltIn& (*Creator)();    //<! creator function for declaring built-in parameters
    std::vector<Creator> _registered;       //<! list of all built-in declaration calls
    //<! structure used to fill the list of declaration calls
    struct RegisterCreator
    {
        explicit inline
        RegisterCreator(const Creator& func)
        {
            _registered.push_back(func);
        }
    };
}
}
//-----------------------------------------------------------------------------------------------
#define BUILTIN_DECL_TYPELESS(_NAME_)                                                           \
    const BuiltIn& _NAME_()                                                                     \
    {                                                                                           \
        static BuiltIn::Ptr ptr = builtIns()                                                    \
            .add(BuiltIn::create("" #_NAME_ ""));                                               \
        return *ptr;                                                                            \
    }                                                                                           \
    RegisterCreator _registered_builtin_##_NAME_##(_NAME_);
//-----------------------------------------------------------------------------------------------
#define BUILTIN_DECL(_CTYPE_, _NAME_, _PARMFLAGS_, _DEFAULT_, _VALIDATOR_)                      \
    const BuiltIn& _NAME_()                                                                     \
    {                                                                                           \
        static BuiltIn::Ptr ptr = builtIns()                                                    \
            .add(BuiltIn::create<_CTYPE_>("" #_NAME_ "", _PARMFLAGS_, _DEFAULT_, _VALIDATOR_)); \
        return *ptr;                                                                            \
    }                                                                                           \
    RegisterCreator _registered_builtin_##_NAME_##(_NAME_);
//-----------------------------------------------------------------------------------------------

namespace smks { namespace parm
{
    BUILTIN_DECL (bool              ,accumulate,                    NO_PARM_FLAG,                   false,                                      nullptr)
    BUILTIN_DECL (xchg::ObjectPtr   ,backplate,                     NO_PARM_FLAG,                   xchg::ObjectPtr(),                          new IsObject<xchg::Image>())
    BUILTIN_DECL (std::string       ,backplatePath,                 NO_PARM_FLAG,                   std::string(),                              nullptr)
    BUILTIN_DECL (float             ,backScattering,                NO_PARM_FLAG,                   0.0f,                                       new LowerBound<float>(0.0f))
    BUILTIN_DECL (unsigned int      ,backScatteringMap,             CREATES_LINK,                   0,                                          nullptr)
    BUILTIN_DECL (xchg::BoundingBox ,boundingBox,                   NO_PARM_FLAG,                   xchg::BoundingBox::UnitBox(),               nullptr)
    BUILTIN_DECL (unsigned int      ,cameraId,                      CREATES_LINK,                   0,                                          nullptr)
    BUILTIN_DECL (bool              ,castsShadows,                  NO_PARM_FLAG,                   true,                                       nullptr)
    BUILTIN_DECL (math::Vector4f    ,center,                        NO_PARM_FLAG,                   math::Vector4f::UnitW(),                    new IsPoint3d())
    BUILTIN_DECL (std::string       ,code,                          NO_PARM_FLAG,                   std::string(),                              nullptr)
    BUILTIN_DECL (std::string       ,config,                        NO_PARM_FLAG,                   std::string(),                              nullptr)
    BUILTIN_DECL (char              ,contrastLevel,                 NO_PARM_FLAG,                   0,                                          new LowerBound<char>(0))
    BUILTIN_DECL (int               ,currentSampleIdx,              NO_PARM_FLAG,                   -1,                                         nullptr)
    BUILTIN_DECL (float             ,currentTime,                   NO_PARM_FLAG,                   0.0f,                                       new LowerBound<float>(0.0f))
    BUILTIN_DECL (char              ,currentlyVisible,              NO_PARM_FLAG,                   1,                                          new Range<char>(-1, 1))
    BUILTIN_DECL (unsigned int      ,denoiserId,                    CREATES_LINK,                   0,                                          nullptr)
    BUILTIN_DECL (math::Vector4f    ,diffuseReflectance,            NO_PARM_FLAG,                   math::Vector4f::Ones(),                     new IsColor())
    BUILTIN_DECL (unsigned int      ,diffuseReflectanceMap,         CREATES_LINK,                   0,                                          nullptr)
    BUILTIN_DECL (math::Vector4f    ,diffuseTransmission,           NO_PARM_FLAG,                   math::Vector4f::UnitW(),                    new IsColor())
    BUILTIN_DECL (unsigned int      ,diffuseTransmissionMap,        CREATES_LINK,                   0,                                          nullptr)
    BUILTIN_DECL (std::string       ,displayName,                   NO_PARM_FLAG,                   std::string(),                              nullptr)
    BUILTIN_DECL (std::string       ,displayViewName,               NO_PARM_FLAG,                   std::string(),                              nullptr)
    BUILTIN_DECL (float             ,extIOR,                        NO_PARM_FLAG,                   1.0f,                                       new LowerBound<float>(0.0f))
    BUILTIN_DECL (math::Vector4f    ,extTransmission,               NO_PARM_FLAG,                   math::Vector4f::Ones(),                     new IsColor())
    BUILTIN_DECL (float             ,exposureFstops,                NO_PARM_FLAG,                   0.0f,                                       new LowerBound<float>(0.0f))
    //---
    BUILTIN_DECL (float             ,eyeCorneaIOR,                  NO_PARM_FLAG,                   1.5f,                                       new Range<float>(0.0f, 3.0f))
    BUILTIN_DECL (math::Vector4f    ,eyeGlobeColor,                 NO_PARM_FLAG,                   math::Vector4f(0.7f, 0.7f, 0.7f, 1.0f),     new IsColor())
    BUILTIN_DECL (float             ,eyeHighlightAngSizeD,          NO_PARM_FLAG,                   0.0f,                                       new Range<float>(0.0f, 180.0f))
    BUILTIN_DECL (math::Vector4f    ,eyeHighlightColor,             NO_PARM_FLAG,                   math::Vector4f::Ones(),                     new IsColor())
    BUILTIN_DECL (float             ,eyeHighlightSmoothness,        NO_PARM_FLAG,                   0.0f,                                       new Range<float>(0.0f, 1.0f))
    BUILTIN_DECL (math::Vector2f    ,eyeHighlightCoordsD,           NO_PARM_FLAG,                   math::Vector2f::Zero(),                     new CwiseValidator2f( new Range<float>(-90.0f, 90.0f), new Range<float>(-360.0f, 360.0f) ))
    BUILTIN_DECL (float             ,eyeIrisAngSizeD,               NO_PARM_FLAG,                   57.0f,                                      new Range<float>(0.0f, 90.0f))
    BUILTIN_DECL (math::Vector4f    ,eyeIrisColor,                  NO_PARM_FLAG,                   math::Vector4f(0.0f, 0.3f, 0.3f, 1.0f),     new IsColor())
    BUILTIN_DECL (float             ,eyeIrisDepth,                  NO_PARM_FLAG,                   0.2f,                                       new Range<float>(1e-4f, 1.0f))
    BUILTIN_DECL (math::Vector2f    ,eyeIrisDimensions,             NO_PARM_FLAG,                   math::Vector2f::Ones(),                     new Range<math::Vector2f>(math::Vector2f::Constant(1e-4f), math::Vector2f::Ones()))
    BUILTIN_DECL (float             ,eyeIrisEdgeBlur,               NO_PARM_FLAG,                   0.0f,                                       new Range<float>(0.0f, 1.0f))
    BUILTIN_DECL (float             ,eyeIrisEdgeOffset,             NO_PARM_FLAG,                   0.0f,                                       new Range<float>(-0.1f, 0.1f))
    BUILTIN_DECL (float             ,eyeIrisNormalSmoothness,       NO_PARM_FLAG,                   0.2f,                                       new Range<float>(0.0f, 1.0f))
    BUILTIN_DECL (float             ,eyeIrisRotationD,              NO_PARM_FLAG,                   0.0f,                                       new Range<float>(0.0f, 360.0f))
    BUILTIN_DECL (float             ,eyePupilBlur,                  NO_PARM_FLAG,                   0.2f,                                       new Range<float>(0.0f, 1.0f))
    BUILTIN_DECL (math::Vector2f    ,eyePupilDimensions,            NO_PARM_FLAG,                   math::Vector2f::Ones(),                     new Range<math::Vector2f>(math::Vector2f::Constant(1e-4f), math::Vector2f::Ones()))
    BUILTIN_DECL (float             ,eyePupilSize,                  NO_PARM_FLAG,                   0.2f,                                       new Range<float>(0.0f, 1.0f))
    BUILTIN_DECL (float             ,eyeRadius,                     NO_PARM_FLAG,                   1.0f,                                       new Range<float>(0.0f, 10.0f))
    //---
    BUILTIN_DECL (float             ,falloff,                       NO_PARM_FLAG,                   5.0f,                                       new LowerBound<float>(1e-2f))
    BUILTIN_DECL (unsigned int      ,falloffMap,                    CREATES_LINK,                   0,                                          nullptr)
    BUILTIN_DECL (std::string       ,filePath,                      NO_PARM_FLAG,                   std::string(),                              nullptr)
    BUILTIN_DECL (char              ,forciblyVisible,               NO_PARM_FLAG,                   -1,                                         new Range<char>(-1, 1))
    BUILTIN_DECL (unsigned int      ,frameBufferId,                 CREATES_LINK,                   0,                                          nullptr)
    BUILTIN_DECL (xchg::Data::Ptr   ,frameData,                     NO_PARM_FLAG,                   nullptr,                                    nullptr)
    BUILTIN_DECL (float             ,framerate,                     NO_PARM_FLAG,                   25.0f,                                      new LowerBound<float>(1e-4f))
    BUILTIN_DECL (std::string       ,fresnelName,                   NO_PARM_FLAG,                   "none",                                     nullptr)
    BUILTIN_DECL (math::Vector4f    ,fresnelValue,                  NO_PARM_FLAG,                   math::Vector4f(0.5f, 0.5f, 0.5f, 1.0f),     new IsColor())
    BUILTIN_DECL (float             ,fresnelWeight,                 NO_PARM_FLAG,                   0.5f,                                       new Range<float>(0.0f, 1.0f))
    BUILTIN_DECL (unsigned int      ,fresnelWeightMap,              CREATES_LINK,                   0,                                          nullptr)
    BUILTIN_DECL (math::Vector4f    ,front,                         NO_PARM_FLAG,                   math::Vector4f(0.0f, 0.0f, -5.0f, 1.0f),    nullptr)
    BUILTIN_DECL (float             ,focalDistance,                 NO_PARM_FLAG,                   5.0f,                                       new LowerBound<float>(0.0f))
    BUILTIN_DECL (bool              ,forceInitialization,           NO_PARM_FLAG,                   false,                                      nullptr)
    BUILTIN_DECL (float             ,gamma,                         NO_PARM_FLAG,                   1.0f,                                       new LowerBound<float>(1e-4f))
    BUILTIN_DECL (math::Vector4f    ,glitterColor,                  NO_PARM_FLAG,                   math::Vector4f::UnitW(),                    new IsColor())
    BUILTIN_DECL (unsigned int      ,glitterColorMap,               CREATES_LINK,                   0,                                          nullptr)
    BUILTIN_DECL (float             ,glitterSpread,                 NO_PARM_FLAG,                   1.0f,                                       new LowerBound<float>(0.0f))
    BUILTIN_DECL (unsigned int      ,glitterSpreadMap,              CREATES_LINK,                   0,                                          nullptr)
    BUILTIN_DECL (char              ,globalInitialization,          SWITCHES_STATE | NEEDS_SCENE,   xchg::ACTIVE,                               new OnlyFiniteStateByUser())
    BUILTIN_DECL (char              ,globallyVisible,               NEEDS_SCENE,                    1,                                          nullptr)
    BUILTIN_DECL (size_t            ,globalNumSamples,              NEEDS_SCENE,                    0,                                          nullptr)
    BUILTIN_DECL (math::Vector4f    ,glossyReflectance,             NO_PARM_FLAG,                   math::Vector4f::Ones(),                     new IsColor())
    BUILTIN_DECL (unsigned int      ,glossyReflectanceMap,          CREATES_LINK,                   0,                                          nullptr)
    //---
    BUILTIN_DECL (size_t            ,glCount,                       NO_PARM_FLAG,                   0,                                          nullptr)
    BUILTIN_DECL (xchg::Data::Ptr   ,glIndices,                     NO_PARM_FLAG,                   nullptr,                                    nullptr)
    BUILTIN_DECL (xchg::DataStream  ,glNormals,                     NO_PARM_FLAG,                   xchg::DataStream(),                         nullptr)
    BUILTIN_DECL (xchg::DataStream  ,glPositions,                   NO_PARM_FLAG,                   xchg::DataStream(),                         nullptr)
    BUILTIN_DECL (xchg::DataStream  ,glTexcoords,                   NO_PARM_FLAG,                   xchg::DataStream(),                         nullptr)
    BUILTIN_DECL (xchg::Data::Ptr   ,glVertices,                    NO_PARM_FLAG,                   nullptr,                                    nullptr)
    BUILTIN_DECL (xchg::DataStream  ,glWidths,                      NO_PARM_FLAG,                   xchg::DataStream(),                         nullptr)
    BUILTIN_DECL (xchg::Data::Ptr   ,glWireIndices,                 NO_PARM_FLAG,                   nullptr,                                    nullptr)
    //---
    BUILTIN_DECL (float             ,halfAngleD,                    NO_PARM_FLAG,                   0.0f,                                       new Range<float>(0.0f, 90.0f))
    BUILTIN_DECL (float             ,halfWidth,                     NO_PARM_FLAG,                   0.0f,                                       new LowerBound<float>(0.0f))
    BUILTIN_DECL (unsigned int      ,hdriMap,                       CREATES_LINK,                   0,                                          nullptr)
    BUILTIN_DECL (size_t            ,height,                        NO_PARM_FLAG,                   0,                                          nullptr)
    BUILTIN_DECL (math::Vector4f    ,horizonScatteringColor,        NO_PARM_FLAG,                   math::Vector4f::Ones(),                     new IsColor())
    BUILTIN_DECL (unsigned int      ,horizonScatteringColorMap,     CREATES_LINK,                   0,                                          nullptr)
    BUILTIN_DECL (float             ,horizonScatteringFalloff,      NO_PARM_FLAG,                   0.0f,                                       new LowerBound<float>(0.0f))
    BUILTIN_DECL (unsigned int      ,horizonScatteringFalloffMap,   CREATES_LINK,                   0,                                          nullptr)
    BUILTIN_DECL (unsigned int      ,id,                            NO_PARM_FLAG,                   0,                                          nullptr)
    BUILTIN_DECL (int               ,illumMask,                     NO_PARM_FLAG,                   -1,                                         nullptr)
    BUILTIN_DECL (xchg::ObjectPtr   ,image,                         NO_PARM_FLAG,                   xchg::ObjectPtr(),                          new IsObject<xchg::Image>())
    BUILTIN_DECL (bool              ,inheritsTransforms,            NO_PARM_FLAG,                   true,                                       nullptr)
    BUILTIN_DECL (char              ,initialization,                SWITCHES_STATE,                 xchg::ACTIVE,                               new OnlyFiniteStateByUser())
    BUILTIN_DECL (std::string       ,inputColorspace,               NO_PARM_FLAG,                   std::string(),                              nullptr)
    BUILTIN_DECL (unsigned int      ,inputId0,                      CREATES_LINK,                   0,                                          nullptr)
    BUILTIN_DECL (unsigned int      ,inputId1,                      CREATES_LINK,                   0,                                          nullptr)
    BUILTIN_DECL (unsigned int      ,integratorId,                  CREATES_LINK,                   0,                                          nullptr)
    BUILTIN_DECL (math::Vector4f    ,intensity,                     NO_PARM_FLAG,                   math::Vector4f::Ones(),                     new IsPoint3dAbove(math::Vector4f::UnitW()))
    BUILTIN_DECL (float             ,intIOR,                        NO_PARM_FLAG,                   1.45f,                                      new LowerBound<float>(0.0f))
    BUILTIN_DECL (math::Vector4f    ,intTransmission,               NO_PARM_FLAG,                   math::Vector4f::Ones(),                     new IsColor())
    BUILTIN_DECL (float             ,IOR,                           NO_PARM_FLAG,                   1.45f,                                      new LowerBound<float>(0.0f))
    BUILTIN_DECL (math::Vector4f    ,IORreal,                       NO_PARM_FLAG,                   math::Vector4f(1.45f, 1.45f, 1.45f, 1.0f),  new IsPoint3dAbove(math::Vector4f::UnitW()))
    BUILTIN_DECL (math::Vector4f    ,IORimag,                       NO_PARM_FLAG,                   math::Vector4f::UnitW(),                    new IsPoint3dAbove(math::Vector4f::UnitW()))
    BUILTIN_DECL (math::Vector4f    ,irradiance,                    NO_PARM_FLAG,                   math::Vector4f::UnitW(),                    new IsPoint3dAbove(math::Vector4f::UnitW()))
    BUILTIN_DECL (bool              ,isProgressive,                 NO_PARM_FLAG,                   false,                                      nullptr)
    BUILTIN_DECL (size_t            ,iteration,                     NO_PARM_FLAG,                   0,                                          nullptr)
    BUILTIN_DECL (math::Vector4f    ,layerColor,                    NO_PARM_FLAG,                   math::Vector4f::Ones(),                     new IsColor())
    BUILTIN_DECL (unsigned int      ,layerColorMap,                 CREATES_LINK,                   0,                                          nullptr)
    BUILTIN_DECL (float             ,layerIOR,                      NO_PARM_FLAG,                   1.45f,                                      new LowerBound<float>(0.0f))
    BUILTIN_DECL (float             ,lensRadius,                    NO_PARM_FLAG,                   0.0f,                                       new LowerBound<float>(0.0f))
    BUILTIN_DECL (char              ,manipulatorFlags,               NO_PARM_FLAG,                   static_cast<char>(xchg::NO_MANIP),          nullptr)
    BUILTIN_DECL (unsigned int      ,materialId,                    CREATES_LINK,                   0,                                          nullptr)
    BUILTIN_DECL (float             ,maxAngleD,                     NO_PARM_FLAG,                   0.0f,                                       new Range<float>(0.0f, 90.0f))
    BUILTIN_DECL (size_t            ,maxCount,                      NO_PARM_FLAG,                   8,                                          nullptr)
    BUILTIN_DECL (float             ,maxDistance,                   NO_PARM_FLAG,                   sys::inf,                                   new LowerBound<float>(0.0f))
    BUILTIN_DECL (float             ,maxPatchDistance,              NO_PARM_FLAG,                   0.8f,                                       new LowerBound<float>(0.0f))
    BUILTIN_DECL (size_t            ,maxRayDepth,                   NO_PARM_FLAG,                   3,                                          nullptr)
    BUILTIN_DECL (float             ,maxRayIntensity,               NO_PARM_FLAG,                   0.0f,                                       new LowerBound<float>(0.0f))
    BUILTIN_DECL (size_t            ,maxTimeSteps,                  NO_PARM_FLAG,                   2,                                          new Range<size_t>(1, 127))
    BUILTIN_DECL (float             ,minAngleD,                     NO_PARM_FLAG,                   0.0f,                                       new Range<float>(0.0f, 90.0f))
    BUILTIN_DECL (size_t            ,minPatchMatchCount,            NO_PARM_FLAG,                   2,                                          nullptr)
    BUILTIN_DECL (float             ,minRayContribution,            NO_PARM_FLAG,                   1e-2f,                                      new LowerBound<float>(1e-2f))
    BUILTIN_DECL (char              ,mouseFlags,                    NO_PARM_FLAG,                   static_cast<char>(xchg::NO_MOUSE_FLAGS),    nullptr)
    BUILTIN_DECL (std::string       ,NDFName,                       NO_PARM_FLAG,                   "powerCosine",                              nullptr)
    BUILTIN_DECL (unsigned int      ,normalMap,                     CREATES_LINK,                   0,                                          nullptr)
    BUILTIN_DECL (size_t            ,numIndexBufferSamples,         NO_PARM_FLAG,                   0,                                          nullptr)
    BUILTIN_DECL (size_t            ,numHistoBins,                  NO_PARM_FLAG,                   20,                                         new LowerBound<size_t>(2))
    BUILTIN_DECL (size_t            ,numLightsForDirect,            NO_PARM_FLAG,                   0,                                          nullptr)
    BUILTIN_DECL (size_t            ,numMipmapLevels,               NO_PARM_FLAG,                   3,                                          new LowerBound<size_t>(1))
    BUILTIN_DECL (size_t            ,numSamples,                    NO_PARM_FLAG,                   0,                                          nullptr)
    BUILTIN_DECL (size_t            ,numSampleSets,                 NO_PARM_FLAG,                   1,                                          nullptr)
    BUILTIN_DECL (size_t            ,numVertexBufferSamples,        NO_PARM_FLAG,                   0,                                          nullptr)
    BUILTIN_DECL (float             ,opacity,                       NO_PARM_FLAG,                   1.0f,                                       new Range<float>(0.0f, 1.0f))
    BUILTIN_DECL (unsigned int      ,opacityMap,                    CREATES_LINK,                   0,                                          nullptr)
    BUILTIN_DECL (std::string       ,parmName,                      NO_PARM_FLAG,                   std::string(),                              nullptr)
    //---
    BUILTIN_DECL_TYPELESS   (parmValue)
    //---
    BUILTIN_DECL (size_t            ,patchHalfSize,                 NO_PARM_FLAG,                   1,                                          new LowerBound<size_t>(1))
    BUILTIN_DECL (unsigned int      ,pixelFilterId,                 CREATES_LINK,                   0,                                          nullptr)
    BUILTIN_DECL (xchg::DataStream  ,positions,                     NO_PARM_FLAG,                   xchg::DataStream(),                         nullptr)
    BUILTIN_DECL (bool              ,primaryVisibility,             NO_PARM_FLAG,                   true,                                       nullptr)
    BUILTIN_DECL (int               ,priority,                      NO_PARM_FLAG,                   0,                                          nullptr)
    BUILTIN_DECL (math::Vector4f    ,radiance,                      NO_PARM_FLAG,                   math::Vector4f::UnitW(),                    new IsPoint3dAbove(math::Vector4f::UnitW()))
    BUILTIN_DECL (bool              ,receivesShadows,               NO_PARM_FLAG,                   true,                                       nullptr)
    BUILTIN_DECL (math::Vector4f    ,reflectance,                   NO_PARM_FLAG,                   math::Vector4f::Ones(),                     new IsColor())
    BUILTIN_DECL (unsigned int      ,reflectanceMap,                CREATES_LINK,                   0,                                          nullptr)
    BUILTIN_DECL (float             ,reflectivity,                  NO_PARM_FLAG,                   0.2f,                                       new Range<float>(0.0f, 1.0f))
    BUILTIN_DECL (std::string       ,regex,                         NO_PARM_FLAG,                   std::string(),                              nullptr)
    BUILTIN_DECL (unsigned int      ,rendererId,                    CREATES_LINK,                   0,                                          nullptr)
    BUILTIN_DECL (char              ,rendering,                     TRIGGERS_ACTION,                xchg::INACTIVE,                             new OnlyFiniteStateByUser())
    BUILTIN_DECL (xchg::IdSet       ,renderPassIds,                 CREATES_LINK,                   xchg::IdSet(),                              nullptr)
    BUILTIN_DECL (math::Vector4f    ,rotateD,                       NO_PARM_FLAG,                   math::Vector4f::UnitW(),                    new Range<math::Vector4f>(math::Vector4f(-180.0f, -180.0f, -180.0f, 1.0f), math::Vector4f(180.0f, 180.0f, 180.0f, 1.0f)))
    BUILTIN_DECL (char              ,rotateOrder,                   NO_PARM_FLAG,                   static_cast<char>(math::RotateOrder::XYZ),  nullptr)
    BUILTIN_DECL (float             ,roughness,                     NO_PARM_FLAG,                   1e-2f,                                      new LowerBound<float>(0.0f))
    BUILTIN_DECL (unsigned int      ,roughnessMap,                  CREATES_LINK,                   0,                                          nullptr)
    BUILTIN_DECL (float             ,roughnessX,                    NO_PARM_FLAG,                   1e-2f,                                      new LowerBound<float>(0.0f))
    BUILTIN_DECL (unsigned int      ,roughnessXMap,                 CREATES_LINK,                   0,                                          nullptr)
    BUILTIN_DECL (float             ,roughnessY,                    NO_PARM_FLAG,                   1e-2f,                                      new LowerBound<float>(0.0f))
    BUILTIN_DECL (unsigned int      ,roughnessYMap,                 CREATES_LINK,                   0,                                          nullptr)
    //---
    BUILTIN_DECL (size_t            ,rtCount,                       NO_PARM_FLAG,                   0,                                          nullptr)
    BUILTIN_DECL (xchg::Data::Ptr   ,rtCounts,                      NO_PARM_FLAG,                   nullptr,                                    nullptr)
    BUILTIN_DECL (xchg::Data::Ptr   ,rtFaceVertices,                NO_PARM_FLAG,                   nullptr,                                    nullptr)
    BUILTIN_DECL (xchg::Data::Ptr   ,rtIndices,                     NO_PARM_FLAG,                   nullptr,                                    nullptr)
    BUILTIN_DECL (xchg::Data::Ptr   ,rtPathIds,                     NO_PARM_FLAG,                   nullptr,                                    nullptr)
    BUILTIN_DECL (xchg::DataStream  ,rtPositions,                   NO_PARM_FLAG,                   xchg::DataStream(),                         nullptr)
    BUILTIN_DECL (xchg::DataStream  ,rtNormals,                     NO_PARM_FLAG,                   xchg::DataStream(),                         nullptr)
    BUILTIN_DECL (xchg::Data::Ptr   ,rtScalps,                      NO_PARM_FLAG,                   nullptr,                                    nullptr)
    BUILTIN_DECL (std::string       ,rtSubdivisionMethod,           NO_PARM_FLAG,                   "osd",                                      nullptr)
    BUILTIN_DECL (xchg::DataStream  ,rtTangentsX,                   NO_PARM_FLAG,                   xchg::DataStream(),                         nullptr)
    BUILTIN_DECL (xchg::DataStream  ,rtTangentsY,                   NO_PARM_FLAG,                   xchg::DataStream(),                         nullptr)
    BUILTIN_DECL (float             ,rtTesselationRate,             NO_PARM_FLAG,                   4.0f,                                       nullptr)
    BUILTIN_DECL (xchg::DataStream  ,rtTexcoords,                   NO_PARM_FLAG,                   xchg::DataStream(),                         nullptr)
    BUILTIN_DECL (xchg::Data::Ptr   ,rtVertices,                    NO_PARM_FLAG,                   nullptr,                                    nullptr)
    BUILTIN_DECL (xchg::DataStream  ,rtWidths,                      NO_PARM_FLAG,                   xchg::DataStream(),                         nullptr)
    //---
    BUILTIN_DECL (unsigned int      ,samplerFactoryId,              CREATES_LINK,                   0,                                          nullptr)
    BUILTIN_DECL (size_t            ,searchWindowHalfSize,          NO_PARM_FLAG,                   1,                                          new LowerBound<size_t>(1))
    BUILTIN_DECL (bool              ,selected,                      NO_PARM_FLAG,                   false,                                      nullptr)
    BUILTIN_DECL (xchg::IdSet       ,selection,                     CREATES_LINK,                   xchg::IdSet(),                              nullptr)
    BUILTIN_DECL (unsigned int      ,selectorId,                    CREATES_LINK,                   0,                                          nullptr)
    BUILTIN_DECL (xchg::IdSet       ,selectorIds,                   CREATES_LINK,                   xchg::IdSet(),                              nullptr)
    BUILTIN_DECL (math::Vector4f    ,scale,                         NO_PARM_FLAG,                   math::Vector4f::Ones(),                     new IsPoint3d())
    BUILTIN_DECL (float             ,shadowBias,                    NO_PARM_FLAG,                   0.0f,                                       new LowerBound<float>(0.0f))
    BUILTIN_DECL (unsigned int      ,shapeId,                       CREATES_LINK,                   0,                                          nullptr)
    BUILTIN_DECL (bool              ,showProgress,                  NO_PARM_FLAG,                   false,                                      nullptr)
    BUILTIN_DECL (float             ,shutter,                       NO_PARM_FLAG,                   1.0f,                                       new Range<float>(0.0f, 1.0f))
    BUILTIN_DECL (std::string       ,shutterType,                   NO_PARM_FLAG,                   "none",                                     nullptr)
    BUILTIN_DECL (unsigned int      ,sourceId,                      CREATES_LINK,                   0,                                          nullptr)
    BUILTIN_DECL (float             ,specularExponent,              NO_PARM_FLAG,                   1.0f,                                       new LowerBound<float>(0.0f))
    BUILTIN_DECL (unsigned int      ,specularExponentMap,           CREATES_LINK,                   0,                                          nullptr)
    BUILTIN_DECL (math::Vector4f    ,specularReflectance,           NO_PARM_FLAG,                   math::Vector4f::UnitW(),                    new IsColor())
    BUILTIN_DECL (unsigned int      ,specularReflectanceMap,        CREATES_LINK,                   0,                                          nullptr)
    BUILTIN_DECL (bool              ,storeAsHalf,                   NO_PARM_FLAG,                   false,                                      nullptr)
    BUILTIN_DECL (bool              ,storeRawRgba,                  NO_PARM_FLAG,                   false,                                      nullptr)
    BUILTIN_DECL (bool              ,storeRgbaAsHalf,               NO_PARM_FLAG,                   false,                                      nullptr)
    BUILTIN_DECL (unsigned int      ,subdivisionLevel,              NO_PARM_FLAG,                   0,                                          new UpperBound<unsigned int>(3))
    BUILTIN_DECL (unsigned int      ,subsurfaceId,                  CREATES_LINK,                   0,                                          nullptr)
    BUILTIN_DECL (float             ,thickness,                     NO_PARM_FLAG,                   1e-1f,                                      new LowerBound<float>(0.0f))
    BUILTIN_DECL (unsigned int      ,thicknessMap,                  CREATES_LINK,                   0,                                          nullptr)
    BUILTIN_DECL (xchg::ObjectPtr   ,tileImages,                    NO_PARM_FLAG,                   xchg::ObjectPtr(),                          new IsObject<xchg::TileImages>())
    BUILTIN_DECL (math::Vector2f    ,timeBounds,                    NO_PARM_FLAG,                   math::Vector2f(0.0f, FLT_MAX),              new MinAndMax(new LowerBound<float>(0.0f)))
    BUILTIN_DECL (unsigned int      ,toneMapperId,                  CREATES_LINK,                   0,                                          nullptr)
    BUILTIN_DECL (math::Vector4f    ,translate,                     NO_PARM_FLAG,                   math::Vector4f::UnitW(),                    new IsPoint3d())
    BUILTIN_DECL (math::Affine3f    ,transform,                     NO_PARM_FLAG,                   math::Affine3f::Identity(),                 nullptr)
    BUILTIN_DECL (math::Vector4f    ,transformUV,                   NO_PARM_FLAG,                   math::Vector4f(1.0f, 1.0f, 0.0f, 0.0f),     nullptr)
    BUILTIN_DECL (math::Vector4f    ,transmission,                  NO_PARM_FLAG,                   math::Vector4f::Ones(),                     new IsColor())
    BUILTIN_DECL (unsigned int      ,transmissionMap,               CREATES_LINK,                   0,                                          nullptr)
    BUILTIN_DECL (float             ,transmissivity,                NO_PARM_FLAG,                   0.5f,                                       new Range<float>(0.0f, 1.0f))
    BUILTIN_DECL (unsigned int      ,transmissivityMap,             CREATES_LINK,                   0,                                          nullptr)
    BUILTIN_DECL (bool              ,transparentShadows,            NO_PARM_FLAG,                   false,                                      nullptr)
    BUILTIN_DECL (math::Vector4f    ,up,                            NO_PARM_FLAG,                   math::Vector4f(0.0f, 2.0f, 0.0f, 1.0f),     new IsPoint3d())
    BUILTIN_DECL (math::Vector4f    ,upVector,                      NO_PARM_FLAG,                   math::Vector4f::UnitY(),                    new IsDirection3d())
    BUILTIN_DECL (bool              ,usedAsPoint,                   NO_PARM_FLAG,                   true,                                       nullptr)
    BUILTIN_DECL (char              ,userAttribInitialization,      SWITCHES_STATE,                 xchg::INACTIVE,                             new OnlyFiniteStateByUser())
    BUILTIN_DECL (std::string       ,userColorName,                 NO_PARM_FLAG,                   std::string(),                              nullptr)
    BUILTIN_DECL (char              ,verbosityLevel,                NO_PARM_FLAG,                   0,                                          new Range<char>(0, 3))
    BUILTIN_DECL (xchg::Data::Ptr   ,vertices,                      NO_PARM_FLAG,                   nullptr,                                    nullptr)
    BUILTIN_DECL (bool              ,vignetting,                    NO_PARM_FLAG,                   false,                                      nullptr)
    BUILTIN_DECL (bool              ,visibleInReflections,          NO_PARM_FLAG,                   true,                                       nullptr)
    BUILTIN_DECL (bool              ,visibleInRefractions,          NO_PARM_FLAG,                   true,                                       nullptr)
    BUILTIN_DECL (float             ,weight,                        NO_PARM_FLAG,                   0.0f,                                       new Range<float>(0.0f, 1.0f))
    BUILTIN_DECL (unsigned int      ,weightMap,                     CREATES_LINK,                   0,                                          nullptr)
    BUILTIN_DECL (size_t            ,width,                         NO_PARM_FLAG,                   0,                                          nullptr)
    BUILTIN_DECL (xchg::DataStream  ,widths,                        NO_PARM_FLAG,                   xchg::DataStream(),                         nullptr)
    BUILTIN_DECL (math::Affine3f    ,worldTransform,                NEEDS_SCENE,                    math::Affine3f::Identity(),                 nullptr)
    BUILTIN_DECL (float             ,xAspectRatio,                  NO_PARM_FLAG,                   1.5f,                                       new LowerBound<float>(0.0f))
    BUILTIN_DECL (int               ,xMax,                          NO_PARM_FLAG,                   -1,                                         nullptr)
    BUILTIN_DECL (int               ,xMin,                          NO_PARM_FLAG,                   -1,                                         nullptr)
    BUILTIN_DECL (float             ,yFieldOfViewD,                 NO_PARM_FLAG,                   37.85f /*eqv to maya's 54.43*/,             new Range<float>(0.0f, 90.0f))
    BUILTIN_DECL (int               ,yMax,                          NO_PARM_FLAG,                   -1,                                         nullptr)
    BUILTIN_DECL (int               ,yMin,                          NO_PARM_FLAG,                   -1,                                         nullptr)
    BUILTIN_DECL (float             ,zFar,                          NO_PARM_FLAG,                   1e+5f,                                      new LowerBound<float>(0.0f))
    BUILTIN_DECL (float             ,zNear,                         NO_PARM_FLAG,                   0.1f,                                       new LowerBound<float>(0.0f))

    void
    initializeAllBuiltIns()
    {
        for (size_t i = 0; i < _registered.size(); ++i)
            _registered[i]();
    }
}
}

