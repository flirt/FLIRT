// ========================================================================= //
// Copyright 2016-2018 SUPAMONKS_STUDIO                                      //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
//                                                                           //
// ========================================================================= //
#include "smks/parm/BuiltIn.hpp"
#include "smks/xchg/BoundingBox.hpp"
#include "smks/xchg/Data.hpp"
#include "smks/xchg/DataStream.hpp"
#include "smks/xchg/IdSet.hpp"
#include "smks/xchg/ObjectPtr.hpp"
#include "smks/xchg/ActionState.hpp"

using namespace smks;

xchg::ParmType
parm::BuiltIn::type() const
{
    if (_typeinfo)
    {
        if (_typeinfo->type() == typeid(bool))                              return xchg::PARMTYPE_BOOL;
        if (_typeinfo->type() == typeid(unsigned int))                      return xchg::PARMTYPE_UINT;
        if (_typeinfo->type() == typeid(size_t))                            return xchg::PARMTYPE_SIZE;
        if (_typeinfo->type() == typeid(char))                              return xchg::PARMTYPE_CHAR;
        if (_typeinfo->type() == typeid(int))                               return xchg::PARMTYPE_INT;
        if (_typeinfo->type() == typeid(math::Vector2i))                    return xchg::PARMTYPE_INT_2;
        if (_typeinfo->type() == typeid(math::Vector3i))                    return xchg::PARMTYPE_INT_3;
        if (_typeinfo->type() == typeid(math::Vector4i))                    return xchg::PARMTYPE_INT_4;
        if (_typeinfo->type() == typeid(float))                             return xchg::PARMTYPE_FLOAT;
        if (_typeinfo->type() == typeid(math::Vector2f))                    return xchg::PARMTYPE_FLOAT_2;
        if (_typeinfo->type() == typeid(math::Vector4f))                    return xchg::PARMTYPE_FLOAT_4;
        if (_typeinfo->type() == typeid(math::Affine3f))                    return xchg::PARMTYPE_AFFINE_3;
        if (_typeinfo->type() == typeid(std::string))                       return xchg::PARMTYPE_STRING;
        if (_typeinfo->type() == typeid(xchg::BoundingBox))                 return xchg::PARMTYPE_BOUNDINGBOX;
        if (_typeinfo->type() == typeid(xchg::DataStream))                  return xchg::PARMTYPE_DATASTREAM;
        if (_typeinfo->type() == typeid(std::shared_ptr<smks::xchg::Data>)) return xchg::PARMTYPE_DATA;
        if (_typeinfo->type() == typeid(xchg::IdSet))                       return xchg::PARMTYPE_IDSET;
        if (_typeinfo->type() == typeid(xchg::ObjectPtr))                   return xchg::PARMTYPE_OBJECT;
    }
    return xchg::UNKNOWN_PARMTYPE;
}

bool
parm::BuiltIn::compatibleWith(xchg::ParmType parmType) const
{
    switch (parmType)
    {
    case xchg::PARMTYPE_BOOL:           return compatibleWith<bool>();
    case xchg::PARMTYPE_UINT:           return compatibleWith<unsigned int>();
    case xchg::PARMTYPE_INT:            return compatibleWith<int>();
    case xchg::PARMTYPE_CHAR:           return compatibleWith<char>();
    case xchg::PARMTYPE_SIZE:           return compatibleWith<size_t>();
    case xchg::PARMTYPE_INT_2:          return compatibleWith<math::Vector2i>();
    case xchg::PARMTYPE_INT_3:          return compatibleWith<math::Vector3i>();
    case xchg::PARMTYPE_INT_4:          return compatibleWith<math::Vector4i>();
    case xchg::PARMTYPE_FLOAT:          return compatibleWith<float>();
    case xchg::PARMTYPE_FLOAT_2:        return compatibleWith<math::Vector2f>();
    case xchg::PARMTYPE_FLOAT_4:        return compatibleWith<math::Vector4f>();
    case xchg::PARMTYPE_AFFINE_3:       return compatibleWith<math::Affine3f>();
    case xchg::PARMTYPE_STRING:         return compatibleWith<std::string>();
    case xchg::PARMTYPE_IDSET:          return compatibleWith<xchg::IdSet>();
    case xchg::PARMTYPE_DATASTREAM:     return compatibleWith<xchg::DataStream>();
    case xchg::PARMTYPE_BOUNDINGBOX:    return compatibleWith<xchg::BoundingBox>();
    case xchg::PARMTYPE_DATA:           return compatibleWith<xchg::Data::Ptr>();
    case xchg::PARMTYPE_OBJECT:         return compatibleWith<xchg::ObjectPtr>();
    default:                            return false;
    }
}
