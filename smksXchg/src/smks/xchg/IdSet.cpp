// ========================================================================= //
// Copyright 2016-2018 SUPAMONKS_STUDIO                                      //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
//                                                                           //
// ========================================================================= //
#include <set>
#include <algorithm>

#include "smks/xchg/IdSet.hpp"

namespace smks { namespace xchg
{
    class IdSet::PImpl:
        public std::set<uint32_t>
    {
    public:
        inline
        bool
        operator==(const IdSet::PImpl&) const;
    };

    struct IdSet::const_iterator::PImpl
    {
        IdSet::PImpl::const_iterator it;
    };
}
}

using namespace smks;

/////////////////////
// class IdSet::PImpl
/////////////////////
bool
xchg::IdSet::PImpl::operator==(const IdSet::PImpl& other) const
{
    if (size() != other.size())
        return false;
    for (const_iterator it1 = begin(), it2 = other.begin(); it1 != end(); ++it1, ++it2)
        if (*it1 != *it2)
            return false;
    return true;
}

//////////////////////////////
// class IdSet::const_iterator
//////////////////////////////
xchg::IdSet::const_iterator::const_iterator():
    _pImpl(new PImpl)
{ }

xchg::IdSet::const_iterator::const_iterator(const const_iterator& other):
    _pImpl(new PImpl)
{
    (*_pImpl) = (*other._pImpl);
}

xchg::IdSet::const_iterator::~const_iterator()
{
    delete _pImpl;
}

xchg::IdSet::const_iterator&
xchg::IdSet::const_iterator::operator=(const const_iterator& other)
{
    (*_pImpl) = (*other._pImpl);
    return *this;
}

bool
xchg::IdSet::const_iterator::operator==(const const_iterator& other) const
{
    return _pImpl->it == other._pImpl->it;
}

xchg::IdSet::const_iterator&
xchg::IdSet::const_iterator::operator++()
{
    ++(_pImpl->it);
    return *this;
}

uint32_t
xchg::IdSet::const_iterator::operator*() const
{
    return *(_pImpl->it);
}

//////////////
// class IdSet
//////////////
xchg::IdSet::IdSet():
    _pImpl(new PImpl)
{ }

xchg::IdSet::IdSet(uint32_t id):
    _pImpl(new PImpl)
{
    insert(id);
}

xchg::IdSet::IdSet(const IdSet& other):
    _pImpl(new PImpl)
{
    (*_pImpl) = (*other._pImpl);
}

xchg::IdSet::~IdSet()
{
    delete _pImpl;
}

xchg::IdSet&
xchg::IdSet::operator=(const IdSet& other)
{
    (*_pImpl) = (*other._pImpl);
    return *this;
}

void
xchg::IdSet::clear()
{
    _pImpl->clear();
}

void
xchg::IdSet::insert(uint32_t id)
{
    _pImpl->insert(id);
}

void
xchg::IdSet::erase(uint32_t id)
{
    _pImpl->erase(id);
}


xchg::IdSet::const_iterator
xchg::IdSet::find(uint32_t id) const
{
    const_iterator ret;

    ret._pImpl->it = _pImpl->find(id);

    return ret;
}

xchg::IdSet::const_iterator
xchg::IdSet::begin() const
{
    const_iterator ret;

    ret._pImpl->it = _pImpl->begin();

    return ret;
}

xchg::IdSet::const_iterator
xchg::IdSet::end() const
{
    const_iterator ret;

    ret._pImpl->it = _pImpl->end();

    return ret;
}

bool
xchg::IdSet::empty() const
{
    return _pImpl->empty();
}

size_t
xchg::IdSet::size() const
{
    return _pImpl->size();
}

// static
void
xchg::IdSet::diff(const IdSet& previous, const IdSet& next, IdSet& added, IdSet& removed)
{
    added.clear();
    removed.clear();

    for (const_iterator it = previous.begin(); it != previous.end(); ++it)
        if (next.find(*it) == next.end())
            removed.insert(*it);
    for (const_iterator it = next.begin(); it != next.end(); ++it)
        if (previous.find(*it) == previous.end())
            added.insert(*it);
}

// static
void
xchg::IdSet::merge(xchg::IdSet& dst, const xchg::IdSet& src)
{
    for (xchg::IdSet::const_iterator id = src.begin(); id != src.end(); ++id)
        dst.insert(*id);
}

// static
void
xchg::IdSet::set_union(const IdSet& a, const IdSet& b, xchg::IdSet& ret)
{
    if (a.size() > b.size())
    {
        ret = a;
        for (xchg::IdSet::const_iterator id = b.begin(); id != b.end(); ++id)
            ret.insert(*id);
    }
    else
    {
        ret = b;
        for (xchg::IdSet::const_iterator id = a.begin(); id != a.end(); ++id)
            ret.insert(*id);
    }
}

bool
xchg::IdSet::operator==(const IdSet& other) const
{
    return _pImpl->operator==(*other._pImpl);
}
