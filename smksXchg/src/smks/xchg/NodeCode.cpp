// ========================================================================= //
// Copyright 2016-2018 SUPAMONKS_STUDIO                                      //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
//                                                                           //
// ========================================================================= //
#include <string>
#include <smks/util/string/getId.hpp>
#include "smks/xchg/NodeCode.hpp"

namespace smks { namespace xchg
{
    class NodeCode::PImpl
    {
    private:
        const std::string   _str;
        const unsigned int  _id;
        const NodeClass     _nodeClasses;
        const bool          _pythonExposed;
    public:
        inline
        PImpl(const std::string&    str,
              NodeClass             nodeClasses,
              bool                  pythonExposed):
            _str            (str),
            _id             (util::string::getId(str.c_str())),
            _nodeClasses    (nodeClasses),
            _pythonExposed  (pythonExposed)
        { }

        inline
        const std::string&
        str() const
        {
            return _str;
        }

        inline
        unsigned int
        id() const
        {
            return _id;
        }

        inline
        NodeClass
        nodeClasses() const
        {
            return _nodeClasses;
        }

        inline
        bool
        pythonExposed() const
        {
            return _pythonExposed;
        }
    };
}
}

using namespace smks;

// static
xchg::NodeCode::Ptr
xchg::NodeCode::create(const char*  str,
                       NodeClass    nodeClasses,
                       bool         pythonExposed)
{
    Ptr ptr(new NodeCode(str, nodeClasses, pythonExposed));
    return ptr;
}

xchg::NodeCode::NodeCode(const char*    str,
                         NodeClass      nodeClasses,
                         bool           pythonExposed):
    _pImpl(new PImpl(str, nodeClasses, pythonExposed))
{ }

xchg::NodeCode::~NodeCode()
{
    delete _pImpl;
}

unsigned int
xchg::NodeCode::id() const
{
    return _pImpl->id();
}

const char*
xchg::NodeCode::c_str() const
{
    return _pImpl->str().c_str();
}

xchg::NodeClass
xchg::NodeCode::nodeClasses() const
{
    return _pImpl->nodeClasses();
}

bool
xchg::NodeCode::pythonExposed() const
{
    return _pImpl->pythonExposed();
}
