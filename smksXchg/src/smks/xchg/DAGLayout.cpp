// ========================================================================= //
// Copyright 2016-2018 SUPAMONKS_STUDIO                                      //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
//                                                                           //
// ========================================================================= //
#include <cassert>
#include <sstream>
#include <boost/unordered_map.hpp>
#include <smks/sys/Exception.hpp>

#include "smks/xchg/DAGLayout.hpp"

#include <iostream>

namespace smks { namespace xchg
{
    class DAGLayout::PImpl
    {
    private:
        typedef boost::unordered_map<unsigned int, size_t>  IdToIndex;
    private:
        size_t              _size;
        size_t              _capacity;
        size_t              _firstDirty;
        IdToIndex           _idToIndex;
        unsigned int*       _ids;
        size_t*             _parent;
        size_t*             _firstChild;
        size_t*             _nextSibling;
        size_t*             _prevSibling;
        AbstractData::Ptr   _data;

    public:
        PImpl();
        ~PImpl();

        inline
        void
        attach(AbstractData::Ptr const& value)
        {
            _data = value;
        }

        inline
        void
        dispose();

        inline
        void
        add(unsigned int node, unsigned int parent);

        inline
        void
        remove(unsigned int node);

        inline
        bool
        has(unsigned int node) const
        {
            return index(node) != INVALID_IDX;
        }

        inline
        unsigned int
        parent(unsigned int node) const
        {
            const size_t idx = index(node);
            return idx != INVALID_IDX
                ? id(_parent[idx])
                : 0;
        }

        inline
        unsigned int
        firstChild(unsigned int node) const
        {
            const size_t idx = index(node);
            return idx != INVALID_IDX
                ? id(_firstChild[idx])
                : 0;
        }

        inline
        unsigned int
        previousSibling(unsigned int node) const
        {
            const size_t idx = index(node);
            return idx != INVALID_IDX
                ? id(_prevSibling[idx])
                : 0;
        }

        inline
        unsigned int
        nextSibling(unsigned int node) const
        {
            const size_t idx = index(node);
            return idx != INVALID_IDX
                ? id(_nextSibling[idx])
                : 0;
        }

        inline
        size_t
        index(unsigned int node) const
        {
            IdToIndex::const_iterator const& it = _idToIndex.find(node);
            return it != _idToIndex.end()
                ? it->second
                : INVALID_IDX;
        }

        inline
        unsigned int
        id(size_t idx) const
        {
            if (idx == INVALID_IDX)
                return 0;   // invalid node ID
            assert(idx < _size);
            return _ids[idx];
        }

        inline
        size_t
        size() const
        {
            return _size;
        }

        inline
        size_t
        capacity() const
        {
            return _capacity;
        }

        inline
        void
        swapIndices(size_t idx1, size_t idx2);

    private:
        inline
        void
        moveIndex(size_t dst, size_t src);

    public:
        inline
        void
        dirty(unsigned int node);

        inline
        bool
        isDirty(unsigned int node) const
        {
            const size_t idx = index(node);
            return idx != INVALID_IDX && _firstDirty <= idx;
        }

        inline
        void
        clean();

    private:
        inline
        void
        reserve(size_t capacity);

    public:
        inline
        std::ostream&
        dump(std::ostream&) const;

        inline
        std::ostream&
        dumpIndexMaps(std::ostream&) const;
    };
}
}

using namespace smks;

/*static*/ const size_t xchg::DAGLayout::INVALID_IDX = -1;

/////////////////////////
// class DAGLayout::PImpl
/////////////////////////
xchg::DAGLayout::PImpl::PImpl():
    _size       (0),
    _capacity   (0),
    _firstDirty (0),
    _idToIndex  (),
    _ids        (nullptr),
    _parent     (nullptr),
    _firstChild (nullptr),
    _nextSibling(nullptr),
    _prevSibling(nullptr),
    _data       (nullptr)
{ }

xchg::DAGLayout::PImpl::~PImpl()
{
    reserve(0); // delete memory if allocated
    _data.reset();
}

void
xchg::DAGLayout::PImpl::dispose()
{
    while (_size > 0)
    {
        for (size_t idx = 0; idx < _size; ++idx)
            if (_parent[idx] == INVALID_IDX)    // look for roots
            {
                remove(id(idx));
                break;
            }
    }
    assert(_size == 0);
    assert(_idToIndex.empty());
    reserve(0);
    assert(_capacity == 0);
}

void
xchg::DAGLayout::PImpl::add(unsigned int node, unsigned int parent)
{
    if (node == 0)
        throw sys::Exception("Invalid node ID.", "DAG Layout Node Addition");
    if (parent == node)
    {
        std::stringstream sstr;
        sstr << "Node ID = " << node << " cannot be its own parent in DAG layout.";
        throw sys::Exception(sstr.str().c_str(), "DAG Layout Node Addition");
    }

    if (_idToIndex.find(node) != _idToIndex.end())
    {
        std::stringstream sstr;
        sstr << "Node ID = " << node << " is already present in DAG layout.";
        throw sys::Exception(sstr.str().c_str(), "DAG Layout Node Addition");
    }

    size_t parentIdx = INVALID_IDX;
    if (parent > 0)
    {
        parentIdx = index(parent);
        if (parentIdx == INVALID_IDX)
        {
            std::stringstream sstr;
            sstr << "Parent ID = " << parent << " not found in DAG layout.";
            throw sys::Exception(sstr.str().c_str(), "DAG Layout Node Addition");
        }
    }

    if (_size == _capacity)
        reserve(_capacity > 0 ? _capacity << 1 : 16);
    assert(_size < _capacity);

    _idToIndex  [node]  = _size;
    _ids        [_size] = node;
    _parent     [_size] = parentIdx;
    _firstChild [_size] = INVALID_IDX;
    _nextSibling[_size] = INVALID_IDX;

    if (parentIdx != INVALID_IDX)
    {
        if (_firstChild[parentIdx] == INVALID_IDX)
        {
            // update parent
            _firstChild[parentIdx] = _size;

            _prevSibling[_size] = INVALID_IDX; // no sibling since parent's first child
        }
        else
        {
            // find previous sibling from parent's children
            size_t sibling = _firstChild[parentIdx];
            while (_nextSibling[sibling] != INVALID_IDX)
                sibling = _nextSibling[sibling];

            // update previous sibling
            _nextSibling[sibling] = _size;

            _prevSibling[_size] = sibling;
        }
    }

    ++_size;
}

void
xchg::DAGLayout::PImpl::remove(unsigned int node)
{
    if (!has(node))
        return;

    // first, recursively remove children
    // (must use permanent node IDs to iterate instead of volatile indices)
    while (firstChild(node))
        remove(firstChild(node));

    // at this point, indices will not change from outside the scope of the function.

    // swap with last entry
    const size_t last = _size-1;

    swapIndices(index(node), last);
    // forget about last entry
    if (_firstChild[_parent[last]] == last)
        _firstChild[_parent[last]] = _nextSibling[last];
    if (valid(_prevSibling[last]))
        _nextSibling[_prevSibling[last]] = _nextSibling[last];
    if (valid(_nextSibling[last]))
        _prevSibling[_nextSibling[last]] = _prevSibling[last];

    _idToIndex.erase(_ids[last]);
    --_size;
    _firstDirty = std::min(_firstDirty, _size);
}

void
xchg::DAGLayout::PImpl::dirty(unsigned int node)
{
    assert(has(node));

    if (_firstDirty <= index(node))
        return;

    // recursively dirty all children
    // (must use permanent node IDs to iterate instead of volatile indices)
    unsigned int child = firstChild(node);
    while (child)
    {
        unsigned int sibling = nextSibling(child);
        dirty(child);
        child = sibling;
    }

    // at this point, indices will not change from outside the scope of the function.
    swapIndices(index(node), _firstDirty - 1);
    --_firstDirty;
}

void
xchg::DAGLayout::PImpl::swapIndices(size_t idx1, size_t idx2)
{
    assert(idx1 != INVALID_IDX);
    assert(idx2 != INVALID_IDX);
    if (idx1 == idx2)
        return;

    if (_size == _capacity)
        reserve(_capacity > 0 ? _capacity << 1 : 16);

    //dump(std::cout << "about to swap " << idx1 << " <-> " << idx2 << "\n") << std::endl;
    moveIndex(_size, idx1);
    //dumpIndexMaps(std::cout << "moved " << idx1 << " -> size = " << _size << "\n") << std::endl;
    moveIndex(idx1, idx2);
    //dumpIndexMaps(std::cout << "moved " << idx2 << " -> " << idx1 << "\n") << std::endl;
    moveIndex(idx2, _size);
    //dumpIndexMaps(std::cout << "moved size = " << _size << " -> " << idx2 << "\n") << std::endl;
    //dump(std::cout << "swapped " << idx1 << " <-> " << idx2 << "\n") << std::endl;
}

void
xchg::DAGLayout::PImpl::moveIndex(size_t dst, size_t src)
{
    if (_data)
        _data->moveEntry(dst, src);

    assert(src < _capacity);
    assert(dst < _capacity);

    const unsigned int id = _ids[src];

    assert(id > 0);
    assert(_idToIndex.find(id) != _idToIndex.end());
    assert(_idToIndex[id] == src);

    _ids        [dst]   = id;
    _parent     [dst]   = _parent       [src];
    _firstChild [dst]   = _firstChild   [src];
    _nextSibling[dst]   = _nextSibling  [src];
    _prevSibling[dst]   = _prevSibling  [src];

    // update node adjancency
    size_t child = _firstChild  [dst];
    while (valid(child))
    {
        _parent[child] = dst;
        child = _nextSibling[child];
    }

    if (valid(_parent[dst]) &&
        _firstChild[_parent[dst]] == src)
        _firstChild[_parent[dst]] = dst;
    if (valid(_prevSibling[dst]))
        _nextSibling[_prevSibling[dst]] = dst;
    if (valid(_nextSibling[dst]))
        _prevSibling[_nextSibling[dst]] = dst;

    _idToIndex[id] = dst;
}

void
xchg::DAGLayout::PImpl::clean()
{
    for (size_t idx = _firstDirty; idx < _size; ++idx)
    {
        while (_parent[idx] != INVALID_IDX &&
            idx < _parent[idx])
            swapIndices(idx, _parent[idx]);

        if (_data)
        {
            if (_parent[idx] != INVALID_IDX)
                _data->recomputeEntry(idx, _parent[idx]);
            else
                _data->recomputeEntry(idx);
        }
    }
    _firstDirty = _size;
}

void
xchg::DAGLayout::PImpl::reserve(size_t capacity)
{
    if (capacity == _capacity)
        return; // useless
    if (capacity > 0 &&
        capacity < _capacity)
        return; // useless

    if (capacity == 0 &&
        _capacity > 0)
    {
        // deallocate memory
        if (_ids)           delete[] _ids;
        if (_parent)        delete[] _parent;
        if (_firstChild)    delete[] _firstChild;
        if (_nextSibling)   delete[] _nextSibling;
        if (_prevSibling)   delete[] _prevSibling;
        _ids            = nullptr;
        _parent         = nullptr;
        _firstChild     = nullptr;
        _nextSibling    = nullptr;
        _prevSibling    = nullptr;

        _idToIndex.clear();
        _firstDirty = 0;
        _size       = 0;
        _capacity   = 0;

        if (_data)
            _data->resize(capacity);

        return;
    }

    unsigned int* ids   = new unsigned int[capacity];
    size_t* parent      = new size_t[capacity];
    size_t* firstChild  = new size_t[capacity];
    size_t* nextSibling = new size_t[capacity];
    size_t* prevSibling = new size_t[capacity];

    if (_capacity > 0)
    {
        memcpy(ids,         _ids,           _capacity * sizeof(unsigned int));
        memcpy(parent,      _parent,        _capacity * sizeof(size_t));
        memcpy(firstChild,  _firstChild,    _capacity * sizeof(size_t));
        memcpy(nextSibling, _nextSibling,   _capacity * sizeof(size_t));
        memcpy(prevSibling, _prevSibling,   _capacity * sizeof(size_t));
    }

    for (size_t i = _capacity; i < capacity; ++i)
    {
        ids         [i] = 0;
        parent      [i] = INVALID_IDX;
        firstChild  [i] = INVALID_IDX;
        nextSibling [i] = INVALID_IDX;
        prevSibling [i] = INVALID_IDX;
    }

    if (_ids)           delete[] _ids;
    if (_parent)        delete[] _parent;
    if (_firstChild)    delete[] _firstChild;
    if (_nextSibling)   delete[] _nextSibling;
    if (_prevSibling)   delete[] _prevSibling;
    _ids            = ids;
    _parent         = parent;
    _firstChild     = firstChild;
    _nextSibling    = nextSibling;
    _prevSibling    = prevSibling;

    _capacity = capacity;
    if (_data)
        _data->resize(capacity);
}

std::ostream&
xchg::DAGLayout::PImpl::dump(std::ostream& out) const
{
    out << "[\t^\tv\t<\t>\t]\n--------------------------------------------\n";
    for (size_t idx = 0; idx < _size; ++idx)
    {
        const unsigned int node = id(idx);
        if (node == 0)
            continue;
        assert(index(node) == idx);
        out
            << "[\t"
            << (_parent[idx]        == INVALID_IDX ? " " : std::to_string(_parent[idx]))        << "\t"
            << (_firstChild[idx]    == INVALID_IDX ? " " : std::to_string(_firstChild[idx]))    << "\t"
            << (_prevSibling[idx]   == INVALID_IDX ? " " : std::to_string(_prevSibling[idx]))   << "\t"
            << (_nextSibling[idx]   == INVALID_IDX ? " " : std::to_string(_nextSibling[idx]))   << "\t]\t["
            << idx << "]\t'" << node << "'"
            << "\t(" << index(node) << ")"
            << "\t" << (isDirty(node) ? "dirty" : "")
            << "\n";
    }
    return out;
}

std::ostream&
xchg::DAGLayout::PImpl::dumpIndexMaps(std::ostream& out) const
{
    //out << "--------------------------------------------\n";
    //out << "size = " << _size << "\tcapacity = " << _capacity << "\n";
    //for (size_t idx = 0; idx < _capacity; ++idx)
    //{
    //  const unsigned int node = id(idx);
    //  IdToIndex::const_iterator const& it = _idToIndex.find(_ids[i]);
    //  if (it == _idToIndex.end())
    //      out << "\t[" << i << "] id = " << _ids[i] << "\n";
    //  else
    //      out << "\t[" << i << "] id = " << _ids[i] << " (idx = " << it->second << ")\n";
    //}
    return out;
}

//////////////////
// class DAGLayout
//////////////////
xchg::DAGLayout::DAGLayout():
    _pImpl(new PImpl)
{ }

xchg::DAGLayout::~DAGLayout()
{
    delete _pImpl;
}

void
xchg::DAGLayout::dispose()
{
    _pImpl->dispose();
}

void
xchg::DAGLayout::attach(AbstractData::Ptr const& data)
{
    _pImpl->attach(data);
}

void
xchg::DAGLayout::add(unsigned int node, unsigned int parent)
{
    _pImpl->add(node, parent);
}

void
xchg::DAGLayout::remove(unsigned int node)
{
    _pImpl->remove(node);
}

bool
xchg::DAGLayout::has(unsigned int node) const
{
    return _pImpl->has(node);
}

unsigned int
xchg::DAGLayout::parent(unsigned int idx) const
{
    return _pImpl->parent(idx);
}

unsigned int
xchg::DAGLayout::firstChild(unsigned int idx) const
{
    return _pImpl->firstChild(idx);
}

unsigned int
xchg::DAGLayout::previousSibling(unsigned int idx) const
{
    return _pImpl->previousSibling(idx);
}

unsigned int
xchg::DAGLayout::nextSibling(unsigned int idx) const
{
    return _pImpl->nextSibling(idx);
}

size_t
xchg::DAGLayout::index(unsigned int node) const
{
    return _pImpl->index(node);
}

unsigned int
xchg::DAGLayout::id(size_t idx) const
{
    return _pImpl->id(idx);
}

size_t
xchg::DAGLayout::capacity() const
{
    return _pImpl->capacity();
}

size_t
xchg::DAGLayout::size() const
{
    return _pImpl->size();
}

void
xchg::DAGLayout::dirty(unsigned int node)
{
    _pImpl->dirty(node);
}

bool
xchg::DAGLayout::isDirty(unsigned int node) const
{
    return _pImpl->isDirty(node);
}

void
xchg::DAGLayout::clean()
{
    _pImpl->clean();
}

void
xchg::DAGLayout::swapIndices(size_t idx1, size_t idx2)
{
    _pImpl->swapIndices(idx1, idx2);
}

std::ostream&
xchg::DAGLayout::dump(std::ostream& out) const
{
    return _pImpl->dump(out);
}

// static
bool
xchg::DAGLayout::valid(size_t idx)
{
    return idx != INVALID_IDX;
}
