// ========================================================================= //
// Copyright 2016-2018 SUPAMONKS_STUDIO                                      //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
//                                                                           //
// ========================================================================= //
#include <algorithm>
#include <smks/sys/sync/Atomic.hpp>
#include <smks/sys/sync/Mutex.hpp>
#include <smks/sys/sysinfo.hpp>

#include "smks/xchg/ConsoleProgress.hpp"

namespace smks { namespace xchg
{
    class ConsoleProgress::PImpl
    {
    private:
        sys::sync::MutexSys _mutex;         //!< Mutex to protect progress output
        size_t              _numElements;   //!< Total number of elements to process
        size_t              _numDrawn;      //!< Number of progress characters drawn
        size_t              _terminalWidth; //!< Width of terminal window in characters

    public:
        inline
        PImpl();

        inline
        void
        begin(const char*, size_t totalSteps);

        inline
        bool
        proceed(size_t currentStep);

        inline
        void
        end(const char*);
    private:
        inline
        void
        drawEmptyBar();
    };
}
}

using namespace smks;

////////////////////////
// class ConsoleProgress::PImpl
////////////////////////
xchg::ConsoleProgress::PImpl::PImpl():
    _numElements    (0),
    _numDrawn       (0),
    _terminalWidth  (0)
{ }

void
xchg::ConsoleProgress::PImpl::begin(const char*, size_t totalSteps)
{
    sys::sync::Lock<sys::sync::MutexSys> lock(_mutex);
    //---
    _numElements    = totalSteps;
    _numDrawn       = 0;
    _terminalWidth  = sys::getTerminalWidth();

    drawEmptyBar();
}

bool
xchg::ConsoleProgress::PImpl::proceed(size_t currentStep)
{
    if (_numElements == 0)
        return false;

    sys::sync::Lock<sys::sync::MutexSys> lock(_mutex);
    //---
    ssize_t curTerminalWidth = sys::getTerminalWidth();
    if (_terminalWidth != size_t(curTerminalWidth))
    {
        drawEmptyBar();
        _terminalWidth = curTerminalWidth;
    }

    //size_t    cur         = _curElement++;
    size_t  width       = std::max(static_cast<size_t>(2), static_cast<size_t>(curTerminalWidth-2));
    size_t  numToDraw   = currentStep * width / std::max(static_cast<size_t>(1), static_cast<size_t>(_numElements - 1));
    for (size_t i = _numDrawn; i < numToDraw; ++i)
        std::cout << "+" << std::flush;

    _numDrawn = numToDraw;

    return true;    // continue
}

void
xchg::ConsoleProgress::PImpl::end(const char*)
{
    sys::sync::Lock<sys::sync::MutexSys> lock(_mutex);
    //---
    ssize_t curTerminalWidth = sys::getTerminalWidth();
    if (_terminalWidth != size_t(curTerminalWidth))
    {
        drawEmptyBar();
        _terminalWidth = curTerminalWidth;
    }

    size_t numToDraw = _terminalWidth - 2 - _numDrawn;
    for (size_t i = _numDrawn; i < numToDraw; ++i)
        std::cout << "+" << std::flush;

    std::cout << "]" << std::endl;
}

void
xchg::ConsoleProgress::PImpl::drawEmptyBar()
{
    std::cout << "\r[" << std::flush;
    const size_t width = std::max(static_cast<size_t>(2), static_cast<size_t>(_terminalWidth - 2));
    for (size_t i = 0; i < width; ++i)
      std::cout << " ";
    std::cout << "]\r";
    std::cout << "[" << std::flush;
    _numDrawn = 0;
}


/////////////////
// class ConsoleProgress
/////////////////
xchg::ConsoleProgress::ConsoleProgress():
    _pImpl(new PImpl())
{ }

xchg::ConsoleProgress::~ConsoleProgress()
{
    delete _pImpl;
}

void
xchg::ConsoleProgress::begin(const char* str, size_t totalSteps)
{
    _pImpl->begin(str, totalSteps);
}

bool
xchg::ConsoleProgress::proceed(size_t currentStep)
{
    return _pImpl->proceed(currentStep);
}

void
xchg::ConsoleProgress::end(const char* str)
{
    _pImpl->end(str);
}
