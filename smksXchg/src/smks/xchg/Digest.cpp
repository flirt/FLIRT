// ========================================================================= //
// Copyright 2016-2018 SUPAMONKS_STUDIO                                      //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
//                                                                           //
// ========================================================================= //
#include <iostream>
#include <iomanip>
#include <memory.h>
#include "smks/xchg/Digest.hpp"

namespace smks { namespace xchg
{
    class Digest::PImpl
    {
    private:
        uint64_t _words[2];
    public:
        PImpl();

        explicit
        PImpl(const uint8_t[16]);

        inline
        void
        copy(const Digest::PImpl&);

        inline
        bool
        operator==(const Digest::PImpl&) const;

        inline
        bool
        operator<(const Digest::PImpl&) const;

        inline
        bool
        asBool() const;

        inline
        std::ostream&
        print(std::ostream&) const;
    };
}
}

using namespace smks;

////////////////////////////
// class xchg::Digest::PImpl
////////////////////////////
xchg::Digest::PImpl::PImpl()
{
    memset(_words, 0, sizeof(uint64_t) << 1);
}

// explicit
xchg::Digest::PImpl::PImpl(const uint8_t d[16])
{
    int i = 0;
    for (int j = 0; j < 2; ++j)
    {
        uint64_t& word_j = _words[j];
        for (int k = 0; k < 8; ++k)
        {
            word_j = word_j | (static_cast<uint64_t>(d[i]) << (k << 3));
            ++i;
        }
    }
}

void
xchg::Digest::PImpl::copy(const Digest::PImpl& other)
{
    memcpy(_words, other._words, sizeof(uint64_t) << 1);
}

bool
xchg::Digest::PImpl::operator==(const Digest::PImpl& other) const
{
    return _words[0] == other._words[0] &&
        _words[1] == other._words[1];
}

inline
bool
xchg::Digest::PImpl::operator<(const Digest::PImpl& other) const
{
    return _words[0] < other._words[0]
        ? true
        : (_words[0] > other._words[0]
            ? false
            : _words[1] < other._words[1]);
}

bool
xchg::Digest::PImpl::asBool() const
{
    return _words[0] != 0 &&
        _words[1] != 0;
}

std::ostream&
xchg::Digest::PImpl::print(std::ostream& out) const
{
    for (int i = 0; i < 2; ++i)
        out
            << std::setfill('0') << std::setw(2) << std::hex << std::noshowbase
            << _words[i];
    return out;
}

/////////////////////
// class xchg::Digest
/////////////////////
xchg::Digest::Digest():
    _pImpl(new PImpl())
{ }

// explicit
xchg::Digest::Digest(const uint8_t d[16]):
    _pImpl(new PImpl(d))
{ }

xchg::Digest::Digest(const Digest& other):
    _pImpl(new PImpl())
{
    _pImpl->copy(*other._pImpl);
}

xchg::Digest::~Digest()
{
    delete _pImpl;
}

xchg::Digest&
xchg::Digest::operator=(const Digest& other)
{
    _pImpl->copy(*other._pImpl);
    return *this;
}


xchg::Digest::operator bool() const
{
    return _pImpl->asBool();
}

bool
xchg::Digest::operator==(const Digest& other) const
{
    return (*_pImpl) == (*other._pImpl);
}

bool
xchg::Digest::operator<(const Digest& other) const
{
    return (*_pImpl) == (*other._pImpl);
}

std::ostream&
xchg::Digest::print(std::ostream& out) const
{
    return _pImpl->print(out);
}
