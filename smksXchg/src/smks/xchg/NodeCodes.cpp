// ========================================================================= //
// Copyright 2016-2018 SUPAMONKS_STUDIO                                      //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
//                                                                           //
// ========================================================================= //
#include <vector>
#include "smks/xchg/NodeCode.hpp"
#include "smks/xchg/NodeCodes.hpp"
#include "smks/xchg/NodeCodeMap.hpp"

namespace smks { namespace xchg
{
    typedef const NodeCode& (*Creator)();   //<! creator function for declaring built-in parameters
    std::vector<Creator> _registered;       //<! list of all built-in declaration calls
    //<! structure used to fill the list of declaration calls
    struct RegisterCreator
    {
        explicit inline
        RegisterCreator(const Creator& func)
        {
            _registered.push_back(func);
        }
    };
}
}
//--------------------------------------------------------------------------
#define NODE_CODE_DECL(_NAME_, _NODE_CLASSES_, _FLAGS_ )                    \
    const NodeCode& _NAME_ ()                                               \
    {                                                                       \
        static NodeCode::Ptr ptr = nodeCodes().add(                         \
            NodeCode::create("" #_NAME_ "", _NODE_CLASSES_, _FLAGS_));      \
        return *ptr;                                                        \
    }                                                                       \
    RegisterCreator _registered_nodecode_##_NAME_##(_NAME_);
//--------------------------------------------------------------------------

namespace smks { namespace xchg { namespace code
{
    // NODE_CODE_DECL (class name, node classes, exposed in python?)
    //---------------
    NODE_CODE_DECL (ambient             , NodeClass::LIGHT              , true)
    NODE_CODE_DECL (ambientOcclusion    , NodeClass::RENDERPASS         , true)
    NODE_CODE_DECL (basicHair           , NodeClass::MATERIAL           , true)
    NODE_CODE_DECL (blend               , NodeClass::MATERIAL           , true)
    NODE_CODE_DECL (box                 , NodeClass::PIXEL_FILTER       , true)
    NODE_CODE_DECL (brushedMetal        , NodeClass::MATERIAL           , true)
    NODE_CODE_DECL (bspline             , NodeClass::PIXEL_FILTER       , true)
    #if defined(_DEBUG)
    NODE_CODE_DECL (debugRayTexcoords   , NodeClass::RENDERPASS         , true)
    NODE_CODE_DECL (debugRayGeomNormal  , NodeClass::RENDERPASS         , true)
    NODE_CODE_DECL (debugHitGeomNormal  , NodeClass::RENDERPASS         , true)
    NODE_CODE_DECL (debugHitShadNormal  , NodeClass::RENDERPASS         , true)
    #endif // defined(_DEBUG)
    NODE_CODE_DECL (default             , NodeClass::RENDERER | NodeClass::FRAMEBUFFER | NodeClass::INTEGRATOR  ,  true)
    NODE_CODE_DECL (depthOfField        , NodeClass::CAMERA             , false)
    NODE_CODE_DECL (dielectric          , NodeClass::MATERIAL           , true)
    NODE_CODE_DECL (diffuseColor        , NodeClass::RENDERPASS         , true)
    NODE_CODE_DECL (directional         , NodeClass::LIGHT              , true)
    NODE_CODE_DECL (distant             , NodeClass::LIGHT              , true)
    NODE_CODE_DECL (eye                 , NodeClass::MATERIAL           , true)
    NODE_CODE_DECL (eyeNormal           , NodeClass::RENDERPASS         , true)
    NODE_CODE_DECL (eyePosition         , NodeClass::RENDERPASS         , true)
    NODE_CODE_DECL (eyeTangent          , NodeClass::RENDERPASS         , true)
    NODE_CODE_DECL (facingRatio         , NodeClass::RENDERPASS         , true)
    NODE_CODE_DECL (filmic              , NodeClass::TONEMAPPER         , true)
    NODE_CODE_DECL (gammaCorrection     , NodeClass::TONEMAPPER         , true)
    NODE_CODE_DECL (glass               , NodeClass::MATERIAL           , true)
    NODE_CODE_DECL (glossyColor         , NodeClass::RENDERPASS         , true)
    NODE_CODE_DECL (hdri                , NodeClass::LIGHT              , true)
    NODE_CODE_DECL (idCoverage          , NodeClass::RENDERPASS         , false)
    NODE_CODE_DECL (lightPath           , NodeClass::RENDERPASS         , false)
    NODE_CODE_DECL (matte               , NodeClass::MATERIAL           , true)
    NODE_CODE_DECL (mesh                , NodeClass::LIGHT              , true)
    NODE_CODE_DECL (metal               , NodeClass::MATERIAL           , true)
    NODE_CODE_DECL (metallicPaint       , NodeClass::MATERIAL           , true)
    NODE_CODE_DECL (mirror              , NodeClass::MATERIAL           , true)
    NODE_CODE_DECL (microfacet          , NodeClass::MATERIAL           , true)
    NODE_CODE_DECL (multijittered       , NodeClass::SAMPLER_FACTORY    , true)
    NODE_CODE_DECL (nearest             , NodeClass::TEXTURE            , true)
    NODE_CODE_DECL (normal              , NodeClass::RENDERPASS         , true)
    NODE_CODE_DECL (obj                 , NodeClass::MATERIAL           , true)
    NODE_CODE_DECL (ocio                , NodeClass::TONEMAPPER         , true)
    NODE_CODE_DECL (pinhole             , NodeClass::CAMERA             , false)
    NODE_CODE_DECL (plastic             , NodeClass::MATERIAL           , true)
    NODE_CODE_DECL (point               , NodeClass::LIGHT              , true)
    NODE_CODE_DECL (position            , NodeClass::RENDERPASS         , true)
    NODE_CODE_DECL (rayHistoFusion      , NodeClass::DENOISER           , true)
    NODE_CODE_DECL (shadowBias          , NodeClass::RENDERPASS         , true)
    NODE_CODE_DECL (shadows             , NodeClass::RENDERPASS         , true)
    NODE_CODE_DECL (singularColor       , NodeClass::RENDERPASS         , true)
    NODE_CODE_DECL (spot                , NodeClass::LIGHT              , true)
    NODE_CODE_DECL (subsurface          , NodeClass::RENDERPASS         , true)
    NODE_CODE_DECL (tangent             , NodeClass::RENDERPASS         , true)
    NODE_CODE_DECL (texcoords           , NodeClass::RENDERPASS         , true)
    NODE_CODE_DECL (thinDielectric      , NodeClass::MATERIAL           , true)
    NODE_CODE_DECL (thinGlass           , NodeClass::MATERIAL           , true)
    NODE_CODE_DECL (translucent         , NodeClass::MATERIAL           , true)
    NODE_CODE_DECL (translucentSS       , NodeClass::MATERIAL           , true)
    NODE_CODE_DECL (velvet              , NodeClass::MATERIAL           , true)

    void
    initializeAllNodeCodes()
    {
        for (size_t i = 0; i < _registered.size(); ++i)
            _registered[i]();
    }
}
}
}
