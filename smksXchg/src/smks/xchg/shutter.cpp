// ========================================================================= //
// Copyright 2016-2018 SUPAMONKS_STUDIO                                      //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
//                                                                           //
// ========================================================================= //
#include "std/to_string_xchg.hpp"
#include "smks/xchg/shutter.hpp"

using namespace smks;

bool
xchg::getShutterOpenAndClose(float          time,
                             float          framerate,
                             ShutterType    shutterType,
                             float          shutterBias,
                             float&         shutterOpen,
                             float&         shutterClose)
{
    shutterOpen     = time;
    shutterClose    = time;

    float bias = shutterBias;
    if (bias < 0.0f)
        bias = 0.0f;
    if (bias > 1.0f)
        bias = 1.0f;

    if (!(framerate > 0.0f))
        return false;

    const float rFramerate = 1.0f / framerate;
    switch (shutterType)
    {
    default:
        return false;
    case BACKWARD_SHUTTER:
        {
            shutterClose    = time;
            shutterOpen     = shutterClose - shutterBias * rFramerate;
        } break;
    case CENTERED_SHUTTER:
        {
            const float dtime = shutterType * 0.5f * rFramerate;
            shutterOpen     = time - dtime;
            shutterClose    = time + dtime;
        } break;
    case FORWARD_SHUTTER:
        {
            shutterOpen     = time;
            shutterClose    = shutterOpen + shutterType * rFramerate;
        } break;
    }
    return shutterOpen < shutterClose;
}

xchg::ShutterType
xchg::getShutterType(const char* str)
{
    for (int i = 0; i < NUM_SHUTTER_TYPES; ++i)
    {
        const ShutterType typ = static_cast<ShutterType>(i);
        if (strcmp(str, std::to_string(typ).c_str()) == 0)
            return typ;
    }
    return UNKNOWN_SHUTTER;
}
