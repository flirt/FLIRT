// ========================================================================= //
// Copyright 2016-2018 SUPAMONKS_STUDIO                                      //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
//                                                                           //
// ========================================================================= //
#include <memory>

#include <smks/sys/Exception.hpp>
#include <smks/math/types.hpp>
#include "smks/xchg/BoundingBox.hpp"
#include "smks/xchg/DataStream.hpp"
#include "smks/xchg/IdSet.hpp"
#include "smks/xchg/ObjectPtr.hpp"
#include "smks/xchg/getParmType.hpp"

using namespace smks;

xchg::ParmType
xchg::getParmType(const std::type_info& typeinfo)
{
    if      (typeinfo == typeid(bool))                          return xchg::PARMTYPE_BOOL;
    else if (typeinfo == typeid(size_t))                        return xchg::PARMTYPE_SIZE;
    else if (typeinfo == typeid(char))                          return xchg::PARMTYPE_CHAR;
    else if (typeinfo == typeid(int))                           return xchg::PARMTYPE_INT;
    else if (typeinfo == typeid(unsigned int))                  return xchg::PARMTYPE_UINT;
    else if (typeinfo == typeid(math::Vector2i))                return xchg::PARMTYPE_INT_2;
    else if (typeinfo == typeid(math::Vector3i))                return xchg::PARMTYPE_INT_3;
    else if (typeinfo == typeid(math::Vector4i))                return xchg::PARMTYPE_INT_4;
    else if (typeinfo == typeid(float))                         return xchg::PARMTYPE_FLOAT;
    else if (typeinfo == typeid(math::Vector2f))                return xchg::PARMTYPE_FLOAT_2;
    else if (typeinfo == typeid(math::Vector4f))                return xchg::PARMTYPE_FLOAT_4;
    else if (typeinfo == typeid(math::Affine3f))                return xchg::PARMTYPE_AFFINE_3;
    else if (typeinfo == typeid(std::string))                   return xchg::PARMTYPE_STRING;
    else if (typeinfo == typeid(xchg::BoundingBox))             return xchg::PARMTYPE_BOUNDINGBOX;
    else if (typeinfo == typeid(xchg::DataStream))              return xchg::PARMTYPE_DATASTREAM;
    else if (typeinfo == typeid(std::shared_ptr<xchg::Data>))   return xchg::PARMTYPE_DATA;
    else if (typeinfo == typeid(xchg::IdSet))                   return xchg::PARMTYPE_IDSET;
    else if (typeinfo == typeid(xchg::ObjectPtr))               return xchg::PARMTYPE_OBJECT;
    throw sys::Exception(
        "Unsupported parameter data type.",
        "std::type_info -> ParmType");
}
