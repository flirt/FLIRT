// ========================================================================= //
// Copyright 2016-2018 SUPAMONKS_STUDIO                                      //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
//                                                                           //
// ========================================================================= //
#include "smks/sys/initialize_xchg.hpp"
#include "../parm/initializeAllBuiltIns.hpp"
#include "../xchg/initializeAllNodeCodes.hpp"

using namespace smks;

namespace smks { namespace sys
{
    static bool _smksXchgInitialized = false;
}
}

void
smks::sys::initializeSmksXchg()
{
    if (_smksXchgInitialized)
        return;

    parm::initializeAllBuiltIns();
    xchg::code::initializeAllNodeCodes();

    _smksXchgInitialized = true;
}

void
smks::sys::finalizeSmksXchg()
{
    if (!_smksXchgInitialized)
        return;

    _smksXchgInitialized = false;
}

bool
smks::sys::smksXchgInitialized()
{
    return _smksXchgInitialized;
}
