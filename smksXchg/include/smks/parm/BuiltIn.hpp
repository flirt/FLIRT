// ========================================================================= //
// Copyright 2016-2018 SUPAMONKS_STUDIO                                      //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
//                                                                           //
// ========================================================================= //
#pragma once

#include <memory>
#include <smks/util/string/getId.hpp>
#include <smks/sys/Exception.hpp>

#include "smks/sys/configure_xchg.hpp"
#include "smks/parm/validators.hpp"
#include "smks/parm/ParmFlags.hpp"
#include "smks/xchg/ParmType.hpp"

namespace smks { namespace parm
{
    class BuiltIn
    {
    public:
        typedef std::shared_ptr<BuiltIn>    Ptr;
        typedef std::weak_ptr<BuiltIn>      WPtr;

    private:
        /////////////////////////
        // class AbstractTypeInfo
        /////////////////////////
        class AbstractTypeInfo
        {
        public:
            virtual inline
            ~AbstractTypeInfo()
            { }

            virtual
            const std::type_info&
            type() const = 0;

            virtual
            ParmFlags
            flags() const = 0;

            virtual
            const void* // void-based type erasure
            defaultValue() const = 0;
        };

        /////////////////
        // class TypeInfo
        /////////////////
        template<typename ValueType>
        class TypeInfo:
            public AbstractTypeInfo
        {
        private:
            const ValueType _value;
            const ParmFlags _flags;

        public:
            inline
            TypeInfo(ParmFlags flags, const ValueType& value):
                _value(value),
                _flags(flags & parm::PARM_DESCR_FLAGS)  // store only descriptive flags
            { }

            inline
            const std::type_info&
            type() const
            {
                return typeid(ValueType);
            }

            inline
            ParmFlags
            flags() const
            {
                return _flags;
            }

            inline
            const void* // void-based type erasure
            defaultValue() const
            {
                return reinterpret_cast<const void*>(&_value);
            }
        };

    private:
        std::string             _name;
        unsigned int            _id;
        AbstractTypeInfo*       _typeinfo;
        AbstractValueValidator* _validator;

    public:
        template<typename ValueType> static inline
        Ptr
        create(const char* name, ParmFlags flags, const ValueType& value, AbstractValueValidator* validator = nullptr)
        {
            Ptr ptr(new BuiltIn);
            ptr->initialize<ValueType>(name, flags, value, validator);
            return ptr;
        }

        static inline
        Ptr
        create(const char* name)
        {
            Ptr ptr(new BuiltIn);
            ptr->initialize(name);
            return ptr;
        }

    private:
        inline
        BuiltIn():
            _name       (),
            _id         (0),
            _typeinfo   (nullptr),
            _validator  (nullptr)
        { }
        // non-copyable
        BuiltIn(const BuiltIn&);
        BuiltIn& operator=(const BuiltIn&);

        template<typename ValueType>  inline
        void
        initialize(const char* name, ParmFlags flags, const ValueType& value, AbstractValueValidator* validator)
        {
            dispose();

            _name       = name ? std::string(name) : std::string();
            _id         = util::string::getId(name);
            _typeinfo   = new TypeInfo<ValueType>(flags, value);
            _validator  = validator;

            if (_validator)
            {
                if (_validator->type() != _typeinfo->type())
                {
                    std::stringstream sstr;
                    sstr
                        << "Clashing parameter types "
                        << "between '" << _name << "' declaration "
                        << "and its value validator.";
                    throw sys::Exception(sstr.str().c_str(), "Built-In Parameter Initialization");
                }
                if (!_validator->isOK(&value, flags))
                {
                    std::stringstream sstr;
                    sstr
                        << "Incorrect default value "
                        << "for parameter '" << _name << "'.";
                    throw sys::Exception(sstr.str().c_str(), "Built-In Parameter Initialization");
                }
            }
        }

        inline
        void
        initialize(const char* name)
        {
            dispose();

            _name   = name ? std::string(name) : std::string();
            _id     = util::string::getId(name);
        }

        inline
        void
        dispose()
        {
            if (_typeinfo)  delete _typeinfo;   _typeinfo   = nullptr;
            if (_validator) delete _validator;  _validator  = nullptr;
        }

    public:
        inline
        ~BuiltIn()
        {
            dispose();
        }

        inline
        const char *const
        c_str() const
        {
            return _name.c_str();
        }

        inline
        unsigned int
        id() const
        {
            return _id;
        }

        FLIRT_XCHG_EXPORT
        xchg::ParmType
        type() const;

        inline
        const std::type_info*
        typeinfo() const
        {
            return _typeinfo
                ? &_typeinfo->type()
                : nullptr;
        }

        inline
        ParmFlags
        flags() const
        {
            return _typeinfo
                ? _typeinfo->flags()
                : NO_PARM_FLAG;
        }

        template<typename ValueType> inline
        bool
        defaultValue(ValueType& out) const
        {
            if (_typeinfo == nullptr ||                 // no type control
                _typeinfo->type() != typeid(ValueType)) // parameter value's type and built-in's type clash!
                return false;

            out = *reinterpret_cast<const ValueType*>(_typeinfo->defaultValue());
            return true;
        }

        template<typename ValueType> inline
        bool
        compatibleWith() const
        {
            return _typeinfo == nullptr || // no actual type control
                _typeinfo->type() == typeid(ValueType);
        }

        FLIRT_XCHG_EXPORT
        bool
        compatibleWith(xchg::ParmType) const;

        template<typename ValueType> inline
        bool
        validate(const ValueType& in, ParmFlags flags, ValueType& out) const
        {
            out = in;

            if (compatibleWith<ValueType>())
            {
                if (_validator)
                    _validator->validate(&in, flags, &out); // conform parameter value with declared range constraints
                return true;
            }
            else
                return false;
        }

    };
}
}
