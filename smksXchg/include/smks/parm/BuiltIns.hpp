// ========================================================================= //
// Copyright 2016-2018 SUPAMONKS_STUDIO                                      //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
//                                                                           //
// ========================================================================= //
#pragma once

#include "smks/sys/configure_xchg.hpp"

namespace smks { namespace parm
{
    class BuiltIn;

    FLIRT_XCHG_EXPORT_VAR const BuiltIn& accumulate ();
    FLIRT_XCHG_EXPORT_VAR const BuiltIn& backplate ();
    FLIRT_XCHG_EXPORT_VAR const BuiltIn& backplatePath ();
    FLIRT_XCHG_EXPORT_VAR const BuiltIn& backScattering ();
    FLIRT_XCHG_EXPORT_VAR const BuiltIn& backScatteringMap ();
    FLIRT_XCHG_EXPORT_VAR const BuiltIn& boundingBox ();
    FLIRT_XCHG_EXPORT_VAR const BuiltIn& cameraId ();
    FLIRT_XCHG_EXPORT_VAR const BuiltIn& castsShadows ();
    FLIRT_XCHG_EXPORT_VAR const BuiltIn& center ();
    FLIRT_XCHG_EXPORT_VAR const BuiltIn& code ();
    FLIRT_XCHG_EXPORT_VAR const BuiltIn& config ();
    FLIRT_XCHG_EXPORT_VAR const BuiltIn& contrastLevel ();
    FLIRT_XCHG_EXPORT_VAR const BuiltIn& currentSampleIdx ();
    FLIRT_XCHG_EXPORT_VAR const BuiltIn& currentTime ();
    FLIRT_XCHG_EXPORT_VAR const BuiltIn& currentlyVisible ();
    FLIRT_XCHG_EXPORT_VAR const BuiltIn& denoiserId ();
    FLIRT_XCHG_EXPORT_VAR const BuiltIn& diffuseReflectance ();
    FLIRT_XCHG_EXPORT_VAR const BuiltIn& diffuseReflectanceMap ();
    FLIRT_XCHG_EXPORT_VAR const BuiltIn& diffuseTransmission ();
    FLIRT_XCHG_EXPORT_VAR const BuiltIn& diffuseTransmissionMap ();
    FLIRT_XCHG_EXPORT_VAR const BuiltIn& displayName ();
    FLIRT_XCHG_EXPORT_VAR const BuiltIn& displayViewName ();
    FLIRT_XCHG_EXPORT_VAR const BuiltIn& extIOR ();
    FLIRT_XCHG_EXPORT_VAR const BuiltIn& extTransmission ();
    FLIRT_XCHG_EXPORT_VAR const BuiltIn& exposureFstops ();
    //---
    FLIRT_XCHG_EXPORT_VAR const BuiltIn& eyeCorneaIOR ();
    FLIRT_XCHG_EXPORT_VAR const BuiltIn& eyeGlobeColor ();
    FLIRT_XCHG_EXPORT_VAR const BuiltIn& eyeHighlightAngSizeD ();
    FLIRT_XCHG_EXPORT_VAR const BuiltIn& eyeHighlightColor ();
    FLIRT_XCHG_EXPORT_VAR const BuiltIn& eyeHighlightSmoothness ();
    FLIRT_XCHG_EXPORT_VAR const BuiltIn& eyeHighlightCoordsD ();
    FLIRT_XCHG_EXPORT_VAR const BuiltIn& eyeIrisAngSizeD ();
    FLIRT_XCHG_EXPORT_VAR const BuiltIn& eyeIrisColor ();
    FLIRT_XCHG_EXPORT_VAR const BuiltIn& eyeIrisDepth ();
    FLIRT_XCHG_EXPORT_VAR const BuiltIn& eyeIrisDimensions ();
    FLIRT_XCHG_EXPORT_VAR const BuiltIn& eyeIrisEdgeBlur ();
    FLIRT_XCHG_EXPORT_VAR const BuiltIn& eyeIrisEdgeOffset ();
    FLIRT_XCHG_EXPORT_VAR const BuiltIn& eyeIrisNormalSmoothness ();
    FLIRT_XCHG_EXPORT_VAR const BuiltIn& eyeIrisRotationD ();
    FLIRT_XCHG_EXPORT_VAR const BuiltIn& eyePupilBlur ();
    FLIRT_XCHG_EXPORT_VAR const BuiltIn& eyePupilDimensions ();
    FLIRT_XCHG_EXPORT_VAR const BuiltIn& eyePupilSize ();
    FLIRT_XCHG_EXPORT_VAR const BuiltIn& eyeRadius ();
    //---
    FLIRT_XCHG_EXPORT_VAR const BuiltIn& falloff ();
    FLIRT_XCHG_EXPORT_VAR const BuiltIn& falloffMap ();
    FLIRT_XCHG_EXPORT_VAR const BuiltIn& filePath ();
    FLIRT_XCHG_EXPORT_VAR const BuiltIn& forciblyVisible ();
    FLIRT_XCHG_EXPORT_VAR const BuiltIn& frameBufferId ();
    FLIRT_XCHG_EXPORT_VAR const BuiltIn& frameData ();
    FLIRT_XCHG_EXPORT_VAR const BuiltIn& framerate ();
    FLIRT_XCHG_EXPORT_VAR const BuiltIn& fresnelName ();
    FLIRT_XCHG_EXPORT_VAR const BuiltIn& fresnelValue ();
    FLIRT_XCHG_EXPORT_VAR const BuiltIn& fresnelWeight ();
    FLIRT_XCHG_EXPORT_VAR const BuiltIn& fresnelWeightMap ();
    FLIRT_XCHG_EXPORT_VAR const BuiltIn& front ();
    FLIRT_XCHG_EXPORT_VAR const BuiltIn& focalDistance ();
    FLIRT_XCHG_EXPORT_VAR const BuiltIn& forceInitialization ();
    FLIRT_XCHG_EXPORT_VAR const BuiltIn& gamma ();
    FLIRT_XCHG_EXPORT_VAR const BuiltIn& glitterColor ();
    FLIRT_XCHG_EXPORT_VAR const BuiltIn& glitterColorMap ();
    FLIRT_XCHG_EXPORT_VAR const BuiltIn& glitterSpread ();
    FLIRT_XCHG_EXPORT_VAR const BuiltIn& glitterSpreadMap ();
    FLIRT_XCHG_EXPORT_VAR const BuiltIn& globalInitialization ();
    FLIRT_XCHG_EXPORT_VAR const BuiltIn& globallyVisible ();
    FLIRT_XCHG_EXPORT_VAR const BuiltIn& globalNumSamples ();
    FLIRT_XCHG_EXPORT_VAR const BuiltIn& glossyReflectance ();
    FLIRT_XCHG_EXPORT_VAR const BuiltIn& glossyReflectanceMap ();
    //---
    FLIRT_XCHG_EXPORT_VAR const BuiltIn& glCount ();
    FLIRT_XCHG_EXPORT_VAR const BuiltIn& glIndices ();
    FLIRT_XCHG_EXPORT_VAR const BuiltIn& glNormals ();
    FLIRT_XCHG_EXPORT_VAR const BuiltIn& glPositions ();
    FLIRT_XCHG_EXPORT_VAR const BuiltIn& glTexcoords ();
    FLIRT_XCHG_EXPORT_VAR const BuiltIn& glVertices ();
    FLIRT_XCHG_EXPORT_VAR const BuiltIn& glWidths ();
    FLIRT_XCHG_EXPORT_VAR const BuiltIn& glWireIndices ();
    //---
    FLIRT_XCHG_EXPORT_VAR const BuiltIn& halfAngleD ();
    FLIRT_XCHG_EXPORT_VAR const BuiltIn& halfWidth ();
    FLIRT_XCHG_EXPORT_VAR const BuiltIn& hdriMap ();
    FLIRT_XCHG_EXPORT_VAR const BuiltIn& height ();
    FLIRT_XCHG_EXPORT_VAR const BuiltIn& horizonScatteringColor ();
    FLIRT_XCHG_EXPORT_VAR const BuiltIn& horizonScatteringColorMap ();
    FLIRT_XCHG_EXPORT_VAR const BuiltIn& horizonScatteringFalloff ();
    FLIRT_XCHG_EXPORT_VAR const BuiltIn& horizonScatteringFalloffMap ();
    FLIRT_XCHG_EXPORT_VAR const BuiltIn& id ();
    FLIRT_XCHG_EXPORT_VAR const BuiltIn& illumMask ();
    FLIRT_XCHG_EXPORT_VAR const BuiltIn& image ();
    FLIRT_XCHG_EXPORT_VAR const BuiltIn& inheritsTransforms ();
    FLIRT_XCHG_EXPORT_VAR const BuiltIn& initialization ();
    FLIRT_XCHG_EXPORT_VAR const BuiltIn& inputId0 ();
    FLIRT_XCHG_EXPORT_VAR const BuiltIn& inputId1 ();
    FLIRT_XCHG_EXPORT_VAR const BuiltIn& inputColorspace ();
    FLIRT_XCHG_EXPORT_VAR const BuiltIn& integratorId ();
    FLIRT_XCHG_EXPORT_VAR const BuiltIn& intensity ();
    FLIRT_XCHG_EXPORT_VAR const BuiltIn& intIOR ();
    FLIRT_XCHG_EXPORT_VAR const BuiltIn& intTransmission ();
    FLIRT_XCHG_EXPORT_VAR const BuiltIn& IOR ();
    FLIRT_XCHG_EXPORT_VAR const BuiltIn& IORreal ();
    FLIRT_XCHG_EXPORT_VAR const BuiltIn& IORimag ();
    FLIRT_XCHG_EXPORT_VAR const BuiltIn& irradiance ();
    FLIRT_XCHG_EXPORT_VAR const BuiltIn& isProgressive ();
    FLIRT_XCHG_EXPORT_VAR const BuiltIn& iteration ();
    FLIRT_XCHG_EXPORT_VAR const BuiltIn& layerColor ();
    FLIRT_XCHG_EXPORT_VAR const BuiltIn& layerColorMap ();
    FLIRT_XCHG_EXPORT_VAR const BuiltIn& layerIOR ();
    FLIRT_XCHG_EXPORT_VAR const BuiltIn& lensRadius ();
    FLIRT_XCHG_EXPORT_VAR const BuiltIn& manipulatorFlags ();
    FLIRT_XCHG_EXPORT_VAR const BuiltIn& materialId ();
    FLIRT_XCHG_EXPORT_VAR const BuiltIn& maxAngleD ();
    FLIRT_XCHG_EXPORT_VAR const BuiltIn& maxCount ();
    FLIRT_XCHG_EXPORT_VAR const BuiltIn& maxDistance ();
    FLIRT_XCHG_EXPORT_VAR const BuiltIn& maxPatchDistance ();
    FLIRT_XCHG_EXPORT_VAR const BuiltIn& maxRayDepth ();
    FLIRT_XCHG_EXPORT_VAR const BuiltIn& maxRayIntensity ();
    FLIRT_XCHG_EXPORT_VAR const BuiltIn& maxTimeSteps ();
    FLIRT_XCHG_EXPORT_VAR const BuiltIn& minAngleD ();
    FLIRT_XCHG_EXPORT_VAR const BuiltIn& minPatchMatchCount ();
    FLIRT_XCHG_EXPORT_VAR const BuiltIn& minRayContribution ();
    FLIRT_XCHG_EXPORT_VAR const BuiltIn& mouseFlags ();
    FLIRT_XCHG_EXPORT_VAR const BuiltIn& NDFName ();
    FLIRT_XCHG_EXPORT_VAR const BuiltIn& normalMap ();
    FLIRT_XCHG_EXPORT_VAR const BuiltIn& numHistoBins ();
    FLIRT_XCHG_EXPORT_VAR const BuiltIn& numIndexBufferSamples ();
    FLIRT_XCHG_EXPORT_VAR const BuiltIn& numLightsForDirect ();
    FLIRT_XCHG_EXPORT_VAR const BuiltIn& numMipmapLevels ();
    FLIRT_XCHG_EXPORT_VAR const BuiltIn& numSamples ();
    FLIRT_XCHG_EXPORT_VAR const BuiltIn& numSampleSets ();
    FLIRT_XCHG_EXPORT_VAR const BuiltIn& numVertexBufferSamples ();
    FLIRT_XCHG_EXPORT_VAR const BuiltIn& opacity ();
    FLIRT_XCHG_EXPORT_VAR const BuiltIn& opacityMap ();
    FLIRT_XCHG_EXPORT_VAR const BuiltIn& patchHalfSize ();
    FLIRT_XCHG_EXPORT_VAR const BuiltIn& parmName ();
    FLIRT_XCHG_EXPORT_VAR const BuiltIn& parmValue ();
    FLIRT_XCHG_EXPORT_VAR const BuiltIn& pixelFilterId ();
    FLIRT_XCHG_EXPORT_VAR const BuiltIn& positions ();
    FLIRT_XCHG_EXPORT_VAR const BuiltIn& primaryVisibility ();
    FLIRT_XCHG_EXPORT_VAR const BuiltIn& priority ();
    FLIRT_XCHG_EXPORT_VAR const BuiltIn& radiance ();
    FLIRT_XCHG_EXPORT_VAR const BuiltIn& receivesShadows ();
    FLIRT_XCHG_EXPORT_VAR const BuiltIn& reflectance ();
    FLIRT_XCHG_EXPORT_VAR const BuiltIn& reflectanceMap ();
    FLIRT_XCHG_EXPORT_VAR const BuiltIn& reflectivity ();
    FLIRT_XCHG_EXPORT_VAR const BuiltIn& regex ();
    FLIRT_XCHG_EXPORT_VAR const BuiltIn& rendererId ();
    FLIRT_XCHG_EXPORT_VAR const BuiltIn& rendering ();
    FLIRT_XCHG_EXPORT_VAR const BuiltIn& renderPassIds ();
    FLIRT_XCHG_EXPORT_VAR const BuiltIn& rotateD ();
    FLIRT_XCHG_EXPORT_VAR const BuiltIn& rotateOrder ();
    FLIRT_XCHG_EXPORT_VAR const BuiltIn& roughness ();
    FLIRT_XCHG_EXPORT_VAR const BuiltIn& roughnessMap ();
    FLIRT_XCHG_EXPORT_VAR const BuiltIn& roughnessX ();
    FLIRT_XCHG_EXPORT_VAR const BuiltIn& roughnessXMap ();
    FLIRT_XCHG_EXPORT_VAR const BuiltIn& roughnessY ();
    FLIRT_XCHG_EXPORT_VAR const BuiltIn& roughnessYMap ();
    //---
    FLIRT_XCHG_EXPORT_VAR const BuiltIn& rtCount ();
    FLIRT_XCHG_EXPORT_VAR const BuiltIn& rtCounts ();
    FLIRT_XCHG_EXPORT_VAR const BuiltIn& rtFaceVertices ();
    FLIRT_XCHG_EXPORT_VAR const BuiltIn& rtIndices ();
    FLIRT_XCHG_EXPORT_VAR const BuiltIn& rtPathIds ();
    FLIRT_XCHG_EXPORT_VAR const BuiltIn& rtPositions ();
    FLIRT_XCHG_EXPORT_VAR const BuiltIn& rtNormals ();
    FLIRT_XCHG_EXPORT_VAR const BuiltIn& rtScalps ();
    FLIRT_XCHG_EXPORT_VAR const BuiltIn& rtSubdivisionMethod ();
    FLIRT_XCHG_EXPORT_VAR const BuiltIn& rtTangentsX ();
    FLIRT_XCHG_EXPORT_VAR const BuiltIn& rtTangentsY ();
    FLIRT_XCHG_EXPORT_VAR const BuiltIn& rtTesselationRate ();
    FLIRT_XCHG_EXPORT_VAR const BuiltIn& rtTexcoords ();
    FLIRT_XCHG_EXPORT_VAR const BuiltIn& rtVertices ();
    FLIRT_XCHG_EXPORT_VAR const BuiltIn& rtWidths ();
    //---
    FLIRT_XCHG_EXPORT_VAR const BuiltIn& samplerFactoryId ();
    FLIRT_XCHG_EXPORT_VAR const BuiltIn& searchWindowHalfSize ();
    FLIRT_XCHG_EXPORT_VAR const BuiltIn& selected ();
    FLIRT_XCHG_EXPORT_VAR const BuiltIn& selection ();
    FLIRT_XCHG_EXPORT_VAR const BuiltIn& selectorId ();
    FLIRT_XCHG_EXPORT_VAR const BuiltIn& selectorIds ();
    FLIRT_XCHG_EXPORT_VAR const BuiltIn& scale ();
    FLIRT_XCHG_EXPORT_VAR const BuiltIn& shadowBias ();
    FLIRT_XCHG_EXPORT_VAR const BuiltIn& shapeId ();
    FLIRT_XCHG_EXPORT_VAR const BuiltIn& showProgress ();
    FLIRT_XCHG_EXPORT_VAR const BuiltIn& shutter ();
    FLIRT_XCHG_EXPORT_VAR const BuiltIn& shutterType ();
    FLIRT_XCHG_EXPORT_VAR const BuiltIn& sourceId ();
    FLIRT_XCHG_EXPORT_VAR const BuiltIn& specularExponent ();
    FLIRT_XCHG_EXPORT_VAR const BuiltIn& specularExponentMap ();
    FLIRT_XCHG_EXPORT_VAR const BuiltIn& specularReflectance ();
    FLIRT_XCHG_EXPORT_VAR const BuiltIn& specularReflectanceMap ();
    FLIRT_XCHG_EXPORT_VAR const BuiltIn& storeAsHalf ();
    FLIRT_XCHG_EXPORT_VAR const BuiltIn& storeRawRgba ();
    FLIRT_XCHG_EXPORT_VAR const BuiltIn& storeRgbaAsHalf ();
    FLIRT_XCHG_EXPORT_VAR const BuiltIn& subdivisionLevel ();
    FLIRT_XCHG_EXPORT_VAR const BuiltIn& subsurfaceId ();
    FLIRT_XCHG_EXPORT_VAR const BuiltIn& thickness ();
    FLIRT_XCHG_EXPORT_VAR const BuiltIn& thicknessMap ();
    FLIRT_XCHG_EXPORT_VAR const BuiltIn& tileImages ();
    FLIRT_XCHG_EXPORT_VAR const BuiltIn& timeBounds ();
    FLIRT_XCHG_EXPORT_VAR const BuiltIn& toneMapperId ();
    FLIRT_XCHG_EXPORT_VAR const BuiltIn& translate ();
    FLIRT_XCHG_EXPORT_VAR const BuiltIn& transform ();
    FLIRT_XCHG_EXPORT_VAR const BuiltIn& transformUV ();
    FLIRT_XCHG_EXPORT_VAR const BuiltIn& transmission ();
    FLIRT_XCHG_EXPORT_VAR const BuiltIn& transmissionMap ();
    FLIRT_XCHG_EXPORT_VAR const BuiltIn& transmissivity ();
    FLIRT_XCHG_EXPORT_VAR const BuiltIn& transmissivityMap ();
    FLIRT_XCHG_EXPORT_VAR const BuiltIn& transparentShadows ();
    FLIRT_XCHG_EXPORT_VAR const BuiltIn& up ();
    FLIRT_XCHG_EXPORT_VAR const BuiltIn& upVector ();
    FLIRT_XCHG_EXPORT_VAR const BuiltIn& usedAsPoint ();
    FLIRT_XCHG_EXPORT_VAR const BuiltIn& userAttribInitialization ();
    FLIRT_XCHG_EXPORT_VAR const BuiltIn& userColorName ();
    FLIRT_XCHG_EXPORT_VAR const BuiltIn& verbosityLevel ();
    FLIRT_XCHG_EXPORT_VAR const BuiltIn& vertices ();
    FLIRT_XCHG_EXPORT_VAR const BuiltIn& vignetting ();
    FLIRT_XCHG_EXPORT_VAR const BuiltIn& visibleInReflections ();
    FLIRT_XCHG_EXPORT_VAR const BuiltIn& visibleInRefractions ();
    FLIRT_XCHG_EXPORT_VAR const BuiltIn& weight ();
    FLIRT_XCHG_EXPORT_VAR const BuiltIn& weightMap ();
    FLIRT_XCHG_EXPORT_VAR const BuiltIn& width ();
    FLIRT_XCHG_EXPORT_VAR const BuiltIn& widths ();
    FLIRT_XCHG_EXPORT_VAR const BuiltIn& worldTransform ();
    FLIRT_XCHG_EXPORT_VAR const BuiltIn& xAspectRatio ();
    FLIRT_XCHG_EXPORT_VAR const BuiltIn& xMax ();
    FLIRT_XCHG_EXPORT_VAR const BuiltIn& xMin ();
    FLIRT_XCHG_EXPORT_VAR const BuiltIn& yFieldOfViewD ();
    FLIRT_XCHG_EXPORT_VAR const BuiltIn& yMax ();
    FLIRT_XCHG_EXPORT_VAR const BuiltIn& yMin ();
    FLIRT_XCHG_EXPORT_VAR const BuiltIn& zFar ();
    FLIRT_XCHG_EXPORT_VAR const BuiltIn& zNear ();
}
}

