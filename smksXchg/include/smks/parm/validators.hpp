// ========================================================================= //
// Copyright 2016-2018 SUPAMONKS_STUDIO                                      //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
//                                                                           //
// ========================================================================= //
#pragma once

#include <memory>
#include <smks/math/types.hpp>
#include "smks/xchg/ObjectPtr.hpp"
#include "smks/xchg/ActionState.hpp"
#include "smks/parm/ParmFlags.hpp"

namespace smks { namespace parm {
    ///////////////////////////////
    // class AbstractValueValidator
    ///////////////////////////////
    class AbstractValueValidator
    {
    public:
        virtual
        ~AbstractValueValidator()
        { }

        virtual
        const std::type_info&
        type() const = 0;

        virtual
        bool
        isOK(const void*, ParmFlags) const = 0; // void-based type erasure

        virtual
        void
        validate(const void*, ParmFlags, void*) const = 0;  // void-based type erasure
    };

    ////////////////////////////
    // class TypedValueValidator
    ////////////////////////////
    template <typename ValueType>
    class TypedValueValidator:
        public AbstractValueValidator
    {
    public:
        inline
        const std::type_info&
        type() const
        {
            return typeid(ValueType);
        }

        inline
        bool
        isOK(const void* value_, ParmFlags flags) const
        {
            const ValueType& value = *(const ValueType*)(value_);

            return _isOK(value, flags);
        }

        inline
        void
        validate(const void* in_, ParmFlags flags, void* out_) const
        {
            const ValueType&    in  = *(const ValueType*)(in_);
            ValueType&          out = *(ValueType*)(out_);

            _validate(in, flags, out);
        }

    protected:
        virtual
        bool
        _isOK(const ValueType&, ParmFlags) const = 0;

        virtual
        void
        _validate(const ValueType&, ParmFlags, ValueType&) const = 0;
    };

    /////////////////////////////////////
    // class CompoundValidator<ValueType>
    /////////////////////////////////////
    template <typename ValueType>
    class CompoundValidator:
        public TypedValueValidator<ValueType>
    {
    private:
        TypedValueValidator<ValueType>* _validators[2];
    public:
        inline
        CompoundValidator(TypedValueValidator<ValueType>* validatorA,
                          TypedValueValidator<ValueType>* validatorB)
        {
            assert(validatorA);
            assert(validatorB);
            _validators[0] = validatorA;
            _validators[1] = validatorB;
        }
        inline
        ~CompoundValidator()
        {
            for (size_t i = 0; i < 2; ++i)
            { delete _validators[i]; _validators[i] = nullptr; }
        }
    private:
        virtual inline
        bool
        _isOK(const ValueType& value, ParmFlags f) const
        {
            return _validators[0]->isOK(&value, f) && _validators[1]->isOK(&value, f);
        }
        virtual inline
        void
        _validate(const ValueType& in, ParmFlags f, ValueType& out) const
        {
            for (size_t i = 0; i < 2; ++i)
                _validators[i]->validate(&in, f, &out);
        }
    };

    /////////////////
    // class Multiple
    /////////////////
    template <typename ValueType>
    class Multiple:
        public TypedValueValidator<ValueType>
    {
    private:
        const ValueType _factor;
    public:
        explicit inline
        Multiple(const ValueType& factor):
            _factor(factor)
        {
            assert(_factor > 0);
        }
    protected:
        virtual inline
        bool
        _isOK(const ValueType& value, ParmFlags) const
        {
            return value % _factor == 0;
        }
        virtual inline
        void
        _validate(const ValueType& in, ParmFlags, ValueType& out) const
        {
            out = (in / _factor) * _factor;
        }
    };

    //////////////////////
    // class LowerBound<T>
    //////////////////////
    template <typename ValueType>
    class LowerBound:
        public TypedValueValidator<ValueType>
    {
    protected:
        const ValueType _lower;
    public:
        explicit inline
        LowerBound(const ValueType& lower):
            _lower(lower)
        { }
    protected:
        virtual inline
        bool
        _isOK(const ValueType& value, ParmFlags) const
        {
            return !(value < _lower);
        }
        virtual inline
        void
        _validate(const ValueType& in, ParmFlags, ValueType& out) const
        {
            out = in < _lower ? _lower : in;
        }
    };

    // Vector2f specialization
    template<> inline LowerBound<math::Vector2f>::LowerBound(const math::Vector2f& lower): _lower(lower) { }
    template<> inline bool LowerBound<math::Vector2f>::_isOK(const math::Vector2f& value, ParmFlags) const
    {
        return (value.array() < _lower.array()).any() == false;
    }
    template<> inline void LowerBound<math::Vector2f>::_validate(const math::Vector2f& in, ParmFlags, math::Vector2f& out) const
    {
        out = _lower.cwiseMax(in);
    }
    // Vector4f specialization
    template<> inline LowerBound<math::Vector4f>::LowerBound(const math::Vector4f& lower): _lower(lower) { }
    template<> inline bool LowerBound<math::Vector4f>::_isOK(const math::Vector4f& value, ParmFlags) const
    {
        return (value.array() < _lower.array()).any() == false;
    }
    template<> inline void LowerBound<math::Vector4f>::_validate(const math::Vector4f& in, ParmFlags, math::Vector4f& out) const
    {
        out = _lower.cwiseMax(in);
    }

    //////////////////////
    // class UpperBound<T>
    //////////////////////
    template <typename ValueType>
    class UpperBound:
        public TypedValueValidator<ValueType>
    {
    protected:
        const ValueType _upper;
    public:
        explicit inline
        UpperBound(const ValueType& upper):
            _upper(upper)
        { }
    protected:
        virtual inline
        bool
        _isOK(const ValueType& value, ParmFlags) const
        {
            return !(_upper < value);
        }
        virtual inline
        void
        _validate(const ValueType& in, ParmFlags, ValueType& out) const
        {
            out = _upper < in ? _upper : in;
        }
    };

    // Vector2f specialization
    template<> UpperBound<math::Vector2f>::UpperBound(const math::Vector2f& upper): _upper(upper) { }
    template<> bool UpperBound<math::Vector2f>::_isOK(const math::Vector2f& value, ParmFlags) const
    {
        return (_upper.array() < value.array()).any() == false;
    }
    template<> void UpperBound<math::Vector2f>::_validate(const math::Vector2f& in, ParmFlags, math::Vector2f& out) const
    {
        out = _upper.cwiseMin(in);
    }
    // Vector4f specialization
    template<> UpperBound<math::Vector4f>::UpperBound(const math::Vector4f& upper): _upper(upper) { }
    template<> bool UpperBound<math::Vector4f>::_isOK(const math::Vector4f& value, ParmFlags) const
    {
        return (_upper.array() < value.array()).any() == false;
    }
    template<> void UpperBound<math::Vector4f>::_validate(const math::Vector4f& in, ParmFlags, math::Vector4f& out) const
    {
        out = _upper.cwiseMin(in);
    }

    /////////////////
    // class Range<T>
    /////////////////
    template <typename ValueType>
    class Range:
        public TypedValueValidator<ValueType>
    {
    protected:
        const ValueType _lower, _upper;
    public:
        inline
        Range(const ValueType& lower, const ValueType& upper):
            _lower(lower < upper ? lower : upper),
            _upper(lower < upper ? upper : lower)
        { }
    protected:
        virtual inline
        bool
        _isOK(const ValueType& value, ParmFlags) const
        {
            return !(value < _lower) && !(_upper < value);
        }
        virtual inline
        void
        _validate(const ValueType& in, ParmFlags, ValueType& out) const
        {
            out = in < _lower ? _lower : (_upper < in ? _upper : in);
        }
    };

    // Vector2f specialization
    template<> inline Range<math::Vector2f>::Range(const math::Vector2f& lower, const math::Vector2f& upper):
        _lower(lower.cwiseMin(upper)),
        _upper(lower.cwiseMax(upper))
    { }
    template<> inline bool Range<math::Vector2f>::_isOK(const math::Vector2f& value, ParmFlags) const
    {
        return (value.array() < _lower.array()).any() == false &&
            (_upper.array() < value.array()).any() == false;
    }
    template<> inline void Range<math::Vector2f>::_validate(const math::Vector2f& in, ParmFlags, math::Vector2f& out) const
    {
        out = _lower.cwiseMax(_upper.cwiseMin(in));
    }
    // Vector4f specialization
    template<> inline Range<math::Vector4f>::Range(const math::Vector4f& lower, const math::Vector4f& upper):
        _lower(lower.cwiseMin(upper)),
        _upper(lower.cwiseMax(upper))
    { }
    template<> inline bool Range<math::Vector4f>::_isOK(const math::Vector4f& value, ParmFlags) const
    {
        return (value.array() < _lower.array()).any() == false &&
            (_upper.array() < value.array()).any() == false;
    }
    template<> inline void Range<math::Vector4f>::_validate(const math::Vector4f& in, ParmFlags, math::Vector4f& out) const
    {
        out = _lower.cwiseMax(_upper.cwiseMin(in));
    }

    ////////////////////////////
    // class CwiseValidator2f
    ////////////////////////////
    class CwiseValidator2f:
        public TypedValueValidator<math::Vector2f>
    {
    private:
        TypedValueValidator<float>* _validators[2];
    public:
        inline
        CwiseValidator2f(TypedValueValidator<float>* xValidator,
                         TypedValueValidator<float>* yValidator)
        {
            assert(xValidator);
            assert(yValidator);
            _validators[0] = xValidator;
            _validators[1] = yValidator;
        }
        inline
        ~CwiseValidator2f()
        {
            for (size_t i = 0; i < 2; ++i)
            { delete _validators[i]; _validators[i] = nullptr; }
        }
    protected:
        virtual inline
        bool
        _isOK(const math::Vector2f& value, ParmFlags f) const
        {
            return _validators[0]->isOK(&value.x(), f) &&
                _validators[1]->isOK(&value.y(), f);
        }
        virtual inline
        void
        _validate(const math::Vector2f& in, ParmFlags f, math::Vector2f& out) const
        {
            _validators[0]->validate(&in.x(), f, &out.x());
            _validators[1]->validate(&in.y(), f, &out.y());
        }
    };

    //////////////////
    // class IsPoint3d
    //////////////////
    class IsPoint3d:
        public TypedValueValidator<math::Vector4f>
    {
    protected:
        virtual inline
        bool
        _isOK(const math::Vector4f& value, ParmFlags) const
        {
            return fabsf(value.w() - 1.0f) < 1e-6f;
        }
        virtual inline
        void
        _validate(const math::Vector4f& in, ParmFlags, math::Vector4f& out) const
        {
            out = in; out.w() = 1.0f;
        }
    };

    /////////////////////////
    // class IsPoint3dInRange
    /////////////////////////
    class IsPoint3dInRange:
        public Range<math::Vector4f>
    {
    public:
        inline
        IsPoint3dInRange(const math::Vector4f& upper, const math::Vector4f& lower):
            Range<math::Vector4f>(lower, upper)
        { }
        virtual inline
        bool
        _isOK(const math::Vector4f& value, ParmFlags flags) const
        {
            return Range<math::Vector4f>::_isOK(value, flags) && fabsf(value.w() - 1.0f) < 1e-6f;
        }
        virtual inline
        void
        _validate(const math::Vector4f& in, ParmFlags flags, math::Vector4f& out) const
        {
            Range<math::Vector4f>::_validate(in, flags, out); out.w() = 1.0f;
        }
    };

    ////////////////
    // class IsColor
    ////////////////
    class IsColor:
        public IsPoint3dInRange
    {
    public:
        inline
        IsColor():
            IsPoint3dInRange(math::Vector4f::UnitW(), math::Vector4f::Ones())
        { }
    };


    ///////////////////////
    // class IsPoint3dAbove
    ///////////////////////
    class IsPoint3dAbove:
        public LowerBound<math::Vector4f>
    {
    public:
        explicit inline
        IsPoint3dAbove(const math::Vector4f& lower):
            LowerBound<math::Vector4f>(lower)
        { }
        virtual inline
        bool
        _isOK(const math::Vector4f& value, ParmFlags flags) const
        {
            return LowerBound<math::Vector4f>::_isOK(value, flags) && fabsf(value.w() - 1.0f) < 1e-6f;
        }
        virtual inline
        void
        _validate(const math::Vector4f& in, ParmFlags flags, math::Vector4f& out) const
        {
            LowerBound<math::Vector4f>::_validate(in, flags, out); out.w() = 1.0f;
        }
    };

    ///////////////////////
    // class IsPoint3dBelow
    ///////////////////////
    class IsPoint3dBelow:
        public UpperBound<math::Vector4f>
    {
    public:
        explicit inline
        IsPoint3dBelow(const math::Vector4f& lower):
            UpperBound<math::Vector4f>(lower)
        { }
        virtual inline
        bool
        _isOK(const math::Vector4f& value, ParmFlags flags) const
        {
            return UpperBound<math::Vector4f>::_isOK(value, flags) && fabsf(value.w() - 1.0f) < 1e-6f;
        }
        virtual inline
        void
        _validate(const math::Vector4f& in, ParmFlags flags, math::Vector4f& out) const
        {
            UpperBound<math::Vector4f>::_validate(in, flags, out); out.w() = 1.0f;
        }
    };

    //////////////////////
    // class IsDirection3d
    //////////////////////
    class IsDirection3d:
        public TypedValueValidator<math::Vector4f>
    {
    protected:
        virtual inline
        bool
        _isOK(const math::Vector4f& value, ParmFlags) const
        {
            return fabsf(value.w()) < 1e-6f;
        }
        virtual inline
        void
        _validate(const math::Vector4f& in, ParmFlags, math::Vector4f& out) const
        {
            out = in; out.w() = 0.0f; out.normalize();
        }
    };

    //////////////////////////
    // class SingleValueByUser
    //////////////////////////
    template<typename ValueType>
    class SingleValueByUser:
        public TypedValueValidator<ValueType>
    {
    protected:
        const ValueType _value;
    public:
        explicit inline
        SingleValueByUser(const ValueType& value):
            _value(value)
        { }
    protected:
        virtual inline
        bool
        _isOK(const ValueType& value, ParmFlags flags) const
        {
            if (flags & SKIPS_RESTRAINTS)
                return true;
            else
                return xchg::equal<ValueType>(value, _value);
        }
        virtual inline
        void
        _validate(const ValueType& in, ParmFlags flags, ValueType& out) const
        {
            if (flags & SKIPS_RESTRAINTS)
                out = in;
            else
                out = _value;
        }
    };

    /////////////////////////////
    // class OnlyFiniteStateByUser
    /////////////////////////////
    class OnlyFiniteStateByUser:
        public TypedValueValidator<char>
    {
    protected:
        virtual inline
        bool
        _isOK(const char& value, ParmFlags flags) const
        {
            if (flags & SKIPS_RESTRAINTS)
                return true;
            else
                return value == xchg::ACTIVE || value == xchg::INACTIVE;
        }
        virtual inline
        void
        _validate(const char& in, ParmFlags flags, char& out) const
        {
            if (flags & SKIPS_RESTRAINTS)
                out = in;
            else
            {
                if (in == xchg::ACTIVATING)
                    out = xchg::ACTIVE;
                else if (in == xchg::DEACTIVATING)
                    out = xchg::INACTIVE;
                else
                    out = in;
            }
        }
    };

    //////////////////
    // class MinAndMax
    //////////////////
    class MinAndMax:
        public TypedValueValidator<math::Vector2f>
    {
    protected:
        TypedValueValidator<float>* _compValidator;

    public:
        explicit inline
        MinAndMax(TypedValueValidator<float>* compValidator = nullptr):
            _compValidator(compValidator)
        { }

        inline
        ~MinAndMax()
        {
            if (_compValidator) delete _compValidator;
        }

    protected:
        virtual
        bool
        _isOK(const math::Vector2f& v, ParmFlags flags) const
        {
            if (_compValidator &&
                (!_compValidator->isOK(&v.x(), flags) || !_compValidator->isOK(&v.y(), flags)))
                return false;
            return !(v.x() > v.y());
        }

        virtual
        void
        _validate(const math::Vector2f& in, ParmFlags flags, math::Vector2f& out) const
        {
            if (_isOK(in, flags))
            {
                out = in;
                return;
            }

            float tx = in.x();
            float ty = in.y();
            if (_compValidator)
            {
                _compValidator->validate(&in.x(), flags, &tx);
                _compValidator->validate(&in.y(), flags, &ty);
            }
            out = tx < ty
                ? math::Vector2f(tx, ty)
                : math::Vector2f(ty, tx);
        }
    };

    //////////////////////////////
    // class ObjectPtrValidator<T>
    //////////////////////////////
    template <typename ObjectType>
    class IsObject:
        public TypedValueValidator<xchg::ObjectPtr>
    {
    protected:
        virtual inline
        bool
        _isOK(const xchg::ObjectPtr& value, ParmFlags) const
        {
            return value.empty() ||
                static_cast<bool>(value.shared_ptr<ObjectType>());
        }
        virtual inline
        void
        _validate(const xchg::ObjectPtr& in, ParmFlags flags, xchg::ObjectPtr& out) const
        {
            if (!_isOK(in, flags))
                throw sys::Exception("ObjectPtr instance's content holder is not of expected type.", "ObjectPtr Parameter Validator");
            out = in;
        }
    };
}
}
