// ========================================================================= //
// Copyright 2016-2018 SUPAMONKS_STUDIO                                      //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
//                                                                           //
// ========================================================================= //
#pragma once

#include "smks/xchg/macro_enum.hpp"
#include "smks/xchg/IdSet.hpp"

namespace smks { namespace parm
{
    enum ParmFlags
    {
        NO_PARM_FLAG        = 0,
        //--- parameter description ----
        CREATES_LINK        = 1 << 0,   // parameter creates a link between two nodes
        SWITCHES_STATE      = 1 << 1,   // parameter causes a state switch (i.e. 'initialization')
        TRIGGERS_ACTION     = 1 << 2,   // parameter triggers action (i.e. 'rendering')
        NEEDS_SCENE         = 1 << 8,   // parameter needs the knoledge of the entire scene to be computed
        //--- parameter setter ---------
        KEEPS_CONNECTIONS   = 1 << 16,  // parameter setter does not erase input connections
        SKIPS_RESTRAINTS    = 1 << 17,  // parameter setter ignores writability/readability rights, value restrictions, etc...
        SKIPS_PREPROCESS    = 1 << 18,  // parameter setter prevents update preprocess (sometimes needed to prevent loops)
        //--- parameter event ----------
        EMITTED_BY_NODE     = 1 << 24,  // parameter event is emitted by node itself
        TRAVERSES_LINKS     = 1 << 25,  // parameter event traverses node link chain
        TRAVERSES_HIERARCHY = 1 << 26,  // parameter event traverses node hierarchy
        IS_ACTIVE_LINK      = 1 << 28,  // parameter evect warns change in node link state (active/inactive)
        //------------------------------
        PARM_ROLE_FLAGS     = 0x000000ff,
        PARM_DESCR_FLAGS    = 0x0000ff00 | PARM_ROLE_FLAGS,
        PARM_SETTER_FLAGS   = 0x00ff0000,
        PARM_EVENT_FLAGS    = 0xff000000
    };
    ENUM_BITWISE_OPS(ParmFlags)

    template<typename ValueType> inline parm::ParmFlags getTypeFlags()              { return parm::NO_PARM_FLAG; }
    template<>                   inline parm::ParmFlags getTypeFlags<xchg::IdSet>() { return parm::CREATES_LINK; }

    inline
    std::ostream&
    to_string(std::ostream& out, ParmFlags f)
    {
        if (f == NO_PARM_FLAG)
            return out;
        out << "- ";
        if (f & smks::parm::CREATES_LINK)           out << "links ";
        if (f & smks::parm::SWITCHES_STATE)         out << "switches ";
        if (f & smks::parm::TRIGGERS_ACTION)        out << "triggers ";
        if (f & smks::parm::NEEDS_SCENE)            out << "global ";
        if (f & smks::parm::KEEPS_CONNECTIONS)      out << "still_connects ";
        if (f & smks::parm::SKIPS_RESTRAINTS)       out << "no_restraints ";
        if (f & smks::parm::SKIPS_PREPROCESS)       out << "no_preprocess ";
        if (f & smks::parm::EMITTED_BY_NODE)        out << "emitted ";
        if (f & smks::parm::TRAVERSES_LINKS)        out << "thru_links ";
        if (f & smks::parm::TRAVERSES_HIERARCHY)    out << "thru_hierarchy ";
        if (f & smks::parm::IS_ACTIVE_LINK)         out << "active_link ";
        return out << "-";
    }
}
}
