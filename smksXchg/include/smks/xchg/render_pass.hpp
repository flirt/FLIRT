// ========================================================================= //
// Copyright 2016-2018 SUPAMONKS_STUDIO                                      //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
//                                                                           //
// ========================================================================= //
#pragma once

namespace smks { namespace xchg
{
    enum FlatRenderPassType
    {
        UNKNOWN_PASS    = -1,
        //--------------
        POSITION_PASS   = 0,
        EYE_POSITION_PASS,
        NORMAL_PASS,
        EYE_NORMAL_PASS,
        TANGENT_PASS,
        EYE_TANGENT_PASS,
        TEXCOORDS_PASS,
        FACING_RATIO_PASS,
        SUBSURFACE_PASS,
        SHADOWS_PASS,
        SHADOW_BIAS_PASS,
        AMBIENT_OCCLUSION_PASS,
        DIFFUSE_COLOR_PASS,
        GLOSSY_COLOR_PASS,
        SINGULAR_COLOR_PASS,
#if defined(_DEBUG)
        DEBUG_RAY_TEXCOORDS_PASS,
        DEBUG_RAY_GEOM_NORMAL_PASS,
        DEBUG_HIT_GEOM_NORMAL_PASS,
        DEBUG_HIT_SHAD_NORMAL_PASS,
#endif // defined(_DEBUG)
        //--------------
        NUM_PASS_TYPES
    };

    static const char* LPE_PASS_NAME        = "lightPath";
    static const char* IDCOVERAGE_PASS_NAME = "idCoverage";
    static const char* RAW_RGBA_PASS_NAME   = "raw";
}
}
