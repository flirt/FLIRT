// ========================================================================= //
// Copyright 2016-2018 SUPAMONKS_STUDIO                                      //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
//                                                                           //
// ========================================================================= //
#pragma once

#include <memory>
#include "smks/sys/configure_xchg.hpp"

namespace smks { namespace xchg
{
    //////////////////
    // class DAGLayout
    //////////////////
    class FLIRT_XCHG_EXPORT DAGLayout
    {
    public:
        typedef std::shared_ptr<DAGLayout> Ptr;

    public:
        /////////////////////
        // class AbstractData
        /////////////////////
        class AbstractData
        {
        public:
            typedef std::shared_ptr<AbstractData> Ptr;

        public:
            virtual inline
            ~AbstractData()
            { }

            virtual
            bool
            inherits(size_t) const = 0;

            virtual
            void
            inherits(size_t, bool) = 0;

            virtual
            void
            resize(size_t) = 0;

            virtual
            void
            moveEntry(size_t dst, size_t src) = 0;

            virtual
            void
            recomputeEntry(size_t current) = 0;

            void
            recomputeEntry(size_t current, size_t parent)
            {
                if (inherits(current))
                    accumulateEntries(current, parent);
                else
                    recomputeEntry(current);
            }
        protected:
            virtual
            void
            accumulateEntries(size_t current, size_t parent) = 0;
        };

        //////////////////////////
        // class AbstractTypedData
        //////////////////////////
        template <typename ValueType>
        class AbstractTypedData:
            public AbstractData
        {
        public:
            virtual
            void
            setLocal(size_t, const ValueType&) = 0;

            virtual
            const ValueType&
            getGlobal(size_t) const = 0;
        };

    private:
        static const size_t INVALID_IDX;
    private:
        class PImpl;
    private:
        PImpl* _pImpl;

    public:
        static inline
        Ptr
        create(AbstractData::Ptr const& data = nullptr)
        {
            Ptr ptr(new DAGLayout);
            if (data)
                ptr->attach(data);
            return ptr;
        }

    protected:
        DAGLayout();
    private:
        // non-copyable
        DAGLayout(const DAGLayout&);
        DAGLayout& operator=(const DAGLayout&);

    public:
        ~DAGLayout();

    protected:
        void
        attach(AbstractData::Ptr const&);

    public:
        void
        dispose();

        void
        add(unsigned int node, unsigned int parent = 0);

        void
        remove(unsigned int node);

        bool
        has(unsigned int node) const;

        unsigned int
        parent(unsigned int node) const;

        unsigned int
        firstChild(unsigned int node) const;

        unsigned int
        previousSibling(unsigned int node) const;

        unsigned int
        nextSibling(unsigned int node) const;

        size_t
        index(unsigned int node) const;

    protected:
        unsigned int
        id(size_t idx) const;

        size_t
        size() const;

        size_t
        capacity() const;

    public:
        void
        dirty(unsigned int node);

        bool
        isDirty(unsigned int node) const;

        void
        clean();    //<! WARNING: likely to compromise previous indices

    protected:
        void
        swapIndices(size_t idx1, size_t idx2);

    public:
        std::ostream&
        dump(std::ostream&) const;  //<! for debugging purposes only

    public:
        static
        bool
        valid(size_t idx);
    };
}
}
