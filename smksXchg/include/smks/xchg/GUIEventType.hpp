// ========================================================================= //
// Copyright 2016-2018 SUPAMONKS_STUDIO                                      //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
//                                                                           //
// ========================================================================= //
#pragma once

namespace smks { namespace xchg
{
    enum GUIEventType
    {
        MOUSE_PRESS_EVENT = 0,
        MOUSE_MOVE_EVENT,
        MOUSE_RELEASE_EVENT,
        MOUSE_DOUBLE_CLICK_EVENT,
        WHEEL_EVENT,
        RESIZE_EVENT,
        KEY_PRESS_EVENT,
        KEY_RELEASE_EVENT,
        PAINT_GL_EVENT,
        NUM_GUI_EVENTS
    };

    // values are in accordance to Qt counterpart
    enum MouseButton
    {
        NO_BUTTON       = 0,
        LEFT_BUTTON     = 1 << 0,
        RIGHT_BUTTON    = 1 << 1,
        MIDDLE_BUTTON   = 1 << 2,
        X_BUTTON_1      = 1 << 3,
        X_BUTTON_2      = 1 << 4
    };

    // values are in accordance to Qt counterpart
    enum KeyboardModifier
    {
        NO_MODIFIER         = 0,
        SHIFT_MODIFIER      = 0x02000000,
        CTRL_MODIFIER   = 0x04000000,
        ALT_MODIFIER        = 0x08000000
    };
}
}
