// ========================================================================= //
// Copyright 2016-2018 SUPAMONKS_STUDIO                                      //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
//                                                                           //
// ========================================================================= //
#pragma once

#include <cassert>
#include <smks/sys/aligned_allocation.hpp>

namespace smks { namespace xchg
{
    template<class ValueType>
    class Vector
    {
    private:
        size_t      _size;      //<! number of valid items
        size_t      _capacity;  //<! number of items allocated
        ValueType*  _data;      //<! data array

    private:
        static __forceinline
        ValueType*
        allocate(size_t num)
        {
            if (num == 0)
                return nullptr;

            ValueType* data = reinterpret_cast<ValueType*>(sys::alignedMalloc(num * sizeof(ValueType), 64));
            for (size_t i = 0; i < num; ++i)
                new (data + i) ValueType();

            return data;
        }

        static __forceinline
        void
        deallocate(size_t num, ValueType* data)
        {
            if (num == 0 || data == nullptr)
                return;

            for (size_t i = 0; i < num; ++i)
                (data + i)->~ValueType();
            sys::alignedFree(data);
        }

    public:
        __forceinline
        Vector():
            _size       (0),
            _capacity   (0),
            _data       (nullptr)
        { }

        explicit __forceinline
        Vector(size_t sz):
            _size       (0),
            _capacity   (0),
            _data       (nullptr)
        {
            if (sz)
                resize(sz);
        }

        __forceinline
        Vector(const Vector<ValueType>& other):
            _size       (other._size),
            _capacity   (other._capacity),
            _data       (allocate(_capacity))
        {
            for (size_t i = 0; i < _size; ++i)
                _data[i] = other._data[i];
        }

        __forceinline
        ~Vector()
        {
            clear();
        }

        __forceinline
        void
        clear()
        {
            if (_data)
                deallocate(_capacity, _data);
            _data       = nullptr;
            _size       = 0;
            _capacity   = 0;
        };

        __forceinline
        bool
        empty() const
        {
            return _size == 0;
        }

        __forceinline
        size_t
        size() const
        {
            return _size;
        };

        __forceinline
        size_t
        capacity() const
        {
            return _capacity;
        }

        __forceinline
        ValueType*
        begin() const
        {
            return _data;
        };

        __forceinline
        ValueType*
        end() const
        {
            return _data + _size;
        };

        __forceinline
        ValueType&
        front() const
        {
            return _data[0];
        };

        __forceinline
        ValueType&
        back() const
        {
            return _data[_size-1];
        };

        __forceinline
        void
        push_back(const ValueType &nt)
        {
            ValueType v = nt; // need local copy as input reference could point to this vector
            reserve(_size + 1);
            _data[_size] = v;
            ++_size;
        }

        __forceinline
        void
        pop_back()
        {
            if (_size > 0)
            {
                _data[_size - 1] = ValueType();
                --_size;
            }
        }

        __forceinline
        Vector<ValueType>&
        operator=(const Vector<ValueType> &other)
        {
            resize(other._size);
            for (size_t i = 0; i < _size; ++i)
                _data[i] = other._data[i];

            return *this;
        }

        __forceinline
        ValueType&
        operator[](size_t i)
        {
            assert(_data);
            assert(i < _size);
            return _data[i];
        };

        __forceinline
        const ValueType&
        operator[](size_t i) const
        {
            assert(_data);
            assert(i < _size);
            return _data[i];
        };

        void
        resize(size_t newSize, bool exact = true)
        {
            if (newSize < _size)
            {
                if (exact)
                {
                    assert(newSize > 0);

                    ValueType*  data        = _data;
                    size_t      capacity    = _capacity;

                    _data       = allocate(newSize);
                    _capacity   = newSize;
                    if (data)
                    {
                        for (size_t i = 0; i < newSize; ++i)
                            _data[i] = data[i];
                        deallocate(capacity, data);
                    }
                }
            }
            else
                reserve(newSize, exact);

            _size = newSize;
        };

        void
        reserve(size_t newSize, bool exact = false)
        {
            if (newSize <= _capacity)
                return;

            size_t newCapacity = _capacity;

            if (exact)
                newCapacity = newSize;
            else
                while (newCapacity < newSize)
                    newCapacity = (1 < (newCapacity * 2))
                        ? newCapacity * 2
                        : 1;
            assert(newCapacity > 0);

            ValueType*  data        = _data;
            size_t      capacity    = _capacity;

            _data       = allocate(newCapacity);
            _capacity   = newCapacity;
            if (data)
            {
                for (size_t i = 0; i < _size; ++i)
                    _data[i] = data[i];
                deallocate(capacity, data);
            }
        }
    };
}
}
