// ========================================================================= //
// Copyright 2016-2018 SUPAMONKS_STUDIO                                      //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
//                                                                           //
// ========================================================================= //
#pragma once

#include <cassert>
#include <smks/sys/Exception.hpp>

namespace smks { namespace xchg
{
    //////////////////
    // enum SceneEvent
    //////////////////
    class SceneEvent
    {
    public:
        ////////////
        // enum Type
        ////////////
        enum Type
        {
            UNKNOWN         = -1,
            NODE_CREATED    = 0,
            NODE_DELETED,
            PARM_CONNECTED,
            PARM_DISCONNECTED,
            NODES_LINKED,
            NODES_UNLINKED,
            LINK_ATTACHED,
            LINK_DETACHED
        };
        enum { MAX_SIZE = 5 };
    private:
        Type            _type;
        unsigned int    _ids[MAX_SIZE];

    public:
        static inline
        SceneEvent
        nodeCreated(unsigned int node,
                    unsigned int parent)
        {
            return SceneEvent(
                NODE_CREATED,
                node,
                parent);
        }

        static inline
        SceneEvent
        nodeDeleted(unsigned int node,
                    unsigned int parent)
        {
            return SceneEvent(
                NODE_DELETED,
                node,
                parent);
        }

        static inline
        SceneEvent
        parmConnected(unsigned int cnx,
                      unsigned int master,
                      unsigned int masterParm,
                      unsigned int slave,
                      unsigned int slaveParm)
        {
            return SceneEvent(
                PARM_CONNECTED,
                cnx,
                master,
                masterParm,
                slave,
                slaveParm);
        }

        static inline
        SceneEvent
        parmDisconnected(unsigned int cnx,
                         unsigned int master,
                         unsigned int masterParm,
                         unsigned int slave,
                         unsigned int slaveParm)
        {
            return SceneEvent(
                PARM_DISCONNECTED,
                cnx,
                master,
                masterParm,
                slave,
                slaveParm);
        }

        static inline
        SceneEvent
        nodesLinked(unsigned int target,
                    unsigned int source)
        {
            return SceneEvent(
                NODES_LINKED,
                target,
                source);
        }

        static inline
        SceneEvent
        nodesUnlinked(unsigned int target,
                      unsigned int source)
        {
            return SceneEvent(
                NODES_UNLINKED,
                target,
                source);
        }

        static inline
        SceneEvent
        linkAttached(unsigned int target,
                     unsigned int source,
                     unsigned int parm)
        {
            return SceneEvent(
                LINK_ATTACHED,
                target,
                source,
                parm);
        }

        static inline
        SceneEvent
        linkDetached(unsigned int target,
                     unsigned int source,
                     unsigned int parm)
        {
            return SceneEvent(
                LINK_DETACHED,
                target,
                source,
                parm);
        }

    public:
        SceneEvent():
            _type(UNKNOWN)
        {
            clearIds();
        }

        SceneEvent(const SceneEvent& other):
            _type(other._type)
        {
            copyIds(other);
        }

        ~SceneEvent()
        {
            clearIds();
        }

        inline
        Type
        type() const
        {
            return _type;
        }

        inline
        bool
        empty() const
        {
            return _type == SceneEvent::UNKNOWN;
        }

        inline
        unsigned int
        node() const
        {
            if (_type == NODE_CREATED ||
                _type == NODE_DELETED)
                return _ids[0];
            throw sys::Exception(
                "Irrelevant getter for scene event.",
                "Node Getter");
        }

        inline
        unsigned int
        parent()  const
        {
            if (_type == NODE_CREATED ||
                _type == NODE_DELETED)
                return _ids[1];
            throw sys::Exception(
                "Irrelevant getter for scene event.",
                "Parent Getter");
        }

        inline
        unsigned int
        target() const
        {
            if (_type == NODES_LINKED ||
                _type == NODES_UNLINKED ||
                _type == LINK_ATTACHED ||
                _type == LINK_DETACHED)
                return _ids[0];
            throw sys::Exception(
                "Irrelevant getter for scene event.",
                "Target Getter");
        }

        inline
        unsigned int
        source() const
        {
            if (_type == NODES_LINKED ||
                _type == NODES_UNLINKED ||
                _type == LINK_ATTACHED ||
                _type == LINK_DETACHED)
                return _ids[1];
            throw sys::Exception(
                "Irrelevant getter for scene event.",
                "Source Getter");
        }

        inline
        unsigned int
        parm() const
        {
            if (_type == LINK_ATTACHED ||
                _type == LINK_DETACHED)
                return _ids[2];
            throw sys::Exception(
                "Irrelevant getter for scene event.",
                "Parameter Getter");
        }

        //<! relevant for connected/disconnected events
        inline
        unsigned int
        connection() const
        {
            if (_type == PARM_CONNECTED ||
                _type == PARM_DISCONNECTED)
                return _ids[0];
            throw sys::Exception(
                "Irrelevant getter for scene event.",
                "Connection Getter");
        }

        inline
        unsigned int
        master() const
        {
            if (_type == PARM_CONNECTED ||
                _type == PARM_DISCONNECTED)
                return _ids[1];
            throw sys::Exception(
                "Irrelevant getter for scene event.",
                "Master Getter");
        }

        inline
        unsigned int
        masterParm() const
        {
            if (_type == PARM_CONNECTED ||
                _type == PARM_DISCONNECTED)
                return _ids[2];
            throw sys::Exception(
                "Irrelevant getter for scene event.",
                "Master Parameter Getter");
        }

        inline
        unsigned int
        slave() const
        {
            if (_type == PARM_CONNECTED ||
                _type == PARM_DISCONNECTED)
                return _ids[3];
            throw sys::Exception(
                "Irrelevant getter for scene event.",
                "Slave Getter");
        }

        inline
        unsigned int
        slaveParm() const
        {
            if (_type == PARM_CONNECTED ||
                _type == PARM_DISCONNECTED)
                return _ids[4];
            throw sys::Exception(
                "Irrelevant getter for scene event.",
                "Slave Parameter Getter");
        }

    private:
        inline
        SceneEvent(Type         type,
                   unsigned int id0 = 0,
                   unsigned int id1 = 0,
                   unsigned int id2 = 0,
                   unsigned int id3 = 0,
                   unsigned int id4 = 0):
            _type(type)
        {
            clearIds();

            switch (type)
            {
            case NODE_CREATED:
            case NODE_DELETED:
                if (!id0)
                    throw sys::Exception(
                        "Invalid node.",
                        "Scene Event Creation");
                //if (!id1)
                //  throw sys::Exception("Invalid parent.", "Scene Event Creation");
                break;
            case PARM_CONNECTED:
            case PARM_DISCONNECTED:
                if (!id0)
                    throw sys::Exception(
                        "Invalid connection.",
                        "Scene Event Creation");
                if (!id1)
                    throw sys::Exception(
                        "Invalid master.",
                        "Scene Event Creation");
                if (!id2)
                    throw sys::Exception(
                        "Invalid master parameter.",
                        "Scene Event Creation");
                if (!id3)
                    throw sys::Exception(
                        "Invalid slave.",
                        "Scene Event Creation");
                if (!id4)
                    throw sys::Exception(
                        "Invalid slave parameter.",
                        "Scene Event Creation");
                break;
            case NODES_LINKED:
            case NODES_UNLINKED:
                if (!id0)
                    throw sys::Exception(
                        "Invalid target.",
                        "Scene Event Creation");
                if (!id1)
                    throw sys::Exception(
                        "Invalid source.",
                        "Scene Event Creation");
                break;
            case LINK_ATTACHED:
            case LINK_DETACHED:
                if (!id0)
                    throw sys::Exception(
                        "Invalid target.",
                        "Scene Event Creation");
                if (!id1)
                    throw sys::Exception(
                        "Invalid source.",
                        "Scene Event Creation");
                if (!id2)
                    throw sys::Exception(
                        "Invalid parm.",
                        "Scene Event Creation");
                break;
            default:
                throw sys::Exception(
                    "Invalid event type.",
                    "Scene Event Creation");
            }

            _ids[0] = id0;
            _ids[1] = id1;
            _ids[2] = id2;
            _ids[3] = id3;
            _ids[4] = id4;
        }

        inline
        void
        clearIds()
        {
            memset(_ids, 0, sizeof(unsigned int) * MAX_SIZE);
        }

        inline
        void
        copyIds(const SceneEvent& evt)
        {
            memcpy(_ids, evt._ids, sizeof(unsigned int) * MAX_SIZE);
        }

    public:
        inline
        SceneEvent&
        operator=(const SceneEvent& evt)
        {
            _type = evt._type;
            copyIds(evt);
            return *this;
        }

        inline
        bool
        operator==(const SceneEvent& other) const
        {
            if (_type != other._type)
                return false;
            for (size_t i = 0; i < MAX_SIZE; ++i)
                if (_ids[i] != other._ids[i])
                    return false;
            return true;
        }

        friend inline
        std::ostream&
        operator<<(std::ostream& out, const SceneEvent& evt)
        {
            switch (evt.type())
            {
            case SceneEvent::UNKNOWN:           return out << "UNKNOWN";
            case SceneEvent::NODE_CREATED:      return out << "NODE_CREATED      [" << evt.node() << "]=<===<=[" << evt.parent() << "]";
            case SceneEvent::NODE_DELETED:      return out << "NODE_DELETED      [" << evt.node() << "]=<   <=[" << evt.parent() << "]";
            case SceneEvent::PARM_CONNECTED:    return out << "PARM_CONNECTED    [" << evt.master() << "].'" << evt.masterParm() << "' ->-(" << evt.connection() << ")->- [" << evt.slave() << "].'" << evt.slaveParm() << "'";
            case SceneEvent::PARM_DISCONNECTED: return out << "PARM_DISCONNECTED [" << evt.master() << "].'" << evt.masterParm() << "' -x-(" << evt.connection() << ")-x- [" << evt.slave() << "].'" << evt.slaveParm() << "'";
            case SceneEvent::NODES_LINKED:      return out << "NODES_LINKED      [" << evt.target() << "]-<---<-[" << evt.source() << "]";
            case SceneEvent::NODES_UNLINKED:    return out << "NODES_UNLINKED    [" << evt.target() << "]-<-x-<-[" << evt.source() << "]";
            case SceneEvent::LINK_ATTACHED:     return out << "LINK_ATTACHED     [" << evt.target() << "]-<---(" << evt.parm() << ")---<-[" << evt.source() << "]";
            case SceneEvent::LINK_DETACHED:     return out << "LINK_DETACHED     [" << evt.target() << "]-<-x (" << evt.parm() << ") x-<-[" << evt.source() << "]";
            default:                            return out;
            }
        }
    };
}
}
