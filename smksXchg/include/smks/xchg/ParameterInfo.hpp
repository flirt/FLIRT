// ========================================================================= //
// Copyright 2016-2018 SUPAMONKS_STUDIO                                      //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
//                                                                           //
// ========================================================================= //
#pragma once

#include <typeinfo>
#include "smks/parm/ParmFlags.hpp"

namespace smks { namespace xchg
{
    struct ParameterInfo
    {
        const char*             name;
        const std::type_info*   typeinfo;
        parm::ParmFlags         flags;
    public:
        inline
        ParameterInfo():
            name(nullptr), typeinfo(nullptr), flags(parm::NO_PARM_FLAG)
        { }

        inline
        operator bool() const
        {
            return name && typeinfo;
        }
    };
}
}
