// ========================================================================= //
// Copyright 2016-2018 SUPAMONKS_STUDIO                                      //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
//                                                                           //
// ========================================================================= //
#pragma once

#include <cassert>
#include <vector>
#include <smks/sys/Exception.hpp>
#include "smks/xchg/DAGLayout.hpp"
#include "smks/xchg/DynamicBitset.hpp"

namespace smks { namespace xchg
{
    template<typename ValueType>
    class DAGHierarchyData:
        public DAGLayout::AbstractTypedData<ValueType>
    {
    public:
        typedef std::shared_ptr<DAGHierarchyData> Ptr;
    protected:
        std::vector<ValueType>  _local;
        std::vector<ValueType>  _global;
        xchg::DynamicBitset*    _inherits;  //<! if null, then all entries inherit data from their parent

    protected:
        explicit
        DAGHierarchyData(bool skippableInheritance):
            _local      (),
            _global     (),
            _inherits   (skippableInheritance ? new DynamicBitset() : nullptr)
        { }

    public:
        virtual inline
        ~DAGHierarchyData()
        {
            if (_inherits)
                delete _inherits;
            _inherits = nullptr;
        }

        inline
        bool
        inherits(size_t idx) const
        {
            return _inherits == nullptr
                ? true // by default, data is inherited
                : _inherits->get(idx);
        }

        inline
        void
        inherits(size_t idx, bool value)
        {
            if (_inherits == nullptr)
                throw sys::Exception("Per-entry inheritance rules is not activated.", "Inherits Setter");
            _inherits->set(idx, value);
        }

        inline
        void
        setLocal(size_t idx, const ValueType& value)
        {
            assert(idx < _local.size());
            _local[idx] = value;
        }

        inline
        const ValueType&
        getGlobal(size_t idx) const
        {
            assert(idx < _global.size());
            return _global[idx];
        }

        inline
        void
        resize(size_t capacity)
        {
            if (capacity > 0)
            {
                _local.resize(capacity);
                _global.resize(capacity);
                if (_inherits)
                    _inherits->resize(capacity, true);
            }
            else
            {
                _local.clear();
                _global.clear();
                _local.shrink_to_fit();
                _global.shrink_to_fit();
                if (_inherits)
                    _inherits->clear();
            }
        }

        inline
        void
        moveEntry(size_t dst, size_t src)
        {
            assert(_local.size() == _global.size());
            assert(dst < _global.size());
            assert(src < _global.size());

            _local[dst]     = _local[src];
            _global[dst]    = _global[src];
            if (_inherits)
                _inherits->set(dst, _inherits->get(src));
        }
    };
}
}
