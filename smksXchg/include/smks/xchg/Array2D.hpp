// ========================================================================= //
// Copyright 2016-2018 SUPAMONKS_STUDIO                                      //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
//                                                                           //
// ========================================================================= //
#pragma once

#include <smks/sys/aligned_allocation.hpp>

namespace smks { namespace xchg
{
    template<typename T>
    class Array2D
    {
        ALIGNED_CLASS
    private:
        size_t  _sizeX;
        size_t  _sizeY;
        T**     _data;

    public:
        inline
        Array2D():
            _sizeX(0),
            _sizeY(0),
            _data(nullptr)
        { }

        inline
        Array2D(size_t _sizeX, size_t _sizeY):
            _sizeX(_sizeX),
            _sizeY(_sizeY)
        {
            _data = new T*[_sizeX];
            for (size_t x = 0; x < _sizeX; x++)
                _data[x] = new T[_sizeY];
        }

        inline
        ~Array2D()
        {
            if (_data)
            {
                for (size_t x = 0; x < _sizeX; x++)
                    if (_data[x])
                        delete[] _data[x];
                delete[] _data;
            }
            _data = nullptr;
        }

        inline
        operator const T**() const
        {
            return const_cast<const T**>(_data);
        }

        inline
        operator T**()
        {
            return _data;
        }

        inline
        const T&
        get(size_t x, size_t y) const
        {
            return _data[x][y];
        }

        inline
        T&
        get(size_t x, size_t y)
        {
            return _data[x][y];
        }

        inline
        void
        set(size_t x, size_t y, const T& value)
        {
            _data[x][y] = value;
        }
    };
}
}
