// ========================================================================= //
// Copyright 2016-2018 SUPAMONKS_STUDIO                                      //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
//                                                                           //
// ========================================================================= //
#pragma once

#include "smks/xchg/GUIEvents.hpp"

namespace smks { namespace xchg
{
    //////////////////////////////////
    // struct AbstractGUIEventCallback
    //////////////////////////////////
    struct AbstractGUIEventCallback
    {
    public:
        typedef std::shared_ptr<AbstractGUIEventCallback> Ptr;
    public:
        virtual inline ~AbstractGUIEventCallback() { }

        virtual void operator()(const GUIEvent&) = 0;
        virtual bool understands(GUIEventType) const = 0;
    };

    ///////////////////////////////////////////////////
    // struct AbstractTypedGUIEventCallback <EventType>
    ///////////////////////////////////////////////////
    template<typename EventType>
    struct AbstractTypedGUIEventCallback:
        public AbstractGUIEventCallback
    {
    public:
        inline
        void
        operator()(const GUIEvent& evt)
        {
            const EventType* e = GUIEvent::cast<EventType>(evt);
            if (e)
                operator()(*e);
        }

        inline
        bool
        understands(GUIEventType type) const
        {
            return EventType::encompasses(type);
        }

    protected:
        virtual void operator()(const EventType&) = 0;
    };

    typedef AbstractTypedGUIEventCallback<MouseEvent>   MouseEventCallback;
    typedef AbstractTypedGUIEventCallback<WheelEvent>   WheelEventCallback;
    typedef AbstractTypedGUIEventCallback<ResizeEvent>  ResizeEventCallback;
    typedef AbstractTypedGUIEventCallback<KeyEvent>     KeyEventCallback;
    typedef AbstractTypedGUIEventCallback<PaintGLEvent> PaintGLEventCallback;
}
}
