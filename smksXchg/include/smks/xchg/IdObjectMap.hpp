// ========================================================================= //
// Copyright 2016-2018 SUPAMONKS_STUDIO                                      //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
//                                                                           //
// ========================================================================= //
#pragma once

namespace smks { namespace xchg
{
    //<! generic class used to stored mapping between object ID
    // and weak pointers to them. Should be used as the * private
    // implementation * for exposed global dictionaries of singleton-like
    // objects (built-in parameters, uniform declarations, available GL program, etc
    // ...).
    // requirement: ObjectType::id() must exist.
    template<typename ObjectType>
    class IdObjectMap
    {
    private:
        typedef std::shared_ptr<ObjectType>                         ObjectTypePtr;
        typedef std::weak_ptr<ObjectType>                           ObjectTypeWPtr;
        typedef boost::unordered_map<unsigned int, ObjectTypeWPtr>  ObjectTypeMap;
    private:
        ObjectTypeMap _m;
    public:
        inline
        IdObjectMap():
            _m()
        { }

        inline
        ObjectTypePtr const&
        add(ObjectTypePtr const& obj)
        {
            if (obj)
                _m.insert(std::pair<unsigned int, ObjectTypeWPtr>(obj->id(), obj));
            return obj;
        }

        inline
        ObjectTypePtr
        begin() const
        {
            return !_m.empty()
                ? _m.begin()->second.lock()
                : nullptr;
        }

        inline
        ObjectTypePtr
        next(ObjectTypePtr const& current) const
        {
            if (current == nullptr)
                return nullptr;

            ObjectTypeMap::const_iterator it = _m.find(current->id());

            if (it == _m.end())
                return nullptr;
            ++it;
            if (it == _m.end())
                return nullptr;

            return it->second.lock();
        }

        inline
        ObjectTypePtr
        find(const char* name) const
        {
            return find(util::string::getId(name));
        }

        inline
        ObjectTypePtr
        find(unsigned int id) const
        {
            ObjectTypeMap::const_iterator const& it = _m.find(id);

            return it != _m.end()
                ? it->second.lock()
                : nullptr;
        }
    };
}
}

