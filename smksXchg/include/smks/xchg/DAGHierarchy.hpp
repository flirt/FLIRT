// ========================================================================= //
// Copyright 2016-2018 SUPAMONKS_STUDIO                                      //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
//                                                                           //
// ========================================================================= //
#pragma once

#include <smks/sys/Exception.hpp>
#include <smks/xchg/DAGLayout.hpp>
#include <smks/xchg/NodeClass.hpp>

namespace smks { namespace xchg
{
    template <typename ValueType>
    class DAGHierarchy
    {
    protected:
        typedef std::shared_ptr<DAGLayout::AbstractTypedData<ValueType>> DataPtr;
    protected:
        DataPtr                 _data;
        xchg::DAGLayout::Ptr    _dagLayout;
    protected:
        explicit inline
        DAGHierarchy(DataPtr const& data):
            _data       (data),
            _dagLayout  (DAGLayout::create(_data))
        { }

    public:
        inline
        std::ostream&
        dump(std::ostream& out) const
        {
            if (_dagLayout)
                _dagLayout->dump(out);
            return out;
        }

        inline
        void
        dispose()
        {
            _dagLayout->dispose();  // triggers data deletion
        }

        virtual inline
        void
        addNode(unsigned int node, unsigned int parent)
        {
            _dagLayout->add(node, parent);
        }

        virtual inline
        void
        removeNode(unsigned int node)
        {
            _dagLayout->remove(node);
        }

        inline
        bool
        has(unsigned int node) const
        {
            return DAGLayout::valid(_dagLayout->index(node));
        }

        virtual inline
        void
        inherits(unsigned int node, bool value)
        {
            const size_t idx = _dagLayout->index(node);

            if (!DAGLayout::valid(idx))
            {
                std::stringstream sstr;
                sstr << "Node ID = " << node << " not found in DAG layout.";
                throw sys::Exception(sstr.str().c_str(), "DAG Component Inherits Setter");
            }

            _data->inherits(idx, value);
            //---
            _dagLayout->dirty(node);
        }

        virtual inline
        void
        setLocal(unsigned int node, const ValueType& value)
        {
            const size_t idx = _dagLayout->index(node);

            if (!DAGLayout::valid(idx))
            {
                std::stringstream sstr;
                    sstr << "Node ID = " << node << " not found in DAG layout.";
                throw sys::Exception(sstr.str().c_str(), "DAG Component Local Setter");
            }

            _data->setLocal(idx, value);
            //---
            _dagLayout->dirty(node);
        }

        virtual inline
        const ValueType&
        getGlobal(unsigned int node)
        {
            if (_dagLayout->isDirty(node))
                _dagLayout->clean();    // WARNING: may change indices
            //---
            const size_t idx = _dagLayout->index(node);

            if (!DAGLayout::valid(idx))
            {
                std::stringstream sstr;
                sstr << "Node ID = " << node << " not found in DAG layout.";
                throw sys::Exception(sstr.str().c_str(), "DAG Component Global Setter");
            }

            return _data->getGlobal(idx);
        }
    protected:
        inline
        std::ostream&
        dumpLayout(std::ostream& out) const
        {
            return _dagLayout->dump(out);
        }
    };
}
}
