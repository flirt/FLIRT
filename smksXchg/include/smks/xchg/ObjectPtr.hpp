// ========================================================================= //
// Copyright 2016-2018 SUPAMONKS_STUDIO                                      //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
//                                                                           //
// ========================================================================= //
#pragma once

#include <memory>
#include <typeinfo>

namespace smks { namespace xchg
{
    class ObjectPtr
    {
        ///////////////////////
        // class AbstractHolder
        ///////////////////////
    private:
        class AbstractHolder
        {
        public:
            virtual
            ~AbstractHolder()
            { }

            virtual
            const std::type_info&
            type() const = 0;

            virtual
            AbstractHolder*
            clone() const = 0;

            virtual
            bool
            operator==(const AbstractHolder&) const = 0;
        };

        //////////////////////////
        // class Holder<ValueType>
        //////////////////////////
    private:
        template<typename ValueType>
        class Holder:
            public AbstractHolder
        {
        public:
            std::shared_ptr<ValueType> ptr;
        public:
            explicit inline
            Holder(const std::shared_ptr<ValueType>& ptr):
                ptr(ptr)
            { }

            inline
            ~Holder()
            {
                ptr = nullptr;
            }

            inline
            const std::type_info&
            type() const
            {
                return typeid(ValueType);
            }

            inline
            AbstractHolder*
            clone() const
            {
                return new Holder<ValueType>(ptr);
            }

            inline
            bool
            operator==(const AbstractHolder& other) const
            {
                return type() == other.type() &&
                    ptr.get() == reinterpret_cast<const Holder<ValueType>*>(&other)->ptr.get();
            }
        };

    private:
        AbstractHolder* _holder;

    public:
        inline
        ObjectPtr():
            _holder(nullptr)
        { }

        template <typename ValueType> inline
        ObjectPtr(const std::shared_ptr<ValueType>& ptr):
            _holder(new Holder<ValueType>(ptr))
        { }

        inline
        ObjectPtr(const ObjectPtr& other):
            _holder(other._holder ? other._holder->clone() : nullptr)
        { }

        inline
        ObjectPtr(ObjectPtr&& other):
            _holder(other._holder ? other._holder : nullptr)
        { }

        inline
        ~ObjectPtr()
        {
            delete _holder;
        }

        inline
        ObjectPtr&
        operator=(const ObjectPtr& other)
        {
            if (this == &other)
                return *this;

            if (_holder)
                delete _holder;
            _holder = other._holder ? other._holder->clone() : nullptr;

            return *this;
        }

        inline
        bool
        empty() const
        {
            return _holder == nullptr;
        }

        template<typename ValueType> inline
        std::shared_ptr<ValueType>
        shared_ptr() const
        {
            return _holder && _holder->type() == typeid(ValueType)
                ? reinterpret_cast<const Holder<ValueType>*>(_holder)->ptr
                : nullptr;
        }

        inline
        bool
        operator==(const ObjectPtr& other) const
        {
            return _holder && other._holder &&
                *_holder == *other._holder;
        }
    };
}
}
