// ========================================================================= //
// Copyright 2016-2018 SUPAMONKS_STUDIO                                      //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
//                                                                           //
// ========================================================================= //
#pragma once

#include "macro_enum.hpp"

namespace smks { namespace xchg
{
    enum NodeClass
    {
        NONE            = 0,
        ANY             = -1,
        //---------------
        SCENE           = 0x00000001,
        ARCHIVE         = 0x00000002,
        XFORM           = 0x00000100,
        CAMERA          = 0x00000200,
        POINTS          = 0x00000400,
        CURVES          = 0x00000800,
        MESH            = 0x00001000,
        LIGHT           = 0x00002000,
        ASSIGN          = 0x00010000,
        SELECTOR        = 0x00020000,
        RENDER_ACTION   = 0x00040000,
        MATERIAL        = 0x00100000,
        SUBSURFACE      = 0x00200000,
        TEXTURE         = 0x00400000,
        RENDERER        = 0x01000000,
        TONEMAPPER      = 0x02000000,
        FRAMEBUFFER     = 0x04000000,
        RENDERPASS      = 0x08000000,
        INTEGRATOR      = 0x10000000,
        SAMPLER_FACTORY = 0x20000000,
        PIXEL_FILTER    = 0x40000000,
        DENOISER        = 0x80000000,
        DRAWABLE        = POINTS    | CURVES        | MESH,
        SCENE_OBJECT    = XFORM     | CAMERA        | DRAWABLE      | LIGHT,
        GRAPH_ACTION    = ASSIGN    | SELECTOR      | RENDER_ACTION,
        SHADER          = MATERIAL  | SUBSURFACE    | TEXTURE,
        RTNODE          = RENDERER  | TONEMAPPER    | FRAMEBUFFER   | RENDERPASS | INTEGRATOR | SAMPLER_FACTORY | PIXEL_FILTER | DENOISER
    };
    ENUM_BITWISE_OPS(NodeClass)
}
}
