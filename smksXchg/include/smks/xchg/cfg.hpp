// ========================================================================= //
// Copyright 2016-2018 SUPAMONKS_STUDIO                                      //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
//                                                                           //
// ========================================================================= //
#pragma once

namespace smks { namespace xchg { namespace cfg
{
    const char* cache_anim              = "cache_anim"; // enable/disable animation caching
    const char* default_curve_width     = "default_curve_width";
    const char* default_point_width     = "default_point_width";
    const char* gl_texture_size         = "gl_texture_size";    // viewport's maximal texture resolution
    const char* gl_udim_tile_size       = "gl_udim_tile_size";  // viewport's maximal UDIM tile resolution
    const char* log_file                = "log_file";
    const char* num_threads             = "num_threads";
    const char* rendering_device        = "rendering_device";   // path used by the rendering client to find its device's module
    const char* rt_core_config          = "rt_core_config";     // configuration string passed to the rendering device
    const char* screen_height           = "screen_height";
    const char* screen_ratio            = "screen_ratio";
    const char* screen_width            = "screen_width";
    const char* viewport_animation      = "viewport_animation"; // enable/disable viewport's animation controls
    const char* viewport_auto_play      = "viewport_auto_play"; // enable/disable viewport's animation auto-play
    const char* viewport_camera_focus   = "viewport_camera_focus";  // enable/disable viewport camera's zoom-to-object
    const char* viewport_camera_switch  = "viewport_camera_switch"; // enable/disable viewport's camera switch
    const char* viewport_debug          = "viewport_debug";
    const char* viewport_device         = "viewport_device";    // path used by the viewport client to find its device's module
    const char* viewport_fbuffer_switch = "viewport_fbuffer_switch";    // enable/disable viewport's switch on framebuffer display
    const char* viewport_hud            = "viewport_hud";   // enable/disable viewport's head-up display
    const char* viewport_light_focus    = "viewport_light_focus";   // enable/disable viewport light's align-to-view
    const char* viewport_manipulator    = "viewport_manipulator";   // enable/disable selection manipulation in viewport
    const char* viewport_obj_loader     = "viewport_obj_loader";    // enable/disable viewport's prompt to load geometric content
    const char* viewport_orbit_camera   = "viewport_orbit_camera";  // enable/disable orbit controls on viewport's camera
    const char* viewport_picking        = "viewport_picking";   // enable/disable object picking in viewport
    const char* viewport_snapshot       = "viewport_snapshot";
    const char* window_title            = "window_title";
    const char* with_rendering          = "with_rendering"; // active/deactivate the creation of the scene's rendering client and device
    const char* with_viewport           = "with_viewport"; // active/deactivate the creation of the scene's viewport client and device
}
}
}
