// ========================================================================= //
// Copyright 2016-2018 SUPAMONKS_STUDIO                                      //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
//                                                                           //
// ========================================================================= //
#pragma once

#include "smks/xchg/Data.hpp"

namespace smks { namespace xchg
{
    class LockedDataStream;

    class DataStream
    {
    private:
        Data::WPtr  _data;
        size_t      _numElements;
        size_t      _stride;
        size_t      _offset;

    public:
        DataStream():
            _data       (),
            _numElements(0),
            _stride     (0),
            _offset     (0)
        { }
        DataStream(Data::Ptr const& data, size_t numElements, size_t stride, size_t offset):
            _data       (data),
            _numElements(numElements),
            _stride     (stride),
            _offset     (offset)
        { }

        virtual inline
        void
        clear()
        {
            _data.reset();
            _numElements    = 0;
            _stride         = 0;
            _offset         = 0;
        }

        inline
        void
        initialize(Data::Ptr const& data, size_t numElements, size_t stride, size_t offset)
        {
            _data           = data;
            _numElements    = numElements;
            _stride         = stride;
            _offset         = offset;
        }

        inline
        size_t
        numElements() const
        {
            return _numElements;
        }

        inline
        size_t
        stride() const
        {
            return _stride;
        }

        inline
        size_t
        offset() const
        {
            return _offset;
        }

        inline
        operator bool() const
        {
            return !_data.expired() &&
                _numElements > 0 &&
                _stride > 0;
        }
        inline
        bool
        lock(LockedDataStream&) const;
        virtual inline
        bool
        locked() const
        {
            return false;
        }

        template<typename ValueType> inline
        const ValueType*
        getPtr(size_t i) const
        {
            Data::Ptr const& data = _data.lock();

            if (!data)
                throw sys::Exception("Invalid data.", "Data Stream Value Getter");
            if (typeid(ValueType) != data->type())
                throw sys::Exception("Incompatible data type.", "Data Stream Value Getter");

            assert(i < numElements());

            return data->getPtr<ValueType>() + (_offset + i * _stride);
        }

        inline
        bool
        operator==(const DataStream& other) const
        {
            if (_numElements != other._numElements ||
                _stride != other._stride ||
                _offset != other._offset)
                return false;

            Data::Ptr const& data       = _data.lock();
            Data::Ptr const& otherData  = other._data.lock();

            if (!data || !otherData)
                return false;
            if (data.get() == otherData.get())
                return true;
            return data->operator==(*otherData);
        }

        friend inline
        std::ostream&
        operator<<(std::ostream& out, const DataStream& s)
        {
            Data::Ptr const& data = s._data.lock();

            out
                << "( #elements = " << s._numElements << ", "
                << "stride = " << s._stride << ", "
                << "offset = " << s._offset;
            if (data)
                out << ", data = " << *data;
            out << " )";
            return out;
        }
    };

    class LockedDataStream:
        public DataStream
    {
        friend class DataStream;
    private:
        Data::Ptr _lockedData;

    public:
        LockedDataStream():
            DataStream  (),
            _lockedData (nullptr)
        { }

        inline
        void
        clear()
        {
            _lockedData = nullptr;
            DataStream::clear();
        }
        inline
        bool
        locked() const
        {
            return true;
        }
    };

    inline
    bool
    xchg::DataStream::lock(LockedDataStream& ret) const
    {
        if (locked())
            throw sys::Exception("Already locked.", "Data Stream Locking");
        if (*this)
        {
            ret.DataStream::operator=(*this);
            ret._lockedData = _data.lock();
            return true;
        }
        else
        {
            ret.clear();
            return false;
        }
    }
}
}
