// ========================================================================= //
// Copyright 2016-2018 SUPAMONKS_STUDIO                                      //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
//                                                                           //
// ========================================================================= //
#pragma once

#include <memory>
#include <typeinfo>
#include <cassert>
#include <ostream>

#include <smks/sys/Exception.hpp>
#include "smks/xchg/equal.hpp"

namespace smks { namespace xchg
{
    /////////////
    // class Data
    /////////////
    class Data
    {
    public:
        typedef std::shared_ptr<Data>   Ptr;
        typedef std::weak_ptr<Data>     WPtr;

    private:
        /////////////////////////
        // class AbstractContent
        /////////////////////////
        class AbstractContent
        {
        public:
            virtual inline
            ~AbstractContent()
            { }

            virtual
            void
            dispose() = 0;

            virtual
            size_t
            size() const = 0;

            virtual
            const void*
            ptr() const = 0;

            virtual
            const std::type_info&
            type() const = 0;

            virtual
            size_t
            numBytesPerDatum() const = 0;

            inline
            bool
            operator==(const AbstractContent& other) const
            {
                if (size() != other.size() ||
                    type() != other.type() ||
                    numBytesPerDatum() != other.numBytesPerDatum())
                    return false;

                const char* bytes       = reinterpret_cast<const char*>(ptr());
                const char* otherBytes  = reinterpret_cast<const char*>(other.ptr());

                if (bytes == otherBytes)
                    return true;
                if (!bytes || !otherBytes)
                    return false;

                return memcmp(bytes, otherBytes, size() * numBytesPerDatum()) == 0;
            }
        };

        /////////////////
        // class Content
        /////////////////
        template<typename ValueType>
        class Content:
            public AbstractContent
        {
        private:
            bool        _allocated;
            size_t      _size;
            const void* _rawData;

        public:
            inline
            Content(const ValueType* data, size_t size, bool copy):
                _allocated  (false),
                _size       (size),
                _rawData    (nullptr)
            {
                if (_size == 0)
                    return;
                if (copy)
                {
                    ValueType* owned = new ValueType[_size];

                    for (size_t i = 0; i < _size; ++i)
                        owned[i] = data[i];
                    //memcpy(owned, data, sizeof(ValueType) * _size);
                    _allocated  = true;
                    _rawData    = reinterpret_cast<const void*>(owned);
                }
                else
                    _rawData    = reinterpret_cast<const void*>(data);
            }

            inline
            ~Content()
            {
                dispose();
            }

            virtual inline
            void
            dispose()
            {
                if (_allocated && _rawData)
                    delete[] reinterpret_cast<const ValueType*>(_rawData);
                _rawData    = nullptr;
                _allocated  = false;
                _size       = 0;
            }

            virtual inline
            size_t
            size() const
            {
                return _size;
            }

            virtual inline
            const void*
            ptr() const
            {
                return _rawData;
            }

            virtual inline
            const std::type_info&
            type() const
            {
                return typeid(ValueType);
            }

            virtual inline
            size_t
            numBytesPerDatum() const
            {
                return sizeof(ValueType);
            }
        };

    private:
        AbstractContent *_content;

    public:
        template<typename ValueType> static inline
        Ptr
        create(const ValueType* data, size_t size, bool copy)
        {
            Ptr ptr(new Data);
            ptr->initialize<ValueType>(data, size, copy);
            return ptr;
        }

    private:
        inline
        Data():
            _content(nullptr)
        { }
        // non-copyable
        Data(const Data&);
        Data& operator=(const Data&);

        template<typename ValueType> inline
        void
        initialize(const ValueType* data, size_t size, bool copy)
        {
            dispose();
            _content = new Content<ValueType>(data, size, copy);
        }

        inline
        void
        dispose()
        {
            if (_content)
                delete _content;
            _content = nullptr;
        }

    public:
        inline
        ~Data()
        {
            dispose();
        }

        inline
        size_t
        size() const
        {
            return _content ? _content->size() : 0;
        }

        inline
        bool
        empty() const
        {
            return size() == 0;
        }

        inline
        const std::type_info&
        type() const
        {
            return _content ? _content->type() : typeid(void);
        }

        template <typename ValueType> inline
        const ValueType*
        getPtr() const
        {
            if (typeid(ValueType) != _content->type())
                throw sys::Exception("Incompatible data type.", "Data Pointer Getter");
            return reinterpret_cast<const ValueType* const>(_content->ptr());
        }

        template <typename ValueType> inline
        const ValueType&
        get(size_t i) const
        {
            assert(i < size());
            if (typeid(ValueType) != _content->type())
                throw sys::Exception("Incompatible data type.", "Data Value Getter");
            return reinterpret_cast<const ValueType*>(_content->ptr())[i];
        }

        inline
        bool
        operator==(const Data& other) const
        {
            return _content && other._content &&
                *_content == *other._content;
        }

        friend inline
        std::ostream&
        operator<<(std::ostream& out, const Data& d)
        {
            return out << "( ptr = " << d._content->ptr() << ", size = " << d._content->size() << " )";
        }
    };

    // parameter type equality test
    template<> inline
    bool
    equal<xchg::Data::Ptr>(xchg::Data::Ptr const& left, xchg::Data::Ptr const& right)
    {
        return (left && right) ? (*left == *right) : (left.get() == right.get());
    }
}
}
