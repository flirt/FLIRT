// ========================================================================= //
// Copyright 2016-2018 SUPAMONKS_STUDIO                                      //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
//                                                                           //
// ========================================================================= //
#pragma once

#include <cassert>
#include <vector>

namespace smks { namespace xchg
{
    template <typename ObjectType>
    class Collection
    {
    private:
        size_t                      _numObjects;
        std::vector<ObjectType*>    _objects;   //<! takes ownership of objects
        std::vector<size_t>         _usedIDs;

    public:
        Collection():
            _numObjects (0),
            _objects    (),
            _usedIDs    ()
        {
            _objects.reserve(128);
            _usedIDs.reserve(32);
        }

    private:
        // non-copyable
        Collection(const Collection&);
        Collection& operator=(const Collection&);

    public:
        inline
        size_t
        add(ObjectType* obj)    //<! takes ownership of object
        {
            assert(obj);
            if (_usedIDs.empty())
            {
                _objects.push_back(obj);
                ++_numObjects;
                return _objects.size() - 1;
            }
            else
            {
                const size_t id = _usedIDs.back();
                _usedIDs.pop_back();
                _objects[id] = obj;
                ++_numObjects;
                return id;
            }
        }

        inline
        void
        remove(ObjectType* obj) //<! deletes the object
        {
            assert(obj);
            assert(obj->id() < _objects.size());
            _usedIDs.push_back(obj->id());
            _objects[obj->id()] = nullptr;
            --_numObjects;
            delete obj;
        }

        inline
        void
        clear()
        {
            for (size_t i = 0; i < _objects.size(); ++i)
                if (_objects[i])
                    remove(_objects[i]);
            _numObjects = 0;
            _objects.clear();
            _usedIDs.clear();
        }

        inline
        bool
        empty() const
        {
            return _numObjects == 0;
        }

        inline
        size_t
        size() const
        {
            return _objects.size();
        }

        inline
        ObjectType*
        get(size_t id)
        {
            assert(id < _objects.size());
            return _objects[id];
        }

        inline
        const ObjectType*
        get(size_t id) const
        {
            assert(id < _objects.size());
            return _objects[id];
        }
    };
}
}
