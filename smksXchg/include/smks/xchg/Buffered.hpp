// ========================================================================= //
// Copyright 2016-2018 SUPAMONKS_STUDIO                                      //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
//                                                                           //
// ========================================================================= //
#pragma once

#include "smks/xchg/Vector.hpp"

namespace smks { namespace xchg
{
    template <typename ValueType>
    class Buffered
    {
    private:
        mutable Vector<ValueType> _array;
    public:
        __forceinline
        Buffered():
            _array()
        { }
    private:
        Buffered(const Buffered&);
        Buffered& operator=(const Buffered&);
    public:
        __forceinline
        void
        clear()
        {
            _array.clear();
        }

        __forceinline
        size_t
        size() const
        {
            return _array.size();
        }

        __forceinline
        const ValueType&
        get(size_t pos) const
        {
            // automatically resize array.
            if (_array.size()<=pos)
                _array.resize(pos+1,0);

            return _array[pos];
        }

        __forceinline
        ValueType&
        get(size_t pos)
        {
            // automatically resize array.
            if (_array.size()<=pos)
                _array.resize(pos+1,0);

            return _array[pos];
        }
    };
}
}
