// ========================================================================= //
// Copyright 2016-2018 SUPAMONKS_STUDIO                                      //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
//                                                                           //
// ========================================================================= //
#pragma once

#include <iosfwd>
#include <cstdint>
#include <memory>
#include <smks/sys/configure_xchg.hpp>

namespace smks { namespace xchg
{
    class FLIRT_XCHG_EXPORT Digest
    {
    public:
        typedef std::shared_ptr<Digest> Ptr;
    private:
        class PImpl;
    private:
        PImpl *_pImpl;

    public:
        static inline
        Ptr
        create(const uint8_t d[16])
        {
            Ptr ptr(new Digest(d));
            return ptr;
        }

    private:
        Digest();
        explicit Digest(const uint8_t[16]);
        Digest(const Digest&);
    public:
        ~Digest();

        Digest&
        operator=(const Digest&);

        operator bool() const;

        bool
        operator==(const Digest&) const;

        bool
        operator<(const Digest&) const;
    private:
        std::ostream&
        print(std::ostream&) const;
    public:
        friend inline
        std::ostream&
        operator<<(std::ostream& out, const Digest& digest)
        {
            return digest.print(out);
        }
    };
}
}
