// ========================================================================= //
// Copyright 2016-2018 SUPAMONKS_STUDIO                                      //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
//                                                                           //
// ========================================================================= //
#pragma once

namespace smks { namespace xchg
{
    enum ParmType
    {
        UNKNOWN_PARMTYPE    = -1,
        //----------------------
        PARMTYPE_BOOL       = 0,
        PARMTYPE_UINT,
        PARMTYPE_INT,
        PARMTYPE_CHAR,
        PARMTYPE_SIZE,
        PARMTYPE_INT_2,
        PARMTYPE_INT_3,
        PARMTYPE_INT_4,
        PARMTYPE_FLOAT,
        PARMTYPE_FLOAT_2,
        PARMTYPE_FLOAT_4,
        PARMTYPE_AFFINE_3,
        PARMTYPE_STRING,
        PARMTYPE_IDSET,
        PARMTYPE_DATASTREAM,
        PARMTYPE_BOUNDINGBOX,
        PARMTYPE_DATA,
        PARMTYPE_OBJECT,
        //----------------------
        NUM_PARMTYPES
    };
}
}
