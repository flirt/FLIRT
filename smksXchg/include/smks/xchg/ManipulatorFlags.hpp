// ========================================================================= //
// Copyright 2016-2018 SUPAMONKS_STUDIO                                      //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
//                                                                           //
// ========================================================================= //
#pragma once

#include "macro_enum.hpp"

namespace smks { namespace xchg
{
    enum ManipulatorFlags
    {
        NO_MANIP            = 0,
        MANIP_OBJECT_MODE   = 1 << 0,
        MANIP_TRANSLATE     = 1 << 1,
        MANIP_ROTATE        = 1 << 2,
        MANIP_SCALE         = 1 << 3
    };
    ENUM_BITWISE_OPS(ManipulatorFlags)
}
}
