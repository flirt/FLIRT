// ========================================================================= //
// Copyright 2016-2018 SUPAMONKS_STUDIO                                      //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
//                                                                           //
// ========================================================================= //
#pragma once

#include "smks/sys/configure_xchg.hpp"

namespace smks { namespace xchg
{
    class NodeCode;

    namespace code
    {
        FLIRT_XCHG_EXPORT_VAR const NodeCode& ambient ();
        FLIRT_XCHG_EXPORT_VAR const NodeCode& ambientOcclusion ();
        FLIRT_XCHG_EXPORT_VAR const NodeCode& basicHair ();
        FLIRT_XCHG_EXPORT_VAR const NodeCode& blend ();
        FLIRT_XCHG_EXPORT_VAR const NodeCode& box ();
        FLIRT_XCHG_EXPORT_VAR const NodeCode& brushedMetal ();
        FLIRT_XCHG_EXPORT_VAR const NodeCode& bspline ();
#if defined(_DEBUG)
        FLIRT_XCHG_EXPORT_VAR const NodeCode& debugRayTexcoords ();
        FLIRT_XCHG_EXPORT_VAR const NodeCode& debugRayGeomNormal ();
        FLIRT_XCHG_EXPORT_VAR const NodeCode& debugHitGeomNormal ();
        FLIRT_XCHG_EXPORT_VAR const NodeCode& debugHitShadNormal ();
#endif // defined(_DEBUG)
        FLIRT_XCHG_EXPORT_VAR const NodeCode& default ();
        FLIRT_XCHG_EXPORT_VAR const NodeCode& depthOfField ();
        FLIRT_XCHG_EXPORT_VAR const NodeCode& dielectric ();
        FLIRT_XCHG_EXPORT_VAR const NodeCode& diffuseColor ();
        FLIRT_XCHG_EXPORT_VAR const NodeCode& directional ();
        FLIRT_XCHG_EXPORT_VAR const NodeCode& distant ();
        FLIRT_XCHG_EXPORT_VAR const NodeCode& eye ();
        FLIRT_XCHG_EXPORT_VAR const NodeCode& eyeNormal ();
        FLIRT_XCHG_EXPORT_VAR const NodeCode& eyePosition ();
        FLIRT_XCHG_EXPORT_VAR const NodeCode& eyeTangent ();
        FLIRT_XCHG_EXPORT_VAR const NodeCode& facingRatio ();
        FLIRT_XCHG_EXPORT_VAR const NodeCode& filmic ();
        FLIRT_XCHG_EXPORT_VAR const NodeCode& gammaCorrection ();
        FLIRT_XCHG_EXPORT_VAR const NodeCode& glass ();
        FLIRT_XCHG_EXPORT_VAR const NodeCode& glossyColor ();
        FLIRT_XCHG_EXPORT_VAR const NodeCode& hdri ();
        FLIRT_XCHG_EXPORT_VAR const NodeCode& idCoverage ();
        FLIRT_XCHG_EXPORT_VAR const NodeCode& lightPath ();
        FLIRT_XCHG_EXPORT_VAR const NodeCode& matte ();
        FLIRT_XCHG_EXPORT_VAR const NodeCode& mesh ();
        FLIRT_XCHG_EXPORT_VAR const NodeCode& metal ();
        FLIRT_XCHG_EXPORT_VAR const NodeCode& metallicPaint ();
        FLIRT_XCHG_EXPORT_VAR const NodeCode& mirror ();
        FLIRT_XCHG_EXPORT_VAR const NodeCode& microfacet ();
        FLIRT_XCHG_EXPORT_VAR const NodeCode& multijittered ();
        FLIRT_XCHG_EXPORT_VAR const NodeCode& nearest ();
        FLIRT_XCHG_EXPORT_VAR const NodeCode& normal ();
        FLIRT_XCHG_EXPORT_VAR const NodeCode& obj ();
        FLIRT_XCHG_EXPORT_VAR const NodeCode& ocio ();
        FLIRT_XCHG_EXPORT_VAR const NodeCode& pinhole ();
        FLIRT_XCHG_EXPORT_VAR const NodeCode& plastic ();
        FLIRT_XCHG_EXPORT_VAR const NodeCode& point ();
        FLIRT_XCHG_EXPORT_VAR const NodeCode& position ();
        FLIRT_XCHG_EXPORT_VAR const NodeCode& rayHistoFusion ();
        FLIRT_XCHG_EXPORT_VAR const NodeCode& shadowBias ();
        FLIRT_XCHG_EXPORT_VAR const NodeCode& shadows ();
        FLIRT_XCHG_EXPORT_VAR const NodeCode& singularColor ();
        FLIRT_XCHG_EXPORT_VAR const NodeCode& spot ();
        FLIRT_XCHG_EXPORT_VAR const NodeCode& subsurface ();
        FLIRT_XCHG_EXPORT_VAR const NodeCode& tangent ();
        FLIRT_XCHG_EXPORT_VAR const NodeCode& texcoords ();
        FLIRT_XCHG_EXPORT_VAR const NodeCode& thinDielectric ();
        FLIRT_XCHG_EXPORT_VAR const NodeCode& thinGlass ();
        FLIRT_XCHG_EXPORT_VAR const NodeCode& translucent ();
        FLIRT_XCHG_EXPORT_VAR const NodeCode& translucentSS ();
        FLIRT_XCHG_EXPORT_VAR const NodeCode& velvet ();
    }
}
}

