// ========================================================================= //
// Copyright 2016-2018 SUPAMONKS_STUDIO                                      //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
//                                                                           //
// ========================================================================= //
#pragma once

#include <memory>
#include <smks/sys/Exception.hpp>
#include "smks/xchg/GUIEventType.hpp"

namespace smks { namespace xchg
{
    class GUIInputEvent;
    class MouseEvent;
    class WheelEvent;
    class ResizeEvent;
    class KeyEvent;
    class PaintGLEvent;

    /////////////////
    // class GUIEvent
    /////////////////
    class GUIEvent
    {
    protected:
        GUIEventType _type;
    protected:
        explicit inline
        GUIEvent(GUIEventType type):
            _type(type)
        {
            if (type == NUM_GUI_EVENTS)
                throw sys::Exception("Invalid GUI event type.", "GUI Event Constructor");
        }
    public:
        inline GUIEventType type() const { return _type; }

        // casting operators
        virtual inline const GUIInputEvent* asInputEvent() const    { return nullptr; }
        virtual inline const PaintGLEvent*  asPaintGLEvent() const  { return nullptr; }
        virtual inline const ResizeEvent*   asResizeEvent() const   { return nullptr; }

        template<typename EventType>    static inline const EventType*      cast(const GUIEvent&);
        template<>                      static inline const MouseEvent*     cast<MouseEvent>(const GUIEvent&);
        template<>                      static inline const WheelEvent*     cast<WheelEvent>(const GUIEvent&);
        template<>                      static inline const ResizeEvent*    cast<ResizeEvent>(const GUIEvent&);
        template<>                      static inline const KeyEvent*   cast<KeyEvent>(const GUIEvent&);
        template<>                      static inline const PaintGLEvent*   cast<PaintGLEvent>(const GUIEvent&);
    };

    ///////////////////
    // class GUIInputEvent
    ///////////////////
    class GUIInputEvent:
        public GUIEvent
    {
    protected:
        int _keyboardModifiers;
    protected:
        explicit inline
        GUIInputEvent(GUIEventType type):
            GUIEvent(type), _keyboardModifiers(xchg::NO_MODIFIER)
        { }
    public:
        inline void keyboardModifiers(int value){ _keyboardModifiers = value;   }
        inline int  keyboardModifiers() const   { return _keyboardModifiers;    }

        // casting operators
        inline const GUIInputEvent*         asInputEvent() const    { return this; }

        virtual inline const MouseEvent*    asMouseEvent() const    { return nullptr; }
        virtual inline const KeyEvent*      asKeyEvent() const      { return nullptr; }
    };

    //////////////////////
    // class MouseEvent
    //////////////////////
    class MouseEvent:
        public GUIInputEvent
    {
    private:
        int         _x, _y;
        MouseButton _button;
    public:
        explicit inline
        MouseEvent(GUIEventType type):
            GUIInputEvent(type), _x(0), _y(0), _button(NO_BUTTON)
        {
            if (!encompasses(type))
                throw sys::Exception("Invalid GUI event type.", "Mouse Event Constructor");
        }

        inline void         x(int value)                { _x = value;       }
        inline int          x() const                   { return _x;        }
        inline void         y(int value)                { _y = value;       }
        inline int          y() const                   { return _y;        }
        inline void         button(MouseButton value)   { _button = value;  }
        inline MouseButton  button() const              { return _button;   }

        inline          const MouseEvent*   asMouseEvent() const { return this;     }
        virtual inline  const WheelEvent*   asWheelEvent() const { return nullptr;  }

        static inline
        bool
        encompasses(GUIEventType type)
        {
            return type == MOUSE_PRESS_EVENT        ||
                type == MOUSE_MOVE_EVENT            ||
                type == MOUSE_RELEASE_EVENT         ||
                type == MOUSE_DOUBLE_CLICK_EVENT    ||
                type == WHEEL_EVENT;
        }
    };

    //////////////////////
    // class WheelGUIEvent
    //////////////////////
    class WheelEvent:
        public MouseEvent
    {
    private:
        int _delta;
    public:
        inline
        WheelEvent():
            MouseEvent(WHEEL_EVENT), _delta(0)
        { }

        inline void delta(int value){ _delta = value;   }
        inline int  delta() const   { return _delta;    }

        // cast operators
        inline const WheelEvent* asWheelEvent() const { return this; }

        static inline
        bool
        encompasses(GUIEventType type)
        {
            return type == WHEEL_EVENT;
        }
    };

    /////////////////
    // class KeyEvent
    /////////////////
    class KeyEvent:
        public GUIInputEvent
    {
    private:
        int         _key;
        std::string _text;
        bool        _isAutoRepeat;
    public:
        explicit inline
        KeyEvent(GUIEventType type):
        GUIInputEvent(type), _key(0), _text(), _isAutoRepeat(false)
        { }

        inline void                 key(int value)                  { _key = value;         }
        inline int                  key() const                     { return _key;          }
        inline void                 text(const std::string& value)  { _text = value;        }
        inline const std::string&   text() const                    { return _text;         }
        inline void                 isAutoRepeat(bool value)        { _isAutoRepeat = value;}
        inline bool                 isAutoRepeat() const            { return _isAutoRepeat; }

        // cast operators
        virtual inline const KeyEvent* asKeyEvent() const { return this; }

        static inline
        bool
        encompasses(GUIEventType type)
        {
            return type == KEY_PRESS_EVENT ||
                type == KEY_RELEASE_EVENT;
        }
    };

    /////////////////////
    // class PaintGLEvent
    /////////////////////
    class PaintGLEvent:
        public GUIEvent
    {
    public:
        inline
        PaintGLEvent():
            GUIEvent(PAINT_GL_EVENT)
        { }

        virtual inline const PaintGLEvent* asPaintGLEvent() const { return this; }

        static inline
        bool
        encompasses(GUIEventType type)
        {
            return type == PAINT_GL_EVENT;
        }
    };

    ////////////////////
    // class ResizeEvent
    ////////////////////
    class ResizeEvent:
        public GUIEvent
    {
    private:
        int _width, _height, _oldWidth, _oldHeight;
    public:
        inline
        ResizeEvent():
            GUIEvent(RESIZE_EVENT), _width(0), _height(0), _oldWidth(0), _oldHeight(0)
        { }

        inline void width(int value)    { _width = value;       }
        inline int  width() const       { return _width;        }
        inline void height(int value)   { _height = value;      }
        inline int  height() const      { return _height;       }
        inline void oldWidth(int value) { _oldWidth = value;    }
        inline int  oldWidth() const    { return _oldWidth;     }
        inline void oldHeight(int value){ _oldHeight = value;   }
        inline int  oldHeight() const   { return _oldHeight;    }

        inline const ResizeEvent* asResizeEvent() const { return this; }

        static inline
        bool
        encompasses(GUIEventType type)
        {
            return type == RESIZE_EVENT;
        }
    };

    template<typename EventType> /*static*/ inline
    const EventType*
    GUIEvent::cast(const GUIEvent&)
    {
        throw sys::Exception("Unsupported GUI event type.", "GUI Event Cast");
    }

    template<> /*static*/ inline
    const MouseEvent*
    GUIEvent::cast<MouseEvent>(const GUIEvent& evt)
    {
        const GUIInputEvent* inEvt = evt.asInputEvent();
        return inEvt ? inEvt->asMouseEvent() : nullptr;
    }

    template<> /*static*/ inline
    const KeyEvent*
    GUIEvent::cast<KeyEvent>(const GUIEvent& evt)
    {
        const GUIInputEvent* inEvt = evt.asInputEvent();
        return inEvt ? inEvt->asKeyEvent() : nullptr;
    }

    template<> /*static*/ inline
    const WheelEvent*
    GUIEvent::cast<WheelEvent>(const GUIEvent& evt)
    {
        const MouseEvent* mEvt = cast<MouseEvent>(evt);
        return mEvt ? mEvt->asWheelEvent() : nullptr;
    }

    template<> /*static*/ inline
    const ResizeEvent*
    GUIEvent::cast<ResizeEvent>(const GUIEvent& evt)
    {
        return evt.asResizeEvent();
    }

    template<> /*static*/ inline
    const PaintGLEvent*
    GUIEvent::cast<PaintGLEvent>(const GUIEvent& evt)
    {
        return evt.asPaintGLEvent();
    }
}
}
