// ========================================================================= //
// Copyright 2016-2018 SUPAMONKS_STUDIO                                      //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
//                                                                           //
// ========================================================================= //
#pragma once

#include <cstdint>
#include <ostream>

#include "smks/sys/configure_xchg.hpp"

namespace smks { namespace xchg
{
    class FLIRT_XCHG_EXPORT IdSet
    {
    public:
        //////////////////////////////
        // class IdSet::const_iterator
        //////////////////////////////
        class FLIRT_XCHG_EXPORT const_iterator
        {
            friend class IdSet;
        private:
            struct PImpl;
        private:
            PImpl* _pImpl;

        public:
            bool
            operator==(const const_iterator&) const;

            inline
            bool
            operator!=(const const_iterator& other) const
            {
                return !this->operator==(other);
            }

            const_iterator&
            operator++();

            uint32_t
            operator*() const;

        private:
            const_iterator();
        public:
            const_iterator(const const_iterator&);
            const_iterator& operator=(const const_iterator&);

            ~const_iterator();
        };

    private:
        class PImpl;
    private:
        PImpl* _pImpl;

    public:
        IdSet();
        explicit IdSet(uint32_t);
        IdSet(const IdSet&);

        ~IdSet();

        IdSet&
        operator=(const IdSet&);

        void
        clear();

        void
        insert(uint32_t);

        void
        erase(uint32_t);

        const_iterator
        find(uint32_t) const;

        const_iterator
        begin() const;

        const_iterator
        end() const;

        bool
        empty() const;

        bool
        operator==(const IdSet&) const;

        inline
        bool
        operator!=(const IdSet& other) const
        {
            return !this->operator==(other);
        }

        size_t
        size() const;

        static
        void
        merge(xchg::IdSet&, const xchg::IdSet&);

        static
        void
        set_union(const IdSet&, const IdSet&, xchg::IdSet&);

        static
        void
        diff(const IdSet& previous, const IdSet& next, IdSet& added, IdSet& removed);

        friend inline
        std::ostream&
        operator<<(std::ostream& out, const smks::xchg::IdSet& x)
        {
            out << "{ ";
            for (const_iterator it = x.begin(); it != x.end(); ++it)
                out << *it << " ";
            out << "}";

            return out;
        }
    };
}
}
