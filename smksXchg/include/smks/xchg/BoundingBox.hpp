// ========================================================================= //
// Copyright 2016-2018 SUPAMONKS_STUDIO                                      //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
//                                                                           //
// ========================================================================= //
#pragma once

#include <limits>
#include <iostream>
#include <smks/math/types.hpp>

namespace smks { namespace xchg
{
    struct BoundingBox
    {
        float minX, minY, minZ, maxX, maxY, maxZ;

    public:
        static inline
        BoundingBox
        UnitBox()
        {
            return BoundingBox(-0.5f, -0.5f, -0.5f, 0.5f, 0.5f, 0.5f);
        }

    public:
        BoundingBox():
            minX(FLT_MAX),
            minY(FLT_MAX),
            minZ(FLT_MAX),
            maxX(-FLT_MAX),
            maxY(-FLT_MAX),
            maxZ(-FLT_MAX)
        { }

        BoundingBox(float xmin,
                    float ymin,
                    float zmin,
                    float xmax,
                    float ymax,
                    float zmax):
            minX(xmin < xmax ? xmin : xmax),
            minY(ymin < ymax ? ymin : ymax),
            minZ(zmin < zmax ? zmin : zmax),
            maxX(xmin < xmax ? xmax : xmin),
            maxY(ymin < ymax ? ymax : ymin),
            maxZ(zmin < zmax ? zmax : zmin)
        { }

        inline
        void
        clear()
        {
            minX    = FLT_MAX;
            minY    = FLT_MAX;
            minZ    = FLT_MAX;
            maxX    = -FLT_MAX;
            maxY    = -FLT_MAX;
            maxZ    = -FLT_MAX;
        }

        inline
        bool
        valid() const
        {
            return
                !(minX > maxX) &&
                !(minY > maxY) &&
                !(minZ > maxZ);
        }

        inline
        bool
        initialize(float xmin,
                   float ymin,
                   float zmin,
                   float xmax,
                   float ymax,
                   float zmax)
        {
            minX = xmin < xmax ? xmin : xmax;
            minY = ymin < ymax ? ymin : ymax;
            minZ = zmin < zmax ? zmin : zmax;
            maxX = xmin < xmax ? xmax : xmin;
            maxY = ymin < ymax ? ymax : ymin;
            maxZ = zmin < zmax ? zmax : zmin;
            return valid();
        }

        inline
        bool
        initialize(const float* ptr,
                   size_t       size,
                   size_t       stride)
        {
            clear();
            if (ptr &&
                size > 0 &&
                stride >= 3)
            {
                const float* current = ptr;
                for (size_t i = 0; i < size; ++i)
                {
                    operator+=(current);
                    current += stride;
                }
            }
            return valid();
        }

        inline
        bool
        operator==(const BoundingBox& other) const
        {
            return
                fabsf(minX - other.minX) < 1e-6f &&
                fabsf(minY - other.minY) < 1e-6f &&
                fabsf(minZ - other.minZ) < 1e-6f &&
                fabsf(maxX - other.maxX) < 1e-6f &&
                fabsf(maxY - other.maxY) < 1e-6f &&
                fabsf(maxZ - other.maxZ) < 1e-6f;
        }

        inline
        BoundingBox&
        operator+=(const float p[3])
        {
            minX    = p[0] < minX ? p[0] : minX;
            minY    = p[1] < minY ? p[1] : minY;
            minZ    = p[2] < minZ ? p[2] : minZ;
            maxX    = p[0] > maxX ? p[0] : maxX;
            maxY    = p[1] > maxY ? p[1] : maxY;
            maxZ    = p[2] > maxZ ? p[2] : maxZ;

            return *this;
        }

        inline
        BoundingBox&
        operator+=(const math::Vector4f& p)
        {
            return this->operator+=(p.data());
        }

        inline
        BoundingBox&
        operator+=(const BoundingBox& other)
        {
            if (other.valid())
            {
                this->operator+=(&other.minX);
                this->operator+=(&other.maxX);
            }
            return *this;
        }

        inline
        math::Vector4f
        min() const
        {
            return math::Vector4f(minX, minY, minZ, 1.0f);
        }

        inline
        math::Vector4f
        max() const
        {
            return math::Vector4f(maxX, maxY, maxZ, 1.0f);
        }

        inline
        void
        corner(size_t i, float p[3]) const
        {
            p[0] = ( i       & 1) ? minX : maxX;
            p[1] = ((i >> 1) & 1) ? minY : maxY;
            p[2] = ((i >> 2) & 1) ? minZ : maxZ;
        }
        inline
        math::Vector4f
        corner(size_t i) const
        {
            math::Vector4f ret(math::Vector4f::Ones());
            corner(i, ret.data());
            return ret;
        }

        friend inline
        std::ostream&
        operator<<(std::ostream& out, const BoundingBox& b)
        {
            return out
                << "( " << b.minX << " " << b.minY << " " << b.minZ << " ) -> "
                << "( " << b.maxX << " " << b.maxY << " " << b.maxZ << " )";
        }
    };
}
}
