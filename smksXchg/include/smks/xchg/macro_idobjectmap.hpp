// ========================================================================= //
// Copyright 2016-2018 SUPAMONKS_STUDIO                                      //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
//                                                                           //
// ========================================================================= //
#pragma once

// macros to use in order to quickly declare and implement id to object dictionaries
#define OBJECT_DICT_DECL(_OBJECT_DICT_, _OBJECT_TYPE_, _EXPORTS_)                                   \
    class _OBJECT_TYPE_;                                                                            \
    class _EXPORTS_ _OBJECT_DICT_                                                                   \
    {                                                                                               \
    private:                                                                                        \
        class PImpl;                                                                                \
    private:                                                                                        \
        PImpl* _pImpl;                                                                              \
                                                                                                    \
    public:                                                                                         \
        _OBJECT_DICT_();                                                                            \
    private:                                                                                        \
        _OBJECT_DICT_(const _OBJECT_DICT_&);                                                        \
        _OBJECT_DICT_& operator=(const _OBJECT_DICT_&);                                             \
    public:                                                                                         \
        ~_OBJECT_DICT_();                                                                           \
                                                                                                    \
        std::shared_ptr<_OBJECT_TYPE_> const&   add(std::shared_ptr<_OBJECT_TYPE_> const&);         \
        std::shared_ptr<_OBJECT_TYPE_>          begin() const;                                      \
        std::shared_ptr<_OBJECT_TYPE_>          next(std::shared_ptr<_OBJECT_TYPE_> const&) const;  \
        std::shared_ptr<_OBJECT_TYPE_>          find(unsigned int) const;                           \
        std::shared_ptr<_OBJECT_TYPE_>          find(const char*) const;                            \
    };

#define OBJECT_DICT_IMPL(_OBJECT_DICT_, _OBJECT_TYPE_)                                                                                                          \
    class _OBJECT_DICT_::PImpl                                                                                                                                  \
    {                                                                                                                                                           \
    public:                                                                                                                                                     \
        ::smks::xchg::IdObjectMap<_OBJECT_TYPE_> m;                                                                                                             \
    };                                                                                                                                                          \
                                                                                                                                                                \
    _OBJECT_DICT_::_OBJECT_DICT_(): _pImpl(new PImpl()) {}                                                                                                      \
    _OBJECT_DICT_::~_OBJECT_DICT_()                     { delete _pImpl; }                                                                                      \
                                                                                                                                                                \
    std::shared_ptr<_OBJECT_TYPE_> const&   _OBJECT_DICT_::add(std::shared_ptr<_OBJECT_TYPE_> const& obj)               { return _pImpl->m.add(obj);        }   \
    std::shared_ptr<_OBJECT_TYPE_>          _OBJECT_DICT_::begin() const                                                { return _pImpl->m.begin();         }   \
    std::shared_ptr<_OBJECT_TYPE_>          _OBJECT_DICT_::next(std::shared_ptr<_OBJECT_TYPE_> const& current) const    { return _pImpl->m.next(current);   }   \
    std::shared_ptr<_OBJECT_TYPE_>          _OBJECT_DICT_::find(unsigned int id) const                                  { return _pImpl->m.find(id);        }   \
    std::shared_ptr<_OBJECT_TYPE_>          _OBJECT_DICT_::find(const char* str) const                                  { return _pImpl->m.find(str);       }
