// ========================================================================= //
// Copyright 2016-2018 SUPAMONKS_STUDIO                                      //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
//                                                                           //
// ========================================================================= //
#pragma once

#include <cassert>
#include <smks/sys/Exception.hpp>

namespace smks { namespace xchg
{
    ///////////////////
    // enum ClientEvent
    ///////////////////
    class ClientEvent
    {
    public:
        ////////////
        // enum Type
        ////////////
        enum Type
        {
            UNKNOWN     = -1,
            SCENE_SET   = 0,
            SCENE_UNSET,
            BINDING_CREATED,
            BINDING_DELETED
        };

    private:
        Type            _type;
        size_t          _numIds;
        unsigned int*   _ids;

    public:
        static inline
        ClientEvent
        sceneSet(unsigned int sceneId)
        {
            return ClientEvent(SCENE_SET, sceneId);
        }

        static inline
        ClientEvent
        sceneUnset(unsigned int sceneId)
        {
            return ClientEvent(SCENE_UNSET, sceneId);
        }

        static inline
        ClientEvent
        bindingCreated(unsigned int node)
        {
            return ClientEvent(BINDING_CREATED, node);
        }

        static inline
        ClientEvent
        bindingDeleted(unsigned int node)
        {
            return ClientEvent(BINDING_DELETED, node);
        }

    public:
        ClientEvent():
            _type   (UNKNOWN),
            _numIds (0),
            _ids    (nullptr)
        { }

        ClientEvent(const ClientEvent& evt):
            _type   (evt._type),
            _numIds (0),
            _ids    (nullptr)
        {
            copyIds(evt);
        }

        ~ClientEvent()
        {
            clearIds();
        }

        inline
        ClientEvent&
        operator=(const ClientEvent& evt)
        {
            _type = evt._type;
            copyIds(evt);
            return *this;
        }

        inline
        Type
        type() const
        {
            return _type;
        }

        inline
        bool
        empty() const
        {
            return _type == ClientEvent::UNKNOWN ||
                _numIds == 0 ||
                _ids[0] == 0;
        }

        //<! only relevant for scene-related events
        inline
        unsigned int
        scene() const
        {
            if (_type != Type::SCENE_SET &&
                _type != Type::SCENE_UNSET)
                throw sys::Exception(
                    "Irrelevant getter for client event.",
                    "Scene Getter");
            assert(_numIds > 0);
            return _ids[0];
        }

        //<! only relevant for scene-related events
        inline
        unsigned int
        node() const
        {
            if (_type != Type::BINDING_CREATED &&
                _type != Type::BINDING_DELETED)
                throw sys::Exception(
                    "Irrelevant getter for client event.",
                    "Node Getter");
            assert(_numIds > 0);
            return _ids[0];
        }

        inline
        bool
        operator==(const ClientEvent& other) const
        {
            if (_type != other._type ||
                _numIds != other._numIds)
                return false;
            for (size_t i = 0; i < _numIds; ++i)
                if (_ids[i] != other._ids[i])
                    return false;
            return true;
        }

    private:
        ClientEvent(Type type, unsigned int id0):
            _type   (type),
            _numIds (1),
            _ids    (new unsigned int[_numIds])
        {
            _ids[0] = id0;
        }

        inline
        void
        clearIds()
        {
            if (_ids)
                delete[] _ids;
            _ids    = nullptr;
            _numIds = 0;
        }

        inline
        void
        copyIds(const ClientEvent& evt)
        {
            clearIds();
            _numIds = evt._numIds;
            _ids    = new unsigned int[_numIds];
            memcpy(_ids, evt._ids, sizeof(unsigned int) * _numIds);
        }

    public:
        friend inline
        std::ostream&
        operator<<(std::ostream& out, const ClientEvent& evt)
        {
            switch (evt.type())
            {
            case ClientEvent::UNKNOWN:
                return out << "UNKNOWN";
            case ClientEvent::SCENE_SET:
                return out << "SCENE_SET\t[" << evt.scene() << "]";
            case ClientEvent::SCENE_UNSET:
                return out << "SCENE_UNSET\t[" << evt.scene() << "]";
            case ClientEvent::BINDING_CREATED:
                return out << "BINDING_CREATED\t[" << evt.node() << "]";
            case ClientEvent::BINDING_DELETED:
                return out << "BINDING_DELETED\t[" << evt.node() << "]";
            default:
                return out;
            }
        }
    };
}
}
