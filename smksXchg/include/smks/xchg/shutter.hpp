// ========================================================================= //
// Copyright 2016-2018 SUPAMONKS_STUDIO                                      //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
//                                                                           //
// ========================================================================= //
#pragma once

#include "smks/sys/configure_xchg.hpp"

namespace smks { namespace xchg
{
    enum ShutterType
    {
        UNKNOWN_SHUTTER     = -1,
        //------------------
        NO_SHUTTER          = 0, // no motion blur
        BACKWARD_SHUTTER    = 1,
        CENTERED_SHUTTER    = 2,
        FORWARD_SHUTTER     = 3,
        //------------------
        NUM_SHUTTER_TYPES
    };

    FLIRT_XCHG_EXPORT
    bool
    getShutterOpenAndClose(float time, float framerate, ShutterType shutterType, float shutterBias, float&, float&);

    FLIRT_XCHG_EXPORT
    ShutterType
    getShutterType(const char*);
}
}
