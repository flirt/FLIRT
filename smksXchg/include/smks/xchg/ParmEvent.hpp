// ========================================================================= //
// Copyright 2016-2018 SUPAMONKS_STUDIO                                      //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
//                                                                           //
// ========================================================================= //
#pragma once

#include <cassert>
#include <smks/parm/ParmFlags.hpp>
#include <smks/sys/Exception.hpp>

namespace smks { namespace xchg
{
    //////////////////
    // enum ParmEvent
    //////////////////
    class ParmEvent
    {
    public:
        ////////////
        // enum Type
        ////////////
        enum Type
        {
            UNKNOWN     = -1,
            PARM_ADDED  = 0,
            PARM_CHANGED,
            PARM_REMOVED
        };
    private:
        Type            _type;
        unsigned int    _parm;
        unsigned int    _node;
        parm::ParmFlags _flags;

    public:
        ParmEvent():
            _type   (UNKNOWN),
            _parm   (0),
            _node   (0),
            _flags  (parm::NO_PARM_FLAG)
        { }

        ParmEvent(Type              type,
                  unsigned int      parm,
                  unsigned int      node,
                  parm::ParmFlags   f):
            _type   (type),
            _parm   (parm),
            _node   (node),
            _flags  (f)
        {
            if (_type == UNKNOWN)
                throw sys::Exception(
                    "Invalid event type.",
                    "Parameter Event Creation");
            if (!_parm)
                throw sys::Exception(
                    "Invalid parameter.",
                    "Parameter Event Creation");
            if (!_node)
                throw sys::Exception(
                    "Invalid node.",
                    "Parameter Event Creation");
        }

        inline
        Type
        type() const
        {
            return _type;
        }

        inline
        unsigned int
        parm() const
        {
            return _parm;
        }

        inline
        unsigned int
        node() const
        {
            return _node;
        }

        inline
        parm::ParmFlags
        flags() const
        {
            return _flags;
        }

        inline
        void
        flags(parm::ParmFlags value)
        {
            _flags = value;
        }

        inline
        bool
        emittedByNode() const
        {
            return (_flags & parm::EMITTED_BY_NODE) != 0;
        }
        inline
        bool
        comesFromLink() const
        {
            return (_flags & parm::EMITTED_BY_NODE) == 0 &&
                (_flags & parm::TRAVERSES_LINKS) != 0;
        }
        inline
        bool
        comesFromAncestor() const
        {
            return (_flags & parm::EMITTED_BY_NODE) == 0 &&
                (_flags & parm::TRAVERSES_HIERARCHY) != 0;
        }
        inline
        bool
        isActiveLink() const
        {
            return (_flags & parm::IS_ACTIVE_LINK) != 0;
        }

    public:
        inline
        bool
        operator==(const ParmEvent& other) const
        {
            return _type == other._type &&
                _parm == other._parm &&
                _node == other._node;
        }

        inline
        operator bool() const
        {
            return _type != xchg::ParmEvent::UNKNOWN &&
                _parm != 0 &&
                _node != 0;
        }

        friend inline
        std::ostream&
        operator<<(std::ostream& out, const ParmEvent& evt)
        {
            switch (evt._type)
            {
            case ParmEvent::UNKNOWN:        out << "UNKNOWN     ";  break;
            case ParmEvent::PARM_ADDED:     out << "PARM_ADDED  ";  break;
            case ParmEvent::PARM_CHANGED:   out << "PARM_CHANGED";  break;
            case ParmEvent::PARM_REMOVED:   out << "PARM_REMOVED";  break;
            default:                                                break;
            }
            out << " [" << evt._node << "].'" << evt._parm << "'";
            parm::to_string(out, evt._flags);
            return out;
        }
    };
}
}
