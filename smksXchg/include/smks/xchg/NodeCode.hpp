// ========================================================================= //
// Copyright 2016-2018 SUPAMONKS_STUDIO                                      //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
//                                                                           //
// ========================================================================= //
#pragma once

#include <memory>

#include "smks/xchg/NodeClass.hpp"
#include "smks/sys/configure_xchg.hpp"

namespace smks { namespace xchg
{
    class FLIRT_XCHG_EXPORT NodeCode
    {
    public:
        typedef std::shared_ptr<NodeCode> Ptr;
    private:
        class PImpl;
    private:
        PImpl* _pImpl;
    public:
        static
        Ptr
        create(const char*, NodeClass, bool pythonExposed);
    private:
        NodeCode(const char*, NodeClass, bool);
        // non-copyable
        NodeCode(const NodeCode&);
        NodeCode& operator=(const NodeCode&);
    public:
        ~NodeCode();

        unsigned int
        id() const;

        const char*
        c_str() const;

        NodeClass
        nodeClasses() const;

        bool
        pythonExposed() const;
    };
}
}
