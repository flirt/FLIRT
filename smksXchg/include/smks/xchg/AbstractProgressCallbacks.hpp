// ========================================================================= //
// Copyright 2016-2018 SUPAMONKS_STUDIO                                      //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
//                                                                           //
// ========================================================================= //
#pragma once

#include "smks/sys/configure_xchg.hpp"

namespace smks { namespace xchg
{
    struct FLIRT_XCHG_EXPORT AbstractProgressCallbacks
    {
    public:
        typedef std::shared_ptr<AbstractProgressCallbacks> Ptr;
    public:
        virtual inline ~AbstractProgressCallbacks() { }

        virtual void begin  (const char*, size_t totalSteps)    = 0;
        virtual bool proceed(size_t currentStep)                = 0;    //<! returns false to stop process
        virtual void end    (const char*)                       = 0;
    };
}
}
