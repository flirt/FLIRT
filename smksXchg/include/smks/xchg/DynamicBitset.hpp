// ========================================================================= //
// Copyright 2016-2018 SUPAMONKS_STUDIO                                      //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
//                                                                           //
// ========================================================================= //
#pragma once

#include <cassert>
#include <cstdint>
#include <memory>

namespace smks { namespace xchg
{
    class DynamicBitset
    {
    private:
        typedef uint64_t word;
    private:
        const size_t    _WORD_SIZE;
        size_t          _size;  //<! in terms of bits
        size_t          _numWords;
        word*           _words;
    public:
        DynamicBitset():
            _WORD_SIZE  (sizeof(word) * CHAR_BIT),
            _size       (0),
            _numWords   (0),
            _words      (nullptr)
        { }
        DynamicBitset(const DynamicBitset& other):
            _WORD_SIZE  (sizeof(word) * CHAR_BIT),
            _size       (0),
            _numWords   (0),
            _words      (nullptr)
        {
            copy(other);
        }
        ~DynamicBitset()
        {
            clear();
        }

        DynamicBitset&
        operator=(const DynamicBitset& other)
        {
            copy(other);
            return *this;
        }
    private:
        void
        copy(const DynamicBitset& other)
        {
            clear();
            if (other._numWords)
            {
                _size       = other._size;
                _numWords   = other._numWords;
                _words      = new uint64_t[other._numWords];
                memcpy(_words, other._words, sizeof(uint64_t) * _numWords);
            }
        }
    public:
        void
        clear()
        {
            if (_words)
                delete[] _words;
            _words      = nullptr;
            _numWords   = 0;
            _size       = 0;
        }

        void
        resize(size_t size, bool value=false)   // capacity is expected in terms of bits
        {
            if (size == _size)
                return; // useless
            if (size == 0 &&
                _size > 0)
            {
                // deallocate memory
                clear();
                return;
            }

            size_t numWords = (size - 1) / _WORD_SIZE + 1;

            if (numWords > _numWords)
            {
                word* words = new word[numWords];

                // copy previous entries
                if (_numWords > 0)
                    memcpy(words, _words, _numWords * sizeof(word));

                // swap memory
                if (_words)
                    delete[] _words;
                _words      = words;
                _numWords   = numWords;
            }
            const size_t prvSize = _size;

            _size = size;
            assignLastValues(prvSize, value);
        }
    private:
        void
        assignLastValues(size_t start, bool value)
        {
            const size_t startIndex = start / _WORD_SIZE;
            const size_t startOffset = start % _WORD_SIZE;

            if (startIndex >= _numWords)
                return;
            if (startOffset == 0)
                _words[startIndex] = value ? -1 : 0;
            else
                for (size_t b = startOffset; b < _WORD_SIZE; ++b)
                    if (value)
                        _words[startIndex] |= (1i64 << (b % _WORD_SIZE));
                    else
                        _words[startIndex] &= ~(1i64 << (b % _WORD_SIZE));

            for (size_t n = startIndex + 1; n < _numWords; ++n)
                _words[n] = value ? -1 : 0;
        }

    public:
        void
        reset(bool value)
        {
            for (size_t n = 0; n < _numWords; ++n)
                _words[n] = value ? -1 : 0;
        }

        bool
        get(size_t b) const
        {
            return (_words[bIndex(b)] & (1i64 << bOffset(b))) != 0;
        }

        void
        set(size_t b, bool value)
        {
            if (value)
                _words[bIndex(b)] |= 1i64 << bOffset(b);
            else
                _words[bIndex(b)] &= ~(1i64 << bOffset(b));
        }

        size_t
        size() const
        {
            return _size;
        }
        size_t
        capacity() const
        {
            return _numWords * _WORD_SIZE;
        }
    private:
        size_t
        bIndex(size_t b) const
        {
            assert(b < _size);
            return b / _WORD_SIZE;
        }
        size_t
        bOffset(size_t b) const
        {
            assert(b < _size);
            return b % _WORD_SIZE;
        }
    };
}
}
