// ========================================================================= //
// Copyright 2016-2018 SUPAMONKS_STUDIO                                      //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
//                                                                           //
// ========================================================================= //
#pragma once

#include <string>
#include <smks/sys/Exception.hpp>
#include "smks/xchg/NodeClass.hpp"
#include "smks/xchg/render_pass.hpp"
#include "smks/xchg/shutter.hpp"
#include "smks/xchg/ParmType.hpp"
#include "smks/xchg/ParmEvent.hpp"
#include "smks/xchg/SceneEvent.hpp"
#include "smks/xchg/ActionState.hpp"
#include "smks/xchg/GUIEventType.hpp"
#include "smks/xchg/ManipulatorFlags.hpp"

namespace std
{
    inline
    string
    to_string(smks::xchg::NodeClass cl)
    {
        switch (cl)
        {
        case smks::xchg::NONE               : return "none";
        case smks::xchg::ANY                : return "generic node";
        case smks::xchg::SCENE              : return "scene";
        case smks::xchg::ARCHIVE            : return "archive";
        case smks::xchg::XFORM              : return "xform";
        case smks::xchg::CAMERA             : return "camera";
        case smks::xchg::POINTS             : return "points";
        case smks::xchg::CURVES             : return "curves";
        case smks::xchg::MESH               : return "mesh";
        case smks::xchg::LIGHT              : return "light";
        case smks::xchg::ASSIGN             : return "assign";
        case smks::xchg::SELECTOR           : return "selector";
        case smks::xchg::RENDER_ACTION      : return "render action";
        case smks::xchg::MATERIAL           : return "material";
        case smks::xchg::SUBSURFACE         : return "subsurface";
        case smks::xchg::TEXTURE            : return "texture";
        case smks::xchg::RENDERER           : return "renderer";
        case smks::xchg::TONEMAPPER         : return "tonemapper";
        case smks::xchg::FRAMEBUFFER        : return "framebuffer";
        case smks::xchg::RENDERPASS         : return "renderpass";
        case smks::xchg::INTEGRATOR         : return "integrator";
        case smks::xchg::SAMPLER_FACTORY    : return "sampler factory";
        case smks::xchg::PIXEL_FILTER       : return "pixel filter";
        case smks::xchg::DENOISER           : return "denoiser";
        case smks::xchg::DRAWABLE           : return "drawable";
        case smks::xchg::SCENE_OBJECT       : return "scene object";
        case smks::xchg::GRAPH_ACTION       : return "graph action";
        case smks::xchg::SHADER             : return "shader";
        case smks::xchg::RTNODE             : return "rtnode";
        //default: sys::Exception("Unsupported node class.", "Node Class To String");
        }

        // at this point, the specified nodeclass is a compound mask.
        static const smks::xchg::NodeClass CLASS_ARRAY[] =
        {
            // the array order is important (it defines how the mask is stringified) :
            // the 'largest' compound classes should appear first in order to provide the
            // most concise string representation.
            smks::xchg::ANY             ,
            smks::xchg::SCENE           ,
            smks::xchg::ARCHIVE         ,
            //----------------------
            smks::xchg::SCENE_OBJECT    ,
            //------------------
            smks::xchg::DRAWABLE        ,
            smks::xchg::POINTS          ,
            smks::xchg::CURVES          ,
            smks::xchg::MESH            ,
            //------------------
            smks::xchg::XFORM           ,
            smks::xchg::CAMERA          ,
            smks::xchg::LIGHT           ,
            //----------------------
            //----------------------
            smks::xchg::GRAPH_ACTION    ,
            smks::xchg::ASSIGN          ,
            smks::xchg::SELECTOR        ,
            smks::xchg::RENDER_ACTION   ,
            //----------------------
            //----------------
            smks::xchg::SHADER          ,
            smks::xchg::MATERIAL        ,
            smks::xchg::SUBSURFACE      ,
            smks::xchg::TEXTURE         ,
            //----------------
            //----------------
            smks::xchg::RTNODE          ,
            smks::xchg::RENDERER        ,
            smks::xchg::TONEMAPPER      ,
            smks::xchg::FRAMEBUFFER     ,
            smks::xchg::RENDERPASS      ,
            smks::xchg::INTEGRATOR      ,
            smks::xchg::SAMPLER_FACTORY ,
            smks::xchg::PIXEL_FILTER    ,
            smks::xchg::DENOISER
            //----------------
        };
        static const size_t CLASS_ARRAY_SIZE = sizeof(CLASS_ARRAY) / sizeof(smks::xchg::NodeClass);

        int         cl_ = cl;
        std::string ret = std::string();

        for (size_t i = 0; i < CLASS_ARRAY_SIZE; ++i)
            if ((cl_ & CLASS_ARRAY[i]) == CLASS_ARRAY[i])
            {
                if (ret.empty())
                    ret = to_string(CLASS_ARRAY[i]);
                else
                    ret += ", " + to_string(CLASS_ARRAY[i]);
                cl_ = cl_ & (~CLASS_ARRAY[i]);
            }

        return ret;
    }

    inline
    string
    to_string(smks::xchg::FlatRenderPassType typ)
    {
        switch (typ)
        {
        case smks::xchg::POSITION_PASS              : return "position";
        case smks::xchg::EYE_POSITION_PASS          : return "eyePosition";
        case smks::xchg::NORMAL_PASS                : return "normal";
        case smks::xchg::EYE_NORMAL_PASS            : return "eyeNormal";
        case smks::xchg::TANGENT_PASS               : return "tangent";
        case smks::xchg::EYE_TANGENT_PASS           : return "eyeTangent";
        case smks::xchg::TEXCOORDS_PASS             : return "texcoords";
        case smks::xchg::FACING_RATIO_PASS          : return "facingRatio";
        case smks::xchg::SUBSURFACE_PASS            : return "subsurface";
        case smks::xchg::SHADOWS_PASS               : return "shadows";
        case smks::xchg::SHADOW_BIAS_PASS           : return "shadowBias";
        case smks::xchg::AMBIENT_OCCLUSION_PASS     : return "ambientOcclusion";
        case smks::xchg::DIFFUSE_COLOR_PASS         : return "diffuseColor";
        case smks::xchg::GLOSSY_COLOR_PASS          : return "glossyColor";
        case smks::xchg::SINGULAR_COLOR_PASS        : return "singularColor";
#if defined(_DEBUG)
        case smks::xchg::DEBUG_RAY_TEXCOORDS_PASS   : return "debugRayTexcoords";
        case smks::xchg::DEBUG_RAY_GEOM_NORMAL_PASS : return "debugRayGeomNormal";
        case smks::xchg::DEBUG_HIT_GEOM_NORMAL_PASS : return "debugHitGeomNormal";
        case smks::xchg::DEBUG_HIT_SHAD_NORMAL_PASS : return "debugHitShadNormal";
#endif // defined(_DEBUG)
        default:
            throw smks::sys::Exception("Invalid render pass type.", "Flat Render Pass Type To String");
        }
    }

    inline
    string
    to_string(smks::xchg::ShutterType typ)
    {
        switch (typ)
        {
        case smks::xchg::NO_SHUTTER         : return "none";
        case smks::xchg::BACKWARD_SHUTTER   : return "backward";
        case smks::xchg::CENTERED_SHUTTER   : return "centered";
        case smks::xchg::FORWARD_SHUTTER    : return "forward";
        default:
            throw smks::sys::Exception("Invalid shutter type.", "Shutter Type To String");
        }
    }

    inline
    string
    to_string(smks::xchg::ParmType typ)
    {
        switch (typ)
        {
        case smks::xchg::UNKNOWN_PARMTYPE       : return "?";
        case smks::xchg::PARMTYPE_BOOL          : return "bool";
        case smks::xchg::PARMTYPE_UINT          : return "unsigned int";
        case smks::xchg::PARMTYPE_INT           : return "int";
        case smks::xchg::PARMTYPE_CHAR          : return "char";
        case smks::xchg::PARMTYPE_SIZE          : return "size";
        case smks::xchg::PARMTYPE_INT_2         : return "2-integer vector";
        case smks::xchg::PARMTYPE_INT_3         : return "3-integer vector";
        case smks::xchg::PARMTYPE_INT_4         : return "4-integer vector";
        case smks::xchg::PARMTYPE_FLOAT         : return "float";
        case smks::xchg::PARMTYPE_FLOAT_2       : return "2-float vector";
        case smks::xchg::PARMTYPE_FLOAT_4       : return "4-float vector";
        case smks::xchg::PARMTYPE_AFFINE_3      : return "3d affine transform";
        case smks::xchg::PARMTYPE_STRING        : return "string";
        case smks::xchg::PARMTYPE_IDSET         : return "ID set";
        case smks::xchg::PARMTYPE_DATASTREAM    : return "data stream";
        case smks::xchg::PARMTYPE_BOUNDINGBOX   : return "bounding box";
        case smks::xchg::PARMTYPE_DATA          : return "data";
        case smks::xchg::PARMTYPE_OBJECT        : return "object";
        default:
            throw smks::sys::Exception("Invalid parameter type.", "Parameter Type To String");
        }
    }

    //inline
    //string
    //to_string(smks::char act)
    //{
    //  switch (act)
    //  {
    //  case smks::xchg::INACTIVE       : return "inactive";
    //  case smks::xchg::ACTIVE         : return "active";
    //  case smks::xchg::ACTIVATING     : return "activating";
    //  case smks::xchg::DEACTIVATING   : return "deactivating";
    //  default:
    //      throw smks::sys::Exception("Invalid action state.", "Action State To String");
    //  }
    //}

    inline
    string
    to_string(smks::xchg::GUIEventType typ)
    {
        switch (typ)
        {
        case smks::xchg::MOUSE_PRESS_EVENT          : return "mouse press event";
        case smks::xchg::MOUSE_MOVE_EVENT           : return "mouse move event";
        case smks::xchg::MOUSE_RELEASE_EVENT        : return "mouse release event";
        case smks::xchg::MOUSE_DOUBLE_CLICK_EVENT   : return "mouse double click event";
        case smks::xchg::WHEEL_EVENT                : return "wheel event";
        case smks::xchg::RESIZE_EVENT               : return "resize event";
        case smks::xchg::PAINT_GL_EVENT             : return "paint GL event";
        case smks::xchg::NUM_GUI_EVENTS             : return "# gui events";
        default:
            throw smks::sys::Exception("Invalid GUI event type.", "GUI Event Type To String");
        }
    }

    inline
    string
    to_string(smks::xchg::ManipulatorFlags mode)
    {
        std::string ret = "";

        if (mode & smks::xchg::MANIP_TRANSLATE)     ret = "translate";
        else if (mode & smks::xchg::MANIP_ROTATE)   ret = "rotate";
        else if (mode & smks::xchg::MANIP_SCALE)    ret = "scale";

        if (ret.empty())
            ret = "no manipulation";
        else
            ret += mode & smks::xchg::MANIP_OBJECT_MODE
                ? " (object mode)"
                : " (world mode)";

        return ret;
    }

    inline
    string
    to_string(smks::xchg::ParmEvent::Type typ)
    {
        switch (typ)
        {
        case smks::xchg::ParmEvent::UNKNOWN         : return "unknown";
        case smks::xchg::ParmEvent::PARM_ADDED      : return "parameter added";
        case smks::xchg::ParmEvent::PARM_CHANGED    : return "parameter changed";
        case smks::xchg::ParmEvent::PARM_REMOVED    : return "parameter removed";
        default:
            throw smks::sys::Exception("Invalid parameter event type.", "Parameter Event Type To String");
        }
    }

    inline
    string
    to_string(smks::xchg::SceneEvent::Type typ)
    {
        switch (typ)
        {
        case smks::xchg::SceneEvent::UNKNOWN            : return "unknown";
        case smks::xchg::SceneEvent::NODE_CREATED       : return "node created";
        case smks::xchg::SceneEvent::NODE_DELETED       : return "node deleted";
        case smks::xchg::SceneEvent::PARM_CONNECTED     : return "parameter connected";
        case smks::xchg::SceneEvent::PARM_DISCONNECTED  : return "parameter disconnected";
        case smks::xchg::SceneEvent::NODES_LINKED       : return "nodes linked";
        case smks::xchg::SceneEvent::NODES_UNLINKED     : return "nodes unlinked";
        case smks::xchg::SceneEvent::LINK_ATTACHED      : return "link attached";
        case smks::xchg::SceneEvent::LINK_DETACHED      : return "link detached";
        default:
            throw smks::sys::Exception("Invalid scene event type.", "Scene Event Type To String");
        }
    }
}
