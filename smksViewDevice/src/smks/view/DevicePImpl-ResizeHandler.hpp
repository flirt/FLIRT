// ========================================================================= //
// Copyright 2016-2018 SUPAMONKS_STUDIO                                      //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
//                                                                           //
// ========================================================================= //
#pragma once

#include <osgGA/GUIEventHandler>

#include "DevicePImpl.hpp"

namespace smks { namespace view
{
    class DevicePImpl::ResizeHandler:
        public osgGA::GUIEventHandler
    {
    public:
        typedef osg::ref_ptr<ResizeHandler> Ptr;
    private:
        DevicePImpl& _pImpl;
    public:
        static inline
        Ptr
        create(DevicePImpl& pImpl)
        {
            Ptr ptr(new ResizeHandler(pImpl));
            return ptr;
        }

        static inline
        void
        destroy(Ptr& ptr)
        {
            ptr = nullptr;
        }

    protected:
        explicit
        ResizeHandler(DevicePImpl& pImpl):
            osgGA::GUIEventHandler(),
            _pImpl(pImpl)
        { }

    public:
        virtual inline
        bool
        handle(const osgGA::GUIEventAdapter&    ea,
               osgGA::GUIActionAdapter&         aa)
        {
            if (ea.getHandled())
                return false;

            switch (ea.getEventType())
            {
            case osgGA::GUIEventAdapter::RESIZE:
                _pImpl.handleResize(
                        ea.getWindowX(),
                        ea.getWindowY(),
                        ea.getWindowWidth(),
                        ea.getWindowHeight());
                return false;
            default:
                return false;
            }
        }
    };
}
}
