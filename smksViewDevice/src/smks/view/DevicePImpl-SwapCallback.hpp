// ========================================================================= //
// Copyright 2016-2018 SUPAMONKS_STUDIO                                      //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
//                                                                           //
// ========================================================================= //
#pragma once

#include <osg/GraphicsContext>

#include "DevicePImpl.hpp"

namespace smks { namespace view
{
    struct DevicePImpl::SwapCallback:
        public osg::GraphicsContext::SwapCallback
    {
    public:
        typedef osg::ref_ptr<SwapCallback> Ptr;
    public:
        static inline
        Ptr
        create()
        {
            Ptr ptr = new SwapCallback();
            return ptr;
        }

        static inline
        void
        destroy(Ptr& ptr)
        {
            ptr = nullptr;
        }
    protected:
        SwapCallback():
            osg::GraphicsContext::SwapCallback()
        { }

    public:
        virtual inline
        void
        swapBuffersImplementation(osg::GraphicsContext* context)
        {
            if (!context)
                return;

            // unbind vertex array object for better integration
            // with external OpenGL calls (overlay GUI for instance)
            if (context->getState())
                context->getState()->unbindVertexArrayObject();

            context->swapBuffersImplementation();
        }
    };
}
}
