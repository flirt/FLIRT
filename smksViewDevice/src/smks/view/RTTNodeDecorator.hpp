// ========================================================================= //
// Copyright 2016-2018 SUPAMONKS_STUDIO                                      //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
//                                                                           //
// ========================================================================= //
#pragma once

#include <memory>
#include <osg/observer_ptr>
#include <osg/Camera>

namespace osg
{
    class GraphicsContext;
    class Node;
}

namespace smks
{
    class ClientBase;
    namespace view
    {

        class RTTNodeDecorator:
            public osg::Group
        {
            friend class EventCallback;
        public:
            typedef osg::ref_ptr<RTTNodeDecorator> Ptr;
        private:
            struct EventCallback;
            class ScreenQuadCamera;
        private:
            typedef std::shared_ptr<ClientBase>     ClientPtr;
            typedef osg::ref_ptr<osg::Camera>       CameraPtr;
            typedef osg::ref_ptr<ScreenQuadCamera>  ScreenQuadCameraPtr;

        protected:
            enum { TEXTURE_UNIT = 0 };

        private:
            const osg::observer_ptr<osg::Node>  _decoratedNode;
            const osg::Camera::BufferComponent  _displayedAttachment;

            CameraPtr           _preRenderCamera;
            ScreenQuadCameraPtr _screenQuadCamera;
            CameraPtr           _postRenderCamera;

        public:
            static
            Ptr
            create(ClientPtr const&             client,
                   osg::Node*                   node,
                   osg::GraphicsContext*        context,
                   osg::Camera::BufferComponent displayedAttachment = osg::Camera::COLOR_BUFFER0,
                   unsigned int                 textureUnit         = 0);

        protected:
            RTTNodeDecorator(ClientPtr const&,
                             osg::Node*,
                             osg::GraphicsContext*,
                             osg::Camera::BufferComponent,
                             unsigned int);

        public:
            osg::Camera*
            preRenderCamera();

            const osg::Camera*
            preRenderCamera() const;

            osg::Camera*
            screenQuadCamera();

            const osg::Camera*
            screenQuadCamera() const;

            osg::Camera*
            postRenderCamera();

            const osg::Camera*
            postRenderCamera() const;

        protected:
            void
            finalize();

            virtual
            void
            setUpPreRenderCameraAttachments(osg::Camera*);

            void
            handleResize(int, int, int, int);
        };
    }
}
