// ========================================================================= //
// Copyright 2016-2018 SUPAMONKS_STUDIO                                      //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
//                                                                           //
// ========================================================================= //
#include <memory>
#include <osg/OperationThread>

#include <smks/xchg/IdSet.hpp>
#include <smks/ClientBase.hpp>
#include "DevicePImpl.hpp"

namespace smks
{
    class ClientBase;
    namespace view
    {
        //<! update whose role is to properly set-up cameras when a specific framebuffer is active
        class DevicePImpl::FrameBufferDisplayOperation:
            public osg::Operation
        {
            friend class ClientObserver;
        public:
            typedef osg::ref_ptr<FrameBufferDisplayOperation> Ptr;
        private:
            typedef std::shared_ptr<ClientBase> ClientPtr;
        private:
            unsigned int _displayedFrameBuffer;
            unsigned int _requestedFrameBuffer;
        public:
            static inline
            Ptr
            create(DevicePImpl&)
            {
                Ptr ptr(new FrameBufferDisplayOperation("__fbuffer_display_op", true));
                return ptr;
            }

            static inline
            void
            destroy(Ptr& ptr)
            {
                ptr = nullptr;
            }

        protected:
            FrameBufferDisplayOperation(const std::string&,
                                        bool keep);
        public:
            virtual
            void
            operator()(osg::Object*);

            inline
            unsigned int
            displayedFrameBuffer() const
            {
                return _displayedFrameBuffer;
            }

            inline
            void
            displayFrameBuffer(unsigned int node)
            {
                _requestedFrameBuffer = node;
                // will effectively be displayed at next update
            }
        };
    }
}
