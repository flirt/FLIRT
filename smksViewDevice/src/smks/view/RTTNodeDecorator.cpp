// ========================================================================= //
// Copyright 2016-2018 SUPAMONKS_STUDIO                                      //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
//                                                                           //
// ========================================================================= //
#include <iostream>

#include <osg/Group>
#include <osg/Geode>
#include <osg/MatrixTransform>
#include <osg/GraphicsContext>
#include <osg/Camera>
#include <osg/Geometry>
#include <osg/Texture2D>
#include <osgGA/EventVisitor>

#include <smks/sys/Exception.hpp>
#include <smks/ClientBase.hpp>
#include <smks/util/osg/NodeMask.hpp>
#include <smks/util/osg/texture.hpp>
#include <smks/util/osg/geometry.hpp>

#include "RTTNodeDecorator.hpp"
#include "RTTNodeDecorator-ScreenQuadCamera.hpp"
#include "ProgramUpdateCallback.hpp"


namespace smks { namespace view
{
    ///////////////////////
    // struct EventCallback
    ///////////////////////
    struct RTTNodeDecorator::EventCallback:
        public osg::NodeCallback
    {
    public:
        inline
        void
        operator() (osg::Node* node, osg::NodeVisitor* nv)
        {
            RTTNodeDecorator*       rtt = static_cast<RTTNodeDecorator*>    (node);
            osgGA::EventVisitor*    ev  = static_cast<osgGA::EventVisitor*> (nv);

            assert(rtt);
            assert(ev);

            const osgGA::EventQueue::Events&    events  = ev->getEvents();
            for (osgGA::EventQueue::Events::const_iterator it = events.begin();
                it != events.end();
                ++it)
            {
                const osgGA::GUIEventAdapter* ea = (*it)->asGUIEventAdapter();

                if (ea->getEventType() != osgGA::GUIEventAdapter::RESIZE)
                    continue;

                rtt->handleResize(ea->getWindowX(),
                                  ea->getWindowY(),
                                  ea->getWindowWidth(),
                                  ea->getWindowHeight());
                break;
            }
            traverse(node, nv);
        }
    };
}
}

using namespace smks;

// static
view::RTTNodeDecorator::Ptr
view::RTTNodeDecorator::create(ClientBase::Ptr  const&          client,
                               osg::Node*                       node,
                               osg::GraphicsContext*            context,
                               osg::Camera::BufferComponent     displayedAttachment,
                               unsigned int                     textureUnit)
{
    Ptr ptr = new RTTNodeDecorator(
        client,
        node,
        context,
        displayedAttachment,
        textureUnit);
    ptr->finalize();
    return ptr;
}

view::RTTNodeDecorator::RTTNodeDecorator(ClientBase::Ptr const&         client,
                                         osg::Node*                     node,
                                         osg::GraphicsContext*          context,
                                         osg::Camera::BufferComponent   displayedAttachment,
                                         unsigned int                   textureUnit):
    osg::Group          (),
    _decoratedNode      (node),
    _displayedAttachment(displayedAttachment),
    _preRenderCamera    (new osg::Camera()),
    _screenQuadCamera   (ScreenQuadCamera::create(client, textureUnit)),
    _postRenderCamera   (new osg::Camera())
{
    if (!_decoratedNode.valid())
        throw sys::Exception(
            "Invalid decorated node.",
            "RTT Node Decorator Construction");
    if (context == nullptr)
        throw sys::Exception(
            "Invalid graphics context.",
            "RTT Node Decorator Construction");

    const std::string baseName = _decoratedNode->getName().empty()
        ? "RTT"
        : "RTT[" + _decoratedNode->getName() + "]";

    setName("__" + baseName);

    // set render-to-texture camera and set specified subgraph as its child
    {
        _preRenderCamera->setName("__" + baseName + ".preRenderCamera");
        _preRenderCamera->setCullMask(util::osg::PRE_CAMERA_MASK);
        _preRenderCamera->setGraphicsContext(context);

        _preRenderCamera->setClearColor (osg::Vec4f(0.0f, 0.0f, 0.0f, 0.0f));
        _preRenderCamera->setClearMask  (GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

        // inherits the main camera's view and projection matrices
        _preRenderCamera->setReferenceFrame     (osg::Transform::RELATIVE_RF);
        _preRenderCamera->setViewMatrix         (osg::Matrixd::identity());
        _preRenderCamera->setProjectionMatrix   (osg::Matrixd::identity());

        _preRenderCamera->setRenderOrder(osg::Camera::PRE_RENDER);
        _preRenderCamera->setRenderTargetImplementation(osg::Camera::FRAME_BUFFER_OBJECT);

        // if no attachment dedicated to depth buffer, then add a renderbuffer to the camera's FBO
        if (_preRenderCamera->getBufferAttachmentMap().find(osg::Camera::DEPTH_BUFFER) == _preRenderCamera->getBufferAttachmentMap().end())
            _preRenderCamera->attach(osg::Camera::DEPTH_BUFFER, GL_DEPTH_COMPONENT24);
        //-----------------------------------------------
        _preRenderCamera->addChild(_decoratedNode.get());
        //-----------------------------------------------
    }

    {
        _screenQuadCamera->setName("__" + baseName + ".screenQuadCamera");
        _screenQuadCamera->setGraphicsContext(context);
    }

    {
        _postRenderCamera->setName("__" + baseName + ".postRenderCamera");
        _postRenderCamera->setCullMask(util::osg::POST_CAMERA_MASK);
        _postRenderCamera->setGraphicsContext(context);

        _postRenderCamera->setClearMask(0);

        // inherits the main camera's view and projection matrices
        _postRenderCamera->setReferenceFrame(osg::Transform::RELATIVE_RF);
        _postRenderCamera->setViewMatrix(osg::Matrixd::identity());
        _postRenderCamera->setProjectionMatrix(osg::Matrixd::identity());

        _postRenderCamera->setRenderOrder(osg::Camera::POST_RENDER);
        //-----------------------------------------------
        _postRenderCamera->addChild(_decoratedNode.get());
        //-----------------------------------------------
    }

    addChild(_preRenderCamera.get());
    addChild(_screenQuadCamera.get());
    addChild(_postRenderCamera.get());
    addEventCallback(new EventCallback());

    handleResize(context->getTraits()->x,
                 context->getTraits()->y,
                 context->getTraits()->width,
                 context->getTraits()->height);
}

osg::Camera*
view::RTTNodeDecorator::preRenderCamera()
{
    return _preRenderCamera.get();
}

const osg::Camera*
view::RTTNodeDecorator::preRenderCamera() const
{
    return _preRenderCamera.get();
}

osg::Camera*
view::RTTNodeDecorator::screenQuadCamera()
{
    return _screenQuadCamera.get();
}

const osg::Camera*
view::RTTNodeDecorator::screenQuadCamera() const
{
    return _screenQuadCamera.get();
}

osg::Camera*
view::RTTNodeDecorator::postRenderCamera()
{
    return _postRenderCamera.get();
}

const osg::Camera*
view::RTTNodeDecorator::postRenderCamera() const
{
    return _postRenderCamera.get();
}

void
view::RTTNodeDecorator::finalize()
{
    assert(_preRenderCamera);
    assert(_screenQuadCamera);
    assert(_postRenderCamera);

    setUpPreRenderCameraAttachments(_preRenderCamera.get());

    // retrieve the texture that should be displayed to the screen
    osg::Camera::BufferAttachmentMap::const_iterator const& it =
        _preRenderCamera->getBufferAttachmentMap().find(_displayedAttachment);

    if (it == _preRenderCamera->getBufferAttachmentMap().end())
        throw sys::Exception("Failed to find buffer to display.", "RTT Node Decorator Finalization");
    if (!it->second._texture.valid())
        throw sys::Exception("Invalid buffer.", "RTT Node Decorator Finalization");

    _screenQuadCamera->quad()->getOrCreateStateSet()->setTextureAttributeAndModes(
        _screenQuadCamera->textureUnit(),
        it->second._texture.get(),
        osg::StateAttribute::ON);
}

// virtual
void
view::RTTNodeDecorator::setUpPreRenderCameraAttachments(osg::Camera* camera)
{
    assert(camera);

    osg::ref_ptr<osg::Texture2D> colorBuffer = util::osg::createRGBATexture(false, osg::Texture::LINEAR, osg::Texture::LINEAR);

    colorBuffer->setName("__RTT.colorBufferDefault");
    colorBuffer->setDataVariance(Object::DYNAMIC);

    camera->attach(osg::Camera::COLOR_BUFFER0, colorBuffer);
}

void
view::RTTNodeDecorator::handleResize(int x, int y, int width, int height)
{
    const osg::Camera::BufferAttachmentMap& attachments = _preRenderCamera->getBufferAttachmentMap();

    for (osg::Camera::BufferAttachmentMap::const_iterator it = attachments.begin(); it != attachments.end(); ++it)
    {
        if (it->second._texture.valid())
        {
            osg::Texture2D* texture2D = dynamic_cast<osg::Texture2D*>(it->second._texture.get());

            if (texture2D)
                texture2D->setTextureSize(width, height);
            it->second._texture->dirtyTextureObject();
        }
    }

    if (_preRenderCamera)
        _preRenderCamera->setRenderingCache(nullptr);
}
