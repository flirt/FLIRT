// ========================================================================= //
// Copyright 2016-2018 SUPAMONKS_STUDIO                                      //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
//                                                                           //
// ========================================================================= //
#pragma once

#include <iostream>
#include <cassert>

#include "ManipulatorGeometry.hpp"
#include "DevicePImpl.hpp"

namespace smks { namespace view
{
    struct DevicePImpl::MouseCapturedByManipulatorCallback:
        public ManipulatorGeometry::MouseCapturedCallback
    {
    public:
        typedef std::shared_ptr<MouseCapturedByManipulatorCallback> Ptr;
    private:
        DevicePImpl& _pImpl;
    public:
        static inline
        Ptr
        create(DevicePImpl& pImpl)
        {
            Ptr ptr(new MouseCapturedByManipulatorCallback(pImpl));
            return ptr;
        }

        static inline
        void
        destroy(Ptr& ptr)
        {
            ptr = nullptr;
        }

    public:
        explicit
        MouseCapturedByManipulatorCallback(DevicePImpl& pImpl):
            _pImpl(pImpl)
        { }

        virtual inline
        void
        operator()(unsigned int contextId)
        {
            _pImpl.handleMouseCapturedByManipulator(contextId);
        }
    };
}
}
