// ========================================================================= //
// Copyright 2016-2018 SUPAMONKS_STUDIO                                      //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
//                                                                           //
// ========================================================================= //
#pragma once

#include <boost/unordered_map.hpp>
#include <osg/Projection>
#include <smks/AbstractClientObserver.hpp>

#include "DevicePImpl.hpp"

namespace smks { namespace view
{
    class DevicePImpl::CameraManager:
        public AbstractClientObserver
    {
    public:
        typedef std::shared_ptr<CameraManager> Ptr;
    private:
        ///////////////////////
        // struct CameraBinding
        ///////////////////////
        struct CameraBinding
        {
            osg::observer_ptr<osg::Node>        osgNode;    // camera's actual bound OSG entity
            osg::observer_ptr<osg::Projection>  osgProj;    // projection below osgNode in OSG hierarchy
        public:
            CameraBinding():
                osgNode(nullptr), osgProj(nullptr)
            { }
        };
        ///////////////////////
    private:
        typedef boost::unordered_map<unsigned int, CameraBinding> Cameras;
    private:
        DevicePImpl&    _pImpl;
        mutable Cameras _cameras;
        unsigned int    _viewportCameraXform;
        unsigned int    _viewportCamera;
        unsigned int    _displayedCamera;
    public:
        static inline
        Ptr
        create(DevicePImpl& pImpl)
        {
            Ptr ptr(new CameraManager(pImpl));
            ptr->build(ptr);
            return ptr;
        }

        static
        void
        destroy(Ptr& ptr)
        {
            if (ptr)
                ptr->dispose(ptr);
            ptr = nullptr;
        }

    protected:
        explicit
        CameraManager(DevicePImpl&);

        void
        build(Ptr const&);

        void
        dispose(Ptr const&);
    public:
        virtual
        void
        handle(ClientBase&,
               const xchg::ClientEvent&);

        virtual
        void
        handle(ClientBase&,
               const xchg::SceneEvent&);
    private:
        void
        handleSceneSet(ClientBase&, unsigned int);
        void
        handleSceneUnset(ClientBase&, unsigned int);

        void
        handleBindingCreated(ClientBase&, unsigned int);
        void
        handleBindingDeleted(ClientBase&, unsigned int);
    public:
        virtual
        void
        handleSceneParmEvent(ClientBase&,
                             const xchg::ParmEvent&);

        void
        handleFrameResized(ClientBase&, int x, int y, int width, int height);

        inline
        unsigned int
        viewportCameraXform() const
        {
            return _viewportCameraXform;
        }

        inline
        unsigned int
        viewportCamera() const
        {
            return _viewportCamera;
        }

        inline
        unsigned int
        displayedCamera() const
        {
            return _displayedCamera;
        }

        const osg::Projection*
        getDisplayedProjection() const;
    private:
        const osg::Projection*
        getProjection(unsigned int node) const;

    public:
        void
        displayCamera(unsigned int node);
    };
}
}
