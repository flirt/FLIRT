// ========================================================================= //
// Copyright 2016-2018 SUPAMONKS_STUDIO                                      //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
//                                                                           //
// ========================================================================= //
#include <smks/parm/BuiltIns.hpp>
#include <smks/parm/BuiltIn.hpp>
#include <smks/xchg/ClientEvent.hpp>
#include <smks/ClientBase.hpp>
#include <smks/ClientBindingBase.hpp>
#include <smks/client_node_filters.hpp>
#include <smks/util/osg/NodeMask.hpp>

#include "DevicePImpl-CameraManager.hpp"
#include "DevicePImpl-CameraManipulator.hpp"
#include "DevicePImpl-ClockTimeUpdate.hpp"
#include "DevicePImpl-FrameBufferDisplayOperation.hpp"
#include "DevicePImpl-GLProgramSwitch.hpp"
#include "DevicePImpl-PickingCallback.hpp"
#include "DevicePImpl-ResizeHandler.hpp"
#include "DevicePImpl-SwapCallback.hpp"
#include "ManipulatorGeometry.hpp"

namespace smks { namespace view
{
    /////////////////////////////////
    // class SelectProjectionsVisitor
    /////////////////////////////////
    class SelectProjectionsVisitor:
        public osg::NodeVisitor
    {
    private:
        typedef osg::ref_ptr<osg::Projection>   ProjectionPtr;
        typedef std::vector<ProjectionPtr>      Projections;
    private:
        Projections _projections;
    public:
        SelectProjectionsVisitor():
            osg::NodeVisitor(osg::NodeVisitor::NODE_VISITOR, osg::NodeVisitor::TRAVERSE_ALL_CHILDREN),
            _projections()
        {
            _projections.reserve(4);
        }

        inline
        const Projections&
        projections() const
        {
            return _projections;
        }
    private:
        inline
        void
        apply(osg::Projection& n)
        {
            _projections.push_back(&n);
            //---
            traverse(n);
        }
    };
}
}

using namespace smks;

// explicit
view::DevicePImpl::CameraManager::CameraManager(DevicePImpl& pImpl):
    _pImpl              (pImpl),
    _cameras            (),
    _viewportCameraXform(0),
    _viewportCamera     (0),
    _displayedCamera    (0)
{ }

void
view::DevicePImpl::CameraManager::build(Ptr const& ptr)
{
    dispose(ptr);

    ClientBase::Ptr const& client = _pImpl.client().lock();
    if (client)
    {
        client->registerObserver(ptr);
        handleSceneSet(*client, client->sceneId());
    }
}

void
view::DevicePImpl::CameraManager::dispose(Ptr const& ptr)
{
    ClientBase::Ptr const& client = _pImpl.client().lock();
    if (client)
    {
        handleSceneUnset(*client, client->sceneId());
        client->deregisterObserver(ptr);
    }
}

// virtual
void
view::DevicePImpl::CameraManager::handle(ClientBase&                client,
                                         const xchg::ClientEvent&   evt)
{
    switch (evt.type())
    {
    case xchg::ClientEvent::SCENE_SET:
        handleSceneSet(client, evt.scene());
        break;
    case xchg::ClientEvent::SCENE_UNSET:
        handleSceneUnset(client, evt.scene());
        break;
    case xchg::ClientEvent::BINDING_CREATED:
        handleBindingCreated(client, evt.node());
        break;
    case xchg::ClientEvent::BINDING_DELETED:
        handleBindingDeleted(client, evt.node());
        break;
    default:
        break;
    }
}

void
view::DevicePImpl::CameraManager::handleSceneSet(ClientBase&    client,
                                                 unsigned int   scene)
{
    if (scene == 0)
        return;

    // creates default camera
    _viewportCameraXform    = client.createXform    ("__default_xform",     scene);
    _viewportCamera         = client.createCamera   ("__default_camera",    _viewportCameraXform);

    // gathers all existing cameras and add entries to camera cache
    xchg::IdSet cameras;

    client.getIds(
        cameras,
        false,
        FilterClientNodeByClass::create(xchg::CAMERA));
    for (xchg::IdSet::const_iterator id = cameras.begin();
        id != cameras.end();
        ++id)
        handleBindingCreated(client, *id);

    // activates default camera
    _displayedCamera = _viewportCamera;
}

void
view::DevicePImpl::CameraManager::handleSceneUnset(ClientBase&  client,
                                                   unsigned int scene)
{
    // deactivates default camera
    _displayedCamera = 0;

    // deletes all remaining entries in camera cache
    while (!_cameras.empty())
        handleBindingDeleted(client, _cameras.begin()->first);

    // destroys default camera
    if (_viewportCamera)
        client.destroyDataNode(_viewportCamera);
    if (_viewportCameraXform)
        client.destroyDataNode(_viewportCameraXform);
    _viewportCamera     = 0;
    _viewportCameraXform    = 0;
}

void
view::DevicePImpl::CameraManager::handleBindingCreated(ClientBase&  client,
                                                       unsigned int node)
{
    if (client.getNodeClass(node) != xchg::CAMERA)
        return;

    ClientBindingBase::Ptr const&   bind    = client.getBinding(node);
    osg::Node*                      osgNode = bind && bind->asOsgObject()
        ? bind->asOsgObject()->asNode()
        : nullptr;

    if (osgNode)
    {
        CameraBinding camBinding;

        camBinding.osgNode  = osgNode;
        _cameras[node]      = camBinding;
    };
}

void
view::DevicePImpl::CameraManager::handleBindingDeleted(ClientBase&  client,
                                                       unsigned int node)
{
    // remove any cached projection associated with node
    Cameras::iterator const& it = _cameras.find(node);

    if (it != _cameras.end())
        _cameras.erase(it);
}

// virtual
void
view::DevicePImpl::CameraManager::handle(ClientBase&                client,
                                         const xchg::SceneEvent&    evt)
{ }

// virtual
void
view::DevicePImpl::CameraManager::handleSceneParmEvent(ClientBase&              client,
                                                       const xchg::ParmEvent&   evt)
{ }

void
view::DevicePImpl::CameraManager::handleFrameResized(ClientBase&    client,
                                                     int            x,
                                                     int            y,
                                                     int            width,
                                                     int            height)
{
    // synchronize trackball camera's aspect ratio
    const float xAspectRatio = height > 0
        ? static_cast<float>(width) / static_cast<float>(height)
        : 0.0f;

    if (_viewportCamera)
        client.setNodeParameter<float>(parm::xAspectRatio(), xAspectRatio, _viewportCamera);
}

const osg::Projection*
view::DevicePImpl::CameraManager::getDisplayedProjection() const
{
    return getProjection(_displayedCamera);
}

const osg::Projection*
view::DevicePImpl::CameraManager::getProjection(unsigned int node) const
{
    if (node == 0)
        return nullptr;

    Cameras::const_iterator const& it = _cameras.find(node);
    if (it == _cameras.end())
        return nullptr;

    osg::ref_ptr<osg::Projection> osgProj = nullptr;
    if (it->second.osgProj.lock(osgProj))
        return osgProj.get();   // camera's associated projection in cache

    // look for projection below osgNode and update cache
    osg::ref_ptr<osg::Node> osgNode;
    if (!it->second.osgNode.lock(osgNode))
        return nullptr; // nothing can be done at this point

    SelectProjectionsVisitor visitor;

    osgNode->accept(visitor);
    if (visitor.projections().empty())
        return nullptr;

    osgProj = visitor.projections().front();    // cache projection
    _cameras[node].osgProj = osgProj;

    return osgProj.get();
}

void
view::DevicePImpl::CameraManager::displayCamera(unsigned int node)
{
    //// all camera bindings are made visible by default
    //for (Cameras::iterator it = _cameras.begin(); it != _cameras.end(); ++it)
    //{
    //  osg::ref_ptr<osg::Node> osgNode;
    //  if (!it->second.osgNode.lock(osgNode))
    //      continue;
    //  osgNode->setNodeMask(util::osg::VISIBLE_SCENE_ENTITY);
    //}

    // if found, activate camera and turn its associated OSG avatar invisible
    {
        Cameras::iterator it = _cameras.find(node);
        if (it != _cameras.end())
        {
            _displayedCamera = node;

            //osg::ref_ptr<osg::Node> osgNode;
            //if (it->second.osgNode.lock(osgNode))
            //  osgNode->setNodeMask(util::osg::INVISIBLE_TO_ALL);
        }
    }
}
