// ========================================================================= //
// Copyright 2016-2018 SUPAMONKS_STUDIO                                      //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
//                                                                           //
// ========================================================================= //
#pragma once

#include <osg/Camera>

#include "DevicePImpl.hpp"

namespace smks { namespace view
{
    class DevicePImpl::PickingCallback:
        public osg::Camera::DrawCallback
    {
    public:
        typedef osg::ref_ptr<PickingCallback> Ptr;
    private:
        enum {INVALID_COORD=-1};
    private:
        DevicePImpl&        _pImpl;
        mutable osg::Vec2i  _position;
        bool                _accumulate;
        bool                _enabled;
    public:
        static inline
        Ptr
        create(DevicePImpl& pImpl)
        {
            Ptr ptr(new PickingCallback(pImpl));
            return ptr;
        }

        static inline
        void
        destroy(Ptr& ptr)
        {
            ptr = nullptr;
        }

    private:
        explicit
        PickingCallback(DevicePImpl&);

    public:
        inline
        void
        setup(int x, int y, bool accumulate)    //<! WARNING: Y orientation = Y increasing upwards
        {
            _position.set(x, y);
            _accumulate = accumulate;
        }

        inline
        void
        enabled(bool value)
        {
            _enabled = value;
        }

        virtual
        void
        operator()(osg::RenderInfo&) const;
    };
}
}
