// ========================================================================= //
// Copyright 2016-2018 SUPAMONKS_STUDIO                                      //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
//                                                                           //
// ========================================================================= //
#pragma once

#include <memory>
#include <cassert>

#include <osg/ref_ptr>
#include <osg/ArgumentParser>
#include <osg/GraphicsContext>

#include <smks/math/types.hpp>
#include <smks/xchg/ParmEvent.hpp>
#include <smks/xchg/VectorPtr.hpp>

#include "Device.hpp"
#include "smks/view/gl/ProgramInput.hpp"

namespace osg
{
    class Node;
    class Program;
    class Uniform;
    class Group;
}

namespace smks
{
    class ClientBase;
    namespace xchg
    {
        class IdSet;
        class ClientEvent;
        class SceneEvent;
        class ParmEvent;
    }
    namespace scn
    {
        class TreeNode;
        class Scene;
    }
    namespace view
    {
        namespace gl
        {
            class UniformInput;
        }
        class SceneGroup;
        class ManipulatorGeometry;

        class DevicePImpl
        {
        private:
            class   ClientObserver;
            class   ResizeHandler;
            struct  SwapCallback;
            class   KeepFBOsBoundCallback;
            class   LateCameraInitialization;
            class   CameraManipulator;
            class   CameraManager;
            class   FrameBufferDisplayOperation;
            class   ClockTimeUpdate;
            class   GLProgramSwitch;
            class   PickingCallback;
            struct  MouseCapturedByManipulatorCallback;
        public:
            typedef std::shared_ptr<DevicePImpl> Ptr;
        private:
            typedef osg::observer_ptr<Device>         DeviceWPtr;
            typedef std::shared_ptr<ClientBase>       ClientPtr;
            typedef std::weak_ptr<ClientBase>         ClientWPtr;
            typedef osg::ref_ptr<osg::Node>           NodePtr;
            typedef osg::ref_ptr<osg::Camera>         CameraPtr;
            typedef osg::ref_ptr<osg::Texture2D>      Texture2DPtr;
            typedef std::shared_ptr<gl::UniformInput> UniformPtr;
            typedef osg::ref_ptr<osg::Program>        ProgramPtr;

            typedef std::shared_ptr<MouseCapturedByManipulatorCallback> MouseCapturedCallbackPtr;

        protected:
            Device&                                   _self;

            Device::Config                            _cfg;
            ClientWPtr                                _client;    //<! also stores resources
            std::shared_ptr<ClientObserver>           _clientObserver;

            osg::ref_ptr<osg::GraphicsContext>        _graphicsContext;
            osg::ref_ptr<osg::Group>                  _clientRoot;
            osg::ref_ptr<osg::Group>                  _root;      //<! useful to allow addition of supplementary nodes above smks scene view

            const UniformPtr                          _uClockTime;
            const UniformPtr                          _uLightDirection;
            const UniformPtr                          _uSelectionColor;

            osg::ref_ptr<SwapCallback>                _swapCallback;      // swap callback (on graphics context)
            osg::ref_ptr<FrameBufferDisplayOperation> _fbufferDisplay;    // update operation
            osg::ref_ptr<ClockTimeUpdate>             _clockTimeUpdate;   // update callback (on client's root)
            osg::ref_ptr<PickingCallback>             _pickingCallback;   // post draw callback (on prerender camera)
            osg::ref_ptr<CameraManipulator>           _camManip;          // camera manipulator
            osg::ref_ptr<ResizeHandler>               _resizeHandler;     // event handler
            osg::ref_ptr<GLProgramSwitch>             _programSwitch;     // event handler

            osg::ref_ptr<osg::Group>                  _rootDecorator;

            osg::ref_ptr<ManipulatorGeometry>         _manipulator;
            MouseCapturedCallbackPtr                  _mouseCapturedCallback;

        public:
            DevicePImpl(Device&);

            void
            build(const char* jsonCfg,
                  ClientPtr const&,
                  osg::Group*,
                  osg::GraphicsContext*);

            void
            dispose();

        public:
            inline
            const Device&
            device() const
            {
                return _self;
            }

            inline
            Device&
            device()
            {
                return _self;
            }

            inline
            ClientWPtr const&
            client() const
            {
                return _client;
            }

            inline
            const osg::Group*
            clientRoot() const
            {
                return _clientRoot.get();
            }

            inline
            osg::Group*
            clientRoot()
            {
                return _clientRoot.get();
            }

            unsigned int
            viewportCameraXform() const;

            unsigned int
            viewportCamera() const;

            unsigned int
            displayedCamera() const;

            void
            displayCamera(unsigned int);

            unsigned int
            displayedFrameBuffer() const;

            void
            displayFrameBuffer(unsigned int);

            void
            pick(int x, int y, bool accumulate); //<! updates scene node's selection

            void
            setViewportLightDirection(const osg::Vec3f&);

            inline
            osg::Group*
            rootDecorator()
            {
                return _rootDecorator.get();
            }

            inline
            const osg::Group*
            rootDecorator() const
            {
                return _rootDecorator.get();
            }

            virtual
            void
            getUsage(osg::ApplicationUsage&) const;

        private:
            void
            updateClockTime(float);

        public:
            virtual
            void
            handle(ClientBase&, const xchg::ClientEvent&);
            virtual
            void
            handle(ClientBase&, const xchg::SceneEvent&);
            virtual
            void
            handleSceneParmEvent(ClientBase&, const xchg::ParmEvent&);

        private:
            void
            handleInitializationChanged(xchg::ParmEvent::Type, unsigned int);
            void
            handleSelectionChanged(xchg::ParmEvent::Type, unsigned int);
            void
            handleManipulatorFlagsChanged(xchg::ParmEvent::Type, unsigned int);

        public:
            void
            handleMouseCapturedByManipulator(unsigned int contextId);

        private:
            void
            handleResize(int x, int y, int width, int height);

            void
            synchronizeCameraSphereOfInterest(const xchg::IdSet&);

            void
            registerCamera(unsigned int);

            void
            deregisterCamera(unsigned int);
        };
    }
}
