// ========================================================================= //
// Copyright 2016-2018 SUPAMONKS_STUDIO                                      //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
//                                                                           //
// ========================================================================= //
#pragma once

#include <memory>
#include <boost/unordered_map.hpp>

#include <osg/Geometry>

#include <smks/xchg/ManipulatorFlags.hpp>
#include <smks/xchg/Buffered.hpp>
#include <smks/math/types.hpp>

namespace smks
{
    class ClientBase;
    namespace view
    {
        namespace gizmo
        {
            class IGizmo;
        }

        class ManipulatorGeometry:
            public osg::Geometry
        {
            friend class UpdateCallback;
            friend struct EventHandler;
        public:
            typedef osg::ref_ptr<ManipulatorGeometry> Ptr;
        private:
            struct EventHandler;
        private:
            /////////////////////////////
            // class PerContextManipData
            /////////////////////////////
            class PerContextManipData
            {
            private:
                gizmo::IGizmo*  _gizmo;
                size_t          _screenWidth, _screenHeight;
                GLuint          _vbo[3];    // [ vertices, triangles, lines ]
            public:
                PerContextManipData();

                void
                setGizmoMode(xchg::ManipulatorFlags);
                void
                setScreenSize(size_t, size_t);
                void
                destroyGizmo();

                size_t
                screenWidth() const;
                size_t
                screenHeight() const;
                gizmo::IGizmo*
                gizmo();
                const gizmo::IGizmo*
                gizmo() const;

                bool    areGLObjectsOk() const;
                GLuint& buffers();
                GLuint  vertexBuffer() const;
                GLuint  triangleBuffer() const;
                GLuint  lineBuffer() const;
            };
            typedef xchg::Buffered<PerContextManipData> PerContextGizmos;
            ///////////////////////////
            // struct OrigTransformData
            ///////////////////////////
            struct OrigTransformData
            {
                math::Affine3f localXfm;
                math::Affine3f worldXfm;
                math::Affine3f rParentWorldXfm;
            };
            ///////////////////////////
            enum { NO_CONTEXT = -1 };
        public:
            ////////////////////////////////////////
            // struct ManipulatorGeometry::MouseCapturedCallback
            ////////////////////////////////////////
            struct MouseCapturedCallback
            {
            public:
                typedef std::shared_ptr<MouseCapturedCallback> Ptr;
            public:
                virtual
                ~MouseCapturedCallback()
                { }

                virtual
                void
                operator()(unsigned int contextId) = 0;
            };
            ////////////////////////////////////////
            class UpdateCallback;
        private:
            typedef std::shared_ptr<ClientBase> ClientPtr;
            typedef std::weak_ptr<ClientBase>   ClientWPtr;
            typedef boost::unordered_map<unsigned int, OrigTransformData> OrigTransformDataMap;
        private:
            const ClientWPtr            _client;

            xchg::ManipulatorFlags       _mode;
            mutable PerContextGizmos    _gizmos;

            unsigned int                _lastId;            //<! ID of the last selected node (unto which the gizmo attaches itself)

            math::Affine3f              _editXfm;
            math::Affine3f              _rOrigEditXfm;      //<! inverse of gizmo's initial transform
            OrigTransformDataMap        _origXfmData;       //<! manipulated nodes' transforms prior manipulation

            unsigned int                _capturingContext;
            MouseCapturedCallback::Ptr  _onMouseCaptured;
        public:
            static
            Ptr
            create(ClientPtr const&);

            static
            void
            destroy(Ptr&);
        protected:
            explicit
            ManipulatorGeometry(ClientPtr const&);
        private:
            // non-copyable
            ManipulatorGeometry(const ManipulatorGeometry&);
            ManipulatorGeometry& operator=(const ManipulatorGeometry&);
        public:
            void
            mode(xchg::ManipulatorFlags);

            void
            onMouseCaptured(MouseCapturedCallback::Ptr const&);

            void
            manipulate(const xchg::IdSet&);

            virtual
            void
            drawImplementation(osg::RenderInfo&) const;
        private:
            bool
            isProgramUpToDate() const;
            void
            loadProgram();

            void
            dispose();
            void
            destroyGizmos();

            void
            filterManipulable(const xchg::IdSet&, xchg::IdSet&) const;

            unsigned int
            getLastId(const xchg::IdSet&) const;

            void
            startManipulation();

            void
            getOrigEditXfm(math::Affine3f&) const;

            void
            applyTransform();
            void
            process(const osgGA::GUIEventAdapter&);
        };
    }
}
