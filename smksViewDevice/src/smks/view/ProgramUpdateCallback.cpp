// ========================================================================= //
// Copyright 2016-2018 SUPAMONKS_STUDIO                                      //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
//                                                                           //
// ========================================================================= //
#include <osg/Node>
#include <osg/NodeVisitor>
#include <osg/Program>

#include <smks/sys/ResourceDatabase.hpp>
#include <smks/ClientBase.hpp>
#include <smks/view/gl/ProgramTemplate.hpp>

#include "ProgramUpdateCallback.hpp"

using namespace smks;

view::ProgramUpdateCallback::ProgramUpdateCallback(ClientBase::Ptr const&       client,
                                                   const gl::ProgramTemplate&   program):
    osg::Callback   (),
    _client         (client),
    _program        (program)
{ }

// virtual
bool
view::ProgramUpdateCallback::run(osg::Object* object, osg::Object* data)
{
    osg::Node* node = reinterpret_cast<osg::Node*>(object);
    if (node)
    {
        osg::StateSet* stateSet = node->getOrCreateStateSet();
        assert(stateSet);
        if (stateSet->getAttribute(osg::StateAttribute::PROGRAM) == nullptr)
        {
            ClientBase::Ptr const&              client      = _client.lock();
            sys::ResourceDatabase::Ptr const&   resources   = client ? client->getOrCreateResourceDatabase() : nullptr;
            osg::ref_ptr<osg::Program> const&   program     = resources ? gl::getOrCreateProgram(_program, resources) : nullptr;

            if (program)
                stateSet->setAttribute(program.get(), osg::StateAttribute::ON);
        }
    }
    return traverse(object, data);
}
