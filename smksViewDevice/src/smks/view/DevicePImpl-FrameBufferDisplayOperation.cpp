// ========================================================================= //
// Copyright 2016-2018 SUPAMONKS_STUDIO                                      //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
//                                                                           //
// ========================================================================= //
#include <iostream>

#include <smks/ClientBase.hpp>
#include <smks/ClientBindingBase.hpp>
#include <smks/util/osg/NodeMask.hpp>

#include "DevicePImpl-CameraManager.hpp"
#include "DevicePImpl-CameraManipulator.hpp"
#include "DevicePImpl-ClockTimeUpdate.hpp"
#include "DevicePImpl-FrameBufferDisplayOperation.hpp"
#include "DevicePImpl-GLProgramSwitch.hpp"
#include "DevicePImpl-PickingCallback.hpp"
#include "DevicePImpl-ResizeHandler.hpp"
#include "DevicePImpl-SwapCallback.hpp"
#include "ManipulatorGeometry.hpp"

using namespace smks;

view::DevicePImpl::FrameBufferDisplayOperation::FrameBufferDisplayOperation(const std::string&  name,
                                                                            bool                keep):
    osg::Operation          (name, keep),
    _displayedFrameBuffer   (0),
    _requestedFrameBuffer   (0)
{ }

// virtual
void
view::DevicePImpl::FrameBufferDisplayOperation::operator()(osg::Object* obj)
{
    if (_requestedFrameBuffer == _displayedFrameBuffer)
        return;

    Device*                 device  = dynamic_cast<Device*>(obj);
    ClientBase::Ptr const&  client  = device ? device->client().lock() : nullptr;

    if (!client ||
        !device->getCamera())
        return;

    if (_requestedFrameBuffer &&
        client->getNodeClass(_requestedFrameBuffer) != xchg::FRAMEBUFFER)
        _requestedFrameBuffer = 0;

    if (_requestedFrameBuffer == 0)
    {
        // no more framebuffer display, deactivate previously active framebuffer's camera
        ClientBindingBase::Ptr const&   bind        = client->getBinding(_displayedFrameBuffer);
        osg::Camera*                    fbufferCam  = bind && bind->asOsgObject()
            ? bind->asOsgObject()->asCamera()
            : nullptr;

        if (fbufferCam)
            fbufferCam->setNodeMask(util::osg::INVISIBLE_TO_ALL);
        // main forward rendering camera clears all main buffers again
        device->getCamera()->setClearMask(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    }
    else
    {
        // activate the newly-active framebuffer's camera
        ClientBindingBase::Ptr const&   bind        = client->getBinding(_requestedFrameBuffer);
        osg::Camera*                    fbufferCam  = bind && bind->asOsgObject()
            ? bind->asOsgObject()->asCamera()
            : nullptr;

        if (fbufferCam)
        {
            fbufferCam->setNodeMask(util::osg::VISIBLE_RT_ENTITY);  //
            fbufferCam->setClearColor(device->getCamera()->getClearColor());
        }
        // main forward camera can only clear depth buffer in order to have the
        // selected framebuffer's content appear in the background
        device->getCamera()->setClearMask(GL_DEPTH_BUFFER_BIT);
    }

    _displayedFrameBuffer = _requestedFrameBuffer;
}
