// ========================================================================= //
// Copyright 2016-2018 SUPAMONKS_STUDIO                                      //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
//                                                                           //
// ========================================================================= //

#include <osg/ValueObject>
#include <osg/Program>
#include <osg/Group>
#include <osg/Uniform>
#include <osg/FrontFace>
#include <osgUtil/Optimizer>
#include <osgViewer/config/SingleScreen>
#include <osgViewer/config/SingleWindow>

#include <smks/sys/Exception.hpp>
#include <smks/sys/Log.hpp>

#include <smks/xchg/IdSet.hpp>
#include <smks/parm/BuiltIns.hpp>
#include <smks/parm/BuiltIn.hpp>
#include <smks/math/matrixAlgo.hpp>
#include <smks/util/osg/math.hpp>
#include <smks/ClientBase.hpp>
#include <smks/ClientBindingBase.hpp>
#include <smks/client_node_filters.hpp>
#include <smks/view/gl/ProgramTemplate.hpp>
#include <smks/view/gl/ProgramTemplates.hpp>
#include <smks/view/gl/UniformDeclaration.hpp>
#include <smks/view/gl/ProgramInputDeclaration.hpp>
#include <smks/view/gl/ProgramInputDeclarations.hpp>

#include "DevicePImpl.hpp"
#include "DevicePImpl-FrameBufferDisplayOperation.hpp"
#include "DevicePImpl-MouseCapturedByManipulatorCallback.hpp"
#include "DevicePImpl-ResizeHandler.hpp"
#include "DevicePImpl-GLProgramSwitch.hpp"
#include "DevicePImpl-SwapCallback.hpp"
#include "DevicePImpl-PickingCallback.hpp"
#include "DevicePImpl-ClockTimeUpdate.hpp"
#include "DevicePImpl-KeepFBOsBoundCallback.hpp"
#include "DevicePImpl-CameraManipulator.hpp"
#include "MRTNodeDecorator.hpp"
#include "ManipulatorGeometry.hpp"
#include <smks/util/color/rgba8.hpp>
#include <smks/util/osg/NodeMask.hpp>
#include <smks/util/osg/callback.hpp>
#include <std/to_string_xchg.hpp>

namespace smks { namespace view
{
    static const std::string OPENGL_VERSION = "3.3";

    ///////////////////////
    // class ClientObserver
    ///////////////////////
    class DevicePImpl::ClientObserver:
        public AbstractClientObserver
    {
    public:
        typedef std::shared_ptr<ClientObserver> Ptr;
    private:
        DevicePImpl&    _pImpl;
    public:
        static inline
        Ptr
        create(DevicePImpl& pImpl)
        {
            Ptr ptr(new ClientObserver(pImpl));
            return ptr;
        }

        static inline
        void
        destroy(Ptr& ptr)
        {
            ptr = nullptr;
        }
    protected:
        ClientObserver(DevicePImpl& pImpl):
            _pImpl(pImpl)
        { }

        virtual inline
        void
        handle(ClientBase& client, const xchg::ClientEvent& e)
        {
            _pImpl.handle(client, e);
        }

        virtual inline
        void
        handle(ClientBase& client, const xchg::SceneEvent& e)
        {
            _pImpl.handle(client, e);
        }

        virtual inline
        void
        handleSceneParmEvent(ClientBase& client, const xchg::ParmEvent& e)
        {
            _pImpl.handleSceneParmEvent(client, e);
        }
    };
}
}

using namespace smks;

// explicit
view::DevicePImpl::DevicePImpl(Device& self):
    _self                 (self),
    _cfg                  (),
    _client               (),
    _clientObserver       (ClientObserver::create(*this)),
    //---------------------
    _graphicsContext      (nullptr),
    _clientRoot           (nullptr),
    _root                 (new osg::Group),
    //---------------------
    _uClockTime           (gl::uClockTime().asUniformDeclaration()->createInstance()),
    _uLightDirection      (gl::uLightDirection().asUniformDeclaration()->createInstance()),
    _uSelectionColor      (gl::uSelectionColor().asUniformDeclaration()->createInstance()),
    //---------------------
    _fbufferDisplay       (nullptr),
    _resizeHandler        (nullptr),
    _programSwitch        (nullptr),
    _camManip             (nullptr),
    _pickingCallback      (nullptr),
    _swapCallback         (nullptr),
    _clockTimeUpdate      (nullptr),
    //---------------------
    _rootDecorator        (nullptr),
    _manipulator          (nullptr),
    _mouseCapturedCallback(nullptr)
{
    _root->setName("__viewer_root");

    gl::setUniformfv(*_uSelectionColor->data(), math::Vector4f(0.263f, 1.0f, 0.639f, 1.0f).data());
}

void
view::DevicePImpl::dispose()
{
    ClientBase::Ptr const& client = _client.lock();

    _root->removeChildren(0, _root->getNumChildren());
    _rootDecorator  = nullptr;

    if (_manipulator)
        _manipulator->onMouseCaptured(nullptr);
    MouseCapturedByManipulatorCallback::destroy(_mouseCapturedCallback);
    ManipulatorGeometry::destroy(_manipulator);

    if (_programSwitch)
        _self.removeEventHandler(_programSwitch.get());
    if (_resizeHandler)
        _self.removeEventHandler(_resizeHandler.get());

    _self.setCameraManipulator(nullptr);

    if (_clientRoot)
    {
        if (_clockTimeUpdate)
            _clientRoot->removeUpdateCallback(_clockTimeUpdate.get());  // reverse order!

        osg::StateSet* stateSet = _clientRoot->getOrCreateStateSet();

        stateSet->removeUniform(_uClockTime     ->data());
        stateSet->removeUniform(_uLightDirection->data());
        stateSet->removeUniform(_uSelectionColor->data());
    }

    if (_fbufferDisplay)
        _self.removeUpdateOperation(_fbufferDisplay.get());

    if (_graphicsContext)
        _graphicsContext->setSwapCallback(nullptr);

    _clientRoot = nullptr;
    if (client)
        client->deregisterObserver(_clientObserver);
    _client.reset();

    GLProgramSwitch::destroy(_programSwitch);
    ResizeHandler::destroy(_resizeHandler);
    CameraManipulator::destroy(_camManip);
    PickingCallback::destroy(_pickingCallback);
    ClockTimeUpdate::destroy(_clockTimeUpdate);
    FrameBufferDisplayOperation::destroy(_fbufferDisplay);
    SwapCallback::destroy(_swapCallback);
}

void
view::DevicePImpl::build(const char*            jsonCfg,
                         ClientBase::Ptr const& client,
                         osg::Group*            clientRoot,
                         osg::GraphicsContext*  graphicsContext)
{
    dispose();

    if (!client)
        throw sys::Exception(
            "Invalid client.",
            "Viewport Device PImpl Building");
    if (clientRoot == nullptr)
        throw sys::Exception(
            "Invalid client root node.",
            "Viewport Device PImpl Building");

    _cfg = Device::Config(jsonCfg);

    //----------------------------------
    // broad configuration of the device
    _self.setThreadingModel(osgViewer::Viewer::SingleThreaded);
    _self.setRunFrameScheme(osgViewer::ViewerBase::ON_DEMAND);
    _self.setName(_cfg.windowTitle);
    //----------------------------------

    _self.osgViewer::Viewer::setSceneData(_root.get());

    int x      = 50;
    int y      = 50;
    int width  = static_cast<int>(_cfg.screenWidth);
    int height = static_cast<int>(_cfg.screenHeight);
    if (_cfg.screenRatio > 0.0f)
        height = static_cast<int>(floorf(static_cast<float>(width) / _cfg.screenRatio));
    if (width == 0 || height == 0)
        throw sys::Exception(
            "Invalid screen dimensions.",
            "Viewport Device PImpl Building");

    //---------------------------------------------------------------------
    _client = client;
    if (client)
        client->registerObserver(_clientObserver);
    _clientRoot         = clientRoot;
    _graphicsContext    = graphicsContext;
    if (_graphicsContext == nullptr)
    {
        // if no graphics context explicitly specified,
        // then create one from scratch in accordance with the configuration.
        osg::GraphicsContext::Traits* traits = new osg::GraphicsContext::Traits();

        traits->x                = x;
        traits->y                = y;
        traits->width            = width;
        traits->height           = height;
        traits->doubleBuffer     = true;
        traits->windowDecoration = true;
        traits->windowName       = std::string(_cfg.windowTitle);
        traits->glContextVersion = OPENGL_VERSION;
        traits->samples          = 1;

        _graphicsContext = osg::GraphicsContext::createGraphicsContext(traits);
    }
    if (!_graphicsContext)
        throw sys::Exception(
            "Failed to initialize graphics context.",
            "Viewport Device PImpl Building");

    _swapCallback    = SwapCallback::create();
    _fbufferDisplay  = FrameBufferDisplayOperation::create(*this);
    _pickingCallback = PickingCallback::create(*this);
    _clockTimeUpdate = ClockTimeUpdate::create(*this);
    _camManip        = CameraManipulator::create(*this);
    _resizeHandler   = ResizeHandler::create(*this);
    _programSwitch   = GLProgramSwitch::create(*this);

    _graphicsContext->setSwapCallback(_swapCallback);
    _graphicsContext->getState()->setUseModelViewAndProjectionUniforms(true);
    _graphicsContext->getState()->setUseVertexAttributeAliasing(true);

    _self.getCamera()->setGraphicsContext(_graphicsContext);

    osgViewer::GraphicsWindow* graphicsWindow = dynamic_cast<osgViewer::GraphicsWindow*>(_graphicsContext.get());
    if (graphicsWindow)
    {
        graphicsWindow->getEventQueue()->getCurrentEventState()->setWindowRectangle(x, y, width, height);
        _self.setEventQueue(graphicsWindow->getEventQueue());
    }

    _self.addUpdateOperation(_fbufferDisplay.get());

    _clientRoot->addUpdateCallback(_clockTimeUpdate.get());

    //---------------------------------------------------------------------
    // setup state set of client's OSG root
    osg::StateSet* stateSet = _clientRoot->getOrCreateStateSet();

    //stateSet->setAttributeAndModes(program, StateAttribute::ON);
    stateSet->setAttribute(new osg::FrontFace(osg::FrontFace::CLOCKWISE), osg::StateAttribute::ON | osg::StateAttribute::OVERRIDE);
    stateSet->setMode(GL_DEPTH_TEST, osg::StateAttribute::ON | osg::StateAttribute::OVERRIDE);
    stateSet->setMode(GL_LIGHTING,   osg::StateAttribute::OFF);

    stateSet->addUniform(_uClockTime->data());
    stateSet->addUniform(_uLightDirection->data());
    stateSet->addUniform(_uSelectionColor->data());

    //---------------------------------------------------------------------
    // preparing the scene
    osg::Camera* camera = _self.getCamera();

    if (camera)
    {
        camera->setName("__viewer_camera");
        camera->setCullMask(util::osg::CAMERA_FULL_MASK);
        camera->setRenderOrder(osg::Camera::POST_RENDER, 0);
        //#########################
        //camera->setGraphicsContext(_graphicsContext.get());
        //#########################
    }

    //------------
    // picking
    _rootDecorator = nullptr;
    if (_cfg.viewport_picking)
    {
        osg::ref_ptr<MRTNodeDecorator> mrt = MRTNodeDecorator::create(
            client,
            _clientRoot,
            _graphicsContext.get(),
            osg::Camera::COLOR_BUFFER0,
            0); // texture unit

        mrt->preRenderCamera()->addCullCallback(new KeepFBOsBoundCallback);
        mrt->preRenderCamera()->setPostDrawCallback(_pickingCallback);
        _rootDecorator = mrt;
    }

    //------------
    // manipulator
    _mouseCapturedCallback  = nullptr;
    _manipulator            = nullptr;
    if (_cfg.viewport_manipulator)
    {
        // create gizmo drawable and place it directly below the root of the complete scene
        _manipulator            = ManipulatorGeometry::create(client);
        _mouseCapturedCallback  = MouseCapturedByManipulatorCallback::create(*this);

        _manipulator->setName("__manipulator");
        _manipulator->onMouseCaptured(_mouseCapturedCallback);

        _root->addChild(_manipulator.get());
    }

    //#############
    _root->addChild(_rootDecorator.valid() ? _rootDecorator.get() : _clientRoot.get());
    //#############

    _self.setCameraManipulator(_camManip.get());

    _self.addEventHandler(_resizeHandler.get());
    //_self.addEventHandler(_programSwitch.get());

    //#############
    _self.realize();
    //#############

    // prior realization, it is unclear how to access the state of the device's camera graphics context.
    _self.getCamera()->getGraphicsContext()->getState()->setUseModelViewAndProjectionUniforms(true);
    _self.getCamera()->getGraphicsContext()->getState()->setUseVertexAttributeAliasing(true);

    // request the resizing of the window (must come after the device's realisation)
    _self.getEventQueue()->windowResize(
            graphicsWindow->getTraits()->x,
            graphicsWindow->getTraits()->y,
            graphicsWindow->getTraits()->width,
            graphicsWindow->getTraits()->height);
}

unsigned int
view::DevicePImpl::viewportCameraXform() const
{
    return _camManip
        ? _camManip->viewportCameraXform()
        : 0;
}

unsigned int
view::DevicePImpl::viewportCamera() const
{
    return _camManip
        ? _camManip->viewportCamera()
        : 0;
}

unsigned int
view::DevicePImpl::displayedCamera() const
{
    return _camManip
        ? _camManip->displayedCamera()
        : 0;
}

void
view::DevicePImpl::displayCamera(unsigned int node)
{
    if (_camManip)
        _camManip->displayCamera(node);
}

unsigned int
view::DevicePImpl::displayedFrameBuffer() const
{
    return _fbufferDisplay
        ? _fbufferDisplay->displayedFrameBuffer()
        : 0;
}

void
view::DevicePImpl::displayFrameBuffer(unsigned int node)
{
    if (_fbufferDisplay)
        _fbufferDisplay->displayFrameBuffer(node);
}

void
view::DevicePImpl::pick(int x, int y, bool accumulate) //<! updates scene node's selection
{
    if (_pickingCallback)
        _pickingCallback->setup(x, y, accumulate);
}

void
view::DevicePImpl::setViewportLightDirection(const osg::Vec3f& value)
{
    if (_uLightDirection && _uLightDirection->data())
        _uLightDirection->data()->set(value);
}

// virtual
void
view::DevicePImpl::handle(ClientBase&,
                          const xchg::ClientEvent&)
{ }

// virtual
void
view::DevicePImpl::handle(ClientBase&,
                          const xchg::SceneEvent&)
{ }

// virtual
void
view::DevicePImpl::handleSceneParmEvent(ClientBase&,
                                        const xchg::ParmEvent& evt)
{
    if (evt.comesFromAncestor() || evt.comesFromLink())
        return;

    if (evt.parm() == parm::initialization().id())
        handleInitializationChanged(evt.type(), evt.node());
    if (evt.emittedByNode())
    {
        if (evt.parm() == parm::selection().id())
            handleSelectionChanged(evt.type(), evt.node());
        else if (evt.parm() == parm::manipulatorFlags().id())
            handleManipulatorFlagsChanged(evt.type(), evt.node());
    }
}

void
view::DevicePImpl::handleInitializationChanged(xchg::ParmEvent::Type type,
                                               unsigned int          containerId)
{
    ClientBase::Ptr const& client = _client.lock();
    if (!client)
        return;

    //char init = xchg::INACTIVE;

    //client->getNodeParameter<char>(parm::initialization(), init, containerId);
    //if (init == xchg::ACTIVE ||
    //  init == xchg::INACTIVE)
    //  _cameraNeedsReset = false;
}

void
view::DevicePImpl::handleSelectionChanged(xchg::ParmEvent::Type type,
                                          unsigned int          containerId)
{
    ClientBase::Ptr const& client = _client.lock();
    if (!client ||
        containerId != client->sceneId())
        return;

    xchg::IdSet selection;

    client->getNodeParameter<xchg::IdSet>(parm::selection(), selection, client->sceneId());
    if (_manipulator)
        _manipulator->manipulate(selection);
}

void
view::DevicePImpl::handleManipulatorFlagsChanged(xchg::ParmEvent::Type type,
                                                unsigned int          containerId)
{
    ClientBase::Ptr const& client = _client.lock();
    if (!client ||
        containerId != client->sceneId())
        return;

    char mode = xchg::NO_MANIP;

    client->getNodeParameter<char>(parm::manipulatorFlags(), mode, client->sceneId());
    if (_manipulator)
        _manipulator->mode(static_cast<xchg::ManipulatorFlags>(mode));
}

void
view::DevicePImpl::handleMouseCapturedByManipulator(unsigned int contextId)
{
    if (_pickingCallback)
        _pickingCallback->enabled(contextId == -1);
}

void
view::DevicePImpl::handleResize(int x, int y, int width, int height)
{
    osg::Camera* camera = _self.getCamera();
    if (camera)
        camera->setViewport(0, 0, width, height);
}

// virtual
void
view::DevicePImpl::getUsage(osg::ApplicationUsage& usage) const
{
    dynamic_cast<const osgViewer::Viewer*>(&_self)
        ->getUsage(usage);

    const std::string& name = _self.getName();
    if (!name.empty())
        usage.setDescription(name);
}

void
view::DevicePImpl::updateClockTime(float value)
{
    if (_uClockTime && _uClockTime->data())
        _uClockTime->data()->set(value);
}
