// ========================================================================= //
// Copyright 2016-2018 SUPAMONKS_STUDIO                                      //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
//                                                                           //
// ========================================================================= //
#pragma once

#include <memory>

#include <smks/sys/Exception.hpp>
#include <smks/xchg/ManipulatorFlags.hpp>
#include <smks/view/AbstractDevice.hpp>

#include "smks/sys/configure_view_device.hpp"

namespace smks
{
    namespace xchg
    {
        class IdSet;
    }

    class ClientBase;

    namespace view
    {
        class DevicePImpl;
        class DeviceConfig;

        class Device:
            public AbstractDevice
        {
        public:
            ////////////////
            // struct Config
            ////////////////
            struct Config
            {
                std::string  windowTitle;
                unsigned int screenWidth;
                unsigned int screenHeight;
                float        screenRatio;
                bool         viewport_manipulator;
                bool         viewport_picking;
            public:
                Config();
                explicit Config(const char*);
            private:
                void setDefaults();
            };
            ////////////////
        private:
            typedef std::shared_ptr<ClientBase> ClientPtr;
        private:
            class EventHandler;
        private:
            DevicePImpl* _pImpl;

        public:
            static inline
            Device*
            create(const char*          jsonCfg,
                  ClientPtr const&      client,
                  osg::Group*           clientRoot,
                  osg::GraphicsContext* graphicsContext = nullptr)
            {
                Device* ptr = new Device();
                ptr->build(jsonCfg, client, clientRoot, graphicsContext);
                return ptr;
            }

            static inline
            void
            destroy(Device* ptr)
            {
                 if (ptr)
                 {
                    ptr->dispose();
                    delete ptr;
                 }
            }

        protected:
            Device();
        private:
            // non-copyable
            Device(const Device&);
            Device& operator=(const Device&);
        public:
            ~Device();
        protected:
            void
            build(const char* jsonCfg,
                  ClientPtr const&,
                  osg::Group* clientRoot,
                  osg::GraphicsContext*);

            void
            dispose();
        public:
            virtual
            void
            setUpThreading();

            virtual
            void
            setSceneData(osg::Node*)
            {
                throw sys::Exception(
                    "Users should not directly set viewport's scene data.",
                    "Viewport Scene Data Setter");
            }

            virtual
            void
            getUsage(osg::ApplicationUsage&) const;

        protected:
            virtual
            void
            frame(double);

        public:
            virtual
            std::weak_ptr<ClientBase> const&
            client() const;

            virtual
            const osg::Group*
            clientRoot() const;

            virtual
            osg::Group*
            clientRoot();

            virtual
            void
            setViewportLightDirection(const osg::Vec3f&);

            virtual
            unsigned int
            viewportCameraXform() const;

            virtual
            unsigned int
            viewportCamera() const;

            virtual
            unsigned int
            displayedCamera() const;

            virtual
            void
            displayCamera(unsigned int);

            virtual
            unsigned int
            displayedFrameBuffer() const;

            virtual
            void
            displayFrameBuffer(unsigned int);

            virtual
            void
            pick(int x, int y, bool accumulate);    //<! updates scene node's selection
        };
    }
}
