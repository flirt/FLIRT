// ========================================================================= //
// Copyright 2016-2018 SUPAMONKS_STUDIO                                      //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
//                                                                           //
// ========================================================================= //
#pragma once

#include <smks/util/osg/ScreenQuadCamera.hpp>
#include "RTTNodeDecorator.hpp"

namespace smks
{
    class ClientBase;
    namespace view
    {
        namespace gl
        {
            class UniformInput;
        }

        class RTTNodeDecorator::ScreenQuadCamera:
            public util::osg::ScreenQuadCamera
        {
        public:
            typedef osg::ref_ptr<ScreenQuadCamera> Ptr;
        private:
            typedef std::shared_ptr<ClientBase>         ClientPtr;
            typedef std::shared_ptr<gl::UniformInput>   UniformPtr;
        private:
            UniformPtr _uRcpTextureSize;

        public:
            static
            Ptr
            create(ClientPtr const&, unsigned int textureUnit = 0);

        private:
            ScreenQuadCamera(ClientPtr const&, unsigned int);

        protected:
            virtual
            void
            handleResize(int, int, int, int);
        };
    }
}
