// ========================================================================= //
// Copyright 2016-2018 SUPAMONKS_STUDIO                                      //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
//                                                                           //
// ========================================================================= //
#pragma once

#include <iostream>
#include <osg/Node>
#include <osg/NodeVisitor>
#include <osg/MatrixTransform>
#include <osg/Geode>

namespace smks { namespace view { namespace debug
{
    inline
    std::ostream&
    dump(std::ostream& out, const osg::Matrix& m)
    {
        out << "[ ";
        for (size_t r = 0; r < 4; ++r)
        {
            for (size_t c = 0; c < 4; ++c)
                out << m(r,c) << " ";
            out << (r < 3 ? ", " : "]");
        }
        return out;
    }

    inline
    std::ostream&
    dump(std::ostream& out, const osg::BoundingBox& b)
    {
        return out
            << "[ " << b.xMin() << " " << b.yMin() << " " << b.zMin()
            << " ] -> [ " << b.xMax() << " " << b.yMax() << " " << b.zMax() << " ]";
    }

    inline
    std::ostream&
    dump(std::ostream&  out,
         osg::Node*     n,
         bool           dumpXform   = false,
         bool           dumpBounds  = false)
    {
        ////////////////
        // class Visitor
        ////////////////
        class Visitor: public osg::NodeVisitor
        {
        private:
            std::ostream&   _out;
            const bool      _dumpXform;
            const bool      _dumpBounds;
            size_t          _depth;
        public:
            Visitor(std::ostream& out, bool dumpXform, bool dumpBounds):
                osg::NodeVisitor(NodeVisitor::TRAVERSE_ALL_CHILDREN),
                _out            (out),
                _dumpXform      (dumpXform),
                _dumpBounds     (dumpBounds),
                _depth          (0)
            { }

            inline
            void
            apply(osg::Node& n)
            {
                prefix(_out, '-') << " '" << n.getName() << "'\n";
                ++_depth;
                traverse(n);
                --_depth;
            }

            inline
            void
            apply(osg::MatrixTransform& n)
            {
                if (!_dumpXform)
                    apply(static_cast<osg::Node&>(n));

                prefix(_out, '-') << " '" << n.getName() << "'\n";
                dump(prefix(_out, ' ') << " ", n.getMatrix()) << "\n";
                ++_depth;
                traverse(n);
                --_depth;
            }

            inline
            void
            apply(osg::Geode& n)
            {
                if (!_dumpXform)
                    apply(static_cast<osg::Node&>(n));

                prefix(_out, '-') << " '" << n.getName() << "'\n";
                dump(prefix(_out, ' ') << " ", n.getBoundingBox()) << "\n";
                ++_depth;
                traverse(n);
                --_depth;
            }

        private:
            inline
            std::ostream&
            prefix(std::ostream& out, const char c) const
            {
                for (size_t i = 0; i < _depth; ++i) out << c;
                return out;
            }
        };
        ////////////////

        if (n)
        {
            Visitor visitor(out, dumpXform, dumpBounds);
            n->accept(visitor);
        }
        return out;
    }

}
} }
