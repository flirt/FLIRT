// ========================================================================= //
// Copyright 2016-2018 SUPAMONKS_STUDIO                                      //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
//                                                                           //
// ========================================================================= //
#pragma once

#include <osgGA/GUIEventHandler>

#include "DevicePImpl.hpp"
#include <smks/util/osg/key.hpp>

namespace smks { namespace view
{
    class DevicePImpl::GLProgramSwitch:
        public osgGA::GUIEventHandler
    {
    public:
        typedef osg::ref_ptr<GLProgramSwitch>   Ptr;
    private:
        typedef osg::observer_ptr<osg::Program> ProgramWPtr;
    private:
        enum {INVALID_IDX=-1};
    private:
        const util::osg::KeyControl _key;

        DevicePImpl&                _pImpl;
        std::vector<ProgramWPtr>    _programs;
        size_t                      _currentProgram;

    public:
        static inline
        Ptr
        create(DevicePImpl&                 pImpl,
               const util::osg::KeyControl& key = util::osg::KeyControl(osgGA::GUIEventAdapter::KEY_G))
        {
            Ptr ptr(new GLProgramSwitch(
                pImpl,
                key));
            return ptr;
        }

        static inline
        void
        destroy(Ptr& ptr)
        {
            if (ptr)
                ptr->dispose();
            ptr = nullptr;
        }

    protected:
        GLProgramSwitch(DevicePImpl&                    pImpl,
                        const util::osg::KeyControl&    key):
            GUIEventHandler (),
            _key            (key),
            _pImpl          (pImpl),
            _programs       (),
            _currentProgram (INVALID_IDX)
        {
            _programs.reserve(16);
        }

        inline
        void
        dispose()
        {
            _programs.clear();
            _currentProgram = INVALID_IDX;
        }
    public:
        virtual inline
        bool
        handle(const osgGA::GUIEventAdapter& ea, osgGA::GUIActionAdapter& aa)
        {
            if (ea.getHandled())
                return false;

            if (_currentProgram == INVALID_IDX ||
                _programs.empty())
                return false;

            _currentProgram = (_currentProgram + 1) % _programs.size();
            if (!_programs[_currentProgram].valid())
                return false;

            osg::Group* osgRoot = _pImpl.clientRoot();
            if (!osgRoot)
                return false;

            osgRoot->getOrCreateStateSet()->setAttributeAndModes(
                _programs[_currentProgram].get(),
                osg::StateAttribute::ON);

            return true;
        }

        inline
        void
        add(osg::Program* program)
        {
            if (program)
                _programs.push_back(program);
        }

        virtual inline
        void
        getUsage(osg::ApplicationUsage& usage) const
        {
            util::osg::addKeyboardBinding(usage, util::osg::BINDINGS_VIEWPORT, 1000, "Switch GL display", _key);
        }
    };
}
}
