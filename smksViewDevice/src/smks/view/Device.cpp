// ========================================================================= //
// Copyright 2016-2018 SUPAMONKS_STUDIO                                      //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
//                                                                           //
// ========================================================================= //
#include <json/json.h>
#include <osgGA/EventHandler>

#include <smks/sys/Log.hpp>
#include <smks/ClientBase.hpp>

#include "Device.hpp"
#include "DevicePImpl.hpp"
#include "DevicePImpl-CameraManager.hpp"
#include "DevicePImpl-CameraManipulator.hpp"
#include "DevicePImpl-ClockTimeUpdate.hpp"
#include "DevicePImpl-FrameBufferDisplayOperation.hpp"
#include "DevicePImpl-GLProgramSwitch.hpp"
#include "DevicePImpl-PickingCallback.hpp"
#include "DevicePImpl-ResizeHandler.hpp"
#include "DevicePImpl-SwapCallback.hpp"
#include "ManipulatorGeometry.hpp"

using namespace smks;

view::Device::Device():
    AbstractDevice(),
    _pImpl        (new DevicePImpl(*this))
{ }

view::Device::~Device()
{
    delete _pImpl;
}

// virtual
void
view::Device::build(const char*             jsonCfg,
                    ClientBase::Ptr const&  client,
                    osg::Group*             clientRoot,
                    osg::GraphicsContext*   graphicsContext)
{
    _pImpl->build(jsonCfg, client, clientRoot, graphicsContext);
}

// virtual
void
view::Device::dispose()
{
    _pImpl->dispose();
}

// virtual
void
view::Device::setUpThreading()
{
    if (_threadingModel == SingleThreaded)
    {
        if (_threadsRunning)
            stopThreading();
    }
    else
    {
        if (!_threadsRunning)
            startThreading();
    }
}

// virtual
void
view::Device::getUsage(osg::ApplicationUsage& usage) const
{
    Viewer::getUsage(usage);
    if (!getName().empty())
        usage.setDescription(getName());
}

// virtual
void
view::Device::frame(double simulationTime)
{
    osgViewer::GraphicsWindow* gw = _camera && _camera->getGraphicsContext() && _camera->getGraphicsContext()->valid()
        ?  dynamic_cast<osgViewer::GraphicsWindow*>(_camera->getGraphicsContext())
        : nullptr;

    // catch the event the window is about to close in order to
    // prevent invalidation of the graphic context
    if (gw)
    {
        osgGA::EventQueue::Events evts;
        bool windowAboutToClose = false;

        gw->getEventQueue()->copyEvents(evts);
        for (osgGA::EventQueue::Events::iterator itr = evts.begin(); itr != evts.end(); ++itr)
            if (itr->get() &&
                itr->get()->asGUIEventAdapter() &&
                itr->get()->asGUIEventAdapter()->getEventType() == osgGA::GUIEventAdapter::CLOSE_WINDOW)
                windowAboutToClose = true;
        if (windowAboutToClose)
        {
            setDone(true);  // the device will cease to function and ignore any event
            gw->getEventQueue()->clear();   // otherwise the window will never reopen
        }
    }
    //#######################################
    osgViewer::Viewer::frame(simulationTime);
    //#######################################
    //// reset the graphics window's state in order to allow external OpenGL calls
    //if (gw && gw->getState())
    //  gw->getState()->reset();
}

// virtual
ClientBase::WPtr const&
view::Device::client() const
{
    return _pImpl->client();
}

// virtual
const osg::Group*
view::Device::clientRoot() const
{
    return _pImpl->clientRoot();
}

// virtual
osg::Group*
view::Device::clientRoot()
{
    return _pImpl->clientRoot();
}

// virtual
void
view::Device::setViewportLightDirection(const osg::Vec3f& value)
{
    _pImpl->setViewportLightDirection(value);
}

// virtual
unsigned int
view::Device::viewportCameraXform() const
{
    return _pImpl->viewportCameraXform();
}

// virtual
unsigned int
view::Device::viewportCamera() const
{
    return _pImpl->viewportCamera();
}

// virtual
unsigned int
view::Device::displayedCamera() const
{
    return _pImpl->displayedCamera();
}

// virtual
void
view::Device::displayCamera(unsigned int node)
{
    if (_pImpl)
        _pImpl->displayCamera(node);
}

// virtual
unsigned int
view::Device::displayedFrameBuffer() const
{
    return _pImpl->displayedFrameBuffer();
}

// virtual
void
view::Device::displayFrameBuffer(unsigned int node)
{
    if (_pImpl)
        _pImpl->displayFrameBuffer(node);
}

// virtual
void
view::Device::pick(int x, int y, bool accumulate)   //<! updates scene node's selection
{
    _pImpl->pick(x, y, accumulate);
}


