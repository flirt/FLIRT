// ========================================================================= //
// Copyright 2016-2018 SUPAMONKS_STUDIO                                      //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
//                                                                           //
// ========================================================================= //
#pragma once

#include <osg/NodeCallback>

#include "DevicePImpl.hpp"

namespace smks { namespace view
{
    // RenderStage unbinds FBOs before executing post-draw callbacks.
    // The only way I know of to access the RenderStage (to disable this
    // unbinding) is with a cull callback.

    class DevicePImpl::KeepFBOsBoundCallback:
        public osg::NodeCallback
    {
    public:
        virtual inline
        void
        operator()(osg::Node*           node,
                   osg::NodeVisitor*    nv)
        {
            if (nv->getVisitorType() == osg::NodeVisitor::CULL_VISITOR)
            {
                // Get the current RenderStage and prevent it from unbinding the FBOs
                // just before our post-draw MSMRTCallback is executed. We need them
                // bound in our callback so we can execute another glBlitFramebuffer.
                // After the blit, MSMRTCallback unbinds the FBOs.
                osgUtil::CullVisitor*   cv  = dynamic_cast<osgUtil::CullVisitor*>(nv);
                // Don't use getRenderStage(). It returns the _root_ RenderStage.
                //osgUtil::RenderStage* rs = cv->getRenderStage();
                osgUtil::RenderStage*   rs  = cv->getCurrentRenderBin()->getStage();

                rs->setDisableFboAfterRender(false);
            }
            traverse(node, nv);
        }
    };
};
}
