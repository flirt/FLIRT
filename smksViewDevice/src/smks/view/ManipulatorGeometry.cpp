// ========================================================================= //
// Copyright 2016-2018 SUPAMONKS_STUDIO                                      //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
//                                                                           //
// ========================================================================= //
#include <osg/Callback>
#include <osg/Depth>
#include <osg/MatrixTransform>
#include <osgGA/EventVisitor>

#include <std/to_string_xchg.hpp>
#include <smks/sys/Log.hpp>
#include <smks/sys/ResourceDatabase.hpp>
#include <smks/math/matrixAlgo.hpp>
#include <smks/xchg/IdSet.hpp>
#include <smks/xchg/MouseFlags.hpp>
#include <smks/ClientBase.hpp>
#include <smks/parm/BuiltIn.hpp>
#include <smks/parm/BuiltIns.hpp>
#include <smks/util/osg/NodeMask.hpp>
#include <smks/util/osg/drawable.hpp>
#include <smks/view/gl/ContextExtensions.hpp>
#include <smks/view/gl/VertexAttrib.hpp>
#include <smks/view/gl/VertexAttribs.hpp>
#include <smks/view/gl/ProgramTemplate.hpp>
#include <smks/view/gl/ProgramTemplates.hpp>

#include "Device.hpp"
#include "ManipulatorGeometry.hpp"
#include "gizmo/IGizmo.hpp"
#include "gizmo/RenderBuffers.hpp"

namespace smks { namespace view
{
    /////////////////////////////////////
    // struct ManipulatorGeometry::EventHandler
    /////////////////////////////////////
    struct ManipulatorGeometry::EventHandler:
        public osg::Drawable::EventCallback
    {
        virtual
        void
        event(osg::NodeVisitor*,
              osg::Drawable*);
    };

    /////////////////////////////////////
    // class ManipulatorGeometry::UpdateCallback
    /////////////////////////////////////
    class ManipulatorGeometry::UpdateCallback:
        public osg::Callback
    {
    public:
        virtual inline
        bool
        run(osg::Object* object,
            osg::Object* data)
        {
            ManipulatorGeometry* geo = reinterpret_cast<ManipulatorGeometry*>(
                object);
            if (geo)
            {
                geo->loadProgram();
                geo->applyTransform();
            }
            return traverse(object, data);
        }
    };
}
}

using namespace smks;

/////////////////////////////////////
// struct ManipulatorGeometry::EventHandler
/////////////////////////////////////
// virtual
void
view::ManipulatorGeometry::EventHandler::event(osg::NodeVisitor*    nv,
                                               osg::Drawable*       drawable)
{
    osgGA::EventVisitor*    ev  = static_cast<osgGA::EventVisitor*>(nv);
    ManipulatorGeometry*    geo = dynamic_cast<ManipulatorGeometry*>(drawable);
    if (!ev ||
        !geo)
        return;

    const osgGA::EventQueue::Events& events = ev->getEvents();
    for (osgGA::EventQueue::Events::const_iterator itr=events.begin();
         itr!=events.end();
         ++itr)
    {
        const osgGA::GUIEventAdapter* ea = (*itr)->asGUIEventAdapter();
        if (ea)
            geo->process(*ea);
    }
}
/////////////////////////////////////

// static
view::ManipulatorGeometry::Ptr
view::ManipulatorGeometry::create(ClientBase::Ptr const& client)
{
    Ptr ptr(new ManipulatorGeometry(client));
    return ptr;
}

// static
void
view::ManipulatorGeometry::destroy(Ptr& ptr)
{
    if (ptr)
        ptr->dispose();
    ptr = nullptr;
}

view::ManipulatorGeometry::ManipulatorGeometry(ClientBase::Ptr const& client):
    osg::Geometry       (),
    _client             (client),
    _mode               (xchg::NO_MANIP),
    _gizmos             (),
    //------------------
    _lastId             (0),
    //------------------
    _editXfm            (math::Affine3f::Identity()),
    _rOrigEditXfm       (math::Affine3f::Identity()),
    _origXfmData        (),
    //------------------
    _capturingContext   (NO_CONTEXT),
    _onMouseCaptured    (nullptr)
{
    setName("__manipulator.geometry");
    util::osg::setupRender(*this);

    osg::StateSet* stateSet = getOrCreateStateSet();
    assert(stateSet);
    stateSet->setDataVariance(osg::Object::DYNAMIC);    // prevents update phase to commence before all draw phases finish
    addUpdateCallback(new UpdateCallback());            // assign program at update phase

    setEventCallback(new EventHandler);

    osg::ref_ptr<osg::Depth> depth = new osg::Depth(osg::Depth::ALWAYS);
    depth->setWriteMask(false);
    stateSet->setAttribute(depth.get());

    stateSet->setMode(GL_BLEND, osg::StateAttribute::ON);
    stateSet->setMode(GL_LIGHTING, osg::StateAttribute::OFF);
    stateSet->setRenderingHint(osg::StateSet::TRANSPARENT_BIN);
    stateSet->setRenderBinDetails(1, "DepthSortedBin");
    setNodeMask(util::osg::INVISIBLE_TO_ALL);
}

void
view::ManipulatorGeometry::dispose()
{
    ClientBase::Ptr const&  client      = _client.lock();
    sys::ResourceDatabase*  resources   = client ? client->getResourceDatabase() : nullptr;
    gl::ContextExtensions*  extensions  = resources ? gl::getContextExtensions(*resources) : nullptr;

    destroyGizmos();

    if (extensions)
        for (size_t contextId = 0; contextId < _gizmos.size(); ++contextId)
        {
            PerContextManipData& gizmoData = _gizmos.get(contextId);
            if (gizmoData.areGLObjectsOk())
                extensions->getOrCreateGLExtension(contextId)->glDeleteBuffers(3, &gizmoData.buffers());
        }
}

void
view::ManipulatorGeometry::destroyGizmos()
{
    for (size_t i = 0; i < _gizmos.size(); ++i)
        _gizmos.get(i).destroyGizmo();
}

void
view::ManipulatorGeometry::manipulate(const xchg::IdSet& ids)
{
    // reset gizmo's transform
    _editXfm        .setIdentity();
    _rOrigEditXfm   .setIdentity();

    if (ids.empty())
        _origXfmData.clear();

    ClientBase::Ptr const& client = _client.lock();
    if (!client)
        return;

    // keep only xform nodes whose transform can be written by users
    xchg::IdSet newIds;

    filterManipulable(ids, newIds);
    _lastId = getLastId(newIds);

    // remove obsolete transform data entries
    for (OrigTransformDataMap::iterator it = _origXfmData.begin(); it != _origXfmData.end(); )
        if (newIds.find(it->first) == newIds.end())
            it = _origXfmData.erase(it);
        else
            ++it;
    // add empty transform data entries
    for (xchg::IdSet::const_iterator id = newIds.begin(); id != newIds.end(); ++id)
        _origXfmData[*id] = OrigTransformData();

    startManipulation();
    setNodeMask(newIds.empty()
        ? util::osg::INVISIBLE_TO_ALL
        : util::osg::VISIBLE_TO_ALL);
}

void
view::ManipulatorGeometry::getOrigEditXfm(math::Affine3f& editXfm) const
{
    editXfm.setIdentity();

    ClientBase::Ptr const& client = _client.lock();
    if (!client ||
        _lastId == 0)
        return;

    client->getNodeParameter<math::Affine3f>(parm::worldTransform(), editXfm, _lastId);

    if (_mode & xchg::MANIP_OBJECT_MODE)
        return;

    // keep only translation when in world mode
    math::Vector4f position = math::Vector4f(
        editXfm.translation()[0],
        editXfm.translation()[1],
        editXfm.translation()[2],
        1.0f
    );
    editXfm.setIdentity();
    editXfm.translation()[0] = position[0];
    editXfm.translation()[1] = position[1];
    editXfm.translation()[2] = position[2];
}

void
view::ManipulatorGeometry::startManipulation()
{
    // reset gizmo's transform
    _editXfm        .setIdentity();
    _rOrigEditXfm   .setIdentity();

    ClientBase::Ptr const& client = _client.lock();
    if (!client ||
        _lastId == 0)
        return;

    // store all selected nodes' initial transforms
    for (OrigTransformDataMap::iterator it = _origXfmData.begin(); it != _origXfmData.end(); ++it)
    {
        client->getNodeParameter<math::Affine3f>(parm::transform(),         it->second.localXfm, it->first);
        client->getNodeParameter<math::Affine3f>(parm::worldTransform(),    it->second.worldXfm, it->first);

        unsigned int parentId = client->getNodeParent(it->first);

        if (parentId > 0)
        {
            math::Affine3f xfm;

            client->getNodeParameter<math::Affine3f>(parm::worldTransform(), xfm, parentId);
            it->second.rParentWorldXfm = xfm.inverse();
        }
        else
            it->second.rParentWorldXfm.setIdentity();

    }

#if defined (FLIRT_LOG_ENABLED)
    {
        LOG(DEBUG)
            << _origXfmData.size() << " initial transforms stored by manipulator\t"
            << "(last ID = " << _lastId << " '" << client->getNodeName(_lastId) << "')";
        for (OrigTransformDataMap::iterator it = _origXfmData.begin(); it != _origXfmData.end(); ++it)
            LOG(DEBUG)
                << "- orig_data[" << it->first << "]:"
                << "\n\t- name = '" << client->getNodeName(it->first) << "'"
                << "\n\t- local xfm = \n" << it->second.localXfm.matrix()
                << "\n\t- inverse(parent world xfm) = \n" << it->second.rParentWorldXfm.matrix();
    }
#endif // defined (FLIRT_LOG_ENABLED)

    // position the manipulator's gizmo following the last selected node
    getOrigEditXfm(_editXfm);
    _rOrigEditXfm = _editXfm.inverse();
    FLIRT_LOG(LOG(DEBUG) << "manipulator's offset transform =\n" << _editXfm.matrix();)
}


unsigned int
view::ManipulatorGeometry::getLastId(const xchg::IdSet& nextIds) const
{
    if (nextIds.empty())
        return 0;

    xchg::IdSet prevIds, addedIds, removedIds;

    for (OrigTransformDataMap::const_iterator it = _origXfmData.begin();
        it != _origXfmData.end();
        ++it)
        prevIds.insert(it->first);

    xchg::IdSet::diff(prevIds, nextIds, addedIds, removedIds);

    if (!addedIds.empty())
        return *addedIds.begin();
    else if (nextIds.find(_lastId) != nextIds.end())
        return _lastId;
    else
        return *nextIds.begin();
}

void
view::ManipulatorGeometry::filterManipulable(const xchg::IdSet& ids,
                                             xchg::IdSet&       ret) const
{
    ret.clear();

    ClientBase::Ptr const& client = _client.lock();
    if (!client)
        return;

    // keep only xform nodes whose transform can be written by users
    xchg::IdSet xforms;

    for (xchg::IdSet::const_iterator id = ids.begin(); id != ids.end(); ++id)
    {
        // try to get the closest editable xform alongside the hierarchy
        unsigned int node = *id;
        while (node)
        {
            if (client->getNodeClass(node) == xchg::XFORM &&
                client->isNodeParameterWritable(parm::transform().id(), node))
                break;
            node = client->getNodeParent(node);
        }
        if (node)
            xforms.insert(node);
    }

    // remove xforms from the manipulated node list whenever a xform is a
    // descendant of a xform about to be manipulated.
    for (xchg::IdSet::const_iterator id = xforms.begin(); id != xforms.end(); ++id)
    {
        bool            removeXform = false;
        unsigned int    parentId    = client->getNodeParent(*id);
        do
        {
            if (parentId == 0)
                break;
            removeXform = (xforms.find(parentId) != xforms.end());
            if (removeXform)
                break;
            parentId = client->getNodeParent(parentId);
        }
        while (parentId > 0);

        if (removeXform == false)
            ret.insert(*id);
    }
}

void
view::ManipulatorGeometry::applyTransform()
{
    if (_mode == xchg::NO_MANIP)
        return;

    ClientBase::Ptr const& client = _client.lock();
    if (!client)
        return;

    const math::Affine3f& deltaXfm = _rOrigEditXfm * _editXfm;  // incremental transform step
    FLIRT_LOG(LOG(DEBUG) << "delta xfm = \n" << deltaXfm.matrix();)

    if (_mode & xchg::MANIP_OBJECT_MODE)
    {
        // objects are transformed in local space
        for (OrigTransformDataMap::iterator it = _origXfmData.begin();
            it != _origXfmData.end();
            ++it)
        {
            const math::Affine3f& newXfm = it->second.localXfm * deltaXfm;

            client->setNodeParameter<math::Affine3f>(parm::transform(), newXfm, it->first);
        }
    }
    else
    {
        // objects are transformed in world space
        for (OrigTransformDataMap::iterator it = _origXfmData.begin();
            it != _origXfmData.end();
            ++it)
        {
            math::Affine3f worldTranslate   = math::Affine3f::Identity();
            math::Affine3f rWorldTranslate  = math::Affine3f::Identity();

            worldTranslate.translation()    = it->second.worldXfm.translation();
            rWorldTranslate.translation()   = -it->second.worldXfm.translation();

            const math::Affine3f& newXfm    =
                it->second.rParentWorldXfm * worldTranslate * deltaXfm * rWorldTranslate * it->second.worldXfm;

            client->setNodeParameter<math::Affine3f>(parm::transform(), newXfm, it->first);
        }
    }
}

void
view::ManipulatorGeometry::onMouseCaptured(MouseCapturedCallback::Ptr const& callback)
{
    _onMouseCaptured = callback;
}

void
view::ManipulatorGeometry::mode(xchg::ManipulatorFlags value)
{
    FLIRT_LOG(LOG(DEBUG) << "manipulator mode changing for '" << std::to_string(value) << "'.";)

    if (_mode == value)
        return;

    destroyGizmos();
    startManipulation();
    _mode = value;
}

void
view::ManipulatorGeometry::process(const osgGA::GUIEventAdapter& ea)
{
    unsigned int contextId = ea.getGraphicsContext() && ea.getGraphicsContext()->getState()
        ? ea.getGraphicsContext()->getState()->getContextID()
        : 0;

    if (_capturingContext != -1 &&
        _capturingContext != contextId)
        return;

    PerContextManipData& gizmoData = _gizmos.get(contextId);
    if (gizmoData.screenWidth() == 0 ||
        gizmoData.screenHeight() == 0)
        gizmoData.setScreenSize(ea.getWindowWidth(), ea.getWindowHeight());

    int x = static_cast<float>(ea.getX());
    int y = static_cast<float>(ea.getY());
    if (ea.getMouseYOrientation() == osgGA::GUIEventAdapter::Y_INCREASING_UPWARDS)
        y = ea.getWindowHeight() - y;

    ClientBase::Ptr const& client = _client.lock();

    switch (ea.getEventType())
    {
    case osgGA::GUIEventAdapter::PUSH:
        if (gizmoData.gizmo() &&
            gizmoData.gizmo()->OnMouseDown(x, y))
        {
            if (_capturingContext != contextId)
            {
                xchg::MouseFlags flags = contextId != NO_CONTEXT
                    ? xchg::MOUSE_CAPTURED
                    : xchg::NO_MOUSE_FLAGS;
                if (client)
                    client->setNodeParameter<char>(parm::mouseFlags(), flags, client->sceneId());

                if (_onMouseCaptured)
                    _onMouseCaptured->operator()(contextId);
                _capturingContext = contextId;
            }
        }
        break;
    case osgGA::GUIEventAdapter::RELEASE:
        if (gizmoData.gizmo())
            gizmoData.gizmo()->OnMouseUp(x, y);
        if (_capturingContext != NO_CONTEXT)
        {
            if (client)
                client->setNodeParameter<char>(parm::mouseFlags(), xchg::NO_MOUSE_FLAGS, client->sceneId());
            if (_onMouseCaptured)
                _onMouseCaptured->operator()(NO_CONTEXT);

            _capturingContext = NO_CONTEXT;
        }
        break;
    case osgGA::GUIEventAdapter::MOVE:
    case osgGA::GUIEventAdapter::DRAG:
        if (gizmoData.gizmo())
            gizmoData.gizmo()->OnMouseMove(x, y);
        break;
    case osgGA::GUIEventAdapter::RESIZE:
        gizmoData.setScreenSize(ea.getWindowWidth(), ea.getWindowHeight());
        break;
    default: break;
    }
}

// virtual
void
view::ManipulatorGeometry::drawImplementation(osg::RenderInfo& renderInfo) const
{
    if (!isProgramUpToDate())
        return;
    if (_mode == xchg::NO_MANIP)
        return;

    ClientBase::Ptr const&          client      = _client.lock();
    const sys::ResourceDatabase*    resources   = client ? client->getResourceDatabase() : nullptr;
    const gl::ContextExtensions*    extensions  = resources ? gl::getContextExtensions(*resources) : nullptr;
    if (!extensions)
        return;

    assert(renderInfo.getState());
    const osg::State&           state       = *renderInfo.getState();
    const unsigned int          contextId   = state.getContextID();
    PerContextManipData&        gizmoData   = _gizmos.get(contextId);
    const osg::GLExtensions*    ext         = const_cast<gl::ContextExtensions*>(extensions)
        ->getOrCreateGLExtension(contextId);
    assert(ext);

    if (gizmoData.gizmo() == nullptr)
    {
        gizmoData.setGizmoMode(_mode);
        if (gizmoData.gizmo())
        {
            float* editXfm = const_cast<float*>(_editXfm.data());
            gizmoData.gizmo()->SetEditMatrix(editXfm);
        }
    }
    if (gizmoData.gizmo() == nullptr)
        return;

    gizmo::RenderBuffers buffers;

    try
    {
        gizmoData.gizmo()->SetCameraMatrix(
            osg::Matrixf(state.getModelViewMatrix()).ptr(),
            osg::Matrixf(state.getProjectionMatrix()).ptr());
        gizmoData.gizmo()->GetRenderBuffers(buffers);
    }
    catch(const std::exception& e)
    {
        std::cerr << "Failed to compute manipulator's buffers.\n" << e.what() << std::endl;
    }

    if (buffers.vertices.empty() ||
        (buffers.triangles.empty() && buffers.lines.empty()))
        return;

    const bool generateBuffer = !gizmoData.areGLObjectsOk();
    if (generateBuffer)
        ext->glGenBuffers(3, &gizmoData.buffers());

    ext->glBindBuffer(GL_ARRAY_BUFFER, gizmoData.vertexBuffer());

    if (generateBuffer)
    {
        gl::iPosition().glVertexAttribPointer(
            ext,
            sizeof(gizmo::Vertex),
            (GLvoid*)0);
        gl::iColor().glVertexAttribPointer(
            ext,
            sizeof(gizmo::Vertex),
            (GLvoid*)(sizeof(float) << 2));
    }

    ext->glBufferData(
        GL_ARRAY_BUFFER,
        buffers.vertices.size() * sizeof(gizmo::Vertex),
        &buffers.vertices.front(),
        GL_DYNAMIC_DRAW);

    gl::iPosition().glEnableVertexAttribArray(ext);
    gl::iColor   ().glEnableVertexAttribArray(ext);

    if (!buffers.triangles.empty())
    {
        ext->glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, gizmoData.triangleBuffer());
        ext->glBufferData(
            GL_ELEMENT_ARRAY_BUFFER,
            buffers.triangles.size() * sizeof(unsigned int),
            &buffers.triangles.front(),
            GL_DYNAMIC_DRAW);
        //-------------------------------------------------------------------------
        glDrawElements(GL_TRIANGLES, buffers.triangles.size(), GL_UNSIGNED_INT, 0);
        //-------------------------------------------------------------------------
    }
    if (!buffers.lines.empty())
    {
        ext->glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, gizmoData.lineBuffer());
        ext->glBufferData(
            GL_ELEMENT_ARRAY_BUFFER,
            buffers.lines.size() * sizeof(unsigned int),
            &buffers.lines.front(),
            GL_DYNAMIC_DRAW);
        //----------------------------------------------------------------
        glDrawElements(GL_LINES, buffers.lines.size(), GL_UNSIGNED_INT, 0);
        //----------------------------------------------------------------
    }

    gl::iPosition().glDisableVertexAttribArray(ext);
    gl::iColor   ().glDisableVertexAttribArray(ext);
    ext->glBindBuffer(GL_ARRAY_BUFFER,          0);
    ext->glBindBuffer(GL_ELEMENT_ARRAY_BUFFER,  0);
}

bool
view::ManipulatorGeometry::isProgramUpToDate() const
{
    return getStateSet() &&
        getStateSet()->getAttribute(::osg::StateAttribute::PROGRAM);
}

void
view::ManipulatorGeometry::loadProgram()
{
    if (isProgramUpToDate())
        return;

    ClientBase::Ptr const&              client      = _client.lock();
    sys::ResourceDatabase::Ptr const&   resources   = client ? client->getOrCreateResourceDatabase() : nullptr;
    if (!resources)
        return;

    view::gl::getOrCreateContextExtensions(*resources);
    osg::ref_ptr<osg::Program> const& program = gl::getOrCreateProgram(
        gl::get_ManipulatorProgram(), resources);
    if (!program)
        return;

    assert(getStateSet());
    getStateSet()->setAttribute(program.get(), ::osg::StateAttribute::OVERRIDE);
}

/////////////////////////////////////////////////
// class ManipulatorGeometry::PerContextManipData
/////////////////////////////////////////////////
view::ManipulatorGeometry::PerContextManipData::PerContextManipData():
    _gizmo          (nullptr),
    _screenWidth    (0),
    _screenHeight   (0)
{
    memset(_vbo, 0, sizeof(GLuint) * 3);
}

size_t
view::ManipulatorGeometry::PerContextManipData::screenWidth() const
{
    return _screenWidth;
}
size_t
view::ManipulatorGeometry::PerContextManipData::screenHeight() const
{
    return _screenHeight;
}

view::gizmo::IGizmo*
view::ManipulatorGeometry::PerContextManipData::gizmo()
{
    return _gizmo;
}

const view::gizmo::IGizmo*
view::ManipulatorGeometry::PerContextManipData::gizmo() const
{
    return _gizmo;
}

void
view::ManipulatorGeometry::PerContextManipData::destroyGizmo()
{
    if (_gizmo)
        delete _gizmo;
    _gizmo = nullptr;
}

void
view::ManipulatorGeometry::PerContextManipData::setGizmoMode(xchg::ManipulatorFlags mode)
{
    destroyGizmo();

    if (mode & xchg::MANIP_TRANSLATE)
        _gizmo = gizmo::CreateMoveGizmo();
    else if (mode & xchg::MANIP_ROTATE)
        _gizmo = gizmo::CreateRotateGizmo();
    else if (mode & xchg::MANIP_SCALE)
        _gizmo = gizmo::CreateScaleGizmo();

    if (_gizmo)
    {
        const gizmo::Location location = mode & xchg::MANIP_OBJECT_MODE
            ? gizmo::LOCATE_LOCAL
            : gizmo::LOCATE_WORLD;

        _gizmo->SetLocation(location);
        _gizmo->SetScreenDimension(static_cast<int>(_screenWidth), static_cast<int>(_screenHeight));
    }
}

void
view::ManipulatorGeometry::PerContextManipData::setScreenSize(size_t w, size_t h)
{
    _screenWidth = w;
    _screenHeight = h;
    if (_gizmo)
        _gizmo->SetScreenDimension(static_cast<int>(w), static_cast<int>(h));
}

bool
view::ManipulatorGeometry::PerContextManipData::areGLObjectsOk() const
{
    return _vbo[0] != 0;
}

GLuint&
view::ManipulatorGeometry::PerContextManipData::buffers()
{
    return _vbo[0];
}

GLuint
view::ManipulatorGeometry::PerContextManipData::vertexBuffer() const
{
    return _vbo[0];
}

GLuint
view::ManipulatorGeometry::PerContextManipData::triangleBuffer() const
{
    return _vbo[1];
}

GLuint
view::ManipulatorGeometry::PerContextManipData::lineBuffer() const
{
    return _vbo[2];
}
