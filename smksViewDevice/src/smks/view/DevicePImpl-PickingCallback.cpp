// ========================================================================= //
// Copyright 2016-2018 SUPAMONKS_STUDIO                                      //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
//                                                                           //
// ========================================================================= //
#include <cassert>

#include <osg/Texture2D>
#include <osgGA/GUIEventAdapter>

#include <smks/sys/Log.hpp>
#include <smks/xchg/IdSet.hpp>
#include <smks/parm/BuiltIn.hpp>
#include <smks/parm/BuiltIns.hpp>
#include <smks/parm/BuiltInMap.hpp>
#include <smks/ClientBase.hpp>
#include <smks/view/gl/ContextExtensions.hpp>

#include "MRTNodeDecorator.hpp"
#include "DevicePImpl-CameraManager.hpp"
#include "DevicePImpl-CameraManipulator.hpp"
#include "DevicePImpl-ClockTimeUpdate.hpp"
#include "DevicePImpl-FrameBufferDisplayOperation.hpp"
#include "DevicePImpl-GLProgramSwitch.hpp"
#include "DevicePImpl-PickingCallback.hpp"
#include "DevicePImpl-ResizeHandler.hpp"
#include "DevicePImpl-SwapCallback.hpp"
#include "ManipulatorGeometry.hpp"
#include <smks/util/color/rgba8.hpp>

using namespace smks;

// explicit
view::DevicePImpl::PickingCallback::PickingCallback(DevicePImpl& pImpl):
    osg::Camera::DrawCallback(),
    _pImpl      (pImpl),
    _accumulate (false),
    _position   (INVALID_COORD, INVALID_COORD),
    _enabled    (true)
{ }

// virtual
void
view::DevicePImpl::PickingCallback::operator()(osg::RenderInfo& renderInfo) const
{
    assert(renderInfo.getState());

    ClientBase::Ptr const&          client      = _pImpl.client().lock();
    const sys::ResourceDatabase*    resources   = client ? client->getResourceDatabase() : nullptr;
    const gl::ContextExtensions*    extensions  = resources ? gl::getContextExtensions(*resources) : nullptr;
    const osg::GLExtensions*        ext         = extensions
        ? const_cast<gl::ContextExtensions*>(extensions)
            ->getOrCreateGLExtension(renderInfo.getContextID())
        : nullptr;

    if (!ext ||
        !ext->isFrameBufferObjectSupported)
        return;

    const MRTNodeDecorator* mrt     = dynamic_cast<const MRTNodeDecorator*>(_pImpl.rootDecorator());
    const osg::Texture2D*   buffer  = mrt ? mrt->objectIdBuffer() : nullptr;
    if (_enabled &&
        buffer &&
        0 <= _position.x() && _position.x() < buffer->getTextureWidth() &&
        0 <= _position.y() && _position.y() < buffer->getTextureHeight())
    {
        unsigned char rgba[4] = {0, 0, 0, 0};

        buffer->apply(*renderInfo.getState());

        // The camera's FBO should still be bound at this point!
        glReadBuffer(GL_COLOR_ATTACHMENT1_EXT);
        glPixelStorei(GL_PACK_ALIGNMENT, 4);
        glReadPixels(_position.x(), _position.y(), 1, 1, GL_RGBA, GL_UNSIGNED_BYTE, rgba);

        const unsigned int id = util::color::uchar4ToUint(rgba[0], rgba[1], rgba[2], rgba[3]); // alpha is not used in object ids
        FLIRT_LOG(LOG(DEBUG)
            << "pixel (" << _position.x() << ", " << _position.y() << ") "
            << "-> ID = " << id << " "
            << "('" << (id ? client->getNodeName(id) : "") << "')";)

        xchg::IdSet selection;

        if (id > 0)
        {
            if (_accumulate)
            {
                client->getNodeParameter<xchg::IdSet>(parm::selection(), selection, client->sceneId());
                if (selection.find(id) == selection.end())
                    selection.insert(id);
                else
                    selection.erase(id);
            }
            else
                selection.insert(id);
        }
        if (!selection.empty())
            client->setNodeParameter<xchg::IdSet>(parm::selection(), selection, client->sceneId());
        else
            client->unsetNodeParameter(parm::selection(), client->sceneId());
    }

    ext->glBindFramebuffer(osg::FrameBufferObject::READ_DRAW_FRAMEBUFFER, 0);
    // because of the KeepFBOsBoundCallback update callback

    _position.set(INVALID_COORD, INVALID_COORD);
}
