// ========================================================================= //
// Copyright 2016-2018 SUPAMONKS_STUDIO                                      //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
//                                                                           //
// ========================================================================= //
#pragma once

#include <osg/Callback>

#include "DevicePImpl.hpp"

namespace smks { namespace view
{
    ///////////////////////////////////////
    // class DevicePImpl::ClockTimeUpdate
    ///////////////////////////////////////
    class DevicePImpl::ClockTimeUpdate:
        public osg::NodeCallback
    {
    public:
        typedef osg::ref_ptr<ClockTimeUpdate> Ptr;
    private:
        DevicePImpl&        _pImpl;
        const osg::Timer_t  _start;

    public:
        static inline
        Ptr
        create(DevicePImpl& pImpl)
        {
            Ptr ptr(new ClockTimeUpdate(pImpl));
            return ptr;
        }

        static inline
        void
        destroy(Ptr& ptr)
        {
            ptr = nullptr;
        }

    protected:
        explicit
        ClockTimeUpdate(DevicePImpl& pImpl):
            osg::NodeCallback(),
            _pImpl(pImpl),
            _start(osg::Timer::instance()->tick())
        {
            updateClockTime();
        }

    public:
        virtual inline
        void
        operator()(osg::Node* n, osg::NodeVisitor* nv)
        {
            if (nv->getVisitorType() == osg::NodeVisitor::UPDATE_VISITOR)
                updateClockTime();

            traverse(n, nv);
        }

    private:
        inline
        void
        updateClockTime()
        {
            const float time = static_cast<float>(
                osg::Timer::instance()->delta_s(
                    _start,
                    osg::Timer::instance()->tick()));

            _pImpl.updateClockTime(time);
        }
    };
}
}
