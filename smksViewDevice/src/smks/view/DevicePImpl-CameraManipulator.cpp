// ========================================================================= //
// Copyright 2016-2018 SUPAMONKS_STUDIO                                      //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
//                                                                           //
// ========================================================================= //
#include <iostream>
#include <smks/ClientBase.hpp>

#include "DevicePImpl-CameraManager.hpp"
#include "DevicePImpl-CameraManipulator.hpp"
#include "DevicePImpl-ClockTimeUpdate.hpp"
#include "DevicePImpl-FrameBufferDisplayOperation.hpp"
#include "DevicePImpl-GLProgramSwitch.hpp"
#include "DevicePImpl-PickingCallback.hpp"
#include "DevicePImpl-ResizeHandler.hpp"
#include "DevicePImpl-SwapCallback.hpp"
#include "ManipulatorGeometry.hpp"

namespace smks { namespace view
{
    /////////////////////////
    // class ProjectionUpdate
    /////////////////////////
    class DevicePImpl::CameraManipulator::ProjectionUpdate:
        public osg::Callback
    {
    private:
        CameraManipulator& _self;
    public:
        explicit
        ProjectionUpdate(CameraManipulator& self):
            osg::Callback(),
            _self(self)
        { }

        virtual inline
        bool run(osg::Object* obj,
                 osg::Object* data)
        {
            osg::Camera*            camera  = dynamic_cast<osg::Camera*>(obj);
            const osg::Projection*  proj    = _self.getDisplayedProjection();

            if (camera &&
                proj)
                camera->setProjectionMatrix(proj->getMatrix());

            return traverse(obj, data);
        }
    };
    /////////////////////////
}
}

using namespace smks;

// explicit
view::DevicePImpl::CameraManipulator::CameraManipulator(DevicePImpl& pImpl):
    osgGA::CameraManipulator(),
    _pImpl      (pImpl),
    _camManager (nullptr),
    _projUpdate (new ProjectionUpdate(*this))
{ }

void
view::DevicePImpl::CameraManipulator::build(Ptr const& ptr)
{
    dispose(ptr);

    _camManager = CameraManager::create(_pImpl);
    _pImpl.device().getCamera()
        ->addUpdateCallback(_projUpdate.get());

}

void
view::DevicePImpl::CameraManipulator::dispose(Ptr const& ptr)
{
    _pImpl.device().getCamera()
        ->removeUpdateCallback(_projUpdate.get());
    CameraManager::destroy(_camManager);
}

unsigned int
view::DevicePImpl::CameraManipulator::viewportCameraXform() const
{
    assert(_camManager);
    return _camManager->viewportCameraXform();
}

unsigned int
view::DevicePImpl::CameraManipulator::viewportCamera() const
{
    assert(_camManager);
    return _camManager->viewportCamera();
}

unsigned int
view::DevicePImpl::CameraManipulator::displayedCamera() const
{
    assert(_camManager);
    return _camManager->displayedCamera();
}

void
view::DevicePImpl::CameraManipulator::displayCamera(unsigned int node)
{
    assert(_camManager);
    _camManager->displayCamera(node);
}

// virtual
void
view::DevicePImpl::CameraManipulator::setByMatrix(const osg::Matrixd& mtx)
{
    assert(_camManager);
}

// virtual
void
view::DevicePImpl::CameraManipulator::setByInverseMatrix(const osg::Matrixd& mtx)
{
    assert(_camManager);
}

// virtual
osg::Matrixd
view::DevicePImpl::CameraManipulator::getMatrix() const
{
    const osg::Projection* proj = getDisplayedProjection();
    if (proj)
    {
        const osg::NodePathList& paths = proj->getParentalNodePaths();
        if (!paths.empty())
            return computeLocalToWorld(paths.front());
    }
    return osg::Matrixd::identity();
}

const osg::Projection*
view::DevicePImpl::CameraManipulator::getDisplayedProjection() const
{
    return _camManager
        ? _camManager->getDisplayedProjection()
        : nullptr;
}

// virtual
osg::Matrixd
view::DevicePImpl::CameraManipulator::getInverseMatrix() const
{
    const osg::Matrixd& localToWorld = getMatrix();
    return osg::Matrixd::inverse(localToWorld);
}

// virtual
bool
view::DevicePImpl::CameraManipulator::handle(const osgGA::GUIEventAdapter&  ea,
                                             osgGA::GUIActionAdapter&       aa)
{
    assert(_camManager);
    if (ea.getHandled())
        return false;

    Device*                 device = dynamic_cast<Device*>(&aa);
    ClientBase::Ptr const&  client = device ? device->client().lock() : nullptr;
    if (!client)
        return false;

    if (ea.getEventType() == osgGA::GUIEventAdapter::RESIZE)
    {
        // update the trackball camera's projection matrix parameter
        _camManager->handleFrameResized(
            *client,
            ea.getWindowX(),
            ea.getWindowY(),
            ea.getWindowWidth(),
            ea.getWindowHeight());
    }
    return false;
}
