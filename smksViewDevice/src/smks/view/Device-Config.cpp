// ========================================================================= //
// Copyright 2016-2018 SUPAMONKS_STUDIO                                      //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
//                                                                           //
// ========================================================================= //
#include <sstream>
#include <json/json.h>

#include <smks/sys/Exception.hpp>
#include <smks/xchg/cfg.hpp>
#include <smks/math/math.hpp>
#include "Device.hpp"

using namespace smks;

void
view::Device::Config::setDefaults()
{
    windowTitle.clear();
    screenWidth          = 1280;
    screenHeight         = 1080;
    screenRatio          = 0.0f; // ignored
    viewport_manipulator = true;
    viewport_picking     = true;
}

view::Device::Config::Config()
{
    setDefaults();
}

// explicit
view::Device::Config::Config(const char* json)
{
    setDefaults();

    if (strlen(json) == 0)
        return;

    Json::Value  root;
    Json::Reader reader;

    if (!reader.parse(json, root))
    {
        std::stringstream sstr;
        sstr
            << "Failed to parse Json configuration: " << reader.getFormatedErrorMessages();
        throw sys::Exception(sstr.str().c_str(), "Viewport Device Configuration");
    }

    windowTitle          = root.get(xchg::cfg::window_title,         windowTitle.c_str()).asString();
    screenWidth          = root.get(xchg::cfg::screen_width,         screenWidth).asUInt();
    screenHeight         = root.get(xchg::cfg::screen_height,        screenHeight).asUInt();
    screenRatio          = static_cast<float>(root.get(xchg::cfg::screen_ratio, screenRatio).asDouble());
    viewport_manipulator = root.get(xchg::cfg::viewport_manipulator, viewport_manipulator).asBool();
    viewport_picking     = root.get(xchg::cfg::viewport_picking,     viewport_picking).asBool();

    if (screenWidth == 0)
        throw sys::Exception(
            "Screen width must be positive.",
            "Viewport Device Configuration");
    if (screenHeight == 0)
        throw sys::Exception(
            "Screen height must be positive.",
            "Viewport Device Configuration");

}
