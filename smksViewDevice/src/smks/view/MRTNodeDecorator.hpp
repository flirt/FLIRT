// ========================================================================= //
// Copyright 2016-2018 SUPAMONKS_STUDIO                                      //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
//                                                                           //
// ========================================================================= //
#pragma once

#include "RTTNodeDecorator.hpp"

namespace smks { namespace view
{
    class MRTNodeDecorator:
        public RTTNodeDecorator
    {
    public:
        typedef osg::ref_ptr<MRTNodeDecorator>  Ptr;
    private:
        typedef std::shared_ptr<ClientBase>     ClientPtr;

    public:
        static
        Ptr
        create(ClientPtr const&             client,
               osg::Node*                   node,
               osg::GraphicsContext*        context,
               osg::Camera::BufferComponent displayedAttachment = osg::Camera::COLOR_BUFFER0,
               unsigned int                 textureUnit         = 0);

    protected:
        MRTNodeDecorator(ClientPtr const&,
                         osg::Node*,
                         osg::GraphicsContext*,
                         osg::Camera::BufferComponent,
                         unsigned int);

        virtual
        void
        setUpPreRenderCameraAttachments(osg::Camera*);

    public:
        const osg::Texture2D*
        objectIdBuffer() const;
    };
}
}
