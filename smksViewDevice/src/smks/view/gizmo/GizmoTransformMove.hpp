// ========================================================================= //
// Modification copyright 2016-2018 SUPAMONKS_STUDIO                         //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
// _________________________________________________________________________ //
// Copyright 2012 Cedric Guillemet                                           //
//                                                                           //
// Permission is hereby granted, free of charge, to any person obtaining     //
// a copy of this software and associated documentation files (the           //
// "Software"), to deal in the Software without restriction, including       //
// without limitation the rights to use, copy, modify, merge, publish,       //
// distribute, sublicense, and/or sell copies of the Software, and to        //
// permit persons to whom the Software is furnished to do so, subject to     //
// the following conditions:                                                 //
//                                                                           //
// The above copyright notice and this permission notice shall be included   //
// in all copies or substantial portions of the Software.                    //
//                                                                           //
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,           //
// EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF        //
// MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.    //
// IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY      //
// CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,      //
// TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE         //
// SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.                    //
//                                                                           //
// ========================================================================= //

#pragma once

#include "GizmoTransform.hpp"

namespace smks { namespace view { namespace gizmo
{
    class CGizmoTransformMove :
        public CGizmoTransform
    {

    public:
        CGizmoTransformMove();
        virtual ~CGizmoTransformMove();

        // return true if gizmo transform capture mouse
        virtual bool OnMouseDown(unsigned int x, unsigned int y);
        virtual void OnMouseMove(unsigned int x, unsigned int y);
        virtual void OnMouseUp(unsigned int x, unsigned int y);

        virtual void GetRenderBuffers(RenderBuffers&) const;
        // snap

        virtual
        void
        SetSnap(float snapx, float snapy, float snapz)
        {
            m_MoveSnap = tvector3(snapx, snapy, snapz);
        }
        virtual void SetSnap(float snap) {}

        tvector3
        GetMoveSnap() const
        {
            return m_MoveSnap;
        }

        virtual void ApplyTransform(tvector3& trans, bool bAbsolute);

    protected:
        enum MOVETYPE
        {
            MOVE_NONE,
            MOVE_X,
            MOVE_Y,
            MOVE_Z,
            MOVE_XY,
            MOVE_XZ,
            MOVE_YZ,
            MOVE_XYZ
        };
        MOVETYPE m_MoveType,m_MoveTypePredict;
        //tplane m_plan;
        //tvector3 m_LockVertex;
        tvector3 m_MoveSnap;

        bool GetOpType(MOVETYPE &type, unsigned int x, unsigned int y);
        tvector3 RayTrace(tvector3& rayOrigin, tvector3& rayDir, tvector3& norm);
    };

}
}
}
