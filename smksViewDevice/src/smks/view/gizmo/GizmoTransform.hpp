// ========================================================================= //
// Modification copyright 2016-2018 SUPAMONKS_STUDIO                         //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
// _________________________________________________________________________ //
// Copyright 2012 Cedric Guillemet                                           //
//                                                                           //
// Permission is hereby granted, free of charge, to any person obtaining     //
// a copy of this software and associated documentation files (the           //
// "Software"), to deal in the Software without restriction, including       //
// without limitation the rights to use, copy, modify, merge, publish,       //
// distribute, sublicense, and/or sell copies of the Software, and to        //
// permit persons to whom the Software is furnished to do so, subject to     //
// the following conditions:                                                 //
//                                                                           //
// The above copyright notice and this permission notice shall be included   //
// in all copies or substantial portions of the Software.                    //
//                                                                           //
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,           //
// EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF        //
// MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.    //
// IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY      //
// CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,      //
// TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE         //
// SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.                    //
//                                                                           //
// ========================================================================= //

#pragma once

#include <iostream>
#include "ZBaseMaths.hpp"
#include "IGizmo.hpp"

namespace smks { namespace view { namespace gizmo
{
    class CGizmoTransform:
        public IGizmo
    {
    protected:
        tmatrix *m_EditMatrix;
        tmatrix m_Model,m_Proj, m_ModelViewProj;
        tmatrix m_invmodel, m_invproj;
        tvector3 m_CamSrc,m_CamDir,m_CamUp;

        tmatrix m_svgMatrix;
        bool m_bUseSnap;
        float mDisplayScale;

        Location mLocation;

        tmatrix mWorkingMatrix; // for dissociated components
        tvector3 *mEditPos, *mEditScale ;
        tquaternion *mEditQT;

        //draw helpers

        unsigned int mMask;
        int mScreenWidth, mScreenHeight;
    public:
        CGizmoTransform()
        {
            m_EditMatrix = NULL;
            m_bUseSnap = false;
            //mCamera = NULL;
            //mTransform = NULL;
            mEditPos =  mEditScale = NULL;
            mEditQT = NULL;
            mMask = AXIS_ALL;
            m_Lng = 1.f;
            mScreenHeight = mScreenWidth = 1;
            mDisplayScale = 1.f;
        }

        virtual ~CGizmoTransform()
        {
        }



        virtual void SetEditMatrix(float *pMatrix)
        {
            m_EditMatrix = (tmatrix*)pMatrix;
            //mTransform = NULL;

            mEditPos = mEditScale = NULL;
            mEditQT = NULL;
        }
        virtual void SetDisplayScale( float aScale ) { mDisplayScale = aScale; }
        /*
        virtual void SetEditTransform(ZTransform *pTransform)
        {
            mTransform = pTransform;
            if(!pTransform)
                m_EditMatrix = NULL;
            else
                m_EditMatrix = (tmatrix*)&mTransform->GetLocalMatrix();
            mEditPos = mEditScale = NULL;
            mEditQT = NULL;
        }
        */
        /*
        virtual void SetEditVQS(tvector3* aPosition, tquaternion *aQT, tvector3 *aScale)
        {
            mEditPos = aPosition;
            mEditQT = aQT;
            mEditScale = aScale;

            //mTransform = NULL;
            m_EditMatrix = NULL;

            tmatrix p1,p2,p3;
            p1.Identity();
            p2.Identity();
            p3.Identity();
            if (aPosition)
                p1.Translation(*aPosition);
            if (aQT)
                p2.RotationQuaternion(*aQT);
            if (aScale)
                p3.Scaling(*aScale);
            mWorkingMatrix = p3 * p2 * p1;
            m_EditMatrix = &mWorkingMatrix;
        }
        */
        virtual void SetScreenDimension( int screenWidth, int screenHeight)
        {
            mScreenWidth = screenWidth;
            mScreenHeight = screenHeight;
        }
        virtual void SetCameraMatrix(const float *Model, const float *Proj)
        {
            m_Model = *(tmatrix*)Model;
            m_Proj = *(tmatrix*)Proj;
            m_ModelViewProj = m_Model * m_Proj;

            m_invmodel=m_Model;
            m_invmodel.Inverse();

            m_invproj=m_Proj;
            m_invproj.Inverse();

            m_CamSrc = m_invmodel.V4.position;
            m_CamDir = m_invmodel.V4.dir;
            m_CamUp = m_invmodel.V4.up;
        }

        void
        TransformFromWorldSpaceToPixelSpace(const tvector3& world_pos, tvector2& pixel_coords) const
        {
            tvector4 p;

            memcpy(static_cast<float*>(p), static_cast<const float*>(world_pos), sizeof(tvector3));
            p[3] = 1.0f;

            p.Transform(m_Model);
            p.Transform(m_Proj);

            const float rW = fabsf(p[3]) > 1e-6f
                ? 1.0f / p[3]
                : 0.0f;

            p[0] *= rW;
            p[1] *= rW;
            p[2] *= rW;

            pixel_coords[0] = mScreenWidth * (0.5f + 0.5f * p[0]);
            pixel_coords[1] = mScreenHeight * (0.5f - 0.5f * p[1]);
        }

        // tools

        void BuildRay(int x, int y, tvector3 &rayOrigin, tvector3 &rayDir) const
        {
            float frameX = (float)mScreenWidth;
            float frameY = (float)mScreenHeight;
            tvector3 screen_space;

            // device space to normalized screen space
            screen_space.x = ( ( (2.f * (float)x) / frameX ) - 1 ) / m_Proj.m[0][0];//.right.x;
            screen_space.y = -( ( (2.f * (float)y) / frameY ) - 1 ) / m_Proj.m[1][1];
            screen_space.z = -1.f;

            // screen space to world space

            rayOrigin = m_invmodel.V4.position;
            rayDir.TransformVector(screen_space, m_invmodel);
            rayDir.Normalize();
        }


        tvector3 GetVector(int vtID) const
        {
            switch (vtID)
            {
            case 0: return tvector3(1,0,0);
            case 1: return tvector3(0,1,0);
            case 2: return tvector3(0,0,1);
            }
            return tvector3(0,0,0);
        }

        tvector3 GetTransformedVector(int vtID) const
        {
            tvector3 vt;
            switch (vtID)
            {
            case 0: vt = tvector3(1,0,0); break;
            case 1: vt = tvector3(0,1,0); break;
            case 2: vt = tvector3(0,0,1); break;
            }
            if (mLocation == LOCATE_LOCAL)
            {
                vt.TransformVector(*m_EditMatrix);
                vt.Normalize();
            }
            return vt;
        }
        virtual void SetAxisMask(unsigned int mask)
        {
            mMask = mask;
        }
    /*
        tvector3 GetTransformedVector(tvector3& vt)
        {
            tmatrix mt = *m_EditMatrix;
            mt.NoTrans();
            vt.TransformPoint(mt);
            return vt;
        }
    */
        float ComputeScreenFactor() const
        {
            if (m_EditMatrix == nullptr)
                return 1.0f;

            tvector4 trf = vector4( m_EditMatrix->V4.position.x, m_EditMatrix->V4.position.y, m_EditMatrix->V4.position.z, 1.f);
            trf.Transform(m_ModelViewProj);
            return mDisplayScale * 0.15f * trf.w;
        }

        tplane m_plan;
        tvector3 m_LockVertex;
        float m_Lng;

        tvector3 RayTrace2(const tvector3& rayOrigin, const tvector3& rayDir, const tvector3& norm, const tmatrix& mt, tvector3 trss, bool lockVTNorm = true)
        {
            extern tvector3 ptd;

            tvector3 df,inters;

            m_plan=vector4(m_EditMatrix->GetTranslation(), norm);
            m_plan.RayInter(inters,rayOrigin,rayDir);
            df.TransformPoint( inters, mt );

            df /= ComputeScreenFactor();
            /*
            ptd = inters;
            df = inters - m_EditMatrix->GetTranslation();
            df /=GetScreenFactor();
            df2 = df;

            df2.TransformPoint(mt);
            df2 *= trss;
            */
            m_LockVertex = df;
            if (lockVTNorm)
            {
                m_LockVertex.Normalize();
            }
            else
            {
                m_LockVertex = inters;
            }
            m_Lng = df.Length();

            return df;
        }

        // snap
        virtual void UseSnap(bool bUseSnap)
        {
            m_bUseSnap = bUseSnap;
        }

        virtual bool IsUsingSnap() const
        {
            return m_bUseSnap;
        }
        //transform
        virtual void ApplyTransform(tvector3& trans, bool bAbsolute) = 0;

        void SetLocation(Location aLocation) { mLocation = aLocation; }
        Location GetLocation() const { return mLocation; }


    protected:


        void SnapIt(float &pos, float &snap) const
        {
            float sn = (float)fmod(pos,snap);
            if (fabs(sn)< (snap*0.25f) ) pos-=sn;
            if (fabs(sn)> (snap*0.75f) ) pos=( (pos-sn) + ((sn>0)?snap:-snap) );
        }

    };
}
}
}
