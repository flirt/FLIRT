// ========================================================================= //
// Modification copyright 2016-2018 SUPAMONKS_STUDIO                         //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
// _________________________________________________________________________ //
// Copyright 2012 Cedric Guillemet                                           //
//                                                                           //
// Permission is hereby granted, free of charge, to any person obtaining     //
// a copy of this software and associated documentation files (the           //
// "Software"), to deal in the Software without restriction, including       //
// without limitation the rights to use, copy, modify, merge, publish,       //
// distribute, sublicense, and/or sell copies of the Software, and to        //
// permit persons to whom the Software is furnished to do so, subject to     //
// the following conditions:                                                 //
//                                                                           //
// The above copyright notice and this permission notice shall be included   //
// in all copies or substantial portions of the Software.                    //
//                                                                           //
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,           //
// EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF        //
// MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.    //
// IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY      //
// CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,      //
// TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE         //
// SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.                    //
//                                                                           //
// ========================================================================= //

#pragma once

#include "GizmoTransform.hpp"

namespace smks { namespace view { namespace gizmo
{
    class CGizmoTransformScale : public CGizmoTransform
    {
    public:
        CGizmoTransformScale();
        virtual ~CGizmoTransformScale();

        // return true if gizmo transform capture mouse
        virtual bool OnMouseDown(unsigned int x, unsigned int y);
        virtual void OnMouseMove(unsigned int x, unsigned int y);
        virtual void OnMouseUp(unsigned int x, unsigned int y);

        virtual void GetRenderBuffers(RenderBuffers&) const;

        /*
        void SetScaleSnap(float snap)
        {
            m_ScaleSnap = snap;
        }
        */
        virtual void SetSnap(float snap) { m_ScaleSnap = snap; }
        virtual void SetSnap(float snapx, float snapy, float snapz) {}

        float GetScaleSnap()
        {
            return m_ScaleSnap;
        }

        virtual void ApplyTransform(tvector3& trans, bool bAbsolute);

    protected:
        enum SCALETYPE
        {
            SCALE_NONE,
            SCALE_X,
            SCALE_Y,
            SCALE_Z,
            SCALE_XY,
            SCALE_XZ,
            SCALE_YZ,
            SCALE_XYZ
        };
        SCALETYPE m_ScaleType,m_ScaleTypePredict;


        unsigned int m_LockX, m_LockY;
        tvector2 m_CenterOnScreen;  //<! pixel coordinates of the gizmo's center
        float m_ScaleSnap;

        bool GetOpType(SCALETYPE &type, unsigned int x, unsigned int y);
        //tvector3 RayTrace(tvector3& rayOrigin, tvector3& rayDir, tvector3& norm, tmatrix& mt, tvector3 trss);
        void SnapScale(float &val);

    };

}
}
}
