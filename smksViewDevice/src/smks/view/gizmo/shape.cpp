// ========================================================================= //
// Modification copyright 2016-2018 SUPAMONKS_STUDIO                         //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
// _________________________________________________________________________ //
// Copyright 2012 Cedric Guillemet                                           //
//                                                                           //
// Permission is hereby granted, free of charge, to any person obtaining     //
// a copy of this software and associated documentation files (the           //
// "Software"), to deal in the Software without restriction, including       //
// without limitation the rights to use, copy, modify, merge, publish,       //
// distribute, sublicense, and/or sell copies of the Software, and to        //
// permit persons to whom the Software is furnished to do so, subject to     //
// the following conditions:                                                 //
//                                                                           //
// The above copyright notice and this permission notice shall be included   //
// in all copies or substantial portions of the Software.                    //
//                                                                           //
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,           //
// EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF        //
// MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.    //
// IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY      //
// CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,      //
// TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE         //
// SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.                    //
//                                                                           //
// ========================================================================= //
#include <iostream>
#include <cassert>
#include <sstream>
#include <stdexcept>

#include "IGizmo.hpp"
#include "ZBaseMaths.hpp"
#include "RenderBuffers.hpp"
#include "shape.hpp"

using namespace smks;


// static
void
view::gizmo::addCircle(const tvector3&  orig,
                       const tvector3&  vtx,
                       const tvector3&  vty,
                       const tvector4&  rgba,
                       RenderBuffers&   ret)
{
    static const size_t NUM     = 50;
    static const float  CSTEP   = cosf(ZPI * 2.0f / static_cast<float>(NUM));
    static const float  SSTEP   = sinf(ZPI * 2.0f / static_cast<float>(NUM));

    const unsigned int start = static_cast<unsigned int>(ret.vertices.size());  // expressed in vertices

    ret.vertices.reserve(ret.vertices   .size() + (NUM + 1));
    ret.lines   .reserve(ret.lines      .size() + (NUM << 1));

    float cAng = 1.0f;
    float sAng = 0.0f;
    for (size_t n = 0; n <= NUM; ++n)
    {
        ret.vertices.push_back(Vertex(
            orig + cAng * vtx + sAng * vty,
            rgba));
        const float cNext = cAng * CSTEP - sAng * SSTEP;
        const float sNext = cAng * SSTEP + sAng * CSTEP;
        cAng = cNext;
        sAng = sNext;
    }
    // -- line strip
    for (unsigned int i = start + 1; i < static_cast<unsigned int>(ret.vertices.size()); ++i)
    {
        ret.lines.push_back(i-1);
        ret.lines.push_back(i);
    }
}

// static
void
view::gizmo::addCircleHalf(const tvector3&  orig,
                           const tvector3&  vtx,
                           const tvector3&  vty,
                           const tvector4&  rgba,
                           tvector4&        camPlan,
                           RenderBuffers&   ret)
{
    static const size_t NUM     = 30;
    static const float  CSTEP   = cosf(ZPI / static_cast<float>(NUM));
    static const float  SSTEP   = sinf(ZPI / static_cast<float>(NUM));

    const unsigned int start = static_cast<unsigned int>(ret.vertices.size());  // expressed in vertices

    ret.vertices.reserve(ret.vertices   .size() + (NUM + 1));
    ret.lines   .reserve(ret.lines      .size() + (NUM << 1));

    float cAng = 1.0f;
    float sAng = 0.0f;
    for (size_t n = 0; n <= NUM; ++n)
    {
        const tvector3& vt = orig + cAng * vtx + sAng * vty;
        if (camPlan.DotNormal(vt))
            ret.vertices.push_back(Vertex(vt, rgba));
        const float cNext = cAng * CSTEP - sAng * SSTEP;
        const float sNext = cAng * SSTEP + sAng * CSTEP;
        cAng = cNext;
        sAng = sNext;
    }
    // -- line strip
    for (unsigned int i = start + 1; i < static_cast<unsigned int>(ret.vertices.size()); ++i)
    {
        ret.lines.push_back(i-1);
        ret.lines.push_back(i);
    }
}

// static
void
view::gizmo::addAxis(const tvector3&    orig,
                     const tvector3&    axis,
                     const tvector3&    vtx,
                     const tvector3&    vty,
                     float              fct,
                     float              fct2,
                     const tvector4&    rgba,
                     RenderBuffers&     ret)
{
    static const size_t NUM     = 30;
    static const float  CSTEP   = cosf(ZPI * 2.0f / static_cast<float>(NUM));
    static const float  SSTEP   = sinf(ZPI * 2.0f / static_cast<float>(NUM));

    ret.vertices    .reserve(ret.vertices   .size() + NUM + 4);
    ret.lines       .reserve(ret.lines      .size() + 2);
    ret.triangles   .reserve(ret.triangles  .size() + 3 * NUM);

    // add axis line
    unsigned int start = static_cast<unsigned int>(ret.vertices.size());    // expressed in vertices

    ret.vertices.push_back(Vertex(orig, rgba));
    ret.vertices.push_back(Vertex(orig + axis, rgba));
    ret.lines.push_back(start);
    ret.lines.push_back(start + 1);

    // add arrow
    ret.vertices.push_back(Vertex(orig + axis, rgba));

    start = static_cast<unsigned int>(ret.vertices.size()); // expressed in vertices

    float cAng = 1.0f;
    float sAng = 0.0f;
    for (size_t n = 0; n <= NUM; ++n)
    {
        ret.vertices.push_back(Vertex(
            orig + fct2 * axis + fct * (cAng * vtx + sAng * vty),
            rgba));
        const float cNext = cAng * CSTEP - sAng * SSTEP;
        const float sNext = cAng * SSTEP + sAng * CSTEP;
        cAng = cNext;
        sAng = sNext;
    }
    // -- triangle fan
    for (unsigned int n = 1; n <= static_cast<unsigned int>(NUM); ++n)
    {
        ret.triangles.push_back(start - 1);
        ret.triangles.push_back(start + (n-1));
        ret.triangles.push_back(start + n);
    }
}

// static
void
view::gizmo::addCamem(const tvector3&   orig,
                      const tvector3&   vtx,
                      const tvector3&   vty,
                      float             ng,
                      RenderBuffers&    ret)
{
    static const size_t NUM     = 30;
    const float cStep   = cosf(ng / static_cast<float>(NUM));
    const float sStep   = sinf(ng / static_cast<float>(NUM));
    float       cAng    = 1.0f;
    float       sAng    = 0.0f;
    tvector4    rgba;

    ret.vertices    .reserve(ret.vertices   .size() + ((NUM+1) << 1));
    ret.lines       .reserve(ret.lines      .size() + NUM + 2);
    ret.triangles   .reserve(ret.triangles  .size() + 3 * (NUM - 1));

    // 1. fill
    unsigned int start = static_cast<unsigned int>(ret.vertices.size());    // expressed in vertices

    rgba.x = 1.0f; rgba.y = 1.0f; rgba.z = 0.0f; rgba.w = 0.5f;
    ret.vertices.push_back(Vertex(orig, rgba));
    cAng = 1.0f;
    sAng = 0.0f;
    for (size_t n = 0; n < NUM; ++n)
    {
        ret.vertices.push_back(Vertex(
            orig + cAng * vtx + sAng * vty,
            rgba));
        const float cNext = cAng * cStep - sAng * sStep;
        const float sNext = cAng * sStep + sAng * cStep;
        cAng = cNext;
        sAng = sNext;
    }

    // -- triangle fan
    for (unsigned int n = 1; n < static_cast<unsigned int>(NUM); ++n)
    {
        ret.triangles.push_back(start);
        ret.triangles.push_back(start + (n-1));
        ret.triangles.push_back(start + n);
    }

    // 2. stroke
    start = static_cast<unsigned int>(ret.vertices.size()); // expressed in vertices
    rgba.x = 1.0f; rgba.y = 1.0f; rgba.z = 0.2f; rgba.w = 1.0f;
    ret.vertices.push_back(Vertex(orig, rgba));
    cAng = 1.0f;
    sAng = 0.0f;
    for (size_t n = 0; n < NUM; ++n)
    {
        ret.vertices.push_back(Vertex(
            orig + cAng * vtx + sAng * vty,
            rgba));
        const float cNext = cAng * cStep - sAng * sStep;
        const float sNext = cAng * sStep + sAng * cStep;
        cAng = cNext;
        sAng = sNext;
    }

    // -- line strip
    ret.lines.push_back(start);
    for (unsigned int n = 0; n < static_cast<unsigned int>(NUM); ++n)
        ret.lines.push_back(start + n);
    ret.lines.push_back(start);
}

// static
void
view::gizmo::addQuad(const tvector3&    orig,
                     float              size,
                     bool               bSelected,
                     const tvector3&    axisU,
                     const tvector3&    axisV,
                     RenderBuffers&     ret)
{
    const tvector3 p[4] = {
        orig,
        orig + size * axisU,
        orig + size * (axisU + axisV),
        orig + size * axisV };

    ret.vertices    .reserve(ret.vertices   .size() + 8);
    ret.lines       .reserve(ret.lines      .size() + 8);
    ret.triangles   .reserve(ret.triangles  .size() + 6);

    tvector4 rgba;
    rgba.x = 1.0f; rgba.y = 1.0f; rgba.z = 1.0f; rgba.w = 0.6f; // color if selected

    // 1. fill
    unsigned int start = static_cast<unsigned int>(ret.vertices.size());    // expressed in vertices

    if (!bSelected)
    { rgba.x = 1.0f; rgba.y = 1.0f; rgba.z = 0.0f; rgba.w = 0.5f; }

    for (unsigned int i = 0; i < 4; ++i)
        ret.vertices.push_back(Vertex(p[i], rgba));

    ret.triangles.push_back(start);
    ret.triangles.push_back(start + 1);
    ret.triangles.push_back(start + 2);

    ret.triangles.push_back(start);
    ret.triangles.push_back(start + 2);
    ret.triangles.push_back(start + 3);

    // 2. stroke
    start = static_cast<unsigned int>(ret.vertices.size()); // expressed in vertices

    if (!bSelected)
    { rgba.x = 1.0f; rgba.y = 1.0f; rgba.z = 0.2f; rgba.w = 1.0f; }

    for (unsigned int i = 0; i < 4; ++i)
        ret.vertices.push_back(Vertex(p[i], rgba));
    for (unsigned int i = 0; i < 4; ++i)
    {
        ret.lines.push_back(start + i);
        ret.lines.push_back(start + ((i+1)%4));
    }
}

// static
void
view::gizmo::addTri(const tvector3& orig,
                    float           size,
                    bool            bSelected,
                    const tvector3& axisU,
                    const tvector3& axisV,
                    RenderBuffers&  ret)
{
    const tvector3 p[3] = {
        orig,
        orig + size * axisU,
        orig + size * axisV };

    ret.vertices    .reserve(ret.vertices   .size() + 6);
    ret.lines       .reserve(ret.lines      .size() + 6);
    ret.triangles   .reserve(ret.triangles  .size() + 3);

    tvector4 rgba;
    rgba.x = 1.0f; rgba.y = 1.0f; rgba.z = 1.0f; rgba.w = 0.6f; // color if selected

    // 1. fill
    unsigned int start = static_cast<unsigned int>(ret.vertices.size());    // expressed in vertices

    if (!bSelected)
    { rgba.x = 1.0f; rgba.y = 1.0f; rgba.z = 0.0f; rgba.w = 0.5f; }

    for (unsigned int i = 0; i < 3; ++i)
    {
        ret.vertices.push_back(Vertex(p[i], rgba));
        ret.triangles.push_back(start + i);
    }

    // 2. stroke
    start = static_cast<unsigned int>(ret.vertices.size()); // expressed in vertices

    if (!bSelected)
    { rgba.x = 1.0f; rgba.y = 1.0f; rgba.z = 0.2f; rgba.w = 1.0f; }

    for (unsigned int i = 0; i < 3; ++i)
        ret.vertices.push_back(Vertex(p[i], rgba));
    for (unsigned int i = 0; i < 3; ++i)
    {
        ret.lines.push_back(start + i);
        ret.lines.push_back(start + ((i+1)%3));
    }
}
