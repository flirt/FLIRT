// ========================================================================= //
// Modification copyright 2016-2018 SUPAMONKS_STUDIO                         //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
// _________________________________________________________________________ //
// Copyright 2012 Cedric Guillemet                                           //
//                                                                           //
// Permission is hereby granted, free of charge, to any person obtaining     //
// a copy of this software and associated documentation files (the           //
// "Software"), to deal in the Software without restriction, including       //
// without limitation the rights to use, copy, modify, merge, publish,       //
// distribute, sublicense, and/or sell copies of the Software, and to        //
// permit persons to whom the Software is furnished to do so, subject to     //
// the following conditions:                                                 //
//                                                                           //
// The above copyright notice and this permission notice shall be included   //
// in all copies or substantial portions of the Software.                    //
//                                                                           //
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,           //
// EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF        //
// MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.    //
// IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY      //
// CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,      //
// TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE         //
// SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.                    //
//                                                                           //
// ========================================================================= //

#include "GizmoTransformMove.hpp"
#include "RenderBuffers.hpp"
#include "shape.hpp"

using namespace smks;

view::gizmo::IGizmo*
view::gizmo::CreateMoveGizmo()
{
    return new CGizmoTransformMove;
}

view::gizmo::tvector3 ptd;
//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

view::gizmo::CGizmoTransformMove::CGizmoTransformMove() : CGizmoTransform()
{
    m_MoveType = MOVE_NONE;
}

view::gizmo::CGizmoTransformMove::~CGizmoTransformMove()
{ }

view::gizmo::tvector3
view::gizmo::CGizmoTransformMove::RayTrace(tvector3& rayOrigin, tvector3& rayDir, tvector3& norm)
{
    tvector3 df,inters;
    m_plan=vector4(m_EditMatrix->GetTranslation(), norm);
    m_plan.RayInter(inters,rayOrigin,rayDir);
    ptd = inters;
    df = inters - m_EditMatrix->GetTranslation();
    df /= ComputeScreenFactor();
    m_LockVertex = inters;
    return df;
}

bool
view::gizmo::CGizmoTransformMove::GetOpType(MOVETYPE &type, unsigned int x, unsigned int y)
{
    tvector3 rayOrigin, rayDir, df;
    BuildRay(x, y, rayOrigin, rayDir);
    m_svgMatrix = *m_EditMatrix;


    tvector3 trss(GetTransformedVector(0).Length(),
        GetTransformedVector(1).Length(),
        GetTransformedVector(2).Length());

    tmatrix mt;
    if (mLocation == LOCATE_LOCAL)
    {
        mt = *m_EditMatrix;
        mt.Inverse();
    }
    else
    {
        // world
        mt.Translation( -m_EditMatrix->V4.position);
    }

    // plan 1 : X/Z
    df = RayTrace2(rayOrigin, rayDir, GetTransformedVector(1), mt, trss, false);

    if ( ( df.x >= 0 ) && (df.x <= 1) && ( fabs(df.z) < 0.1f ) ) { type = MOVE_X; return true; }
    else if ( ( df.z >= 0 ) && (df.z <= 1) && ( fabs(df.x) < 0.1f ) ){ type = MOVE_Z; return true; }
    else if ( (df.x<0.5f) && (df.z<0.5f) && (df.x>0) && (df.z>0)) { type = MOVE_XZ; return true; }
    else {

        //plan 2 : X/Y
        df = RayTrace2(rayOrigin, rayDir, GetTransformedVector(2), mt, trss, false);

        if ( ( df.x >= 0 ) && (df.x <= 1) && ( fabs(df.y) < 0.1f ) ) { type = MOVE_X; return true; }
        if ( ( df.y >= 0 ) && (df.y <= 1) && ( fabs(df.x) < 0.1f ) ) { type = MOVE_Y; return true; }
        else if ( (df.x<0.5f) && (df.y<0.5f) && (df.x>0) && (df.y>0)) { type = MOVE_XY; return true; }
        else
        {
            //plan 3: Y/Z
            df = RayTrace2(rayOrigin, rayDir, GetTransformedVector(0), mt, trss, false);

            if ( ( df.y >= 0 ) && (df.y <= 1) && ( fabs(df.z) < 0.1f ) ) { type = MOVE_Y; return true; }
            else if ( ( df.z >= 0 ) && (df.z <= 1) && ( fabs(df.y) < 0.1f ) ) { type = MOVE_Z; return true; }
            else if ( (df.y<0.5f) && (df.z<0.5f) && (df.y>0) && (df.z>0)) { type = MOVE_YZ; return true; }

        }
    }

    type = MOVE_NONE;
    return false;
}


bool
view::gizmo::CGizmoTransformMove::OnMouseDown(unsigned int x, unsigned int y)
{
    if (m_EditMatrix)
    {
        return GetOpType(m_MoveType, x, y);
    }

    m_MoveType = MOVE_NONE;
    return false;
}


void
view::gizmo::CGizmoTransformMove::OnMouseMove(unsigned int x, unsigned int y)
{
    if (m_MoveType != MOVE_NONE)
    {
        tvector3 rayOrigin,rayDir,df, inters;

        BuildRay(x, y, rayOrigin, rayDir);
        m_plan.RayInter(inters,rayOrigin,rayDir);

        tvector3 axeX(1,0,0),axeY(0,1,0),axeZ(0,0,1);


        if (mLocation == LOCATE_LOCAL)
        {
            axeX.TransformVector(*m_EditMatrix);
            axeY.TransformVector(*m_EditMatrix);
            axeZ.TransformVector(*m_EditMatrix);
            axeX.Normalize();
            axeY.Normalize();
            axeZ.Normalize();
        }

        df = inters - m_LockVertex;

        switch (m_MoveType)
        {
        case MOVE_XZ:   df = tvector3(df.Dot(axeX) , 0,df.Dot(axeZ));   break;
        case MOVE_X:    df = tvector3(df.Dot(axeX) , 0,0);              break;
        case MOVE_Z:    df = tvector3(0, 0,df.Dot(axeZ));               break;
        case MOVE_XY:   df = tvector3(df.Dot(axeX) ,df.Dot(axeY), 0);   break;
        case MOVE_YZ:   df = tvector3(0,df.Dot(axeY) ,df.Dot(axeZ));    break;
        case MOVE_Y:    df = tvector3(0,df.Dot(axeY), 0);               break;
        }

        tvector3 adf;

        tmatrix mt;
        if (m_bUseSnap)
        {
            SnapIt(df.x,m_MoveSnap.x);
            SnapIt(df.y,m_MoveSnap.y);
            SnapIt(df.z,m_MoveSnap.z);
        }

        adf = df.x*axeX + df.y*axeY + df.z*axeZ;

        mt.Translation(adf);
        *m_EditMatrix = m_svgMatrix;
        m_EditMatrix->Multiply(mt);
        //if (mTransform) mTransform->Update();

        if (mEditPos)
            *mEditPos = m_EditMatrix->V4.position;
    }
    else
    {
        // predict move
        if (m_EditMatrix)
        {
            GetOpType(m_MoveTypePredict, x, y);
        }
    }

}

void
view::gizmo::CGizmoTransformMove::OnMouseUp(unsigned int x, unsigned int y)
{
    m_MoveType = MOVE_NONE;
}

void
view::gizmo::CGizmoTransformMove::GetRenderBuffers(RenderBuffers& buffers) const
{
    buffers.clear();

    if (m_EditMatrix)
    {
        const float screenFactor = ComputeScreenFactor();
        tvector3 orig = m_EditMatrix->GetTranslation();

        tvector3 axeX(1,0,0),axeY(0,1,0),axeZ(0,0,1);


        if (mLocation == LOCATE_LOCAL)
        {
            axeX.TransformVector(*m_EditMatrix);
            axeY.TransformVector(*m_EditMatrix);
            axeZ.TransformVector(*m_EditMatrix);
            axeX.Normalize();
            axeY.Normalize();
            axeZ.Normalize();
        }

        addQuad(orig, 0.5f*screenFactor, (m_MoveTypePredict == MOVE_XZ), axeX, axeZ, buffers);
        addQuad(orig, 0.5f*screenFactor, (m_MoveTypePredict == MOVE_XY), axeX, axeY, buffers);
        addQuad(orig, 0.5f*screenFactor, (m_MoveTypePredict == MOVE_YZ), axeY, axeZ, buffers);

        axeX *= screenFactor;
        axeY *= screenFactor;
        axeZ *= screenFactor;

        // plan1
        if (m_MoveTypePredict != MOVE_X)
            addAxis(orig,axeX,axeY,axeZ,0.05f,0.83f,vector4(1,0,0,1), buffers);
        else
            addAxis(orig,axeX,axeY,axeZ, 0.05f,0.83f,vector4(1,1,1,1), buffers);

        //plan2
        if (m_MoveTypePredict != MOVE_Y)
            addAxis(orig,axeY,axeX,axeZ, 0.05f,0.83f,vector4(0,1,0,1), buffers);
        else
            addAxis(orig,axeY,axeX,axeZ, 0.05f,0.83f,vector4(1,1,1,1), buffers);

        //plan3
        if (m_MoveTypePredict != MOVE_Z)
            addAxis(orig,axeZ,axeX,axeY, 0.05f,0.83f,vector4(0,0,1,1), buffers);
        else
            addAxis(orig,axeZ,axeX,axeY, 0.05f,0.83f,vector4(1,1,1,1), buffers);
}
}

void
view::gizmo::CGizmoTransformMove::ApplyTransform(tvector3& trans, bool bAbsolute)
{
    if (bAbsolute)
    {
        m_EditMatrix->m16[12] = trans.x;
        m_EditMatrix->m16[13] = trans.y;
        m_EditMatrix->m16[14] = trans.z;
    }
    else
    {
        m_EditMatrix->m16[12] += trans.x;
        m_EditMatrix->m16[13] += trans.y;
        m_EditMatrix->m16[14] += trans.z;
    }
}

