// ========================================================================= //
// Modification copyright 2016-2018 SUPAMONKS_STUDIO                         //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
// _________________________________________________________________________ //
// Copyright 2012 Cedric Guillemet                                           //
//                                                                           //
// Permission is hereby granted, free of charge, to any person obtaining     //
// a copy of this software and associated documentation files (the           //
// "Software"), to deal in the Software without restriction, including       //
// without limitation the rights to use, copy, modify, merge, publish,       //
// distribute, sublicense, and/or sell copies of the Software, and to        //
// permit persons to whom the Software is furnished to do so, subject to     //
// the following conditions:                                                 //
//                                                                           //
// The above copyright notice and this permission notice shall be included   //
// in all copies or substantial portions of the Software.                    //
//                                                                           //
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,           //
// EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF        //
// MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.    //
// IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY      //
// CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,      //
// TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE         //
// SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.                    //
//                                                                           //
// ========================================================================= //

namespace smks { namespace view { namespace gizmo
{
    enum Location
    {
        LOCATE_VIEW,
        LOCATE_WORLD,
        LOCATE_LOCAL,
    };

    enum RotateAxis
    {
        AXIS_X = 1,
        AXIS_Y = 2,
        AXIS_Z = 4,
        AXIS_TRACKBALL = 8,
        AXIS_SCREEN = 16,
        AXIS_ALL = 31
    };

    struct RenderBuffers;

    class IGizmo
    {
    public:
        virtual ~IGizmo() { }
        virtual void SetEditMatrix(float*) = 0;

        virtual void SetCameraMatrix(const float *model, const float *proj) = 0;
        virtual void SetScreenDimension(int screenWidth, int screenHeight) = 0;
        virtual void SetDisplayScale(float aScale) = 0;

        // return true if gizmo transform capture mouse
        virtual bool OnMouseDown(unsigned int x, unsigned int y) = 0;
        virtual void OnMouseMove(unsigned int x, unsigned int y) = 0;
        virtual void OnMouseUp(unsigned int x, unsigned int y) = 0;

        // snaping
        virtual void UseSnap(bool) = 0;
        virtual bool IsUsingSnap() const = 0;
        virtual void SetSnap(float snapx, float snapy, float snapz) = 0;
        virtual void SetSnap(float snap) = 0;

        virtual void SetLocation(Location aLocation)  = 0;
        virtual Location GetLocation() const = 0;
        virtual void SetAxisMask(unsigned int mask) = 0;

        // rendering
        virtual void GetRenderBuffers(RenderBuffers&) const = 0;
    };

    // Create new gizmo
    IGizmo *CreateMoveGizmo();
    IGizmo *CreateRotateGizmo();
    IGizmo *CreateScaleGizmo();
}
}
}
