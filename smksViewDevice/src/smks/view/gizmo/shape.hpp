// ========================================================================= //
// Modification copyright 2016-2018 SUPAMONKS_STUDIO                         //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
// _________________________________________________________________________ //
// Copyright 2012 Cedric Guillemet                                           //
//                                                                           //
// Permission is hereby granted, free of charge, to any person obtaining     //
// a copy of this software and associated documentation files (the           //
// "Software"), to deal in the Software without restriction, including       //
// without limitation the rights to use, copy, modify, merge, publish,       //
// distribute, sublicense, and/or sell copies of the Software, and to        //
// permit persons to whom the Software is furnished to do so, subject to     //
// the following conditions:                                                 //
//                                                                           //
// The above copyright notice and this permission notice shall be included   //
// in all copies or substantial portions of the Software.                    //
//                                                                           //
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,           //
// EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF        //
// MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.    //
// IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY      //
// CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,      //
// TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE         //
// SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.                    //
//                                                                           //
// ========================================================================= //
#pragma once

#include <vector>
struct tvector3;
struct tvector4;

namespace smks { namespace view { namespace gizmo
{
    void
    addCircle(const tvector3& orig, const tvector3& vtx, const tvector3& vty, const tvector4& rgba, RenderBuffers&);

    void
    addCircleHalf(const tvector3& orig, const tvector3& vtx, const tvector3& vty, const tvector4& rgba, tvector4& camPlan, RenderBuffers&);

    void
    addAxis(const tvector3& orig, const tvector3& axis, const tvector3& vtx, const tvector3& vty, float fct, float fct2, const tvector4& rgba, RenderBuffers&);

    void
    addCamem(const tvector3& orig, const tvector3& vtx, const tvector3& vty, float ng, RenderBuffers&);

    void
    addQuad(const tvector3& orig, float size, bool bSelected, const tvector3& axisU, const tvector3& axisV, RenderBuffers&);

    void
    addTri(const tvector3& orig, float size, bool bSelected, const tvector3& axisU, const tvector3& axisV, RenderBuffers&);
}
}
}
