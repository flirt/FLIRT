// ========================================================================= //
// Modification copyright 2016-2018 SUPAMONKS_STUDIO                         //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
// _________________________________________________________________________ //
// Copyright 2012 Cedric Guillemet                                           //
//                                                                           //
// Permission is hereby granted, free of charge, to any person obtaining     //
// a copy of this software and associated documentation files (the           //
// "Software"), to deal in the Software without restriction, including       //
// without limitation the rights to use, copy, modify, merge, publish,       //
// distribute, sublicense, and/or sell copies of the Software, and to        //
// permit persons to whom the Software is furnished to do so, subject to     //
// the following conditions:                                                 //
//                                                                           //
// The above copyright notice and this permission notice shall be included   //
// in all copies or substantial portions of the Software.                    //
//                                                                           //
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,           //
// EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF        //
// MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.    //
// IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY      //
// CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,      //
// TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE         //
// SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.                    //
//                                                                           //
// ========================================================================= //

#include "GizmoTransformScale.hpp"
#include "RenderBuffers.hpp"
#include "shape.hpp"

using namespace smks;

extern view::gizmo::tvector3 ptd;


view::gizmo::IGizmo*
view::gizmo::CreateScaleGizmo()
{
    return new CGizmoTransformScale;
}


//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

view::gizmo::CGizmoTransformScale::CGizmoTransformScale() : CGizmoTransform()
{
    m_ScaleType = SCALE_NONE;
}

view::gizmo::CGizmoTransformScale::~CGizmoTransformScale()
{ }



bool
view::gizmo::CGizmoTransformScale::GetOpType(SCALETYPE &type, unsigned int x, unsigned int y)
{
    // init
    tvector3 trss(GetTransformedVector(0).Length(),
        GetTransformedVector(1).Length(),
        GetTransformedVector(2).Length());

    m_LockX = x;
    m_LockY = y;
    m_svgMatrix = *m_EditMatrix;

    TransformFromWorldSpaceToPixelSpace(m_EditMatrix->GetTranslation(), m_CenterOnScreen);

    tmatrix mt;
    mt = *m_EditMatrix;
    mt.NoTrans();
    mt.Inverse();


    //tmatrix mt;
    if (mLocation == LOCATE_LOCAL)
    {
        mt = *m_EditMatrix;
        mt.Inverse();
    }
    else
    {
        // world
        mt.Translation( -m_EditMatrix->V4.position);
    }

    // ray casting
    tvector3 rayOrigin,rayDir,df2;
    BuildRay(x, y, rayOrigin, rayDir);

    // plan 1 : X/Z
    df2 = RayTrace2(rayOrigin, rayDir, GetTransformedVector(1), mt, trss);


    if ( (df2.x<0.2f) && (df2.z<0.2f) && (df2.x>0) && (df2.z>0)) { type = SCALE_XYZ; return true; }
    else if ( ( df2.x >= 0 ) && (df2.x <= 1) && ( fabs(df2.z) < 0.1f ) ) { type = SCALE_X; return true; }
    else if ( ( df2.z >= 0 ) && (df2.z <= 1) && ( fabs(df2.x) < 0.1f ) ) { type = SCALE_Z; return true; }
    else if ( (df2.x<0.5f) && (df2.z<0.5f) && (df2.x>0) && (df2.z>0)) { type = SCALE_XZ; return true; }
    else
    {
        //plan 2 : X/Y
        df2 = RayTrace2(rayOrigin, rayDir, GetTransformedVector(2), mt, trss);

        if ( (df2.x<0.2f) && (df2.y<0.2f) && (df2.x>0) && (df2.y>0)) { type = SCALE_XYZ; return true; }
        else if ( ( df2.x >= 0 ) && (df2.x <= 1) && ( fabs(df2.y) < 0.1f ) ) { type = SCALE_X; return true; }
        else if ( ( df2.y >= 0 ) && (df2.y <= 1) && ( fabs(df2.x) < 0.1f ) ) { type = SCALE_Y; return true; }
        else if ( (df2.x<0.5f) && (df2.y<0.5f) && (df2.x>0) && (df2.y>0)) { type = SCALE_XY; return true; }
        else
        {
            //plan 3: Y/Z
            df2 = RayTrace2(rayOrigin, rayDir, GetTransformedVector(0), mt, trss);

            if ( (df2.y<0.2f) && (df2.z<0.2f) && (df2.y>0) && (df2.z>0)) { type = SCALE_XYZ; return true; }
            else if ( ( df2.y >= 0 ) && (df2.y <= 1) && ( fabs(df2.z) < 0.1f ) ) { type = SCALE_Y; return true; }
            else if ( ( df2.z >= 0 ) && (df2.z <= 1) && ( fabs(df2.y) < 0.1f ) ) { type = SCALE_Z; return true; }
            else if ( (df2.y<0.5f) && (df2.z<0.5f) && (df2.y>0) && (df2.z>0)) { type = SCALE_YZ; return true; }
        }
    }

    type = SCALE_NONE;
    return false;
}


bool
view::gizmo::CGizmoTransformScale::OnMouseDown(unsigned int x, unsigned int y)
{
    if (m_EditMatrix)
    {
        return GetOpType(m_ScaleType, x, y);
    }

    m_ScaleType = SCALE_NONE;
    return false;
}

void
view::gizmo::CGizmoTransformScale::SnapScale(float &val)
{
    if (m_bUseSnap)
    {
        val*=(100.0f);
        SnapIt(val,m_ScaleSnap);
        val/=(100.0f);
    }
}

void
view::gizmo::CGizmoTransformScale::OnMouseMove(unsigned int x, unsigned int y)
{
    if (m_ScaleType != SCALE_NONE)
    {
        tvector3 rayOrigin, rayDir, df, inters, machin;
        tvector3 scVect, scVect2;

        BuildRay(x, y, rayOrigin, rayDir);
        m_plan.RayInter(inters, rayOrigin, rayDir);

        switch (m_ScaleType)
        {
        case SCALE_XZ: scVect = tvector3(1,0,1); break;
        case SCALE_X:  scVect = tvector3(1,0,0); break;
        case SCALE_Z:  scVect = tvector3(0,0,1); break;
        case SCALE_XY: scVect = tvector3(1,1,0); break;
        case SCALE_YZ: scVect = tvector3(0,1,1); break;
        case SCALE_Y:  scVect = tvector3(0,1,0); break;
        case SCALE_XYZ:scVect = tvector3(1,1,1); break;
        }

        df = inters-m_EditMatrix->GetTranslation();
        df /= ComputeScreenFactor();
        scVect2 = tvector3(1,1,1) - scVect;

        /********************************************
        if (m_ScaleType == SCALE_XYZ)
        {
            int difx = x - m_LockX;
            float lng2 = 1.0f + ( float(difx) / 200.0f);
            SnapScale(lng2);
            scVect *=lng2;
        }
        else
        {
            int difx = x - m_LockX;
            int dify = y - m_LockY;

            float len = sqrtf( (float)(difx*difx) + (float)(dify*dify) );

            float lng2 = len /100.f;

            //
            //float lng2 = ( df.Dot(m_LockVertex));
   //         char tmps[512];
   //         sprintf(tmps, "%5.4f\n", lng2 );
   //         OutputDebugStringA( tmps );

            //
            //if (lng2 < 1.f)
            //{
            //  if ( lng2<= 0.001f )
            //      lng2 = 0.001f;
            //  else
            //  {
            //      //lng2+=4.f;
            //      lng2/=5.f;
            //  }
            //}
   //         else
   //         {
   //             int a = 1;
   //         }


            SnapScale(lng2);
            scVect *= lng2;
            scVect += scVect2;
        }
        ********************************************/

        float       scale = 0.0f;
        tvector2    ref_vector;
        ref_vector[0] = static_cast<float>(m_LockX) - m_CenterOnScreen[0];
        ref_vector[1] = static_cast<float>(m_LockY) - m_CenterOnScreen[1];
        const float sqrLenRef = ref_vector.Dot(ref_vector);

        if (sqrLenRef > 1e-6f)
        {
            tvector2 cur_vector;
            cur_vector[0] = static_cast<float>(static_cast<int>(x)) - m_CenterOnScreen[0];  // warning x and y are unsigned int
            cur_vector[1] = static_cast<float>(static_cast<int>(y)) - m_CenterOnScreen[1];

            scale = ref_vector.Dot(cur_vector) / sqrLenRef;
        }
        if (scale > 1.0f)
            scale *= scale; // non-linear scale boost when greater than 1
        else if (fabsf(scale) < 1e-4f)
            scale = 1e-4f;

        SnapScale(scale);
        scVect *= scale;
        scVect += scVect2;

        tmatrix mt,mt2;


        //printf("scVect = (%f, %f, %f)\n", scVect[0], scVect[1], scVect[2]);

        mt.Scaling(scVect);

        mt2.Identity();
        mt2.SetLine(0,GetTransformedVector(0));
        mt2.SetLine(1,GetTransformedVector(1));
        mt2.SetLine(2,GetTransformedVector(2));

        //mt2.Translation(0,0,0);
        //mt.Multiply(mt2);

        if (mLocation == LOCATE_WORLD)
        {
            mt2 = mt * m_svgMatrix;
        }
        else
        {
            mt2 = mt * m_svgMatrix;//.Multiply(m_svgMatrix);
        }
        *m_EditMatrix = mt2;
        //if (mTransform) mTransform->Update();
    }
    else
    {
        // predict move
        if (m_EditMatrix)
        {
            GetOpType(m_ScaleTypePredict, x, y);
        }
    }

}

void
view::gizmo::CGizmoTransformScale::OnMouseUp(unsigned int x, unsigned int y)
{
    m_ScaleType = SCALE_NONE;
}

void
view::gizmo::CGizmoTransformScale::GetRenderBuffers(RenderBuffers& buffers) const
{
    if (m_EditMatrix)
    {
        const float screenFactor = ComputeScreenFactor();

        //glDisable(GL_DEPTH_TEST);
        tvector3 orig(m_EditMatrix->m16[12], m_EditMatrix->m16[13], m_EditMatrix->m16[14]);


        // axis
        tvector3 axeX(1,0,0),axeY(0,1,0),axeZ(0,0,1);
        if (mLocation == LOCATE_LOCAL)
        {
            axeX.TransformVector(*m_EditMatrix);
            axeY.TransformVector(*m_EditMatrix);
            axeZ.TransformVector(*m_EditMatrix);
            axeX.Normalize();
            axeY.Normalize();
            axeZ.Normalize();
        }

        addTri(orig, 0.5f * screenFactor,((m_ScaleTypePredict==SCALE_XZ)||(m_ScaleTypePredict==SCALE_XYZ)), axeX, axeZ, buffers);
        addTri(orig, 0.5f * screenFactor,((m_ScaleTypePredict==SCALE_XY)||(m_ScaleTypePredict==SCALE_XYZ)), axeX, axeY, buffers);
        addTri(orig, 0.5f * screenFactor,((m_ScaleTypePredict==SCALE_YZ)||(m_ScaleTypePredict==SCALE_XYZ)), axeY, axeZ, buffers);
        //DrawTri(orig, 0.5f*GetScreenFactor(),((m_ScaleTypePredict==SCALE_XZ)||(m_ScaleTypePredict==SCALE_XYZ)), axeX, axeZ);
        //DrawTri(orig, 0.5f*GetScreenFactor(),((m_ScaleTypePredict==SCALE_XY)||(m_ScaleTypePredict==SCALE_XYZ)), axeX, axeY);
        //DrawTri(orig, 0.5f*GetScreenFactor(),((m_ScaleTypePredict==SCALE_YZ)||(m_ScaleTypePredict==SCALE_XYZ)), axeY, axeZ);

        axeX *= screenFactor;
        axeY *= screenFactor;
        axeZ *= screenFactor;


        //// plan1
        //if (m_ScaleTypePredict != SCALE_X)
        //  DrawAxis(orig,axeX,axeY,axeZ,0.05f,0.83f,vector4(1,0,0,1));
        //else
        //  DrawAxis(orig,axeX,axeY,axeZ,0.05f,0.83f,vector4(1,1,1,1));

        ////plan2
        //if (m_ScaleTypePredict != SCALE_Y)
        //  DrawAxis(orig,axeY,axeX,axeZ,0.05f,0.83f,vector4(0,1,0,1));
        //else
        //  DrawAxis(orig,axeY,axeX,axeZ,0.05f,0.83f,vector4(1,1,1,1));

        ////plan3
        //if (m_ScaleTypePredict != SCALE_Z)
        //  DrawAxis(orig,axeZ,axeX,axeY,0.05f,0.83f,vector4(0,0,1,1));
        //else
        //  DrawAxis(orig,axeZ,axeX,axeY,0.05f,0.83f,vector4(1,1,1,1));

        // plan1
        if (m_ScaleTypePredict != SCALE_X)
            addAxis(orig,axeX,axeY,axeZ,0.05f,0.83f,vector4(1,0,0,1),buffers);
        else
            addAxis(orig,axeX,axeY,axeZ,0.05f,0.83f,vector4(1,1,1,1),buffers);

        //plan2
        if (m_ScaleTypePredict != SCALE_Y)
            addAxis(orig,axeY,axeX,axeZ,0.05f,0.83f,vector4(0,1,0,1),buffers);
        else
            addAxis(orig,axeY,axeX,axeZ,0.05f,0.83f,vector4(1,1,1,1),buffers);

        //plan3
        if (m_ScaleTypePredict != SCALE_Z)
            addAxis(orig,axeZ,axeX,axeY,0.05f,0.83f,vector4(0,0,1,1),buffers);
        else
            addAxis(orig,axeZ,axeX,axeY,0.05f,0.83f,vector4(1,1,1,1),buffers);
    }
}

void
view::gizmo::CGizmoTransformScale::ApplyTransform(tvector3& trans, bool bAbsolute)
{
    if (bAbsolute)
    {
        tmatrix m_InvOrigScale,m_OrigScale;

        m_OrigScale.Scaling(GetTransformedVector(0).Length(),
        GetTransformedVector(1).Length(),
        GetTransformedVector(2).Length());

        m_InvOrigScale.Inverse(m_OrigScale);
        m_svgMatrix = *m_EditMatrix;

        tmatrix mt;
        mt.Scaling(trans.x/100.0f,trans.y/100.0f,trans.z/100.0f);
        mt.Multiply(m_InvOrigScale);
        mt.Multiply(m_svgMatrix);
        *m_EditMatrix = mt;
    }
    else
    {
        tmatrix mt,mt2;
        m_svgMatrix = *m_EditMatrix;
        mt.Scaling(trans.x/100.0f,trans.y/100.0f,trans.z/100.0f);

        mt2.SetLine(0,GetTransformedVector(0));
        mt2.SetLine(1,GetTransformedVector(1));
        mt2.SetLine(2,GetTransformedVector(2));
        mt2.Translation(0,0,0);
        mt.Multiply(mt2);
        mt.Multiply(m_svgMatrix);
        *m_EditMatrix = mt;
    }

}
