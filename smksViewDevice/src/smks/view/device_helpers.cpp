// ========================================================================= //
// Copyright 2016-2018 SUPAMONKS_STUDIO                                      //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
//                                                                           //
// ========================================================================= //
#include <smks/sys/Log.hpp>

#include "smks/view/device_helpers.hpp"
#include "Device.hpp"

#if defined(FLIRT_LOG_ENABLED)
//INITIALIZE_NULL_EXTERN_EASYLOGGINGPP;
INITIALIZE_NULL_EASYLOGGINGPP;
#endif // defined(FLIRT_LOG_ENABLED)

namespace smks { namespace view
{
    static
    AbstractDevice*
    createDevice(
        const char* jsonCfg,
        std::shared_ptr<ClientBase> const&,
        osg::Group* clientRoot,
        osg::GraphicsContext*);

    static
    void
    destroyDevice(AbstractDevice*);
}
}

using namespace smks;

view::AbstractDevice*
create(const char*                          jsonCfg,
       std::shared_ptr<ClientBase> const&   client,
       osg::Group*                          clientRoot,
       osg::GraphicsContext*                graphicsContext)
{
    return view::createDevice(jsonCfg, client, clientRoot, graphicsContext);
}

void
destroy(view::AbstractDevice* device)
{
    view::destroyDevice(device);
}

// static
view::AbstractDevice*
view::createDevice(
    const char*                          jsonCfg,
    std::shared_ptr<ClientBase> const&   client,
    osg::Group*                          clientRoot,
    osg::GraphicsContext*                graphicsContext)
{
#if defined(FLIRT_LOG_ENABLED)
    el::Helpers::setStorage(smks::sys::sharedLoggingRepository());
#endif //defined(FLIRT_LOG_ENABLED)

    return Device::create(jsonCfg, client, clientRoot, graphicsContext);
}

// static
void
view::destroyDevice(view::AbstractDevice* device)
{
    if (dynamic_cast<Device*>(device))
        Device::destroy(
            dynamic_cast<Device*>(device));

#if defined(FLIRT_LOG_ENABLED)
    el::Helpers::setStorage(nullptr);
#endif //defined(FLIRT_LOG_ENABLED)
}

