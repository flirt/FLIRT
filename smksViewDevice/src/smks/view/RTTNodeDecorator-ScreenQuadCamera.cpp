// ========================================================================= //
// Copyright 2016-2018 SUPAMONKS_STUDIO                                      //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
//                                                                           //
// ========================================================================= //
#include <osg/Uniform>
#include <osg/Geometry>
#include <osg/StateSet>
#include <osg/MatrixTransform>

#include <smks/ClientBase.hpp>
#include <smks/view/gl/ProgramInput.hpp>
#include <smks/view/gl/ProgramTemplate.hpp>
#include <smks/view/gl/ProgramTemplates.hpp>
#include <smks/view/gl/ProgramInputDeclarations.hpp>

#include "RTTNodeDecorator-ScreenQuadCamera.hpp"
#include "ProgramUpdateCallback.hpp"

using namespace smks;

// static
view::RTTNodeDecorator::ScreenQuadCamera::Ptr
view::RTTNodeDecorator::ScreenQuadCamera::create(ClientBase::Ptr const& client,
                                                 unsigned int           textureUnit)
{
    Ptr ptr(new ScreenQuadCamera(client, textureUnit));
    return ptr;
}

view::RTTNodeDecorator::ScreenQuadCamera::ScreenQuadCamera(ClientBase::Ptr const&   client,
                                                           unsigned int             textureUnit):
    util::osg::ScreenQuadCamera (textureUnit),
    _uRcpTextureSize            (gl::get_TextureProgram().createUniform(gl::uRecTextureSize()))
{
    view::gl::UniformInput::Ptr const& uDiffuseTexture = gl::get_TextureProgram().createUniform(
        gl::uDiffuseTexture(),
        static_cast<int>(textureUnit));

    quad()->getOrCreateStateSet()->addUniform(uDiffuseTexture->data());
    quad()->getOrCreateStateSet()->addUniform(_uRcpTextureSize->data());
    quad()->addUpdateCallback(new ProgramUpdateCallback(client, gl::get_TextureProgram()));
}

// virtual
void
view::RTTNodeDecorator::ScreenQuadCamera::handleResize(int x, int y, int width, int height)
{
    util::osg::ScreenQuadCamera::handleResize(x, y, width, height);
    _uRcpTextureSize->data()->set(osg::Vec2f(
        width > 0   ? 1.0f / static_cast<float>(width)  : 0.0f,
        height > 0  ? 1.0f / static_cast<float>(height) : 0.0f));
}
