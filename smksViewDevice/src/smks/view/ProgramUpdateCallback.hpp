// ========================================================================= //
// Copyright 2016-2018 SUPAMONKS_STUDIO                                      //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
//                                                                           //
// ========================================================================= //
#pragma once

#include <memory>
#include <osg/Callback>

namespace smks
{
    class ClientBase;
    namespace view
    {
        namespace gl
        {
            class ProgramTemplate;
        }

        // once attached to a drawable, this callback can load and assign
        // a shading program from the resource database just before
        // rendering.
        class ProgramUpdateCallback:
            public osg::Callback
        {
        private:
            const std::weak_ptr<ClientBase> _client;
            const gl::ProgramTemplate&      _program;
        public:
            ProgramUpdateCallback(std::shared_ptr<ClientBase> const&,
                                  const gl::ProgramTemplate&);

            virtual
            bool
            run(osg::Object*, osg::Object*);
        };
    }
}
