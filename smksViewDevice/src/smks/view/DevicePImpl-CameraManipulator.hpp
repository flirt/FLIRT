// ========================================================================= //
// Copyright 2016-2018 SUPAMONKS_STUDIO                                      //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
//                                                                           //
// ========================================================================= //
#pragma once

#include <boost/unordered_map.hpp>

#include <osgGA/CameraManipulator>

#include <smks/AbstractClientObserver.hpp>

#include "DevicePImpl.hpp"

namespace smks { namespace view
{
    class DevicePImpl::CameraManipulator:
        public osgGA::CameraManipulator
    {
    public:
        typedef osg::ref_ptr<CameraManipulator> Ptr;
    private:
        class ProjectionUpdate;
    private:
        typedef std::shared_ptr<CameraManager> CameraManagerPtr;
        typedef osg::ref_ptr<ProjectionUpdate> UpdateCallbackPtr;
    private:
        DevicePImpl&        _pImpl;
        CameraManagerPtr    _camManager;
        UpdateCallbackPtr   _projUpdate;

    public:
        static inline
        Ptr
        create(DevicePImpl& pImpl)
        {
            Ptr ptr(new CameraManipulator(pImpl));
            ptr->build(ptr);
            return ptr;
        }

        static inline
        void
        destroy(Ptr& ptr)
        {
            if (ptr)
                ptr->dispose(ptr);
            ptr = nullptr;
        }

    protected:
        explicit
        CameraManipulator(DevicePImpl&);

        void
        build(Ptr const&);

        void
        dispose(Ptr const&);
    public:
        unsigned int
        viewportCameraXform() const;

        unsigned int
        viewportCamera() const;

        unsigned int
        displayedCamera() const;

        void
        displayCamera(unsigned int);

        virtual
        void
        setByMatrix(const osg::Matrixd&);

        virtual
        void
        setByInverseMatrix(const osg::Matrixd&);

        virtual
        osg::Matrixd
        getMatrix() const;

    private:
        const osg::Projection*
        getDisplayedProjection() const;

    public:
        virtual
        osg::Matrixd
        getInverseMatrix() const;

        virtual
        bool
        handle(const osgGA::GUIEventAdapter&,
               osgGA::GUIActionAdapter&);
    };
}
}
