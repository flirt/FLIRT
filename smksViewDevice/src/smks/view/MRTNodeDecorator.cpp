// ========================================================================= //
// Copyright 2016-2018 SUPAMONKS_STUDIO                                      //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
//                                                                           //
// ========================================================================= //
#include <osg/Geode>
#include <osg/Geometry>
#include <osg/MatrixTransform>
#include <osg/Texture2D>

#include <smks/ClientBase.hpp>
#include <smks/util/osg/texture.hpp>

#include "RTTNodeDecorator-ScreenQuadCamera.hpp"
#include "MRTNodeDecorator.hpp"

using namespace smks;

// static
view::MRTNodeDecorator::Ptr
view::MRTNodeDecorator::create(ClientBase::Ptr const&       client,
                               osg::Node*                   node,
                               osg::GraphicsContext*        context,
                               osg::Camera::BufferComponent displayedAttachment,
                               unsigned int                 textureUnit)
{
    Ptr ptr = new MRTNodeDecorator(
        client,
        node,
        context,
        displayedAttachment,
        textureUnit);
    ptr->finalize();
    return ptr;
}

view::MRTNodeDecorator::MRTNodeDecorator(ClientBase::Ptr const& client,
                                         osg::Node*                     node,
                                         osg::GraphicsContext*          context,
                                         osg::Camera::BufferComponent   displayedAttachment,
                                         unsigned int                   textureUnit):
    RTTNodeDecorator(client, node, context, displayedAttachment, textureUnit)
{ }

// virtual
void
view::MRTNodeDecorator::setUpPreRenderCameraAttachments(osg::Camera* camera)
{
    assert(camera);

    osg::ref_ptr<osg::Texture2D> colorBuffer    = util::osg::createRGBATexture(false, osg::Texture::LINEAR, osg::Texture::LINEAR);
    osg::ref_ptr<osg::Texture2D> objectidBuffer = util::osg::createRGBATexture(false, osg::Texture::NEAREST, osg::Texture::NEAREST);

    colorBuffer->setName("__MRT.colorBufferTexture__");
    colorBuffer->setDataVariance(Object::DYNAMIC);

    objectidBuffer->setName("__MRT.objectIdBufferTexture__");
    objectidBuffer->setDataVariance(Object::DYNAMIC);

    camera->attach(osg::Camera::COLOR_BUFFER0, colorBuffer.get(), 0, 0, false);
    camera->attach(osg::Camera::COLOR_BUFFER1, objectidBuffer.get(), 0, 0, false);
}

const osg::Texture2D*
view::MRTNodeDecorator::objectIdBuffer() const
{
    osg::Camera::BufferAttachmentMap::const_iterator const& it =
        preRenderCamera()->getBufferAttachmentMap().find(osg::Camera::COLOR_BUFFER1);

    return it != preRenderCamera()->getBufferAttachmentMap().end() &&
        it->second._texture.valid()
        ? dynamic_cast<const osg::Texture2D*>(it->second._texture.get())
        : nullptr;
}
