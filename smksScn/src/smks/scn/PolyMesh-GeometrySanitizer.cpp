// ========================================================================= //
// Copyright 2016-2018 SUPAMONKS_STUDIO                                      //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
//                                                                           //
// ========================================================================= //

#include <smks/sys/Exception.hpp>

#include "PolyMesh-GeometrySanitizer.hpp"
#include "AbstractPolyMeshSchema.hpp"
#include "PolyMesh-TBBClearBufferElementKernel.hpp"
#include "PolyMesh-TBBComputeVertexNormalKernel.hpp"
#include "PolyMesh-TBBNormalizeVec3Kernel.hpp"
#include <smks/util/math/array.hpp>

using namespace Alembic;
using namespace smks;

static const int _grainSize = 100;

// static
size_t
scn::PolyMesh::GeometrySanitizer::triangulateNgons(unsigned int                     N,
                                                   const AbstractPolyMeshSchema*    schema,
                                                   std::vector<unsigned int>&       oCounts,
                                                   std::vector<unsigned int>&       oIndices,
                                                   std::vector<unsigned int>&       varyingRedir)
{
    size_t numTriangulated = 0;

    if (!schema ||
        schema->numSamples() == 0)
        throw sys::Exception("Invalid 'schema' argument for n-gon triangulation.", "Geometry Sanitizer");
    if (N < 3)
        throw sys::Exception("Invalid 'N' argument for n-gon triangulation.", "Geometry Sanitizer");

    oCounts     .clear();
    oIndices    .clear();
    varyingRedir.clear();

    // original topology
    size_t      numFaces    = 0;
    size_t      numIndices  = 0;
    const int*  counts      = schema->lockFaceCounts (numFaces);
    const int*  indices     = schema->lockFaceIndices(numIndices);

    if (numIndices > 0 &&
        numFaces > 0 &&
        indices &&
        counts)
    {
        oCounts     .reserve(numFaces   << 1);
        oIndices    .reserve(numIndices << 1);
        varyingRedir.reserve(numIndices << 1);

        const int*  currentFaceIndices  = indices;
        const int*  currentFaceCount    = counts;
        size_t      accCount            = 0;

        for (size_t i = 0; i < numFaces; ++i)
        {
            if (*currentFaceCount > static_cast<int>(N))
            {
                if (*currentFaceCount > 2)
                {
                    // triangulate n-gon
                    for (size_t j = 2; j < *currentFaceCount; ++j)
                    {
                        oCounts     .push_back(3);

                        oIndices    .push_back(currentFaceIndices[0]);
                        oIndices    .push_back(currentFaceIndices[j-1]);
                        oIndices    .push_back(currentFaceIndices[j]);

                        varyingRedir.push_back(static_cast<unsigned int>(accCount));
                        varyingRedir.push_back(static_cast<unsigned int>(accCount + j - 1));
                        varyingRedir.push_back(static_cast<unsigned int>(accCount + j));
                    }
                    ++numTriangulated;
                }
            }
            else
            {
                // directly copy polygon
                oCounts.push_back(*currentFaceCount);

                for (size_t j = 0; j < *currentFaceCount; ++j)
                {
                    oIndices    .push_back(currentFaceIndices[j]);

                    varyingRedir.push_back(static_cast<unsigned int>(accCount + j));
                }
            }

            // move to next polygon
            currentFaceIndices  += *currentFaceCount;
            accCount            += *currentFaceCount;
            ++currentFaceCount;
        }
    }

    oCounts     .shrink_to_fit();
    oIndices    .shrink_to_fit();
    varyingRedir.shrink_to_fit();

    schema->unlockFaceIndices();
    schema->unlockFaceCounts();

    return numTriangulated;
}

// static
const float*
scn::PolyMesh::GeometrySanitizer::computeVertexNormals(const AbstractPolyMeshSchema*    schema,
                                                       size_t&                          numNormals)
{
    if (!schema ||
        schema->numSamples() == 0)
        throw sys::Exception("Invalid 'schema' argument for vertex normal computation.", "Geometry Sanitizer");

    float*          normals         = nullptr;

    size_t          numFaces        = 0;
    size_t          numIndices      = 0;
    size_t          numPositions    = 0;
    const int*      counts          = schema->lockFaceCounts(numFaces);
    const int*      indices         = schema->lockFaceIndices(numIndices);
    const float*    positions       = schema->lockPositions(numPositions);

    if (numPositions > 0)
    {
        numNormals  = numPositions;
        normals     = new float[3 * numPositions];
        memset(normals, 0, sizeof(float) * 3 * numPositions);
        computeVertexNormals(numPositions, positions, numFaces, counts, indices, false, normals, 3);
    }

    schema->unlockFaceCounts();
    schema->unlockFaceIndices();
    schema->unlockPositions();

    return normals;
}

// static
void
scn::PolyMesh::GeometrySanitizer::computeVertexNormals(const AbstractPolyMeshSchema*    schema,
                                                       float*                           oBuffer,
                                                       size_t                           oStride)
{
    if (!schema ||
        schema->numSamples() == 0)
        throw sys::Exception("Invalid 'schema' argument for vertex normal computation.", "Geometry Sanitizer");

    size_t          numFaces        = 0;
    size_t          numIndices      = 0;
    size_t          numPositions    = 0;
    const int*      counts          = schema->lockFaceCounts(numFaces);
    const int*      indices         = schema->lockFaceIndices(numIndices);
    const float*    positions       = schema->lockPositions(numPositions);

    computeVertexNormals(numPositions, positions, numFaces, counts, indices, true, oBuffer, oStride);

    schema->unlockFaceCounts();
    schema->unlockFaceIndices();
    schema->unlockPositions();
}


// static
void
scn::PolyMesh::GeometrySanitizer::computeVertexNormals(size_t       numPositions,
                                                       const float* positions,
                                                       size_t       numFaces,
                                                       const int*   counts,
                                                       const int*   indices,
                                                       bool         clearBuffer,
                                                       float*       oBuffer,
                                                       size_t       oStride)
{
    if (numFaces        == 0 ||
        numPositions    == 0 ||
        counts      == nullptr ||
        indices     == nullptr ||
        positions   == nullptr)
        return;

    std::vector<int> firstIndex (numFaces, 0);

    for (size_t f = 1; f < numFaces; ++f)
        firstIndex[f] = firstIndex[f-1] + counts[f-1];

    if (clearBuffer)
    {
        TBBClearBufferElementKernel kernel  (oBuffer, oStride, 3);
        tbb::blocked_range<int>     range   (0, static_cast<int>(numPositions), _grainSize);

        tbb::parallel_for(range, kernel);
    }
    {
        TBBComputeVertexNormalKernel    kernel  (numFaces, counts, &firstIndex.front(), indices, positions, 3, oBuffer, oStride);
        tbb::blocked_range<int>         range   (0, static_cast<int>(numFaces), _grainSize);

        //kernel.sequentialRun();
        tbb::parallel_for(range, kernel);
    }
    {
        normalizeVec3Array(oBuffer, numPositions, oStride);
    }
}

// static
void
scn::PolyMesh::GeometrySanitizer::normalizeVec3Array(float* buffer,
                                                     size_t num,
                                                     size_t stride)
{
    const int           grainSize   = 100;

    TBBNormalizeVec3Kernel  kernel  (buffer, stride);
    tbb::blocked_range<int> range   (0, static_cast<int>(num), _grainSize);

    tbb::parallel_for(range, kernel);
}
