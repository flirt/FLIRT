// ========================================================================= //
// Copyright 2016-2018 SUPAMONKS_STUDIO                                      //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
//                                                                           //
// ========================================================================= //
#pragma once

#include <memory>
#include <smks/xchg/DAGHierarchyData.hpp>
#include "DAGHierarchy.hpp"

namespace smks { namespace scn
{
    ////////////////////////////
    // class VisibilityHierarchy
    ////////////////////////////
    class VisibilityHierarchy:
        public DAGHierarchy<char>
    {
    public:
        typedef std::shared_ptr<VisibilityHierarchy> Ptr;
    public:
        //////////////////////////////////
        // class VisibilityComponent::Data
        //////////////////////////////////
        class Data:
            public xchg::DAGHierarchyData<char>
        {
        public:
            typedef std::shared_ptr<Data> Ptr;
        public:
            static inline
            Ptr
            create()
            {
                Ptr ptr(new Data);
                return ptr;
            }
        private:
            Data():
                xchg::DAGHierarchyData<char>(false)
            { }
        public:
            virtual inline
            void
            recomputeEntry(size_t current)
            {
                assert(_local.size() == _global.size());
                assert(current < _global.size());
                _global[current] = _local[current] > 0 ? 1 : 0;
            }

            inline
            void
            accumulateEntries(size_t current, size_t parent)
            {
                assert(_local.size() == _global.size());
                assert(current < _global.size());
                assert(parent < _global.size());
                _global[current] = _global[parent] > 0 && _local[current] > 0 ? 1 : 0;
            }
        };
        //////////////////////////////////
    public:
        static inline
        Ptr
        create()
        {
            Ptr ptr(new VisibilityHierarchy(Data::create()));
            return ptr;
        }

    private:
        explicit inline
        VisibilityHierarchy(Data::Ptr const& data):
            DAGHierarchy<char>(data)
        { }

    public:
        inline
        xchg::NodeClass
        nodeClass() const
        {
            return xchg::SCENE_OBJECT; // those nodes must redirect parm events related to 'numSamples' parameter to the scene
        }
    };
}
}
