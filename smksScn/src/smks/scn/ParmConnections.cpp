// ========================================================================= //
// Copyright 2016-2018 SUPAMONKS_STUDIO                                      //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
//                                                                           //
// ========================================================================= //
#include "smks/scn/ParmConnections.hpp"

namespace smks { namespace scn
{
    class ParmConnections::PImpl
    {
    private:
        ParmConnections&        _self;

        const TreeNode::WPtr    _node;
        const std::string       _name;
        Connections             _connections;
    public:
        PImpl(ParmConnections&      self,
              TreeNode::Ptr const&  node,
              const char*           name):
            _self       (self),
            _node       (node),
            _name       (name),
            _connections()
        { }

        inline
        const std::string&
        name() const
        {
            return _name;
        }

        inline
        bool
        empty() const
        {
            return _connections.empty();
        }

        inline
        ParmConnectionBase::WPtr const&
        front() const
        {
            assert(!_connections.empty());
            return _connections.front();
        }

        inline
        void
        add(ParmConnectionBase::WPtr const& added_)
        {
            ParmConnectionBase::Ptr const& added = added_.lock();
            if (!added)
                return;

            for (size_t i = 0; i < _connections.size(); ++i)
            {
                ParmConnectionBase::Ptr const& cnx = _connections[i].lock();
                if (!cnx)
                    continue;
                if (cnx->type() != added->type())
                    throw sys::Exception("New connection incompatible with existing connections in the set.", "Parameter Connection Addition");
                if (cnx.get() == added.get())
                    return;
            }
            _connections.push_back(added);
            _self.dirty();
        }

        inline
        void
        clear()
        {
            while (!_connections.empty())
                remove(_connections.front());
        }

        inline
        void
        remove(ParmConnectionBase::WPtr const& removed_)
        {
            ParmConnectionBase::Ptr const& removed = removed_.lock();
            for (size_t i = 0; i < _connections.size(); ++i)
            {
                ParmConnectionBase::Ptr const& cnx = _connections[i].lock();
                if (cnx.get() == removed.get())
                {
                    std::swap(_connections[i], _connections.back());
                    _connections.pop_back();
                    _self.dirty();
                    return;
                }
            }
        }

        inline
        size_t
        numConnections() const
        {
            return _connections.size();
        }

        inline
        ParmConnectionBase::WPtr const&
        getConnection(size_t i) const
        {
            assert(i < _connections.size());
            return *(_connections.begin() + i);
        }

        inline
        TreeNode::WPtr const&
        node() const
        {
            return _node;
        }
    };
}
}

using namespace smks;

scn::ParmConnections::ParmConnections(TreeNode::Ptr const&  node,
                                      const char*           name):
    _pImpl(new PImpl(*this, node, name))
{ }

// virtual
scn::ParmConnections::~ParmConnections()
{
    delete _pImpl;
}

const char*
scn::ParmConnections::name() const
{
    return _pImpl->name().c_str();
}

bool
scn::ParmConnections::empty() const
{
    return _pImpl->empty();
}

scn::ParmConnectionBase::WPtr const&
scn::ParmConnections::front() const
{
    return _pImpl->front();
}

void
scn::ParmConnections::add(ParmConnectionBase::WPtr const& added_)
{
    _pImpl->add(added_);
}

void
scn::ParmConnections::clear()
{
    _pImpl->clear();
}

void
scn::ParmConnections::remove(ParmConnectionBase::WPtr const& removed_)
{
    _pImpl->remove(removed_);
}

size_t
scn::ParmConnections::numConnections() const
{
    return _pImpl->numConnections();
}

scn::ParmConnectionBase::WPtr const&
scn::ParmConnections::getConnection(size_t i) const
{
    return _pImpl->getConnection(i);
}

scn::TreeNode::WPtr const&
scn::ParmConnections::node() const
{
    return _pImpl->node();
}
