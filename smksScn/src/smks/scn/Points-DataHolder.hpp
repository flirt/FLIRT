// ========================================================================= //
// Copyright 2016-2018 SUPAMONKS_STUDIO                                      //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
//                                                                           //
// ========================================================================= //
#pragma once

#include "smks/scn/Points.hpp"
#include "AbstractObjectDataHolder.hpp"
#include "AbstractPointsSchema.hpp"
#include "BufferAttributes.hpp"
#include "../util/data/ArrayCache.hpp"

namespace smks { namespace scn
{
    class Points::DataHolder:
        public AbstractObjectDataHolder
    {
    private:
        AbstractPointsSchema*                       _schema; // does not own it
        float                                       _defaultWidth;
        BufferAttributes                            _vertexAttribs;
        util::data::ArrayCache<std::vector<float>>  _vertices;
        util::data::ArrayCache<xchg::BoundingBox>   _bounds;

    public:
        DataHolder();

        void
        build(AbstractObjectSchema*);

        void
        initialize();

        void
        uninitialize();

        void
        synchronizeWithSchema();

        inline
        void
        dirtyCacheEntry(size_t idx, bool synchronize)
        {
            throw sys::Exception(
                "Only implemented for scene objects users can manually define.",
                "Dirty Points Data");
        }

    private:
        void
        unsetAllParameters();

        const float*
        getVertices(size_t&, BufferAttributes&);

        const xchg::BoundingBox&
        getBounds();
    };
}
}
