// ========================================================================= //
// Copyright 2016-2018 SUPAMONKS_STUDIO                                      //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
//                                                                           //
// ========================================================================= //
#pragma once

#include "AbstractObjectSchema.hpp"
#include "BufferAttributes.hpp"

namespace smks
{
    namespace xchg
    {
        struct BoundingBox;
    }
    namespace scn
    {
        class Points;

        class AbstractPointsSchema:
            public AbstractObjectSchema
        {
        public:
            inline const AbstractPointsSchema*  asPointsSchema() const  { return this; }
            inline AbstractPointsSchema*        asPointsSchema()        { return this; }

            virtual
            bool
            has(Property) const = 0;

            virtual
            const float*
            lockPositions(size_t&) const = 0;

            virtual
            void
            unlockPositions() const = 0;

            virtual
            const float*
            lockWidths(size_t&) const = 0;

            virtual
            void
            unlockWidths() const = 0;

            virtual
            void
            getSelfBounds(xchg::BoundingBox&) const = 0;
        };
    }
}
