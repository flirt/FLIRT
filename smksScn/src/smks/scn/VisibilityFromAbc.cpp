// ========================================================================= //
// Copyright 2016-2018 SUPAMONKS_STUDIO                                      //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
//                                                                           //
// ========================================================================= //
#include <smks/sys/Exception.hpp>
#include <smks/parm/BuiltIn.hpp>
#include <smks/parm/BuiltIns.hpp>
#include <smks/xchg/ParmEvent.hpp>

#include "smks/scn/SchemaBasedSceneObject.hpp"
#include "VisibilityFromAbc.hpp"

using namespace smks;

scn::VisibilityFromAbc::VisibilityFromAbc(SchemaBasedSceneObject&           node,
                                          const Alembic::AbcGeom::IObject&  object,
                                          TreeNode::WPtr const&             archive_):
    _node               (node),
    _object             (object.valid() ? object.getPtr() : nullptr),
    _archive            (archive_),
    //-------------------
    _visibility         (),
    _initialized        (false),
    _constantVisibility (-1),
    _currentSampleId    (-1)
{
    if (_object == nullptr)
        throw sys::Exception("Incorrect Alembic object parameter.", "Alembic Visibility Constructor");

    scn::TreeNode::Ptr const& archive = _archive.lock();
    if (archive &&
        !archive->asArchive())
        throw sys::Exception("Incorrect archive parameter.", "Alembic Visibility Constructor");
}

void
scn::VisibilityFromAbc::uninitialize()
{
    _node._unsetParameter(parm::currentlyVisible(), parm::SKIPS_RESTRAINTS);

    _visibility.reset();
    _initialized        = false;
    _constantVisibility = -1;
    _currentSampleId    = -1;
}

void
scn::VisibilityFromAbc::initialize()
{
    if (_initialized)
        return;

    assert(_object);

    uninitialize();     // _constantVisibility  = -1;

    if (_object->getProperties() &&
        _object->getProperties()->getPropertyHeader(Alembic::AbcGeom::kVisibilityPropertyName))
    {
        _visibility = Alembic::AbcGeom::IVisibilityProperty(
            _object->getProperties(),
            Alembic::AbcGeom::kVisibilityPropertyName);

        assert(_visibility.valid());
        assert(_visibility.getDataType().getPod() == Alembic::Abc::kInt8POD); // FIXME: will it always be the case?
    }
    _initialized = true;
    //---
    char forciblyVisible = -1;

    _node.getParameter<char>(parm::forciblyVisible(), forciblyVisible);
    setCurrentlyVisible(forciblyVisible);
}

bool
scn::VisibilityFromAbc::getTimeBounds(float& minTime, float& maxTime) const
{
    assert(_initialized);

    minTime = FLT_MAX;
    maxTime = -FLT_MAX;

    if (_constantVisibility >= 0)
        return false;

    Alembic::Abc::TimeSamplingPtr const&    sampling    = _visibility.getTimeSampling();
    const size_t                            numSamples  = _visibility.getNumSamples();

    if (sampling &&
        numSamples > 1)
    {
        minTime = static_cast<float>(sampling->getSampleTime(0));
        maxTime = static_cast<float>(sampling->getSampleTime(numSamples-1));
    }

    return minTime < maxTime;
}

void
scn::VisibilityFromAbc::setForciblyVisible(char value)
{
    setCurrentlyVisible(value);
}

char
scn::VisibilityFromAbc::getConstantlyVisible() const    // returns 1 if visible, 0 if invisible or -1 if animated
{
    if (!_initialized)
        return 0;
    if (!_visibility.valid())
        return 1;
    if (!_visibility.isConstant())
        return -1;
    return _visibility.getValue() == Alembic::AbcGeom::kVisibilityHidden
        ? 0
        : 1;
}

void
scn::VisibilityFromAbc::setCurrentlyVisible(char forciblyVisible)
{
    // override or naturally determine constant visibility
    if (!_initialized)
        _constantVisibility = 0;
    else if (forciblyVisible < 0)
        _constantVisibility = getConstantlyVisible();
    else
        _constantVisibility = forciblyVisible == 0 ? 0 : 1;

    //------------
    float currentTime = 0.0f;

    if (_constantVisibility < 0)
        // only when visibility is not constant do we need actual current time
        _node.getParameter<float>(parm::currentTime(), currentTime);
    setCurrentTime(currentTime);    // actually update node's parameter
}

bool
scn::VisibilityFromAbc::setCurrentTime(float time)
{
    if (_constantVisibility >= 0)
        _node.setParameter<char>(parm::currentlyVisible(), _constantVisibility, parm::SKIPS_RESTRAINTS);

    else if (_initialized)
    {
        assert(_visibility.valid());
        assert(!_visibility.isConstant());

        const int prevSampleId = _currentSampleId;

        const Alembic::Abc::ISampleSelector selector(
            static_cast<Alembic::Abc::chrono_t>(time),
            Alembic::Abc::ISampleSelector::kNearIndex
        );

        _currentSampleId    = static_cast<int>(selector.getIndex(
            _visibility.getTimeSampling(),
            _visibility.getNumSamples())
        );

        if (_currentSampleId != prevSampleId)
        {
            const char visible = _visibility.getValue(selector) == Alembic::AbcGeom::kVisibilityHidden ? 0 : 1;
            _node.setParameter<char>(parm::currentlyVisible(), visible, parm::SKIPS_RESTRAINTS);
        }
    }

    char currentlyVisible = 0;
    _node.getParameter<char>(parm::currentlyVisible(), currentlyVisible, parm::SKIPS_RESTRAINTS);

    return currentlyVisible > 0;
}

bool
scn::VisibilityFromAbc::isParameterRelevantForClass(unsigned int parmId) const
{
    return parmId == parm::currentlyVisible().id();
}

char
scn::VisibilityFromAbc::isParameterWritable(unsigned int parmId) const
{
    if (isParameterRelevantForClass(parmId))
        return 0;
    return -1;
}

bool
scn::VisibilityFromAbc::overrideParameterDefaultValue(unsigned int parmId, parm::Any& defaultValue) const
{
    if (parmId == parm::currentlyVisible().id())
    {
        // no parameter -> node is invisible
        defaultValue = parm::Any(static_cast<char>(0));
        return true;
    }
    return false;
}
