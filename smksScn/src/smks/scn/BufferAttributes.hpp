// ========================================================================= //
// Copyright 2016-2018 SUPAMONKS_STUDIO                                      //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
//                                                                           //
// ========================================================================= //
#pragma once

#include <stdint.h>

#include "smks/sys/configure_scn.hpp"

namespace smks
{
    namespace scn
    {
        ////////////////
        // enum Property
        ////////////////
        enum Property
        {
            PROP_BOUNDS         = 1 << 0,
            PROP_POSITION       = 1 << 1,
            PROP_NORMAL         = 1 << 2,
            PROP_TEXCOORD       = 1 << 3,
            PROP_VELOCITY       = 1 << 4,
            PROP_WIDTH          = 1 << 5,
            PROP_SCALP_NORMAL   = 1 << 6,
            PROP_SCALP_TEXCOORD = 1 << 7
        };

        typedef uint32_t PropertyFlags;

        /////////////////////////
        // class BufferAttributes
        /////////////////////////
        class FLIRT_SCN_EXPORT BufferAttributes
        {
        private:
            class PImpl;
            struct Declaration;

        public:
            PImpl*  _pImpl;

        public:
            BufferAttributes();
            BufferAttributes(const BufferAttributes&);
            ~BufferAttributes();

            BufferAttributes&
            operator=(const BufferAttributes&);

            void
            clear();

            void
            append(Property, size_t numFloats);

            size_t
            stride() const;

            bool
            empty() const;

            bool
            has(Property) const;

            uint32_t
            flags() const;

            size_t
            offset(Property) const;

            size_t
            numFloats(Property) const;
        };
    }
}
