// ========================================================================= //
// Copyright 2016-2018 SUPAMONKS_STUDIO                                      //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
//                                                                           //
// ========================================================================= //
#pragma once

#include <string>
#include <memory>
#include <smks/xchg/ParmType.hpp>

#include <Alembic/Abc/IScalarProperty.h>
#include <Alembic/Abc/IArrayProperty.h>
#include <Alembic/Abc/ICompoundProperty.h>
#include <Alembic/AbcCoreAbstract/TimeSampling.h>

namespace smks { namespace scn
{
    class TreeNode;

    ///////////////////////////
    // class UserAttribParmBase
    ///////////////////////////
    class UserAttribParmBase
    {
    public:
        typedef std::shared_ptr<UserAttribParmBase> Ptr;
    private:
        typedef Alembic::AbcCoreAbstract::ALEMBIC_VERSION_NS::DataType DataType;
    public:
        struct SampleReader;
    protected:
        const std::string   _parmName;
        const unsigned int  _parmId;
        // determined by setup() method
        xchg::ParmType      _parmType;
        SampleReader*       _sampleReader;
    protected:
        UserAttribParmBase(const std::string&, const DataType&);

        void
        setup(const DataType&);
    private:
        // non-copyable
        UserAttribParmBase(const UserAttribParmBase&);
        UserAttribParmBase& operator=(const UserAttribParmBase&);
    public:
        virtual
        ~UserAttribParmBase();

        inline
        const std::string&
        name() const
        {
            return _parmName;
        }
        inline
        unsigned int
        id() const
        {
            return _parmId;
        }
        inline
        xchg::ParmType
        type() const
        {
            return _parmType;
        }

        virtual
        bool
        valid() const;

        virtual
        void
        setCurrentTime(TreeNode&, float) = 0;
        virtual
        bool
        getTimeBounds(float&, float&) const = 0;
    protected:
        static
        xchg::ParmType
        getParmType(const DataType&);

        int
        getSampleIndex(float, Alembic::Abc::TimeSamplingPtr const&, size_t);
        void
        setCurrentParmValue(TreeNode&, const void* rawData);
    };

    /////////////////////////////
    // class ScalarUserAttribParm
    /////////////////////////////
    class ScalarUserAttribParm:
        public UserAttribParmBase
    {
    public:
        typedef std::shared_ptr<ScalarUserAttribParm> Ptr;
    private:
        typedef Alembic::Abc::ALEMBIC_VERSION_NS::IScalarProperty IScalarProperty;
    private:
        IScalarProperty _prop;
        size_t          _sampleIdx;
    public:
        static inline
        Ptr
        create(const std::string& parmName, const Alembic::Abc::IScalarProperty& prop)
        {
            Ptr ptr(new ScalarUserAttribParm(parmName, prop));
            return ptr;
        }
    protected:
        ScalarUserAttribParm(const std::string&, const Alembic::Abc::IScalarProperty&);

        bool
        valid() const;
        void
        setCurrentTime(TreeNode&, float);
        bool
        getTimeBounds(float&, float&) const;
    };

    ////////////////////////////
    // class ArrayUserAttribParm
    ////////////////////////////
    class ArrayUserAttribParm:
        public UserAttribParmBase
    {
    public:
        typedef std::shared_ptr<ArrayUserAttribParm> Ptr;
    private:
        typedef Alembic::Abc::ALEMBIC_VERSION_NS::IArrayProperty IArrayProperty;
    private:
        IArrayProperty  _prop;
        size_t          _sampleIdx;
    public:
        static inline
        Ptr
        create(const std::string& parmName, const Alembic::Abc::IArrayProperty& prop)
        {
            Ptr ptr(new ArrayUserAttribParm(parmName, prop));
            return ptr;
        }
    protected:
        ArrayUserAttribParm(const std::string&, const Alembic::Abc::IArrayProperty&);

        bool
        valid() const;
        void
        setCurrentTime(TreeNode&, float);
        bool
        getTimeBounds(float&, float&) const;
    };
}
}
