// ========================================================================= //
// Copyright 2016-2018 SUPAMONKS_STUDIO                                      //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
//                                                                           //
// ========================================================================= //
#pragma once

#include "PolyMesh-AbstractGeometry.hpp"
#include "BufferAttributes.hpp"
#include "../util/data/ArrayCache.hpp"

namespace smks
{
    namespace scn
    {
        ///////////////////
        // struct Triangles
        ///////////////////
        struct Triangles
        {
            std::vector<unsigned int>   indices;
            std::vector<unsigned int>   vtxRedir;   // face vertex index -> ABC vertex index (for vertex-varying properties)
            std::vector<unsigned int>   fvrRedir;   // face vertex index -> ABC face vertex index (for face-varying properties)

        public:
            inline
            Triangles():
                indices (),
                vtxRedir(),
                fvrRedir()
            { }

            inline
            Triangles(const std::vector<unsigned int>& indices,
                      const std::vector<unsigned int>& vtxRedir,
                      const std::vector<unsigned int>& fvrRedir):
                indices (indices),
                vtxRedir(vtxRedir),
                fvrRedir(fvrRedir)
            { }
        };
    }

    namespace util
    {
        namespace data
        {
            ///////////////////////////////
            // struct DirtyEntry<Triangles>
            ///////////////////////////////
            template<>
            struct DirtyEntry<scn::Triangles>:
                public std::unary_function<scn::Triangles&, void>
            {
                virtual inline
                void
                operator()(scn::Triangles& x) const
                {
                    x.indices   .clear();
                    x.indices   .shrink_to_fit();
                    x.vtxRedir  .clear();
                    x.vtxRedir  .shrink_to_fit();
                    x.fvrRedir  .clear();
                    x.fvrRedir  .shrink_to_fit();
                }
            };

            //////////////////////////////////////
            // struct IsCacheEntryDirty<Triangles>
            //////////////////////////////////////
            template<>
            struct IsCacheEntryDirty<scn::Triangles>:
                public std::unary_function<scn::Triangles const&, bool>
            {
                virtual inline
                bool
                operator()(const scn::Triangles& x) const
                {
                    return x.indices.empty();
                }
            };
        }
    }


    namespace scn
    {
        class PolyMesh::FacetedCoarseTriGeometry:
            public PolyMesh::AbstractGeometry
        {
        public:
            typedef std::shared_ptr<FacetedCoarseTriGeometry>       Ptr;
            typedef std::shared_ptr<const FacetedCoarseTriGeometry> CPtr;

        private:
            static const unsigned int                           _FACE_COUNT = 3;
        private:
            const AbstractPolyMeshSchema*                       _schema; // owned by polymesh

            BufferAttributes                                    _faceVertexAttribs;
            util::data::ArrayCache<Triangles>                   _triangles;
            util::data::ArrayCache<std::vector<unsigned int>>   _wireIndices;
            util::data::ArrayCache<std::vector<float>>          _faceVertices;

        public:
            static
            Ptr
            create();

        private:
            FacetedCoarseTriGeometry();

        public:
            ~FacetedCoarseTriGeometry();

            void
            dispose();

            bool
            initialize(const AbstractPolyMeshSchema*, bool caching);

            inline
            void
            setSubdivisionLevel(unsigned int)
            { }

            inline
            const float*
            getVertices(size_t&             numVertices,
                        BufferAttributes&   attribs)
            {
                return getFaceVertices(numVertices, attribs);
            }

            const float*
            getFaceVertices(size_t&, BufferAttributes&);

            const unsigned int*
            getFaceIndices(size_t&);

            const unsigned int*
            getFaceCounts(size_t&, bool&);

            const unsigned int*
            getWireframeLines(size_t&);

            const unsigned int*
            getFaceVertexToVertexMapping(size_t& size)
            {
                size = 0;
                return nullptr; // no mapping
            }

        private:
            bool
            initialized() const;

            const Triangles*
            getTriangles();
        };
    }
}
