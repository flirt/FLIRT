// ========================================================================= //
// Copyright 2016-2018 SUPAMONKS_STUDIO                                      //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
//                                                                           //
// ========================================================================= //
#pragma once

#include <memory>
#include <smks/xchg/DAGHierarchy.hpp>
#include <smks/scn/TreeNode.hpp>

namespace smks { namespace scn
{
    template <typename ValueType>
    class DAGHierarchy:
        public xchg::DAGHierarchy<ValueType>
    {
    public:
        typedef std::shared_ptr<DAGHierarchy<ValueType>>                        Ptr;
    protected:
        typedef std::shared_ptr<xchg::DAGLayout::AbstractTypedData<ValueType>>  DataPtr;
    public:
        explicit inline
        DAGHierarchy(DataPtr const& data):
            xchg::DAGHierarchy<ValueType>(data)
        { }

        virtual inline
        ~DAGHierarchy()
        { }

        virtual inline
        xchg::NodeClass
        nodeClass() const = 0;

        inline
        bool
        accept(const scn::TreeNode& n) const
        {
            return (nodeClass() & n.nodeClass()) != 0 &&
                (~nodeClass() & n.nodeClass()) == 0;
        }
    };
}
}
