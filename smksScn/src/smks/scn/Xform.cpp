// ========================================================================= //
// Copyright 2016-2018 SUPAMONKS_STUDIO                                      //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
//                                                                           //
// ========================================================================= //

#include <Alembic/Abc/IObject.h>

#include <smks/sys/Log.hpp>
#include <smks/sys/Exception.hpp>
#include <smks/xchg/SceneEvent.hpp>
#include <smks/xchg/ParmEvent.hpp>
#include <smks/parm/BuiltIn.hpp>
#include <smks/parm/BuiltIns.hpp>

#include "smks/scn/Xform.hpp"
#include "smks/scn/Scene.hpp"
#include "XformParmBasedSchema.hpp"
#include "XformSchemaFromAbc.hpp"
#include "AbstractObjectDataHolder.hpp"

#include "../util/data/ArrayCache.hpp"
#include "../util/data/DirtyBoolean.hpp"
#include "../util/data/IsBooleanDirty.hpp"
#include "../util/math/DirtyXform.hpp"
#include "../util/math/IsXformDirty.hpp"

namespace smks { namespace scn
{
    //////////////////////////
    // class Xform::DataHolder
    //////////////////////////
    class Xform::DataHolder:
        public AbstractObjectDataHolder
    {
    private:
        typedef util::data::ArrayCache<math::Affine3f, util::math::IsXformDirty, util::math::DirtyXform>    XFormCache;
        typedef util::data::ArrayCache<char, util::data::IsBooleanDirty, util::data::DirtyBoolean>          BooleanCache;
    private:
        AbstractXformSchema*    _schema; // does not own it
        XFormCache              _xform;
        BooleanCache            _inherits;

    public:
        inline
        DataHolder():
            _schema     (nullptr),
            _xform      (),
            _inherits   ()
        { }

        inline
        void
        build(AbstractObjectSchema*);

        inline
        void
        initialize();

        inline
        void
        uninitialize();

        inline
        void
        synchronizeWithSchema();
    private:
        inline
        void
        getTransform(math::Affine3f&, bool&);

    public:
        inline
        void
        dirtyCacheEntry(size_t idx, bool synchronize);

    private:
        inline
        void
        unsetAllParameters();

        inline
        bool
        areParametersUserDefined() const;
    };
}
}

using namespace smks;

////////////////////
// class Xform::DataHolder
////////////////////
void
scn::Xform::DataHolder::build(AbstractObjectSchema* schema)
{
    _schema = schema ? schema->asXformSchema() : nullptr;
    assert(_schema);

    //if (areParametersUserDefined())
    //{
    //  SchemaBasedSceneObject::Ptr const& node = _schema->node().lock();
    //  assert(node);

    //  AbstractObjectDataHolder::initialize(*node); // immediately forces node initialization
    //}
}

void
scn::Xform::DataHolder::uninitialize()
{
    unsetAllParameters();

    _xform.dirtyAll();
    _xform.dispose();

    _inherits.dirtyAll();
    _inherits.dispose();
}

void
scn::Xform::DataHolder::initialize()
{
    uninitialize();
    assert(_schema);
    if (_schema->numSamples() > 0)  // schema has been initialized
    {
        // get general scene configuration
        TreeNode::Ptr const& root        = _schema->node().root().lock();
        const Scene::Config& cfg         = root && root->asScene() ? root->asScene()->config() : Scene::Config();
        const size_t         numSamples  = cfg.cacheAnim() ? _schema->numSamples() : 0;

        _xform.initialize(numSamples);
        _xform.dirtyAll();

        _inherits.initialize(numSamples);
        _inherits.dirtyAll();
    }
}

bool
scn::Xform::DataHolder::areParametersUserDefined() const
{
    assert(_schema);
    return _schema->isParameterWritable(parm::transform().id()) &&
        _schema->isParameterWritable(parm::inheritsTransforms().id());
}

void
scn::Xform::DataHolder::unsetAllParameters()
{
    assert(_schema);
    unsetParameter(_schema->node(), parm::transform(),          parm::SKIPS_RESTRAINTS);
    unsetParameter(_schema->node(), parm::inheritsTransforms(), parm::SKIPS_RESTRAINTS);
}

void
scn::Xform::DataHolder::synchronizeWithSchema()
{
    assert(_schema);
    if (_schema->numSamples() > 0)  // schema has been initialized
    {
        SchemaBasedSceneObject& node = _schema->node();

        math::Affine3f  curTransform    (math::Affine3f::Identity());
        math::Affine3f  nxtTransform    (math::Affine3f::Identity());
        bool            curInherits     = true;
        bool            nxtInherits     = true;

        node.getParameter<math::Affine3f>   (parm::transform(),             curTransform);
        node.getParameter<bool>             (parm::inheritsTransforms(),    curInherits);

        getTransform(nxtTransform, nxtInherits);

        // update node's parameters if needed
        if (!xchg::equal<math::Affine3f>(nxtTransform, curTransform))
            setParameter<math::Affine3f>(node, parm::transform(), nxtTransform, parm::SKIPS_RESTRAINTS);
        if (nxtInherits != curInherits)
            setParameter<bool>(node, parm::inheritsTransforms(), nxtInherits, parm::SKIPS_RESTRAINTS);
    }
    else
        unsetAllParameters();
}

void
scn::Xform::DataHolder::getTransform(math::Affine3f& xfm, bool& inheritsXforms)
{
    assert(_schema && _schema->numSamples() > 0);

    const int               sampleIdx       = _schema->currentSampleIdx();
    const math::Affine3f*   cachedXform     = _xform.get(sampleIdx);
    const char*             cachedInherits  = _inherits.get(sampleIdx);

    if (cachedXform)
        xfm = *cachedXform;
    if (cachedInherits)
        inheritsXforms = (*cachedInherits) != 0;

    if (cachedXform && cachedInherits)
        return;

    // actual computation
    _schema->synchronizeTransform(xfm, inheritsXforms);
    // cache update
    xfm = *_xform.set(sampleIdx, xfm);
    inheritsXforms = (*_inherits.set(sampleIdx, inheritsXforms ? 1 : 0)) != 0;
}

void
scn::Xform::DataHolder::dirtyCacheEntry(size_t idx, bool synchronize)
{
    _xform.dirty(idx);
    _inherits.dirty(idx);

    if (synchronize)
        synchronizeWithSchema();
}

//////////////
// class Xform
//////////////
// static
scn::Xform::Ptr
scn::Xform::create(const char*              name,
                   TreeNode::WPtr const&    parent)
{
    Ptr ptr (new Xform(name, parent));
    ptr->build(
        ptr,
        new XformParmBasedSchema(*ptr),
        new Xform::DataHolder());
    return ptr;
}

// static
scn::Xform::Ptr
scn::Xform::create(const Alembic::Abc::IObject& object,
                   TreeNode::WPtr const&        parent,
                   TreeNode::WPtr const&        archive)
{
    Ptr ptr (new Xform(object, parent, archive));
    ptr->build(
        ptr,
        new XformSchemaFromAbc(*ptr, object, archive),
        new Xform::DataHolder());
    return ptr;
}

scn::Xform::Xform(const char*           name,
                  TreeNode::WPtr const& parent):
    SchemaBasedSceneObject(name, parent)
{ }

scn::Xform::Xform(const Alembic::Abc::IObject&  object,
                  TreeNode::WPtr const&         parent,
                  TreeNode::WPtr const&         archive):
    SchemaBasedSceneObject(object.getName().c_str(), parent)
{ }

//-------------------
// parameter handling
//-------------------
// virtual
bool
scn::Xform::mustSendToScene(const xchg::ParmEvent& evt) const
{
    return evt.parm() == parm::transform().id() ||
        evt.parm() == parm::inheritsTransforms().id() ||
        SchemaBasedSceneObject::mustSendToScene(evt);
}
//-------------------
