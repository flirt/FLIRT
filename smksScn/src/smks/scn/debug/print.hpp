// ========================================================================= //
// Copyright 2016-2018 SUPAMONKS_STUDIO                                      //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
//                                                                           //
// ========================================================================= //
#pragma once

#include <iostream>

#include <Alembic/Abc/All.h>
#include <Alembic/AbcGeom/All.h>

#include "smks/scn/TreeNode.hpp"

namespace smks { namespace scn { namespace debug
{
    std::ostream&
    print(std::ostream&, const Alembic::Abc::IObject&, size_t);

    std::ostream&
    print(std::ostream&, const Alembic::Abc::ICompoundProperty&, size_t);

    std::ostream&
    print(std::ostream&, const TreeNode*, size_t);

    std::ostream&
    printChildren(std::ostream&, const Alembic::Abc::IObject&);

    std::string
    getIObjectTypeName(const Alembic::Abc::IObject&);
}
}
}


inline
std::ostream&
smks::scn::debug::print(std::ostream&               out,
                       const Alembic::Abc::IObject& n,
                       size_t                       depth)
{
    out << "\n";
    for (unsigned int d = 0; d < depth; ++d)
        out << "*";
    out << " " << n.getFullName() << " " << std::endl;

    if (Alembic::AbcGeom::IXformSchema::matches(n.getMetaData()))
        out << "[Xform]";
    if (Alembic::AbcGeom::IPointsSchema::matches(n.getMetaData()))
        out << "[Points]";
    if (Alembic::AbcGeom::ICurvesSchema::matches(n.getMetaData()))
        out << "[Curves]";
    if (Alembic::AbcGeom::IPolyMeshSchema::matches(n.getMetaData()))
        out << "[PolyMesh]";
    if (Alembic::AbcGeom::ISubD::matches(n.getMetaData()))
        out << "[SubD]";
    if (Alembic::AbcGeom::ICameraSchema::matches(n.getMetaData()))
        out << "[Camera]";
    out << std::endl;

    print(out, n.getProperties(), depth);

    for (size_t i = 0; i < n.getNumChildren(); ++i)
        print(out, n.getChild(i), depth+1);

    return out;
}

inline
std::ostream&
smks::scn::debug::print(std::ostream&                           out,
                        const Alembic::Abc::ICompoundProperty&  props,
                        size_t                                  depth)
{
    if (!props.getName().empty())
    {
        for (unsigned int d = 0; d < depth; ++d)
            out << "-";
        out << "C: '" << props.getName() << "'" << std::endl;
    }

    for (size_t i = 0; i < props.getNumProperties(); ++i)
    {
        const Alembic::Abc::PropertyHeader& header = props.getPropertyHeader(i);
        if (header.isArray())
        {
            for (unsigned int d = 0; d <= depth; ++d)
                out << "-";
            out << "a: '" << header.getName() << "'\t"
                << Alembic::Abc::IArrayProperty(props, header.getName()).getNumSamples() << " samples"
                << std::endl;
        }
        else if (header.isScalar())
        {
            for (unsigned int d = 0; d <= depth; ++d)
                out << "-";
            out << "s: '" << header.getName() << "'\t"
                << Alembic::Abc::IScalarProperty(props, header.getName()).getNumSamples() << " samples"
                << std::endl;
        }
        else
            print(out, Alembic::Abc::ICompoundProperty(props, header.getName()), depth + 1);
    }

    return out;
}

inline
std::ostream&
smks::scn::debug::print(std::ostream&   out,
                        const TreeNode* n,
                        size_t          depth)
{
    if (n)
    {
        //out << "\n";
        for (unsigned int d = 0; d < depth; ++d)
            out << "*";
        out << " " << n->name() << "\t(" << n->id() << ")";

        if (n->asScene())
            out << "\t[Scene]";
        else if (n->asArchive())
            out << "\t[Archive]";
        else if (n->asSceneObject())
        {
            const SceneObject* obj = n->asSceneObject();

            if (obj->asXform())
                out << "\t[Xform]";
            else if (obj->asCamera())
                out << "\t[Camera]";
            else if (obj->asDrawable())
            {
                const Drawable* drw = obj->asDrawable();

                if (drw->asPoints())
                    out << "\t[Points]";
                else if (drw->asCurves())
                    out << "\t[Curves]";
                else if (drw->asPolyMesh())
                    out << "\t[Mesh]";
                else
                    out << "\t[?Drawable?]";
            }
            else
                out << "\t[?SceneObject?]";
        }
        else
            out << "\t[?TreeNode?]";

        out << std::endl;

        for (unsigned int i = 0; i < n->numChildren(); ++i)
            print(out, n->child(i).get(), depth + 1);
    }
    return out;
}

inline
std::ostream&
smks::scn::debug::printChildren(std::ostream&                   out,
                                const Alembic::Abc::IObject&    obj)
{
    for (size_t i = 0; i < obj.getNumChildren(); ++i)
    {
        const Alembic::Abc::IObject& child = obj.getChild(i);

        out << "\t- " << getIObjectTypeName(child) << ":\t'" << child.getFullName() << "' \n";
    }
    return out;
}

inline
std::string
smks::scn::debug::getIObjectTypeName(const Alembic::Abc::IObject& obj)
{
    if (Alembic::AbcGeom::ICamera::matches(obj.getMetaData()))
        return "Camera";
    else if (Alembic::AbcGeom::ICurves::matches(obj.getMetaData()))
        return "Curves";
    else if (Alembic::AbcGeom::IFaceSet::matches(obj.getMetaData()))
        return "FaceSet";
    else if (Alembic::AbcGeom::ILight::matches(obj.getMetaData()))
        return "Light";
    else if (Alembic::AbcGeom::INuPatch::matches(obj.getMetaData()))
        return "NuPatch";
    else if (Alembic::AbcGeom::IPoints::matches(obj.getMetaData()))
        return "Points";
    else if (Alembic::AbcGeom::IPolyMesh::matches(obj.getMetaData()))
        return "PolyMesh";
    else if (Alembic::AbcGeom::ISubD::matches(obj.getMetaData()))
        return "SubD";
    else if (Alembic::AbcGeom::IXform::matches(obj.getMetaData()))
        return "Xform";
    else
        return "Unknown";
}
