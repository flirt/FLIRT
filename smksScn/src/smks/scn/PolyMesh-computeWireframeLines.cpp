// ========================================================================= //
// Copyright 2016-2018 SUPAMONKS_STUDIO                                      //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
//                                                                           //
// ========================================================================= //
#include "smks/scn/PolyMesh.hpp"
#include "PolyMesh-AbstractGeometry.hpp"

using namespace smks;

// static
void
scn::PolyMesh::computeWireframeLines(AbstractGeometry&  geom,
                                     std::vector<unsigned int>& out)
{
    out.clear();

    size_t  numFaces        = 0;
    size_t  numIndices      = 0;
    bool    isCountConstant = false;

    const unsigned int* counts  = geom.getFaceCounts    (numFaces, isCountConstant);
    const unsigned int* indices = geom.getFaceIndices   (numIndices);

    if (numFaces    == 0 ||
        counts      == nullptr ||
        indices     == nullptr)
        return;

    // get an estimate of the number of edges
    size_t  numEdges    = 0;
    size_t  count       = counts[0];
    if (isCountConstant)
        numEdges = numFaces * count;
    else
        for (size_t f = 0; f < numFaces; ++f)
            numEdges += counts[f];

    if (numEdges == 0)
        return;

    // fill index buffer
    out.resize(numEdges << 1, 0);

    size_t          offset  = 0;
    unsigned int*   oIndex  = &out.front();
    for (size_t f = 0; f < numFaces; ++f)
    {
        if (!isCountConstant)
            count = counts[f];

        for (size_t i = 0; i < count; ++i)
        {
            oIndex[0]   = indices[offset + i];
            oIndex[1]   = indices[offset + ((i+1) % count)];
            oIndex      += 2;
        }

        offset += count;
    }
}
