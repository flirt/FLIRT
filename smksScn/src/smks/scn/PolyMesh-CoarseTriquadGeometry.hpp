// ========================================================================= //
// Copyright 2016-2018 SUPAMONKS_STUDIO                                      //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
//                                                                           //
// ========================================================================= //
#pragma once

#include "PolyMesh-AbstractGeometry.hpp"
#include "BufferAttributes.hpp"
#include "../util/data/ArrayCache.hpp"

namespace smks
{
    namespace scn
    {
        ///////////////////////////
        // struct TrianglesAndQuads
        ///////////////////////////
        struct TrianglesAndQuads
        {
            std::vector<unsigned int>   counts;
            std::vector<unsigned int>   indices;
            std::vector<unsigned int>   faceVertexToVertex;

        public:
            inline
            TrianglesAndQuads():
                counts              (),
                indices             (),
                faceVertexToVertex  ()
            { }

            inline
            TrianglesAndQuads(const std::vector<unsigned int>&  counts,
                              const std::vector<unsigned int>&  indices,
                              const std::vector<unsigned int>&  faceVertexToVertex):
                counts              (counts),
                indices             (indices),
                faceVertexToVertex  (faceVertexToVertex)
            { }
        };
    }

    namespace util
    {
        namespace data
        {
            ///////////////////////////////////////
            // struct DirtyEntry<TrianglesAndQuads>
            ///////////////////////////////////////
            template<>
            struct DirtyEntry<scn::TrianglesAndQuads>:
                public std::unary_function<scn::TrianglesAndQuads&, void>
            {
                virtual inline
                void
                operator()(scn::TrianglesAndQuads& x) const
                {
                    x.counts                .clear();
                    x.counts                .shrink_to_fit();
                    x.indices               .clear();
                    x.indices               .shrink_to_fit();
                    x.faceVertexToVertex    .clear();
                    x.faceVertexToVertex    .shrink_to_fit();
                }
            };

            //////////////////////////////////////////////
            // struct IsCacheEntryDirty<TrianglesAndQuads>
            //////////////////////////////////////////////
            template<>
            struct IsCacheEntryDirty<scn::TrianglesAndQuads>:
                public std::unary_function<scn::TrianglesAndQuads const&, bool>
            {
                virtual inline
                bool
                operator()(const scn::TrianglesAndQuads& x) const
                {
                    return x.counts.empty();
                }
            };
        }
    }

    namespace scn
    {
        class PolyMesh::CoarseTriquadGeometry:
            public PolyMesh::AbstractGeometry
        {
        public:
            typedef std::shared_ptr<CoarseTriquadGeometry>          Ptr;
            typedef std::shared_ptr<const CoarseTriquadGeometry>    CPtr;

        private:
            const AbstractPolyMeshSchema*                       _schema; // owned by polymesh

            BufferAttributes                                    _vertexAttribs;
            BufferAttributes                                    _faceVertexAttribs;
            util::data::ArrayCache<TrianglesAndQuads>           _trisAndQuads;
            util::data::ArrayCache<std::vector<unsigned int>>   _wireIndices;
            util::data::ArrayCache<std::vector<float>>          _vertices;
            util::data::ArrayCache<std::vector<float>>          _faceVertices;

        public:
            static
            Ptr
            create();

        private:
            CoarseTriquadGeometry();

        public:
            ~CoarseTriquadGeometry();

            bool
            initialize(const AbstractPolyMeshSchema*, bool caching);

            void
            dispose();

            inline
            void
            setSubdivisionLevel(unsigned int)
            { }

            // will only contain the vertex positions
            const float*
            getVertices(size_t&, BufferAttributes&);

            const float*
            getFaceVertices(size_t&, BufferAttributes&);

            const unsigned int*
            getFaceCounts(size_t&, bool&);

            const unsigned int*
            getFaceIndices(size_t&);

            const unsigned int*
            getWireframeLines(size_t&);

        private:
            bool
            initialized() const;

            const TrianglesAndQuads*
            getTrianglesAndQuads();

            const unsigned int*
            getFaceVertexToVertexMapping(size_t&);
        };
    }
}
