// ========================================================================= //
// Copyright 2016-2018 SUPAMONKS_STUDIO                                      //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
//                                                                           //
// ========================================================================= //
#include <smks/sys/Exception.hpp>
#include <smks/parm/BuiltIns.hpp>
#include <smks/xchg/NodeCode.hpp>
#include <smks/xchg/NodeCodes.hpp>

#include "smks/scn/FrameBuffer.hpp"

using namespace smks;

// static
scn::FrameBuffer::Ptr
scn::FrameBuffer::create(const char*            code,
                         const char*            name,
                         TreeNode::WPtr const&  parent)
{
    if (code == nullptr ||
        strlen(code) == 0)
        throw sys::Exception("Invalid code.", "Frame Buffer Creation");

    Ptr ptr(new FrameBuffer(name, parent));
    ptr->build(ptr);

    size_t width;
    size_t height;

    ptr->getParameter<size_t>(parm::width(),    width);     // default values
    ptr->getParameter<size_t>(parm::height(),   height);    // default values

    ptr->setParameter<std::string>  (parm::code     (), code, parm::SKIPS_RESTRAINTS);
    ptr->setParameter<size_t>       (parm::width    (), width);
    ptr->setParameter<size_t>       (parm::height   (), height);

    return ptr;
}

// virtual
bool
scn::FrameBuffer::isParameterWritable(unsigned int parmId) const
{
    if (isParameterRelevantForClass(parmId))
        return parmId != parm::code().id();
    return RtNode::isParameterWritable(parmId);
}

// virtual
bool
scn::FrameBuffer::isParameterRelevant(unsigned int parmId)const
{
    return isParameterRelevantForClass(parmId) || RtNode::isParameterRelevant(parmId);
}

bool
scn::FrameBuffer::isParameterRelevantForClass(unsigned int parmId) const
{
    if (parmId == parm::code().id())
        return true;

    const unsigned int codeId = getCode();
    if (codeId == xchg::code::default().id())
    {
        return
            parmId == parm::width           ().id() ||
            parmId == parm::height          ().id() ||
            parmId == parm::frameData       ().id() ||
            parmId == parm::denoiserId      ().id() ||
            parmId == parm::toneMapperId    ().id() ||
            parmId == parm::renderPassIds   ().id() ||
            parmId == parm::storeRawRgba    ().id() ||
            parmId == parm::storeRgbaAsHalf ().id() ||
            parmId == parm::verbosityLevel  ().id();
    }
    return false;
}

// virtual
bool
scn::FrameBuffer::overrideParameterDefaultValue(unsigned int parmId, parm::Any& value) const
{
    if (parmId == parm::width().id())
    {
        size_t typedValue = 1920;
        value = parm::Any(typedValue);
        return true;
    }
    else if (parmId == parm::height().id())
    {
        size_t typedValue = 1080;
        value = parm::Any(typedValue);
        return true;
    }
    else if (parmId == parm::verbosityLevel().id())
    {
        char typedValue = 1;
        value = parm::Any(typedValue);
        return true;
    }
    return false;
}
