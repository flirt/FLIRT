// ========================================================================= //
// Copyright 2016-2018 SUPAMONKS_STUDIO                                      //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
//                                                                           //
// ========================================================================= //
#pragma once

#include <memory>
#include "smks/scn/PolyMesh.hpp"

namespace smks { namespace scn
{
    class PolyMesh::AbstractGeometry
    {
    public:
        typedef std::shared_ptr<AbstractGeometry> Ptr;

    public:
        virtual
        ~AbstractGeometry()
        { }

        virtual
        bool
        initialize(const AbstractPolyMeshSchema*, bool caching) = 0;

        virtual
        void
        dispose() = 0;

        virtual
        void
        setSubdivisionLevel(unsigned int) = 0;

        virtual
        const float*
        getVertices(size_t&, BufferAttributes&) = 0;

        virtual
        const float*
        getFaceVertices(size_t&, BufferAttributes&) = 0;

        virtual
        const unsigned int*
        getFaceIndices(size_t&) = 0;

        virtual
        const unsigned int*
        getFaceCounts(size_t&, bool& isConstant) = 0;

        virtual
        const unsigned int*
        getWireframeLines(size_t&) = 0;

        virtual
        const unsigned int*
        getFaceVertexToVertexMapping(size_t&) = 0;
    };
}
}
