// ========================================================================= //
// Copyright 2016-2018 SUPAMONKS_STUDIO                                      //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
//                                                                           //
// ========================================================================= //
#pragma once

#include <memory>
#include <smks/math/types.hpp>
#include <smks/xchg/DynamicBitset.hpp>
#include "DAGHierarchy.hpp"

namespace smks { namespace scn
{
    class TreeNode;

    ///////////////////////////
    // class TransformHierarchy
    ///////////////////////////
    class TransformHierarchy:
        public DAGHierarchy<math::Affine3f>
    {
    public:
        typedef std::shared_ptr<TransformHierarchy> Ptr;
    public:
        /////////////////////////////////
        // class TransformHierarchy::Data
        /////////////////////////////////
        class Data:
            public xchg::DAGLayout::AbstractTypedData<math::Affine3f>
        {
        public:
            typedef std::shared_ptr<Data> Ptr;
        private:
            math::Affine3fv     _local;
            math::Affine3fv     _global;
            xchg::DynamicBitset _inherits;
        public:
            EIGEN_MAKE_ALIGNED_OPERATOR_NEW

            static inline
            Ptr
            create()
            {
                Ptr ptr(new Data);
                return ptr;
            }

        protected:
            Data();

        public:
            bool
            inherits(size_t) const;

            void
            inherits(size_t, bool);

            void
            setLocal(size_t, const math::Affine3f&);

            const math::Affine3f&
            getGlobal(size_t) const;

            void
            resize(size_t);

            void
            moveEntry(size_t dst, size_t src);

            void
            recomputeEntry(size_t current);

        protected:
            void
            accumulateEntries(size_t current, size_t parent);
        };
        /////////////////////////////////
    public:
        static
        Ptr
        create()
        {
            Ptr ptr(new TransformHierarchy(Data::create()));
            return ptr;
        }

    private:
        explicit inline
        TransformHierarchy(Data::Ptr const& data):
            DAGHierarchy<math::Affine3f>(data)
        { }

    public:
        inline
        xchg::NodeClass
        nodeClass() const
        {
            return xchg::XFORM; // those nodes must redirect parm events related to 'transform' parameter to the scene
        }
    };

}
}
