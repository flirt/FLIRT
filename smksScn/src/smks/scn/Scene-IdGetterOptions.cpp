// ========================================================================= //
// Copyright 2016-2018 SUPAMONKS_STUDIO                                      //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
//                                                                           //
// ========================================================================= //
#include <cassert>
#include <string>
#include <vector>

#include "smks/scn/Scene.hpp"
#include "smks/scn/TreeNodeFilter.hpp"

namespace smks { namespace scn
{
    class Scene::IdGetterOptions::PImpl
    {
    private:
        unsigned int                _fromId;
        bool                        _upwards;
        AbstractTreeNodeFilter::Ptr _filter;
        std::vector<std::string>    _regexes;
        bool                        _accumulate;

    public:
        inline
        PImpl():
            _fromId     (0),
            _upwards    (false),
            _filter     (nullptr),
            _regexes    (),
            _accumulate (false)
        { }

        inline
        void
        fromId(unsigned int id)
        {
            _fromId = id;
        }

        inline
        unsigned int
        fromId() const
        {
            return _fromId;
        }

        inline
        void
        upwards(bool value)
        {
            _upwards = value;
        }

        inline
        bool
        upwards() const
        {
            return _upwards;
        }

        inline
        void
        filter(AbstractTreeNodeFilter::Ptr const& value)
        {
            _filter = value;
        }

        inline
        AbstractTreeNodeFilter::Ptr const&
        filter() const
        {
            return _filter;
        }

        inline
        void
        addRegex(const std::string& regex)
        {
            if (!regex.empty())
                _regexes.push_back(regex);
        }

        inline
        void
        removeRegex(size_t i)
        {
            if (i >= 0 && i < _regexes.size())
            {
                std::swap(_regexes[i], _regexes.back());
                _regexes.pop_back();
            }
        }

        inline
        size_t
        numRegexes() const
        {
            return _regexes.size();
        }

        inline
        const std::string&
        regex(size_t i) const
        {
            assert(i < _regexes.size());
            return _regexes[i];
        }

        inline
        void
        accumulate(bool value)
        {
            _accumulate = value;
        }

        inline
        bool
        accumulate() const
        {
            return _accumulate;
        }
    };
}
}

using namespace smks;

scn::Scene::IdGetterOptions::IdGetterOptions():
    _pImpl(new PImpl)
{ }

scn::Scene::IdGetterOptions::~IdGetterOptions()
{
    delete _pImpl;
}

scn::AbstractTreeNodeFilter::Ptr const&
scn::Scene::IdGetterOptions::filter() const
{
    return _pImpl->filter();
}

void
scn::Scene::IdGetterOptions::filter(scn::AbstractTreeNodeFilter::Ptr const& value)
{
    return _pImpl->filter(value);
}

void
scn::Scene::IdGetterOptions::fromId(unsigned int id)
{
    _pImpl->fromId(id);
}

unsigned int
scn::Scene::IdGetterOptions::fromId() const
{
    return _pImpl->fromId();
}

void
scn::Scene::IdGetterOptions::addRegex(const char* regex)
{
    if (regex)
        _pImpl->addRegex(std::string(regex));
}

void
scn::Scene::IdGetterOptions::removeRegex(size_t i)
{
    _pImpl->removeRegex(i);
}

size_t
scn::Scene::IdGetterOptions::numRegexes() const
{
    return _pImpl->numRegexes();
}

const char*
scn::Scene::IdGetterOptions::regex(size_t i) const
{
    return _pImpl->regex(i).c_str();
}

void
scn::Scene::IdGetterOptions::upwards(bool value)
{
    _pImpl->upwards(value);
}

bool
scn::Scene::IdGetterOptions::upwards() const
{
    return _pImpl->upwards();
}

void
scn::Scene::IdGetterOptions::accumulate(bool value)
{
    _pImpl->accumulate(value);
}

bool
scn::Scene::IdGetterOptions::accumulate() const
{
    return _pImpl->accumulate();
}
