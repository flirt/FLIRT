// ========================================================================= //
// Copyright 2016-2018 SUPAMONKS_STUDIO                                      //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
//                                                                           //
// ========================================================================= //

#include <Alembic/Abc/IObject.h>

#include <smks/sys/Log.hpp>

#include "smks/scn/Points.hpp"
#include "PointsSchemaFromAbc.hpp"
#include "Points-DataHolder.hpp"

using namespace smks;

// static
scn::Points::Ptr
scn::Points::create(const IObject&          object,
                    TreeNode::WPtr const&   parent,
                    TreeNode::WPtr const&   archive)
{
    Ptr ptr (new Points(object, parent, archive));
    ptr->build(
        ptr,
        new PointsSchemaFromAbc(*ptr, object, archive),
        new Points::DataHolder());
    return ptr;
}

scn::Points::Points(const IObject&          object,
                    TreeNode::WPtr const&   parent,
                    TreeNode::WPtr const&   archive):
    Drawable(object.getName().c_str(), parent)
{ }
