// ========================================================================= //
// Copyright 2016-2018 SUPAMONKS_STUDIO                                      //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
//                                                                           //
// ========================================================================= //
#pragma once

#include <memory>
#include <string>

namespace smks
{
    namespace xchg
    {
        class ParmEvent;
        class IdSet;
    }
    namespace parm
    {
        class BuiltIn;
        class Any;
    }
    namespace scn
    {
        class TreeNode;
        class AbstractCameraSchema;
        class AbstractCurvesSchema;
        class AbstractPointsSchema;
        class AbstractPolyMeshSchema;
        class AbstractXformSchema;

        class AbstractObjectSchema
        {
        public:
            virtual inline
            ~AbstractObjectSchema()
            { }

            virtual
            bool
            lazyInitialization() const = 0;

            virtual
            const SchemaBasedSceneObject&
            node() const = 0;

            virtual
            SchemaBasedSceneObject&
            node() = 0;

            virtual
            void
            uninitialize() = 0;

            virtual
            size_t  //<! return number of samples
            initialize() = 0;

            virtual
            void
            uninitializeUserAttribs() = 0;

            virtual
            void
            initializeUserAttribs() = 0;

            virtual
            bool //<! return current visibility, or false if not initialized
            setCurrentTime(float) = 0;

            virtual
            size_t
            numSamples() const = 0; //<! = 0 if not initialized

            virtual
            int
            currentSampleIdx() const = 0;   //<! -1 if not initialized

            virtual
            void
            getStoredTimes(xchg::Vector<float>&) const = 0;
        public:
            //----------------
            // dynamic casting
            //----------------
            virtual inline const AbstractCameraSchema*      asCameraSchema  ()  const   { return nullptr; }
            virtual inline const AbstractCurvesSchema*      asCurvesSchema  ()  const   { return nullptr; }
            virtual inline const AbstractPointsSchema*      asPointsSchema  ()  const   { return nullptr; }
            virtual inline const AbstractPolyMeshSchema*    asPolyMeshSchema()  const   { return nullptr; }
            virtual inline const AbstractXformSchema*       asXformSchema   ()  const   { return nullptr; }

            virtual inline AbstractCameraSchema*            asCameraSchema  ()          { return nullptr; }
            virtual inline AbstractCurvesSchema*            asCurvesSchema  ()          { return nullptr; }
            virtual inline AbstractPointsSchema*            asPointsSchema  ()          { return nullptr; }
            virtual inline AbstractPolyMeshSchema*          asPolyMeshSchema()          { return nullptr; }
            virtual inline AbstractXformSchema*             asXformSchema   ()          { return nullptr; }

            //-------------------
            // parameter handling
            //-------------------
            virtual inline
            bool
            isParameterRelevantForClass(unsigned int) const
            {
                return false;
            }
            virtual inline
            bool
            isParameterReadable(unsigned int) const
            {
                return true;
            }
            virtual inline
            char //<! returns -1 when cannot decide
            isParameterWritable(unsigned int) const
            {
                return -1;
            }
            virtual inline
            bool
            overrideParameterDefaultValue(unsigned int, parm::Any&) const
            {
                return false;
            }
            virtual inline
            bool
            preprocessParameterEdit(const char*, unsigned int, parm::ParmFlags, const parm::Any&)
            {
                return false;
            }

            virtual inline
            void
            handle(const xchg::ParmEvent&)
            { }
            virtual
            bool
            mustSendToScene(const xchg::ParmEvent&) const
            {
                return false;
            }


        protected:
            // inherited friendship
            void
            dirtySample(SchemaBasedSceneObject& node, int idx, bool synchronize) const
            {
                node.dirtySample(idx, synchronize);
            }

            // inherited friendship
            template <typename ValueType> inline
            unsigned int
            setParameter(SchemaBasedSceneObject& node, const parm::BuiltIn& parm, const ValueType& parmValue, parm::ParmFlags flags) const
            {
                return node._setParameter<ValueType>(parm.c_str(), parm.id(), &parm, parmValue, flags);
            }

            // inherited friendship
            inline
            void
            unsetParameter(SchemaBasedSceneObject& node, const parm::BuiltIn& parm, parm::ParmFlags flags) const
            {
                node._unsetParameter(parm.id(), &parm, flags);
            }
        };
    }
}
