// ========================================================================= //
// Copyright 2016-2018 SUPAMONKS_STUDIO                                      //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
//                                                                           //
// ========================================================================= //
#include <smks/sys/Log.hpp>
#include <smks/sys/Exception.hpp>
#include <smks/xchg/Vector.hpp>
#include <smks/xchg/ParmEvent.hpp>
#include <smks/parm/BuiltIns.hpp>

#include "smks/scn/Scene.hpp"
#include "smks/scn/SchemaBasedSceneObject.hpp"

#include "AbstractObjectSchema.hpp"
#include "AbstractObjectDataHolder.hpp"

namespace smks { namespace scn
{
    class SchemaBasedSceneObject::PImpl
    {
    private:
        SchemaBasedSceneObject::WPtr    _self;
        AbstractObjectSchema*           _schema;    // takes ownership
        AbstractObjectDataHolder*       _data;      // takes ownership
        float                           _cachedTime;
    public:
        inline
        PImpl();
        inline
        ~PImpl();

        inline AbstractObjectSchema*        schema()        const { return _schema; }
        inline AbstractObjectDataHolder*    dataHolder()    const { return _data; }

        inline
        void
        preBuild(SchemaBasedSceneObject::Ptr const&, AbstractObjectSchema*, AbstractObjectDataHolder*);
        inline
        void
        postBuild();
        inline
        void
        erase();

        inline
        void
        uninitialize(); // completely unloads content (undo initialize())
        inline
        void
        initialize();   // loads entirety of content from schema

        inline
        void
        uninitializeUserAttribs();  // completely unloads user attributes (undo initializeUserAttribs())
        inline
        void
        initializeUserAttribs();    // loads user attributes from schema

        inline
        void
        dirtySample(int idx, bool synchronize);

        inline
        bool
        isParameterRelevantForClass(unsigned int) const;
        inline
        bool
        isParameterReadable(unsigned int) const;
        inline
        char    //<! returns -1 when cannot decide
        isParameterWritable(unsigned int) const;
        inline
        bool
        overrideParameterDefaultValue(unsigned int, parm::Any&) const;
        inline
        bool
        preprocessParameterEdit(const char*, unsigned int, parm::ParmFlags, const parm::Any&);
        inline
        void
        handleParmEvent(const xchg::ParmEvent&);
        inline
        bool
        mustSendToScene(const xchg::ParmEvent&) const;

        inline
        void
        getStoredTimes(xchg::Vector<float>&) const;

    private:
        inline
        void
        handleCurrentTimeChanged(xchg::ParmEvent::Type, unsigned int containerId);

        //<! there is no need to explicitly set current time parameter to children.
        // the parameter update event is naturally propagated.
        inline
        void
        synchronizeWithTime(float);
        inline
        float
        getClosestAncestorCurrentTime() const;
    };
}
}

using namespace smks;

//////////////////////////////////////
// class SchemaBasedSceneObject::PImpl
//////////////////////////////////////
scn::SchemaBasedSceneObject::PImpl::PImpl():
    _self       (),
    _schema     (nullptr),
    _data       (nullptr),
    _cachedTime (-1.0f)
{ }

scn::SchemaBasedSceneObject::PImpl::~PImpl()
{
    if (_schema)
        delete _schema;
    if (_data)
        delete _data;
}

void
scn::SchemaBasedSceneObject::PImpl::preBuild(SchemaBasedSceneObject::Ptr const& self,
                                             AbstractObjectSchema*              schema, // takes ownership
                                             AbstractObjectDataHolder*          data)   // takes ownership
{
    erase();
    //---
    _self   = self;
    _schema = schema;
    _data   = data;
}

void
scn::SchemaBasedSceneObject::PImpl::postBuild()
{
    SchemaBasedSceneObject::Ptr const& self = std::dynamic_pointer_cast<SchemaBasedSceneObject>(_self.lock());

    if (!self)
        throw sys::Exception("Invalid node pointer.", "Schema-Based Scene Object Building");
    if (!_schema)
        throw sys::Exception("Invalid schema.", "Schema-Based Scene Object Building");
    if (!_data)
        throw sys::Exception("Invalid data holder.", "Schema-Based Scene Object Building");

    _data->build(_schema);
    //---
    if (!_schema->lazyInitialization())
        initialize();
}

void
scn::SchemaBasedSceneObject::PImpl::erase()
{
    TreeNode::Ptr const& self = _self.lock();
    if (self)
    {
        self->unsetParameter(parm::userAttribInitialization());
        self->unsetParameter(parm::initialization());
    }

    if (_schema)
        delete _schema;
    _schema = nullptr;

    if (_data)
        delete _data;
    _data = nullptr;

    _cachedTime = -1.0f;
    _self.reset();
}

void
scn::SchemaBasedSceneObject::PImpl::uninitialize()  // completely unloads content (undo initialize())
{
    if (!_schema)
        return;
    assert(_data);
    _data   ->uninitialize();
    _schema ->uninitialize();
    assert(_schema->numSamples() == 0);
}

void
scn::SchemaBasedSceneObject::PImpl::initialize()    // loads entirety of content from schema
{
    assert(_schema && _data);
    if (_schema->numSamples() > 0)
        return;

    _schema ->initialize();
    _data   ->initialize();
    //---
    _cachedTime = -1.0f;
    synchronizeWithTime(getClosestAncestorCurrentTime());
    assert(_schema->numSamples() > 0);
}

void
scn::SchemaBasedSceneObject::PImpl::uninitializeUserAttribs()   // completely unloads user attributes (undo initializeUserAttribs())
{
    if (!_schema)
        return;
    _schema->uninitializeUserAttribs();
}

void
scn::SchemaBasedSceneObject::PImpl::initializeUserAttribs() // loads user attributes from schema
{
    assert(_schema && _data);
    _schema->initializeUserAttribs();
    //---
    _cachedTime = -1.0f;
    synchronizeWithTime(getClosestAncestorCurrentTime());
}

void
scn::SchemaBasedSceneObject::PImpl::dirtySample(int idx, bool synchronize)
{
    assert(_data);
    _data->dirtyCacheEntry(idx, synchronize);
}

//-------------------
// parameter handling
//-------------------
bool
scn::SchemaBasedSceneObject::PImpl::isParameterRelevantForClass(unsigned int parmId) const
{
    if (parmId == parm::currentTime     ().id() ||
        parmId == parm::currentlyVisible().id() ||
        parmId == parm::currentSampleIdx().id() ||
        parmId == parm::numSamples      ().id() ||
        parmId == parm::timeBounds      ().id())
        return true;
    if (_schema)
        return _schema->isParameterRelevantForClass(parmId);
    return false;
}

bool
scn::SchemaBasedSceneObject::PImpl::isParameterReadable(unsigned int parmId) const
{
    return !_schema || _schema->isParameterReadable(parmId);
}

char
scn::SchemaBasedSceneObject::PImpl::isParameterWritable(unsigned int parmId) const
{
    char ret = -1;
    if (isParameterRelevantForClass(parmId))
        ret = _schema ? _schema->isParameterWritable(parmId) : ret;
    return ret;
}

bool
scn::SchemaBasedSceneObject::PImpl::overrideParameterDefaultValue(unsigned int parmId, parm::Any& defaultValue) const
{
    if (_schema && _schema->overrideParameterDefaultValue(parmId, defaultValue))
        return true;
    return false;
}

bool
scn::SchemaBasedSceneObject::PImpl::preprocessParameterEdit(const char*         parmName,
                                                            unsigned int        parmId,
                                                            parm::ParmFlags     f,
                                                            const parm::Any&    value)
{
    if (_schema)
        return _schema->preprocessParameterEdit(parmName, parmId, f, value);
    return false;
}

// tries to find relevant value by traversing ancestors upwards
float
scn::SchemaBasedSceneObject::PImpl::getClosestAncestorCurrentTime() const
{
    float currentTime = 0.0f;

    SchemaBasedSceneObject::Ptr const&  self    = _self.lock();
    TreeNode::Ptr const&                root    = self ? self->root().lock() : nullptr;
    if (self)
        self->getParameter<float>(parm::currentTime(), currentTime, root ? root->id() : self->id());
    return currentTime;
}

void
scn::SchemaBasedSceneObject::PImpl::handleParmEvent(const xchg::ParmEvent& evt)
{
    if (_schema)
        _schema->handle(evt);

    if (evt.parm() == parm::currentTime().id())
        handleCurrentTimeChanged(evt.type(), evt.node());
}

void
scn::SchemaBasedSceneObject::PImpl::handleCurrentTimeChanged(xchg::ParmEvent::Type type, unsigned int containerId)
{
    // current time update naturally flows along the node hierarchy.
    // the parameter is set only sparcely (depending on the user actions).
    // however, the current time value is stored locally in order to early skip computations.

    TreeNode::Ptr const&    self = _self.lock();
    TreeNode::Ptr const&    root = self ? self->root().lock() : nullptr;
    if (!root)
        return;

    // determine the new current time value
    float currentTime = 0.0f;

    if (type == xchg::ParmEvent::PARM_REMOVED)
        getClosestAncestorCurrentTime();
    else if (root->asScene()->hasDescendant(containerId))
    {
        // just pick value from container
        TreeNode::Ptr const& container = root->asScene()->descendantById(containerId).lock();

        if (container)
            container->getParameter<float>(parm::currentTime(), currentTime);
        else
            parm::currentTime().defaultValue(currentTime);
    }

    synchronizeWithTime(currentTime);   // early quits if time did not change enough
}
//-------------------
void
scn::SchemaBasedSceneObject::PImpl::synchronizeWithTime(float time)
{
    if (fabsf(time - _cachedTime) < 1e-4f)
        return; // time actually barely changed

    assert(_schema && _data);
    const bool visible = _schema->setCurrentTime(time);
    if (visible)
        _data->synchronizeWithSchema();
    _cachedTime = time;
}

bool
scn::SchemaBasedSceneObject::PImpl::mustSendToScene(const xchg::ParmEvent& evt) const
{
    return (_schema && _schema->mustSendToScene(evt)) ||
        evt.parm() == parm::timeBounds().id() ||    //<! update scene's time bounds
        evt.parm() == parm::numSamples().id();      //<! update NumSamplesHierarchy
}

void
scn::SchemaBasedSceneObject::PImpl::getStoredTimes(xchg::Vector<float>& times) const
{
    if (_schema)
        _schema->getStoredTimes(times);
    else
        times.clear();
}

/////////////////////////////////
// class SchemaBasedSceneObject
/////////////////////////////////
scn::SchemaBasedSceneObject::SchemaBasedSceneObject(const char* name, TreeNode::WPtr const& parent):
    SceneObject (name, parent),
    _pImpl      (new PImpl())
{ }

scn::SchemaBasedSceneObject::~SchemaBasedSceneObject()
{
#if defined(DEBUG_DTOR)
    FLIRT_LOG(LOG(DEBUG) << "deleting schema-based scene object '" << name() << "' (ID = " << id() << ")";)
#endif // defined(DEBUG_DTOR)
    delete _pImpl;
}

// virtual
void
scn::SchemaBasedSceneObject::build(Ptr const&                   ptr,
                                   AbstractObjectSchema*        schema,
                                   AbstractObjectDataHolder*    data)
{
    // PImpl must be built first for it to register the schema. The schema instance is used
    // to determine default values for some parameters and those are likely to get queries
    // when SceneObject::build() delivers its NODE_CREATED event.
    //---
    _pImpl->preBuild(ptr, schema, data);
    //---
    SceneObject::build(ptr);
    //---
    _pImpl->postBuild();
}

// virtual
void
scn::SchemaBasedSceneObject::erase()
{
    _pImpl->erase();
    //---
    SceneObject::erase();
}

void
scn::SchemaBasedSceneObject::uninitialize() // completely unloads content (undo initialize())
{
    _pImpl->uninitialize();
}
void
scn::SchemaBasedSceneObject::initialize()   // loads entirety of content from schema
{
    _pImpl->initialize();
}

void
scn::SchemaBasedSceneObject::uninitializeUserAttribs()  // completely unloads user attributes (undo initializeUserAttribs())
{
    _pImpl->uninitializeUserAttribs();
}
void
scn::SchemaBasedSceneObject::initializeUserAttribs()    // loads user attributes from schema
{
    _pImpl->initializeUserAttribs();
}

void
scn::SchemaBasedSceneObject::dirtySample(int idx, bool synchronize)
{
    _pImpl->dirtySample(idx, synchronize);
}

//-------------------
// parameter handling
//-------------------
// virtual
bool
scn::SchemaBasedSceneObject::isParameterRelevant(unsigned int parmId) const
{
    return _pImpl->isParameterRelevantForClass(parmId) || SceneObject::isParameterRelevant(parmId);
}
// virtual
bool
scn::SchemaBasedSceneObject::isParameterReadable(unsigned int parmId) const
{
    return SceneObject::isParameterReadable(parmId) ||
        _pImpl->isParameterReadable(parmId);
}
// virtual
bool
scn::SchemaBasedSceneObject::isParameterWritable(unsigned int parmId) const
{
    char ret = _pImpl->isParameterWritable(parmId);
    return ret == 1
        ? true
        : ret == 0 ? false : SceneObject::isParameterWritable(parmId);
}
// virtual
bool
scn::SchemaBasedSceneObject::overrideParameterDefaultValue(unsigned int parmId, parm::Any& defaultValue) const
{
    return SceneObject::overrideParameterDefaultValue(parmId, defaultValue) ||
        _pImpl->overrideParameterDefaultValue(parmId, defaultValue);
}
// virtual
bool
scn::SchemaBasedSceneObject::preprocessParameterEdit(const char*        parmName,
                                                     unsigned int       parmId,
                                                     parm::ParmFlags    f,
                                                     const parm::Any&   value)
{
    bool ret = false;
    ret = SceneObject::preprocessParameterEdit(parmName, parmId, f, value)  || ret;
    ret = _pImpl->preprocessParameterEdit(parmName, parmId, f, value)       || ret;
    return ret;
}
// virtual
void
scn::SchemaBasedSceneObject::handleParmEvent(const xchg::ParmEvent& evt)
{
    SceneObject::handleParmEvent(evt);
    //---
    _pImpl->handleParmEvent(evt);
}

// virtual
bool
scn::SchemaBasedSceneObject::mustSendToScene(const xchg::ParmEvent& evt) const
{
    if (_pImpl->mustSendToScene(evt))
        return true;
    //---
    return SceneObject::mustSendToScene(evt);
}

// virtual
void
scn::SchemaBasedSceneObject::getStoredTimes(xchg::Vector<float>& times) const
{
    _pImpl->getStoredTimes(times);
}

scn::AbstractObjectSchema*
scn::SchemaBasedSceneObject::schema()
{
    return _pImpl->schema();
}

scn::AbstractObjectDataHolder*
scn::SchemaBasedSceneObject::dataHolder()
{
    return _pImpl->dataHolder();
}
