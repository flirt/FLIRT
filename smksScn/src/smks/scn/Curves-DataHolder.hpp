// ========================================================================= //
// Copyright 2016-2018 SUPAMONKS_STUDIO                                      //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
//                                                                           //
// ========================================================================= //
#pragma once

#include "smks/scn/Curves.hpp"
#include "BufferAttributes.hpp"
#include "../util/data/ArrayCache.hpp"
#include "AbstractObjectDataHolder.hpp"

//////////////////////
// struct Curves::DataHolder
//////////////////////
namespace smks
{
    namespace scn
    {
        //////////////////////
        // struct BezierCounts
        //////////////////////
        struct BezierCounts
        {
            std::vector<size_t> numCurvesPerPath;
            size_t              numVertices;
        };
    }

    namespace util { namespace data
    {
        //////////////////////////////////
        // struct DirtyEntry<BezierCounts>
        //////////////////////////////////
        template<>
        struct DirtyEntry<scn::BezierCounts>:
            public std::unary_function<scn::BezierCounts&, void>
        {
            virtual inline
            void
            operator()(scn::BezierCounts& x) const
            {
                x.numCurvesPerPath.clear();
                x.numCurvesPerPath.shrink_to_fit();
                x.numVertices = 0;
            }
        };
        /////////////////////////////////////////
        // struct IsCacheEntryDirty<BezierCounts>
        /////////////////////////////////////////
        template<>
        struct IsCacheEntryDirty<scn::BezierCounts>:
            public std::unary_function<scn::BezierCounts const&, bool>
        {
            virtual inline
            bool
            operator()(const scn::BezierCounts& x) const
            {
                return x.numCurvesPerPath.empty();
            }
        };
    }
    }

    namespace scn
    {
        class Curves::DataHolder:
            public AbstractObjectDataHolder
        {
        public:
            ////////////////////////////////////////
            // enum Curves::DataHolder::GeomFeatures
            ////////////////////////////////////////
            enum GeomFeatures
            {
                NONE                    = 0,
                SEPARATE_BEZIER_CURVES  = 1 << 0,
            };

        private:
            class Indices;
            class PerBezierCurveIndices;
            class PerBezierPathIndices;

        private:
            typedef std::shared_ptr<Indices>                        IndicesPtr;
            typedef boost::unordered_map<GeomFeatures, IndicesPtr>  IndicesMap;

        private:
            AbstractCurvesSchema*                       _schema; // does not own it
            float                                       _defaultWidth;
            BufferAttributes                            _vertexAttribs;
            BufferAttributes                            _scalpAttribs;
            util::data::ArrayCache<BezierCounts>        _bezierCounts;
            IndicesMap                                  _indices;
            util::data::ArrayCache<std::vector<float>>  _vertices;
            util::data::ArrayCache<std::vector<float>>  _scalps;
            util::data::ArrayCache<xchg::BoundingBox>   _bounds;

        public:
            DataHolder();

            void
            build(AbstractObjectSchema*);

            void
            initialize();

            void
            uninitialize();

            void
            synchronizeWithSchema();

            inline
            void
            dirtyCacheEntry(size_t idx, bool synchronize)
            {
                throw sys::Exception(
                    "Only implemented for scene objects users can manually define.",
                    "Dirty Curves Data");
            }

        private:
            void
            unsetAllParameters();

            void
            glSynchronize();

            void
            rtSynchronize();

            size_t
            numBezierVertices(GeomFeatures);

            const size_t*
            numBezierCurvesPerPath(GeomFeatures, size_t& numPaths);

            const unsigned int*
            getIndices(GeomFeatures, size_t&, size_t& numIndicesPerPrimitive);

            const float*
            getVertices(GeomFeatures, size_t&, BufferAttributes&);

            const float*
            getScalps(GeomFeatures, size_t&, BufferAttributes&);

            const xchg::BoundingBox&
            getBounds();

            IndicesPtr
            getIndices(GeomFeatures);

            const BezierCounts*
            computeAndCacheBezierCounts(GeomFeatures);

            void
            getCubicBezierVertexPositions(float* oPtr, size_t oSize, size_t oStride) const;

            void
            getCubicBezierVertexWidths(float* oPtr, size_t oSize, size_t oStride) const;

            // convenience function
            static
            void
            getCubicBezierIndexBuffer(const AbstractCurvesSchema*,
                                      size_t&                       numBezierVertices,
                                      std::vector<size_t>&          numBezierCurvesPerPath,
                                      std::vector<unsigned int>&    firstCPIndices);
        };
    }
}
