// ========================================================================= //
// Copyright 2016-2018 SUPAMONKS_STUDIO                                      //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
//                                                                           //
// ========================================================================= //
#pragma once

#include <opensubdiv/sdc/types.h>
#include <opensubdiv/far/topologyRefiner.h>
#include <opensubdiv/far/primvarRefiner.h>
#include <opensubdiv/far/stencilTable.h>
#include <opensubdiv/far/stencilTableFactory.h>
#include <opensubdiv/osd/bufferDescriptor.h>

#include "PolyMesh-AbstractGeometry.hpp"
#include "../util/data/ArrayCache.hpp"

namespace OpenSubdiv
{
namespace OPENSUBDIV_VERSION
{
    namespace Far
    {
        class PatchTable;
    }
    namespace Osd
    {
        class CpuPatchTable;
        class CpuVertexBuffer;
    }
}
}

namespace smks
{
    namespace scn
    {
        ////////////////////
        // struct OsdPatches
        ////////////////////
        struct OsdPatches
        {
            std::vector<unsigned int>   indices;
            std::vector<unsigned int>   vtxRedir;   //<! face vertex ID -> OSD vertex ID
        };
    }

    namespace util
    {
        namespace data
        {
            ////////////////////////////
            // struct UpdateRefinerLevel
            ////////////////////////////
            struct UpdateRefinerLevel:
                public UpdateEntry<OpenSubdiv::Far::TopologyRefiner*>
            {
            private:
                const int _level; //<! subdivision level

            public:
                explicit
                UpdateRefinerLevel(int);

                virtual
                void
                operator()(OpenSubdiv::Far::TopologyRefiner*&) const;
            };

            ////////////////////////////////
            // struct DirtyEntry<OsdPatches>
            ////////////////////////////////
            template<>
            struct DirtyEntry<scn::OsdPatches>:
                public std::unary_function<scn::OsdPatches&, void>
            {
                virtual
                void
                operator()(scn::OsdPatches&) const;
            };

            ////////////////////////////////////////
            // struct IsCacheEntryDirty<OsdPatches>
            ////////////////////////////////////////
            template<>
            struct IsCacheEntryDirty<scn::OsdPatches>:
                public std::unary_function<scn::OsdPatches const&, bool>
            {
                virtual
                bool
                operator()(const scn::OsdPatches&) const;
            };
        }
    }

    namespace scn
    {
        class PolyMesh::FacetedSmoothGeometry:
            public PolyMesh::AbstractGeometry
        {
        public:
            typedef std::shared_ptr<FacetedSmoothGeometry>          Ptr;
            typedef std::shared_ptr<const FacetedSmoothGeometry>    CPtr;

        private:
            enum { MAX_LEVEL = 4 };

        private:
            const AbstractPolyMeshSchema*   _schema;    // owned by polymesh

            const unsigned int              _faceCount; //<! either 3 for triangles or 4 for quads
            unsigned int                    _level;
            OpenSubdiv::Sdc::SchemeType     _type;

            BufferAttributes                _faceVertexAttribs;
            size_t                          _fvarChannelUV;
            size_t                          _numFVarChannels;

            util::data::ArrayCache<OpenSubdiv::Far::TopologyRefiner*>       _refiner;
            util::data::ArrayCache<OpenSubdiv::Far::PrimvarRefiner*>        _primvarRefiner;    //<! for interpolating face-varying primvar data
            util::data::ArrayCache<OsdPatches>                              _patches;
            util::data::ArrayCache<const OpenSubdiv::Far::StencilTable*>    _vertexStencil;     //<! for interpolating vertex data
            util::data::ArrayCache<const OpenSubdiv::Far::StencilTable*>    _varyingStencil;    //<! for interpolating varying primvar data
            util::data::ArrayCache<std::vector<unsigned int>>               _wireIndices;
            util::data::ArrayCache<std::vector<float>>                      _faceVertices;

        public:
            static
            Ptr
            create(bool triangulate);

        private:
            explicit
            FacetedSmoothGeometry(bool triangulate);

        public:
            ~FacetedSmoothGeometry();

            void
            dispose();

            virtual
            bool
            initialize(const AbstractPolyMeshSchema*, bool caching);

            inline
            OpenSubdiv::Sdc::SchemeType
            type() const
            {
                return _type;
            }

            inline
            size_t
            subdivisionLevel() const
            {
                return static_cast<size_t>(_level);
            }

            void
            setSubdivisionLevel(unsigned int);

            inline
            const float*
            getVertices(size_t&             numFaceVertices,
                        BufferAttributes&   attribs)
            {
                return getFaceVertices(numFaceVertices, attribs);
            }

            const float*
            getFaceVertices(size_t&, BufferAttributes&);

            const unsigned int*
            getFaceIndices(size_t&);

            const unsigned int*
            getFaceCounts(size_t&, bool& isConstant);

            const unsigned int*
            getFaceVertexToVertexMapping(size_t& size)
            {
                size = 0;
                return nullptr; // no mapping
            }

            const unsigned int *
            getWireframeLines(size_t&);

        private:
            bool
            initialized() const;

            //bool // returns true if effective changes occured
            //updateLevel();

            bool // returns true if effective changes occured
            type(OpenSubdiv::Sdc::SchemeType);

            void
            dirtyRefiners();

            void
            dirtyPrimvarRefiners();

            void
            dirtyStencilTables();

            void
            dirtyPatches();

            void
            dirtyFaceVertexBuffers();

            OpenSubdiv::Far::TopologyRefiner *const
            getRefiner();

            OpenSubdiv::Far::PrimvarRefiner *const
            getPrimvarRefiner();

            const OsdPatches*
            getPatches();

            const OpenSubdiv::Far::StencilTable *const
            getStencilTable(OpenSubdiv::Far::StencilTableFactory::Mode);

            const std::vector<float>*
            getFaceVertexBuffer();

            const float*
            smootheVertices(size_t&,
                            BufferAttributes&);

            void
            fillPositionsAndNormals(std::vector<float>&);

            void
            fillTexCoords(std::vector<float>&);

            void
            mapVertexToFaceVertex(size_t length, const float*, size_t, float*, size_t);
        };
    }
}
