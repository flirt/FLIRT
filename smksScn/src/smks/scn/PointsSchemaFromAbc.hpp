// ========================================================================= //
// Copyright 2016-2018 SUPAMONKS_STUDIO                                      //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
//                                                                           //
// ========================================================================= //
#pragma once

#include <Alembic/AbcGeom/IPoints.h>

#include "AbstractPointsSchema.hpp"

namespace smks { namespace scn
{
    class TreeNode;
    class SchemaBasedSceneObject;
    class SchemaBaseFromAbc;

    class PointsSchemaFromAbc:
        public AbstractPointsSchema
    {
    private:
        struct SampleHolder;

    private:
        const Alembic::Abc::IObject     _object;        //<! raw object
        SchemaBaseFromAbc*              _base;
        Alembic::AbcGeom::IPointsSchema _points;        //<! typed object's schema
        PropertyFlags                   _flags;
        SampleHolder*                   _locks;

    public:
        PointsSchemaFromAbc(SchemaBasedSceneObject&,
                            const Alembic::Abc::IObject&,
                            std::weak_ptr<TreeNode> const& archive);
        ~PointsSchemaFromAbc();

        const SchemaBasedSceneObject&
        node() const;
        SchemaBasedSceneObject&
        node();

        inline
        bool
        lazyInitialization() const
        {
            return true;
        }
        void
        uninitialize();
        size_t  //<! return number of samples
        initialize();

        void
        uninitializeUserAttribs();
        void
        initializeUserAttribs();
    private:
        const Alembic::AbcGeom::IPointsSchema&
        getAbcSchema();
    public:
        //-------------------
        // parameter handling
        //-------------------
        bool
        isParameterRelevantForClass(unsigned int) const;
        bool
        isParameterReadable(unsigned int) const;
        char
        isParameterWritable(unsigned int) const;
        bool
        overrideParameterDefaultValue(unsigned int, parm::Any&) const;
        void
        handle(const xchg::ParmEvent&);
        bool
        mustSendToScene(const xchg::ParmEvent&) const;
        //-------------------

        bool //<! return current visibility
        setCurrentTime(float);

        size_t
        numSamples() const;

        int
        currentSampleIdx() const;

        void
        getStoredTimes(xchg::Vector<float>&) const;

        bool
        has(Property) const;

        const float*
        lockPositions(size_t&) const;

        void
        unlockPositions() const;

        const float*
        lockWidths(size_t&) const;

        void
        unlockWidths() const;

        void
        getSelfBounds(xchg::BoundingBox&) const;

    private:
        const float*
        lockPositions(int, size_t&) const;

        const float*
        lockWidths(int, size_t&) const;

        void
        getSelfBounds(int, xchg::BoundingBox&) const;

    private:
        static
        bool
        checkBounds(const Alembic::AbcGeom::IPointsSchema&);

        static
        bool
        checkWidths(const Alembic::AbcGeom::IPointsSchema&);
    };
}
}
