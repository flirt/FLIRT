// ========================================================================= //
// Copyright 2016-2018 SUPAMONKS_STUDIO                                      //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
//                                                                           //
// ========================================================================= //
#include <cassert>
#include <stack>
#include <smks/sys/Exception.hpp>
#include "smks/scn/Scene.hpp"
#include "smks/scn/TreeNode.hpp"

#include "detectNodeLinkCycle.hpp"

namespace smks { namespace scn
{
    struct Graph
    {
    public:
        enum Status { UNPROCESSED=0, BEING_PROCESSED=1, FULLY_PROCESSED=2 };
        typedef boost::unordered_map<unsigned int, TreeNode::WPtr>  Nodes;
        typedef boost::unordered_map<unsigned int, Status>          Statuses;
    public:
        const Nodes&        nodes;
        const unsigned int  target, source;
    public:
        inline
        Graph(const Nodes& nodes, unsigned int target, unsigned int source):
            nodes(nodes), target(target), source(source)
        { }
    };

    static inline
    bool
    dfs(const Graph&, unsigned int current, Graph::Statuses&);
}
}

using namespace smks;

bool
scn::detectNodeLinkCycle(const Graph::Nodes&    nodes,
                         unsigned int           target,
                         unsigned int           source)
{
    if (nodes.find(target) == nodes.end())
        throw sys::Exception("Link target node does not exist yet.", "Link Cycle Detection");
    if (nodes.find(source) == nodes.end())
        return false;   // non-existing source nodes cannot cause cycle
    if (source == target)
        return true;

    Graph           graph(nodes, target, source);
    Graph::Statuses statuses;

    for (Graph::Nodes::const_iterator it = graph.nodes.begin(); it != graph.nodes.end(); ++it)
        statuses[it->first] = Graph::UNPROCESSED;
    for (Graph::Statuses::iterator it = statuses.begin(); it != statuses.end(); ++it)
    {
        if (it->second != Graph::UNPROCESSED)
            continue;
        if (dfs(graph, it->first, statuses))
            return true;
    }
    return false;
}

bool
scn::dfs(const Graph&       graph,
         unsigned int       current,
         Graph::Statuses&   statuses)
{
    Graph::Status& status(statuses[current]);
    assert(status == Graph::UNPROCESSED);
    status = Graph::BEING_PROCESSED;

    Graph::Nodes::const_iterator const& it = graph.nodes.find(current);
    assert(it != graph.nodes.end());
    TreeNode::Ptr const& node = it->second.lock();
    assert(node);

    xchg::IdSet targets;
    node->getLinkTargets(targets);
    // artificially add link being tested
    if (current == graph.source)
        targets.insert(graph.target);

    for (xchg::IdSet::const_iterator id = targets.begin(); id != targets.end(); ++id)
    {
        const Graph::Status& targetStatus(statuses[*id]);
        if (targetStatus == Graph::FULLY_PROCESSED)
            continue;
        else if (targetStatus == Graph::BEING_PROCESSED)
            return true;    //  there is a cycle
        if (dfs(graph, *id, statuses))
            return true;
    }
    status = Graph::FULLY_PROCESSED;
    return false;
}
