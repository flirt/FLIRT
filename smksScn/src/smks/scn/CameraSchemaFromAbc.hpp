// ========================================================================= //
// Copyright 2016-2018 SUPAMONKS_STUDIO                                      //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
//                                                                           //
// ========================================================================= //
#pragma once

#include <Alembic/AbcGeom/ICamera.h>

#include "AbstractCameraSchema.hpp"

namespace smks { namespace scn
{
    class SchemaBaseFromAbc;

    class CameraSchemaFromAbc:
        public AbstractCameraSchema
    {
    private:
        const Alembic::Abc::IObject     _object;        //<! raw object
        SchemaBaseFromAbc*              _base;
        Alembic::AbcGeom::ICameraSchema _camera;        //<! typed object's schema

    public:
        CameraSchemaFromAbc(SchemaBasedSceneObject&,
                            const Alembic::Abc::IObject&,
                            TreeNode::WPtr const& archive);
        ~CameraSchemaFromAbc();

        SchemaBasedSceneObject&
        node();
        const SchemaBasedSceneObject&
        node() const;

        inline
        bool
        lazyInitialization() const
        {
            return true;
        }
        void
        uninitialize();
        size_t  //<! return number of samples
        initialize();

        void
        uninitializeUserAttribs();
        void
        initializeUserAttribs();
    private:
        const Alembic::AbcGeom::ICameraSchema&
        getAbcSchema();

    public:
        //-------------------
        // parameter handling
        //-------------------
        bool
        isParameterRelevantForClass(unsigned int) const;
        bool
        isParameterReadable(unsigned int) const;
        char
        isParameterWritable(unsigned int) const;
        bool
        overrideParameterDefaultValue(unsigned int, parm::Any&) const;
        void
        handle(const xchg::ParmEvent&);
        bool
        mustSendToScene(const xchg::ParmEvent&) const;
        //-------------------

        bool //<! return current visibility
        setCurrentTime(float);

        size_t
        numSamples() const;

        int
        currentSampleIdx() const;

        void
        getStoredTimes(xchg::Vector<float>&) const;

        void
        getAttributes(Camera::Attributes&) const;

        void
        getFilmBackOperations(Camera::FilmBackOperations&) const;

    private:
        void
        getAttributes(int sampleId, Camera::Attributes&) const;

        void
        getFilmBackOperations(int sampleId, Camera::FilmBackOperations&) const;
    };
}
}
