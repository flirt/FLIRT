// ========================================================================= //
// Copyright 2016-2018 SUPAMONKS_STUDIO                                      //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
//                                                                           //
// ========================================================================= //
#include <smks/sys/Log.hpp>
#include <smks/sys/Exception.hpp>
#include <smks/xchg/Data.hpp>
#include <smks/xchg/DataStream.hpp>

#include <smks/parm/Container.hpp>
#include <smks/parm/BuiltIns.hpp>

#include "smks/scn/Scene.hpp"
#include "PolyMesh-DataHolder.hpp"
#include "AbstractPolyMeshSchema.hpp"
#include "PolyMesh-FacetedCoarseTriGeometry.hpp"
#include "PolyMesh-CoarseTriquadGeometry.hpp"
#include "macros_opensubdiv.hpp"
#include "PolyMesh-FacetedSmoothGeometry.hpp"


using namespace smks;

scn::PolyMesh::DataHolder::DataHolder():
    _schema     (nullptr),
    _geometries (),
    _bounds     ()
{ }

void
scn::PolyMesh::DataHolder::build(AbstractObjectSchema* schema)
{
    _schema = schema ? schema->asPolyMeshSchema() : nullptr;
    assert(_schema);
}

void
scn::PolyMesh::DataHolder::uninitialize()
{
    unsetAllParameters();

    for (GeometryMap::iterator it = _geometries.begin(); it != _geometries.end(); ++it)
        if (it->second)
            it->second->dispose();
    _geometries.clear();

    _bounds.dirtyAll();
    _bounds.dispose();
}

void
scn::PolyMesh::DataHolder::initialize()
{
    uninitialize();
    assert(_schema);
    if (_schema->numSamples() > 0)  // schema has been initialized
    {
        // get general scene configuration
        TreeNode::Ptr const& root = _schema->node().root().lock();
        const Scene::Config& cfg  = root && root->asScene() ? root->asScene()->config() : Scene::Config();

        _bounds.initialize(cfg.cacheAnim() ? _schema->numVertexBufferSamples() : 0);
        _bounds.dirtyAll();
    }
}

void
scn::PolyMesh::DataHolder::synchronizeWithSchema()
{
    assert(_schema);
    if (_schema->numSamples() > 0)  // schema has been initialized
    {
        SchemaBasedSceneObject& node = _schema->node();

        setParameter<xchg::BoundingBox>(node, parm::boundingBox(), getBounds(), parm::SKIPS_RESTRAINTS);

        // get general scene configuration
        TreeNode::Ptr const&    root = node.root().lock();
        const Scene::Config& cfg  = root && root->asScene() ? root->asScene()->config() : Scene::Config();
        if (cfg.withViewport())
            glSynchronize();
        if (cfg.withRendering())
            rtSynchronize();
    }
    else
        unsetAllParameters();
}

void
scn::PolyMesh::DataHolder::unsetAllParameters()
{
    assert(_schema);
    SchemaBasedSceneObject& node = _schema->node();

    // get general scene configuration
    TreeNode::Ptr const&    root = node.root().lock();
    const Scene::Config& cfg  = root && root->asScene() ? root->asScene()->config() : Scene::Config();

    unsetParameter(node, parm::boundingBox(), parm::SKIPS_RESTRAINTS);
    if (cfg.withViewport())
    {
        unsetParameter(node, parm::glVertices       (), parm::SKIPS_RESTRAINTS);
        unsetParameter(node, parm::glPositions      (), parm::SKIPS_RESTRAINTS);
        unsetParameter(node, parm::glNormals        (), parm::SKIPS_RESTRAINTS);
        unsetParameter(node, parm::glTexcoords      (), parm::SKIPS_RESTRAINTS);
        unsetParameter(node, parm::glIndices        (), parm::SKIPS_RESTRAINTS);
        unsetParameter(node, parm::glCount          (), parm::SKIPS_RESTRAINTS);
        unsetParameter(node, parm::glWireIndices    (), parm::SKIPS_RESTRAINTS);
    }
    if (cfg.withRendering())
    {
        unsetParameter(node, parm::rtVertices       (), parm::SKIPS_RESTRAINTS);
        unsetParameter(node, parm::rtFaceVertices   (), parm::SKIPS_RESTRAINTS);
        unsetParameter(node, parm::rtPositions      (), parm::SKIPS_RESTRAINTS);
        unsetParameter(node, parm::rtNormals        (), parm::SKIPS_RESTRAINTS);
        unsetParameter(node, parm::rtTexcoords      (), parm::SKIPS_RESTRAINTS);
        unsetParameter(node, parm::rtIndices        (), parm::SKIPS_RESTRAINTS);
        unsetParameter(node, parm::rtCounts         (), parm::SKIPS_RESTRAINTS);
        unsetParameter(node, parm::rtCount          (), parm::SKIPS_RESTRAINTS);
    }
}

void
scn::PolyMesh::DataHolder::glSynchronize()
{
    assert(_schema && _schema->numSamples() > 0);
    SchemaBasedSceneObject& node = _schema->node();

    xchg::Data::Ptr     vertices    = nullptr;
    xchg::Data::Ptr     indices     = nullptr;
    xchg::DataStream    positions, normals, texcoords;
    size_t              count       = 0;
    xchg::Data::Ptr     wireIndices = nullptr;


    // determine geometry selection flags
    unsigned int    subdLevel   = 0;
    GeomFeatures    f           = GeomFeatures::NONE;

    node.getParameter<unsigned int>(parm::subdivisionLevel(), subdLevel);
    //f = subdLevel == 0 ? COARSE_TRIS : SMOOTH_QUADS;
    f = subdLevel == 0 ? COARSE_TRIS : SMOOTH_TRIS; //<! quads as drawing primitives are depreceated in GL3 core profile
    assert(f & GeomFeatures::FACETED);  // face vertices = vertices

    AbstractGeometry::Ptr const& geometry = getGeometry(f);

    if (geometry)
    {
        size_t              numVertices     = 0;
        size_t              numIndices      = 0;
        size_t              numFaces        = 0;
        bool                isCountConstant = false;
        BufferAttributes    vtxAttribs;

        geometry->setSubdivisionLevel(subdLevel);

        const float*        vtxData     = geometry->getFaceVertices (numVertices, vtxAttribs);
        const unsigned int* idxData     = geometry->getFaceIndices  (numIndices);
        const unsigned int* countData   = geometry->getFaceCounts   (numFaces, isCountConstant);

        if (vtxData && numVertices > 0 && vtxAttribs.stride() > 0)
        {
            vertices = xchg::Data::create<float>(vtxData, numVertices * vtxAttribs.stride(), false);
            if (vtxAttribs.has(Property::PROP_POSITION))
                positions.initialize(vertices, numVertices, vtxAttribs.stride(), vtxAttribs.offset(Property::PROP_POSITION));
            if (vtxAttribs.has(Property::PROP_NORMAL))
                normals.initialize  (vertices, numVertices, vtxAttribs.stride(), vtxAttribs.offset(Property::PROP_NORMAL));
            if (vtxAttribs.has(Property::PROP_TEXCOORD))
                texcoords.initialize(vertices, numVertices, vtxAttribs.stride(), vtxAttribs.offset(Property::PROP_TEXCOORD));
        }
        if (idxData && numIndices > 0)
            indices = xchg::Data::create<unsigned int>(idxData, numIndices, false);
        if (countData && numFaces > 0 && isCountConstant)
            count = countData[0];

        bool selected = false;

        node.getParameter<bool>(parm::selected(), selected);
        if (selected)
        {
            size_t              numWireIndices  = 0;
            const unsigned int* wireData        = geometry->getWireframeLines(numWireIndices);

            if (wireData && numWireIndices > 0)
                wireIndices = xchg::Data::create<unsigned int>(wireData, numWireIndices, false);
        }
    }

    if(vertices     )   setParameter<xchg::Data::Ptr>   (node, parm::glVertices     (), vertices    , parm::SKIPS_RESTRAINTS);  else unsetParameter(node, parm::glVertices      (), parm::SKIPS_RESTRAINTS);
    if(positions    )   setParameter<xchg::DataStream>  (node, parm::glPositions    (), positions   , parm::SKIPS_RESTRAINTS);  else unsetParameter(node, parm::glPositions     (), parm::SKIPS_RESTRAINTS);
    if(normals      )   setParameter<xchg::DataStream>  (node, parm::glNormals      (), normals     , parm::SKIPS_RESTRAINTS);  else unsetParameter(node, parm::glNormals       (), parm::SKIPS_RESTRAINTS);
    if(texcoords    )   setParameter<xchg::DataStream>  (node, parm::glTexcoords    (), texcoords   , parm::SKIPS_RESTRAINTS);  else unsetParameter(node, parm::glTexcoords     (), parm::SKIPS_RESTRAINTS);
    if(indices      )   setParameter<xchg::Data::Ptr>   (node, parm::glIndices      (), indices     , parm::SKIPS_RESTRAINTS);  else unsetParameter(node, parm::glIndices       (), parm::SKIPS_RESTRAINTS);
    if(count > 0    )   setParameter<size_t>            (node, parm::glCount        (), count       , parm::SKIPS_RESTRAINTS);  else unsetParameter(node, parm::glCount         (), parm::SKIPS_RESTRAINTS);
    if(wireIndices  )   setParameter<xchg::Data::Ptr>   (node, parm::glWireIndices  (), wireIndices , parm::SKIPS_RESTRAINTS);  else unsetParameter(node, parm::glWireIndices   (), parm::SKIPS_RESTRAINTS);
}

void
scn::PolyMesh::DataHolder::rtSynchronize()
{
    assert(_schema && _schema->numSamples() > 0);
    SchemaBasedSceneObject& node = _schema->node();

    xchg::Data::Ptr     vertices        = nullptr;
    xchg::Data::Ptr     faceVertices    = nullptr;
    xchg::Data::Ptr     indices         = nullptr;
    xchg::Data::Ptr     counts          = nullptr;
    xchg::DataStream    positions, normals, texcoords;
    size_t              count           = 0;

    // determine geometry selection flags
    unsigned int    subdLevel   = 0;
    std::string     subdMethod;
    GeomFeatures    f           = GeomFeatures::NONE;

    node.getParameter<unsigned int> (parm::subdivisionLevel     (), subdLevel);
    node.getParameter<std::string>  (parm::rtSubdivisionMethod  (), subdMethod);
    f = subdLevel == 0 ? COARSE_TRIS : (subdMethod == "embree" ? UNFACETED_TRIQUADS : SMOOTH_TRIS);

    AbstractGeometry::Ptr const& geometry = getGeometry(f);

    if (geometry)
    {
        size_t              numVertices     = 0;
        size_t              numFaceVertices = 0;
        size_t              numIndices      = 0;
        size_t              numFaces        = 0;
        bool                isCountConstant = false;
        BufferAttributes    vtxAttribs, fvtxAttribs;

        geometry->setSubdivisionLevel(subdLevel);

        const float*        vtxData     = geometry->getVertices     (numVertices, vtxAttribs);
        const float*        fvtxData    = geometry->getFaceVertices (numFaceVertices, fvtxAttribs);
        const unsigned int* idxData     = geometry->getFaceIndices  (numIndices);
        const unsigned int* countData   = geometry->getFaceCounts   (numFaces, isCountConstant);

        if (vtxData && numVertices > 0 && vtxAttribs.stride() > 0)
        {
            vertices = xchg::Data::create<float>(vtxData, numVertices * vtxAttribs.stride(), false);
            if (vtxAttribs.has(Property::PROP_POSITION))
                positions.initialize(vertices, numVertices, vtxAttribs.stride(), vtxAttribs.offset(Property::PROP_POSITION));
            if (vtxAttribs.has(Property::PROP_NORMAL))
                normals.initialize  (vertices, numVertices, vtxAttribs.stride(), vtxAttribs.offset(Property::PROP_NORMAL));
        }
        if (fvtxData && numFaceVertices > 0 && fvtxAttribs.stride() > 0)
        {
            faceVertices = xchg::Data::create<float>(fvtxData, numFaceVertices * fvtxAttribs.stride(), false);
            if (fvtxAttribs.has(Property::PROP_NORMAL))
                normals.initialize  (faceVertices, numFaceVertices, fvtxAttribs.stride(), fvtxAttribs.offset(Property::PROP_NORMAL));
            if (fvtxAttribs.has(Property::PROP_TEXCOORD))
                texcoords.initialize(faceVertices, numFaceVertices, fvtxAttribs.stride(), fvtxAttribs.offset(Property::PROP_TEXCOORD));
        }
        if (idxData && numIndices > 0)
            indices = xchg::Data::create<unsigned int>(idxData, numIndices, false);
        if (countData && numFaces > 0)
        {
            if (!isCountConstant)
                counts  = xchg::Data::create<unsigned int>(countData, numFaces, false);
            else
                count   = countData[0];
        }
    }

    if(vertices     )   setParameter<xchg::Data::Ptr>   (node, parm::rtVertices     (), vertices    , parm::SKIPS_RESTRAINTS);  else unsetParameter(node, parm::rtVertices      (), parm::SKIPS_RESTRAINTS);
    if(faceVertices )   setParameter<xchg::Data::Ptr>   (node, parm::rtFaceVertices(), faceVertices, parm::SKIPS_RESTRAINTS);   else unsetParameter(node, parm::rtFaceVertices  (), parm::SKIPS_RESTRAINTS);
    if(positions    )   setParameter<xchg::DataStream>  (node, parm::rtPositions    (), positions   , parm::SKIPS_RESTRAINTS);  else unsetParameter(node, parm::rtPositions     (), parm::SKIPS_RESTRAINTS);
    if(normals      )   setParameter<xchg::DataStream>  (node, parm::rtNormals      (), normals     , parm::SKIPS_RESTRAINTS);  else unsetParameter(node, parm::rtNormals       (), parm::SKIPS_RESTRAINTS);
    if(texcoords    )   setParameter<xchg::DataStream>  (node, parm::rtTexcoords    (), texcoords   , parm::SKIPS_RESTRAINTS);  else unsetParameter(node, parm::rtTexcoords     (), parm::SKIPS_RESTRAINTS);
    if(indices      )   setParameter<xchg::Data::Ptr>   (node, parm::rtIndices      (), indices     , parm::SKIPS_RESTRAINTS);  else unsetParameter(node, parm::rtIndices       (), parm::SKIPS_RESTRAINTS);
    if(counts       )   setParameter<xchg::Data::Ptr>   (node, parm::rtCounts       (), counts      , parm::SKIPS_RESTRAINTS);  else unsetParameter(node, parm::rtCounts        (), parm::SKIPS_RESTRAINTS);
    if(count > 0    )   setParameter<size_t>            (node, parm::rtCount        (), count       , parm::SKIPS_RESTRAINTS);  else unsetParameter(node, parm::rtCount         (), parm::SKIPS_RESTRAINTS);
}

const xchg::BoundingBox&
scn::PolyMesh::DataHolder::getBounds()
{
    assert(_schema && _schema->numSamples() > 0);

    const int                   vertexSampleId  = _schema->currentVertexSampleIdx();
    const xchg::BoundingBox*    cache           = _bounds.get(vertexSampleId);
    if (cache)
        return *cache;

    // actual computation
    xchg::BoundingBox bounds;

    if (_schema->has(Property::PROP_BOUNDS))
        _schema->getSelfBounds(bounds);
    else
    {
        size_t          numPositions    = 0;
        const float*    positions       = _schema->lockPositions(numPositions);

        bounds.initialize(positions, numPositions, 3);
        _schema->unlockPositions();
    }

    // cache update
    return *_bounds.set(vertexSampleId, bounds);
}

scn::PolyMesh::AbstractGeometry::Ptr
scn::PolyMesh::DataHolder::getGeometry(GeomFeatures f)
{
    assert(_schema && _schema->numSamples() > 0);

    if (!supported(f))
    {
        std::stringstream sstr;
        sstr
            << "Unsupported geometry features "
            << "(value = " << std::to_string(f) << ") "
            << "for mesh '" << _schema->node().name() << "'.";
        throw sys::Exception(sstr.str().c_str(), "PolyMesh Geometry Getter");
    }

    if (_geometries.find(f) == _geometries.end())
    {
        // get general scene configuration
        TreeNode::Ptr const&    root = _schema->node().root().lock();
        const Scene::Config& cfg  = root && root->asScene() ? root->asScene()->config() : Scene::Config();
        // decide kind of geometry
        AbstractGeometry::Ptr   geometry = nullptr;

        if (f == COARSE_TRIS)
            geometry = FacetedCoarseTriGeometry::create();
        else if (f == UNFACETED_TRIQUADS)
            geometry = CoarseTriquadGeometry::create();
        else if (f == SMOOTH_QUADS)
            geometry = FacetedSmoothGeometry::create(false);
        else if (f == SMOOTH_TRIS)
            geometry = FacetedSmoothGeometry::create(true);
        assert(geometry);
        geometry->initialize(_schema, cfg.cacheAnim());
        _geometries[f] = geometry;
    }

    return _geometries[f];
}

// static
bool
scn::PolyMesh::DataHolder::supported(GeomFeatures f)
{
    return f == COARSE_TRIS ||
        f == UNFACETED_TRIQUADS ||
        f == SMOOTH_QUADS ||
        f == SMOOTH_TRIS;
}
