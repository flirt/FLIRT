// ========================================================================= //
// Copyright 2016-2018 SUPAMONKS_STUDIO                                      //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
//                                                                           //
// ========================================================================= //

#include <Alembic/Abc/IObject.h>

#include <smks/sys/Log.hpp>
#include <smks/sys/Exception.hpp>

#include <smks/xchg/SceneEvent.hpp>
#include <smks/xchg/ParmEvent.hpp>
#include <smks/parm/BuiltIn.hpp>
#include <smks/parm/BuiltIns.hpp>

#include <smks/math/matrixAlgo.hpp>
#include "smks/scn/Scene.hpp"
#include "AbstractObjectDataHolder.hpp"
#include "CameraParmBasedSchema.hpp"
#include "CameraSchemaFromAbc.hpp"

#include "../util/data/ArrayCache.hpp"

namespace smks
{
    namespace util { namespace data
    {
        ///////////////////////////////////////////////
        // struct IsCacheEntryDirty<Camera::Attributes>
        ///////////////////////////////////////////////
        template<>
        struct IsCacheEntryDirty<scn::Camera::Attributes>
        {
            virtual inline
            bool
            operator()(const scn::Camera::Attributes& x) const
            {
                return !(x.zNear < x.zFar);
            }
        };

        ////////////////////////////////////////
        // struct DirtyEntry<Camera::Attributes>
        ////////////////////////////////////////
        template<>
        struct DirtyEntry<scn::Camera::Attributes>
        {
            virtual inline
            void
            operator()(scn::Camera::Attributes& x) const
            {
                memset(&x, 0, sizeof(scn::Camera::Attributes));
            }
        };
    }
    }

    namespace scn
    {
        class Camera::DataHolder:
            public AbstractObjectDataHolder
        {
        private:
            AbstractCameraSchema*                           _schema; // does not own it
            util::data::ArrayCache<scn::Camera::Attributes> _attribs;

        public:
            inline
            DataHolder():
                _schema(nullptr), _attribs()
            { }

            inline
            void
            build(AbstractObjectSchema*);

            inline
            void
            initialize();

            inline
            void
            uninitialize();

            inline
            void
            synchronizeWithSchema();

            inline
            void
            dirtyCacheEntry(size_t idx, bool synchronize);
        private:
            inline
            void
            unsetAllParameters();

            inline
            const Imath::M44f&
            getPerspectiveProjection();

            inline
            bool
            areParametersUserDefined() const;

            inline
            const scn::Camera::Attributes&
            getAttributes();
        };
    }
}

using namespace smks;

/////////////////////
// class Camera::DataHolder
/////////////////////
void
scn::Camera::DataHolder::build(AbstractObjectSchema* schema)
{
    _schema = schema ? schema->asCameraSchema() : nullptr;
    assert(_schema);

    //if (areParametersUserDefined())
    //{
    //  SchemaBasedSceneObject::Ptr const& node = _schema->node().lock();
    //  assert(node);

    //  // parameter values are defined by users, store default values in parameter set first
    //  const parm::BuiltIn* parms[] = { &parm::xAspectRatio(), &parm::yFieldOfViewD(), &parm::zNear(), &parm::zFar() };
    //  for (size_t i = 0; i < sizeof(parms)/sizeof(const parm::BuiltIn*); ++i)
    //  {
    //      float parm1f = 0.0f;

    //      parms[i]->defaultValue(parm1f);
    //      setParameter<float>(*node, *parms[i], parm1f, parm::SKIPS_RESTRAINTS);
    //  }
    //  // call to initilize ?? PEL
    //}
}

void
scn::Camera::DataHolder::uninitialize()
{
    unsetAllParameters();

    _attribs.dirtyAll();
    _attribs.dispose();
}

void
scn::Camera::DataHolder::initialize()
{
    uninitialize();
    assert(_schema);
    if (_schema->numSamples() > 0)  // schema has been initialized
    {
        // get general scene configuration
        TreeNode::Ptr const&    root = _schema->node().root().lock();
        const Scene::Config& cfg  = root && root->asScene() ? root->asScene()->config() : Scene::Config();

        _attribs.initialize(cfg.cacheAnim() ? _schema->numSamples() : 0);
        _attribs.dirtyAll();
    }
}

void
scn::Camera::DataHolder::synchronizeWithSchema()
{
    assert(_schema);
    if (_schema->numSamples() > 0)  // schema has been initialized
    {
        SchemaBasedSceneObject& node = _schema->node();

        if (!areParametersUserDefined())
        {
            // values cannot be set by users and are instead imposed by schema
            Camera::Attributes attribs;

            _schema->getAttributes(attribs);
            setParameter<float>(node, parm::xAspectRatio    (), attribs.xAspectRatio,   parm::SKIPS_RESTRAINTS);
            setParameter<float>(node, parm::yFieldOfViewD   (), attribs.yFieldOfViewD,  parm::SKIPS_RESTRAINTS);
            setParameter<float>(node, parm::zNear           (), attribs.zNear,          parm::SKIPS_RESTRAINTS);
            setParameter<float>(node, parm::zFar            (), attribs.zFar,           parm::SKIPS_RESTRAINTS);

            AbstractObjectDataHolder::initialize(node); // immediately forces node initialization
        }
    }
    else
        unsetAllParameters();
}

void
scn::Camera::DataHolder::unsetAllParameters()
{
    assert(_schema);
    SchemaBasedSceneObject& node = _schema->node();

    unsetParameter(node, parm::xAspectRatio (), parm::SKIPS_RESTRAINTS);
    unsetParameter(node, parm::yFieldOfViewD(), parm::SKIPS_RESTRAINTS);
    unsetParameter(node, parm::zNear        (), parm::SKIPS_RESTRAINTS);
    unsetParameter(node, parm::zFar         (), parm::SKIPS_RESTRAINTS);
}

bool
scn::Camera::DataHolder::areParametersUserDefined() const
{
    assert(_schema);
    return _schema->isParameterWritable(parm::xAspectRatio().id()) &&
        _schema->isParameterWritable(parm::yFieldOfViewD().id()) &&
        _schema->isParameterWritable(parm::zNear        ().id()) &&
        _schema->isParameterWritable(parm::zFar         ().id());
}

const scn::Camera::Attributes&
scn::Camera::DataHolder::getAttributes()
{
    assert(_schema && _schema->numSamples() > 0);

    const int                       sampleIdx   = _schema->currentSampleIdx();
    const scn::Camera::Attributes*  cache       = _attribs.get(sampleIdx);

    if (cache)
        return *cache;

    // actual computation
    Attributes attribs;
    _schema->getAttributes(attribs);

    // cache update
    return *_attribs.set(sampleIdx, attribs);
}

void
scn::Camera::DataHolder::dirtyCacheEntry(size_t idx, bool synchronize)
{
    _attribs.dirty(idx);

    if (synchronize)
        synchronizeWithSchema();
}

///////////////
// class Camera
///////////////
// static
scn::Camera::Ptr
scn::Camera::create(const char*             name,
                    TreeNode::WPtr const&   parent)
{
    Ptr ptr (new Camera(name, parent));
    ptr->build(
        ptr,
        new CameraParmBasedSchema(*ptr),
        new Camera::DataHolder());
    return ptr;
}

// static
scn::Camera::Ptr
scn::Camera::create(const Alembic::Abc::IObject&    object,
                    TreeNode::WPtr const&           parent,
                    TreeNode::WPtr const&           archive)
{
    Ptr ptr (new Camera(object, parent, archive));
    ptr->build(
        ptr,
        new CameraSchemaFromAbc(*ptr, object, archive),
        new Camera::DataHolder());
    return ptr;
}

scn::Camera::Camera(const char*             name,
                    TreeNode::WPtr const&   parent):
    SchemaBasedSceneObject(name, parent)
{ }

scn::Camera::Camera(const Alembic::Abc::IObject&    object,
                    TreeNode::WPtr const&           parent,
                    TreeNode::WPtr const&           archive):
SchemaBasedSceneObject(object.getName().c_str(), parent)
{ }
