// ========================================================================= //
// Copyright 2016-2018 SUPAMONKS_STUDIO                                      //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
//                                                                           //
// ========================================================================= //
#include <smks/sys/Exception.hpp>
#include <smks/parm/BuiltIns.hpp>
#include <smks/xchg/NodeCode.hpp>
#include <smks/xchg/NodeCodes.hpp>

#include "smks/scn/Material.hpp"

using namespace smks;

// static
scn::Material::Ptr
scn::Material::create(const char*           code,
                      const char*           name,
                      TreeNode::WPtr const& parent)
{
    if (code == nullptr ||
        strlen(code) == 0)
        throw sys::Exception(
            "Empty material code.",
            "Material Creation");

    Ptr ptr(new Material(name, parent));

    ptr->build(ptr);
    ptr->setParameter<std::string>(parm::code(), code, parm::SKIPS_RESTRAINTS);

    return ptr;
}

// virtual
bool
scn::Material::isParameterWritable(unsigned int parmId) const
{
    if (isParameterRelevantForClass(parmId))
        return parmId != parm::code().id();
    return Shader::isParameterWritable(parmId);
}

// virtual
bool
scn::Material::isParameterRelevant(unsigned int parmId)const
{
    return isParameterRelevantForClass(parmId) || Shader::isParameterRelevant(parmId);
}

bool
scn::Material::isParameterRelevantForClass(unsigned int parmId) const
{
    if (parmId == parm::code        ().id() ||
        parmId == parm::normalMap   ().id())
        return true;

    const unsigned int code = getCode();
    if (code == xchg::code::brushedMetal().id())
    {
        return
            parmId == parm::reflectance     ().id() ||
            parmId == parm::reflectanceMap  ().id() ||
            parmId == parm::roughnessX      ().id() ||
            parmId == parm::roughnessXMap   ().id() ||
            parmId == parm::roughnessY      ().id() ||
            parmId == parm::roughnessYMap   ().id() ||
            parmId == parm::IORreal         ().id() ||
            parmId == parm::IORimag         ().id();
    }
    else if (code == xchg::code::metal().id())
    {
        return
            parmId == parm::reflectance     ().id() ||
            parmId == parm::reflectanceMap  ().id() ||
            parmId == parm::roughness       ().id() ||
            parmId == parm::roughnessMap    ().id() ||
            parmId == parm::IORreal         ().id() ||
            parmId == parm::IORimag         ().id();
    }
    else if (code == xchg::code::metallicPaint().id())
    {
        return
            parmId == parm::layerColor          ().id() ||
            parmId == parm::layerColorMap       ().id() ||
            parmId == parm::glitterColor        ().id() ||
            parmId == parm::glitterColorMap     ().id() ||
            parmId == parm::glitterSpread       ().id() ||
            parmId == parm::glitterSpreadMap    ().id() ||
            parmId == parm::layerIOR            ().id();
    }
    else if (code == xchg::code::dielectric().id()  ||
        code == xchg::code::glass().id())
    {
        return
            parmId == parm::intIOR          ().id() ||
            parmId == parm::intTransmission ().id() ||
            parmId == parm::extIOR          ().id() ||
            parmId == parm::extTransmission ().id();
    }
    else if (code == xchg::code::thinDielectric().id()  ||
        code == xchg::code::thinGlass().id())
    {
        return
            parmId == parm::transmission    ().id() ||
            parmId == parm::transmissionMap ().id() ||
            parmId == parm::thickness       ().id() ||
            parmId == parm::thicknessMap    ().id() ||
            parmId == parm::IOR             ().id();
    }
    else if (code == xchg::code::matte().id())
    {
        return
            parmId == parm::reflectance     ().id() ||
            parmId == parm::reflectanceMap  ().id();
    }
    else if (code == xchg::code::plastic().id())
    {
        return
            parmId == parm::diffuseReflectance      ().id() ||
            parmId == parm::diffuseReflectanceMap   ().id() ||
            parmId == parm::roughness               ().id() ||
            parmId == parm::roughnessMap            ().id() ||
            parmId == parm::IOR                     ().id();
    }
    else if (code == xchg::code::mirror().id())
    {
        return
            parmId == parm::reflectance     ().id() ||
            parmId == parm::reflectanceMap  ().id();
    }
    else if (code == xchg::code::velvet().id())
    {
        return
            parmId == parm::reflectance                 ().id() ||
            parmId == parm::reflectanceMap              ().id() ||
            parmId == parm::backScattering              ().id() ||
            parmId == parm::backScatteringMap           ().id() ||
            parmId == parm::horizonScatteringColor      ().id() ||
            parmId == parm::horizonScatteringColorMap   ().id() ||
            parmId == parm::horizonScatteringFalloff    ().id() ||
            parmId == parm::horizonScatteringFalloffMap ().id();
    }
    else if (code == xchg::code::translucent().id())
    {
        return
            parmId == parm::transmission    ().id() ||
            parmId == parm::transmissionMap ().id();
    }
    else if (code == xchg::code::translucentSS().id())
    {
        return
            parmId == parm::diffuseReflectance      ().id() ||
            parmId == parm::diffuseReflectanceMap   ().id() ||
            parmId == parm::glossyReflectance       ().id() ||
            parmId == parm::glossyReflectanceMap    ().id() ||
            parmId == parm::diffuseTransmission     ().id() ||
            parmId == parm::diffuseTransmissionMap  ().id() ||
            parmId == parm::roughness               ().id() ||
            parmId == parm::roughnessMap            ().id() ||
            parmId == parm::IOR                     ().id() ||
            parmId == parm::fresnelWeight           ().id() ||
            parmId == parm::fresnelWeightMap        ().id() ||
            parmId == parm::falloff                 ().id() ||
            parmId == parm::falloffMap              ().id() ||
            parmId == parm::transmissivity          ().id() ||
            parmId == parm::transmissivityMap       ().id();
    }
    else if (code == xchg::code::obj().id())
    {
        return
            parmId == parm::diffuseReflectance      ().id() ||
            parmId == parm::diffuseReflectanceMap   ().id() ||
            parmId == parm::specularReflectance     ().id() ||
            parmId == parm::specularReflectanceMap  ().id() ||
            parmId == parm::specularExponent        ().id() ||
            parmId == parm::specularExponentMap     ().id() ||
            parmId == parm::opacity                 ().id() ||
            parmId == parm::opacityMap              ().id();
    }
    else if (code == xchg::code::basicHair().id())
    {
        return
            parmId == parm::diffuseReflectance  ().id() ||
            parmId == parm::roughnessX          ().id() ||
            parmId == parm::roughnessY          ().id() ||
            parmId == parm::reflectivity        ().id() ||
            parmId == parm::transparentShadows  ().id();
    }
    else if (code == xchg::code::microfacet().id())
    {
        return
            parmId == parm::fresnelName ().id() ||
            parmId == parm::NDFName     ().id() ||
            parmId == parm::fresnelValue().id() ||
            parmId == parm::reflectance ().id() ||
            parmId == parm::roughness   ().id() ||
            parmId == parm::intIOR      ().id() ||
            parmId == parm::extIOR      ().id() ||
            parmId == parm::IORreal     ().id() ||
            parmId == parm::IORimag     ().id();
    }
    else if (code == xchg::code::eye().id())
    {
        return
            parmId == parm::center                  ().id() ||
            parmId == parm::front                   ().id() ||
            parmId == parm::up                      ().id() ||
            parmId == parm::usedAsPoint             ().id() ||
            //---
            parmId == parm::eyeCorneaIOR            ().id() ||
            parmId == parm::eyeGlobeColor           ().id() ||
            parmId == parm::eyeHighlightAngSizeD    ().id() ||
            parmId == parm::eyeHighlightColor       ().id() ||
            parmId == parm::eyeHighlightSmoothness  ().id() ||
            parmId == parm::eyeHighlightCoordsD     ().id() ||
            parmId == parm::eyeIrisAngSizeD         ().id() ||
            parmId == parm::eyeIrisColor            ().id() ||
            parmId == parm::eyeIrisDepth            ().id() ||
            parmId == parm::eyeIrisDimensions       ().id() ||
            parmId == parm::eyeIrisEdgeBlur         ().id() ||
            parmId == parm::eyeIrisEdgeOffset       ().id() ||
            parmId == parm::eyeIrisNormalSmoothness ().id() ||
            parmId == parm::eyeIrisRotationD        ().id() ||
            parmId == parm::eyePupilBlur            ().id() ||
            parmId == parm::eyePupilDimensions      ().id() ||
            parmId == parm::eyePupilSize            ().id() ||
            parmId == parm::eyeRadius               ().id();
    }
    else if (code == xchg::code::blend().id())
    {
        return
            parmId == parm::inputId0    ().id() ||
            parmId == parm::inputId1    ().id() ||
            parmId == parm::weight      ().id() ||
            parmId == parm::weightMap   ().id();
    }

    return false;
}

