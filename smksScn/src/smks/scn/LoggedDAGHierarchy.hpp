// ========================================================================= //
// Copyright 2016-2018 SUPAMONKS_STUDIO                                      //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
//                                                                           //
// ========================================================================= //
#pragma once

#include <string>
#include <smks/sys/Log.hpp>
#include "DAGHierarchy.hpp"

namespace smks { namespace scn
{
    template <typename ValueType>
    class LoggedDAGHierarchy:
        public DAGHierarchy<ValueType>
    {
    private:
        const std::string _name;
    public:
        inline
        LoggedDAGHierarchy(const char* name, DataPtr const& data):
            DAGHierarchy<ValueType> (data),
            _name                   (name)
        { }

        inline
        void
        addNode(unsigned int node, unsigned int parent)
        {
            FLIRT_LOG(LOG(DEBUG)
                << "\n<DAGHierarchy>\t"
                << _name << "->addNode(" << node << ", " << parent << ");";)
            DAGHierarchy<ValueType>::addNode(node, parent);
        }

        inline
        void
        removeNode(unsigned int id)
        {
            FLIRT_LOG(LOG(DEBUG)
                << "\n<DAGHierarchy>\t"
                << _name << "->removeNode(" << id << ");";)
                //---
            DAGHierarchy<ValueType>::removeNode(id);
        }

        inline
        void
        setLocal(unsigned int node, const ValueType& value)
        {
            FLIRT_LOG(LOG(DEBUG)
                << "\n<DAGHierarchy>\t"
                << _name << "->setLocal(" << node << ", " << std::to_string(value) << ");";)
                //---
            DAGHierarchy<ValueType>::setLocal(node, value);
        };

        inline
        const ValueType&
        getGlobal(unsigned int node)
        {
            const ValueType& ret = DAGHierarchy<ValueType>::getGlobal(node);
            //---
            FLIRT_LOG(LOG(DEBUG)
                << "\n<DAGHierarchy>\t"
                << _name << "->getGlobal(" << node << ");"
                << "\t// returns " << std::to_string(ret);)
            //---
            return ret;
        }
    };
}
}
