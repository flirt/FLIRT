// ========================================================================= //
// Copyright 2016-2018 SUPAMONKS_STUDIO                                      //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
//                                                                           //
// ========================================================================= //
#pragma once

#include <smks/xchg/equal.hpp>

namespace smks { namespace scn
{
    template<typename ValueType> static inline
    bool    //<! return false if the value stored in argument is the same as the value stored in the node's parameter set
    getBuiltInFromNodeOrArgument(scn::TreeNode&         node,
                                 const parm::BuiltIn&   parm,
                                 unsigned int           argId,
                                 const parm::Any&       arg,
                                 bool (*equal)(const ValueType&, const ValueType&),
                                 ValueType&             parmValue)
    {
        node.getParameter<ValueType>(parm, parmValue);  // existing value from node
        if (!arg.empty() &&
            argId == parm.id())
        {
            const ValueType& argValue = *parm::Any::cast<ValueType>(&arg);  // value from argument
            if (equal(argValue, parmValue) == true)
                return false;   // value from argument is the same as node's, can stop computations
            parmValue = argValue;
        }
        return true;
    }

    template<typename ValueType> static inline
    bool    //<! return false if the value stored in argument is the same as the value stored in the node's parameter set
    getBuiltInFromNodeOrArgument(scn::TreeNode&                     node,
                                 const parm::BuiltIn&               parm,
                                 unsigned int                       argId,
                                 const parm::Any&                   arg,
                                 const xchg::EqualTo<ValueType>&    equal,
                                 ValueType&                         parmValue)
    {
        node.getParameter<ValueType>(parm, parmValue);  // existing value from node
        if (!arg.empty() &&
            argId == parm.id())
        {
            const ValueType& argValue = *parm::Any::cast<ValueType>(&arg);  // value from argument
            if (equal.operator()(argValue, parmValue) == true)
                return false;   // value from argument is the same as node's, can stop computations
            parmValue = argValue;
        }
        return true;
    }
}
}
