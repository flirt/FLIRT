// ========================================================================= //
// Copyright 2016-2018 SUPAMONKS_STUDIO                                      //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
//                                                                           //
// ========================================================================= //
#include <ImathMatrix.h>
#include <smks/math/matrixAlgo.hpp>
#include <smks/xchg/ParmEvent.hpp>
#include <smks/parm/BuiltIn.hpp>
#include <smks/parm/BuiltIns.hpp>
#include <smks/parm/AbstractObserver.hpp>

#include "CameraParmBasedSchema.hpp"

using namespace smks;

// explicit
scn::CameraParmBasedSchema::CameraParmBasedSchema(SchemaBasedSceneObject& node):
    AbstractCameraSchema(),
    _node               (node),
    _initialized        (false)
{ }

//-------------------
// parameter handling
//-------------------
char
scn::CameraParmBasedSchema::isParameterWritable(unsigned int parmId) const
{
    if (parmId == parm::currentTime     ().id() ||
        parmId == parm::currentSampleIdx().id() ||
        parmId == parm::timeBounds      ().id() ||
        parmId == parm::numSamples      ().id())
        return 0;
    if (isParameterRelevantForClass(parmId))
        return 1;
    return -1;
}
bool
scn::CameraParmBasedSchema::isParameterRelevantForClass(unsigned int parmId) const
{
    return
        parmId == parm::xAspectRatio    ().id() ||
        parmId == parm::yFieldOfViewD   ().id() ||
        parmId == parm::zNear           ().id() ||
        parmId == parm::zFar            ().id();
}

void
scn::CameraParmBasedSchema::handle(const xchg::ParmEvent& evt)
{
    if (evt.parm() == parm::yFieldOfViewD   ().id() ||
        evt.parm() == parm::xAspectRatio    ().id() ||
        evt.parm() == parm::zNear           ().id() ||
        evt.parm() == parm::zFar            ().id())
    {
        const bool synchronize = true;
        AbstractObjectSchema::dirtySample(_node, currentSampleIdx(), synchronize); // triggers sample reevaluation
    }
}
//-------------------

void
scn::CameraParmBasedSchema::getAttributes(Camera::Attributes& attribs) const
{
    memset(&attribs, 0, sizeof(Camera::Attributes));

    parm::xAspectRatio  ().defaultValue(attribs.xAspectRatio);
    parm::yFieldOfViewD ().defaultValue(attribs.yFieldOfViewD);
    parm::zNear         ().defaultValue(attribs.zNear);
    parm::zFar          ().defaultValue(attribs.zFar);

    {
        _node.getParameter<float>(parm::xAspectRatio    (), attribs.xAspectRatio);
        _node.getParameter<float>(parm::yFieldOfViewD   (), attribs.yFieldOfViewD);
        _node.getParameter<float>(parm::zNear           (), attribs.zNear);
        _node.getParameter<float>(parm::zFar            (), attribs.zFar);
    }
}

void
scn::CameraParmBasedSchema::getFilmBackOperations(Camera::FilmBackOperations&) const
{ }
