// ========================================================================= //
// Copyright 2016-2018 SUPAMONKS_STUDIO                                      //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
//                                                                           //
// ========================================================================= //
#include <smks/xchg/Data.hpp>
#include <smks/xchg/DataStream.hpp>
#include <smks/parm/BuiltIn.hpp>
#include <smks/parm/BuiltIns.hpp>

#include "smks/scn/Scene.hpp"
#include "Points-DataHolder.hpp"
#include "AbstractPointsSchema.hpp"


using namespace smks;

scn::Points::DataHolder::DataHolder():
    _schema         (nullptr),
    _defaultWidth   (Scene::Config().defaultPointWidth()),
    _vertexAttribs  (),
    _vertices       (),
    _bounds         ()
{ }

void
scn::Points::DataHolder::build(AbstractObjectSchema* schema)
{
    _schema = schema ? schema->asPointsSchema() : nullptr;
    assert(_schema);
}

void
scn::Points::DataHolder::uninitialize()
{
    unsetAllParameters();

    _defaultWidth = Scene::Config().defaultPointWidth();

    _vertexAttribs.clear();

    _vertices   .dirtyAll();
    _bounds     .dirtyAll();

    _vertices   .dispose();
    _bounds     .dispose();
}

void
scn::Points::DataHolder::initialize()
{
    uninitialize();
    assert(_schema);
    if (_schema->numSamples() > 0)  // schema has been initialized
    {
        // get general scene configuration
        TreeNode::Ptr const& root = _schema->node().root().lock();
        const Scene::Config& cfg  = root && root->asScene() ? root->asScene()->config() : Scene::Config();

        _defaultWidth   = cfg.defaultPointWidth();

        _vertices   .initialize(cfg.cacheAnim() ? _schema->numSamples() : 0);
        _bounds     .initialize(cfg.cacheAnim() ? _schema->numSamples() : 0);

        _vertices   .dirtyAll();
        _bounds     .dirtyAll();

        assert(_schema->has(Property::PROP_POSITION));
        _vertexAttribs.append(Property::PROP_POSITION, 3);
        _vertexAttribs.append(Property::PROP_WIDTH, 1); // will compute it anyway
    }
}

void
scn::Points::DataHolder::synchronizeWithSchema()
{
    assert(_schema);
    if (_schema->numSamples() > 0)  // schema has been initialized
    {
        SchemaBasedSceneObject& node = _schema->node();

        setParameter<xchg::BoundingBox>(node, parm::boundingBox(), getBounds(), parm::SKIPS_RESTRAINTS);

        xchg::Data::Ptr     vertices = nullptr;
        xchg::DataStream    positions;
        xchg::DataStream    widths;

        size_t              numVertices = 0;
        BufferAttributes    attribs;
        const float*        data = getVertices(numVertices, attribs);

        if (data && numVertices > 0 && attribs.stride() > 0)
        {
            vertices = xchg::Data::create<float>(data, numVertices * attribs.stride(), false);
            if (attribs.has(Property::PROP_POSITION))
                positions.initialize(vertices, numVertices, attribs.stride(), attribs.offset(Property::PROP_POSITION));
            if (attribs.has(Property::PROP_WIDTH))
                widths.initialize(vertices, numVertices, attribs.stride(), attribs.offset(Property::PROP_WIDTH));
        }

        if (vertices)   setParameter<xchg::Data::Ptr> (node, parm::vertices(),  vertices,   parm::SKIPS_RESTRAINTS); else unsetParameter(node, parm::vertices   (), parm::SKIPS_RESTRAINTS);
        if (positions)  setParameter<xchg::DataStream>(node, parm::positions(), positions,  parm::SKIPS_RESTRAINTS); else unsetParameter(node, parm::positions  (), parm::SKIPS_RESTRAINTS);
        if (widths)     setParameter<xchg::DataStream>(node, parm::widths(),    widths,     parm::SKIPS_RESTRAINTS); else unsetParameter(node, parm::widths (), parm::SKIPS_RESTRAINTS);
    }
    else unsetAllParameters();
}

void
scn::Points::DataHolder::unsetAllParameters()
{
    assert(_schema);
    SchemaBasedSceneObject& node = _schema->node();
    unsetParameter(node, parm::boundingBox  (), parm::SKIPS_RESTRAINTS);
    unsetParameter(node, parm::vertices     (), parm::SKIPS_RESTRAINTS);
    unsetParameter(node, parm::positions    (), parm::SKIPS_RESTRAINTS);
    unsetParameter(node, parm::widths       (), parm::SKIPS_RESTRAINTS);
}

const float*
scn::Points::DataHolder::getVertices(size_t&            numVertices,
                                     BufferAttributes&  attribs)
{
    assert(_schema && _schema->numSamples() > 0);
    numVertices = 0;
    attribs.clear();

    assert(_vertexAttribs.stride() > 0);
    attribs = _vertexAttribs;

    const int                   sampleId    = _schema->currentSampleIdx();
    const std::vector<float>*   cache       = _vertices.get(sampleId);
    if (cache)
    {
        numVertices = cache
            ? cache->size() / _vertexAttribs.stride()
            : 0;
        return numVertices > 0
            ? &cache->front()
            : nullptr;
    }

    // actual computation
    const float*        positions   = _schema->lockPositions(numVertices);
    std::vector<float>  vertices    (numVertices * _vertexAttribs.stride());

    if (vertices.empty())
    {
        _schema->unlockPositions();
        return nullptr;
    }

    assert(_vertexAttribs.has(Property::PROP_POSITION));
    assert(_vertexAttribs.has(Property::PROP_WIDTH));
    float*          oPosition   = &vertices.front() + _vertexAttribs.offset(Property::PROP_POSITION);
    float*          oWidths     = &vertices.front() + _vertexAttribs.offset(Property::PROP_WIDTH);
    const size_t    oStride     = _vertexAttribs.stride();

    const float*    iPosition   = positions;
    for (size_t i = 0; i < numVertices; ++i)
    {
        memcpy(oPosition, iPosition, sizeof(float) * 3);
        iPosition   += 3;
        oPosition   += oStride;
    }
    _schema->unlockPositions();

    if (_schema->has(Property::PROP_WIDTH))
    {
        size_t          numWidths   = 0;
        const float*    widths      = _schema->lockWidths(numWidths);

        if (numWidths == 1)
            for (size_t i = 0; i < numVertices; ++i)
            {
                *oWidths    = widths[0];
                oWidths     += _vertexAttribs.stride();
            }
        else if (numWidths == numVertices)
            for (size_t i = 0; i < numVertices; ++i)
            {
                *oWidths    = widths[i];
                oWidths     += _vertexAttribs.stride();
            }
        else
        {
            std::stringstream sstr;
            sstr
                << "Cannot fill vertex buffer with width information "
                << "for point set '" << _schema->node().name() << "'.";
            throw sys::Exception(sstr.str().c_str(), "Points Vertex Getter");
        }

        _schema->unlockWidths();
    }
    else
        for (size_t i = 0; i < numVertices; ++i)
        {
            *oWidths    = _defaultWidth;
            oWidths     += _vertexAttribs.stride();
        }

    // cache update
    cache       = _vertices.set(sampleId, vertices);
    numVertices = cache
        ? (cache->size() / _vertexAttribs.stride())
        : 0;

    return numVertices > 0
        ? &cache->front()
        : nullptr;
}

const xchg::BoundingBox&
scn::Points::DataHolder::getBounds()
{
    assert(_schema && _schema->numSamples() > 0);

    const int                   sampleId    = _schema->currentSampleIdx();
    const xchg::BoundingBox*    cache       = _bounds.get(sampleId);
    if (cache)
        return *cache;

    // actual computation
    xchg::BoundingBox bounds;

    if (_schema->has(Property::PROP_BOUNDS))
        _schema->getSelfBounds(bounds);
    else
    {
        size_t          numPositions    = 0;
        const float*    positions       = _schema->lockPositions(numPositions);

        bounds.initialize(positions, numPositions, 3);
        _schema->unlockPositions();
    }

    // cache update
    return *_bounds.set(sampleId, bounds);
}
