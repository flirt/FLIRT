// ========================================================================= //
// Copyright 2016-2018 SUPAMONKS_STUDIO                                      //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
//                                                                           //
// ========================================================================= //
#include <smks/sys/Exception.hpp>
#include <smks/parm/BuiltIns.hpp>
#include <smks/xchg/NodeCode.hpp>
#include <smks/xchg/NodeCodes.hpp>

#include "smks/scn/RenderPass.hpp"

using namespace smks;

// static
scn::RenderPass::Ptr
scn::RenderPass::create(const char*             code,
                        const char*             name,
                        TreeNode::WPtr const&   parent)
{
    if (code == nullptr ||
        strlen(code) == 0)
        throw sys::Exception("Invalid code.", "Render Pass Creation");

    Ptr ptr(new RenderPass(name, parent));
    ptr->build(ptr);
    ptr->setParameter<std::string>(parm::code(), code, parm::SKIPS_RESTRAINTS);
    return ptr;
}

// virtual
bool
scn::RenderPass::isParameterWritable(unsigned int parmId) const
{
    if (isParameterRelevantForClass(parmId))
        return parmId != parm::code().id();
    return RtNode::isParameterWritable(parmId);
}

// virtual
bool
scn::RenderPass::isParameterRelevant(unsigned int parmId)const
{
    return isParameterRelevantForClass(parmId) || RtNode::isParameterRelevant(parmId);
}

bool
scn::RenderPass::isParameterRelevantForClass(unsigned int parmId) const
{
    if (parmId == parm::code        ().id() ||
        parmId == parm::storeAsHalf ().id())
        return true;

    const unsigned int code = getCode();
    if (code == xchg::code::ambientOcclusion().id())
    {
        return
            parmId == parm::numSamples      ().id() ||
            parmId == parm::maxDistance     ().id() ||
            parmId == parm::userColorName   ().id();
    }
    else if (code == xchg::code::lightPath().id())
        return parmId == parm::regex().id();
    else if (code == xchg::code::idCoverage().id())
    {
        return
            parmId == parm::selectorIds ().id() ||
            parmId == parm::maxCount    ().id();
    }
    return false;
}
