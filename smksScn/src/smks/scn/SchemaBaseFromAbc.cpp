// ========================================================================= //
// Copyright 2016-2018 SUPAMONKS_STUDIO                                      //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
//                                                                           //
// ========================================================================= //

#include <algorithm>

#include <Alembic/Abc/IObject.h>
#include <Alembic/Abc/ICompoundProperty.h>
#include <Alembic/AbcGeom/IXform.h>
#include <Alembic/AbcGeom/IPoints.h>
#include <Alembic/AbcGeom/ICurves.h>
#include <Alembic/AbcGeom/IPolyMesh.h>
#include <Alembic/AbcGeom/ICamera.h>

#include <smks/sys/Log.hpp>
#include <smks/sys/Exception.hpp>
#include <smks/parm/BuiltIns.hpp>
#include <smks/parm/BuiltInMap.hpp>
#include <smks/xchg/SceneEvent.hpp>
#include <smks/xchg/ParmEvent.hpp>
#include <smks/xchg/Vector.hpp>
#include <std/to_string_xchg.hpp>
#include "smks/scn/TreeNode.hpp"
#include "smks/scn/SchemaBasedSceneObject.hpp"
#include "smks/scn/Scene.hpp"
#include "smks/scn/Archive.hpp"
#include "smks/scn/Camera.hpp"
#include "smks/scn/PolyMesh.hpp"
#include "smks/scn/Curves.hpp"
#include "smks/scn/Points.hpp"
#include "smks/scn/Xform.hpp"

#include "SchemaBaseFromAbc.hpp"
#include "VisibilityFromAbc.hpp"
#include "UserAttribParm.hpp"

using namespace smks;

scn::SchemaBaseFromAbc::SchemaBaseFromAbc(SchemaBasedSceneObject&       node,
                                          const Alembic::Abc::IObject&  object,
                                          TreeNode::WPtr const&         archive):
    _visibility         (node, object, archive),
    //-------------------
    _timeSampling       (nullptr),
    _numSamples         (0),
    _currentSampleIdx   (INVALID_SAMPLE_IDX),
    _userAttribParms    ()
{ }

scn::SchemaBaseFromAbc::~SchemaBaseFromAbc()
{
    uninitialize();
    uninitializeUserAttribs();
}

//-------------------
// parameter handling
//-------------------
bool
scn::SchemaBaseFromAbc::isParameterRelevantForClass(unsigned int parmId) const
{
    return
        parmId == parm::initialization          ().id() ||
        parmId == parm::userAttribInitialization().id() ||
        parmId == parm::currentTime             ().id() ||
        parmId == parm::timeBounds              ().id() ||
        parmId == parm::numSamples              ().id() ||
        parmId == parm::currentSampleIdx        ().id() ||
        parmId == parm::forciblyVisible         ().id() ||
        _visibility.isParameterRelevantForClass(parmId);
    // user attributes are not by essence relevant to the node's behavior
}

bool
scn::SchemaBaseFromAbc::isParameterReadable(unsigned int parmId) const
{
    return _visibility.isParameterReadable(parmId);
}

char
scn::SchemaBaseFromAbc::isParameterWritable(unsigned int parmId) const
{
    char ret = -1;
    if (isParameterRelevantForClass(parmId))
    {
        if (parmId == parm::initialization          ().id() ||
            parmId == parm::userAttribInitialization().id() ||
            parmId == parm::currentTime             ().id() ||
            parmId == parm::forciblyVisible         ().id())
            return 1;   // write
        else if (parmId == parm::timeBounds ().id() ||
            parmId == parm::numSamples      ().id() ||
            parmId == parm::currentSampleIdx().id())
            return 0;   // read-only
        ret = _visibility.isParameterWritable(parmId);
    }
    return ret;
}

bool
scn::SchemaBaseFromAbc::overrideParameterDefaultValue(unsigned int parmId, parm::Any& defaultValue) const
{
    if (_visibility.overrideParameterDefaultValue(parmId, defaultValue))
        return true;

    if (parmId == parm::initialization().id())
    {
        // no parameter -> node is not initialized
        defaultValue = parm::Any(static_cast<char>(xchg::INACTIVE));
        return true;
    }
    return false;
}

void
scn::SchemaBaseFromAbc::handle(const xchg::ParmEvent& evt)
{
    char parm1c = 0;
    if (evt.parm() == parm::initialization().id())
    {
        node().getParameter<char>(parm::initialization(), parm1c);
        if (parm1c == xchg::ACTIVATING)
            node().initialize();
        else if (parm1c == xchg::DEACTIVATING)
            node().uninitialize();
    }
    else if (evt.parm() == parm::userAttribInitialization().id())
    {
        node().getParameter<char>(parm::userAttribInitialization(), parm1c);
        if (parm1c == xchg::ACTIVATING)
            node().initializeUserAttribs();
        else if (parm1c == xchg::DEACTIVATING)
            node().uninitializeUserAttribs();
    }
    else if (evt.parm() == parm::forciblyVisible().id())
    {
        node().getParameter<char>(parm::forciblyVisible(), parm1c);
        _visibility.setForciblyVisible(parm1c);
    }
}

bool
scn::SchemaBaseFromAbc::mustSendToScene(const xchg::ParmEvent& evt) const
{
    return evt.parm() == parm::userAttribInitialization().id() ||
        evt.parm() == parm::initialization().id() ||    //<! update InitializationHierarchy
        evt.parm() == parm::numSamples().id();          //<! update NumSamplesHierarchy
}
//-------------------

// virtual
void
scn::SchemaBaseFromAbc::uninitialize()
{
    _numSamples         = 0;
    _currentSampleIdx   = INVALID_SAMPLE_IDX;

    node()._unsetParameter(parm::numSamples     (), parm::SKIPS_RESTRAINTS);
    node()._unsetParameter(parm::currentSampleIdx(), parm::SKIPS_RESTRAINTS);

    // unset all parameters defined from user attributes attached to the ABC object
    for (UserAttribParmMap::const_iterator it = _userAttribParms.begin(); it != _userAttribParms.end(); ++it)
        node().unsetParameter(it->second->id());
    _userAttribParms.clear();

    _visibility.uninitialize();
    _timeSampling   = nullptr;
    //---
    updateTimeBounds();
}

//---------------------------------------------------------------------------------------------
#define IMPL_INITIALIZE_NODE_DATA_FROM_ABC_SCHEMA(_SCHEMA_, _STR_)                              \
    size_t                                                                                      \
    scn::SchemaBaseFromAbc::initialize(const _SCHEMA_& schema)                                  \
    {                                                                                           \
        if (_numSamples > 0)                                                                    \
            return _numSamples;                                                                 \
        if (!schema.valid())                                                                    \
            throw sys::Exception(                                                               \
                "Invalid " #_STR_ " schema.",                                                   \
                "Alembic Schema Base Initialization");                                          \
                                                                                                \
        _numSamples = std::max(static_cast<size_t>(1), schema.getNumSamples());                 \
        _visibility.initialize();                                                               \
        _timeSampling = schema.getTimeSampling();                                               \
                                                                                                \
        node().setParameter<size_t>(parm::numSamples(), _numSamples, parm::SKIPS_RESTRAINTS);   \
                                                                                                \
        updateTimeBounds();                                                                     \
                                                                                                \
        return _numSamples;                                                                     \
    }
//---------------------------------------------------------------------------------------------
IMPL_INITIALIZE_NODE_DATA_FROM_ABC_SCHEMA(Alembic::AbcGeom::ICameraSchema,      "camera")
IMPL_INITIALIZE_NODE_DATA_FROM_ABC_SCHEMA(Alembic::AbcGeom::ICurvesSchema,      "curves")
IMPL_INITIALIZE_NODE_DATA_FROM_ABC_SCHEMA(Alembic::AbcGeom::IPointsSchema,      "points")
IMPL_INITIALIZE_NODE_DATA_FROM_ABC_SCHEMA(Alembic::AbcGeom::IPolyMeshSchema,    "polymesh")
IMPL_INITIALIZE_NODE_DATA_FROM_ABC_SCHEMA(Alembic::AbcGeom::IXformSchema,       "xform")

//-------------------------------------------------------------------------------
#define IMPL_INITIALIZE_USER_ATTRIBS_FROM_ABC_SCHEMA(_SCHEMA_)                  \
    void scn::SchemaBaseFromAbc::initializeUserAttribs(const _SCHEMA_& schema)  \
    {                                                                           \
        if (_userAttribParms.empty() &&                                         \
            schema.valid())                                                     \
        {                                                                       \
            initializeUserAttribs(schema.getArbGeomParams(), "");               \
            updateTimeBounds();                                                 \
        }                                                                       \
    }
//-------------------------------------------------------------------------------
IMPL_INITIALIZE_USER_ATTRIBS_FROM_ABC_SCHEMA(Alembic::AbcGeom::ICameraSchema)
IMPL_INITIALIZE_USER_ATTRIBS_FROM_ABC_SCHEMA(Alembic::AbcGeom::ICurvesSchema)
IMPL_INITIALIZE_USER_ATTRIBS_FROM_ABC_SCHEMA(Alembic::AbcGeom::IPointsSchema)
IMPL_INITIALIZE_USER_ATTRIBS_FROM_ABC_SCHEMA(Alembic::AbcGeom::IPolyMeshSchema)
IMPL_INITIALIZE_USER_ATTRIBS_FROM_ABC_SCHEMA(Alembic::AbcGeom::IXformSchema)


// at the time of the writing, Maya stores:
// - user attributes under the '.argGeomParams' compound property,
// - array properties ONLY.
void
scn::SchemaBaseFromAbc::initializeUserAttribs(const Alembic::Abc::ICompoundProperty&    prop,
                                              const std::string&                        prefix)
{

    if (!prop.valid() ||
        !prop.isCompound())
        return;

    for (unsigned int i = 0; i < prop.getNumProperties(); ++i)
    {
        const Alembic::Abc::PropertyHeader& header = prop.getPropertyHeader(i);
        const std::string& name = prefix.empty()
            ? header.getName()
            : prefix + "/" + header.getName();

        if (header.getPropertyType() == Alembic::Abc::kCompoundProperty)
            initializeUserAttribs(Alembic::Abc::ICompoundProperty(prop, header.getName()), name);
        else
        {
            UserAttribParmBase::Ptr attrParm = nullptr;

            if (header.getPropertyType() == Alembic::Abc::kScalarProperty)
                attrParm = ScalarUserAttribParm::create(name, Alembic::Abc::IScalarProperty(prop, header.getName()));
            else if (header.getPropertyType() == Alembic::Abc::kArrayProperty)
                attrParm = ArrayUserAttribParm::create(name, Alembic::Abc::IArrayProperty(prop, header.getName()));
            if (attrParm && attrParm->type() != xchg::UNKNOWN_PARMTYPE)
            {
                if (!node().isParameterWritable(attrParm->id()))
                {
                    std::cerr << "Ignoring user attribute '" << attrParm->name() << "': read-only parameter for node '" << node().name() << "'." << std::endl;
                    return;
                }

                parm::BuiltIn::Ptr const& builtIn = parm::builtIns().find(attrParm->id());
                if (builtIn && !builtIn->compatibleWith(attrParm->type()))
                {
                    std::cerr << "Ignoring user attribute '" << attrParm->name() << "': conflicting type with eponymous built-in parameter." << std::endl;
                    return;
                }
                _userAttribParms.insert(std::pair<unsigned int, UserAttribParmBase::Ptr>(attrParm->id(), attrParm));
            }
        }
    }
}

void
scn::SchemaBaseFromAbc::uninitializeUserAttribs()
{
    // unset all parameters defined from user attributes attached to the ABC object
    for (UserAttribParmMap::const_iterator it = _userAttribParms.begin(); it != _userAttribParms.end(); ++it)
        node().unsetParameter(it->second->id());
    _userAttribParms.clear();
    updateTimeBounds();
}

void
scn::SchemaBaseFromAbc::synchronizeUserAttribs(float time)
{
    for (UserAttribParmMap::const_iterator it = _userAttribParms.begin(); it != _userAttribParms.end(); ++it)
        it->second->setCurrentTime(node(), time);
}

// virtual
bool
scn::SchemaBaseFromAbc::setCurrentTime(float time)
{
    bool visible = false;

    // synchronize node's main data (visibility and time)
    if (_numSamples > 0)
    {
        assert(_visibility.initialized());

        visible = _visibility.setCurrentTime(time);
        if (visible)
        {
            _currentSampleIdx = getSampleIndex(time);
            assert(_currentSampleIdx >= 0 && _currentSampleIdx < static_cast<int>(_numSamples));

            node().setParameter<int>(parm::currentSampleIdx(), _currentSampleIdx, parm::SKIPS_RESTRAINTS);
        }
    }

    // synchronize user attributes
    synchronizeUserAttribs(time);

    return visible;
}

void
scn::SchemaBaseFromAbc::getStoredTimes(xchg::Vector<float>& ret) const
{
    ret.clear();
    if (_numSamples <= 1 ||
        !_timeSampling)
        return;

    ret.reserve(_numSamples);
    for (size_t i = 0; i < _numSamples; ++i)
        ret.push_back(
            static_cast<float>(_timeSampling->getSampleTime(i))
        );
}

int
scn::SchemaBaseFromAbc::getSampleIndex(float time) const
{
    return _numSamples > 1
        ? static_cast<int>(
        Alembic::Abc::ISampleSelector(
            static_cast<Alembic::Abc::chrono_t>(time),
            Alembic::Abc::ISampleSelector::TimeIndexType::kNearIndex).getIndex(
                _timeSampling,
                _numSamples))
        : 0;
}

bool
scn::SchemaBaseFromAbc::getTimeBounds(float& minTime, float& maxTime) const
{
    minTime = FLT_MAX;
    maxTime = -FLT_MAX;

    float tmpMin = FLT_MAX;
    float tmpMax = -FLT_MAX;

    if (_numSamples > 0)
    {
        if (_timeSampling &&
            _numSamples > 1)
        {
            minTime = static_cast<float>(_timeSampling->getSampleTime(static_cast<Alembic::Abc::index_t>(0)));
            maxTime = static_cast<float>(_timeSampling->getSampleTime(static_cast<Alembic::Abc::index_t>(_numSamples-1)));
        }
        if (_visibility.getTimeBounds(tmpMin, tmpMax))
        {
            minTime = std::min(minTime, tmpMin);
            maxTime = std::max(maxTime, tmpMax);
        }
    }
    for (UserAttribParmMap::const_iterator it = _userAttribParms.begin(); it != _userAttribParms.end(); ++it)
        if (it->second->getTimeBounds(tmpMin, tmpMax))
        {
            minTime = std::min(minTime, tmpMin);
            maxTime = std::max(maxTime, tmpMax);
        }

    return minTime < maxTime;
}

void
scn::SchemaBaseFromAbc::updateTimeBounds()
{
    math::Vector2f timeBounds(FLT_MAX, -FLT_MAX);
    if (getTimeBounds(timeBounds.x(), timeBounds.y()))
        node().setParameter<math::Vector2f>(parm::timeBounds(), timeBounds, parm::SKIPS_RESTRAINTS);
    else
        node()._unsetParameter(parm::timeBounds(), parm::SKIPS_RESTRAINTS);
}
