// ========================================================================= //
// Copyright 2016-2018 SUPAMONKS_STUDIO                                      //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
//                                                                           //
// ========================================================================= //
#include <smks/xchg/IdSet.hpp>
#include <smks/parm/BuiltIns.hpp>

#include <smks/sys/Log.hpp>
#include <smks/xchg/SceneEvent.hpp>
#include <smks/xchg/ParmEvent.hpp>

#include "smks/scn/TreeNodeSelector.hpp"
#include "smks/scn/Scene.hpp"

#include <smks/util/string/match.hpp>

namespace smks { namespace scn
{
    ////////////////////////////
    // class TreeNodeSelector::PImpl
    ////////////////////////////
    class TreeNodeSelector::PImpl
    {
    private:
        Scene::IdGetterOptions* _getOptions;

    public:
        inline
        PImpl():
            _getOptions (new Scene::IdGetterOptions)
        {
            _getOptions->accumulate(false);
        }

        inline
        ~PImpl()
        {
            delete _getOptions;
        }

        void
        handleParmEvent(TreeNodeSelector&, const xchg::ParmEvent&);

        void
        handleSceneEvent(TreeNodeSelector&, const xchg::SceneEvent&);
    };
}
}

using namespace smks;

////////////////////////////
// class TreeNodeSelector::PImpl
////////////////////////////
void
scn::TreeNodeSelector::PImpl::handleSceneEvent(TreeNodeSelector&        self,
                                               const xchg::SceneEvent&  evt)
{
    xchg::IdSet selection;
    bool        changed = false;

    self.getParameter<xchg::IdSet>(parm::selection(), selection);
    switch (evt.type())
    {
    default:
        break;
    case xchg::SceneEvent::NODE_CREATED:
        {
            if (_getOptions->numRegexes() > 0)
            {
                TreeNode::Ptr const& node = self.getNodeFromRoot(evt.node());
                if (node &&
                    util::string::matchWithRegex(_getOptions->regex(0), node->fullName()))
                {
                    selection.insert(evt.node());
                    changed = true;
                }
            }
            break;
        }
    case xchg::SceneEvent::NODE_DELETED:
        {
            if (selection.find(evt.node()) != selection.end())
            {
                selection.erase(evt.node());
                changed = true;
            }
            break;
        }
    }
    if (changed)
    {
        if (!selection.empty())
            self.setParameter<xchg::IdSet>(parm::selection(), selection, parm::SKIPS_RESTRAINTS);
        else
            self._unsetParameter(parm::selection(), parm::SKIPS_RESTRAINTS);
    }
}

void
scn::TreeNodeSelector::PImpl::handleParmEvent(TreeNodeSelector&         self,
                                              const xchg::ParmEvent&    evt)
{
    if (evt.node() != self.id() ||  // event ignored because it does not involve my parameters.
        evt.parm() != parm::regex().id())
        return;

    // match regex against the scene's whole content
    TreeNode::Ptr const& scene = self.root().lock();

    if (!scene)
        return;

    xchg::IdSet selection;

    // clear getter options
    while (_getOptions->numRegexes() > 0)
        _getOptions->removeRegex(0);

    // get regex from parameter set
    std::string regex;

    self.getParameter<std::string>(parm::regex(), regex);
    if (!regex.empty())
    {
        // set regex and match it against the content of the whole scene
        _getOptions->addRegex(regex.c_str());
        scene->asScene()->getIds(selection, _getOptions);
    }

    if (!selection.empty())
        self.setParameter<xchg::IdSet>(parm::selection(), selection, parm::SKIPS_RESTRAINTS);
    else
        self._unsetParameter(parm::selection(), parm::SKIPS_RESTRAINTS);
}

//////////////////////
// class TreeNodeSelector
//////////////////////
// static
scn::TreeNodeSelector::Ptr
scn::TreeNodeSelector::create(const char* regex, const char* name, TreeNode::WPtr const& parent)
{
    Ptr ptr(new TreeNodeSelector(name, parent));

    ptr->build(ptr);
    ptr->setParameter<std::string>(parm::regex(), regex);

    return ptr;
}

scn::TreeNodeSelector::TreeNodeSelector(const char* name, TreeNode::WPtr const& parent):
    GraphAction (name, parent),
    _pImpl      (new PImpl)
{
}

scn::TreeNodeSelector::~TreeNodeSelector()
{
    delete _pImpl;
}

// virtual
void
scn::TreeNodeSelector::handleParmEvent(const xchg::ParmEvent& evt)
{
    GraphAction::handleParmEvent(evt);
    //---
    _pImpl->handleParmEvent(*this, evt);
}

// virtual
void
scn::TreeNodeSelector::handleSceneEvent(const xchg::SceneEvent& evt)
{
    TreeNode::handleSceneEvent(evt);
    //---
    _pImpl->handleSceneEvent(*this, evt);
}

// virtual
bool
scn::TreeNodeSelector::isParameterWritable(unsigned int parmId) const
{
    if (isParameterRelevantForClass(parmId))
        return parmId != parm::selection().id();
    return GraphAction::isParameterWritable(parmId);
}

// virtual
bool
scn::TreeNodeSelector::isParameterRelevant(unsigned int parmId)const
{
    return isParameterRelevantForClass(parmId) || GraphAction::isParameterRelevant(parmId);
}

bool
scn::TreeNodeSelector::isParameterRelevantForClass(unsigned int parmId) const
{
    return
        parmId == parm::regex       ().id() ||
        parmId == parm::selection   ().id();
}
