// ========================================================================= //
// Copyright 2016-2018 SUPAMONKS_STUDIO                                      //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
//                                                                           //
// ========================================================================= //
#include <sstream>
#include <json/json.h>

#include <smks/sys/Exception.hpp>
#include <smks/xchg/cfg.hpp>
#include "smks/scn/Scene.hpp"

namespace smks { namespace scn
{
    struct Scene::Config::PImpl
    {
        bool  withViewport;
        bool  withRendering;
        bool  cacheAnim;
        float defaultPointWidth;
        float defaultCurveWidth;
    };
}
}

using namespace smks;

void
scn::Scene::Config::setDefaults()
{
    _pImpl->withViewport      = true;
    _pImpl->withRendering     = true;
    _pImpl->cacheAnim         = true;
    _pImpl->defaultPointWidth = 1e-2f;
    _pImpl->defaultCurveWidth = 1e-2f;
}

scn::Scene::Config::Config():
    _pImpl(new PImpl())
{
    setDefaults();
}

scn::Scene::Config::Config(const Config& other):
    _pImpl(new PImpl())
{
    *_pImpl = *other._pImpl;
}

scn::Scene::Config&
scn::Scene::Config::operator=(const Config& other)
{
    *_pImpl = *other._pImpl;
    return *this;
}

// explicit
scn::Scene::Config::Config(const char* json):
    _pImpl(new PImpl())
{
    setDefaults();

    if (strlen(json) == 0)
        return;

    Json::Value  root;
    Json::Reader reader;

    if (!reader.parse(json, root))
    {
        std::stringstream sstr;
        sstr
            << "Failed to parse Json configuration: " << reader.getFormatedErrorMessages();
        throw sys::Exception(sstr.str().c_str(), "Scene Configuration");
    }

    _pImpl->cacheAnim         = root.get(xchg::cfg::cache_anim,     _pImpl->cacheAnim)     .asBool();
    _pImpl->withViewport      = root.get(xchg::cfg::with_viewport,  _pImpl->withViewport)  .asBool();
    _pImpl->withRendering     = root.get(xchg::cfg::with_rendering, _pImpl->withRendering) .asBool();
    _pImpl->defaultPointWidth = static_cast<float>(root.get(xchg::cfg::default_point_width, _pImpl->defaultPointWidth) .asDouble());
    _pImpl->defaultCurveWidth = static_cast<float>(root.get(xchg::cfg::default_curve_width, _pImpl->defaultCurveWidth) .asDouble());

    if (_pImpl->defaultPointWidth < 1e-6f)
        throw sys::Exception(
            "Default point width must be positive.",
            "Scene Configuration");
    if (_pImpl->defaultCurveWidth < 1e-6f)
        throw sys::Exception(
            "Default curve width must be positive.",
            "Scene Configuration");
}

scn::Scene::Config::~Config()
{
    delete _pImpl;
}

bool  scn::Scene::Config::withViewport() const      { return _pImpl->withViewport; }
bool  scn::Scene::Config::withRendering() const     { return _pImpl->withRendering; }
bool  scn::Scene::Config::cacheAnim() const         { return _pImpl->cacheAnim; }
float scn::Scene::Config::defaultPointWidth() const { return _pImpl->defaultPointWidth; }
float scn::Scene::Config::defaultCurveWidth() const { return _pImpl->defaultCurveWidth; }
