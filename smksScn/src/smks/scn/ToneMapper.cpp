// ========================================================================= //
// Copyright 2016-2018 SUPAMONKS_STUDIO                                      //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
//                                                                           //
// ========================================================================= //
#include <smks/sys/Exception.hpp>
#include <smks/parm/BuiltIns.hpp>
#include <smks/xchg/NodeCode.hpp>
#include <smks/xchg/NodeCodes.hpp>

#include "smks/scn/ToneMapper.hpp"

using namespace smks;

// static
scn::ToneMapper::Ptr
scn::ToneMapper::create(const char*             code,
                        const char*             name,
                        TreeNode::WPtr const&   parent)
{
    if (code == nullptr ||
        strlen(code) == 0)
        throw sys::Exception("Invalid code.", "Tone Mapper Creation");

    Ptr ptr(new ToneMapper(name, parent));
    ptr->build(ptr);
    ptr->setParameter<std::string>(parm::code(), code, parm::SKIPS_RESTRAINTS);
    return ptr;
}

// virtual
bool
scn::ToneMapper::isParameterWritable(unsigned int parmId) const
{
    const unsigned int code = getCode();
    if (code == xchg::code::gammaCorrection().id())
    {
        if (parmId == parm::gamma().id())
            return true;
    }
    else if (code == xchg::code::ocio().id())
    {
        if (parmId == parm::config          ().id() ||
            parmId == parm::inputColorspace ().id() ||
            parmId == parm::displayName     ().id() ||
            parmId == parm::displayViewName ().id() ||
            parmId == parm::exposureFstops  ().id())
            return true;
    }
    else if (code == xchg::code::filmic().id())
    {
        if (parmId == parm::contrastLevel   ().id() ||
            parmId == parm::exposureFstops  ().id())
            return true;
    }
    return RtNode::isParameterWritable(parmId);
}

// virtual
bool
scn::ToneMapper::isParameterRelevant(unsigned int parmId)const
{
    return isParameterRelevantForClass(parmId) || RtNode::isParameterRelevant(parmId);
}

bool
scn::ToneMapper::isParameterRelevantForClass(unsigned int parmId) const
{
    if (parmId == parm::code().id())
        return true;

    const unsigned int code = getCode();
    if (code == xchg::code::gammaCorrection().id())
    {
        return parmId == parm::gamma().id();
    }
    else if (code == xchg::code::ocio().id())
    {
        return
            parmId == parm::config          ().id() ||
            parmId == parm::inputColorspace ().id() ||
            parmId == parm::displayName     ().id() ||
            parmId == parm::displayViewName ().id() ||
            parmId == parm::exposureFstops  ().id();
    }
    else if (code == xchg::code::filmic().id())
    {
        return
            parmId == parm::contrastLevel   ().id() ||
            parmId == parm::exposureFstops  ().id();
    }
    return false;
}
