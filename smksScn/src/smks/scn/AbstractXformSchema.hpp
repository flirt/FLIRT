// ========================================================================= //
// Copyright 2016-2018 SUPAMONKS_STUDIO                                      //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
//                                                                           //
// ========================================================================= //
#pragma once

#include <smks/math/types.hpp>
#include "AbstractObjectSchema.hpp"

namespace Imath
{
    template <class T> class Matrix44;
}

namespace smks
{
    namespace parm
    {
        class Any;
    }

    namespace scn
    {
        class Xform;

        class AbstractXformSchema:
            public AbstractObjectSchema
        {
        public:
            inline const AbstractXformSchema*   asXformSchema() const   { return this; }
            inline AbstractXformSchema*         asXformSchema()         { return this; }

            virtual
            void
            synchronizeTransform(math::Affine3f&,
                                 bool& inheritsXforms) = 0;
        };
    }
}
