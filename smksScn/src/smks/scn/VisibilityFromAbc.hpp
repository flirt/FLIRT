// ========================================================================= //
// Copyright 2016-2018 SUPAMONKS_STUDIO                                      //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
//                                                                           //
// ========================================================================= //
#pragma once

#include <Alembic/AbcGeom/Visibility.h>

namespace smks { namespace scn
{
    class TreeNode;
    class SchemaBasedSceneObject;

    class VisibilityFromAbc
    {
    private:
        typedef std::weak_ptr<TreeNode> TreeNodeWPtr;
    private:
        //<! set at construction
        SchemaBasedSceneObject&                 _node;
        Alembic::Abc::ObjectReaderPtr           _object;                //<! do not own it
        const TreeNodeWPtr                      _archive;
        //<! set at initialization
        Alembic::AbcGeom::IVisibilityProperty   _visibility;
        bool                                    _initialized;
        char                                    _constantVisibility;    //<! -1 if animated
        int                                     _currentSampleId;
        //char                                  _currentVisibility;     //<! -1 if unspecified

    public:
        VisibilityFromAbc(SchemaBasedSceneObject&,
                          const Alembic::AbcGeom::IObject&,
                          TreeNodeWPtr const& archive);

        inline
        SchemaBasedSceneObject&
        node()
        {
            return _node;
        }
        inline
        const SchemaBasedSceneObject&
        node() const
        {
            return _node;
        }

        inline
        TreeNodeWPtr const&
        archive() const
        {
            return _archive;
        }

        void
        uninitialize();
        void
        initialize();

        inline
        bool
        initialized() const
        {
            return _initialized;
        }

        bool
        getTimeBounds(float& minTime, float& maxTime) const;

        bool
        setCurrentTime(float);

        void
        setForciblyVisible(char);
    private:
        char
        getConstantlyVisible() const;   // returns 1 if visible, 0 if invisible or -1 if animated

        void
        setCurrentlyVisible(char forciblyVisible=-1);

    public:
        //-------------------
        // parameter handling
        //-------------------
        bool
        isParameterRelevantForClass(unsigned int) const;
        char
        isParameterWritable(unsigned int) const;
        inline
        bool
        isParameterReadable(unsigned int) const
        {
            return true;
        }
        bool
        overrideParameterDefaultValue(unsigned int, parm::Any&) const;
    };
} // namespace scn
} // namespace smks
