// ========================================================================= //
// Copyright 2016-2018 SUPAMONKS_STUDIO                                      //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
//                                                                           //
// ========================================================================= //
#pragma once

#include <vector>

#include "smks/scn/PolyMesh.hpp"

namespace smks
{
    namespace scn
    {
        class PolyMesh::GeometrySanitizer
        {
        public:
            static
            size_t
            triangulateNgons(unsigned int               N,
                             const AbstractPolyMeshSchema*,
                             std::vector<unsigned int>& oCounts,
                             std::vector<unsigned int>& oIndices,           // (useful for vertex-varying properties)
                             std::vector<unsigned int>& fvrRedirection);    // oIndices[i] = indices[varyingRedir[i]] (useful for face-varying properties)

            static
            void
            normalizeVec3Array(float*, size_t num, size_t stride);

            static
            const float*
            computeVertexNormals(const AbstractPolyMeshSchema*,
                                 size_t&);

            static
            void
            computeVertexNormals(const AbstractPolyMeshSchema*,
                                 float* oBuffer,
                                 size_t oStride);

        private:
            static
            void
            computeVertexNormals(size_t         numPositions,
                                 const float*   positions,
                                 size_t         numFaces,
                                 const int*     counts,
                                 const int*     indices,
                                 bool           clearBuffer,
                                 float*         oBuffer,
                                 size_t         oStride);
        };
    }
}
