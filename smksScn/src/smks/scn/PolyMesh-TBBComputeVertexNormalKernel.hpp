// ========================================================================= //
// Copyright 2016-2018 SUPAMONKS_STUDIO                                      //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
//                                                                           //
// ========================================================================= //
#pragma once

#include <tbb/parallel_for.h>

namespace smks { namespace scn
{
    class TBBComputeVertexNormalKernel
    {
    private:
        const size_t        _numFaces;
        const size_t        _count;         // if constant face count
        const int *const    _counts;        // if varying face count
        const int *const    _firstIndex;    // if varying face count
        const int *const    _indices;
        const float *const  _positionBuffer;
        const size_t        _positionStride;

        float *const        _oBuffer;
        const size_t        _oStride;

    public:
        // varying face count
        TBBComputeVertexNormalKernel(size_t         numFaces,
                                     const int*     counts,
                                     const int*     firstIndex,
                                     const int*     indices,
                                     const float*   positionBuffer,
                                     size_t         positionStride,
                                     float*         oBuffer,
                                     size_t         oStride);
        // constant face count
        TBBComputeVertexNormalKernel(size_t         numFaces,
                                     size_t         count,
                                     const int*     indices,
                                     const float*   positionBuffer,
                                     size_t         positionStride,
                                     float*         oBuffer,
                                     size_t         oStride);
        TBBComputeVertexNormalKernel(const TBBComputeVertexNormalKernel&);

        void
        operator()(tbb::blocked_range<int> const&) const;

        void
        sequentialRun() const;

    private:
        void
        checkKernelCorrectness() const;

        static
        void
        cross(float*, const float*, const float*, const float*);
    };
}
}
