// ========================================================================= //
// Copyright 2016-2018 SUPAMONKS_STUDIO                                      //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
//                                                                           //
// ========================================================================= //
#include <Alembic/Abc/TypedArraySample.h>

#include <smks/sys/Exception.hpp>
#include <smks/xchg/ParmEvent.hpp>
#include <smks/xchg/BoundingBox.hpp>
#include <smks/parm/BuiltIn.hpp>
#include <smks/parm/BuiltIns.hpp>

#include "smks/scn/SchemaBasedSceneObject.hpp"
#include "SchemaBaseFromAbc.hpp"
#include "PolyMeshSchemaFromAbc.hpp"

namespace smks { namespace scn
{
    ////////////////////////////////////////////
    // class PolyMeshSchemaFromAbc::SampleHolder
    ////////////////////////////////////////////
    struct PolyMeshSchemaFromAbc::SampleHolder
    {
        Alembic::Abc::P3fArraySamplePtr     positions;
        Alembic::Abc::N3fArraySamplePtr     normals;
        Alembic::Abc::V3fArraySamplePtr     velocities;
        Alembic::Abc::V2fArraySamplePtr     texCoords;
        Alembic::Abc::Int32ArraySamplePtr   faceCounts;
        Alembic::Abc::Int32ArraySamplePtr   faceIndices;
        Alembic::Abc::UInt32ArraySamplePtr  texCoordsIndices;

    public:
        inline
        SampleHolder():
            positions       (nullptr),
            normals         (nullptr),
            velocities      (nullptr),
            texCoords       (nullptr),
            faceCounts      (nullptr),
            faceIndices     (nullptr),
            texCoordsIndices(nullptr)
        { }

        inline
        ~SampleHolder()
        {
            clear();
        }

        inline
        void
        clear()
        {
            positions           = nullptr;
            normals             = nullptr;
            velocities          = nullptr;
            texCoords           = nullptr;
            faceCounts          = nullptr;
            faceIndices         = nullptr;
            texCoordsIndices    = nullptr;
        }
    };
    ////////////////////////////////////////////
}
}

using namespace smks;

scn::PolyMeshSchemaFromAbc::PolyMeshSchemaFromAbc(SchemaBasedSceneObject&       node,
                                                  const Alembic::Abc::IObject&  object,
                                                  TreeNode::WPtr const&         archive):
    AbstractPolyMeshSchema  (),
    _object                 (object),
    _base                   (new SchemaBaseFromAbc(node, object, archive)),
    _mesh                   (),
    _numVertexBufferSamples (0),
    _numIndexBufferSamples  (0),
    _flags                  (0),
    _locks                  (new SampleHolder)
{
    if (!object.valid())
        throw sys::Exception("Incorrect Alembic object parameter.", "Alembic PolyMesh Schema Constructor");
    //---
    assert(numSamples() == 0);
}

scn::PolyMeshSchemaFromAbc::~PolyMeshSchemaFromAbc()
{
    uninitialize();
    delete _base;
    delete _locks;
}

scn::SchemaBasedSceneObject&
scn::PolyMeshSchemaFromAbc::node()
{
    return _base->node();
}
const scn::SchemaBasedSceneObject&
scn::PolyMeshSchemaFromAbc::node() const
{
    return _base->node();
}

const Alembic::AbcGeom::IPolyMeshSchema&
scn::PolyMeshSchemaFromAbc::getAbcSchema()
{
    if (_mesh.valid())
        return _mesh;

    assert(_object.valid());
    if (!Alembic::AbcGeom::IPolyMesh::matches(_object.getMetaData()))
    {
        std::stringstream sstr;
        sstr
            << "Alembic object '" << _object.getName() << "' is not a mesh.";
        throw sys::Exception(sstr.str().c_str(), "Mesh Alembic Schema Getter");
    }

    Alembic::AbcGeom::IPolyMesh mesh(_object, Alembic::Abc::kWrapExisting);

    if (!mesh.valid())
    {
        std::stringstream sstr;
        sstr
            << "Failed to get a valid Alembic mesh "
            << "from '" << _object.getName() << "'.";
        throw sys::Exception(sstr.str().c_str(), "Mesh Alembic Schema Getter");
    }

    _mesh = mesh.getSchema();
    assert(_mesh.valid());
    return _mesh;
}

void
scn::PolyMeshSchemaFromAbc::uninitialize()
{
    _mesh.reset();
    _numVertexBufferSamples = 0;
    _numIndexBufferSamples  = 0;

    {
        unsetParameter(node(), parm::numVertexBufferSamples(), parm::SKIPS_RESTRAINTS);
        unsetParameter(node(), parm::numIndexBufferSamples(), parm::SKIPS_RESTRAINTS);
    }
    _flags = 0;
    _locks->clear();
    //---
    _base->uninitialize();
    assert(numSamples() == 0);
}

size_t
scn::PolyMeshSchemaFromAbc::initialize()
{
    if (numSamples() > 0)
        return numSamples();

    uninitialize();

    const Alembic::AbcGeom::IPolyMeshSchema& mesh = getAbcSchema();
    const size_t numSamples = _base->initialize(mesh);
    //---
    if (numSamples > 0)
    {
        Alembic::AbcCoreAbstract::TimeSamplingPtr const& refTimeSampling = mesh.getTimeSampling();

        if (!mesh.getPositionsProperty().valid())
        {
            std::stringstream sstr;
            sstr
                << "Polymesh '" << _object.getName() << "' "
                << "must have a valid positions property.";
            throw sys::Exception(sstr.str().c_str(), "PolyMesh Schema Initialization");
        }

        if (!mesh.getFaceCountsProperty().valid())
        {
            std::stringstream sstr;
            sstr
                << "Polymesh '" << _object.getName() << "' "
                << "must have a valid face counts property.";
            throw sys::Exception(sstr.str().c_str(), "PolyMesh Schema Initialization");
        }

        if (!mesh.getFaceIndicesProperty().valid())
        {
            std::stringstream sstr;
            sstr
                << "Polymesh '" << _object.getName() << "' "
                << "must have a valid face indices property.";
            throw sys::Exception(sstr.str().c_str(), "PolyMesh Schema Initialization");
        }

        if (mesh.getPositionsProperty().getTimeSampling() != refTimeSampling)
        {
            std::stringstream sstr;
            sstr
                << "Polymesh '" << _object.getName() << "' "
                << "must have its positions property synchronized with its other properties.";
            throw sys::Exception(sstr.str().c_str(), "PolyMesh Schema Initialization");
        }

        const bool isTopoVarying = mesh.getTopologyVariance() == Alembic::AbcGeom::kHeterogeneousTopology ||
            mesh.getTopologyVariance() == Alembic::AbcGeom::kHeterogenousTopology;

        if (isTopoVarying)
        {
            if (mesh.getFaceCountsProperty().getTimeSampling() != refTimeSampling)
            {
                std::stringstream sstr;
                sstr
                    << "Polymesh '" << _object.getName() << "' "
                    << "must have its face counts property synchronized with its other properties.";
                throw sys::Exception(sstr.str().c_str(), "PolyMesh Schema Initialization");
            }
            if (mesh.getFaceIndicesProperty().getTimeSampling() != refTimeSampling)
            {
                std::stringstream sstr;
                sstr
                    << "Polymesh '" << _object.getName() << "' "
                    << "must have its face indices property synchronized with its other properties.";
                throw sys::Exception(sstr.str().c_str(), "PolyMesh Schema Initialization");
            }
        }

        assert(mesh.getPositionsProperty().valid());
        assert(mesh.getFaceCountsProperty().valid());
        assert(mesh.getFaceIndicesProperty().valid());
        assert(mesh.getNumSamples() == mesh.getPositionsProperty().getNumSamples());

        {
            _numVertexBufferSamples = mesh.getPositionsProperty().getNumSamples();
            _numIndexBufferSamples  = mesh.getTopologyVariance() == Alembic::AbcGeom::kHeterogeneousTopology ||
                mesh.getTopologyVariance() == Alembic::AbcGeom::kHeterogenousTopology
                ? mesh.getFaceIndicesProperty().getNumSamples()
                : 1;
            setParameter<size_t>(node(), parm::numVertexBufferSamples(), _numVertexBufferSamples, parm::SKIPS_RESTRAINTS);
            setParameter<size_t>(node(), parm::numIndexBufferSamples(), _numIndexBufferSamples, parm::SKIPS_RESTRAINTS);
        }

        _flags |= Property::PROP_POSITION;
        if (checkNormals(mesh))
            _flags |= Property::PROP_NORMAL;
        if (checkTexCoords(mesh))
            _flags |= Property::PROP_TEXCOORD;
        if (checkMotions(mesh))
            _flags |= Property::PROP_VELOCITY;
        if (checkBounds(mesh))
            _flags |= Property::PROP_BOUNDS;
    }
    return numSamples;
}

void
scn::PolyMeshSchemaFromAbc::uninitializeUserAttribs()
{
    _base->uninitializeUserAttribs();
}

void
scn::PolyMeshSchemaFromAbc::initializeUserAttribs()
{
    _base->initializeUserAttribs(getAbcSchema());
}

//-----------------------------------
// parameter handling
//-----------------------------------
bool
scn::PolyMeshSchemaFromAbc::isParameterRelevantForClass(unsigned int parmId) const
{
    return
        _base->isParameterRelevantForClass(parmId)      ||
        parmId == parm::boundingBox             ().id() ||
        parmId == parm::numVertexBufferSamples  ().id() ||
        parmId == parm::numIndexBufferSamples   ().id() ||
        parmId == parm::subdivisionLevel        ().id() ||
        parmId == parm::glVertices              ().id() ||
        parmId == parm::glPositions             ().id() ||
        parmId == parm::glNormals               ().id() ||
        parmId == parm::glTexcoords             ().id() ||
        parmId == parm::glIndices               ().id() ||
        parmId == parm::glCount                 ().id() ||
        parmId == parm::glWireIndices           ().id() ||
        parmId == parm::rtVertices              ().id() ||
        parmId == parm::rtFaceVertices          ().id() ||
        parmId == parm::rtPositions             ().id() ||
        parmId == parm::rtNormals               ().id() ||
        parmId == parm::rtTexcoords             ().id() ||
        parmId == parm::rtTangentsX             ().id() ||
        parmId == parm::rtTangentsY             ().id() ||
        parmId == parm::rtIndices               ().id() ||
        parmId == parm::rtCount                 ().id() ||
        parmId == parm::rtCounts                ().id() ||
        parmId == parm::rtSubdivisionMethod     ().id();
}
bool
scn::PolyMeshSchemaFromAbc::isParameterReadable(unsigned int parmId) const
{
    return _base->isParameterReadable(parmId);
}
char
scn::PolyMeshSchemaFromAbc::isParameterWritable(unsigned int parmId) const
{
    char ret = -1;
    if (isParameterRelevantForClass(parmId))
    {
        ret = _base->isParameterWritable(parmId);
        if (ret == -1)
            ret =
                parmId == parm::subdivisionLevel    ().id() ||
                parmId == parm::rtSubdivisionMethod ().id()
                ? 1
                : 0;
    }
    return ret;
}
bool
scn::PolyMeshSchemaFromAbc::overrideParameterDefaultValue(unsigned int parmId, parm::Any& defaultValue) const
{
    return _base->overrideParameterDefaultValue(parmId, defaultValue);
}
void
scn::PolyMeshSchemaFromAbc::handle(const xchg::ParmEvent& evt)
{
    _base->handle(evt);
}
bool
scn::PolyMeshSchemaFromAbc::mustSendToScene(const xchg::ParmEvent& evt) const
{
    return _base->mustSendToScene(evt);
}
//-----------------------------------

bool //<! return current visibility
scn::PolyMeshSchemaFromAbc::setCurrentTime(float time)
{
    return _base->setCurrentTime(time);
}

size_t
scn::PolyMeshSchemaFromAbc::numSamples() const
{
    return _base->numSamples();
}

int
scn::PolyMeshSchemaFromAbc::currentSampleIdx() const
{
    return _base->currentSampleIdx();
}

int
scn::PolyMeshSchemaFromAbc::currentVertexSampleIdx() const
{
    return currentSampleIdx();
}

int
scn::PolyMeshSchemaFromAbc::currentIndexSampleIdx() const
{
    const int vertexSampleId = currentVertexSampleIdx();

    return vertexSampleId >= 0
        ? (_numIndexBufferSamples > 0
        ? std::min(vertexSampleId, static_cast<int>(_numIndexBufferSamples - 1))
        : 0)
        : -1;
}

void
scn::PolyMeshSchemaFromAbc::getStoredTimes(xchg::Vector<float>& ret) const
{
    _base->getStoredTimes(ret);
}

bool
scn::PolyMeshSchemaFromAbc::has(Property p) const
{
    return (_flags & p) != 0;
}

const float*
scn::PolyMeshSchemaFromAbc::lockPositions(size_t& numPositions) const
{
    return lockPositions(currentVertexSampleIdx(), numPositions);
}

const float*
scn::PolyMeshSchemaFromAbc::lockPositions(int       vertexSampleId,
                                          size_t&   numPositions) const
{
    numPositions = 0;

    assert(numSamples() > 0);
    assert(vertexSampleId >= 0 && vertexSampleId < static_cast<int>(numVertexBufferSamples()));
    assert(_mesh.getPositionsProperty().valid());

    if (_locks->positions)
    {
        std::stringstream sstr;
        sstr
            << "Previous positions sample has not been properly unlocked "
            << "for polymesh '" << _object.getName() << "'.";
        throw sys::Exception(sstr.str().c_str(), "Polymesh Positions Getter");
    }

    Alembic::Abc::P3fArraySamplePtr const& data = _mesh.getPositionsProperty()
        .getValue(Alembic::Abc::ISampleSelector(static_cast<Alembic::Abc::index_t>(vertexSampleId)));

    Alembic::AbcCoreAbstract::ArraySampleKey const& key = data->getKey();
    if (data == nullptr ||
        data->size() == 0)
        return nullptr;

    _locks->positions   = data;
    numPositions        = _locks->positions->size();

    return reinterpret_cast<const float*>(_locks->positions->getData());
}

void
scn::PolyMeshSchemaFromAbc::unlockPositions() const
{
    _locks->positions   = nullptr;
}

const float*
scn::PolyMeshSchemaFromAbc::lockNormals(size_t& numNormals) const
{
    return lockNormals(currentVertexSampleIdx(), numNormals);
}

const float*
scn::PolyMeshSchemaFromAbc::lockNormals(int     vertexSampleId,
                                        size_t& numNormals) const
{
    numNormals = 0;

    assert(numSamples() > 0);
    assert(vertexSampleId >= 0 && vertexSampleId < static_cast<int>(numVertexBufferSamples()));

    if (!has(Property::PROP_NORMAL))
        return nullptr;
    if (_locks->normals)
    {
        std::stringstream sstr;
        sstr
            << "Previous normals sample has not been properly unlocked "
            << "for polymesh '" << _object.getName() << "'.";
        throw sys::Exception(sstr.str().c_str(), "Polymesh Normals Getter");
    }

    assert(_mesh.getNormalsParam().valid());
    assert(_mesh.getNormalsParam().getValueProperty().valid());
    assert(_mesh.getNormalsParam().getScope() == Alembic::AbcGeom::kVertexScope || _mesh.getNormalsParam().getScope() == Alembic::AbcGeom::kFacevaryingScope);

    Alembic::Abc::N3fArraySamplePtr const& data = _mesh.getNormalsParam().getValueProperty()
        .getValue(Alembic::Abc::ISampleSelector(static_cast<Alembic::Abc::index_t>(vertexSampleId)));

    if (data == nullptr ||
        data->size() == 0)
        return nullptr;

    _locks->normals = data;
    numNormals      = _locks->normals->size();

    return reinterpret_cast<const float*>(data->getData());
}

void
scn::PolyMeshSchemaFromAbc::unlockNormals() const
{
    _locks->normals = nullptr;
}

const float*
scn::PolyMeshSchemaFromAbc::lockVelocities(size_t& numVelocities) const
{
    return lockVelocities(currentVertexSampleIdx(), numVelocities);
}

const float*
scn::PolyMeshSchemaFromAbc::lockVelocities(int      vertexSampleId,
                                           size_t&  numVelocities) const
{
    numVelocities = 0;

    assert(numSamples() > 0);
    assert(vertexSampleId >= 0 && vertexSampleId < static_cast<int>(numVertexBufferSamples()));

    if (!has(Property::PROP_VELOCITY))
        return nullptr;
    if (_locks->velocities)
    {
        std::stringstream sstr;
        sstr
            << "Previous velocities sample has not been properly unlocked "
            << "for polymesh '" << _object.getName() << "'.";
        throw sys::Exception(sstr.str().c_str(), "Polymesh Velocities Getter");
    }

    assert(_mesh.getVelocitiesProperty().valid());

    Alembic::Abc::V3fArraySamplePtr const& data = _mesh.getVelocitiesProperty()
        .getValue(Alembic::Abc::ISampleSelector(static_cast<Alembic::Abc::index_t>(vertexSampleId)));

    if (data == nullptr ||
        data->size() == 0)
        return nullptr;

    _locks->velocities  = data;
    numVelocities       = _locks->velocities->size();

    return reinterpret_cast<const float*>(data->getData());
}

void
scn::PolyMeshSchemaFromAbc::unlockVelocities() const
{
    _locks->velocities  = nullptr;
}

const float*
scn::PolyMeshSchemaFromAbc::lockTexCoords(size_t& numTexCoords) const
{
    return lockTexCoords(currentVertexSampleIdx(), numTexCoords);
}

const float*
scn::PolyMeshSchemaFromAbc::lockTexCoords(int       vertexSampleId,
                                          size_t&   numTexCoords) const
{
    numTexCoords = 0;

    assert(numSamples() > 0);
    assert(vertexSampleId >= 0 && vertexSampleId < static_cast<int>(numVertexBufferSamples()));

    if (!has(Property::PROP_TEXCOORD))
        return nullptr;
    if (_locks->texCoords)
    {
        std::stringstream sstr;
        sstr
            << "Previous texture coordinate sample has not been properly unlocked "
            << "for polymesh '" << _object.getName() << "'.";
        throw sys::Exception(sstr.str().c_str(), "Polymesh TexCoord Getter");
    }

    assert(_mesh.getUVsParam().valid());
    assert(_mesh.getUVsParam().getValueProperty().valid());
    assert(_mesh.getUVsParam().getScope() == Alembic::AbcGeom::kFacevaryingScope);

    Alembic::Abc::V2fArraySamplePtr const& data = _mesh.getUVsParam().getValueProperty()
        .getValue(Alembic::Abc::ISampleSelector(static_cast<Alembic::Abc::index_t>(vertexSampleId)));

    if (data == nullptr ||
        data->size() == 0)
        return nullptr;

    _locks->texCoords   = data;
    numTexCoords        = data->size();

    return reinterpret_cast<const float*>(data->getData());
}

void
scn::PolyMeshSchemaFromAbc::unlockTexCoords() const
{
    _locks->texCoords   = nullptr;
}

void
scn::PolyMeshSchemaFromAbc::getSelfBounds(xchg::BoundingBox& bounds) const
{
    getSelfBounds(_base->currentSampleIdx(), bounds);
}

void
scn::PolyMeshSchemaFromAbc::getSelfBounds(int                   sampleId,
                                          xchg::BoundingBox&    bounds) const
{
    bounds.clear();

    assert(numSamples() > 0);
    assert(sampleId >= 0 && sampleId < static_cast<int>(numSamples()));

    if (!has(Property::PROP_BOUNDS))
        return;

    assert(_mesh.getSelfBoundsProperty().valid());

    const Alembic::Abc::Box3d& box = _mesh.getSelfBoundsProperty()
        .getValue(Alembic::Abc::ISampleSelector(static_cast<Alembic::Abc::index_t>(sampleId)));

    if (box.isEmpty())
        return;

    const Alembic::Abc::V3d& min = box.min;
    const Alembic::Abc::V3d& max = box.max;

    bounds.minX = static_cast<float>(min.x);
    bounds.minY = static_cast<float>(min.y);
    bounds.minZ = static_cast<float>(min.z);
    bounds.maxX = static_cast<float>(max.x);
    bounds.maxY = static_cast<float>(max.y);
    bounds.maxZ = static_cast<float>(max.z);
}

size_t
scn::PolyMeshSchemaFromAbc::getNumVertices() const
{
    return getNumVertices(currentVertexSampleIdx());
}

size_t
scn::PolyMeshSchemaFromAbc::getNumVertices(int vertexSampleId) const
{
    assert(numSamples() > 0);
    assert(vertexSampleId >= 0 && vertexSampleId < static_cast<int>(numVertexBufferSamples()));
    assert(_mesh.getPositionsProperty().valid());

    return _mesh.getPositionsProperty()
            .getValue(Alembic::Abc::ISampleSelector(static_cast<Alembic::Abc::index_t>(vertexSampleId)))
            ->size();
}

const int*
scn::PolyMeshSchemaFromAbc::lockFaceCounts(size_t& numFaceCounts) const
{
    return lockFaceCounts(currentIndexSampleIdx(), numFaceCounts);
}

const int*
scn::PolyMeshSchemaFromAbc::lockFaceCounts(int      indexSampleId,
                                           size_t&  numFaceCounts) const
{
    numFaceCounts = 0;

    assert(numSamples() > 0);
    assert(indexSampleId >= 0 && indexSampleId < static_cast<int>(numIndexBufferSamples()));
    assert(_mesh.getFaceCountsProperty().valid());

    if (_locks->faceCounts)
    {
        std::stringstream sstr;
        sstr
            << "Previous face counts sample has not been properly unlocked "
            << "for polymesh '" << _object.getName() << "'.";
        throw sys::Exception(sstr.str().c_str(), "Polymesh Face Counts Getter");
    }

    Alembic::Abc::Int32ArraySamplePtr const& data = _mesh.getFaceCountsProperty()
        .getValue(Alembic::Abc::ISampleSelector(static_cast<Alembic::Abc::index_t>(indexSampleId)));

    if (data == nullptr ||
        data->size() == 0)
        return nullptr;

    _locks->faceCounts  = data;
    numFaceCounts       = _locks->faceCounts->size();

    return reinterpret_cast<const int*>(_locks->faceCounts->getData());
}

void
scn::PolyMeshSchemaFromAbc::unlockFaceCounts() const
{
    _locks->faceCounts  = nullptr;
}

const int*
scn::PolyMeshSchemaFromAbc::lockFaceIndices(size_t& numFaceIndices) const
{
    return lockFaceIndices(currentIndexSampleIdx(), numFaceIndices);
}

const int*
scn::PolyMeshSchemaFromAbc::lockFaceIndices(int     indexSampleId,
                                            size_t& numFaceIndices) const
{
    numFaceIndices = 0;

    assert(numSamples() > 0);
    assert(indexSampleId >= 0 && indexSampleId < static_cast<int>(numIndexBufferSamples()));
    assert(_mesh.getFaceIndicesProperty().valid());

    if (_locks->faceIndices)
    {
        std::stringstream sstr;
        sstr
            << "Previous face indices sample has not been properly unlocked "
            << "for polymesh '" << _object.getName() << "'.";
        throw sys::Exception(sstr.str().c_str(), "Polymesh Face Indices Getter");
    }

    Alembic::Abc::Int32ArraySamplePtr const& data = _mesh.getFaceIndicesProperty()
        .getValue(Alembic::Abc::ISampleSelector(static_cast<Alembic::Abc::index_t>(indexSampleId)));

    if (data == nullptr ||
        data->size() == 0)
        return nullptr;

    _locks->faceIndices = data;
    numFaceIndices      = _locks->faceIndices->size();

    return reinterpret_cast<const int*>(_locks->faceIndices->getData());
}

void
scn::PolyMeshSchemaFromAbc::unlockFaceIndices() const
{
    _locks->faceIndices = nullptr;
}

const unsigned int*
scn::PolyMeshSchemaFromAbc::lockTexCoordsIndices(size_t& numTexCoordsIndices) const
{
    return lockTexCoordsIndices(currentIndexSampleIdx(), numTexCoordsIndices);
}

const unsigned int*
scn::PolyMeshSchemaFromAbc::lockTexCoordsIndices(int        indexSampleId,
                                                 size_t&    numTexCoordsIndices) const
{
    numTexCoordsIndices = 0;

    assert(numSamples() > 0);
    assert(indexSampleId >= 0 && indexSampleId < static_cast<int>(numIndexBufferSamples()));

    if (!has(Property::PROP_TEXCOORD))
        return nullptr;
    if (_locks->texCoordsIndices)
    {
        std::stringstream sstr;
        sstr
            << "Previous texture coordinate indices sample has not been properly unlocked "
            << "for polymesh '" << _object.getName() << "'.";
        throw sys::Exception(sstr.str().c_str(), "Polymesh Texture Coordinate Indices Getter");
    }

    assert(_mesh.getUVsParam().valid());
    assert(_mesh.getUVsParam().getIndexProperty().valid());
    assert(_mesh.getUVsParam().getScope() == Alembic::AbcGeom::kFacevaryingScope);

    Alembic::Abc::UInt32ArraySamplePtr const& data = _mesh.getUVsParam().getIndexProperty()
        .getValue(Alembic::Abc::ISampleSelector(static_cast<Alembic::Abc::index_t>(indexSampleId)));

    if (data == nullptr ||
        data->size() == 0)
        return nullptr;

    _locks->texCoordsIndices    = data;
    numTexCoordsIndices         = _locks->texCoordsIndices->size();

    return reinterpret_cast<const unsigned int*>(_locks->texCoordsIndices->getData());
}

void
scn::PolyMeshSchemaFromAbc::unlockTexCoordsIndices() const
{
    _locks->texCoordsIndices    = nullptr;
}

// static
bool
scn::PolyMeshSchemaFromAbc::checkBounds(const Alembic::AbcGeom::IPolyMeshSchema& mesh)
{
    if (!mesh.getSelfBoundsProperty().valid() ||
        !mesh.getPositionsProperty().valid())
        return false;
    else if (mesh.getSelfBoundsProperty().getTimeSampling() != mesh.getPositionsProperty().getTimeSampling())
        return false;
    else
        return true;
}

// static
bool
scn::PolyMeshSchemaFromAbc::checkNormals(const Alembic::AbcGeom::IPolyMeshSchema& mesh)
{
    if (!mesh.getNormalsParam().valid() ||
        !mesh.getPositionsProperty().valid())
        return false;
    else if (mesh.getNormalsParam().getScope() != Alembic::AbcGeom::kVertexScope &&
        mesh.getNormalsParam().getScope() != Alembic::AbcGeom::kFacevaryingScope)
    {
        std::cerr
            << "Polymesh '" << mesh.getObject().getName() << "' "
            << "must have its normals specified per vertex or per face "
            << "(found " << std::to_string(mesh.getNormalsParam().getScope()) << "). "
            << "Skipping normals." << std::endl;
        return false;
    }
    else if (mesh.getNormalsParam().getTimeSampling() != mesh.getPositionsProperty().getTimeSampling())
    {
        std::cerr
            << "Polymesh '" << mesh.getObject().getName() << "' "
            << "must have its normals property synchronized with its positions property. "
            << "Skipping normals." << std::endl;
        return false;
    }
    else
        return true;
}

// static
bool
scn::PolyMeshSchemaFromAbc::checkMotions(const Alembic::AbcGeom::IPolyMeshSchema& mesh)
{
    if (!mesh.getVelocitiesProperty().valid() ||
        !mesh.getPositionsProperty().valid())
        return false;
    else if (mesh.getVelocitiesProperty().getTimeSampling() != mesh.getPositionsProperty().getTimeSampling())
    {
        std::cerr
            << "Polymesh '" << mesh.getObject().getName() << "' "
            << "must have its velocities property synchronized with its positions property. "
            << "Skipping velocities." << std::endl;
        return false;
    }
    else
        return true;
}

// static
bool
scn::PolyMeshSchemaFromAbc::checkTexCoords(const Alembic::AbcGeom::IPolyMeshSchema& mesh)
{
    if (!mesh.getUVsParam().valid() ||
        !mesh.getUVsParam().getValueProperty().valid() ||
        !mesh.getUVsParam().getIndexProperty().valid() ||
        !mesh.getPositionsProperty().valid() ||
        !mesh.getFaceIndicesProperty().valid())
        return false;
    else if (mesh.getUVsParam().getScope() != Alembic::AbcGeom::kFacevaryingScope)
    {
        std::cerr << "Polymesh '" << mesh.getObject().getName()
            << "' must have its UV coordinates specified per face "
            << "(found " << std::to_string(mesh.getUVsParam().getScope()) << "). "
            << "Skipping texture coordinates." << std::endl;
        return false;
    }
    else if (mesh.getUVsParam().getValueProperty().getTimeSampling() != mesh.getPositionsProperty().getTimeSampling())
    {
        std::cerr
            << "Polymesh '" << mesh.getObject().getName() << "' "
            << "must have its UV coordinates property synchronized with its positions property. "
            << "Skipping texture coordinates." << std::endl;
        return false;
    }
    else if (mesh.getUVsParam().getIndexProperty().getTimeSampling() != mesh.getFaceIndicesProperty().getTimeSampling())
    {
        std::cerr
            << "Polymesh '" << mesh.getObject().getName() << "' "
            << "must have its UV coordinate indices property synchronized with its face "
            << "indices property. Skipping texture coordinates." << std::endl;
        return false;
    }
    else
        return true;
}
