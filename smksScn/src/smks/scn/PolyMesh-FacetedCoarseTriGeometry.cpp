// ========================================================================= //
// Copyright 2016-2018 SUPAMONKS_STUDIO                                      //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
//                                                                           //
// ========================================================================= //

#include <smks/sys/Log.hpp>
#include <smks/sys/Exception.hpp>

#include <smks/parm/Container.hpp>
#include <smks/util/math/array.hpp>

#include "PolyMesh-FacetedCoarseTriGeometry.hpp"
#include "PolyMesh-GeometrySanitizer.hpp"
#include "AbstractPolyMeshSchema.hpp"

using namespace Alembic;
using namespace smks;

// static
scn::PolyMesh::FacetedCoarseTriGeometry::Ptr
scn::PolyMesh::FacetedCoarseTriGeometry::create()
{
    Ptr ptr (new FacetedCoarseTriGeometry);

    return ptr;
}

scn::PolyMesh::FacetedCoarseTriGeometry::FacetedCoarseTriGeometry():
    PolyMesh::AbstractGeometry  (),
    _schema                     (nullptr),
    _faceVertexAttribs          (),
    _triangles                  (),
    _wireIndices                (),
    _faceVertices               ()
{ }

scn::PolyMesh::FacetedCoarseTriGeometry::~FacetedCoarseTriGeometry()
{
    dispose();
}

// virtual
void
scn::PolyMesh::FacetedCoarseTriGeometry::dispose()
{
    _schema = nullptr;

    _faceVertexAttribs  .clear();
    _triangles          .dispose();
    _wireIndices        .dispose();
    _faceVertices       .dispose();
}

// virtual
bool
scn::PolyMesh::FacetedCoarseTriGeometry::initialize(const AbstractPolyMeshSchema* schema, bool caching)
{
    assert(schema);
    dispose();
    _schema = schema;
    if (initialized())
    {
        if (!schema->has(Property::PROP_POSITION))
        {
            std::stringstream sstr;
            sstr
                << "No position information "
                << "for mesh '" << _schema->node().name() << "'.";
            throw sys::Exception(sstr.str().c_str(), "GL Geometry Initialization");
        }

        _faceVertexAttribs.append(Property::PROP_POSITION,  3);
        _faceVertexAttribs.append(Property::PROP_NORMAL,        3); // will manually compute them
        if (schema->has(Property::PROP_TEXCOORD))
            _faceVertexAttribs.append(Property::PROP_TEXCOORD,  2);
        if (schema->has(Property::PROP_VELOCITY))
            _faceVertexAttribs.append(Property::PROP_VELOCITY,  3);

        _triangles      .initialize(caching ? schema->numIndexBufferSamples()   : 0);
        _wireIndices    .initialize(caching ? schema->numIndexBufferSamples()   : 0);
        _faceVertices   .initialize(caching ? schema->numVertexBufferSamples()  : 0);

        return true;
    }
    else
        return false;
}

bool
scn::PolyMesh::FacetedCoarseTriGeometry::initialized() const
{
    return _schema && _schema->numSamples() > 0;
}

const scn::Triangles*
scn::PolyMesh::FacetedCoarseTriGeometry::getTriangles()
{
    if (!initialized())
        return nullptr;

    const int               indexSampleId   = _schema->currentIndexSampleIdx();
    const Triangles *const  cache           = _triangles.get(indexSampleId);
    if (cache)
        return cache;

    // actual computation
    std::vector<unsigned int>   counts;
    Triangles                   triangles;

    GeometrySanitizer::triangulateNgons(3, _schema, counts, triangles.vtxRedir, triangles.fvrRedir);
    assert(util::math::allEqual<unsigned int>(counts, 3));
    assert(triangles.vtxRedir.size() == triangles.fvrRedir.size());

    triangles.indices.resize(triangles.vtxRedir.size());
    for (unsigned int i = 0; i < triangles.indices.size(); ++i)
        triangles.indices[i] = i; // dummy index buffer

    // cache computation
    return _triangles.set(indexSampleId, triangles);
}

// virtual
const float*
scn::PolyMesh::FacetedCoarseTriGeometry::getFaceVertices(size_t&            numFaceVertices,
                                                         BufferAttributes&  attribs)
{
    numFaceVertices = 0;
    attribs.clear();

    if (!initialized())
        return nullptr;

    attribs             = _faceVertexAttribs;
    const size_t stride = _faceVertexAttribs.stride();

    const int                   vertexSampleId  = _schema->currentVertexSampleIdx();
    const std::vector<float>*   cache           = _faceVertices.get(vertexSampleId);
    if (cache)
    {
        assert(stride > 0 && cache->size() % stride == 0);
        numFaceVertices = cache->size() / stride;
        return numFaceVertices > 0
            ? &cache->front()
            : nullptr;
    }

    // actual computation
    const Triangles* triangles = getTriangles();

    if (triangles == nullptr ||
        triangles->indices.empty() ||
        triangles->vtxRedir.empty() ||
        triangles->fvrRedir.empty())
        return nullptr;

    const std::vector<unsigned int>&    vtxRedir    = triangles->vtxRedir;
    const std::vector<unsigned int>&    fvrRedir    = triangles->fvrRedir;

    numFaceVertices = vtxRedir.size();

    assert(fvrRedir.size() == numFaceVertices);
    assert(_faceVertexAttribs.has(Property::PROP_POSITION));
    assert(_faceVertexAttribs.has(Property::PROP_NORMAL));

    std::vector<float>  faceVertices(numFaceVertices * stride, 0.0f);

    // if normals not already stored in archive, then compute them.
    // note : must come first in order not to have schema's properties being already locked.
    bool            ownsNormals = false; // tell whether normals have been locally computed
    size_t          numNormals  = 0;
    const float*    normals     = nullptr;

    if (_schema->has(Property::PROP_NORMAL))
        normals     = _schema->lockNormals(numNormals);
    else
    {
        normals     = GeometrySanitizer::computeVertexNormals(_schema, numNormals);
        ownsNormals = true;
    }

    // get vertex positions
    size_t          numPositions    = 0;
    const float*    positions       = _schema->lockPositions(numPositions);

    // get texture coordinates
    size_t              numTexCoords        = 0;
    size_t              numTexCoordsIndices = 0;
    const float*        texCoords           = nullptr;
    const unsigned int* texCoordsIndices    = nullptr;

    if (_schema->has(Property::PROP_TEXCOORD))
    {
        texCoords           = _schema->lockTexCoords(numTexCoords);
        texCoordsIndices    = _schema->lockTexCoordsIndices(numTexCoordsIndices);
    }

    // get velocity vectors
    size_t          numVelocities   = 0;
    const float*    velocities      = nullptr;

    if (_schema->has(Property::PROP_VELOCITY))
        velocities  = _schema->lockVelocities(numVelocities);

    FLIRT_LOG(LOG(DEBUG)
        << "mesh '" << _schema->node().name() << "'"
        //<< "\n\t- # varying-to-vertex redirection = " << numVaryings
        << "\n\t- # positions = " << numPositions
        << "\n\t- # normals = " << numNormals
        << "\n\t- # texcoords = " << numTexCoords
        << "\n\t- # texcoord indices = " << numTexCoordsIndices
        << "\n\t- # velocities = " << numVelocities;)

    //assert(numPositions > 0   && numPositions <= numVertices);
    //assert(numNormals == 0    || numNormals == numPositions || numNormals == numVaryings);
    //assert(numVelocities == 0 || numVelocities == numPositions);
    //assert(numTexCoordsIndices == 0 || numTexCoordsIndices == numVaryings);

    const bool perVertexNormals = (numNormals == numPositions);

    // fill vertex buffer (and triangulate with the topology's corresponding face indices)
    float*  oPositions  = &faceVertices.front() + _faceVertexAttribs.offset(Property::PROP_POSITION);
    float*  oNormals    = &faceVertices.front() + _faceVertexAttribs.offset(Property::PROP_NORMAL);
    float*  oTexCoords  = &faceVertices.front() + _faceVertexAttribs.offset(Property::PROP_TEXCOORD);
    float*  oVelocities = &faceVertices.front() + _faceVertexAttribs.offset(Property::PROP_VELOCITY);

    for (size_t i = 0; i < numFaceVertices; ++i)
    {
        {
            memcpy(oPositions, positions + 3 * vtxRedir[i], sizeof(float) * 3);
        }
        if (numNormals > 0)
        {
            if (perVertexNormals)
                memcpy(oNormals, normals + 3 * vtxRedir[i], sizeof(float) * 3);
            else
                memcpy(oNormals, normals + 3 * fvrRedir[i], sizeof(float) * 3);
        }
        if (numTexCoords)
        {
            //assert(numTexCoordsIndices == numVaryings);
            memcpy(oTexCoords, texCoords + (texCoordsIndices[fvrRedir[i]] << 1), sizeof(float) << 1);
        }
        if (numVelocities > 0)
        {
            memcpy(oVelocities, velocities + 3 * vtxRedir[i], sizeof(float) * 3);
        }

        oPositions  += _faceVertexAttribs.stride();
        oNormals    += _faceVertexAttribs.stride();
        oTexCoords  += _faceVertexAttribs.stride();
        oVelocities += _faceVertexAttribs.stride();
    }

    // normalize normals stored in the vertex buffer
    GeometrySanitizer::normalizeVec3Array(
        &faceVertices.front() + _faceVertexAttribs.offset(Property::PROP_NORMAL),
        numFaceVertices,
        _faceVertexAttribs.stride());

    _schema->unlockPositions();
    if (ownsNormals)
        delete[] normals;
    else
        _schema->unlockNormals();
    _schema->unlockTexCoords();
    _schema->unlockTexCoordsIndices();
    _schema->unlockVelocities();

    // cache computed data
    _faceVertices.set(vertexSampleId, faceVertices);
    cache = _faceVertices.get(vertexSampleId);
    assert(cache);

    return !cache->empty()
        ? &cache->front()
        : nullptr;
}

// virtual
const unsigned int*
scn::PolyMesh::FacetedCoarseTriGeometry::getFaceCounts(size_t&  numFaces,
                                                       bool&    isConstant)
{
    size_t              numIndices  = 0;
    const unsigned int* indices     = getFaceIndices(numIndices);

    assert(numIndices % _FACE_COUNT == 0);
    numFaces    = numIndices / _FACE_COUNT;
    isConstant  = true;

    return &_FACE_COUNT;
}

// virtual
const unsigned int*
scn::PolyMesh::FacetedCoarseTriGeometry::getFaceIndices(size_t& numIndices)
{
    numIndices  = 0;

    assert(initialized());

    const Triangles* cache = getTriangles();

    if (cache == nullptr ||
        cache->indices.empty())
        return nullptr;

    numIndices  = cache->indices.size();

    return numIndices > 0
        ? &cache->indices.front()
        : nullptr;
}

// virtual
const unsigned int*
scn::PolyMesh::FacetedCoarseTriGeometry::getWireframeLines(size_t& numWireIndices)
{
    numWireIndices = 0;

    if (!initialized())
        return nullptr;

    const int                           indexSampleId   = _schema->currentIndexSampleIdx();
    const std::vector<unsigned int>*    cache           = _wireIndices.get(indexSampleId);
    if (cache)
    {
        assert((cache->size() % 2) == 0);
        numWireIndices = cache->size();
        return numWireIndices > 0
            ? &cache->front()
            : nullptr;
    }

    // actual computation
    std::vector<unsigned int> wireIndices;

    const Triangles* triangles = getTriangles();
    if (triangles == nullptr ||
        triangles->vtxRedir.empty())
        return nullptr;

    size_t      numFaces    = 0;
    size_t      numIndices  = 0;
    const int*  counts      = _schema->lockFaceCounts(numFaces);
    const int*  indices     = _schema->lockFaceIndices(numIndices);

    // get an estimate of the total number of edges
    const int numEdges = util::math::sum<int>(numFaces, counts);

    if (numEdges <= 0)
        return nullptr;

    // build inverse vertex mapping
    std::vector<unsigned int> vtxInvRedir(triangles->vtxRedir.size(), 0);

    for (unsigned int i = 0; i < triangles->vtxRedir.size(); ++i)
    {
        assert(triangles->vtxRedir[i] < vtxInvRedir.size());
        vtxInvRedir[triangles->vtxRedir[i]] = i;
    }

    // build wireframe index buffer
    numWireIndices = numEdges << 1;
    wireIndices.resize(numWireIndices, 0);

    const int*      currentFaceCount    = counts;
    const int*      currentFaceIndices  = indices;
    unsigned int*   oIndices            = &wireIndices.front();
    for (size_t f = 0; f < numFaces; ++f)
    {
        const int count = *currentFaceCount;
        for (int i0 = 0; i0 < count - 1; ++i0)
        {
            const int i1 = i0 > 0
                ? i0 - 1
                : count - 1;

            const int vtxIndex0 = currentFaceIndices[i0];
            const int vtxIndex1 = currentFaceIndices[i1];

            oIndices[0] = vtxInvRedir[vtxIndex0];
            oIndices[1] = vtxInvRedir[vtxIndex1];
            oIndices    += 2;
        }
        currentFaceIndices  += count;
        ++currentFaceCount;
    }
    _schema->unlockFaceCounts();
    _schema->unlockFaceIndices();

    // cache computed data
    _wireIndices.set(indexSampleId, wireIndices);
    cache = _wireIndices.get(indexSampleId);
    assert(cache);

    return !cache->empty()
        ? &cache->front()
        : nullptr;
}
