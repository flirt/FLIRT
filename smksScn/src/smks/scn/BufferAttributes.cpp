// ========================================================================= //
// Copyright 2016-2018 SUPAMONKS_STUDIO                                      //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
//                                                                           //
// ========================================================================= //
#include <boost/unordered_map.hpp>
#include <smks/sys/Exception.hpp>

#include "BufferAttributes.hpp"

namespace smks { namespace scn
{
    ///////////////////////////////////////
    // struct BufferAttributes::Declaration
    ///////////////////////////////////////
    struct BufferAttributes::Declaration
    {
        const size_t numFloats, offset;

    public:
        inline
        Declaration(size_t numFloats, size_t offset): numFloats(numFloats), offset(offset) { }
    };

    ////////////////////////////////
    // class BufferAttributes::PImpl
    ////////////////////////////////
    class BufferAttributes::PImpl
    {
    private:
        typedef boost::unordered_map<Property, Declaration> DeclarationMap;

    private:
        size_t          _stride;
        DeclarationMap  _declarations;

    public:
        inline
        PImpl():
            _stride         (0),
            _declarations   ()
        {
        }

        inline
        PImpl(const PImpl& other):
            _stride         (other._stride),
            _declarations   (other._declarations)
        {
        }

        inline
        const PImpl&
        operator=(const PImpl& other)
        {
            _stride         = other._stride;
            _declarations   = other._declarations;

            return *this;
        }

        inline
        void
        clear()
        {
            _stride = 0;
            _declarations.clear();
        }

        inline
        bool
        empty() const
        {
            return _declarations.empty();
        }

        inline
        size_t
        stride() const
        {
            return _stride;
        }

        inline
        void
        append(Property type, size_t numFloats)
        {
            if (_declarations.find(type) != _declarations.end())
                throw sys::Exception("Cannot declare same attribute several times.", "Vertex Attribute Addition");

            _declarations.insert(std::pair<Property, Declaration>(
                type,
                Declaration(numFloats, _stride)));
            _stride += numFloats;
        }

        inline
        bool
        has(Property type) const
        {
            return _declarations.find(type) != _declarations.end();
        }

        inline
        uint32_t
        flags() const
        {
            uint32_t ret = 0;

            if (has(Property::PROP_POSITION))
                ret |= Property::PROP_POSITION;

            if (has(Property::PROP_NORMAL))
                ret |= Property::PROP_NORMAL;

            if (has(Property::PROP_TEXCOORD))
                ret |= Property::PROP_TEXCOORD;

            if (has(Property::PROP_VELOCITY))
                ret |= Property::PROP_VELOCITY;

            if (has(Property::PROP_WIDTH))
                ret |= Property::PROP_WIDTH;

            if (has(Property::PROP_SCALP_NORMAL))
                ret |= Property::PROP_SCALP_NORMAL;

            if (has(Property::PROP_SCALP_TEXCOORD))
                ret |= Property::PROP_SCALP_TEXCOORD;

            return ret;
        }

        inline
        size_t
        offset(Property type) const
        {
            DeclarationMap::const_iterator const& it = _declarations.find(type);

            return it != _declarations.end()
                ? it->second.offset
                : 0;
        }

        inline
        size_t
        numFloats(Property type) const
        {
            DeclarationMap::const_iterator const& it = _declarations.find(type);

            return it != _declarations.end()
                ? it->second.numFloats
                : 0;
        }
    };
}
}

using namespace smks;

scn::BufferAttributes::BufferAttributes():
    _pImpl(new PImpl)
{
}

scn::BufferAttributes::BufferAttributes(const BufferAttributes& other):
    _pImpl(new PImpl(*other._pImpl))
{
}

scn::BufferAttributes::~BufferAttributes()
{
    delete _pImpl;
}

scn::BufferAttributes&
scn::BufferAttributes::operator=(const BufferAttributes& other)
{
    *_pImpl = *other._pImpl;

    return *this;
}

void
scn::BufferAttributes::clear()
{
    _pImpl->clear();
}

bool
scn::BufferAttributes::empty() const
{
    return _pImpl->empty();
}

size_t
scn::BufferAttributes::stride() const
{
    return _pImpl->stride();
}

void
scn::BufferAttributes::append(Property type, size_t numFloats)
{
    _pImpl->append(type, numFloats);
}

bool
scn::BufferAttributes::has(Property type) const
{
    return _pImpl->has(type);
}

uint32_t
scn::BufferAttributes::flags() const
{
    return _pImpl->flags();
}

size_t
scn::BufferAttributes::offset(Property type) const
{
    return _pImpl->offset(type);
}

size_t
scn::BufferAttributes::numFloats(Property type) const
{
    return _pImpl->numFloats(type);
}
