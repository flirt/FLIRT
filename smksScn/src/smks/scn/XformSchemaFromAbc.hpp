// ========================================================================= //
// Copyright 2016-2018 SUPAMONKS_STUDIO                                      //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
//                                                                           //
// ========================================================================= //
#pragma once

#include <Alembic/AbcGeom/IXform.h>

#include "AbstractXformSchema.hpp"

namespace smks { namespace scn
{
    class TreeNode;
    class SchemaBasedSceneObject;
    class SchemaBaseFromAbc;

    class XformSchemaFromAbc:
        public AbstractXformSchema
    {
    private:
        const Alembic::Abc::IObject     _object; //<! raw object
        SchemaBaseFromAbc*              _base;
        Alembic::AbcGeom::IXformSchema  _xform;  //<! typed object's schema

    public:
        XformSchemaFromAbc(SchemaBasedSceneObject&,
                           const Alembic::Abc::IObject&,
                           std::weak_ptr<TreeNode> const& archive);
        ~XformSchemaFromAbc();

        const SchemaBasedSceneObject&
        node() const;
        SchemaBasedSceneObject&
        node();

        inline
        bool
        lazyInitialization() const
        {
            return true;
        }
        void
        uninitialize();
        size_t  //<! return number of samples
        initialize();

        void
        uninitializeUserAttribs();
        void
        initializeUserAttribs();
    private:
        const Alembic::AbcGeom::IXformSchema&
        getAbcSchema();
    public:
        //-------------------
        // parameter handling
        //-------------------
        bool
        isParameterRelevantForClass(unsigned int) const;
        bool
        isParameterReadable(unsigned int) const;
        char
        isParameterWritable(unsigned int) const;
        bool
        overrideParameterDefaultValue(unsigned int, parm::Any&) const;
        void
        handle(const xchg::ParmEvent&);
        bool
        mustSendToScene(const xchg::ParmEvent&) const;
        //-------------------

        bool //<! return current visibility
        setCurrentTime(float);

        size_t
        numSamples() const;

        int
        currentSampleIdx() const;

        void
        getStoredTimes(xchg::Vector<float>&) const;

        void
        synchronizeTransform(math::Affine3f&,
                             bool& inheritsXforms);

    private:
        void
        getTransform(int sampleId,
                     math::Affine3f&,
                     bool& inheritsXforms) const;
    };
}
}
