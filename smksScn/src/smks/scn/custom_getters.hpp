// ========================================================================= //
// Copyright 2016-2018 SUPAMONKS_STUDIO                                      //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
//                                                                           //
// ========================================================================= //
#pragma once

#include <smks/math/types.hpp>
#include <smks/parm/BuiltIn.hpp>

#include "smks/scn/Scene.hpp"
#include "smks/scn/ParmGetter.hpp"

namespace smks { namespace scn
{
    ///////////////////////////////////////
    // class FromSceneParmGetter<ValueType>
    ///////////////////////////////////////
    template<typename ValueType>
    class FromSceneParmGetter:
        public TypedParmGetter<ValueType>
    {
    public:
        typedef std::shared_ptr<FromSceneParmGetter<ValueType>> Ptr;
    private:
        const TreeNode::WPtr _node;
    public:
        static inline
        Ptr
        create(TreeNode::Ptr const& n)
        {
            Ptr ptr(new FromSceneParmGetter<ValueType>(n));
            return ptr;
        }
    protected:
        explicit inline
        FromSceneParmGetter(TreeNode::Ptr const& n):
            TypedParmGetter<ValueType>(),
            _node(n)
        { }
    public:
        inline
        bool
        operator()(const parm::BuiltIn& parm, ValueType& parmValue, unsigned int /*upToNodeId*/)
        {
            TreeNode::Ptr const& node = _node.lock();
            TreeNode::Ptr const& root = node->root().lock();

            if (!root || !root->asScene())
            {
                parm.defaultValue(parmValue);
                return false;
            }
            root->asScene()->computeGlobalParameter<ValueType>(parm, node->id(), parmValue);
            return true;
        }
    };
}
}
