// ========================================================================= //
// Copyright 2016-2018 SUPAMONKS_STUDIO                                      //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
//                                                                           //
// ========================================================================= //

#include <Alembic/Abc/IObject.h>

#include <smks/sys/Log.hpp>
#include <smks/xchg/Data.hpp>
#include <smks/xchg/DataStream.hpp>
#include <smks/xchg/ParmEvent.hpp>
#include <smks/parm/BuiltIns.hpp>
#include <smks/parm/BuiltInMap.hpp>

#include "smks/scn/PolyMesh.hpp"
#include "PolyMeshSchemaFromAbc.hpp"
#include "PolyMesh-DataHolder.hpp"
#include "AbstractObjectDataHolder.hpp"

using namespace Alembic;
using namespace smks;

/////////////////
// class PolyMesh
/////////////////
// static
scn::PolyMesh::Ptr
scn::PolyMesh::create(const Abc::IObject&   object,
                      TreeNode::WPtr const& parent,
                      TreeNode::WPtr const& archive)
{
    Ptr ptr (new PolyMesh(object, parent, archive));
    ptr->build(
        ptr,
        new PolyMeshSchemaFromAbc(*ptr, object, archive),
        new PolyMesh::DataHolder());
    return ptr;
}

scn::PolyMesh::PolyMesh(const Abc::IObject&     object,
                        TreeNode::WPtr const&   parent,
                        TreeNode::WPtr const&   archive):
    Drawable(object.getName().c_str(), parent)
{ }

// virtual
void
scn::PolyMesh::handleParmEvent(const xchg::ParmEvent& evt)
{
    Drawable::handleParmEvent(evt);
    //---
    if (evt.node() != id())
        return;

    if (evt.parm() == parm::subdivisionLevel    ().id() ||
        evt.parm() == parm::rtSubdivisionMethod ().id() ||
        evt.parm() == parm::selected            ().id())
    {
        if (dataHolder())
            dataHolder()->synchronizeWithSchema();
    }
}

std::ostream&
scn::PolyMesh::glDump(std::ostream& out) const
{
    xchg::Data::Ptr     vertices    = nullptr;
    xchg::Data::Ptr     indices     = nullptr;
    xchg::DataStream    positions;
    xchg::DataStream    normals;
    xchg::DataStream    texcoords;
    size_t              count       = 0;

    getParameter<xchg::Data::Ptr>   (parm::glVertices   (), vertices    );
    getParameter<xchg::Data::Ptr>   (parm::glIndices    (), indices     );
    getParameter<xchg::DataStream>  (parm::glPositions  (), positions   );
    getParameter<xchg::DataStream>  (parm::glNormals    (), normals     );
    getParameter<xchg::DataStream>  (parm::glTexcoords  (), texcoords   );
    getParameter<size_t>            (parm::glCount      (), count       );

    if (vertices    == nullptr  || vertices->empty() ||
        indices     == nullptr  || indices->empty() ||
        !positions || !normals || !texcoords || count == 0)
        return out;

    const float*        vtxData     = vertices  ->getPtr<float>();
    const unsigned int* idxData     = indices   ->getPtr<unsigned int>();
    size_t              numFaces    = indices->size() / count;
    size_t              idxOffset   = 0;

    for (size_t f = 0; f < numFaces; ++f)
    {
        for (size_t i = 0; i < count; ++i)
        {
            const size_t vid = idxData[idxOffset];

            out << "f[" << f << "] v[" << vid << "]: ";
            if (positions)
            {
                const float* ptr = positions.getPtr<float>(vid);
                out << "P = (" << ptr[0] << " " << ptr[1] << " " << ptr[2] << ") ";
            }
            if (normals)
            {
                const float* ptr = normals.getPtr<float>(vid);
                out << "P = (" << ptr[0] << " " << ptr[1] << " " << ptr[2] << ") ";
            }
            if (texcoords)
            {
                const float* ptr = texcoords.getPtr<float>(vid);
                out << "P = (" << ptr[0] << " " << ptr[1] << ") ";
            }
            out << "\n";
            ++idxOffset;
        }
        out << "\n";
    }
    return out;
}

std::ostream&
scn::PolyMesh::rtDump(std::ostream& out) const
{
    return out;
}
