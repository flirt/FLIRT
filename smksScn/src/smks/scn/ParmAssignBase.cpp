// ========================================================================= //
// Copyright 2016-2018 SUPAMONKS_STUDIO                                      //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
//                                                                           //
// ========================================================================= //
#include <iostream>
#include <boost/unordered_map.hpp>

#include <smks/xchg/IdSet.hpp>
#include <smks/xchg/SceneEvent.hpp>
#include <smks/xchg/ParmEvent.hpp>
#include <smks/parm/BuiltIns.hpp>

#include "smks/scn/ParmAssignBase.hpp"
#include "smks/scn/Scene.hpp"
#include "smks/scn/TreeNodeSelector.hpp"
#include "smks/scn/TreeNodeVisitor.hpp"
#include "smks/scn/TreeNodeFilter.hpp"
#include "smks/scn/ParmConnection.hpp"

namespace smks { namespace scn
{
    ////////////////////
    // class NodeVisitor
    ////////////////////
    class ParmAssignBase::NodeVisitor:
        public TreeNodeVisitor
    {
    public:
        typedef std::shared_ptr<NodeVisitor> Ptr;

    private:
        ParmAssignBase*                     _assign;
        const AbstractTreeNodeFilter::Ptr   _filter;
    public:
        Operation                           op;

    public:
        static inline
        Ptr
        create(ParmAssignBase* assign, Direction dir, AbstractTreeNodeFilter::Ptr const& filter)
        {
            Ptr ptr(new NodeVisitor(assign, dir, filter));

            return ptr;
        }

    public:
        inline
        NodeVisitor(ParmAssignBase* assign, Direction dir, AbstractTreeNodeFilter::Ptr const& filter):
            TreeNodeVisitor (dir),
            _assign         (assign),
            _filter         (filter),
            op              (Operation::ADD)
        { }

        virtual
        void
        apply(TreeNode& n)
        {
            assert(_assign);
            if (!_filter || (*_filter)(n))
                _assign->process(n.id(), op);
            traverse(n);
        }
    };

    //////////////
    // class PImpl
    //////////////
    class ParmAssignBase::PImpl
    {
    private:
        typedef boost::unordered_map<unsigned int, unsigned int> NodeConnectionIdMap;

    private:
        NodeVisitor::Ptr        _visitor;
        ParmAssignBase::WPtr    _self;
        xchg::IdSet             _lastSelection;
        NodeConnectionIdMap     _assignees;         // this --['parmValue' > parmName]--> assignee

    public:
        explicit inline
        PImpl(NodeVisitor::Ptr const& visitor = nullptr):
            _visitor        (visitor),
            _self           (),
            _lastSelection  (),
            _assignees      ()
        {
        }

        inline
        void
        build(TreeNode::Ptr const&);

        inline
        void
        erase();

        inline
        void
        process(unsigned int targetId, Operation);

        inline
        void
        handleSceneEvent(const xchg::SceneEvent&);

        inline
        void
        handleParmEvent(const xchg::ParmEvent&);

    private:
        inline
        void
        processSelection();

        inline
        void
        process(const Scene*, const xchg::IdSet&, Operation);

        inline
        void
        handleSelectionChanged(xchg::ParmEvent::Type);

        inline
        void
        handleParmNameChanged(xchg::ParmEvent::Type);

        inline
        void
        handleParmValueChanged(xchg::ParmEvent::Type);

        inline
        void
        handlePriorityChanged(xchg::ParmEvent::Type);

        inline
        void
        handleAssigneeCreatedOrDeleted(const xchg::SceneEvent&);

        inline
        void
        connectToAssignee(unsigned int);

        inline
        void
        disconnectFromAssignee(unsigned int);

        inline
        void
        disconnectAllAssignees();
    };
}
}

using namespace smks;

//////////////////////////////
// class ParmAssignBase::PImpl
//////////////////////////////
void
scn::ParmAssignBase::PImpl::build(TreeNode::Ptr const& ptr)
{
    assert(ptr);
    _self = std::static_pointer_cast<ParmAssignBase>(ptr);
}

void
scn::ParmAssignBase::PImpl::erase()
{
    disconnectAllAssignees();
    _lastSelection.clear();
}

void
scn::ParmAssignBase::PImpl::handleSceneEvent(const xchg::SceneEvent& evt)
{
    switch (evt.type())
    {
    default:
        break;
    case xchg::SceneEvent::NODE_CREATED:
    case xchg::SceneEvent::NODE_DELETED:
        if (_assignees.find(evt.node()) != _assignees.end())
            handleAssigneeCreatedOrDeleted(evt);
        break;
    }
}

void
scn::ParmAssignBase::PImpl::handleParmEvent(const xchg::ParmEvent& evt)
{
    if (evt.parm() == parm::selection().id())
        handleSelectionChanged(evt.type());
    else if (!evt.comesFromLink())
    {
        if (evt.parm() == parm::parmName().id())
            handleParmNameChanged(evt.type());
        else if (evt.parm() == parm::parmValue().id())
            handleParmValueChanged(evt.type());
        else if (evt.parm() == parm::priority().id())
            handlePriorityChanged(evt.type());
    }
}

void
scn::ParmAssignBase::PImpl::handleSelectionChanged(xchg::ParmEvent::Type typ)
{
    processSelection();
}

void
scn::ParmAssignBase::PImpl::handleParmNameChanged(xchg::ParmEvent::Type typ)
{
    // since the change in parameter name causes the connection to change name and id,
    // we must all disconnect them before reconnecting them.
    disconnectAllAssignees();
    _lastSelection.clear();

    if (typ != xchg::ParmEvent::Type::PARM_REMOVED)
        processSelection(); // can only reconnect if there is indeed a new parameter name.
}

void
scn::ParmAssignBase::PImpl::handleParmValueChanged(xchg::ParmEvent::Type typ)
{ }

void
scn::ParmAssignBase::PImpl::handlePriorityChanged(xchg::ParmEvent::Type typ)
{
    ParmAssignBase::Ptr const&  self    = _self.lock();
    TreeNode::Ptr const&        root    = self ? self->root().lock() : nullptr;
    const Scene*                scene   = root ? root->asScene() : nullptr;
    if (!scene)
        return;

    int priority = 0;

    self->getParameter<int>(parm::priority(), priority);
    for (NodeConnectionIdMap::const_iterator it = _assignees.begin(); it != _assignees.end(); ++it)
        if (scene->hasConnection(it->second))
        {
            ParmConnectionBase::Ptr const& cnx = scene->connectionById(it->second);

            if (cnx)
                cnx->priority(priority);
        }
}

void
scn::ParmAssignBase::PImpl::handleAssigneeCreatedOrDeleted(const xchg::SceneEvent& evt)
{
    if (evt.type() == xchg::SceneEvent::NODE_CREATED)
        connectToAssignee(evt.node());  //<! should never be called though (already created since assigned to)
    else if (evt.type() == xchg::SceneEvent::NODE_DELETED)
        disconnectFromAssignee(evt.node());
}

void
scn::ParmAssignBase::PImpl::processSelection()
{
    ParmAssignBase::Ptr const&  self    = _self.lock();
    TreeNode::Ptr const&        root    = self ? self->root().lock()    : nullptr;
    const Scene*                scene   = root ? root->asScene()        : nullptr;

    if (!scene)
        return;

    // get current selection either from a linked selector or from our parameter set
    xchg::IdSet curSelection;

    self->getParameter<xchg::IdSet>(parm::selection(), curSelection);
    if (curSelection.empty())
    {
        unsigned int selectorId = 0;

        self->getParameter<unsigned int>(parm::selectorId(), selectorId);
        if (scene->hasDescendant(selectorId))
        {
            TreeNode::Ptr const& selector = scene->descendantById(selectorId).lock();

            if (selector && selector->is(xchg::NodeClass::SELECTOR))
                selector->asGraphAction()->asTreeNodeSelector()->getParameter<xchg::IdSet>(parm::selection(), curSelection);
        }
    }

    // compare wrt to last processed selection
    xchg::IdSet added, removed;
    xchg::IdSet::diff(_lastSelection, curSelection, added, removed);

    process(scene, added,   Operation::ADD);
    process(scene, removed, Operation::REMOVE);

    _lastSelection = curSelection;
}

void
scn::ParmAssignBase::PImpl::process(const Scene* scene, const xchg::IdSet& ids, Operation op)
{
    if (!scene)
        return;
    if (_visitor)
    {
        //<! have the visitor define assignees from the selection.
        _visitor->op = op;
        for (xchg::IdSet::const_iterator id = ids.begin(); id != ids.end(); ++id)
            if (scene->hasDescendant(*id))
            {
                TreeNode::Ptr const& node = scene->descendantById(*id).lock();
                if (node)
                    node->accept(*_visitor);
            }
    }
    else
    {
        //<! if no visitor has been defined, then assignees directly correspond to
        // the selection.
        for (xchg::IdSet::const_iterator id = ids.begin(); id != ids.end(); ++id)
            process(*id, op);
    }
}

void
scn::ParmAssignBase::PImpl::process(unsigned int targetId, Operation op)
{
    TreeNode::Ptr const& self   = _self.lock();
    TreeNode::Ptr const& root   = self ? self->root().lock() : nullptr;

    if (!root)
        return;

    const Scene* scene = root->asScene();
    assert(scene);
    if (op == Operation::ADD)
        connectToAssignee(targetId);
    else if (op == Operation::REMOVE)
        disconnectFromAssignee(targetId);
}

void
scn::ParmAssignBase::PImpl::connectToAssignee(unsigned int targetId)
{
    ParmAssignBase::Ptr const& self = _self.lock();

    if (!self ||
        targetId == 0 ||
        _assignees.find(targetId) != _assignees.end())
        return;

    std::string parmName;

    self->getParameter<std::string>(parm::parmName(), parmName);
    if (parmName.empty())
        return;

    TreeNode::Ptr const& scene = self->root().lock();
    if (!scene)
        return;

    int priority = 0;

    self->getParameter<int>(parm::priority(), priority);

    const unsigned int cnxId = self->createConnectionToAssignee(scene->asScene(), targetId, parmName.c_str(), priority);
    if (cnxId > 0)
        _assignees.insert(std::pair<unsigned int, unsigned int>(targetId, cnxId));
}

void
scn::ParmAssignBase::PImpl::disconnectFromAssignee(unsigned int targetId)
{
    NodeConnectionIdMap::const_iterator const& it = _assignees.find(targetId);

    if (it == _assignees.end())
        return;

    const unsigned int cnxId = it->second;

    _assignees.erase(it);

    ParmAssignBase::Ptr const&  self    = _self.lock();
    TreeNode::Ptr const&        scene   = self ? self->root().lock() : nullptr;

    if (scene)
        self->removeConnectionFromAssignee(scene->asScene(), cnxId);
}

void
scn::ParmAssignBase::PImpl::disconnectAllAssignees()
{
    while (!_assignees.empty())
        disconnectFromAssignee(_assignees.begin()->first);
}

///////////////////////
// class ParmAssignBase
///////////////////////
inline
scn::ParmAssignBase::ParmAssignBase(const char* name, TreeNode::WPtr const& parent):
    GraphAction (name, parent),
    _pImpl      (new PImpl)
{ }

inline
scn::ParmAssignBase::ParmAssignBase(TreeNodeVisitor::Direction          visitorDir,
                                    AbstractTreeNodeFilter::Ptr const&  visitorFilter,
                                    const char*                         name,
                                    TreeNode::WPtr const&               parent):
    GraphAction (name, parent),
    _pImpl      (new PImpl(NodeVisitor::create(this, visitorDir, visitorFilter)))
{ }

scn::ParmAssignBase::~ParmAssignBase()
{
    delete _pImpl;
}

void
scn::ParmAssignBase::build(TreeNode::Ptr const& ptr)
{
    GraphAction::build(ptr);
    //---
    _pImpl->build(ptr);
}

void
scn::ParmAssignBase::erase()
{
    _pImpl->erase();
    //---
    GraphAction::erase();
}

// virtual
void
scn::ParmAssignBase::handleSceneEvent(const xchg::SceneEvent& evt)
{
    GraphAction::handleSceneEvent(evt);
    //---
    _pImpl->handleSceneEvent(evt);
}

// virtual
void
scn::ParmAssignBase::handleParmEvent(const xchg::ParmEvent& evt)
{
    GraphAction::handleParmEvent(evt);
    //---
    _pImpl->handleParmEvent(evt);
}

void
scn::ParmAssignBase::process(unsigned int targetId, Operation op)
{
    _pImpl->process(targetId, op);
}

// virtual
bool
scn::ParmAssignBase::isParameterWritable(unsigned int parmId) const
{
    if (isParameterRelevantForClass(parmId))
        return true;
    return GraphAction::isParameterWritable(parmId);
}

// virtual
bool
scn::ParmAssignBase::isParameterRelevant(unsigned int parmId)const
{
    return isParameterRelevantForClass(parmId) || GraphAction::isParameterRelevant(parmId);
}

bool
scn::ParmAssignBase::isParameterRelevantForClass(unsigned int parmId) const
{
    return
        parmId == parm::parmName    ().id() ||
        parmId == parm::priority    ().id() ||
        parmId == parm::selection   ().id() ||
        parmId == parm::selectorId  ().id();
}
