// ========================================================================= //
// Copyright 2016-2018 SUPAMONKS_STUDIO                                      //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
//                                                                           //
// ========================================================================= //
#include "PolyMesh-TBBNormalizeVec3Kernel.hpp"

#include <smks/sys/Exception.hpp>
#include <smks/math/matrixAlgo.hpp>

using namespace smks;

scn::TBBNormalizeVec3Kernel::TBBNormalizeVec3Kernel(float* buffer, size_t stride):
    _buffer (buffer),
    _stride (stride)
{
    checkKernelCorrectness();
}

scn::TBBNormalizeVec3Kernel::TBBNormalizeVec3Kernel(const TBBNormalizeVec3Kernel& k):
    _buffer (k._buffer),
    _stride (k._stride)
{
    checkKernelCorrectness();
}

void
scn::TBBNormalizeVec3Kernel::checkKernelCorrectness() const
{
    if (!_buffer)
        throw sys::Exception("No valid buffer.", "Vector3 Normalization TBB Kernel");
    if (_stride == 0)
        throw sys::Exception("Buffer stride must be positive.", "Vector3 Normalization TBB Kernel");
}

void
scn::TBBNormalizeVec3Kernel::operator()(tbb::blocked_range<int> const& r) const
{
    float* ptr = _buffer + r.begin() * _stride;
    for (int f = r.begin(); f < r.end(); ++f)
    {
        const float sqLen   = ptr[0] * ptr[0] + ptr[1] * ptr[1] + ptr[2] * ptr[2];
        if (sqLen < 1e-6f)
            continue;

        const float rLen    = math::rsqrt(sqLen);

        ptr[0] *= rLen;
        ptr[1] *= rLen;
        ptr[2] *= rLen;

        ptr += _stride;
    }
}
