// ========================================================================= //
// Copyright 2016-2018 SUPAMONKS_STUDIO                                      //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
//                                                                           //
// ========================================================================= //
#include <ImathMatrix.h>

#include <smks/sys/Log.hpp>
#include <smks/math/matrixAlgo.hpp>
#include <smks/xchg/equal.hpp>
#include <smks/xchg/ParmEvent.hpp>
#include <smks/parm/BuiltIn.hpp>
#include <smks/parm/BuiltIns.hpp>

#include "XformParmBasedSchema.hpp"
#include "node_utils.hpp"

using namespace smks;

// explicit
scn::XformParmBasedSchema::XformParmBasedSchema(SchemaBasedSceneObject& node):
    AbstractXformSchema (),
    _node               (node),
    _initialized        (false)
{ }

//-------------------
// parameter handling
//-------------------
char
scn::XformParmBasedSchema::isParameterWritable(unsigned int parmId) const
{
    if (parmId == parm::currentTime     ().id() ||
        parmId == parm::currentSampleIdx().id() ||
        parmId == parm::timeBounds      ().id() ||
        parmId == parm::numSamples      ().id())
        return 0;
    if (isParameterRelevantForClass(parmId))
        return 1;
    return -1;
}
bool
scn::XformParmBasedSchema::isParameterRelevantForClass(unsigned int parmId) const
{
    return
        parmId == parm::center              ().id() ||
        parmId == parm::front               ().id() ||
        parmId == parm::upVector            ().id() ||
        parmId == parm::up                  ().id() ||
        parmId == parm::translate           ().id() ||
        parmId == parm::rotateD             ().id() ||
        parmId == parm::rotateOrder         ().id() ||
        parmId == parm::scale               ().id() ||
        parmId == parm::transform           ().id() ||
        parmId == parm::inheritsTransforms  ().id();
}

bool
scn::XformParmBasedSchema::preprocessParameterEdit(const char*      parmName,
                                                   unsigned int     parmId,
                                                   parm::ParmFlags  f,
                                                   const parm::Any& value)
{
    // capture the updates of all parameters that only partially specify the
    // transform ('translate', 'rotateD', etc...) in order to only actually
    // update the value of the complete 'transform' parameter.
    // this will trigger a resynchronization of all those 'partial' parameters.
    if (parmId == parm::translate   ().id() ||
        parmId == parm::rotateD     ().id() ||
        parmId == parm::rotateOrder ().id() ||
        parmId == parm::scale       ().id())
    {
        math::Vector4f  translate, rotateD, scale;
        char            rotateOrder;

        if (!getBuiltInFromNodeOrArgument<math::Vector4f>   (_node, parm::translate(),      parmId, value, xchg::equal, translate)  ||
            !getBuiltInFromNodeOrArgument<math::Vector4f>   (_node, parm::rotateD(),        parmId, value, xchg::equal, rotateD)    ||
            !getBuiltInFromNodeOrArgument<math::Vector4f>   (_node, parm::scale(),          parmId, value, xchg::equal, scale)      ||
            !getBuiltInFromNodeOrArgument<char>             (_node, parm::rotateOrder(),    parmId, value, xchg::equal, rotateOrder))
            return false;   // no change in terms of value

        math::Affine3f  xfm;

        math::composeEuler(xfm, translate, static_cast<math::RotateOrder>(rotateOrder), rotateD, scale);
        setParameter<char>          (_node, parm::rotateOrder(),    rotateOrder,    parm::SKIPS_RESTRAINTS | parm::SKIPS_PREPROCESS);
        setParameter<math::Affine3f>(_node, parm::transform(),      xfm,            parm::SKIPS_RESTRAINTS | parm::SKIPS_PREPROCESS);

        return true;
    }
    else if (parmId == parm::center ().id() ||
        parmId == parm::front       ().id() ||
        parmId == parm::upVector    ().id() ||
        parmId == parm::up          ().id())
    {
        ///////////////////////////////
        // struct RelativePositionEqual
        ///////////////////////////////
        struct RelativePositionEqual:
            public xchg::EqualTo<math::Vector4f>
        {
            math::Vector4f ref;
        public:
            inline
            bool
            operator()(const math::Vector4f& rpos1,
                       const math::Vector4f& rpos2) const
            {
                math::Vector4f  dir1        = rpos1 - ref;
                math::Vector4f  dir2        = rpos2 - ref;
                const float     lenRPos1    = math::len3(dir1);
                const float     lenRPos2    = math::len3(dir2);

                if (lenRPos1 < 1e-6f || lenRPos2 < 1e-6f)
                    return false;

                return xchg::areDirectionsEqual(math::rcp(lenRPos1) * dir1, math::rcp(lenRPos2) * dir2);
            }
        };
        ///////////////////////////////
        math::Affine3f  xfm;
        math::Vector4f  eyePosition, targetPosition, upVector, upPosition, scale;

        if (!getBuiltInFromNodeOrArgument<math::Vector4f>(_node, parm::center(),    parmId, value, xchg::equal, eyePosition)        ||
            !getBuiltInFromNodeOrArgument<math::Vector4f>(_node, parm::upVector(),  parmId, value, xchg::areDirectionsEqual, upVector))
            return false; // no actual change

        RelativePositionEqual predicate;
        predicate.ref = eyePosition;

        if (!getBuiltInFromNodeOrArgument<math::Vector4f>(_node, parm::front(), parmId, value, predicate, targetPosition) ||
            !getBuiltInFromNodeOrArgument<math::Vector4f>(_node, parm::up(),    parmId, value, predicate, upPosition))
            return false; // no actual change

        _node.getParameter<math::Vector4f>(parm::scale(), scale);

        if (parmId == parm::up().id())
        {
            const math::Vector4f& newUpVector = (upPosition - eyePosition).normalized();

            if (xchg::areDirectionsEqual(newUpVector, upVector))
                return false;   // up changed position, but not actual direction
            upVector = newUpVector;
        }

        FLIRT_LOG(LOG(DEBUG)
            << "xform '" << _node.name() << "'\tlook at:"
            << "\n\t- eye position = " << eyePosition.transpose()
            << "\n\t- target position = " << targetPosition.transpose()
            << "\n\t- up vector = " << upVector.transpose()
            << "\n\t- scale = " << scale.transpose();)
        math::lookAtPoint(xfm, eyePosition, targetPosition, upVector, scale);
        setParameter<math::Affine3f>(_node, parm::transform(), xfm, parm::SKIPS_RESTRAINTS | parm::SKIPS_PREPROCESS);

        return true;
    }
    return false;
}

void
scn::XformParmBasedSchema::handle(const xchg::ParmEvent& evt)
{
    if (evt.parm() == parm::transform().id())
    {
        const bool synchronize = true;
        dirtySample(_node, currentSampleIdx(), synchronize);    // triggers sample reevaluation
    }
}
//-------------------
void
scn::XformParmBasedSchema::synchronizeTransform(math::Affine3f& xfm,
                                                bool&           inherits)
{
    // synchronization for parameter-driven schema is triggered by the setting of the transform parameter.
    xfm.setIdentity();

    _node.getParameter<math::Affine3f>  (parm::transform(),             xfm);
    _node.getParameter<bool>            (parm::inheritsTransforms(),    inherits);

    // decompose the transform in order to set the values of the parameters that depend on it
    char rotateOrder;

    node().getParameter<char>(parm::rotateOrder(), rotateOrder);

    math::Vector4f  newTranslate;
    math::Vector4f  newRotateD;
    math::Vector4f  newScale;
    math::Vector4f  newForwardVector;
    math::Vector4f  newUpVector;

    math::decompose(xfm, newTranslate, static_cast<math::RotateOrder>(rotateOrder), newRotateD, newScale, &newForwardVector, &newUpVector);

    const math::Vector4f& newEyePosition = newTranslate;    // simply for convenience

    setParameter<math::Vector4f>(_node, parm::translate(),  newTranslate,   parm::SKIPS_RESTRAINTS | parm::SKIPS_PREPROCESS);
    setParameter<math::Vector4f>(_node, parm::rotateD(),    newRotateD,     parm::SKIPS_RESTRAINTS | parm::SKIPS_PREPROCESS);
    setParameter<math::Vector4f>(_node, parm::scale(),      newScale,       parm::SKIPS_RESTRAINTS | parm::SKIPS_PREPROCESS);
    setParameter<math::Vector4f>(_node, parm::center(),     newEyePosition, parm::SKIPS_RESTRAINTS | parm::SKIPS_PREPROCESS);
    setParameter<math::Vector4f>(_node, parm::upVector(),   newUpVector,    parm::SKIPS_RESTRAINTS | parm::SKIPS_PREPROCESS);

    //----------------------------------------------------------------------
    // update the value of the positional built-in only when it would cause a change in terms
    // of direction from the eye's new position
#define UPDATE_POSITION_UPON_DIRECTION_CHANGE(_POS_BUILTIN_, _NEW_DIRECTION_)                   \
    {                                                                                           \
        math::Vector4f curPosition;                                                             \
                                                                                                \
        _node.getParameter<math::Vector4f>(_POS_BUILTIN_, curPosition);                         \
                                                                                                \
        bool updateNewPosition      = false;                                                    \
        math::Vector4f  curVector   = curPosition - newEyePosition;                             \
        float           curDistance = curVector.norm();                                         \
                                                                                                \
        if (curDistance > 1e-6f)                                                                \
        {                                                                                       \
            curVector *= math::rcp(curDistance);                                                \
            updateNewPosition = xchg::areDirectionsEqual(curVector, _NEW_DIRECTION_) == false;  \
        }                                                                                       \
        else                                                                                    \
        {                                                                                       \
            curDistance = 1.0f;                                                                 \
            updateNewPosition = true;                                                           \
        }                                                                                       \
        if (updateNewPosition)                                                                  \
        {                                                                                       \
            math::Vector4f newPosition = newEyePosition + curDistance * _NEW_DIRECTION_;        \
            setParameter<math::Vector4f>(_node,                                                 \
                _POS_BUILTIN_,                                                                  \
                newPosition,                                                                    \
                parm::SKIPS_RESTRAINTS | parm::SKIPS_PREPROCESS);                               \
        }                                                                                       \
    }
    //----------------------------------------------------------------------

    UPDATE_POSITION_UPON_DIRECTION_CHANGE(parm::front(),    newForwardVector)
    UPDATE_POSITION_UPON_DIRECTION_CHANGE(parm::up(),       newUpVector)
}

