// ========================================================================= //
// Copyright 2016-2018 SUPAMONKS_STUDIO                                      //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
//                                                                           //
// ========================================================================= //
#pragma once

#include <memory>
#include "smks/scn/SchemaBasedSceneObject.hpp"

namespace smks
{
    namespace xchg
    {
        class IdSet;
    }
    namespace parm
    {
        class BuiltIn;
        class Any;
    }
    namespace scn
    {
        class AbstractObjectSchema;
        class AbstractObjectDataHolder
        {
        public:
            virtual inline
            ~AbstractObjectDataHolder()
            { }

            virtual
            void
            build(AbstractObjectSchema*) = 0;

            virtual
            void
            initialize() = 0;

            virtual
            void
            uninitialize() = 0;

            virtual
            void
            synchronizeWithSchema() = 0;

            virtual
            void
            dirtyCacheEntry(size_t idx, bool synchronize) = 0;

        protected:
            // inherited friendship
            inline
            void
            initialize(SchemaBasedSceneObject& node) const
            {
                return node.initialize();
            }

            // inherited friendship
            template <typename ValueType> inline
            unsigned int
            setParameter(SchemaBasedSceneObject&    node,
                         const parm::BuiltIn&       parm,
                         const ValueType&           parmValue,
                         parm::ParmFlags            flags) const
            {
                return node._setParameter<ValueType>(
                    parm.c_str(),
                    parm.id(),
                    &parm,
                    parmValue,
                    flags);
            }
            // inherited friendship
            inline
            void
            unsetParameter(SchemaBasedSceneObject&  node,
                           const parm::BuiltIn&     parm,
                           parm::ParmFlags          flags) const
            {
                node._unsetParameter(parm, flags);
            }
        };
    }
}
