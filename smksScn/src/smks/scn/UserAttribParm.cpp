// ========================================================================= //
// Copyright 2016-2018 SUPAMONKS_STUDIO                                      //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
//                                                                           //
// ========================================================================= //
#include <string>
#include <Alembic/AbcCoreAbstract/DataType.h>
#include <Alembic/Util/PlainOldDataType.h>

#include <smks/sys/Log.hpp>
#include <smks/scn/TreeNode.hpp>
#include <smks/math/types.hpp>
#include <smks/xchg/ParmType.hpp>
#include <smks/util/string/getId.hpp>
#include <std/to_string_xchg.hpp>
#include "UserAttribParm.hpp"

namespace std
{
    static inline string to_string(Alembic::Util::PlainOldDataType);
}

namespace smks { namespace scn
{
    //////////////////////////////////////////
    // struct UserAttribParmBase::SampleReader
    //////////////////////////////////////////
    struct UserAttribParmBase::SampleReader
    {
    public:
        virtual inline
        ~SampleReader()
        { }
        virtual
        void
        operator()(const void*, void*) const = 0;
    };

    ///////////////////////////
    // struct TypedSampleReader
    ///////////////////////////
    template<typename PropPodType, int NumElements, typename OutType>
    struct TypedSampleReader:
        public UserAttribParmBase::SampleReader
    {
    public:
        inline
        void
        operator()(const void* rawData, void* oData) const
        {
            const char* iPtr = static_cast<const char*>(rawData);
            OutType*    oPtr = reinterpret_cast<OutType*>(oData);
            assert(iPtr);
            assert(oPtr);
            for (int i = 0; i < NumElements; ++i)
            {
                *oPtr = static_cast<OutType>(*reinterpret_cast<const PropPodType*>(iPtr));
                iPtr += sizeof(PropPodType);
                ++oPtr;
            }
        }
    };

    ////////////////////////////
    // struct StringSampleReader
    ////////////////////////////
    template<typename PropStringType>
    struct StringSampleReader:
        public UserAttribParmBase::SampleReader
    {
    public:
        inline
        void
        operator()(const void* rawData, void* oData) const
        {
            const PropStringType*   iStr = static_cast<const PropStringType*>(rawData);
            std::string*            oStr = reinterpret_cast<std::string*>(oData);
            assert(iStr);
            assert(oStr);
            if (iStr->size() > 0)
            {
                oStr->resize(iStr->size());
                for (size_t i = 0; i < iStr->size(); ++i)
                    (*oStr)[i] = static_cast<char>(iStr->c_str()[i]);
            }
            else
                oStr->clear();
        }
    };
}
}

std::string
std::to_string(Alembic::Util::PlainOldDataType type)
{
    if (type == Alembic::Util::kBooleanPOD ) return "bool_t"    ;
    if (type == Alembic::Util::kUint8POD   ) return "uint8_t"   ;
    if (type == Alembic::Util::kInt8POD    ) return "int8_t"    ;
    if (type == Alembic::Util::kUint16POD  ) return "uint16_t"  ;
    if (type == Alembic::Util::kInt16POD   ) return "int16_t"   ;
    if (type == Alembic::Util::kUint32POD  ) return "uint32_t"  ;
    if (type == Alembic::Util::kInt32POD   ) return "int32_t"   ;
    if (type == Alembic::Util::kUint64POD  ) return "uint64_t"  ;
    if (type == Alembic::Util::kInt64POD   ) return "int64_t"   ;
    if (type == Alembic::Util::kFloat16POD ) return "float16_t" ;
    if (type == Alembic::Util::kFloat32POD ) return "float32_t" ;
    if (type == Alembic::Util::kFloat64POD ) return "float64_t" ;
    if (type == Alembic::Util::kStringPOD  ) return "string"    ;
    if (type == Alembic::Util::kWstringPOD ) return "wstring"   ;
    return "";
}

using namespace smks;

///////////////////////////
// class UserAttribParmBase
///////////////////////////
scn::UserAttribParmBase::UserAttribParmBase(const std::string& parmName, const DataType& type):
    _parmName       (parmName),
    _parmId         (util::string::getId(parmName.c_str())),
    _parmType       (xchg::UNKNOWN_PARMTYPE),
    _sampleReader   (nullptr)
{
    setup(type);
}

// virtual
bool
scn::UserAttribParmBase::valid() const
{
    return _parmType != xchg::UNKNOWN_PARMTYPE &&
        _sampleReader;
}

inline
scn::UserAttribParmBase::~UserAttribParmBase()
{
    if (_sampleReader)
        delete _sampleReader;
}

void
scn::UserAttribParmBase::setup(const DataType& type)
{
    using namespace Alembic;
    //---
    _parmType = xchg::UNKNOWN_PARMTYPE;
    if (_sampleReader)
        delete _sampleReader;
    _sampleReader = nullptr;
    //---
    const Util::PlainOldDataType    pod     = type.getPod();
    const size_t                    extent  = static_cast<size_t>(type.getExtent());

    if (extent == 4)
    {
        if      (pod==Util::kBooleanPOD)    { _parmType = xchg::PARMTYPE_INT_4;     _sampleReader = new TypedSampleReader<Util::bool_t,     4, int>();   }
        else if (pod==Util::kUint8POD)      { _parmType = xchg::PARMTYPE_INT_4;     _sampleReader = new TypedSampleReader<Util::uint8_t,    4, int>();   }
        else if (pod==Util::kInt8POD)       { _parmType = xchg::PARMTYPE_INT_4;     _sampleReader = new TypedSampleReader<Util::int8_t,     4, int>();   }
        else if (pod==Util::kUint16POD)     { _parmType = xchg::PARMTYPE_INT_4;     _sampleReader = new TypedSampleReader<Util::uint16_t,   4, int>();   }
        else if (pod==Util::kInt16POD)      { _parmType = xchg::PARMTYPE_INT_4;     _sampleReader = new TypedSampleReader<Util::int16_t,    4, int>();   }
        else if (pod==Util::kUint32POD)     { _parmType = xchg::PARMTYPE_INT_4;     _sampleReader = new TypedSampleReader<Util::uint32_t,   4, int>();   }
        else if (pod==Util::kInt32POD)      { _parmType = xchg::PARMTYPE_INT_4;     _sampleReader = new TypedSampleReader<Util::int32_t,    4, int>();   }
        else if (pod==Util::kUint64POD)     { _parmType = xchg::PARMTYPE_INT_4;     _sampleReader = new TypedSampleReader<Util::uint64_t,   4, int>();   }
        else if (pod==Util::kInt64POD)      { _parmType = xchg::PARMTYPE_INT_4;     _sampleReader = new TypedSampleReader<Util::int64_t,    4, int>();   }
        else if (pod==Util::kFloat16POD)    { _parmType = xchg::PARMTYPE_FLOAT_4;   _sampleReader = new TypedSampleReader<Util::float16_t,  4, float>(); }
        else if (pod==Util::kFloat32POD)    { _parmType = xchg::PARMTYPE_FLOAT_4;   _sampleReader = new TypedSampleReader<Util::float32_t,  4, float>(); }
        else if (pod==Util::kFloat64POD)    { _parmType = xchg::PARMTYPE_FLOAT_4;   _sampleReader = new TypedSampleReader<Util::float64_t,  4, float>(); }
    }
    else if (extent == 3)
    {
        if      (pod==Util::kBooleanPOD)    { _parmType = xchg::PARMTYPE_INT_3;     _sampleReader = new TypedSampleReader<Util::bool_t,     3, int>();   }
        else if (pod==Util::kUint8POD)      { _parmType = xchg::PARMTYPE_INT_3;     _sampleReader = new TypedSampleReader<Util::uint8_t,    3, int>();   }
        else if (pod==Util::kInt8POD)       { _parmType = xchg::PARMTYPE_INT_3;     _sampleReader = new TypedSampleReader<Util::int8_t,     3, int>();   }
        else if (pod==Util::kUint16POD)     { _parmType = xchg::PARMTYPE_INT_3;     _sampleReader = new TypedSampleReader<Util::uint16_t,   3, int>();   }
        else if (pod==Util::kInt16POD)      { _parmType = xchg::PARMTYPE_INT_3;     _sampleReader = new TypedSampleReader<Util::int16_t,    3, int>();   }
        else if (pod==Util::kUint32POD)     { _parmType = xchg::PARMTYPE_INT_3;     _sampleReader = new TypedSampleReader<Util::uint32_t,   3, int>();   }
        else if (pod==Util::kInt32POD)      { _parmType = xchg::PARMTYPE_INT_3;     _sampleReader = new TypedSampleReader<Util::int32_t,    3, int>();   }
        else if (pod==Util::kUint64POD)     { _parmType = xchg::PARMTYPE_INT_3;     _sampleReader = new TypedSampleReader<Util::uint64_t,   3, int>();   }
        else if (pod==Util::kInt64POD)      { _parmType = xchg::PARMTYPE_INT_3;     _sampleReader = new TypedSampleReader<Util::int64_t,    3, int>();   }
        else if (pod==Util::kFloat16POD)    { _parmType = xchg::PARMTYPE_FLOAT_4;   _sampleReader = new TypedSampleReader<Util::float16_t,  3, float>(); }
        else if (pod==Util::kFloat32POD)    { _parmType = xchg::PARMTYPE_FLOAT_4;   _sampleReader = new TypedSampleReader<Util::float32_t,  3, float>(); }
        else if (pod==Util::kFloat64POD)    { _parmType = xchg::PARMTYPE_FLOAT_4;   _sampleReader = new TypedSampleReader<Util::float64_t,  3, float>(); }
    }
    else if (extent == 2)
    {
        if      (pod==Util::kBooleanPOD)    { _parmType = xchg::PARMTYPE_INT_2;     _sampleReader = new TypedSampleReader<Util::bool_t,     2, int>();   }
        else if (pod==Util::kUint8POD)      { _parmType = xchg::PARMTYPE_INT_2;     _sampleReader = new TypedSampleReader<Util::uint8_t,    2, int>();   }
        else if (pod==Util::kInt8POD)       { _parmType = xchg::PARMTYPE_INT_2;     _sampleReader = new TypedSampleReader<Util::int8_t,     2, int>();   }
        else if (pod==Util::kUint16POD)     { _parmType = xchg::PARMTYPE_INT_2;     _sampleReader = new TypedSampleReader<Util::uint16_t,   2, int>();   }
        else if (pod==Util::kInt16POD)      { _parmType = xchg::PARMTYPE_INT_2;     _sampleReader = new TypedSampleReader<Util::int16_t,    2, int>();   }
        else if (pod==Util::kUint32POD)     { _parmType = xchg::PARMTYPE_INT_2;     _sampleReader = new TypedSampleReader<Util::uint32_t,   2, int>();   }
        else if (pod==Util::kInt32POD)      { _parmType = xchg::PARMTYPE_INT_2;     _sampleReader = new TypedSampleReader<Util::int32_t,    2, int>();   }
        else if (pod==Util::kUint64POD)     { _parmType = xchg::PARMTYPE_INT_2;     _sampleReader = new TypedSampleReader<Util::uint64_t,   2, int>();   }
        else if (pod==Util::kInt64POD)      { _parmType = xchg::PARMTYPE_INT_2;     _sampleReader = new TypedSampleReader<Util::int64_t,    2, int>();   }
        else if (pod==Util::kFloat16POD)    { _parmType = xchg::PARMTYPE_FLOAT_2;   _sampleReader = new TypedSampleReader<Util::float16_t,  2, float>(); }
        else if (pod==Util::kFloat32POD)    { _parmType = xchg::PARMTYPE_FLOAT_2;   _sampleReader = new TypedSampleReader<Util::float32_t,  2, float>(); }
        else if (pod==Util::kFloat64POD)    { _parmType = xchg::PARMTYPE_FLOAT_2;   _sampleReader = new TypedSampleReader<Util::float64_t,  2, float>(); }
    }
    else if (extent == 1)
    {
        if      (pod==Util::kBooleanPOD)    { _parmType = xchg::PARMTYPE_BOOL;      _sampleReader = new TypedSampleReader<Util::bool_t,     1, bool>();         }
        else if (pod==Util::kUint8POD)      { _parmType = xchg::PARMTYPE_CHAR;      _sampleReader = new TypedSampleReader<Util::uint8_t,    1, char>();         }
        else if (pod==Util::kInt8POD)       { _parmType = xchg::PARMTYPE_CHAR;      _sampleReader = new TypedSampleReader<Util::int8_t,     1, char>();         }
        else if (pod==Util::kUint16POD)     { _parmType = xchg::PARMTYPE_INT;       _sampleReader = new TypedSampleReader<Util::uint16_t,   1, int>();          }
        else if (pod==Util::kInt16POD)      { _parmType = xchg::PARMTYPE_INT;       _sampleReader = new TypedSampleReader<Util::int16_t,    1, int>();          }
        else if (pod==Util::kUint32POD)     { _parmType = xchg::PARMTYPE_UINT;      _sampleReader = new TypedSampleReader<Util::uint32_t,   1, unsigned int>(); }
        else if (pod==Util::kInt32POD)      { _parmType = xchg::PARMTYPE_INT;       _sampleReader = new TypedSampleReader<Util::int32_t,    1, int>();          }
        else if (pod==Util::kUint64POD)     { _parmType = xchg::PARMTYPE_SIZE;      _sampleReader = new TypedSampleReader<Util::uint64_t,   1, size_t>();       }
        else if (pod==Util::kInt64POD)      { _parmType = xchg::PARMTYPE_SIZE;      _sampleReader = new TypedSampleReader<Util::int64_t,    1, size_t>();       }
        else if (pod==Util::kFloat16POD)    { _parmType = xchg::PARMTYPE_FLOAT;     _sampleReader = new TypedSampleReader<Util::float16_t,  1, float>();        }
        else if (pod==Util::kFloat32POD)    { _parmType = xchg::PARMTYPE_FLOAT;     _sampleReader = new TypedSampleReader<Util::float32_t,  1, float>();        }
        else if (pod==Util::kFloat64POD)    { _parmType = xchg::PARMTYPE_FLOAT;     _sampleReader = new TypedSampleReader<Util::float64_t,  1, float>();        }
        else if (pod==Util::kStringPOD)     { _parmType = xchg::PARMTYPE_STRING;    _sampleReader = new StringSampleReader<Util::string>();     }
        else if (pod==Util::kWstringPOD)    { _parmType = xchg::PARMTYPE_STRING;    _sampleReader = new StringSampleReader<Util::wstring>();    }
    }
}

int
scn::UserAttribParmBase::getSampleIndex(float time, Alembic::Abc::TimeSamplingPtr const& timeSampling, size_t numSamples)
{
    return timeSampling && numSamples > 0
        ? static_cast<int>(Alembic::Abc::ISampleSelector(
            static_cast<Alembic::Abc::chrono_t>(time),
            Alembic::Abc::ISampleSelector::TimeIndexType::kNearIndex).getIndex(
                timeSampling,
                numSamples))
        : -1;   // invalid index
}

void
scn::UserAttribParmBase::setCurrentParmValue(TreeNode& node, const void* rawData)
{
    if (rawData == nullptr)
        return;
    switch (_parmType)
    {
    case xchg::PARMTYPE_BOOL:
        {
            bool parm1b = false;
            (*_sampleReader)(rawData, &parm1b);
            node.setParameter<bool>(_parmName.c_str(), parm1b);
            break;
        }
    case xchg::PARMTYPE_CHAR:
        {
            char parm1c = 0;
            (*_sampleReader)(rawData, &parm1c);
            node.setParameter<char>(_parmName.c_str(), parm1c);
            break;
        }
    case xchg::PARMTYPE_INT:
        {
            int parm1i = 0;
            (*_sampleReader)(rawData, &parm1i);
            node.setParameter<int>(_parmName.c_str(), parm1i);
            break;
        }
    case xchg::PARMTYPE_INT_2:
        {
            math::Vector2i parm2i(math::Vector2i::Zero());
            (*_sampleReader)(rawData, parm2i.data());
            node.setParameter<math::Vector2i>(_parmName.c_str(), parm2i);
            break;
        }
    case xchg::PARMTYPE_INT_3:
        {
            math::Vector3i parm3i(math::Vector3i::Zero());
            (*_sampleReader)(rawData, parm3i.data());
            node.setParameter<math::Vector3i>(_parmName.c_str(), parm3i);
            break;
        }
    case xchg::PARMTYPE_INT_4:
        {
            math::Vector4i parm4i(math::Vector4i::Zero());
            (*_sampleReader)(rawData, parm4i.data());
            node.setParameter<math::Vector4i>(_parmName.c_str(), parm4i);
            break;
        }
    case xchg::PARMTYPE_UINT:
        {
            unsigned int parm1ui = 0;
            (*_sampleReader)(rawData, &parm1ui);
            node.setParameter<unsigned int>(_parmName.c_str(), parm1ui);
            break;
        }
    case xchg::PARMTYPE_SIZE:
        {
            size_t parm1ui64 = 0;
            (*_sampleReader)(rawData, &parm1ui64);
            node.setParameter<size_t>(_parmName.c_str(), parm1ui64);
            break;
        }
    case xchg::PARMTYPE_FLOAT:
        {
            float parm1f = 0;
            (*_sampleReader)(rawData, &parm1f);
            node.setParameter<float>(_parmName.c_str(), parm1f);
            break;
        }
    case xchg::PARMTYPE_FLOAT_2:
        {
            math::Vector2f parm2f(math::Vector2f::Zero());
            (*_sampleReader)(rawData, parm2f.data());
            node.setParameter<math::Vector2f>(_parmName.c_str(), parm2f);
            break;
        }
    case xchg::PARMTYPE_FLOAT_4:
        {
            math::Vector4f parm4f(math::Vector4f::Zero());
            (*_sampleReader)(rawData, parm4f.data());
            node.setParameter<math::Vector4f>(_parmName.c_str(), parm4f);
            break;
        }
    case xchg::PARMTYPE_STRING:
        {
            std::string parmStr;
            (*_sampleReader)(rawData, &parmStr);
            node.setParameter<std::string>(_parmName.c_str(), parmStr);
            break;
        }
    default:
        break;
    }
}

/////////////////////////////
// class ScalarUserAttribParm
/////////////////////////////
scn::ScalarUserAttribParm::ScalarUserAttribParm(const std::string&                   parmName,
                                                const Alembic::Abc::IScalarProperty& prop):
    UserAttribParmBase  (parmName, prop.getDataType()),
    _prop               (prop),
    _sampleIdx          (-1)
{
    FLIRT_LOG(LOG(DEBUG)
        << "creating parameter '" << _parmName << "' (ID = " << _parmId << ", type = " << std::to_string(_parmType) << ") "
        << "from scalar user attribute '" << prop.getName() << "' "
        << "(" << std::to_string(prop.getDataType().getPod()) << " x " << static_cast<int>(prop.getDataType().getExtent()) << ").";)
    if (!_prop.valid() ||
        _prop.getNumSamples() == 0 ||
        _prop.getTimeSampling() == nullptr)
    {
        std::stringstream sstr;
        sstr << "Scalar User Attribute '" << parmName << "' Construction";
        sys::Exception("Invalid property.", sstr.str().c_str());
    }
}

bool
scn::ScalarUserAttribParm::valid() const
{
    return UserAttribParmBase::valid() &&
        _prop.valid() &&
        _prop.getTimeSampling() &&
        _prop.getNumSamples() > 0;
}

void
scn::ScalarUserAttribParm::setCurrentTime(TreeNode& node, float time)
{
    assert(valid());

    const int sampleIdx = getSampleIndex(time, _prop.getTimeSampling(), _prop.getNumSamples());
    if (sampleIdx == _sampleIdx ||
        sampleIdx == -1)
        return; // sample did not change, there is nothing to do

    Alembic::Abc::ScalarSample sample(_prop.getDataType());

    _prop.get(&sample, sampleIdx);
    setCurrentParmValue(node, sample.getData());
    _sampleIdx = sampleIdx;
}

bool
scn::ScalarUserAttribParm::getTimeBounds(float& minTime, float& maxTime) const
{
    assert(valid());

    minTime = FLT_MAX;
    maxTime = -FLT_MAX;

    const size_t numSamples = _prop.getNumSamples();
    if (numSamples > 1)
    {
        const Alembic::Abc::TimeSampling& timeSampling = *_prop.getTimeSampling();

        minTime = static_cast<float>(timeSampling.getSampleTime(static_cast<Alembic::Abc::index_t>(0)));
        maxTime = static_cast<float>(timeSampling.getSampleTime(static_cast<Alembic::Abc::index_t>(numSamples-1)));
    }

    return minTime < maxTime;
}

/////////////////////////////
// class ArrayUserAttribParm
/////////////////////////////
scn::ArrayUserAttribParm::ArrayUserAttribParm(const std::string& parmName, const Alembic::Abc::IArrayProperty& prop):
    UserAttribParmBase  (parmName, prop.getDataType()),
    _prop               (prop),
    _sampleIdx          (-1)
{
    FLIRT_LOG(LOG(DEBUG)
        << "creating parameter '" << _parmName << "' (ID = " << _parmId << ", type = " << std::to_string(_parmType) << ") "
        << "from array user attribute '" << prop.getName() << "' "
        << "(" << std::to_string(prop.getDataType().getPod()) << " x " << static_cast<int>(prop.getDataType().getExtent()) << ").";)

    if (!_prop.valid() ||
        _prop.getNumSamples() == 0 ||
        _prop.getTimeSampling() == nullptr)
    {
        std::stringstream sstr;
        sstr << "Array User Attribute '" << parmName << "' Construction";
        sys::Exception("Invalid property.", sstr.str().c_str());
    }
}

bool
scn::ArrayUserAttribParm::valid() const
{
    return UserAttribParmBase::valid() &&
        _prop.valid() &&
        _prop.getTimeSampling() &&
        _prop.getNumSamples() > 0;
}

void
scn::ArrayUserAttribParm::setCurrentTime(TreeNode& node, float time)
{
    assert(valid());

    const int sampleIdx = getSampleIndex(time, _prop.getTimeSampling(), _prop.getNumSamples());
    if (sampleIdx == _sampleIdx ||
        sampleIdx == -1)
        return; // sample did not change, there is nothing to do

    Alembic::Abc::ArraySamplePtr sample = nullptr;

    _prop.get(sample, sampleIdx);
    if (sample)
        setCurrentParmValue(node, sample->getData());
    _sampleIdx = sampleIdx;
}

bool
scn::ArrayUserAttribParm::getTimeBounds(float& minTime, float& maxTime) const
{
    assert(valid());

    minTime = FLT_MAX;
    maxTime = -FLT_MAX;

    const size_t numSamples = _prop.getNumSamples();
    if (numSamples > 1)
    {
        const Alembic::Abc::TimeSampling& timeSampling = *_prop.getTimeSampling();

        minTime = static_cast<float>(timeSampling.getSampleTime(static_cast<Alembic::Abc::index_t>(0)));
        maxTime = static_cast<float>(timeSampling.getSampleTime(static_cast<Alembic::Abc::index_t>(numSamples-1)));
    }

    return minTime < maxTime;
}
