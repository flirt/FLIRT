// ========================================================================= //
// Copyright 2016-2018 SUPAMONKS_STUDIO                                      //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
//                                                                           //
// ========================================================================= //
#pragma once

#include "Curves-DataHolder.hpp"

namespace smks { namespace scn
{
    class AbstractCurvesSchema;

    //////////////////////////////
    // class Curves::DataHolder::Indices
    //////////////////////////////
    class Curves::DataHolder::Indices
    {
    public:
        typedef std::shared_ptr<Indices> Ptr;

    private:
        const AbstractCurvesSchema*                         _schema; // does not own it
        util::data::ArrayCache<std::vector<unsigned int>>   _indices;

    public:
        Indices();

        bool
        initialize(const AbstractCurvesSchema*, bool caching);

        //bool
        //initialized() const;

        void
        dispose();

        virtual
        size_t
        numIndicesPerPrimitive() const = 0;

        const unsigned int*
        get(size_t&);

        const unsigned int*
        set(const std::vector<unsigned int>&    firstCPIndices,
            size_t                              numBezierVertices,
            size_t&);

    protected:
        virtual
        void
        computeIndices(const std::vector<unsigned int>& firstCPIndices,
                       size_t                           numBezierVertices,
                       std::vector<unsigned int>&) const = 0;
    };


    ///////////////////////////////////////////
    // class Curves::DataHolder::PerBezierPathIndices
    ///////////////////////////////////////////
    class Curves::DataHolder::PerBezierPathIndices:
        public Curves::DataHolder::Indices
    {
    public:
        typedef std::shared_ptr<PerBezierPathIndices>   Ptr;

    public:
        static inline
        Ptr
        create()
        {
            Ptr ptr (new PerBezierPathIndices);

            return ptr;
        }

    protected:
        inline
        PerBezierPathIndices():
            Indices()
        { }

    public:
        virtual inline
        size_t
        numIndicesPerPrimitive() const
        {
            return 1;
        }

    private:
        void
        computeIndices(const std::vector<unsigned int>& firstCPIndices,
                       size_t                           numBezierVertices,
                       std::vector<unsigned int>&) const;
    };


    ////////////////////////////////////////////
    // class Curves::DataHolder::PerBezierCurveIndices
    ////////////////////////////////////////////
    class Curves::DataHolder::PerBezierCurveIndices:
        public Curves::DataHolder::Indices
    {
    public:
        typedef std::shared_ptr<PerBezierCurveIndices>  Ptr;

    public:
        static inline
        Ptr
        create()
        {
            Ptr ptr (new PerBezierCurveIndices);

            return ptr;
        }

    protected:
        inline
        PerBezierCurveIndices():
            Indices()
        { }

    public:
        virtual inline
        size_t
        numIndicesPerPrimitive() const
        {
            return 4;
        }

    private:
        void
        computeIndices(const std::vector<unsigned int>& firstCPIndices,
                       size_t                           numBezierVertices,
                       std::vector<unsigned int>&) const;
    };
}
}
