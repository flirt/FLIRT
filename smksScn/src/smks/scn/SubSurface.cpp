// ========================================================================= //
// Copyright 2016-2018 SUPAMONKS_STUDIO                                      //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
//                                                                           //
// ========================================================================= //
#include <smks/sys/Exception.hpp>
#include <smks/parm/BuiltIn.hpp>
#include <smks/parm/BuiltIns.hpp>

#include "smks/scn/SubSurface.hpp"

using namespace smks;

// static
scn::SubSurface::Ptr
scn::SubSurface::create(const char*             code,
                        const char*             name,
                        TreeNode::WPtr const&   parent)
{
    if (code == nullptr ||
        strlen(code) == 0)
        throw sys::Exception("Empty subsurface code.", "SubSurface Creation");

    Ptr ptr(new SubSurface(name, parent));

    ptr->build(ptr);
    ptr->setParameter<std::string>(parm::code(), code, parm::SKIPS_RESTRAINTS);

    return ptr;
}

// virtual
bool
scn::SubSurface::isParameterWritable(unsigned int parmId) const
{
    if (isParameterRelevantForClass(parmId))
        return parmId != parm::code().id();
    return Shader::isParameterWritable(parmId);
}

// virtual
bool
scn::SubSurface::isParameterRelevant(unsigned int parmId)const
{
    return isParameterRelevantForClass(parmId) || Shader::isParameterRelevant(parmId);
}

bool
scn::SubSurface::isParameterRelevantForClass(unsigned int parmId) const
{
    return parmId == parm::code().id();
}
