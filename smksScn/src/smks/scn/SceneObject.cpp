// ========================================================================= //
// Copyright 2016-2018 SUPAMONKS_STUDIO                                      //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
//                                                                           //
// ========================================================================= //
#include <smks/parm/BuiltIn.hpp>
#include <smks/parm/BuiltIns.hpp>
#include <smks/xchg/ParmEvent.hpp>

#include "smks/scn/SceneObject.hpp"

using namespace smks;

//-------------------
// parameter handling
//-------------------
// virtual
bool
scn::SceneObject::isParameterWritable(unsigned int parmId) const
{
    if (isParameterRelevantForClass(parmId))
        return false;
    return TreeNode::isParameterWritable(parmId);
}

// virtual
bool
scn::SceneObject::isParameterRelevant(unsigned int parmId)const
{
    return isParameterRelevantForClass(parmId) || TreeNode::isParameterRelevant(parmId);
}

bool
scn::SceneObject::isParameterRelevantForClass(unsigned int parmId) const
{
    return parmId == parm::currentlyVisible().id();
}

// virtual
bool
scn::SceneObject::mustSendToScene(const xchg::ParmEvent& evt) const
{
    return evt.parm() == parm::currentlyVisible().id() ||
        TreeNode::mustSendToScene(evt);
}
//-------------------
