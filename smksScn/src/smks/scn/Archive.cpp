// ========================================================================= //
// Copyright 2016-2018 SUPAMONKS_STUDIO                                      //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
//                                                                           //
// ========================================================================= //
#include <string>

#include <ImathVec.h>
#include <Alembic/AbcCoreFactory/IFactory.h>
#include <Alembic/AbcCoreHDF5/All.h>
#include <Alembic/AbcCoreOgawa/All.h>
#include <Alembic/AbcGeom/All.h>

#include <boost/unordered_set.hpp>
#include <boost/unordered_map.hpp>

#include <json/json.h>

#include <smks/parm/BuiltIn.hpp>
#include <smks/parm/BuiltIns.hpp>
#include <smks/sys/Log.hpp>
#include <smks/sys/Exception.hpp>
#include <smks/sys/ResourceDatabase.hpp>
#include <smks/sys/ResourcePathFinder.hpp>

#include "smks/sys/initialize_scn.hpp"

#include "smks/scn/Archive.hpp"
#include "smks/scn/Scene.hpp"
#include "smks/scn/PolyMesh.hpp"
#include "smks/scn/Curves.hpp"
#include "smks/scn/Points.hpp"
#include "smks/scn/Xform.hpp"
#include "smks/scn/Camera.hpp"
#include "debug/print.hpp"

#include <smks/util/string/path.hpp>

namespace smks { namespace scn
{
    ///////////////////////
    // class Archive::PImpl
    ///////////////////////
    class Archive::PImpl
    {
    private:
        class BuildVisitor;

    private:
        const std::string   _absPath;
        xchg::IdSet         _overridenVisible;
        xchg::IdSet         _overridenInvisible;

    public:
        explicit inline
        PImpl(const char*);

        inline
        void
        build(TreeNode::Ptr const&);

        inline
        const char*
        filePath() const
        {
            return _absPath.c_str();
        }
    };

    /////////////////////////////////////
    // class Archive::PImpl::BuildVisitor
    /////////////////////////////////////
    class Archive::PImpl::BuildVisitor
    {
    private:
        size_t  _numXforms;
        size_t  _numPoints;
        size_t  _numCurves;
        size_t  _numMeshes;
        size_t  _numCameras;

    public:
        inline
        BuildVisitor():
            _numXforms  (0),
            _numPoints  (0),
            _numCurves  (0),
            _numMeshes  (0),
            _numCameras (0)
        { }

        smks::scn::TreeNode::Ptr
        apply(const Alembic::Abc::IObject&, TreeNode::WPtr const&, TreeNode::WPtr const&);

        inline
        size_t
        numXforms() const
        {
            return _numXforms;
        }

        inline
        size_t
        numPoints() const
        {
            return _numPoints;
        }

        inline
        size_t
        numCurves() const
        {
            return _numCurves;
        }

        inline
        size_t
        numMeshes() const
        {
            return _numMeshes;
        }

        inline
        size_t
        numCameras() const
        {
            return _numCameras;
        }
    };
}
}

using namespace smks;
using namespace Alembic;

////////////////////////////////////
// class Archive::PImpl::BuildVisitor
////////////////////////////////////

scn::TreeNode::Ptr
scn::Archive::PImpl::BuildVisitor::apply(const Abc::IObject&    object,
                                        TreeNode::WPtr const&   parent,
                                        TreeNode::WPtr const&   archive)
{
    assert(parent.lock());

    if (!object.valid())
        throw sys::Exception("Invalid Alembic object.", "TreeNode Creation");

    TreeNode::Ptr           current         = nullptr;
    const Abc::MetaData&    metaData        = object.getMetaData();
    bool                    stopTraverse    = false;

    if (AbcGeom::IPolyMesh::matches(metaData))
    {
        current         = PolyMesh::create(object, parent, archive);
        stopTraverse    = true;
        ++_numMeshes;
    }
    else if (AbcGeom::IPoints::matches(metaData))
    {
        current         = Points::create(object, parent, archive);
        stopTraverse    = true;
        ++_numPoints;
    }
    else if (AbcGeom::ICurves::matches(metaData))
    {
        current         = Curves::create(object, parent, archive);
        stopTraverse    = true;
        ++_numCurves;
    }
    else if (AbcGeom::IXform::matches(metaData))
    {
        current         = Xform::create(object, parent, archive);
        ++_numXforms;
    }
    else if (AbcGeom::ICamera::matches(metaData))
    {
        current         = Camera::create(object, parent, archive);
        ++_numCameras;
    }
    else
    {
        FLIRT_LOG(LOG(DEBUG)
            << "object's metadata does not match any supported types "
            << "('" << object.getFullName() << "')";)
    }

    if (!stopTraverse)
        for (unsigned int i=0; i < object.getNumChildren(); ++i)
            apply(
                object.getChild(i),
                current ? current : parent,
                archive);
    else if (object.getNumChildren() > 0)
    {
        std::stringstream sstr;
        sstr
            << "'" << object.getFullName() << "' "
            << "(" << debug::getIObjectTypeName(object) << ") "
            << "has " << object.getNumChildren() << " ignored children:\n";
        debug::printChildren(sstr, object);
        FLIRT_LOG(LOG(DEBUG) << sstr.str();)
        std::cerr << sstr.str() << std::endl;
    }

    return current;
}


///////////////////////
// class Archive::PImpl
///////////////////////
// explicit
scn::Archive::PImpl::PImpl(const char* absPath):
    _absPath            (absPath),
    _overridenVisible   (),
    _overridenInvisible ()
{ }

void
scn::Archive::PImpl::build(TreeNode::Ptr const& self)
{
    assert(self);

    AbcCoreFactory::IFactory    factory;
    Abc::IArchive               archiveAbc = factory.getArchive(_absPath);
    //new Abc::IArchive(AbcCoreOgawa::ReadArchive(), _absPath, Abc::ErrorHandler::kQuietNoopPolicy);

    if (!archiveAbc.valid())
    {
        std::stringstream sstr;
        sstr << "Failed to load Alembic archive from '" << _absPath << "'.";
        FLIRT_LOG(LOG(DEBUG) << sstr.str();)
        throw sys::Exception(sstr.str().c_str(), "Archive Initialization");
    }

    std::cout << "Building the cache's hierarchy..."; std::cout.flush();
    clock_t start2 = clock();
    BuildVisitor visitor;

    visitor.apply(archiveAbc.getTop(), self, self);
    std::cout
        << "\rCache hierarchy from '" << _absPath << "'\n"
        << "(computed in " << (clock() - start2) / double(CLOCKS_PER_SEC) << " seconds)\n"
        << "\t- " << visitor.numXforms() << " xform(s)\n"
        << "\t- " << visitor.numMeshes() << " mesh(es)\n"
        << "\t- " << visitor.numPoints() << " point set(s)\n"
        << "\t- " << visitor.numCurves() << " curve set(s)\n"
        << "\t- " << visitor.numCameras() << " camera(s)\n";
}


////////////////
// class Archive
////////////////
// static
scn::Archive::Ptr
scn::Archive::create(const char*            filePath,
                     TreeNode::Ptr const&   parent)
{
    const size_t    BUFFER_SIZE = 512;
    char            BUFFER[BUFFER_SIZE];

    //-------------------------------------------
    // first, resolve the filepath to the archive
    sys::ResourcePathFinder::Ptr    pathFinder = parent ? parent->getResourcePathFinder() : nullptr;
    std::string                     resolved, absPath, baseName;
    try
    {
        resolved = std::string(filePath);
        if (pathFinder)
        {
            if (pathFinder->resolveFileName(filePath, BUFFER, BUFFER_SIZE) == 0)
            {
                std::stringstream sstr;
                sstr << "Could not resolve archive filename '" << filePath << "'.";
                FLIRT_LOG(LOG(DEBUG) << sstr.str();)
                throw sys::Exception(sstr.str().c_str(), "Archive Creation");
            }
            resolved = std::string(BUFFER);
        }
        if (util::path::abspath(resolved.c_str(), BUFFER, BUFFER_SIZE))
            absPath = std::string(BUFFER);
        if (util::path::basename(absPath.c_str(), BUFFER, BUFFER_SIZE))
            baseName = std::string(BUFFER);
    }
    catch(const sys::Exception& e)
    {
        std::stringstream sstr;
        sstr << "Abort archive node creation: " << e.what();
        FLIRT_LOG(LOG(DEBUG) << sstr.str();)
        std::cerr << sstr.str() << std::endl;

        return nullptr;
    }
    //-------------------------------------------

    assert(!absPath.empty());
    assert(!baseName.empty());

    Ptr ptr (new Archive(absPath.c_str(), baseName.c_str(), parent));
    ptr->build(ptr);
    return ptr;
}

scn::Archive::Archive(const char*           absPath,
                      const char*           baseName,
                      TreeNode::Ptr const&  parent):
    TreeNode(baseName, parent),
    _pImpl  (new PImpl(absPath))
{ }

scn::Archive::~Archive()
{
#ifdef DEBUG_DTOR
    FLIRT_LOG(LOG(DEBUG) << "deleting archive '" << name() << "' (ID = " << id() << ")";)
#endif // DEBUG_DTOR

    erase();
    delete _pImpl;
}

// virtual
void
scn::Archive::build(TreeNode::Ptr const& self)
{
    assert(self);

    TreeNode::build(self);
    _pImpl->build(self);
}

const char *const
scn::Archive::filePath() const
{
    return _pImpl->filePath();
}

