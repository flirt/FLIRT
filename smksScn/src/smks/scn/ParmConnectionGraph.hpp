// ========================================================================= //
// Copyright 2016-2018 SUPAMONKS_STUDIO                                      //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
//                                                                           //
// ========================================================================= //
#pragma once

#include <list>
#include <stack>
#include <functional>
#include <boost/unordered_set.hpp>

#include "smks/scn/Scene.hpp"

namespace smks { namespace scn
{
    class ParmConnectionGraph
    {
    private:
        //////////////
        // class Node
        //////////////
        class Node
        {
        public:
            typedef boost::unordered_set<std::size_t> Successors;
        private:
            const unsigned int  _nodeId, _parmId;   //<! data relative to the SmkS scene
            Successors          _successors;        //<! connection graph IDs
            size_t              _numPredecessors;

        public:
            inline
            Node(unsigned int nodeId, unsigned int parmId):
                _nodeId(nodeId), _parmId(parmId), _successors(), _numPredecessors(0)
            { }

            inline
            unsigned int
            nodeId() const
            {
                return _nodeId;
            }

            inline
            unsigned int
            parmId() const
            {
                return _parmId;
            }

            inline
            size_t
            numPredecessors() const
            {
                return _numPredecessors;
            }

            inline
            void
            incrNumPredecessors()
            {
                ++_numPredecessors;
            }

            inline
            void
            decrNumPredecessors()
            {
                --_numPredecessors;
            }

            inline
            const Successors&
            successors() const
            {
                return _successors;
            }

            inline
            void
            addSuccessor(std::size_t id)
            {
                _successors.insert(id);
            }

            inline
            void
            removeSuccessor(std::size_t id)
            {
                _successors.erase(id);
            }

            inline
            bool
            isolated() const
            {
                return _numPredecessors == 0 && _successors.empty();
            }

            inline
            bool
            operator==(const Node& other) const
            {
                return _nodeId == other._nodeId && _parmId == other._parmId;
            }

            static inline
            std::size_t
            getId(unsigned int nodeId, unsigned int parmId)
            {
                std::size_t seed = 0;
                if (nodeId != 0 && parmId != 0)
                {
                    boost::hash_combine(seed, nodeId);
                    boost::hash_combine(seed, parmId);
                }
                return seed;
            }

            static inline
            std::size_t
            getId(const Node& n)
            {
                return getId(n.nodeId(), n.parmId());
            }
        };

        ////////////////////
        // struct NodeStatus
        ////////////////////
        struct NodeStatus
        {
            bool visited, onStack;
        public:
            inline
            NodeStatus():
                visited(false), onStack(false)
            { }
        };

    private:
        typedef boost::unordered_map<std::size_t, Node>         NodeMap;
        typedef boost::unordered_map<std::size_t, NodeStatus>   NodeStatusMap;

    private:
        NodeMap _graph;

    public:
        ParmConnectionGraph();

    public:

        void
        clear();

        void
        add(unsigned int inNodeId,
            unsigned int inParmId,
            unsigned int outNodeId,
            unsigned int outParmId);

        void
        remove(unsigned int inNodeId,
               unsigned int inParmId,
               unsigned int outNodeId,
               unsigned int outParmId);

        bool
        cyclic() const;

    private:
        bool
        cyclic(size_t, NodeStatusMap&) const;
    };
}
}
