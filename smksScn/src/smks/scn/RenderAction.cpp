// ========================================================================= //
// Copyright 2016-2018 SUPAMONKS_STUDIO                                      //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
//                                                                           //
// ========================================================================= //
#include <smks/xchg/SceneEvent.hpp>
#include <smks/xchg/ParmEvent.hpp>
#include <smks/parm/BuiltIns.hpp>

#include "smks/scn/RenderAction.hpp"
#include "smks/scn/ParmEdit.hpp"
#include "smks/scn/Scene.hpp"
#include "smks/scn/AbstractSceneObserver.hpp"
#include "smks/scn/AbstractParmObserver.hpp"

namespace smks { namespace scn
{
    ////////////////////////////
    // class RenderAction::PImpl
    ////////////////////////////
    class RenderAction::PImpl
    {
    private:
        class SceneObserver;        //!< observer used to track scene object node creation/deletion
        class SceneObjParmObserver; //!< observer used to redirect all visible scene objects' to PImpl
    private:
        typedef std::shared_ptr<SceneObserver>          SceneObserverPtr;
        typedef std::shared_ptr<SceneObjParmObserver>   SceneObjParmObserverPtr;
    private:
        RenderAction&           _self;
        SceneObserverPtr        _sceneObserver;
        SceneObjParmObserverPtr _sceneObjParmObserver;
        //<! rendering-centric nodes bound to render action
        unsigned int    _camera;
        unsigned int    _frameBuffer;
        unsigned int    _toneMapper;
        unsigned int    _renderer;
        unsigned int    _integrator;
        unsigned int    _pixelFiter;
        unsigned int    _samplerFactory;
        bool            _iterationBeingReset;

    public:
        explicit inline
        PImpl(RenderAction& self):
            _self                   (self),
            _sceneObserver          (nullptr),
            _sceneObjParmObserver   (nullptr),
            //----------------------
            _camera                 (0),
            _frameBuffer            (0),
            _toneMapper             (0),
            _renderer               (0),
            _integrator             (0),
            _pixelFiter             (0),
            _samplerFactory         (0),
            //----------------------
            _iterationBeingReset    (false)
        { }
    public:
        inline
        void
        build();
        inline
        void
        erase();
    public:
        inline
        ~PImpl()
        {
            erase();
        }
        inline
        void
        handleSceneEvent(Scene&, const xchg::SceneEvent&);
        inline
        void
        handleParmEventFromRenderAction(const xchg::ParmEvent&);
        inline
        void
        handleParmEventFromSceneObject(TreeNode&, const xchg::ParmEvent&);
    private:
        inline
        void
        beginParameterTracking(unsigned int);
        inline
        void
        endParameterTracking(unsigned int);
        inline
        void
        getRendererSourceNodes();
        inline
        void
        getFrameBufferSourceNodes();
        inline
        bool
        filterParmEventFromRenderAction(const TreeNode&, const xchg::ParmEvent&) const;
        inline
        bool
        filterParmEventFromSceneObject(const TreeNode&, const xchg::ParmEvent&) const;
    };

    ///////////////////////////////////////////
    // class RenderAction::PImpl::SceneObserver
    ///////////////////////////////////////////
    class RenderAction::PImpl::SceneObserver:
        public AbstractSceneObserver
    {
    public:
        typedef std::shared_ptr<SceneObserver> Ptr;
    private:
        RenderAction::PImpl& _pImpl;
    public:
        static inline
        Ptr
        create(RenderAction::PImpl& pImpl)
        {
            Ptr ptr(new SceneObserver(pImpl));
            return ptr;
        }
    private:
        explicit inline
        SceneObserver(RenderAction::PImpl& pImpl):
            _pImpl(pImpl)
        { }
        inline
        void
        handle(Scene& scene, const xchg::SceneEvent& evt)
        {
            _pImpl.handleSceneEvent(scene, evt);
        }
    };

    ///////////////////////////////////////////
    // class RenderAction::PImpl::SceneObserver
    ///////////////////////////////////////////
    class RenderAction::PImpl::SceneObjParmObserver:
        public AbstractParmObserver
    {
    public:
        typedef std::shared_ptr<SceneObjParmObserver> Ptr;
    private:
        RenderAction::PImpl& _pImpl;
    public:
        static inline
        Ptr
        create(RenderAction::PImpl& pImpl)
        {
            Ptr ptr(new SceneObjParmObserver(pImpl));
            return ptr;
        }
    private:
        explicit inline
        SceneObjParmObserver(RenderAction::PImpl& pImpl):
            _pImpl(pImpl)
        { }
        inline
        void
        handle(scn::TreeNode& node, const xchg::ParmEvent& evt)
        {
            _pImpl.handleParmEventFromSceneObject(node, evt);
        }
    };

}
}

using namespace smks;

////////////////////////////
// class RenderAction::PImpl
////////////////////////////
void
scn::RenderAction::PImpl::build()
{
    TreeNode::Ptr const&    root    = _self.root().lock();
    Scene*                  scene   = root ? root->asScene() : nullptr;
    if (!scene)
        throw sys::Exception("Invalid root scene.", "Render Action PImpl Construction");

    // register observer to the scene in order to track drawable creation/deletion
    _sceneObserver = SceneObserver::create(*this);
    scene->registerObserver(_sceneObserver);

    xchg::IdSet nodes;

    // register parameter observer to all drawables
    _sceneObjParmObserver = SceneObjParmObserver::create(*this);
    scene->getIds(nodes);
    for (xchg::IdSet::const_iterator id = nodes.begin(); id != nodes.end(); ++id)
        beginParameterTracking(*id);

    // gather IDs of RT nodes involved with the render action
    _self.getParameter<unsigned int>(parm::cameraId(),      _camera);
    _self.getParameter<unsigned int>(parm::frameBufferId(), _frameBuffer);
    _self.getParameter<unsigned int>(parm::rendererId(),    _renderer);
    getFrameBufferSourceNodes();
    getRendererSourceNodes();

    _iterationBeingReset = false;
}

void
scn::RenderAction::PImpl::erase()
{
    TreeNode::Ptr const&    root    = _self.root().lock();
    Scene*                  scene   = root ? root->asScene() : nullptr;
    if (!scene)
        return;

    xchg::IdSet nodes;

    // deregister all parameter observer of drawables
    scene->getIds(nodes);
    for (xchg::IdSet::const_iterator id = nodes.begin(); id != nodes.end(); ++id)
        endParameterTracking(*id);

    // deregister observer to the scene in order to track drawable creation/deletion
    scene->deregisterObserver(_sceneObserver);

    _sceneObjParmObserver   = nullptr;
    _sceneObserver          = nullptr;

    _camera         = 0;
    _frameBuffer    = 0;
    _renderer       = 0;
    _integrator     = 0;
    _pixelFiter     = 0;
    _samplerFactory = 0;

    _iterationBeingReset = false;
}

void
scn::RenderAction::PImpl::handleSceneEvent(Scene&, const xchg::SceneEvent& evt)
{
    if (evt.type() == xchg::SceneEvent::NODE_CREATED)
        beginParameterTracking(evt.node());
    else if (evt.type() == xchg::SceneEvent::NODE_DELETED)
        endParameterTracking(evt.node());
}

void
scn::RenderAction::PImpl::beginParameterTracking(unsigned int nodeId)
{
    TreeNode::Ptr const& node = _self.getNodeFromRoot(nodeId);
    if (!node)
        return;

    if (node->nodeClass() & (xchg::CAMERA | xchg::DRAWABLE | xchg::LIGHT))
        node->registerObserver(_sceneObjParmObserver);
}

void
scn::RenderAction::PImpl::endParameterTracking(unsigned int nodeId)
{
    TreeNode::Ptr const& node = _self.getNodeFromRoot(nodeId);
    if (!node)
        return;

    if (node->nodeClass() & (xchg::CAMERA | xchg::DRAWABLE | xchg::LIGHT))
        node->deregisterObserver(_sceneObjParmObserver);
}

void
scn::RenderAction::PImpl::handleParmEventFromRenderAction(const xchg::ParmEvent& evt)
{
    TreeNode::Ptr const& node = _self.getNodeFromRoot(evt.node());
    if (!node)
        return;

    //--------------------------------------------
    // update Rt nodes involved with render action
    if (node->id() == _self.id())
    {
        if (evt.parm() == parm::cameraId().id())
            node->getParameter<unsigned int>(parm::cameraId(), _camera);
        else if (evt.parm() == parm::frameBufferId().id())
            node->getParameter<unsigned int>(parm::frameBufferId(), _frameBuffer);
        else if (evt.parm() == parm::rendererId().id())
        {
            node->getParameter<unsigned int>(parm::rendererId(), _renderer);
            getRendererSourceNodes();
        }
    }
    else if (node->id() == _renderer)
    {
        if (evt.parm() == parm::integratorId().id())
            node->getParameter<unsigned int>(parm::integratorId(), _integrator);
        else if (evt.parm() == parm::pixelFilterId().id())
            node->getParameter<unsigned int>(parm::pixelFilterId(), _pixelFiter);
        else if (evt.parm() == parm::samplerFactoryId().id())
            node->getParameter<unsigned int>(parm::samplerFactoryId(), _samplerFactory);
    }
    else if (node->id() == _frameBuffer)
    {
        if (evt.parm() == parm::toneMapperId().id())
            node->getParameter<unsigned int>(parm::toneMapperId(), _frameBuffer);
    }
    //--------------------------------------------

    if (node->id() == _self.id())
    {
        if (evt.parm() == parm::rendering().id() &&
            evt.type() == xchg::ParmEvent::PARM_REMOVED)
        {
            _self.incrIterationCount(); // rendering ended  -> increment iteration if progressive rendering on
            _iterationBeingReset = false;
        }
    }

    //############################################
    if (!_iterationBeingReset &&
        filterParmEventFromRenderAction(*node, evt))
    {
        _self.resetIterationCount();
        _iterationBeingReset = true;
    }
    //############################################
}

void
scn::RenderAction::PImpl::handleParmEventFromSceneObject(scn::TreeNode&,
                                                         const xchg::ParmEvent& evt)
{
    if (_iterationBeingReset)
        return;

    TreeNode::Ptr const& node = _self.getNodeFromRoot(evt.node());
    if (!node)
        return;

    //############################################
    if (node &&
        filterParmEventFromSceneObject(*node, evt))
    {
        _self.resetIterationCount();
        _iterationBeingReset = true;
    }
    //############################################
}

bool
scn::RenderAction::PImpl::filterParmEventFromRenderAction(const TreeNode&           node,
                                                          const xchg::ParmEvent&    evt) const
{
    if (node.id() == _self.id())    // render action
        return
            evt.parm() != parm::filePath        ().id() &&
            evt.parm() != parm::rendering       ().id() &&
            evt.parm() != parm::iteration       ().id() &&
            evt.parm() != parm::isProgressive   ().id() &&
            node.isParameterRelevant(evt.parm());

    else if (node.id() == _camera)
        return node.isParameterRelevant(evt.parm());

    else if (node.id() == _frameBuffer)
        return
            evt.parm() != parm::storeRawRgba    ().id() &&
            evt.parm() != parm::storeRgbaAsHalf ().id() &&
            evt.parm() != parm::verbosityLevel  ().id() &&
            evt.parm() != parm::denoiserId      ().id() &&
            evt.parm() != parm::renderPassIds   ().id() &&
            node.isParameterRelevant(evt.parm());

    else if (node.id() == _toneMapper)
        return node.isParameterRelevant(evt.parm());

    else if (node.id() == _renderer)
        return
            evt.parm() != parm::accumulate  ().id() &&
            evt.parm() != parm::showProgress().id() &&
            node.isParameterRelevant(evt.parm());

    else if (node.id() == _integrator)
        return node.isParameterRelevant(evt.parm());

    else if (node.id() == _samplerFactory)
        return
            evt.parm() != parm::numSamples().id() &&
            node.isParameterRelevant(evt.parm());

    else if (node.id() == _pixelFiter)
        return node.isParameterRelevant(evt.parm());

    else
        return false;
}

bool
scn::RenderAction::PImpl::filterParmEventFromSceneObject(const TreeNode&        node,
                                                         const xchg::ParmEvent& evt) const
{
    if (node.nodeClass() & xchg::CAMERA)
        return node.id() == _camera &&
            node.isParameterRelevant(evt.parm());
    else if (node.nodeClass() & xchg::SCENE_OBJECT)
        return !evt.comesFromLink() &&
            node.isParameterRelevant(evt.parm());
    else if (node.nodeClass() & xchg::SHADER)
        return evt.comesFromLink() &&
            node.isParameterRelevant(evt.parm());
    else
        return false;
}

void
scn::RenderAction::PImpl::getRendererSourceNodes()
{
    TreeNode::Ptr const& node = _self.getNodeFromRoot(_renderer);
    if (node)
    {
        node->getParameter<unsigned int>(parm::integratorId(),      _integrator);
        node->getParameter<unsigned int>(parm::pixelFilterId(),     _pixelFiter);
        node->getParameter<unsigned int>(parm::samplerFactoryId(),  _samplerFactory);
    }
    else
    {
        _integrator     = 0;
        _pixelFiter     = 0;
        _samplerFactory = 0;
    }
}

void
scn::RenderAction::PImpl::getFrameBufferSourceNodes()
{
    TreeNode::Ptr const& node = _self.getNodeFromRoot(_frameBuffer);
    if (node)
        node->getParameter<unsigned int>(parm::toneMapperId(), _toneMapper);
    else
        _toneMapper = 0;
}


/////////////////////
// class RenderAction
/////////////////////
// static
scn::RenderAction::Ptr
scn::RenderAction::create(const char* name, TreeNode::WPtr const& parent)
{
    Ptr ptr(new RenderAction(name, parent));
    ptr->build(ptr);
    return ptr;
}

scn::RenderAction::RenderAction(const char* name, TreeNode::WPtr const& parent):
    GraphAction (name, parent),
    _pImpl      (new PImpl(*this))
{ }

scn::RenderAction::~RenderAction()
{
    delete _pImpl;
}

// virtual
void
scn::RenderAction::build(RenderAction::Ptr const& ptr)
{
    GraphAction::build(ptr);
    //---
    _pImpl->build();
}

// virtual
void
scn::RenderAction::erase()
{
    _pImpl->erase();
    //---
    GraphAction::erase();
}

// virtual
void
scn::RenderAction::handleParmEvent(const xchg::ParmEvent& evt)
{
    GraphAction::handleParmEvent(evt);
    //---
    _pImpl->handleParmEventFromRenderAction(evt);
}

// virtual
bool
scn::RenderAction::isParameterWritable(unsigned int parmId) const
{
    if (isParameterRelevantForClass(parmId))
        return parmId != parm::iteration().id();
    return GraphAction::isParameterWritable(parmId);
}

// virtual
bool
scn::RenderAction::isParameterRelevant(unsigned int parmId)const
{
    return isParameterRelevantForClass(parmId) || GraphAction::isParameterRelevant(parmId);
}

bool
scn::RenderAction::isParameterRelevantForClass(unsigned int parmId) const
{
    return
        parmId == parm::filePath        ().id() ||
        parmId == parm::rendererId      ().id() ||
        parmId == parm::frameBufferId   ().id() ||
        parmId == parm::cameraId        ().id() ||
        parmId == parm::rendering       ().id() ||
        parmId == parm::isProgressive   ().id() ||
        parmId == parm::iteration       ().id();
}

void
scn::RenderAction::resetIterationCount()
{
    size_t iteration = 0;

    getParameter<size_t>(parm::iteration(), iteration);
    if (iteration > 0)
        setParameter<size_t>(parm::iteration(), 0, parm::SKIPS_RESTRAINTS);
}

void
scn::RenderAction::incrIterationCount()
{
    size_t iteration = 0;

    getParameter<size_t>(parm::iteration(), iteration);
    setParameter<size_t>(parm::iteration(), ++iteration, parm::SKIPS_RESTRAINTS);
}
