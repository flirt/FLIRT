// ========================================================================= //
// Copyright 2016-2018 SUPAMONKS_STUDIO                                      //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
//                                                                           //
// ========================================================================= //

#include "Curves-DataHolder-Indices.hpp"
#include "AbstractCurvesSchema.hpp"

using namespace smks;

//////////////////////////////
// class Curves::DataHolder::Indices
//////////////////////////////
scn::Curves::DataHolder::Indices::Indices():
    _schema (nullptr),
    _indices()
{ }

bool
scn::Curves::DataHolder::Indices::initialize(const AbstractCurvesSchema* schema, bool caching)
{
    assert(schema);
    dispose();
    _schema = schema;

    if (_schema->numSamples() > 0)
    {
        _indices.initialize(caching ? schema->numIndexBufferSamples() : 0);
        return true;
    }
    else
        return false;
}

//bool
//scn::Curves::DataHolder::Indices::initialized() const
//{
//  return _schema &&
//      _schema->numSamples() > 0;
//}

void
scn::Curves::DataHolder::Indices::dispose()
{
    _schema = nullptr;

    _indices.dirtyAll();
    _indices.dispose();
}

const unsigned int*
scn::Curves::DataHolder::Indices::get(size_t& numIndices)
{
    numIndices = 0;

    if (!_schema || _schema->numSamples() == 0)
        return nullptr;

    const int                           indexSampleId   = _schema->currentIndexSampleIdx();
    const std::vector<unsigned int>*    cache           = _indices.get(indexSampleId);
    if (cache)
    {
        numIndices = cache->size();
        return numIndices > 0
            ? &cache->front()
            : nullptr;
    }

    // actual computation
    size_t                      numBezierVertices = 0;
    std::vector<size_t>         numBezierCurvesPerPath;
    std::vector<unsigned int>   firstCPIndices;

    getCubicBezierIndexBuffer(_schema, numBezierVertices, numBezierCurvesPerPath, firstCPIndices);

    return set(firstCPIndices, numBezierVertices, numIndices);
}

const unsigned int*
scn::Curves::DataHolder::Indices::set(
    const std::vector<unsigned int>&    firstCPIndices,
    size_t                              numBezierVertices,
    size_t&                             numIndices)
{
    numIndices = 0;

    if (!_schema || _schema->numSamples() == 0)
        return nullptr;

    std::vector<unsigned int>           indices;
    const std::vector<unsigned int>*    cached          = nullptr;
    const int                           indexSampleId   = _schema->currentIndexSampleIdx();

    computeIndices(firstCPIndices, numBezierVertices, indices);
    cached      = _indices.set(indexSampleId, indices);
    numIndices  = cached
        ? cached->size()
        : 0;

    return cached && !cached->empty()
        ? &cached->front()
        : nullptr;
}

////////////////////////////////////////////
// class Curves::DataHolder::PerBezierPathIndices
////////////////////////////////////////////
void
scn::Curves::DataHolder::PerBezierPathIndices::computeIndices(
    const std::vector<unsigned int>&    firstCPIndices,
    size_t                              numBezierVertices,
    std::vector<unsigned int>&          indices) const
{
    indices = firstCPIndices;
}

////////////////////////////////////////////
// class Curves::DataHolder::PerBezierCurveIndices
////////////////////////////////////////////
void
scn::Curves::DataHolder::PerBezierCurveIndices::computeIndices(
    const std::vector<unsigned int>&    firstCPIndices,
    size_t                              numBezierVertices,
    std::vector<unsigned int>&          indices) const
{
    indices.clear();
    indices.reserve(firstCPIndices.size() << 2);
    for (size_t i = 0; i < firstCPIndices.size(); ++i)
    {
        assert(firstCPIndices[i] >= 0 && firstCPIndices[i] + 3 < static_cast<int>(numBezierVertices));
        for (unsigned int j = 0; j < 4; ++j)
            indices.push_back(firstCPIndices[i] + j);
    }
}
