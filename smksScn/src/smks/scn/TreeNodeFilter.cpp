// ========================================================================= //
// Copyright 2016-2018 SUPAMONKS_STUDIO                                      //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
//                                                                           //
// ========================================================================= //
#include <smks/util/string/Regex.hpp>
#include "smks/scn/TreeNodeFilter.hpp"

namespace smks { namespace scn
{
    class FilterTreeNodeByName::PImpl
    {
    private:
        const char* const           _query;
        util::string::Regex::Ptr    _regex;
        const bool                  _useFullName;
    public:
        inline
        PImpl(const char*   query,
              bool          isQueryRegex,
              bool          useFullName):
            _query      (query),
            _regex      (isQueryRegex ? util::string::Regex::create(query) : nullptr),
            _useFullName(useFullName)
        { }

        inline
        bool
        operator()(const TreeNode& n) const
        {
            const char* name = _useFullName
                ? n.fullName()
                : n.name();
            return _regex
                ? _regex->match(name)
                : strcmp(name, _query) == 0;
        }
    };
}
}

using namespace smks;

scn::FilterTreeNodeByName::FilterTreeNodeByName(const char* query,
                                                bool        isQueryRegex,
                                                bool        useFullName):
    _pImpl(new PImpl(query, isQueryRegex, useFullName))
{ }

scn::FilterTreeNodeByName::~FilterTreeNodeByName()
{
    delete _pImpl;
}

bool
scn::FilterTreeNodeByName::operator()(const TreeNode& n) const
{
    return (*_pImpl)(n);
}
