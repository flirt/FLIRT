// ========================================================================= //
// Copyright 2016-2018 SUPAMONKS_STUDIO                                      //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
//                                                                           //
// ========================================================================= //
#pragma once

#include <Alembic/AbcGeom/IPolyMesh.h>

#include "AbstractPolyMeshSchema.hpp"

namespace smks { namespace scn
{
    class TreeNode;
    class SchemaBasedSceneObject;
    class SchemaBaseFromAbc;

    class PolyMeshSchemaFromAbc:
        public AbstractPolyMeshSchema
    {
    private:
        struct SampleHolder;

    private:
        const Alembic::Abc::IObject         _object;    //<! raw object
        SchemaBaseFromAbc*                  _base;
        Alembic::AbcGeom::IPolyMeshSchema   _mesh;      //<! typed object's schema
        size_t                              _numVertexBufferSamples;
        size_t                              _numIndexBufferSamples;
        PropertyFlags                       _flags;
        SampleHolder*                       _locks;

    public:
        PolyMeshSchemaFromAbc(SchemaBasedSceneObject&,
                              const Alembic::Abc::IObject&,
                              std::weak_ptr<TreeNode> const& archive);

        ~PolyMeshSchemaFromAbc();

    public:
        const SchemaBasedSceneObject&
        node() const;
        SchemaBasedSceneObject&
        node();

        inline
        bool
        lazyInitialization() const
        {
            return true;
        }
        void
        uninitialize();
        size_t  //<! return number of samples
        initialize();

        void
        uninitializeUserAttribs();
        void
        initializeUserAttribs();
    private:
        const Alembic::AbcGeom::IPolyMeshSchema&
        getAbcSchema();
    public:
        //-------------------
        // parameter handling
        //-------------------
        bool
        isParameterRelevantForClass(unsigned int) const;
        bool
        isParameterReadable(unsigned int) const;
        char
        isParameterWritable(unsigned int) const;
        bool
        overrideParameterDefaultValue(unsigned int, parm::Any&) const;
        void
        handle(const xchg::ParmEvent&);
        bool
        mustSendToScene(const xchg::ParmEvent&) const;
        //-------------------

        bool //<! return current visibility
        setCurrentTime(float);

        size_t
        numSamples() const;

        inline
        size_t
        numVertexBufferSamples() const
        {
            return _numVertexBufferSamples;
        }

        inline
        size_t
        numIndexBufferSamples() const
        {
            return _numIndexBufferSamples;
        }

        int
        currentSampleIdx() const;

        int
        currentVertexSampleIdx() const;

        int
        currentIndexSampleIdx() const;

        void
        getStoredTimes(xchg::Vector<float>&) const;

        bool
        has(Property) const;

        ////////////////////////////////
        // vertex buffer related queries
        ////////////////////////////////
        const float*
        lockPositions(size_t&) const;

        void
        unlockPositions() const;

        const float*
        lockNormals(size_t&) const;

        void
        unlockNormals() const;

        const float*
        lockVelocities(size_t&) const;

        void
        unlockVelocities() const;

        const float*
        lockTexCoords(size_t&) const; // (u, v)*

        void
        unlockTexCoords() const;

        void
        getSelfBounds(xchg::BoundingBox&) const;

        ///////////////////////////////
        // index buffer related queries
        ///////////////////////////////
        size_t
        getNumVertices() const;

        const int*
        lockFaceCounts(size_t&) const;

        void
        unlockFaceCounts() const;

        const int*
        lockFaceIndices(size_t&) const;

        void
        unlockFaceIndices() const;

        const unsigned int*
        lockTexCoordsIndices(size_t&) const;

        void
        unlockTexCoordsIndices() const;

    private:
        ////////////////////////////////
        // vertex buffer related queries
        ////////////////////////////////
        const float*
        lockPositions(int vertexSampleId, size_t&) const;

        const float*
        lockNormals(int vertexSampleId, size_t&) const;

        const float*
        lockVelocities(int vertexSampleId, size_t&) const;

        const float*
        lockTexCoords(int vertexSampleId, size_t&) const; // (u, v)*

        void
        getSelfBounds(int vertexSampleId, xchg::BoundingBox&) const;

        ///////////////////////////////
        // index buffer related queries
        ///////////////////////////////
        size_t
        getNumVertices(int vertexSampleId) const;

        const int*
        lockFaceCounts(int indexSampleId, size_t&) const;

        const int*
        lockFaceIndices(int indexSampleId, size_t&) const;

        const unsigned int*
        lockTexCoordsIndices(int indexSampleId, size_t&) const;

    private:
        static
        bool
        checkBounds(const Alembic::AbcGeom::IPolyMeshSchema&);

        static
        bool
        checkNormals(const Alembic::AbcGeom::IPolyMeshSchema&);

        static
        bool
        checkMotions(const Alembic::AbcGeom::IPolyMeshSchema&);

        static
        bool
        checkTexCoords(const Alembic::AbcGeom::IPolyMeshSchema&);
    };
}
}
