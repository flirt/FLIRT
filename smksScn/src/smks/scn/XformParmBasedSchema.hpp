// ========================================================================= //
// Copyright 2016-2018 SUPAMONKS_STUDIO                                      //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
//                                                                           //
// ========================================================================= //
#pragma once

#include <smks/xchg/Vector.hpp>
#include "smks/scn/SchemaBasedSceneObject.hpp"
#include "AbstractXformSchema.hpp"

namespace smks { namespace scn
{
    //<! special xform schema class allowing for users to control transforms via the following set of node parameters :
    //  - (eyePosition, targetPosition, upVector)
    //  - (translate, rotate, rotateOrder, scale)
    class XformParmBasedSchema:
        public AbstractXformSchema
    {
    private:
        SchemaBasedSceneObject& _node;  //<! used to get user parameters
        bool                    _initialized;

    public:
        explicit
        XformParmBasedSchema(SchemaBasedSceneObject&);

    public:
        inline
        SchemaBasedSceneObject&
        node()
        {
            return _node;
        }
        inline
        const SchemaBasedSceneObject&
        node() const
        {
            return _node;
        }

        //-------------------
        // parameter handling
        //-------------------
        char
        isParameterWritable(unsigned int) const;
        bool
        isParameterRelevantForClass(unsigned int) const;
        bool
        preprocessParameterEdit(const char*, unsigned int, parm::ParmFlags, const parm::Any&);
        void
        handle(const xchg::ParmEvent&);
        //-------------------

        inline bool     lazyInitialization() const  { return false; }
        inline void     uninitialize()              { _initialized = false; }
        inline size_t   initialize()                { _initialized = true; return numSamples(); }
        inline void     uninitializeUserAttribs()   { }
        inline void     initializeUserAttribs()     { }

        inline size_t   numSamples()        const   { return _initialized ? 1 : 0; }    // constant
        inline int      currentSampleIdx()  const   { return _initialized ? 0 : -1; }   // constant
        inline bool     setCurrentTime(float)       { return _initialized; }            // always visible
        inline void     getStoredTimes(xchg::Vector<float>& times) const { times.clear(); }

        void
        synchronizeTransform(math::Affine3f&,
                             bool& inherits);
    };
}
}
