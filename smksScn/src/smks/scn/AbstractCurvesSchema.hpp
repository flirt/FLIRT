// ========================================================================= //
// Copyright 2016-2018 SUPAMONKS_STUDIO                                      //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
//                                                                           //
// ========================================================================= //
#pragma once

#include "AbstractObjectSchema.hpp"
#include "BasisType.hpp"
#include "CurveOrder.hpp"
#include "BufferAttributes.hpp"

namespace Imath
{
    template <class T> class Matrix44;
}

namespace smks
{
    namespace xchg
    {
        struct BoundingBox;
    }
    namespace scn
    {
        class Curves;

        class AbstractCurvesSchema:
            public AbstractObjectSchema
        {
        public:
            inline const AbstractCurvesSchema*  asCurvesSchema() const  { return this; }
            inline AbstractCurvesSchema*        asCurvesSchema()        { return this; }

            virtual
            size_t
            numVertexBufferSamples() const = 0;

            virtual
            size_t
            numIndexBufferSamples() const = 0;

            virtual
            int
            currentVertexSampleIdx() const = 0;

            virtual
            int
            currentIndexSampleIdx() const = 0;

            virtual
            bool
            has(Property) const = 0;

            virtual
            void
            getBasisAndType(CurveOrder&, bool& periodic, BasisType&) const = 0;

            ////////////////
            // vertex buffer
            ////////////////
            virtual
            const float*
            lockPositions(size_t&) const = 0;

            virtual
            void
            unlockPositions() const = 0;

            virtual
            const float*
            lockWidths(size_t&) const = 0;

            virtual
            void
            unlockWidths() const = 0;

            virtual
            void
            getSelfBounds(xchg::BoundingBox&) const = 0;

            ///////////
            // topology
            ///////////
            virtual
            const int*
            lockNumVertices(size_t&) const = 0;

            virtual
            void
            unlockNumVertices() const = 0;

            virtual
            const float*
            lockKnots(size_t&) const = 0;

            virtual
            void
            unlockKnots() const = 0;

            ////////////////////
            // scalp information
            ////////////////////
            virtual
            const float*
            lockScalpNormals(size_t&) const = 0;

            virtual
            void
            unlockScalpNormals() const = 0;

            virtual
            const float*
            lockScalpTexCoords(size_t&) const = 0;

            virtual
            void
            unlockScalpTexCoords() const = 0;
        };
    }
}
