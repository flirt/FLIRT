// ========================================================================= //
// Copyright 2016-2018 SUPAMONKS_STUDIO                                      //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
//                                                                           //
// ========================================================================= //
#pragma once

#include <tbb/parallel_for.h>

namespace smks { namespace scn
{
    class TBBClearBufferElementKernel
    {
    private:
        float *const    _buffer;
        const size_t    _stride;
        const size_t    _numElements;

    public:
        TBBClearBufferElementKernel(float* buffer, size_t stride, size_t numElements);
        TBBClearBufferElementKernel(const TBBClearBufferElementKernel&);

        void
        operator()(tbb::blocked_range<int> const&) const;

    private:
        void
        checkKernelCorrectness() const;
    };
}
}
