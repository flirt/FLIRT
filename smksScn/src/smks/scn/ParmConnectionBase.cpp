// ========================================================================= //
// Copyright 2016-2018 SUPAMONKS_STUDIO                                      //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
//                                                                           //
// ========================================================================= //
#include <smks/parm/Container.hpp>

#include <smks/util/string/getId.hpp>
#include <smks/xchg/SceneEvent.hpp>

#include "smks/scn/Scene.hpp"
#include "smks/scn/ParmConnectionBase.hpp"
#include "smks/scn/ParmConnections.hpp"
#include "smks/scn/AbstractSceneObserver.hpp"

namespace smks { namespace scn
{
    /////////////////////
    // class ParmObserver
    /////////////////////
    class ParmConnectionBase::ParmObserver:
        public parm::AbstractObserver
    {
    public:
        typedef std::shared_ptr<ParmObserver> Ptr;
    private:
        PImpl& _pImpl;

    public:
        static inline
        Ptr
        create(PImpl& pimpl)
        {
            Ptr ptr(new ParmObserver(pimpl));
            return ptr;
        }

    protected:
        explicit inline
        ParmObserver(PImpl& pimpl):
            parm::AbstractObserver  (),
            _pImpl                  (pimpl)
        { }

        virtual
        void
        handle(unsigned int, parm::Event);
    };

    //////////////////////
    // class SceneObserver
    //////////////////////
    class ParmConnectionBase::SceneObserver:
        public AbstractSceneObserver
    {
    public:
        typedef std::shared_ptr<SceneObserver> Ptr;
    private:
        PImpl& _pImpl;

    public:
        static inline
        Ptr
        create(PImpl& pimpl)
        {
            Ptr ptr(new SceneObserver(pimpl));
            return ptr;
        }

    protected:
        explicit inline
        SceneObserver(PImpl& pimpl):
            _pImpl(pimpl)
        { }

        virtual
        void
        handle(Scene&, const xchg::SceneEvent&);
    };

    ///////////////////////////
    // class ParmConnectionBase::PImpl
    ///////////////////////////
    class ParmConnectionBase::PImpl
    {
    private:
        const Scene::WPtr           _scene;
        const unsigned int          _inNodeId;
        const unsigned int          _outNodeId;
        const std::string           _inParmName;
        const std::string           _outParmName;
        const unsigned int          _id;
        const unsigned int          _inParmId;
        const unsigned int          _outParmId;

        ParmConnectionBase::WPtr    _self;
        int                         _priority;
        TreeNode::WPtr              _inNode;
        TreeNode::WPtr              _outNode;

        SceneObserver::Ptr          _sceneObserver;
        ParmObserver::Ptr           _inParmsObserver;

    public:
        inline
        PImpl(unsigned int          inNodeId,
              const char*           inParmName,
              unsigned int          outNodeId,
              const char*           outParmName,
              int                   priority,
              Scene::WPtr const&    scene);

        inline
        ~PImpl();

        inline
        std::string
        to_string() const;

        inline
        void
        build(ParmConnectionBase::Ptr const&);

        inline
        void
        erase();

        inline
        unsigned int
        id() const
        {
            return _id;
        }

        inline
        unsigned int
        inNodeId() const
        {
            return _inNodeId;
        }

        inline
        const char*
        inParmName() const
        {
            return _inParmName.c_str();
        }

        inline
        unsigned int
        inParmId() const
        {
            return _inParmId;
        }

        inline
        unsigned int
        outNodeId() const
        {
            return _outNodeId;
        }

        inline
        const char*
        outParmName() const
        {
            return _outParmName.c_str();
        }

        inline
        unsigned int
        outParmId() const
        {
            return _outParmId;
        }

        inline
        int
        priority() const
        {
            return _priority;
        }

        inline
        void
        priority(int);

        inline
        TreeNode::WPtr const&
        inNode() const
        {
            return _inNode;
        }

        inline
        TreeNode::WPtr const&
        outNode() const
        {
            return _outNode;
        }

        inline
        bool
        established() const
        {
            return !_inNode.expired() && !_outNode.expired();
        }

        inline
        void
        connectInput();

        inline
        void
        disconnectInput();

        inline
        void
        connectOutput();

        inline
        void
        disconnectOutput();

        inline
        void
        handleSceneEvent(Scene&, const xchg::SceneEvent&);

        inline
        void
        handleInParmEvent(unsigned int parmId, parm::Event);

        inline
        Scene::WPtr const&
        scene() const
        {
            return _scene;
        }

    private:
        inline
        TreeNode::Ptr
        getNode(unsigned int nodeId) const
        {
            Scene::Ptr const& scene = _scene.lock();

            return scene && scene->hasDescendant(nodeId)
                ? scene->descendantById(nodeId).lock()
                : nullptr;
        }
    };
}
}

using namespace smks;

// virtual
void
scn::ParmConnectionBase::ParmObserver::handle(unsigned int parmId, parm::Event typ)
{
    _pImpl.handleInParmEvent(parmId, typ);
}

// virtual
void
scn::ParmConnectionBase::SceneObserver::handle(Scene& scene, const xchg::SceneEvent& evt)
{
    _pImpl.handleSceneEvent(scene, evt);
}

///////////////////////////
// class ParmConnectionBase::PImpl
///////////////////////////
scn::ParmConnectionBase::PImpl::PImpl(unsigned int          inNodeId,
                                      const char*           inParmName,
                                      unsigned int          outNodeId,
                                      const char*           outParmName,
                                      int                   priority,
                                      Scene::WPtr const&    sc):
    _scene          (sc),
    _inNodeId       (inNodeId),
    _outNodeId      (outNodeId),
    _inParmName     (inParmName),
    _outParmName    (outParmName),
    _id             (util::string::getId(to_string().c_str())),
    _inParmId       (util::string::getId(_inParmName.c_str())),
    _outParmId      (util::string::getId(_outParmName.c_str())),
    _priority       (priority),
    _inNode         (),
    _outNode        (),
    _sceneObserver  (SceneObserver::create(*this)),
    _inParmsObserver(ParmObserver::create(*this))
{
    if (_inNodeId == _outNodeId &&
        _inParmId == _outParmId)
    {
        std::stringstream sstr;
        sstr << "Self parameter connection '" + to_string() + "' forbidden.";
        throw sys::Exception(sstr.str().c_str(), "Parameter Connection Creation");
    }

    Scene::Ptr const& scene = _scene.lock();
    if (scene)
        scene->registerObserver(_sceneObserver);
}

inline
scn::ParmConnectionBase::PImpl::~PImpl()
{
    erase();
}

void
scn::ParmConnectionBase::PImpl::build(ParmConnectionBase::Ptr const& self)
{
    _self = self;

    connectInput();
    connectOutput();

    if (established())
    {
        Scene::Ptr const& scene = _scene.lock();
        if (scene)
            scene->deliver(xchg::SceneEvent::parmConnected(_id, _inNodeId, _inParmId, _outNodeId, _outParmId));
    }
}

void
scn::ParmConnectionBase::PImpl::erase()
{
    const bool wasEstablished = established();

    disconnectInput();
    disconnectOutput();

    if (wasEstablished)
    {
        Scene::Ptr const& scene = _scene.lock();
        if (scene)
            scene->deliver(xchg::SceneEvent::parmDisconnected(_id, _inNodeId, _inParmId, _outNodeId, _outParmId));
    }

    Scene::Ptr const& scene = _scene.lock();
    if (scene)
        scene->deregisterObserver(_sceneObserver);
}

std::string
scn::ParmConnectionBase::PImpl::to_string() const
{
    return "[" + std::to_string(_inNodeId) + "]." + _inParmName + ">>[" + std::to_string(_outNodeId) + "]." + _outParmName;
}

void
scn::ParmConnectionBase::PImpl::priority(int value)
{
    const bool changed = _priority != value;

    _priority = value;

    // change in priority
    if (changed)
    {
        {
            TreeNode::Ptr const&    node        = getNode(_inNodeId);
            ParmConnections*        connections = node ? node->getOutputParmConnections(_inParmId) : nullptr;
            if (connections)
            {
                connections->dirty();
                connections->evaluate();
            }
        }
        {
            TreeNode::Ptr const&    node        = getNode(_outNodeId);
            ParmConnections*        connections = node ? node->getInputParmConnections(_outParmId) : nullptr;
            if (connections)
            {
                connections->dirty();
                connections->evaluate();
            }
        }
    }
}

void
scn::ParmConnectionBase::PImpl::connectInput()
{
    _inNode.reset();

    TreeNode::Ptr const& node = getNode(_inNodeId);

    if (!node)
        return;
    _inNode = node;
    // now tracks changes in input node's parameter set
    node->registerParameterObserver(_inParmsObserver);
    // add connection to input node's output connections
    ParmConnections* connections = node->getOutputParmConnections(_inParmId);
    if (!connections)
    {
        node->declareOutputParmConnections(_inParmName.c_str());
        connections = node->getOutputParmConnections(_inParmId);
    }
    else if (connections->name() != _inParmName)
    {
        std::stringstream sstr;
        sstr
            << "Output parameter name collision "
            << "between '" << _inParmName << "' "
            << "and '" << connections->name() << "'.";
        throw sys::Exception(sstr.str().c_str(), "Input Node Connection");
    }
    connections->add(_self.lock());
}

void
scn::ParmConnectionBase::PImpl::disconnectInput()
{
    TreeNode::Ptr const& node = _inNode.lock();
    if (node)
    {
        node->deregisterParameterObserver(_inParmsObserver);

        ParmConnections* connections = node ? node->getOutputParmConnections(_inParmId) : nullptr;
        if (connections)
        {
            connections->remove(_self.lock());
            connections->evaluate();
            if (connections->empty())
                node->undeclareOutputParmConnections(_inParmId);
        }
    }
    _inNode.reset();
}

void
scn::ParmConnectionBase::PImpl::connectOutput()
{
    _outNode.reset();

    TreeNode::Ptr const& node = getNode(_outNodeId);

    if (!node)
        return;
    _outNode = node;
    // add connection to output node's input connections
    ParmConnections* connections = node->getInputParmConnections(_outParmId);
    if (!connections)
    {
        node->declareInputParmConnections(_outParmName.c_str());
        connections = node->getInputParmConnections(_outParmId);
    }
    else if (connections->name() != _outParmName)
    {
        std::stringstream sstr;
        sstr
            << "Input parameter name collision "
            << "between '" << _outParmName << "' "
            << "and '" << connections->name() << "'.";
        throw sys::Exception(sstr.str().c_str(), "Output Node Connection");
    }
    connections->add(_self.lock());
    // evaluate output node's input parameter connections
    connections->evaluate();
}

void
scn::ParmConnectionBase::PImpl::disconnectOutput()
{
    TreeNode::Ptr const& node = _outNode.lock();
    if (node)
    {
        ParmConnections* connections = node ? node->getInputParmConnections(_outParmId) : nullptr;
        if (connections)
        {
            connections->remove(_self.lock());
            connections->evaluate();
            if (connections->empty())
                node->undeclareInputParmConnections(_outParmId);
        }
    }
    _outNode.reset();
}

void
scn::ParmConnectionBase::PImpl::handleSceneEvent(Scene&,
                                                 const xchg::SceneEvent& evt)
{
    const bool wasEstablished = established();

    if (evt.type() == xchg::SceneEvent::NODE_CREATED)
    {
        if (evt.node() == _inNodeId)
            connectInput();
        else if (evt.node() == _outNodeId)
            connectOutput();
    }
    else if (evt.type() == xchg::SceneEvent::NODE_DELETED)
    {
        if (evt.node() == _inNodeId)
            disconnectInput();
        else if (evt.node() == _outNodeId)
            disconnectOutput();
    }

    if (established() != wasEstablished)
    {
        Scene::Ptr const& scene = _scene.lock();
        if (scene)
            scene->deliver(wasEstablished
                ? xchg::SceneEvent::parmDisconnected(_id, _inNodeId, _inParmId, _outNodeId, _outParmId)
                : xchg::SceneEvent::parmConnected(_id, _inNodeId, _inParmId, _outNodeId, _outParmId));
    }
}

void
scn::ParmConnectionBase::PImpl::handleInParmEvent(unsigned int parmId, parm::Event typ)
{
    if (parmId == _inParmId)
    {
        TreeNode::Ptr const&    outNode     = getNode(_outNodeId);
        ParmConnections*        connections = outNode ? outNode->getInputParmConnections(_outParmId) : nullptr;
        if (connections)
            connections->evaluate();
    }
    else if (parmId == _outParmId)
    {
        TreeNode::Ptr const&    inNode      = getNode(_inNodeId);
        ParmConnections*        connections = inNode ? inNode->getOutputParmConnections(_inParmId) : nullptr;
        if (connections)
            connections->evaluate();
    }
}

////////////////////
// class ParmConnectionBase
////////////////////
scn::ParmConnectionBase::ParmConnectionBase(unsigned int       inNodeId,
                                            const char*        inParmName,
                                            unsigned int       outNodeId,
                                            const char*        outParmName,
                                            int                priority,
                                            Scene::WPtr const& scene):
    _pImpl(new PImpl(inNodeId, inParmName, outNodeId, outParmName, priority, scene))
{
}

scn::ParmConnectionBase::~ParmConnectionBase()
{
    delete _pImpl;
}

scn::Scene::WPtr const&
scn::ParmConnectionBase::scene() const
{
    return _pImpl->scene();
}

void
scn::ParmConnectionBase::build(Ptr const& ptr)
{
    _pImpl->build(ptr);
}

void
scn::ParmConnectionBase::erase()
{
    _pImpl->erase();
}

unsigned int
scn::ParmConnectionBase::id() const
{
    return _pImpl->id();
}

unsigned int
scn::ParmConnectionBase::inNodeId() const
{
    return _pImpl->inNodeId();
}

const char*
scn::ParmConnectionBase::inParmName() const
{
    return _pImpl->inParmName();
}

unsigned int
scn::ParmConnectionBase::outNodeId() const
{
    return _pImpl->outNodeId();
}

const char*
scn::ParmConnectionBase::outParmName() const
{
    return _pImpl->outParmName();
}

int
scn::ParmConnectionBase::priority() const
{
    return _pImpl->priority();
}

void
scn::ParmConnectionBase::priority(int value)
{
    _pImpl->priority(value);
}

bool
scn::ParmConnectionBase::established() const
{
    return _pImpl->established();
}

void
scn::ParmConnectionBase::connectInput()
{
    _pImpl->connectInput();
}

void
scn::ParmConnectionBase::disconnectInput()
{
    _pImpl->disconnectInput();
}

void
scn::ParmConnectionBase::connectOutput()
{
    _pImpl->connectOutput();
}

void
scn::ParmConnectionBase::disconnectOutput()
{
    _pImpl->disconnectOutput();
}

unsigned int
scn::ParmConnectionBase::inParmId() const
{
    return _pImpl->inParmId();
}

unsigned int
scn::ParmConnectionBase::outParmId() const
{
    return _pImpl->outParmId();
}

scn::TreeNode::WPtr const&
scn::ParmConnectionBase::inNode() const
{
    return _pImpl->inNode();
}

scn::TreeNode::WPtr const&
scn::ParmConnectionBase::outNode() const
{
    return _pImpl->outNode();
}
