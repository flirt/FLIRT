// ========================================================================= //
// Copyright 2016-2018 SUPAMONKS_STUDIO                                      //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
//                                                                           //
// ========================================================================= //

#include "macros_opensubdiv.hpp"

#include <opensubdiv/far/topologyDescriptor.h>
#include <opensubdiv/far/topologyRefinerFactory.h>
#include <opensubdiv/far/patchTableFactory.h>
#include <opensubdiv/osd/cpuPatchTable.h>
#include <opensubdiv/osd/cpuVertexBuffer.h>
#include <opensubdiv/osd/tbbEvaluator.h>

#include <smks/sys/Log.hpp>
#include <smks/sys/Exception.hpp>

#include <smks/parm/Container.hpp>
#include <smks/parm/BuiltIns.hpp>

#include "smks/scn/PolyMesh.hpp"
#include "AbstractPolyMeshSchema.hpp"
#include "PolyMesh-FacetedSmoothGeometry.hpp"
#include "PolyMesh-TBBClearBufferElementKernel.hpp"
#include "PolyMesh-TBBComputeVertexNormalKernel.hpp"
#include "PolyMesh-TBBNormalizeVec3Kernel.hpp"

using namespace Alembic;
using namespace OpenSubdiv;
using namespace smks;

static const short QUAD_TO_TRIS[] = { 0, 1, 2, 3, 0, 2 }; // quad -> two triangles

// explicit
util::data::UpdateRefinerLevel::UpdateRefinerLevel(int lvl):
    UpdateEntry<OpenSubdiv::Far::TopologyRefiner*>  (),
    _level                                          (lvl)
{
}

// virtual
void
util::data::UpdateRefinerLevel::operator()(OpenSubdiv::Far::TopologyRefiner*& x) const
{
    if (x)
    {
        x->Unrefine();
        x->RefineUniform(Far::TopologyRefiner::UniformOptions(_level));
    }
}

// virtual
void
util::data::DirtyEntry<scn::OsdPatches>::operator()(scn::OsdPatches& x) const
{
    x.indices.clear();
    x.indices.shrink_to_fit();

    x.vtxRedir.clear();
    x.vtxRedir.shrink_to_fit();
}

// virtual
bool
util::data::IsCacheEntryDirty<scn::OsdPatches>::operator()(const scn::OsdPatches& x) const
{
    return x.indices.empty() ||
        x.vtxRedir.empty();
}

// static
scn::PolyMesh::FacetedSmoothGeometry::Ptr
scn::PolyMesh::FacetedSmoothGeometry::create(bool triangulate)
{
    Ptr ptr (new FacetedSmoothGeometry(triangulate));

    return ptr;
}

// explicit
scn::PolyMesh::FacetedSmoothGeometry::FacetedSmoothGeometry(bool triangulate):
    PolyMesh::AbstractGeometry  (),
    _schema                     (nullptr),
    _faceCount                  (triangulate ? 3 : 4),
    _level                      (1),
    _type                       (Sdc::SchemeType::SCHEME_CATMARK),
    _faceVertexAttribs          (),
    _fvarChannelUV              (0),
    _numFVarChannels            (0),
    _refiner                    (),
    _primvarRefiner             (),
    _patches                    (),
    _vertexStencil              (),
    _varyingStencil             (),
    _wireIndices                (),
    _faceVertices               ()
{ }

scn::PolyMesh::FacetedSmoothGeometry::~FacetedSmoothGeometry()
{
    dispose();
}

void
scn::PolyMesh::FacetedSmoothGeometry::dispose()
{
    _schema             = nullptr;
    _faceVertexAttribs  .clear();
    _fvarChannelUV      = 0;
    _numFVarChannels    = 0;

    dirtyRefiners();

    _refiner        .dispose();
    _primvarRefiner .dispose();
    _patches        .dispose();
    _vertexStencil  .dispose();
    _varyingStencil .dispose();
    _wireIndices    .dispose();
    _faceVertices   .dispose();
}

void
scn::PolyMesh::FacetedSmoothGeometry::dirtyRefiners()
{
    dirtyPrimvarRefiners();
    dirtyPatches();
    dirtyStencilTables();
    dirtyFaceVertexBuffers();

    _refiner.dirtyAll();
}

void
scn::PolyMesh::FacetedSmoothGeometry::dirtyPrimvarRefiners()
{
    _primvarRefiner.dirtyAll();
}

void
scn::PolyMesh::FacetedSmoothGeometry::dirtyPatches()
{
    _patches    .dirtyAll();
    _wireIndices.dirtyAll();
}

void
scn::PolyMesh::FacetedSmoothGeometry::dirtyStencilTables()
{
    _vertexStencil  .dirtyAll();
    _varyingStencil .dirtyAll();
}

void
scn::PolyMesh::FacetedSmoothGeometry::dirtyFaceVertexBuffers()
{
    _faceVertices.dirtyAll();
}

/*******************************
 * initialization
 *******************************/

// virtual
bool
scn::PolyMesh::FacetedSmoothGeometry::initialize(const AbstractPolyMeshSchema* schema, bool caching)
{
    assert(schema);
    dispose();
    _schema = schema;
    if (initialized())
    {
        if (!schema->has(Property::PROP_POSITION))
            throw sys::Exception("PolyMesh schema must contain position information.", "OSD Geometry Initialization");

        _faceVertexAttribs.append(Property::PROP_POSITION,  3);
        _faceVertexAttribs.append(Property::PROP_NORMAL,    3); // will manually compute them
        if (schema->has(Property::PROP_TEXCOORD))
        {
            _faceVertexAttribs.append(Property::PROP_TEXCOORD,  2);

            _fvarChannelUV = 0;
            ++_numFVarChannels;
        }

        assert(_faceVertexAttribs.offset(Property::PROP_POSITION) == 0);

        _refiner        .initialize (caching ? _schema->numIndexBufferSamples() : 0);
        _primvarRefiner .initialize (caching ? _schema->numIndexBufferSamples() : 0);
        _patches        .initialize (caching ? _schema->numIndexBufferSamples() : 0);
        _vertexStencil  .initialize (caching ? _schema->numIndexBufferSamples() : 0);
        _varyingStencil .initialize (caching ? _schema->numIndexBufferSamples() : 0);
        _wireIndices    .initialize (caching ? _schema->numIndexBufferSamples() : 0);

        _faceVertices   .initialize (caching ? _schema->numVertexBufferSamples() : 0);

        return true;
    }
    else
        return false;
}

bool
scn::PolyMesh::FacetedSmoothGeometry::initialized() const
{
    return _schema && _schema->numSamples() > 0;
}

// virtual
void
scn::PolyMesh::FacetedSmoothGeometry::setSubdivisionLevel(unsigned int value)
{
    const unsigned int lvl = std::max(static_cast<unsigned int>(1), std::min(static_cast<unsigned int>(MAX_LEVEL), value));

    if (lvl == _level)
        return;

    // effective change of subdivision level
    dirtyPrimvarRefiners();
    dirtyPatches();
    dirtyStencilTables();
    dirtyFaceVertexBuffers();

    util::data::UpdateRefinerLevel* update = new util::data::UpdateRefinerLevel(static_cast<int>(_level));

    _refiner.updateAll(update);
    _level = lvl;
    delete update;
}

bool
scn::PolyMesh::FacetedSmoothGeometry::type(Sdc::SchemeType value)
{
    if (value == _type)
        return false;

    dirtyRefiners();
    _type = value;

    return true;
}

/*******************************
 * face count buffer
 *******************************/
// virtual
const unsigned int*
scn::PolyMesh::FacetedSmoothGeometry::getFaceCounts(size_t& numFaces,
                                                    bool&   isConstant)
{
    numFaces    = 0;
    isConstant  = true;

    assert(initialized());

    //updateLevel();    // must come before any cache access: will dirty obsolete data.

    const OsdPatches* patches   = getPatches();

    if (patches == nullptr)
        return nullptr;

    numFaces = patches->indices.size() / _faceCount;

    return &_faceCount;
}

/*******************************
 * face index buffer
 *******************************/
// virtual
const unsigned int*
scn::PolyMesh::FacetedSmoothGeometry::getFaceIndices(size_t& numIndices)
{
    numIndices  = 0;

    assert(initialized());

    //updateLevel();    // must come before any cache access: will dirty obsolete data.

    const int           indexSampleId   = _schema->currentIndexSampleIdx();
    const OsdPatches*   patches         = getPatches();

    if (patches == nullptr)
        return nullptr;

    numIndices = patches->indices.size();

    return numIndices > 0
        ? &patches->indices.front()
        : nullptr;
}

/*******************************
 * topology refiner
 *******************************/
Far::TopologyRefiner *const
scn::PolyMesh::FacetedSmoothGeometry::getRefiner()
{
    if (!initialized())
        return nullptr;

    const int                           indexSampleId   = _schema->currentIndexSampleIdx();
    Far::TopologyRefiner *const *const  cache           = _refiner.get(indexSampleId);
    if (cache)
        return *cache;

    // actual computation
    // - prepare topology description
    Far::TopologyDescriptor descr;
    size_t                  numFaces    = 0;
    size_t                  numIndices  = 0;

    descr.numVertices           = static_cast<int>(_schema->getNumVertices());
    descr.numVertsPerFace       = _schema->lockFaceCounts(numFaces);
    descr.numFaces              = static_cast<int>(numFaces);
    descr.vertIndicesPerFace    = _schema->lockFaceIndices(numIndices);

    if (descr.numVertices           == 0 ||
        descr.numVertsPerFace       == nullptr ||
        descr.numFaces              == 0 ||
        descr.vertIndicesPerFace    == nullptr)
    {
        _schema->unlockFaceCounts();
        _schema->unlockFaceIndices();
        throw sys::Exception("Polymesh does not define any vertex data.", "OSD Refiner Initialization");
    }

    Far::TopologyDescriptor::FVarChannel*   fvarChannels = _numFVarChannels > 0
        ? new Far::TopologyDescriptor::FVarChannel[_numFVarChannels]
        : nullptr;

    if (_faceVertexAttribs.has(Property::PROP_TEXCOORD))
    {
        assert(fvarChannels);

        size_t              numTexIndices   = 0;
        const unsigned int* texIndices      = _schema->lockTexCoordsIndices(numTexIndices);

        fvarChannels[_fvarChannelUV].numValues      = static_cast<int>(numTexIndices);
        fvarChannels[_fvarChannelUV].valueIndices   = reinterpret_cast<const int*>(texIndices);
    }
    descr.numFVarChannels   = static_cast<int>(_numFVarChannels);
    descr.fvarChannels      = fvarChannels;

    // - topology refiner creation
    Sdc::Options options;

    options.SetVtxBoundaryInterpolation (Sdc::Options::VtxBoundaryInterpolation::VTX_BOUNDARY_EDGE_ONLY);
    options.SetFVarLinearInterpolation  (Sdc::Options::FVarLinearInterpolation::FVAR_LINEAR_NONE);

    Far::TopologyRefiner* refiner = Far::TopologyRefinerFactory<Far::TopologyDescriptor>::Create(
            descr,
            Far::TopologyRefinerFactory<Far::TopologyDescriptor>::Options(_type, options));

    if (refiner == nullptr)
        throw sys::Exception("Failed to create OSD topology refiner.", "OSD Refiner Creation");

    refiner->Unrefine();
    refiner->RefineUniform(Far::TopologyRefiner::UniformOptions(static_cast<int>(_level)));

    _schema->unlockFaceCounts();
    _schema->unlockFaceIndices();
    _schema->unlockTexCoordsIndices();

    if (fvarChannels)
        delete[] fvarChannels;

    // cache update
    return *_refiner.set(indexSampleId, refiner);
}

OpenSubdiv::Far::PrimvarRefiner *const
scn::PolyMesh::FacetedSmoothGeometry::getPrimvarRefiner()
{
    if (!initialized())
        return nullptr;

    const int                           indexSampleId   = _schema->currentIndexSampleIdx();
    Far::PrimvarRefiner *const *const   cache           = _primvarRefiner.get(indexSampleId);
    if (cache)
        return *cache;

    // actual computation
    const Far::TopologyRefiner* refiner = getRefiner();
    assert(refiner);

    // cache update
    return *_primvarRefiner.set(indexSampleId, new Far::PrimvarRefiner(*refiner));
}

/*******************************
 * patch tables / face count
 *******************************/
const scn::OsdPatches*
scn::PolyMesh::FacetedSmoothGeometry::getPatches()
{
    if (!initialized())
        return nullptr;

    const int               indexSampleId   = _schema->currentIndexSampleIdx();
    const scn::OsdPatches*  cache           = _patches.get(indexSampleId);
    if (cache)
        return cache;

    // actual computation
    OsdPatches                      patches;
    const Far::TopologyRefiner*     refiner = getRefiner();
    Far::PatchTableFactory::Options options;

    assert(refiner);
    options.triangulateQuads    = false;    //<! will be done manually ?
    options.generateAllLevels   = false;

    Far::PatchTable*    farPatches  = Far::PatchTableFactory::Create(*refiner, options);
    Osd::CpuPatchTable* cpuPatches  = Osd::CpuPatchTable::Create(farPatches, nullptr);

    if (cpuPatches->GetNumPatchArrays() != 1)
        throw sys::Exception(
            "Subdivided mesh composed of several patch types. Not currently supported.",
            "OSD Patch Table Initialization");

    const int*      patchIndices    = cpuPatches->GetPatchIndexBuffer();
    const size_t    patchFaceCount  = Far::PatchDescriptor::GetNumControlVertices(cpuPatches->GetPatchArrayBuffer()[0].desc.GetType());

    // fill dummy index buffer
    const bool  triangulate     = (_faceCount != patchFaceCount);
    size_t      numFaceVertices = 0;

    if (!triangulate)
    {
        numFaceVertices = cpuPatches->GetPatchIndexSize();
        patches.indices .resize(numFaceVertices);
        patches.vtxRedir.resize(numFaceVertices);
        for (size_t i = 0; i < numFaceVertices; ++i)
        {
            patches.indices[i]  = static_cast<unsigned int>(i);
            patches.vtxRedir[i] = patchIndices[i]; // warning: accounts for coarse vertices too
        }
    }
    else
    {
        assert(_faceCount == 3 && patchFaceCount == 4); // must triangulate quads into pairs of triangles
        const size_t numFaces   = refiner->GetLevel(static_cast<int>(_level)).GetNumFaces();
        numFaceVertices         = numFaces * 6;
        patches.indices .resize(numFaceVertices);
        patches.vtxRedir.resize(numFaceVertices);
        for (size_t f = 0, i = 0; f < numFaces; ++f)
        {
            for (size_t j = 0; j < 6; ++j, ++i)
            {
                patches.indices[i]  = static_cast<unsigned int>(i);
                patches.vtxRedir[i] = patchIndices[QUAD_TO_TRIS[j]]; // warning: accounts for coarse vertices too
            }
            patchIndices += patchFaceCount;
        }
    }
    delete farPatches;
    delete cpuPatches;

    // cache update
    return _patches.set(indexSampleId, patches);
}

/*******************************
 * stencil table
 *******************************/
const Far::StencilTable *const
scn::PolyMesh::FacetedSmoothGeometry::getStencilTable(Far::StencilTableFactory::Mode interp)
{
    if (!initialized())
        return nullptr;

    const int                               indexSampleId   = _schema->currentIndexSampleIdx();
    const Far::StencilTable *const *const   cache           = interp == Far::StencilTableFactory::INTERPOLATE_VERTEX
        ? _vertexStencil.get(indexSampleId)
        : _varyingStencil.get(indexSampleId);
    if (cache)
        return *cache;

    // create stencil table
    const Far::TopologyRefiner*         refiner = getRefiner();
    Far::StencilTableFactory::Options   options;

    assert(refiner);
    options.generateOffsets             = true;
    options.generateIntermediateLevels  = false;
    options.interpolationMode           = interp;

    const Far::StencilTable * stencils  = Far::StencilTableFactory::Create(*refiner, options);

    // cache update
    return *( interp == Far::StencilTableFactory::INTERPOLATE_VERTEX
        ? _vertexStencil    .set(indexSampleId, stencils)
        : _varyingStencil   .set(indexSampleId, stencils) );
}

/*******************************
 * vertex buffer cache
 *******************************/
// virtual
const float*
scn::PolyMesh::FacetedSmoothGeometry::getFaceVertices(size_t&           numFaceVertices,
                                                      BufferAttributes& attribs)
{
    numFaceVertices = 0;
    attribs.clear();

    if (!initialized())
        return nullptr;

    attribs = _faceVertexAttribs;

    //updateLevel();    // must come before any cache access: will dirty obsolete data.

    const int                   vertexSampleId  = _schema->currentVertexSampleIdx();
    const std::vector<float>*   cache           = _faceVertices.get(vertexSampleId);
    if (cache)
    {
        assert(_faceVertexAttribs.stride() > 0);
        numFaceVertices = cache->size() / _faceVertexAttribs.stride();

        return numFaceVertices > 0
            ? &cache->front()
            : nullptr;
    }

    // actual computation
    const Far::TopologyRefiner* refiner = getRefiner();
    const OsdPatches*           patches = getPatches();

    assert(refiner && refiner->IsUniform());
    assert(patches);
    assert(patches->indices.size() % _faceCount == 0);
    assert(patches && patches->indices.size() == patches->vtxRedir.size());

    numFaceVertices = patches->indices.size();

    std::vector<float> faceVertices (numFaceVertices * _faceVertexAttribs.stride(), 0.0f);

    assert(_faceVertexAttribs.has(Property::PROP_POSITION));
    assert(_faceVertexAttribs.has(Property::PROP_NORMAL));
    fillPositionsAndNormals(faceVertices);

    if (_faceVertexAttribs.has(Property::PROP_TEXCOORD))
        fillTexCoords(faceVertices);

    // cache computed data
    _faceVertices.set(vertexSampleId, faceVertices);
    cache = _faceVertices.get(vertexSampleId);
    assert(cache);

    return !cache->empty()
        ? &cache->front()
        : nullptr;
}

void
scn::PolyMesh::FacetedSmoothGeometry::fillPositionsAndNormals(std::vector<float>& faceVertices)
{
    if (faceVertices.empty())
        return;

    assert(_faceVertexAttribs.has(Property::PROP_POSITION));

    const Far::TopologyRefiner* refiner         = getRefiner();
    const Far::StencilTable*    vertexStencils  = getStencilTable(Far::StencilTableFactory::INTERPOLATE_VERTEX);

    assert(refiner && refiner->IsUniform());
    assert(vertexStencils);

    // access coarse positions
    size_t          numCoarseVertices   = 0;
    const float*    coarsePositions     = _schema->lockPositions(numCoarseVertices);

    assert(static_cast<int>(numCoarseVertices) == refiner->GetLevel(0).GetNumVertices());

    const size_t    numVertices     = refiner->GetLevel(static_cast<int>(_level)).GetNumVertices();
    const int       numBufVertices  = static_cast<int>(numCoarseVertices + numVertices);
    const int       bufferStride    = 3 + 3; // position + normal

    Osd::CpuVertexBuffer*   buffer          = Osd::CpuVertexBuffer::Create(bufferStride, numBufVertices);
    float *const            posBuffer       = buffer->BindCpuBuffer();
    float *const            normalBuffer    = buffer->BindCpuBuffer() + 3;

    {
        // POSITIONS
        ////////////
        // fill coarse position data
        float* currentPos = posBuffer;
        for (size_t i = 0; i < numCoarseVertices; ++i)
        {
            memcpy(currentPos, coarsePositions, sizeof(float) * 3);
            coarsePositions += 3;
            currentPos      += bufferStride;
        }
        _schema->unlockPositions();

        // evaluate stencils in order to interpolate new positions
        const Osd::BufferDescriptor srcDescr(0, 3, bufferStride);   // offset, length, stride
        const Osd::BufferDescriptor dstDescr(static_cast<int>(numCoarseVertices * bufferStride), 3, bufferStride);

        Osd::TbbEvaluator::EvalStencils(
            buffer, srcDescr,
            buffer, dstDescr,
            vertexStencils);
    }

    {
        // NORMALS
        //////////
        // compute normals from interpolated positions
        const int grainSize = 100;
        {
            TBBClearBufferElementKernel kernel  (normalBuffer, bufferStride, 3); // normal buffer
            tbb::blocked_range<int>     range   (0, numBufVertices, grainSize);

            tbb::parallel_for(range, kernel);
        }
        {
            const OsdPatches*   patches = getPatches();

            assert(patches);
            assert(patches->vtxRedir.size() % _faceCount == 0);

            size_t      numFaces    = patches->vtxRedir.size() / _faceCount;
            const int*  indices     = (const int*)&patches->vtxRedir.front();

            if (numFaces > 0 &&
                indices)
            {
                TBBComputeVertexNormalKernel kernel(
                    numFaces, _faceCount, indices,
                    posBuffer,      bufferStride,   // position buffer
                    normalBuffer,   bufferStride);  // normal buffer
                tbb::blocked_range<int> range (0, static_cast<int>(numFaces), grainSize);

                tbb::parallel_for(range, kernel);
            }
        }
        {
            TBBNormalizeVec3Kernel  kernel  (normalBuffer, bufferStride);
            tbb::blocked_range<int> range   (0, numBufVertices, grainSize);

            tbb::parallel_for(range, kernel);
        }
    }

    // fill output vertex buffer
    assert(bufferStride <= _faceVertexAttribs.stride());
    mapVertexToFaceVertex(
        bufferStride,
        buffer->BindCpuBuffer(),
        bufferStride,
        &faceVertices.front() + _faceVertexAttribs.offset(Property::PROP_POSITION),
        _faceVertexAttribs.stride());

    delete buffer;
}

void
scn::PolyMesh::FacetedSmoothGeometry::mapVertexToFaceVertex(size_t          length,
                                                            const float*    iData,
                                                            size_t          iStride,
                                                            float*          oData,
                                                            size_t          oStride)
{
    if (iData == nullptr ||
        iStride == 0 ||
        oData == nullptr ||
        oStride == 0 ||
        length == 0)
        return;

    const Far::TopologyRefiner* refiner     = getRefiner();
    const OsdPatches*           patches     = getPatches();
    assert(patches);

    if (patches->indices.empty())
        return;

    assert(patches->indices.size() % _faceCount == 0);
    assert(patches->indices.size() == patches->vtxRedir.size());

    const size_t    numFaceVertices = patches->indices.size();
    float*          oPtr            = oData;

    for (size_t i = 0; i < numFaceVertices; ++i)
    {
        memcpy(oPtr, iData + patches->vtxRedir[i] * iStride, sizeof(float) * length);
        oPtr += oStride;
    }
}

void
scn::PolyMesh::FacetedSmoothGeometry::fillTexCoords(std::vector<float>& faceVertices)
{
    if (faceVertices.empty() ||
        !_faceVertexAttribs.has(Property::PROP_TEXCOORD))
        return;

    const Far::TopologyRefiner* refiner = getRefiner();

    assert(refiner && refiner->IsUniform());
    assert(static_cast<int>(_level) <= refiner->GetMaxLevel());
    assert(static_cast<int>(_fvarChannelUV) < refiner->GetNumFVarChannels());

    const int bufferSize = refiner->GetNumFVarValuesTotal(static_cast<int>(_fvarChannelUV));
    if (bufferSize == 0)
        return;

    size_t          numCoarseTexCoords  = 0;
    const float*    coarseTexCoords     = _schema->lockTexCoords(numCoarseTexCoords);
    if (numCoarseTexCoords == 0 ||
        coarseTexCoords == nullptr)
    {
        _schema->unlockTexCoords();
        return;
    }

    assert(numCoarseTexCoords < bufferSize);

    ////////////////
    // struct FVarUv
    ////////////////
    struct FVarUv
    {
        float u, v;

    public:
        inline
        void
        Clear()
        {
            u = 0.0f;
            v = 0.0f;
        }

        inline
        void
        AddWithWeight(FVarUv const& src, float wgt)
        {
            u += src.u * wgt;
            v += src.v * wgt;
        }
    };

    std::vector<FVarUv> buffer(bufferSize);

    // fill coarse texture coordinates (redirection already in refiners)
    for (size_t i = 0; i < numCoarseTexCoords; ++i)
    {
        buffer[i].u = coarseTexCoords[0];
        buffer[i].v = coarseTexCoords[1];
        coarseTexCoords += 2;
    }
    _schema->unlockTexCoords();

    // interpolate texture coordinates
    const Far::PrimvarRefiner*  primvarRefiner = getPrimvarRefiner();

    FVarUv* src = &buffer.front();
    for (size_t lvl = 1; lvl <= _level; ++lvl)
    {
        FVarUv* dst = src + refiner->GetLevel(static_cast<int>(lvl-1))
            .GetNumFVarValues(static_cast<int>(_fvarChannelUV));

        primvarRefiner->InterpolateFaceVarying(static_cast<int>(lvl), src, dst, static_cast<int>(_fvarChannelUV));
        src = dst;
    }

    // fill output vertex buffer
    const Far::TopologyLevel&   level           = refiner->GetLevel(static_cast<int>(_level));
    const int                   offset          = refiner->GetNumFVarValuesTotal(static_cast<int>(_fvarChannelUV)) - level.GetNumFVarValues(static_cast<int>(_fvarChannelUV));
    const size_t                numFaces        = level.GetNumFaces();

    float*  texCoords = &faceVertices.front() + _faceVertexAttribs.offset(Property::PROP_TEXCOORD);

    assert(_faceVertexAttribs.stride() > 0);
    assert(faceVertices.size() % _faceVertexAttribs.stride() == 0);

    size_t  numFaceVertices     = faceVertices.size() / _faceVertexAttribs.stride();
    size_t  vertexId            = 0;

    for (int f = 0; f < numFaces; ++f)
    {
        const Far::ConstIndexArray& fvars = level.GetFaceFVarValues(f, static_cast<int>(_fvarChannelUV));
        if (fvars.size() == _faceCount)
        {
            for (unsigned int c = 0; c < _faceCount; ++c)
            {
                const int       vId = offset + fvars[c];
                const FVarUv&   uv  = buffer[vId];

                texCoords[0]    = uv.u;
                texCoords[1]    = uv.v;
                texCoords       += _faceVertexAttribs.stride();

                ++vertexId;
            }
        }
        else
        {
            assert(_faceCount == 3 && fvars.size() == 4); // must triangulate quads into pairs of triangles

            for (size_t i = 0; i < 6; ++i)
            {
                const size_t    vId = offset + fvars[QUAD_TO_TRIS[i]];
                const FVarUv&   uv  = buffer[vId];

                texCoords[0]    = uv.u;
                texCoords[1]    = uv.v;
                texCoords       += _faceVertexAttribs.stride();

                ++vertexId;
            }
        }
    }
}

/*******************************
 * wireframe line indices
 *******************************/
// virtual
const unsigned int *
scn::PolyMesh::FacetedSmoothGeometry::getWireframeLines(size_t& numWireIndices)
{
    numWireIndices = 0;

    if (!initialized())
        return nullptr;

    const int                           indexSampleId   = _schema->currentIndexSampleIdx();
    const std::vector<unsigned int>*    cache           = _wireIndices.get(indexSampleId);
    if (cache)
    {
        assert((cache->size() % 2) == 0);
        numWireIndices = cache->size();
        return numWireIndices > 0
            ? &cache->front()
            : nullptr;
    }

    // actual computation
    std::vector<unsigned int> wireIndices;

    computeWireframeLines(*this, wireIndices);

    // cache computed data
    _wireIndices.set(indexSampleId, wireIndices);
    cache = _wireIndices.get(indexSampleId);
    assert(cache);

    numWireIndices = cache->size();
    return !cache->empty()
        ? &cache->front()
        : nullptr;
}
