// ========================================================================= //
// Copyright 2016-2018 SUPAMONKS_STUDIO                                      //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
//                                                                           //
// ========================================================================= //
#pragma once

#include <memory>
#include <iostream>
#include <smks/xchg/IdSet.hpp>

namespace smks { namespace scn
{
    template <class ObjectType>
    class Tracked:
        public ObjectType
    {
    public:
        typedef std::shared_ptr<Tracked<ObjectType>> Ptr;
    private:
        xchg::IdSet _visited;
    public:
        static inline
        Ptr
        create(const ObjectType& o)
        {
            Ptr ptr(new Tracked<ObjectType>(o));
            return ptr;
        }
    private:
        explicit inline
        Tracked(const ObjectType& o):
            ObjectType  (o),
            _visited    ()
        { }
    public:
        inline
        bool
        visited(unsigned int n) const
        {
            return _visited.find(n) != _visited.end();
        }
        inline
        void
        visits(unsigned int n)
        {
            _visited.insert(n);
        }
        friend inline
        std::ostream&
        operator<<(std::ostream& out, const Tracked<ObjectType>& t)
        {
            return out
                << static_cast<ObjectType>(t) << "\n"
                << "visited: " << t._visited;
        }
    };
}
}
