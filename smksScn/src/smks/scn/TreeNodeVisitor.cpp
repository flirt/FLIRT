// ========================================================================= //
// Copyright 2016-2018 SUPAMONKS_STUDIO                                      //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
//                                                                           //
// ========================================================================= //
#include "smks/scn/TreeNodeVisitor.hpp"

#include "smks/scn/TreeNode.hpp"
#include "smks/scn/Scene.hpp"
#include "smks/scn/Archive.hpp"
#include "smks/scn/Camera.hpp"
#include "smks/scn/PolyMesh.hpp"
#include "smks/scn/Curves.hpp"
#include "smks/scn/Points.hpp"
#include "smks/scn/Xform.hpp"

using namespace smks;

// explicit
scn::TreeNodeVisitor::TreeNodeVisitor(Direction direction):
    _direction(direction)
{ }

// virtual
scn::TreeNodeVisitor::~TreeNodeVisitor()
{ }

// virtual
void
scn::TreeNodeVisitor::apply(scn::TreeNode& node)
{
    traverse(node);
}

void
scn::TreeNodeVisitor::traverse(scn::TreeNode& node)
{
    node.traverse(*this);
}
