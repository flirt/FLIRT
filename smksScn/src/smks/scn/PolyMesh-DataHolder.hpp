// ========================================================================= //
// Copyright 2016-2018 SUPAMONKS_STUDIO                                      //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
//                                                                           //
// ========================================================================= //
#pragma once

#include "smks/scn/PolyMesh.hpp"
#include "AbstractObjectDataHolder.hpp"
#include "PolyMesh-AbstractGeometry.hpp"
#include "../util/data/ArrayCache.hpp"

namespace smks { namespace scn
{
    ////////////////////////
    // struct PolyMesh::DataHolder
    ////////////////////////
    class PolyMesh::DataHolder:
        public AbstractObjectDataHolder
    {
    private:
        ////////////////////////////////////
        // enum PolyMesh::DataHolder::GeomFeatures
        ////////////////////////////////////
        enum GeomFeatures
        {
            NONE                = 0,
            TRIS_OK             = 1 << 0,
            QUADS_OK            = 1 << 1,
            FACETED             = 1 << 2,
            SMOOTHED            = 1 << 3,
            COARSE_TRIS         = FACETED   | TRIS_OK,              //<! low-res tri-mesh used for OpenGL
            UNFACETED_TRIQUADS  = SMOOTHED  | TRIS_OK | QUADS_OK,   //<! unfaceted triquad-mesh whose smoothing is left up to embree
            SMOOTH_QUADS        = SMOOTHED  | FACETED | QUADS_OK,   //<! opensubdiv quad-mesh used for OpenGL
            SMOOTH_TRIS         = SMOOTHED  | FACETED | TRIS_OK     //<! opensubdiv tri-mesh used for Flirt
        };

    protected:
        typedef boost::unordered_map<GeomFeatures, AbstractGeometry::Ptr>   GeometryMap;

    private:
        AbstractPolyMeshSchema*                     _schema;    // does not own it
        GeometryMap                                 _geometries;
        util::data::ArrayCache<xchg::BoundingBox>   _bounds;

    public:
        DataHolder();

        void
        build(AbstractObjectSchema*);

        void
        initialize();

        void
        uninitialize();

        void
        synchronizeWithSchema();

        inline
        void
        dirtyCacheEntry(size_t idx, bool synchronize)
        {
            throw sys::Exception(
                "Only implemented for scene objects users can manually define.",
                "Dirty PolyMesh Data");
        }

    private:
        void
        unsetAllParameters();

        void
        glSynchronize();

        void
        rtSynchronize();

        const xchg::BoundingBox&
        getBounds();

        AbstractGeometry::Ptr
        getGeometry(GeomFeatures);

    private:
        static
        bool
        supported(GeomFeatures);
    };
}
}
