// ========================================================================= //
// Copyright 2016-2018 SUPAMONKS_STUDIO                                      //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
//                                                                           //
// ========================================================================= //
#include "PolyMesh-TBBClearBufferElementKernel.hpp"

#include <smks/sys/Exception.hpp>

using namespace smks;

scn::TBBClearBufferElementKernel::TBBClearBufferElementKernel(float*    buffer,
                                                              size_t    stride,
                                                              size_t    numElements):
    _buffer     (buffer),
    _stride     (stride),
    _numElements(numElements)
{
    checkKernelCorrectness();
}

scn::TBBClearBufferElementKernel::TBBClearBufferElementKernel(const TBBClearBufferElementKernel& k):
    _buffer     (k._buffer),
    _stride     (k._stride),
    _numElements(k._numElements)
{
    checkKernelCorrectness();
}

void
scn::TBBClearBufferElementKernel::checkKernelCorrectness() const
{
    if (!_buffer)
        throw sys::Exception("No valid buffer.", "Buffer Clear TBB Kernel");
    if (_stride == 0)
        throw sys::Exception("Stride must be positive.", "Buffer Clear TBB Kernel");
    if (_numElements == 0)
        throw sys::Exception("Element number must be positive.", "Buffer Clear TBB Kernel");
    if (_numElements > _stride)
        throw sys::Exception("Element number cannot be greater than buffer stride.", "Buffer Clear TBB Kernel");
}

void
scn::TBBClearBufferElementKernel::operator()(tbb::blocked_range<int> const& r) const
{
    float* ptr = _buffer + r.begin() * _stride;

    // reset normals to 0
    for (int i = r.begin(); i < r.end(); ++i)
    {
        memset(ptr, 0, _numElements * sizeof(float));
        ptr += _stride;
    }
}
