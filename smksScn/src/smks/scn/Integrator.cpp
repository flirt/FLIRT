// ========================================================================= //
// Copyright 2016-2018 SUPAMONKS_STUDIO                                      //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
//                                                                           //
// ========================================================================= //
#include <smks/sys/Exception.hpp>
#include <smks/sys/Log.hpp>
#include <smks/sys/ResourceDatabase.hpp>
#include <smks/img/UDIM.hpp>
#include <smks/parm/BuiltIns.hpp>
#include <smks/xchg/ParmEvent.hpp>
#include <smks/xchg/Image.hpp>
#include <smks/xchg/NodeCode.hpp>
#include <smks/xchg/NodeCodes.hpp>

#include "smks/scn/Scene.hpp"
#include "smks/scn/Integrator.hpp"

using namespace smks;

// static
scn::Integrator::Ptr
scn::Integrator::create(const char*             code,
                        const char*             name,
                        TreeNode::WPtr const&   parent)
{
    if (code == nullptr ||
        strlen(code) == 0)
        throw sys::Exception("Invalid code.", "Integrator Creation");

    Ptr ptr(new Integrator(name, parent));
    ptr->build(ptr);
    ptr->setParameter<std::string>(parm::code(), code, parm::SKIPS_RESTRAINTS);
    return ptr;
}

void
scn::Integrator::handleParmEvent(const xchg::ParmEvent& evt)
{
    RtNode::handleParmEvent(evt);
    //---
    if (evt.parm() == parm::backplatePath().id())
    {
        std::string backplatePath;
        if ( (evt.type() == xchg::ParmEvent::PARM_ADDED || evt.type() == xchg::ParmEvent::PARM_CHANGED) &&
            getParameter<std::string>(parm::backplatePath(), backplatePath) &&
            !backplatePath.empty() )
        {
            TreeNode::Ptr const&    root    = this->root().lock();
            Scene*                  scene   = root ? root->asScene() : nullptr;
            if (scene)
            {
                if (img::isUDIMFilePath(backplatePath.c_str()))
                {
                    std::stringstream sstr;
                    sstr
                        << "Backplate filepath '" << backplatePath << "' "
                        << "refers to a UDIM-like texture set. Ignored.";
                    FLIRT_LOG(LOG(DEBUG) << sstr.str();)

                    _unsetParameter(parm::backplate(), parm::SKIPS_RESTRAINTS);
                }
                else
                {
                    xchg::Image::Ptr const& image = xchg::Image::create(
                        backplatePath.c_str(),
                        scene->getOrCreateResourceDatabase());
                    xchg::ObjectPtr obj(image);
                    FLIRT_LOG(LOG(DEBUG)
                        << "create backplate image for integrator '" << name() << "' "
                        << "(filepath = '" << backplatePath << "').";)

                    setParameter<xchg::ObjectPtr>(parm::backplate(), obj, parm::SKIPS_RESTRAINTS);
                }
            }
        }
        else
            _unsetParameter(parm::backplate(), parm::SKIPS_RESTRAINTS);
    }
}

// virtual
bool
scn::Integrator::isParameterWritable(unsigned int parmId) const
{
    if (isParameterRelevantForClass(parmId))
        return
            parmId != parm::code        ().id() &&
            parmId != parm::backplate   ().id();
    return RtNode::isParameterWritable(parmId);
}

// virtual
bool
scn::Integrator::isParameterRelevant(unsigned int parmId) const
{
    return isParameterRelevantForClass(parmId) || RtNode::isParameterRelevant(parmId);
}

bool
scn::Integrator::isParameterRelevantForClass(unsigned int parmId) const
{
    if (parmId == parm::code().id())
        return true;

    const unsigned int code = getCode();
    if (code == xchg::code::default().id())
    {
        return
            parmId == parm::maxRayIntensity     ().id() ||
            parmId == parm::numLightsForDirect  ().id() ||
            parmId == parm::minRayContribution  ().id() ||
            parmId == parm::maxRayDepth         ().id() ||
            parmId == parm::backplate           ().id() ||
            parmId == parm::backplatePath       ().id();
    }
    return false;
}
