// ========================================================================= //
// Copyright 2016-2018 SUPAMONKS_STUDIO                                      //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
//                                                                           //
// ========================================================================= //
#include "BasisType.hpp"

using namespace smks;

std::string
std::to_string(scn::BasisType type)
{
    switch (type)
    {
    case scn::BasisType::NO_BASIS:          return "none";
    case scn::BasisType::BEZIER_BASIS:      return "Bezier";
    case scn::BasisType::BSPLINE_BASIS:     return "B-spline";
    case scn::BasisType::CATMULLROM_BASIS:  return "Catmull-Rom";
    case scn::BasisType::HERMITE_BASIS:     return "Hermite";
    case scn::BasisType::POWER_BASIS:       return "power";
    default:                                return "?";
    }
}
