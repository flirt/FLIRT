// ========================================================================= //
// Copyright 2016-2018 SUPAMONKS_STUDIO                                      //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
//                                                                           //
// ========================================================================= //
#include <smks/sys/Exception.hpp>

#include "ParmConnectionGraph.hpp"

using namespace smks;

scn::ParmConnectionGraph::ParmConnectionGraph():
    _graph()
{
    clear();
}

void
scn::ParmConnectionGraph::clear()
{
    _graph.clear();
}

void
scn::ParmConnectionGraph::add(unsigned int inNodeId,
                              unsigned int inParmId,
                              unsigned int outNodeId,
                              unsigned int outParmId)
{
    if (inNodeId == 0 || inParmId == 0 || outNodeId == 0 || outParmId == 0)
        return;

    const size_t inId   = Node::getId(inNodeId,  inParmId);
    const size_t outId  = Node::getId(outNodeId, outParmId);

    if (inId == 0)
        throw sys::Exception("Invalid input information for parameter connection.", "Parameter Connection Graph");
    if (outId == 0)
        throw sys::Exception("Invalid output information for parameter connection.", "Parameter Connection Graph");

    NodeMap::iterator inIt = _graph.find(inId);
    if (inIt == _graph.end())
        inIt = _graph.insert(std::pair<size_t, Node>(inId, Node(inNodeId, inParmId))).first;
#if defined(CHECK_IDS)
    else if (inNodeId != inIt->second.nodeId() ||
        inParmId != inIt->second.parmId())
        throw sys::Exception("Id collision between edges of the scene's parameter connection graph.", "Parameter Connection Graph");
#endif // defined(CHECK_IDS)

    NodeMap::iterator outIt = _graph.find(outId);
    if (outIt == _graph.end())
        outIt = _graph.insert(std::pair<size_t, Node>(outId, Node(outNodeId, outParmId))).first;
#if defined(CHECK_IDS)
    else if (outNodeId != outIt->second.nodeId() ||
        outParmId != outIt->second.parmId())
        throw sys::Exception("Id collision between edges of the scene's parameter connection graph.", "Parameter Connection Graph");
#endif // defined(CHECK_IDS)

    assert(inIt     != _graph.end());
    assert(outIt    != _graph.end());

    // record connection
    outIt->second.incrNumPredecessors();
    inIt ->second.addSuccessor(outId);
}

void
scn::ParmConnectionGraph::remove(unsigned int inNodeId,
                                 unsigned int inParmId,
                                 unsigned int outNodeId,
                                 unsigned int outParmId)
{
    const size_t inId   = Node::getId(inNodeId,  inParmId);
    const size_t outId  = Node::getId(outNodeId, outParmId);

    NodeMap::iterator inIt  = _graph.find(inId);
    NodeMap::iterator outIt = _graph.find(outId);

    // remove connection information
    if (outIt != _graph.end())
    {
        outIt->second.decrNumPredecessors();
        if (outIt->second.isolated())
            _graph.erase(outIt);
    }
    if (inIt != _graph.end())
    {
        inIt->second.removeSuccessor(outId);
        if (inIt->second.isolated())
            _graph.erase(inIt);
    }
}

bool
scn::ParmConnectionGraph::cyclic() const
{
    NodeStatusMap nodeStatus;

    for (NodeMap::const_iterator it = _graph.begin(); it != _graph.end(); ++it)
        nodeStatus.insert(std::pair<size_t, NodeStatus>(it->first, NodeStatus()));

    for (NodeMap::const_iterator it = _graph.begin(); it != _graph.end(); ++it)
        if (cyclic(it->first, nodeStatus))
            return true;
    return false;
}

bool
scn::ParmConnectionGraph::cyclic(size_t v, NodeStatusMap& nodeStatus) const
{
    NodeMap::const_iterator const& itNodeV      = _graph.find(v);
    NodeStatusMap::iterator const& itStatusV    = nodeStatus.find(v);

    assert(itNodeV      != _graph.end());
    assert(itStatusV    != nodeStatus.end());

    if(!itStatusV->second.visited)
    {
        // Mark the current node as visited and part of recursion stack
        itStatusV->second.visited   = true;
        itStatusV->second.onStack   = true;

        // Recur for all the vertices adjacent to this vertex
        const Node::Successors& successorsV = itNodeV->second.successors();

        for (Node::Successors::const_iterator itW = successorsV.begin(); itW != successorsV.end(); ++itW)
        {
            NodeStatusMap::iterator const& itStatusW = nodeStatus.find(*itW);

            assert(itStatusW != nodeStatus.end());
            if (!itStatusW->second.visited &&
                cyclic(*itW, nodeStatus))
                return true;
            else if (itStatusW->second.onStack)
                return true;
        }
    }
    itStatusV->second.onStack = false;  // remove the vertex from recursion stack

    return false;
}
