// ========================================================================= //
// Copyright 2016-2018 SUPAMONKS_STUDIO                                      //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
//                                                                           //
// ========================================================================= //
#include <smks/sys/Exception.hpp>
#include <smks/sys/Log.hpp>
#include <smks/sys/ResourceDatabase.hpp>
#include <smks/img/UDIM.hpp>
#include <smks/parm/BuiltIns.hpp>
#include <smks/xchg/NodeCode.hpp>
#include <smks/xchg/NodeCodes.hpp>
#include <smks/xchg/ParmEvent.hpp>
#include <smks/xchg/Image.hpp>
#include <smks/xchg/TileImages.hpp>

#include "smks/scn/Scene.hpp"
#include "smks/scn/Texture.hpp"

using namespace smks;

// static
scn::Texture::Ptr
scn::Texture::create(const char*            code,
                     const char*            name,
                     TreeNode::WPtr const&  parent)
{
    if (code == nullptr ||
        strlen(code) == 0)
        throw sys::Exception("Empty texture code.", "Texture Creation");

    Ptr ptr(new Texture(name, parent));

    ptr->build(ptr);
    ptr->setParameter<std::string>(parm::code(), code, parm::SKIPS_RESTRAINTS);

    return ptr;
}

void
scn::Texture::handleParmEvent(const xchg::ParmEvent& evt)
{
    Shader::handleParmEvent(evt);
    //---
    if (evt.parm() == parm::filePath().id())
    {
        std::string filePath;
        if ( (evt.type() == xchg::ParmEvent::PARM_ADDED || evt.type() == xchg::ParmEvent::PARM_CHANGED) &&
            getParameter<std::string>(parm::filePath(), filePath) &&
            !filePath.empty() )
        {
            TreeNode::Ptr const&    root    = this->root().lock();
            Scene*                  scene   = root ? root->asScene() : nullptr;
            if (scene)
            {
                if (img::isUDIMFilePath(filePath.c_str()))
                {
                    xchg::TileImages::Ptr const& tileImages = xchg::TileImages::create(
                        filePath.c_str(),
                        scene->getOrCreateResourceDatabase());
                        xchg::ObjectPtr obj(tileImages);

                    FLIRT_LOG(LOG(DEBUG)
                        << "create UDIM tile carrier for texture '" << name() << "' "
                        << "(filepath = '" << filePath << "').";)
                    setParameter<xchg::ObjectPtr>   (parm::tileImages(), obj,   parm::SKIPS_RESTRAINTS);
                    _unsetParameter                 (parm::image(),             parm::SKIPS_RESTRAINTS);
                }
                else
                {
                    xchg::Image::Ptr const& image = xchg::Image::create(
                        filePath.c_str(),
                        scene->getOrCreateResourceDatabase());
                    xchg::ObjectPtr obj(image);

                    FLIRT_LOG(LOG(DEBUG)
                        << "create image carrier for texture '" << name() << "' "
                        << "(filepath = '" << filePath << "').";)
                    setParameter<xchg::ObjectPtr>   (parm::image(), obj,    parm::SKIPS_RESTRAINTS);
                    _unsetParameter                 (parm::tileImages(),    parm::SKIPS_RESTRAINTS);
                }
            }
        }
        else
        {
            _unsetParameter(parm::image(),      parm::SKIPS_RESTRAINTS);
            _unsetParameter(parm::tileImages(), parm::SKIPS_RESTRAINTS);
        }
    }
}

// virtual
bool
scn::Texture::isParameterWritable(unsigned int parmId) const
{
    if (isParameterRelevantForClass(parmId))
        return
            parmId != parm::code        ().id() &&
            parmId != parm::image       ().id() &&
            parmId != parm::tileImages  ().id();
    return Shader::isParameterWritable(parmId);
}

// virtual
bool
scn::Texture::isParameterRelevant(unsigned int parmId)const
{
    return isParameterRelevantForClass(parmId) || Shader::isParameterRelevant(parmId);
}

bool
scn::Texture::isParameterRelevantForClass(unsigned int parmId) const
{
    return
        parmId == parm::code        ().id() ||
        parmId == parm::image       ().id() ||
        parmId == parm::tileImages  ().id() ||
        parmId == parm::filePath    ().id() ||
        parmId == parm::transformUV ().id() ||
        parmId == parm::scale       ().id();
}
