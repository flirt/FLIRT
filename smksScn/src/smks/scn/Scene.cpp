// ========================================================================= //
// Copyright 2016-2018 SUPAMONKS_STUDIO                                      //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
//                                                                           //
// ========================================================================= //
#include <iomanip>
#include <vector>
#include <stack>
#include <deque>

#include <boost/unordered_set.hpp>
#include <boost/unordered_map.hpp>

#include <ImathVec.h>
#include <ImathMatrix.h>

#include <smks/sys/Log.hpp>
#include <smks/sys/Exception.hpp>
#include <smks/sys/ResourceDatabase.hpp>

#include <smks/xchg/BoundingBox.hpp>
#include <smks/xchg/IdSet.hpp>
#include <smks/xchg/SceneEvent.hpp>
#include <smks/xchg/ParmEvent.hpp>
#include <smks/xchg/GUIEvents.hpp>
#include <smks/xchg/AbstractGUIEventCallback.hpp>
#include <smks/xchg/AbstractProgressCallbacks.hpp>
#include <smks/xchg/VectorPtr.hpp>
#include <std/to_string_xchg.hpp>

#include <smks/util/string/getId.hpp>

#include "smks/scn/Scene.hpp"
#include "smks/scn/Archive.hpp"
#include "smks/scn/SceneObject.hpp"
#include "smks/scn/Xform.hpp"
#include "smks/scn/Camera.hpp"
#include "smks/scn/Drawable.hpp"
#include "smks/scn/PolyMesh.hpp"
#include "smks/scn/Curves.hpp"
#include "smks/scn/Points.hpp"
#include "smks/scn/Shader.hpp"
#include "smks/scn/GraphAction.hpp"
#include "smks/scn/ParmAssign.hpp"
#include "smks/scn/RtNode.hpp"
#include "smks/scn/TreeNodeFilter.hpp"
//---
#include "smks/scn/AbstractSceneObserver.hpp"
#include "smks/scn/TreeNodeVisitor.hpp"
#include "smks/scn/ParmConnectionBase.hpp"
#include "smks/scn/IdConnection.hpp"
#include <smks/util/string/match.hpp>
#include "ParmConnectionGraph.hpp"

#include "TransformHierarchy.hpp"
#include "VisibilityHierarchy.hpp"
#include "NumSamplesHierarchy.hpp"
#include "InitializationHierarchy.hpp"
#include "Tracked.hpp"
#include "detectNodeLinkCycle.hpp"

//----------------------------------------------------------------------------------
#define IMPL_COMPUTE_GLOBAL_PARM_SPECIALIZATION(_CTYPE_)                            \
    template<>                                                                      \
    void                                                                            \
    smks::scn::Scene::computeGlobalParameter<_CTYPE_>(const parm::BuiltIn&  parm,   \
                                                      unsigned int          nodeId, \
                                                      _CTYPE_&              ret)    \
    {                                                                               \
        _pImpl->computeGlobalParameter<_CTYPE_>(parm, nodeId, ret);                 \
    }
//----------------------------------------------------------------------------------

namespace smks { namespace scn
{
    typedef Tracked<xchg::ParmEvent> TrackedParmEvent;

    /////////////////////
    // class ScenePImpl
    /////////////////////
    class ScenePImpl
    {
    private:
        typedef boost::unordered_map<unsigned int, TreeNode::WPtr>          NodeMap;
        typedef boost::unordered_map<unsigned int, ParmConnectionBase::Ptr> ParmConnectionMap;
        typedef boost::unordered_map<unsigned int, xchg::IdSet>             IdSetMap;
        typedef xchg::VectorPtr<xchg::AbstractGUIEventCallback>             GUIEventCallbacks;
        typedef xchg::VectorPtr<xchg::AbstractProgressCallbacks>            ProgressCallbacks;
    private:
        const Scene::Config                    _cfg;

        Scene&                                 _self;
        float                                  _minTime;
        float                                  _maxTime;
        xchg::IdSet                            _prvSelection;          //<! previously processed selection

        NodeMap                                _descendants;
        ParmConnectionMap                      _parmConnections;
        IdSetMap                               _nodeToConnectionIds;   //<! connection ID target -> connection IDs
        ParmConnectionGraph                    _parmConnectionGraph;

        std::deque<xchg::SceneEvent>           _pendingSceneEvents;    //<! FIXME: should implement a proper event queue
        std::deque<TrackedParmEvent::Ptr>      _pendingExtParmEvents;  //<! FIXME: should implement a proper event queue
        xchg::VectorPtr<AbstractSceneObserver> _observers;
        //---
        std::vector<GUIEventCallbacks>         _guiEventCallbacks;
        ProgressCallbacks                      _progressCallbacks;
        //---
        TransformHierarchy::Ptr                _transformTree;         //<! structure responsible for computing world matrices in a data-oriented way
        VisibilityHierarchy::Ptr               _visibilityTree;        //<! used to compute global visibility
        NumSamplesHierarchy::Ptr               _numSamplesTree;        //<! used to compute geometric flags
        InitializationHierarchy::Ptr           _initdTree;             //<! used to directly decide whether a node and ancestors are properly initialized

        sys::ResourceDatabase::Ptr             _resources;

    public:
        ScenePImpl(Scene&       self,
                   const char*  jsonCfg):
            _cfg                 (jsonCfg),
            _self                (self),
            _prvSelection        (),
            _descendants         (),
            _parmConnections     (),
            _nodeToConnectionIds (),
            _parmConnectionGraph (),
            _pendingSceneEvents  (),
            _pendingExtParmEvents(),
            _observers           (),
            //---
            _guiEventCallbacks   (xchg::NUM_GUI_EVENTS, GUIEventCallbacks()),
            _progressCallbacks   (),
            //---
            _transformTree       (TransformHierarchy::create()),
            _visibilityTree      (VisibilityHierarchy::create()),
            _numSamplesTree      (NumSamplesHierarchy::create()),
            _initdTree           (InitializationHierarchy::create()),
            _resources           (nullptr)
        {
            _observers.reserve(8);
        }

        inline
        const Scene::Config&
        config() const
        {
            return _cfg;
        }

        inline
        void
        build(TreeNode::Ptr const&);

        inline
        void
        clear()
        {
            _self.unsetParameter(parm::timeBounds());
            _descendants            .clear();
            _pendingSceneEvents     .clear();
            _pendingExtParmEvents   .clear();
            _observers              .clear();
            for (size_t i = 0; i < _guiEventCallbacks.size(); ++i)
                _guiEventCallbacks[i].clear();
            _progressCallbacks.clear();

            while (!_parmConnections.empty())
                disconnect(_parmConnections.begin()->first);

            _transformTree  ->dispose();
            _visibilityTree ->dispose();
            _numSamplesTree ->dispose();
            _initdTree      ->dispose();

            _resources = nullptr;
        }

        void
        getIds(xchg::IdSet& ret, const Scene::IdGetterOptions* options) const
        {
            xchg::IdSet tmp;

            visitAndGatherIds(tmp, options);
            filterIds(tmp, options, ret);
        }

    private:
        inline
        void
        visitAndGatherIds(xchg::IdSet&, const Scene::IdGetterOptions*) const;

        inline
        void
        filterIds(const xchg::IdSet&, const Scene::IdGetterOptions*, xchg::IdSet&) const;

    public:
        inline
        void
        computeBounds(TreeNode*, xchg::BoundingBox&);

    private:
        template <typename ValueType> inline
        void
        getGlobalValue(unsigned int, ValueType&, const ValueType&, DAGHierarchy<ValueType>&);
    public:
        template<typename ValueType>
        void
        computeGlobalParameter(const parm::BuiltIn& parm, unsigned int nodeId, ValueType& ret)
        {
            throw sys::Exception("Unsupported parameter type.", "Global Computation For '" + std::string(parm.c_str()) + "' (PImpl)");
        }
        DEF_COMPUTE_GLOBAL_PARM_SPECIALIZATION(char)
        DEF_COMPUTE_GLOBAL_PARM_SPECIALIZATION(size_t)
        DEF_COMPUTE_GLOBAL_PARM_SPECIALIZATION(math::Affine3f)
    public:
        inline
        bool
        loadDescendant(unsigned int, float currentTime);

        inline
        bool
        hasDescendant(unsigned int id) const
        {
            return _descendants.find(id) != _descendants.end();
        }

        inline
        TreeNode::WPtr const&
        descendantById(unsigned int id) const
        {
            NodeMap::const_iterator const& it = _descendants.find(id);

            if (it == _descendants.end())
            {
                std::stringstream sstr;
                sstr << "Failed to find descendant in scene (ID = " << id << ").";
                throw sys::Exception(sstr.str().c_str(), "Scene Descendant Getter");
            }

            return it->second;
        }

        inline
        TreeNode::Ptr
        ascendantByClass(unsigned int, xchg::NodeClass, bool selfAllowed) const;

        inline
        void
        registerDescendant(TreeNode::Ptr const& n)
        {
            if (!n)
                return;
            if (n->id() == 0)
            {
                std::stringstream sstr;
                sstr << "No node ID for '" << n->name() << "'.";
                throw sys::Exception(sstr.str().c_str(), "Scene Object Registration");
            }

            NodeMap::const_iterator const& it = _descendants.find(n->id());

            if (it != _descendants.end())
            {
                std::stringstream sstr;
                sstr
                    << "Node ID collision in the scene\n"
                    << "\t- ID = " << n->id() << "\n"
                    << "\t- '" << n->fullName() << "'\n"
                    << "\t- '"  << it->second.lock()->fullName() << ".";
                throw sys::Exception(sstr.str().c_str(), "Scene Object Registration");
            }

            _descendants.insert(std::pair<unsigned int, TreeNode::WPtr>(n->id(), n)); // MUTEXME
            FLIRT_LOG(LOG(DEBUG)
                << "Node '" << n->name() << "' (ID = " << n->id() << ") "
                << "registered as '" << std::to_string(n->nodeClass()) << "'.";)
        }

        inline
        void
        deregisterDescendant(unsigned int id)
        {
            NodeMap::const_iterator const& it = _descendants.find(id);

            if (it != _descendants.end())
                _descendants.erase(it);
            FLIRT_LOG(LOG(DEBUG)
                << "Node associated with ID = " << id << " deregistered.";)
        }

        inline
        unsigned int //<! returns connection ID
        registerConnection(ParmConnectionBase::Ptr const& cnx);

        inline
        unsigned int //<! returns connection ID
        registerConnectionId(IdConnection::Ptr const& cnx);

        inline
        bool
        hasConnection(unsigned int cnxId) const
        {
            return _parmConnections.find(cnxId) != _parmConnections.end();
        }

        inline
        bool
        hasIdConnection(unsigned int cnxId) const;

        inline
        size_t
        numConnections() const
        {
            return _parmConnections.size();
        }

        inline
        ParmConnectionBase::Ptr const&
        connectionById(unsigned int cnxId) const
        {
            ParmConnectionMap::const_iterator const& it = _parmConnections.find(cnxId);

            if (it == _parmConnections.end())
            {
                std::stringstream sstr;
                sstr
                    << "Failed to find parameter connection in scene "
                    << "(ID = " << cnxId << ").";
                throw sys::Exception(sstr.str().c_str(), "Scene Connection Getter");
            }

            return it->second;
        }

        inline
        bool
        connectable(unsigned int    inNodeId,
                    const char*     inParmName,
                    unsigned int    outNodeId,
                    const char*     outParmName);

        inline
        void
        disconnect(unsigned int);

        inline
        void
        disconnectId(unsigned int);

        inline
        bool
        detectNodeLinkCycle(unsigned int target, unsigned int source) const;

        inline
        void
        registerObserver(AbstractSceneObserver::Ptr const& obs)
        {
            _observers.add(obs);
        }
        inline
        void
        deregisterObserver(AbstractSceneObserver::Ptr const& obs)
        {
            _observers.remove(obs);
        }

        inline
        void
        handleSceneEvent(const xchg::SceneEvent&);

        inline
        void
        handleParmEvent(const xchg::ParmEvent&);

        //inline
        //bool
        //handleDescendantParmEvent(const xchg::ParmEvent&);

        inline
        void
        deliver(const xchg::SceneEvent&);

        inline
        unsigned int
        addNode(const char* name, unsigned int parentId);

        inline
        unsigned int
        addArchive(const std::string& filePath, unsigned int parentId);

    private:
        inline
        void
        handleNodeCreatedOrDeleted(const xchg::SceneEvent&);
        inline
        void
        handleParmConnectedOrDisconnected(const xchg::SceneEvent&);

        inline
        void
        addOrRemoveNodeLink(const xchg::SceneEvent&);

        template<typename ValueType> inline
        void
        addOrRemoveTreeEntry(const xchg::SceneEvent&,
                             DAGHierarchy<ValueType>&,
                             const parm::BuiltIn& globalParm,
                             const parm::BuiltIn& localParm,
                             const parm::BuiltIn* inheritsParm=nullptr);

        inline
        void
        handleSelectionChanged(xchg::ParmEvent::Type);

        inline
        void
        handleTimeBoundsChanged(xchg::ParmEvent::Type, unsigned int);
        inline
        void
        handleSceneTimeBoundsChanged(xchg::ParmEvent::Type);
        inline
        void
        handleDescendantTimeBoundsChanged(xchg::ParmEvent::Type, unsigned int);

        template<typename ValueType> inline
        bool
        handleIfTreeEntryChanged(const xchg::ParmEvent&,
                                 DAGHierarchy<ValueType>&,
                                 const parm::BuiltIn& globalParm,
                                 const parm::BuiltIn& localParm,
                                 const parm::BuiltIn* inheritsParm=nullptr);
        void
        haveNodeHandleParmEvent(xchg::ParmEvent::Type, const parm::BuiltIn&, scn::TreeNode&);

    private:
        inline
        bool
        getTimeBounds(math::Vector2f&) const;

        void
        addNodeLink(unsigned int nodeId, unsigned int parmId);
        void
        removeNodeLink(unsigned int nodeId, unsigned int parmId);

    public:
        //--------------------------
        // external parameter events
        //--------------------------
        inline
        void
        pushExternalEvent(TrackedParmEvent::Ptr const& evt)
        {
            _pendingExtParmEvents.push_back(evt);
        }
        inline
        bool
        hasExternalEvent() const
        {
            return !_pendingExtParmEvents.empty();
        }
        inline
        TrackedParmEvent::Ptr
        popExternalEvent()
        {
            if (_pendingExtParmEvents.empty())
                return nullptr;

            TrackedParmEvent::Ptr ret = _pendingExtParmEvents.front();
            _pendingExtParmEvents.pop_front();
            return ret;
        }

        // GUI events
        //-----------
        inline
        void
        handleGUIEvent(const xchg::GUIEvent& evt)
        {
            assert(evt.type() != xchg::NUM_GUI_EVENTS);
            const GUIEventCallbacks& callbacks = _guiEventCallbacks[evt.type()];
            for (size_t i = 0; i < callbacks.size(); ++i)
            {
                assert(callbacks[i]);
                callbacks[i]->operator()(evt);
            }
        }

        inline
        void
        registerGUIEventCallback(xchg::GUIEventType typ, xchg::AbstractGUIEventCallback::Ptr const& callback)
        {
            if (typ == xchg::NUM_GUI_EVENTS)
                throw sys::Exception(
                    "Invalid GUI event type.",
                    "GUI Event Callback Registration");
            if (!callback->understands(typ))
                throw sys::Exception(
                    "Mismatching GUI event type and callback.",
                    "GUI Event Callback Registration");

            _guiEventCallbacks[typ].add(callback);
        }

        inline
        void
        deregisterGUIEventCallback(xchg::GUIEventType typ, xchg::AbstractGUIEventCallback::Ptr const& callback)
        {
            if (typ == xchg::NUM_GUI_EVENTS)
                throw sys::Exception(
                    "Invalid GUI event type.",
                    "GUI Event Callback Registration");

            _guiEventCallbacks[typ].remove(callback);
        }

        // Progress
        //---------
        inline
        void
        notifyProgressBeginning(const char*, size_t totalSteps);
        inline
        bool
        notifyProgressProceeding(size_t);
        inline
        void
        notifyProgressEnd(const char*);

        inline
        void
        registerProgressCallbacks(xchg::AbstractProgressCallbacks::Ptr const&);

        inline
        void
        deregisterProgressCallbacks(xchg::AbstractProgressCallbacks::Ptr const&);

        // resource management
        //--------------------
        inline
        void
        setSharedResourceDatabase(sys::ResourceDatabase::Ptr const& value)
        {
            _resources = value;
        }

        inline
        sys::ResourceDatabase::Ptr const&
        getOrCreateResourceDatabase();
    };
}
}

using namespace smks;

/////////////////////
// class ScenePImpl
/////////////////////
void
scn::ScenePImpl::build(TreeNode::Ptr const& self)
{
    assert(self);
    assert(self->asScene());
    //_self = std::static_pointer_cast<Scene>(self);
}

scn::TreeNode::Ptr
scn::ScenePImpl::ascendantByClass(unsigned int      nodeId,
                                  xchg::NodeClass   nodeClass,
                                  bool              selfAllowed) const
{
    AbstractTreeNodeFilter::Ptr const&  filter  = FilterTreeNodeByClass::create(nodeClass);
    NodeMap::const_iterator const&      it      = _descendants.find(nodeId);
    TreeNode::Ptr                       node    =  it != _descendants.end() ? it->second.lock() : nullptr;
    do
    {
        if (!node)
            break;
        if (node->id() > 0 && (*filter)(*node))
        {
            if (node->id() != nodeId || selfAllowed)
                return node;
        }
        node = node->parent().lock();
    }
    while (node);

    return 0;
}

void
scn::ScenePImpl::computeBounds(TreeNode* n, xchg::BoundingBox& bounds)
{
    //////////////////////
    // class BoundsVisitor
    //////////////////////
    class BoundsVisitor:
        public TreeNodeVisitor
    {
        EIGEN_MAKE_ALIGNED_OPERATOR_NEW
    private:
        math::Affine3f      _currentLocalToWorld;
        xchg::BoundingBox&  _bounds;

    public:
        explicit inline
        BoundsVisitor(xchg::BoundingBox& bounds):
            TreeNodeVisitor         (TreeNodeVisitor::DESCENDING),
            _currentLocalToWorld    (),
            _bounds                 (bounds)
        {
            _currentLocalToWorld.setIdentity();
        }

        inline
        void
        apply(TreeNode& n)
        {
            // save current state
            //const Imath::M44d savedLocalToWorld = _currentLocalToWorld;
            const math::Affine3f savedLocalToWorld = _currentLocalToWorld;

            char init = xchg::INACTIVE;

            n.getParameter<char>(parm::initialization(), init);
            if (init == xchg::ACTIVE)
            {
                SceneObject* obj = n.asSceneObject();

                if (obj)
                {
                    if (obj->asXform())
                    {
                        // update current local-to-world transformation
                        //Imath::M44d   xform;

                        //memcpy(xform.getValue(), obj->asXform()->getMatrix(), sizeof(double) << 4);
                        math::Affine3f xfm;

                        obj->asXform()->getParameter<math::Affine3f>(parm::transform(), xfm);
                        _currentLocalToWorld = xfm * _currentLocalToWorld;
                    }
                    else if (obj->asDrawable())
                    {
                        // update bounds with object's transformed bounds
                        xchg::BoundingBox bounds = xchg::BoundingBox::UnitBox();

                        obj->asDrawable()->getParameter<xchg::BoundingBox>(parm::boundingBox(), bounds);
                        if (bounds.valid())
                        {
                            for (size_t i = 0; i < 8; ++i)
                            {
                                math::Vector4f p;

                                bounds.corner(i, p.data());
                                p[3] = 1.0f;

                                const math::Vector4f& q = _currentLocalToWorld * p;

                                _bounds += q.data();
                            }
                        }
                    }
                }
            }

            traverse(n);
            // restore state
            _currentLocalToWorld = savedLocalToWorld;
        }
    };
    //--- class BoundsVisitor

    if (n)
    {
        BoundsVisitor visitor(bounds);

        n->accept(visitor);
    }
}

template <typename ValueType>
void
scn::ScenePImpl::getGlobalValue(unsigned int             nodeId,
                                ValueType&               value,
                                const ValueType&         defaultValue,
                                DAGHierarchy<ValueType>& tree)
{
    value = defaultValue;
    NodeMap::const_iterator const& it   = _descendants.find(nodeId);
    TreeNode::Ptr                  node = it != _descendants.end() ? it->second.lock() : nullptr;
    do
    {
        if (!node)
            break;
        if (tree.has(node->id()))
        {
            value = tree.getGlobal(node->id());
            break;
        }
        node = node->parent().lock();
    }
    while(node);
}

template<>
void
scn::ScenePImpl::computeGlobalParameter<char>(const parm::BuiltIn& parm, unsigned int nodeId, char& ret)
{
    ret = 0;
    if (parm.id() == parm::globallyVisible().id())
        getGlobalValue<char>(nodeId, ret, 1, *_visibilityTree);
    else if (parm.id() == parm::globalInitialization().id())
        getGlobalValue<char>(nodeId, ret, 1, *_initdTree);
}

template<>
void
scn::ScenePImpl::computeGlobalParameter<size_t>(const parm::BuiltIn& parm, unsigned int nodeId, size_t& ret)
{
    ret = 0;
    if (parm.id() == parm::globalNumSamples().id())
        getGlobalValue<size_t>(nodeId, ret, 0, *_numSamplesTree);
}

template<>
void
scn::ScenePImpl::computeGlobalParameter<math::Affine3f>(const parm::BuiltIn& parm, unsigned int nodeId, math::Affine3f& ret)
{
    ret.setIdentity();
    if (parm.id() == parm::worldTransform().id())
        getGlobalValue<math::Affine3f>(nodeId, ret, math::Affine3f::Identity(), *_transformTree);
}

void
scn::ScenePImpl::visitAndGatherIds(xchg::IdSet& ret, const Scene::IdGetterOptions* options) const
{
    //////////////////////
    // class NodeCollector
    //////////////////////
    class NodeCollector:
        public TreeNodeVisitor
    {
    private:
        const AbstractTreeNodeFilter::Ptr   _filter;
        xchg::IdSet&                        _ret;

    public:
        inline
        NodeCollector(bool                                  upwards,
                      AbstractTreeNodeFilter::Ptr const&    filter,
                      xchg::IdSet&                          ret):
        TreeNodeVisitor (upwards ? TreeNodeVisitor::ASCENDING : TreeNodeVisitor::DESCENDING),
            _filter     (filter),
            _ret        (ret)
        { }

        inline
        void
        apply(TreeNode& n)
        {
            if (_filter == nullptr ||
                _filter->operator()(n))
                _ret.insert(n.id());
            traverse(n);
        }
    };
    //////////////////////

    ret.clear();

    TreeNode* fromNode = &_self;    // scene by default
    assert(fromNode);

    if (options && options->fromId() > 0)
    {
        NodeMap::const_iterator const& it = _descendants.find(options->fromId());
        fromNode = it != _descendants.end()
            ? it->second.lock().get()
            : nullptr;

        if (!fromNode)
        {
            std::stringstream sstr;
            sstr
                << "Cannot gather node IDs: "
                << "Failed to find starting node ID = " << options->fromId() << ".";
            FLIRT_LOG(LOG(DEBUG) << sstr.str());
            std::cerr << sstr.str() << std::endl;

            return;
        }
    }

    NodeCollector visitor (
        options ? options->upwards() : false,
        options ? options->filter() : nullptr,
        ret);

    assert(fromNode);
    fromNode->accept(visitor);
}

void
scn::ScenePImpl::filterIds(const xchg::IdSet&               ids,
                           const Scene::IdGetterOptions*    options,
                           xchg::IdSet&                     filtered) const
{
    if (!options ||
        !options->accumulate())
        filtered.clear();

    if (options &&
        options->numRegexes() > 0)
    {
        // prepare candidates by building the ID -> fullname mapping
        boost::unordered_map<unsigned int, std::string> candidates;

        for (xchg::IdSet::const_iterator id = ids.begin(); id != ids.end(); ++id)
        {
            assert(_descendants.find(*id) != _descendants.end());
            TreeNode::Ptr const& node = _descendants.find(*id)->second.lock();
            assert(node);
            candidates.insert(std::pair<unsigned int, std::string>(node->id(), node->fullName()));
        }

        // match candidates
        for (size_t i = 0; i < options->numRegexes(); ++i)
            util::string::matchWithRegex(options->regex(i), candidates);

        // convert to exposable ID list
        for (boost::unordered_map<unsigned int, std::string>::const_iterator it = candidates.begin();
            it != candidates.end();
            ++it)
            filtered.insert(it->first);
    }
    else
    {
        for (xchg::IdSet::const_iterator id = ids.begin(); id != ids.end(); ++id)
            filtered.insert(*id);
    }
}

bool
scn::ScenePImpl::connectable(unsigned int   inNodeId,
                             const char*    inParmName,
                             unsigned int   outNodeId,
                             const char*    outParmName)
{
    if (inNodeId == 0   || inParmName == nullptr    || strlen(inParmName) == 0 ||
        outNodeId == 0  || outParmName == nullptr   || strlen(outParmName) == 0)
        return false;

    const unsigned int  inParmId    = util::string::getId(inParmName);
    const unsigned int  outParmId   = util::string::getId(outParmName);

    if (inNodeId == outNodeId &&
        inParmId == outParmId)
    {
        std::stringstream sstr;
        sstr
            << "WARNING: parameter self-connection forbidden "
            << "for parameter '" << inParmName << "' "
            << "for node ID = " << inNodeId << ".";
        FLIRT_LOG(LOG(DEBUG) << sstr.str();)
        std::cerr << sstr.str() << std::endl;

        return false;
    }

    bool cyclic = false;

    _parmConnectionGraph.add(inNodeId, inParmId, outNodeId, outParmId);
    {
        cyclic = _parmConnectionGraph.cyclic();
        if (cyclic)
        {
            std::stringstream sstr;
            sstr
                << "WARNING: parameter connection "
                << "from parameter '" << inParmName << "' (node ID = " << inNodeId << ") "
                << "to parameter '" << outParmName << "' (node ID = " << outNodeId << ") "
                << "would cause a connection cycle.";
            FLIRT_LOG(LOG(DEBUG) << sstr.str();)
            std::cerr << sstr.str() << std::endl;
        }
    }
    _parmConnectionGraph.remove(inNodeId, inParmId, outNodeId, outParmId);

    return cyclic == false;
}

unsigned int //<! returns connection ID
scn::ScenePImpl::registerConnection(ParmConnectionBase::Ptr const& cnx)
{
    if (!cnx)
        return 0;

    ParmConnectionMap::const_iterator it = _parmConnections.find(cnx->id());

    if (it != _parmConnections.end())
    {
        std::stringstream sstr;
        sstr
            << "Connection ID collision between "
            << "[" << cnx->inNodeId() << "]." << cnx->inParmName()
            << " >> "
            << "[" << cnx->outNodeId() << "]." <<  cnx->outParmName()
            << " and "
            << "[" << it->second->inNodeId() << "]." << it->second->inParmName()
            << " >> "
            << "[" << it->second->outNodeId() << "]." <<  it->second->outParmName()
            << ".";
        throw sys::Exception(sstr.str().c_str(), "Connection Registration");
    }

    _parmConnections.insert(std::pair<unsigned int, ParmConnectionBase::Ptr>(cnx->id(), cnx));
    _parmConnectionGraph.add(cnx->inNodeId(), cnx->inParmId(), cnx->outNodeId(), cnx->outParmId());

    return cnx->id();
}

unsigned int //<! returns connection ID
scn::ScenePImpl::registerConnectionId(IdConnection::Ptr const& cnx)
{
    const unsigned int cnxId = registerConnection(cnx);
    if (cnx)
    {
        _nodeToConnectionIds[cnx->outNodeId()].insert(cnxId);
        addNodeLink(cnx->outNodeId(), cnx->outParmId());
    }

    return cnxId;
}

void
scn::ScenePImpl::disconnect(unsigned int cnxId)
{
    ParmConnectionMap::iterator const&  it  = _parmConnections.find(cnxId);
    ParmConnectionBase::Ptr             cnx = nullptr;
    // it is important to keep a valid reference to the disconnected edge in order
    // to prevent the call of its destructor during disconnection.
    if (it != _parmConnections.end())
    {
        cnx = it->second;

        _parmConnectionGraph.remove(cnx->inNodeId(), cnx->inParmId(), cnx->outNodeId(), cnx->outParmId());
        _parmConnections.erase(it);
    }
}

void
scn::ScenePImpl::disconnectId(unsigned int cnxId)
{
    if (!hasConnection(cnxId))
        return;

    ParmConnectionBase::Ptr const& cnx = connectionById(cnxId);

    if (cnx)
    {
        removeNodeLink(cnx->outNodeId(), cnx->outParmId());
        _nodeToConnectionIds[cnx->outNodeId()].erase(cnxId);
    }
    disconnect(cnxId);
}

bool
scn::ScenePImpl::hasIdConnection(unsigned int cnxId) const
{
    if (!hasConnection(cnxId))
        return false;
    for (IdSetMap::const_iterator it = _nodeToConnectionIds.begin(); it != _nodeToConnectionIds.end(); ++it)
        if (it->second.find(cnxId) != it->second.end())
            return true;
    return false;
}

bool
scn::ScenePImpl::detectNodeLinkCycle(unsigned int target, unsigned int source) const
{
    return scn::detectNodeLinkCycle(_descendants, target, source);
}

void
scn::ScenePImpl::handleSceneEvent(const xchg::SceneEvent& evt)
{
    switch (evt.type())
    {
    default:
        break;
    case xchg::SceneEvent::NODE_CREATED:
    case xchg::SceneEvent::NODE_DELETED:
        handleNodeCreatedOrDeleted(evt);
        break;
    case xchg::SceneEvent::PARM_CONNECTED:
    case xchg::SceneEvent::PARM_DISCONNECTED:
        handleParmConnectedOrDisconnected(evt);
        break;
    }
}

void
scn::ScenePImpl::handleNodeCreatedOrDeleted(const xchg::SceneEvent& evt)
{
    assert(evt.type() == xchg::SceneEvent::NODE_CREATED ||
        evt.type() == xchg::SceneEvent::NODE_DELETED);

    addOrRemoveNodeLink(evt);
    addOrRemoveTreeEntry<math::Affine3f>(
        evt,
        *_transformTree,
        parm::worldTransform(),
        parm::transform(),
        &parm::inheritsTransforms());
    addOrRemoveTreeEntry<char>(
        evt,
        *_visibilityTree,
        parm::globallyVisible(),
        parm::currentlyVisible());
    addOrRemoveTreeEntry<size_t>(
        evt,
        *_numSamplesTree,
        parm::globalNumSamples(),
        parm::numSamples());
    addOrRemoveTreeEntry<char>(
        evt,
        *_initdTree,
        parm::globalInitialization(),
        parm::initialization());
}

void
scn::ScenePImpl::handleParmConnectedOrDisconnected(const xchg::SceneEvent& evt)
{
    assert(evt.type() == xchg::SceneEvent::PARM_CONNECTED ||
        evt.type() == xchg::SceneEvent::PARM_DISCONNECTED);

#if defined(FLIRT_LOG_ENABLED)
    {
        TreeNode::Ptr const& masterNode = hasDescendant(evt.master())   ? descendantById(evt.master()).lock()   : nullptr;
        TreeNode::Ptr const& slaveNode  = hasDescendant(evt.slave())    ? descendantById(evt.slave()).lock()    : nullptr;
        if (masterNode && slaveNode)
        {
            xchg::ParameterInfo masterParmInfo;
            xchg::ParameterInfo slaveParmInfo;

            if (masterNode->getParameterInfo(evt.masterParm(), masterParmInfo) &&
                slaveNode->getParameterInfo(evt.slaveParm(), slaveParmInfo))
            {
                std::stringstream sstr;
                sstr << "'" << masterNode->name() << "'.'" << masterParmInfo.name << "'";
                if (evt.type() == xchg::SceneEvent::PARM_CONNECTED)
                    sstr << " connected to ";
                else
                    sstr << " disconnected from ";
                sstr << "'" << slaveNode->name() << "'.'" << slaveParmInfo.name << "'";
                LOG(DEBUG) << sstr.str().c_str();
            }
        }
    }
#endif // defined(FLIRT_LOG_ENABLED)
}

void
scn::ScenePImpl::addOrRemoveNodeLink(const xchg::SceneEvent& evt)
{
    assert(evt.type() == xchg::SceneEvent::NODE_CREATED ||
        evt.type() == xchg::SceneEvent::NODE_DELETED);

    IdSetMap::const_iterator const& it = _nodeToConnectionIds.find(evt.node());
    if (it == _nodeToConnectionIds.end())
        return;

    const xchg::IdSet& cnxIds = it->second;
    for (xchg::IdSet::const_iterator id = cnxIds.begin(); id != cnxIds.end(); ++id)
        if (hasConnection(*id))
        {
            if (evt.type() == xchg::SceneEvent::NODE_CREATED)
                addNodeLink(evt.node(), connectionById(*id)->outParmId());
            else
                removeNodeLink(evt.node(), connectionById(*id)->outParmId());
        }
}

template<typename ValueType>
void
scn::ScenePImpl::addOrRemoveTreeEntry(const xchg::SceneEvent&   evt,
                                      DAGHierarchy<ValueType>&  tree,
                                      const parm::BuiltIn&      globalParm,
                                      const parm::BuiltIn&      localParm,
                                      const parm::BuiltIn*      inheritsParm)
{
    assert(evt.type() == xchg::SceneEvent::NODE_CREATED ||
        evt.type() == xchg::SceneEvent::NODE_DELETED);

    assert(globalParm.compatibleWith<ValueType>());
    assert(localParm.compatibleWith<ValueType>());
    assert((globalParm.flags() & parm::NEEDS_SCENE) != 0);
    assert((localParm.flags() & parm::NEEDS_SCENE) == 0);
    assert(inheritsParm == nullptr || inheritsParm->compatibleWith<bool>());

    NodeMap::const_iterator const&  it      = _descendants.find(evt.node());
    TreeNode::Ptr const&            node    = it != _descendants.end()
        ? it->second.lock()
        : nullptr;
    if (!node)
        return;

    if (evt.type() == xchg::SceneEvent::NODE_CREATED)
    {
        if (tree.accept(*node))
        {
            // add node to hierarchy
            TreeNode::Ptr const& asc = node
                ? ascendantByClass(node->id(), tree.nodeClass(), false)
                : nullptr;

            tree.addNode(node->id(), asc ? asc->id() : 0);

            // set node's local value
            ValueType parmValue;

            node->getParameter<ValueType>(localParm, parmValue);
            tree.setLocal(node->id(), parmValue);

            // set node's inheritance rule, if any
            if (inheritsParm)
            {
                bool inheritsValue;

                node->getParameter<bool>(*inheritsParm, inheritsValue);
                tree.inherits(node->id(), inheritsValue);
            }
        }
        // notify the node's descendants its global value changed
        haveNodeHandleParmEvent(xchg::ParmEvent::PARM_ADDED, globalParm, *node);
    }
    else // if (evt.type() == xchg::SceneEvent::NODE_DELETED)
    {
        if (tree.accept(*node))
            tree.removeNode (node->id());
        haveNodeHandleParmEvent(xchg::ParmEvent::PARM_REMOVED, globalParm, *node);
    }

}

void
scn::ScenePImpl::handleParmEvent(const xchg::ParmEvent& evt)
{
    if (evt.parm() == parm::selection().id())
        handleSelectionChanged(evt.type());
    else if (evt.parm() == parm::timeBounds().id())
        handleTimeBoundsChanged(evt.type(),
                                evt.node());
    else
    {
        // process specific parameters that may cause scene-bound computations
        bool handled =
            handleIfTreeEntryChanged<math::Affine3f>(
                evt,
                *_transformTree,
                parm::worldTransform(),
                parm::transform(),
                &parm::inheritsTransforms())
            ||
            handleIfTreeEntryChanged<char>(
                evt,
                *_visibilityTree,
                parm::globallyVisible(),
                parm::currentlyVisible())
            ||
            handleIfTreeEntryChanged<size_t>(
                evt,
                *_numSamplesTree,
                parm::globalNumSamples(),
                parm::numSamples())
            ||
            handleIfTreeEntryChanged<char>(
                evt,
                *_initdTree,
                parm::globalInitialization(),
                parm::initialization()) ;
    }
}

void
scn::ScenePImpl::handleSelectionChanged(xchg::ParmEvent::Type type)
{
    xchg::IdSet nxtSelection, selectedIds, deselectedIds;

    _self.getParameter<xchg::IdSet>(parm::selection(), nxtSelection);
    xchg::IdSet::diff(_prvSelection, nxtSelection, selectedIds, deselectedIds);

    // flag newly selected descendants
    bool selected = true;
    for (xchg::IdSet::const_iterator id = selectedIds.begin(); id != selectedIds.end(); ++id)
        if (_self.hasDescendant(*id))
        {
            TreeNode::Ptr const& desc = _self.descendantById(*id).lock();
            if (desc)
                desc->setParameter<bool>(parm::selected(), selected, parm::SKIPS_RESTRAINTS);
        }
    // unflag deselected descendants
    selected = false;
    for (xchg::IdSet::const_iterator id = deselectedIds.begin(); id != deselectedIds.end(); ++id)
        if (_self.hasDescendant(*id))
        {
            TreeNode::Ptr const& desc = _self.descendantById(*id).lock();
            if (desc)
                desc->setParameter<bool>(parm::selected(), selected, parm::SKIPS_RESTRAINTS);
        }
    _prvSelection = nxtSelection;
}

void
scn::ScenePImpl::handleTimeBoundsChanged(xchg::ParmEvent::Type type, unsigned int containerId)
{
    // the behavior depends on whether the change concerns the scene node
    // (possible update of the current time if now out-of-bounds), or one
    // of its descendants (change in values of the scene's time bounds).

    if (containerId == _self.id())
        handleSceneTimeBoundsChanged(type);
    else
        handleDescendantTimeBoundsChanged(type, containerId);
}

void
scn::ScenePImpl::handleSceneTimeBoundsChanged(xchg::ParmEvent::Type type)
{
    // the scene must ensure the fact 'currentTime' remains in the valid time range.
    float           currentTime = 0.0f;
    math::Vector2f  sceneTimeBounds(FLT_MAX, -FLT_MAX);
    if (_self.getParameter<float>           (parm::currentTime(),   currentTime) &&
        _self.getParameter<math::Vector2f>  (parm::timeBounds(),    sceneTimeBounds))
    {
        if (sceneTimeBounds.x() < sceneTimeBounds.y() &&
            (currentTime < sceneTimeBounds.x()) || (sceneTimeBounds.y() < currentTime))
        {
            currentTime = std::max(sceneTimeBounds.x(), std::min(sceneTimeBounds.y(), currentTime));
            _self.setParameter<float>(parm::currentTime(), currentTime, parm::SKIPS_RESTRAINTS);
        }
    }
}

void
scn::ScenePImpl::handleDescendantTimeBoundsChanged(xchg::ParmEvent::Type type, unsigned int containerId)
{
    if (containerId == _self.id())
        return;

    // update the scene's time bounds if the changed descendant's time bounds modifies its range.
    math::Vector2f sceneTimeBounds(FLT_MAX, -FLT_MAX);

    if (type == xchg::ParmEvent::PARM_REMOVED ||
        hasDescendant(containerId))
        getTimeBounds(sceneTimeBounds); // the scene must recompute its updated time bounds itself
    else
    {
        math::Vector2f descTimeBounds(FLT_MAX, -FLT_MAX);
        scn::TreeNode::Ptr const& desc = _self.descendantById(containerId).lock();

        if (desc && desc->getParameter<math::Vector2f>(parm::timeBounds(), descTimeBounds))
        {
            sceneTimeBounds.x() = std::min(sceneTimeBounds.x(), descTimeBounds.x());
            sceneTimeBounds.y() = std::max(sceneTimeBounds.y(), descTimeBounds.y());
        }
        else
            getTimeBounds(sceneTimeBounds); // the scene must recompute its updated time bounds itself
    }

    if (sceneTimeBounds.x() > sceneTimeBounds.y())
        _self._unsetParameter(parm::timeBounds(), parm::SKIPS_RESTRAINTS);  // unbounded time range
    else
        _self.setParameter<math::Vector2f>(parm::timeBounds(), sceneTimeBounds, parm::SKIPS_RESTRAINTS);    // bounded time range
}

template<typename ValueType>
bool
scn::ScenePImpl::handleIfTreeEntryChanged(const xchg::ParmEvent&    evt,
                                          DAGHierarchy<ValueType>&  tree,
                                          const parm::BuiltIn&      globalParm,
                                          const parm::BuiltIn&      localParm,
                                          const parm::BuiltIn*      inheritsParm)
{
    assert(globalParm.compatibleWith<ValueType>());
    assert(localParm.compatibleWith<ValueType>());
    assert((globalParm.flags() & parm::NEEDS_SCENE) != 0);
    assert((localParm.flags() & parm::NEEDS_SCENE) == 0);
    assert(inheritsParm == nullptr || inheritsParm->compatibleWith<bool>());

    const bool localValueChanged    = evt.parm() == localParm.id();
    const bool inheritsValueChanged = inheritsParm && evt.parm() == inheritsParm->id();

    if (!localValueChanged &&
        !inheritsValueChanged)
        return false;

    if (evt.node() == _self.id())
        return false;   // node is the scene

    // retrieve the descendant's parameter value and
    // update the component used by the scene to compute the global value.
    NodeMap::const_iterator const&  it      = _descendants.find(evt.node());
    TreeNode::Ptr const&            node    = it != _descendants.end() ? it->second.lock() : nullptr;
    if (!node || !tree.has(node->id()))
        return false;

    // update node's local value
    if (localValueChanged)
    {
        ValueType localValue;

        node->getParameter<ValueType>(localParm, localValue);
        tree.setLocal(node->id(), localValue);
    }

    // update node's inheritance rule
    else if (inheritsValueChanged)
    {
        bool inheritsValue;

        node->getParameter<bool>(*inheritsParm, inheritsValue);
        tree.inherits(node->id(), inheritsValue);
    }

    // notify that the global parameter's value have changed
    haveNodeHandleParmEvent(xchg::ParmEvent::PARM_CHANGED, globalParm, *node);

    return true;
}

void
scn::ScenePImpl::haveNodeHandleParmEvent(xchg::ParmEvent::Type  typ,
                                         const parm::BuiltIn&   parm,
                                         scn::TreeNode&         node)
{
    const parm::ParmFlags f     = parm.flags() | parm::EMITTED_BY_NODE | parm::TRAVERSES_HIERARCHY;
    TrackedParmEvent::Ptr evt   = TrackedParmEvent::create(
        xchg::ParmEvent(typ, parm.id(), node.id(), f) );

    assert((evt->flags() & parm::TRAVERSES_LINKS) == 0);
    node.handleAndDeliver(evt);
}

void
scn::ScenePImpl::notifyProgressBeginning(const char* str, size_t totalSteps)
{
    for (size_t i = 0; i < _progressCallbacks.size(); ++i)
        if (_progressCallbacks[i])
            _progressCallbacks[i]->begin(str, totalSteps);
}

bool
scn::ScenePImpl::notifyProgressProceeding(size_t currentStep)
{
    bool proceed = true;
    for (size_t i = 0; i < _progressCallbacks.size(); ++i)
        if (_progressCallbacks[i])
            proceed = _progressCallbacks[i]->proceed(currentStep) && proceed;

    return proceed;
}

void
scn::ScenePImpl::notifyProgressEnd(const char* str)
{
    for (size_t i = 0; i < _progressCallbacks.size(); ++i)
        if (_progressCallbacks[i])
            _progressCallbacks[i]->end(str);
}

void
scn::ScenePImpl::registerProgressCallbacks(xchg::AbstractProgressCallbacks::Ptr const& callbacks)
{
    if (callbacks == nullptr)
        return;
    for (size_t i = 0; i < _progressCallbacks.size(); ++i)
        if (_progressCallbacks[i].get() == callbacks.get())
            return;
    _progressCallbacks.push_back(callbacks);
}

void
scn::ScenePImpl::deregisterProgressCallbacks(xchg::AbstractProgressCallbacks::Ptr const& callbacks)
{
    for (size_t i = 0; i < _progressCallbacks.size(); ++i)
        if (_progressCallbacks[i].get() == callbacks.get())
        {
            std::swap(_progressCallbacks[i], _progressCallbacks.back());
            _progressCallbacks.pop_back();
            return;
        }
}

bool
scn::ScenePImpl::getTimeBounds(math::Vector2f& sceneTimeBounds) const
{
    //////////////////////////
    // class TimeBoundsVisitor
    //////////////////////////
    class TimeBoundsVisitor:
        public TreeNodeVisitor
    {
    public:
        math::Vector2f sceneTimeBounds;
    public:
        inline
        TimeBoundsVisitor():
            TreeNodeVisitor (TreeNodeVisitor::DESCENDING),
            sceneTimeBounds(math::Vector2f(FLT_MAX, -FLT_MAX))
        { }

        inline
        void
        apply(TreeNode& n)
        {
            math::Vector2f objTimeBounds;

            if (n.asSceneObject() &&
                n.getParameter<math::Vector2f>(parm::timeBounds(), objTimeBounds))
            {
                sceneTimeBounds.x() = std::min(sceneTimeBounds.x(), objTimeBounds.x());
                sceneTimeBounds.y() = std::max(sceneTimeBounds.y(), objTimeBounds.y());
            }
            traverse(n);
        }
    };
    /////////////////////////

    TimeBoundsVisitor   visitor;

    _self.accept(visitor);
    sceneTimeBounds = visitor.sceneTimeBounds;

    return !(sceneTimeBounds.x() > sceneTimeBounds.y());
}

void
scn::ScenePImpl::deliver(const xchg::SceneEvent& evt)
{
    assert(!evt.empty());
    for (size_t i = 0; i < _pendingSceneEvents.size(); ++i)
        if (_pendingSceneEvents[i] == evt)
            return; // event being already stored in 'queue'

    _pendingSceneEvents.push_back(evt); // MUTEXME
    if (_pendingSceneEvents.size() > 1) //<! event delivery must be delayed
        return;

    while (!_pendingSceneEvents.empty())
    {
        //----------------------------------
        // notify all registered descendants
        for (NodeMap::iterator it = _descendants.begin(); it != _descendants.end(); ++it)
        {
            TreeNode::Ptr const& desc = it->second.lock();

            if (desc)
                desc->handleSceneEvent(_pendingSceneEvents.front());
        }
        // notify all observers
        for (size_t i = 0; i < _observers.size(); ++i)
        {
            assert(_observers[i]);
            _observers[i]->handle(_self, _pendingSceneEvents.front());
        }
        //----------------------------------
        _pendingSceneEvents.pop_front();    //<! top event has been processed, can pop it now
    }
}

void
scn::ScenePImpl::addNodeLink(unsigned int nodeId, unsigned int parmId)
{
    TreeNode::Ptr const& node = hasDescendant(nodeId)
        ? descendantById(nodeId).lock()
        : nullptr;
    if (node)
        node->addNodeLink(parmId);
}

void
scn::ScenePImpl::removeNodeLink(unsigned int nodeId, unsigned int parmId)
{
    TreeNode::Ptr const& node = hasDescendant(nodeId)
        ? descendantById(nodeId).lock()
        : nullptr;
    if (node)
        node->removeNodeLink(parmId);
}

sys::ResourceDatabase::Ptr const&
scn::ScenePImpl::getOrCreateResourceDatabase()
{
    if (!_resources)
    {
        std::stringstream sstr;
        sstr << "resources <- scene[" << _self.id() << "]";
        _resources = sys::ResourceDatabase::create(sstr.str().c_str());
    }

    return _resources;
}

//////////////
// class Scene
//////////////
// static
scn::Scene::Ptr
scn::Scene::create(const char* json)
{
    Ptr ptr (new Scene(json));
    ptr->build(ptr);
    return ptr;
}

// explicit
scn::Scene::Scene(const char* json):
    TreeNode(),
    _pImpl  (new ScenePImpl(*this, json))
{ }

scn::Scene::~Scene()
{
#ifdef DEBUG_DTOR
    FLIRT_LOG(LOG(DEBUG)
        << "deleting scene '" << name() << "' (ID = " << id() << ")";)
#endif // DEBUG_DTOR

    erase();
    delete _pImpl;
}

// virtual
void
scn::Scene::build(TreeNode::Ptr const& self)
{
    assert(self);

    TreeNode::build(self);
    //---
    _pImpl->build(self);
    registerDescendant(self);
}

const scn::Scene::Config&
scn::Scene::config() const
{
    return _pImpl->config();
}

bool
scn::Scene::hasDescendant(unsigned int id) const
{
    return _pImpl->hasDescendant(id);
}

size_t
scn::Scene::numConnections() const
{
    return _pImpl->numConnections();
}

scn::TreeNode::WPtr const&
scn::Scene::descendantById(unsigned int id) const
{
    return _pImpl->descendantById(id);
}

void
scn::Scene::getIds(xchg::IdSet& ret, const IdGetterOptions* options) const
{
    _pImpl->getIds(ret, options);
}

void
scn::Scene::computeBounds(xchg::BoundingBox& bounds)
{
    _pImpl->computeBounds(this, bounds);
}

IMPL_COMPUTE_GLOBAL_PARM_SPECIALIZATION(char)
IMPL_COMPUTE_GLOBAL_PARM_SPECIALIZATION(size_t)
IMPL_COMPUTE_GLOBAL_PARM_SPECIALIZATION(math::Affine3f)

void
scn::Scene::registerDescendant(TreeNode::Ptr const& n)
{
    _pImpl->registerDescendant(n);
}

void
scn::Scene::deregisterDescendant(unsigned int id)
{
    _pImpl->deregisterDescendant(id);
}

unsigned int
scn::Scene::registerConnection(ParmConnectionBase::Ptr const& cnx)
{
    return _pImpl->registerConnection(cnx);
}

unsigned int
scn::Scene::registerConnectionId(IdConnection::Ptr const& cnx)
{
    return _pImpl->registerConnectionId(cnx);
}

bool
scn::Scene::hasConnection(unsigned int cnxId) const
{
    return _pImpl->hasConnection(cnxId);
}

bool
scn::Scene::hasIdConnection(unsigned int cnxId) const
{
    return _pImpl->hasIdConnection(cnxId);
}

std::shared_ptr<scn::ParmConnectionBase> const&
scn::Scene::connectionById(unsigned int cnxId) const
{
    return _pImpl->connectionById(cnxId);
}

bool
scn::Scene::connectable(unsigned int    inNodeId,
                        const char*     inParmName,
                        unsigned int    outNodeId,
                        const char*     outParmName)
{
    return _pImpl->connectable(inNodeId, inParmName, outNodeId, outParmName);
}

void
scn::Scene::disconnect(unsigned int cnxId)
{
    _pImpl->disconnect(cnxId);
}

void
scn::Scene::disconnectId(unsigned int cnxId)
{
    _pImpl->disconnectId(cnxId);
}

bool
scn::Scene::detectNodeLinkCycle(unsigned int target, unsigned int source) const
{
    return _pImpl->detectNodeLinkCycle(target, source);
}

void
scn::Scene::registerObserver(AbstractSceneObserver::Ptr const& observer)
{
    _pImpl->registerObserver(observer);
}

void
scn::Scene::deregisterObserver(AbstractSceneObserver::Ptr const& observer)
{
    _pImpl->deregisterObserver(observer);
}

// virtual
bool
scn::Scene::isParameterRelevant(unsigned int parmId)const
{
    return isParameterRelevantForClass(parmId) || TreeNode::isParameterRelevant(parmId);
}
bool
scn::Scene::isParameterRelevantForClass(unsigned int parmId) const
{
    return
        parmId == parm::currentTime     ().id() ||
        parmId == parm::selection       ().id() ||
        parmId == parm::framerate       ().id() ||
        parmId == parm::timeBounds      ().id() ||
        parmId == parm::manipulatorFlags().id() ||
        parmId == parm::mouseFlags      ().id();
}

// virtual
bool
scn::Scene::isParameterWritable(unsigned int parmId) const
{
    if (isParameterRelevantForClass(parmId))
        return parmId != parm::timeBounds().id();
    return TreeNode::isParameterWritable(parmId);
}

// virtual
void
scn::Scene::handleParmEvent(const xchg::ParmEvent& evt)
{
    TreeNode::handleParmEvent(evt);
    //---
    _pImpl->handleParmEvent(evt);
}

// virtual
void
scn::Scene::handleSceneEvent(const xchg::SceneEvent& evt)
{
    TreeNode::handleSceneEvent(evt);
    //---
    _pImpl->handleSceneEvent(evt);
}

// virtual
void
scn::Scene::deliver(const xchg::SceneEvent& evt)
{
    _pImpl->deliver(evt);
}

unsigned int
scn::Scene::connectId(unsigned int  inNodeId,
                      const char*   inParmName,
                      unsigned int  outNodeId,
                      const char*   outParmName,
                      int           priority)
{
    return connectable(inNodeId, inParmName, outNodeId, outParmName)
        ? registerConnectionId(IdConnection::create(
            inNodeId, inParmName, outNodeId, outParmName, priority, std::static_pointer_cast<Scene>(self().lock())))
        : 0;
}

bool
scn::Scene::isNameValid(const char* n, int parentId) const
{
    const std::string name(n);

    if (name.empty())
        return false;
    else if (!hasDescendant(parentId))
        return false;

    TreeNode::Ptr const& parent = descendantById(parentId).lock();

    if (!parent)
        return false;

    const unsigned int id = util::string::getId(TreeNode::getFullName(name, parent).c_str());

    return !hasDescendant(id);  // must prevent node ID collisions within a same scene
}

//--------------------------
// external parameter events
//--------------------------
void
scn::Scene::pushExternalEvent(TrackedParmEvent::Ptr const& evt)
{
    _pImpl->pushExternalEvent(evt);
}

bool
scn::Scene::hasExternalEvent() const
{
    return _pImpl->hasExternalEvent();
}

scn::TrackedParmEvent::Ptr
scn::Scene::popExternalEvent()
{
    return _pImpl->popExternalEvent();
}


//-----------
// GUI events
//-----------
void
scn::Scene::handleGUIEvent(const xchg::GUIEvent& evt)
{
    _pImpl->handleGUIEvent(evt);
}

void
scn::Scene::registerGUIEventCallback(xchg::GUIEventType typ, xchg::AbstractGUIEventCallback::Ptr const& callback)
{
    _pImpl->registerGUIEventCallback(typ, callback);
}

void
scn::Scene::deregisterGUIEventCallback(xchg::GUIEventType typ, xchg::AbstractGUIEventCallback::Ptr const& callback)
{
    _pImpl->deregisterGUIEventCallback(typ, callback);
}

//---------
// Progress
//---------
void
scn::Scene::registerProgressCallbacks(xchg::AbstractProgressCallbacks::Ptr const& callbacks)
{
    _pImpl->registerProgressCallbacks(callbacks);
}

void
scn::Scene::deregisterProgressCallbacks(xchg::AbstractProgressCallbacks::Ptr const& callbacks)
{
    _pImpl->deregisterProgressCallbacks(callbacks);
}

void
scn::Scene::notifyProgressBeginning(const char* str, size_t totalSteps)
{
    _pImpl->notifyProgressBeginning(str, totalSteps);
}

bool
scn::Scene::notifyProgressProceeding(size_t currentStep)
{
    return _pImpl->notifyProgressProceeding(currentStep);
}

void
scn::Scene::notifyProgressEnd(const char* str)
{
    _pImpl->notifyProgressEnd(str);
}

//--------------------
// resource management
//--------------------
void
scn::Scene::setSharedResourceDatabase(sys::ResourceDatabase::Ptr const& value)
{
    _pImpl->setSharedResourceDatabase(value);
}

sys::ResourceDatabase::Ptr const&
scn::Scene::getOrCreateResourceDatabase()
{
    return _pImpl->getOrCreateResourceDatabase();
}
