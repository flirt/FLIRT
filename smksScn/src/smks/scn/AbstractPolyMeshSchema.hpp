// ========================================================================= //
// Copyright 2016-2018 SUPAMONKS_STUDIO                                      //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
//                                                                           //
// ========================================================================= //
#pragma once

#include "AbstractObjectSchema.hpp"
#include "BufferAttributes.hpp"

namespace smks
{
    namespace xchg
    {
        struct BoundingBox;
    }
    namespace scn
    {
        class PolyMesh;

        class AbstractPolyMeshSchema:
            public AbstractObjectSchema
        {
        public:
            inline const AbstractPolyMeshSchema*    asPolyMeshSchema() const    { return this; }
            inline AbstractPolyMeshSchema*          asPolyMeshSchema()          { return this; }

            virtual
            size_t
            numVertexBufferSamples() const = 0;

            virtual
            size_t
            numIndexBufferSamples() const = 0;

            virtual
            int
            currentVertexSampleIdx() const = 0;

            virtual
            int
            currentIndexSampleIdx() const = 0;

            virtual
            bool
            has(Property) const = 0;

            ////////////////////////////////
            // vertex buffer related queries
            ////////////////////////////////
            virtual
            const float*
            lockPositions(size_t&) const = 0;

            virtual
            void
            unlockPositions() const = 0;

            virtual
            const float*
            lockNormals(size_t&) const = 0;

            virtual
            void
            unlockNormals() const = 0;

            virtual
            const float*
            lockVelocities(size_t&) const = 0;

            virtual
            void
            unlockVelocities() const = 0;

            virtual
            const float*
            lockTexCoords(size_t&) const = 0; // (u, v)*

            virtual
            void
            unlockTexCoords() const = 0;

            virtual
            void
            getSelfBounds(xchg::BoundingBox&) const = 0;

            ///////////////////////////////
            // index buffer related queries
            ///////////////////////////////
            virtual
            size_t
            getNumVertices() const = 0;

            virtual
            const int*
            lockFaceCounts(size_t&) const = 0;

            virtual
            void
            unlockFaceCounts() const = 0;

            virtual
            const int*
            lockFaceIndices(size_t&) const = 0;

            virtual
            void
            unlockFaceIndices() const = 0;

            virtual
            const unsigned int*
            lockTexCoordsIndices(size_t&) const = 0;

            virtual
            void
            unlockTexCoordsIndices() const = 0;
        };
    }
}
