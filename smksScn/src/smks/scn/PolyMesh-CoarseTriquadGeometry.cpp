// ========================================================================= //
// Copyright 2016-2018 SUPAMONKS_STUDIO                                      //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
//                                                                           //
// ========================================================================= //

#include <smks/sys/Log.hpp>

#include <smks/parm/Container.hpp>

#include "PolyMesh-CoarseTriquadGeometry.hpp"
#include "PolyMesh-GeometrySanitizer.hpp"
#include "AbstractPolyMeshSchema.hpp"

using namespace Alembic;
using namespace smks;

// static
scn::PolyMesh::CoarseTriquadGeometry::Ptr
scn::PolyMesh::CoarseTriquadGeometry::create()
{
    Ptr ptr (new CoarseTriquadGeometry);

    return ptr;
}

scn::PolyMesh::CoarseTriquadGeometry::CoarseTriquadGeometry():
    AbstractGeometry    (),
    _schema             (nullptr),
    _vertexAttribs      (),
    _faceVertexAttribs  (),
    _trisAndQuads       (),
    _wireIndices        (),
    _vertices           (),
    _faceVertices       ()
{ }

scn::PolyMesh::CoarseTriquadGeometry::~CoarseTriquadGeometry()
{
    dispose();
}

// virtual
void
scn::PolyMesh::CoarseTriquadGeometry::dispose()
{
    _schema = nullptr;
    _vertexAttribs      .clear();
    _faceVertexAttribs  .clear();
    _trisAndQuads       .dispose();
    _wireIndices        .dispose();
    _vertices           .dispose();
    _faceVertices       .dispose();
}

// virtual
bool
scn::PolyMesh::CoarseTriquadGeometry::initialize(const AbstractPolyMeshSchema* schema, bool caching)
{
    dispose();
    _schema = schema;
    if (initialized())
    {
        if (!schema->has(Property::PROP_POSITION))
            throw sys::Exception("PolyMesh schema must contain position information.", "GL Geometry Initialization");

        _vertexAttribs.append(Property::PROP_POSITION,  3);
        _vertexAttribs.append(Property::PROP_NORMAL,    3); // will manually compute them

        if (schema->has(Property::PROP_TEXCOORD))
            _faceVertexAttribs.append(Property::PROP_TEXCOORD,  2);

        _trisAndQuads   .initialize(caching ? schema->numIndexBufferSamples()   : 0);
        _wireIndices    .initialize(caching ? schema->numIndexBufferSamples()   : 0);
        _vertices       .initialize(caching ? schema->numVertexBufferSamples()  : 0);
        _faceVertices   .initialize(caching ? schema->numVertexBufferSamples()  : 0);

        return true;
    }
    else
        return false;
}

// virtual
bool
scn::PolyMesh::CoarseTriquadGeometry::initialized() const
{
    return _schema && _schema->numSamples() > 0;
}

const scn::TrianglesAndQuads*
scn::PolyMesh::CoarseTriquadGeometry::getTrianglesAndQuads()
{
    if (!initialized())
        return nullptr;

    const int                       indexSampleId   = _schema->currentIndexSampleIdx();
    const TrianglesAndQuads *const  cache           = _trisAndQuads.get(indexSampleId);
    if (cache)
        return cache;

    // actual computation
    TrianglesAndQuads trisAndQuads;

    const size_t numNgons = GeometrySanitizer::triangulateNgons(
        4,
        _schema,
        trisAndQuads.counts,
        trisAndQuads.indices,
        trisAndQuads.faceVertexToVertex);

    return _trisAndQuads.set(indexSampleId, trisAndQuads);
}

// virtual
const unsigned int*
scn::PolyMesh::CoarseTriquadGeometry::getFaceCounts(size_t& numFaces,
                                                    bool&   isConstant)
{
    numFaces = 0;

    assert(initialized());

    const TrianglesAndQuads* cache = getTrianglesAndQuads();
    if (cache == nullptr ||
        cache->counts.empty())
        return nullptr;

    numFaces = cache->counts.size();

    return numFaces > 0
        ? &cache->counts.front()
        : nullptr;
}

// virtual
const unsigned int*
scn::PolyMesh::CoarseTriquadGeometry::getFaceIndices(size_t& numIndices)
{
    numIndices = 0;

    assert(initialized());

    const TrianglesAndQuads* cache = getTrianglesAndQuads();
    if (cache == nullptr ||
        cache->indices.empty())
        return nullptr;

    numIndices = cache->indices.size();

    return numIndices > 0
        ? &cache->indices.front()
        : nullptr;
}

// virtual
const unsigned int*
scn::PolyMesh::CoarseTriquadGeometry::getFaceVertexToVertexMapping(size_t& numFaceIndices)
{
    numFaceIndices = 0;

    assert(initialized());

    const TrianglesAndQuads* cache = getTrianglesAndQuads();
    if (cache == nullptr ||
        cache->faceVertexToVertex.empty())
        return nullptr;

    numFaceIndices = cache->faceVertexToVertex.size();

    return numFaceIndices > 0
        ? &cache->faceVertexToVertex.front()
        : nullptr;
}

// virtual
const float*
scn::PolyMesh::CoarseTriquadGeometry::getVertices(size_t&           numVertices,
                                                  BufferAttributes& attribs)
{
    numVertices = 0;
    attribs.clear();

    if (!initialized())
        return nullptr;

    attribs             = _vertexAttribs;
    const size_t stride = _vertexAttribs.stride();
    assert(_vertexAttribs.has(Property::PROP_POSITION));
    assert(_vertexAttribs.has(Property::PROP_NORMAL));

    const int                   vertexSampleId  = _schema->currentVertexSampleIdx();
    const std::vector<float>*   cache           = _vertices.get(vertexSampleId);
    if (cache)
    {
        assert(stride > 0 && cache->size() % stride == 0);
        numVertices = cache->size() / stride;
        return numVertices > 0
            ? &cache->front()
            : nullptr;
    }

    const float* iPositions = _schema->lockPositions(numVertices);

    if (numVertices == 0 ||
        !iPositions)
    {
        _schema->unlockPositions();
        return nullptr;
    }

    std::vector<float> vertices(numVertices * attribs.stride(), 0.0f);
    assert(!vertices.empty());

    // copy vertex positions
    float* oPositions   = &vertices.front() + _vertexAttribs.offset(Property::PROP_POSITION);

    for (size_t i = 0; i < numVertices; ++i)
    {
        memcpy(oPositions, iPositions, sizeof(float) * 3);
        iPositions  += 3;
        oPositions  += _vertexAttribs.stride();
    }
    _schema->unlockPositions();


    // compute vertex normals
    float* oNormals = &vertices.front() + _vertexAttribs.offset(Property::PROP_NORMAL);

    GeometrySanitizer::computeVertexNormals(_schema, oNormals, _vertexAttribs.stride());

    // cache data
    _vertices.set(vertexSampleId, vertices);
    cache = _vertices.get(vertexSampleId);
    assert(cache);

    return !cache->empty()
        ? &cache->front()
        : nullptr;
}

// virtual
const float*
scn::PolyMesh::CoarseTriquadGeometry::getFaceVertices(size_t&           numFaceVertices,
                                                      BufferAttributes& attribs)
{
    numFaceVertices = 0;
    attribs.clear();

    if (!initialized())
        return nullptr;

    attribs             = _faceVertexAttribs;
    const size_t stride = _faceVertexAttribs.stride();

    if (stride == 0)
        return nullptr;

    const int                   vertexSampleId  = _schema->currentVertexSampleIdx();
    const std::vector<float>*   cache           = _faceVertices.get(vertexSampleId);
    if (cache)
    {
        assert(stride > 0 && cache->size() % stride == 0);
        numFaceVertices = cache->size() / stride;
        return numFaceVertices > 0
            ? &cache->front()
            : nullptr;
    }

    // get face-varying data to vertex data redirection.
    // can be used to directly retrieve data from ABC since there is no
    // change in terms of vertex reordering (no additional vertices created).
    const TrianglesAndQuads* trisAndQuads = getTrianglesAndQuads();

    if (trisAndQuads == nullptr ||
        trisAndQuads->faceVertexToVertex.empty())
        return nullptr;

    const std::vector<unsigned int>& redir = trisAndQuads->faceVertexToVertex;

    numFaceVertices = redir.size();

    std::vector<float> faceVertices(numFaceVertices * stride, 0.0f);
    assert(!faceVertices.empty());

    if (_faceVertexAttribs.has(Property::PROP_TEXCOORD))
    {
        size_t              numTexCoords    = 0;
        size_t              numTexIndices   = 0;
        const float*        texCoords       = _schema->lockTexCoords(numTexCoords);
        const unsigned int* texIndices      = _schema->lockTexCoordsIndices(numTexIndices);
        const bool          redirTex        = numTexIndices > 0;

        float*              oUvData         = &faceVertices.front() + _faceVertexAttribs.offset(Property::PROP_TEXCOORD);

        for (size_t fvrIndex = 0; fvrIndex < numFaceVertices; ++fvrIndex)
        {
            const size_t texIndex = redirTex
                ? texIndices[redir[fvrIndex]] // texIndices[vtxIndex]
                : redir[fvrIndex]; // vtxIndex

            assert(texIndex < numTexCoords);
            memcpy(oUvData, texCoords + (texIndex << 1), sizeof(float) << 1);
            oUvData += stride;
        }

        _schema->unlockTexCoords();
        _schema->unlockTexCoordsIndices();
    }

    // cache data
    _faceVertices.set(vertexSampleId, faceVertices);
    cache = _faceVertices.get(vertexSampleId);
    assert(cache);

    return !cache->empty()
        ? &cache->front()
        : nullptr;
}

// virtual
const unsigned int *
scn::PolyMesh::CoarseTriquadGeometry::getWireframeLines(size_t& numWireIndices)
{
    numWireIndices = 0;

    if (!initialized())
        return nullptr;

    const int                           indexSampleId   = _schema->currentIndexSampleIdx();
    const std::vector<unsigned int>*    cache           = _wireIndices.get(indexSampleId);
    if (cache)
    {
        assert((cache->size() % 2) == 0);
        numWireIndices = cache->size();
        return numWireIndices > 0
            ? &cache->front()
            : nullptr;
    }

    // actual computation
    std::vector<unsigned int> wireIndices;

    computeWireframeLines(*this, wireIndices);

    // cache computed data
    _wireIndices.set(indexSampleId, wireIndices);
    cache = _wireIndices.get(indexSampleId);
    assert(cache);

    numWireIndices = cache->size();
    return !cache->empty()
        ? &cache->front()
        : nullptr;
}
