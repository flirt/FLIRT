// ========================================================================= //
// Copyright 2016-2018 SUPAMONKS_STUDIO                                      //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
//                                                                           //
// ========================================================================= //
#include <cassert>
#include <iostream>

#include <smks/sys/Exception.hpp>

#include "PolyMesh-TBBComputeVertexNormalKernel.hpp"

using namespace smks;

scn::TBBComputeVertexNormalKernel::TBBComputeVertexNormalKernel(size_t          numFaces,
                                                                const int*      counts,
                                                                const int*      firstIndex,
                                                                const int*      indices,
                                                                const float*    positionBuffer,
                                                                size_t          positionStride,
                                                                float*          oBuffer,
                                                                size_t          oStride):
    _numFaces       (numFaces),
    _count          (0),
    _counts         (counts),
    _firstIndex     (firstIndex),
    _indices        (indices),
    _positionBuffer (positionBuffer),
    _positionStride (positionStride),
    _oBuffer        (oBuffer),
    _oStride        (oStride)
{
    checkKernelCorrectness();
}

scn::TBBComputeVertexNormalKernel::TBBComputeVertexNormalKernel(size_t          numFaces,
                                                                size_t          count,
                                                                const int*      indices,
                                                                const float*    positionBuffer,
                                                                size_t          positionStride,
                                                                float*          oBuffer,
                                                                size_t          oStride):
    _numFaces       (numFaces),
    _count          (count),
    _counts         (nullptr),
    _firstIndex     (nullptr),
    _indices        (indices),
    _positionBuffer (positionBuffer),
    _positionStride (positionStride),
    _oBuffer        (oBuffer),
    _oStride        (oStride)
{
    checkKernelCorrectness();
}

scn::TBBComputeVertexNormalKernel::TBBComputeVertexNormalKernel(const TBBComputeVertexNormalKernel& k):
    _numFaces       (k._numFaces),
    _count          (k._count),
    _counts         (k._counts),
    _firstIndex     (k._firstIndex),
    _indices        (k._indices),
    _positionBuffer (k._positionBuffer),
    _positionStride (k._positionStride),
    _oBuffer        (k._oBuffer),
    _oStride        (k._oStride)
{
    checkKernelCorrectness();
}

void
scn::TBBComputeVertexNormalKernel::checkKernelCorrectness() const
{
    if (_count == 0)
    {
        if (!_counts)
            throw sys::Exception("No valid face count data.", "Vertex Normal Computation TBB Kernel");
        if (!_firstIndex)
            throw sys::Exception("No valid face 1st index data.", "Vertex Normal Computation TBB Kernel");
    }
    if (!_indices)
        throw sys::Exception("No valid face index data.", "Vertex Normal Computation TBB Kernel");
    if (!_positionBuffer)
        throw sys::Exception("No valid position data.", "Vertex Normal Computation TBB Kernel");
    if (_oStride == 0)
        throw sys::Exception("Output vertex stride must be positive.", "Vertex Normal Computation TBB Kernel");
    if (!_oBuffer)
        throw sys::Exception("No valid output buffer.", "Vertex Normal Computation TBB Kernel");
}

void
scn::TBBComputeVertexNormalKernel::operator()(tbb::blocked_range<int> const& r) const
{
    const bool constantCount = _count > 0;

    int count       = 0;
    int firstIndex  = 0;

    // constant face count
    if (constantCount)
    {
        count       = static_cast<int>(_count);
        firstIndex  = static_cast<int>(_count * r.begin());
    }

    for (int f = r.begin(); f < r.end(); ++f)
    {
        float normal[3] = { 0.0f, 0.0f, 0.0f };

        // varying face count
        if (!constantCount)
        {
            count       = _counts[f];
            firstIndex  = _firstIndex[f];
        }

        // compute face normal
        cross(
            normal,
            _positionBuffer + _positionStride * _indices[firstIndex],
            _positionBuffer + _positionStride * _indices[firstIndex + 1],
            _positionBuffer + _positionStride * _indices[firstIndex + 2]);

        // add normal to all vertices of the face
        for (int j = 0; j < count; ++j)
        {
            float* dst = _oBuffer + _indices[firstIndex + j] * _oStride;

            dst[0] += normal[0];
            dst[1] += normal[1];
            dst[2] += normal[2];
        }

        // constant face count
        if (constantCount)
            firstIndex  += static_cast<int>(_count);
    }
}



// static
void
scn::TBBComputeVertexNormalKernel::cross(float *n,
                                         const float *p0,
                                         const float *p1,
                                         const float *p2)
{
    const float a[3] = { p1[0] - p0[0], p1[1] - p0[1], p1[2] - p0[2] };
    const float b[3] = { p0[0] - p2[0], p0[1] - p2[1], p0[2] - p2[2] };

    n[0] = a[1] * b[2] - a[2] * b[1];
    n[1] = a[2] * b[0] - a[0] * b[2];
    n[2] = a[0] * b[1] - a[1] * b[0];

    const float sqrLen  = n[0] * n[0] + n[1] * n[1] + n[2] * n[2];
    const float rcpLen  = sqrLen > 0.0f ? 1.0f / sqrtf(sqrLen) : 0.0f;

    n[0] *= rcpLen;
    n[1] *= rcpLen;
    n[2] *= rcpLen;
}

void
scn::TBBComputeVertexNormalKernel::sequentialRun() const
{
    const bool constantCount = _count > 0;

    int count       = 0;
    int firstIndex  = 0;

    // constant face count
    if (constantCount)
    {
        count       = static_cast<int>(_count);
        firstIndex  = static_cast<int>(_count * 0);
    }

    for (size_t f = 0; f < _numFaces; ++f)
    {
        float normal[3] = { 0.0f, 0.0f, 0.0f };

        // varying face count
        if (!constantCount)
        {
            count       = _counts[f];
            firstIndex  = _firstIndex[f];
        }

        // compute face normal
        cross(
            normal,
            _positionBuffer + _positionStride * _indices[firstIndex],
            _positionBuffer + _positionStride * _indices[firstIndex + 1],
            _positionBuffer + _positionStride * _indices[firstIndex + 2]);

        // add normal to all vertices of the face
        for (int j = 0; j < count; ++j)
        {
            float* dst = _oBuffer + _indices[firstIndex + j] * _oStride;

            dst[0] += normal[0];
            dst[1] += normal[1];
            dst[2] += normal[2];
        }

        // constant face count
        if (constantCount)
            firstIndex  += static_cast<int>(_count);
    }
}
