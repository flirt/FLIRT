// ========================================================================= //
// Copyright 2016-2018 SUPAMONKS_STUDIO                                      //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
//                                                                           //
// ========================================================================= //
#include <sstream>
#include <Alembic/Abc/TypedArraySample.h>

#include <smks/sys/Log.hpp>
#include <smks/sys/Exception.hpp>
#include <smks/parm/BuiltIns.hpp>
#include <smks/parm/BuiltInMap.hpp>
#include <smks/xchg/BoundingBox.hpp>

#include "smks/scn/SchemaBasedSceneObject.hpp"
#include "SchemaBaseFromAbc.hpp"
#include "CurvesSchemaFromAbc.hpp"

namespace smks { namespace scn
{
    static const std::string PNAME_WIDTH    = "width";
    static const std::string PNAME_UV       = "uv";
    static const std::string PNAME_NORMALS  = "N";

    /////////////////////////////////////
    // class CurvesSchemaFromAbc::SampleHolder
    /////////////////////////////////////
    struct CurvesSchemaFromAbc::SampleHolder
    {
        Alembic::Abc::P3fArraySamplePtr     positions;
        Alembic::Abc::Int32ArraySamplePtr   numVertices;
        Alembic::Abc::FloatArraySamplePtr   knots;
        Alembic::Abc::FloatArraySamplePtr   widths;
        Alembic::Abc::N3fArraySamplePtr     scalpNormals;
        Alembic::Abc::V2fArraySamplePtr     scalpTexCoords;

    public:
        inline
        SampleHolder():
            positions       (nullptr),
            numVertices     (nullptr),
            knots           (nullptr),
            widths          (nullptr),
            scalpNormals    (nullptr),
            scalpTexCoords  (nullptr)
        { }

        inline
        ~SampleHolder()
        {
            clear();
        }

        inline
        void
        clear()
        {
            positions       = nullptr;
            numVertices     = nullptr;
            knots           = nullptr;
            widths          = nullptr;
            scalpNormals    = nullptr;
            scalpTexCoords  = nullptr;
        }
    };
    /////////////////////////////////////
}
}

using namespace smks;

scn::CurvesSchemaFromAbc::CurvesSchemaFromAbc(SchemaBasedSceneObject&       node,
                                              const Alembic::Abc::IObject&  object,
                                              TreeNode::WPtr const&         archive):
    AbstractCurvesSchema    (),
    _object                 (object),
    _base                   (new SchemaBaseFromAbc(node, object, archive)),
    _curves                 (),
    _numVertexBufferSamples (0),
    _numIndexBufferSamples  (0),
    _flags                  (0),
    _locks                  (new SampleHolder)
{
    if (!object.valid())
        throw sys::Exception(
            "Incorrect Alembic object parameter.",
            "Alembic Curves Schema Constructor");
    //---
    assert(numSamples() == 0);
}

scn::CurvesSchemaFromAbc::~CurvesSchemaFromAbc()
{
    uninitialize();
    delete _base;
    delete _locks;
}

scn::SchemaBasedSceneObject&
scn::CurvesSchemaFromAbc::node()
{
    return _base->node();
}
const scn::SchemaBasedSceneObject&
scn::CurvesSchemaFromAbc::node() const
{
    return _base->node();
}

const Alembic::AbcGeom::ICurvesSchema&
scn::CurvesSchemaFromAbc::getAbcSchema()
{
    if (_curves.valid())
        return _curves;

    assert(_object.valid());
    if (!Alembic::AbcGeom::ICurves::matches(_object.getMetaData()))
    {
        std::stringstream sstr;
        sstr << "Alembic object '" << _object.getName() << "' is not a curve set.";
        throw sys::Exception(sstr.str().c_str(), "Curves Alembic Schema Getter");
    }

    Alembic::AbcGeom::ICurves curves(_object, Alembic::Abc::kWrapExisting);

    if (!curves.valid())
    {
        std::stringstream sstr;
        sstr << "Failed to get a valid Alembic curve set from '" << _object.getName() << "'.";
        throw sys::Exception(sstr.str().c_str(), "Curves Alembic Schema Getter");
    }

    _curves = curves.getSchema();
    assert(_curves.valid());
    return _curves;
}

void
scn::CurvesSchemaFromAbc::uninitialize()
{
    _curves.reset();
    _numVertexBufferSamples = 0;
    _numIndexBufferSamples  = 0;

    unsetParameter(node(), parm::numVertexBufferSamples(), parm::SKIPS_RESTRAINTS);
    unsetParameter(node(), parm::numIndexBufferSamples(), parm::SKIPS_RESTRAINTS);

    _flags = 0;
    _locks->clear();
    //---
    _base->uninitialize();
    assert(numSamples() == 0);
}

size_t
scn::CurvesSchemaFromAbc::initialize()
{
    if (numSamples() > 0)
        return numSamples();

    uninitialize();

    const Alembic::AbcGeom::ICurvesSchema& curves = getAbcSchema();
    //---
    const size_t numSamples = _base->initialize(curves);
    if (numSamples > 0)
    {
        Alembic::AbcCoreAbstract::TimeSamplingPtr const& refTimeSampling = curves.getTimeSampling();

        if (!curves.getPositionsProperty().valid())
        {
            std::stringstream sstr;
            sstr << "Curves '" << _object.getName() << "' must have a valid positions property.";
            throw sys::Exception(sstr.str().c_str(), "Curves Schema Initialization");
        }

        if (!curves.getNumVerticesProperty().valid())
        {
            std::stringstream sstr;
            sstr << "Curves '" << _object.getName() << "' must have a valid #vertices property.";
            throw sys::Exception(sstr.str().c_str(), "Curves Schema Initialization");
        }

        if (!curves.getKnotsProperty().valid())
        {
            std::stringstream sstr;
            sstr << "Curves '" << _object.getName() << "' must have a valid knots property.";
            throw sys::Exception(sstr.str().c_str(), "Curves Schema Initialization");
        }

        if (curves.getPositionsProperty().getTimeSampling() != refTimeSampling)
        {
            std::stringstream sstr;
            sstr
                << "Curves '" << _object.getName() << "' "
                << "must have its positions property synchronized with its other properties.";
            throw sys::Exception(sstr.str().c_str(), "Curves Schema Initialization");
        }

        const bool isTopoVarying = curves.getTopologyVariance() == Alembic::AbcGeom::kHeterogeneousTopology ||
            curves.getTopologyVariance() == Alembic::AbcGeom::kHeterogenousTopology;

        if (isTopoVarying)
        {
            if (curves.getNumVerticesProperty().getTimeSampling() != refTimeSampling)
            {
                std::stringstream sstr;
                sstr
                    << "Curves '" << _object.getName() << "' "
                    << "must have its vertex counts property synchronized with its other properties.",
                throw sys::Exception(sstr.str().c_str(), "Curves Schema Initialization");
            }

            if (curves.getKnotsProperty().getTimeSampling() != refTimeSampling)
            {
                std::stringstream sstr;
                sstr
                    << "Curves '" << _object.getName() << "' "
                    << "must have its knot property synchronized with its other properties.";
                throw sys::Exception(sstr.str().c_str(), "Curves Schema Initialization");
            }
        }
        assertAllTopologies(curves);
        assert(curves.getPositionsProperty().valid());
        assert(curves.getNumVerticesProperty().valid());
        assert(curves.getKnotsProperty().valid());

        {
            _numVertexBufferSamples = curves.getPositionsProperty().getNumSamples();
            _numIndexBufferSamples  = curves.getTopologyVariance() == Alembic::AbcGeom::kHeterogeneousTopology ||
                curves.getTopologyVariance() == Alembic::AbcGeom::kHeterogenousTopology
                ? curves.getNumVerticesProperty().getNumSamples()
                : 1;
            setParameter<size_t>(node(), parm::numVertexBufferSamples(), _numVertexBufferSamples, parm::SKIPS_RESTRAINTS);
            setParameter<size_t>(node(), parm::numIndexBufferSamples(), _numIndexBufferSamples, parm::SKIPS_RESTRAINTS);
        }

        _flags      = Property::PROP_POSITION;
        if (checkWidths())
            _flags  |= Property::PROP_WIDTH;
        if (checkScalpNormals(curves))
            _flags  |= Property::PROP_SCALP_NORMAL;
        if (checkScalpTexCoords(curves))
            _flags  |= Property::PROP_SCALP_TEXCOORD;
        if (checkBounds(curves))
            _flags  |= Property::PROP_BOUNDS;
    }
    return numSamples;
}

void
scn::CurvesSchemaFromAbc::uninitializeUserAttribs()
{
    _base->uninitializeUserAttribs();
}

void
scn::CurvesSchemaFromAbc::initializeUserAttribs()
{
    _base->initializeUserAttribs(getAbcSchema());
}

//-----------------------------------
// parameter handling
//-----------------------------------
bool
scn::CurvesSchemaFromAbc::isParameterRelevantForClass(unsigned int parmId) const
{
    return
        _base->isParameterRelevantForClass(parmId) ||
        parmId == parm::boundingBox             ().id() ||
        parmId == parm::numVertexBufferSamples  ().id() ||
        parmId == parm::numIndexBufferSamples   ().id() ||
        parmId == parm::glVertices              ().id() ||
        parmId == parm::glPositions             ().id() ||
        parmId == parm::glWidths                ().id() ||
        parmId == parm::glNormals               ().id() ||
        parmId == parm::glTexcoords             ().id() ||
        parmId == parm::glIndices               ().id() ||
        parmId == parm::glCount                 ().id() ||
        parmId == parm::rtVertices              ().id() ||
        parmId == parm::rtPositions             ().id() ||
        parmId == parm::rtWidths                ().id() ||
        parmId == parm::rtScalps                ().id() ||
        parmId == parm::rtNormals               ().id() ||
        parmId == parm::rtTexcoords             ().id() ||
        parmId == parm::rtIndices               ().id() ||
        parmId == parm::rtPathIds               ().id() ||
        parmId == parm::rtCount                 ().id() ||
        parmId == parm::rtTesselationRate       ().id();
}
bool
scn::CurvesSchemaFromAbc::isParameterReadable(unsigned int parmId) const
{
    return _base->isParameterReadable(parmId);
}
char
scn::CurvesSchemaFromAbc::isParameterWritable(unsigned int parmId) const
{
    char ret = -1;
    if (isParameterRelevantForClass(parmId))
    {
        ret = _base->isParameterWritable(parmId);
        if (ret == -1)
            ret = 0;
    }
    return ret;
}
bool
scn::CurvesSchemaFromAbc::overrideParameterDefaultValue(unsigned int parmId, parm::Any& defaultValue) const
{
    return _base->overrideParameterDefaultValue(parmId, defaultValue);
}
void
scn::CurvesSchemaFromAbc::handle(const xchg::ParmEvent& evt)
{
    _base->handle(evt);
}
bool
scn::CurvesSchemaFromAbc::mustSendToScene(const xchg::ParmEvent& evt) const
{
    return _base->mustSendToScene(evt);
}
//-----------------------------------

bool //<! return current visibility
scn::CurvesSchemaFromAbc::setCurrentTime(float time)
{
    return _base->setCurrentTime(time);
}

size_t
scn::CurvesSchemaFromAbc::numSamples() const
{
    return _base->numSamples();
}

int
scn::CurvesSchemaFromAbc::currentSampleIdx() const
{
    return _base->currentSampleIdx();
}

int
scn::CurvesSchemaFromAbc::currentVertexSampleIdx() const
{
    return currentSampleIdx();
}

int
scn::CurvesSchemaFromAbc::currentIndexSampleIdx() const
{
    const int vertexSampleId = currentVertexSampleIdx();

    return vertexSampleId >= 0
        ? (_numIndexBufferSamples > 0
        ? std::min(vertexSampleId, static_cast<int>(_numIndexBufferSamples - 1))
        : 0)
        : -1;
}

void
scn::CurvesSchemaFromAbc::getStoredTimes(xchg::Vector<float>& ret) const
{
    _base->getStoredTimes(ret);
}

bool
scn::CurvesSchemaFromAbc::has(Property p) const
{
    return (_flags & p) != 0;
}

void
scn::CurvesSchemaFromAbc::getBasisAndType(CurveOrder&   order,
                                          bool&         periodic,
                                          BasisType&    basis) const
{
    getBasisAndType(currentIndexSampleIdx(), order, periodic, basis);
}

void
scn::CurvesSchemaFromAbc::getBasisAndType(int           indexSampleId,
                                          CurveOrder&   order,
                                          bool&         periodic,
                                          BasisType&    basis) const
{
    assert(_curves.valid());

    const Alembic::AbcGeom::ICurvesSchema::Sample& sample = _curves.getValue(
        Alembic::Abc::ISampleSelector(static_cast<Alembic::Abc::index_t>(indexSampleId)));

    switch (sample.getType())
    {
    case Alembic::AbcGeom::kCubic:
        order = ORDER_CUBIC;
        break;
    case Alembic::AbcGeom::kLinear:
        order = ORDER_LINEAR;
        break;
    case Alembic::AbcGeom::kVariableOrder:
        order = ORDER_VARIABLE;
        break;
    default:
        {
            std::stringstream sstr;
            sstr << "Undefined curve type for curve set '" << _object.getName() << "'.";
            throw sys::Exception(sstr.str().c_str(), "Curves Basis/Type Getter");
        }
    }

    periodic = sample.getWrap() == Alembic::AbcGeom::kPeriodic;

    switch (sample.getBasis())
    {
    case Alembic::AbcGeom::kNoBasis:
        basis = BasisType::NO_BASIS;
        break;
    case Alembic::AbcGeom::kBezierBasis:
        basis = BasisType::BEZIER_BASIS;
        break;
    case Alembic::AbcGeom::kBsplineBasis:
        basis = BasisType::BSPLINE_BASIS;
        break;
    case Alembic::AbcGeom::kCatmullromBasis:
        basis = BasisType::CATMULLROM_BASIS;
        break;
    case Alembic::AbcGeom::kHermiteBasis:
        basis = BasisType::HERMITE_BASIS;
        break;
    case Alembic::AbcGeom::kPowerBasis:
        basis = BasisType::POWER_BASIS;
        break;
    default:
        {
            std::stringstream sstr;
            sstr << "Undefined basis type for curve set '" << _object.getName() << "'.";
            throw sys::Exception(sstr.str().c_str(), "Curves Basis/Type Getter");
        }
    }
}

const float*
scn::CurvesSchemaFromAbc::lockPositions(size_t& numPositions) const
{
    return lockPositions(_base->currentSampleIdx(), numPositions);
}

const float*
scn::CurvesSchemaFromAbc::lockPositions(int     vertexSampleId,
                                        size_t& numPositions) const
{
    numPositions = 0;

    assert(numSamples() > 0);
    assert(vertexSampleId >= 0 && vertexSampleId < static_cast<int>(numVertexBufferSamples()));
    assert(_curves.getPositionsProperty().valid());

    if (_locks->positions)
    {
        std::stringstream sstr;
        sstr
            << "Previous positions sample has not been properly unlocked "
            << "for curves '" << _object.getName() << "'.";
        throw sys::Exception(sstr.str().c_str(), "Curves Positions Getter");
    }

    Alembic::Abc::P3fArraySamplePtr const& data = _curves.getPositionsProperty()
        .getValue(Alembic::Abc::ISampleSelector(static_cast<Alembic::Abc::index_t>(vertexSampleId)));

    if (data == nullptr ||
        data->size() == 0)
        return nullptr;

    _locks->positions   = data;
    numPositions        = _locks->positions->size();

    return reinterpret_cast<const float*>(_locks->positions->getData());
}

void
scn::CurvesSchemaFromAbc::unlockPositions() const
{
    _locks->positions   = nullptr;
}

const float*
scn::CurvesSchemaFromAbc::lockWidths(size_t& numWidths) const
{
    return lockWidths(_base->currentSampleIdx(), numWidths);
}

const float*
scn::CurvesSchemaFromAbc::lockWidths(int        vertexSampleId,
                                     size_t&    numWidths) const
{
    numWidths = 0;

    assert(numSamples() > 0);
    assert(vertexSampleId >= 0 && vertexSampleId < static_cast<int>(numVertexBufferSamples()));

    if (!has(Property::PROP_WIDTH))
        return nullptr;
    if (_locks->widths)
    {
        std::stringstream sstr;
        sstr
            << "Previous widths sample has not been properly unlocked "
            << "for curves '" << _object.getName() << "'.";
        throw sys::Exception(sstr.str().c_str(), "Curves Widths Getter");
    }

    assert(_curves.getWidthsParam().valid());
    assert(_curves.getWidthsParam().getValueProperty().valid());
    assert(_curves.getWidthsParam().getScope() == Alembic::AbcGeom::kConstantScope ||
        _curves.getWidthsParam().getScope() == Alembic::AbcGeom::kVaryingScope);

    Alembic::Abc::FloatArraySamplePtr const& data = _curves.getWidthsParam().getValueProperty()
        .getValue(Alembic::Abc::ISampleSelector(static_cast<Alembic::Abc::index_t>(vertexSampleId)));

    if (data == nullptr ||
        data->size() == 0)
        return nullptr;

    _locks->widths  = data;
    numWidths       = _locks->widths->size();

    return reinterpret_cast<const float*>(_locks->widths->getData());
}

void
scn::CurvesSchemaFromAbc::unlockWidths() const
{
    _locks->widths = nullptr;
}

void
scn::CurvesSchemaFromAbc::getSelfBounds(xchg::BoundingBox& bounds) const
{
    getSelfBounds(_base->currentSampleIdx(), bounds);
}

void
scn::CurvesSchemaFromAbc::getSelfBounds(int                 vertexSampleId,
                                        xchg::BoundingBox&  bounds) const
{
    bounds.clear();

    assert(numSamples() > 0);
    assert(vertexSampleId >= 0 && vertexSampleId < static_cast<int>(numVertexBufferSamples()));

    if (!has(Property::PROP_BOUNDS))
        return;

    assert(_curves.getSelfBoundsProperty().valid());

    const Alembic::Abc::Box3d& box = _curves.getSelfBoundsProperty()
        .getValue(Alembic::Abc::ISampleSelector(static_cast<Alembic::Abc::index_t>(vertexSampleId)));

    if (box.isEmpty())
        return;

    const Alembic::Abc::V3d& min = box.min;
    const Alembic::Abc::V3d& max = box.max;

    bounds.minX = static_cast<float>(min.x);
    bounds.minY = static_cast<float>(min.y);
    bounds.minZ = static_cast<float>(min.z);
    bounds.maxX = static_cast<float>(max.x);
    bounds.maxY = static_cast<float>(max.y);
    bounds.maxZ = static_cast<float>(max.z);
}

const int*
scn::CurvesSchemaFromAbc::lockNumVertices(size_t& numCurves) const
{
    return lockNumVertices(currentIndexSampleIdx(), numCurves);
}

const int*
scn::CurvesSchemaFromAbc::lockNumVertices(int       indexSampleId,
                                          size_t&   numCurves) const
{
    numCurves = 0;

    assert(numSamples() > 0);
    assert(indexSampleId >= 0 && indexSampleId < static_cast<int>(numIndexBufferSamples()));
    assert(_curves.getNumVerticesProperty().valid());

    if (_locks->numVertices)
    {
        std::stringstream sstr;
        sstr
            << "Previous #vertices sample has not been properly unlocked "
            << "for curves '" << _object.getName() << "'.";
        throw sys::Exception(sstr.str().c_str(), "Curves #vertices Getter");
    }

    Alembic::Abc::Int32ArraySamplePtr const& data = _curves.getNumVerticesProperty()
        .getValue(Alembic::Abc::ISampleSelector(static_cast<Alembic::Abc::index_t>(indexSampleId)));

    if (data == nullptr ||
        data->size() == 0)
        return nullptr;

    _locks->numVertices = data;
    numCurves           = _locks->numVertices->size();

    return reinterpret_cast<const int*>(_locks->numVertices->getData());
}

void
scn::CurvesSchemaFromAbc::unlockNumVertices() const
{
    _locks->numVertices = nullptr;
}

const float*
scn::CurvesSchemaFromAbc::lockKnots(size_t& numKnots) const
{
    return lockKnots(currentIndexSampleIdx(), numKnots);
}

const float*
scn::CurvesSchemaFromAbc::lockKnots(int     indexSampleId,
                                    size_t& numKnots) const
{
    numKnots = 0;

    assert(numSamples() > 0);
    assert(indexSampleId >= 0 && indexSampleId < static_cast<int>(numIndexBufferSamples()));
    assert(_curves.getKnotsProperty().valid());

    if (_locks->knots)
    {
        std::stringstream sstr;
        sstr
            << "Previous knots sample has not been properly unlocked "
            << "for curves '" << _object.getName() << "'.";
        throw sys::Exception(sstr.str().c_str(), "Curves Knots Getter");
    }

    Alembic::Abc::FloatArraySamplePtr const& data = _curves.getKnotsProperty()
        .getValue(Alembic::Abc::ISampleSelector(static_cast<Alembic::Abc::index_t>(indexSampleId)));

    if (data == nullptr ||
        data->size() == 0)
        return nullptr;

    _locks->knots   = data;
    numKnots        = _locks->knots->size();

    return reinterpret_cast<const float*>(_locks->knots->getData());
}

void
scn::CurvesSchemaFromAbc::unlockKnots() const
{
    _locks->knots = nullptr;
}

const float*
scn::CurvesSchemaFromAbc::lockScalpNormals(size_t& numNormals) const
{
    return lockScalpNormals(currentVertexSampleIdx(), numNormals);
}

const float*
scn::CurvesSchemaFromAbc::lockScalpNormals(int      vertexSampleId,
                                           size_t&  numNormals) const
{
    numNormals = 0;

    assert(numSamples() > 0);
    assert(vertexSampleId >= 0 && vertexSampleId < static_cast<int>(numVertexBufferSamples()));

    if (!has(Property::PROP_SCALP_NORMAL))
        return nullptr;
    if (_locks->scalpNormals)
    {
        std::stringstream sstr;
        sstr
            << "Previous scalp normals sample has not been properly unlocked "
            << "for curves '" << _object.getName() << "'.";
        throw sys::Exception(sstr.str().c_str(), "Curves Scalp Normals Getter");
    }

    assert(_curves.getNormalsParam().valid());
    assert(_curves.getNormalsParam().getValueProperty().valid());
    assert(_curves.getNormalsParam().getScope() == Alembic::AbcGeom::kUniformScope);

    Alembic::Abc::N3fArraySamplePtr const& data = _curves.getNormalsParam().getValueProperty()
        .getValue(Alembic::Abc::ISampleSelector(static_cast<Alembic::Abc::index_t>(vertexSampleId)));

    if (data == nullptr ||
        data->size() == 0)
        return nullptr;

    _locks->scalpNormals    = data;
    numNormals              = _locks->scalpNormals->size();

    return reinterpret_cast<const float*>(_locks->scalpNormals->getData());
}

void
scn::CurvesSchemaFromAbc::unlockScalpNormals() const
{
    _locks->scalpNormals = nullptr;
}

const float*
scn::CurvesSchemaFromAbc::lockScalpTexCoords(size_t&    numTexCoords) const
{
    return lockScalpTexCoords(currentVertexSampleIdx(), numTexCoords);
}

const float*
scn::CurvesSchemaFromAbc::lockScalpTexCoords(int        vertexSampleId,
                                             size_t&    numTexCoords) const
{
    numTexCoords = 0;

    assert(numSamples() > 0);
    assert(vertexSampleId >= 0 && vertexSampleId < static_cast<int>(numVertexBufferSamples()));

    if (!has(Property::PROP_SCALP_TEXCOORD))
        return nullptr;
    if (_locks->scalpTexCoords)
    {
        std::stringstream sstr;
        sstr
            << "Previous scalp texcoords sample has not been properly unlocked "
            << "for curves '" << _object.getName() << "'.";
        throw sys::Exception(sstr.str().c_str(), "Curves Scalp TexCoords Getter");
    }
    assert(_curves.getUVsParam().valid());
    assert(_curves.getUVsParam().getValueProperty().valid());
    assert(_curves.getUVsParam().getScope() == Alembic::AbcGeom::kUniformScope);

    Alembic::Abc::V2fArraySamplePtr const& data = _curves.getUVsParam().getValueProperty()
        .getValue(Alembic::Abc::ISampleSelector(static_cast<Alembic::Abc::index_t>(vertexSampleId)));

    if (data == nullptr ||
        data->size() == 0)
        return nullptr;

    _locks->scalpTexCoords  = data;
    numTexCoords            = _locks->scalpTexCoords->size();

    return reinterpret_cast<const float*>(_locks->scalpTexCoords->getData());
}

void
scn::CurvesSchemaFromAbc::unlockScalpTexCoords() const
{
    _locks->scalpTexCoords = nullptr;
}


// static
std::string
scn::CurvesSchemaFromAbc::printInfos(const Alembic::AbcGeom::ICurvesSchema& curves)
{
    if (!curves.valid())
        return std::string();

    std::stringstream sstr;

    sstr
        << "Curves '" << curves.getObject().getFullName() << "'"
        << "\n\t- positions.valid\t? " << curves.getPositionsProperty().valid()
        << "\n\t- numVertices.valid\t? " << curves.getNumVerticesProperty().valid()
        << "\n\t- knots.valid\t? " << curves.getKnotsProperty().valid();
    for (int i = 0; i < static_cast<int>(curves.getNumSamples()); ++i)
    {
        const Alembic::AbcGeom::ICurvesSchema::Sample& sample = curves.getValue(
            Alembic::Abc::ISampleSelector(static_cast<Alembic::Abc::index_t>(i)));

        sstr
            << "\n\t- topology[" << i << "/" << curves.getNumSamples() << "] = { "
            << std::to_string(sample.getType()) << " "
            << std::to_string(sample.getWrap()) << " "
            << std::to_string(sample.getBasis())
            << " }";
    }

    return sstr.str();
}

// static
void
scn::CurvesSchemaFromAbc::assertAllTopologies(const Alembic::AbcGeom::ICurvesSchema& curves)
{
    for (int i = 0; i < static_cast<int>(curves.getNumSamples()); ++i)
    {
        const Alembic::AbcGeom::ICurvesSchema::Sample& sample = curves.getValue(
            Alembic::Abc::ISampleSelector(static_cast<Alembic::Abc::index_t>(i)));

        if (sample.getType() != Alembic::AbcGeom::CurveType::kCubic &&
            sample.getType() != Alembic::AbcGeom::CurveType::kLinear)
        {
            std::stringstream sstr;

            sstr
                << "All samples of curves '" << curves.getObject().getName() << "' "
                << "must either be linear or cubic.";
            throw sys::Exception(sstr.str().c_str(), "Curves Schema Initialization");
        }

        if (sample.getWrap() != Alembic::AbcGeom::CurvePeriodicity::kNonPeriodic)
        {
            std::stringstream sstr;

            sstr
                << "All samples of curves '" << curves.getObject().getName() << "' "
                << "must be non-periodic.";
            throw sys::Exception(sstr.str().c_str(), "Curves Schema Initialization");
        }

        if (sample.getBasis() != Alembic::AbcGeom::BasisType::kBezierBasis &&
            sample.getBasis() != Alembic::AbcGeom::BasisType::kBsplineBasis)
        {
            std::stringstream sstr;

            sstr
                << "All samples of curves '" << curves.getObject().getName() << "' "
                << "must either be Bezier curves or B-splines.";
            throw sys::Exception(sstr.str().c_str(), "Curves Schema Initialization");
        }
    }
}

// static
bool
scn::CurvesSchemaFromAbc::checkBounds(const Alembic::AbcGeom::ICurvesSchema& curves)
{
    if (!curves.getSelfBoundsProperty().valid() ||
        !curves.getPositionsProperty().valid())
        return false;
    else if (curves.getSelfBoundsProperty().getTimeSampling() != curves.getPositionsProperty().getTimeSampling())
        return false;
    else
        return true;
}

// static
bool
scn::CurvesSchemaFromAbc::checkWidths(const Alembic::AbcGeom::ICurvesSchema& curves)
{
    if (!curves.getWidthsParam().valid() ||
        !curves.getPositionsProperty().valid())
        return false;
    else if (curves.getWidthsParam().getTimeSampling() != curves.getPositionsProperty().getTimeSampling())
        return false;
    else if (curves.getWidthsParam().getScope() != Alembic::AbcGeom::GeometryScope::kConstantScope &&
        curves.getWidthsParam().getScope() != Alembic::AbcGeom::GeometryScope::kVaryingScope)
    {
        std::stringstream sstr;
        sstr
            << "Curves '" << curves.getObject().getFullName() << "' "
            << "must have its widths be constant or specified per vertex "
            << "(found " << std::to_string(curves.getWidthsParam().getScope()) << "). "
            << "Skipping widths.";
        FLIRT_LOG(LOG(DEBUG) << sstr.str();)
        std::cerr << sstr.str() << std::endl;

        return false;
    }
    else if (curves.getWidthsParam().getTimeSampling() != curves.getPositionsProperty().getTimeSampling())
    {
        std::stringstream sstr;
        sstr
            << "Curves '" << curves.getObject().getFullName() << "' "
            << "must have its widths synchronized with its positions. "
            << "Skipping widths.";
        FLIRT_LOG(LOG(DEBUG) << sstr.str();)
        std::cerr << sstr.str() << std::endl;

        return false;
    }
    else
        return true;
}

bool
scn::CurvesSchemaFromAbc::checkWidths() const
{
    return checkWidths(_curves);
}

// static
bool
scn::CurvesSchemaFromAbc::checkScalpNormals(const Alembic::AbcGeom::ICurvesSchema& curves)
{
    if (!curves.valid())
        return false;

    else if (!curves.getNormalsParam().valid() ||
        !curves.getPositionsProperty().valid())
        return false;

    else if (curves.getNormalsParam().getScope() != Alembic::AbcGeom::kUniformScope)
    {
        std::stringstream sstr;
        sstr
            << "Curves '" << curves.getObject().getFullName() << "' "
            << "must have its scalp normals specified per curve "
            << "(found " << std::to_string(curves.getNormalsParam().getScope()) << "). "
            << "Skipping scalp normals.";
        FLIRT_LOG(LOG(DEBUG) << sstr.str();)
        std::cerr << sstr.str() << std::endl;

        return false;
    }

    else if (curves.getNormalsParam().getTimeSampling() != curves.getPositionsProperty().getTimeSampling())
    {
        std::stringstream sstr;
        sstr
            << "Curves '" << curves.getObject().getFullName() << "' "
            << "must have its scalp normals synchronized with its positions. "
            << "Skipping scalp normals.";
        FLIRT_LOG(LOG(DEBUG) << sstr.str();)
        std::cerr << sstr.str() << std::endl;

        return false;
    }

    else
        return true;
}

// static
bool
scn::CurvesSchemaFromAbc::checkScalpTexCoords(const Alembic::AbcGeom::ICurvesSchema& curves)
{
    if (!curves.valid())
        return false;

    else if (!curves.getUVsParam().valid() ||
        !curves.getPositionsProperty().valid())
        return false;

    else if (curves.getUVsParam().getScope() != Alembic::AbcGeom::kUniformScope)
    {
        std::stringstream sstr;
        sstr
            << "Curves '" << curves.getObject().getFullName() << "' "
            << "must have its scalp texcoords specified per curve "
            << "(found " << std::to_string(curves.getUVsParam().getScope()) << "). "
            << "Skipping scalp texcoords.";
        FLIRT_LOG(LOG(DEBUG) << sstr.str();)
        std::cerr << sstr.str() << std::endl;

        return false;
    }

    else if (curves.getUVsParam().getTimeSampling() != curves.getPositionsProperty().getTimeSampling())
    {
        std::stringstream sstr;
        sstr
            << "Curves '" << curves.getObject().getFullName() << "' "
            << "must have its scalp texcoords synchronized with its positions. "
            << "Skipping scalp texcoords.";
        FLIRT_LOG(LOG(DEBUG) << sstr.str();)
        std::cerr << sstr.str() << std::endl;

        return false;
    }

    else
        return true;
}
