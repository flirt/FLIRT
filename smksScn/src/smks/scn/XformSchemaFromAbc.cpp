// ========================================================================= //
// Copyright 2016-2018 SUPAMONKS_STUDIO                                      //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
//                                                                           //
// ========================================================================= //
#include <smks/sys/Exception.hpp>
#include <smks/parm/BuiltIn.hpp>
#include <smks/parm/BuiltIns.hpp>

#include "smks/scn/SchemaBasedSceneObject.hpp"
#include "SchemaBaseFromAbc.hpp"
#include "XformSchemaFromAbc.hpp"

using namespace smks;

scn::XformSchemaFromAbc::XformSchemaFromAbc(SchemaBasedSceneObject&         node,
                                            const Alembic::Abc::IObject&    object,
                                            TreeNode::WPtr const&           archive):
    AbstractXformSchema (),
    _object             (object),
    _base               (new SchemaBaseFromAbc(node, object, archive)),
    _xform              ()
{
    if (!object.valid())
        throw sys::Exception("Incorrect Alembic object parameter.", "Alembic Xform Schema Constructor");
    //---
    assert(numSamples() == 0);
}

scn::XformSchemaFromAbc::~XformSchemaFromAbc()
{
    uninitialize();
    delete _base;
}

scn::SchemaBasedSceneObject&
scn::XformSchemaFromAbc::node()
{
    return _base->node();
}
const scn::SchemaBasedSceneObject&
scn::XformSchemaFromAbc::node() const
{
    return _base->node();
}

const Alembic::AbcGeom::IXformSchema&
scn::XformSchemaFromAbc::getAbcSchema()
{
    if (_xform.valid())
        return _xform;

    assert(_object.valid());
    if (!Alembic::AbcGeom::IXform::matches(_object.getMetaData()))
    {
        std::stringstream sstr;
        sstr << "Alembic object '" << _object.getName() << "' is not a xform.";
        throw sys::Exception(sstr.str().c_str(), "XForm Alembic Schema Getter");
    }

    Alembic::AbcGeom::IXform xform(_object, Alembic::Abc::kWrapExisting);

    if (!xform.valid())
    {
        std::stringstream sstr;
        sstr << "Failed to get a valid Alembic xform from '" << _object.getName() << "'.";
        throw sys::Exception(sstr.str().c_str(), "XForm Alembic Schema Getter");
    }

    _xform = xform.getSchema();
    assert(_xform.valid());
    return _xform;
}

void
scn::XformSchemaFromAbc::uninitialize()
{
    _xform.reset();
    //---
    _base->uninitialize();
    assert(numSamples() == 0);
}

size_t
scn::XformSchemaFromAbc::initialize()
{
    if (numSamples() > 0)
        return numSamples();

    uninitialize();

    const Alembic::AbcGeom::IXformSchema& xform = getAbcSchema();
    //---
    const size_t numSamples = _base->initialize(xform);

    return numSamples;
}

void
scn::XformSchemaFromAbc::uninitializeUserAttribs()
{
    _base->uninitializeUserAttribs();
}

void
scn::XformSchemaFromAbc::initializeUserAttribs()
{
    _base->initializeUserAttribs(getAbcSchema());
}

//-----------------------------------
// parameter handling
//-----------------------------------
bool
scn::XformSchemaFromAbc::isParameterRelevantForClass(unsigned int parmId) const
{
    return
        _base->isParameterRelevantForClass(parmId) ||
        parmId == parm::transform().id() ||
        parmId == parm::inheritsTransforms().id();
}
bool
scn::XformSchemaFromAbc::isParameterReadable(unsigned int parmId) const
{
    return _base->isParameterReadable(parmId);
}
char
scn::XformSchemaFromAbc::isParameterWritable(unsigned int parmId) const
{
    char ret = -1;
    if (isParameterRelevantForClass(parmId))
    {
        ret = _base->isParameterWritable(parmId);
        if (ret == -1)
            ret = 0;
    }
    return ret;
}
bool
scn::XformSchemaFromAbc::overrideParameterDefaultValue(unsigned int parmId, parm::Any& defaultValue) const
{
    return _base->overrideParameterDefaultValue(parmId, defaultValue);
}
void
scn::XformSchemaFromAbc::handle(const xchg::ParmEvent& evt)
{
    _base->handle(evt);
}
bool
scn::XformSchemaFromAbc::mustSendToScene(const xchg::ParmEvent& evt) const
{
    return _base->mustSendToScene(evt);
}
//-----------------------------------

bool //<! return current visibility
scn::XformSchemaFromAbc::setCurrentTime(float time)
{
    return _base->setCurrentTime(time);
}

size_t
scn::XformSchemaFromAbc::numSamples() const
{
    return _base->numSamples();
}

int
scn::XformSchemaFromAbc::currentSampleIdx() const
{
    return _base->currentSampleIdx();
}

void
scn::XformSchemaFromAbc::getStoredTimes(xchg::Vector<float>& ret) const
{
    _base->getStoredTimes(ret);
}

void
scn::XformSchemaFromAbc::synchronizeTransform(math::Affine3f&   xfm,
                                              bool&             inheritsXforms)
{
    getTransform(
        _base->currentSampleIdx(),
        xfm, inheritsXforms);
}

void
scn::XformSchemaFromAbc::getTransform(int               sampleId,
                                      math::Affine3f&   xfm,
                                      bool&             inheritsXforms) const
{
    assert(numSamples() > 0);
    assert(sampleId >= 0 && sampleId < static_cast<int>(numSamples()));

    Alembic::AbcGeom::XformSample xformSample;

    _xform.get(xformSample, Alembic::Abc::ISampleSelector(static_cast<Alembic::Abc::index_t>(sampleId)));

    const Imath::M44d&  mat     = xformSample.getMatrix();
    const double*       data    = mat.getValue();

    for (size_t i = 0; i < 16; ++i)
        xfm.data()[i] = static_cast<float>(data[i]);
    inheritsXforms = xformSample.getInheritsXforms();
}
