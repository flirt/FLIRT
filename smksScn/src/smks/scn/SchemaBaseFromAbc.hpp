// ========================================================================= //
// Copyright 2016-2018 SUPAMONKS_STUDIO                                      //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
//                                                                           //
// ========================================================================= //
#pragma once

namespace Alembic {
namespace Abc {
namespace ALEMBIC_VERSION_NS {
    class IObject;
    class ICompoundProperty;
} }
namespace AbcGeom {
namespace ALEMBIC_VERSION_NS {
    class IXformSchema;
    class IPointsSchema;
    class ICurvesSchema;
    class IPolyMeshSchema;
    class ICameraSchema;
} }
}

#include <Alembic/AbcCoreAbstract/TimeSampling.h>
#include "VisibilityFromAbc.hpp"

namespace smks { namespace scn
{
    class TreeNode;
    class UserAttribParmBase;

    class SchemaBaseFromAbc
    {
        friend class AbstractDataHolder;
    private:
        typedef std::weak_ptr<TreeNode>                                 TreeNodeWPtr;
        typedef std::shared_ptr<UserAttribParmBase>                     UserAttribParmPtr;
        typedef boost::unordered_map<unsigned int, UserAttribParmPtr>   UserAttribParmMap;
        typedef Alembic::Abc::ALEMBIC_VERSION_NS::IObject               IObject;
        typedef Alembic::Abc::ALEMBIC_VERSION_NS::ICompoundProperty     ICompoundProperty;
        typedef Alembic::AbcGeom::ALEMBIC_VERSION_NS::IXformSchema      IXformSchema;
        typedef Alembic::AbcGeom::ALEMBIC_VERSION_NS::IPointsSchema     IPointsSchema;
        typedef Alembic::AbcGeom::ALEMBIC_VERSION_NS::ICurvesSchema     ICurvesSchema;
        typedef Alembic::AbcGeom::ALEMBIC_VERSION_NS::IPolyMeshSchema   IPolyMeshSchema;
        typedef Alembic::AbcGeom::ALEMBIC_VERSION_NS::ICameraSchema     ICameraSchema;
    private:
        enum { INVALID_SAMPLE_IDX = -1 };
    private:
        VisibilityFromAbc                           _visibility;
        //<! set at initialization
        Alembic::AbcCoreAbstract::TimeSamplingPtr   _timeSampling;
        size_t                                      _numSamples;        //<! 0 when not initialized
        int                                         _currentSampleIdx;  //<! INVALID_SAMPLE_IDX when not initialized
        UserAttribParmMap                           _userAttribParms;
    public:
        SchemaBaseFromAbc(SchemaBasedSceneObject&,
                          const IObject&,
                          TreeNodeWPtr const& archive);
        ~SchemaBaseFromAbc();

        //-------------------
        // parameter handling
        //-------------------
        bool
        isParameterRelevantForClass(unsigned int) const;
        bool
        isParameterReadable(unsigned int) const;
        char
        isParameterWritable(unsigned int) const;
        bool
        overrideParameterDefaultValue(unsigned int, parm::Any&) const;

        void
        handle(const xchg::ParmEvent&);
        bool
        mustSendToScene(const xchg::ParmEvent&) const;
        //------------------
        // data node content
        //------------------
        void
        uninitialize();

        size_t initialize(const IXformSchema&);
        size_t initialize(const IPointsSchema&);
        size_t initialize(const ICurvesSchema&);
        size_t initialize(const IPolyMeshSchema&);
        size_t initialize(const ICameraSchema&);

        //----------------
        // user attributes
        //----------------
        void
        uninitializeUserAttribs();

        void initializeUserAttribs(const IXformSchema&);
        void initializeUserAttribs(const IPointsSchema&);
        void initializeUserAttribs(const ICurvesSchema&);
        void initializeUserAttribs(const IPolyMeshSchema&);
        void initializeUserAttribs(const ICameraSchema&);
    private:
        void
        initializeUserAttribs(const ICompoundProperty&, const std::string& prefix = "");
        void
        synchronizeUserAttribs(float);

    public:
        //----------------
        // temporal update
        //----------------
        bool
        setCurrentTime(float);

        inline
        size_t
        numSamples() const
        {
            return _numSamples;
        }

        inline
        int
        currentSampleIdx() const
        {
            return _currentSampleIdx;
        }

        void
        getStoredTimes(xchg::Vector<float>&) const;

        inline
        const SchemaBasedSceneObject&
        node() const
        {
            return _visibility.node();
        }
        inline
        SchemaBasedSceneObject&
        node()
        {
            return _visibility.node();
        }

    private:
        int
        getSampleIndex(float) const;
        bool
        getTimeBounds(float&, float&) const;

        void
        updateTimeBounds();
    };
}
}
