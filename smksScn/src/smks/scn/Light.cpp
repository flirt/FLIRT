// ========================================================================= //
// Copyright 2016-2018 SUPAMONKS_STUDIO                                      //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
//                                                                           //
// ========================================================================= //
#include <smks/sys/Log.hpp>
#include <smks/sys/Exception.hpp>
#include <smks/xchg/NodeCode.hpp>
#include <smks/xchg/NodeCodes.hpp>
#include <smks/parm/BuiltIns.hpp>
#include <smks/xchg/ParmEvent.hpp>
#include <smks/xchg/Image.hpp>
#include <smks/math/matrixAlgo.hpp>

#include "smks/scn/Scene.hpp"
#include "smks/scn/Light.hpp"
#include "node_utils.hpp"

namespace smks { namespace scn
{
    static inline
    bool
    cwiseEqual(const math::Vector4f& a, const math::Vector4f& b)
    {
        assert(math::abs(a.w() - b.w()) < 1e-6f);

        return a.cwiseEqual(b).all();
    }

    static inline
    bool
    areDirectionsEqual(const math::Vector4f& a, const math::Vector4f& b)
    {
        assert(math::abs(a.w()) < 1e-6f);
        assert(math::abs(b.w()) < 1e-6f);
        assert(math::abs(a.squaredNorm() - 1.0f) < 1e-3f);
        assert(math::abs(b.squaredNorm() - 1.0f) < 1e-3f);

        return math::abs(a.dot(b) - 1.0f) < 1e-4f;
    }

    static inline
    bool
    equal(char a, char b)
    {
        return a == b;
    }
}
}

using namespace smks;

// static
scn::Light::Ptr
scn::Light::create(const char*              code,
                   const char*              name,
                   TreeNode::WPtr const&    parent)
{
    if (code == nullptr ||
        strlen(code) == 0)
        throw sys::Exception("Empty light code.", "Light Creation");

    Ptr ptr(new Light(name, parent));

    ptr->build(ptr);
    ptr->setParameter<std::string>(parm::code(), code, parm::SKIPS_RESTRAINTS);

    return ptr;
}

void
scn::Light::handleParmEvent(const xchg::ParmEvent& evt)
{
    SceneObject::handleParmEvent(evt);
    //---
    const unsigned int code = getCode();
    if (code == xchg::code::hdri().id())
    {
        if (evt.parm() == parm::filePath().id())
        {
            std::string filePath;
            if ( (evt.type() == xchg::ParmEvent::PARM_ADDED || evt.type() == xchg::ParmEvent::PARM_CHANGED) &&
                getParameter<std::string>(parm::filePath(), filePath) &&
                !filePath.empty() )
            {
                TreeNode::Ptr const&    root    = this->root().lock();
                Scene*                  scene   = root ? root->asScene() : nullptr;
                if (scene)
                {
                    xchg::Image::Ptr const& image = xchg::Image::create(
                        filePath.c_str(),
                        scene->getOrCreateResourceDatabase());
                    xchg::ObjectPtr obj(image);

                    FLIRT_LOG(LOG(DEBUG)
                        << "create image carrier for HDRI light '" << name() << "' "
                        << "(filepath = '" << filePath << "').";)
                    setParameter<xchg::ObjectPtr>(parm::image(), obj, parm::SKIPS_RESTRAINTS);
                }
            }
            else
                _unsetParameter(parm::image(), parm::SKIPS_RESTRAINTS);
        }
    }
}

// virtual
bool
scn::Light::isParameterWritable(unsigned int parmId) const
{
    if (isParameterRelevantForClass(parmId))
        return
            parmId != parm::code    ().id() &&
            parmId != parm::image   ().id();
    return SceneObject::isParameterWritable(parmId);
}

// virtual
bool
scn::Light::isParameterRelevant(unsigned int parmId)const
{
    return isParameterRelevantForClass(parmId) || SceneObject::isParameterRelevant(parmId);
}

bool
scn::Light::isParameterRelevantForClass(unsigned int parmId) const
{
    if (parmId == parm::code        ().id() ||
        parmId == parm::illumMask   ().id())
        return true;

    const unsigned int code = getCode();
    if (code == xchg::code::point().id())
    {
        return parmId == parm::intensity().id();
    }
    else if (code == xchg::code::directional().id())
    {
        return parmId == parm::irradiance().id();
    }
    else if (code == xchg::code::ambient().id())
    {
        return parmId == parm::radiance().id();
    }
    else if (code == xchg::code::distant().id())
    {
        return
            parmId == parm::radiance    ().id() ||
            parmId == parm::halfAngleD  ().id();
    }
    else if (code == xchg::code::spot().id())
    {
        return
            parmId == parm::intensity().id() ||
            parmId == parm::minAngleD().id() ||
            parmId == parm::maxAngleD().id();
    }
    else if (code == xchg::code::hdri().id())
    {
        return
            parmId == parm::filePath    ().id() ||
            parmId == parm::intensity   ().id() ||
            parmId == parm::image       ().id();
    }
    else if (code == xchg::code::mesh().id())
    {
        return
            parmId == parm::radiance().id() ||
            parmId == parm::shapeId ().id();
    }
    return false;
}

