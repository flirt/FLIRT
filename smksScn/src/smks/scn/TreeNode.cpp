// ========================================================================= //
// Copyright 2016-2018 SUPAMONKS_STUDIO                                      //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
//                                                                           //
// ========================================================================= //
#include <iostream>
#include <cassert>
#include <vector>
#include <deque>
#include <algorithm>

#include <smks/sys/Log.hpp>
#include <smks/sys/Exception.hpp>
#include <smks/sys/ResourceDatabase.hpp>
#include <smks/sys/ResourcePathFinder.hpp>
#include <smks/parm/BuiltIns.hpp>
#include <smks/util/string/getId.hpp>
#include <smks/xchg/SceneEvent.hpp>
#include <smks/xchg/ParmEvent.hpp>
#include <smks/xchg/Vector.hpp>
#include <smks/xchg/VectorPtr.hpp>

#include "smks/scn/TreeNode.hpp"
#include "smks/scn/TreeNodeVisitor.hpp"
#include "smks/scn/Scene.hpp"
#include "smks/scn/Archive.hpp"
#include "smks/scn/Camera.hpp"
#include "smks/scn/PolyMesh.hpp"
#include "smks/scn/Curves.hpp"
#include "smks/scn/Points.hpp"
#include "smks/scn/Xform.hpp"
#include "smks/scn/GraphAction.hpp"
#include "smks/scn/Shader.hpp"
#include "smks/scn/RtNode.hpp"
#include "smks/scn/ParmConnections.hpp"
#include "smks/scn/AbstractParmObserver.hpp"
#include "smks/parm/Container.hpp"
#include "custom_getters.hpp"
#include "Tracked.hpp"
#include <std/to_string_xchg.hpp>

namespace smks { namespace scn
{
    static const char SEP('/');
    static const int  INVALID_CHILD_INDEX(-1);

    typedef Tracked<xchg::ParmEvent> TrackedParmEvent;

    ////////////////////////////////////
    // class TreeNode::ContainerObserver
    ////////////////////////////////////
    class TreeNode::ContainerObserver:
        public parm::AbstractObserver
    {
    public:
        typedef std::shared_ptr<ContainerObserver> Ptr;
    private:
        TreeNode& _self;

    public:
        static inline
        Ptr
        create(TreeNode& self)
        {
            Ptr ptr(new ContainerObserver(self));
            return ptr;
        }
    private:
        explicit inline
        ContainerObserver(TreeNode& self):
            parm::AbstractObserver  (),
            _self                   (self)
        { }

        inline
        void
        handle(unsigned int parmId, parm::Event typ)
        {
            xchg::ParmEvent::Type evtTyp = xchg::ParmEvent::UNKNOWN;
            switch (typ)
            {
            case smks::parm::PARM_ADDED:    evtTyp = xchg::ParmEvent::PARM_ADDED;   break;
            case smks::parm::PARM_CHANGED:  evtTyp = xchg::ParmEvent::PARM_CHANGED; break;
            case smks::parm::PARM_REMOVED:  evtTyp = xchg::ParmEvent::PARM_REMOVED; break;
            default:                                                                break;
            }

            // get the parameter descriptive flags from built-in parameters or stored user info
            parm::BuiltIn::Ptr const& builtIn = parm::builtIns().find(parmId);
            parm::ParmFlags f = builtIn
                ? builtIn->flags()
                : _self.getParameterDescriptiveFlags(parmId); // TODO: use parameter flag getter on node

            f = f | parm::EMITTED_BY_NODE | parm::TRAVERSES_LINKS | parm::TRAVERSES_HIERARCHY;
            if (_self.isLink(parmId))
                f = f | parm::IS_ACTIVE_LINK;

            TrackedParmEvent::Ptr evt = TrackedParmEvent::create(
                xchg::ParmEvent(evtTyp, parmId, _self.id(), f));
            //--------------------------
            _self.handleAndDeliver(evt);
            //--------------------------
        }
    };

    ////////////////////////////
    // class TreeNode::ParmStack
    ////////////////////////////
    class TreeNode::ParmStack
    {
    private:
        std::vector<unsigned int> _data;
    public:
        inline
        ParmStack():
            _data()
        {
            _data.reserve(16);
        }

        inline
        void
        push(unsigned int parmId)
        {
            _data.push_back(parmId);
        }

        inline
        bool
        has(unsigned int parmId) const
        {
            for (size_t i = 0; i < _data.size(); ++i)
                if (_data[i] == parmId)
                    return true;
            return false;
        }

        inline
        unsigned int
        top() const
        {
            return !_data.empty() ? _data.back() : 0;
        }

        inline
        void
        pop()
        {
            if (!_data.empty())
                _data.pop_back();
        }
    };

    ////////////////////////
    // class TreeNode::PImpl
    ////////////////////////
    class TreeNode::PImpl
    {
        /////////////////////
        // struct PImpl::Link
        /////////////////////
    private:
        struct Link // indexed by parameter ID
        {
            xchg::IdSet     linkSources;    //<! list of currently linked node IDs
            unsigned int    refCount;       //<! # connections & calls to setter
        public:
            inline
            Link():
                linkSources(), refCount(0)
            { }
        };

        /////////////////////////////////
        // struct PImpl::LinkSourceData
        /////////////////////////////////
    private:
        struct LinkSourceData   // indexed by linked node ID
        {
            xchg::IdSet links;  //<! list of link parameter IDs
        public:
            inline
            LinkSourceData():
                links()
            { }
        };

    private:
        typedef boost::unordered_map<unsigned int, ParmConnections*>        ParmConnectionsMap;
        typedef boost::unordered_map<unsigned int, Link>                    LinkMap;
        typedef boost::unordered_map<unsigned int, LinkSourceData>          LinkSourceDataMap;
        typedef boost::unordered_map<unsigned int, AbstractParmGetter::Ptr> ParmGetterMap;
        typedef boost::unordered_map<unsigned int, parm::ParmFlags>         ParmFlagsMap;

    private:
        TreeNode&                               _self;

        const std::string                       _name;
        const TreeNode::WPtr                    _parent;
        const std::string                       _fullName;  //<! directly used to generate Id
        const unsigned int                      _id;

        TreeNode::WPtr                          _wptr;      //<! weak pointer to self in order to generate smart pointers w/out using enable_shared_from_this
        std::vector<TreeNode::Ptr>              _children;  //<! should be the only STRONG node smart pointers
        TreeNode::WPtr                          _root;
        size_t                                  _depth;

        float                                   _currentTime;
        parm::Container::Ptr                    _parms;

        ContainerObserver::Ptr                  _parmObserver;  //<! direct listener on the parm::Container
        xchg::VectorPtr<AbstractParmObserver>   _observers;     //<! external parameter observers
        LinkMap                                 _links;         //<! link parm ID           -> { linked source node IDs, #refcount }
        LinkSourceDataMap                       _linkSources;   //<! link source node ID    -> { observer, link parm IDs }
        xchg::IdSet                             _linkTargets;

        ParmConnectionsMap                      _parmInConnections;
        ParmConnectionsMap                      _parmOutConnections;

        ParmGetterMap                           _parmGetters;

        ParmStack                               _parmsBeingHandled;
        std::deque<TrackedParmEvent::Ptr>       _pendingParmEvents;
        std::deque<ParmEdit::Ptr>               _delayedParmEdits;

        ParmFlagsMap                            _parmFlags;

    public:
        explicit
        PImpl(TreeNode& self):
            _self               (self),
            _name               (1, SEP),
            _parent             (),
            _fullName           (1, SEP),
            _id                 (util::string::getId(_fullName.c_str())),
            _wptr               (),
            _children           (),
            _root               (),
            _depth              (1),
            _currentTime        (-1.0f),
            _parms              (nullptr),
            _parmObserver       (nullptr),
            _observers          (),
            _links              (),
            _linkSources        (),
            _linkTargets        (),
            _parmInConnections  (),
            _parmOutConnections (),
            _parmGetters        (),
            _parmsBeingHandled  (),
            _pendingParmEvents  (),
            _delayedParmEdits   (),
            _parmFlags          ()
        {
            _children.reserve(8);
        }

        PImpl(TreeNode&             self,
              const char*           name,
              TreeNode::WPtr const& parent):
            _self               (self),
            _name               (cleanName(name)),
            _parent             (fitInHierarchy(_name, parent)  ? parent                                : TreeNode::WPtr()),
            _fullName           (_parent.lock()                 ? TreeNode::getFullName(_name, _parent) : _name),
            _id                 (util::string::getId(_fullName.c_str())),
            _wptr               (),
            _children           (),
            _root               (),
            _depth              (1),
            _currentTime        (-1.0f),
            _parms              (nullptr),
            _parmObserver       (nullptr),
            _observers          (),
            _links              (),
            _linkSources        (),
            _linkTargets        (),
            _parmInConnections  (),
            _parmOutConnections (),
            _parmGetters        (),
            _parmsBeingHandled  (),
            _pendingParmEvents  (),
            _delayedParmEdits   (),
            _parmFlags      ()
        {
            _children.reserve(8);
        }

        inline
        void
        build(TreeNode::Ptr const& self)
        {
            assert(self);
            _wptr = self;

            // make sure the node can be added as a child of the specified parent
            TreeNode::Ptr const& parent = _parent.lock();
            if (parent &&
                parent->_pImpl->childIndex(_id) != INVALID_CHILD_INDEX)
            {
                std::stringstream sstr;
                sstr << "Node '" << self->name() << "' is already a child of node '" << parent->name() << "'.";
                //parent.reset();   // in order to prevent hierarchy corruption for my valid twin node
                FLIRT_LOG(LOG(DEBUG) << sstr.str();)
                throw sys::Exception(sstr.str().c_str(), "Tree Node Hierarchy Update");
            }

            // fit into hierarchy
            updateRoot();
            updateDepth();
            assert(_depth > 0);

            // add me to my parent's children and register me in the scene
            if (parent)
                parent->_pImpl->appendChild(self);  // will register the node to the root scene

            // register custom parameter getters
            setCustomGetter<math::Affine3f> (parm::worldTransform(),        FromSceneParmGetter<math::Affine3f> ::create(self));
            setCustomGetter<char>           (parm::globallyVisible(),       FromSceneParmGetter<char>           ::create(self));
            setCustomGetter<size_t>         (parm::globalNumSamples(),      FromSceneParmGetter<size_t>         ::create(self));
            setCustomGetter<char>           (parm::globalInitialization(),  FromSceneParmGetter<char>           ::create(self));

            deliver(xchg::SceneEvent::nodeCreated(_id, parent ? parent->id() : 0));
        }

        inline
        void
        erase()
        {
            if (_depth == 0)
                return;

            _parmGetters.clear();

            // depth-first traversal for deletion
            while (!_children.empty())
            {
                Ptr firstChild = _children.front(); // keep a reference to the first child

                // erase the shared pointer stored in the children
                std::swap(_children.front(), _children.back());
                _children.pop_back();
                // now wipe the first child out thx to the reference we kept
                destroy(firstChild);
            }

            // deliver deleting signal before all structure is down
            TreeNode::Ptr const& parent = _parent.lock();

            // delete parameter set and deregister from it before delivering node deletion signal
            if (_parms)
            {
                _parms->clear();
                _parms->deregisterObserver(_parmObserver);
            }
            _parms          = nullptr;
            _parmObserver   = nullptr;

            _pendingParmEvents  .clear();
            _observers          .clear();

            //// if node is the source of currently valid links, deliver unlinking events while both
            //// end nodes of those links are still valid (useful for scene clients).
            while (!_linkTargets.empty())
            {
                scn::TreeNode::Ptr const& target = getNodeFromRoot(*_linkTargets.begin());
                if (target)
                    target->_pImpl->closeLinkFrom(_id);
            }

            deliver(xchg::SceneEvent::nodeDeleted(_id, parent ? parent->id() : 0));

            // deregister from the scene if any
            TreeNode::Ptr const& root = _root.lock();
            if (root &&
                root->asScene())
                root->asScene()->deregisterDescendant(_id);

            while (!_parmInConnections.empty())
                undeclareInputParmConnections(_parmInConnections.begin()->first);
            while (!_parmOutConnections.empty())
                undeclareOutputParmConnections(_parmOutConnections.begin()->first);

            while (!_links.empty())
            {
                const unsigned int refCount = _links.begin()->second.refCount;
                for (unsigned int i = 0; i < refCount; ++i)
                    removeNodeLink(_links.begin()->first);
            }
            _links.clear();
            _linkSources.clear();

            _root.reset();
            _depth          = 0;
            _currentTime    = -1.0f;

            _wptr.reset();

            // deregister from my parent's children set
            //<! MUST COME LAST since the parent is the last one owning a valid shared pointer to self
            if (parent)
                parent->_pImpl->deregisterChild(_id);
        }

        inline
        unsigned int
        id() const
        {
            return _id;
        }

        inline
        const std::string&
        fullName() const
        {
            return _fullName;
        }

        inline
        const std::string&
        name() const
        {
            return _name;
        }

        inline
        TreeNode::WPtr const&
        self() const
        {
            return _wptr;
        }

        inline
        unsigned int
        numChildren() const
        {
            return static_cast<unsigned int>(_children.size());
        }

        inline
        TreeNode::Ptr const&
        child(size_t i) const
        {
            assert(i < _children.size());
            return _children[i];
        }

        inline
        TreeNode::WPtr const&
        parent() const
        {
            return _parent;
        }

        inline
        TreeNode::WPtr const&
        root() const
        {
            return _root;
        }

        inline
        size_t
        depth() const
        {
            return _depth;
        }

        inline
        float
        cachedCurrentTime() const
        {
            return _currentTime;
        }

        inline
        void
        currentTime(float value)
        {
            _currentTime = value;
        }

        inline
        parm::Container::Ptr const&
        getOrCreateParameters()
        {
            if (_parms == nullptr)
            {
                _parms          = parm::Container::create();
                _parmObserver   = ContainerObserver::create(_self);

                _parms->registerObserver(_parmObserver);
            }

            return _parms;
        }

        inline
        parm::Container::CPtr
        parameters() const
        {
            return std::const_pointer_cast<parm::Container>(_parms);
        }

        inline
        bool
        getParameterInfo(unsigned int parmId, xchg::ParameterInfo& parmInfo) const
        {
            parmInfo.name       = nullptr;
            parmInfo.typeinfo   = nullptr;
            parmInfo.flags      = parm::NO_PARM_FLAG;

            if (!_parms)
                return false;

            const std::pair<const char*, const std::type_info*>& info = _parms->getInfo(parmId);
            if (info.first == nullptr || info.second == nullptr)
                return false;

            parmInfo.name       = info.first;
            parmInfo.typeinfo   = info.second;

            // WARNING: parameter flags are only stored for user-defined parameter
            parm::BuiltIn::Ptr const& builtIn = parm::builtIns().find(parmId);

            parmInfo.flags = builtIn
                ? builtIn->flags()
                : getParameterDescriptiveFlags(parmId);

            return true;
        }

        inline
        parm::ParmFlags
        getParameterDescriptiveFlags(unsigned int parmId) const
        {
            ParmFlagsMap::const_iterator const& it = _parmFlags.find(parmId);
            return it != _parmFlags.end()
                ? it->second
                : parm::NO_PARM_FLAG;
        }

        inline
        void
        setParameterDescriptiveFlags(unsigned int parmId, parm::ParmFlags f)
        {
            if (parmId &&
                f != parm::NO_PARM_FLAG)
                _parmFlags.insert(
                    std::pair<unsigned int, parm::ParmFlags>(parmId, f & parm::PARM_DESCR_FLAGS));
        }

        // REMOVEME
        inline
        TreeNode::Ptr
        getNodeFromRoot(unsigned int nodeId) const
        {
            Ptr const&      root    = _root.lock();
            const Scene*    scene   = root ? root->asScene() : nullptr;

            return scene && scene->hasDescendant(nodeId)
                ? scene->descendantById(nodeId).lock()
                : nullptr;
        }

        inline
        void
        setCustomGetter(unsigned int parmId, AbstractParmGetter::Ptr const& getter)
        {
            if (getter)
                _parmGetters.insert(std::pair<unsigned int, AbstractParmGetter::Ptr>(parmId, getter));
            else
                _parmGetters.erase(parmId);
        }

        template<typename ValueType> inline
        void
        setCustomGetter(const parm::BuiltIn& parm, std::shared_ptr<TypedParmGetter<ValueType>> const& getter)
        {
            if (!parm.compatibleWith<ValueType>())
            {
                std::stringstream sstr;
                sstr << "Getter Registration For Parameter '" << parm.c_str() << "'";
                throw sys::Exception("Incompatible parameter type.", sstr.str().c_str());
            }

            setCustomGetter(parm.id(), getter);
        }

        inline
        AbstractParmGetter::Ptr
        hasCustomGetter(unsigned int parmId) const
        {
            ParmGetterMap::const_iterator const& it = _parmGetters.find(parmId);

            return it != _parmGetters.end() ? it->second : nullptr;
        }

    private:
        inline
        void
        updateRoot()
        {
            TreeNode::Ptr const& parent = _parent.lock();

            _root = parent
                ? (parent->root().lock() ? parent->root() : _parent)
                : _wptr;

            for (unsigned int i = 0; i < _children.size(); ++i)
                _children[i]->_pImpl->updateRoot();
        }

        inline
        void
        updateDepth()
        {
            _depth = _parent.lock()
                ? _parent.lock()->depth() + 1
                : 1;

            for (unsigned int i = 0; i < _children.size(); ++i)
                _children[i]->_pImpl->updateDepth();
        }

        inline
        void
        appendChild(TreeNode::Ptr const& n)
        {
            if (n)
            {
                // check the child is not already present in order to prevent multiple inclusion
                assert(childIndex(n->id()) == INVALID_CHILD_INDEX);
                _children.push_back(n);

                TreeNode::Ptr const& root = _root.lock();

                if (root &&
                    root->asScene())
                    root->asScene()->registerDescendant(n);
            }
        }

    public:
        inline
        int
        childIndex(unsigned int id)
        {
            for (size_t i = 0; i < _children.size(); ++i)
                if (_children[i]->id() == id)
                    return static_cast<int>(i);
            return INVALID_CHILD_INDEX;
        }

    public:
        inline
        void
        deregisterChild(unsigned int id)
        {
            const int index = childIndex(id);

            if (index != INVALID_CHILD_INDEX)
            {
                std::swap(_children[index], _children.back());
                _children.pop_back();
            }
        }

    public:
        inline
        void
        traverse(TreeNodeVisitor& visitor)
        {
            if (visitor.direction() == TreeNodeVisitor::DESCENDING)
                for (unsigned int i = 0; i < _children.size(); ++i)
                    _children[i]->accept(visitor);
            else
            {
                TreeNode::Ptr const& parent = _parent.lock();

                if (parent)
                    parent->accept(visitor);
            }
        }

        inline
        void
        deliver(const xchg::SceneEvent& evt)
        {
            TreeNode::Ptr const& root = _root.lock();

            if (root &&
                root->asScene())
                root->deliver(evt);
        }

        inline
        void
        declareInputParmConnections(const char* parmName)
        {
            if (parmName == nullptr ||
                strlen(parmName) == 0)
                return;

            TreeNode::Ptr const& self = _wptr.lock();
            assert(self);

            const unsigned int parmId = util::string::getId(parmName);
            if (_parmInConnections.find(parmId) == _parmInConnections.end())
                _parmInConnections.insert(
                    std::pair<unsigned int, ParmConnections*>(
                        parmId,
                        new InputParmConnections(self, parmName)));
        }

        inline
        const ParmConnections*
        getInputParmConnections(unsigned int parmId) const
        {
            ParmConnectionsMap::const_iterator it = _parmInConnections.find(parmId);
            return it != _parmInConnections.end()
                ? it->second
                : nullptr;
        }

        inline
        ParmConnections*
        getInputParmConnections(unsigned int parmId)
        {
            ParmConnectionsMap::iterator it = _parmInConnections.find(parmId);
            return it != _parmInConnections.end()
                ? it->second
                : nullptr;
        }

        inline
        void
        undeclareInputParmConnections(unsigned int parmId)
        {
            ParmConnectionsMap::iterator it = _parmInConnections.find(parmId);
            if (it == _parmInConnections.end())
                return;
            assert(it->second);
            it->second->clear();
            _parmInConnections.erase(it);
        }

        inline
        void
        eraseInputParmConnections(unsigned int parmId)
        {
            TreeNode::Ptr const& root = this->root().lock();

            if (root &&
                root->asScene())
            {
                ParmConnectionsMap::iterator it = _parmInConnections.end();
                while ((it = _parmInConnections.find(parmId)) != _parmInConnections.end() &&
                    it->second &&
                    !it->second->empty())
                {
                    ParmConnectionBase::Ptr const& cnx = it->second->front().lock();
                    if (cnx)
                        root->asScene()->disconnect(cnx->id());
                }
            }
            undeclareInputParmConnections(parmId);
        }

        inline
        void
        eraseInputIdConnections(unsigned int parmId)
        {
            TreeNode::Ptr const& root = this->root().lock();

            if (root &&
                root->asScene())
            {
                ParmConnectionsMap::iterator it = _parmInConnections.end();
                while ((it = _parmInConnections.find(parmId)) != _parmInConnections.end() &&
                    it->second &&
                    !it->second->empty())
                {
                    ParmConnectionBase::Ptr const& cnx = it->second->front().lock();
                    if (cnx)
                        root->asScene()->disconnectId(cnx->id());
                }
            }
            undeclareInputParmConnections(parmId);
        }

        inline
        void
        declareOutputParmConnections(const char* parmName)
        {
            if (parmName == nullptr ||
                strlen(parmName) == 0)
                return;

            TreeNode::Ptr const& self = _wptr.lock();
            assert(self);

            const unsigned int parmId = util::string::getId(parmName);
            if (_parmOutConnections.find(parmId) == _parmOutConnections.end())
                _parmOutConnections.insert(
                    std::pair<unsigned int, ParmConnections*>(
                        parmId,
                        new ParmConnections(self, parmName)));
        }

        inline
        const ParmConnections*
        getOutputParmConnections(unsigned int parmId) const
        {
            ParmConnectionsMap::const_iterator it = _parmOutConnections.find(parmId);
            return it != _parmOutConnections.end()
                ? it->second
                : nullptr;
        }

        inline
        ParmConnections*
        getOutputParmConnections(unsigned int parmId)
        {
            ParmConnectionsMap::iterator it = _parmOutConnections.find(parmId);
            return it != _parmOutConnections.end()
                ? it->second
                : nullptr;
        }

        inline
        void
        undeclareOutputParmConnections(unsigned int parmId)
        {
            ParmConnectionsMap::iterator it = _parmOutConnections.find(parmId);
            if (it == _parmOutConnections.end())
                return;
            assert(it->second);
            it->second->clear();
            _parmOutConnections.erase(it);
        }

        ///////////////
        // node linking
        ///////////////
        inline
        void
        updateParameterId(unsigned int parmId, unsigned int id)
        { }

        inline
        void
        addNodeLink(unsigned int parmId)
        {
            if (parmId == 0)
                return;

            LinkMap::iterator it = _links.find(parmId);
            if (it == _links.end())
                it = _links.insert(std::pair<unsigned int, Link>(parmId, Link())).first;
            assert(it != _links.end());
            ++it->second.refCount;

            updateNodeLink(parmId, true);
        }

        inline
        void
        removeNodeLink(unsigned int parmId)
        {
            updateNodeLink(parmId, false);

            LinkMap::iterator const& it = _links.find(parmId);

            if (it == _links.end())
                return;
            --it->second.refCount;
            if (it->second.refCount == 0)
                _links.erase(it);
        }

        inline
        bool
        detectNodeLinkCycle(unsigned int sourceId)
        {
            assert(sourceId);
            if (sourceId == _id)
                return true;

            TreeNode::Ptr const&    root    = _root.lock();
            const Scene*            scene   = root ? root->asScene() : nullptr;

            return scene->detectNodeLinkCycle(_id, sourceId);
        }

        inline
        void
        link(unsigned int parmId,
             unsigned int sourceId)
        {
            if (parmId == 0 || sourceId == 0)
                return;
            if (detectNodeLinkCycle(sourceId))
            {
                std::stringstream sstr;
                sstr
                    << "WARNING: node link (parameter ID = " << parmId << ") "
                    << "from node ID = " << sourceId << " "
                    << "to node ID = " << _id << " "
                    << "causes a node link cycle and will remain inactive.";
                std::cerr << sstr.str() << std::endl;
                FLIRT_LOG(LOG(DEBUG) << sstr.str();)
                return;
            }

            LinkSourceDataMap::iterator it = _linkSources.find(sourceId);

            if (it == _linkSources.end())
            {
                it = _linkSources.insert(
                    std::pair<unsigned int, LinkSourceData>(sourceId, LinkSourceData())
                ).first;
                openLinkFrom(sourceId);
            }

            assert(it != _linkSources.end());
            xchg::IdSet& links = it->second.links;
            if (links.find(parmId) == links.end())
            {
                links.insert(parmId);
                if (getNodeFromRoot(sourceId))
                    deliver(xchg::SceneEvent::linkAttached(_id, sourceId, parmId));
            }
        }

        inline
        void
        unlink(unsigned int parmId,
               unsigned int sourceId)
        {
            if (parmId      == 0 ||
                sourceId    == 0 ||
                sourceId    == _id)
                return;

            LinkSourceDataMap::iterator const& it = _linkSources.find(sourceId);
            if (it == _linkSources.end())
                return;

            xchg::IdSet& links = it->second.links;
            if (links.find(parmId) != links.end())
            {
                if (getNodeFromRoot(sourceId))
                    deliver(xchg::SceneEvent::linkDetached(_id, sourceId, parmId));
                links.erase(parmId);
            }
            if (links.empty())
            {
                closeLinkFrom(sourceId);
                _linkSources.erase(it);
            }
        }

        inline
        bool
        isLink(unsigned int parmId) const
        {
            return _links.find(parmId) != _links.end();
        }

        inline
        void
        getLinkSources(xchg::IdSet& nodeIds, xchg::NodeClass cl) const
        {
            nodeIds.clear();
            for (LinkSourceDataMap::const_iterator it = _linkSources.begin(); it != _linkSources.end(); ++it)
            {
                scn::TreeNode::Ptr const& source = getNodeFromRoot(it->first);
                if (source && (source->nodeClass() & cl))
                    nodeIds.insert(source->id());
            }
        }

        inline
        void
        getLinkTargets(xchg::IdSet& nodeIds, xchg::NodeClass cl) const
        {
            nodeIds.clear();
            for (xchg::IdSet::const_iterator id = _linkTargets.begin(); id != _linkTargets.end(); ++id)
            {
                scn::TreeNode::Ptr const& target = getNodeFromRoot(*id);
                if (target && (target->nodeClass() & cl))
                    nodeIds.insert(target->id());
            }
            nodeIds = _linkTargets;
        }

        inline
        size_t
        getNumLinksFrom(unsigned int nodeId) const
        {
            LinkSourceDataMap::const_iterator const& it = _linkSources.find(nodeId);

            return it != _linkSources.end()
                ? it->second.links.size()
                : 0;
        }

        inline
        void
        getLinksFrom(unsigned int nodeId, xchg::IdSet& parmIds) const
        {
            LinkSourceDataMap::const_iterator const& it = _linkSources.find(nodeId);
            if (it != _linkSources.end())
                parmIds = it->second.links;
            else
                parmIds.clear();
        }

        inline
        bool
        isLinkFrom(unsigned int parmId, unsigned int nodeId) const
        {
            LinkSourceDataMap::const_iterator const& it = _linkSources.find(nodeId);
            return it != _linkSources.end() &&
                it->second.links.find(parmId) != it->second.links.end();
        }

        size_t
        getNumLinksTo(unsigned int nodeId) const
        {
            scn::TreeNode::Ptr const& target = getNodeFromRoot(nodeId);
            return target ? target->getNumLinksFrom(_id) : 0;
        }

        void
        getLinksTo(unsigned int nodeId, xchg::IdSet& parmIds) const
        {
            scn::TreeNode::Ptr const& target = getNodeFromRoot(nodeId);
            if (target)
                target->getLinksFrom(_id, parmIds);
            else
                parmIds.clear();
        }

        inline
        bool
        isLinkTo(unsigned int parmId, unsigned int nodeId) const
        {
            scn::TreeNode::Ptr const& target = getNodeFromRoot(nodeId);
            return target
                ? target->isLinkFrom(parmId, _id)
                : false;
        }

        void
        declareLinkTarget(unsigned int nodeId)
        {
            if (nodeId)
                _linkTargets.insert(nodeId);
        }

        void
        undeclareLinkTarget(unsigned int nodeId)
        {
            _linkTargets.erase(nodeId);
        }

        bool
        isLinkTarget(unsigned int nodeId) const
        {
            return _linkTargets.find(nodeId) != _linkTargets.end();
        }

        ///////////////////////////////
        // external parameter observers
        ///////////////////////////////
        inline
        void
        registerObserver(ParmObserverPtr const& obs)
        {
            _observers.add(obs);
        }
        inline
        void
        deregisterObserver(ParmObserverPtr const& obs)
        {
            _observers.remove(obs);
        }

        void
        handleAndDeliver(TrackedParmEvent::Ptr const& evt_)
        {
            assert(evt_ && static_cast<bool>(*evt_));

            const bool hadNoDelayedEdits    = _delayedParmEdits.empty();
            const bool hadNoPendingEvents   = _pendingParmEvents.empty();

            TreeNode::Ptr const&    root    = _self.root().lock();
            Scene*                  scene   = root ? root->asScene()        : nullptr;

            if (evt_->node() == _id)
            {
                _parmsBeingHandled.push(evt_->parm());
                FLIRT_LOG(LOG(DEBUG)
                    << "flagged parameter '" << _self.name() << "'." << evt_->parm()
                    << " as being handled.";)
            }

            //----------------------------------------
            _pendingParmEvents.push_back(evt_); // MUTEXME
            //----------------------------------------
            if (hadNoPendingEvents)
            {
                while (!_pendingParmEvents.empty())
                {
                    TrackedParmEvent::Ptr evt = _pendingParmEvents.front();
                    _pendingParmEvents.pop_front();
                    //---
                    // if parameter event needs to be redirected to the root node,
                    // records it to the scene's external parameter event queue
                    // before actually processing it.
                    if (scene &&
                        scene->id() != _id &&           // node is not the root already
                        evt->node() == _id &&           // parameter is owned by the node itself
                        _self.mustSendToScene(*evt))    // parameter must be sent to root according to owning node
                    {
                        scene->pushExternalEvent(evt);
                    }

                    if (!evt->visited(_id))
                    {
                        evt->visits(_id);
                        //---------------
                        // have node handle its own parameter event
                        _self.handleParmEvent(*evt);    //-> can cause parameter event queue to fill up

                        // notifies the node's registered external observers (bindings...)
                        for (size_t i = 0; i < _observers.size(); ++i)
                        {
                            assert(_observers[i]);
                            _observers[i]->handle(_self, *evt); //-> can cause parameter event queue to fill up
                        }
                    }

                    // if requested, propagate parameter event to the node's targets (unless parameter override)
                    if (!_linkTargets.empty() &&
                        (evt->flags() & parm::TRAVERSES_LINKS) != 0)
                    {
                        const parm::ParmFlags f = evt->flags();

                        evt->flags(f & (~parm::EMITTED_BY_NODE) & (~parm::TRAVERSES_HIERARCHY));
                        for (xchg::IdSet::const_iterator trg = _linkTargets.begin(); trg != _linkTargets.end(); ++trg)
                        {
                            if (*trg == evt->node())
                                continue;
                            TreeNode::Ptr const& target = scene->descendantById(*trg).lock();
                            assert(target); // link targets should always exist (contrary to link sources)

                            if (target->parameterExists(evt->parm()))   continue;   // target overrides parameter
                            //---------------------------
                            target->handleAndDeliver(evt);
                            //---------------------------
                        }
                        evt->flags(f);
                    }

                    // if requested, propagate parameter event to the node's children in the hierarchy (unless parameter override)
                    if (!_children.empty() &&
                        (evt->flags() & parm::TRAVERSES_HIERARCHY) != 0)
                    {
                        const parm::ParmFlags f = evt->flags();

                        evt->flags(f & (~parm::EMITTED_BY_NODE) & (~parm::TRAVERSES_LINKS));
                        for (size_t i = 0; i < _children.size(); ++i)
                        {
                            TreeNode::Ptr const& child = _children[i];
                            assert(child);

                            if (child->id() == _id)                     continue;
                            if (child->parameterExists(evt->parm()))    continue;   // child overrides parameter
                            //---------------------------
                            child->handleAndDeliver(evt);
                            //---------------------------
                        };
                        evt->flags(f);
                    }
                }
                // once the parameter event has been handled by the node and sent to observers and children,
                // the root node's external parameter event queue can be processed and emptied.
                if (scene)
                    while (scene->hasExternalEvent())
                    {
                        TrackedParmEvent::Ptr evt = scene->popExternalEvent();
                        //---
                        const parm::ParmFlags f = evt->flags();

                        evt->flags(f & (~parm::EMITTED_BY_NODE) & (~parm::TRAVERSES_LINKS) & (~parm::TRAVERSES_HIERARCHY));
                        //---------------------------
                        root->handleAndDeliver(evt);
                        //---------------------------
                        evt->flags(f);
                    }
            }

            if (evt_->node() == _id)
            {
                assert(_parmsBeingHandled.top() == evt_->parm());
                _parmsBeingHandled.pop();
                FLIRT_LOG(LOG(DEBUG)
                    << "unflagged parameter '" << _self.name() << "'." << evt_->parm()
                    << " as being handled.";)
            }

            // process all delayed parameter edits
            if (hadNoDelayedEdits)
                while (!_delayedParmEdits.empty())
                {
                    ParmEdit::Ptr edit = _delayedParmEdits.front();
                    _delayedParmEdits.pop_front();
                    if (edit)
                        edit->apply(_self);
                }
        }

        inline
        void
        beginParameterHandling(unsigned int parmId)
        {
            _parmsBeingHandled.push(parmId);
        }
        inline
        bool
        isParameterBeingHandled(unsigned int parmId) const
        {
            return _parmsBeingHandled.has(parmId);
        }
        inline
        void
        endParameterHandling(unsigned int parmId)
        {
            // flag the specified parameter as not being handled, and start processing the node's delayed parameter edits
            _parmsBeingHandled.pop();
            while (!_delayedParmEdits.empty())
            {
                ParmEdit::Ptr edit = _delayedParmEdits.front();
                _delayedParmEdits.pop_front();
                if (edit)
                    edit->apply(_self);
            }
        }

    public:
        inline
        void
        delay(ParmEdit::Ptr const& edit)
        {
            if (!edit)
                return;

            FLIRT_LOG(LOG(DEBUG)
                << "delays parameter edit '" << _self.name() << "'."
                << edit->parmId();)

            bool found = false;
            for (size_t i = 0; i < _delayedParmEdits.size(); ++i)
                if (edit->parmId() == _delayedParmEdits[i]->parmId())
                {
                    found = true;
                    break;
                }
            if (found)
            {
                std::stringstream sstr;
                sstr
                    << "WARNING: multiple delayed edits "
                    << "for parameter ID = " << edit->parmId() << " "
                    << "(node = '" << _self.name() << "').";
                FLIRT_LOG(LOG(DEBUG) << sstr.str();)
                std::cerr << sstr.str() << std::endl;
            }

            _delayedParmEdits.push_back(edit);
        }

        /////////////////
        // event handlers
        /////////////////
        inline
        void
        handleParmEvent(const xchg::ParmEvent& evt)
        {
            handleIfLink(evt);
            handleIfSwitchOrTrigger(evt);
        }

        inline
        void
        handleSceneEvent(const xchg::SceneEvent& evt)
        {
            switch (evt.type())
            {
            default:
                break;
            case xchg::SceneEvent::NODE_CREATED:
            case xchg::SceneEvent::NODE_DELETED:
                if (_linkSources.find(evt.node()) != _linkSources.end())
                    handleLinkedNodeCreatedOrDeleted(evt);
                break;
            }
        }

        inline
        bool
        preprocessParameterEdit(const char* parmName, unsigned int parmId, parm::ParmFlags f, const parm::Any& value)
        {
            bool ret = false;
            ret = preprocessIfSwitchOrTrigger(parmName, parmId, f, value) || ret;
            return ret;
        }

    private:
        inline
        void
        handleIfLink(const xchg::ParmEvent& evt)
        {
            if (evt.node() != _id)
                return;

            updateNodeLink(evt.parm(), evt.type() != parm::Event::PARM_REMOVED);
        }

        inline
        void
        updateNodeLink(unsigned int parmId, bool relink)
        {
            // check whether the parameter is a node link, and if so update it.
            LinkMap::iterator const& it = _links.find(parmId);
            if (it == _links.end())
                return;

            const xchg::IdSet&  linkSources = it->second.linkSources;
            xchg::IdSet         newLinkSources;

            if (relink &&
                _parms)
            {
                if (_parms->existsAs<unsigned int>(parmId))
                    newLinkSources  .insert(_parms->get<unsigned int>(parmId));
                else if (_parms->existsAs<xchg::IdSet>(parmId))
                    newLinkSources  = _parms->get<xchg::IdSet>(parmId);
            }

            xchg::IdSet newlyLinked, newlyUnlinked;

            xchg::IdSet::diff(linkSources, newLinkSources, newlyLinked, newlyUnlinked);
            for (xchg::IdSet::const_iterator id = newlyUnlinked.begin(); id != newlyUnlinked.end(); ++id)
                unlink(parmId, *id);
            for (xchg::IdSet::const_iterator id = newlyLinked.begin(); id != newlyLinked.end(); ++id)
                link(parmId, *id);

            it->second.linkSources = newLinkSources;

            //const unsigned int nxtLinkedId = relink && _parms && _parms->existsAs<unsigned int>(parmId)
            //  ? _parms->get<unsigned int>(parmId)
            //  : 0;

            //if (nxtLinkedId != curLinkedId)
            //{
            //  unlink  (parmId, curLinkedId);  // no link removed if not found
            //  link    (parmId, nxtLinkedId);  // no link added if id = 0
            //  it->second.linkSources = nxtLinkedId;
            //}
        }

        inline
        bool
        preprocessIfSwitchOrTrigger(const char* parmName, unsigned int parmId, parm::ParmFlags f, const parm::Any& value)
        {
            if ((f & parm::TRIGGERS_ACTION) == 0 && (f & parm::SWITCHES_STATE) == 0)
                return false;   // nothing needs to be done

            assert(!_self.parameterExists(parmId) || _self.parameterExistsAs<char>(parmId));

            if (f & parm::TRIGGERS_ACTION)
                return false;   // nothing needs to be done

            else if (f & parm::SWITCHES_STATE)
            {
                char                    curState = xchg::INACTIVE;
                char                    nxtState = xchg::INACTIVE;
                const parm::ParmFlags   nxtFlags = f | parm::SKIPS_RESTRAINTS | parm::SKIPS_PREPROCESS;

                _self.getParameter<char>(parmId, curState, xchg::INACTIVE);
                if (!value.empty())
                    nxtState = *parm::Any::cast<char>(&value);
                else
                    _self.getParameterDefault<char>(parmId, nxtState);

                if (nxtState == curState)
                    return false;   // nothing needs to be done
                else if (nxtState == xchg::ACTIVE)
                {
                    _self._setParameter<char>(parmName, parmId, parm::builtIns().find(parmId).get(), xchg::ACTIVATING, nxtFlags);
                    return true;    // skips remaining of current update
                }
                else if (nxtState == xchg::INACTIVE)
                {
                    _self._setParameter<char>(parmName, parmId, parm::builtIns().find(parmId).get(), xchg::DEACTIVATING, nxtFlags);
                    return true;    // skips remaining of current update
                }
            }
            return false;
        }

        inline
        void
        handleIfSwitchOrTrigger(const xchg::ParmEvent& evt)
        {
            if (evt.node() != _id ||
                ((evt.flags() & parm::TRIGGERS_ACTION) == 0 && (evt.flags() & parm::SWITCHES_STATE) == 0))
                return;

            const parm::ParmFlags editFlags = evt.flags() | parm::SKIPS_RESTRAINTS | parm::SKIPS_PREPROCESS;

            // if trigger, then always unset AFTER all observers have performed their action
            if (evt.flags() & parm::TRIGGERS_ACTION)
                delay(ParmUnsetEdit::create(evt.parm(), editFlags));

            // if switch, then always set state AFTER all observers have handled the state transition
            else if (evt.flags() & parm::SWITCHES_STATE)
            {
                char dftState   = xchg::INACTIVE;
                char curState   = xchg::INACTIVE;
                char nxtState   = xchg::INACTIVE;

                _self.getParameterDefault   <char>(evt.parm(), dftState);
                _self.getParameter          <char>(evt.parm(), curState, dftState);
                if (curState == xchg::ACTIVATING)
                {
                    nxtState = xchg::ACTIVE;
                    if (nxtState == dftState)
                        delay(ParmUnsetEdit::create(evt.parm(), editFlags));
                    else
                        delay(ParmUpdateEdit<char>::create(evt.parm(), nxtState, editFlags));
                }
                else if (curState == xchg::DEACTIVATING)
                {
                    nxtState = xchg::INACTIVE;
                    if (nxtState == dftState)
                        delay(ParmUnsetEdit::create(evt.parm(), editFlags));
                    else
                        delay(ParmUpdateEdit<char>::create(evt.parm(), nxtState, editFlags));
                }
            }
        }

    public:
        inline
        void
        deliverEventForAllParmsFrom(unsigned int linkedId, xchg::ParmEvent::Type typ)
        {
            deliverEventForAllParmsFrom(getNodeFromRoot(linkedId), typ);
        }

    private:
        inline
        void
        deliverEventForAllParmsFrom(TreeNode::Ptr const& source, xchg::ParmEvent::Type typ)
        {
            if (!source)
                return;

            const size_t    numKeys = source->parameters()->size();
            unsigned int*   keys    = new unsigned int[numKeys];

            source->parameters()->getKeys(numKeys, keys);
            for (size_t i = 0; i < numKeys; ++i)
            {
                const unsigned int parmId = keys[i];
                if (parmId == 0)
                    continue;

                parm::ParmFlags f = source->getParameterDescriptiveFlags(parmId) | parm::TRAVERSES_LINKS;
                if (source->isLink(parmId))
                    f = f | parm::IS_ACTIVE_LINK;

                TrackedParmEvent::Ptr evt = TrackedParmEvent::create(
                    xchg::ParmEvent(typ, parmId, source->id(), f));

                assert((evt->flags() & parm::EMITTED_BY_NODE) == 0);
                assert((evt->flags() & parm::TRAVERSES_HIERARCHY) == 0);
                //---------------------------
                _self.handleAndDeliver(evt);
                //---------------------------
            }
            delete[] keys;
        }

        inline
        void
        handleLinkedNodeCreatedOrDeleted(const xchg::SceneEvent& evt)
        {
            if (evt.type() == xchg::SceneEvent::NODE_CREATED)
                openLinkFrom(evt.node());
            else if (evt.type() == xchg::SceneEvent::NODE_DELETED)
                closeLinkFrom(evt.node());
        }

        inline
        void
        openLinkFrom(unsigned int sourceId)
        {
            TreeNode::Ptr const& source = getNodeFromRoot(sourceId);
            if (source == nullptr ||
                source->isLinkTarget(_id)) // already done
                return;

            LinkSourceDataMap::iterator const& it = _linkSources.find(source->id());

            source->declareLinkTarget(_id);
            deliver(xchg::SceneEvent::nodesLinked(_id, source->id()));
            deliverEventForAllParmsFrom(source, xchg::ParmEvent::PARM_ADDED);

            if (it != _linkSources.end())
            {
                const xchg::IdSet& links = it->second.links;
                for (xchg::IdSet::const_iterator parm = links.begin(); parm!= links.end(); ++parm)
                    deliver(xchg::SceneEvent::linkAttached(_id, source->id(), *parm));
            }
        }

    public:
        inline
        void
        closeLinkFrom(unsigned int sourceId)
        {
            TreeNode::Ptr const& source = getNodeFromRoot(sourceId);
            if (source == nullptr ||
                !source->isLinkTarget(_id)) // already done
                return;

            LinkSourceDataMap::iterator const& it = _linkSources.find(source->id());

            if (it != _linkSources.end())
            {
                const xchg::IdSet& links = it->second.links;
                for (xchg::IdSet::const_iterator parm = links.begin(); parm!= links.end(); ++parm)
                    deliver(xchg::SceneEvent::linkDetached(_id, source->id(), *parm));
            }

            deliverEventForAllParmsFrom(source, xchg::ParmEvent::PARM_REMOVED);
            deliver(xchg::SceneEvent::nodesUnlinked(_id, source->id()));
            source->undeclareLinkTarget(_id);
        }

    private:
        static inline
        std::string
        cleanName(const std::string& n)
        {
            std::string name(n);

            std::replace(name.begin(), name.end(), SEP, '_');

            return name;
        }
    };
}
}

using namespace smks;

/////////////////
// class TreeNode
/////////////////
// static
bool
scn::TreeNode::fitInHierarchy(const std::string& name, TreeNode::WPtr const& prnt)
{
    TreeNode::Ptr const& parent = prnt.lock();
    if (!parent)
    {
        std::stringstream sstr;
        sstr << "Invalid parent node. '" << name << "' will not fit in node hierarchy.";
        FLIRT_LOG(LOG(DEBUG) << sstr.str();)
        std::cerr << sstr.str() << std::endl;

        return false;
    }

    TreeNode::Ptr const& root = parent->root().lock();

    if (!root || !root->asScene())
    {
        std::stringstream sstr;
        sstr << "Invalid parent's root node. '" << name << "' will not fit in node hierarchy.";
        FLIRT_LOG(LOG(DEBUG) << sstr.str();)
        std::cerr << sstr.str() << std::endl;

        return false;
    }

    const unsigned int      id      = util::string::getId(TreeNode::getFullName(name, parent).c_str());
    TreeNode::Ptr const&    desc    = root->asScene()->hasDescendant(id)
        ? root->asScene()->descendantById(id).lock()
        : nullptr;

    if (desc)
    {
        std::stringstream sstr;
        sstr
            << "ID collision with existing '" << desc->name() << "' node. "
            << "'" << name << "' will not fit in node hierarchy.";
        FLIRT_LOG(LOG(DEBUG) << sstr.str();)
        std::cerr << sstr.str() << std::endl;

        return false;
    }

    return true;
}

// static
scn::TreeNode::Ptr
scn::TreeNode::create(const char* name, TreeNode::WPtr const& parent)
{
    Ptr ptr (new TreeNode(name, parent));
    ptr->build(ptr);
    return ptr;
}

// static
void
scn::TreeNode::destroy(Ptr& node)
{
    if (!node)
        return;
    FLIRT_LOG(LOG(DEBUG)
        << "destroying node '" << node->name() << "' "
        << "(ID = " << node->id() << ", "
        << "type = " << std::to_string(node->nodeClass()) << ")...";)
    node->erase();
    FLIRT_LOG(LOG(DEBUG)
        << "node '" << node->name() << "' "
        << "(ID = " << node->id() << ", "
        << "type = " << std::to_string(node->nodeClass()) << ") destroyed.";)
    node = nullptr;
}

scn::TreeNode::TreeNode():
    _pImpl(new PImpl(*this))
{ }

scn::TreeNode::TreeNode(const char* name, TreeNode::WPtr const& parent):
    _pImpl(new PImpl(*this, name, parent))
{ }

// virtual
scn::TreeNode::~TreeNode()
{
#ifdef DEBUG_DTOR
    FLIRT_LOG(LOG(DEBUG) << "deleting node '" << name() << "' (ID = " << id() << ")";)
#endif // DEBUG_DTOR

    erase();
    delete _pImpl;
}

// virtual
void
scn::TreeNode::erase()
{
    //if (initialized())
    //  uninitialize();
    _pImpl->erase();
}

// virtual
void
scn::TreeNode::build(TreeNode::Ptr const& self)
{
    _pImpl->build(self);
}

unsigned int
scn::TreeNode::id() const
{
    return _pImpl->id();
}

const char*
scn::TreeNode::fullName() const
{
    return _pImpl->fullName().c_str();
}

const char*
scn::TreeNode::name() const
{
    return _pImpl->name().c_str();
}

// static
std::string
scn::TreeNode::getFullName(const std::string&       name,
                           TreeNode::WPtr const&    parent)
{
    if (name.empty())
        throw sys::Exception("No node name specified.", "Node Full Name Getter");

    assert(name.find(SEP) == std::string::npos);

    TreeNode::Ptr const& p = parent.lock();
    if (!p)
        throw sys::Exception("Parent node is invalid.", "Node Full Name Getter");

    const std::string parentFullName (p->fullName());

    assert(!parentFullName.empty());
    return !p->asScene()
        ? parentFullName + SEP + name
        : parentFullName + name;
}

scn::TreeNode::WPtr const&
scn::TreeNode::self() const
{
    return _pImpl->self();
}

unsigned int
scn::TreeNode::numChildren() const
{
    return _pImpl->numChildren();
}

scn::TreeNode::Ptr const&
scn::TreeNode::child(size_t i) const
{
    return _pImpl->child(i);
}

scn::TreeNode::WPtr const&
scn::TreeNode::parent() const
{
    return _pImpl->parent();
}

scn::TreeNode::WPtr const&
scn::TreeNode::root() const
{
    return _pImpl->root();
}

size_t
scn::TreeNode::depth() const
{
    return _pImpl->depth();
}

parm::Container::Ptr const&
scn::TreeNode::parameters()
{
    return _pImpl->getOrCreateParameters();
}

parm::Container::CPtr
scn::TreeNode::parameters() const
{
    return _pImpl->parameters();
}

void
scn::TreeNode::getCurrentParameters(xchg::IdSet& parmIds) const
{
    parmIds.clear();
    if (parameters())
    {
        const size_t    numKeys = parameters()->size();
        unsigned int*   keys    = new unsigned int[numKeys];

        parameters()->getKeys(numKeys, keys);
        for (size_t i = 0; i < numKeys; ++i)
            if (keys[i] > 0)
                parmIds.insert(keys[i]);
        delete[] keys;
    }
}

// virtual
bool
scn::TreeNode::isParameterWritable(unsigned int parmId) const
{
    if (isParameterRelevantForClass(parmId))
        return false;

    parm::BuiltIn::Ptr const& parm = parm::builtIns().find(parmId);
    if (parm)
    {
#if defined(FLIRT_IRRELEVANT_BUILTIN_WRITABLE)
        return true;    // irrelevant built-in parameters writable
#else // defined(FLIRT_IRRELEVANT_BUILTIN_WRITABLE)
        return false;   // irrelevant built-in parameters NOT writable
#endif // defined(FLIRT_IRRELEVANT_BUILTIN_WRITABLE)
    }
    return true; // user-defined parameters are always writable
}

bool
scn::TreeNode::isParameterRelevantForClass(unsigned int parmId) const
{
    return
        parmId == parm::selected            ().id() ||
        parmId == parm::worldTransform      ().id() ||
        parmId == parm::globalNumSamples    ().id() ||
        parmId == parm::globallyVisible     ().id() ||
        parmId == parm::globalInitialization().id();
}

void
scn::TreeNode::getRelevantParameters(xchg::IdSet& parmIds) const
{
    parmIds.clear();
    parm::BuiltIn::Ptr parm = parm::builtIns().begin();
    while (parm)
    {
        if (isParameterRelevant(parm->id()))
            parmIds.insert(parm->id());
        parm = parm::builtIns().next(parm);
    }
}

void
scn::TreeNode::setCustomGetter(unsigned int parmId, AbstractParmGetter::Ptr const& getter)
{
    _pImpl->setCustomGetter(parmId, getter);
}

scn::AbstractParmGetter::Ptr
scn::TreeNode::hasCustomGetter(unsigned int parmId) const
{
    return _pImpl->hasCustomGetter(parmId);
}

bool
scn::TreeNode::parameterExists(unsigned int parmId, unsigned int upToNodeId, Ptr* foundInNode) const
{
    Ptr node = this->self().lock();
    while (true)
    {
        if (!node)
            break;
        if (node->parameters() &&
            node->parameters()->hasKey(parmId))
        {
            if (foundInNode) *foundInNode = node;
            return true;
        }
        if (upToNodeId == 0 || node->id() == upToNodeId)
            break;
        node = node->parent().lock(); // go up the node hierarchy if allowed
    }
    if (foundInNode) *foundInNode = nullptr;
    return false;
}

void
scn::TreeNode::delay(ParmEdit::Ptr const& edit)
{
    _pImpl->delay(edit);
}

// virtual
void
scn::TreeNode::accept(TreeNodeVisitor& visitor)
{
    visitor.apply(*this);
}

// virtual
void
scn::TreeNode::traverse(TreeNodeVisitor& visitor)
{
    _pImpl->traverse(visitor);
}

// virtual
void
scn::TreeNode::handleSceneEvent(const xchg::SceneEvent& evt)
{
    _pImpl->handleSceneEvent(evt);
}

// virtual
bool    //<! returns true if the parameter update has been handled by function
scn::TreeNode::preprocessParameterEdit(const char* parmName, unsigned int parmId, parm::ParmFlags f, const parm::Any& value)
{
    return _pImpl->preprocessParameterEdit(parmName, parmId, f, value);
}

// virtual
void
scn::TreeNode::handleParmEvent(const xchg::ParmEvent& evt)
{
    _pImpl->handleParmEvent(evt);
}

void
scn::TreeNode::registerObserver(AbstractParmObserver::Ptr const& observer)
{
    _pImpl->registerObserver(observer);
}

void
scn::TreeNode::deregisterObserver(AbstractParmObserver::Ptr const& observer)
{
    _pImpl->deregisterObserver(observer);
}

// virtual
void
scn::TreeNode::deliver(const xchg::SceneEvent& evt)
{
    _pImpl->deliver(evt);
}

void
scn::TreeNode::handleAndDeliver(TrackedParmEvent::Ptr const& evt)
{
    _pImpl->handleAndDeliver(evt);
}

void
scn::TreeNode::setParameterDescriptiveFlags(unsigned int parmId, parm::ParmFlags f)
{
    _pImpl->setParameterDescriptiveFlags(parmId, f);
}

parm::ParmFlags
scn::TreeNode::getParameterDescriptiveFlags(unsigned int parmId) const
{
    return  _pImpl->getParameterDescriptiveFlags(parmId);
}

bool
scn::TreeNode::isParameterBeingHandled(unsigned int parmId) const
{
    return _pImpl->isParameterBeingHandled(parmId);
}

scn::TreeNode::Ptr
scn::TreeNode::getNodeFromRoot(unsigned int nodeId) const
{
    return _pImpl->getNodeFromRoot(nodeId);
}

// remove node from the root's current selection
void
scn::TreeNode::deselect()
{
    xchg::IdSet selection;
    Ptr         root = this->root().lock();

    if (root &&
        root->getParameter<xchg::IdSet>(parm::selection(), selection) &&
        selection.find(id()) != selection.end())
    {
        selection.erase(id());
        root->setParameter<xchg::IdSet>(parm::selection(), selection);
    }
}

void
scn::TreeNode::declareInputParmConnections(const char* parmName)
{
    _pImpl->declareInputParmConnections(parmName);
}

const scn::ParmConnections*
scn::TreeNode::getInputParmConnections(unsigned int parmId) const
{
    return _pImpl->getInputParmConnections(parmId);
}

scn::ParmConnections*
scn::TreeNode::getInputParmConnections(unsigned int parmId)
{
    return _pImpl->getInputParmConnections(parmId);
}

void
scn::TreeNode::undeclareInputParmConnections(unsigned int parmId)
{
    _pImpl->undeclareInputParmConnections(parmId);
}

void
scn::TreeNode::declareOutputParmConnections(const char* parmName)
{
    _pImpl->declareOutputParmConnections(parmName);
}

const scn::ParmConnections*
scn::TreeNode::getOutputParmConnections(unsigned int parmId) const
{
    return _pImpl->getOutputParmConnections(parmId);
}

scn::ParmConnections*
scn::TreeNode::getOutputParmConnections(unsigned int parmId)
{
    return _pImpl->getOutputParmConnections(parmId);
}

void
scn::TreeNode::undeclareOutputParmConnections(unsigned int parmId)
{
    _pImpl->undeclareOutputParmConnections(parmId);
}

void
scn::TreeNode::eraseInputParmConnections(unsigned int parmId)
{
    _pImpl->eraseInputParmConnections(parmId);
}

void
scn::TreeNode::eraseInputIdConnections(unsigned int parmId)
{
    _pImpl->eraseInputIdConnections(parmId);
}

void
scn::TreeNode::warnIrrelevantParm(const parm::BuiltIn& parm) const
{
    std::stringstream sstr;
    sstr
        << "WARNING: "
        << "built-in parameter '" << parm.c_str() << "' "
        << "is not relevant for node '" << name() << "' "
        << "(" << std::to_string(nodeClass()) << ").";
    std::cerr << sstr.str() << std::endl;
    FLIRT_LOG(LOG(DEBUG) << sstr.str();)
}

void
scn::TreeNode::warnReadOnlyParm(const char* parmName) const
{
    std::stringstream sstr;
    sstr
        << "WARNING: "
        << "cannot set read-only parameter '" << parmName << "' "
        << "on node '" << name() << "'.";
    std::cerr << sstr.str() << std::endl;
    FLIRT_LOG(LOG(DEBUG) << sstr.str();)
}

void
scn::TreeNode::warnRedundantParmEdit(const char* parmName) const
{
    std::stringstream sstr;
    sstr
        << "WARNING: "
        << "parameter '" << parmName << "' "
        << "of node '" << name() << "' is being handled.";
    std::cerr << sstr.str() << std::endl;
    FLIRT_LOG(LOG(DEBUG) << sstr.str();)
}

void
scn::TreeNode::warnRedundantParmEdit(unsigned int parmId) const
{
    std::stringstream sstr;
    sstr
        << "WARNING: "
        << "parameter ID = " << parmId << " "
        << "of node '" << name() << "' is being handled.";
    std::cerr << sstr.str() << std::endl;
    FLIRT_LOG(LOG(DEBUG) << sstr.str();)
}

void
scn::TreeNode::warnNoDefaultValue(const char* parmName) const
{
    std::stringstream sstr;
    sstr
        << "WARNING: "
        << "failed to get default value for parameter '" << parmName << "' "
        << "on node '" << name() << "'.";
    std::cerr << sstr.str() << std::endl;
    FLIRT_LOG(LOG(DEBUG) << sstr.str();)
}

void
scn::TreeNode::throwIncompatibleParmError(const char* parmName,
                                          const char* errorHeading) const
{
    std::stringstream sstr;
    sstr
        << "Data type clash with parameter '" << parmName << "' "
        << "on node '" << name() << "'.";
    std::cerr << sstr.str() << std::endl;
    FLIRT_LOG(LOG(DEBUG) << sstr.str();)
    throw sys::Exception(sstr.str().c_str(), errorHeading);
}

void
scn::TreeNode::throwIncompatibleParmError(unsigned int parmId,
                                          const char*  errorHeading) const
{
    std::stringstream sstr;
    sstr
        << "Data type clash with parameter ID = " << parmId << " "
        << "on node '" << name() << "'.";
    std::cerr << sstr.str() << std::endl;
    FLIRT_LOG(LOG(DEBUG) << sstr.str();)
    throw sys::Exception(sstr.str().c_str(), errorHeading);
}

void
scn::TreeNode::throwInvalidParmValueError(const char* parmName,
                                          const char* errorHeading) const
{
    std::stringstream sstr;
    sstr
        << "Failed to validate value for parameter '" << parmName << "' "
        << "on node '" << name() << "'.";
    std::cerr << sstr.str() << std::endl;
    FLIRT_LOG(LOG(DEBUG) << sstr.str();)
    throw sys::Exception(sstr.str().c_str(), errorHeading);
}

void
scn::TreeNode::throwUnknownParmError(unsigned int parmId,
                                     const char*  errorHeading) const
{
    std::stringstream sstr;
    sstr
        << "Failed to recognize parameter with ID = " << parmId << " "
        << "on node '" << name() << "': it is neither a built-in, "
        << "nor a stored parameter of the specified type.";
    std::cerr << sstr.str() << std::endl;
    FLIRT_LOG(LOG(DEBUG) << sstr.str();)
    throw sys::Exception(sstr.str().c_str(), errorHeading);
}

void
scn::TreeNode::declareLinkTarget(unsigned int nodeId)
{
    _pImpl->declareLinkTarget(nodeId);
}

void
scn::TreeNode::undeclareLinkTarget(unsigned int nodeId)
{
    _pImpl->undeclareLinkTarget(nodeId);
}

bool
scn::TreeNode::isLinkTarget(unsigned int nodeId) const
{
    return _pImpl->isLinkTarget(nodeId);
}

void
scn::TreeNode::getLinkSources(xchg::IdSet& nodeIds, xchg::NodeClass cl) const
{
    _pImpl->getLinkSources(nodeIds, cl);
}

void
scn::TreeNode::getLinkTargets(xchg::IdSet& nodeIds, xchg::NodeClass cl) const
{
    _pImpl->getLinkTargets(nodeIds, cl);
}

size_t
scn::TreeNode::getNumLinksFrom(unsigned int linkedId) const
{
    return _pImpl->getNumLinksFrom(linkedId);
}

void
scn::TreeNode::getLinksFrom(unsigned int linkedId, xchg::IdSet& parmIds) const
{
    return _pImpl->getLinksFrom(linkedId, parmIds);
}

bool
scn::TreeNode::isLinkFrom(const char* parmName, unsigned int nodeId) const
{
    return _pImpl->isLinkFrom(util::string::getId(parmName), nodeId);
}

bool
scn::TreeNode::isLinkFrom(unsigned int parmId, unsigned int nodeId) const
{
    return _pImpl->isLinkFrom(parmId, nodeId);
}

size_t
scn::TreeNode::getNumLinksTo(unsigned int nodeId) const
{
    return _pImpl->getNumLinksTo(nodeId);
}

void
scn::TreeNode::getLinksTo(unsigned int nodeId, xchg::IdSet& parmIds) const
{
    _pImpl->getLinksTo(nodeId, parmIds);
}

bool
scn::TreeNode::isLinkTo(const char* parmName, unsigned int nodeId) const
{
    return _pImpl->isLinkTo(util::string::getId(parmName), nodeId);
}

bool
scn::TreeNode::isLinkTo(unsigned int parmId, unsigned int nodeId) const
{
    return _pImpl->isLinkTo(parmId, nodeId);
}

bool
scn::TreeNode::isLink(const char* parmName) const
{
    return _pImpl->isLink(util::string::getId(parmName));
}

bool
scn::TreeNode::isLink(unsigned int parmId) const
{
    return _pImpl->isLink(parmId);
}

void
scn::TreeNode::addNodeLink(unsigned int parmId)
{
    _pImpl->addNodeLink(parmId);
}

void
scn::TreeNode::removeNodeLink(unsigned int parmId)
{
    _pImpl->removeNodeLink(parmId);
}

bool
scn::TreeNode::getParameterInfo(unsigned int            parmId,
                                xchg::ParameterInfo&    parmInfo,
                                unsigned int            upToNodeId,
                                Ptr*                    oNode) const
{
    if (oNode)
        *oNode = nullptr;

    Ptr node = self().lock();
    while (true)
    {
        if (!node)
            break;
        if (node->_getParameterInfo(parmId, parmInfo))
        {
            if (oNode)
                *oNode = node;
            return true;
        }
        if (upToNodeId == 0 ||
            node->id() == upToNodeId)
            break;
        node = node->parent().lock(); // go up the node hierarchy if allowed
    }
    return false;
}

bool
scn::TreeNode::_getParameterInfo(unsigned int parmId, xchg::ParameterInfo& parmInfo) const
{
    return _pImpl->getParameterInfo(parmId, parmInfo); //<! only look at calling node instance's content
}

sys::ResourceDatabase::Ptr
scn::TreeNode::getResourceDatabase()
{
    TreeNode::Ptr const& root = this->root().lock();

    return root && root->asScene()
        ? root->asScene()->getOrCreateResourceDatabase()
        : nullptr;
}

sys::ResourcePathFinder::Ptr
scn::TreeNode::getResourcePathFinder()
{
    sys::ResourceDatabase::Ptr const& resources = getResourceDatabase();

    return resources
        ? resources->getOrCreatePathFinder()
        : nullptr;
}

unsigned int
scn::TreeNode::getCode() const
{
    std::string code;
    return getParameter<std::string>(parm::code(), code)
        ? util::string::getId(code.c_str())
        : 0;
}

void
scn::TreeNode::getStoredTimes(xchg::Vector<float>& ret) const
{
    ret.clear();
}
