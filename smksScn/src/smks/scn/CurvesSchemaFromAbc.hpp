// ========================================================================= //
// Copyright 2016-2018 SUPAMONKS_STUDIO                                      //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
//                                                                           //
// ========================================================================= //
#pragma once

#include <Alembic/AbcGeom/ICurves.h>

#include "AbstractCurvesSchema.hpp"

namespace smks { namespace scn
{
    class TreeNode;
    class SchemaBasedSceneObject;
    class SchemaBaseFromAbc;

    class CurvesSchemaFromAbc:
        public AbstractCurvesSchema
    {
    private:
        struct SampleHolder;
    private:
        const Alembic::Abc::IObject             _object;    //<! raw object
        SchemaBaseFromAbc*                      _base;
        Alembic::AbcGeom::ICurvesSchema         _curves;    //<! typed object's schema
        size_t                                  _numVertexBufferSamples;
        size_t                                  _numIndexBufferSamples;
        PropertyFlags                           _flags;
        SampleHolder*                           _locks;

    public:
        CurvesSchemaFromAbc(SchemaBasedSceneObject&,
                            const Alembic::Abc::IObject&,
                            std::weak_ptr<TreeNode> const& archive);
        ~CurvesSchemaFromAbc();

    protected:
        const SchemaBasedSceneObject&
        node() const;
        SchemaBasedSceneObject&
        node();

        inline
        bool
        lazyInitialization() const
        {
            return true;
        }
        void
        uninitialize();
        size_t  //<! return number of samples
        initialize();

        void
        uninitializeUserAttribs();
        void
        initializeUserAttribs();
    private:
        const Alembic::AbcGeom::ICurvesSchema&
        getAbcSchema();
    public:
        //-------------------
        // parameter handling
        //-------------------
        bool
        isParameterRelevantForClass(unsigned int) const;
        bool
        isParameterReadable(unsigned int) const;
        char
        isParameterWritable(unsigned int) const;
        bool
        overrideParameterDefaultValue(unsigned int, parm::Any&) const;
        void
        handle(const xchg::ParmEvent&);
        bool
        mustSendToScene(const xchg::ParmEvent&) const;
        //-------------------

        bool //<! return current visibility
        setCurrentTime(float);

        size_t
        numSamples() const;

        inline
        size_t
        numVertexBufferSamples() const
        {
            return _numVertexBufferSamples;
        }

        inline
        size_t
        numIndexBufferSamples() const
        {
            return _numIndexBufferSamples;
        }

        int
        currentSampleIdx() const;

        int
        currentVertexSampleIdx() const;

        int
        currentIndexSampleIdx() const;

        void
        getStoredTimes(xchg::Vector<float>&) const;

        bool
        has(Property) const;

        void
        getBasisAndType(CurveOrder&, bool& isPeriodic, BasisType&) const;

        ////////////////
        // vertex buffer
        ////////////////
        const float*
        lockPositions(size_t&) const;

        void
        unlockPositions() const;

        const float*
        lockWidths(size_t&) const;

        void
        unlockWidths() const;

        void
        getSelfBounds(xchg::BoundingBox&) const;

        ///////////
        // topology
        ///////////
        const int*
        lockNumVertices(size_t&) const;

        void
        unlockNumVertices() const;

        const float*
        lockKnots(size_t&) const;

        void
        unlockKnots() const;

        ////////////////////
        // scalp information
        ////////////////////
        const float*
        lockScalpNormals(size_t&) const;

        void
        unlockScalpNormals() const;

        const float*
        lockScalpTexCoords(size_t&) const;

        void
        unlockScalpTexCoords() const;

    private:
        void
        getBasisAndType(int indexSampleId, CurveOrder&, bool& periodic, BasisType&) const;

        ////////////////
        // vertex buffer
        ////////////////
        const float*
        lockPositions(int vertexSampleId, size_t&) const;

        const float*
        lockWidths(int vertexSampleId, size_t&) const;

        void
        getSelfBounds(int vertexSampleId, xchg::BoundingBox&) const;

        ///////////
        // topology
        ///////////
        const int*
        lockNumVertices(int indexSampleId, size_t&) const;

        const float*
        lockKnots(int indexSampleId, size_t&) const;

        ////////////////////
        // scalp information
        ////////////////////
        const float*
        lockScalpNormals(int vertexSampleId, size_t&) const;

        const float*
        lockScalpTexCoords(int vertexSampleId, size_t&) const;

    private:
        static
        std::string
        printInfos(const Alembic::AbcGeom::ICurvesSchema&);

        static
        void
        assertAllTopologies(const Alembic::AbcGeom::ICurvesSchema&);

        static
        bool
        checkBounds(const Alembic::AbcGeom::ICurvesSchema&);

        static
        bool
        checkWidths(const Alembic::AbcGeom::ICurvesSchema&);

        bool
        checkWidths() const;

        static
        bool
        checkScalpNormals(const Alembic::AbcGeom::ICurvesSchema&);

        static
        bool
        checkScalpTexCoords(const Alembic::AbcGeom::ICurvesSchema&);
    };
}
}
