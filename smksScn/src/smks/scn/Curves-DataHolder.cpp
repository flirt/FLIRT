// ========================================================================= //
// Copyright 2016-2018 SUPAMONKS_STUDIO                                      //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
//                                                                           //
// ========================================================================= //
#include <smks/sys/Log.hpp>
#include <smks/xchg/Data.hpp>
#include <smks/xchg/DataStream.hpp>
#include <smks/parm/BuiltIn.hpp>
#include <smks/parm/BuiltIns.hpp>

#include "smks/scn/Scene.hpp"
#include "Curves-DataHolder.hpp"
#include "Curves-DataHolder-Indices.hpp"
#include "AbstractCurvesSchema.hpp"

#include "BasisType.hpp"

#include "../util/math/BSplineToCubicBezierConverter.hpp"

namespace smks { namespace scn
{
    static
    CurveOrder
    getBSplineOrderOrThrow(const AbstractCurvesSchema& schema)
    {
        CurveOrder  order;
        bool        periodic;
        BasisType   basis;

        schema.getBasisAndType(order, periodic, basis);

        if (periodic)
        {
            std::stringstream sstr;
            sstr << "Periodic curves are not supported (object = ''" << schema.node().name() << ").";
            throw sys::Exception(sstr.str().c_str(), "Curves Position");
        }
        if (basis != BasisType::BSPLINE_BASIS)
        {
            std::stringstream sstr;
            sstr << "Only B-spline curves are supported (object = ''" << schema.node().name() << ").";
            throw sys::Exception(sstr.str().c_str(), "Curves Position");
        }
        return order;
    }
}
}

using namespace smks;

scn::Curves::DataHolder::DataHolder():
    _schema         (nullptr),
    _defaultWidth   (Scene::Config().defaultCurveWidth()),
    _vertexAttribs  (),
    _scalpAttribs   (),
    _bezierCounts   (),
    _indices        (),
    _vertices       (),
    _scalps         (),
    _bounds         ()
{ }

void
scn::Curves::DataHolder::build(AbstractObjectSchema* schema)
{
    _schema = schema ? schema->asCurvesSchema() : nullptr;
    assert(_schema);
}

void
scn::Curves::DataHolder::uninitialize()
{
    unsetAllParameters();

    _defaultWidth = Scene::Config().defaultCurveWidth();

    for (IndicesMap::iterator it = _indices.begin(); it != _indices.end(); ++it)
        if (it->second)
            it->second->dispose();
    _indices        .clear();

    _vertexAttribs  .clear();
    _scalpAttribs   .clear();

    _bezierCounts   .dirtyAll();
    _vertices       .dirtyAll();
    _scalps         .dirtyAll();
    _bounds         .dirtyAll();

    _bezierCounts   .dispose();
    _vertices       .dispose();
    _scalps         .dispose();
    _bounds         .dispose();
}

void
scn::Curves::DataHolder::initialize()
{
    uninitialize();
    assert(_schema);
    if (_schema->numSamples() > 0)  // schema has been initialized
    {
        // get general scene configuration
        TreeNode::Ptr const& root = _schema->node().root().lock();
        const Scene::Config& cfg  = root && root->asScene() ? root->asScene()->config() : Scene::Config();

        _defaultWidth   = cfg.defaultCurveWidth();
        _bezierCounts   .initialize(cfg.cacheAnim() ? _schema->numIndexBufferSamples() : 0);

        _vertices       .initialize(cfg.cacheAnim() ? _schema->numVertexBufferSamples() : 0);
        _scalps         .initialize(cfg.cacheAnim() ? _schema->numVertexBufferSamples() : 0);
        _bounds         .initialize(cfg.cacheAnim() ? _schema->numVertexBufferSamples() : 0);

        _bezierCounts   .dirtyAll();
        _vertices       .dirtyAll();
        _scalps         .dirtyAll();
        _bounds         .dirtyAll();

        assert(_schema->has(Property::PROP_POSITION));
        _vertexAttribs.append(Property::PROP_POSITION, 3);
        _vertexAttribs.append(Property::PROP_WIDTH, 1); // will compute it anyway

        if (_schema->has(Property::PROP_SCALP_NORMAL))
            _scalpAttribs.append(Property::PROP_SCALP_NORMAL, 3);
        if (_schema->has(Property::PROP_SCALP_TEXCOORD))
            _scalpAttribs.append(Property::PROP_SCALP_TEXCOORD, 2);
    }
}

void
scn::Curves::DataHolder::synchronizeWithSchema()
{
    assert(_schema);
    if (_schema->numSamples() > 0)  // schema has been initialized
    {
        // get general scene configuration
        TreeNode::Ptr const& root = _schema->node().root().lock();
        const Scene::Config& cfg  = root && root->asScene() ? root->asScene()->config() : Scene::Config();

        setParameter<xchg::BoundingBox>(_schema->node(), parm::boundingBox(), getBounds(), parm::SKIPS_RESTRAINTS);
        if (cfg.withViewport())
            glSynchronize();
        if (cfg.withRendering())
            rtSynchronize();
    }
    else
        unsetAllParameters();
}

void
scn::Curves::DataHolder::unsetAllParameters()
{
    assert(_schema);
    SchemaBasedSceneObject& node = _schema->node();

    // get general scene configuration
    TreeNode::Ptr const& root = node.root().lock();
    const Scene::Config& cfg  = root && root->asScene() ? root->asScene()->config() : Scene::Config();

    unsetParameter(node, parm::boundingBox(), parm::SKIPS_RESTRAINTS);
    if (cfg.withViewport())
    {
        unsetParameter(node, parm::glVertices(),    parm::SKIPS_RESTRAINTS);
        unsetParameter(node, parm::glPositions(),   parm::SKIPS_RESTRAINTS);
        unsetParameter(node, parm::glWidths(),      parm::SKIPS_RESTRAINTS);
        unsetParameter(node, parm::glNormals(),     parm::SKIPS_RESTRAINTS);
        unsetParameter(node, parm::glTexcoords(),   parm::SKIPS_RESTRAINTS);
        unsetParameter(node, parm::glIndices(),     parm::SKIPS_RESTRAINTS);
        unsetParameter(node, parm::glCount(),       parm::SKIPS_RESTRAINTS);
    }
    if (cfg.withRendering())
    {
        unsetParameter(node, parm::rtVertices(),    parm::SKIPS_RESTRAINTS);
        unsetParameter(node, parm::rtPositions(),   parm::SKIPS_RESTRAINTS);
        unsetParameter(node, parm::rtWidths(),      parm::SKIPS_RESTRAINTS);
        unsetParameter(node, parm::rtScalps(),      parm::SKIPS_RESTRAINTS);
        unsetParameter(node, parm::rtNormals(),     parm::SKIPS_RESTRAINTS);
        unsetParameter(node, parm::rtTexcoords(),   parm::SKIPS_RESTRAINTS);
        unsetParameter(node, parm::rtIndices(),     parm::SKIPS_RESTRAINTS);
        unsetParameter(node, parm::rtPathIds(),     parm::SKIPS_RESTRAINTS);
        unsetParameter(node, parm::rtCount(),       parm::SKIPS_RESTRAINTS);
    }
}

void
scn::Curves::DataHolder::glSynchronize()
{
    assert(_schema && _schema->numSamples() > 0);
    SchemaBasedSceneObject& node = _schema->node();

    const GeomFeatures  f           = GeomFeatures::SEPARATE_BEZIER_CURVES;
    xchg::Data::Ptr     vertices    = nullptr;
    xchg::Data::Ptr     indices     = nullptr;
    xchg::DataStream    positions, widths, normals, texcoords;

    size_t              numVertices = 0;
    size_t              numIndices  = 0;
    size_t              count       = 0;
    BufferAttributes    vtxAttribs;

    const float*        vtxData = getVertices   (f, numVertices,    vtxAttribs);
    const unsigned int* idxData = getIndices    (f, numIndices,     count);

    if (vtxData && numVertices > 0 && vtxAttribs.stride() > 0)
    {
        vertices = xchg::Data::create<float>(vtxData, numVertices * vtxAttribs.stride(), false);
        if (vtxAttribs.has(Property::PROP_POSITION))
            positions.initialize(vertices, numVertices, vtxAttribs.stride(), vtxAttribs.offset(Property::PROP_POSITION));
        if (vtxAttribs.has(Property::PROP_WIDTH))
            widths.initialize   (vertices, numVertices, vtxAttribs.stride(), vtxAttribs.offset(Property::PROP_WIDTH));
        if (vtxAttribs.has(Property::PROP_SCALP_NORMAL))
            normals.initialize  (vertices, numVertices, vtxAttribs.stride(), vtxAttribs.offset(Property::PROP_SCALP_NORMAL));
        if (vtxAttribs.has(Property::PROP_SCALP_TEXCOORD))
            texcoords.initialize(vertices, numVertices, vtxAttribs.stride(), vtxAttribs.offset(Property::PROP_SCALP_TEXCOORD));
    }
    if (idxData && numIndices > 0)
        indices = xchg::Data::create<unsigned int>(idxData, numIndices, false);

    if(vertices)    setParameter<xchg::Data::Ptr>   (node, parm::glVertices(),  vertices,   parm::SKIPS_RESTRAINTS);    else unsetParameter(node, parm::glVertices(),   parm::SKIPS_RESTRAINTS);
    if(positions)   setParameter<xchg::DataStream>  (node, parm::glPositions(), positions,  parm::SKIPS_RESTRAINTS);    else unsetParameter(node, parm::glPositions(),  parm::SKIPS_RESTRAINTS);
    if(widths)      setParameter<xchg::DataStream>  (node, parm::glWidths(),    widths,     parm::SKIPS_RESTRAINTS);    else unsetParameter(node, parm::glWidths(),     parm::SKIPS_RESTRAINTS);
    if(normals)     setParameter<xchg::DataStream>  (node, parm::glNormals(),   normals,    parm::SKIPS_RESTRAINTS);    else unsetParameter(node, parm::glNormals(),    parm::SKIPS_RESTRAINTS);
    if(texcoords)   setParameter<xchg::DataStream>  (node, parm::glTexcoords(), texcoords,  parm::SKIPS_RESTRAINTS);    else unsetParameter(node, parm::glTexcoords(),  parm::SKIPS_RESTRAINTS);
    if(indices)     setParameter<xchg::Data::Ptr>   (node, parm::glIndices(),   indices,    parm::SKIPS_RESTRAINTS);    else unsetParameter(node, parm::glIndices(),    parm::SKIPS_RESTRAINTS);
    if(count > 0)   setParameter<size_t>            (node, parm::glCount(),     count,      parm::SKIPS_RESTRAINTS);    else unsetParameter(node, parm::glCount(),      parm::SKIPS_RESTRAINTS);
}

void
scn::Curves::DataHolder::rtSynchronize()
{
    assert(_schema && _schema->numSamples() > 0);
    SchemaBasedSceneObject& node = _schema->node();

    const GeomFeatures  f           = GeomFeatures::NONE;
    xchg::Data::Ptr     vertices    = nullptr;
    xchg::Data::Ptr     scalps      = nullptr;
    xchg::Data::Ptr     indices     = nullptr;
    xchg::Data::Ptr     pathIds     = nullptr;
    xchg::DataStream    positions, widths, normals, texcoords;

    size_t              numVertices = 0;
    size_t              numScalps   = 0;
    size_t              numIndices  = 0;
    size_t              numPaths    = 0;
    size_t              count       = 0;
    BufferAttributes    vtxAttribs, sclpAttribs;

    const float*        vtxData     = getVertices           (f, numVertices,    vtxAttribs);
    const float*        sclpData    = getScalps             (f, numScalps,      sclpAttribs);
    const unsigned int* idxData     = getIndices            (f, numIndices,     count);
    const size_t*       pathData    = numBezierCurvesPerPath(f, numPaths);

    if (vtxData && numVertices > 0 && vtxAttribs.stride() > 0)
    {
        vertices = xchg::Data::create<float>(vtxData, numVertices * vtxAttribs.stride(), false);
        if (vtxAttribs.has(Property::PROP_POSITION))
            positions.initialize(vertices, numVertices, vtxAttribs.stride(), vtxAttribs.offset(Property::PROP_POSITION));
        if (vtxAttribs.has(Property::PROP_WIDTH))
            widths.initialize   (vertices, numVertices, vtxAttribs.stride(), vtxAttribs.offset(Property::PROP_WIDTH));
    }
    if (sclpData && numScalps > 0 && sclpAttribs.stride() > 0)
    {
        scalps = xchg::Data::create<float>(sclpData, numScalps * sclpAttribs.stride(), false);
        if (sclpAttribs.has(Property::PROP_SCALP_NORMAL))
            normals.initialize  (scalps, numScalps, sclpAttribs.stride(), sclpAttribs.offset(Property::PROP_SCALP_NORMAL));
        if (sclpAttribs.has(Property::PROP_SCALP_TEXCOORD))
            texcoords.initialize(scalps, numScalps, sclpAttribs.stride(), sclpAttribs.offset(Property::PROP_SCALP_TEXCOORD));
    }
    if (idxData && numIndices > 0)
    {
        indices = xchg::Data::create<unsigned int>(idxData, numIndices, false);
    }
    if (pathData && numPaths > 0 && numIndices > 0)
    {
        // store array that maps all segments to the path it takes part to (useful for scalp information remapping)
        unsigned int* data = new unsigned int[numIndices];

        for (size_t i = 0, k = 0; i < numPaths; ++i)
            for (size_t j = 0; j < pathData[i]; ++j)
            {
                assert(k < numIndices);
                data[k++] = static_cast<unsigned int>(i);
            }
        pathIds = xchg::Data::create<unsigned int>(data, numIndices, true);

        delete[] data;
    }

    if(vertices)    setParameter<xchg::Data::Ptr>   (node, parm::rtVertices(),  vertices,   parm::SKIPS_RESTRAINTS);    else unsetParameter(node, parm::rtVertices(),   parm::SKIPS_RESTRAINTS);
    if(positions)   setParameter<xchg::DataStream>  (node, parm::rtPositions(), positions,  parm::SKIPS_RESTRAINTS);    else unsetParameter(node, parm::rtPositions(),  parm::SKIPS_RESTRAINTS);
    if(widths)      setParameter<xchg::DataStream>  (node, parm::rtWidths(),    widths,     parm::SKIPS_RESTRAINTS);    else unsetParameter(node, parm::rtWidths(),     parm::SKIPS_RESTRAINTS);
    if(scalps)      setParameter<xchg::Data::Ptr>   (node, parm::rtScalps(),    scalps,     parm::SKIPS_RESTRAINTS);    else unsetParameter(node, parm::rtScalps(),     parm::SKIPS_RESTRAINTS);
    if(normals)     setParameter<xchg::DataStream>  (node, parm::rtNormals(),   normals,    parm::SKIPS_RESTRAINTS);    else unsetParameter(node, parm::rtNormals(),    parm::SKIPS_RESTRAINTS);
    if(texcoords)   setParameter<xchg::DataStream>  (node, parm::rtTexcoords(), texcoords,  parm::SKIPS_RESTRAINTS);    else unsetParameter(node, parm::rtTexcoords(),  parm::SKIPS_RESTRAINTS);
    if(indices)     setParameter<xchg::Data::Ptr>   (node, parm::rtIndices(),   indices,    parm::SKIPS_RESTRAINTS);    else unsetParameter(node, parm::rtIndices(),    parm::SKIPS_RESTRAINTS);
    if(pathIds)     setParameter<xchg::Data::Ptr>   (node, parm::rtPathIds(),   pathIds,    parm::SKIPS_RESTRAINTS);    else unsetParameter(node, parm::rtPathIds(),    parm::SKIPS_RESTRAINTS);
    if(count > 0)   setParameter<size_t>            (node, parm::rtCount(),     count,      parm::SKIPS_RESTRAINTS);    else unsetParameter(node, parm::rtCount(),      parm::SKIPS_RESTRAINTS);
}

size_t
scn::Curves::DataHolder::numBezierVertices(GeomFeatures f)
{
    assert(_schema);
    if (_schema->numSamples() == 0)
        return 0;

    const int           indexSampleId   = _schema->currentIndexSampleIdx();
    const BezierCounts* cache           = _bezierCounts.get(indexSampleId);
    if (cache)
        return cache->numVertices;

    // actual computation & cache update
    cache = computeAndCacheBezierCounts(f);

    return cache
        ? cache->numVertices
        : 0;
}

const size_t*
scn::Curves::DataHolder::numBezierCurvesPerPath(
    GeomFeatures    f,
    size_t&         numPaths)
{
    assert(_schema);
    numPaths = 0;

    if (_schema->numSamples() == 0)
        return nullptr;

    const int           indexSampleId   = _schema->currentIndexSampleIdx();
    const BezierCounts* cache           = _bezierCounts.get(indexSampleId);
    if (cache)
    {
        numPaths = cache->numCurvesPerPath.size();

        return numPaths > 0
            ? &cache->numCurvesPerPath.front()
            : nullptr;
    }

    // actual computation & cache update
    cache       = computeAndCacheBezierCounts(f);
    numPaths    = cache
        ? cache->numCurvesPerPath.size()
        : 0;

    return numPaths > 0
        ? &cache->numCurvesPerPath.front()
        : nullptr;
}

const scn::BezierCounts*
scn::Curves::DataHolder::computeAndCacheBezierCounts(GeomFeatures f)
{
    assert(_schema);
    if (_schema->numSamples() == 0)
        return nullptr;

    BezierCounts                bezierCounts;
    std::vector<unsigned int>   firstCPVertices;
    const int                   indexSampleId   = _schema->currentIndexSampleIdx();

    getCubicBezierIndexBuffer(
        _schema,
        bezierCounts.numVertices,
        bezierCounts.numCurvesPerPath,
        firstCPVertices);

    // cache update
    Indices::Ptr indices = getIndices(f);
    if (indices)
    {
        size_t numIndices = 0;

        indices->set(firstCPVertices, bezierCounts.numVertices, numIndices);
    }
    return _bezierCounts.set(indexSampleId, bezierCounts);
}

const unsigned int*
scn::Curves::DataHolder::getIndices(
    GeomFeatures    f,
    size_t&         numIndices,
    size_t&         numIndicesPerPrimitive)
{
    assert(_schema);
    numIndices              = 0;
    numIndicesPerPrimitive  = 0;

    if (_schema->numSamples() == 0)
        return nullptr;

    Indices::Ptr const& indices = getIndices(f);

    numIndicesPerPrimitive  = indices
        ? indices->numIndicesPerPrimitive()
        : 0;

    return indices
        ? indices->get(numIndices)
        : nullptr;
}

const float*
scn::Curves::DataHolder::getVertices(
    GeomFeatures        f,
    size_t&             numVertices,
    BufferAttributes&   attribs)
{
    assert(_schema);
    numVertices = 0;
    attribs.clear();

    if (_schema->numSamples() == 0)
        return nullptr;

    attribs = _vertexAttribs;
    assert(_vertexAttribs.stride() > 0);

    const int                   vertexSampleId  = _schema->currentVertexSampleIdx();
    const std::vector<float>*   cache           = _vertices.get(vertexSampleId);
    if (cache)
    {
        numVertices = cache->size() / _vertexAttribs.stride();
        return numVertices > 0
            ? &cache->front()
            : nullptr;
    }

    // actual computation
    numVertices = numBezierVertices(f);

    std::vector<float> vertices (numVertices * _vertexAttribs.stride());

    if (!vertices.empty())
    {
        assert(_vertexAttribs.has(Property::PROP_POSITION));
        assert(_vertexAttribs.has(Property::PROP_WIDTH));

        float*  positions   = &vertices.front() + _vertexAttribs.offset(Property::PROP_POSITION);
        float*  widths      = &vertices.front() + _vertexAttribs.offset(Property::PROP_WIDTH);

        getCubicBezierVertexPositions   (positions, numVertices, _vertexAttribs.stride());
        getCubicBezierVertexWidths      (widths,    numVertices, _vertexAttribs.stride());
    }

    // cache update
    cache       = _vertices.set(vertexSampleId, vertices);
    numVertices = cache
        ? cache->size() / _vertexAttribs.stride()
        : 0;
    return numVertices > 0
        ? &cache->front()
        : nullptr;
}

void
scn::Curves::DataHolder::getCubicBezierVertexPositions(
    float* oPtr,
    size_t oSize,
    size_t oStride) const
{
    assert(_schema);
    if (_schema->numSamples() == 0 ||
        oPtr == nullptr ||
        oSize == 0 ||
        oStride == 0)
        return;

    CurveOrder      order               = getBSplineOrderOrThrow(*_schema);
    size_t          numBSplinePositions = 0;
    size_t          numBSplines         = 0;
    size_t          numBSplineKnots     = 0;
    const float*    bSplinePositions    = _schema->lockPositions(numBSplinePositions);
    const int*      bSplineNumVertices  = _schema->lockNumVertices(numBSplines);
    const float*    bSplineKnots        = _schema->lockKnots(numBSplineKnots);

    if (numBSplinePositions > 0 &&
        numBSplines > 0 &&
        numBSplineKnots > 0)
    {
        util::math::BSplineToCubicBezierConverter::getVertexBuffer(
            order,
            numBSplines,
            bSplineNumVertices,
            bSplineKnots,
            bSplinePositions,
            3,
            oPtr,
            oStride);
    }

    _schema->unlockPositions();
    _schema->unlockNumVertices();
    _schema->unlockKnots();
}

void
scn::Curves::DataHolder::getCubicBezierVertexWidths(
    float*  oPtr,
    size_t  oSize,
    size_t  oStride) const
{
    assert(_schema);
    if (_schema->numSamples() == 0 ||
        oPtr == nullptr ||
        oSize == 0 ||
        oStride == 0)
        return;

    size_t          numBSplineWidths    = 0;
    size_t          numBSplinePositions = 0;
    const float*    bSplineWidths       = _schema->lockWidths(numBSplineWidths);
    const float*    bSplinePositions    = _schema->lockPositions(numBSplinePositions);

    _schema->unlockPositions(); // data not necessary

    if (numBSplineWidths == numBSplinePositions)
    {
        CurveOrder      order               = getBSplineOrderOrThrow(*_schema);
        size_t          numBSplines         = 0;
        size_t          numBSplineKnots     = 0;
        const int*      bSplineNumVertices  = _schema->lockNumVertices(numBSplines);
        const float*    bSplineKnots        = _schema->lockKnots(numBSplineKnots);

        util::math::BSplineToCubicBezierConverter::getVertexBuffer(
            order,
            numBSplines,
            bSplineNumVertices,
            bSplineKnots,
            bSplineWidths,
            1,
            oPtr,
            oStride);

        _schema->unlockKnots();
        _schema->unlockNumVertices();
    }
    else
    {
        // assign constant width to all vertices
        const float width = numBSplineWidths >= 1 && bSplineWidths
            ? bSplineWidths[0]
            : _defaultWidth;

        float* oCurrent = oPtr;
        for (size_t i = 0; i < oSize; ++i)
        {
            *oCurrent   = width;
            oCurrent    += oStride;
        }
        FLIRT_LOG(LOG(DEBUG)
            << "set constant width (= " << width << ") "
            << "to curves in '" << _schema->node().name() <<"'";)
    }
    _schema->unlockWidths();
}

const float*
scn::Curves::DataHolder::getScalps(
    GeomFeatures,
    size_t&             numScalps,
    BufferAttributes&   attribs)
{
    assert(_schema);
    numScalps = 0;
    attribs.clear();

    if (_schema->numSamples() == 0)
        return nullptr;

    attribs = _scalpAttribs;
    if (!_scalpAttribs.has(Property::PROP_SCALP_NORMAL) ||
        !_scalpAttribs.has(Property::PROP_SCALP_TEXCOORD))
        return nullptr;

    assert(_scalpAttribs.stride() > 0);

    const int                   vertexSampleId  = _schema->currentVertexSampleIdx();
    const std::vector<float>*   cache           = _scalps.get(vertexSampleId);
    if (cache)
    {
        numScalps = cache->size() / _scalpAttribs.stride();
        return numScalps > 0
            ? &cache->front()
            : nullptr;
    }

    // actual computation
    numScalps = 0;
    const int* numVertices = _schema->lockNumVertices(numScalps);
    _schema->unlockNumVertices();

    std::vector<float> scalps(numScalps * _scalpAttribs.stride());

    if (scalps.empty())
        return nullptr;

    // - scalp texture coordinates
    size_t          numScalpTexCoords   = 0;
    const float*    scalpTexCoords      = _schema->lockScalpTexCoords(numScalpTexCoords);

    if (numScalpTexCoords == numScalps)
    {
        const float*    current     = scalpTexCoords;
        float*          oCurrent    = &scalps.front() + _scalpAttribs.offset(Property::PROP_SCALP_TEXCOORD);

        for (size_t i = 0; i < numScalps; ++i)
        {
            memcpy(oCurrent, current, sizeof(float) << 1);
            current     += 2;
            oCurrent    += _scalpAttribs.stride();
        }
    }
    _schema->unlockScalpTexCoords();

    // - scalp normals
    size_t          numScalpNormals = 0;
    const float*    scalpNormals    = _schema->lockScalpNormals(numScalpNormals);

    if (numScalpNormals == numScalps)
    {
        const float*    current     = scalpNormals;
        float*          oCurrent    = &scalps.front() + _scalpAttribs.offset(Property::PROP_SCALP_NORMAL);

        for (size_t i = 0; i < numScalps; ++i)
        {
            memcpy(oCurrent, current, sizeof(float) * 3);
            current     += 3;
            oCurrent    += _scalpAttribs.stride();
        }
    }
    _schema->unlockScalpNormals();

    // cache update
    cache       = _scalps.set(vertexSampleId, scalps);
    numScalps   = cache
        ? cache->size() / _scalpAttribs.stride()
        : 0;

    return numScalps > 0
        ? &cache->front()
        : nullptr;
}

const xchg::BoundingBox&
scn::Curves::DataHolder::getBounds()
{
    assert(_schema && _schema->numSamples() > 0);

    const int                   vertexSampleId  = _schema->currentVertexSampleIdx();
    const xchg::BoundingBox*    cache           = _bounds.get(vertexSampleId);
    if (cache)
        return *cache;

    // actual computation
    xchg::BoundingBox bounds;

    if (_schema->has(Property::PROP_BOUNDS))
        _schema->getSelfBounds(bounds);
    else
    {
        size_t          numPositions    = 0;
        const float*    positions       = _schema->lockPositions(numPositions);

        bounds.initialize(positions, numPositions, 3);
        _schema->unlockPositions();
    }

    // cache update
    return *_bounds.set(vertexSampleId, bounds);
}

scn::Curves::DataHolder::Indices::Ptr
scn::Curves::DataHolder::getIndices(GeomFeatures f)
{
    assert(_schema);
    if (_schema->numSamples() == 0)
        return nullptr;

    if (_indices.find(f) == _indices.end())
    {
        // get general scene configuration
        TreeNode::Ptr const& root = _schema->node().root().lock();
        const Scene::Config& cfg  = root && root->asScene() ? root->asScene()->config() : Scene::Config();

        Indices::Ptr indices = nullptr;

        if (f & GeomFeatures::SEPARATE_BEZIER_CURVES)
            indices = PerBezierCurveIndices::create();
        else
            indices = PerBezierPathIndices::create();

        assert(indices);
        indices->initialize(_schema, cfg.cacheAnim());
        _indices[f] = indices;
    }

    return _indices[f];
}

// static
void
scn::Curves::DataHolder::getCubicBezierIndexBuffer(
    const AbstractCurvesSchema* curves,
    size_t&                     numBezierVertices,
    std::vector<size_t>&        numBezierCurvesPerPath,
    std::vector<unsigned int>&  firstCPIndices)
{
    numBezierVertices       = 0;
    numBezierCurvesPerPath  .clear();
    firstCPIndices          .clear();

    // initialize index buffer from the knot sequence
    CurveOrder      order       = getBSplineOrderOrThrow(*curves);
    size_t          numCurves   = 0;
    size_t          numKnots    = 0;
    const int*      numVertices = curves->lockNumVertices(numCurves);
    const float*    knots       = curves->lockKnots(numKnots);

    if (numCurves > 0 &&
        numKnots > 0 &&
        numVertices != nullptr &&
        knots != nullptr)
    {
        util::math::BSplineToCubicBezierConverter::getIndexBuffer(
            order,
            numCurves,
            numVertices,
            knots,
            numBezierVertices,
            numBezierCurvesPerPath,
            firstCPIndices
        );
    }

    curves->unlockNumVertices();
    curves->unlockKnots();
}
