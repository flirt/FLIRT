// ========================================================================= //
// Copyright 2016-2018 SUPAMONKS_STUDIO                                      //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
//                                                                           //
// ========================================================================= //
#include <smks/sys/Exception.hpp>
#include <smks/sys/Log.hpp>
#include <smks/parm/BuiltIn.hpp>
#include <smks/parm/BuiltIns.hpp>

#include <smks/math/matrixAlgo.hpp>
#include "smks/scn/SchemaBasedSceneObject.hpp"
#include "SchemaBaseFromAbc.hpp"
#include "CameraSchemaFromAbc.hpp"

using namespace smks;

scn::CameraSchemaFromAbc::CameraSchemaFromAbc(SchemaBasedSceneObject&       node,
                                              const Alembic::Abc::IObject&  object,
                                              TreeNode::WPtr const&         archive):
    AbstractCameraSchema(),
    _object             (object),
    _base               (new SchemaBaseFromAbc(node, object, archive)),
    _camera             ()
{
    if (!object.valid())
        throw sys::Exception("Incorrect Alembic object parameter.", "Alembic Camera Schema Constructor");
    //---
    assert(numSamples() == 0);
}

scn::CameraSchemaFromAbc::~CameraSchemaFromAbc()
{
    uninitialize();
    delete _base;
}

scn::SchemaBasedSceneObject&
scn::CameraSchemaFromAbc::node()
{
    return _base->node();
}
const scn::SchemaBasedSceneObject&
scn::CameraSchemaFromAbc::node() const
{
    return _base->node();
}

const Alembic::AbcGeom::ICameraSchema&
scn::CameraSchemaFromAbc::getAbcSchema()
{
    if (_camera.valid())
        return _camera;

    assert(_object.valid());
    if (!Alembic::AbcGeom::ICamera::matches(_object.getMetaData()))
    {
        std::stringstream sstr;
        sstr
            << "Alembic object '" << _object.getName() << "' "
            << "is not a camera.";
        throw sys::Exception(sstr.str().c_str(), "Camera Alembic Schema Getter");
    }

    Alembic::AbcGeom::ICamera camera(_object, Alembic::Abc::kWrapExisting);

    if (!camera.valid())
    {
        std::stringstream sstr;
        sstr
            << "Failed to get a valid Alembic camera "
            << "from '" << _object.getName() << "'.";
        throw sys::Exception(sstr.str().c_str(), "Camera Alembic Schema Getter");
    }

    _camera = camera.getSchema();
    assert(_camera.valid());
    return _camera;
}

void
scn::CameraSchemaFromAbc::uninitialize()
{
    _camera.reset();
    //---
    _base->uninitialize();
    assert(numSamples() == 0);
}

size_t
scn::CameraSchemaFromAbc::initialize()
{
    if (numSamples() > 0)
        return numSamples();

    uninitialize();

    const Alembic::AbcGeom::ICameraSchema& camera = getAbcSchema();
    //---
    const size_t numSamples = _base->initialize(camera);

    return numSamples;
}

void
scn::CameraSchemaFromAbc::uninitializeUserAttribs()
{
    _base->uninitializeUserAttribs();
}

void
scn::CameraSchemaFromAbc::initializeUserAttribs()
{
    _base->initializeUserAttribs(getAbcSchema());
}

//-----------------------------------
// parameter handling
//-----------------------------------
bool
scn::CameraSchemaFromAbc::isParameterRelevantForClass(unsigned int parmId) const
{
    return
        _base->isParameterRelevantForClass(parmId)  ||
        parmId == parm::xAspectRatio    ().id()     ||
        parmId == parm::yFieldOfViewD   ().id()     ||
        parmId == parm::zNear           ().id()     ||
        parmId == parm::zFar            ().id();
}
bool
scn::CameraSchemaFromAbc::isParameterReadable(unsigned int parmId) const
{
    return _base->isParameterReadable(parmId);
}
char
scn::CameraSchemaFromAbc::isParameterWritable(unsigned int parmId) const
{
    char ret = -1;
    if (isParameterRelevantForClass(parmId))
    {
        ret = _base->isParameterWritable(parmId);
        if (ret == -1)
            ret = 0;
    }
    return ret;
}
bool
scn::CameraSchemaFromAbc::overrideParameterDefaultValue(unsigned int parmId, parm::Any& defaultValue) const
{
    return _base->overrideParameterDefaultValue(parmId, defaultValue);
}
void
scn::CameraSchemaFromAbc::handle(const xchg::ParmEvent& evt)
{
    _base->handle(evt);
}
bool
scn::CameraSchemaFromAbc::mustSendToScene(const xchg::ParmEvent& evt) const
{
    return _base->mustSendToScene(evt);
}
//-----------------------------------

bool //<! return current visibility
scn::CameraSchemaFromAbc::setCurrentTime(float time)
{
    return _base->setCurrentTime(time);
}

size_t
scn::CameraSchemaFromAbc::numSamples() const
{
    return _base->numSamples();
}

void
scn::CameraSchemaFromAbc::getStoredTimes(xchg::Vector<float>& ret) const
{
    _base->getStoredTimes(ret);
}

int
scn::CameraSchemaFromAbc::currentSampleIdx() const
{
    return _base->currentSampleIdx();
}

void
scn::CameraSchemaFromAbc::getAttributes(Camera::Attributes& attribs) const
{
    getAttributes(_base->currentSampleIdx(), attribs);
}

void
scn::CameraSchemaFromAbc::getFilmBackOperations(Camera::FilmBackOperations& ops) const
{
    getFilmBackOperations(_base->currentSampleIdx(), ops);
}

void
scn::CameraSchemaFromAbc::getAttributes(int sampleId, Camera::Attributes& attribs) const
{
    assert(numSamples() > 0);
    assert(sampleId >= 0 && sampleId < static_cast<int>(numSamples()));

    Alembic::AbcGeom::CameraSample cameraSample;

    _camera.get(cameraSample, Alembic::Abc::ISampleSelector(static_cast<Alembic::Abc::index_t>(sampleId)));

    const double focalLength    = cameraSample.getFocalLength();
    const double xAperture      = 10.0 * cameraSample.getHorizontalAperture();
    const double yAperture      = 10.0 * cameraSample.getVerticalAperture();
    assert(xAperture > 1e-6);
    assert(yAperture > 1e-6);

    const double xAspectRatio   = xAperture / yAperture;
    const double xFieldOfView   = cameraSample.getFieldOfView() * static_cast<double>(sys::degrees_to_radians);
    const double yFieldOfView   = 2.0 * atan( tan(xFieldOfView * 0.5) * (yAperture / xAperture));

    attribs.focalLength     = static_cast<float>(focalLength);
    attribs.xAspectRatio    = static_cast<float>(xAspectRatio);
    attribs.yAperture       = static_cast<float>(yAperture);
    attribs.yFieldOfViewD   = static_cast<float>(yFieldOfView * static_cast<double>(sys::radians_to_degrees));
    attribs.zNear           = static_cast<float>(cameraSample.getNearClippingPlane());
    attribs.zFar            = static_cast<float>(cameraSample.getFarClippingPlane());
    attribs.focusDistance   = static_cast<float>(cameraSample.getFocusDistance());
    attribs.fStop           = static_cast<float>(cameraSample.getFStop());

    FLIRT_LOG(LOG(DEBUG)
        << "attributes from camera '" << _object.getName() << "'"
        << "\n\t- focal length (mm) = " << attribs.focalLength
        << "\n\t- aperture (mm) = " << xAperture << " x " << attribs.yAperture
        << "\n\t- horizontal field of view (deg) = " << cameraSample.getFieldOfView()
        << "\n\t- vertical field of view (deg) = " << attribs.yFieldOfViewD
        << "\n\t- horizontal aspect ratio = " << attribs.xAspectRatio
        << "\n\t- clipping planes = " << attribs.zNear << " -> " << attribs.zFar
        << "\n\t- focus distance = " << attribs.focusDistance
        << "\n\t- f-stop = " << attribs.fStop;)
}

void
scn::CameraSchemaFromAbc::getFilmBackOperations(int sampleId, Camera::FilmBackOperations& ops) const
{
    assert(numSamples() > 0);
    assert(sampleId >= 0 && sampleId < static_cast<int>(numSamples()));

    ops.fit             = Camera::FilmFit::FILL;
    ops.preScale        = 1.0;
    ops.postScale       = 1.0;
    ops.cameraScale     = 1.0;
    ops.filmFitOffsetX  = 0.0;
    ops.filmFitOffsetY  = 0.0;
    ops.filmTranslateX  = 0.0;
    ops.filmTranslateY  = 0.0;

    Alembic::AbcGeom::CameraSample cameraSample;

    _camera.get(cameraSample, Alembic::Abc::ISampleSelector(static_cast<Alembic::Abc::index_t>(sampleId)));

    const std::size_t numOps = cameraSample.getNumOps();
    for (std::size_t i = 0; i < numOps; ++i)
    {
        const Alembic::AbcGeom::FilmBackXformOp& op = cameraSample[i];

        if (op.getHint() == "filmFitFill")
            ops.fit = Camera::FilmFit::FILL;
        else if (op.getHint() == "filmFitHorz")
            ops.fit = Camera::FilmFit::HORIZONTAL;
        else if (op.getHint() == "filFitVert")
            ops.fit = Camera::FilmFit::VERTICAL;
        else if (op.getHint() == "filmFitOver")
            ops.fit = Camera::FilmFit::OVERSCAN;
        else if (op.getHint() == "filmFitOffs")
        {
            ops.filmFitOffsetX = op.getChannelValue(0);
            ops.filmFitOffsetY = op.getChannelValue(1);
        }
        else if (op.getHint() == "preScale")
            ops.preScale = op.getChannelValue(0);
        else if (op.getHint() == "filmTranslate")
        {
            ops.filmTranslateX = op.getChannelValue(0);
            ops.filmTranslateY = op.getChannelValue(1);
        }
        else if (op.getHint() == "postScale")
            ops.postScale = op.getChannelValue(0);
        else if (op.getHint() == "cameraScale")
            ops.cameraScale = op.getChannelValue(0);
    }
}
