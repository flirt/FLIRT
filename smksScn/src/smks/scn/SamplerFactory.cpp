// ========================================================================= //
// Copyright 2016-2018 SUPAMONKS_STUDIO                                      //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
//                                                                           //
// ========================================================================= //
#include <smks/sys/Exception.hpp>
#include <smks/parm/BuiltIns.hpp>
#include <smks/xchg/NodeCode.hpp>
#include <smks/xchg/NodeCodes.hpp>

#include "smks/scn/SamplerFactory.hpp"

using namespace smks;

// static
scn::SamplerFactory::Ptr
scn::SamplerFactory::create(const char*             code,
                            const char*             name,
                            TreeNode::WPtr const&   parent)
{
    if (code == nullptr ||
        strlen(code) == 0)
        throw sys::Exception("Invalid code.", "SamplerFactory Creation");

    Ptr ptr(new SamplerFactory(name, parent));
    ptr->build(ptr);
    ptr->setParameter<std::string>(parm::code(), code, parm::SKIPS_RESTRAINTS);
    return ptr;
}

// virtual
bool
scn::SamplerFactory::isParameterWritable(unsigned int parmId) const
{
    if (isParameterRelevantForClass(parmId))
        return parmId != parm::code().id();
    return RtNode::isParameterWritable(parmId);
}

// virtual
bool
scn::SamplerFactory::isParameterRelevant(unsigned int parmId)const
{
    return isParameterRelevantForClass(parmId) || RtNode::isParameterRelevant(parmId);
}

bool
scn::SamplerFactory::isParameterRelevantForClass(unsigned int parmId) const
{
    if (parmId == parm::code().id())
        return true;

    const unsigned int code = getCode();
    if (code == xchg::code::multijittered().id())
    {
        return
            parmId == parm::numSampleSets   ().id() ||
            parmId == parm::numSamples      ().id();
    }
    return false;
}

// virtual
bool
scn::SamplerFactory::overrideParameterDefaultValue(unsigned int parmId, parm::Any& value) const
{
    if (parmId == parm::numSamples().id())
    {
        size_t typedValue = 64;
        value = parm::Any(typedValue);
        return true;
    }
    return false;
}
