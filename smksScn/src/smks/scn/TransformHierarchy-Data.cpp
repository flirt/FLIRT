// ========================================================================= //
// Copyright 2016-2018 SUPAMONKS_STUDIO                                      //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
//                                                                           //
// ========================================================================= //
#include <smks/scn/TreeNode.hpp>
#include "TransformHierarchy.hpp"

using namespace smks;

scn::TransformHierarchy::Data::Data():
    _local      (),
    _global     (),
    _inherits   ()
{ }

bool
scn::TransformHierarchy::Data::inherits(size_t idx) const
{
    return _inherits.get(idx);
}

void
scn::TransformHierarchy::Data::inherits(size_t idx, bool value)
{
    _inherits.set(idx, value);
}

void
scn::TransformHierarchy::Data::setLocal(size_t idx, const math::Affine3f& value)
{
    assert(idx < _local.size());
    _local[idx] = value;
}

const math::Affine3f&
scn::TransformHierarchy::Data::getGlobal(size_t idx) const
{
    assert(idx < _global.size());
    return _global[idx];
}

void
scn::TransformHierarchy::Data::resize(size_t capacity)
{
    if (capacity > 0)
    {
        _local.resize(capacity);
        _global.resize(capacity);
        _inherits.resize(capacity, true);
    }
    else
    {
        _local.clear();
        _global.clear();
        _local.shrink_to_fit();
        _global.shrink_to_fit();
        _inherits.clear();
    }
}

void
scn::TransformHierarchy::Data::moveEntry(size_t dst, size_t src)
{
    assert(_local.size() == _global.size());
    assert(dst < _global.size());
    assert(src < _global.size());

    _local[dst]     = _local[src];
    _global[dst]    = _global[src];
    _inherits.set(dst, _inherits.get(src));
}

void
scn::TransformHierarchy::Data::recomputeEntry(size_t current)
{
    assert(_local.size() == _global.size());
    assert(current < _global.size());

    _global[current] = _local[current];
}

void
scn::TransformHierarchy::Data::accumulateEntries(size_t current, size_t parent)
{
    assert(_local.size() == _global.size());
    assert(current < _global.size());
    assert(parent < current);

    _global[current] = _global[parent] * _local[current];
}
