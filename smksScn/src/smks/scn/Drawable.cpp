// ========================================================================= //
// Copyright 2016-2018 SUPAMONKS_STUDIO                                      //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
//                                                                           //
// ========================================================================= //
#include <smks/xchg/ParmEvent.hpp>
#include <smks/parm/BuiltIn.hpp>
#include <smks/parm/BuiltIns.hpp>

#include "smks/scn/Drawable.hpp"

using namespace smks;

// virtual
bool
scn::Drawable::isParameterWritable(unsigned int parmId) const
{
    if (isParameterRelevantForClass(parmId))
        return true;
    return SchemaBasedSceneObject::isParameterWritable(parmId);
}

// virtual
bool
scn::Drawable::isParameterRelevant(unsigned int parmId)const
{
    return isParameterRelevantForClass(parmId) || SchemaBasedSceneObject::isParameterRelevant(parmId);
}

bool
scn::Drawable::isParameterRelevantForClass(unsigned int parmId) const
{
    return
        parmId == parm::materialId          ().id() ||
        parmId == parm::subsurfaceId        ().id() ||
        parmId == parm::illumMask           ().id() ||
        parmId == parm::castsShadows        ().id() ||
        parmId == parm::receivesShadows     ().id() ||
        parmId == parm::primaryVisibility   ().id() ||
        parmId == parm::visibleInReflections().id() ||
        parmId == parm::visibleInRefractions().id() ||
        parmId == parm::shadowBias          ().id();
}
