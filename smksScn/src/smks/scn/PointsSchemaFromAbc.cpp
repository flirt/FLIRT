// ========================================================================= //
// Copyright 2016-2018 SUPAMONKS_STUDIO                                      //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
//                                                                           //
// ========================================================================= //
#include <Alembic/Abc/TypedArraySample.h>

#include <smks/sys/Exception.hpp>
#include <smks/xchg/BoundingBox.hpp>
#include <smks/parm/BuiltIn.hpp>
#include <smks/parm/BuiltIns.hpp>

#include "smks/scn/SchemaBasedSceneObject.hpp"
#include "SchemaBaseFromAbc.hpp"
#include "PointsSchemaFromAbc.hpp"

namespace smks { namespace scn
{
    /////////////////////////////////////
    // class PointsSchemaFromAbc::SampleHolder
    /////////////////////////////////////
    struct PointsSchemaFromAbc::SampleHolder
    {
        Alembic::Abc::P3fArraySamplePtr     positions;
        Alembic::Abc::FloatArraySamplePtr   widths;

    public:
        inline
        SampleHolder():
            positions   (nullptr),
            widths      (nullptr)
        { }

        inline
        ~SampleHolder()
        {
            clear();
        }

        inline
        void
        clear()
        {
            positions   = nullptr;
            widths      = nullptr;
        }
    };
    /////////////////////////////////////
}
}

using namespace smks;

scn::PointsSchemaFromAbc::PointsSchemaFromAbc(SchemaBasedSceneObject&       node,
                                              const Alembic::Abc::IObject&  object,
                                              TreeNode::WPtr const&         archive):
    AbstractPointsSchema(),
    _object             (object),
    _base               (new SchemaBaseFromAbc(node, object, archive)),
    _points             (),
    _flags              (0),
    _locks              (new SampleHolder)
{
    if (!object.valid())
        throw sys::Exception("Incorrect Alembic object parameter.", "Alembic Points Schema Constructor");
    //---
    assert(numSamples() == 0);
}

scn::PointsSchemaFromAbc::~PointsSchemaFromAbc()
{
    uninitialize();
    delete _base;
    delete _locks;
}

const scn::SchemaBasedSceneObject&
scn::PointsSchemaFromAbc::node() const
{
    return _base->node();
}
scn::SchemaBasedSceneObject&
scn::PointsSchemaFromAbc::node()
{
    return _base->node();
}

const Alembic::AbcGeom::IPointsSchema&
scn::PointsSchemaFromAbc::getAbcSchema()
{
    if (_points.valid())
        return _points;

    assert(_object.valid());
    if (!Alembic::AbcGeom::IPoints::matches(_object.getMetaData()))
    {
        std::stringstream sstr;
        sstr
            << "Alembic object '" << _object.getName() << "' "
            << "is not a point set.",
        throw sys::Exception(sstr.str().c_str(), "Points Alembic Schema Getter");
    }

    Alembic::AbcGeom::IPoints points(_object, Alembic::Abc::kWrapExisting);

    if (!points.valid())
    {
        std::stringstream sstr;
        sstr
            << "Failed to get a valid Alembic point set "
            << "from '" << _object.getName() << "'.";
        throw sys::Exception(sstr.str().c_str(), "Points Alembic Schema Getter");
    }

    _points = points.getSchema();
    assert(_points.valid());
    return _points;
}

void
scn::PointsSchemaFromAbc::uninitialize()
{
    _points.reset();
    _flags = 0;
    _locks->clear();
    //---
    _base->uninitialize();
    assert(numSamples() == 0);
}

size_t
scn::PointsSchemaFromAbc::initialize()
{
    if (numSamples() > 0)
        return numSamples();

    uninitialize();

    const Alembic::AbcGeom::IPointsSchema& points = getAbcSchema();
    //---
    const size_t numSamples = _base->initialize(points);
    if (numSamples > 0)
    {
        _flags      |= Property::PROP_POSITION;
        if (checkWidths(points))
            _flags  |= Property::PROP_WIDTH;
        if (checkBounds(points))
            _flags  |= Property::PROP_BOUNDS;
    }
    return numSamples;
}

void
scn::PointsSchemaFromAbc::uninitializeUserAttribs()
{
    _base->uninitializeUserAttribs();
}

void
scn::PointsSchemaFromAbc::initializeUserAttribs()
{
    _base->initializeUserAttribs(getAbcSchema());
}

//-----------------------------------
// parameter handling
//-----------------------------------
bool
scn::PointsSchemaFromAbc::isParameterRelevantForClass(unsigned int parmId) const
{
    return
        _base->isParameterRelevantForClass(parmId) ||
        parmId == parm::boundingBox ().id() ||
        parmId == parm::vertices    ().id() ||
        parmId == parm::positions   ().id() ||
        parmId == parm::widths      ().id();
}
bool
scn::PointsSchemaFromAbc::isParameterReadable(unsigned int parmId) const
{
    return _base->isParameterReadable(parmId);
}
char
scn::PointsSchemaFromAbc::isParameterWritable(unsigned int parmId) const
{
    char ret = -1;
    if (isParameterRelevantForClass(parmId))
    {
        ret = _base->isParameterWritable(parmId);
        if (ret == -1)
            ret = 0;
    }
    return ret;
}
bool
scn::PointsSchemaFromAbc::overrideParameterDefaultValue(unsigned int parmId, parm::Any& defaultValue) const
{
    return _base->overrideParameterDefaultValue(parmId, defaultValue);
}
void
scn::PointsSchemaFromAbc::handle(const xchg::ParmEvent& evt)
{
    _base->handle(evt);
}
bool
scn::PointsSchemaFromAbc::mustSendToScene(const xchg::ParmEvent& evt) const
{
    return _base->mustSendToScene(evt);
}
//-----------------------------------

bool //<! return current visibility
scn::PointsSchemaFromAbc::setCurrentTime(float time)
{
    return _base->setCurrentTime(time);
}

size_t
scn::PointsSchemaFromAbc::numSamples() const
{
    return _base->numSamples();
}

int
scn::PointsSchemaFromAbc::currentSampleIdx() const
{
    return _base->currentSampleIdx();
}

void
scn::PointsSchemaFromAbc::getStoredTimes(xchg::Vector<float>& ret) const
{
    _base->getStoredTimes(ret);
}

bool
scn::PointsSchemaFromAbc::has(Property p) const
{
    return (_flags & p) != 0;
}

const float*
scn::PointsSchemaFromAbc::lockPositions(size_t& numPositions) const
{
    return lockPositions(_base->currentSampleIdx(), numPositions);
}

const float*
scn::PointsSchemaFromAbc::lockPositions(int     sampleIdx,
                                        size_t& numPositions) const
{
    numPositions = 0;

    assert(numSamples() > 0);
    assert(sampleIdx >= 0 && sampleIdx < static_cast<int>(numSamples()));
    assert(_points.getPositionsProperty().valid());

    if (_locks->positions)
    {
        std::stringstream sstr;
        sstr
            << "Previous positions sample has not been properly unlocked "
            << "for points '" << _object.getName() << "'.";
        throw sys::Exception(sstr.str().c_str(), "Points Positions Getter");
    }

    Alembic::Abc::P3fArraySamplePtr const& data = _points.getPositionsProperty()
        .getValue(Alembic::Abc::ISampleSelector(static_cast<Alembic::Abc::index_t>(sampleIdx)));

    if (data == nullptr ||
        data->size() == 0)
        return nullptr;

    _locks->positions   = data;
    numPositions        = _locks->positions->size();

    return reinterpret_cast<const float*>(_locks->positions->getData());
}

void
scn::PointsSchemaFromAbc::unlockPositions() const
{
    _locks->positions   = nullptr;
}

const float*
scn::PointsSchemaFromAbc::lockWidths(size_t& numWidths) const
{
    return lockWidths(_base->currentSampleIdx(), numWidths);
}

const float*
scn::PointsSchemaFromAbc::lockWidths(int        sampleIdx,
                                     size_t&    numWidths) const
{
    numWidths = 0;

    assert(numSamples() > 0);
    assert(sampleIdx >= 0 && sampleIdx < static_cast<int>(numSamples()));

    if (!has(Property::PROP_WIDTH))
        return nullptr;
    if (_locks->widths)
    {
        std::stringstream sstr;
        sstr
            << "Previous widths sample has not been properly unlocked "
            << "for points '" << _object.getName() << "'.";
        throw sys::Exception(sstr.str().c_str(), "Points Widths Getter");
    }

    assert(_points.getWidthsParam().valid());
    assert(_points.getWidthsParam().getValueProperty().valid());
    //assert(_points.getWidthsParam().getScope() == AbcGeom::kConstantScope || _points.getWidthsParam().getScope() == AbcGeom::kVertexScope);

    Alembic::Abc::FloatArraySamplePtr const& data = _points.getWidthsParam().getValueProperty()
        .getValue(Alembic::Abc::ISampleSelector(static_cast<Alembic::Abc::index_t>(sampleIdx)));

    if (data == nullptr ||
        data->size() == 0)
        return nullptr;

    _locks->widths  = data;
    numWidths       = _locks->widths->size();

    return reinterpret_cast<const float*>(_locks->widths->getData());
}

void
scn::PointsSchemaFromAbc::unlockWidths() const
{
    _locks->widths = nullptr;
}

void
scn::PointsSchemaFromAbc::getSelfBounds(xchg::BoundingBox& bounds) const
{
    getSelfBounds(_base->currentSampleIdx(), bounds);
}

void
scn::PointsSchemaFromAbc::getSelfBounds(int                 sampleIdx,
                                        xchg::BoundingBox&  bounds) const
{
    bounds.clear();

    assert(numSamples() > 0);
    assert(sampleIdx >= 0 && sampleIdx < static_cast<int>(numSamples()));

    if (!has(Property::PROP_BOUNDS))
        return;

    assert(_points.getSelfBoundsProperty().valid());

    const Alembic::Abc::Box3d& box = _points.getSelfBoundsProperty()
        .getValue(Alembic::Abc::ISampleSelector(static_cast<Alembic::Abc::index_t>(sampleIdx)));

    if (box.isEmpty())
        return;

    const Alembic::Abc::V3d& min = box.min;
    const Alembic::Abc::V3d& max = box.max;

    bounds.minX = static_cast<float>(min.x);
    bounds.minY = static_cast<float>(min.y);
    bounds.minZ = static_cast<float>(min.z);
    bounds.maxX = static_cast<float>(max.x);
    bounds.maxY = static_cast<float>(max.y);
    bounds.maxZ = static_cast<float>(max.z);
}

// static
bool
scn::PointsSchemaFromAbc::checkBounds(const Alembic::AbcGeom::IPointsSchema& points)
{
    if (!points.getSelfBoundsProperty().valid() ||
        !points.getPositionsProperty().valid())
        return false;
    else if (points.getSelfBoundsProperty().getTimeSampling() != points.getPositionsProperty().getTimeSampling())
        return false;
    else
        return true;
}

// static
bool
scn::PointsSchemaFromAbc::checkWidths(const Alembic::AbcGeom::IPointsSchema& points)
{
    if (!points.getWidthsParam().valid() ||
        !points.getPositionsProperty().valid())
        return false;
    else if (points.getWidthsParam().getTimeSampling() != points.getPositionsProperty().getTimeSampling())
        return false;
    //else if (points.getWidthsParam().getScope() != AbcGeom::GeometryScope::kConstantScope &&
    //  points.getWidthsParam().getScope() != AbcGeom::kVertexScope)
    //{
    //  std::cerr
    //      << "Points '" << points.getObject().getFullName() << "' must have its widths be constant or specified per vertex (found "
    //      << std::to_string(points.getWidthsParam().getScope()) << "). Skipping widths."
    //      << std::endl;
    //  return false;
    //}
    else if (points.getWidthsParam().getTimeSampling() != points.getPositionsProperty().getTimeSampling())
    {
        std::cerr
            << "Points '" << points.getObject().getFullName() << "' "
            << "must have its widths property synchronized with its positions property. "
            << "Skipping widths." << std::endl;
        return false;
    }
    else
        return true;
}
