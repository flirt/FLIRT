// ========================================================================= //
// Copyright 2016-2018 SUPAMONKS_STUDIO                                      //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
//                                                                           //
// ========================================================================= //
#include <ImathVec.h>
#include "CubicBezierArcLength.hpp"

namespace smks { namespace util { namespace math
{
    static
    float
    compute(const Imath::V3f P[4], float err, size_t maxIter);

    static
    void
    split(const Imath::V3f P[4], Imath::V3f Pleft[4], Imath::V3f Pright[4]);

    static
    void
    addIfClose(const Imath::V3f P[4], float err, int iter, float&);
}
}
}

using namespace smks;

// static
float
util::math::CubicBezierArcLength::compute(const float*  p0,
                                          const float*  p1,
                                          const float*  p2,
                                          const float*  p3,
                                          float         err,
                                          size_t        maxIter)
{
    Imath::V3f  P[4] = {
        Imath::V3f(p0[0], p0[1], p0[2]),
        Imath::V3f(p1[0], p1[1], p1[2]),
        Imath::V3f(p2[0], p2[1], p2[2]),
        Imath::V3f(p3[0], p3[1], p3[2])
    };

    return smks::util::math::compute(P, err, maxIter);
}

// static
float
util::math::compute(const Imath::V3f    P[4],
                    float               err,
                    size_t              maxIter)
{
    float arcLength = 0.0f;

    addIfClose(P, err, static_cast<int>(maxIter - 1), arcLength);

    return arcLength;
}

// static
void
util::math::split(const Imath::V3f  P[4],
                  Imath::V3f        Pleft[4],
                  Imath::V3f        Pright[4])
{
    // triangle computation
    const Imath::V3f& P_10 = 0.5f * (P[0] + P[1]);
    const Imath::V3f& P_11 = 0.5f * (P[1] + P[2]);
    const Imath::V3f& P_12 = 0.5f * (P[2] + P[3]);

    const Imath::V3f& P_20 = 0.5f * (P_10 + P_11);
    const Imath::V3f& P_21 = 0.5f * (P_11 + P_12);

    const Imath::V3f& P_30 = 0.5f * (P_20 + P_21);

    Pleft[0] = P[0];
    Pleft[1] = P_10;
    Pleft[2] = P_20;
    Pleft[3] = P_30;

    Pright[0] = P_30;
    Pright[1] = P_21;
    Pright[2] = P_12;
    Pright[3] = P[3];
}

// static
void
util::math::addIfClose(const Imath::V3f P[4],
                       float            err,
                       int              iter,
                       float&           arcLength)
{
    if (iter < 0)
        return;

    const float chordLength = (P[3]-P[0]).length();
    const float arcLength_  = (P[1]-P[0]).length() + (P[2]-P[1]).length() + (P[3]-P[2]).length();

    if ((arcLength - chordLength) > err)
    {
        Imath::V3f  Pleft[4];
        Imath::V3f  Pright[4];

        split(P, Pleft, Pright);
        addIfClose(Pleft, err, iter-1, arcLength);
        addIfClose(Pright, err, iter-1, arcLength);
    }
    else
    {
        arcLength += arcLength_;
    }
}
