// ========================================================================= //
// Copyright 2016-2018 SUPAMONKS_STUDIO                                      //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
//                                                                           //
// ========================================================================= //
#pragma once

#include <vector>
#include <ImathVec.h>

namespace smks { namespace util { namespace math
{
    // WARNING: The converter does not own any data.
    class BSplineToCubicBezierConverter
    {
    public:
        static
        void
        getIndexBuffer(size_t                       order,  // = 2 (linear), = 4 (cubic)
                       size_t                       numBSplines,
                       const int*                   bSplineNumVertices,
                       const float*                 bSplineKnots,
                       size_t&                      numBezierVertices,
                       std::vector<size_t>&         numBezierCurvesPerBSpline,
                       std::vector<unsigned int>&   indexBuffer);

        static
        void
        getVertexBuffer(size_t          order,  // = 2 (linear), = 4 (cubic)
                        size_t          numBSplines,
                        const int*      bsplineNumVertices,
                        const float*    bsplineKnots,
                        const float*    bsplineVertices,
                        size_t          bsplineStride, // in floats
                        float*          bezierVertices,
                        size_t          bezierStride); // in floats

        static
        void
        getWidthBuffer(size_t       order,  // = 2 (linear), = 4 (cubic)
                       size_t       numBSplines,
                       const int*   bsplineNumWidths,
                       const float* bsplineKnots,
                       const float* bsplineWidths,
                       size_t       bsplineStride, // in floats
                       float*       bezierWidths,
                       size_t       bezierStride); // in floats
    };
}
}
}
