// ========================================================================= //
// Copyright 2016-2018 SUPAMONKS_STUDIO                                      //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
//                                                                           //
// ========================================================================= //
#include <cassert>
#include <sstream>

#include "BSplineToCubicBezierConverter.hpp"

#include <smks/sys/Log.hpp>
#include <smks/sys/Exception.hpp>

#define ORDER_LINEAR 2
#define ORDER_CUBIC 4

namespace smks { namespace util { namespace math
{
    enum BSplineType
    {
        UNSUPPORTED=-1,
        LINEAR_UNIFORM, // all internal knots have constant spacing
        CUBIC_UNIFORM,  // all internal knots have constant spacing
        CUBIC_BEZIER    // all internal knots have 3 multiplicity
    };

    static
    BSplineType
    getBSplineType(size_t order, size_t numCVs, const float* knots, size_t& numBezierCurves);

    template<unsigned NumFloats> static
    void
    convertData(size_t          order,
                size_t          numBSplines,
                const int*      bsplineNumVertices,
                const float*    bsplineKnots,
                const float*    bsplineData,
                size_t          bsplineStride, // in floats
                float*          bezierData,
                size_t          bezierStride);

    template<unsigned NumFloats> static
    void
    interpBezierDataFromUniformBSpline(
        const float Paaa[NumFloats],
        const float Paab[NumFloats],
        const float Pabc[NumFloats],
        const float Pbcd[NumFloats],
        float       Pabb[NumFloats],
        float       Pbbc[NumFloats],
        float       Pbbb[NumFloats]);
    template<unsigned NumFloats> static
    void
    copy(float dst[NumFloats], const float src[NumFloats]);

    static
    bool
    increasing(size_t, const float*);
}
}
}

using namespace smks;

// static
void
util::math::BSplineToCubicBezierConverter::getIndexBuffer(
    size_t                      order,
    size_t                      numBsplines,
    const int*                  bsplineNumVertices,
    const float*                bsplineKnots,
    size_t&                     numBezierVertices,
    std::vector<size_t>&        numBezierCurvesPerBSpline,
    std::vector<unsigned int>&  indexBuffer)
{
    numBezierVertices = 0;

    numBezierCurvesPerBSpline.clear();
    numBezierCurvesPerBSpline.resize(numBsplines, 0);

    indexBuffer.clear();
    indexBuffer.reserve(1024);

    const int*      curNumBSVertices    = bsplineNumVertices;
    const float*    curKnots            = bsplineKnots;

#if defined(FLIRT_LOG_ENABLED)
    size_t totalNumBZCurves = 0;
#endif // defined(FLIRT_LOG_ENABLED)

    //-----------------------
    // for all B-spline paths
    for (size_t i = 0; i < numBsplines; ++i)
    {
        const int       numBSVertices   = *curNumBSVertices;
        const size_t    numKnots        = numBSVertices + order;

        const BSplineType type = getBSplineType(
            order, numBSVertices, curKnots, numBezierCurvesPerBSpline[i]);

        // for each bezier curve, store index of 1st control point
        for (size_t j = 0; j < numBezierCurvesPerBSpline[i]; ++j)
        {
            indexBuffer.push_back(static_cast<unsigned int>(numBezierVertices));
            numBezierVertices += (order == ORDER_CUBIC ? 3 : 2);
        }
        numBezierVertices += (order == ORDER_CUBIC ? 1 : 2);

#if defined(FLIRT_LOG_ENABLED)
        totalNumBZCurves += numBezierCurvesPerBSpline[i];
#endif // defined(FLIRT_LOG_ENABLED)
        ++curNumBSVertices;
        curKnots += numKnots;
    }
    //-----------------------

#if defined(FLIRT_LOG_ENABLED)
    LOG(DEBUG)
        << order << "-order B-spline index buffer initialization:\n"
        << "\t- # bezier vertices = " << numBezierVertices << "\n"
        << "\t- # bezier curves = " << totalNumBZCurves;
#endif // defined(FLIRT_LOG_ENABLED)

    indexBuffer.shrink_to_fit();
}

// static
void
util::math::BSplineToCubicBezierConverter::getVertexBuffer(
    size_t          order,
    size_t          numBsplines,
    const int*      bsplineNumVertices,
    const float*    bsplineKnots,
    const float*    bsplineVertices,
    size_t          bsplineStride,  // in floats
    float*          bezierVertices,
    size_t          bezierStride)   // in floats
{
    convertData<3>(
        order,
        numBsplines,
        bsplineNumVertices,
        bsplineKnots,
        bsplineVertices,
        bsplineStride,
        bezierVertices,
        bezierStride);
}

// static
void
util::math::BSplineToCubicBezierConverter::getWidthBuffer(
    size_t          order,
    size_t          numBsplines,
    const int*      bsplineNumWidths,
    const float*    bsplineKnots,
    const float*    bsplineWidths,
    size_t          bsplineStride,  // in floats
    float*          bezierWidths,
    size_t          bezierStride)   // in floats
{
    convertData<1>(
        order,
        numBsplines,
        bsplineNumWidths,
        bsplineKnots,
        bsplineWidths,
        bsplineStride,
        bezierWidths,
        bezierStride);
}

template<unsigned NumFloats> // static
void
util::math::convertData(
    size_t          order,
    size_t          numBsplines,
    const int*      bsplineNumVertices,
    const float*    bsplineKnots,
    const float*    bsplineVertices,
    size_t          bsplineStride,  // in floats
    float*          bezierVertices,
    size_t          bezierStride)   // in floats
{
    if (numBsplines == 0 ||
        bsplineNumVertices == nullptr ||
        bsplineKnots == nullptr ||
        bsplineVertices == nullptr ||
        bezierVertices == nullptr)
        return;
    assert(bsplineStride > 0);
    assert(bezierStride > 0);

    // inputs
    const int*      curNumBSVertices    = bsplineNumVertices;
    const float*    curKnots            = bsplineKnots;
    const float*    curBSVertex         = bsplineVertices;
    // outputs
    size_t  totalNumBZCurves    = 0;
    float*  curBZVertex         = bezierVertices;

    for (size_t i = 0; i < numBsplines; ++i)
    {
        const size_t    numBSVertices   = static_cast<size_t>(*curNumBSVertices);
        const size_t    numKnots        = numBSVertices + order;
        size_t          numBZCurves     = 0;

        const BSplineType type = getBSplineType(
            order, numBSVertices, curKnots, numBZCurves);

        totalNumBZCurves += numBZCurves;
        //-----------------------
        if (type == CUBIC_BEZIER)
        {
            assert(4 <= numBSVertices && (numBSVertices - 1) % 3 == 0);

            for (size_t n = 0; n < numBSVertices; ++n)
            {
                copy<NumFloats>(curBZVertex, curBSVertex);  curBZVertex += bezierStride;
                //---
                curBSVertex += bsplineStride;
            }
        }
        //------------------------------
        else if (type == LINEAR_UNIFORM)
        {
            assert(numBSVertices == numBZCurves + 1);

            for (size_t n = 0; n < numBSVertices; ++n)
            {
                copy<NumFloats>(curBZVertex, curBSVertex);  curBZVertex += bezierStride;
                copy<NumFloats>(curBZVertex, curBSVertex);  curBZVertex += bezierStride;
                //---
                curBSVertex += bsplineStride;
            }
        }
        //-----------------------------
        else if (type == CUBIC_UNIFORM)
        {
            assert(4 < numBSVertices);  // if numBSVertices == 4 then BEZIER

            size_t n = 0;   // number of processed B-spline positions
            // B-spline data
            float Paaa[NumFloats];
            float Paab[NumFloats];
            float Pabc[NumFloats];
            float Pbcd[NumFloats];
            // intermediary Bezier data
            float Pabb[NumFloats];
            float Pbbc[NumFloats];
            float Pbbb[NumFloats];

            copy<NumFloats>(Paaa, curBSVertex); curBSVertex += bsplineStride; ++n;
            copy<NumFloats>(Paab, curBSVertex); curBSVertex += bsplineStride; ++n;
            copy<NumFloats>(Pabc, curBSVertex); curBSVertex += bsplineStride; ++n;
            copy<NumFloats>(Pbcd, curBSVertex); curBSVertex += bsplineStride; ++n;

            copy<NumFloats>(curBZVertex, Paaa); curBZVertex += bezierStride;
            while (true)
            {
                interpBezierDataFromUniformBSpline<NumFloats>(
                    Paaa, Paab, Pabc, Pbcd,
                    Pabb, Pbbc, Pbbb);

                copy<NumFloats>(curBZVertex, Paab); curBZVertex += bezierStride;
                copy<NumFloats>(curBZVertex, Pabb); curBZVertex += bezierStride;

                if (n == numBSVertices - 1)
                    break;

                copy<NumFloats>(curBZVertex, Pbbb); curBZVertex += bezierStride;

                copy<NumFloats>(Paaa, Pbbb);
                copy<NumFloats>(Paab, Pbbc);
                copy<NumFloats>(Pabc, Pbcd);
                copy<NumFloats>(Pbcd, curBSVertex); curBSVertex += bsplineStride; ++n;
            }

            // the B-spline is clamped on both sides, finish last Bezier segment.
            const float* ptr = curBSVertex;
            copy<NumFloats>(Paaa, curBSVertex); curBSVertex += bsplineStride; ++n;
            ptr -= bsplineStride; copy<NumFloats>(Paab, ptr); // look at previous vertices
            ptr -= bsplineStride; copy<NumFloats>(Pabc, ptr);
            ptr -= bsplineStride; copy<NumFloats>(Pbcd, ptr);

            interpBezierDataFromUniformBSpline<NumFloats>(
                Paaa, Paab, Pabc, Pbcd,
                Pabb, Pbbc, Pbbb);

            copy<NumFloats>(curBZVertex,    Pbbb);  curBZVertex += bezierStride;
            copy<NumFloats>(curBZVertex,    Pabb);  curBZVertex += bezierStride;
            copy<NumFloats>(curBZVertex,    Paab);  curBZVertex += bezierStride;
            copy<NumFloats>(curBZVertex,    Paaa);  curBZVertex += bezierStride;
        }
        //-----------------------------
        else
        {
            // fails to convert to bezier segments
            std::stringstream sstr;
            sstr
                << "Unsupported " << order << "-order B-spline knot sequence\n"
                << "\t- " << numBSVertices << " control points\n"
                << "\t- knots = { ";
            for (int i = 0; i < numKnots; ++i)
                sstr << curKnots[i] << " ";
            sstr << "}";
            FLIRT_LOG(LOG(DEBUG) << sstr.str().c_str();)
            std::cerr << sstr.str() << std::endl;
            //---
            curBSVertex += bsplineStride * numBSVertices;
        }

        ++curNumBSVertices;
        curKnots += numKnots;
    }

    FLIRT_LOG(LOG(DEBUG)
        << ":\n"
        << "\t- # bezier vertices = " << static_cast<int>(curBZVertex - bezierVertices) / bezierStride << "\n"
        << "\t- # bezier curves = " << totalNumBZCurves;)
}

template<unsigned NumFloats> static
void
util::math::interpBezierDataFromUniformBSpline(
    const float Paaa[NumFloats],
    const float Paab[NumFloats],
    const float Pabc[NumFloats],
    const float Pbcd[NumFloats],
    float       Pabb[NumFloats],
    float       Pbbc[NumFloats],
    float       Pbbb[NumFloats])
{
    for (unsigned k = 0; k < NumFloats; ++k)
    {
        Pabb[k] = 0.5f * (Paab[k] + Pabc[k]);
        Pbbc[k] = 0.66666666667f * Pabc[k] + 0.33333333333f * Pbcd[k];
        Pbbb[k] = 0.5f * (Pabb[k] + Pbbc[k]);
    }
}

template<unsigned NumFloats> // static
void
util::math::copy(float dst[NumFloats], const float src[NumFloats])
{
    memcpy(dst, src, sizeof(float) * NumFloats);
}

// static
util::math::BSplineType
util::math::getBSplineType(
    size_t          order,
    size_t          numCVs,
    const float*    knots,
    size_t&         numBezierCurves)
{
    assert(1 <= order && order <= numCVs);
    assert(knots);
    // # knots = # vertices + order
    assert(increasing(numCVs + order, knots));

    numBezierCurves = 0;
    if (order != ORDER_LINEAR &&
        order != ORDER_CUBIC)
        return UNSUPPORTED;

    // make sure the B-spline interpolates both ends
    bool interpolateBothEnds = true;

    for (size_t i = 1; i < order; ++i)
        interpolateBothEnds = interpolateBothEnds &&
            (fabsf(knots[i] - knots[i - 1]) < 1e-4f) &&
            (fabsf(knots[numCVs + i] - knots[numCVs + i - 1]) < 1e-4f);
    if (!interpolateBothEnds)
        return UNSUPPORTED;

    const size_t numInternalKnots = numCVs - order;

    // returns asap if there are no internal knot
    if (numInternalKnots == 0)
    {
        numBezierCurves = 1;
        return order == ORDER_CUBIC
            ? CUBIC_BEZIER
            : UNSUPPORTED;
    }

    assert(numCVs > order);

    // check whether the B-spline is uniform
    bool uniform = true;

    const float first   = knots[order - 1];
    const float last    = knots[numCVs];
    const float step    = (last - first) / static_cast<float>(numInternalKnots + 1);
    for (size_t i = order - 1; i < numCVs - 1; ++i)
        uniform = uniform &&
            fabsf((knots[i + 1] - knots[i]) - step) < 1e-4f;
    if (uniform)
    {
        numBezierCurves = ORDER_CUBIC
            ? numInternalKnots + 1
            : numInternalKnots;
        return order == ORDER_CUBIC
            ? CUBIC_UNIFORM
            : LINEAR_UNIFORM;
    }

    if (order == ORDER_CUBIC)
    {
        // check whether cubic b-splines are already expressed
        // in terms of bezier segments.
        bool inBezierForm = true;

        const size_t mult = ORDER_CUBIC - 1;    // multiplicity = 3
        assert(mult > 0);
        if (numInternalKnots % mult)
            inBezierForm = false;

        const size_t numChunks = numInternalKnots / mult;
        size_t offset = order;
        for (size_t c = 0; c < numChunks; ++c, offset += mult)
            for (size_t i = 1; i < mult; ++i)
                inBezierForm = inBezierForm &&
                    fabsf(knots[offset + i] - knots[offset]) < 1e-4f;
        if (inBezierForm)
        {
            numBezierCurves = numChunks + 1;
            return CUBIC_BEZIER;
        }
    }
    return UNSUPPORTED;
}

// static
bool
util::math::increasing(size_t num, const float* values)
{
    for (size_t i = 0; i < num - 1; ++i)
        if (values[i] > values[i+1])
            return false;
    return true;
}

