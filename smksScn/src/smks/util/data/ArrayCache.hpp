// ========================================================================= //
// Copyright 2016-2018 SUPAMONKS_STUDIO                                      //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
//                                                                           //
// ========================================================================= //
#pragma once

#include <cassert>
#include <vector>
#include <boost/unordered_map.hpp>

#include <smks/sys/Exception.hpp>
#include <smks/xchg/Vector.hpp>
#include "IsCacheEntryDirty.hpp"
#include "DirtyEntry.hpp"
#include "UpdateEntry.hpp"

namespace smks { namespace util { namespace data
{
    template<typename T, typename P = IsCacheEntryDirty<T>, typename C = DirtyEntry<T> >
    class ArrayCache
    {
    public:
        typedef T   Value;
        typedef P   IsCacheEntryDirty;
        typedef C   DirtyEntry;

    private:
        enum { IGNORE_LAST = -2, UNSPECIFED_LAST = -1 };

    private:
        IsCacheEntryDirty   _isDirty;
        DirtyEntry          _dirty;
        xchg::Vector<Value> _entries;   //<! aligned allocation (64)
        int                 _last;      //<! useful only if caching deactivated

    public:
        inline
        ArrayCache();

        inline
        void
        dispose();
        // dispose() will NOT dirty entries.
        // important if dirty function is to deallocate memory used by entries.

        inline
        bool
        dirty(size_t, const DirtyEntry* = nullptr);

        inline
        void
        dirtyAll(const DirtyEntry* = nullptr);

        inline
        void
        updateAll(const UpdateEntry<T>* = nullptr);

        inline
        void
        initialize(size_t, const DirtyEntry* = nullptr);

        inline
        const Value*
        get(size_t, const IsCacheEntryDirty* = nullptr) const;

        inline
        const Value*
        set(size_t, const T&);

        inline
        bool
        caching() const;

        inline
        bool
        empty() const;
    };
}
}
#include "ArrayCache-inl.hpp"
}
