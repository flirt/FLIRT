// ========================================================================= //
// Copyright 2016-2018 SUPAMONKS_STUDIO                                      //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
//                                                                           //
// ========================================================================= //

template<typename T, typename P, typename C> inline
util::data::ArrayCache<T,P,C>::ArrayCache():
    _isDirty    (),
    _entries    (),
    _last       (IGNORE_LAST)
{ }

template<typename T, typename P, typename C> inline
void
util::data::ArrayCache<T,P,C>::dispose()
{
    _entries.clear();
    _last = IGNORE_LAST;
}

template<typename T, typename P, typename C> inline
bool
util::data::ArrayCache<T,P,C>::dirty(size_t idx, const DirtyEntry* dirty)
{
    if (idx >= _entries.size())
        return false;

    if (dirty)
        (*dirty)(_entries[idx]);
    else
        _dirty(_entries[idx]);

    return true;
}

template<typename T, typename P, typename C> inline
void
util::data::ArrayCache<T,P,C>::dirtyAll(const DirtyEntry* dirty)
{
    for (size_t i = 0; i < _entries.size(); ++i)
        if (dirty)
            (*dirty)(_entries[i]);
        else
            _dirty(_entries[i]);
}

template<typename T, typename P, typename C> inline
void
util::data::ArrayCache<T,P,C>::updateAll(const UpdateEntry<T>* update)
{
    if (update == nullptr)
        return;

    for (size_t i = 0; i < _entries.size(); ++i)
        (*update)(_entries[i]);
}

template<typename T, typename P, typename C> inline
bool
util::data::ArrayCache<T,P,C>::caching() const
{
    return _last == IGNORE_LAST;
}

template<typename T, typename P, typename C> inline
void
util::data::ArrayCache<T,P,C>::initialize(size_t size, const DirtyEntry* dirty)
{
    dispose();
    if (size == 0)
    {
        // caching deactivated
        _entries.resize(1);
        _last = UNSPECIFED_LAST;
    }
    else
    {
        _entries.resize(size);
        dirtyAll(dirty);
    }
}

template<typename T, typename P, typename C> inline
const typename util::data::ArrayCache<T,P,C>::Value*
util::data::ArrayCache<T,P,C>::get(size_t                   index,
                                   const IsCacheEntryDirty* isDirty) const
{
    assert(!_entries.empty()); // initialize() has not been called yet
    if (caching())
    {
        return index < _entries.size() &&
            (isDirty
                ? (*isDirty)(_entries[index])
                : _isDirty(_entries[index]))
            ? nullptr
            : &_entries[index];
    }
    else if (index == _last)
        return (isDirty
                ? (*isDirty)(_entries.front())
                : _isDirty(_entries.front()))
            ? nullptr
            : &_entries.front();
    else
        return nullptr;
}

template<typename T, typename P, typename C> inline
const typename util::data::ArrayCache<T,P,C>::Value*
util::data::ArrayCache<T,P,C>::set(size_t       index,
                                   const Value& value)
{
    assert(!_entries.empty()); // initialize() has not been called yet
    if (caching())
    {
        assert(index < _entries.size());
        _entries[index] = value;

        return &_entries[index];
    }
    else
    {
        _entries.front() = value;
        _last = static_cast<int>(index);

        return &_entries.front();
    }
}

template<typename T, typename P, typename C> inline
bool
util::data::ArrayCache<T,P,C>::empty() const
{
    return _entries.empty();
}
