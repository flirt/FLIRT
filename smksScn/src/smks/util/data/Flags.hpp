// ========================================================================= //
// Copyright 2016-2018 SUPAMONKS_STUDIO                                      //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
//                                                                           //
// ========================================================================= //
#pragma once

#include <cassert>
#include <iostream>
#include <vector>

namespace smks { namespace util
{
    class Flags
    {
    private:
        typedef size_t  FlagSubset;

    private:
        size_t                  _numFlags;
        std::vector<FlagSubset> _flags;

    public:
        inline
        Flags():
            _numFlags(0),
            _flags()
        {
        }

        inline
        void
        clear()
        {
            _numFlags = 0;
            _flags.clear();
            _flags.shrink_to_fit();
        }

        inline
        void
        set(bool value)
        {
            if (!_flags.empty())
                memset(&_flags.front(), value ? -1 : 0, sizeof(FlagSubset) * _flags.size());
        }

        inline
        void
        resize(size_t size, bool value = false)
        {
            _numFlags = size;
            _flags.resize(1 + (size / (sizeof(FlagSubset) << 3)));
            set(value);
        }

        inline
        size_t
        size() const
        {
            return _numFlags;
        }

        inline
        void
        set(int i, bool value)
        {
            assert(i >= 0 && i < _numFlags);

            const div_t j = div(i, static_cast<int>(sizeof(FlagSubset) << 3));
            assert(j.quot < _flags.size());

            _flags[j.quot] = value
                ? _flags[j.quot] | (size_t(1) << j.rem)
                : _flags[j.quot] & ~(size_t(1) << j.rem);
        }

        inline
        bool
        get(int i) const
        {
            assert(i >= 0 && i < _numFlags);

            const div_t j = div(i, static_cast<int>(sizeof(FlagSubset) << 3));
            assert(j.quot < _flags.size());

            return ((_flags[j.quot] >> j.rem) & 0x1) != 0 ? true : false;
        }

        inline friend
        std::ostream&
        operator<<(std::ostream& out, const Flags& f)
        {
            out << "[ ";
            for (size_t i = 0; i < f.size(); ++i)
            {
                if (i > 0 && i < f.size()-1)
                {
                    if (i % 100 == 0)
                        out << " | ";
                    else if (i % 50 == 0)
                        out << " : ";
                    else if (i % 10 == 0)
                        out << ".";
                }
                out << (f.get(static_cast<int>(i)) ? "1" : "0");
            }
            out << " ]";

            return out;
        }

    };
}
}
