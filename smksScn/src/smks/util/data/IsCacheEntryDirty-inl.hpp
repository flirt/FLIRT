// ========================================================================= //
// Copyright 2016-2018 SUPAMONKS_STUDIO                                      //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
//                                                                           //
// ========================================================================= //

#include <smks/xchg/BoundingBox.hpp>

template<typename S>
struct smks::util::data::IsCacheEntryDirty<std::vector<S>>:
    public std::unary_function<std::vector<S> const&, bool>
{
    virtual inline
    bool
    operator()(std::vector<S> const& x) const
    {
        return x.empty();
    }
};

template<typename S>
struct smks::util::data::IsCacheEntryDirty<const S*>:
    public std::unary_function<const S* const&, bool>
{
    virtual inline
    bool
    operator()(const S* const& x) const
    {
        return x == nullptr;
    }
};

template<typename S>
struct smks::util::data::IsCacheEntryDirty<S*>:
    public std::unary_function<S* const&, bool>
{
    virtual inline
    bool
    operator()(S* const& x) const
    {
        return x == nullptr;
    }
};

template<>
struct smks::util::data::IsCacheEntryDirty<smks::xchg::BoundingBox>:
    public std::unary_function<smks::xchg::BoundingBox const&, bool>
{
    virtual inline
    bool
    operator()(smks::xchg::BoundingBox const& x) const
    {
        return !x.valid();
    }
};
