// ========================================================================= //
// Copyright 2016-2018 SUPAMONKS_STUDIO                                      //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
//                                                                           //
// ========================================================================= //

#include <smks/xchg/BoundingBox.hpp>

template<typename S>
struct smks::util::data::DirtyEntry<std::vector<S>>:
    public std::unary_function<std::vector<S>&, void>
{
    virtual inline
    void
    operator()(std::vector<S>& x) const
    {
        x.clear();
        x.shrink_to_fit();
    }
};

template<typename S>
struct smks::util::data::DirtyEntry<const S*>:
    public std::unary_function<const S*&, void>
{
    virtual inline
    void
    operator()(const S*& x) const
    {
        delete x;
        x = nullptr;
    }
};

template<typename S>
struct smks::util::data::DirtyEntry<S*>:
    public std::unary_function<S*&, void>
{
    virtual inline
    void
    operator()(S*& x) const
    {
        delete x;
        x = nullptr;
    }
};

template<>
struct smks::util::data::DirtyEntry<smks::xchg::BoundingBox>:
    public std::unary_function<smks::xchg::BoundingBox&, void>
{
    virtual inline
    void
    operator()(smks::xchg::BoundingBox& x) const
    {
        x.clear();
    }
};
