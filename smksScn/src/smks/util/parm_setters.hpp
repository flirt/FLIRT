// ========================================================================= //
// Copyright 2016-2018 SUPAMONKS_STUDIO                                      //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
//                                                                           //
// ========================================================================= //
#pragma once

#include <smks/scn/TreeNode.hpp>

namespace smks { namespace util
{

template<typename ValueType> inline
ValueType&
set(ValueType& member, scn::TreeNode::Ptr const& node, const char* parmName, const ValueType& parmValue)
{
    member = parmValue;
    if (node)
        node->setParameter(parmName, parmValue);
    return member;
}

inline
unsigned int&
setId(unsigned int& member, scn::TreeNode::Ptr const& node, const char* parmName, unsigned int id)
{
    member = id;
    if (node)
        node->setParameterId(parmName, id);
    return member;
}

template<typename ValueType> inline
ValueType&
update(ValueType& member, scn::TreeNode::Ptr const& node, unsigned int parmId, const ValueType& parmValue)
{
    member = parmValue;
    if (node)
        node->updateParameter(parmId, parmValue);
    return member;
}

inline
unsigned int&
updateId(unsigned int& member, scn::TreeNode::Ptr const& node, unsigned int parmId, unsigned int id)
{
    member = id;
    if (node)
        node->updateParameterId(parmId, id);
    return member;
}

}
}
