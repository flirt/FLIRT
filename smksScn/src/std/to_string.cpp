// ========================================================================= //
// Copyright 2016-2018 SUPAMONKS_STUDIO                                      //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
//                                                                           //
// ========================================================================= //
#include "to_string.hpp"

using namespace Alembic;

std::string
std::to_string(Alembic::AbcGeom::GeometryScope scope)
{
    switch (scope)
    {
    case Alembic::AbcGeom::v7::kConstantScope:
        return "constant scope";
    case Alembic::AbcGeom::v7::kUniformScope:
        return "uniform scope";
    case Alembic::AbcGeom::v7::kVaryingScope:
        return "varying scope";
    case Alembic::AbcGeom::v7::kVertexScope:
        return "vertex scope";
    case Alembic::AbcGeom::v7::kFacevaryingScope:
        return "face varying scope";
    case Alembic::AbcGeom::v7::kUnknownScope:
    default:
        return "unknown scope";
    }
}

std::string
std::to_string(AbcGeom::CurveType type)
{
    switch (type)
    {
    case Alembic::AbcGeom::v7::kCubic:
        return "cubic";
    case Alembic::AbcGeom::v7::kLinear:
        return "linear";
    case Alembic::AbcGeom::v7::kVariableOrder:
        return "variable order";
    default:
        return "undefined";
    }
}

std::string
std::to_string(AbcGeom::BasisType type)
{
    switch (type)
    {
    case Alembic::AbcGeom::v7::kNoBasis:
        return "none";
    case Alembic::AbcGeom::v7::kBezierBasis:
        return "Bezier";
    case Alembic::AbcGeom::v7::kBsplineBasis:
        return "B-spline";
    case Alembic::AbcGeom::v7::kCatmullromBasis:
        return "Catmull-Rom";
    case Alembic::AbcGeom::v7::kHermiteBasis:
        return "Hermite";
    case Alembic::AbcGeom::v7::kPowerBasis:
        return "power";
    default:
        return "undefined";
    }
}

std::string
std::to_string(Alembic::AbcGeom::CurvePeriodicity wrap)
{
    switch (wrap)
    {
    case Alembic::AbcGeom::v7::kNonPeriodic:
        return "non-periodic";
    case Alembic::AbcGeom::v7::kPeriodic:
        return "periodic";
    default:
        return "undefined";
    }
}

std::string
std::to_string(const Alembic::AbcGeom::ICurvesSchema::Sample& sample)
{
    if (!sample.valid())
        return "invalid curve set";

    const AbcGeom::CurveType type   = sample.getType();
    const AbcGeom::BasisType basis  = sample.getBasis();

    unsigned int order = 0;
    switch (type)
    {
    case AbcGeom::CurveType::kLinear:
        order = 2;
        break;

    case AbcGeom::CurveType::kCubic:
        order = 4;
        break;

    default:
        return "unsupported variable-order curve set order";
    }

    if (sample.getCurvesNumVertices()->size() == 0)
        return "no curve in set";

    if (sample.getPositions()->size() == 0)
        return "no positions in curve set";

    const unsigned int  numPaths        = static_cast<unsigned int>(sample.getNumCurves());
    const int*          numPathVertices = &(*sample.getCurvesNumVertices())[0];
    const Abc::V3f*     positions       = &(*sample.getPositions())[0];
    const unsigned int  numKnots        = sample.getKnots() ? static_cast<unsigned int>(sample.getKnots()->size()) : 0;
    const float*        knots           = numKnots > 0 ? &(*sample.getKnots())[0] : nullptr;

    std::stringstream   sstr;

    for (unsigned int i = 0; i < numPaths; ++i)
    {
        sstr << std::to_string(basis) << " path [" << (i+1) << "/" << numPaths << "]\n";

        const unsigned int numCurveVertices = *numPathVertices;

        if (numCurveVertices > 0)
        {
            sstr << "\t- " << numCurveVertices << " CP = { ";
            for (unsigned int j = 0; j < numCurveVertices; ++j)
                sstr << positions[j] << " ";
            sstr << "}\n";
        }
        if (numKnots > 0)
        {
            const unsigned int numCurveKnots = numCurveVertices + order;

            sstr << "\t- " << numCurveKnots << " knots = { ";
            for (unsigned int j = 0; j < numCurveKnots; ++j)
                sstr << knots[j] << " ";
            sstr << "}\n";

            knots += numCurveKnots;
        }

        ++numPathVertices;
        positions += numCurveVertices;
    }

    return sstr.str();
}
