// ========================================================================= //
// Copyright 2016-2018 SUPAMONKS_STUDIO                                      //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
//                                                                           //
// ========================================================================= //
#pragma once

#include "smks/scn/TreeNode.hpp"

namespace smks { namespace scn
{
    class Material;
    class SubSurface;
    class Texture;

    class FLIRT_SCN_EXPORT Shader:
        public TreeNode
    {
    protected:
        inline
        Shader(const char* name, TreeNode::WPtr const& parent):
            TreeNode(name, parent)
        { }

    public:
        virtual inline
        xchg::NodeClass
        nodeClass() const
        {
            return xchg::NodeClass::SHADER;
        }

        virtual inline
        Shader*
        asShader()
        {
            return this;
        }

        virtual inline
        const Shader*
        asShader() const
        {
            return this;
        }

        virtual inline
        Material*
        asMaterial()
        {
            return nullptr;
        }

        virtual inline
        const Material*
        asMaterial() const
        {
            return nullptr;
        }

        virtual inline
        SubSurface*
        asSubSurface()
        {
            return nullptr;
        }

        virtual inline
        const SubSurface*
        asSubSurface() const
        {
            return nullptr;
        }

        virtual inline
        Texture*
        asTexture()
        {
            return nullptr;
        }

        virtual inline
        const Texture*
        asTexture() const
        {
            return nullptr;
        }
    };
}
}
