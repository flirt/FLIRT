// ========================================================================= //
// Copyright 2016-2018 SUPAMONKS_STUDIO                                      //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
//                                                                           //
// ========================================================================= //
#pragma once

#include "smks/scn/TreeNode.hpp"

namespace smks { namespace scn
{
    class FLIRT_SCN_EXPORT Archive:
        public TreeNode
    {
    public:
        typedef std::shared_ptr<Archive> Ptr;

    private:
        // non-copyable
        Archive(const Archive&);
        Archive& operator=(const Archive&);

    private:
        class PImpl;

    private:
        PImpl* _pImpl;

    public:
        static
        Ptr
        create(const char* filePath, TreeNode::Ptr const& parent);

    protected:
        Archive(const char* absPath, const char* baseName, TreeNode::Ptr const& parent);

        virtual
        void
        build(TreeNode::Ptr const& self);

    public:
        ~Archive();

        const char *const
        filePath() const;

        virtual inline
        xchg::NodeClass
        nodeClass() const
        {
            return xchg::NodeClass::ARCHIVE;
        }

        virtual inline
        Archive*
        asArchive()
        {
            return this;
        }

        virtual inline
        const Archive*
        asArchive() const
        {
            return this;
        }
    };
}
}
