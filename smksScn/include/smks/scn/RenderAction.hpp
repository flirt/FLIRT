// ========================================================================= //
// Copyright 2016-2018 SUPAMONKS_STUDIO                                      //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
//                                                                           //
// ========================================================================= //
#pragma once

#include "smks/scn/GraphAction.hpp"

namespace smks { namespace scn
{
    class FLIRT_SCN_EXPORT RenderAction:
        public GraphAction
    {
    public:
        typedef std::shared_ptr<RenderAction>   Ptr;
        typedef std::weak_ptr<RenderAction>     WPtr;
    private:
        class PImpl;
        friend class PImpl;
    private:
        PImpl* _pImpl;

    public:
        static
        Ptr
        create(const char*, TreeNode::WPtr const&);

    protected:
        RenderAction(const char*, TreeNode::WPtr const&);

        virtual
        void
        build(Ptr const&);

        virtual
        void
        erase();
    public:
        ~RenderAction();

        virtual
        bool
        isParameterWritable(unsigned int) const;

        virtual
        bool
        isParameterRelevant(unsigned int)const;
    private:
        bool
        isParameterRelevantForClass(unsigned int) const;

    protected:
        virtual
        void
        handleParmEvent(const xchg::ParmEvent&);
    private:
        void
        resetIterationCount();
        void
        incrIterationCount();
    public:
        virtual inline
        xchg::NodeClass
        nodeClass() const
        {
            return xchg::NodeClass::RENDER_ACTION;
        }

        virtual inline
        RenderAction*
        asRenderAction()
        {
            return this;
        }

        virtual inline
        const RenderAction*
        asRenderAction() const
        {
            return this;
        }
    };
}
}
