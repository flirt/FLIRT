// ========================================================================= //
// Copyright 2016-2018 SUPAMONKS_STUDIO                                      //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
//                                                                           //
// ========================================================================= //
#pragma once

#include "smks/scn/ParmAssign.hpp"

namespace smks { namespace scn
    {
        class IdAssign:
            public ParmAssign<unsigned int>
        {
        public:
            typedef std::shared_ptr<IdAssign> Ptr;
        public:
            static
            Ptr
            create(const char*              parmName,
                   unsigned int             parmValue,
                   const char*              name,
                   TreeNode::WPtr const&    parent)
            {
                if (parmName == nullptr ||
                    strlen(parmName) == 0)
                    throw sys::Exception("Invalid parameter name.", "Parameter Assignment Creation");

                Ptr ptr(new IdAssign(name, parent));

                ptr->build(ptr);
                ptr->setParameter<std::string>  (parm::parmName (), parmName);
                ptr->setParameter<unsigned int> (parm::parmValue(), parmValue);

                return ptr;
            }

            //<! when visitor parameters are specified (direction and filter),
            // assignees may differ from the selection.
            static
            Ptr
            create(const char*                          parmName,
                   unsigned int                         parmValue,
                   TreeNodeVisitor::Direction           visitorDir,
                   AbstractTreeNodeFilter::Ptr const&   visitorFilter,
                   const char*                          name,
                   TreeNode::WPtr const&                parent)
            {
                if (parmName == nullptr ||
                    strlen(parmName) == 0)
                    throw sys::Exception("Invalid parameter name.", "Parameter Assignment Creation");

                Ptr ptr(new IdAssign(visitorDir, visitorFilter, name, parent));

                ptr->build(ptr);
                ptr->setParameter<std::string>  (parm::parmName (), parmName);
                ptr->setParameter<unsigned int> (parm::parmValue(), parmValue);

                return ptr;
            }

        protected:
            inline
            IdAssign(const char* name, TreeNode::WPtr const& parent):
                ParmAssign<unsigned int>(name, parent)
            { }

            inline
            IdAssign(TreeNodeVisitor::Direction         visitorDir,
                     AbstractTreeNodeFilter::Ptr const& visitorFilter,
                     const char*                        name,
                     TreeNode::WPtr const&              parent):
                ParmAssign<unsigned int>(visitorDir, visitorFilter, name, parent)
            { }

            virtual inline
            unsigned int
            createConnectionToAssignee(Scene* scene, unsigned int targetId, const char* parmName, int priority)
            {
                return scene
                    ? scene->connectId(id(), parm::parmValue().c_str(), targetId, parmName, priority)
                    : 0;
            }

            virtual inline
            void
            removeConnectionFromAssignee(Scene* scene, unsigned int cnxId)
            {
                if (scene)
                    scene->disconnectId(cnxId);
            }
        };
    }
}
