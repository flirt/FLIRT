// ========================================================================= //
// Copyright 2016-2018 SUPAMONKS_STUDIO                                      //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
//                                                                           //
// ========================================================================= //
#pragma once

#include "smks/scn/TreeNode.hpp"

namespace smks { namespace scn
{
    class Xform;
    class Camera;
    class Drawable;
    class Light;

    class FLIRT_SCN_EXPORT SceneObject:
        public TreeNode
    {
        friend class VisibilityFromAbc;
        friend class SchemaBaseFromAbc;
    public:
        typedef std::shared_ptr<SceneObject>    Ptr;
        typedef std::weak_ptr<SceneObject>      WPtr;

    protected:
        inline
        SceneObject(const char* name, TreeNode::WPtr const& parent):
            TreeNode(name, parent)
        { }
    //----------------------------------------------------------------------------------------------
    // parameter handling
    //----------------------------------------------------------------------------------------------
    public:
        virtual
        bool
        isParameterWritable(unsigned int) const;

        virtual
        bool
        isParameterRelevant(unsigned int)const;
    private:
        bool
        isParameterRelevantForClass(unsigned int) const;
    protected:
        virtual
        bool
        mustSendToScene(const xchg::ParmEvent&) const;
    //----------------------------------------------------------------------------------------------
    public:
        virtual inline
        xchg::NodeClass
        nodeClass() const
        {
            return xchg::NodeClass::SCENE_OBJECT;
        }

        virtual inline
        SceneObject*
        asSceneObject()
        {
            return this;
        }

        virtual inline
        const SceneObject*
        asSceneObject() const
        {
            return this;
        }

        virtual inline
        Drawable*
        asDrawable()
        {
            return nullptr;
        }

        virtual inline
        const Drawable*
        asDrawable() const
        {
            return nullptr;
        }

        virtual inline
        Xform*
        asXform()
        {
            return nullptr;
        }

        virtual inline
        const Xform*
        asXform() const
        {
            return nullptr;
        }

        virtual inline
        Camera*
        asCamera()
        {
            return nullptr;
        }

        virtual inline
        const Camera*
        asCamera() const
        {
            return nullptr;
        }

        virtual inline
        Light*
        asLight()
        {
            return nullptr;
        }

        virtual inline
        const Light*
        asLight() const
        {
            return nullptr;
        }
    };
}
}
