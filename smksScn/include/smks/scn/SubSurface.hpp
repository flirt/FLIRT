// ========================================================================= //
// Copyright 2016-2018 SUPAMONKS_STUDIO                                      //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
//                                                                           //
// ========================================================================= //
#pragma once

#include "smks/scn/Shader.hpp"

namespace smks { namespace scn
{
    class FLIRT_SCN_EXPORT SubSurface:
        public Shader
    {
    public:
        typedef std::shared_ptr<SubSurface> Ptr;
        typedef std::weak_ptr<SubSurface>   WPtr;

    public:
        static
        Ptr
        create(const char* code,
               const char* name,
               TreeNode::WPtr const&);

    protected:
        inline
        SubSurface(const char* name, TreeNode::WPtr const& parent):
            Shader(name, parent)
        { }

    public:
        virtual
        bool
        isParameterWritable(unsigned int) const;

        virtual
        bool
        isParameterRelevant(unsigned int)const;
    private:
        bool
        isParameterRelevantForClass(unsigned int) const;

    public:
        virtual inline
        xchg::NodeClass
        nodeClass() const
        {
            return xchg::NodeClass::SUBSURFACE;
        }

        virtual inline
        SubSurface*
        asSubSurface()
        {
            return this;
        }

        virtual inline
        const SubSurface*
        asSubSurface() const
        {
            return this;
        }
    };
}
}
