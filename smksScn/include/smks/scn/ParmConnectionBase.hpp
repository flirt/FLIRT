// ========================================================================= //
// Copyright 2016-2018 SUPAMONKS_STUDIO                                      //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
//                                                                           //
// ========================================================================= //
#pragma once

#include <typeinfo>
#include <memory>

#include "smks/sys/configure_scn.hpp"

namespace smks { namespace scn
{
    class TreeNode;
    class Scene;

    ////////////////////
    // class ParmConnectionBase
    ////////////////////
    class FLIRT_SCN_EXPORT ParmConnectionBase
    {
        friend class ScenePImpl;

    public:
        typedef std::shared_ptr<ParmConnectionBase> Ptr;
        typedef std::weak_ptr<ParmConnectionBase>   WPtr;
    private:
        typedef std::weak_ptr<TreeNode>             TreeNodeWPtr;
        typedef std::weak_ptr<Scene>                SceneWPtr;

    private:
        class PImpl;
        class SceneObserver;
        class ParmObserver;
    private:
        PImpl* _pImpl;

    protected:
        ParmConnectionBase(unsigned int inNodeId,
                           const char*  inParmName,
                           unsigned int outNodeId,
                           const char*  outParmName,
                           int          priority,
                           SceneWPtr const&);

    public:
        ~ParmConnectionBase();

        unsigned int
        id() const;

        unsigned int
        inNodeId() const;

        const char*
        inParmName() const;

        unsigned int
        outNodeId() const;

        const char*
        outParmName() const;

        int
        priority() const;

        void
        priority(int);

        virtual
        const std::type_info&
        type() const = 0;

        virtual
        void
        evaluate() = 0;

        bool
        established() const;

        void
        erase();

    protected:
        SceneWPtr const&
        scene() const;

        //std::string
        //to_string() const;

        void
        build(Ptr const&);

        void
        connectInput();

        void
        disconnectInput();

        void
        connectOutput();

        void
        disconnectOutput();

    public:
        unsigned int
        inParmId() const;

        unsigned int
        outParmId() const;

    protected:
        const TreeNodeWPtr&
        inNode() const;

        const TreeNodeWPtr&
        outNode() const;
    };
}
}
