// ========================================================================= //
// Copyright 2016-2018 SUPAMONKS_STUDIO                                      //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
//                                                                           //
// ========================================================================= //
#pragma once

#include <smks/sys/Exception.hpp>
#include <smks/xchg/ParmEvent.hpp>
#include <smks/xchg/GUIEventType.hpp>

#include "smks/scn/ParmConnection.hpp"
#include "smks/scn/TreeNode.hpp"

#define DEF_COMPUTE_GLOBAL_PARM_SPECIALIZATION(_CTYPE_) \
    template<> void computeGlobalParameter<_CTYPE_>(const parm::BuiltIn&, unsigned int nodeId, _CTYPE_&);

namespace smks
{
    namespace xchg
    {
        struct BoundingBox;
        class IdSet;
        class GUIEvent;
        struct AbstractGUIEventCallback;
        struct AbstractProgressCallbacks;
        template <typename ValueType> class DAGHierarchy;
    }
    namespace sys
    {
        class ResourceDatabase;
    }

    namespace scn
    {
        struct AbstractTreeNodeFilter;
        class AbstractSceneObserver;
        class TreeNodeVisitor;
        class ParmAssignBase;
        class ParmConnectionBase;
        class IdConnection;
        class ParmConnectionGraph;
        class ScenePImpl;

        template<class ObjectType> class Tracked;

        class FLIRT_SCN_EXPORT Scene:
            public TreeNode
        {
            friend class ScenePImpl;
            friend class TreeNode;
            friend class Archive;
            friend class ParmConnectionBase;

        public:
            typedef std::shared_ptr<Scene>  Ptr;
            typedef std::weak_ptr<Scene>    WPtr;

        public:
            ////////////////
            // struct Config
            ////////////////
            struct FLIRT_SCN_EXPORT Config
            {
            public:
                bool  withViewport() const;
                bool  withRendering() const;
                bool  cacheAnim() const;
                float defaultPointWidth() const;
                float defaultCurveWidth() const;
            private:
                struct PImpl;
            private:
                PImpl* _pImpl;
            public:
                Config();
                Config(const Config&);
                explicit Config(const char*);
                Config& operator=(const Config&);
                ~Config();
            private:
                void setDefaults();
            };
            ////////////////////////
            // class IdGetterOptions
            ////////////////////////
            class FLIRT_SCN_EXPORT IdGetterOptions
            {
            private:
                class PImpl;
            private:
                PImpl *_pImpl;

            public:
                IdGetterOptions();
                ~IdGetterOptions();
            private:
                // non-copyable
                IdGetterOptions(const IdGetterOptions&);
                IdGetterOptions& operator=(const IdGetterOptions&);

            public:
                void
                fromId(unsigned int);

                unsigned int
                fromId() const;

                void
                upwards(bool);

                bool
                upwards() const;

                void
                filter(std::shared_ptr<AbstractTreeNodeFilter> const&);

                std::shared_ptr<AbstractTreeNodeFilter> const&
                filter() const;

                void
                accumulate(bool);

                bool
                accumulate() const;

                void
                addRegex(const char*);

                void
                removeRegex(size_t);

                size_t
                numRegexes() const;

                const char*
                regex(size_t) const;
            };

        private:
            typedef std::shared_ptr<AbstractSceneObserver>              SceneObserverPtr;
            typedef std::shared_ptr<IdConnection>                       IdConnectionPtr;
            typedef std::shared_ptr<xchg::AbstractGUIEventCallback>     AbstractGUIEventCallbackPtr;
            typedef std::shared_ptr<xchg::AbstractProgressCallbacks>    AbstractProgressCallbacksPtr;
            typedef std::shared_ptr<sys::ResourceDatabase>              ResourceDatabasePtr;

        private:
            class TransformHierarchy;

        private:
            ScenePImpl* _pImpl;

        public:
            static
            Ptr
            create(const char* = "");

        protected:
            explicit
            Scene(const char*);

            virtual
            void
            build(TreeNode::Ptr const&);

        public:
            ~Scene();

            const Config&
            config() const;

            void
            computeBounds(xchg::BoundingBox&);

            template<typename ValueType>
            void
            computeGlobalParameter(const parm::BuiltIn& parm, unsigned int nodeId, ValueType& ret)
            {
                throw sys::Exception("Unsupported parameter type.", "Global Computation For '" + std::string(parm.c_str()) + "'");
            }
            DEF_COMPUTE_GLOBAL_PARM_SPECIALIZATION(char)
            DEF_COMPUTE_GLOBAL_PARM_SPECIALIZATION(size_t)
            DEF_COMPUTE_GLOBAL_PARM_SPECIALIZATION(math::Affine3f)

        public:
            bool
            hasDescendant(unsigned int) const;

            TreeNode::WPtr const&
            descendantById(unsigned int) const;

            inline
            unsigned int
            connectId(unsigned int  inNodeId,
                      const char*   parmName,
                      unsigned int  outNodeId,
                      int           priority = 0)
            {
                return connectId(inNodeId, parmName, outNodeId, parmName, priority);
            }

            unsigned int
            connectId(unsigned int  inNodeId,
                      const char*   inParmName,
                      unsigned int  outNodeId,
                      const char*   outParmName,
                      int           priority = 0);

            template <typename ValueType> inline
            unsigned int //<! returns connection ID
            connect(unsigned int    inNodeId,
                    const char*     parmName,
                    unsigned int    outNodeId,
                    int             priority = 0)
            {
                return connect<ValueType>(inNodeId, parmName, outNodeId, parmName, priority);
            }

            template <typename ValueType> inline
            unsigned int //<! returns connection ID
            connect(unsigned int    inNodeId,
                    const char*     inParmName,
                    unsigned int    outNodeId,
                    const char*     outParmName,
                    int             priority = 0)
            {
                return connectable(inNodeId, inParmName, outNodeId, outParmName)
                    ? registerConnection(ParmConnection<ValueType>::create(
                        inNodeId, inParmName, outNodeId, outParmName, priority, std::static_pointer_cast<Scene>(self().lock())))
                    : 0;
            }

            bool
            hasConnection(unsigned int) const;

            bool
            hasIdConnection(unsigned int) const;

            size_t
            numConnections() const;

            std::shared_ptr<ParmConnectionBase> const&
            connectionById(unsigned int) const;

            void
            disconnect(unsigned int);

            void
            disconnectId(unsigned int);
        private:
            bool
            detectNodeLinkCycle(unsigned int target, unsigned int source) const;
        public:
            void
            getIds(xchg::IdSet&, const IdGetterOptions* = nullptr) const;

            void
            registerObserver(SceneObserverPtr const&);
            void
            deregisterObserver(SceneObserverPtr const&);

            virtual
            bool
            isParameterRelevant(unsigned int)const;
        private:
            bool
            isParameterRelevantForClass(unsigned int) const;

        public:
            virtual
            bool
            isParameterWritable(unsigned int) const;

            // GUI events
            void
            handleGUIEvent(const xchg::GUIEvent&);

            void
            registerGUIEventCallback(xchg::GUIEventType, AbstractGUIEventCallbackPtr const&);
            void
            deregisterGUIEventCallback(xchg::GUIEventType, AbstractGUIEventCallbackPtr const&);

            // progress
            void
            notifyProgressBeginning(const char*, size_t totalSteps);
            bool
            notifyProgressProceeding(size_t);
            void
            notifyProgressEnd(const char*);

            void
            registerProgressCallbacks(AbstractProgressCallbacksPtr const&);
            void
            deregisterProgressCallbacks(AbstractProgressCallbacksPtr const&);

            virtual inline
            xchg::NodeClass
            nodeClass() const
            {
                return xchg::NodeClass::SCENE;
            }

            virtual inline
            Scene*
            asScene()
            {
                return this;
            }

            virtual inline
            const Scene*
            asScene() const
            {
                return this;
            }

        private:
            void
            registerDescendant(TreeNode::Ptr const&);

            void
            deregisterDescendant(unsigned int);

            unsigned int
            registerConnection(ParmConnectionBase::Ptr const&);

            unsigned int
            registerConnectionId(IdConnectionPtr const&);

            void
            addNodeLink(unsigned int nodeId, unsigned int parmId);

            void
            removeNodeLink(unsigned int nodeId, unsigned int parmId);

        protected:
            virtual
            void
            handleSceneEvent(const xchg::SceneEvent&);

            virtual
            void
            handleParmEvent(const xchg::ParmEvent&);

            virtual
            void
            deliver(const xchg::SceneEvent&);

        private:
            // external parameter events
            //--------------------------
            void
            pushExternalEvent(std::shared_ptr<Tracked<xchg::ParmEvent>> const&);
            bool
            hasExternalEvent() const;
            std::shared_ptr<Tracked<xchg::ParmEvent>>
            popExternalEvent();
            //--------------------------
            bool
            isNameValid(const char*, int parentId) const;

            bool
            connectable(unsigned int    inNodeId,
                        const char*     inParmName,
                        unsigned int    outNodeId,
                        const char*     outParmName);

        public:
            void
            setSharedResourceDatabase(ResourceDatabasePtr const&);

            ResourceDatabasePtr const&
            getOrCreateResourceDatabase();
        };
    }
}
