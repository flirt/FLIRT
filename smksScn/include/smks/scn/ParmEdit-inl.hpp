// ========================================================================= //
// Copyright 2016-2018 SUPAMONKS_STUDIO                                      //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
//                                                                           //
// ========================================================================= //
#pragma once

namespace smks { namespace scn
{
    template<typename ValueType> inline // static
    unsigned int
    ParmEdit::set(scn::TreeNode&    node,
                  const char*       parmName,
                  unsigned int      parmId,
                  const ValueType&  parmValue,
                  parm::ParmFlags   flags)
    {
        return node._setParameter<ValueType>(parmName, parmId, parm::builtIns().find(parmId).get(), parmValue, flags);
    }

    template<typename ValueType> inline // static
    void
    ParmEdit::update(scn::TreeNode&     node,
                     unsigned int       parmId,
                     const ValueType&   parmValue,
                     parm::ParmFlags    flags)
    {
        node._updateParameter<ValueType>(parmId, parmValue, flags);
    }

    inline // static
    void
    ParmEdit::unset(scn::TreeNode&  node,
                    unsigned int    parmId,
                    parm::ParmFlags flags)
    {
        node._unsetParameter(parmId, parm::builtIns().find(parmId).get(), flags);
    }
}
}
