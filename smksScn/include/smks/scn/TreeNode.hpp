// ========================================================================= //
// Copyright 2016-2018 SUPAMONKS_STUDIO                                      //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
//                                                                           //
// ========================================================================= //
#pragma once

#include <iostream>
#include <memory>
#include <string>

#include <smks/xchg/NodeClass.hpp>
#include <smks/xchg/ParameterInfo.hpp>
#include <smks/parm/BuiltIn.hpp>
#include <smks/parm/BuiltInMap.hpp>
#include <smks/parm/Container.hpp>

#include "smks/scn/ParmGetter.hpp"
#include "smks/scn/ParmEdit.hpp"
#include "smks/sys/configure_scn.hpp"

namespace smks
{
    namespace xchg
    {
        class SceneEvent;
        class ParmEvent;
        template<class ValueType> class Vector;
    }
    namespace sys
    {
        class ResourceDatabase;
        class ResourcePathFinder;
    }

    namespace scn
    {
        class Scene;
        class ScenePImpl;
        class Archive;
        class SceneObject;
        class Xform;
        class Camera;
        class Drawable;
        class Points;
        class Curves;
        class PolyMesh;
        class Light;
        class GraphAction;
        class TreeNodeSelector;
        class ParmAssignBase;
        class Shader;
        class Material;
        class SubSurface;
        class RtNode;
        class Renderer;
        class ToneMapper;
        class FrameBuffer;
        class RenderPass;

        class ParmConnections;
        class ParmEdit;
        class Ids;
        class TreeNodeVisitor;
        class AbstractParmObserver;

        template<class ObjectType> class Tracked;

        /////////////////
        // class TreeNode
        /////////////////
        class FLIRT_SCN_EXPORT TreeNode
        {
            friend class parm::Container;
            friend class ScenePImpl;
            friend class AbstractObjectSchema;
            friend class AbstractObjectDataHolder;
            friend class SchemaBaseFromAbc;
            friend class ParmConnectionBase;
            friend class ParmConnections;
            friend class ParmEdit;
            friend class InputParmConnections;
            template<typename ValueType> friend class ParmConnection;
            friend class IdConnection;

        public:
            typedef std::shared_ptr<TreeNode>   Ptr;
            typedef std::weak_ptr<TreeNode>     WPtr;
        private:
            typedef std::shared_ptr<AbstractParmObserver> ParmObserverPtr;

        private:
            class PImpl;
            class ContainerObserver;
            class ParmStack;
        private:
            PImpl* _pImpl;

        public:
            static
            Ptr
            create(const char*, WPtr const& parent);

            template<class NodeType> static
            void
            destroy(std::shared_ptr<NodeType>& ptr)
            {
                destroy(std::static_pointer_cast<TreeNode>(ptr));
                ptr = nullptr;
            }

            static
            void
            destroy(Ptr&);

        private:
            // non-copyable
            TreeNode(const TreeNode&);
            TreeNode& operator=(const TreeNode&);

        protected:
            TreeNode(); //<! only useful for scene root node
            TreeNode(const char* name, WPtr const& parent);

            virtual
            void
            build(Ptr const&);

            //!< deconnects calling node from the hierarchy, hence killing it
            virtual
            void
            erase();

        public:
            virtual
            ~TreeNode();

        public:
            virtual inline
            bool
            getTimeBounds(float& minTime, float& maxTime) const
            {
                minTime = FLT_MAX;
                maxTime = -FLT_MAX;

                return minTime < maxTime;
            }

            unsigned int
            id() const;

            const char*
            fullName() const;

            const char*
            name() const;

            WPtr const&
            self() const;

            unsigned int
            numChildren() const;

            Ptr const&
            child(size_t) const;

            WPtr const&
            parent() const;

            WPtr const&
            root() const;

            size_t
            depth() const;

            ///////////////
            // node linking
            ///////////////
        public:
            void
            getLinkSources(xchg::IdSet& nodeIds, xchg::NodeClass = xchg::NodeClass::ANY) const;

            void
            getLinkTargets(xchg::IdSet& nodeIds, xchg::NodeClass = xchg::NodeClass::ANY) const;

            size_t
            getNumLinksFrom(unsigned int nodeId) const;

            void
            getLinksFrom(unsigned int nodeId, xchg::IdSet& parmIds) const;

            bool
            isLinkFrom(const char* parmName, unsigned int nodeId) const;

            bool
            isLinkFrom(unsigned int parmId, unsigned int nodeId) const;

            size_t
            getNumLinksTo(unsigned int nodeId) const;

            void
            getLinksTo(unsigned int nodeId, xchg::IdSet& parmIds) const;

            bool
            isLinkTo(const char* parmName, unsigned int nodeId) const;

            bool
            isLinkTo(unsigned int parmId, unsigned int nodeId) const;

            bool
            isLink(const char* parmName) const;

            bool
            isLink(unsigned int parmId) const;

            // can only be called by Scene or itself
        private:
            void
            addNodeLink(unsigned int parmId);

            void
            removeNodeLink(unsigned int parmId);

            //////////////////
            // node parameters
            //////////////////
        private:
            ////////////
            // IMPORTANT
            //    direct access to the parameter container without possible control/specialization by node must remain forbidden.
            ////////////
            parm::Container::Ptr const&
            parameters();

            parm::Container::CPtr
            parameters() const;

        public:
            //----------------------------------------------------------------------------------------------
            // PARAMETER EXISTENCE TESTS
            //----------------------------------------------------------------------------------------------
            // typeless parameter existence tests
            inline
            bool
            parameterExists(const char* parmName, unsigned int upToNodeId = 0, Ptr* foundInNode = nullptr) const
            {
                return parameterExists(util::string::getId(parmName), upToNodeId, foundInNode);
            }
            bool
            parameterExists(unsigned int parmId, unsigned int upToNodeId = 0, Ptr* foundInNode = nullptr) const;

            // typed parameter existence tests
            template<typename ValueType> inline
            bool
            parameterExistsAs(const char* parmName, unsigned int upToNodeId = 0, Ptr* foundInNode = nullptr) const
            {
                return parameterExistsAs<ValueType>(util::string::getId(parmName), upToNodeId, foundInNode);
            }
            template<typename ValueType> inline
            bool
            parameterExistsAs(unsigned int parmId, unsigned int upToNodeId = 0, Ptr* foundInNode = nullptr) const
            {
                return _parameterExistsAs<ValueType>(parmId, parm::NO_PARM_FLAG, upToNodeId, foundInNode);
            }

            // specific parameter existence tests
            inline
            bool
            parameterExistsAsId(const char* parmName, unsigned int upToNodeId = 0, Ptr* foundInNode = nullptr) const
            {
                return parameterExistsAsId(util::string::getId(parmName), upToNodeId, foundInNode);
            }
            inline
            bool
            parameterExistsAsId(unsigned int parmId, unsigned int upToNodeId = 0, Ptr* foundInNode = nullptr) const
            {
                return _parameterExistsAs<unsigned int>(parmId, parm::CREATES_LINK, upToNodeId, foundInNode);
            }
            inline
            bool
            parameterExistsAsSwitch(const char* parmName, unsigned int upToNodeId = 0, Ptr* foundInNode = nullptr) const
            {
                return parameterExistsAsSwitch(util::string::getId(parmName), upToNodeId, foundInNode);
            }
            inline
            bool
            parameterExistsAsSwitch(unsigned int parmId, unsigned int upToNodeId = 0, Ptr* foundInNode = nullptr) const
            {
                return _parameterExistsAs<char>(parmId, parm::SWITCHES_STATE, upToNodeId, foundInNode);
            }

        private:
            template<typename ValueType>
            bool
            _parameterExistsAs(unsigned int     parmId,
                               parm::ParmFlags  f,
                               unsigned int     upToNodeId  = 0,
                               Ptr*             foundInNode = nullptr) const
            {
                Ptr node = this->self().lock();
                while (true)
                {
                    if (!node)
                        break;
                    //----------------------------------------------------------
                    bool found = node->parameters()->existsAs<ValueType>(parmId);
                    if (found &&
                        f != parm::NO_PARM_FLAG)
                    {
                        parm::BuiltIn::Ptr const&   builtIn = parm::builtIns().find(parmId);
                        const parm::ParmFlags       flags   = builtIn
                            ? builtIn->flags()
                            : node->getParameterDescriptiveFlags(parmId);
                        found = (f & flags) == f;
                    }
                    //----------------------------------------------------------
                    if (found)
                    {
                        if (foundInNode)
                            *foundInNode = node;
                        return true;
                    }
                    if (upToNodeId == 0 || node->id() == upToNodeId)
                        break;
                    node = node->parent().lock(); // go up the node hierarchy if allowed
                }
                if (foundInNode)
                    *foundInNode = nullptr;
                return false;
            }
            //----------------------------------------------------------------------------------------------

        public:
            //----------------------------------------------------------------------------------------------
            // PARAMETER GETTERS
            //----------------------------------------------------------------------------------------------
            template<typename ValueType> inline
            bool
            getParameter(const parm::BuiltIn& parm, ValueType& value, unsigned int upToNodeId = 0) const
            {
                if (!parm.compatibleWith<ValueType>())
                    throwIncompatibleParmError(parm.c_str(), "Built-In Parameter Getter");

                AbstractParmGetter::Ptr const& getter = hasCustomGetter(parm.id());
                if (getter) // custom getter
                {
                    if (getter->type() != typeid(ValueType))
                        throwIncompatibleParmError(parm.c_str(), "Built-In Parameter Getter");

                    TypedParmGetter<ValueType>::Ptr const& tGetter = std::dynamic_pointer_cast<TypedParmGetter<ValueType>>(getter);
                    assert(tGetter);
                    if ((*tGetter)(parm, value, upToNodeId))
                        return true;
                }
                // get value from parameter set
                Ptr node = nullptr;
                if (parameterExistsAs<ValueType>(parm.id(), upToNodeId, &node))
                {
                    value = node->parameters()->get<ValueType>(parm.id());
                    return true;
                }
                // default value
                parm::Any any;

                if (overrideParameterDefaultValue(parm.id(), any))
                    value = parm::Any::cast<ValueType>(any);
                else if (!parm.defaultValue(value))
                {
                    warnNoDefaultValue(parm.c_str());
                    value = ValueType();
                }

                return false;
            }

            template<typename ValueType> inline
            bool
            getParameter(const char* parmName, ValueType& value, const ValueType& defaultValue = ValueType(), unsigned int upToNodeId = 0) const
            {
                return getParameter(util::string::getId(parmName), value, defaultValue, upToNodeId);
            }

            template<typename ValueType>
            bool
            getParameter(unsigned int parmId, ValueType& value, const ValueType& defaultValue = ValueType(), unsigned int upToNodeId = 0) const
            {
                AbstractParmGetter::Ptr const& getter = hasCustomGetter(parmId);
                if (getter) // custom getter
                {
                    parm::BuiltIn::Ptr const& parm = parm::builtIns().find(parmId);
                    if (parm)
                    {
                        if (getter->type() != typeid(ValueType))
                            throwIncompatibleParmError(parmId, "Parameter Getter");

                        TypedParmGetter<ValueType>::Ptr const& tGetter = std::dynamic_pointer_cast<TypedParmGetter<ValueType>>(getter);
                        assert(tGetter);
                        if ((*tGetter)(*parm, value, upToNodeId))
                            return true;
                    }
                }
                // get value from parameter set
                Ptr node = nullptr;
                if (parameterExistsAs<ValueType>(parmId, upToNodeId, &node))
                {
                    value = node->parameters()->get<ValueType>(parmId);
                    return true;
                }
                // default value
                if (!getParameterDefault<ValueType>(parmId, value))
                    value = defaultValue;
                return false;
            }

        protected:
            template<typename ValueType>
            bool    // returns true if a default value could have been found either by asking node, or built-in map.
            getParameterDefault(unsigned int parmId, ValueType& value) const
            {
                // get parameter default override by the calling node
                parm::Any any;
                if (overrideParameterDefaultValue(parmId, any))
                {
                    value = parm::Any::cast<ValueType>(any);
                    return true;
                }
                // get parameter default value if built-in
                parm::BuiltIn::Ptr const& builtIn = parm::builtIns().find(parmId);
                return builtIn
                    ? builtIn->defaultValue<ValueType>(value)
                    : false;
            };

            void
            setCustomGetter(unsigned int, AbstractParmGetter::Ptr const&);
        private:
            AbstractParmGetter::Ptr
            hasCustomGetter(unsigned int) const;

        protected:
            virtual inline
            bool
            overrideParameterDefaultValue(unsigned int, parm::Any&) const
            {
                return false;
            }

        public:
            //<! returns information (name, type_info, flags) from effectively stored parameters
            inline
            bool
            getParameterInfo(const char* parmName, xchg::ParameterInfo& parmInfo, unsigned int upToNodeId = 0, Ptr* oNode = nullptr) const
            {
                const unsigned int parmId = util::string::getId(parmName);
                return getParameterInfo(parmId, parmInfo, upToNodeId, oNode);
            }
            bool
            getParameterInfo(unsigned int parmId, xchg::ParameterInfo&, unsigned int upToNodeId = 0, Ptr* = nullptr) const;
        private:
            bool
            _getParameterInfo(unsigned int parmId, xchg::ParameterInfo&) const; //<! only look at calling node instance's content

            //<! save/get *descriptive* parameter flags during setting/update.
            // WARNING: should only get called for user parameters as built-in parameters
            // store their own flags.
            void
            setParameterDescriptiveFlags(unsigned int, parm::ParmFlags);

            parm::ParmFlags
            getParameterDescriptiveFlags(unsigned int parmId) const;

        public:
            void
            getCurrentParameters(xchg::IdSet&) const;

        private:
            void
            eraseInputParmConnections(unsigned int parmId);

            void
            eraseInputIdConnections(unsigned int parmId);

        private:
            //--------------------------------------------------
            // error handling and warnings for parameter setters
            void
            warnReadOnlyParm(const char* parmName) const;

            void
            warnRedundantParmEdit(const char* parmName) const;

            void
            warnRedundantParmEdit(unsigned int parmId) const;

            void
            warnIrrelevantParm(const parm::BuiltIn&) const;

            void
            warnNoDefaultValue(const char* parmName) const;

            void
            throwIncompatibleParmError(const char* parmName,
                                       const char* errorHeading) const;

            void
            throwIncompatibleParmError(unsigned int parmId,
                                       const char*  errorHeading) const;

            void
            throwInvalidParmValueError(const char* parmName,
                                       const char* errorHeading) const;

            void
            throwUnknownParmError(unsigned int parmId,
                                  const char*  errorHeading) const;
            //--------------------------------------------------

        public:
            //----------------------------------------------------------------------------------------------
            // PARAMETER UPDATERS
            //--------------------------------------------------------------------------------------------
            template<typename ValueType> inline
            void
            updateParameter(unsigned int parmId, const ValueType& parmValue)
            {
                _updateParameter<ValueType>(parmId, parmValue, parm::NO_PARM_FLAG);
            }
        private:
            template<typename ValueType> inline
            void
            _updateParameter(unsigned int       parmId,
                             const ValueType&   parmValue,
                             parm::ParmFlags    f)
            {
                parm::BuiltIn::Ptr const& builtIn = parm::builtIns().find(parmId);

                if (!builtIn &&
                    !parameters()->existsAs<ValueType>(parmId))
                    throwUnknownParmError(parmId, "Parameter Update"); // failed to recognized parameter from its ID alone

                // if needed, then get stored parameter's info
                xchg::ParameterInfo parmInfo;
                if (builtIn)
                {
                    parmInfo.name       = builtIn->c_str();
                    parmInfo.typeinfo   = builtIn->typeinfo();
                    parmInfo.flags      = builtIn->flags();
                }
                else if (!_getParameterInfo(parmId, parmInfo))
                    return; // parameter has not been found

                const parm::ParmFlags flags = f | (builtIn ? builtIn->flags() : getParameterDescriptiveFlags(parmId));
                //------------
                const unsigned int ret = _setParameter<ValueType>(parmInfo.name, parmId, builtIn.get(), parmValue, flags);
                //------------
                assert(ret == 0 || ret == parmId);
            }

            //----------------------------------------------------------------------------------------------
            // PARAMETER SETTERS
            //----------------------------------------------------------------------------------------------
        public:
            template<typename ValueType> inline
            unsigned int //<! returns parameter id
            setParameter(const char* parmName, const ValueType& parmValue)
            {
                return setParameter<ValueType>(parmName, parmValue, parm::NO_PARM_FLAG);
            }
            template<typename ValueType> inline
            unsigned int //<! returns parameter id
            setParameter(const parm::BuiltIn& parm, const ValueType& parmValue)
            {
                return setParameter<ValueType>(parm, parmValue, parm::NO_PARM_FLAG);
            }

            inline
            unsigned int //<! returns parameter id
            setParameterId(const char* parmName, unsigned int parmValue)
            {
                return setParameter<unsigned int>(parmName, parmValue, parm::CREATES_LINK);
            }
            inline
            unsigned int //<! returns parameter id
            setParameterTrigger(const char* parmName)
            {
                return setParameter<char>(parmName, xchg::ACTIVE, parm::TRIGGERS_ACTION);
            }
            inline
            unsigned int //<! returns parameter id
            setParameterSwitch(const char* parmName, bool state)
            {
                return setParameter<char>(parmName, state ? xchg::ACTIVE : xchg::INACTIVE, parm::SWITCHES_STATE);
            }
        protected:
            template<typename ValueType> inline
            unsigned int
            setParameter(const parm::BuiltIn& parm, const ValueType& parmValue, parm::ParmFlags flags)
            {
                return _setParameter<ValueType>(parm.c_str(), parm.id(), &parm, parmValue, flags);
            }

            template<typename ValueType> inline
            unsigned int //<! returns parameter id
            setParameter(const char* parmName, const ValueType& parmValue, parm::ParmFlags flags)
            {
                const unsigned int          parmId  = util::string::getId(parmName);
                parm::BuiltIn::Ptr const&   builtIn = parm::builtIns().find(parmId);
                return _setParameter<ValueType>(parmName, parmId, builtIn.get(), parmValue, flags);
            }

            template<typename ValueType> inline
            unsigned int //<! returns parameter id
            setParameter(const char* parmName, unsigned int parmId, const ValueType& parmValue, parm::ParmFlags flags)
            {
                parm::BuiltIn::Ptr const&   builtIn = parm::builtIns().find(parmId);
                return _setParameter<ValueType>(parmName, parmId, builtIn.get(), parmValue, flags);
            }
        private:
            template<typename ValueType>
            unsigned int //<! returns parameter id
            _setParameter(const char*           parmName,
                          unsigned int          parmId,
                          const parm::BuiltIn*  builtIn,
                          const ValueType&      parmValue,
                          parm::ParmFlags       f)
            {
                assert(parmName && strlen(parmName) > 0);
                assert(parmId == util::string::getId(parmName));
                assert(!builtIn || builtIn->id() == parmId);
                //------------
                if (builtIn &&
                    !builtIn->compatibleWith<ValueType>())
                    throwIncompatibleParmError(builtIn->c_str(), "Node Parameter Setter");

                const parm::ParmFlags flags = f |
                    parm::getTypeFlags<ValueType>() |
                    (builtIn ? builtIn->flags() : parm::NO_PARM_FLAG);
                //------------
                if ( ((flags & parm::SKIPS_RESTRAINTS) == 0 && !isParameterWritable(parmId)) ||
                     (flags & parm::NEEDS_SCENE) != 0 )
                {
                     warnReadOnlyParm(parmName);
                     return 0;
                }
                if (builtIn &&
                    !isParameterRelevant(builtIn->id()))
                    warnIrrelevantParm(*builtIn);

                if ((flags & parm::KEEPS_CONNECTIONS) == 0) // input connections are overwritten if set by user or node itself.
                    eraseInputParmConnections(parmId);
                if ((flags & parm::CREATES_LINK) && !isLink(parmId))
                    addNodeLink(parmId);

                ValueType validValue = parmValue;

                if (builtIn &&
                    !builtIn->validate<ValueType>(parmValue, flags, validValue))
                    throwInvalidParmValueError(parmName, "Node Parameter Setter");

                if ((flags & parm::SKIPS_PREPROCESS) == 0)
                {
                    const bool skip = preprocessParameterEdit(parmName, parmId, flags, parm::Any(validValue));
                    if (skip)
                        return parmId;
                }

                if (mustWarnAgainstEdit<ValueType>(parmId, validValue))
                    warnRedundantParmEdit(parmName);

                // save the descriptive flags for non built-in parameters
                if (!builtIn)
                    setParameterDescriptiveFlags(parmId, flags);
                //------------
                const unsigned int ret = parameters()->set<ValueType>(parmName, parmId, validValue);
                //------------
                assert(ret == parmId);
                return ret;
            }

            template<typename ValueType> inline
            bool
            mustWarnAgainstEdit(unsigned int parmId, const ValueType& nxtValue)
            {
                if (!isParameterBeingHandled(parmId))
                    return false;
                ValueType curValue;
                return getParameter<ValueType>(parmId, curValue)
                    ? xchg::equal<ValueType>(curValue, nxtValue) == false
                    : true;
            }
            inline
            bool
            mustWarnAgainstEdit(unsigned int parmId)
            {
                if (!isParameterBeingHandled(parmId))
                    return false;
                return parameterExists(parmId);
            }
        protected:
            virtual
            bool    //<! returns true if the caught parameter edit must be skipped
            preprocessParameterEdit(const char* parmName, unsigned int parmId, parm::ParmFlags, const parm::Any&);
        //----------------------------------------------------------------------------------------------

        //----------------------------------------------------------------------------------------------
        // PARAMETER UNSETTERS
        //----------------------------------------------------------------------------------------------
        public:
            inline
            void
            unsetParameter(const parm::BuiltIn& parm)
            {
                _unsetParameter(parm.id(), &parm, parm::NO_PARM_FLAG);
            }

            inline
            void
            unsetParameter(const char* parmName)
            {
                const unsigned int          parmId  = util::string::getId(parmName);
                parm::BuiltIn::Ptr const&   builtIn = parm::builtIns().find(parmId);
                _unsetParameter(parmId, builtIn.get(), parm::NO_PARM_FLAG);
            }

            inline
            void
            unsetParameter(unsigned int parmId)
            {
                parm::BuiltIn::Ptr const& builtIn = parm::builtIns().find(parmId);
                _unsetParameter(parmId, builtIn.get(), parm::NO_PARM_FLAG);
            }
        protected:
            inline
            void
            _unsetParameter(const parm::BuiltIn& builtIn, parm::ParmFlags f)
            {
                _unsetParameter(builtIn.id(), &builtIn, f);
            }
        private:
            inline
            void
            _unsetParameter(unsigned int         parmId,
                            const parm::BuiltIn* builtIn,
                            parm::ParmFlags      f)
            {
                assert(!builtIn || builtIn->id() == parmId);

                if (!parameterExists(parmId))
                    return; // nothing needs to be done

                xchg::ParameterInfo parmInfo;
                if (builtIn)
                {
                    parmInfo.name       = builtIn->c_str();
                    parmInfo.typeinfo   = builtIn->typeinfo();
                    parmInfo.flags      = builtIn->flags();
                }
                else if (!_getParameterInfo(parmId, parmInfo))
                    return; // parameter has not been found

                assert(parmInfo.name && strlen(parmInfo.name) > 0);
                assert(parmId == util::string::getId(parmInfo.name));

                const parm::ParmFlags flags = f | parmInfo.flags;
                //------------
                if ((flags & parm::SKIPS_RESTRAINTS) == 0 && !isParameterWritable(parmId))
                    return;
                if ((flags & parm::KEEPS_CONNECTIONS) == 0) // input connections are overwritten if set by user or node itself.
                    eraseInputParmConnections(parmId);
                if ((flags & parm::SKIPS_PREPROCESS) == 0)
                {
                    const bool skip = preprocessParameterEdit(parmInfo.name, parmId, flags, parm::Any());
                    if (skip)
                        return;
                }

                if (builtIn &&
                    !isParameterRelevant(builtIn->id()))
                    warnIrrelevantParm(*builtIn);

                if (mustWarnAgainstEdit(parmId))
                    warnRedundantParmEdit(parmId);

                //------------
                parameters()->unset(parmId);
                //------------
                if (isLink(parmId))
                    removeNodeLink(parmId);
            }
        protected:
            void
            delay(ParmEdit::Ptr const&);
        public:
            inline
            bool
            isParameterReadable(const char* parmName) const
            {
                return isParameterReadable(util::string::getId(parmName));
            }

            virtual inline
            bool
            isParameterReadable(unsigned int parmId) const
            {
                return true;
            }

            inline
            bool
            isParameterWritable(const char* parmName) const
            {
                return isParameterWritable(util::string::getId(parmName));
            }

            virtual
            bool
            isParameterWritable(unsigned int) const;

            //<! returns true when node reacts to parameters with the specified ID
            virtual inline
            bool
            isParameterRelevant(unsigned int parmId) const
            {
                return isParameterRelevantForClass(parmId);
            }
        private:
            bool
            isParameterRelevantForClass(unsigned int) const;
        public:
            void
            getRelevantParameters(xchg::IdSet&) const;

        public:
            void
            registerObserver(ParmObserverPtr const&);

            void
            deregisterObserver(ParmObserverPtr const&);

        private:
            inline
            void
            registerParameterObserver(parm::AbstractObserver::Ptr const& observer)
            {
                parameters()->registerObserver(observer);
            }

            inline
            void
            deregisterParameterObserver(parm::AbstractObserver::Ptr const& observer)
            {
                parameters()->deregisterObserver(observer);
            }

        public:
            virtual
            void
            accept(TreeNodeVisitor&);

            virtual
            void
            traverse(TreeNodeVisitor&);

        private:
            void
            declareInputParmConnections(const char*);

            const ParmConnections*
            getInputParmConnections(unsigned int) const;

            ParmConnections*
            getInputParmConnections(unsigned int);

            void
            undeclareInputParmConnections(unsigned int);

            void
            declareOutputParmConnections(const char*);

            const ParmConnections*
            getOutputParmConnections(unsigned int) const;

            ParmConnections*
            getOutputParmConnections(unsigned int);

            void
            undeclareOutputParmConnections(unsigned int);

            void
            declareLinkTarget(unsigned int nodeId);

            void
            undeclareLinkTarget(unsigned int nodeId);

            bool
            isLinkTarget(unsigned int nodeId) const;

        public:
            virtual
            void
            getStoredTimes(xchg::Vector<float>&) const;

        public:
            virtual inline
            xchg::NodeClass
            nodeClass() const
            {
                return xchg::NodeClass::ANY;
            }

            inline
            bool
            is(xchg::NodeClass cl) const
            {
                return (cl & nodeClass()) != 0;
            }

            virtual inline
            Scene*
            asScene()
            {
                return nullptr;
            }

            virtual inline
            const Scene*
            asScene() const
            {
                return nullptr;
            }

            virtual inline
            Archive*
            asArchive()
            {
                return nullptr;
            }

            virtual inline
            const Archive*
            asArchive() const
            {
                return nullptr;
            }

            virtual inline
            SceneObject*
            asSceneObject()
            {
                return nullptr;
            }

            virtual inline
            const SceneObject*
            asSceneObject() const
            {
                return nullptr;
            }

            virtual inline
            Shader*
            asShader()
            {
                return nullptr;
            }

            virtual inline
            const Shader*
            asShader() const
            {
                return nullptr;
            }

            virtual inline
            GraphAction*
            asGraphAction()
            {
                return nullptr;
            }

            virtual inline
            const GraphAction*
            asGraphAction() const
            {
                return nullptr;
            }

            virtual inline
            RtNode*
            asRtNode()
            {
                return nullptr;
            }

            virtual inline
            const RtNode*
            asRtNode() const
            {
                return nullptr;
            }

        protected:
            Ptr
            getNodeFromRoot(unsigned int) const;

            // remove node from the root's current selection
            void
            deselect();

        protected:
            virtual
            void
            handleSceneEvent(const xchg::SceneEvent&);

            virtual
            void
            handleParmEvent(const xchg::ParmEvent&);

            virtual
            void
            deliver(const xchg::SceneEvent&);

        private:
            //---------------------------------------------
            // parameter event handling
            //-------------------------
            //<! handle parameter event, propagate it along the hierarchy,
            // send it to the scene when needed, and process delayed edits at the very end.
            void
            handleAndDeliver(std::shared_ptr<Tracked<xchg::ParmEvent>> const&);
            bool
            isParameterBeingHandled(unsigned int) const;

        protected:
            virtual inline
            bool
            mustSendToScene(const xchg::ParmEvent&) const
            {
                return false;
            }
            //---------------------------------------------

            static
            bool
            fitInHierarchy(const std::string&, TreeNode::WPtr const&);

            static
            std::string
            getFullName(const std::string&, TreeNode::WPtr const&);

            unsigned int
            getCode() const;
        public:
            std::shared_ptr<sys::ResourceDatabase>
            getResourceDatabase();

            std::shared_ptr<sys::ResourcePathFinder>
            getResourcePathFinder();

            std::ostream&
            dump(std::ostream&) const;
        };
    }
}

#include "smks/scn/ParmEdit-inl.hpp"
