// ========================================================================= //
// Copyright 2016-2018 SUPAMONKS_STUDIO                                      //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
//                                                                           //
// ========================================================================= //
#pragma once

#include <smks/parm/ParmFlags.hpp>

#include "smks/scn/ParmConnectionBase.hpp"
#include "smks/scn/TreeNode.hpp"

namespace smks { namespace scn
{
    class Scene;

    template<typename ValueType>
    class ParmConnection:
        public ParmConnectionBase
    {
        friend class Scene;

    public:
        typedef std::shared_ptr<ParmConnection> Ptr;
        typedef std::weak_ptr<ParmConnection>   WPtr;
    private:
        typedef std::weak_ptr<Scene>            SceneWPtr;

    private:    // only Scene can create parameter connections
        static inline
        Ptr
        create(unsigned int     inNodeId,
               const char*      inParmName,
               unsigned int     outNodeId,
               const char*      outParmName,
               int              priority,
               SceneWPtr const& scene)
        {
            Ptr ptr(new ParmConnection(inNodeId, inParmName, outNodeId, outParmName, priority, scene));

            ptr->build(ptr);
            return ptr;
        }

    protected:
        inline
        ParmConnection(unsigned int     inNodeId,
                       const char*      inParmName,
                       unsigned int     outNodeId,
                       const char*      outParmName,
                       int              priority,
                       SceneWPtr const& scene):
            ParmConnectionBase(inNodeId, inParmName, outNodeId, outParmName, priority, scene)
        { }

    public:
        virtual inline
        const std::type_info&
        type() const
        {
            return typeid(ValueType);
        }

        virtual inline
        void
        evaluate()
        {
            TreeNode::Ptr const inNode  = this->inNode().lock();
            TreeNode::Ptr const outNode = this->outNode().lock();

            if (!inNode || !outNode)
                return;

            if (!inNode->parameterExistsAs<ValueType>(inParmId()))
            {
                parm::BuiltIn::Ptr const& builtIn = parm::builtIns().find(outParmId());
                outNode->_unsetParameter(outParmId(), builtIn.get(), parm::KEEPS_CONNECTIONS);
            }
            else
            {
                ValueType parmValue;

                inNode->getParameter<ValueType>(inParmId(), parmValue, ValueType());
                if (!outNode->parameterExistsAs<ValueType>(outParmId()))
                    outNode->setParameter<ValueType>(outParmName(), parmValue, parm::KEEPS_CONNECTIONS);
                else
                    outNode->_updateParameter<ValueType>(outParmId(), parmValue, parm::KEEPS_CONNECTIONS);
            }
        }
    };
}
}
