// ========================================================================= //
// Copyright 2016-2018 SUPAMONKS_STUDIO                                      //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
//                                                                           //
// ========================================================================= //
#pragma once

#include "smks/scn/TreeNode.hpp"

namespace smks { namespace scn
{
    class Renderer;
    class ToneMapper;
    class FrameBuffer;
    class RenderPass;
    class Integrator;
    class SamplerFactory;
    class PixelFilter;
    class Denoiser;

    class FLIRT_SCN_EXPORT RtNode:
        public TreeNode
    {
    protected:
        inline
        RtNode(const char* name, TreeNode::WPtr const& parent):
            TreeNode(name, parent)
        { }

    public:
        virtual inline
        xchg::NodeClass
        nodeClass() const
        {
            return xchg::NodeClass::RTNODE;
        }

        virtual inline
        RtNode*
        asRtNode()
        {
            return this;
        }

        virtual inline
        const RtNode*
        asRtNode() const
        {
            return this;
        }

        virtual inline
        Renderer*
        asRenderer()
        {
            return nullptr;
        }

        virtual inline
        const Renderer*
        asRenderer() const
        {
            return nullptr;
        }

        virtual inline
        ToneMapper*
        asToneMapper()
        {
            return nullptr;
        }

        virtual inline
        const ToneMapper*
        asToneMapper() const
        {
            return nullptr;
        }

        virtual inline
        FrameBuffer*
        asFrameBuffer()
        {
            return nullptr;
        }

        virtual inline
        const FrameBuffer*
        asFrameBuffer() const
        {
            return nullptr;
        }

        virtual inline
        RenderPass*
        asRenderPass()
        {
            return nullptr;
        }

        virtual inline
        const RenderPass*
        asRenderPass() const
        {
            return nullptr;
        }

        virtual inline
        Integrator*
        asIntegrator()
        {
            return nullptr;
        }

        virtual inline
        const Integrator*
        asIntegrator() const
        {
            return nullptr;
        }

        virtual inline
        SamplerFactory*
        asSamplerFactory()
        {
            return nullptr;
        }

        virtual inline
        const SamplerFactory*
        asSamplerFactory() const
        {
            return nullptr;
        }

        virtual inline
        PixelFilter*
        asPixelFilter()
        {
            return nullptr;
        }

        virtual inline
        const PixelFilter*
        asPixelFilter() const
        {
            return nullptr;
        }

        virtual inline
        Denoiser*
        asDenoiser()
        {
            return nullptr;
        }

        virtual inline
        const Denoiser*
        asDenoiser() const
        {
            return nullptr;
        }
    };
}
}
