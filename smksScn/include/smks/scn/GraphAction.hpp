// ========================================================================= //
// Copyright 2016-2018 SUPAMONKS_STUDIO                                      //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
//                                                                           //
// ========================================================================= //
#pragma once

#include "smks/scn/TreeNode.hpp"

namespace smks { namespace scn
{
    class ParmAssignBase;
    class TreeNodeSelector;
    class RenderAction;

    class FLIRT_SCN_EXPORT GraphAction:
        public TreeNode
    {
    protected:
        inline
        GraphAction(const char* name, TreeNode::WPtr const& parent):
            TreeNode(name, parent)
        { }

    public:
        virtual inline
        xchg::NodeClass
        nodeClass() const
        {
            return xchg::NodeClass::GRAPH_ACTION;
        }

        virtual inline
        GraphAction*
        asGraphAction()
        {
            return this;
        }

        virtual inline
        const GraphAction*
        asGraphAction() const
        {
            return this;
        }

        virtual inline
        ParmAssignBase*
        asParmAssign()
        {
            return nullptr;
        }

        virtual inline
        const ParmAssignBase*
        asParmAssign() const
        {
            return nullptr;
        }

        virtual inline
        TreeNodeSelector*
        asTreeNodeSelector()
        {
            return nullptr;
        }

        virtual inline
        const TreeNodeSelector*
        asTreeNodeSelector() const
        {
            return nullptr;
        }

        virtual inline
        RenderAction*
        asRenderAction()
        {
            return nullptr;
        }

        virtual inline
        const RenderAction*
        asRenderAction() const
        {
            return nullptr;
        }
    };
}
}
