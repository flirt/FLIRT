// ========================================================================= //
// Copyright 2016-2018 SUPAMONKS_STUDIO                                      //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
//                                                                           //
// ========================================================================= //
#pragma once

#include <memory>
#include <smks/parm/BuiltIn.hpp>
#include "smks/scn/TreeNode.hpp"

namespace smks { namespace scn
{
    class TreeNode;

    /////////////////
    // class ParmEdit
    /////////////////
    class ParmEdit
    {
    public:
        typedef std::shared_ptr<ParmEdit> Ptr;
    protected:
        const unsigned int      _parmId;
        const parm::ParmFlags   _flags;
    protected:
        inline
        ParmEdit(unsigned int parmId, parm::ParmFlags flags):
            _parmId (parmId),
            _flags  (flags)
        { }
    public:
        virtual inline
        ~ParmEdit()
        { }

        virtual
        void
        apply(scn::TreeNode&) = 0;

        inline
        unsigned int
        parmId() const
        {
            return _parmId;
        }
    protected:
        template<typename ValueType> static inline
        unsigned int
        set(scn::TreeNode&, const char*, unsigned int, const ValueType&, parm::ParmFlags);

        template<typename ValueType> static inline
        void
        update(scn::TreeNode&, unsigned int, const ValueType&, parm::ParmFlags);

        static inline
        void
        unset(scn::TreeNode&, unsigned int, parm::ParmFlags);
    };

    ///////////////////////////////
    // class ParmSetEdit<ValueType>
    ///////////////////////////////
    template<typename ValueType>
    class ParmSetEdit:
        public ParmEdit
    {
    private:
        const std::string   _parmName;
        const ValueType     _parmValue;
    public:
        static inline
        Ptr
        create(const char* parmName, unsigned int parmId, const ValueType& parmValue, parm::ParmFlags flags)
        {
            Ptr ptr(new ParmSetEdit(parmName, parmId, parmValue, flags));
            return ptr;
        }
        static inline
        Ptr
        create(const parm::BuiltIn& parm, const ValueType& parmValue, parm::ParmFlags flags)
        {
            Ptr ptr(new ParmSetEdit(parm.c_str(), parm.id(), parmValue, flags));
            return ptr;
        }
    private:
        inline
        ParmSetEdit(const char* parmName, unsigned int parmId, const ValueType& parmValue, parm::ParmFlags flags):
            ParmEdit    (parmId, flags),
            _parmName   (parmName),
            _parmValue  (parmValue)
        { }
    public:
        inline
        void
        apply(scn::TreeNode& node)
        {
            ParmEdit::set<ValueType>(node, _parmName.c_str(), _parmId, _parmValue, _flags);
        }
    };

    //////////////////////////////////
    // class ParmUpdateEdit<ValueType>
    //////////////////////////////////
    template<typename ValueType>
    class ParmUpdateEdit:
        public ParmEdit
    {
    private:
        const ValueType _parmValue;
    public:
        static inline
        Ptr
        create(unsigned int parmId, const ValueType& parmValue, parm::ParmFlags flags)
        {
            Ptr ptr(new ParmUpdateEdit(parmId, parmValue, flags));
            return ptr;
        }
    private:
        inline
        ParmUpdateEdit(unsigned int parmId, const ValueType& parmValue, parm::ParmFlags flags):
            ParmEdit    (parmId, flags),
            _parmValue  (parmValue)
        { }
    public:
        inline
        void
        apply(scn::TreeNode& node)
        {
            ParmEdit::update<ValueType>(node, _parmId, _parmValue, _flags);
        }
    };

    //////////////////////
    // class ParmUnsetEdit
    //////////////////////
    class ParmUnsetEdit:
        public ParmEdit
    {
    public:
        static inline
        Ptr
        create(unsigned int parmId, parm::ParmFlags flags)
        {
            Ptr ptr(new ParmUnsetEdit(parmId, flags));
            return ptr;
        }
        static inline
        Ptr
        create(const parm::BuiltIn& parm, parm::ParmFlags flags)
        {
            Ptr ptr(new ParmUnsetEdit(parm.id(), flags));
            return ptr;
        }
    private:
        inline
        ParmUnsetEdit(unsigned int parmId, parm::ParmFlags flags):
            ParmEdit(parmId, flags)
        { }
    public:
        inline
        void
        apply(scn::TreeNode& node)
        {
            ParmEdit::unset(node, _parmId, _flags);
        }
    };
}
}
