// ========================================================================= //
// Copyright 2016-2018 SUPAMONKS_STUDIO                                      //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
//                                                                           //
// ========================================================================= //
#pragma once

#include "smks/scn/SchemaBasedSceneObject.hpp"

namespace Alembic {
namespace Abc {
namespace ALEMBIC_VERSION_NS {
    class IObject;
} }
}

namespace smks { namespace scn
{
    class AbstractCameraSchema;

    class FLIRT_SCN_EXPORT Camera:
        public SchemaBasedSceneObject
    {
        friend class CameraParmBasedSchema;
    public:
        typedef std::shared_ptr<Camera> Ptr;
        typedef std::weak_ptr<Camera>   WPtr;
    public:
        ////////////////////
        // struct Attributes
        ////////////////////
        struct FLIRT_SCN_EXPORT Attributes
        {
            float focalLength;      //<! in millimeters
            float xAspectRatio;
            float yAperture;        //<! in millimeters
            float yFieldOfViewD;    //<! in degrees
            float zNear;
            float zFar;
            float focusDistance;
            float fStop;

        public:
            inline
            Attributes():
                focalLength     (0.0f),
                xAspectRatio    (0.0f),
                yAperture       (0.0f),
                yFieldOfViewD   (0.0f),
                zNear           (0.0f),
                zFar            (0.0f),
                focusDistance   (0.0f),
                fStop           (0.0f)
            { }
        };

    public:
        typedef unsigned short  ComplianceTests;

        /////////////////////////
        // enum ComplianceTestBit
        /////////////////////////
        enum ComplianceTestBit
        {
            NO_FILM_OFFSET = 0,
            NO_LENS_SQUEEZE_RATIO,
            NO_OVERSCAN,
            NO_PRE_SCALE,
            NO_POST_SCALE,
            NO_CAMERA_SCALE,
            NO_FILM_TRANSLATE,
            NO_FILM_FIT_OFFSET,
            ONLY_FILL_FILM_FIT
        };

        ///////////////
        // enum FilmFit
        ///////////////
        enum FilmFit
        {
            FILL,
            HORIZONTAL,
            VERTICAL,
            OVERSCAN
        };

        ////////////////////////////
        // struct FilmBackOperations
        ////////////////////////////
        struct FLIRT_SCN_EXPORT FilmBackOperations
        {
            FilmFit fit;
            double  preScale;
            double  cameraScale;
            double  postScale;
            double  filmFitOffsetX;
            double  filmFitOffsetY;
            double  filmTranslateX;
            double  filmTranslateY;
        };

    private:
        typedef Alembic::Abc::ALEMBIC_VERSION_NS::IObject IObject;
    private:
        class DataHolder;

    public:
        static
        Ptr
        create(const char*,
               TreeNode::WPtr const& parent);
        static
        Ptr
        create(const IObject&,
               TreeNode::WPtr const& parent,
               TreeNode::WPtr const& archive);

    protected:
        Camera(const char*,
               TreeNode::WPtr const&);
        Camera(const IObject&,
               TreeNode::WPtr const& parent,
               TreeNode::WPtr const& archive);

    public:
        virtual inline
        xchg::NodeClass
        nodeClass() const
        {
            return xchg::NodeClass::CAMERA;
        }

        virtual inline
        Camera*
        asCamera()
        {
            return this;
        }

        virtual inline
        const Camera*
        asCamera() const
        {
            return this;
        }
    };
}
}
