// ========================================================================= //
// Copyright 2016-2018 SUPAMONKS_STUDIO                                      //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
//                                                                           //
// ========================================================================= //
#pragma once

#include "smks/scn/SchemaBasedSceneObject.hpp"

namespace Alembic {
namespace Abc {
namespace ALEMBIC_VERSION_NS {
    class IObject;
} }
}

namespace smks { namespace scn
{
    class AbstractXformSchema;

    class FLIRT_SCN_EXPORT Xform:
        public SchemaBasedSceneObject
    {
        friend class XformParmBasedSchema;
    public:
        typedef std::shared_ptr<Xform>  Ptr;
        typedef std::weak_ptr<Xform>    WPtr;
    private:
        typedef Alembic::Abc::ALEMBIC_VERSION_NS::IObject IObject;
    private:
        class DataHolder;
    public:
        static
        Ptr
        create(const char*,
               TreeNode::WPtr const&);
        static
        Ptr
        create(const IObject&,
               TreeNode::WPtr const& parent,
               TreeNode::WPtr const& archive);

    protected:
        Xform(const char*,
              TreeNode::WPtr const&);
        Xform(const IObject&,
              TreeNode::WPtr const& parent,
              TreeNode::WPtr const& archive);
    protected:
        //-------------------
        // parameter handling
        //-------------------
        virtual
        bool
        mustSendToScene(const xchg::ParmEvent&) const;
        //-------------------
    public:
        virtual inline
        xchg::NodeClass
        nodeClass() const
        {
            return xchg::NodeClass::XFORM;
        }

        virtual inline
        Xform*
        asXform()
        {
            return this;
        }

        virtual inline
        const Xform*
        asXform() const
        {
            return this;
        }
    };
}
}
