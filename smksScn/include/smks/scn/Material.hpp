// ========================================================================= //
// Copyright 2016-2018 SUPAMONKS_STUDIO                                      //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
//                                                                           //
// ========================================================================= //
#pragma once

#include "smks/scn/Shader.hpp"

namespace smks { namespace scn
{
    /////////////////
    // class Material
    /////////////////
    class FLIRT_SCN_EXPORT Material:
        public Shader
    {
    public:
        typedef std::shared_ptr<Material>   Ptr;
        typedef std::weak_ptr<Material>     WPtr;

    public:
        static
        Ptr
        create(const char*  code,
               const char*  name,
               TreeNode::WPtr const&);

    protected:
        inline
        Material(const char* name, TreeNode::WPtr const& parent):
            Shader(name, parent)
        { }

    public:
        virtual
        bool
        isParameterWritable(unsigned int) const;

        virtual
        bool
        isParameterRelevant(unsigned int)const;
    private:
        bool
        isParameterRelevantForClass(unsigned int) const;

    public:
        virtual inline
        xchg::NodeClass
        nodeClass() const
        {
            return xchg::NodeClass::MATERIAL;
        }

        virtual inline
        Material*
        asMaterial()
        {
            return this;
        }

        virtual inline
        const Material*
        asMaterial() const
        {
            return this;
        }
    };
}
}
