// ========================================================================= //
// Copyright 2016-2018 SUPAMONKS_STUDIO                                      //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
//                                                                           //
// ========================================================================= //
#pragma once

#include <vector>

#include <smks/sys/Exception.hpp>
#include <smks/parm/BuiltIn.hpp>
#include <smks/parm/BuiltIns.hpp>

#include "smks/scn/TreeNode.hpp"
#include "smks/scn/ParmConnectionBase.hpp"

namespace smks { namespace scn
{
    ////////////////////////
    // class ParmConnections
    ////////////////////////
    class ParmConnections
    {
    protected:
        typedef std::vector<ParmConnectionBase::WPtr> Connections;
    private:
        class PImpl;
    private:
        PImpl* _pImpl;

    public:
        ParmConnections(TreeNode::Ptr const&, const char* name);

        virtual
        ~ParmConnections();

        const char*
        name() const;

        bool
        empty() const;

        ParmConnectionBase::WPtr const&
        front() const;

        void
        add(ParmConnectionBase::WPtr const&);

        void
        clear();

        void
        remove(ParmConnectionBase::WPtr const&);

        virtual inline
        void
        dirty()
        { }

        virtual inline
        void
        evaluate()
        { }

    protected:
        size_t
        numConnections() const;

        ParmConnectionBase::WPtr const&
        getConnection(size_t) const;

        TreeNode::WPtr const&
        node() const;
    };


    /////////////////////////////
    // class InputParmConnections
    /////////////////////////////
    class InputParmConnections:
        public ParmConnections
    {
    private:
        int _best;  //<! ID of the connection with maximal priority

    public:
        inline
        InputParmConnections(TreeNode::Ptr const& node, const char* name):
            ParmConnections (node, name),
            _best           (-1)
        { }

        virtual inline
        void
        dirty()
        {
            _best = -1;
        }

        virtual inline
        void
        evaluate()
        {
            if (_best < 0)
                _best = getBestConnection(parm::priority().id());

            if (_best < 0)
            {
                TreeNode::Ptr const& node = this->node().lock();
                if (node)
                {
                    const unsigned int          parmId  = util::string::getId(name());
                    parm::BuiltIn::Ptr const&   parm    = parm::builtIns().find(parmId);
                    node->_unsetParameter(parmId, parm.get(), parm::KEEPS_CONNECTIONS);
                }
                return;
            }
            ParmConnectionBase::Ptr const& cnx = getConnection(_best).lock();

            if (cnx)
                cnx->evaluate();
        }

    private:
        inline
        int
        getBestConnection(unsigned int priorityId) const
        {
            int best        = -1;
            int maxPriority = -INT_MAX;
            for (size_t i = 0; i < numConnections(); ++i)
            {
                ParmConnectionBase::Ptr const& cnx = getConnection(i).lock();
                if (!cnx ||
                    !cnx->established())
                    continue;

                if (maxPriority < cnx->priority())
                {
                    best        = static_cast<int>(i);
                    maxPriority = cnx->priority();
                }
            }
            return best;
        }
    };
}
}
