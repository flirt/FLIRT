// ========================================================================= //
// Copyright 2016-2018 SUPAMONKS_STUDIO                                      //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
//                                                                           //
// ========================================================================= //
#pragma once

#include <smks/sys/Exception.hpp>
#include <smks/parm/BuiltIn.hpp>
#include <smks/parm/BuiltIns.hpp>

#include "smks/scn/ParmAssignBase.hpp"
#include "smks/scn/Scene.hpp"

namespace smks { namespace scn
{
    template<typename ValueType>
    class ParmAssign:
        public ParmAssignBase
    {
    public:
        typedef std::shared_ptr<ParmAssign> Ptr;
    public:
        static
        Ptr
        create(const char*              parmName,
               ValueType                parmValue,
               const char*              name,
               TreeNode::WPtr const&    parent)
        {
            if (parmName == nullptr ||
                strlen(parmName) == 0)
                throw sys::Exception("Invalid parameter name.", "Parameter Assignment Creation");

            Ptr ptr(new ParmAssign(name, parent));

            ptr->build(ptr);
            ptr->setParameter<std::string>  (parm::parmName(),  parmName);
            ptr->setParameter<ValueType>    (parm::parmValue(), parmValue);

            return ptr;
        }

        //<! when visitor parameters are specified (direction and filter),
        // assignees may differ from the selection.
        static
        Ptr
        create(const char*                          parmName,
               ValueType                            parmValue,
               TreeNodeVisitor::Direction           visitorDir,
               AbstractTreeNodeFilter::Ptr const&   visitorFilter,
               const char*                          name,
               TreeNode::WPtr const&                parent)
        {
            if (parmName == nullptr ||
                strlen(parmName) == 0)
                throw sys::Exception("Invalid parameter name.", "Parameter Assignment Creation");

            Ptr ptr(new ParmAssign(visitorDir, visitorFilter, name, parent));

            ptr->build(ptr);
            ptr->setParameter<std::string>  (parm::parmName(),  parmName);
            ptr->setParameter<ValueType>    (parm::parmValue(), parmValue);

            return ptr;
        }

    protected:
        inline
        ParmAssign(const char* name, TreeNode::WPtr const& parent):
            ParmAssignBase(name, parent)
        { }

        inline
        ParmAssign(TreeNodeVisitor::Direction           visitorDir,
                   AbstractTreeNodeFilter::Ptr const&   visitorFilter,
                   const char*                          name,
                   TreeNode::WPtr const&                parent):
            ParmAssignBase(visitorDir, visitorFilter, name, parent)
        { }

    public:
        virtual inline
        bool
        isParameterWritable(unsigned int parmId) const
        {
            if (isParameterRelevantForClass(parmId))
                return true;
            return ParmAssignBase::isParameterWritable(parmId);
        }

        virtual inline
        bool
        isParameterRelevant(unsigned int parmId)const
        {
            return isParameterRelevantForClass(parmId) || ParmAssignBase::isParameterRelevant(parmId);
        }

    private:
        inline
        bool
        isParameterRelevantForClass(unsigned int parmId) const
        {
            return parmId == parm::parmValue().id();
        }

        virtual inline
        const std::type_info&
        type() const
        {
            return typeid(ValueType);
        }

    protected:
        virtual inline
        unsigned int
        createConnectionToAssignee(Scene* scene, unsigned int targetId, const char* parmName, int priority)
        {
            return scene
                ? scene->connect<ValueType>(id(), parm::parmValue().c_str(), targetId, parmName, priority)
                : 0;
        }

        virtual inline
        void
        removeConnectionFromAssignee(Scene* scene, unsigned int cnxId)
        {
            if (scene)
                scene->disconnect(cnxId);
        }
    };
}
}
