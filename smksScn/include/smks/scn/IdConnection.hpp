// ========================================================================= //
// Copyright 2016-2018 SUPAMONKS_STUDIO                                      //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
//                                                                           //
// ========================================================================= //
#pragma once

#include "smks/scn/ParmConnection.hpp"

namespace smks { namespace scn
{
    class Scene;

    class IdConnection:
        public ParmConnection<unsigned int>
    {
        friend class Scene;

    public:
        typedef std::shared_ptr<IdConnection>   Ptr;
        typedef std::weak_ptr<IdConnection>     WPtr;
    private:
        typedef std::weak_ptr<Scene>            SceneWPtr;

    private:    // only Scene can create parameter connections
        static inline
        Ptr
        create(unsigned int     inNodeId,
               const char*      inParmName,
               unsigned int     outNodeId,
               const char*      outParmName,
               int              priority,
               SceneWPtr const& scene)
        {
            Ptr ptr(new IdConnection(inNodeId, inParmName, outNodeId, outParmName, priority, scene));

            ptr->build(ptr);
            return ptr;
        }

    protected:
        inline
        IdConnection(unsigned int       inNodeId,
                     const char*        inParmName,
                     unsigned int       outNodeId,
                     const char*        outParmName,
                     int                priority,
                     SceneWPtr const&   scene):
            ParmConnection<unsigned int>(inNodeId, inParmName, outNodeId, outParmName, priority, scene)
            { }

    public:
        virtual inline
        void
        evaluate()
        {
            TreeNode::Ptr const inNode      = this->inNode().lock();
            TreeNode::Ptr const outNode     = this->outNode().lock();

            if (!inNode || !outNode)
                return;

            unsigned int inNodeId = 0;

            if (inNode)
                inNode->getParameter<unsigned int>(inParmId(), inNodeId, 0);

            if (!inNode->parameterExistsAs<unsigned int>(inParmId()))
            {
                parm::BuiltIn::Ptr const& parm = parm::builtIns().find(outParmId());
                outNode->_unsetParameter(outParmId(), parm.get(), parm::KEEPS_CONNECTIONS);
            }
            else
            {
                if (!outNode->parameterExistsAs<unsigned int>(outParmId()))
                    outNode->setParameter<unsigned int>(outParmName(), inNodeId, parm::CREATES_LINK | parm::KEEPS_CONNECTIONS);
                else
                    outNode->_updateParameter<unsigned int>(outParmId(), inNodeId, parm::CREATES_LINK | parm::KEEPS_CONNECTIONS);
            }
        }
    };
}
}
