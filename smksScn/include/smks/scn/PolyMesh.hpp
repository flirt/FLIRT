// ========================================================================= //
// Copyright 2016-2018 SUPAMONKS_STUDIO                                      //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
//                                                                           //
// ========================================================================= //
#pragma once

#include "smks/scn/Drawable.hpp"

namespace Alembic {
namespace Abc {
namespace ALEMBIC_VERSION_NS {
    class IObject;
} }
}

namespace smks { namespace scn
{
    class BufferAttributes;
    class AbstractPolyMeshSchema;

    class FLIRT_SCN_EXPORT PolyMesh:
        public Drawable
    {
        friend class PolyMeshSchemaFromAbc;
        friend class Data;
    public:
        typedef std::shared_ptr<PolyMesh>   Ptr;
        typedef std::weak_ptr<PolyMesh>     WPtr;
    private:
        typedef Alembic::Abc::ALEMBIC_VERSION_NS::IObject IObject;

    protected:
        class DataHolder;
        class AbstractGeometry;
        class GeometryBase;
        class CoarseTriquadGeometry;
        class FacetedCoarseTriGeometry;
        class FacetedSmoothGeometry;

        class GeometrySanitizer;

    public:
        static
        Ptr
        create(const IObject&,
               TreeNode::WPtr const& parent,
               TreeNode::WPtr const& archive);

    protected:
        PolyMesh(const IObject&,
                 TreeNode::WPtr const&,
                 TreeNode::WPtr const&);

        void
        handleParmEvent(const xchg::ParmEvent&);

    public:
        std::ostream&
        glDump(std::ostream&) const;

        std::ostream&
        rtDump(std::ostream&) const;

    public:
        virtual inline
        xchg::NodeClass
        nodeClass() const
        {
            return xchg::NodeClass::MESH;
        }

        virtual inline
        PolyMesh*
        asPolyMesh()
        {
            return this;
        }

        virtual inline
        const PolyMesh*
        asPolyMesh() const
        {
            return this;
        }

    private:
        static
        void
        computeWireframeLines(AbstractGeometry&, std::vector<unsigned int>&);
    };
}
}
