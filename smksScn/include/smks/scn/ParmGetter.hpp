// ========================================================================= //
// Copyright 2016-2018 SUPAMONKS_STUDIO                                      //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
//                                                                           //
// ========================================================================= //
#pragma once

#include <memory>

namespace smks { namespace scn
{
    ///////////////////////////
    // class AbstractParmGetter
    ///////////////////////////
    class AbstractParmGetter
    {
    public:
        typedef std::shared_ptr<AbstractParmGetter> Ptr;
    public:
        inline
        AbstractParmGetter()
        { }
    private:
        AbstractParmGetter(const AbstractParmGetter&);
        AbstractParmGetter& operator=(const AbstractParmGetter&);
    public:
        virtual inline
        ~AbstractParmGetter()
        { }

        virtual
        const std::type_info&
        type() const = 0;
    };

    ///////////////////////////////////
    // class TypedParmGetter<ValueType>
    ///////////////////////////////////
    template<typename ValueType>
    class TypedParmGetter:
        public AbstractParmGetter
    {
    public:
        typedef std::shared_ptr<TypedParmGetter> Ptr;
    protected:
        inline
        TypedParmGetter():
            AbstractParmGetter()
        { }
    public:
        inline
        const std::type_info&
        type() const
        {
            return typeid(ValueType);
        }

        virtual
        bool
        operator()(const parm::BuiltIn&, ValueType&, unsigned int upToNodeId) = 0;
    };
}
}
