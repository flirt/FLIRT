// ========================================================================= //
// Copyright 2016-2018 SUPAMONKS_STUDIO                                      //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
//                                                                           //
// ========================================================================= //
#pragma once

#include <smks/xchg/VectorPtr.hpp>

#include "smks/scn/TreeNode.hpp"

namespace smks { namespace scn
{
    ////////////////////////////////
    // struct AbstractTreeNodeFilter
    ////////////////////////////////
    struct FLIRT_SCN_EXPORT AbstractTreeNodeFilter
    {
    public:
        typedef std::shared_ptr<AbstractTreeNodeFilter> Ptr;

    public:
        virtual
        ~AbstractTreeNodeFilter()
        { }

        virtual
        bool
        operator()(const TreeNode&) const = 0;
    };

    ////////////////////////////////
    // struct CompoundTreeNodeFilter
    ////////////////////////////////
    struct CompoundTreeNodeFilter:
        public AbstractTreeNodeFilter
    {
    public:
        typedef std::shared_ptr<CompoundTreeNodeFilter> Ptr;

    private:
        xchg::VectorPtr<AbstractTreeNodeFilter> _filters;

    public:
        static inline
        Ptr
        create()
        {
            Ptr ptr(new CompoundTreeNodeFilter());
            return ptr;
        }

    protected:
        CompoundTreeNodeFilter():
            _filters()
        { }

    public:
        inline
        bool
        operator()(const TreeNode& n) const
        {
            for (size_t i = 0; i < _filters.size(); ++i)
                if (_filters[i]->operator()(n))
                    return true;
            return false;
        }

        inline
        void
        add(AbstractTreeNodeFilter::Ptr const& filter)
        {
            if (filter.get() == this)
                return;
            _filters.add(filter);
        }

        inline
        void
        remove(AbstractTreeNodeFilter::Ptr const& filter)
        {
            _filters.remove(filter);
        }
    };

    ///////////////////////////////
    // struct FilterTreeNodeByClass
    ///////////////////////////////
    struct FilterTreeNodeByClass:
        public AbstractTreeNodeFilter
    {
    public:
        typedef std::shared_ptr<FilterTreeNodeByClass> Ptr;

    private:
        const xchg::NodeClass _query;

    public:
        static inline
        Ptr
        create(xchg::NodeClass query)
        {
            Ptr ptr(new FilterTreeNodeByClass(query));
            return ptr;
        }

    protected:
        explicit
        FilterTreeNodeByClass(xchg::NodeClass query):
            _query(query)
        { }

    public:
        bool
        operator()(const TreeNode& n) const
        {
            return (_query & n.nodeClass()) != 0 &&
                (~_query & n.nodeClass()) == 0;
        }
    };

    /////////////////////////////////
    // struct FilterTreeNodeByBuiltIn
    /////////////////////////////////
    template<typename ValueType>
    struct FilterTreeNodeByBuiltIn:
        public AbstractTreeNodeFilter
    {
    public:
        typedef std::shared_ptr<FilterTreeNodeByBuiltIn> Ptr;

    private:
        const parm::BuiltIn*    _builtIn;   // does not own it
        const ValueType         _parmValue;

    public:
        static inline
        Ptr
        create(const parm::BuiltIn& builtIn,
               const ValueType&     parmValue)
        {
            Ptr ptr(new FilterTreeNodeByBuiltIn(builtIn, parmValue));
            return ptr;
        }

    protected:
        FilterTreeNodeByBuiltIn(const parm::BuiltIn& builtIn,
                                const ValueType& parmValue):
            _builtIn    (&builtIn),
            _parmValue  (parmValue)
        {
#if defined(_DEBUG)
            if (!_builtIn->compatibleWith<ValueType>())
                throw sys::Exception(
                    "Inconsistent built-in based filter definition.",
                    "Built-In Based Node Filter");
#endif // defined(_DEBUG)
        }

    public:
        inline
        bool
        operator()(const TreeNode& n) const
        {
            ValueType parmValue;
            n.getParameter<ValueType>(*_builtIn, parmValue);
            return xchg::equal<ValueType>(parmValue, _parmValue);
        }
    };

    /////////////////////////////
    // class FilterTreeNodeByName
    /////////////////////////////
    class FLIRT_SCN_EXPORT FilterTreeNodeByName:
        public AbstractTreeNodeFilter
    {
    public:
        typedef std::shared_ptr<FilterTreeNodeByName> Ptr;
    private:
        class PImpl;
    private:
        PImpl* _pImpl;
    public:
        static inline
        Ptr
        create(const char*  query,
               bool         isQueryRegex,
               bool         useFullName = false)
        {
            Ptr ptr(new FilterTreeNodeByName(
                query,
                isQueryRegex,
                useFullName));
            return ptr;
        }
    protected:
        FilterTreeNodeByName(const char*    query,
                             bool           isQueryRegex,
                             bool           useFullName);
    public:
        ~FilterTreeNodeByName();
    protected:
        bool
        operator()(const TreeNode&) const;
    };
}
}
