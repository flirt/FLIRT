// ========================================================================= //
// Copyright 2016-2018 SUPAMONKS_STUDIO                                      //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
//                                                                           //
// ========================================================================= //
#pragma once

#include <memory>
#include "smks/scn/SceneObject.hpp"

namespace smks { namespace scn
{
    class AbstractObjectSchema;
    class AbstractObjectDataHolder;

    class FLIRT_SCN_EXPORT SchemaBasedSceneObject:
        public SceneObject
    {
        friend class SchemaBaseFromAbc;
        friend class AbstractObjectSchema;
        friend class AbstractObjectDataHolder;
    public:
        typedef std::shared_ptr<SchemaBasedSceneObject> Ptr;
        typedef std::weak_ptr<SchemaBasedSceneObject>   WPtr;
    private:
        class PImpl;
    private:
        PImpl* _pImpl;
    protected:
        SchemaBasedSceneObject(const char*, TreeNode::WPtr const&);

        virtual
        void
        build(Ptr const&,
              AbstractObjectSchema*,        // takes ownership
              AbstractObjectDataHolder*);   // takes ownership

        virtual
        void
        erase();
    public:
        ~SchemaBasedSceneObject();

    private:
        void
        uninitialize(); // completely unloads content (undo initialize())
        void
        initialize();   // loads entirety of content from schema

        void
        uninitializeUserAttribs();  // completely unloads user attributes (undo initializeUserAttribs())
        void
        initializeUserAttribs();    // loads user attributes from schema

        void
        dirtySample(int idx, bool synchronize);

    public:
        //-------------------
        // parameter handling
        //-------------------
        virtual
        bool
        isParameterRelevant(unsigned int)const;
    private:
        bool
        isParameterRelevantForClass(unsigned int) const;
    public:
        virtual
        bool
        isParameterReadable(unsigned int) const;
        virtual
        bool
        isParameterWritable(unsigned int) const;
        virtual
        bool
        overrideParameterDefaultValue(unsigned int, parm::Any&) const;
        virtual
        bool
        preprocessParameterEdit(const char*, unsigned int, parm::ParmFlags, const parm::Any&);

    protected:
        virtual
        void
        handleParmEvent(const xchg::ParmEvent&);

        virtual
        bool
        mustSendToScene(const xchg::ParmEvent&) const;

        AbstractObjectSchema*
        schema();
        AbstractObjectDataHolder*
        dataHolder();

    public:
        void
        getStoredTimes(xchg::Vector<float>&) const;
    };
}
}
