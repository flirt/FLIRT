// ========================================================================= //
// Copyright 2016-2018 SUPAMONKS_STUDIO                                      //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
//                                                                           //
// ========================================================================= //
#pragma once

#include "smks/scn/GraphAction.hpp"
#include "smks/scn/TreeNodeVisitor.hpp"
#include "smks/scn/TreeNodeFilter.hpp"

namespace smks { namespace scn
{
    class FLIRT_SCN_EXPORT ParmAssignBase:
        public GraphAction
    {
    public:
        typedef std::shared_ptr<ParmAssignBase> Ptr;
        typedef std::weak_ptr<ParmAssignBase>   WPtr;

    protected:
        enum    Operation { ADD = 0, REMOVE };
    private:
        class   PImpl;
        class   NodeVisitor;

    private:
        PImpl *_pImpl;

    protected:
        ParmAssignBase(const char*, TreeNode::WPtr const&);

        ParmAssignBase(TreeNodeVisitor::Direction,
                       AbstractTreeNodeFilter::Ptr const&,
                       const char*,
                       TreeNode::WPtr const&);
        void
        build(TreeNode::Ptr const&);

    public:
        ~ParmAssignBase();

    protected:
        virtual
        void
        erase();

    public:
        virtual
        const std::type_info&
        type() const = 0;

        virtual
        bool
        isParameterWritable(unsigned int) const;

        virtual
        bool
        isParameterRelevant(unsigned int)const;
    private:
        bool
        isParameterRelevantForClass(unsigned int) const;

    public:
        virtual inline
        xchg::NodeClass
        nodeClass() const
        {
            return xchg::NodeClass::ASSIGN;
        }

        virtual inline
        ParmAssignBase*
        asParmAssignBase()
        {
            return this;
        }

        virtual inline
        const ParmAssignBase*
        asParmAssignBase() const
        {
            return this;
        }

    protected:
        virtual
        void
        handleSceneEvent(const xchg::SceneEvent&);

        virtual
        void
        handleParmEvent(const xchg::ParmEvent&);

        void
        process(unsigned int targetId, Operation);  //<! called by visitor

        virtual
        unsigned int
        createConnectionToAssignee(Scene*, unsigned int targetId, const char* parmName, int priority) = 0;

        virtual
        void
        removeConnectionFromAssignee(Scene*, unsigned int cnxId) = 0;
    };
}
}
