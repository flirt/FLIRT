// ========================================================================= //
// Copyright 2016-2018 SUPAMONKS_STUDIO                                      //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
//                                                                           //
// ========================================================================= //
#pragma once

#include "smks/scn/SchemaBasedSceneObject.hpp"

namespace smks { namespace scn
{
    class Points;
    class Curves;
    class PolyMesh;

    class FLIRT_SCN_EXPORT Drawable:
        public SchemaBasedSceneObject
    {
    public:
        typedef std::shared_ptr<Drawable>   Ptr;
        typedef std::weak_ptr<Drawable>     WPtr;

    protected:
        inline
        Drawable(const char* name, TreeNode::WPtr const& parent):
            SchemaBasedSceneObject(name, parent)
        { }

    public:
        virtual
        bool
        isParameterWritable(unsigned int) const;

        virtual
        bool
        isParameterRelevant(unsigned int)const;
    private:
        bool
        isParameterRelevantForClass(unsigned int) const;
    public:
        virtual inline
        xchg::NodeClass
        nodeClass() const
        {
            return xchg::NodeClass::DRAWABLE;
        }

        virtual inline
        Drawable*
        asDrawable()
        {
            return this;
        }

        virtual inline
        const Drawable*
        asDrawable() const
        {
            return this;
        }

        virtual inline
        Points*
        asPoints()
        {
            return nullptr;
        }

        virtual inline
        const Points*
        asPoints() const
        {
            return nullptr;
        }

        virtual inline
        Curves*
        asCurves()
        {
            return nullptr;
        }

        virtual inline
        const Curves*
        asCurves() const
        {
            return nullptr;
        }

        virtual inline
        PolyMesh*
        asPolyMesh()
        {
            return nullptr;
        }

        virtual inline
        const PolyMesh*
        asPolyMesh() const
        {
            return nullptr;
        }
    };
}
}
