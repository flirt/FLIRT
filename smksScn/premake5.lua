-- ========================================================================= --
-- Copyright 2016-2018 SUPAMONKS_STUDIO                                      --
-- Author: Pierre-Edouard Landes <pel@supamonks.com>                         --
--                                                                           --
-- This program is free software: you can redistribute it and/or modify      --
-- it under the terms of the GNU Lesser General Public License as            --
-- published by the Free Software Foundation, either version 3 of the        --
-- License, or (at your option) any later version.                           --
--                                                                           --
-- This program is distributed in the hope that it will be useful,           --
-- but WITHOUT ANY WARRANTY; without even the implied warranty of            --
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             --
-- GNU Lesser General Public License for more details.                       --
--                                                                           --
-- You should have received a copy of the GNU Lesser General Public License  --
-- along with this program.  If not, see <https://www.gnu.org/licenses/>.    --
--                                                                           --
-- ========================================================================= --


smks.module.uses_scn = function ()

    smks.module.links(
        smks.module.name.scn,
        smks.module.kind.scn)

    smks.module.includes(smks.module.name.sys)
    smks.module.includes(smks.module.name.xchg)
    smks.module.includes(smks.module.name.parm)

end


smks.module.copy_scn_to_targetdir = function()

    smks.module.copy_to_targetdir(
        smks.module.name.scn)

    smks.module.copy_util_to_targetdir()
    smks.module.copy_math_to_targetdir()
    smks.module.copy_xchg_to_targetdir()

end


project(smks.module.name.scn)

    language 'C++'
    smks.module.set_kind(smks.module.kind.scn)

    files
    {
        'include/**.hpp',
        'src/**.hpp',
        'src/**.cpp',
    }
    defines
    {
        'FLIRT_SCN_DLL',
        'FLIRT_IRRELEVANT_BUILTIN_WRITABLE',
    }
    includedirs
    {
        'include',
    }

    filter 'configurations:Debug'
        defines
        {
            'DEBUG_DTOR',
            'CHECK_IDS',
            'LOG_EVENTS',
            -- 'FLIRT_LOG_CURVES',
        }
    filter {}

    smks.dep.boost.includes()
    smks.dep.ilmbase.links()
    smks.dep.opensubdiv.links()
    smks.dep.alembic.links()
    smks.dep.zlib.links()
    smks.dep.json.links()

    smks.module.includes(smks.module.name.parm)
    smks.module.includes(smks.module.name.sys)
    smks.module.uses_log()
    smks.module.uses_strid()
    smks.module.uses_math()
    smks.module.uses_util()
    smks.module.uses_asset()
    smks.module.uses_xchg()


