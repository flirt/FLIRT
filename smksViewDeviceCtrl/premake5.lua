-- ========================================================================= --
-- Copyright 2016-2018 SUPAMONKS_STUDIO                                      --
-- Author: Pierre-Edouard Landes <pel@supamonks.com>                         --
--                                                                           --
-- This program is free software: you can redistribute it and/or modify      --
-- it under the terms of the GNU Lesser General Public License as            --
-- published by the Free Software Foundation, either version 3 of the        --
-- License, or (at your option) any later version.                           --
--                                                                           --
-- This program is distributed in the hope that it will be useful,           --
-- but WITHOUT ANY WARRANTY; without even the implied warranty of            --
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             --
-- GNU Lesser General Public License for more details.                       --
--                                                                           --
-- You should have received a copy of the GNU Lesser General Public License  --
-- along with this program.  If not, see <https://www.gnu.org/licenses/>.    --
--                                                                           --
-- ========================================================================= --


smks.module.uses_view_device_ctrl = function ()

    smks.module.links(
        smks.module.name.view_device_ctrl,
        smks.module.kind.view_device_ctrl)

    smks.dep.osg.includes()

    smks.module.includes(smks.module.name.sys)
    smks.module.includes(smks.module.name.util_osg)

end


smks.module.copy_view_device_ctrl_to_targetdir = function ()

    smks.module.copy_to_targetdir(
        smks.module.name.view_device_ctrl)

    smks.module.copy_clientbase_to_targetdir()
    smks.module.copy_util_osg_to_targetdir()

end


project(smks.module.name.view_device_ctrl)

    language 'C++'
    smks.module.set_kind(smks.module.kind.view_device_ctrl)

    files
    {
        'include/**.hpp',
        'src/**.hpp',
        'src/**.cpp'
    }
    defines
    {
        'FLIRT_VIEW_DEVICE_CTRL_DLL',
    }
    includedirs
    {
        'include',
    }

    smks.dep.boost.includes()
    smks.dep.json.links()
    smks.dep.osg.links()

    smks.module.uses_log()
    smks.module.uses_util()
    smks.module.uses_asset()
    smks.module.uses_xchg()
    smks.module.uses_clientbase()
    smks.module.uses_view_gl()
    smks.module.uses_util_osg()
    smks.module.includes(smks.module.name.view_device_api)
