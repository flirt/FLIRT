// ========================================================================= //
// Copyright 2016-2018 SUPAMONKS_STUDIO                                      //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
//                                                                           //
// ========================================================================= //
#include <iostream>

#include <osg/ComputeBoundsVisitor>

#include <smks/xchg/IdSet.hpp>
#include <smks/xchg/BoundingBox.hpp>
#include <smks/xchg/ClientEvent.hpp>
#include <smks/xchg/SceneEvent.hpp>
#include <smks/xchg/ParmEvent.hpp>
#include <smks/xchg/MouseFlags.hpp>
#include <smks/parm/BuiltIns.hpp>
#include <smks/parm/BuiltIn.hpp>
#include <smks/ClientBase.hpp>
#include <smks/AbstractClientObserver.hpp>
#include <smks/AbstractClientNodeObserver.hpp>
#include <smks/client_node_filters.hpp>
#include <smks/view/AbstractDevice.hpp>

#include "smks/view/OrbitCameraHandler.hpp"
#include <smks/sys/Log.hpp>
#include <smks/sys/Exception.hpp>

namespace smks { namespace view
{
    static const osg::Vec3d LEFT_VECTOR     = osg::Vec3d(1.0, 0.0, 0.0);
    static const osg::Vec3d FORWARD_VECTOR  = osg::Vec3d(0.0, 0.0, -1.0);
    static const osg::Vec3d UP_VECTOR       = osg::Vec3d(0.0, 1.0, 0.0);

    //////////////
    // class PImpl
    //////////////
    class OrbitCameraHandler::PImpl
    {
    private:
        class YUpFrameCallback;
        class ClientObserver;
        class ClientNodeObserver;
    private:
        OrbitCameraHandler&             _self;
        AbstractClientObserver::Ptr     _clientObserver;
        AbstractClientNodeObserver::Ptr _nodeObserver;
        bool                            _enabled;
        mutable bool                    _backToHomePosition;
    public:
        explicit
        PImpl(OrbitCameraHandler& self);

        inline
        void
        build(AbstractDevice*);

        inline
        void
        dispose(AbstractDevice*);

        inline
        bool
        handle(const osgGA::GUIEventAdapter&,
               osgGA::GUIActionAdapter&);

        inline
        void
        enabled(bool value)
        {
            _enabled = value;
        }

        inline
        bool
        enabled() const
        {
            return _enabled;
        }

        inline
        void
        handle(ClientBase&, const xchg::ClientEvent&);
    private:
        inline
        void
        handleSceneSet(ClientBase&, unsigned int);

        inline
        void
        handleSceneUnset(ClientBase&, unsigned int);

    public:
        inline
        void
        handle(ClientBase&, const xchg::SceneEvent&);
    private:
        inline
        void
        handleNodeCreated(ClientBase&, unsigned int);

        inline
        void
        handleNodeDeleted(ClientBase&, unsigned int);

    public:
        void
        handleSceneParmEvent(ClientBase&, const xchg::ParmEvent&);

        inline
        void
        handle(ClientBase&, unsigned int, const xchg::ParmEvent&);
    };

    ///////////////////////
    // class ClientObserver
    ///////////////////////
    class OrbitCameraHandler::PImpl::ClientObserver:
        public AbstractClientObserver
    {
    private:
        PImpl& _pImpl;
    public:
        static inline
        Ptr
        create(PImpl& pImpl)
        {
            Ptr ptr(new ClientObserver(pImpl));
            return ptr;
        }
    protected:
        explicit
        ClientObserver(PImpl& pImpl):
            _pImpl(pImpl)
        { }
    public:
        virtual inline
        void
        handle(ClientBase& client, const xchg::ClientEvent& e)
        {
            _pImpl.handle(client, e);
        }

        virtual inline
        void
        handle(ClientBase& client, const xchg::SceneEvent& e)
        {
            _pImpl.handle(client, e);
        }

        virtual inline
        void
        handleSceneParmEvent(ClientBase& client, const xchg::ParmEvent& e)
        {
            _pImpl.handleSceneParmEvent(client, e);
        }
    };

    ///////////////////////////
    // class ClientNodeObserver
    ///////////////////////////
    class OrbitCameraHandler::PImpl::ClientNodeObserver:
        public AbstractClientNodeObserver
    {
    private:
        PImpl& _pImpl;
    public:
        static inline
        Ptr
        create(PImpl& pImpl)
        {
            Ptr ptr(new ClientNodeObserver(pImpl));
            return ptr;
        }
    protected:
        explicit
        ClientNodeObserver(PImpl& pImpl):
            _pImpl(pImpl)
        { }
    public:
        virtual inline
        void
        handle(ClientBase& client, unsigned int node, const xchg::ParmEvent& e)
        {
            _pImpl.handle(client, node, e);
        }
    };

    /////////////////////////
    // class YUpFrameCallback
    /////////////////////////
    class OrbitCameraHandler::PImpl::YUpFrameCallback:
        public osgGA::CameraManipulator::CoordinateFrameCallback
    {
    private:
        const osg::CoordinateFrame _frame;

    public:
        YUpFrameCallback():
            _frame(
                LEFT_VECTOR.x(),    LEFT_VECTOR.y(),    LEFT_VECTOR.z(),    0.0, // side vector
                FORWARD_VECTOR.x(), FORWARD_VECTOR.y(), FORWARD_VECTOR.z(), 0.0, // front vector
                UP_VECTOR.x(),      UP_VECTOR.y(),      UP_VECTOR.z(),      0.0, // up vector
                0.0,                0.0,                0.0,                1.0)
        { }

        virtual inline
        osg::CoordinateFrame
        getCoordinateFrame(const osg::Vec3d&) const
        {
            return _frame;
        }
    };
    /////////////////////////
}
}

using namespace smks;

// explicit
view::OrbitCameraHandler::PImpl::PImpl(OrbitCameraHandler& self):
    _self               (self),
    _clientObserver          (ClientObserver::create(*this)),
    _nodeObserver            (ClientNodeObserver::create(*this)),
    _enabled            (true),
    _backToHomePosition (true)
{
    _self.setAutoComputeHomePosition(false);

    // in order to simulate Maya camera
    _self.setWheelZoomFactor(-1.0 * _self.getWheelZoomFactor());
    _self.setVerticalAxisFixed(true);
    _self.setAllowThrow(false);

    _self.setCoordinateFrameCallback(new YUpFrameCallback());
}

// virtual
void
view::OrbitCameraHandler::PImpl::build(AbstractDevice* device)
{
    if (!device)
        return;
//    device->registerObserver(_deviceObs);

    ClientBase::Ptr const& client = device->client().lock();

    if (!client)
        return;
    client->registerObserver(_clientObserver);
    if (client->sceneId())
        handleSceneSet(*client, client->sceneId());
}

// virtual
void
view::OrbitCameraHandler::PImpl::dispose(AbstractDevice* device)
{
    if (!device)
        return;
//    device->deregisterObserver(_deviceObs);

    ClientBase::Ptr const& client = device->client().lock();

    if (!client)
        return;
    client->deregisterObserver(_clientObserver);
    if (client->sceneId())
        handleSceneUnset(*client, client->sceneId());

    //_deviceObs  = nullptr;
    _clientObserver  = nullptr;
    _nodeObserver    = nullptr;
}

// virtual
bool
view::OrbitCameraHandler::PImpl::handle(const osgGA::GUIEventAdapter&   ea,
                                        osgGA::GUIActionAdapter&        aa)
{
    if (ea.getHandled())
        return false;

    //---------------
    if (!enabled())
        return false;
    //---------------

    AbstractDevice*                 device  = dynamic_cast<AbstractDevice*>(&aa);
    ClientBase::Ptr const&  client  = device ? device->client().lock() : nullptr;
    if (!client)
        return false;

    if (device->viewportCameraXform() == 0 ||
        device->viewportCamera() == 0 ||
        device->displayedCamera() != device->viewportCamera())
        return false;

    /////////////////////////
    if (_backToHomePosition)
    {
        _self.home(ea, aa);
        _backToHomePosition = false;
    }
    /////////////////////////

    // at this point, the default camera is active and is valid.
    _self.OrbitManipulator::handle(ea, aa);

    osg::Vec3d eye;
    osg::Vec3d center;
    osg::Vec3d up;
    osg::Vec3d forward;
    osg::Vec3d side;

    _self.getTransformation(eye, center, up);

    up.normalize();

    forward = center - eye; // mapped to -z axis
    forward.normalize();

    side = forward ^ up;

    math::Affine3f xfm(math::Affine3f::Identity());

    xfm.matrix()
        << static_cast<float>(side.x()) , static_cast<float>(up.x()) , static_cast<float>(-forward.x()), static_cast<float>(eye.x())
        , static_cast<float>(side.y()) , static_cast<float>(up.y()) , static_cast<float>(-forward.y()), static_cast<float>(eye.y())
        , static_cast<float>(side.z()) , static_cast<float>(up.z()) , static_cast<float>(-forward.z()), static_cast<float>(eye.z())
        , 0.0f, 0.0f, 0.0f , 1.0f;

    client->setNodeParameter<math::Affine3f>(
        parm::transform(),
        xfm,
        device->viewportCameraXform());

    return false;
}

void
view::OrbitCameraHandler::PImpl::handle(ClientBase& client, const xchg::SceneEvent& e)
{
    if (e.type() == xchg::SceneEvent::NODE_CREATED)
        handleNodeCreated(client, e.node());
    else if (e.type() == xchg::SceneEvent::NODE_DELETED)
        handleNodeDeleted(client, e.node());
}

void
view::OrbitCameraHandler::PImpl::handle(ClientBase& client, const xchg::ClientEvent& e)
{
    if (e.type() == xchg::ClientEvent::SCENE_SET)
        handleSceneSet(client, e.scene());
    else if (e.type() == xchg::ClientEvent::SCENE_SET)
        handleSceneUnset(client, e.scene());
}

void
view::OrbitCameraHandler::PImpl::handleSceneSet(ClientBase& client, unsigned int scene)
{
    xchg::IdSet ids;

    client.getIds(ids, false, FilterClientNodeByClass::create(xchg::NodeClass::DRAWABLE));
    for (xchg::IdSet::const_iterator id = ids.begin(); id != ids.end(); ++id)
        handleNodeCreated(client, *id);

    char flags = 0;
    client.getNodeParameter<char>(parm::mouseFlags(), flags, scene);
    enabled((flags & xchg::MOUSE_CAPTURED) == 0);
}

void
view::OrbitCameraHandler::PImpl::handleSceneUnset(ClientBase& client, unsigned int scene)
{
    xchg::IdSet ids;

    client.getIds(ids, false, FilterClientNodeByClass::create(xchg::NodeClass::DRAWABLE));
    for (xchg::IdSet::const_iterator id = ids.begin(); id != ids.end(); ++id)
        handleNodeDeleted(client, *id);

    enabled(true);
}

void
view::OrbitCameraHandler::PImpl::handleNodeCreated(ClientBase& client, unsigned int node)
{
    if (client.getNodeClass(node) & xchg::NodeClass::DRAWABLE)
        client.registerObserver(_nodeObserver, node);
}

void
view::OrbitCameraHandler::PImpl::handleNodeDeleted(ClientBase& client, unsigned int node)
{
    if (client.getNodeClass(node) & xchg::NodeClass::DRAWABLE)
        client.deregisterObserver(_nodeObserver, node);
}

void
view::OrbitCameraHandler::PImpl::handleSceneParmEvent(ClientBase& client, const xchg::ParmEvent& e)
{
    if (e.parm() == parm::mouseFlags().id() &&
        e.emittedByNode())
    {
        char flags = 0;

        client.getNodeParameter<char>(parm::mouseFlags(), flags, e.node());
        enabled((flags & xchg::MOUSE_CAPTURED) == 0);
    }
}

void
view::OrbitCameraHandler::PImpl::handle(ClientBase& client, unsigned int node, const xchg::ParmEvent& e)
{
    char state = xchg::INACTIVE;

    if (e.parm() == parm::initialization().id() &&
        client.getNodeParameter<char>(parm::initialization(), state, node) &&
        state == xchg::ACTIVE)
            _backToHomePosition = true; // camera will return to its home position whenever a new scene object is loaded
}

///////////////////////////
// class OrbitCameraHandler
///////////////////////////
view::OrbitCameraHandler::OrbitCameraHandler():
    osgGA::OrbitManipulator(),
    _pImpl(new PImpl(*this))
{ }

view::OrbitCameraHandler::~OrbitCameraHandler()
{
    delete _pImpl;
}

// virtual
void
view::OrbitCameraHandler::build(AbstractDevice* device)
{
    _pImpl->build(device);
}

// virtual
void
view::OrbitCameraHandler::dispose(AbstractDevice* device)
{
    _pImpl->dispose(device);
}

// virtual
bool
view::OrbitCameraHandler::handle(const osgGA::GUIEventAdapter&  ea,
                                 osgGA::GUIActionAdapter&       aa)
{
    return _pImpl->handle(ea, aa);
}

void
view::OrbitCameraHandler::enabled(bool value)
{
    _pImpl->enabled(value);
}

bool
view::OrbitCameraHandler::enabled() const
{
    return _pImpl->enabled();
}

// virtual
void
view::OrbitCameraHandler::computeHomePosition(const osg::Camera*    camera,
                                              bool                  useBoundingBox)
{
    const AbstractDevice*   device  = camera ? dynamic_cast<const AbstractDevice*>(camera->getView()) : nullptr;
    ClientBase::Ptr const&  client  = device ? device->client().lock() : nullptr;
    osg::BoundingSphere     boundingSphere;

    if (client)
        //---------------------------------------------------
        getSphereOfInterest(boundingSphere, *client, nullptr);
        //---------------------------------------------------

    if (!boundingSphere.valid())
        // unit sphere by default
        boundingSphere.set(osg::Vec3f(0.0f, 0.0f, 0.0f), 0.5f);

    // set dist to default
    double dist = 3.5f * boundingSphere.radius();

    if (camera)
    {

        // try to compute dist from frustum
        double left,right,bottom,top,zNear,zFar;
        if (camera->getProjectionMatrixAsFrustum(left,right,bottom,top,zNear,zFar))
        {
            double vertical2 = fabs(right - left) / zNear / 2.;
            double horizontal2 = fabs(top - bottom) / zNear / 2.;
            double dim = horizontal2 < vertical2 ? horizontal2 : vertical2;
            double viewAngle = atan2(dim,1.);
            dist = boundingSphere.radius() / sin(viewAngle);
        }
        else
        {
            // try to compute dist from ortho
            if (camera->getProjectionMatrixAsOrtho(left,right,bottom,top,zNear,zFar))
            {
                dist = fabs(zFar - zNear) / 2.;
            }
        }
    }

    // set home position
    const osg::Vec3d eye    = boundingSphere.center() - FORWARD_VECTOR * dist;
    const osg::Vec3d center = boundingSphere.center();
    const osg::Vec3d up     = UP_VECTOR;

    // update CameraManipulator's data
    setHomePosition(eye, center, up, getAutoComputeHomePosition());
    // update OrbitManipulator's data
    setTransformation(eye, center, up);
}

// virtual
osg::Matrixd
view::OrbitCameraHandler::getMatrix() const
{
    throw sys::Exception(
        "Direct query to manipulator's matrix forbidden. Use scene data instead.",
        "Orbit Camera Handler");
}

// virtual
osg::Matrixd
view::OrbitCameraHandler::getInverseMatrix() const
{
    throw sys::Exception(
        "Direct query to manipulator's inverse matrix forbidden. Use scene data instead.",
        "Orbit Camera Handler");
}

//static
void
view::OrbitCameraHandler::getSphereOfInterest(osg::BoundingSphere&  boundingSphere,
                                              const ClientBase&     client,
                                              const xchg::IdSet*    selection)
{
    boundingSphere.init();

    xchg::IdSet ids;
    if (selection)
        ids = *selection;
    else
    {
        // if no specified selection, use all initialized drawables
        CompoundClientNodeFilter::Ptr filter = CompoundClientNodeFilter::create();

        filter->add(FilterClientNodeByClass::create(xchg::DRAWABLE));
        filter->add(FilterClientNodeByBuiltIn<char>::create(parm::initialization(), xchg::ACTIVE));
        client.getIds(ids, false, filter);
    }

    if (!ids.empty())
    {
        // get the world bounds of selected nodes
        xchg::BoundingBox worldBounds;

        client.getWorldBounds(ids, worldBounds);

        // convert bounding box to bounding sphere
        const math::Vector4f& boundsMin    = worldBounds.min();
        const math::Vector4f& boundsMax    = worldBounds.max();
        const math::Vector4f  boundsCenter = (boundsMin + boundsMax) * 0.5f;
        const float           radius       = (boundsMax - boundsMin).norm() * 0.5f;

        boundingSphere.set(
            osg::Vec3f(boundsCenter.x(), boundsCenter.y(), boundsCenter.z()),
            radius);

#if defined(FLIRT_LOG_ENABLED)
        {
            std::stringstream sstr;

            sstr << "camera focus on { ";
            for (xchg::IdSet::const_iterator it = ids.begin(); it != ids.end(); ++it)
                sstr << "'" << client.getNodeName(*it) << "' ";
            sstr << "}\n"
                << "=> sphere of interest:\n"
                << "\t- center = (" << boundsCenter.x() << ", " << boundsCenter.y() << ", " << boundsCenter.z() << ")\n"
                << "\t- rad = " << radius;
            LOG(DEBUG) << sstr.str();
        }
#endif // defined(FLIRT_LOG_ENABLED)
    }

    if (!boundingSphere.valid())
        // unit sphere by default
        boundingSphere.set(osg::Vec3f(0.0f, 0.0f, 0.0f), 0.5f);
}
