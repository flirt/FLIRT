// ========================================================================= //
// Copyright 2016-2018 SUPAMONKS_STUDIO                                      //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
//                                                                           //
// ========================================================================= //
#include <smks/parm/BuiltIn.hpp>
#include <smks/parm/BuiltIns.hpp>
#include <smks/ClientBase.hpp>
#include <smks/util/osg/key.hpp>

#include <smks/view/AbstractDevice.hpp>
#include <smks/xchg/ManipulatorFlags.hpp>

#include "smks/view/ManipulatorControl.hpp"

namespace smks { namespace view
{
    class ManipulatorControl::PImpl
    {
    private:
        const util::osg::KeyControl _keyNone;
        const util::osg::KeyControl _keyTranslate;
        const util::osg::KeyControl _keyTranslateInWorld;
        const util::osg::KeyControl _keyRotate;
        const util::osg::KeyControl _keyRotateInWorld;
        const util::osg::KeyControl _keyScale;
        const util::osg::KeyControl _keyScaleInWorld;
    public:
        PImpl(
            const util::osg::KeyControl& keyNone,
            const util::osg::KeyControl& keyTranslate,
            const util::osg::KeyControl& keyTranslateInWorld,
            const util::osg::KeyControl& keyRotate,
            const util::osg::KeyControl& keyRotateInWorld,
            const util::osg::KeyControl& keyScale,
            const util::osg::KeyControl& keyScaleInWorld);

        inline
        bool
        handle(const osgGA::GUIEventAdapter&,
               osgGA::GUIActionAdapter&);

        inline
        void
        getUsage(osg::ApplicationUsage&) const;
    };
}
}

using namespace smks;

//////////////
// class PImpl
//////////////
view::ManipulatorControl::PImpl::PImpl(const util::osg::KeyControl& keyNone,
                                       const util::osg::KeyControl& keyTranslate,
                                       const util::osg::KeyControl& keyTranslateInWorld,
                                       const util::osg::KeyControl& keyRotate,
                                       const util::osg::KeyControl& keyRotateInWorld,
                                       const util::osg::KeyControl& keyScale,
                                       const util::osg::KeyControl& keyScaleInWorld):
    _keyNone            (keyNone),
    _keyTranslate       (keyTranslate),
    _keyTranslateInWorld(keyTranslateInWorld),
    _keyRotate          (keyRotate),
    _keyRotateInWorld   (keyRotateInWorld),
    _keyScale           (keyScale),
    _keyScaleInWorld    (keyScaleInWorld)
{ }

bool
view::ManipulatorControl::PImpl::handle(const osgGA::GUIEventAdapter&   ea,
                                        osgGA::GUIActionAdapter&        aa)
{
    if (ea.getHandled())
        return false;

    if (ea.getEventType() != osgGA::GUIEventAdapter::KEYUP)
        return false;

    AbstractDevice*                 device  = dynamic_cast<AbstractDevice*>(&aa);
    ClientBase::Ptr const&  client  = device ? device->client().lock() : nullptr;

    if (!client)
        return false;

    if (_keyNone.compatibleWith(ea))
    {
        client->setNodeParameter<char>(
            parm::manipulatorFlags(),
            xchg::NO_MANIP,
            client->sceneId());
        return true;
    }
    else if (_keyTranslate.compatibleWith(ea))
    {
        client->setNodeParameter<char>(
            parm::manipulatorFlags(),
            xchg::MANIP_TRANSLATE | xchg::MANIP_OBJECT_MODE,
            client->sceneId());
        return true;
    }
    else if (_keyTranslateInWorld.compatibleWith(ea))
    {
        client->setNodeParameter<char>(
            parm::manipulatorFlags(),
            xchg::MANIP_TRANSLATE,
            client->sceneId());
        return true;
    }
    else if (_keyRotate.compatibleWith(ea))
    {
        client->setNodeParameter<char>(
            parm::manipulatorFlags(),
            xchg::MANIP_ROTATE | xchg::MANIP_OBJECT_MODE,
            client->sceneId());
        return true;
    }
    else if (_keyRotateInWorld.compatibleWith(ea))
    {
        client->setNodeParameter<char>(
            parm::manipulatorFlags(),
            xchg::MANIP_ROTATE,
            client->sceneId());
        return true;
    }
    else if (_keyScale.compatibleWith(ea))
    {
        client->setNodeParameter<char>(
            parm::manipulatorFlags(),
            xchg::MANIP_SCALE | xchg::MANIP_OBJECT_MODE,
            client->sceneId());
        return true;
    }
    else if (_keyScaleInWorld.compatibleWith(ea))
    {
        client->setNodeParameter<char>(
            parm::manipulatorFlags(),
            xchg::MANIP_SCALE,
            client->sceneId());
        return true;
    }
    return false;
}

void
view::ManipulatorControl::PImpl::getUsage(osg::ApplicationUsage& usage) const
{
    util::osg::addKeyboardBinding(usage, util::osg::BINDINGS_MANIPULATION, 0, "Deactivate",                          _keyNone);
    util::osg::addKeyboardBinding(usage, util::osg::BINDINGS_MANIPULATION, 1, "Translate selection (Alt+ in world)", _keyTranslate);
    util::osg::addKeyboardBinding(usage, util::osg::BINDINGS_MANIPULATION, 2, "Rotate selection (Alt+ in world)",    _keyRotate);
    util::osg::addKeyboardBinding(usage, util::osg::BINDINGS_MANIPULATION, 3, "Scale selection (Alt+ in world)",     _keyScale);
}

//////////////////////////////
// class ManipulatorControl
//////////////////////////////
view::ManipulatorControl::ManipulatorControl(const util::osg::KeyControl& keyNone,
                                             const util::osg::KeyControl& keyTranslate,
                                             const util::osg::KeyControl& keyTranslateInWorld,
                                             const util::osg::KeyControl& keyRotate,
                                             const util::osg::KeyControl& keyRotateInWorld,
                                             const util::osg::KeyControl& keyScale,
                                             const util::osg::KeyControl& keyScaleInWorld):
    osgGA::GUIEventHandler(),
    _pImpl(new PImpl(keyNone, keyTranslate, keyTranslateInWorld, keyRotate, keyRotateInWorld, keyScale, keyScaleInWorld))
{ }

view::ManipulatorControl::~ManipulatorControl()
{
    delete _pImpl;
}

// virtual
bool
view::ManipulatorControl::handle(const osgGA::GUIEventAdapter&  ea,
                                 osgGA::GUIActionAdapter&       aa)
{
    return _pImpl->handle(ea, aa);
}

// virtual
void
view::ManipulatorControl::getUsage(osg::ApplicationUsage& usage) const
{
    _pImpl->getUsage(usage);
}
