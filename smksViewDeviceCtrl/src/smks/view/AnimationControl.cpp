// ========================================================================= //
// Copyright 2016-2018 SUPAMONKS_STUDIO                                      //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
//                                                                           //
// ========================================================================= //
#include <cassert>

#include <smks/xchg/ParmEvent.hpp>
#include <smks/parm/BuiltIn.hpp>
#include <smks/parm/BuiltIns.hpp>

#include <smks/util/osg/key.hpp>
#include <smks/util/osg/callback.hpp>

#include <smks/view/AbstractDevice.hpp>

#include "smks/view/Animation.hpp"
#include "smks/view/AnimationControl.hpp"

namespace smks { namespace view
{
    class AnimationControl::PImpl
    {
    private:
        const util::osg::KeyControl     _keyPlay;
        const util::osg::KeyControl     _keyPrevFrame;
        const util::osg::KeyControl     _keyNextFrame;
        const util::osg::KeyControl     _keyFirstFrame;
        const util::osg::KeyControl     _keyLastFrame;
        const util::osg::KeyControl     _keyReverse;
        const util::osg::KeyControl     _keyChangeMode;

        osg::observer_ptr<Animation>    _anim;

    public:
        PImpl(const util::osg::KeyControl&  keyPlay,
              const util::osg::KeyControl&  keyPrevFrame,
              const util::osg::KeyControl&  keyNextFrame,
              const util::osg::KeyControl&  keyFirstFrame,
              const util::osg::KeyControl&  keyLastFrame,
              const util::osg::KeyControl&  keyReverse,
              const util::osg::KeyControl&  keyChangeMode);

        inline
        bool
        handle(const osgGA::GUIEventAdapter&,
               osgGA::GUIActionAdapter&);

        inline
        void
        getUsage(osg::ApplicationUsage&) const;
    };
}
}

using namespace smks;

///////////////////////////////////
// class AnimationControl::PImpl
///////////////////////////////////
view::AnimationControl::PImpl::PImpl(const util::osg::KeyControl& keyPlay,
                                     const util::osg::KeyControl& keyPrevFrame,
                                     const util::osg::KeyControl& keyNextFrame,
                                     const util::osg::KeyControl& keyFirstFrame,
                                     const util::osg::KeyControl& keyLastFrame,
                                     const util::osg::KeyControl& keyReverse,
                                     const util::osg::KeyControl& keyChangeMode):
    _keyPlay        (keyPlay),
    _keyPrevFrame   (keyPrevFrame),
    _keyNextFrame   (keyNextFrame),
    _keyFirstFrame  (keyFirstFrame),
    _keyLastFrame   (keyLastFrame),
    _keyReverse     (keyReverse),
    _keyChangeMode  (keyChangeMode),
    _anim           (nullptr)
{ }

bool
view::AnimationControl::PImpl::handle(const osgGA::GUIEventAdapter& ea,
                                      osgGA::GUIActionAdapter&      aa)
{
    if (ea.getHandled())
        return false;

    if (ea.getEventType() != osgGA::GUIEventAdapter::KEYUP)
        return false;

    Animation::Ptr anim;
    if (!_anim.lock(anim))
    {
        // query and cache animation callback
        AbstractDevice* device = dynamic_cast<AbstractDevice*>(&aa);
        _anim = util::osg::getUpdateCallback<Animation>(device->clientRoot());
        return false;
    }

    if (_keyPlay.compatibleWith(ea))
    {
        if (anim->playing())
            anim->pause();
        else
            anim->play();
        return true;
    }
    else if (_keyNextFrame.compatibleWith(ea))
    {
        if (anim->playing())
            anim->pause();
        else
            anim->nextFrame();
        return true;
    }
    else if (_keyPrevFrame.compatibleWith(ea))
    {
        if (anim->playing())
            anim->pause();
        else
            anim->previousFrame();
        return true;
    }
    else if (_keyFirstFrame.compatibleWith(ea))
    {
        anim->firstFrame();
        return true;
    }
    else if (_keyLastFrame.compatibleWith(ea))
    {
        anim->lastFrame();
        return true;
    }
    else if (_keyReverse.compatibleWith(ea))
    {
        if (anim->direction() == PLAY_FORWARD)
            anim->direction(PLAY_BACKWARD);
        else if (anim->direction() == PLAY_BACKWARD)
            anim->direction(PLAY_FORWARD);
        return true;
    }
    else if (_keyChangeMode.compatibleWith(ea))
    {
        if (anim->mode() == ANIM_FREE_FPS)
            anim->mode(ANIM_CONST_FPS);
        else if (anim->mode() == ANIM_CONST_FPS)
            anim->mode(ANIM_FREE_FPS);
        return true;
    }
    return false;
}

void
view::AnimationControl::PImpl::getUsage(osg::ApplicationUsage& usage) const
{
    util::osg::addKeyboardBinding(usage, util::osg::BINDINGS_ANIMATION, 0, "Play/pause", _keyPlay);
    util::osg::addKeyboardBinding(usage, util::osg::BINDINGS_ANIMATION, 1, "Previous/next frame", _keyPrevFrame, _keyNextFrame);
    util::osg::addKeyboardBinding(usage, util::osg::BINDINGS_ANIMATION, 2, "Reverse animation", _keyReverse);
    util::osg::addKeyboardBinding(usage, util::osg::BINDINGS_ANIMATION, 3, "Constrained/free framerate", _keyChangeMode);
}

///////////////////////////////////
// class AnimationControl
///////////////////////////////////
view::AnimationControl::AnimationControl(const util::osg::KeyControl& keyPlay,
                                         const util::osg::KeyControl& keyPrevFrame,
                                         const util::osg::KeyControl& keyNextFrame,
                                         const util::osg::KeyControl& keyFirstFrame,
                                         const util::osg::KeyControl& keyLastFrame,
                                         const util::osg::KeyControl& keyReverse,
                                         const util::osg::KeyControl& keyChangeMode):
    osgGA::GUIEventHandler(),
    _pImpl(new PImpl(keyPlay, keyPrevFrame, keyNextFrame, keyFirstFrame, keyLastFrame, keyReverse, keyChangeMode))
{ }

view::AnimationControl::~AnimationControl()
{
    delete _pImpl;
}

// virtual
bool
view::AnimationControl::handle(const osgGA::GUIEventAdapter&    ea,
                               osgGA::GUIActionAdapter&         aa)
{
    return _pImpl->handle(ea, aa);
}

// virtual
void
view::AnimationControl::getUsage(osg::ApplicationUsage& usage) const
{
    _pImpl->getUsage(usage);
}
