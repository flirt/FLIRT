// ========================================================================= //
// Copyright 2016-2018 SUPAMONKS_STUDIO                                      //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
//                                                                           //
// ========================================================================= //
#include <ostream>
#include <fstream>
#include <vector>
#include <iomanip>
#include <std/to_string_osg.hpp>
#include <json/json.h>

#include <smks/sys/Log.hpp>
#include <smks/sys/ResourceDatabase.hpp>
#include <smks/sys/ResourcePathFinder.hpp>
#include <smks/sys/TextResource.hpp>
#include <smks/xchg/ClientEvent.hpp>
#include <smks/xchg/SceneEvent.hpp>
#include <smks/parm/BuiltIn.hpp>
#include <smks/parm/BuiltIns.hpp>
#include <smks/parm/BuiltInMap.hpp>
#include <smks/xchg/NodeCode.hpp>
#include <smks/xchg/NodeCodes.hpp>
#include <smks/ClientBase.hpp>
#include <smks/client_node_filters.hpp>
#include <smks/view/AbstractDevice.hpp>
#include <smks/AbstractClientNodeFilter.hpp>
#include <smks/util/osg/key.hpp>
#include <smks/util/string/parse.hpp>
#include <smks/xchg/IdSet.hpp>

#include "smks/view/Renderer.hpp"

namespace smks { namespace view
{
    //--------------------------------------------------
    // keys used in json file
    static const char* keyRenderAction  = "action";
    static const char* keyRenderer      = "renderer";
    static const char* keyIntegrator    = "integrator";
    static const char* keySampler       = "sampler";
    static const char* keyPixelFilter   = "pixelFilter";
    static const char* keyFramebuffer   = "framebuffer";
    static const char* keyTonemapper    = "tonemapper";
    static const char* keyDenoiser      = "denoiser";
    static const char* keyPasses        = "passes";
    //--------------------------------------------------

    class Renderer::PImpl
    {
    private:
        const util::osg::KeyControl _keyRender;
        const util::osg::KeyControl _keyRenderAnim;
        const util::osg::KeyControl _keyUndo;

        ClientBase::WPtr    _client;
        //--- locally defined nodes as dictated by configuration file ---
        std::string         _rndrConfigFile;
        unsigned int        _rndrRoot;
        unsigned int        _action;
        unsigned int        _renderer;
        unsigned int        _integrator;
        unsigned int        _sampler;
        unsigned int        _pixelFilter;
        unsigned int        _framebuffer;
        unsigned int        _denoiser;
        unsigned int        _tonemapper;
        xchg::IdSet         _passes;

    public:
        PImpl(const util::osg::KeyControl& keyRender,
              const util::osg::KeyControl& keyRenderAnim,
              const util::osg::KeyControl& keyUndo):
            _keyRender      (keyRender),
            _keyRenderAnim  (keyRenderAnim),
            _keyUndo        (keyUndo),
            _client         (),
            _rndrConfigFile (),
            _rndrRoot       (0),
            _action         (0),
            _renderer       (0),
            _integrator     (0),
            _sampler        (0),
            _pixelFilter    (0),
            _framebuffer    (0),
            _denoiser       (0),
            _tonemapper     (0),
            _passes         ()
        { }

        inline
        ~PImpl()
        {
            //undoIt();
        }

        inline
        void
        getUsage(osg::ApplicationUsage& usage) const
        {
            util::osg::addKeyboardBinding(usage, util::osg::BINDINGS_RENDERING, 0, "Render frame", _keyRender);
        }

        inline
        bool
        handle(const osgGA::GUIEventAdapter&    ea,
               osgGA::GUIActionAdapter&         aa)
        {
            if (ea.getHandled())
                return false;

            if (ea.getEventType() != osgGA::GUIEventAdapter::KEYUP)
                return false;

            AbstractDevice* device = dynamic_cast<AbstractDevice*>(&aa);
            if (!device)
                return false;

            if (_keyRender.compatibleWith(ea))
            {
                doIt(*device, false);
                return true;
            }
            else if (_keyRenderAnim.compatibleWith(ea))
            {
                doIt(*device, true);
                return true;
            }
            else if (_keyUndo.compatibleWith(ea))
            {
                undoIt();
                return true;
            }
            return false;
        }

        inline
        void
        configurationFile(const std::string&);
    private:
        inline
        void
        doIt(AbstractDevice&, bool animate);

        inline
        unsigned int //<! returns render action
        getOrCreateRtNodes(AbstractDevice&);
        inline
        unsigned int //<! returns render action
        getRenderActionFromScene(AbstractDevice&) const;
        inline
        unsigned int //<! returns render action
        createRtNodesFromConfig(AbstractDevice&);
        inline
        void
        saveConfiguration(const AbstractDevice&, const std::string& configPath) const;

        inline
        void
        undoIt();

    private:
        static inline
        std::string
        loadStr(const Json::Value&, const char*);

        static inline
        std::string
        loadCode(const Json::Value& jValue)
        {
            return loadStr(jValue, parm::code().c_str());
        }

        static inline
        void
        loadAllParameters(const ClientBase&, const Json::Value&, unsigned int);

        static inline
        void
        saveAllParameters(const ClientBase&, unsigned int, Json::Value&);
    };
}
}

using namespace smks;

///////////////////////////////
// class Renderer::PImpl
///////////////////////////////
void
view::Renderer::PImpl::configurationFile(const std::string& value)
{
    if (value == _rndrConfigFile)
        return;
    undoIt();
    _rndrConfigFile = value;
}

void
view::Renderer::PImpl::doIt(AbstractDevice& device,
                            bool    animate)
{
    ClientBase::Ptr const& client = device.client().lock();
    if (!client)
        return;
    _client = client;

    FLIRT_LOG(LOG(DEBUG) << "\n\n###\ndo render modifs (animate ? " << animate << ")\n###";)

    // create nodes mandatory for rendering following the specified config file's instructions
    // or consider the first encountered render action defined by the user
    const unsigned int action = getOrCreateRtNodes(device);
    if (!action)
    {
        std::cerr << "Failed to create/find valid render action node. Abort rendering." << std::endl;
        return;
    }
    std::cout << "Rendering using render action '" << client->getNodeName(action) << "'" << std::endl;

    // retrieve the scene's timebounds and framerate (useful when animation is required)
    math::Vector2f  timeBounds;
    float           framerate;

    client->getNodeParameter<math::Vector2f>(parm::timeBounds(),    timeBounds, client->sceneId());
    client->getNodeParameter<float>         (parm::framerate(),     framerate,  client->sceneId());

    const bool isAnimated   = timeBounds.x() < timeBounds.y() && framerate > 0.0f;
    const size_t startFrame = isAnimated ? static_cast<size_t>(floorf(framerate * timeBounds.x())) : 0;
    const size_t endFrame   = isAnimated ? static_cast<size_t>(floorf(framerate * timeBounds.y())) : 0;
    const size_t numFrames  = endFrame - startFrame + 1;

    if (animate && numFrames > 1)
    {
        std::string filePath, baseName, ext;
        int         frameRange[3] = { startFrame, endFrame, 1 };
        std::string userRange;

        client->getNodeParameter<std::string>(parm::filePath(), filePath, action);
        const size_t lastDot = filePath.find_last_of('.');
        if (lastDot == std::string::npos)
        {
            baseName = "output";
            ext = "exr";
        }
        else
        {
            baseName = filePath.substr(0, lastDot);
            ext = filePath.substr(lastDot+1);
        }

        std::cout
            << "---\nFound animation consisting of " << numFrames << " frames "
            << "(" << startFrame << " to " << endFrame << ")."
            << " Enter start end step "
            << "(i.e. \"" << frameRange[0] << " " << frameRange[1] << " " << frameRange[2] << "\"): "
            << std::endl;
        getline(std::cin, userRange);
        if (!userRange.empty())
        {
            const size_t n = util::string::parseIntegers(userRange.c_str(), frameRange, sizeof(frameRange)/sizeof(int));
            if (n <= 0) frameRange[0] = startFrame;
            if (n <= 1) frameRange[1] = endFrame;
            if (n <= 2) frameRange[2] = 1;
            frameRange[0] = std::max(static_cast<int>(startFrame), frameRange[0]);
            frameRange[1] = std::min(static_cast<int>(endFrame), frameRange[1]);
            frameRange[2] = std::max(1, frameRange[2]);
        }
        std::cout << "-- Frame range: " << frameRange[0] << " -> " << frameRange[1] << " (+" << frameRange[2] << ")" << std::endl;

        for (int f=frameRange[0]; f<=frameRange[1]; f+=frameRange[2])
        {
            std::stringstream sstr;
            const float currentTime = static_cast<float>(f)/framerate;

            sstr << baseName << "." << std::setw(4) << std::setfill('0') << f << "." << ext;
            client->setNodeParameter<std::string>(parm::filePath(), sstr.str(), action);
            client->setNodeParameter<float>(parm::currentTime(), currentTime, client->sceneId());
            std::cout << "\n-- Frame " << f << "\ttime = " << currentTime << "\t-> " << sstr.str() << std::endl;
            //######################
            client->setNodeParameter<char>(parm::rendering(), xchg::ACTIVE, action);
            //######################
        }
        // reset file path
        client->setNodeParameter<std::string>(parm::filePath(), filePath, action);
    }
    else
        //######################
        client->setNodeParameter<char>(parm::rendering(), xchg::ACTIVE, action);
        //######################

    //saveConfiguration(*client, configPath);
}

unsigned int //<! returns render action
view::Renderer::PImpl::getOrCreateRtNodes(AbstractDevice& device)
{
    unsigned int action = createRtNodesFromConfig(device);
    if (action == 0)
        action = getRenderActionFromScene(device);
    return action;
}

unsigned int //<! returns render action
view::Renderer::PImpl::getRenderActionFromScene(AbstractDevice& device) const
{
    xchg::IdSet             actions;
    ClientBase::Ptr const&  client = device.client().lock();

    if (client)
    client->getIds(actions, false, FilterClientNodeByClass::create(xchg::RENDER_ACTION));

    return !actions.empty()
        ? *actions.begin()
        : 0;
}

unsigned int //<! returns render action
view::Renderer::PImpl::createRtNodesFromConfig(AbstractDevice& device)
{
    if (_rndrConfigFile.empty())
    {
        std::cerr
            << "No render configuration file specified. "
            << "Cannot create additional render nodes."
            << std::endl;
        return 0;
    }

    if (_rndrRoot)
        return _action; // local RT nodes already defined, nothing needs to be done.

    ClientBase::Ptr const&              client      = device.client().lock();
    sys::ResourceDatabase::Ptr const&   resources   = client ? client->getOrCreateResourceDatabase() : nullptr;
    if (!resources)
        throw sys::Exception(
            "Failed to get resource database from client.",
            "View Renderer");

    Json::Value                 cfgRoot;
    const sys::TextResource*    cfgText = resources->load<sys::TextResource>(_rndrConfigFile.c_str());
    if (cfgText)
    {
        Json::Reader cfgReader;

        std::cout
            << "Found rendering configuration file at "
            << "'" << cfgText->fileName() << "'."
            << std::endl;
        if (!cfgReader.parse(cfgText->text(), cfgRoot))
            std::cerr
                << "Failed to parse rendering configuration from "
                << "'" << cfgText->fileName() << "':\n" << cfgReader.getFormatedErrorMessages()
                << std::endl;
    }
    else
    {
        std::stringstream sstr;
        sstr << "Failed to find rendering configuration file at '" << _rndrConfigFile << "'.";
        throw sys::Exception(sstr.str().c_str(), "View Renderer");
    }

    _rndrRoot = client->createDataNode("__rndr_root__", client->sceneId());
    {
        const Json::Value&  json            = cfgRoot[keyRenderAction];
        _action = client->createRenderAction("__rndr_action__", _rndrRoot);
        loadAllParameters(*client, json, _action);
    }
    {
        const Json::Value&  json = cfgRoot[keyRenderer];
        _renderer = client->createRenderer(loadCode(json).c_str(), "__rndr_renderer__", _rndrRoot);
        loadAllParameters(*client, json, _renderer);
    }
    {
        const Json::Value&  json = cfgRoot[keyIntegrator];
        _integrator = client->createIntegrator(loadCode(json).c_str(), "__rndr_integrator__", _rndrRoot);
        loadAllParameters(*client, json, _integrator);
    }
    {
        const Json::Value&  json = cfgRoot[keySampler];
        _sampler = client->createSamplerFactory(loadCode(json).c_str(), "__rndr_sampler__", _rndrRoot);
        loadAllParameters(*client, json, _sampler);
    }
    if (!cfgRoot[keyTonemapper].isNull())
    {
        const Json::Value&  json = cfgRoot[keyTonemapper];
        _tonemapper = client->createToneMapper(loadCode(json).c_str(), "__rndr_tonemapper__", _rndrRoot);
        loadAllParameters(*client, json, _tonemapper);
    }
    if (!cfgRoot[keyDenoiser].isNull())
    {
        const Json::Value&  json = cfgRoot[keyDenoiser];
        _denoiser = client->createDenoiser(loadCode(json).c_str(), "__rndr_denoiser__", _rndrRoot);
        loadAllParameters(*client, json, _denoiser);
    }
    {
        const Json::Value&  json = cfgRoot[keyPixelFilter];
        _pixelFilter = client->createPixelFilter(loadCode(json).c_str(), "__rndr_filter__", _rndrRoot);
        loadAllParameters(*client, json, _pixelFilter);
    }
    {
        const Json::Value&  json = cfgRoot[keyFramebuffer];
        _framebuffer = client->createFrameBuffer(loadCode(json).c_str(), "__rndr_framebuffer__", _rndrRoot);
        loadAllParameters(*client, json, _framebuffer);
    }
    {
        for (size_t i = 0; i < cfgRoot[keyPasses].size(); ++i)
        {
            const Json::Value&  json    = cfgRoot[keyPasses][i];
            const std::string&  code    = loadCode(json);
            unsigned int        pass    = 0;

            if (code == xchg::code::lightPath().c_str())
                pass = client->createLightPathExpressionPass(
                    loadStr(json, parm::regex().c_str()).c_str(),
                    ("__rndr_lpepass_" + std::to_string(i) + "__").c_str(),
                    _rndrRoot);

            else if (code == xchg::code::idCoverage().c_str())
                pass = client->createShapeIdCoveragePass(
                    ("__rndr_idpass_" + std::to_string(i) + "__").c_str(),
                    _rndrRoot);

            else
                pass = client->createFlatRenderPass(
                    code.c_str(),
                    ("__rndr_flatpass_" + std::to_string(i) + "__").c_str(),
                    _rndrRoot);
            loadAllParameters(*client, json, pass);
            _passes.insert(pass);
        }
    }

    //-----------------------
    // link RT nodes together
    if (_denoiser)
        client->setNodeParameterId(parm::denoiserId().c_str(),  _denoiser,      _framebuffer);
    if (_tonemapper)
        client->setNodeParameterId(parm::toneMapperId().c_str(), _tonemapper,   _framebuffer);
    client->setNodeParameter<xchg::IdSet>(parm::renderPassIds(), _passes,       _framebuffer);

    client->setNodeParameterId(parm::integratorId().c_str(),    _integrator,    _renderer);
    client->setNodeParameterId(parm::samplerFactoryId().c_str(), _sampler,      _renderer);
    client->setNodeParameterId(parm::pixelFilterId().c_str(),   _pixelFilter,   _renderer);

    // set-up render action
    client->setNodeParameterId(parm::rendererId().c_str(),      _renderer,      _action);
    client->setNodeParameterId(parm::frameBufferId().c_str(),   _framebuffer,   _action);
    //-----------------------
    // set device's current camera as the rendering camera
    const unsigned int camera = device.displayedCamera();
    if (!camera)
        throw sys::Exception(
            "Failed to get the device's active camera.",
            "View Renderer");
    client->setNodeParameterId(parm::cameraId().c_str(), camera, _action);

    return _action;
}

void
view::Renderer::PImpl::saveConfiguration(const AbstractDevice&      device,
                                         const std::string& configPath) const
{
    ClientBase::Ptr const& client = device.client().lock();
    if (!client)
        return;

    std::fstream fs;

    fs.open(configPath.c_str(), std::fstream::out);
    if (!fs.is_open())
    {
        std::cerr
            << "Failed to open rendering configuration file at '" << configPath << "'."
            << std::endl;
        return;
    }

    Json::Value cfgRoot;

    saveAllParameters(*client, _action,     cfgRoot[keyRenderAction]);
    saveAllParameters(*client, _renderer,   cfgRoot[keyRenderer]);
    saveAllParameters(*client, _integrator, cfgRoot[keyIntegrator]);
    saveAllParameters(*client, _sampler,        cfgRoot[keySampler]);
    if (_tonemapper)
        saveAllParameters(*client, _tonemapper, cfgRoot[keyTonemapper]);
    if (_denoiser)
        saveAllParameters(*client, _denoiser,   cfgRoot[keyDenoiser]);
    saveAllParameters(*client, _pixelFilter,    cfgRoot[keyPixelFilter]);
    saveAllParameters(*client, _framebuffer,    cfgRoot[keyFramebuffer]);
    for (xchg::IdSet::const_iterator id = _passes.begin(); id != _passes.end(); ++id)
    {
        Json::Value jValue;
        saveAllParameters(*client, *id, jValue);
        cfgRoot[keyPasses].append(jValue);
    }

    fs << cfgRoot;
    fs.close();

    std::cout
        << "Saved rendering configuration file at '" << configPath << "'."
        << std::endl;
}

void
view::Renderer::PImpl::undoIt()
{
    FLIRT_LOG("\n\n###\nundo render modifs\n###");

    ClientBase::Ptr const& client = _client.lock();
    if (client && _rndrRoot)
        client->destroyDataNode(_rndrRoot);
    _rndrRoot       = 0;
    _action         = 0;
    _renderer       = 0;
    _integrator     = 0;
    _pixelFilter    = 0;
    _framebuffer    = 0;
    _denoiser       = 0;
    _sampler        = 0;
    _tonemapper     = 0;
    _passes.clear();
}

// static inline
std::string
view::Renderer::PImpl::loadStr(const Json::Value&   jValue,
                               const char*          key)
{
    return jValue.isMember(key)
        ? jValue.get(key, "").asString()
        : std::string();
}

// static inline
void
view::Renderer::PImpl::loadAllParameters(const ClientBase&  client,
                                         const Json::Value& jValue,
                                         unsigned int       node)
{
    if (node == 0)
        return;
    const Json::Value::Members& members = jValue.getMemberNames();
    for (size_t i = 0; i < members.size(); ++i)
    {
        const std::string&          member  = members[i];
        parm::BuiltIn::Ptr const&   parm    = parm::builtIns().find(member.c_str());
        if (!parm)
        {
            std::cerr
                << "Ignored '" << member << "' json member for node '" << client.getNodeName(node) << "' for loading:\n\t"
                << "not a proper built-in parameter name." << std::endl;
            continue;
        }
        if (!client.isNodeParameterWritable(parm->id(), node))
        {
            std::cerr
                << "Ignored '" << member << "' json member for node '" << client.getNodeName(node) << "' for loading:\n\t"
                << "not writable." << std::endl;
            continue;
        }
        switch (parm->type())
        {
        case xchg::PARMTYPE_BOOL:
            {
                bool parmValue = jValue.get(member, false).asBool();
                client.setNodeParameter<bool>(*parm, parmValue, node);
                FLIRT_LOG(LOG(DEBUG) << "json -> '" << client.getNodeName(node) << "'." << parm->c_str() << " = " << parmValue;)
            } break;
        case xchg::PARMTYPE_UINT:
            {
                unsigned int parmValue = jValue.get(member, 0).asUInt();
                client.setNodeParameter<unsigned int>(*parm, parmValue, node);
                FLIRT_LOG(LOG(DEBUG) << "json -> '" << client.getNodeName(node) << "'." << parm->c_str() << " = " << parmValue;)
            } break;
        case xchg::PARMTYPE_INT:
            {
                int parmValue = jValue.get(member, 0).asInt();
                client.setNodeParameter<int>(*parm, parmValue, node);
                FLIRT_LOG(LOG(DEBUG) << "json -> '" << client.getNodeName(node) << "'." << parm->c_str() << " = " << parmValue;)
            } break;
        case xchg::PARMTYPE_CHAR:
            {
                char parmValue = static_cast<char>(jValue.get(member, 0).asInt());
                client.setNodeParameter<char>(*parm, parmValue, node);
                FLIRT_LOG(LOG(DEBUG) << "json -> '" << client.getNodeName(node) << "'." << parm->c_str() << " = " << static_cast<int>(parmValue);)
            } break;
        case xchg::PARMTYPE_SIZE:
            {
                size_t parmValue = static_cast<size_t>(jValue.get(member, 0).asUInt());
                client.setNodeParameter<size_t>(*parm, parmValue, node);
                FLIRT_LOG(LOG(DEBUG) << "json -> '" << client.getNodeName(node) << "'." << parm->c_str() << " = " << parmValue;)
            } break;
        case xchg::PARMTYPE_FLOAT:
            {
                float parmValue = static_cast<float>(jValue.get(member, 0.0).asDouble());
                client.setNodeParameter<float>(*parm, parmValue, node);
                FLIRT_LOG(LOG(DEBUG) << "json -> '" << client.getNodeName(node) << "'." << parm->c_str() << " = " << parmValue;)
            } break;
        case xchg::PARMTYPE_STRING:
            {
                std::string parmValue = jValue.get(member, "").asString();
                client.setNodeParameter<std::string>(*parm, parmValue, node);
                FLIRT_LOG(LOG(DEBUG) << "json -> '" << client.getNodeName(node) << "'." << parm->c_str() << " = " << parmValue;)
            } break;
        default:
            std::cerr
                << "Ignored '" << member << "' json member for node '" << client.getNodeName(node) << "' for loading:\n\t"
                << "unsupported parameter type." << std::endl;
            break;
        }
    }
}

// static inline
void
view::Renderer::PImpl::saveAllParameters(const ClientBase&  client,
                                         unsigned int       node,
                                         Json::Value&       jValue)
{
    if (node == 0)
        return;
    xchg::IdSet parms;

    client.getCurrentNodeParameters(parms, node);
    for (xchg::IdSet::const_iterator id = parms.begin(); id != parms.end(); ++id)
    {
        parm::BuiltIn::Ptr const& parm = parm::builtIns().find(*id);
        if (!parm)
        {
            xchg::ParameterInfo parmInfo;

            if (client.getNodeParameterInfo(*id, parmInfo, node))
                std::cerr
                    << "Ignored '" << parmInfo.name << "' parameter from node '" << client.getNodeName(node) << "' for saving:\n\t"
                    << "not a proper built-in parameter name." << std::endl;
            else
                std::cerr
                    << "Ignored parameter with ID = " << *id << " from node '" << client.getNodeName(node) << "' for saving:\n\t"
                    << "not a proper built-in parameter name." << std::endl;
            continue;
        }
        switch (parm->type())
        {
        case xchg::PARMTYPE_BOOL:
            {
                bool parmValue = false;
                client.getNodeParameter<bool>(*parm, parmValue, node);
                jValue[parm->c_str()] = parmValue;
                FLIRT_LOG(LOG(DEBUG) << "json <- '" << client.getNodeName(node) << "'." << parm->c_str() << " = " << parmValue;)
            } break;
        case xchg::PARMTYPE_UINT:
            {
                unsigned int parmValue = 0;
                client.getNodeParameter<unsigned int>(*parm, parmValue, node);
                jValue[parm->c_str()] = parmValue;
                FLIRT_LOG(LOG(DEBUG) << "json <- '" << client.getNodeName(node) << "'." << parm->c_str() << " = " << parmValue;)
            } break;
        case xchg::PARMTYPE_INT:
            {
                int parmValue = 0;
                client.getNodeParameter<int>(*parm, parmValue, node);
                jValue[parm->c_str()] = parmValue;
                FLIRT_LOG(LOG(DEBUG) << "json <- '" << client.getNodeName(node) << "'." << parm->c_str() << " = " << parmValue;)
            } break;
        case xchg::PARMTYPE_CHAR:
            {
                char parmValue = 0;
                client.getNodeParameter<char>(*parm, parmValue, node);
                jValue[parm->c_str()] = static_cast<int>(parmValue);
                FLIRT_LOG(LOG(DEBUG) << "json <- '" << client.getNodeName(node) << "'." << parm->c_str() << " = " << parmValue;)
            } break;
        case xchg::PARMTYPE_SIZE:
            {
                size_t parmValue = 0;
                client.getNodeParameter<size_t>(*parm, parmValue, node);
                jValue[parm->c_str()] = static_cast<unsigned int>(parmValue);
                FLIRT_LOG(LOG(DEBUG) << "json <- '" << client.getNodeName(node) << "'." << parm->c_str() << " = " << parmValue;)
            } break;
        case xchg::PARMTYPE_FLOAT:
            {
                float parmValue = 0.0f;
                client.getNodeParameter<float>(*parm, parmValue, node);
                jValue[parm->c_str()] = static_cast<double>(parmValue);
                FLIRT_LOG(LOG(DEBUG) << "json <- '" << client.getNodeName(node) << "'." << parm->c_str() << " = " << parmValue;)
            } break;
        case xchg::PARMTYPE_STRING:
            {
                std::string parmValue;
                client.getNodeParameter<std::string>(*parm, parmValue, node);
                jValue[parm->c_str()] = parmValue;
                FLIRT_LOG(LOG(DEBUG) << "json <- '" << client.getNodeName(node) << "'." << parm->c_str() << " = " << parmValue;)
            } break;
        default:
            break;
        }
    }
}

////////////////////////
// class Renderer
////////////////////////
view::Renderer::Renderer(const util::osg::KeyControl& keyRender,
                         const util::osg::KeyControl& keyRenderAnim,
                         const util::osg::KeyControl& keyUndo):
    osgGA::GUIEventHandler(),
    _pImpl(new PImpl(keyRender, keyRenderAnim, keyUndo))
{ }

view::Renderer::~Renderer()
{
    delete _pImpl;
}

// virtual
bool
view::Renderer::handle(const osgGA::GUIEventAdapter&    ea,
                       osgGA::GUIActionAdapter&         aa)
{
    return _pImpl->handle(ea, aa);
}

// virtual
void
view::Renderer::getUsage(osg::ApplicationUsage& usage) const
{
    _pImpl->getUsage(usage);
}

void
view::Renderer::configurationFile(const char* value)
{
    _pImpl->configurationFile(value);
}
