// ========================================================================= //
// Copyright 2016-2018 SUPAMONKS_STUDIO                                      //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
//                                                                           //
// ========================================================================= //
#include <smks/view/AbstractDevice.hpp>

#include "smks/view/LightFocus.hpp"

namespace smks { namespace view
{
    static const size_t     NUM_TWEEN_FRAMES    = 8;
    static const float      MIN_TWEEN           = 0.2f;
    static const osg::Vec3f FORWARD             = osg::Vec3f(0.0f, 0.0f, 1.0f);

    class LightFocus::PImpl
    {
    private:
        const util::osg::KeyControl _key;

        osg::Quat   _currentOrientation;
        osg::Quat   _targetOrientation;
        osg::Vec3f  _targetDirection;
        size_t      _numTweenFrames;

    public:
        explicit
        PImpl(const util::osg::KeyControl&);

        inline
        bool
        handle(const osgGA::GUIEventAdapter&,
               osgGA::GUIActionAdapter&);

        inline
        void
        getUsage(osg::ApplicationUsage&) const;
    };
}
}

using namespace smks;

// explicit
view::LightFocus::PImpl::PImpl(const util::osg::KeyControl& key):
    _key                (key),
    _currentOrientation (),
    _targetOrientation  (),
    _targetDirection    (0.0f, 0.0f, 0.0f),
    _numTweenFrames     (0)
{ }

bool
view::LightFocus::PImpl::handle(const osgGA::GUIEventAdapter&   ea,
                                osgGA::GUIActionAdapter&        aa)
{
    AbstractDevice* device = dynamic_cast<AbstractDevice*>(&aa);
    if (!device)
        return false;

    if (_targetDirection.length() < 1e-5f)
    {
        // initialize light direction for the first time
        _currentOrientation = device->getCamera()->getViewMatrix().getRotate();
        _targetOrientation  = _currentOrientation;
        _targetDirection    = _currentOrientation * FORWARD;
        device->setViewportLightDirection(_targetDirection);
    }

    if (_numTweenFrames > 0)
    {
        const float tweenMult   = 1.0f - static_cast<float>(_numTweenFrames-1)/static_cast<float>(NUM_TWEEN_FRAMES-1);
        const float tween       = MIN_TWEEN + tweenMult * (1.0f - MIN_TWEEN);

        _currentOrientation.slerp(
            tween,
            _currentOrientation,
            _targetOrientation);

        const osg::Vec3f& currentDirection = _currentOrientation * FORWARD;

        device->setViewportLightDirection(currentDirection);

        --_numTweenFrames;
        if ((_targetDirection - currentDirection).length() < 1e-3f)
            _numTweenFrames = 0;
    }

    if (ea.getHandled())
        return false;

    if (ea.getEventType() == osgGA::GUIEventAdapter::KEYUP &&
        _key.compatibleWith(ea) &&
        _numTweenFrames == 0)
    {
        _numTweenFrames     = NUM_TWEEN_FRAMES;
        _targetOrientation  = device->getCamera()->getInverseViewMatrix().getRotate();
        _targetDirection    = _targetOrientation * FORWARD;

        return true;
    }

    return false;
}

void
view::LightFocus::PImpl::getUsage(osg::ApplicationUsage& usage) const
{
    util::osg::addKeyboardBinding(usage, util::osg::BINDINGS_VIEWPORT, 4, "Align light with view", _key);
}

//////////////////////
// class LightFocus
//////////////////////
// explicit
view::LightFocus::LightFocus(const util::osg::KeyControl& key):
    osgGA::GUIEventHandler(),
    _pImpl(new PImpl(key))
{ }

view::LightFocus::~LightFocus()
{
    delete _pImpl;
}

// virtual
bool
view::LightFocus::handle(const osgGA::GUIEventAdapter&  ea,
                         osgGA::GUIActionAdapter&       aa)
{
    return _pImpl->handle(ea, aa);
}

// virtual
void
view::LightFocus::getUsage(osg::ApplicationUsage& usage) const
{
    _pImpl->getUsage(usage);
}
