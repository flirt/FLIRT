// ========================================================================= //
// Copyright 2016-2018 SUPAMONKS_STUDIO                                      //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
//                                                                           //
// ========================================================================= //
#include <smks/parm/BuiltIns.hpp>
#include <smks/ClientBase.hpp>
#include <smks/client_node_filters.hpp>
#include <smks/view/AbstractDevice.hpp>
#include <smks/util/io/input.hpp>
#include <smks/sys/Log.hpp>

#include "smks/view/SceneObjLoader.hpp"

namespace smks { namespace view
{
    class SceneObjLoader::PImpl
    {
    private:
        const util::osg::KeyControl _keyDo;
        const util::osg::KeyControl _keyUndo;
    private:
        enum { ALL_NODES = -1, CANCEL = -2 };
    public:
        PImpl(const util::osg::KeyControl&,
              const util::osg::KeyControl&);

        inline
        bool
        handle(const osgGA::GUIEventAdapter&,
               osgGA::GUIActionAdapter&);

    private:
        inline
        void
        initialize(ClientBase&, bool doUninitialize);

    public:
        inline
        void
        getUsage(osg::ApplicationUsage&) const;
    };
}
}

using namespace smks;

//////////////
// class PImpl
//////////////
view::SceneObjLoader::PImpl::PImpl(const util::osg::KeyControl& keyDo,
                                   const util::osg::KeyControl& keyUndo):
    _keyDo  (keyDo),
    _keyUndo(keyUndo)
{ }

bool
view::SceneObjLoader::PImpl::handle(const osgGA::GUIEventAdapter&   ea,
                                    osgGA::GUIActionAdapter&        aa)
{
    if (ea.getHandled())
        return false;

    if (ea.getEventType() != osgGA::GUIEventAdapter::KEYUP)
        return false;

    AbstractDevice*                 device = dynamic_cast<view::AbstractDevice*>(&aa);
    ClientBase::Ptr const&  client = device ? device->client().lock() : nullptr;
    if (!client)
        return false;

    if (_keyDo.compatibleWith(ea))
    {
        initialize(*client, false);
        return true;
    }
    else if (_keyUndo.compatibleWith(ea))
    {
        initialize(*client, true);
        return true;
    }
    return false;
}

void
view::SceneObjLoader::PImpl::initialize(ClientBase& client,
                                        bool        doUninitialize)
{
    ////////////////////////////////////////
    // struct FilterExplicitInitializedNodes
    ////////////////////////////////////////
    struct FilterActiveNodes:
        public AbstractClientNodeFilter
    {
    public:
        typedef std::shared_ptr<FilterActiveNodes> Ptr;
    public:
        static inline
        Ptr
        create()
        {
            Ptr ptr(new FilterActiveNodes());
            return ptr;
        }
    public:
        inline
        bool
        operator()(unsigned int node, const ClientBase& client) const
        {
            char init = xchg::INACTIVE;
            bool status = client.getNodeParameter(parm::initialization(), init, node);

            return status && xchg::equal<char>(init, xchg::ACTIVE);
        }
    };
    ////////////////////////////////////////

    static const unsigned int CHUNK = 20;
    xchg::IdSet ids;
    xchg::IdSet toProcess;
    xchg::IdSet selection;
    std::vector<unsigned int> vecIds;

    if (doUninitialize &&
        client.getNodeParameter<xchg::IdSet>(parm::selection(), selection, client.sceneId()) &&
        !selection.empty())
    {
        // automatically uninitialize current selection
        for (xchg::IdSet::const_iterator id = selection.begin(); id != selection.end(); ++id)
            toProcess.insert(*id);
    }
    else
    {
        // filter node IDs according to their initialization status
        AbstractClientNodeFilter::Ptr filter = nullptr;
        if (doUninitialize)
            filter = FilterActiveNodes::create();
        else
            filter = FilterClientNodeByBuiltIn<char>::create(parm::initialization(), xchg::INACTIVE);

        client.getIds(ids, false, filter);
        if (ids.empty())
        {
            std::cout
                << "no " << (doUninitialize ? "initialized" : "uninitialized") << " node found"
                << std::endl;
            return;
        }

        vecIds.reserve(ids.size());
        for (xchg::IdSet::const_iterator id = ids.begin(); id != ids.end(); ++id)
            vecIds.push_back(*id);
        std::cout
            << vecIds.size() << " "
            << (doUninitialize ? "initialized" : "uninitialized")
            << " node(s) found"
            << std::endl;


        if (vecIds.size() == 1)
            toProcess.insert(vecIds.front());
        else
        {
            int n0  = -1;
            if (ids.size() < CHUNK)
                n0 = 0;

            if (n0 >= 0)
                for (size_t dn = 0; dn < CHUNK; ++dn)
                    if (n0 + dn < vecIds.size())
                        std::cout
                            << "\t[" << (n0 + dn) << "]\t"
                            << client.getNodeName(vecIds[n0 + dn])
                            << std::endl;

            std::cout
                << (doUninitialize ? "Uninitialize" : "Initialize") << " nodes? "
                << "Pick index (or " << ALL_NODES << " for all, or " << CANCEL << " to cancel): ";
            std::cout.flush();
            const int idx = util::io::getInteger();

            if (idx == CANCEL)
                return;

            if (idx == ALL_NODES)
                for (size_t i = 0; i < vecIds.size(); ++i)
                    toProcess.insert(vecIds[i]);
            else if (idx >= 0 && idx < vecIds.size())
                toProcess.insert(vecIds[idx]);
        }
    }

    for (xchg::IdSet::const_iterator id = toProcess.begin(); id != toProcess.end(); ++id)
        client.setNodeParameter<char>(parm::initialization(), doUninitialize ? xchg::INACTIVE : xchg::ACTIVE, *id);

    math::Vector2f timeBounds(FLT_MAX, -FLT_MAX);
    if (client.getNodeParameter<math::Vector2f>(parm::timeBounds(), timeBounds, client.sceneId()))
        std::cout
            << "scene's time bounds = "
            << "[" << timeBounds.x() << " " << timeBounds.y() << "]"
            << std::endl;
}

// virtual
void
view::SceneObjLoader::PImpl::getUsage(osg::ApplicationUsage& usage) const
{
    util::osg::addKeyboardBinding(usage, util::osg::BINDINGS_SCENE_EDITING, 2, "Load node content (Alt+ to unload)", _keyDo);
}

///////////////////////
// class SceneObjLoader
///////////////////////
view::SceneObjLoader::SceneObjLoader(const util::osg::KeyControl& keyDo,
                                     const util::osg::KeyControl& keyUndo):
    osgGA::GUIEventHandler(),
    _pImpl(new PImpl(keyDo, keyUndo))
{ }

view::SceneObjLoader::~SceneObjLoader()
{
    delete _pImpl;
}

// virtual
bool
view::SceneObjLoader::handle(const osgGA::GUIEventAdapter&  ea,
                             osgGA::GUIActionAdapter&       aa)
{
    return _pImpl->handle(ea, aa);
}

// virtual
void
view::SceneObjLoader::getUsage(osg::ApplicationUsage& usage) const
{
    _pImpl->getUsage(usage);
}
