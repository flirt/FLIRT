// ========================================================================= //
// Copyright 2016-2018 SUPAMONKS_STUDIO                                      //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
//                                                                           //
// ========================================================================= //
#pragma once

#include <memory>
#include <vector>

#include <osg/ref_ptr>
#include <osg/observer_ptr>

#include <smks/xchg/ActionState.hpp>
#include <smks/xchg/NodeClass.hpp>
#include <smks/AbstractClientObserver.hpp>

namespace smks
{
    class ClientBase;
    struct AbstractClientNodeFilter;

    namespace view
    {
        class NodeSwitch
        {
        public:
            typedef std::shared_ptr<NodeSwitch> Ptr;
        private:
            class ClientObserver;
        private:
            typedef std::shared_ptr<AbstractClientNodeFilter>   FilterPtr;
            typedef std::shared_ptr<ClientObserver>             ClientObserverPtr;
            typedef std::shared_ptr<ClientBase>                 ClientPtr;
        private:
            enum { INVALID_IDX  = -1 };
        protected:
            const xchg::NodeClass       _nodeClass;
            const char                  _nodeInitialization;
            const bool                  _emptySlotAllowed;
        private:
            ClientObserverPtr           _clientObserver;
        protected:
            std::vector<unsigned int>   _nodeIds;
        private:
            unsigned int                _defaultId;
            size_t                      _currentIndex;  // should not be directly set

        public:
            static
            Ptr
            create(ClientPtr const&,
                   xchg::NodeClass,
                   char initialization,
                   bool emptySlotAllowed = false);

            static
            void
            destroy(ClientPtr const&, Ptr&);
        protected:
            NodeSwitch(xchg::NodeClass,
                       char initialization,
                       bool emptySlotAllowed);

            virtual
            void
            build(ClientPtr const&);

            virtual
            void
            dispose(ClientPtr const&);
        private:
            // non-copyable
            NodeSwitch(const NodeSwitch&);
            NodeSwitch& operator=(const NodeSwitch&);
        public:
            virtual
            ~NodeSwitch()
            { }

            void
            next(ClientBase&);

            unsigned int
            currentNodeId() const;

        protected:
            virtual
            void
            addNodeId(ClientBase&, unsigned int);
            virtual
            void
            removeNodeId(ClientBase&, unsigned int);
        private:
            size_t
            getNodeIndex(unsigned int) const;

        public:
            virtual
            void
            handle(ClientBase&, const xchg::ClientEvent&);
            virtual
            void
            handle(ClientBase&, const xchg::SceneEvent&);
            virtual
            void
            handleSceneParmEvent(ClientBase&, const xchg::ParmEvent&);

        private:
            void
            handleSceneSet(ClientBase&, unsigned int);

            void
            handleSceneUnset(ClientBase&, unsigned int);

        protected:
            virtual inline
            void
            handleCurrentNodeAboutToChange(ClientBase&, unsigned int)
            { }

        private:
            void
            currentIndex(ClientBase&, size_t);

            size_t
            currentIndex() const;
        };
    }
}
