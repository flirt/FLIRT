// ========================================================================= //
// Copyright 2016-2018 SUPAMONKS_STUDIO                                      //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
//                                                                           //
// ========================================================================= //
#include <sstream>
#include <iostream>
#include <smks/util/osg/key.hpp>
#include <smks/sys/Log.hpp>

#include "smks/view/Animation.hpp"
#include "AnimationProfiler.hpp"

namespace smks { namespace view
{
    class AnimationProfiler::LoopHandler:
        public Animation::AbstractLoopHandler
    {
    public:
        typedef std::shared_ptr<LoopHandler> Ptr;
    private:
        AnimationProfiler& _timer;
    public:
        static inline
        Ptr
        create(AnimationProfiler& timer)
        {
            Ptr ptr(new LoopHandler(timer));
            return ptr;
        }
    private:
        explicit
        LoopHandler(AnimationProfiler& timer):
            _timer(timer)
        { }

        inline
        void
        handle(Animation& anim, ClientBase& client)
        {
            _timer.handleAnimationLooped(anim, client);
        }
    };
}
}

using namespace smks;

view::AnimationProfiler::AnimationProfiler(Animation*                   anim,
                                           const util::osg::KeyControl& key):
    osgGA::GUIEventHandler(),
    _key        (key),
    _anim       (anim),
    _start      (),
    _loopHandler(LoopHandler::create(*this))
{
    if (anim)
        anim->registerLoopHandler(_loopHandler);
}

view::AnimationProfiler::~AnimationProfiler()
{
    Animation::Ptr anim;
    if (_anim.lock(anim))
        anim->deregisterLoopHandler(_loopHandler);
}

bool
view::AnimationProfiler::handle(const osgGA::GUIEventAdapter&   ea,
                                osgGA::GUIActionAdapter&        aa)
{
    if (ea.getHandled())
        return false;

    switch (ea.getEventType())
    {
    default:
        break;
    case osgGA::GUIEventAdapter::KEYUP:
        return handleKeyUp(ea, aa);
    }
    return false;
}

bool
view::AnimationProfiler::handleKeyUp(const osgGA::GUIEventAdapter&  ea,
                                     osgGA::GUIActionAdapter&       aa)
{
    Animation::Ptr anim;

    if (_anim.lock(anim) &&
        _key.compatibleWith(ea))
    {
        anim->firstFrame();
        _start = clock();
        anim->play();

        std::stringstream sstr;
        sstr << "Profiling animation playback...";
        FLIRT_LOG(LOG(DEBUG) << sstr.str();)
        std::cout << sstr.str() << "\n";

        return true;
    }
    return false;
}

void
view::AnimationProfiler::handleAnimationLooped(Animation&   anim,
                                               ClientBase&  client)
{
    anim.pause();
    const double timeDiff = static_cast<double>(clock()-_start)/static_cast<double>(CLOCKS_PER_SEC);

    std::stringstream sstr;
    sstr << "Animation playback lasted " << timeDiff << " second(s).";
    FLIRT_LOG(LOG(DEBUG) << sstr.str();)
    std::cout << sstr.str() << "\n";
}

// virtual
void
view::AnimationProfiler::getUsage(osg::ApplicationUsage& usage) const
{
    util::osg::addKeyboardBinding(usage, util::osg::BINDINGS_ANIMATION, 1000, "Profile animation", _key);
}
