// ========================================================================= //
// Copyright 2016-2018 SUPAMONKS_STUDIO                                      //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
//                                                                           //
// ========================================================================= //
#include <osg/NodeVisitor>

#include <smks/math/types.hpp>
#include <smks/sys/Exception.hpp>
#include <smks/sys/Log.hpp>
#include <smks/parm/BuiltIns.hpp>
#include <smks/parm/BuiltIn.hpp>
#include <smks/xchg/ClientEvent.hpp>
#include <smks/xchg/ParmEvent.hpp>
#include <smks/ClientBase.hpp>
#include <smks/AbstractClientObserver.hpp>
#include <smks/view/AbstractDevice.hpp>

#include "smks/view/Animation.hpp"

namespace smks { namespace view
{
    /////////////////////////
    // class Animation::PImpl
    /////////////////////////
    class Animation::PImpl
    {
    private:
        class ClientObserver;
    private:
        typedef std::shared_ptr<ClientObserver> ClientObserverPtr;
    private:
        Animation&              _self;
        const ClientBase::WPtr  _client;

        AnimationMode           _mode;
        PlayingDirection        _direction;
        // directly read from the scene's parameters
        math::Vector2f          _sceneTimeBounds;
        float                   _framerate;

        float                   _lastTime;          // in global time
        float                   _lastSceneTime;
        int                     _lastSceneFrame;
        bool                    _playing;
        clock_t                 _refClock;          // clock tick used to constraint framerate
        int                     _refFrame;          // reference frame number used to constraint framerate
        float                   _gotoRatio;
        int                     _frameNumberShift;
        float                   _playbackWindowStart;
        float                   _playbackWindowEnd;

        ClientObserverPtr       _observer;
        LoopHandlers            _loopHandlers;
    public:
        PImpl(Animation&, ClientBase::Ptr const&);

        inline
        void
        build(ClientBase::Ptr const&, Animation::Ptr const&);

        inline
        void
        dispose(ClientBase::Ptr const&);

        inline
        void
        runOperation(osg::Node*, osg::NodeVisitor*);

        inline
        bool
        playing() const
        {
            return _playing;
        }

        inline
        void
        play();

        inline
        void
        pause();

        inline
        void
        start();

        inline
        void
        stop();

        inline
        void
        goTo(float);

        inline
        void
        firstFrame();

        inline
        void
        lastFrame();

        inline
        void
        previousFrame();

        inline
        void
        nextFrame();

        inline
        AnimationMode
        mode() const
        {
            return _mode;
        }

        inline
        void
        mode(AnimationMode value)
        {
            _mode = value;
        }

        inline
        PlayingDirection
        direction() const
        {
            return _direction;
        }

        inline
        void
        direction(PlayingDirection value)
        {
            _direction = value;
        }

        inline
        void
        registerLoopHandler(AbstractLoopHandler::Ptr const&);

        inline
        void
        deregisterLoopHandler(AbstractLoopHandler::Ptr const&);
    private:
        inline
        void
        handleAnimationLooped(ClientBase&);

        inline
        void
        handle(ClientBase&, const xchg::ClientEvent&);

        inline
        void
        handleSceneSet(ClientBase&, unsigned int);

        inline
        void
        handleSceneUnset(ClientBase&, unsigned int);

        inline
        void
        handleSceneParmEvent(ClientBase&, const xchg::ParmEvent&);

        inline
        void
        dirtyLastSceneTimeState();

        inline
        void
        dirtyReferenceClockAndTime();
    };

    /////////////////////////////////////////
    // class Animation::PImpl::ClientObserver
    ////////////////////////////////////////
    class Animation::PImpl::ClientObserver:
        public AbstractClientObserver
    {
    public:
        typedef std::shared_ptr<ClientObserver> Ptr;
    private:
        PImpl& _pImpl;
    public:
        static inline
        Ptr
        create(PImpl& pImpl)
        {
            Ptr ptr(new ClientObserver(pImpl));
            return ptr;
        }
    protected:
        explicit
        ClientObserver(PImpl& pImpl):
            _pImpl(pImpl)
        { }
    public:
        inline
        void
        handle(ClientBase& client, const xchg::ClientEvent& evt)
        {
            _pImpl.handle(client, evt);
        }

        inline
        void
        handle(ClientBase&, const xchg::SceneEvent&)
        { }

        inline
        void
        handleSceneParmEvent(ClientBase& client, const xchg::ParmEvent& evt)
        {
            _pImpl.handleSceneParmEvent(client, evt);
        }
    };
    //////////////////////////////////

    static inline
    float
    modulo(float x, float xmin, float xrange)
    {
        assert(!(xrange < 0.0f));
        float t = fmodf(x - xmin, xrange);
        if (t < 0.0f)
            t += xrange;
        t += xmin;
        return t;
    }

    static inline
    bool
    isRatioOK(float x)
    {
        return !(x < 0.0f) && !(x > 1.0f);
    }

    static
    void
    setSceneCurrentTime(ClientBase& client, float value)
    {
        if (value < 0.0f)
            client.unsetNodeParameter(parm::currentTime().id());
        else
            client.setNodeParameter<float>(parm::currentTime(), value, client.sceneId());
    }
}
}

using namespace smks;

//////////////////////////////
// class Animation::PImpl
//////////////////////////////
// explicit
view::Animation::PImpl::PImpl(Animation&                self,
                              ClientBase::Ptr const&    client):
    _self               (self),
    _client             (client),
    _mode               (ANIM_FREE_FPS),
    _direction          (PLAY_FORWARD),
    _sceneTimeBounds    (FLT_MAX, -FLT_MAX),
    _framerate          (25.0f),
    _lastTime           (-1.0f),
    _lastSceneTime      (-1.0f),
    _lastSceneFrame     (-1),
    _playing            (false),
    _refClock           (-1),
    _refFrame           (-1),
    _gotoRatio          (-1.0f),
    _frameNumberShift   (0),
    _playbackWindowStart(FLT_MAX),
    _playbackWindowEnd  (-FLT_MAX),
    _observer           (nullptr),
    _loopHandlers       ()
{ }

// virtual
void
view::Animation::PImpl::build(ClientBase::Ptr const&    client,
                              Ptr const&                ptr)
{
    _observer = ClientObserver::create(*this);
    if (client)
    {
        client->registerObserver(_observer);
        handleSceneSet(*client, client->sceneId());
    }
    stop();
}

// virtual
void
view::Animation::PImpl::dispose(ClientBase::Ptr const& client)
{
    stop();
    while (!_loopHandlers.empty())
        _loopHandlers.pop_back();

    if (client)
    {
        handleSceneUnset(*client, client->sceneId());
        client->deregisterObserver(_observer);
    }
    _observer = nullptr;
}

void
view::Animation::PImpl::play()
{
    if (_playing)
        return;

    dirtyLastSceneTimeState();
    if (!_playing)
        dirtyReferenceClockAndTime();

    _playing = true;
}

void
view::Animation::PImpl::pause()
{
    if (!_playing)
        return;
    _playing = false;
    dirtyLastSceneTimeState();
}

void
view::Animation::PImpl::start()
{
    stop();
    play();
}

void
view::Animation::PImpl::stop()
{
    firstFrame();
}

void
view::Animation::PImpl::goTo(float ratio)
{
    pause();
    _gotoRatio = std::max(0.0f, std::min(1.0f, ratio));
}

void
view::Animation::PImpl::firstFrame()
{
    goTo(0.0f);
}

void
view::Animation::PImpl::lastFrame()
{
    goTo(1.0f);
}


void
view::Animation::PImpl::previousFrame()
{
    if (_playing)
        return;

    _frameNumberShift = _direction == PLAY_FORWARD ? -1 : 1;
}

void
view::Animation::PImpl::nextFrame()
{
    if (_playing)
        return;

    _frameNumberShift = _direction == PLAY_FORWARD ? 1 : -1;
}

void
view::Animation::PImpl::dirtyLastSceneTimeState()
{
    _lastSceneTime  = -1.0f;
    _lastSceneFrame = -1;
}

void
view::Animation::PImpl::dirtyReferenceClockAndTime()
{
    _refClock   = -1;
    _refFrame   = -1;
}

void
view::Animation::PImpl::runOperation(osg::Node*         n,
                                     osg::NodeVisitor*  nv)
{
    ClientBase::Ptr const& client = _client.lock();

    if (client &&
        nv->getVisitorType() == osg::NodeVisitor::UPDATE_VISITOR &&
        nv->getFrameStamp())
    {
        if (_sceneTimeBounds.x() < _sceneTimeBounds.y())
        {
            const int minFrame = static_cast<int>(floorf(_sceneTimeBounds.x() * _framerate));
            const int maxFrame = static_cast<int>(floorf(_sceneTimeBounds.y() * _framerate));

            assert(minFrame >= 0);
            assert(maxFrame >= 0);
            assert(minFrame <= maxFrame);

            if (_lastSceneFrame < 0)
            {
                float currentTime = 0.0f;
                if (client->getNodeParameter<float>(parm::currentTime(), currentTime, client->sceneId()))
                    _lastSceneFrame = static_cast<int>(floorf(currentTime * _framerate));
                else
                    _lastSceneFrame = minFrame;
            }

            const int   numFrames   = maxFrame - minFrame + 1;
            const float currentTime = static_cast<float>(nv->getFrameStamp()->getSimulationTime());
            assert(numFrames > 0);

            if (_lastTime < 0.0f)
                _lastTime = currentTime;

            float start = _sceneTimeBounds.x();
            float end   = _sceneTimeBounds.y();

            if (!(_playbackWindowStart > _playbackWindowEnd))
            {
                start   = _playbackWindowStart;
                end     = _playbackWindowEnd;
            }

            const float duration = end - start;
            //const unsigned int    numFrames   = std::max(1, static_cast<int>(ceil(_framerate * duration)));


            if (isRatioOK(_gotoRatio))
            {
                // not currently playing, but must go to specific animation location
                const float ratio   = std::max(0.0f, std::min(1.0f, _direction == PLAY_FORWARD ? _gotoRatio : 1.0f - _gotoRatio));
                int         frame   = static_cast<int>(floor(minFrame + ratio * (maxFrame - minFrame)));

                assert(frame >= minFrame && frame <= maxFrame);

                const float currentTime = start + ratio * duration;
                //if (frame >= numFrames)
                    //frame = numFrames - 1;

                //-----------------
                setSceneCurrentTime(*client, currentTime);
                //-----------------

                const bool looped = (_direction == PLAY_FORWARD && _lastSceneTime > currentTime)
                    || (_direction == PLAY_BACKWARD && _lastSceneTime < currentTime);

                if (looped)
                    handleAnimationLooped(*client);

                _lastSceneTime  = currentTime;
                _lastSceneFrame = frame;

                dirtyReferenceClockAndTime();
                _gotoRatio = -1.0;
            } // isRatioOK(_gotoRatio)
            else
            {
                if (_playing)
                {
                    const float rDuration = duration > 0.0f
                        ? 1.0f / duration
                        : 0.0f;

                    //if (_lastSceneFrame < 0)
                    //  _lastSceneFrame = minFrame;

                    if (_lastSceneTime < 0.0f)
                    {
                        handleAnimationLooped(*client);

                        const float ratio = static_cast<float>(_lastSceneFrame - minFrame) / static_cast<float>(maxFrame - minFrame);
                        assert(!(ratio < 0.0f) && !(ratio > 1.0f));

                        _lastSceneTime = _sceneTimeBounds.x() + ratio * duration;
                    }

                    int frame = 0;
                    // compute scene frame number
                    switch(_mode)
                    {
                    case ANIM_FREE_FPS:
                        {
                            frame = minFrame + (((_lastSceneFrame - minFrame) + (_direction == PLAY_FORWARD ? 1 : -1) + numFrames) % numFrames);
                            break;
                        }

                    case ANIM_CONST_FPS:
                    default:
                        {
                            if (_refFrame < 0)
                            {
                                _refClock   = clock();
                                _refFrame   = _lastSceneFrame;
                            }

                            const float deltaInSeconds  = (clock() - _refClock) / static_cast<float>(CLOCKS_PER_SEC);
                            const int   deltaInFrames   = deltaInSeconds > 0.0
                                ? static_cast<unsigned int>(_framerate * deltaInSeconds)
                                : 0;

                            frame = _refFrame + (_direction == PLAY_FORWARD ? deltaInFrames : - deltaInFrames);
                            if (frame > maxFrame)
                            {
                                frame       = minFrame;
                                _refClock   = clock();
                                _refFrame   = frame;
                            }
                            else if (frame < minFrame)
                            {
                                frame       = maxFrame;
                                _refClock   = clock();
                                _refFrame   = frame;
                            }

                            break;
                        }
                    }// end of scene frame number computation

                    assert(frame >= minFrame && frame <= maxFrame);

                    if (frame != _lastSceneFrame)
                    {
                        const float ratio       = static_cast<float>(frame - minFrame) / static_cast<float>(maxFrame - minFrame);
                        assert(!(ratio < 0.0f) && !(ratio > 1.0f));
                        const float currentTime = _sceneTimeBounds.x() + ratio * duration;

                        //-----------------
                        setSceneCurrentTime(*client, currentTime);
                        //-----------------

                        const bool looped = (_direction == PLAY_FORWARD && _lastSceneFrame > frame) ||
                            (_direction == PLAY_BACKWARD && _lastSceneFrame < frame);

                        if (looped)
                            handleAnimationLooped(*client);

                        _lastSceneTime  = currentTime;
                        _lastSceneFrame = frame;
                    }
                }
                else if (_frameNumberShift != 0)
                {
                    // not currently playing, but different frame required for display
                    int frame   = _lastSceneFrame + _frameNumberShift;
                    if (frame > maxFrame)
                        frame   = minFrame;
                    else if (frame < minFrame)
                        frame   = maxFrame;
                    assert(frame >= minFrame && frame <= maxFrame);

                    const float ratio       = static_cast<float>(frame - minFrame) / static_cast<float>(maxFrame - minFrame);
                    assert(!(ratio < 0.0) && !(ratio > 1.0));
                    const float currentTime = _sceneTimeBounds.x() + ratio * duration;

                    //-----------------
                    setSceneCurrentTime(*client, currentTime);
                    //-----------------

                    const bool looped = (_direction == PLAY_FORWARD && _lastSceneTime > currentTime)
                        || (_direction == PLAY_BACKWARD && _lastSceneTime < currentTime);

                    if (looped)
                        handleAnimationLooped(*client);

                    _lastSceneTime  = currentTime;
                    _lastSceneFrame = frame;

                    _frameNumberShift = 0;
                    dirtyReferenceClockAndTime();
                } // else if (_frameNumberShift != 0)
            } // else isRatioOK(_gotoRatio)

            _lastTime = currentTime;
        } // scene->areTimeBoundsOK()
    }
}

void
view::Animation::PImpl::handle(ClientBase&              client,
                               const xchg::ClientEvent& evt)
{
    if (evt.type() == xchg::ClientEvent::SCENE_SET)
        handleSceneSet(client, client.sceneId());
    else if (evt.type() == xchg::ClientEvent::SCENE_UNSET)
        handleSceneUnset(client, client.sceneId());
}

void
view::Animation::PImpl::handleSceneSet(ClientBase&  client,
                                       unsigned int sceneId)
{
    client.getNodeParameter<float>(parm::framerate(), _framerate, client.sceneId());
    if (!client.getNodeParameter<math::Vector2f>(parm::timeBounds(), _sceneTimeBounds, client.sceneId()))
        _sceneTimeBounds = math::Vector2f(FLT_MAX, -FLT_MAX);

    dirtyLastSceneTimeState();
}

void
view::Animation::PImpl::handleSceneUnset(ClientBase&    client,
                                         unsigned int   sceneId)
{
    _framerate          = 25.0f;
    _sceneTimeBounds    = math::Vector2f(FLT_MAX, -FLT_MAX);
    dirtyLastSceneTimeState();
}

void
view::Animation::PImpl::handleSceneParmEvent(ClientBase&            client,
                                             const xchg::ParmEvent& evt)
{
    if (evt.parm() == parm::framerate().id())
    {
        client.getNodeParameter<float>(parm::framerate(), _framerate, client.sceneId());

        dirtyLastSceneTimeState();
    }
    else if (evt.parm() == parm::timeBounds().id())
    {
        if (!client.getNodeParameter<math::Vector2f>(parm::timeBounds(), _sceneTimeBounds, client.sceneId()))
            _sceneTimeBounds = math::Vector2f(FLT_MAX, -FLT_MAX);

        if (_sceneTimeBounds.x() > _sceneTimeBounds.y())
            dirtyLastSceneTimeState();
        else if (_lastSceneTime < _sceneTimeBounds.x() ||
            _sceneTimeBounds.y() < _lastSceneTime)
            dirtyLastSceneTimeState();
    }
}

void
view::Animation::PImpl::registerLoopHandler(AbstractLoopHandler::Ptr const& handler)
{
    if (!handler)
        return;
    for (size_t i = 0; i < _loopHandlers.size(); ++i)
        if (_loopHandlers[i] == handler)
            return;
    _loopHandlers.push_back(handler);
}
void
view::Animation::PImpl::deregisterLoopHandler(AbstractLoopHandler::Ptr const& handler)
{
    if (!handler)
        return;
    for (size_t i = 0; i < _loopHandlers.size(); ++i)
        if (_loopHandlers[i] == handler)
        {
            std::swap(_loopHandlers[i], _loopHandlers.back());
            _loopHandlers.pop_back();
            return;
        }
}
void
view::Animation::PImpl::handleAnimationLooped(ClientBase& client)
{
    for (size_t i = 0; i < _loopHandlers.size(); ++i)
        if (_loopHandlers[i])
            _loopHandlers[i]->handle(_self, client);
}

/////////////////////////////
// class Animation
/////////////////////////////
// explicit
view::Animation::Animation(ClientBase::Ptr const& client):
    _pImpl(new PImpl(*this, client))
{ }

view::Animation::~Animation()
{
    delete _pImpl;
}

// virtual
void
view::Animation::build(ClientBase::Ptr const&   client,
                       Ptr const&               ptr)
{
    _pImpl->build(client, ptr);
}

// virtual
void
view::Animation::dispose(ClientBase::Ptr const& client)
{
    _pImpl->dispose(client);
}

// virtual
void
view::Animation::operator()(osg::Node*          n,
                            osg::NodeVisitor*   nv)
{
    _pImpl->runOperation(n, nv);
    traverse(n, nv);
}

bool
view::Animation::playing() const
{
    return _pImpl->playing();
}

void
view::Animation::play()
{
    _pImpl->play();
}

void
view::Animation::pause()
{
    _pImpl->pause();
}

void
view::Animation::start()
{
    _pImpl->start();
}

void
view::Animation::stop()
{
    _pImpl->stop();
}

void
view::Animation::goTo(float ratio)
{
    _pImpl->goTo(ratio);
}

void
view::Animation::firstFrame()
{
    _pImpl->firstFrame();
}

void
view::Animation::lastFrame()
{
    _pImpl->lastFrame();
}

void
view::Animation::previousFrame()
{
    _pImpl->previousFrame();
}

void
view::Animation::nextFrame()
{
    _pImpl->nextFrame();
}

view::AnimationMode
view::Animation::mode() const
{
    return _pImpl->mode();
}

void
view::Animation::mode(AnimationMode value)
{
    _pImpl->mode(value);
}

view::PlayingDirection
view::Animation::direction() const
{
    return _pImpl->direction();
}

void
view::Animation::direction(PlayingDirection value)
{
    _pImpl->direction(value);
}

void
view::Animation::registerLoopHandler(AbstractLoopHandler::Ptr const& handler)
{
    _pImpl->registerLoopHandler(handler);
}

void
view::Animation::deregisterLoopHandler(AbstractLoopHandler::Ptr const& handler)
{
    _pImpl->deregisterLoopHandler(handler);
}
