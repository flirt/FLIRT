// ========================================================================= //
// Copyright 2016-2018 SUPAMONKS_STUDIO                                      //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
//                                                                           //
// ========================================================================= //
#pragma once

#include <memory>
#include <ctime>

#include <osgGA/GUIEventHandler>

#include <smks/util/osg/key.hpp>

namespace smks { namespace view
{
    class Animation;

    class AnimationProfiler:
        public osgGA::GUIEventHandler
    {
        friend class LoopHandler;
    public:
        typedef osg::ref_ptr<AnimationProfiler> Ptr;
    private:
        class LoopHandler;
    private:
        typedef osg::observer_ptr<Animation>    AnimationWPtr;
    private:
        const util::osg::KeyControl     _key;

        const AnimationWPtr             _anim;
        clock_t                         _start;
        std::shared_ptr<LoopHandler>    _loopHandler;

    public:
        static
        Ptr
        create(Animation* anim,
               const util::osg::KeyControl& key = util::osg::KeyControl(osgGA::GUIEventAdapter::KEY_T))
        {
            Ptr ptr(new AnimationProfiler(
                anim,
                key));
            return ptr;
        }

        static
        void
        destroy(Ptr& ptr)
        {
            ptr = nullptr;
        }
    protected:
        AnimationProfiler(Animation*,
                          const util::osg::KeyControl&);

    public:
        ~AnimationProfiler();

        bool
        handle(const osgGA::GUIEventAdapter&,
               osgGA::GUIActionAdapter&);

    private:
        bool
        handleKeyUp(const osgGA::GUIEventAdapter&,
                    osgGA::GUIActionAdapter&);
        void
        handleAnimationLooped(Animation&,
                              ClientBase&);

    public:
        virtual
        void
        getUsage(osg::ApplicationUsage&) const;
    };
}
}
