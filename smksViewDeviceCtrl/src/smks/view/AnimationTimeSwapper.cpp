// ========================================================================= //
// Copyright 2016-2018 SUPAMONKS_STUDIO                                      //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
//                                                                           //
// ========================================================================= //

#include <sstream>
#include <iomanip>

#include <osgText/Text>

#include <smks/parm/BuiltIns.hpp>
#include <smks/parm/BuiltIn.hpp>
#include <smks/ClientBase.hpp>
#include <smks/view/AbstractDevice.hpp>

#include "smks/view/Animation.hpp"
#include "smks/view/AnimationTimeSwapper.hpp"
#include "smks/view/hud/Handler.hpp"
#include <smks/util/osg/callback.hpp>
#include <smks/util/osg/key.hpp>

namespace smks { namespace view
{
    class AnimationTimeSwapper::PImpl
    {
    private:
        bool    _wasPlaying;
        int     _refX;
        float   _refCurrentTime;
        bool    _enabled;
        int     _detectionMinY;
        int     _detectionMaxY;

        osg::observer_ptr<Animation> _anim;
    public:
        PImpl();

        inline
        bool
        handle(const osgGA::GUIEventAdapter&,
               osgGA::GUIActionAdapter&);

        inline
        void
        getUsage(osg::ApplicationUsage&) const;

        inline
        int
        detectionMinY() const
        {
            return _detectionMinY;
        }

        inline
        int
        detectionMaxY() const
        {
            return _detectionMaxY;
        }

        inline
        void
        detectionRangeY(int min, int max);
    private:
        inline
        bool
        detected(int x, int y) const;

        inline
        bool
        active() const;

        inline
        void
        enabled(osgViewer::Viewer*, bool);

        inline
        float
        getNewTimeAbsolute(int val, int range, float minTime, float maxTime) const;

        inline
        float
        getNewTimeRelative(int val, int range, float minTime, float maxTime) const;
    };
}
}

using namespace smks;

/////////////////////////////////////////
// class AnimationTimeSwapper::PImpl
/////////////////////////////////////////
// explicit
view::AnimationTimeSwapper::PImpl::PImpl():
    _wasPlaying     (false),
    _refX           (INVALID_POS),
    _refCurrentTime (0.0f),
    _enabled        (false),
    _detectionMinY  (0),
    _detectionMaxY  (50),
    _anim           (nullptr)
{ }

void
view::AnimationTimeSwapper::PImpl::detectionRangeY(int min, int max)
{
    if (min <= max)
    {
        _detectionMinY = min;
        _detectionMaxY = max;
    }
    else
    {
        _detectionMinY = max;
        _detectionMaxY = min;
    }
}

bool
view::AnimationTimeSwapper::PImpl::detected(int x, int y) const
{
    return _detectionMinY <= y && y <= _detectionMaxY;
}

// virtual
bool
view::AnimationTimeSwapper::PImpl::handle(const osgGA::GUIEventAdapter& ea,
                                          osgGA::GUIActionAdapter&      aa)
{
    if (ea.getHandled())
        return false;

    AbstractDevice* device  = dynamic_cast<AbstractDevice*>(&aa);
    ClientBase::Ptr client  = device ? device->client().lock() : nullptr;
    if (!client)
    {
        _refX = INVALID_POS;
        return false;
    }

    Animation::Ptr anim;
    if (!_anim.lock(anim))
    {
        // query and cache animation callback
        _anim = util::osg::getUpdateCallback<Animation>(device->clientRoot());
        _refX = INVALID_POS;
        return false;
    }

    const int x = static_cast<int>(ea.getX());
    const int y = ea.getMouseYOrientation() == osgGA::GUIEventAdapter::Y_INCREASING_DOWNWARDS
        ? ea.getWindowHeight() - static_cast<int>(ea.getY())
        : static_cast<int>(ea.getY());

    if (ea.getEventType() == osgGA::GUIEventAdapter::KEYDOWN)
        enabled(device, true);
    else if (ea.getEventType() == osgGA::GUIEventAdapter::KEYUP)
        enabled(device, false);

    if (!active())
    {
        if (ea.getEventType() == osgGA::GUIEventAdapter::PUSH &&
            ea.getButton() == osgGA::GUIEventAdapter::LEFT_MOUSE_BUTTON &&
            detected(x, y))
        {
            _wasPlaying     = anim->playing();
            _refX           = x;
            client->getNodeParameter<float>(parm::currentTime(), _refCurrentTime, client->sceneId());
            //_refCurrentTime   = client->currentTime();
            return true;
        }
        else
        {
            _refX = INVALID_POS;
            return false;
        }
    }
    else
    {
        math::Vector2f sceneTimeBounds(FLT_MAX, -FLT_MAX);

        if (client->getNodeParameter<math::Vector2f>(parm::timeBounds(), sceneTimeBounds, client->sceneId()) &&
            !(sceneTimeBounds.x() > sceneTimeBounds.y()))
        {
            const int   rangeX      = static_cast<int>(floorf(ea.getXmax() - ea.getXmin()));
            const float minTime     = sceneTimeBounds.x();
            const float maxTime     = sceneTimeBounds.y();
            const float duration    = maxTime - minTime;

#ifdef MOUSE_RELATIVE_TIME
            float newTime   = getNewTimeRelative(x, rangeX, minTime, maxTime);
#else
            float newTime   = getNewTimeAbsolute(x, rangeX, minTime, maxTime);
#endif // MOUSE_RELATIVE_TIME

            newTime = std::max(minTime, std::min(maxTime, newTime));
            assert(duration > 0.0f);

            const float ratio = std::max(0.0f, std::min(1.0f, (newTime - minTime) / duration));

            //################
            anim->goTo(ratio);
            //################

            view::hud::Handler* hud = util::osg::getEventHandler<view::hud::Handler>(
                device);

            if (ea.getEventType() == osgGA::GUIEventAdapter::RELEASE)
            {
                if (_wasPlaying)
                    anim->play();

                _wasPlaying     = false;
                _refX           = INVALID_POS;
                _refCurrentTime = 0.0f;

                if (hud)
                    hud->setNotification();
            }
            else if (hud)
            {
                float framerate = 0.0f;
                if (client->getNodeParameter<float>(parm::framerate(), framerate, client->sceneId()))
                {
                    std::stringstream sstr;
                    sstr
                        << "Go to " << static_cast<int>(floorf(newTime * framerate))
                        << " (" << std::setprecision(5) << newTime << ")";
                    hud->setNotification(sstr.str(), x, y);
                }
            }
            return true;
        }
    }

    _refX = INVALID_POS;
    return false;
}

void
view::AnimationTimeSwapper::PImpl::enabled(osgViewer::Viewer*   viewer,
                                           bool                 value)
{
    hud::Handler* hud = util::osg::getEventHandler<hud::Handler>(
        viewer);

    if (hud)
        hud->showTimeline(value);
    _enabled = value;
}

float
view::AnimationTimeSwapper::PImpl::getNewTimeRelative(int   val,
                                                      int   range,
                                                      float minTime,
                                                      float maxTime) const
{
    if (minTime > maxTime)
        return 0.0f;

    const float stepUnit = range > 1e-6f
        ? 4.0f * 1.1f * (maxTime - minTime) / static_cast<float>(range)
        : 0.0f;

    const int numSteps = static_cast<int>(floorf(val - _refX) * 0.25f);

    return static_cast<float>(_refCurrentTime + numSteps * stepUnit);
}

float
view::AnimationTimeSwapper::PImpl::getNewTimeAbsolute(int   val,
                                                      int   range,
                                                      float minTime,
                                                      float maxTime) const
{
    if (minTime > maxTime)
        return 0.0f;

    const int MARGIN = 10;

    if (range <= (MARGIN << 1))
        return minTime;

    const float ratio = std::max<float>(0.0f, std::min<float>(1.0f,
        (val - MARGIN) / static_cast<float>(range - (MARGIN << 1))));

    return minTime + ratio * (maxTime - minTime);
}

bool
view::AnimationTimeSwapper::PImpl::active() const
{
    return _refX >= 0;
}

void
view::AnimationTimeSwapper::PImpl::getUsage(osg::ApplicationUsage& usage) const
{
    util::osg::addMouseBinding(usage, util::osg::BINDINGS_VIEWPORT, 3, "Change time (bottom of the screen)", osgGA::GUIEventAdapter::LEFT_MOUSE_BUTTON);
}

/////////////////////////////
// class AnimationTimeSwapper
/////////////////////////////
view::AnimationTimeSwapper::AnimationTimeSwapper():
    osgGA::GUIEventHandler(),
    _pImpl(new PImpl())
{ }

view::AnimationTimeSwapper::~AnimationTimeSwapper()
{
    delete _pImpl;
}

// virtual
bool
view::AnimationTimeSwapper::handle(const osgGA::GUIEventAdapter&    ea,
                                   osgGA::GUIActionAdapter&         aa)
{
    return _pImpl->handle(ea, aa);
}

// virtual
void
view::AnimationTimeSwapper::getUsage(osg::ApplicationUsage& usage) const
{
    _pImpl->getUsage(usage);
}

int
view::AnimationTimeSwapper::detectionMinY() const
{
    return _pImpl->detectionMinY();
}

int
view::AnimationTimeSwapper::detectionMaxY() const
{
    return _pImpl->detectionMaxY();
}

void
view::AnimationTimeSwapper::detectionRangeY(int min, int max)
{
    _pImpl->detectionRangeY(min, max);
}
