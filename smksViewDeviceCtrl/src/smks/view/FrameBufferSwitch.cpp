// ========================================================================= //
// Copyright 2016-2018 SUPAMONKS_STUDIO                                      //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
//                                                                           //
// ========================================================================= //
#include <iostream>
#include <smks/sys/Log.hpp>
#include <smks/parm/BuiltIns.hpp>
#include <smks/parm/BuiltIn.hpp>

#include <smks/xchg/ClientEvent.hpp>
#include <smks/xchg/SceneEvent.hpp>
#include <smks/xchg/ParmEvent.hpp>

#include <smks/ClientBase.hpp>
#include <smks/client_node_filters.hpp>
#include <smks/util/osg/key.hpp>

#include <smks/view/AbstractDevice.hpp>
#include "smks/view/FrameBufferSwitch.hpp"
#include "NodeSwitch.hpp"

namespace smks { namespace view
{
    //////////////
    // class PImpl
    //////////////
    class FrameBufferSwitch::PImpl
    {
    private:
        class FrameBufferNodeSwitch;
    private:
        typedef std::shared_ptr<FrameBufferNodeSwitch> NodeSwitchPtr;
    private:
        const util::osg::KeyControl _keyNext;
        const util::osg::KeyControl _keyProgressive;

        bool            _isProgressive;         //<! flag forcing currently active framebuffer to be active
        unsigned int    _frameBufferToDisplay;  //<! ID of framebuffer that is displayed at next frame update
        unsigned int    _displayedFrameBuffer;  //<! ID of currently displayed framebuffer
        xchg::IdSet     _currentRenderActions;  //<! render actions the current framebuffer is linked to
        NodeSwitchPtr   _switch;

    public:
        PImpl(ClientBase::Ptr const&,
              const util::osg::KeyControl&,
              const util::osg::KeyControl&);

        inline
        void
        build(ClientBase::Ptr const&);

        inline
        void
        dispose(ClientBase::Ptr const&);

        inline
        unsigned int
        displayedFrameBuffer() const
        {
            return _displayedFrameBuffer;
        }

        inline
        bool
        inProgressiveRenderingMode() const
        {
            return _isProgressive;
        }

    private:
        inline
        void
        handleDisplayedFrameBufferAboutToChange(ClientBase&,
                                                unsigned int);

        inline
        void
        displayFrameBufferIfNeeded(AbstractDevice&);    // called at each frame

        inline
        void
        handle(ClientBase&, const xchg::SceneEvent&);

        inline
        void
        toogleProgressiveRenderingMode(ClientBase&);

        inline
        void
        tryToGrabRenderAction(ClientBase&, unsigned int);

        inline
        void
        tryToFreeRenderAction(ClientBase&, unsigned int);
    public:
        inline
        bool
        handle(const osgGA::GUIEventAdapter&,
               osgGA::GUIActionAdapter&);

        inline
        void
        getUsage(osg::ApplicationUsage&) const;
    };

    //////////////////////////////
    // class FrameBufferNodeSwitch
    //////////////////////////////
    class FrameBufferSwitch::PImpl::FrameBufferNodeSwitch:
        public NodeSwitch
    {
    public:
        typedef std::shared_ptr<FrameBufferNodeSwitch> Ptr;
    private:
        PImpl& _pImpl;
    public:
        static inline
        Ptr
        create(PImpl&                   pImpl,
               ClientBase::Ptr const&   client,
               xchg::NodeClass          nodeClass,
               char                     nodeInitialization,
               bool                     emptySlotAllowed = false)
        {
            Ptr ptr(new FrameBufferNodeSwitch(
                pImpl,
                nodeClass,
                nodeInitialization,
                emptySlotAllowed));

            ptr->build(client);
            return ptr;
        }

        static inline
        void
        destroy(ClientBase::Ptr const&  client,
                Ptr&                    ptr)
        {
            ptr->dispose(client);
            ptr = nullptr;
        }

    protected:
        FrameBufferNodeSwitch(PImpl&            pImpl,
                              xchg::NodeClass   nodeClass,
                              char              nodeInitialization,
                              bool              emptySlotAllowed):
            NodeSwitch  (nodeClass, nodeInitialization, emptySlotAllowed),
            _pImpl      (pImpl)
        { }

        virtual inline
        void
        handleCurrentNodeAboutToChange(ClientBase&  client,
                                       unsigned int node)
        {
            _pImpl.handleDisplayedFrameBufferAboutToChange(client, node);
        }

        virtual inline
        void
        handle(ClientBase&              client,
               const xchg::SceneEvent&  evt)
        {
            NodeSwitch::handle(client, evt);
            //---
            _pImpl.handle(client, evt);
        }
    };
}
}

using namespace smks;

//////////////
// class PImpl
//////////////
view::FrameBufferSwitch::PImpl::PImpl(ClientBase::Ptr const&        client,
                                      const util::osg::KeyControl&  keyNext,
                                      const util::osg::KeyControl&  keyProgressive):
    _keyNext                (keyNext),
    _keyProgressive         (keyProgressive),
    _isProgressive          (false),
    _frameBufferToDisplay   (0),
    _displayedFrameBuffer   (0),
    _currentRenderActions   (),
    _switch                 (FrameBufferNodeSwitch::create(*this, client, xchg::FRAMEBUFFER, xchg::ACTIVE, true))
{ }

// virtual
void
view::FrameBufferSwitch::PImpl::build(ClientBase::Ptr const& client)
{ }

// virtual
void
view::FrameBufferSwitch::PImpl::dispose(ClientBase::Ptr const& client)
{
    FrameBufferNodeSwitch::destroy(client, _switch);
}

void
view::FrameBufferSwitch::PImpl::handleDisplayedFrameBufferAboutToChange(
    ClientBase&     client,
    unsigned int    node)
{
    _frameBufferToDisplay = node;
}

void
view::FrameBufferSwitch::PImpl::displayFrameBufferIfNeeded(AbstractDevice& device)
{
    unsigned int displayed = device.displayedFrameBuffer();

    if (_frameBufferToDisplay == _displayedFrameBuffer)
        return;

    ClientBase::Ptr const& client = device.client().lock();
    if (!client)
        return;

    // reset rendering state of render actions linked to the previous framebuffer
    while(!_currentRenderActions.empty())
        tryToFreeRenderAction(*client, *_currentRenderActions.begin());

    //----------------------------------------------
    device.displayFrameBuffer(_frameBufferToDisplay);
    _displayedFrameBuffer = _frameBufferToDisplay;
    //----------------------------------------------

    // gather all render actions linked to the new current framebuffer node
    xchg::IdSet targets;

    if (_displayedFrameBuffer)
        client->getNodeLinkTargets(_displayedFrameBuffer, targets);
    for (xchg::IdSet::const_iterator id = targets.begin(); id != targets.end(); ++id)
        tryToGrabRenderAction(*client, *id);
}

void
view::FrameBufferSwitch::PImpl::handle(ClientBase&              client,
                                       const xchg::SceneEvent&  evt)
{
    // when the current framebuffer is (un)linked to a render action node, we must add(remove)
    // the 'isProgressive' parameter to the render action node.
    if (evt.type() == xchg::SceneEvent::LINK_ATTACHED)
        tryToGrabRenderAction(client, evt.target());
    else if (evt.type() == xchg::SceneEvent::LINK_DETACHED)
        tryToFreeRenderAction(client, evt.target());
}

void
view::FrameBufferSwitch::PImpl::tryToGrabRenderAction(ClientBase&   client,
                                                      unsigned int  node)
{
    if (_displayedFrameBuffer == 0)
        return;
    if (client.getNodeClass(node) != xchg::RENDER_ACTION)
        return;
    if (!client.isLinkBetweenNodes(parm::frameBufferId().id(), node, _displayedFrameBuffer))
        return;

    _currentRenderActions.insert(node);
    if (_isProgressive &&
        _currentRenderActions.size() > 1)
        std::cerr
            << "WARNING: framebuffer '" << client.getNodeName(_displayedFrameBuffer) << "' "
            << "is linked to " << _currentRenderActions.size() << " render actions "
            << "while in progressive rendering." << std::endl;
    //#############################
    client.setNodeParameter<bool>(parm::isProgressive(), _isProgressive, node);
    //#############################
}

void
view::FrameBufferSwitch::PImpl::tryToFreeRenderAction(ClientBase&   client,
                                                      unsigned int  node)
{
    if (_currentRenderActions.find(node) == _currentRenderActions.end())
        return;

    //#########################
    client.unsetNodeParameter(parm::isProgressive(), node);
    //#########################
    _currentRenderActions.erase(node);
}

void
view::FrameBufferSwitch::PImpl::toogleProgressiveRenderingMode(ClientBase& client)
{
    _isProgressive = !_isProgressive;

    xchg::IdSet renderActions;

    if (_isProgressive)
    {
        // activate progressive rendering for render actions
        // associated with CURRENTLY displayed framebuffer only
        renderActions = _currentRenderActions;
        if (renderActions.size() > 1)
            std::cerr
                << "WARNING: framebuffer '" << client.getNodeName(_displayedFrameBuffer) << "' "
                << "is linked to " << renderActions.size() << " render actions "
                << "while in progressive rendering." << std::endl;
    }
    else
        // deactivate progressive rendering for ALL render actions present in the scene
        client.getIds(renderActions, false, FilterClientNodeByClass::create(xchg::RENDER_ACTION));
    //#########################
    for (xchg::IdSet::const_iterator id = renderActions.begin(); id != renderActions.end(); ++id)
        client.setNodeParameter<bool>(parm::isProgressive(), _isProgressive, *id);
    //#########################
}

// virtual
bool
view::FrameBufferSwitch::PImpl::handle(const osgGA::GUIEventAdapter&    ea,
                                       osgGA::GUIActionAdapter&         aa)
{
    if (ea.getHandled())
        return false;

    AbstractDevice*                 device = dynamic_cast<AbstractDevice*>(&aa);
    ClientBase::Ptr const&  client = device ? device->client().lock() : nullptr;
    if (!client)
        return false;

    //----------------------------
    displayFrameBufferIfNeeded(*device);
    //----------------------------
    if (ea.getEventType() == osgGA::GUIEventAdapter::KEYUP)
    {
        // pick next entry in switch
        if (_keyNext.compatibleWith(ea))
        {
            _switch->next(*client);
            return true;
        }
        else if (_keyProgressive.compatibleWith(ea))
        {
            toogleProgressiveRenderingMode(*client);
            return true;
        }
    }
    return false;
}

// virtual
void
view::FrameBufferSwitch::PImpl::getUsage(osg::ApplicationUsage& usage) const
{
    util::osg::addKeyboardBinding(usage, util::osg::BINDINGS_RENDERING, 1, "Switch framebuffer",    _keyNext);
    util::osg::addKeyboardBinding(usage, util::osg::BINDINGS_RENDERING, 2, "Progressive rendering", _keyProgressive);
}

//////////////////////////
// class FrameBufferSwitch
//////////////////////////
view::FrameBufferSwitch::FrameBufferSwitch(ClientBase::Ptr const&       client,
                                           const util::osg::KeyControl& keyNext,
                                           const util::osg::KeyControl& keyProgressive):
    osgGA::GUIEventHandler(),
    _pImpl(new PImpl(client, keyNext, keyProgressive))
{ }

view::FrameBufferSwitch::~FrameBufferSwitch()
{
    delete _pImpl;
}

// virtual
void
view::FrameBufferSwitch::build(ClientBase::Ptr const& client)
{
    _pImpl->build(client);
}

// virtual
void
view::FrameBufferSwitch::dispose(ClientBase::Ptr const& client)
{
    _pImpl->dispose(client);
}


unsigned int
view::FrameBufferSwitch::displayedFrameBuffer() const
{
    return _pImpl->displayedFrameBuffer();
}

bool
view::FrameBufferSwitch::inProgressiveRenderingMode() const
{
    return _pImpl->inProgressiveRenderingMode();
}

// virtual
bool
view::FrameBufferSwitch::handle(const osgGA::GUIEventAdapter&   ea,
                                osgGA::GUIActionAdapter&        aa)
{
    return _pImpl->handle(ea, aa);
}

// virtual
void
view::FrameBufferSwitch::getUsage(osg::ApplicationUsage& usage) const
{
    return _pImpl->getUsage(usage);
}
