// ========================================================================= //
// Copyright 2016-2018 SUPAMONKS_STUDIO                                      //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
//                                                                           //
// ========================================================================= //
#include <smks/xchg/IdSet.hpp>
#include <smks/xchg/BoundingBox.hpp>
#include <smks/parm/BuiltIns.hpp>
#include <smks/parm/BuiltIn.hpp>
#include <smks/sys/constants.hpp>
#include <smks/sys/Log.hpp>
#include <smks/ClientBase.hpp>
#include <smks/client_node_filters.hpp>

#include <smks/view/AbstractDevice.hpp>
#include "smks/view/CameraFocus.hpp"

namespace smks { namespace view
{
    static const unsigned short NUM_TWEEN_FRAMES    = 8;
    static const double         MIN_TWEEN           = 0.2;

    //////////////
    // class PImpl
    //////////////
    class CameraFocus::PImpl
    {
    private:
        CameraFocus& _self;

        const util::osg::KeyControl _keyHome;
        const util::osg::KeyControl _keyFocus;

        unsigned short      _numTweenFrames;
        osg::BoundingSphere _sphereOfInterest;
        bool                _validTarget;
        double              _targetDistance;
        osg::Vec3d          _targetCenter;

    public:
        PImpl(CameraFocus&,
              const util::osg::KeyControl&,
              const util::osg::KeyControl&);
    public:
        inline
        bool
        handle(const osgGA::GUIEventAdapter&,
               osgGA::GUIActionAdapter&);

        inline
        void
        getUsage(osg::ApplicationUsage&) const;

    private:
        inline
        void
        tweenCameraFocusToTarget(const osg::Camera*);

        inline
        bool
        computeTargetCenterAndDistance(const osg::Camera*);
    };
}
}

using namespace smks;

//////////////
// class PImpl
//////////////
view::CameraFocus::PImpl::PImpl(CameraFocus&                    self,
                                const util::osg::KeyControl&    keyHome,
                                const util::osg::KeyControl&    keyFocus):
    _self               (self),
    _keyHome            (keyHome),
    _keyFocus           (keyFocus),
    _numTweenFrames     (0),
    _sphereOfInterest   (),
    _validTarget        (false),
    _targetDistance     (0.0),
    _targetCenter       ()
{
    _self.setAutoComputeHomePosition(true);
}

// virtual
bool
view::CameraFocus::PImpl::handle(const osgGA::GUIEventAdapter&  ea,
                                 osgGA::GUIActionAdapter&       aa)
{
    if (ea.getHandled())
        return false;

    //---------------
    if (!_self.enabled())
        return false;
    //---------------

    AbstractDevice*         device  = dynamic_cast<AbstractDevice*>(&aa);
    ClientBase::Ptr const&  client  = device ? device->client().lock() : nullptr;
    osg::Camera*            camera  = device ? device->getCamera() : nullptr;
    if (!client ||
        !camera)
        return false;

    tweenCameraFocusToTarget(camera);
    if (ea.getEventType() == osgGA::GUIEventAdapter::KEYUP)
    {
        if (_keyFocus.compatibleWith(ea))
        {
            xchg::IdSet selection;

            client->getNodeParameter<xchg::IdSet>(parm::selection(), selection);
            OrbitCameraHandler::getSphereOfInterest(_sphereOfInterest, *client, &selection);
            _numTweenFrames = NUM_TWEEN_FRAMES;

            return true;
        }
        else if (_keyHome.compatibleWith(ea))
        {
            _self.flushMouseEventStack();
            _self._thrown = false;
            _self.home(ea,aa);

            return true;
        }
        return false;
    }
    else if (ea.getEventType() == osgGA::GUIEventAdapter::KEYDOWN)
        return false;

    return _self.OrbitCameraHandler::handle(ea, aa);
}

void
view::CameraFocus::PImpl::tweenCameraFocusToTarget(const osg::Camera* camera)
{
    if (_numTweenFrames == 0)
        return;

    if (!_validTarget)
    {
        _validTarget = computeTargetCenterAndDistance(camera);
        if (!_validTarget)
        {
            _numTweenFrames = 0;    // impossible to find valid target. no tween
            return;
        }
    }

    const osg::Vec3d&   currentCenter   = _self.getCenter();
    const double        currentDistance = _self.getDistance();
    const bool          tweenCenter     = (_targetCenter - currentCenter).length2() > 1e-3;
    const bool          tweenDistance   = abs(_targetDistance - currentDistance) > 1e-3;

    const double        tweenMult       = 1.0 - (static_cast<double>(_numTweenFrames) - 1.0) / (static_cast<double>(NUM_TWEEN_FRAMES) - 1.0);
    const float         tween           = MIN_TWEEN + tweenMult * (1.0 - MIN_TWEEN);

    if (tweenCenter)
        _self.setCenter(
            currentCenter + (_targetCenter - currentCenter) * tween);

    if (tweenDistance)
        _self.setDistance(
            currentDistance + (_targetDistance - currentDistance) * tween);

    --_numTweenFrames;

    if (!tweenCenter && !tweenDistance)
        _numTweenFrames = 0;
    if (_numTweenFrames == 0)
        _validTarget = false;
}

bool
view::CameraFocus::PImpl::computeTargetCenterAndDistance(const osg::Camera* camera)
{
    if (!camera ||
        !_sphereOfInterest.valid())
        return false;

    double  fovY, aspect, zNear, zFar;
    if (!camera->getProjectionMatrixAsPerspective(fovY, aspect, zNear, zFar))
        return false;

    const float tanFovY = tanf(static_cast<float>(fovY) * static_cast<float>(sys::degrees_to_radians));
    if (tanFovY < 1e-6f)
        return false;

    _targetCenter   = _sphereOfInterest.center();
    _targetDistance = zNear + _sphereOfInterest.radius() * (1.0f + 1.0f / tanFovY);
    return true;
}

// virtual
void
view::CameraFocus::PImpl::getUsage(osg::ApplicationUsage& usage) const
{
    util::osg::addKeyboardBinding(usage, util::osg::BINDINGS_VIEWPORT, 1, "Focus view on selection", _keyFocus);
    util::osg::addKeyboardBinding(usage, util::osg::BINDINGS_VIEWPORT, 2, "Reset view",              _keyHome);
}

////////////////////
// class CameraFocus
////////////////////
view::CameraFocus::CameraFocus(const util::osg::KeyControl& keyHome,
                               const util::osg::KeyControl& keyFocus):
    OrbitCameraHandler(),
    _pImpl(new PImpl(*this, keyHome, keyFocus))
{ }

view::CameraFocus::~CameraFocus()
{
    delete _pImpl;
}

// virtual
bool
view::CameraFocus::handle(const osgGA::GUIEventAdapter& ea,
                          osgGA::GUIActionAdapter&      aa)
{
    return _pImpl->handle(ea, aa);
}

// virtual
void
view::CameraFocus::getUsage(osg::ApplicationUsage& usage) const
{
    _pImpl->getUsage(usage);
}
