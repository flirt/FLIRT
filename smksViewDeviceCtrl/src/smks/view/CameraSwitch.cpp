// ========================================================================= //
// Copyright 2016-2018 SUPAMONKS_STUDIO                                      //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
//                                                                           //
// ========================================================================= //
#include <smks/ClientBase.hpp>
#include <smks/view/AbstractDevice.hpp>

#include "NodeSwitch.hpp"
#include "smks/view/CameraSwitch.hpp"

namespace smks { namespace view
{
    //////////////
    // class PImpl
    //////////////
    class CameraSwitch::PImpl
    {
        private:
            class CameraNodeSwitch; // inherits from NodeSwitch and reimplements current index setter
        private:
            typedef std::shared_ptr<CameraNodeSwitch> NodeSwitchPtr;
        private:
            const util::osg::KeyControl _keyNext;

            unsigned int                _cameraToDisplay;
            NodeSwitchPtr               _switch;

        public:
            PImpl(ClientBase::Ptr const&,
                  const util::osg::KeyControl&);

            inline
            void
            build(ClientBase::Ptr const&);

            inline
            void
            dispose(ClientBase::Ptr const&);

        public:
            inline
            bool
            handle(const osgGA::GUIEventAdapter&,
                   osgGA::GUIActionAdapter&);

            inline
            void
            getUsage(osg::ApplicationUsage&) const;

        private:
            inline
            void
            handleDisplayedCameraAboutToChange(ClientBase&,
                                               unsigned int);

            inline
            void
            displayCameraIfNeeded(AbstractDevice&); // called at each frame

            inline
            size_t
            getCameraIndex(unsigned int) const;
    };

    /////////////////////////
    // class CameraNodeSwitch
    /////////////////////////
    class CameraSwitch::PImpl::CameraNodeSwitch:
        public NodeSwitch
    {
    public:
        typedef std::shared_ptr<CameraNodeSwitch> Ptr;
    private:
        PImpl& _pImpl;
    public:
        static inline
        Ptr
        create(PImpl&                 pImpl,
               ClientBase::Ptr const& client,
               xchg::NodeClass        nodeClass,
               char                   state,
               bool                   emptySlotAllowed)
        {
            Ptr ptr(new CameraNodeSwitch(
                pImpl,
                nodeClass,
                state,
                emptySlotAllowed));

            ptr->build(client);
            return ptr;
        }

        static inline
        void
        destroy(ClientBase::Ptr const&  client,
                Ptr&                    ptr)
        {
            if (ptr)
                ptr->dispose(client);
            ptr = nullptr;
        }

    private:
        CameraNodeSwitch(PImpl&          pImpl,
                         xchg::NodeClass nodeClass,
                         char            state,
                         bool            emptySlotAllowed):
            NodeSwitch  (nodeClass, state, emptySlotAllowed),
            _pImpl      (pImpl)
        { }

        inline
        void
        handleCurrentNodeAboutToChange(ClientBase&  client,
                                       unsigned int node)
        {
            _pImpl.handleDisplayedCameraAboutToChange(client, node);
        }
    };
}
}

using namespace smks;

//////////////
// class PImpl
//////////////
view::CameraSwitch::PImpl::PImpl(ClientBase::Ptr const&         client,
                                 const util::osg::KeyControl&   keyNext):
    _keyNext        (keyNext),
    _cameraToDisplay(0),
    _switch         (CameraNodeSwitch::create(*this, client, xchg::CAMERA, xchg::ACTIVE, false))
{ }

// virtual
void
view::CameraSwitch::PImpl::build(ClientBase::Ptr const& client)
{ }

// virtual
void
view::CameraSwitch::PImpl::dispose(ClientBase::Ptr const& client)
{
    CameraNodeSwitch::destroy(client, _switch);
}

// virtual
bool
view::CameraSwitch::PImpl::handle(const osgGA::GUIEventAdapter& ea,
                                  osgGA::GUIActionAdapter&      aa)
{
    if (ea.getHandled())
        return false;

    AbstractDevice*                 device = dynamic_cast<AbstractDevice*>(&aa);
    ClientBase::Ptr const&  client = device ? device->client().lock() : nullptr;
    if (!client)
        return false;

    //----------------------------
    displayCameraIfNeeded(*device);
    //----------------------------
    if (ea.getEventType() == osgGA::GUIEventAdapter::KEYUP)
    {
        // pick next camera in list
        if (_keyNext.compatibleWith(ea))
        {
            _switch->next(*client);
            return true;
        }
    }
    return false;
}

void
view::CameraSwitch::PImpl::handleDisplayedCameraAboutToChange(ClientBase&   client,
                                                              unsigned int  node)
{
    _cameraToDisplay = node;    // will effectively be displayed next frame
}

void
view::CameraSwitch::PImpl::displayCameraIfNeeded(AbstractDevice& device)
{
    if (_cameraToDisplay == 0)
        return;

    device.displayCamera(_cameraToDisplay);
    _cameraToDisplay = 0;
}

// virtual
void
view::CameraSwitch::PImpl::getUsage(osg::ApplicationUsage& usage) const
{
    util::osg::addKeyboardBinding(usage, util::osg::BINDINGS_VIEWPORT, 0, "Switch camera", _keyNext);
}

/////////////////////
// class CameraSwitch
/////////////////////
view::CameraSwitch::CameraSwitch(ClientBase::Ptr const&       client,
                                 const util::osg::KeyControl& keyNext):
    osgGA::GUIEventHandler(),
    _pImpl(new PImpl(client, keyNext))
{ }

// virtual
void
view::CameraSwitch::build(ClientBase::Ptr const& client)
{
    _pImpl->build(client);
}

// virtual
void
view::CameraSwitch::dispose(ClientBase::Ptr const& client)
{
    _pImpl->dispose(client);
}

view::CameraSwitch::~CameraSwitch()
{
    delete _pImpl;
}

// virtual
bool
view::CameraSwitch::handle(const osgGA::GUIEventAdapter& ea,
                           osgGA::GUIActionAdapter&      aa)
{
    return _pImpl->handle(ea, aa);
}

// virtual
void
view::CameraSwitch::getUsage(osg::ApplicationUsage& usage) const
{
    _pImpl->getUsage(usage);
}
