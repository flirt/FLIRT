// ========================================================================= //
// Copyright 2016-2018 SUPAMONKS_STUDIO                                      //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
//                                                                           //
// ========================================================================= //
#include <iostream>
#include <osgText/Text>

#include <std/to_string_xchg.hpp>
#include <smks/xchg/ClientEvent.hpp>
#include <smks/xchg/SceneEvent.hpp>
#include <smks/xchg/ParmEvent.hpp>

#include <smks/parm/BuiltIns.hpp>
#include <smks/parm/BuiltIn.hpp>
#include <smks/ClientBase.hpp>
#include <smks/AbstractClientNodeFilter.hpp>
#include <smks/AbstractClientObserver.hpp>
#include <smks/AbstractClientNodeObserver.hpp>

#include <smks/util/osg/text.hpp>
#include <smks/util/color/rgba8.hpp>
#include <smks/view/gl/ProgramInput.hpp>
#include <smks/view/gl/ProgramInputDeclarations.hpp>
#include <smks/view/gl/ProgramTemplate.hpp>
#include <smks/view/gl/ProgramTemplates.hpp>

#include "SceneObjCounts.hpp"

namespace smks { namespace view { namespace hud
{
    ////////////////
    // enum InfoType
    ////////////////
    enum InfoType
    {
        VISIBLE = 0,
        INVISIBLE,
        NOT_READY,
        _NUM_INFO_TYPES
    };

    ////////////////
    // enum NodeType
    ////////////////
    enum NodeType
    {
        OTHER_NODE = -1,
        CAMERA_NODE,
        MESH_NODE,
        CURVES_NODE,
        POINTS_NODE,
        LIGHT_NODE,
        _NUM_NODE_TYPES
    };

    //////////////////////
    // struct NodeTypeInfo
    //////////////////////
    struct SceneObjCounts::NodeTypeInfo
    {
        xchg::IdSet nodes[_NUM_INFO_TYPES];
    public:
        NodeTypeInfo()
        {
            for (int i = 0; i < _NUM_INFO_TYPES; ++i)
                nodes[i].clear();
        }
    private:
        // non-copyable
        NodeTypeInfo(const NodeTypeInfo&);
        NodeTypeInfo& operator=(const NodeTypeInfo&);
    public:
        void
        erase(unsigned int node)
        {
            for (int i = 0; i < _NUM_INFO_TYPES; ++i)
                nodes[i].erase(node);
        }

        void
        mergeTo(xchg::IdSet& ret) const
        {
            for (int i = 0; i < _NUM_INFO_TYPES; ++i)
                xchg::IdSet::merge(ret, nodes[i]);
        }
    };

    ///////////////////////
    // class ClientObserver
    ///////////////////////
    class SceneObjCounts::ClientObserver:
        public AbstractClientObserver
    {
    public:
        typedef std::shared_ptr<ClientObserver> Ptr;
    private:
        SceneObjCounts& _self;
    public:
        static
        Ptr
        create(SceneObjCounts& self)
        {
            Ptr ptr(new ClientObserver(self));
            return ptr;
        }
    protected:
        explicit
        ClientObserver(SceneObjCounts& self):
            _self(self)
        { }
    public:
        void
        handle(ClientBase& client, const xchg::ClientEvent& evt)
        {
            _self.handle(client, evt);
        }
        void
        handle(ClientBase& client, const xchg::SceneEvent& evt)
        {
            _self.handle(client, evt);
        }
        void
        handleSceneParmEvent(ClientBase& client, const xchg::ParmEvent& evt)
        {
            _self.handleSceneParmEvent(client, evt);
        }
    };

    /////////////////////
    // class ParmObserver
    /////////////////////
    class SceneObjCounts::ParmObserver:
        public AbstractClientNodeObserver
    {
    public:
        typedef std::shared_ptr<ParmObserver> Ptr;
    private:
        SceneObjCounts& _self;
    public:
        static
        Ptr
        create(SceneObjCounts& self)
        {
            Ptr ptr(new ParmObserver(self));
            return ptr;
        }
    protected:
        explicit
        ParmObserver(SceneObjCounts& self):
            _self(self)
        { }
    public:
        void
        handle(ClientBase&              client,
               unsigned int             node,
               const xchg::ParmEvent&   evt)
        {
            _self.handle(client, node, evt);
        }
    };

    ///////////////////////
    // class UpdateCallback
    ///////////////////////
    class SceneObjCounts::UpdateCallback:
        public osg::Callback
    {
    private:
        SceneObjCounts& _self;
    public:
        explicit
        UpdateCallback(SceneObjCounts& self):
            osg::Callback(),
            _self(self)
        { }
    public:
        virtual
        bool
        run(osg::Object* object, osg::Object* data)
        {
            _self.update();
            return traverse(object, data);
        }
    };

    static
    void
    setNodeName(osg::Node&, const char* prefix, const char* name);

    static
    const char*
    getLabelName(InfoType);
    static
    osg::Vec4f
    getLabelTextColor(InfoType);

    static
    NodeType
    getNodeType(const ClientBase&, unsigned int);

    static
    xchg::NodeClass
    getNodeClass(NodeType);

    static
    const char*
    getNodeTypeName(NodeType);
}
}
}

using namespace smks;

view::hud::SceneObjCounts::SceneObjCounts(ClientBase::WPtr const&   cl,
                                          const char*               osgName):
    osg::MatrixTransform(),
    _infoPerType(new NodeTypeInfo[_NUM_NODE_TYPES]),
    _labelType  (nullptr),
    _labelCounts(new TextPtr[_NUM_NODE_TYPES]),
    _geode      (new osg::Geode()),
    _client     (cl),
    _clientObs  (ClientObserver::create(*this)),
    _parmObs    (ParmObserver::create(*this)),
    _dirty      (true)
{
    ClientBase::Ptr const& client = _client.lock();
    if (!client)
        return;

    client->registerObserver(_clientObs);
    if (client->sceneId() != 0)
        handleSceneSetOrUnset(*client, false);
    addUpdateCallback(new UpdateCallback(*this));

    setNodeName(*_geode, osgName, "geode");

    int x = 0;
    int y = 0;
    {
        std::stringstream sstr;

        for (int t = 0; t < _NUM_NODE_TYPES; ++t)
            sstr << getNodeTypeName(static_cast<NodeType>(t)) << "\n";
        _labelType = util::osg::createLabel(sstr.str().c_str(), x, y);
        _geode->addDrawable(_labelType.get());

        setNodeName(*_labelType, osgName, "label_types");
    }
    x += 60;
    for (int i = 0; i < _NUM_INFO_TYPES; ++i)
    {
        std::stringstream sstr;

        x+= 56; // right alignment
        _labelCounts[i] = util::osg::createLabel("", x, y);
        _geode->addDrawable(_labelCounts[i].get());

        _labelCounts[i]->setColor(getLabelTextColor(static_cast<InfoType>(i)));
        _labelCounts[i]->setAlignment(osgText::TextBase::RIGHT_BASE_LINE);
        setNodeName(*_labelCounts[i], osgName, getLabelName(static_cast<InfoType>(i)));
    }
    addChild(_geode);

    update();
}

view::hud::SceneObjCounts::~SceneObjCounts()
{
    _geode->removeChild(_labelType);
    for (int i = 0; i < _NUM_INFO_TYPES; ++i)
        _geode->removeChild(_labelCounts[i]);
    removeChild(_geode);

    ClientBase::Ptr const& client = _client.lock();

    if (client)
    {
        handleSceneSetOrUnset(*client, true);
        client->deregisterObserver(_clientObs);
    }

    delete[] _infoPerType;
    delete[] _labelCounts;
}

void
view::hud::SceneObjCounts::handle(ClientBase& client, const xchg::ClientEvent& evt)
{
    if (evt.type() == xchg::ClientEvent::SCENE_SET)
        handleSceneSetOrUnset(client, false);
    else if (evt.type() == xchg::ClientEvent::SCENE_UNSET)
        handleSceneSetOrUnset(client, true);
}

void
view::hud::SceneObjCounts::handle(ClientBase& client, const xchg::SceneEvent& evt)
{
    if (evt.type() == xchg::SceneEvent::NODE_CREATED)
        handleNodeAddedOrDeleted(client, evt.node(), false);
    else if (evt.type() == xchg::SceneEvent::NODE_DELETED)
        handleNodeAddedOrDeleted(client, evt.node(), true);
}

void
view::hud::SceneObjCounts::handleSceneParmEvent(ClientBase&, const xchg::ParmEvent&)
{ }

void
view::hud::SceneObjCounts::handle(ClientBase&               client,
                                  unsigned int              node,
                                  const xchg::ParmEvent&    evt)
{
    if (evt.parm() == parm::globalInitialization().id() ||
        evt.parm() == parm::globallyVisible().id())
        handleNodeStatusChanged(client, node, false);
}

void
view::hud::SceneObjCounts::update()
{
    if (!_dirty)
        return;

    for (int c = 0; c < _NUM_INFO_TYPES; ++c)
    {
        std::stringstream sstr;
        for (int r = 0; r < _NUM_NODE_TYPES; ++r)
        {
            const NodeTypeInfo& info = _infoPerType[r];
            sstr << info.nodes[c].size() << "\n";
        }
        _labelCounts[c]->setText(sstr.str());
        _labelCounts[c]->dirtyGLObjects();
    }
    _dirty = false;
}

void
view::hud::SceneObjCounts::handleSceneSetOrUnset(
    ClientBase& client,
    bool        unset)
{
    xchg::IdSet nodes;

    // clear currently-tracked nodes
    getTrackedNodes(nodes);
    for (xchg::IdSet::const_iterator node = nodes.begin();
        node != nodes.end();
        ++node)
        handleNodeAddedOrDeleted(client, *node, true);

    if (!unset &&
        client.sceneId() != 0)
    {
        // track new scene's nodes
        client.getIds(nodes);
        for (xchg::IdSet::const_iterator node = nodes.begin();
            node != nodes.end();
            ++node)
            handleNodeAddedOrDeleted(client, *node, false);
    }
}

void
view::hud::SceneObjCounts::handleNodeAddedOrDeleted(
    ClientBase&     client,
    unsigned int    node,
    bool            deleted)
{
    if (handleNodeStatusChanged(client, node, deleted))
    {
        if (deleted)
            client.deregisterObserver(_parmObs, node);
        else
            client.registerObserver(_parmObs, node);
    }
}

bool //<! returns true if node is taken into account
view::hud::SceneObjCounts::handleNodeStatusChanged(
    const ClientBase&   client,
    unsigned int        node,
    bool                deleted)
{
    // filter out nodes based on their type
    const NodeType typ = getNodeType(client, node);
    if (typ == OTHER_NODE)
        return false;

    _dirty = true;

    NodeTypeInfo& info = _infoPerType[typ];
    info.erase(node);

    if (deleted)
        return true;

    char visible    = 0;
    char active     = 0;

    client.getNodeParameter<char>(parm::globallyVisible(), visible, node);
    client.getNodeParameter<char>(parm::globalInitialization(), active, node);

    xchg::IdSet& nodes = active != xchg::ACTIVE
        ? info.nodes[NOT_READY]
        : (visible > 0
            ? info.nodes[VISIBLE]
            : info.nodes[INVISIBLE]);
    nodes.insert(node);

    return true;
}

void
view::hud::SceneObjCounts::getTrackedNodes(xchg::IdSet& ret) const
{
    ret.clear();
    for (size_t i = 0; i < _NUM_INFO_TYPES; ++i)
        _infoPerType[i].mergeTo(ret);
}

// static
void
view::hud::setNodeName(osg::Node& node, const char* prefix, const char* name)
{
    std::stringstream sstr;

    sstr << (strlen(prefix) > 0 ? prefix : "obj_counts") << "." << name;
    node.setName(sstr.str());
}

// static
const char*
view::hud::getLabelName(InfoType typ)
{
    switch (typ)
    {
    case VISIBLE:   return "label_visible";
    case INVISIBLE: return "label_invisible";
    case NOT_READY: return "label_not_ready";
    default:        return "";
    }
}

// static
osg::Vec4f
view::hud::getLabelTextColor(InfoType typ)
{
    switch (typ)
    {
    case VISIBLE:   return osg::Vec4f(0.0f, 1.0f, 0.0f, 1.0f);
    case INVISIBLE: return osg::Vec4f(0.0f, 1.0f, 1.0f, 1.0f);
    case NOT_READY: return osg::Vec4f(1.0f, 0.0f, 0.0f, 1.0f);
    default:        return osg::Vec4f(1.0f, 1.0f, 1.0f, 1.0f);
    }
}

// static
view::hud::NodeType
view::hud::getNodeType(const ClientBase& cl, unsigned int node)
{
    switch (cl.getNodeClass(node))
    {
    case xchg::CAMERA:  return CAMERA_NODE;
    case xchg::POINTS:  return POINTS_NODE;
    case xchg::CURVES:  return CURVES_NODE;
    case xchg::MESH:    return MESH_NODE;
    case xchg::LIGHT:   return LIGHT_NODE;
    default:            return OTHER_NODE;
    }
}
// static
xchg::NodeClass
view::hud::getNodeClass(NodeType typ)
{
    switch (typ)
    {
    case CAMERA_NODE:   return xchg::CAMERA;
    case MESH_NODE:     return xchg::MESH;
    case CURVES_NODE:   return xchg::CURVES;
    case POINTS_NODE:   return xchg::POINTS;
    case LIGHT_NODE:    return xchg::LIGHT;
    default:            return xchg::NONE;
    }
}
// static
const char*
view::hud::getNodeTypeName(NodeType typ)
{
    switch (typ)
    {
    case CAMERA_NODE:   return "Cameras";
    case MESH_NODE:     return "Meshes";
    case CURVES_NODE:   return "Curves";
    case POINTS_NODE:   return "Points";
    case LIGHT_NODE:    return "Lights";
    default:            return "";
    }
}

