// ========================================================================= //
// Copyright 2016-2018 SUPAMONKS_STUDIO                                      //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
//                                                                           //
// ========================================================================= //
#include <osg/PolygonMode>
#include <osg/MatrixTransform>
#include <osgViewer/Viewer>
#include <osgViewer/Renderer>
#include <osgText/Text>

#include <smks/sys/Exception.hpp>

#include <smks/sys/TextResource.hpp>
#include <smks/sys/ResourceDatabase.hpp>
#include <smks/view/gl/ProgramInput.hpp>
#include <smks/view/gl/ProgramInputDeclarations.hpp>
#include <smks/view/gl/ProgramTemplate.hpp>
#include <smks/view/gl/ProgramTemplates.hpp>
#include <smks/ClientBase.hpp>

#include <smks/math/matrixAlgo.hpp>
#include <smks/util/color/rgba8.hpp>
#include <smks/util/osg/NodeMask.hpp>
#include <smks/util/osg/key.hpp>
#include <smks/util/osg/UnitSquareXY.hpp>
#include <smks/util/osg/geometry.hpp>
#include <smks/util/osg/text.hpp>

#include <smks/view/AbstractDevice.hpp>
#include "smks/view/Animation.hpp"
#include "smks/view/hud/Handler.hpp"
#include "PinnedNode.hpp"
#include "cb_animation.hpp"
#include "cb_manipulation.hpp"
#include "cb_selection.hpp"
#include "cb_progressive.hpp"
#include "cb_framerate.hpp"
#include "cb_frame_color.hpp"
#include "cb_current_camera.hpp"
#include "cb_current_framebuffer.hpp"
#include "SceneObjCounts.hpp"
#include "TextPanel.hpp"

#include <std/to_string_osg.hpp>

namespace smks { namespace view { namespace hud
{
    static const float  HUD_AREA_WIDTH      = 1280.0f;
    static const float  HUD_AREA_HEIGHT     = 720.0f;
    static const float  KEY_DESCR_SPACE     = 5.0f;
    static const float  LINE_SPACING        = 0.6f; // expressed as a ratio of font's height
    static const float  TITLE_LINE_SPACING  = 0.8f; // expressed as a ratio of font's height
    static const float  BOX_PADDING         = 8.0f;
    static const float  BOX_MARGIN          = 8.0f;
    static const float  MAX_UPDATE_DELTA    = 50.0f;
    static const size_t NUM_COLUMNS         = 3;

    static const unsigned int       DFLT_TEXT_COLOR     = -1;       // white
    static const unsigned int       HIGH_TEXT_COLOR     = util::color::uchar4ToUint(0, 255, 255, 255);
    static gl::UniformInput::Ptr    U_DFLT_TEXT_COLOR   = nullptr;
    static gl::UniformInput::Ptr    U_HIGH_TEXT_COLOR   = nullptr;

    /////////////////
    // enum LabelCode
    /////////////////
    typedef enum LabelCode
    {
        LABEL_ANIMATION = 0,
        LABEL_CURRENT_CAMERA,
        LABEL_CURRENT_FRAMEBUFFER,
        LABEL_FRAME_COLOR,
        LABEL_SELECTION,
        LABEL_FRAMERATE,
        LABEL_MANIPULATION,
        LABEL_PROGRESSIVE_RENDER,
        _NUM_LABELS
    };

    static
    const char*
    getLabelName(LabelCode);

    static
    osg::Vec2f
    getLabelPosition(LabelCode);

    static
    osg::ref_ptr<osg::Drawable::CullCallback>
    getLabelCullCallback(LabelCode, AbstractDevice*);

    static
    osg::ref_ptr<osg::Drawable::DrawCallback>
    getLabelDrawCallback(LabelCode, AbstractDevice*, float);

    /////////////////////
    // struct BindingInfo
    /////////////////////
    struct BindingInfo
    {
        std::string key, descr;
    };
    typedef std::map<size_t, BindingInfo> BindingInfoMap;

    /////////////////
    // struct HelpBox
    /////////////////
    struct HelpBox
    {
        osg::ref_ptr<osg::MatrixTransform> root, fg, bg;
        float width, height;
    public:
        inline
        HelpBox():
            root(nullptr), fg(nullptr), bg(nullptr), width(0.0f), height(0.0f)
        { }
    };

    static
    HelpBox
    createHelpBox(const std::string& title,
                  const BindingInfoMap&,
                  sys::ResourceDatabase::Ptr const&,
                  const std::string& osgName);
    static
    HelpBox
    createHelpBox(const util::osg::KeyControl&,
                  const std::string&,
                  sys::ResourceDatabase::Ptr const&,
                  const std::string& osgName);
    static
    void
    fitHelpBoxBackground(HelpBox&);
    static
    void
    fitHelpBoxBackground(HelpBox&, size_t width, size_t height);

    static void visible(osg::Node*, bool);
    static void visible(PinnedNode::Ptr const&, bool);
    static bool visible(const osg::Node*);
    static bool visible(PinnedNode::Ptr const&);

    //////////////
    // class PImpl
    //////////////
    class Handler::PImpl:
        public osgGA::GUIEventHandler
    {
    public:
        typedef osg::ref_ptr<Handler> Ptr;
    private:
        typedef osg::ref_ptr<osgText::Text>             TextPtr;
        typedef osg::ref_ptr<osg::MatrixTransform>      MatrixTransformPtr;
        typedef osg::ref_ptr<osg::Program>              ProgramPtr;
        typedef osg::ref_ptr<osg::Uniform>              UniformPtr;
        typedef std::shared_ptr<util::osg::BasicShape>  BasicShapePtr;
        typedef std::shared_ptr<sys::ResourceDatabase>  ResourcesPtr;
    private:
        class RemoveCallbacksVisitor;
    private:
        typedef osg::observer_ptr<AbstractDevice>        DeviceWPtr;
        typedef std::vector<std::shared_ptr<PinnedNode>> PinnedNodes;
    protected:
        const util::osg::KeyControl _keyHelp;
        const util::osg::KeyControl _keyInfo;

        DeviceWPtr                  _device;

        osg::ref_ptr<osg::Camera>   _camera;
        osg::ref_ptr<osg::Group>    _root;
        //--------------------------
        // information subscene
        MatrixTransformPtr          _groupInfo;
        MatrixTransformPtr          _objCounts;
        MatrixTransformPtr          _groupInfoBinding;
        MatrixTransformPtr          _groupHelpBinding;
        //--------------------------
        // help subscene
        MatrixTransformPtr          _groupHelp;
        //--------------------------
        // timeline
        MatrixTransformPtr          _groupTimeline;
        TextPtr                     _notification;

        PinnedNodes                 _pinnedNodes;

        osg::Matrix                 _pixelToWorld;

        osg::ref_ptr<osg::ApplicationUsage> _applicationUsage;
        osg::Vec2f                          _timelineRangeY;

    public:
        PImpl(osg::ApplicationUsage*,
              const util::osg::KeyControl& keyHelp,
              const util::osg::KeyControl& keyInfo);

        inline
        void
        buildScene(AbstractDevice*);

        inline
        void
        disposeScene();

    public:
        inline
        osg::ApplicationUsage*
        applicationUsage() const
        {
            return _applicationUsage;
        }

        inline
        void
        applicationUsage(osg::ApplicationUsage* value)
        {
            _applicationUsage = value;
        }

        inline
        bool
        handle(const osgGA::GUIEventAdapter&,
               osgGA::GUIActionAdapter&);

    protected:
        inline
        void
        handleResize(int, int, int, int);

    public:
        inline
        void
        showHelp(bool);

        inline
        void
        showInfo(bool);

        inline
        void
        showTimeline(bool);

        inline
        bool
        isHelpVisible() const;

        inline
        bool
        isInfoVisible() const;

        inline
        bool
        isTimelineVisible() const;

        inline
        void
        backgroundColor(unsigned int);

        inline
        void
        setNotification(const std::string& = std::string(),
                        float normalizedX=0.5f,
                        float normalizedY=0.5f);

        inline
        const osg::Vec2f&
        timelineRangeY() const
        {
            return _timelineRangeY;
        }

        inline
        void
        timelineRangeY(const osg::Vec2f&);

    private:
        inline
        osg::MatrixTransform*
        initializeHelp(AbstractDevice*);

        inline
        osg::MatrixTransform*
        initializeInfo(AbstractDevice*);

        inline
        osg::MatrixTransform*
        initializeTimeline(AbstractDevice*);

        static
        osg::Matrixf
        mapUnitSquare(float xMin, float xMax, float yMin, float yMax, float z);
    };


    ///////////////////////////////
    // class RemoveCallbacksVisitor
    ///////////////////////////////
    class Handler::PImpl::RemoveCallbacksVisitor:
        public osg::NodeVisitor
    {
    public:
        RemoveCallbacksVisitor():
            osg::NodeVisitor(osg::NodeVisitor::TRAVERSE_ALL_CHILDREN)
        { }

        inline
        void
        apply(osg::Drawable& node)
        {
            osg::Callback*                  cullCallback = node.getCullCallback();
            osg::Drawable::DrawCallback*    drawCallback = node.getDrawCallback();

            if (cullCallback)
                node.removeCullCallback(cullCallback);
            if (drawCallback)
                node.setDrawCallback(nullptr);
        }
    };
}
}
}

using namespace smks;

// static
view::hud::HelpBox
view::hud::createHelpBox(
    const util::osg::KeyControl&        key,
    const std::string&                  description,
    sys::ResourceDatabase::Ptr const&   resources,
    const std::string&                  osgName)
{
    std::stringstream sstr;
    sstr << key;

    BindingInfoMap  bindings;
    BindingInfo     binding = { sstr.str(), description };

    bindings[0] = binding;

    return createHelpBox("", bindings, resources, osgName);
}

// static
view::hud::HelpBox
view::hud::createHelpBox(
    const std::string&                  title,
    const BindingInfoMap&               bindings,
    sys::ResourceDatabase::Ptr const&   resources,
    const std::string&                  osgName)
{
    typedef osg::ref_ptr<osgText::Text> TextPtr;

    HelpBox ret;

    ret.width   = 0.0f;
    ret.height  = 0.0f;

    ret.root                = new osg::MatrixTransform();
    ret.fg                  = new osg::MatrixTransform();   // used to map center to origin
    ret.bg                  = new osg::MatrixTransform();   // scale unit box used as background
    osg::Geode* geodeKey    = new osg::Geode();
    osg::Geode* geodeDescr  = new osg::Geode();

    osg::BoundingBox bounds;

    //-------------
    // keys
    int x = 0;
    int y = 0;

    if (U_HIGH_TEXT_COLOR)
        geodeKey->getOrCreateStateSet()->addUniform(U_HIGH_TEXT_COLOR->data());
    for (BindingInfoMap::const_iterator it = bindings.begin(); it != bindings.end(); ++it)
    {
        std::stringstream   sstr;
        TextPtr const&      label = util::osg::createLabel(it->second.key.c_str(), x, y);

        sstr << osgName << ".label_key_" << it->first;
        label->setName(sstr.str());
        geodeKey->addDrawable(label.get());

        y -= static_cast<int>(LINE_SPACING * label->getFontHeight());
    }
    bounds = geodeKey->getBoundingBox();

    //-------------
    // descriptions
    x = (bounds.xMax() - bounds.xMin()) + KEY_DESCR_SPACE;
    y = 0.0f;

    if (U_DFLT_TEXT_COLOR)
        geodeDescr->getOrCreateStateSet()->addUniform(U_DFLT_TEXT_COLOR->data());
    for (BindingInfoMap::const_iterator it = bindings.begin(); it != bindings.end(); ++it)
    {
        std::stringstream   sstr;
        TextPtr const&      label = util::osg::createLabel(it->second.descr.c_str(), x, y);

        sstr << osgName << ".label_descr_" << it->first;
        label->setName(sstr.str());
        geodeDescr->addDrawable(label.get());

        y -= static_cast<int>(LINE_SPACING * label->getFontHeight());
    }
    bounds.expandBy(geodeDescr->getBoundingBox());

    //-------------
    // title
    if (!title.empty())
    {
        std::stringstream   sstr;
        osg::Geode*         geodeTitle  = new osg::Geode();
        TextPtr const&      label       = util::osg::createLabel(title.c_str());

        sstr << osgName << ".label_title";
        label->setName(sstr.str());
        geodeTitle->addDrawable(label.get());

        if (U_HIGH_TEXT_COLOR)
            geodeTitle->getOrCreateStateSet()->addUniform(U_HIGH_TEXT_COLOR->data());

        const osg::BoundingBox& titleBounds = geodeTitle->getBoundingBox();

        const float x = (bounds.xMin() + bounds.xMax()) * 0.5f - (titleBounds.xMax() - titleBounds.xMin()) * 0.5f;
        const float y = TITLE_LINE_SPACING * label->getFontHeight();

        label->setPosition(osg::Vec3f(x, y, 0.0f));
        geodeTitle->setName((osgName + ".geodeTitle").c_str());
        bounds.expandBy(geodeTitle->getBoundingBox());

        ret.fg->addChild(geodeTitle);
    }

    //-------------
    // background
    if (resources)
    {
        const float cx  = (bounds.xMax() + bounds.xMin()) * 0.5f;
        const float cy  = (bounds.yMax() + bounds.yMin()) * 0.5f;
        ret.width       = bounds.xMax() - bounds.xMin();
        ret.height      = bounds.yMax() - bounds.yMin();

        util::osg::BasicShape* shapeBG = new util::osg::UnitSquareXY(resources);

        shapeBG->fillColor(util::color::uchar4ToUint(64, 64, 64, 128));
        shapeBG->strokeColor(util::color::uchar4ToUint(255, 255, 255, 255));
        shapeBG->setName((osgName + ".shapeBG").c_str());
        ret.bg->addChild(shapeBG);

        ret.fg->setMatrix(osg::Matrixf::translate(-cx, -cy, 0.0f));
    }
    //-------------
    ret.root    ->setName((osgName + ".root")       .c_str());
    ret.fg      ->setName((osgName + ".fg")         .c_str());
    ret.bg      ->setName((osgName + ".bg")         .c_str());
    geodeKey    ->setName((osgName + ".geodeKey")   .c_str());
    geodeDescr  ->setName((osgName + ".geodeDescr") .c_str());

    ret.root->addChild(ret.fg);
    ret.root->addChild(ret.bg);

    ret.fg->addChild(geodeKey);
    ret.fg->addChild(geodeDescr);

    return ret;
}

// static
void
view::hud::fitHelpBoxBackground(HelpBox& box)
{
    fitHelpBoxBackground(box, box.width, box.height);
}

// static
void
view::hud::fitHelpBoxBackground(HelpBox& box, size_t width, size_t height)
{
    box.width   = width + BOX_PADDING;
    box.height  = height + BOX_PADDING;

    box.bg->setMatrix(
        osg::Matrixf::scale(box.width, box.height, 1.0f) *
        osg::Matrixf::translate(0.0f, 0.0f, -1.0f));

    box.width   += BOX_MARGIN;
    box.height  += BOX_MARGIN;
}

// static
void
view::hud::visible(osg::Node* n, bool value)
{
    if (n)
        n->setNodeMask(value
            ? util::osg::VISIBLE_TO_ALL
            : util::osg::INVISIBLE_TO_ALL);
}

// static
void
view::hud::visible(PinnedNode::Ptr const& n, bool value)
{
    if (n)
        visible(n->asNode(), value);
}

// static
bool
view::hud::visible(const osg::Node* n)
{
    return n &&
        n->getNodeMask() == util::osg::VISIBLE_TO_ALL;
}

// static
bool
view::hud::visible(PinnedNode::Ptr const& n)
{
    return n && visible(n->asNode());
}


////////////////////////////////////////////////////////////////////
// class Handler::PImpl
///////////////////////
view::hud::Handler::PImpl::PImpl(osg::ApplicationUsage*         usage,
                                 const util::osg::KeyControl&   keyHelp,
                                 const util::osg::KeyControl&   keyInfo):
    osgGA::GUIEventHandler(),
    _keyHelp            (keyHelp),
    _keyInfo            (keyInfo),
    _device             (),
    _camera             (nullptr),
    _root               (nullptr),
    _groupInfo          (nullptr),
    _objCounts          (nullptr),
    _groupInfoBinding   (nullptr),
    _groupHelpBinding   (nullptr),
    _groupHelp          (nullptr),
    _groupTimeline      (nullptr),
    _notification       (nullptr),
    _pinnedNodes        (),
    _pixelToWorld       (osg::Matrix::identity()),
    _applicationUsage   (usage),
    _timelineRangeY     (0.0f, 50.0f)
{ }

void
view::hud::Handler::PImpl::buildScene(AbstractDevice* device)
{
    if (_camera)
        return; // already initialized

    _device = device;

    ClientBase::Ptr const&              client      = device ? device->client().lock() : nullptr;
    sys::ResourceDatabase::Ptr const&   resources   = client ? client->getOrCreateResourceDatabase() : nullptr;
    osg::ref_ptr<osg::Program>          textProg    = resources
        ? gl::getOrCreateProgram(gl::get_TextProgram(), resources)
        : nullptr;

    if (!textProg)
        return;

    // first, retrieve the usage information we are supposed to display
    if (!_applicationUsage)
        applicationUsage(new osg::ApplicationUsage());
    device->getUsage(*_applicationUsage);

    // prepare HUD interface's main camera
    _camera = new osg::Camera();

    _camera->setCullMask(util::osg::CAMERA_FULL_MASK);
    _camera->setRenderer(new osgViewer::Renderer(_camera.get()));
    _camera->setRenderOrder(osg::Camera::POST_RENDER, 1);

    if (device->getCamera())
        _camera->setGraphicsContext(device->getCamera()->getGraphicsContext());

    _camera->setReferenceFrame  (osg::Transform::ABSOLUTE_RF);
    _camera->setViewMatrix      (osg::Matrix::identity());
    _camera->setProjectionMatrix(osg::Matrix::ortho2D(0, HUD_AREA_WIDTH, 0, HUD_AREA_HEIGHT));

    _camera->setClearMask(0); // only clear the depth buffer

    // prepare the HUD interface's scene
    _root = new osg::Group;

    osg::StateSet* stateSet = _root->getOrCreateStateSet();

    stateSet->setMode       (GL_LIGHTING,               osg::StateAttribute::OFF);
    stateSet->setMode       (GL_BLEND,                  osg::StateAttribute::ON);
    stateSet->setMode       (GL_DEPTH_TEST,             osg::StateAttribute::OFF);
    stateSet->setAttribute  (new osg::PolygonMode(),    osg::StateAttribute::PROTECTED);

    U_DFLT_TEXT_COLOR                   = gl::get_TextProgram().createUniform(gl::uTextColor(), DFLT_TEXT_COLOR);
    U_HIGH_TEXT_COLOR                   = gl::get_TextProgram().createUniform(gl::uTextColor(), HIGH_TEXT_COLOR);
    gl::UniformInput::Ptr uTextGlyphs   = gl::get_TextProgram().createUniform(gl::uTextGlyphs(), 0);

    stateSet->addUniform(uTextGlyphs        ->data());
    stateSet->addUniform(U_DFLT_TEXT_COLOR  ->data());
    stateSet->setAttributeAndModes(textProg, osg::StateAttribute::ON);

    if (!_applicationUsage)
        applicationUsage(new osg::ApplicationUsage());

    device->getUsage(*_applicationUsage);

    _groupHelp      = initializeHelp(device);
    _groupInfo      = initializeInfo(device);
    _groupTimeline  = initializeTimeline(device);

    showHelp(false);
    showInfo(true);
    showTimeline(false);
    //-------------------------------------
    _root->addChild(_groupHelp.get());
    _root->addChild(_groupInfo.get());
    _root->addChild(_groupTimeline.get());

    _camera->addChild(_root.get());

    _root   ->setName("__hud_root");
    _camera ->setName("__hud_camera");

    osg::Group* sceneData = reinterpret_cast<osg::Group*>(device->getSceneData());
    if (sceneData)
        sceneData->addChild(_camera);
}

void
view::hud::Handler::PImpl::disposeScene()
{
    osg::ref_ptr<AbstractDevice> device;
    if (!_device.lock(device))
        return;

    RemoveCallbacksVisitor visitor;
    if (_root)
        _root->accept(visitor);

    osg::Group* sceneData = reinterpret_cast<osg::Group*>(
        device->getSceneData());
    if (sceneData)
        sceneData->removeChild(_camera);

    _pixelToWorld = osg::Matrix::identity();

    _notification       = nullptr;
    _groupTimeline      = nullptr;

    _groupHelp          = nullptr;

    _groupHelpBinding   = nullptr;
    _groupInfoBinding   = nullptr;
    _objCounts          = nullptr;
    _groupInfo          = nullptr;

    _pinnedNodes.clear();
    _root               = nullptr;
    _camera             = nullptr;
}

osg::MatrixTransform*
view::hud::Handler::PImpl::initializeHelp(AbstractDevice* device)
{
    osg::MatrixTransform* ret = new osg::MatrixTransform();

    ClientBase::Ptr const&              client      = device ? device->client().lock() : nullptr;
    sys::ResourceDatabase::Ptr const&   resources   = client ? client->getOrCreateResourceDatabase() : nullptr;
    if (!resources)
        return ret;

    osg::MatrixTransform*   helpRemap   = new osg::MatrixTransform(); // used to map center to origin

    ret->setName("__hud_help_root");
    helpRemap->setName("__hud_help_remap");
    ret->addChild(helpRemap);

    const osg::ApplicationUsage::UsageMap& bindings = _applicationUsage->getKeyboardMouseBindings();
    if (bindings.empty())
        return ret;

    const int            numSections = static_cast<int>(util::osg::NUM_BINDING_SECTIONS);
    std::vector<HelpBox> helpBoxes;

    helpBoxes.reserve(numSections);
    for (int sct = 0; sct < numSections; ++sct)
    {
        BindingInfoMap  bindingInfos;
        std::string     sectionName = std::to_string(static_cast<util::osg::BindingSection>(sct));
        std::string     prefix      = "__bindings_" + sectionName;

        std::transform(sectionName.begin(), sectionName.end(), sectionName.begin(), ::toupper);
        for(osg::ApplicationUsage::UsageMap::const_iterator it = bindings.begin(); it != bindings.end(); ++it)
        {
            util::osg::BindingSection   section;
            size_t                      idx     = 0;
            const char*                 keys    = util::osg::splitSectionFromKeys(it->first.c_str(), section, idx);

            if (section != sct)
                continue;

            BindingInfo info = { keys, it->second };
            bindingInfos[idx] = info;
        }
        if (!bindingInfos.empty())
        {
            helpBoxes.push_back(
                createHelpBox(sectionName,
                              bindingInfos,
                              resources,
                              prefix));
            helpRemap->addChild(helpBoxes.back().root);
        }
    }
    //-------------
    // layout boxes

    // get help boxes' standard width
    float boxWidth = 0.0f;
    for (size_t i = 0; i < helpBoxes.size(); ++i)
        boxWidth = std::max(boxWidth, helpBoxes[i].width);

    // adjust boxes' background and remapping transforms
    for (size_t i = 0; i < helpBoxes.size(); ++i)
        fitHelpBoxBackground(helpBoxes[i], boxWidth, helpBoxes[i].height);

    // layout help boxes
    float columnHeight [NUM_COLUMNS];
    for (size_t c = 0; c < NUM_COLUMNS; ++c)
        columnHeight[c] = 0.0f;

    float xmin = FLT_MAX;
    float ymin = FLT_MAX;
    float xmax = -FLT_MAX;
    float ymax = -FLT_MAX;
    for (size_t i = 0; i < helpBoxes.size(); ++i)
    {
        // pick columns with max height (y upwards)
        size_t  col         = 0;
        float   maxHeight   = columnHeight[0];
        for (size_t c = 1; c < NUM_COLUMNS; ++c)
            if (maxHeight < columnHeight[c])
            {
                maxHeight = columnHeight[c];
                col = c;
            }
        // add current box at the end of the column
        HelpBox&    box = helpBoxes[i];
        const float cx  = col * box.width;
        const float cy  = maxHeight - box.height * 0.5f;

        box.root->setMatrix(osg::Matrixf::translate(cx, cy, 0.0f));
        columnHeight[col] -= box.height;
        xmin = std::min(xmin, cx - 0.5f * box.width);
        xmax = std::max(xmax, cx + 0.5f * box.width);
        ymin = std::min(ymin, cy - 0.5f * box.height);
        ymax = std::max(ymax, cy + 0.5f * box.height);
    }

    // recenter all help content
    helpRemap->setMatrix(
        osg::Matrixf::translate(-(xmin + xmax) * 0.5f, -(ymin + ymax) * 0.5f, 0.0f));

    PinnedGroup::create(_pinnedNodes, ret, 0.5f, 0.5f);
    return ret;
}

osg::MatrixTransform*
view::hud::Handler::PImpl::initializeInfo(AbstractDevice* device)
{
    osg::MatrixTransform* ret = new osg::MatrixTransform;

    ClientBase::Ptr const&              client      = device ? device->client().lock() : nullptr;
    sys::ResourceDatabase::Ptr const&   resources   = client ? client->getOrCreateResourceDatabase() : nullptr;
    if (!resources)
        return ret;

    ret->setName("__info_xform");

    // create all labels showing scene related information
    if (device)
    {
        for (size_t i = 0; i < _NUM_LABELS; ++i)
        {
#if defined(NDEBUG)
            if (i == LABEL_FRAMERATE)
                continue;
#endif // defined(NDEBUG)

            const osg::Vec2f&   relPos  = getLabelPosition(static_cast<LabelCode>(i));
            PinnedText::Ptr     label   = PinnedText::create(
                _pinnedNodes,
                util::osg::createLabel(""),
                relPos.x(), relPos.y());

            ret->addChild(label->asNode());

            label->asNode()->setName(
                getLabelName(static_cast<LabelCode>(i)));
            label->asNode()->setCullCallback(
                getLabelCullCallback(static_cast<LabelCode>(i), device));
            label->asNode()->asDrawable()->setDrawCallback(
                getLabelDrawCallback(static_cast<LabelCode>(i), device, MAX_UPDATE_DELTA));
        }
    }

    _notification = util::osg::createLabel();
    _notification->setName("__label_notification");
    ret->addChild(_notification.get());

    _objCounts = TextPanel::create(resources, "__obj_counts_panel");
    if (client)
    {
        _objCounts->addChild(
            SceneObjCounts::create(client, "__obj_counts"));
    }
    PinnedGroup::create(_pinnedNodes, _objCounts, 0.1f, 0.0975f);
    ret->addChild(_objCounts.get());

    // add specific key bindings information
    HelpBox boxHelp = createHelpBox(_keyHelp, "Help", resources, "__box_help");
    HelpBox boxInfo = createHelpBox(_keyInfo, "Info", resources, "__box_info");

    const size_t boxWidth   = std::max(boxHelp.width, boxInfo.width);
    const size_t boxHeight  = std::max(boxHelp.height, boxInfo.height);

    fitHelpBoxBackground(boxHelp, boxWidth, boxHeight);
    fitHelpBoxBackground(boxInfo, boxWidth, boxHeight);

    _groupHelpBinding = boxHelp.root;
    _groupInfoBinding = boxInfo.root;

    PinnedGroup::create(_pinnedNodes, _groupHelpBinding, 0.925f, 0.025f);
    PinnedGroup::create(_pinnedNodes, _groupInfoBinding, 0.975f, 0.025f);

    ret->addChild(_groupHelpBinding.get());
    ret->addChild(_groupInfoBinding.get());

    return ret;
}

osg::MatrixTransform*
view::hud::Handler::PImpl::initializeTimeline(AbstractDevice* device)
{
    osg::MatrixTransform* ret = new osg::MatrixTransform;

    ClientBase::Ptr const&              client      = device ? device->client().lock() : nullptr;
    sys::ResourceDatabase::Ptr const&   resources   = client ? client->getOrCreateResourceDatabase() : nullptr;
    if (!resources)
        return ret;

    util::osg::BasicShape*  timelineShape   = new util::osg::UnitSquareXY(resources);

    timelineShape->strokeColor(util::color::uchar4ToUint(0, 255, 0, 255));
    timelineShape->setName("__timeline_shape");
    ret->addChild(timelineShape);
    ret->setName("__timeline_xform");

    return ret;
}

bool
view::hud::Handler::PImpl::handle(const osgGA::GUIEventAdapter& ea,
                                  osgGA::GUIActionAdapter&      aa)
{
    if (ea.getHandled())
        return false;

    AbstractDevice* device = dynamic_cast<AbstractDevice*>(&aa);
    if (!device)
        return false;

    //################
    buildScene(device);
    //################
    switch(ea.getEventType())
    {
    default:
        break;

    case osgGA::GUIEventAdapter::RESIZE:
        handleResize(ea.getWindowX(),
                     ea.getWindowY(),
                     ea.getWindowWidth(),
                     ea.getWindowHeight());
        break; // Others may want to handle resize event themselves

    case osgGA::GUIEventAdapter::KEYUP:
        if (_keyHelp.compatibleWith(ea))
        {
            showHelp (!isHelpVisible());
            return true;
        }
        else if (_keyInfo.compatibleWith(ea))
        {
            showInfo (!isInfoVisible());
            return true;
        }
        break;
    }
    return false;
}

void
view::hud::Handler::PImpl::handleResize(int x, int y, int width, int height)
{
    if (_camera.valid())
    {
        _camera->setProjectionMatrix(osg::Matrix::ortho2D(0, width, 0, height));
        _camera->setViewport        (0, 0, width, height);

        _pixelToWorld = osg::Matrix::inverse(
            _camera->getViewMatrix()
            * _camera->getProjectionMatrix()
            * _camera->getViewport()->computeWindowMatrix()
        );

        for (size_t i = 0; i < _pinnedNodes.size(); ++i)
            _pinnedNodes[i]->handleResize(width, height, _pixelToWorld);

        if (_groupTimeline.valid())
        {
            _groupTimeline->setMatrix(
                mapUnitSquare(0.0f, width, _timelineRangeY.x(), _timelineRangeY.y(), -0.1f));
        }
    }
}

void
view::hud::Handler::PImpl::showHelp(bool value)
{
    visible(_groupHelp.get(), value);
    if (_groupHelpBinding)
        visible(_groupHelpBinding->asNode(), !value);
}

void
view::hud::Handler::PImpl::showInfo(bool value)
{
    visible(_groupInfo.get(), value);
}

void
view::hud::Handler::PImpl::showTimeline(bool value)
{
    visible(_groupTimeline.get(), value);
}

bool
view::hud::Handler::PImpl::isHelpVisible() const
{
    return visible(_groupHelp.get());
}

bool
view::hud::Handler::PImpl::isInfoVisible() const
{
    return visible(_groupInfo.get());
}

bool
view::hud::Handler::PImpl::isTimelineVisible() const
{
    return visible(_groupTimeline.get());
}

void
view::hud::Handler::PImpl::timelineRangeY(const osg::Vec2f& value)
{
    if (value.x() < value.y())
        _timelineRangeY.set(value.x(), value.y());
    else
        _timelineRangeY.set(value.y(), value.x());
}

void
view::hud::Handler::PImpl::setNotification(const std::string& str, float x, float y)
{
    const osg::Vec4f& pos = osg::Vec4f(x, y, 0.0f, 1.0f) * _pixelToWorld;

    _notification->setText(str);
    _notification->dirtyGLObjects();
    _notification->setPosition(osg::Vec3f(pos.x(), pos.y(), 0.0f));
}

/////////////////////
// class hud::Handler
/////////////////////
view::hud::Handler::Handler(osg::ApplicationUsage*          usage,
                            const util::osg::KeyControl&    keyHelp,
                            const util::osg::KeyControl&    keyInfo):
    osgGA::GUIEventHandler(),
    _pImpl(new PImpl(usage, keyHelp, keyInfo))
{ }

view::hud::Handler::~Handler()
{
    delete _pImpl;
}

void
view::hud::Handler::buildScene(AbstractDevice* device)
{
    _pImpl->buildScene(device);
}

void
view::hud::Handler::disposeScene()
{
    _pImpl->disposeScene();
}

osg::ApplicationUsage*
view::hud::Handler::applicationUsage() const
{
    return _pImpl->applicationUsage();
}

// virtual
void
view::hud::Handler::applicationUsage(osg::ApplicationUsage* usage)
{
    return _pImpl->applicationUsage(usage);
}

// virtual
bool
view::hud::Handler::handle(const osgGA::GUIEventAdapter&    ea,
                           osgGA::GUIActionAdapter&         aa)
{
    return _pImpl->handle(ea, aa);
}

void
view::hud::Handler::showHelp(bool value)
{
    _pImpl->showHelp(value);
}

void
view::hud::Handler::showInfo(bool value)
{
    _pImpl->showInfo(value);
}

void
view::hud::Handler::showTimeline(bool value)
{
    _pImpl->showTimeline(value);
}

bool
view::hud::Handler::isHelpVisible() const
{
    return _pImpl->isHelpVisible();
}

bool
view::hud::Handler::isInfoVisible() const
{
    return _pImpl->isInfoVisible();
}

bool
view::hud::Handler::isTimelineVisible() const
{
    return _pImpl->isTimelineVisible();
}

void
view::hud::Handler::setNotification(const std::string& text,
                                    float normalizedX,
                                    float normalizedY)
{
    _pImpl->setNotification(text, normalizedX, normalizedY);
}

const osg::Vec2f&
view::hud::Handler::timelineRangeY() const
{
    return _pImpl->timelineRangeY();
}

void
view::hud::Handler::timelineRangeY(const osg::Vec2f& value)
{
    _pImpl->timelineRangeY(value);
}

/////////////////////////////////


// static
osg::Matrixf
view::hud::Handler::PImpl::mapUnitSquare(float xMin, float xMax, float yMin, float yMax, float z)
{
    const float width   = std::max(0.0f, xMax - xMin);
    const float height  = std::max(0.0f, yMax - yMin);

    return osg::Matrix::scale(width, height, 1.0f) *
           osg::Matrix::translate(osg::Vec3(xMin + width * 0.5f, yMin + height * 0.5f, z));
}

// static
const char*
view::hud::getLabelName(LabelCode code)
{
    switch (code)
    {
    case LABEL_ANIMATION:           return "__animation_label";
    case LABEL_CURRENT_CAMERA:      return "__cur_camera_label";
    case LABEL_CURRENT_FRAMEBUFFER: return "__cur_fbuffer_label";
    case LABEL_FRAME_COLOR:         return "__frame_color_label";
    case LABEL_SELECTION:           return "__selection_label";
    case LABEL_FRAMERATE:           return "__framerate_label";
    case LABEL_MANIPULATION:        return "__manipulation_label";
    case LABEL_PROGRESSIVE_RENDER:  return "__prog_render_label";
    default:                        return "";
    }
}

// static
osg::Vec2f
view::hud::getLabelPosition(LabelCode code)
{
    switch (code)
    {
    case LABEL_ANIMATION:           return osg::Vec2f(0.01f, 0.01f);
    case LABEL_CURRENT_CAMERA:      return osg::Vec2f(0.5f, 1.0f - 0.025f);
    case LABEL_CURRENT_FRAMEBUFFER: return osg::Vec2f(0.5f, 1.0f - 0.05f);
    case LABEL_FRAME_COLOR:         return osg::Vec2f(0.5f, 1.0f - 0.075f);
    case LABEL_SELECTION:           return osg::Vec2f(0.01f, 1.0f - 0.025f);
    case LABEL_FRAMERATE:           return osg::Vec2f(0.4f, 0.01f);
    case LABEL_MANIPULATION:        return osg::Vec2f(0.5f, 0.01f);
    case LABEL_PROGRESSIVE_RENDER:  return osg::Vec2f(0.65f, 0.01f);
    default:                        return osg::Vec2f(0.0f, 0.0f);
    }
}

// static
osg::ref_ptr<osg::Drawable::CullCallback>
view::hud::getLabelCullCallback(LabelCode       code,
                                AbstractDevice* device)
{
    switch (code)
    {
    case LABEL_ANIMATION:           return new AnimationCullCallback();
    case LABEL_CURRENT_CAMERA:      return new CurrentCameraCullCallback();
    case LABEL_CURRENT_FRAMEBUFFER: return new CurrentFrameBufferCullCallback();
    case LABEL_FRAME_COLOR:         return new FrameColorCullCallback();
    case LABEL_SELECTION:           return new SelectionCullCallback();
    case LABEL_FRAMERATE:           return new FramerateCullCallback();
    case LABEL_MANIPULATION:        return new ManipulationCullCallback();
    case LABEL_PROGRESSIVE_RENDER:  return new ProgressiveCullCallback();
    default:                        return nullptr;
    }
}

// static
osg::ref_ptr<osg::Drawable::DrawCallback>
view::hud::getLabelDrawCallback(LabelCode       code,
                                AbstractDevice* device,
                                float           delta)
{
    switch (code)
    {
    case LABEL_ANIMATION:           return new AnimationDrawCallback(delta);
    case LABEL_CURRENT_CAMERA:      return new CurrentCameraDrawCallback(delta);
    case LABEL_CURRENT_FRAMEBUFFER: return new CurrentFrameBufferDrawCallback(delta);
    case LABEL_FRAME_COLOR:         return new FrameColorDrawCallback(delta);
    case LABEL_SELECTION:           return new SelectionDrawCallback(delta);
    case LABEL_FRAMERATE:           return new FramerateDrawCallback(delta);
    case LABEL_MANIPULATION:        return new ManipulationDrawCallback(delta);
    case LABEL_PROGRESSIVE_RENDER:  return new ProgressiveDrawCallback(delta);
    default:                        return nullptr;
    }
}
