// ========================================================================= //
// Copyright 2016-2018 SUPAMONKS_STUDIO                                      //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
//                                                                           //
// ========================================================================= //
#include <osg/Geode>
#include <osg/ComputeBoundsVisitor>

#include <smks/sys/ResourceDatabase.hpp>
#include <smks/util/color/rgba8.hpp>
#include <smks/util/osg/NodeMask.hpp>
#include <smks/util/osg/UnitSquareXY.hpp>

#include "TextPanel.hpp"

namespace smks { namespace view { namespace hud
{
    static const float BOX_PADDING = 8.0f;
}
}
}


using namespace smks;

view::hud::TextPanel::TextPanel(
    sys::ResourceDatabase::Ptr const&   resources,
    const std::string&                  title,
    const std::string&                  name):
    osg::MatrixTransform(),
    _foregroundGeode    (new osg::Geode()),
    _foreground         (new osg::MatrixTransform()),
    _background         (new osg::MatrixTransform())
{
    const std::string n = name.empty()
        ? "text_panel"
        : name;
    setName((n + ".root").c_str());
    _foregroundGeode->setName((n + ".fg_geode").c_str());
    _foreground->setName((n + ".fg").c_str());
    _background->setName((n + ".bg").c_str());

    _foreground->addChild(_foregroundGeode.get());
    _addChild(_foreground.get());
    _addChild(_background.get());

    if (resources)
    {
        util::osg::BasicShape* box = new util::osg::UnitSquareXY(resources);

        box->setName((n + ".box_shape").c_str());
        box->fillColor(util::color::uchar4ToUint(64, 64, 64, 128));
        box->strokeColor(util::color::uchar4ToUint(255, 255, 255, 255));
        _background->addChild(box);
    }
    updateMatrices();
}

void
view::hud::TextPanel::updateMatrices()
{
    osg::ComputeBoundsVisitor visitor;

    _foregroundGeode->accept(visitor);

    const osg::BoundingBoxf& bounds = visitor.getBoundingBox();

    if (bounds.valid())
    {
        // recenter foreground
        _foreground->setMatrix(osg::Matrixf::translate(
            -0.5f * (bounds.xMin() + bounds.xMax()),
            -0.5f * (bounds.yMin() + bounds.yMax()),
            0.0f));

        // fit background shape to foreground
        const float width   = BOX_PADDING + (bounds.xMax() - bounds.xMin());
        const float height  = BOX_PADDING + (bounds.yMax() - bounds.yMin());

        _background->setNodeMask(util::osg::VISIBLE_TO_ALL);
        _background->setMatrix(
            osg::Matrixf::scale(width, height, 1.0f) *
            osg::Matrixf::translate(0.0f, 0.0f, -1.0f));
    }
    else
        _background->setNodeMask(util::osg::INVISIBLE_TO_ALL);
}
