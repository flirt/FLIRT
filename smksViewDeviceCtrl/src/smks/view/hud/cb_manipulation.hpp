// ========================================================================= //
// Copyright 2016-2018 SUPAMONKS_STUDIO                                      //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
//                                                                           //
// ========================================================================= //
#pragma once

#include <sstream>
#include <smks/parm/BuiltIns.hpp>
#include <smks/parm/BuiltIn.hpp>
#include <smks/xchg/ManipulatorFlags.hpp>

#include "CullCallbackBase.hpp"
#include "DrawCallbackBase.hpp"

namespace smks { namespace view { namespace hud
{
    class ManipulationCullCallback:
        public CullCallbackBase
    {
    protected:
        virtual inline
        bool
        cull(const AbstractDevice& device) const
        {
            return false;
        }
    };

    class ManipulationDrawCallback:
        public DrawCallbackBase
    {
    protected:
        virtual inline
        void
        getLabelText(const AbstractDevice& device, std::string& ret) const
        {
            ret.clear();

            ClientBase::Ptr const& client = device.client().lock();
            if (!client)
                return;

            std::stringstream sstr;
            char chr = xchg::NO_MANIP;

            client->getNodeParameter<char>(parm::manipulatorFlags(), chr, client->sceneId());
            xchg::ManipulatorFlags mode = static_cast<xchg::ManipulatorFlags>(chr);
            if (mode == xchg::NO_MANIP)
                sstr << "No manipulation";
            else
            {
                if (mode & xchg::MANIP_TRANSLATE)   sstr << "Translate";
                else if (mode & xchg::MANIP_ROTATE) sstr << "Rotate";
                else if (mode & xchg::MANIP_SCALE)  sstr << "Scale";

                if (mode & xchg::MANIP_OBJECT_MODE) sstr << " in local space";
                else                                sstr << " in world space";
            }
            ret = sstr.str();
        }
    public:
        explicit
        ManipulationDrawCallback(float maxUpdateDelta):
            DrawCallbackBase(maxUpdateDelta)
        { }
    };
}
}
}
