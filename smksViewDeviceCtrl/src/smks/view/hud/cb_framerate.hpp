// ========================================================================= //
// Copyright 2016-2018 SUPAMONKS_STUDIO                                      //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
//                                                                           //
// ========================================================================= //
#pragma once

#include <iomanip>
#include <sstream>
#include "CullCallbackBase.hpp"
#include "DrawCallbackBase.hpp"

namespace smks { namespace view { namespace hud
{
    class FramerateCullCallback:
        public CullCallbackBase
    {
    protected:
        virtual inline
        bool
        cull(const AbstractDevice& device) const
        {
            const_cast<AbstractDevice*>(&device)
                ->getViewerStats()->collectStats("frame_rate", true);
            return false;
        }
    };

    class FramerateDrawCallback:
        public DrawCallbackBase
    {
    protected:
        virtual inline
        void
        getLabelText(const AbstractDevice& device, std::string& ret) const
        {
            ret.clear();

            const osg::Stats*   stats       = device.getViewerStats();
            double              framerate   = 0.0;
            if (!stats ||
                !stats->getAveragedAttribute("Frame rate", framerate))
                return;

            std::stringstream   sstr;
            sstr
                << std::setprecision(2) << std::fixed
                << "Display: " << framerate << " fps";
            ret = sstr.str();
        }
    public:
        explicit
        FramerateDrawCallback(float maxUpdateDelta):
            DrawCallbackBase(maxUpdateDelta)
        { }
    };
}
}
}
