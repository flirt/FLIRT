// ========================================================================= //
// Copyright 2016-2018 SUPAMONKS_STUDIO                                      //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
//                                                                           //
// ========================================================================= //
#pragma once

#include <memory>
#include <osg/MatrixTransform>
#include <osgText/Text>

namespace smks { namespace view { namespace hud
{
    ///////////////////
    // class PinnedNode
    ///////////////////
    class PinnedNode
    {
    public:
        typedef std::shared_ptr<PinnedNode> Ptr;
    protected:
        typedef std::vector<Ptr> Vec;
    protected:
        osg::ref_ptr<osg::Node> _node;
    private:
        osg::Vec2f              _relPos;
    protected:
        PinnedNode(osg::Node* node, float x, float y):
            _node   (node),
            _relPos (x, y)
        { }
    public:
        virtual
        ~PinnedNode()
        { }

        osg::Node*
        asNode()
        {
            return _node.get();
        }
        const osg::Node*
        asNode() const
        {
            return _node.get();
        }

        void
        relPos(float x, float y)
        {
            _relPos.set(x, y);
        }

        void
        handleResize(int                    width,
                     int                    height,
                     const osg::Matrixf&    pixelToWorld)
        {
            const float         px          = _relPos.x() * width;
            const float         py          = _relPos.y() * height;
            const osg::Vec4f&   worldPos    = osg::Vec4f(px, py, 0.0f, 1.0f) * pixelToWorld;

            moveTo(worldPos.x(), worldPos.y());
        }

    protected:
        virtual
        void
        moveTo(float, float) = 0;
    };

    ///////////////////
    // class PinnedText
    ///////////////////
    class PinnedText:
        public PinnedNode
    {
    public:
        typedef std::shared_ptr<PinnedText> Ptr;
    public:
        static
        Ptr
        create(Vec& nodes, osgText::Text* text, float x=0.0f, float y=0.0f)
        {
            Ptr ptr(new PinnedText(text, x, y));
            nodes.push_back(ptr);
            return ptr;
        }
    private:
        PinnedText(osgText::Text* text, float x, float y):
            PinnedNode(text, x, y)
        { }
    private:
        void
        moveTo(float x, float y)
        {
            osgText::Text* n = dynamic_cast<osgText::Text*>(_node.get());
            if (n)
                n->setPosition(osg::Vec3f(x, y, 0.0f));
        }
    };

    ////////////////////
    // class PinnedGroup
    ////////////////////
    class PinnedGroup:
        public PinnedNode
    {
    public:
        typedef std::shared_ptr<PinnedGroup> Ptr;
    public:
        static inline
        Ptr
        create(Vec& nodes, osg::MatrixTransform* xfm, float x=0.0f, float y=0.0f)
        {
            Ptr ptr(new PinnedGroup(xfm, x, y));
            nodes.push_back(ptr);
            return ptr;
        }
    private:
        PinnedGroup(osg::MatrixTransform* xfm, float x, float y):
            PinnedNode(xfm, x, y)
        { }
    private:
        void
        moveTo(float x, float y)
        {
            osg::MatrixTransform* n = dynamic_cast<osg::MatrixTransform*>(_node.get());
                n->setMatrix(osg::Matrixf::translate(x, y, 0.0f));
        }
    };
}
}
}
