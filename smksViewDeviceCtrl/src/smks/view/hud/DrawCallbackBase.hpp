// ========================================================================= //
// Copyright 2016-2018 SUPAMONKS_STUDIO                                      //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
//                                                                           //
// ========================================================================= //
#pragma once

#include <osg/Drawable>
#include <osgText/Text>

#include <smks/ClientBase.hpp>
#include <smks/view/AbstractDevice.hpp>

namespace smks { namespace view { namespace hud
{
    class DrawCallbackBase:
        public osg::Drawable::DrawCallback
    {
    private:
        float                   _maxUpdateDelta;  // in milliseconds
        mutable osg::Timer_t    _lastUpdateTime;

    protected:
        explicit
        DrawCallbackBase(float maxUpdateDelta):
            _maxUpdateDelta(maxUpdateDelta),
            _lastUpdateTime(0)
        { }

    public:
        virtual inline
        void
        drawImplementation(osg::RenderInfo&     renderInfo,
                           const osg::Drawable* drawable) const
        {
            osg::Timer_t    tick    = osg::Timer::instance()->tick();
            osgText::Text*  label   = const_cast<osgText::Text*>(
                dynamic_cast<const osgText::Text*>(drawable));

            if (label)
            {
                const double delta = osg::Timer::instance()->delta_m(_lastUpdateTime, tick);
                if (delta > _maxUpdateDelta)
                {
                    AbstractDevice* device = dynamic_cast<AbstractDevice*>(renderInfo.getView());
                    if (device)
                    {
                        std::string text;

                        getLabelText(*device, text);
                        label->setText(text);
                        label->dirtyGLObjects();

                        _lastUpdateTime = osg::Timer::instance()->tick();
                    }
                }
            }
            drawable->drawImplementation(renderInfo);
        }

    protected:
        virtual
        void
        getLabelText(const AbstractDevice&, std::string&) const = 0;
    };
}
}
}
