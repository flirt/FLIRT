// ========================================================================= //
// Copyright 2016-2018 SUPAMONKS_STUDIO                                      //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
//                                                                           //
// ========================================================================= //
#pragma once

#include <sstream>
#include <smks/parm/BuiltIns.hpp>
#include <smks/parm/BuiltIn.hpp>
#include <smks/util/osg/callback.hpp>

#include "smks/view/Animation.hpp"
#include "CullCallbackBase.hpp"
#include "DrawCallbackBase.hpp"

namespace smks { namespace view { namespace hud
{
    class AnimationCullCallback:
        public CullCallbackBase
    {
    protected:
        virtual inline
        bool
        cull(const AbstractDevice& device) const
        {
            return false;
        }
    };

    class AnimationDrawCallback:
        public DrawCallbackBase
    {
    private:
        mutable osg::observer_ptr<Animation> _anim;
    protected:
        virtual inline
        void
        getLabelText(const AbstractDevice& device, std::string& ret) const
        {
            ret.clear();

            Animation::Ptr anim;
            if (!_anim.lock(anim))
            {
                _anim = const_cast<Animation*>(
                    util::osg::getUpdateCallback<Animation>(
                        device.clientRoot()));
                return;
            }

            ClientBase::Ptr const& client = device.client().lock();
            if (!client)
                return;

            math::Vector2f  sceneTimeBounds (FLT_MAX, -FLT_MAX);
            float           currentTime     = 0.0f;
            float           framerate       = 0.0f;

            if (!client->getNodeParameter<math::Vector2f>(parm::timeBounds(), sceneTimeBounds, client->sceneId()) ||
                !client->getNodeParameter<float>(parm::currentTime(), currentTime, client->sceneId()) ||
                !client->getNodeParameter<float>(parm::framerate(), framerate, client->sceneId()) ||
                sceneTimeBounds.x() > sceneTimeBounds.y())
                return;

            const int minFrame  = static_cast<int>(floorf(sceneTimeBounds.x() * framerate));
            const int maxFrame  = static_cast<int>(floorf(sceneTimeBounds.y() * framerate));
            const int frame     = static_cast<int>(floorf(currentTime * framerate));

            std::stringstream sstr;

            switch(anim->mode())
            {
            case ANIM_FREE_FPS:
                sstr << "Every frame";
                break;

            case ANIM_CONST_FPS:
            default:
                sstr << "Real-time (" << framerate << " fps)";
                break;
            }
            sstr << "(" << framerate << " fps)";

            sstr << ":    " << minFrame << " / " << frame << " / " << maxFrame;
            if (anim->playing())
                sstr << "    [" << (anim->direction() == PLAY_FORWARD ? ">" : "<") << "]";
            ret = sstr.str();
        }
    public:
        explicit
        AnimationDrawCallback(float maxUpdateDelta):
            DrawCallbackBase(maxUpdateDelta),
            _anim()
        { }
    };
}
}
}
