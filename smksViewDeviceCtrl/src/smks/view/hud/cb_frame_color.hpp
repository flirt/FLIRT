// ========================================================================= //
// Copyright 2016-2018 SUPAMONKS_STUDIO                                      //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
//                                                                           //
// ========================================================================= //
#pragma once

#include <sstream>

#include <smks/xchg/Data.hpp>
#include <smks/parm/BuiltIn.hpp>
#include <smks/parm/BuiltIns.hpp>
#include <smks/util/osg/callback.hpp>

#include "CullCallbackBase.hpp"
#include "DrawCallbackBase.hpp"

namespace smks { namespace view { namespace hud
{
    class FrameColorCullCallback:
        public CullCallbackBase
    {
    protected:
        virtual inline
        bool
        cull(const AbstractDevice& device) const
        {
            return device.displayedFrameBuffer() == 0;
        }
    };

    class FrameColorDrawCallback:
        public DrawCallbackBase
    {
    private:
        /*!
        *  Private event handler used to track color below the mouse cursor.
        */
        class Handler:
            public osgGA::GUIEventHandler
        {
        private:
            std::string _text;
        public:
            virtual inline
            bool
            handle(const osgGA::GUIEventAdapter&    ea,
                   osgGA::GUIActionAdapter&         aa)
            {
                _text.clear();

                AbstractDevice*                 device  = dynamic_cast<AbstractDevice*>(&aa);
                ClientBase::Ptr const&  client  = device ? device->client().lock() : nullptr;
                if (!client)
                    return false;

                const unsigned int frameBuffer = device->displayedFrameBuffer();
                if (frameBuffer == 0)
                    return false;

                size_t          frameWidth  = 0;
                size_t          frameHeight = 0;
                xchg::Data::Ptr frameData   = nullptr;

                if (client->getNodeParameter<size_t>            (parm::width(),     frameWidth,     frameBuffer) &&
                    client->getNodeParameter<size_t>            (parm::height(),    frameHeight,    frameBuffer) &&
                    client->getNodeParameter<xchg::Data::Ptr>   (parm::frameData(), frameData,      frameBuffer) &&
                    frameData)
                {
                    float x = ea.getXnormalized();  // in [-1, 1]
                    float y = ea.getYnormalized();

                    int px = static_cast<int>((x + 1.0f) * 0.5f * static_cast<float>(frameWidth));
                    int py = static_cast<int>((y + 1.0f) * 0.5f * static_cast<float>(frameHeight));

                    px = std::max<int>(0, std::min<int>(frameWidth - 1, px));
                    py = std::max<int>(0, std::min<int>(frameHeight - 1, py));

                    const int idx = (px + frameWidth * py) << 2;
                    frameData->get<float>(idx);

                    std::stringstream str;
                    str.precision(3);
                    str
                        << "Pixel: " << px << ", " << py << " | "
                        << "RGBA " << frameData->get<float>(idx)
                        << " " << frameData->get<float>(idx + 1)
                        << " " << frameData->get<float>(idx + 2)
                        << " " << frameData->get<float>(idx + 3);
                    _text = str.str();
                }
                return false; // never flags the event as handled
            }

            const std::string&
            text() const
            {
                return _text;
            }

            Handler():
                osgGA::GUIEventHandler(),
                _text()
            { }
        };

    private:
        mutable osg::ref_ptr<Handler>   _handler;
        mutable AbstractDevice*                 _device;    // does not own memory
    protected:
        virtual inline
        void
        getLabelText(const AbstractDevice& device, std::string& ret) const
        {
            ret.clear();

            if (!_handler)
            {
                _device     = const_cast<AbstractDevice*>(&device); // ugly
                _handler    = new Handler();
                _device->addEventHandler(_handler);
                return;
            }
            ret = _handler->text();
        }
    public:
        explicit
        FrameColorDrawCallback(float maxUpdateDelta):
            DrawCallbackBase(maxUpdateDelta),
            _handler(nullptr),
            _device (nullptr)
        { }

        ~FrameColorDrawCallback()
        {
            if (_handler && _device)
                _device->removeEventHandler(_handler);
            _handler    = nullptr;
            _device     = nullptr;
        }
    };
}
}
}
