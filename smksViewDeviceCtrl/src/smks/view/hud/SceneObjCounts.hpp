// ========================================================================= //
// Copyright 2016-2018 SUPAMONKS_STUDIO                                      //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
//                                                                           //
// ========================================================================= //
#pragma once

#include <memory>
#include <osg/MatrixTransform>
#include <smks/xchg/IdSet.hpp>

namespace smks
{
    class ClientBase;
    namespace xchg
    {
        class ClientEvent;
        class SceneEvent;
        class ParmEvent;
    }

    namespace view
    {
        namespace hud
        {
            class SceneObjCounts:
                public osg::MatrixTransform
            {
            public:
                typedef osg::ref_ptr<SceneObjCounts> Ptr;
            private:
                struct NodeTypeInfo;
                class ClientObserver;
                class ParmObserver;
                class UpdateCallback;
            private:
                typedef std::weak_ptr<ClientBase>       ClientWPtr;
                typedef std::shared_ptr<ClientObserver> ClientObserverPtr;
                typedef std::shared_ptr<ParmObserver>   ParmObserverPtr;
                typedef osg::ref_ptr<osg::Geode>        GeodePtr;
                typedef osg::ref_ptr<osgText::Text>     TextPtr;
            private:
                NodeTypeInfo*       _infoPerType;   // indexed by supported node type
                TextPtr             _labelType;
                TextPtr*            _labelCounts;
                GeodePtr            _geode;

                ClientWPtr          _client;
                ClientObserverPtr   _clientObs;
                ParmObserverPtr     _parmObs;

                mutable bool        _dirty;

            public:
                static inline
                Ptr
                create(ClientWPtr const&    client,
                       const char*          osgName="")
                {
                    Ptr ptr(new SceneObjCounts(client, osgName));
                    return ptr;
                }
            protected:
                SceneObjCounts(ClientWPtr const&, const char*);
            private:
                // non-copyable
                SceneObjCounts(const SceneObjCounts&);
                SceneObjCounts& operator=(const SceneObjCounts&);
            public:
                ~SceneObjCounts();
            private:
                void
                handle(ClientBase&, const xchg::ClientEvent&);
                void
                handle(ClientBase&, const xchg::SceneEvent&);
                void
                handleSceneParmEvent(ClientBase&, const xchg::ParmEvent&);
                void
                handle(ClientBase&, unsigned int, const xchg::ParmEvent&);

                void
                update();

                void
                handleSceneSetOrUnset(ClientBase&, bool unset);
                void
                handleNodeAddedOrDeleted(ClientBase&, unsigned int, bool deleted);
                bool    //<! returns true if node is taken into account
                handleNodeStatusChanged(const ClientBase&, unsigned int, bool deleted);

                void
                getTrackedNodes(xchg::IdSet&) const;
            };
        }
    }
}
