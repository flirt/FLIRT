// ========================================================================= //
// Copyright 2016-2018 SUPAMONKS_STUDIO                                      //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
//                                                                           //
// ========================================================================= //
#pragma once

#include <osg/MatrixTransform>

namespace smks
{
    namespace sys
    {
        class ResourceDatabase;
    }

    namespace view { namespace hud
    {
        //<! class used to easily create box around given HUD elements.
        class TextPanel:
            public osg::MatrixTransform
        {
        public:
            typedef osg::ref_ptr<TextPanel> Ptr;
        private:
            typedef std::shared_ptr<sys::ResourceDatabase>  ResourcesPtr;
            typedef osg::ref_ptr<osg::MatrixTransform>      MatrixTransformPtr;
            typedef osg::ref_ptr<osg::Geode>                GeodePtr;
        private:
            GeodePtr            _foregroundGeode;
            MatrixTransformPtr  _foreground;
            MatrixTransformPtr  _background;
        public:
            static
            Ptr
            create(ResourcesPtr const& resources,
                   const char* name)
            {
                Ptr ptr(new TextPanel(resources, "", name));
                return ptr;
            }

        protected:
            TextPanel(ResourcesPtr const&,
                      const std::string& title,
                      const std::string& name);
        public:
            virtual
            bool
            addChild(Node* child)
            {
                if (_foregroundGeode->addChild(child))
                {
                    updateMatrices();
                    return true;
                }
                return false;
            }
            virtual
            bool
            removeChild(Node* child)
            {
                if (_foregroundGeode->removeChild(child))
                {
                    updateMatrices();
                    return true;
                }
                return false;
            }
        private:
            bool
            _addChild(Node* child)
            {
                return osg::MatrixTransform::addChild(child);
            }
            bool
            _removeChild(Node* child)
            {
                return osg::MatrixTransform::removeChild(child);
            }

            void
            updateMatrices();
        };
    }
}
}
