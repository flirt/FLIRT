// ========================================================================= //
// Copyright 2016-2018 SUPAMONKS_STUDIO                                      //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
//                                                                           //
// ========================================================================= //
#pragma once

#include <iomanip>
#include <sstream>

#include <smks/parm/BuiltIns.hpp>
#include <smks/parm/BuiltIn.hpp>
#include <smks/util/osg/callback.hpp>

#include "CullCallbackBase.hpp"
#include "DrawCallbackBase.hpp"

namespace smks { namespace view { namespace hud
{
    class CurrentCameraCullCallback:
        public CullCallbackBase
    {
    protected:
        virtual inline
        bool
        cull(const AbstractDevice& device) const
        {
            return device.displayedCamera() == 0;
        }
    public:
        CurrentCameraCullCallback()
        { }
    };

    class CurrentCameraDrawCallback:
        public DrawCallbackBase
    {
    protected:
        virtual inline
        void
        getLabelText(const AbstractDevice& device, std::string& ret) const
        {
            ret.clear();

            ClientBase::Ptr const& client = device.client().lock();
            if (!client)
                return;

            const unsigned int camera = device.displayedCamera();
            if (camera == 0)
                return;

            std::stringstream   sstr;
            float               xAspectRatio = 1.0f;

            sstr.precision(3);
            sstr << "CAM: " << client->getNodeName(camera);
            if (client->getNodeParameter<float>(parm::xAspectRatio(), xAspectRatio, camera))
            {
                sstr << ", ratio: " << xAspectRatio;

                osgViewer::ViewerBase::Windows windows;

                const_cast<AbstractDevice*>(&device)->getWindows(windows);  // ugly
                if (windows.size() == 1)
                {
                    int winx, winy, winWidth, winHeight;

                    windows.front()->getWindowRectangle(winx, winy, winWidth, winHeight);
                    const float winRatio = winHeight > 0
                        ? static_cast<float>(winWidth) / static_cast<float>(winHeight)
                        : 0.0f;

                    if (winRatio > 0.0f &&
                        fabsf(winRatio - xAspectRatio))
                        sstr << " [window ratio: " << winRatio << "]";
                }
            }
            ret = sstr.str();
        }
    public:
        explicit
        CurrentCameraDrawCallback(float maxUpdateDelta):
            DrawCallbackBase(maxUpdateDelta)
        { }
    };
}
}
}
