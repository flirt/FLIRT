// ========================================================================= //
// Copyright 2016-2018 SUPAMONKS_STUDIO                                      //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
//                                                                           //
// ========================================================================= //
#pragma once

#include <sstream>
#include <smks/parm/BuiltIns.hpp>
#include <smks/parm/BuiltIn.hpp>
#include <smks/util/osg/callback.hpp>

#include "CullCallbackBase.hpp"
#include "DrawCallbackBase.hpp"
#include "smks/view/FrameBufferSwitch.hpp"

namespace smks { namespace view { namespace hud
{
    class ProgressiveCullCallback:
        public CullCallbackBase
    {
    protected:
        virtual inline
        bool
        cull(const AbstractDevice& device) const
        {
            return false;
        }
    };

    class ProgressiveDrawCallback:
        public DrawCallbackBase
    {
    private:
        mutable osg::observer_ptr<FrameBufferSwitch> _fbufferSwitch;
    protected:
        virtual inline
        void
        getLabelText(const AbstractDevice& device, std::string& ret) const
        {
            ret.clear();

            FrameBufferSwitch::Ptr fbufferSwitch;
            if (!_fbufferSwitch.lock(fbufferSwitch))
            {
                _fbufferSwitch = const_cast<FrameBufferSwitch*>(
                    util::osg::getEventHandler<FrameBufferSwitch>(
                        &device));
                return;
            }

            if (fbufferSwitch->inProgressiveRenderingMode())
                ret = "Progressive rendering";
        }
    public:
        explicit
        ProgressiveDrawCallback(float maxUpdateDelta):
            DrawCallbackBase(maxUpdateDelta),
            _fbufferSwitch()
        { }
    };
}
}
}
