// ========================================================================= //
// Copyright 2016-2018 SUPAMONKS_STUDIO                                      //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
//                                                                           //
// ========================================================================= //
#pragma once

#include <iomanip>
#include <sstream>

#include <smks/parm/BuiltIns.hpp>
#include <smks/parm/BuiltIn.hpp>
#include <smks/util/osg/callback.hpp>

#include "CullCallbackBase.hpp"
#include "DrawCallbackBase.hpp"

namespace smks { namespace view { namespace hud
{
    class CurrentFrameBufferCullCallback:
        public CullCallbackBase
    {
    protected:
        virtual inline
        bool
        cull(const AbstractDevice& device) const
        {
            return device.displayedFrameBuffer() == 0;
        }
    };

    class CurrentFrameBufferDrawCallback:
        public DrawCallbackBase
    {
    protected:
        virtual inline
        void
        getLabelText(const AbstractDevice& device, std::string& ret) const
        {
            ret.clear();

            ClientBase::Ptr const& client = device.client().lock();
            if (!client)
                return;

            const unsigned int frameBuffer = device.displayedFrameBuffer();
            if (frameBuffer == 0)
                return;

            size_t              frameWidth      = 0;
            size_t              frameHeight     = 0;
            bool                isProgressive   = false;
            std::stringstream   sstr;

            sstr.precision(3);
            sstr << "FRM: " << client->getNodeName(frameBuffer);
            if (client->getNodeParameter<size_t>(parm::width(), frameWidth, frameBuffer) &&
                client->getNodeParameter<size_t>(parm::height(), frameHeight, frameBuffer))
            {
                sstr << ", " << frameWidth << " x " << frameHeight;
                if (frameHeight > 0)
                    sstr
                        << ", ratio:"
                        << static_cast<float>(frameWidth) / static_cast<float>(frameHeight);
            }
            ret = sstr.str();
        }
    public:
        explicit
        CurrentFrameBufferDrawCallback(float maxUpdateDelta):
            DrawCallbackBase(maxUpdateDelta)
        { }
    };
}
}
}
