// ========================================================================= //
// Copyright 2016-2018 SUPAMONKS_STUDIO                                      //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
//                                                                           //
// ========================================================================= //
#include <smks/sys/Log.hpp>

#include <smks/xchg/ClientEvent.hpp>
#include <smks/xchg/SceneEvent.hpp>
#include <smks/xchg/ParmEvent.hpp>
#include <smks/parm/BuiltIns.hpp>

#include <smks/ClientBase.hpp>
#include <smks/ClientBindingBase.hpp>
#include <smks/client_node_filters.hpp>

#include <std/to_string_xchg.hpp>

#include "NodeSwitch.hpp"

namespace smks { namespace view
{
    class NodeSwitch::ClientObserver:
        public AbstractClientObserver
    {
    public:
        typedef std::shared_ptr<ClientObserver> Ptr;
    private:
        NodeSwitch& _self;
    public:
        static inline
        Ptr
        create(NodeSwitch& self)
        {
            Ptr ptr(new ClientObserver(self));
            return ptr;
        }
    protected:
        explicit
        ClientObserver(NodeSwitch& self):
            _self(self)
        { }

        inline
        void
        handle(ClientBase& client, const xchg::ClientEvent& e)
        {
            _self.handle(client, e);
        }

        inline
        void
        handle(ClientBase& client, const xchg::SceneEvent& e)
        {
            _self.handle(client, e);
        }

        inline
        void
        handleSceneParmEvent(ClientBase& client, const xchg::ParmEvent& e)
        {
            _self.handleSceneParmEvent(client, e);
        }
    };
}
}

using namespace smks;

// static
view::NodeSwitch::Ptr
view::NodeSwitch::create(ClientBase::Ptr const& client,
                         xchg::NodeClass        nodeClass,
                         char                   nodeInitialization,
                         bool                   emptySlotAllowed)
{
    Ptr ptr(new NodeSwitch(
        nodeClass,
        nodeInitialization,
        emptySlotAllowed));

    ptr->build(client);
    return ptr;
}

// static
void
view::NodeSwitch::destroy(ClientBase::Ptr const&    client,
                          Ptr&                      ptr)
{
    if (ptr)
        ptr->dispose(client);
    ptr = nullptr;
}

view::NodeSwitch::NodeSwitch(xchg::NodeClass    nodeClass,
                             char               nodeInitialization,
                             bool               emptySlotAllowed):
    _nodeClass          (nodeClass),
    _nodeInitialization (nodeInitialization),
    _emptySlotAllowed   (emptySlotAllowed),
    //---
    _clientObserver     (ClientObserver::create(*this)),
    _nodeIds            (),
    _defaultId          (emptySlotAllowed ? 0 : INVALID_IDX),
    _currentIndex       (INVALID_IDX)
{ }

// virtual
void
view::NodeSwitch::build(ClientBase::Ptr const& client)
{
    if (client)
    {
        client->registerObserver(_clientObserver);
        handleSceneSet(*client, client->sceneId());
    }
}

// virtual
void
view::NodeSwitch::dispose(ClientBase::Ptr const& client)
{
    if (client)
    {
        handleSceneUnset(*client, client->sceneId());
        client->deregisterObserver(_clientObserver);
    }
    _clientObserver = nullptr;
}

void
view::NodeSwitch::next(ClientBase& client)
{
    currentIndex(
        client,
        !_nodeIds.empty()
            ? (currentIndex() + 1) % _nodeIds.size()
            : INVALID_IDX);
}

size_t
view::NodeSwitch::currentIndex() const
{
    return _currentIndex;
}

void
view::NodeSwitch::currentIndex(ClientBase& client, size_t value)
{
    {
        // if needed, run current node changed handler
        const unsigned int curNode = currentNodeId();
        const unsigned int nxtNode = value < _nodeIds.size()
            ? _nodeIds[value]
            : 0;
        if (nxtNode != curNode)
            handleCurrentNodeAboutToChange(client, nxtNode);
    }
    _currentIndex = value;
}

unsigned int
view::NodeSwitch::currentNodeId() const
{
    return currentIndex() < _nodeIds.size()
        ? _nodeIds[currentIndex()]
        : 0;
}

size_t
view::NodeSwitch::getNodeIndex(unsigned int id) const
{
    for (size_t i = 0; i < _nodeIds.size(); ++i)
        if (_nodeIds[i] == id)
            return i;
    return INVALID_IDX;
}

// virtual
void
view::NodeSwitch::addNodeId(ClientBase& client, unsigned int id)
{
    if (id == 0 &&
        !_emptySlotAllowed)
        return;

    for (size_t i = 0; i < _nodeIds.size(); ++i)
        if (_nodeIds[i] == id)
            return;
    _nodeIds.push_back(id);

    if (currentIndex() == INVALID_IDX)
        currentIndex (
            client,
            getNodeIndex(_defaultId));
    if (currentIndex() == INVALID_IDX)
        currentIndex (
            client,
            _nodeIds.size() - 1);

#if defined(FLIRT_LOG_ENABLED)
    {
        if (id > 0)
            LOG(DEBUG)
                << "node ID = " << id
                << " '" << client.getNodeName(id) << "' "
                << "(" << std::to_string(client.getNodeClass(id)) << ")"
                << " added to switch (0x" << static_cast<void*>(this) << ").";
    }
#endif // defined(FLIRT_LOG_ENABLED)
}

// virtual
void
view::NodeSwitch::removeNodeId(ClientBase& client, unsigned int id)
{
    for (size_t i = 0; i < _nodeIds.size(); ++i)
        if (_nodeIds[i] == id)
        {
            if (currentIndex() == i)
                currentIndex (
                    client,
                    getNodeIndex(_defaultId));
            //-------------------------------------
            std::swap(_nodeIds[i], _nodeIds.back());
            _nodeIds.pop_back();
            //-------------------------------------
            if (currentIndex() == INVALID_IDX)
                currentIndex (
                    client,
                    !_nodeIds.empty()
                        ? _nodeIds.size() - 1
                        : INVALID_IDX);

#if defined(FLIRT_LOG_ENABLED)
            {
                if (id > 0)
                    LOG(DEBUG)
                        << "node ID = " << id
                        << " '" << client.getNodeName(id) << "' "
                        << "(" << std::to_string(client.getNodeClass(id)) << ")"
                        << " removed from switch (0x" << static_cast<void*>(this) << ").";
            }
#endif // defined(FLIRT_LOG_ENABLED)
            return;
        }
}

// virtual
void
view::NodeSwitch::handle(ClientBase&                client,
                         const xchg::ClientEvent&   evt)
{
    if (evt.type() == xchg::ClientEvent::SCENE_SET)
        handleSceneSet(client, evt.scene());
    else if (evt.type() == xchg::ClientEvent::SCENE_UNSET)
        handleSceneUnset(client, evt.scene());
}

// virtual
void
view::NodeSwitch::handleSceneSet(ClientBase&    client,
                                 unsigned int   sceneId)
{
    handleSceneUnset(client, sceneId);

    assert(_nodeIds.empty());
    assert(currentIndex() == INVALID_IDX);
    if (sceneId > 0)
    {
        // traverse scene in order to find and store existing (and initialized) camera nodes' IDs
        xchg::IdSet                     ids;
        CompoundClientNodeFilter::Ptr   filter = CompoundClientNodeFilter::create();

        filter->add(FilterClientNodeByClass::create(_nodeClass));
        filter->add(FilterClientNodeByBuiltIn<char>::create(parm::initialization(), _nodeInitialization));
        client.getIds(ids, false, filter);
        _nodeIds.reserve(ids.size());
        for (xchg::IdSet::const_iterator id = ids.begin(); id != ids.end(); ++id)
            addNodeId(client, *id);

        if (_emptySlotAllowed)
            addNodeId(client, 0); // default node id
    }
}

void
view::NodeSwitch::handleSceneUnset(ClientBase&  client,
                                   unsigned int sceneId)
{
    currentIndex (client, INVALID_IDX);
    while (!_nodeIds.empty())
        removeNodeId(client, _nodeIds.front());
}

// virtual
void
view::NodeSwitch::handle(ClientBase&                client,
                         const xchg::SceneEvent&    evt)
{
    if (evt.type() != xchg::SceneEvent::NODE_CREATED &&
        evt.type() != xchg::SceneEvent::NODE_DELETED)
        return;

    assert(evt.type() == xchg::SceneEvent::NODE_CREATED ||
        evt.type() == xchg::SceneEvent::NODE_DELETED);

    char            state       = xchg::INACTIVE;
    xchg::NodeClass nodeClass   = client.getNodeClass(evt.node());

    if ((_nodeClass & nodeClass) != 0 &&
        (~_nodeClass & nodeClass) == 0)
    {
        if (evt.type() == xchg::SceneEvent::NODE_CREATED)
        {
            client.getNodeParameter<char>(parm::initialization(), state, evt.node());
            if (state == _nodeInitialization)
                addNodeId(client, evt.node());
        }
        else if (evt.type() == xchg::SceneEvent::NODE_DELETED)
            removeNodeId(client, evt.node());
    }
}

void
view::NodeSwitch::handleSceneParmEvent(ClientBase&              client,
                                       const xchg::ParmEvent&   evt)
{
    if (evt.parm() != parm::initialization().id() ||
        evt.comesFromLink() ||
        evt.comesFromAncestor())
        return;

    // node below scene has reported its change in term of initialization status
    xchg::NodeClass nodeClass = client.getNodeClass(evt.node());
    if ((_nodeClass & nodeClass) != 0 &&
        (~_nodeClass & nodeClass) == 0)
    {
        char state = xchg::INACTIVE;

        client.getNodeParameter<char>(parm::initialization(), state, evt.node());
        if (state == _nodeInitialization)
            addNodeId(client, evt.node());
        else
            removeNodeId(client, evt.node());
    }
}

