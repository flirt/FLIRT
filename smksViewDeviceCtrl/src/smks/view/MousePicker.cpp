// ========================================================================= //
// Copyright 2016-2018 SUPAMONKS_STUDIO                                      //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
//                                                                           //
// ========================================================================= //
#include <smks/view/AbstractDevice.hpp>
#include "smks/view/MousePicker.hpp"

using namespace smks;

// explicit
view::MousePicker::MousePicker():
    osgGA::GUIEventHandler()
{ }

bool
view::MousePicker::handle(const osgGA::GUIEventAdapter& ea,
                          osgGA::GUIActionAdapter&      aa)
{
    if (ea.getHandled())
        return false;

    AbstractDevice* device = dynamic_cast<AbstractDevice*>(&aa);
    if (!device)
        return false;

    if (ea.getButton() == osgGA::GUIEventAdapter::LEFT_MOUSE_BUTTON &&
        ea.getEventType() == osgGA::GUIEventAdapter::RELEASE)
    {
        // the texture accesses expect an upwards increasing Y-axis
        const int x = static_cast<int>(ea.getX());
        const int y = ea.getMouseYOrientation() == osgGA::GUIEventAdapter::Y_INCREASING_DOWNWARDS
            ? ea.getWindowHeight() - static_cast<int>(ea.getY())
            : static_cast<int>(ea.getY());

        device->pick(x, y, (ea.getModKeyMask() & osgGA::GUIEventAdapter::MODKEY_SHIFT) != 0);
        return true;
    }

    return false;
}
