// ========================================================================= //
// Copyright 2016-2018 SUPAMONKS_STUDIO                                      //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
//                                                                           //
// ========================================================================= //
#pragma once

#include <smks/util/osg/key.hpp>
#include "smks/view/OrbitCameraHandler.hpp"
#include "smks/sys/configure_view_device_ctrl.hpp"

namespace smks { namespace view
{
    class FLIRT_VIEW_DEVICE_CTRL_EXPORT CameraFocus:
        public OrbitCameraHandler
    {
    public:
        typedef osg::ref_ptr<CameraFocus> Ptr;
    private:
        class PImpl;
    private:
        PImpl* _pImpl;
    public:
        static inline
        Ptr
        create(AbstractDevice*              device,
               const util::osg::KeyControl& keyHome     = util::osg::KeyControl(osgGA::GUIEventAdapter::KEY_Home),
               const util::osg::KeyControl& keyFocus    = util::osg::KeyControl(osgGA::GUIEventAdapter::KEY_F))
        {
            Ptr ptr(new CameraFocus(
                keyHome,
                keyFocus));
            ptr->build(device);
            return ptr;
        }

        static inline
        void
        destroy(AbstractDevice* device,
                Ptr&            ptr)
        {
            if (ptr)
                ptr->dispose(device);
            ptr = nullptr;
        }

    protected:
        CameraFocus(const util::osg::KeyControl&,
                    const util::osg::KeyControl&);
    public:
        ~CameraFocus();

        virtual
        bool
        handle(const osgGA::GUIEventAdapter&,
               osgGA::GUIActionAdapter&);

        virtual
        void
        getUsage(osg::ApplicationUsage&) const;
    };
}
}
