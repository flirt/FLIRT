// ========================================================================= //
// Copyright 2016-2018 SUPAMONKS_STUDIO                                      //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
//                                                                           //
// ========================================================================= //
#pragma once

#include <memory>
#include <osg/NodeCallback>
#include "smks/sys/configure_view_device_ctrl.hpp"

namespace smks
{
    class ClientBase;
    namespace view
    {
        /////////////////////
        // enum AnimationMode
        /////////////////////
        enum AnimationMode
        {
            ANIM_FREE_FPS = 0,
            ANIM_CONST_FPS
        };
        ////////////////////////
        // enum PlayingDirection
        ////////////////////////
        enum PlayingDirection
        {
            PLAY_FORWARD = 0,
            PLAY_BACKWARD
        };


        class FLIRT_VIEW_DEVICE_CTRL_EXPORT Animation:
            public osg::NodeCallback
        {
        public:
            typedef osg::ref_ptr<Animation> Ptr;
        public:
            ///////////////////////////////////////
            // class Animation::AbstractLoopHandler
            ///////////////////////////////////////
            class AbstractLoopHandler
            {
            public:
                typedef std::shared_ptr<AbstractLoopHandler> Ptr;
            public:
                virtual
                ~AbstractLoopHandler()
                { }

                virtual
                void
                handle(Animation&, ClientBase&) = 0;
            };
            //////////////////////////////////////
        private:
            class PImpl;
        private:
            typedef std::shared_ptr<ClientBase>             ClientPtr;
            typedef std::vector<AbstractLoopHandler::Ptr>   LoopHandlers;
        private:
            PImpl* _pImpl;

        public:
            static inline
            Ptr
            create(ClientPtr const& client)
            {
                Ptr ptr(new Animation(client));
                ptr->build(client, ptr);
                return ptr;
            }

            static inline
            void
            destroy(ClientPtr const& client, Ptr& ptr)
            {
                if (ptr)
                    ptr->dispose(client);
                ptr = nullptr;
            }
        protected:
            explicit
            Animation(ClientPtr const&);

            virtual
            void
            build(ClientPtr const&, Ptr const&);

            virtual
            void
            dispose(ClientPtr const&);
        private:
            // non-copyable
            Animation(const Animation&);
            Animation& operator=(const Animation&);
        public:
            ~Animation();

            virtual
            void
            operator()(osg::Node*, osg::NodeVisitor*);

            bool
            playing() const;

            void
            play();

            void
            pause();

            void
            start();

            void
            stop();

            void
            goTo(float);

            void
            firstFrame();

            void
            lastFrame();

            void
            previousFrame();

            void
            nextFrame();

            AnimationMode
            mode() const;

            void
            mode(AnimationMode);

            PlayingDirection
            direction() const;

            void
            direction(PlayingDirection);

            void
            registerLoopHandler(AbstractLoopHandler::Ptr const&);

            void
            deregisterLoopHandler(AbstractLoopHandler::Ptr const&);
        };
    }
}
