// ========================================================================= //
// Copyright 2016-2018 SUPAMONKS_STUDIO                                      //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
//                                                                           //
// ========================================================================= //
#pragma once

#include <osgGA/GUIEventHandler>
#include <smks/util/osg/key.hpp>
#include "smks/sys/configure_view_device_ctrl.hpp"

namespace smks { namespace view
{
    class AbstractDevice;

    namespace hud
    {
        class FLIRT_VIEW_DEVICE_CTRL_EXPORT Handler:
            public osgGA::GUIEventHandler
        {
        public:
            typedef osg::ref_ptr<Handler>   Ptr;
        private:
            typedef osg::ref_ptr<AbstractDevice>    DevicePtr;
        private:
            class PImpl;
        private:
            PImpl* _pImpl;
        public:
            static inline
            Ptr
            create(osg::ApplicationUsage*       usage   = nullptr,
                   const util::osg::KeyControl& keyHelp = util::osg::KeyControl(osgGA::GUIEventAdapter::KEY_H),
                   const util::osg::KeyControl& keyInfo = util::osg::KeyControl(osgGA::GUIEventAdapter::KEY_I))
            {
                Ptr ptr(new Handler(
                    usage,
                    keyHelp,
                    keyInfo));
                return ptr;
            }

            static inline
            void
            destroy(Ptr& ptr)
            {
                if (ptr)
                    ptr->disposeScene();
                ptr = nullptr;
            }

        protected:
            Handler(osg::ApplicationUsage*,
                    const util::osg::KeyControl& keyHelp,
                    const util::osg::KeyControl& keyInfo);

            void
            buildScene(AbstractDevice*);

            void
            disposeScene();
        private:
            // non-copyable
            Handler(const Handler&);
            Handler& operator=(const Handler&);
        public:
            ~Handler();

            osg::ApplicationUsage*
            applicationUsage() const;

            virtual
            void
            applicationUsage(osg::ApplicationUsage*);

            virtual
            bool
            handle(const osgGA::GUIEventAdapter&,
                   osgGA::GUIActionAdapter&);

            void
            showHelp(bool);

            void
            showInfo(bool);

            void
            showTimeline(bool);

            bool
            isHelpVisible() const;

            bool
            isInfoVisible() const;

            bool
            isTimelineVisible() const;

            void
            setNotification(const std::string& = std::string(),
                            float normalizedX=0.5f,
                            float normalizedY=0.5f);

            const osg::Vec2f&
            timelineRangeY() const;

            void
            timelineRangeY(const osg::Vec2f&);
        };
    }
}
}
