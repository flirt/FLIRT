// ========================================================================= //
// Copyright 2016-2018 SUPAMONKS_STUDIO                                      //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
//                                                                           //
// ========================================================================= //
#pragma once

#include <osgGA/GUIEventHandler>
#include "smks/sys/configure_view_device_ctrl.hpp"

namespace osgViewer
{
    class Viewer;
}
namespace smks { namespace view
{
    class Animation;

    class FLIRT_VIEW_DEVICE_CTRL_EXPORT AnimationTimeSwapper:
        public osgGA::GUIEventHandler
    {
    public:
        typedef osg::ref_ptr<AnimationTimeSwapper> Ptr;
    private:
        enum { INVALID_POS = -1 };
    private:
        class PImpl;
    private:
        PImpl* _pImpl;
    public:
        static inline
        Ptr
        create()
        {
            Ptr ptr(new AnimationTimeSwapper());
            return ptr;
        }

        static inline
        void
        destroy(Ptr& ptr)
        {
            ptr = nullptr;
        }

    protected:
        AnimationTimeSwapper();
    private:
        // non-copyable
        AnimationTimeSwapper(const AnimationTimeSwapper&);
        AnimationTimeSwapper& operator=(const AnimationTimeSwapper&);
    public:
        ~AnimationTimeSwapper();

        virtual
        bool
        handle(const osgGA::GUIEventAdapter&,
               osgGA::GUIActionAdapter&);

        virtual
        void
        getUsage(osg::ApplicationUsage&) const;

        int
        detectionMinY() const;

        int
        detectionMaxY() const;

        void
        detectionRangeY(int min, int max);
    };
}
}
