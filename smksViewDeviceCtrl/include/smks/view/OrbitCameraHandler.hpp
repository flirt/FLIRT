// ========================================================================= //
// Copyright 2016-2018 SUPAMONKS_STUDIO                                      //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
//                                                                           //
// ========================================================================= //
#pragma once

#include <osgGA/OrbitManipulator>
#include <smks/util/osg/key.hpp>
#include "smks/sys/configure_view_device_ctrl.hpp"

namespace smks { namespace view
{
    class AbstractDevice;

    class FLIRT_VIEW_DEVICE_CTRL_EXPORT OrbitCameraHandler:
        public osgGA::OrbitManipulator
    {
    public:
        typedef osg::ref_ptr<OrbitCameraHandler> Ptr;
    private:
        class PImpl;
    private:
        PImpl* _pImpl;
    public:
        static inline
        Ptr
        create(AbstractDevice* device)
        {
            Ptr ptr(new OrbitCameraHandler());
            ptr->build(device);
            return ptr;
        }

        static inline
        void
        destroy(AbstractDevice* device,
                Ptr&            ptr)
        {
            if (ptr)
                ptr->dispose(device);
            ptr = nullptr;
        }

    protected:
        OrbitCameraHandler();

        virtual
        void
        build(AbstractDevice*);

        virtual
        void
        dispose(AbstractDevice*);
    private:
        // non-copyable
        OrbitCameraHandler(const OrbitCameraHandler&);
        OrbitCameraHandler& operator=(const OrbitCameraHandler&);
    public:
        ~OrbitCameraHandler();

        virtual
        bool
        handle(const osgGA::GUIEventAdapter&,
               osgGA::GUIActionAdapter&);

        void
        enabled(bool);

        bool
        enabled() const;
    protected:
        virtual
        void
        computeHomePosition(const osg::Camera* camera = nullptr,
                            bool useBoundingBox = false);

        // must not directly set OSG camera's position, but update the scene instead
        virtual
        void
        updateCamera(osg::Camera& camera)
        { }

        virtual
        osg::Matrixd
        getMatrix() const;

        virtual
        osg::Matrixd
        getInverseMatrix() const;

        static
        void
        getSphereOfInterest(osg::BoundingSphere&,
                            const ClientBase&,
                            const xchg::IdSet* selection = nullptr);
    };
}
}
