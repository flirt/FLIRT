// ========================================================================= //
// Copyright 2016-2018 SUPAMONKS_STUDIO                                      //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
//                                                                           //
// ========================================================================= //
#pragma once

#include <osgGA/GUIEventHandler>
#include <smks/util/osg/key.hpp>
#include "smks/sys/configure_view_device_ctrl.hpp"

namespace smks
{
    namespace scn
    {
        class Archive;
    }

    namespace view
    {
        class Animation;

        class FLIRT_VIEW_DEVICE_CTRL_EXPORT AnimationControl:
            public osgGA::GUIEventHandler
        {
        public:
            typedef osg::ref_ptr<AnimationControl>  Ptr;
        private:
            class PImpl;
        public:
            PImpl* _pImpl;
        public:
            static inline
            Ptr
            create(const util::osg::KeyControl& keyPlay         = util::osg::KeyControl(osgGA::GUIEventAdapter::KEY_Space),
                   const util::osg::KeyControl& keyPrevFrame    = util::osg::KeyControl(osgGA::GUIEventAdapter::KEY_Left),
                   const util::osg::KeyControl& keyNextFrame    = util::osg::KeyControl(osgGA::GUIEventAdapter::KEY_Right),
                   const util::osg::KeyControl& keyFirstFrame   = util::osg::KeyControl(osgGA::GUIEventAdapter::MODKEY_ALT, osgGA::GUIEventAdapter::KEY_Left),
                   const util::osg::KeyControl& keyLastFrame    = util::osg::KeyControl(osgGA::GUIEventAdapter::MODKEY_ALT, osgGA::GUIEventAdapter::KEY_Right),
                   const util::osg::KeyControl& keyReverse      = util::osg::KeyControl(osgGA::GUIEventAdapter::KEY_BackSpace),
                   const util::osg::KeyControl& keyChangeMode   = util::osg::KeyControl(osgGA::GUIEventAdapter::KEY_A))
            {
                Ptr ptr(new AnimationControl(
                    keyPlay,
                    keyPrevFrame,
                    keyNextFrame,
                    keyFirstFrame,
                    keyLastFrame,
                    keyReverse,
                    keyChangeMode));
                return ptr;
            }

            static inline
            void
            destroy(Ptr& ptr)
            {
                ptr = nullptr;
            }

        protected:
            AnimationControl(
                const util::osg::KeyControl& keyPlay        = util::osg::KeyControl(osgGA::GUIEventAdapter::KEY_Space),
                const util::osg::KeyControl& keyPrevFrame   = util::osg::KeyControl(osgGA::GUIEventAdapter::KEY_Left),
                const util::osg::KeyControl& keyNextFrame   = util::osg::KeyControl(osgGA::GUIEventAdapter::KEY_Right),
                const util::osg::KeyControl& keyFirstFrame  = util::osg::KeyControl(osgGA::GUIEventAdapter::MODKEY_ALT, osgGA::GUIEventAdapter::KEY_Left),
                const util::osg::KeyControl& keyLastFrame   = util::osg::KeyControl(osgGA::GUIEventAdapter::MODKEY_ALT, osgGA::GUIEventAdapter::KEY_Right),
                const util::osg::KeyControl& keyReverse     = util::osg::KeyControl(osgGA::GUIEventAdapter::KEY_BackSpace),
                const util::osg::KeyControl& keyChangeMode  = util::osg::KeyControl(osgGA::GUIEventAdapter::KEY_A));
        private:
            // non-copyable
            AnimationControl(const AnimationControl&);
            AnimationControl& operator=(const AnimationControl&);
        public:
            ~AnimationControl();

            virtual
            bool
            handle(const osgGA::GUIEventAdapter&, osgGA::GUIActionAdapter&);

            virtual
            void
            getUsage(osg::ApplicationUsage&) const;
        };
    }
}
