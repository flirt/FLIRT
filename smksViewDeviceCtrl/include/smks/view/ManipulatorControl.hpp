// ========================================================================= //
// Copyright 2016-2018 SUPAMONKS_STUDIO                                      //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
//                                                                           //
// ========================================================================= //
#pragma once

#include <osgGA/GUIEventHandler>
#include <smks/util/osg/key.hpp>
#include "smks/sys/configure_view_device_ctrl.hpp"

namespace smks { namespace view
{
    class FLIRT_VIEW_DEVICE_CTRL_EXPORT ManipulatorControl:
        public osgGA::GUIEventHandler
    {
    public:
        typedef osg::ref_ptr<ManipulatorControl> Ptr;
    private:
        class PImpl;
    private:
        PImpl* _pImpl;

    public:
        static inline
        Ptr
        create(const util::osg::KeyControl& keyNone             = util::osg::KeyControl(osgGA::GUIEventAdapter::KEY_Q),
               const util::osg::KeyControl& keyTranslate        = util::osg::KeyControl(osgGA::GUIEventAdapter::KEY_W),
               const util::osg::KeyControl& keyTranslateInWorld = util::osg::KeyControl(osgGA::GUIEventAdapter::MODKEY_ALT, osgGA::GUIEventAdapter::KEY_W),
               const util::osg::KeyControl& keyRotate           = util::osg::KeyControl(osgGA::GUIEventAdapter::KEY_E),
               const util::osg::KeyControl& keyRotateInWorld    = util::osg::KeyControl(osgGA::GUIEventAdapter::MODKEY_ALT, osgGA::GUIEventAdapter::KEY_E),
               const util::osg::KeyControl& keyScale            = util::osg::KeyControl(osgGA::GUIEventAdapter::KEY_R),
               const util::osg::KeyControl& keyScaleInWorld     = util::osg::KeyControl(osgGA::GUIEventAdapter::MODKEY_ALT, osgGA::GUIEventAdapter::KEY_R))
        {
            Ptr ptr(new ManipulatorControl(
                keyNone,
                keyTranslate,
                keyTranslateInWorld,
                keyRotate,
                keyRotateInWorld,
                keyScale,
                keyScaleInWorld));
            return ptr;
        }

        static inline
        void
        destroy(Ptr& ptr)
        {
            ptr = nullptr;
        }

    protected:
        ManipulatorControl(
            const util::osg::KeyControl& keyNone,
            const util::osg::KeyControl& keyTranslate,
            const util::osg::KeyControl& keyTranslateInWorld,
            const util::osg::KeyControl& keyRotate,
            const util::osg::KeyControl& keyRotateInWorld,
            const util::osg::KeyControl& keyScale,
            const util::osg::KeyControl& keyScaleInWorld);
    private:
        // non-copyable
        ManipulatorControl(const ManipulatorControl&);
        ManipulatorControl& operator=(const ManipulatorControl&);
    public:
        ~ManipulatorControl();

        virtual
        bool
        handle(const osgGA::GUIEventAdapter&,
               osgGA::GUIActionAdapter&);

        virtual
        void
        getUsage(osg::ApplicationUsage&) const;
    };
}
}
