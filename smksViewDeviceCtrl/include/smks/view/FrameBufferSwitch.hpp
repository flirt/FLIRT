// ========================================================================= //
// Copyright 2016-2018 SUPAMONKS_STUDIO                                      //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
//                                                                           //
// ========================================================================= //
#pragma once

#include <osgGA/GUIEventHandler>
#include <smks/util/osg/key.hpp>
#include "smks/sys/configure_view_device_ctrl.hpp"

namespace smks
{
    class ClientBase;
    namespace view
    {
        class FLIRT_VIEW_DEVICE_CTRL_EXPORT FrameBufferSwitch:
            public osgGA::GUIEventHandler
        {
        public:
            typedef osg::ref_ptr<FrameBufferSwitch> Ptr;
        private:
            typedef std::shared_ptr<ClientBase> ClientPtr;
        private:
            class PImpl;
        private:
            PImpl* _pImpl;
        public:
            static inline
            Ptr
            create(ClientPtr const&             client,
                   const util::osg::KeyControl& keyNext         = util::osg::KeyControl(osgGA::GUIEventAdapter::KEY_Rightbracket),
                   const util::osg::KeyControl& keyProgressive  = util::osg::KeyControl(osgGA::GUIEventAdapter::KEY_P))
            {
                Ptr ptr(new FrameBufferSwitch(
                    client,
                    keyNext,
                    keyProgressive));
                ptr->build(client);
                return ptr;
            }

            static inline
            void
            destroy(ClientPtr const& client, Ptr& ptr)
            {
                if (ptr)
                    ptr->dispose(client);
                ptr = nullptr;
            }

        protected:
            FrameBufferSwitch(ClientPtr const&,
                              const util::osg::KeyControl&,
                              const util::osg::KeyControl&);

            virtual
            void
            build(ClientPtr const&);

            virtual
            void
            dispose(ClientPtr const&);
        private:
            // non-copyable
            FrameBufferSwitch(const FrameBufferSwitch&);
            FrameBufferSwitch& operator=(const FrameBufferSwitch&);
        public:
            ~FrameBufferSwitch();

            unsigned int
            displayedFrameBuffer() const;

            bool
            inProgressiveRenderingMode() const;

            virtual
            bool
            handle(const osgGA::GUIEventAdapter&,
                   osgGA::GUIActionAdapter&);

            virtual
            void
            getUsage(osg::ApplicationUsage&) const;
        };
    }
}
