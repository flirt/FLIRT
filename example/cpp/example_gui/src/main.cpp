// ========================================================================= //
// Copyright 2016-2018 SUPAMONKS_STUDIO                                      //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
//                                                                           //
// ========================================================================= //
#include <string>

#include <tclap/CmdLine.h>

#include <smks/sys/Log.hpp>
#include <smks/sys/version.hpp>
#include <smks/sys/compile_info.hpp>

#include <smks/sys/initialize_asset.hpp>
#include <smks/sys/initialize_scn.hpp>
#include <smks/sys/initialize_rndr_client.hpp>
#include <smks/sys/initialize_view_client.hpp>
#include <smks/sys/initialize_view_device_ctrl.hpp>

#include <smks/sys/ResourceDatabase.hpp>
#include <smks/sys/ResourcePathFinder.hpp>
#include <smks/sys/delay.hpp>

#include <smks/parm/BuiltIns.hpp>
#include <smks/parm/BuiltIn.hpp>

#include <smks/xchg/cfg.hpp>
#include <smks/xchg/ConsoleProgress.hpp>
#include <smks/xchg/IdSet.hpp>
#include <smks/xchg/NodeCode.hpp>
#include <smks/xchg/NodeCodes.hpp>

#include <smks/scn/Scene.hpp>
#include <smks/scn/Xform.hpp>
#include <smks/scn/Archive.hpp>
#include <smks/scn/TreeNodeFilter.hpp>

#include <smks/rndr/Client.hpp>

#include <smks/view/Client.hpp>
#include <smks/view/AbstractDevice.hpp>
#include <smks/view/Animation.hpp>
#include <smks/view/AnimationControl.hpp>
#include <smks/view/AnimationTimeSwapper.hpp>
#include <smks/view/MousePicker.hpp>
#include <smks/view/CameraFocus.hpp>
#include <smks/view/CameraSwitch.hpp>
#include <smks/view/FrameBufferSwitch.hpp>
#include <smks/view/LightFocus.hpp>
#include <smks/view/hud/Handler.hpp>
#include <smks/view/Renderer.hpp>
#include <smks/view/ManipulatorControl.hpp>
#include <smks/view/SceneObjLoader.hpp>
#if defined(FLIRT_WITH_DEBUG)
#include <smks/sys/initialize_view_debug.hpp>
#include <smks/view/TestSubSceneLoader.hpp>
#include <smks/view/NodeCreator.hpp>
#include <smks/view/ParameterSetter.hpp>
#include <smks/view/MaterialSetter.hpp>
#endif // defined(FLIRT_WITH_DEBUG)

#include <smks/util/string/parse.hpp>
#include <smks/util/string/path.hpp>

#if defined(FLIRT_LOG_ENABLED)
INITIALIZE_NULL_EASYLOGGINGPP;
#endif // defined(FLIRT_LOG_ENABLED)

const std::string   g_windowTitle     = "FLIRT C++ Example";
const std::string   g_viewportDevice  = "SmksViewDevice";
const std::string   g_renderingDevice = "SmksRndrDevice";
const std::string   g_abcfile         = "abc/default.abc";
const std::string   g_logfile         = "flirtpp.log";
const std::string   g_rndrCfg         = "render_config.json";
const unsigned int  g_screenWidth     = 1280;
const unsigned int  g_screenHeight    = 720;
const float         g_framerate       = 25.0f;

//-------------------------------------------------------------------------------------------
// Example description:
//
// This examples creates a scene as well as clients editing it and listening to its updates.
// A scene visualization device is associated with each of these clients:
// Here, an OpenGL-based viewport associated with an instance of the smks::view::Client class;
// and an optional path-tracing renderer associated with an instance of the smks::rndr::Client class.
//
namespace smks
{
    struct Arguments
    {
        std::string abcFile;            // input Alembic geometry cache
        std::string logFile;            // logging output file (only in debug)
        std::string rndrCfg;            // JSon configuration used for rendering settings
        float       screenRatio;        // horizontal screen ratio
        float       framerate;
        bool        autoPlay;           // flag used to immediately start animation
        bool        noAutoLoad;         // disables immediate geometric content loading
        bool        noRendering;        // disables rendering client
        bool        noViewportControl;  // disables C++ based viewport controls
        bool        noAnimCaching;      // disables animation caching
        bool        minimalView;        // disables MRT by viewport and related controls

        Arguments():
            abcFile          (g_abcfile),
            logFile          (g_logfile),
            rndrCfg          (g_rndrCfg),
            screenRatio      (static_cast<float>(g_screenWidth) / static_cast<float>(g_screenHeight)),
            framerate        (g_framerate),
            noAutoLoad       (false),
            autoPlay         (false),
            noRendering      (false),
            noViewportControl(false),
            noAnimCaching    (false),
            minimalView      (false)
        { }
    };

    template<typename Type> static
    std::ostream&
    addConfigOption(std::ostream&, const char*, const Type&);

    template<> static
    std::ostream&
    addConfigOption<bool>(std::ostream&, const char*, const bool&);

    template<> static
    std::ostream&
    addConfigOption<std::string>(std::ostream&, const char*, const std::string&);

    static
    void
    parseArguments(int, char**, Arguments&);

    static
    void
    writeJsonConfiguration(const Arguments&, size_t numThreads, std::string&);

    static
    void
    addIncludePaths(const Arguments&, sys::ResourcePathFinder&);

    static
    void
    getUninitializedSceneObjects(scn::Scene&, xchg::IdSet&);

    static
    void
    initializeAllSceneObjects(scn::Scene&);
}

using namespace smks;

int
main(int argc, char **argv)
{
    int status = 0;

    scn::Scene::Ptr                      scene               = nullptr;

    rndr::Client::Ptr                    rndrClient          = nullptr;
    view::Client::Ptr                    viewClient          = nullptr;

    xchg::AbstractProgressCallbacks::Ptr progress            = nullptr;

    view::AbstractDevice*                viewDevice          = nullptr;
    view::Animation::Ptr                 viewAnimCallback    = nullptr;
    view::AnimationControl::Ptr          viewAnimCtrl        = nullptr;
    view::AnimationTimeSwapper::Ptr      viewAnimTimeSwapper = nullptr;
    view::MousePicker::Ptr               viewMousePicker     = nullptr;
    view::CameraFocus::Ptr               viewCamFocus        = nullptr;
    view::CameraSwitch::Ptr              viewCamSwitch       = nullptr;
    view::FrameBufferSwitch::Ptr         viewFbufferSwitch   = nullptr;
    view::LightFocus::Ptr                viewLightFocus      = nullptr;
    view::hud::Handler::Ptr              viewHudHandler      = nullptr;
    view::Renderer::Ptr                  viewRenderer        = nullptr;
    view::ManipulatorControl::Ptr        viewManipCtrl       = nullptr;
    view::SceneObjLoader::Ptr            viewObjLoader       = nullptr;
#if defined(FLIRT_WITH_DEBUG)
    view::TestSubSceneLoader::Ptr        viewTestLoader      = nullptr;
    view::NodeCreator::Ptr               viewNodeCreator     = nullptr;
    view::ParameterSetter::Ptr           viewParmSetter      = nullptr;
    view::MaterialSetter::Ptr            viewMtlSetter       = nullptr;
#endif // defined(FLIRT_WITH_DEBUG)

    try
    {
        size_t numThreads = 0;
#if defined(_DEBUG)
        numThreads = 1;
#endif // defined(DEBUG)

#if defined(FLIRT_LOG_ENABLED)
        el::Helpers::setStorage(smks::sys::sharedLoggingRepository());
        el::Loggers::reconfigureAllLoggers(el::Level::Global, el::ConfigurationType::ToStandardOutput, "false");
        el::Loggers::reconfigureAllLoggers(el::Level::Global, el::ConfigurationType::Format,           "%datetime [%fbase:%line]: %msg");
        el::Loggers::reconfigureAllLoggers(el::Level::Global, el::ConfigurationType::ToFile,           "false");
#endif // defined(FLIRT_LOG_ENABLED)
        sys::initializeSmksAsset();
        sys::initializeSmksScn(numThreads);
        sys::initializeSmksViewClient();
        sys::initializeSmksRndrClient();
        sys::initializeSmksViewDeviceCtrl();
#if defined(FLIRT_WITH_DEBUG)
        sys::initializeViewDebug();
#endif // defined(FLIRT_WITH_DEBUG)

        smks::Arguments args;

        parseArguments(argc, argv, args);

#if defined(FLIRT_LOG_ENABLED)
        if (!args.logFile.empty())
        {
            el::Loggers::reconfigureAllLoggers(el::Level::Global, el::ConfigurationType::ToFile,   "true");
            el::Loggers::reconfigureAllLoggers(el::Level::Global, el::ConfigurationType::Filename, args.logFile);
            std::cout << "Logging in file '" << args.logFile << "'" << std::endl;
        }
#endif // defined(FLIRT_LOG_ENABLED)

        // log compilation-relative information
        FLIRT_LOG(LOG(DEBUG)
            << "Starts logging for " << g_windowTitle << "\n"
            << "FLIRT_VERSION\t= " << sys::FLIRT_MAJOR << "." << sys::FLIRT_MINOR << "." << sys::FLIRT_TWEAK << "\n"
            << "BUILD_REPO\t= " << sys::BUILD_REPO << "\n"
            << "BUILD_COMMIT\t= " << sys::BUILD_COMMIT << "\n"
            << "BUILD_DATE\t= " << sys::BUILD_DATE << "\n";)

        // write modules' configuration in accordance to user's arguments
        std::string jsonCfg;

        writeJsonConfiguration(args, numThreads, jsonCfg);
        FLIRT_LOG(LOG(DEBUG) << "Configuration\n" << jsonCfg);

        // create scene and main clients (viewport and rendering clients)
        scene      = scn::Scene::create(jsonCfg.c_str());
        viewClient = view::Client::create(jsonCfg.c_str(), nullptr);
        if (!args.noRendering)
            rndrClient = rndr::Client::create(jsonCfg.c_str());

        // create resource database and pathfinder, configure them,
        // and share them between scene and clients
        // BEFORE said clients are associated the scene.
        sys::ResourceDatabase::Ptr const&   resources  = sys::ResourceDatabase::create("resources");
        sys::ResourcePathFinder::Ptr const& pathFinder = sys::ResourcePathFinder::create(argv[0]);

        addIncludePaths(args, *pathFinder);
        resources->setSharedPathFinder(pathFinder);

        scene->setSharedResourceDatabase(resources);
        viewClient->setSharedResourceDatabase(resources);
        if (rndrClient)
            rndrClient->setSharedResourceDatabase(resources);

        // set clients' shared client
        viewClient->setScene(scene);
        if (rndrClient)
            rndrClient->setScene(scene);

        // register console-based progress callbacks on scene
        progress = xchg::ConsoleProgress::create();
        scene->registerProgressCallbacks(progress);

        scene->setParameter<float>(parm::framerate(), args.framerate);

        // create additional event handlers for viewport device
        if (!args.minimalView)
        {
            viewMousePicker     = view::MousePicker::create();
            viewFbufferSwitch   = view::FrameBufferSwitch::create(viewClient);
            viewHudHandler      = view::hud::Handler::create();
        }

        viewDevice = viewClient->device();
        if (!args.noViewportControl)
        {
            viewAnimCallback    = view::Animation::create(viewClient);
            viewAnimCtrl        = view::AnimationControl::create();
            viewAnimTimeSwapper = view::AnimationTimeSwapper::create();
            viewCamFocus        = view::CameraFocus::create(viewDevice);
            viewCamSwitch       = view::CameraSwitch::create(viewClient);
            viewLightFocus      = view::LightFocus::create();
            if (rndrClient)
                viewRenderer    = view::Renderer::create();
            viewManipCtrl       = view::ManipulatorControl::create();
            viewObjLoader       = view::SceneObjLoader::create();

#if defined(FLIRT_WITH_DEBUG)
            viewTestLoader      = view::TestSubSceneLoader::create();
            viewNodeCreator     = view::NodeCreator::create(viewClient);
            viewParmSetter      = view::ParameterSetter::create(viewClient);
            viewMtlSetter       = view::MaterialSetter::create("matte");
#endif // defined(FLIRT_WITH_DEBUG)
        }

        // configure rendering settings
        if (viewRenderer)
            viewRenderer->configurationFile(args.rndrCfg.c_str());

        if (viewAnimTimeSwapper)
            viewAnimTimeSwapper->detectionRangeY(0, 25);
        if (viewHudHandler)
            viewHudHandler->timelineRangeY(osg::Vec2f(0.0f, 25.0f));

        if (viewDevice)
        {
            if (viewAnimCallback)
                viewClient->root()->addUpdateCallback(viewAnimCallback.get());

            if (viewAnimCtrl)
                viewDevice->addEventHandler(viewAnimCtrl.get());
            if (viewAnimTimeSwapper)
                viewDevice->addEventHandler(viewAnimTimeSwapper.get());
            if (viewMousePicker)
                viewDevice->addEventHandler(viewMousePicker.get());
            if (viewCamFocus)
                viewDevice->addEventHandler(viewCamFocus.get());
            if (viewCamSwitch)
                viewDevice->addEventHandler(viewCamSwitch.get());
            if (viewFbufferSwitch)
                viewDevice->addEventHandler(viewFbufferSwitch.get());
            if (viewLightFocus)
                viewDevice->addEventHandler(viewLightFocus.get());
            if (viewHudHandler)
                viewDevice->addEventHandler(viewHudHandler.get());
            if (viewRenderer)
                viewDevice->addEventHandler(viewRenderer.get());
            if (viewManipCtrl)
                viewDevice->addEventHandler(viewManipCtrl.get());
            if (viewObjLoader)
                viewDevice->addEventHandler(viewObjLoader.get());

#if defined(FLIRT_WITH_DEBUG)
            if (viewTestLoader)
                viewDevice->addEventHandler(viewTestLoader.get());
            if (viewNodeCreator)
                viewDevice->addEventHandler(viewNodeCreator.get());
            if (viewParmSetter)
                viewDevice->addEventHandler(viewParmSetter.get());
            if (viewMtlSetter)
                viewDevice->addEventHandler(viewMtlSetter.get());
#endif // defined(FLIRT_WITH_DEBUG)
        }

        // create group directly below scene's root
        // and loads input geometric cache under it
        {
            scn::Xform::Ptr inputGroup = scn::Xform::create("__input__", scene);
            scn::Archive::create(
                !args.abcFile.empty() ? args.abcFile.c_str() : g_abcfile.c_str(),
                inputGroup);
        }

        // initialize all scene objects if requested
        if (!args.noAutoLoad)
            initializeAllSceneObjects(*scene);
        else
        {
            xchg::IdSet nodes;
            getUninitializedSceneObjects(*scene, nodes);
            if (!nodes.empty())
                std::cout
                    << "\nWARNING\n"
                    << "\t" << nodes.size() << " scene object(s) loaded, "
                    << "but not initialized yet."
                    << std::endl;
        }

        // directly runs animation if requested
        if (args.autoPlay &&
            viewAnimCallback)
            viewAnimCallback->play();

        // starts viewport rendering loop
        status = viewDevice
            ? viewDevice->run()
            : 0;

        goto clean_up;
    }
    catch(const sys::Exception& e)
    {
        FLIRT_LOG(LOG(DEBUG) << e.what();)
        std::cerr
            << "\n\n-----"
            << "\nERROR"
            << "\n-----\n"
            << e.what()
            << "\n";
        smks::sys::delay(10000);

        status = 1;
        goto clean_up;
    }

clean_up:

    // remove all viewport's event handlers
    if (viewDevice)
    {
#if defined(FLIRT_WITH_DEBUG)
        if (viewMtlSetter)
            viewDevice->removeEventHandler(viewMtlSetter.get());
        if (viewParmSetter)
            viewDevice->removeEventHandler(viewParmSetter.get());
        if (viewNodeCreator)
            viewDevice->removeEventHandler(viewNodeCreator.get());
        if (viewTestLoader)
            viewDevice->removeEventHandler(viewTestLoader.get());
#endif // defined(FLIRT_WITH_DEBUG)

        if (viewObjLoader)
            viewDevice->removeEventHandler(viewObjLoader.get());
        if (viewManipCtrl)
            viewDevice->removeEventHandler(viewManipCtrl.get());
        if (viewRenderer)
            viewDevice->removeEventHandler(viewRenderer.get());
        if (viewHudHandler)
            viewDevice->removeEventHandler(viewHudHandler.get());
        if (viewLightFocus)
            viewDevice->removeEventHandler(viewLightFocus.get());
        if (viewFbufferSwitch)
            viewDevice->removeEventHandler(viewFbufferSwitch.get());
        if (viewCamSwitch)
            viewDevice->removeEventHandler(viewCamSwitch.get());
        if (viewCamFocus)
            viewDevice->removeEventHandler(viewCamFocus.get());
        if (viewMousePicker)
            viewDevice->removeEventHandler(viewMousePicker.get());
        if (viewAnimTimeSwapper)
            viewDevice->removeEventHandler(viewAnimTimeSwapper.get());
        if (viewAnimCtrl)
            viewDevice->removeEventHandler(viewAnimCtrl.get());

        if (viewAnimCallback)
            viewClient->root()->removeUpdateCallback(viewAnimCallback.get());
    }
    // deallocate memory
#if defined(FLIRT_WITH_DEBUG)
    view::MaterialSetter::destroy(viewMtlSetter);
    view::ParameterSetter::destroy(viewClient, viewParmSetter);
    view::NodeCreator::destroy(viewClient, viewNodeCreator);
    view::TestSubSceneLoader::destroy(viewTestLoader);
#endif // defined(FLIRT_WITH_DEBUG)
    view::SceneObjLoader::destroy(viewObjLoader);
    view::ManipulatorControl::destroy(viewManipCtrl);
    view::Renderer::destroy(viewRenderer);
    view::hud::Handler::destroy(viewHudHandler);
    view::LightFocus::destroy(viewLightFocus);
    view::FrameBufferSwitch::destroy(viewClient, viewFbufferSwitch);
    view::CameraSwitch::destroy(viewClient, viewCamSwitch);
    view::CameraFocus::destroy(viewDevice, viewCamFocus);
    view::MousePicker::destroy(viewMousePicker);
    view::AnimationTimeSwapper::destroy(viewAnimTimeSwapper);
    view::AnimationControl::destroy(viewAnimCtrl);
    view::Animation::destroy(viewClient, viewAnimCallback);

    if (scene)
        scene->deregisterProgressCallbacks(progress);
    progress = nullptr;

    if (rndrClient)
        rndrClient->setScene(nullptr);
    if (viewClient)
        viewClient->setScene(nullptr);

    viewDevice = nullptr;
    view::Client::destroy(viewClient);
    rndr::Client::destroy(rndrClient);

    scn::Scene::destroy(scene);

#if defined(FLIRT_WITH_DEBUG)
    sys::finalizeViewDebug();
#endif // defined(FLIRT_WITH_DEBUG)
    sys::finalizeSmksRndrClient();
    sys::finalizeSmksViewClient();
    sys::finalizeSmksViewDeviceCtrl();
    sys::finalizeSmksScn();
    sys::finalizeSmksAsset();

#if defined(FLIRT_LOG_ENABLED)
    LOG(DEBUG) << "exiting.";
    el::Helpers::setStorage(nullptr);
#endif // defined(FLIRT_LOG_ENABLED)

    return status;
}

// static
void
smks::getUninitializedSceneObjects(scn::Scene&  scene,
                                   xchg::IdSet& nodes)
{
    /////////////////////////////////
    // struct UninitializedNodeFilter
    /////////////////////////////////
    struct UninitializedNodeFilter:
        public scn::AbstractTreeNodeFilter
    {
    public:
        typedef std::shared_ptr<UninitializedNodeFilter> Ptr;
    public:
        static
        Ptr
        create()
        {
            Ptr ptr(new UninitializedNodeFilter());
            return ptr;
        }
    public:
        virtual inline
        bool
        operator()(const scn::TreeNode& node) const
        {
            if (!node.isParameterRelevant(parm::initialization().id()) ||
                !node.isParameterRelevant(parm::userAttribInitialization().id()))
                return false;

            char init = 0, attribInit = 0;
            node.getParameter<char>(parm::initialization(), init);
            node.getParameter<char>(parm::userAttribInitialization(), attribInit);
            return init == xchg::INACTIVE || attribInit == xchg::INACTIVE;
        }
    };

    // apply filter on scene to get nodes' IDs
    scn::Scene::IdGetterOptions opts;

    opts.accumulate(false);
    opts.filter(UninitializedNodeFilter::create());
    scene.getIds(nodes, &opts);
}

// static
void
smks::initializeAllSceneObjects(scn::Scene& scene)
{
    xchg::IdSet nodes;

    // traverse all nodes and manually force initialization of their content
    getUninitializedSceneObjects(scene, nodes);
    for (xchg::IdSet::const_iterator id = nodes.begin(); id != nodes.end(); ++id)
    {
        if (!scene.hasDescendant(*id))
            continue;

        scn::TreeNode::Ptr const& node = scene.descendantById(*id).lock();
        if (node)
        {
            node->setParameter<char>(parm::initialization(), xchg::ACTIVE);
            node->setParameter<char>(parm::userAttribInitialization(), xchg::ACTIVE);
        }
    }
}

// static
void
smks::parseArguments(int          argc,
                     char**       argv,
                     Arguments&   args)
{
    args = Arguments();
    try
    {
        TCLAP::CmdLine cmd(g_windowTitle);

        TCLAP::ValueArg<std::string> abcFile          ("i", "abcFile",     "Alembic cache filename", false, args.abcFile, "string");
        TCLAP::ValueArg<std::string> logFile          ("l", "log",         "Log filename (only in debug)", false, args.logFile, "string");
        TCLAP::ValueArg<std::string> rndrCfg          ("r", "rndrCfg",     "rendering configuration file (in json format)", false, args.rndrCfg, "string");
        TCLAP::ValueArg<std::string> screenRatio      ("s", "screenRatio", "Screen ratio (either as a float or as a resolution)", false, std::to_string(args.screenRatio), "string");
        TCLAP::ValueArg<double>      framerate        ("f", "fps",         "Expected framerate", false, args.framerate, "double");
        TCLAP::SwitchArg             autoPlay         ("p", "autoPlay",    "Immediately plays animation", args.autoPlay);
        TCLAP::SwitchArg             noAutoLoad       ("L", "noAutoLoad",  "Disables immediate geometric content loading", args.noAutoLoad);
        TCLAP::SwitchArg             noRendering      ("R", "noRendering", "Disables rendering", args.noRendering);
        TCLAP::SwitchArg             noViewportControl("V", "noViewCtrl",  "Disables viewport controls", args.noViewportControl);
        TCLAP::SwitchArg             noAnimCaching    ("C", "noAnimCache", "Disables animation caching", args.noAnimCaching);
        TCLAP::SwitchArg             minimalView      ("z", "minimal",     "Minimal viewport display and controls (for debugging purposes)", args.minimalView);

        cmd.add(abcFile);
        cmd.add(logFile);
        cmd.add(rndrCfg);
        cmd.add(screenRatio);
        cmd.add(framerate);
        cmd.add(autoPlay);
        cmd.add(noAutoLoad);
        cmd.add(noRendering);
        cmd.add(noViewportControl);
        cmd.add(noAnimCaching);
        cmd.add(minimalView);

        cmd.parse(argc, argv);

        const float ratio = util::string::parseRatio2d(
            screenRatio.getValue().c_str(),
            args.screenRatio);

        args.abcFile = abcFile.getValue();
        args.logFile = logFile.getValue();
        args.rndrCfg = rndrCfg.getValue();

        if (ratio > 0.0f)
            args.screenRatio   = ratio;
        if (framerate.getValue() > 0.0)
            args.framerate     = framerate.getValue();

        args.autoPlay          = autoPlay.getValue();
        args.noAutoLoad        = noAutoLoad.getValue();
        args.noRendering       = noRendering.getValue();
        args.noViewportControl = noViewportControl.getValue();
        args.noAnimCaching     = noAnimCaching.getValue();
        args.minimalView       = minimalView.getValue();
    }
    catch(TCLAP::ArgException& e)
    {
        std::cerr
            << "TCLAP error\n\t" << e.error() << " for arg " << e.argId()
            << std::endl;
    }
}

// static
void
smks::writeJsonConfiguration(const smks::Arguments& args,
                             size_t                 numThreads,
                             std::string&           jsonCfg)
{
    std::stringstream sstr;

    const size_t width  = static_cast<size_t>(args.screenRatio * g_screenHeight);
    const size_t height = g_screenHeight;

    sstr << "{ ";
    addConfigOption<unsigned int>(sstr, xchg::cfg::num_threads,        numThreads)          << ", ";
    addConfigOption<std::string> (sstr, xchg::cfg::window_title,       g_windowTitle)       << ", ";
    addConfigOption<std::string> (sstr, xchg::cfg::viewport_device,    g_viewportDevice)    << ", ";
    addConfigOption<std::string> (sstr, xchg::cfg::rendering_device,   g_renderingDevice)   << ", ";
    addConfigOption<unsigned int>(sstr, xchg::cfg::screen_width,       width)               << ", ";
    addConfigOption<unsigned int>(sstr, xchg::cfg::screen_height,      height)              << ", ";
    addConfigOption<float>       (sstr, xchg::cfg::screen_ratio,       args.screenRatio)    << ", ";
    addConfigOption<bool>        (sstr, xchg::cfg::cache_anim,         !args.noAnimCaching) << ", ";
    addConfigOption<bool>        (sstr, xchg::cfg::viewport_auto_play, args.autoPlay)       << ", ";
    addConfigOption<bool>        (sstr, xchg::cfg::viewport_picking,   !args.minimalView)   << " ";
    sstr << "}\n";

    jsonCfg = sstr.str();
}

// static
void
smks::addIncludePaths(const smks::Arguments&    args,
                      sys::ResourcePathFinder&  pathFinder)
{
    const size_t BUFFER_SIZE = 256;
    char         BUFFER[BUFFER_SIZE];

    // add repository's asset directory to path finder
    if (util::path::getRepositoryRoot("asset", BUFFER, BUFFER_SIZE))
        pathFinder.addIncludeDirectory(BUFFER);
}

template<typename Type> inline //static
std::ostream&
smks::addConfigOption(std::ostream& out,
                      const char*   name,
                      const Type&   value)
{
    return out
        << "\"" << name << "\": "
        << value
        << " ";
}

template<> inline //static
std::ostream&
smks::addConfigOption<bool>(std::ostream& out,
                            const char*   name,
                            const bool&   value)
{
    return out
        << "\"" << name << "\": "
        << (value ? "true" : "false")
        << " ";
}

template<> inline //static
std::ostream&
smks::addConfigOption<std::string>(std::ostream&      out,
                                   const char*        name,
                                   const std::string& value)
{
    return out
        << "\"" << name << "\": "
        << "\"" << value << "\""
        << " ";
}
