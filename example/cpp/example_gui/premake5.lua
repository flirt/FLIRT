-- ========================================================================= --
-- Copyright 2016-2018 SUPAMONKS_STUDIO                                      --
-- Author: Pierre-Edouard Landes <pel@supamonks.com>                         --
--                                                                           --
-- This program is free software: you can redistribute it and/or modify      --
-- it under the terms of the GNU Lesser General Public License as            --
-- published by the Free Software Foundation, either version 3 of the        --
-- License, or (at your option) any later version.                           --
--                                                                           --
-- This program is distributed in the hope that it will be useful,           --
-- but WITHOUT ANY WARRANTY; without even the implied warranty of            --
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             --
-- GNU Lesser General Public License for more details.                       --
--                                                                           --
-- You should have received a copy of the GNU Lesser General Public License  --
-- along with this program.  If not, see <https://www.gnu.org/licenses/>.    --
--                                                                           --
-- ========================================================================= --


project 'example_gui'

    language 'C++'
    smks.module.set_kind('ConsoleApp')

    files
    {
        'src/**.hpp',
        'src/**.cpp',
    }

    smks.dep.boost.includes()
    smks.dep.json.links()
    smks.dep.tclap.includes()
    smks.dep.osg.links()

    smks.compile_info.links()

    smks.module.uses_log()
    smks.module.uses_strid()
    smks.module.uses_sys()
    smks.module.uses_util()
    smks.module.uses_asset()
    smks.module.uses_xchg()
    smks.module.uses_scn()
    smks.module.uses_clientbase()
    smks.module.uses_rndr_client()
    smks.module.uses_view_client()
    smks.module.includes(smks.module.name.rndr_device_api)
    smks.module.includes(smks.module.name.view_device_api)
    smks.module.uses_util_osg()
    smks.module.uses_view_device_ctrl()

    dependson
    {
        'SmksRndrDevice',
        'SmksViewDevice',
    }

    smks.module.copy_rndr_client_to_targetdir()
    smks.module.copy_rndr_device_to_targetdir()
    smks.module.copy_view_client_to_targetdir()
    smks.module.copy_view_device_to_targetdir()
    smks.module.copy_view_device_ctrl_to_targetdir()

    if os.isdir(smks.repo_path('smksViewDebug')) then
        filter {}
        defines
        {
            'FLIRT_WITH_DEBUG',
        }
        smks.module.uses_view_debug()
        smks.module.copy_view_debug_to_targetdir()
    end

    smks.module.copy_default_abc_to_targetdir()
