# =========================================================================== #
# Copyright 2016-2018 SUPAMONKS_STUDIO                                        #
# Author: Pierre-Edouard Landes <pel@supamonks.com>                           #
#                                                                             #
# This program is free software: you can redistribute it and/or modify        #
# it under the terms of the GNU Lesser General Public License as              #
# published by the Free Software Foundation, either version 3 of the          #
# License, or (at your option) any later version.                             #
#                                                                             #
# This program is distributed in the hope that it will be useful,             #
# but WITHOUT ANY WARRANTY; without even the implied warranty of              #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               #
# GNU Lesser General Public License for more details.                         #
#                                                                             #
# You should have received a copy of the GNU Lesser General Public License    #
# along with this program.  If not, see <https://www.gnu.org/licenses/>.      #
#                                                                             #
# =========================================================================== #
from PySide import QtCore
from PySide import QtOpenGL
from import_pyflirt import import_pyflirt


#  ----------------------------------------------------------------------------
#  Class description:
#
#  This class implements a PySide widget used to display the pyflirt module's
#  viewport content.
#
class QFlirtWidget(QtOpenGL.QGLWidget):

    def __init__(self, compiler, parent=None, shareWidget=None):
        self._compiler = compiler
        QtOpenGL.QGLWidget.__init__(
            self, parent=parent, shareWidget=shareWidget)
        # ensures the widget receives mouse move events even though no mouse
        # button has been pressed.
        self.setMouseTracking(True)
        # ensures the widget receives keyboard events.
        self.setFocusPolicy(QtCore.Qt.StrongFocus)

    def paintGL(self):
        import_pyflirt(compiler=self._compiler).paint_gl()

    def mousePressEvent(self, event):
        import_pyflirt(compiler=self._compiler).mouse_press_event(
            x=event.x(), y=event.y(), button=event.button(),
            modifiers=int(event.modifiers()))
        self.update()

    def mouseMoveEvent(self, event):
        import_pyflirt(compiler=self._compiler).mouse_move_event(
            x=event.x(), y=event.y(), button=event.button(),
            modifiers=int(event.modifiers()))
        self.update()

    def mouseReleaseEvent(self, event):
        import_pyflirt(compiler=self._compiler).mouse_release_event(
            x=event.x(), y=event.y(), button=event.button(),
            modifiers=int(event.modifiers()))
        self.update()

    def wheelEvent(self, event):
        import_pyflirt(compiler=self._compiler).wheel_event(
            delta=event.delta(), x=event.x(), y=event.y(),
            modifiers=int(event.modifiers()))
        self.update()

    def keyPressEvent(self, event):
        import_pyflirt(compiler=self._compiler).key_press_event(
            key=event.key(), text=event.text(),
            auto_repeat=event.isAutoRepeat(), modifiers=int(event.modifiers()))
        self.update()

    def keyReleaseEvent(self, event):
        import_pyflirt(compiler=self._compiler).key_release_event(
            key=event.key(), text=event.text(),
            auto_repeat=event.isAutoRepeat(), modifiers=int(event.modifiers()))
        self.update()

    def resizeEvent(self, event):
        import_pyflirt(compiler=self._compiler).resize_event(
            width=event.size().width(), height=event.size().height(),
            old_width=event.oldSize().width(),
            old_height=event.oldSize().height())
        self.update()
