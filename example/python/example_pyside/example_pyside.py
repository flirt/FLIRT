# =========================================================================== #
# Copyright 2016-2018 SUPAMONKS_STUDIO                                        #
# Author: Pierre-Edouard Landes <pel@supamonks.com>                           #
#                                                                             #
# This program is free software: you can redistribute it and/or modify        #
# it under the terms of the GNU Lesser General Public License as              #
# published by the Free Software Foundation, either version 3 of the          #
# License, or (at your option) any later version.                             #
#                                                                             #
# This program is distributed in the hope that it will be useful,             #
# but WITHOUT ANY WARRANTY; without even the implied warranty of              #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               #
# GNU Lesser General Public License for more details.                         #
#                                                                             #
# You should have received a copy of the GNU Lesser General Public License    #
# along with this program.  If not, see <https://www.gnu.org/licenses/>.      #
#                                                                             #
# =========================================================================== #
import sys
import os
import json
import argparse
from import_pyside import import_pyside
from import_pyflirt import import_pyflirt
from custom_progress import CustomProgress


#  ----------------------------------------------------------------------------
#  Example description:
#
#  This example shows how to create a PySide widget using the pyflirt module's
#  viewport client.
#
def example_pyside(compiler, json_cfg={}, input_abc='', scripts=[]):

    cfg = 'Release'
    if hasattr(sys, 'gettotalrefcount'):
        raw_input('-- attach debugger if needed...')
        json_cfg['num_threads'] = 1
        cfg = 'Debug'

    #  get module information and display it along user arguments
    pyflirt = import_pyflirt(compiler=compiler)
    module_info = pyflirt.get_module_info()
    print '-- compiler: "{:s}"\n' \
        '-- config:\n\t{:s}\n' \
        '-- scripts:\n\t{:s}' \
        '-- module info:\n\t{:s}'.format(
            compiler,
            '\n\t'.join(['{:s}: {}'
                        .format(k, v) for (k, v) in json_cfg.items()]),
            '\n\t'.join(scripts),
            '\n\t'.join(['{:s}: {}'
                        .format(k, v) for (k, v) in module_info.items()]))

    #  initialize and configure module
    pyflirt.initialize(cfg=json.dumps(json_cfg))

    #  add pyflirt release path to include paths in order to find resources
    #  such as shaders
    pyflirt.add_include_directory(
        path=os.path.join(
            os.path.dirname(__file__),
            '..',
            '..',
            '..',
            '_pyflirt',
            compiler,
            'bin',
            cfg))

    #  register custom progress monitoring callbacks
    cb = CustomProgress(chunk=100)
    cb_id = pyflirt.register_progress_callbacks(
        begin=cb.begin,
        proceed=cb.proceed,
        end=cb.end)

    input_group = pyflirt.create_data_node(
        name='__input__', parent=pyflirt.get_scene())

    #  load input Alembic archive if any
    if input_abc:
        pyflirt.create_archive(path=input_abc, parent=input_group)

    #  execute all user scripts for initializing the scene
    for s in scripts:
        print '\n---\n-- execute script \'{:s}\'\n---'.format(s)
        execfile(s)
        print '---'

    #  if no drawable present in the scene, then add default archive
    def _is_drawable(node):
        node_class = pyflirt.get_node_class(node=node)
        return (node_class & pyflirt.NodeClass.DRAWABLE) != 0

    if not pyflirt.get_node_ids(func=_is_drawable):
        pyflirt.create_archive(
            path=os.path.join('abc', 'default.abc'), parent=input_group)

    #  load uninitialized nodes' content by setting their initialization state
    #  to ACTIVE
    def _needs_initialization(node):
        relevant_parms = pyflirt.get_relevant_node_parameters(node=node)
        for parm in [
                pyflirt.BuiltIns.initialization['id'],
                pyflirt.BuiltIns.userAttribInitialization['id']]:
            if parm not in relevant_parms:
                continue
            parm_value = pyflirt.get_node_parameter(parm=parm, node=node)
            if parm_value != pyflirt.ActionState.ACTIVE:
                return True
        return False

    for node in pyflirt.get_node_ids(func=_needs_initialization):
        pyflirt.set_node_parameter(
            parm=pyflirt.BuiltIns.initialization['id'],
            parm_value=pyflirt.ActionState.ACTIVE,
            node=node)

    #  create Qt application and pyflirt widget
    app = import_pyside(compiler=compiler).QtGui.QApplication(sys.argv)
    from q_flirt_widget import QFlirtWidget
    w = QFlirtWidget(compiler=compiler)
    w.setWindowTitle('FLIRT Python Example')
    w.show()
    app.exec_()

    #  deregister custom progress callbacks
    pyflirt.deregister_progress_callbacks(prg=cb_id)

    pyflirt.finalize()
    print '-- pyflirt finalized'


def _example_pyside(argv):
    # parse arguments and pass them to main function
    parser = argparse.ArgumentParser(description='FLIRT Python Example')

    parser.add_argument(
        '-c', '--compiler', required=True, type=str,
        help='compiler used to build FLIRT module')
    parser.add_argument(
        '-j', '--json_cfg', required=False, type=str, default='',
        help='json file used for module configuration')
    parser.add_argument(
        '-l', '--log', required=False, type=str, default='',
        help='log filename')
    parser.add_argument(
        '-i', '--input', required=False, type=str, default='',
        help='path to Alembic archive')
    parser.add_argument(
        '-s', '--scripts', nargs='*', required=False, type=str, default=[],
        help='paths to Python scripts run at startup (for scene '
        'initialization for instance)')

    args = parser.parse_args()

    json_cfg = {}
    if args.json_cfg:
        with open(args.json_cfg, 'r') as f:
            json_cfg = json.load(f)

    if args.log:
        json_cfg['log_file'] = args.log
    if 'log_file' not in json_cfg or not json_cfg['log_file']:
        json_cfg['log_file'] = 'pyflirt.log'

    scripts = [os.path.abspath(s) for s in args.scripts if os.path.isfile(s)]

    example_pyside(
        compiler=args.compiler, json_cfg=json_cfg,
        input_abc=args.input, scripts=scripts)


if __name__ == '__main__':
    try:
        _example_pyside(argv=sys.argv)
    except Exception as e:
        print 'ERROR\n%s' % e.message
        raise
