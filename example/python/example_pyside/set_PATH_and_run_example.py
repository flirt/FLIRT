# =========================================================================== #
# Copyright 2016-2018 SUPAMONKS_STUDIO                                        #
# Author: Pierre-Edouard Landes <pel@supamonks.com>                           #
#                                                                             #
# This program is free software: you can redistribute it and/or modify        #
# it under the terms of the GNU Lesser General Public License as              #
# published by the Free Software Foundation, either version 3 of the          #
# License, or (at your option) any later version.                             #
#                                                                             #
# This program is distributed in the hope that it will be useful,             #
# but WITHOUT ANY WARRANTY; without even the implied warranty of              #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               #
# GNU Lesser General Public License for more details.                         #
#                                                                             #
# You should have received a copy of the GNU Lesser General Public License    #
# along with this program.  If not, see <https://www.gnu.org/licenses/>.      #
#                                                                             #
# =========================================================================== #
import os
import sys
import argparse
import subprocess
from qt_path import qt_path
from pyside_path import pyside_path


def _set_PATH_and_run_example(args):

    # parse arguments and pass them to main function
    parser = argparse.ArgumentParser(description='FLIRT Python Example')
    parser.add_argument(
        '-c', '--compiler', required=True, type=str,
        help='compiler used to build _pyflirt module')
    args, unknown = parser.parse_known_args()

    # update PATH environment variable
    qt_bindir = qt_path(args.compiler, 'bin')
    pyside_bindir = pyside_path(args.compiler, 'bin')

    if os.path.isdir(qt_bindir):
        print '-- expects Qt binaries in:\n\t"{:s}"'.format(qt_bindir)
    else:
        raise RuntimeError(
            '"{:s}" is not a valid directory.'.format(qt_bindir))

    if os.path.isdir(pyside_bindir):
        print '-- expects PySide binaries in:\n\t"{:s}"'.format(pyside_bindir)
    else:
        raise RuntimeError(
            '"{:s}" is not a valid directory.'.format(pyside_bindir))

    env = os.environ.copy()
    env['PATH'] = '{:s};{:s};{:s}'.format(
        qt_bindir, pyside_bindir, env['PATH'])

    # run example via subprocess with updated environment
    script = os.path.join(os.path.dirname(__file__), 'example_pyside.py')
    cmd = [
        sys.executable,
        script,
        '-c',
        args.compiler]
    if unknown:
        cmd.append(unknown)
    subprocess.Popen(cmd, env=env, shell=True)


if __name__ == '__main__':
    try:
        _set_PATH_and_run_example(args=sys.argv)
    except Exception as e:
        raise
