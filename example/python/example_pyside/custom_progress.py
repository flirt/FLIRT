# =========================================================================== #
# Copyright 2016-2018 SUPAMONKS_STUDIO                                        #
# Author: Pierre-Edouard Landes <pel@supamonks.com>                           #
#                                                                             #
# This program is free software: you can redistribute it and/or modify        #
# it under the terms of the GNU Lesser General Public License as              #
# published by the Free Software Foundation, either version 3 of the          #
# License, or (at your option) any later version.                             #
#                                                                             #
# This program is distributed in the hope that it will be useful,             #
# but WITHOUT ANY WARRANTY; without even the implied warranty of              #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               #
# GNU Lesser General Public License for more details.                         #
#                                                                             #
# You should have received a copy of the GNU Lesser General Public License    #
# along with this program.  If not, see <https://www.gnu.org/licenses/>.      #
#                                                                             #
# =========================================================================== #


#  ----------------------------------------------------------------------------
#  Class description:
#
#  This class implements a set of simple callbacks that display the progress
#  of the pyflirt module's rendering client as text on the output console.
#
class CustomProgress(object):

    def __init__(self, chunk):
        self._progress_name = ''
        self._total_num_steps = 0
        self._chunk = chunk

    def begin(self, progress_name, total_num_steps):
        self._progress_name = progress_name
        self._total_num_steps = total_num_steps
        print '-- \'{:s}\' begins'.format(
            self._progress_name)

    def proceed(self, cur_step):
        if cur_step % self._chunk == 0:
            print '-- \'{:s}\': {:d} / {:d}'.format(
                self._progress_name,
                cur_step,
                self._total_num_steps)

    def end(self, progress_name):
        print '-- \'{:s}\' ends'.format(
            self._progress_name)
