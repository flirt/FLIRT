# =========================================================================== #
# Copyright 2016-2018 SUPAMONKS_STUDIO                                        #
# Author: Pierre-Edouard Landes <pel@supamonks.com>                           #
#                                                                             #
# This program is free software: you can redistribute it and/or modify        #
# it under the terms of the GNU Lesser General Public License as              #
# published by the Free Software Foundation, either version 3 of the          #
# License, or (at your option) any later version.                             #
#                                                                             #
# This program is distributed in the hope that it will be useful,             #
# but WITHOUT ANY WARRANTY; without even the implied warranty of              #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               #
# GNU Lesser General Public License for more details.                         #
#                                                                             #
# You should have received a copy of the GNU Lesser General Public License    #
# along with this program.  If not, see <https://www.gnu.org/licenses/>.      #
#                                                                             #
# =========================================================================== #
import sys
from pyside_path import pyside_path


def import_pyside(compiler):
    if 'PySide' in sys.modules:
        return sys.modules['PySide']

    pyside_moddir = pyside_path(compiler, 'lib', 'site-packages')
    sys.path.insert(0, pyside_moddir)

    print '-- import PySide from "{:s}"'.format(pyside_moddir)
    from PySide import QtCore
    from PySide import QtGui
    from PySide import QtOpenGL

    return sys.modules['PySide']
