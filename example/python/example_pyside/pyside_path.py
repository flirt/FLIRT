# =========================================================================== #
# Copyright 2016-2018 SUPAMONKS_STUDIO                                        #
# Author: Pierre-Edouard Landes <pel@supamonks.com>                           #
#                                                                             #
# This program is free software: you can redistribute it and/or modify        #
# it under the terms of the GNU Lesser General Public License as              #
# published by the Free Software Foundation, either version 3 of the          #
# License, or (at your option) any later version.                             #
#                                                                             #
# This program is distributed in the hope that it will be useful,             #
# but WITHOUT ANY WARRANTY; without even the implied warranty of              #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               #
# GNU Lesser General Public License for more details.                         #
#                                                                             #
# You should have received a copy of the GNU Lesser General Public License    #
# along with this program.  If not, see <https://www.gnu.org/licenses/>.      #
#                                                                             #
# =========================================================================== #
import sys
from dep_path import dep_path


def pyside_path(compiler, *argv):
    # -------------------------------------------------------------------------
    debug = hasattr(sys, 'gettotalrefcount')
    return dep_path(
        'pyside-{:s}'.format(compiler),
        'PySide-1.2.4',
        'pyside_install',
        'py2.7-qt4.8.6-64bit-{:s}'.format('debug' if debug else 'release'),
        *argv)
    # -------------------------------------------------------------------------
