-- ========================================================================= --
-- Copyright 2016-2018 SUPAMONKS_STUDIO                                      --
-- Author: Pierre-Edouard Landes <pel@supamonks.com>                         --
--                                                                           --
-- This program is free software: you can redistribute it and/or modify      --
-- it under the terms of the GNU Lesser General Public License as            --
-- published by the Free Software Foundation, either version 3 of the        --
-- License, or (at your option) any later version.                           --
--                                                                           --
-- This program is distributed in the hope that it will be useful,           --
-- but WITHOUT ANY WARRANTY; without even the implied warranty of            --
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             --
-- GNU Lesser General Public License for more details.                       --
--                                                                           --
-- You should have received a copy of the GNU Lesser General Public License  --
-- along with this program.  If not, see <https://www.gnu.org/licenses/>.    --
--                                                                           --
-- ========================================================================= --


smks.compile_info = {}


smks.compile_info.path = function (...)

    return smks.repo_path('compile_info', ...)

end


smks.compile_info.links = function ()

    local outdir = smks.module.outdir('StaticLib')
    if not outdir then
        return
    end

    filter {}
    includedirs
    {
        smks.compile_info.path('include'),
    }
    libdirs
    {
        smks.compile_info.path(outdir),
    }
    links
    {
        'compile_info',
    }

end


project 'compile_info'

    language 'C++'
    smks.module.set_kind('StaticLib')

    -- run the python script that gathers git information first
    -- if commit information is needed, specify path to git executable
    -- via the --git option.
    local git_path = '' -- path/to/git (looks in PATH by default)
    local generated_hpp = smks.compile_info.path(
        'include', 'smks', 'sys', 'compile_info.hpp')
    local generated_cpp = smks.compile_info.path(
        'src', 'smks', 'sys', 'compile_info.cpp')
    local cmd = string.format(
        '%q %q -c %s --git %q --out_hpp %q --out_cpp %q',
        smks.dep.python.path('python'),
        smks.repo_path('script', 'create_compile_info_cpp.py'),
        smks.compiler(),
        git_path,
        generated_hpp,
        generated_cpp)

    prebuildcommands
    {
        cmd,
    }

    defines
    {
        string.format('FLIRT_COMPILER=%s', smks.compiler()),
    }
    files
    {
        'include/**.hpp',
        'src/**.hpp',
        'src/**.cpp',
        generated_hpp,
        generated_cpp, -- must be explicitly set
    }
    includedirs
    {
        'include',
    }
