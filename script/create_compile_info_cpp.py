# =========================================================================== #
# Copyright 2016-2018 SUPAMONKS_STUDIO                                        #
# Author: Pierre-Edouard Landes <pel@supamonks.com>                           #
#                                                                             #
# This program is free software: you can redistribute it and/or modify        #
# it under the terms of the GNU Lesser General Public License as              #
# published by the Free Software Foundation, either version 3 of the          #
# License, or (at your option) any later version.                             #
#                                                                             #
# This program is distributed in the hope that it will be useful,             #
# but WITHOUT ANY WARRANTY; without even the implied warranty of              #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               #
# GNU Lesser General Public License for more details.                         #
#                                                                             #
# You should have received a copy of the GNU Lesser General Public License    #
# along with this program.  If not, see <https://www.gnu.org/licenses/>.      #
#                                                                             #
# =========================================================================== #
import os
import sys
import subprocess
import datetime
import argparse


class CompileInfo:

    BUILD_DATE = 'BUILD_DATE'
    BUILD_TIMESTAMP = 'BUILD_TIMESTAMP'
    BUILD_REPO = 'BUILD_REPO'
    BUILD_COMMIT = 'BUILD_COMMIT'
    BUILD_COMPILER = 'BUILD_COMPILER'

    def __init__(self, compiler, git_path):
        now = datetime.datetime.now()

        self._date = now.strftime("%Y/%m/%d %H:%M")
        self._timestamp = int(now.strftime("%Y%m%d%H%M"))
        self._compiler = compiler
        self._repo = ''
        self._commit = ''

        git_exe = git_path
        if not os.path.isfile(git_exe):
            if git_exe:
                print '\nWARNING: "{:s}" is no valid executable.\n'.format(
                    git_exe)
            git_exe = 'git'  # fallback to git, hoping it is in PATH

        self._repo = self._run_git_command(
            cmd=[git_exe, 'config', '--get', 'remote.origin.url'])
        self._commit = self._run_git_command(
            cmd=[git_exe, 'rev-parse', 'HEAD'])

        if not self._repo:
            print '\nFailed to perform git commands. ' \
                'Is git executable in PATH?\n'

    def _run_git_command(self, cmd):
        try:
            output = subprocess.check_output(
                cmd, shell=True, stderr=subprocess.STDOUT, cwd='..')
        except subprocess.CalledProcessError as e:
            print 'Command "{:s}" returned non-zero exit status {:d}'.format(
                ' '.join(cmd), e.returncode)
            return ''
        else:
            return output[:-1]

    def as_hpp_file(self):
        content = '#pragma once\n\n'
        content += '// generated in a pre-build command\n'
        content += r'namespace smks { namespace sys {' + '\n'

        content += '\textern const char* {:s};\n'.format(
            CompileInfo.BUILD_DATE)
        content += '\textern size_t {:s};\n'.format(
            CompileInfo.BUILD_TIMESTAMP)
        content += '\textern const char* {:s};\n'.format(
            CompileInfo.BUILD_REPO)
        content += '\textern const char* {:s};\n'.format(
            CompileInfo.BUILD_COMMIT)
        content += '\textern const char* {:s};\n'.format(
            CompileInfo.BUILD_COMPILER)

        content += r'} }' + '\n'
        return content

    def as_cpp_file(self):
        content = '// generated in a pre-build command\n'
        content += r'namespace smks { namespace sys {' + '\n'

        content += '\t/*extern*/ const char* {:s} = \"{:s}\";\n'.format(
            CompileInfo.BUILD_DATE, self._date)
        content += '\t/*extern*/ size_t {:s} = {:d};\n'.format(
            CompileInfo.BUILD_TIMESTAMP, self._timestamp)
        content += '\t/*extern*/ const char* {:s} = \"{:s}\";\n'.format(
            CompileInfo.BUILD_REPO, self._repo)
        content += '\t/*extern*/ const char* {:s} = \"{:s}\";\n'.format(
            CompileInfo.BUILD_COMMIT, self._commit)
        content += '\t/*extern*/ const char* {:s} = \"{:s}\";\n'.format(
            CompileInfo.BUILD_COMPILER, self._compiler)

        content += r'} }' + '\n'
        return content


def _save_file(out_path, content):
    if not out_path:
        return
    out_dir = os.path.abspath(os.path.join(out_path, '..'))
    if not os.path.isdir(out_dir):
        os.makedirs(out_dir)
        print '-- created \'%s\' directory' % out_dir

    with open(out_path, 'w') as f:
        f.write(content)

    if os.path.isfile(out_path):
        print '-- file saved in \'%s\'' % os.path.abspath(out_path)


def create_compile_info_cpp(compiler, out_hpp_path, out_cpp_path, git_path):
    print '-- generating source files storing compilation details'
    info = CompileInfo(compiler=compiler, git_path=git_path)
    _save_file(out_path=out_hpp_path, content=info.as_hpp_file())
    _save_file(out_path=out_cpp_path, content=info.as_cpp_file())


def _create_compile_info_cpp(argv):
    parser = argparse.ArgumentParser(
        description='Generates source files storing compilation details.')

    parser.add_argument(
        '-c', '--compiler', type=str, default='',
        help='compiler used to build FLIRT module')
    parser.add_argument(
        '--out_hpp', type=str, default='compile_info.hpp',
        help='path to the output c++ header file')
    parser.add_argument(
        '--out_cpp', type=str, default='compile_info.cpp',
        help='path to the output c++ source file')
    parser.add_argument(
        '--git', type=str, default='', required=False,
        help='path to git executable file')

    args = parser.parse_args()

    create_compile_info_cpp(
        compiler=args.compiler,
        out_hpp_path=args.out_hpp,
        out_cpp_path=args.out_cpp,
        git_path=args.git)


if __name__ == "__main__":
    try:
        _create_compile_info_cpp(argv=sys.argv)
    except SystemExit:
        raise
    except Exception as e:
        sys.stderr.write(
            '\n###\nan internal error occured:\n\n{:s}\n###\n'.format(
                e.message))
        raise
