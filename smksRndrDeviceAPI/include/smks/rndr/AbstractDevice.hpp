// ========================================================================= //
// Copyright 2016-2018 SUPAMONKS_STUDIO                                      //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
//                                                                           //
// ========================================================================= //
#pragma once

#include <memory>
#include <smks/math/types.hpp>

namespace smks
{
    namespace img
    {
        class OutputImage;
    }
    namespace xchg
    {
        struct  BoundingBox;
        class   IdSet;
        class   DataStream;
        class   Data;
        class   ObjectPtr;
    }

    class ClientBase;

    namespace rndr
    {
        /*
        *  Rendering device interface for rndr::Client.
        *
        *  In order to be usable by the rndr::Client class,
        *  the module must expose the following symbols :
        *
        *  rndr::AbstractDevice*
        *  create (rndr::ClientBase::Ptr const&,
        *          size_t numThreads,
        *          const char*);
        *
        *  void
        *  destroy (rndr::AbstractDevice*);
        *
        */
        class AbstractDevice
        {
        public:
            virtual
            ~AbstractDevice()
            { }

            ///////////////////
            // type definitions
            ///////////////////
            typedef struct __Handle { }                             *Handle;
            typedef struct __SceneHandle:       public __Handle { } *SceneHandle;
            typedef struct __ShapeHandle:       public __Handle { } *ShapeHandle;
            typedef struct __LightHandle:       public __Handle { } *LightHandle;
            typedef struct __MaterialHandle:    public __Handle { } *MaterialHandle;
            typedef struct __SubSurfaceHandle:  public __Handle { } *SubSurfaceHandle;
            typedef struct __TextureHandle:     public __Handle { } *TextureHandle;
            typedef struct __PrimitiveHandle:   public __Handle { } *PrimitiveHandle;
            typedef struct __CameraHandle:      public __Handle { } *CameraHandle;
            typedef struct __ToneMapperHandle:  public __Handle { } *ToneMapperHandle;
            typedef struct __RenderPassHandle:  public __Handle { } *RenderPassHandle;
            typedef struct __RendererHandle:    public __Handle { } *RendererHandle;
            typedef struct __SamplerHandle:     public __Handle { } *SamplerFactoryHandle;
            typedef struct __IntegratorHandle:  public __Handle { } *IntegratorHandle;
            typedef struct __PixelFilterHandle: public __Handle { } *PixelFilterHandle;
            typedef struct __FrameBufferHandle: public __Handle { } *FrameBufferHandle;
            typedef struct __DenoiserHandle:    public __Handle { } *DenoiserHandle;
            typedef struct __SelectionHandle:   public __Handle { } *SelectionHandle;

            /*! Hit structure. Describes hit layout for ray queries. */
            struct Hit
            {
                unsigned int    prim;   //!< ID of hit primitive
                float           dist;   //!< distance of hit
            };

            /*! Framebuffer data structure.
            * Stores color information alongs other other passes' data. */
            struct FrameBufferData
            {
                void* data;
                void* dataAOV;
                void* dataId;
                void* dataCoverage;
            };

            //////////////////////////
            // initialization
            //////////////////////////
            virtual
            void
            build(const char* jsonCfg,
                  std::shared_ptr<ClientBase> const&) = 0;

            virtual
            void
            dispose() = 0;

            //////////////////////////
            // handle reference counts
            //////////////////////////
            /*! Increases the reference counter of the handle. */
            virtual
            void
            incRef(Handle) = 0;

            /*! Decreases the reference counter of the handle. If zero is reached
            the handle is destroyed, but not necessarily the referenced object. */
            virtual
            void
            decRef(Handle) = 0;

            /*! Returns the ID of the corresponding node in the scene. */
            virtual
            unsigned int
            getScnId(Handle) const = 0;

            //////////////////
            // handle creators
            //////////////////
            ///*! Creates a new scene. */
            virtual
            SceneHandle
            newScene(const char*    type,
                     unsigned int   scnId) = 0;

            /*! Creates a new camera. */
            virtual
            CameraHandle
            newCamera(const char*   type,
                      unsigned int  scnId) = 0;

            /*! Creates a new material. */
            virtual
            MaterialHandle
            newMaterial(const char*     type,
                        unsigned int    scnId) = 0;

            /*! Creates a new sub-surface. */
            virtual
            SubSurfaceHandle
            newSubSurface(const char*   type,
                          unsigned int  scnId) = 0;

            /*! Creates a new texture. */
            virtual
            TextureHandle
            newTexture(const char*  type,
                       unsigned int scnId) = 0;

            /*! Creates a new shape. */
            virtual
            ShapeHandle
            newShape(const char*    type,
                     unsigned int   scnId) = 0;

            /*! Creates a new light source. */
            virtual
            LightHandle
            newLight(const char*    type,
                     unsigned int   scnId) = 0;

            /*! Creates a new object selection group. */
            virtual
            SelectionHandle
            newSelection(const char*    type,
                         unsigned int   scnId) = 0;

            /*! Creates a new denoiser. */
            virtual
            DenoiserHandle
            newDenoiser(const char*     type,
                        unsigned int    scnId) = 0;

            /*! Creates a new tonemapper. */
            virtual
            ToneMapperHandle
            newToneMapper(const char*   type,
                          unsigned int  scnId) = 0;

            /*! Creates a new render pass. */
            virtual
            RenderPassHandle
            newRenderPass(const char*   type,
                          unsigned int  scnId) = 0;

            /*! Creates a new renderer. */
            virtual
            RendererHandle
            newRenderer(const char*     type,
                        unsigned int    scnId) = 0;

            /*! Creates a new framebuffer. */
            virtual
            FrameBufferHandle
            newFrameBuffer(const char*  type,
                           unsigned int scnId) = 0;

            /*! Creates a new integrator. */
            virtual
            IntegratorHandle
            newIntegrator(const char*   type,
                          unsigned int  scnId) = 0;

            /*! Creates a new sampler. */
            virtual
            SamplerFactoryHandle
            newSamplerFactory(const char*   type,
                              unsigned int  scnId) = 0;

            /*! Creates a new pixel filter. */
            virtual
            PixelFilterHandle
            newPixelFilter(const char*  type,
                           unsigned int scnId) = 0;

            ///////////////////////////////
            // general link/unlink handlers
            ///////////////////////////////
            virtual
            bool    //<! returns true if link event has been handled by function
            link(unsigned int parmId, Handle target, Handle source) = 0;

            virtual
            bool    //<! returns true if unlink event has been handled by function
            unlink(unsigned int parmId, Handle target, Handle source) = 0;

            /////////////////////////////////
            // scene object parameter setting
            /////////////////////////////////
            virtual unsigned int    setBool         (Handle, const char*, const bool&)                          = 0;
            virtual unsigned int    setChar         (Handle, const char*, const char&)                          = 0;
            virtual unsigned int    setUInt         (Handle, const char*, const unsigned int&)                  = 0;
            virtual unsigned int    setUInt64       (Handle, const char*, const size_t&)                        = 0;
            virtual unsigned int    setInt1         (Handle, const char*, const int&)                           = 0;
            virtual unsigned int    setInt2         (Handle, const char*, const math::Vector2i&)                = 0;
            virtual unsigned int    setInt3         (Handle, const char*, const math::Vector3i&)                = 0;
            virtual unsigned int    setInt4         (Handle, const char*, const math::Vector4i&)                = 0;
            virtual unsigned int    setFloat1       (Handle, const char*, const float&)                         = 0;
            virtual unsigned int    setFloat2       (Handle, const char*, const math::Vector2f&)                = 0;
            virtual unsigned int    setFloat4       (Handle, const char*, const math::Vector4f&)                = 0;
            virtual unsigned int    setTransform    (Handle, const char*, const math::Affine3f&)                = 0;
            virtual unsigned int    setString       (Handle, const char*, const char*)                          = 0;
            virtual unsigned int    setIdSet        (Handle, const char*, const xchg::IdSet&)                   = 0;
            virtual unsigned int    setBounds       (Handle, const char*, const xchg::BoundingBox&)             = 0;
            virtual unsigned int    setDataStream   (Handle, const char*, const xchg::DataStream&)              = 0;
            virtual unsigned int    setData         (Handle, const char*, const std::shared_ptr<xchg::Data>&)   = 0;
            virtual unsigned int    setObjectPtr    (Handle, const char*, const xchg::ObjectPtr&)               = 0;
            virtual void            unset           (Handle, unsigned int)                                      = 0;

            /*! Clear all parameters cached in the handle to reduce memory
            *  consumption. For instance, mesh handles no longer reference data
            *  buffers passed at creation time. */
            virtual
            void
            dispose(Handle) = 0;

            /*! Destroys handle. Should coincide with the deletion of the handle's
            *  associated data node. */
            virtual
            void
            destroy(Handle) = 0;

            /*! Records all uncommitted parameter value changes for the current frame. */
            virtual
            void
            commitFrameState(Handle) = 0;

            /*! Returns true if the queried parameter corresponds to a
            *  parameter that should be sampled per subframe during
            *  motion blur. */
            virtual
            bool
            perSubframeParameter(Handle,
                                 unsigned int parmId) const = 0;

            /*! Indicates the beginning of multilinear motion blur handling.
            *  This function should be used to allocate the memory needed to
            *  cache different parameter values for each subframe. */
            virtual
            void
            beginSubframeCommits(Handle,
                                 size_t numSubframes) = 0;

            /*! Records all uncommitted parameter value changes for the current subframe.
            *  Only values of parameters flagged as varying per subframe should be
            *  considered. */
            virtual
            void
            commitSubframeState(Handle,
                                size_t subframe) = 0;

            /*! Indicates the end of multilinear motion blur computation.
            *  This function should be used to perform precomputations on
            *  values from parameters flagged as varying per subframe. */
            virtual
            void
            endSubframeCommits(Handle) = 0;

            ///////////////////
            // scene primitives
            ///////////////////
            virtual
            PrimitiveHandle
            newPrimitive(const char* type) = 0;

            virtual
            void
            destroy(PrimitiveHandle) = 0;

            virtual
            void
            add(SceneHandle,
                PrimitiveHandle) = 0;

            virtual
            void
            remove(SceneHandle,
                   PrimitiveHandle) = 0;

            virtual
            void
            setShape(PrimitiveHandle,
                     ShapeHandle) = 0;

            virtual
            void
            setLight(PrimitiveHandle,
                     LightHandle) = 0;

            virtual
            void
            setMaterial(PrimitiveHandle,
                        MaterialHandle) = 0;

            virtual
            void
            setSubSurface(PrimitiveHandle,
                          SubSurfaceHandle) = 0;

            /////////////////////////
            // material configuration
            /////////////////////////
            virtual
            void
            setInput(MaterialHandle,
                     MaterialHandle rtInput,
                     size_t         idx) = 0;

            ////////////////////////////
            // framebuffer configuration
            ////////////////////////////
            virtual
            void
            setDenoiser(FrameBufferHandle,
                        DenoiserHandle) = 0;

            virtual
            void
            setToneMapper(FrameBufferHandle,
                          ToneMapperHandle) = 0;

            virtual
            void
            add(FrameBufferHandle,
                RenderPassHandle) = 0;

            virtual
            void
            remove(FrameBufferHandle,
                   RenderPassHandle) = 0;

            ////////////////////////
            // shading configuration
            ////////////////////////
            virtual
            void
            setupMaterial(MaterialHandle,
                          unsigned int parmId,
                          TextureHandle) = 0;

            virtual
            void
            setupSubSurface(SubSurfaceHandle,
                            unsigned int parmId,
                            TextureHandle) = 0;

            /////////////////////////
            // renderer configuration
            /////////////////////////
            virtual
            void
            setSamplerFactory(RendererHandle,
                              SamplerFactoryHandle) = 0;

            virtual
            void
            setIntegrator(RendererHandle,
                          IntegratorHandle) = 0;

            virtual
            void
            setFilter(RendererHandle,
                      PixelFilterHandle) = 0;

            ///////////////
            // render calls
            ///////////////
            /*! Renders a frame of the scene using a given renderer and camera,
            *  into the specified framebuffer. The number of subframe corresponds
            *  to the number of steps used for multilinear motion blur. */
            virtual
            void
            renderFrame(RendererHandle,
                        SceneHandle,
                        CameraHandle,
                        FrameBufferHandle,
                        size_t numSubframes) = 0;

            /*! Retrieves the frame stored in framebuffer as an image.*/
            virtual
            void
            getFrame(FrameBufferHandle,
                     img::OutputImage&) = 0;

            /*! Retrieves the frame stored in framebuffer as a 2D array of pixels
            *  in floating-point RGBA format.*/
            virtual
            const float*
            getFrameData(FrameBufferHandle,
                         size_t& width,
                         size_t& height) = 0;

            virtual
            void
            getMetadata(FrameBufferHandle,
                        img::OutputImage&) = 0;

            virtual
            void
            getMetadata(SceneHandle,
                        img::OutputImage&) = 0;

            virtual
            void
            getMetadata(RendererHandle,
                        img::OutputImage&) = 0;

            virtual
            void
            getMetadata(ToneMapperHandle,
                        img::OutputImage&) = 0;

            virtual
            void
            getMetadata(CameraHandle,
                        img::OutputImage&) = 0;
        };
    }
}
