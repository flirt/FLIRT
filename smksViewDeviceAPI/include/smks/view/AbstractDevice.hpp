// ========================================================================= //
// Modification copyright 2016-2018 SUPAMONKS_STUDIO                         //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
// _________________________________________________________________________ //
// Copyright 2009-2013 Intel Corporation                                     //
//                                                                           //
// Licensed under the Apache License, Version 2.0 (the "License");           //
// you may not use this file except in compliance with the License.          //
// You may obtain a copy of the License at                                   //
//                                                                           //
//     http://www.apache.org/licenses/LICENSE-2.0                            //
//                                                                           //
// Unless required by applicable law or agreed to in writing, software       //
// distributed under the License is distributed on an "AS IS" BASIS,         //
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  //
// See the License for the specific language governing permissions and       //
// limitations under the License.                                            //
//                                                                           //
// ========================================================================= //
#pragma once

#include <memory>

#include <osgViewer/Viewer>

#include "smks/sys/configure_view_device_api.hpp"

namespace smks
{
    class ClientBase;
    namespace view
    {
        class FLIRT_VIEW_DEVICE_API_EXPORT AbstractDevice:
            public osgViewer::Viewer
        {
        public:
            AbstractDevice():
                osgViewer::Viewer()
            { }

            virtual
            ~AbstractDevice()
            { }

            virtual
            void
            build(const char*                           jsonCfg,
                  std::shared_ptr<ClientBase> const&    client,
                  osg::Group*                           clientRoot,
                  osg::GraphicsContext*) = 0;

            virtual
            void
            dispose() = 0;

            virtual
            std::weak_ptr<ClientBase> const&
            client() const = 0;

            virtual
            const osg::Group*
            clientRoot() const = 0;

            virtual
            osg::Group*
            clientRoot() = 0;

            virtual
            void
            setViewportLightDirection(const osg::Vec3f&) = 0;

            virtual
            unsigned int
            viewportCameraXform() const = 0;

            virtual
            unsigned int
            viewportCamera() const = 0;

            virtual
            unsigned int
            displayedCamera() const = 0;

            virtual
            void
            displayCamera(unsigned int) = 0;

            virtual
            unsigned int
            displayedFrameBuffer() const = 0;

            virtual
            void
            displayFrameBuffer(unsigned int) = 0;

            virtual
            void
            pick(int x, int y, bool accumulate) = 0;    //<! updates scene node's selection
        };
    }
}
