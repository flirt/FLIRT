// ========================================================================= //
// Copyright 2016-2018 SUPAMONKS_STUDIO                                      //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
//                                                                           //
// ========================================================================= //
#pragma once

#include <sstream>
#include <memory>

#include <smks/math/types.hpp>
#include <smks/xchg/NodeClass.hpp>
#include <smks/xchg/ActionState.hpp>
#include <smks/xchg/ParameterInfo.hpp>
#include <smks/sys/Exception.hpp>

#include "smks/macro_parm.hpp"
#include "smks/macro_assign.hpp"
#include "smks/sys/configure_clientbase.hpp"

namespace smks
{
    namespace parm
    {
        class BuiltIn;
    }

    namespace xchg
    {
        struct  BoundingBox;
        class   IdSet;
        class   DataStream;
        class   Data;
        class   SceneEvent;
        class   ParmEvent;
        class   ClientEvent;
        class   ObjectPtr;
        template<class EventType> class AbstractObserver;
        template<class ValueType> class Vector;
        class   AbstractClientObserver;
        struct  AbstractProgressCallbacks;
    }

    namespace scn
    {
        class TreeNode;
        class Scene;
        class ParmConnectionBase;
    }

    namespace sys
    {
        class ResourceDatabase;
    }

    class  ClientBindingBase;
    class  AbstractClientObserver;
    class  AbstractClientNodeObserver;
    struct AbstractClientNodeFilter;

    class FLIRT_CLIENTBASE_EXPORT ClientBase
    {
    public:
        typedef std::shared_ptr<ClientBase> Ptr;
        typedef std::weak_ptr<ClientBase>   WPtr;
    private:
        typedef std::shared_ptr<scn::Scene>                      ScenePtr;
        typedef std::shared_ptr<scn::TreeNode>                   TreeNodePtr;
        typedef std::weak_ptr<scn::TreeNode>                     TreeNodeWPtr;
        typedef std::shared_ptr<ClientBindingBase>               ClientBindingBasePtr;
        typedef std::shared_ptr<AbstractClientObserver>          AbstractClientObserverPtr;
        typedef std::shared_ptr<AbstractClientNodeObserver>      AbstractClientNodeObserverPtr;
        typedef std::shared_ptr<AbstractClientNodeFilter>        AbstractClientNodeFilterPtr;
        typedef std::shared_ptr<xchg::AbstractProgressCallbacks> AbstractProgressCallbacksPtr;
        typedef std::shared_ptr<sys::ResourceDatabase>           ResourcesPtr;

    private:
        class PImpl;
        class SceneObserver;
        class SceneParmObserver;
        class ParmObserverWrap;

    private:
        PImpl *_pImpl;

    protected:
        ClientBase();
    private:
        // non-copyable
        ClientBase(const ClientBase&);
        ClientBase& operator=(const ClientBase&);

    protected:
        virtual
        void
        build(Ptr const&);

        virtual
        void
        dispose();

    public:
        virtual
        ~ClientBase();

        WPtr const&
        self();

        void
        setScene(ScenePtr const&);

        //<! return scene node unique ID (or 0 if no scene set)
        unsigned int
        sceneId() const;

        ResourcesPtr const&
        getOrCreateResourceDatabase();

        sys::ResourceDatabase*
        getResourceDatabase();

        const sys::ResourceDatabase*
        getResourceDatabase() const;

        void
        setSharedResourceDatabase(ResourcesPtr const&);

        void
        getIds(xchg::IdSet&,
               bool         accumulate = false,
               AbstractClientNodeFilterPtr const& = nullptr,
               size_t       numRegexes = 0,
               const char** regexes    = nullptr,
               unsigned int startId    = 0,
               bool         upwards    = false) const;

        bool
        nodeExists(unsigned int) const;

        const char*
        getNodeName(unsigned int) const;

        const char*
        getNodeFullName(unsigned int) const;

        size_t
        getNodeNumChildren(unsigned int) const;

        unsigned int
        getNodeChild(unsigned int, size_t) const;

        unsigned int
        getNodeParent(unsigned int) const;

        xchg::NodeClass
        getNodeClass(unsigned int) const;

        unsigned int
        createDataNode(const char*  name,
                       unsigned int parentId = 0);

        unsigned int
        createArchive(const char*   filePath,
                      unsigned int  parentId = 0);

        unsigned int
        createXform(const char*     name,
                    unsigned int    parentId = 0);

        unsigned int
        createCamera(const char*    name,
                     unsigned int   parentId = 0);

        unsigned int
        createSelector(const char*  regex,
                       const char*  name,
                       unsigned int parentId = 0);

        unsigned int
        createMaterial(const char*  code,
                       const char*  name,
                       unsigned int parentId = 0);

        unsigned int
        createSubSurface(const char*    code,
                         const char*    name,
                         unsigned int   parentId = 0);

        unsigned int
        createTexture(const char*   code,
                      const char*   name,
                      unsigned int  parentId = 0);

        unsigned int
        createLight(const char*     type,
                    const char*     name,
                    unsigned int    parentId = 0);

        unsigned int
        createFrameBuffer(const char*   code,
                          const char*   name,
                          unsigned int  parentId = 0);

        unsigned int
        createDenoiser(const char*  code,
                       const char*  name,
                       unsigned int parentId = 0);

        unsigned int
        createToneMapper(const char*    code,
                         const char*    name,
                         unsigned int   parentId = 0);

        unsigned int
        createRenderer(const char*  code,
                       const char*  name,
                       unsigned int parentId = 0);

        unsigned int
        createFlatRenderPass(const char*    code,
                             const char*    name,
                             unsigned int   parentId = 0);

        unsigned int
        createLightPathExpressionPass(const char*   lpe,
                                      const char*   name,
                                      unsigned int  parentId = 0);

        unsigned int
        createShapeIdCoveragePass(const char*   name,
                                  unsigned int  parentId = 0);

        unsigned int
        createIntegrator(const char*  code,
                         const char*  name,
                         unsigned int parentId = 0);

        unsigned int
        createSamplerFactory(const char*  code,
                             const char*  name,
                             unsigned int parentId = 0);

        unsigned int
        createPixelFilter(const char*  code,
                          const char*  name,
                          unsigned int parentId = 0);

        unsigned int
        createRenderAction(const char*  name,
                           unsigned int parentId = 0);

        template<typename ValueType> inline
        unsigned int
        createAssign(const char*      parmName,
                     const ValueType& parmValue,
                     const char*      name,
                     unsigned int     parentId = 0)
        {
            return createAssign<ValueType>(
                parmName,
                parmValue,
                name,
                getNode(parentId));
        }

    protected:
        template<typename ValueType> static
        unsigned int
        createAssign(const char*    parmName,
                     const ValueType&,
                     const char*    name,
                     TreeNodePtr const&)
        {
            std::stringstream sstr;
            sstr
                << "Failed to create assign node for parameter '" << parmName << "': "
                << "Not implemented for the requested type.";
            throw sys::Exception(sstr.str().c_str(), "Parameter Assignment Creation");
        }

        SPECIALIZE_ASSIGN_TYPE_DECL(bool)
        SPECIALIZE_ASSIGN_TYPE_DECL(unsigned int)
        SPECIALIZE_ASSIGN_TYPE_DECL(int)
        SPECIALIZE_ASSIGN_TYPE_DECL(size_t)
        SPECIALIZE_ASSIGN_TYPE_DECL(float)
        SPECIALIZE_ASSIGN_TYPE_DECL(std::string)

    public:
        unsigned int
        createIdAssign(const char*  parmName,
                       unsigned int parmValue,
                       const char*  name,
                       unsigned int parentId = 0);

        void
        destroyDataNode(unsigned int);

        // node parameter handling
        /////////////////////////////
        bool
        isNodeParameterWritable(const char*  parmName,
                                unsigned int nodeId = 0) const;

        bool
        isNodeParameterWritable(unsigned int parmId,
                                unsigned int nodeId = 0) const;

        bool
        isNodeParameterReadable(const char*  parmName,
                                unsigned int nodeId = 0) const;

        bool
        isNodeParameterReadable(unsigned int parmId,
                                unsigned int nodeId = 0) const;

        //----------------------------------------------------------------------------------------------
        // PARAMETER EXISTENCE TESTS
        //----------------------------------------------------------------------------------------------
        bool
        nodeParameterExists(const char*  parmName,
                            unsigned int fromNodeId      = 0,
                            unsigned int upToNodeId      = 0,
                            unsigned int *foundInNodeId  = nullptr) const;

        bool
        nodeParameterExists(unsigned int parmId,
                            unsigned int fromNodeId     = 0,
                            unsigned int upToNodeId     = 0,
                            unsigned int *foundInNodeId = nullptr) const;

        template<typename ValueType> inline
        bool
        nodeParameterExistsAs(const char*  parmName,
                              unsigned int fromNodeId      = 0,
                              unsigned int upToNodeId      = 0,
                              unsigned int *foundInNodeId  = nullptr) const
        {
            return nodeParameterExistsAs<ValueType>(
                parmName,
                getNode(fromNodeId),
                upToNodeId,
                foundInNodeId);
        }

        template<typename ValueType> inline
        bool
        nodeParameterExistsAs(unsigned int parmId,
                              unsigned int fromNodeId = 0,
                              unsigned int upToNodeId = 0,
                              unsigned int *foundInNodeId = nullptr) const
        {
            return nodeParameterExistsAs<ValueType>(
                parmId,
                getNode(fromNodeId),
                upToNodeId,
                foundInNodeId);
        }

        bool
        nodeParameterExistsAsId(const char*  parmName,
                                unsigned int fromNodeId      = 0,
                                unsigned int upToNodeId      = 0,
                                unsigned int *foundInNodeId  = nullptr) const;
        bool
        nodeParameterExistsAsId(unsigned int parmId,
                                unsigned int fromNodeId      = 0,
                                unsigned int upToNodeId      = 0,
                                unsigned int *foundInNodeId  = nullptr) const;

        bool
        nodeParameterExistsAsSwitch(const char*  parmName,
                                    unsigned int fromNodeId      = 0,
                                    unsigned int upToNodeId      = 0,
                                    unsigned int *foundInNodeId  = nullptr) const;
        bool
        nodeParameterExistsAsSwitch(unsigned int parmId,
                                    unsigned int fromNodeId      = 0,
                                    unsigned int upToNodeId      = 0,
                                    unsigned int *foundInNodeId  = nullptr) const;

        //----------------------------------------------------------------------------------------------
        // PARAMETER UPDATERS
        //--------------------------------------------------------------------------------------------
        template<typename ValueType> inline
        void
        updateNodeParameter(unsigned int     parmId,
                            const ValueType& parmValue,
                            unsigned int     nodeId = 0)
        {
            updateNodeParameter<ValueType>(parmId, parmValue, getNode(nodeId));
        }

        //----------------------------------------------------------------------------------------------
        // PARAMETER SETTERS
        //----------------------------------------------------------------------------------------------
        template<typename ValueType> inline
        unsigned int
        setNodeParameter(const char*      parmName,
                         const ValueType& parmValue,
                         unsigned int     nodeId = 0) const
        {
            return setNodeParameter<ValueType>(parmName, parmValue, getNode(nodeId));
        }

        template<typename ValueType> inline
        unsigned int
        setNodeParameter(const parm::BuiltIn& parm,
                         const ValueType&     parmValue,
                         unsigned int         nodeId = 0) const
        {
            return setNodeParameter<ValueType>(parm, parmValue, getNode(nodeId));
        }

        unsigned int
        setNodeParameterId(const char*  parmName,
                           unsigned int parmValue,
                           unsigned int nodeId = 0);

        unsigned int
        setNodeParameterTrigger(const char*  parmName,
                                unsigned int nodeId = 0);

        unsigned int
        setNodeParameterSwitch(const char*  parmName,
                               bool         parmValue,
                               unsigned int nodeId = 0);

        //----------------------------------------------------------------------------------------------
        // PARAMETER GETTERS
        //----------------------------------------------------------------------------------------------
        template<typename ValueType> inline
        bool
        getNodeParameter(const parm::BuiltIn& parm,
                         ValueType&           parmValue,
                         unsigned int         fromNodeId = 0,
                         unsigned int         upToNodeId = 0) const
        {
            return getNodeParameter<ValueType>(
                parm,
                parmValue,
                getNode(fromNodeId),
                upToNodeId);
        }

        template<typename ValueType> inline
        bool
        getNodeParameter(const char*      parmName,
                         ValueType&       parmValue,
                         const ValueType& defaultValue,
                         unsigned int     fromNodeId = 0,
                         unsigned int     upToNodeId = 0) const
        {
            return getNodeParameter<ValueType>(
                parmName,
                parmValue,
                defaultValue,
                getNode(fromNodeId),
                upToNodeId);
        }

        template<typename ValueType> inline
        bool
        getNodeParameter(unsigned int     parmId,
                         ValueType&       parmValue,
                         const ValueType& defaultValue,
                         unsigned int     fromNodeId = 0,
                         unsigned int     upToNodeId = 0) const
        {
            return getNodeParameter<ValueType>(
                parmId,
                parmValue,
                defaultValue,
                getNode(fromNodeId),
                upToNodeId);
        }

        bool
        getNodeParameterInfo(unsigned int   parmId,
                             xchg::ParameterInfo&,
                             unsigned int   nodeId = 0) const;
        bool
        getNodeParameterInfo(const char*    parmName,
                             xchg::ParameterInfo&,
                             unsigned int   nodeId = 0) const;

        //----------------------------------------------------------------------------------------------
        // PARAMETER GETTERS
        //----------------------------------------------------------------------------------------------
        void
        unsetNodeParameter(const parm::BuiltIn&,
                           unsigned int nodeId = 0);
        void
        unsetNodeParameter(const char*  parmName,
                           unsigned int nodeId = 0);
        void
        unsetNodeParameter(unsigned int parmId,
                           unsigned int nodeId = 0);

        void
        getCurrentNodeParameters(xchg::IdSet&,
                                 unsigned int nodeId = 0) const;

        // A parameter name is 'relevant' wrt a specific node instance
        // if the node's class declares it will react to the
        // changes of value of such a parameter.
        void
        getRelevantNodeParameters(xchg::IdSet&,
                                  unsigned int nodeId = 0) const;

        // node linking
        ///////////////
        size_t
        getNumLinksBetweenNodes(unsigned int targetId,
                                unsigned int sourceId) const;

        void
        getLinksBetweenNodes(unsigned int targetId,
                             unsigned int sourceId,
                             xchg::IdSet&) const;   //<! returns a set of parameter IDs

        bool
        isLinkBetweenNodes(unsigned int parmId,
                           unsigned int targetId,
                           unsigned int sourceId) const;

        void
        getNodeLinkSources(unsigned int,
                           xchg::IdSet&) const; //<! returns a set of node IDs

        void
        getNodeLinkTargets(unsigned int,
                           xchg::IdSet&) const; //<! returns a set of node IDs

        // node parameter connection
        ////////////////////////////
        template<typename ValueType>
        unsigned int
        connectNodeParameters(unsigned int  masterNodeId,
                              const char*   masterParmName,
                              unsigned int  slaveNodeId,
                              const char*   slaveParmName,
                              int           priority = 0)
        {
            return connectNodeParameters<ValueType>(
                scene(),
                masterNodeId,
                masterParmName,
                slaveNodeId,
                slaveParmName,
                priority);
        }

        unsigned int
        connectIdNodeParameters(unsigned int    masterNodeId,
                                const char*     masterParmName,
                                unsigned int    slaveNodeId,
                                const char*     slaveParmName,
                                int             priority = 0);

        bool
        hasNodeParameterConnection(unsigned int) const;

        bool
        hasIdNodeParameterConnection(unsigned int) const;

        std::shared_ptr<scn::ParmConnectionBase>
        getNodeParameterConnection(unsigned int) const;

        void
        disconnectNodeParameters(unsigned int);

        void
        disconnectIdNodeParameters(unsigned int);

        // binding getter
        /////////////////
        std::shared_ptr<ClientBindingBase>
        getBinding(unsigned int id) const;

        // observer handling
        ////////////////////
        void
        registerObserver(AbstractClientObserverPtr const&);
        void
        deregisterObserver(AbstractClientObserverPtr const&);

        void
        registerObserver(AbstractClientNodeObserverPtr const&,
                         unsigned int nodeId);
        void
        deregisterObserver(AbstractClientNodeObserverPtr const&,
                           unsigned int nodeId);

    protected:
        ScenePtr const&
        scene() const;

        // register GUI event callbacks
        ///////////////////////////////
        virtual inline
        void
        registerGUIEventCallbacks(scn::Scene&)
        { }
        virtual inline
        void
        deregisterGUIEventCallbacks(scn::Scene&)
        { }

    public:
        // register progress callbacks
        ///////////////////////////////
        void
        notifyProgressBeginning(const char*, size_t totalSteps);
        bool
        notifyProgressProceeding(size_t);
        void
        notifyProgressEnd(const char*);
        void
        registerProgressCallbacks(AbstractProgressCallbacksPtr const&);
        void
        deregisterProgressCallbacks(AbstractProgressCallbacksPtr const&);

    protected:
        void
        getBindingKeys(xchg::IdSet&) const;

        virtual
        void
        handleSceneEvent(scn::Scene&, const xchg::SceneEvent&);

        virtual
        void
        handleSceneParmEvent(const xchg::ParmEvent&);

        virtual inline
        void
        postSetScene(ScenePtr const&)
        { }

        virtual inline
        void
        preUnsetScene(ScenePtr const&)
        { }

        virtual
        ClientBindingBasePtr
        createBinding(TreeNodeWPtr const&) = 0;

        virtual
        void
        destroyBinding(ClientBindingBasePtr const&) = 0;

    private:
        unsigned int
        registerBinding(ClientBindingBasePtr const&);

        void
        deregisterBinding(unsigned int);

    protected:
        TreeNodePtr
        getNode(unsigned int nodeId = 0) const;

        template<typename ValueType> static
        bool
        nodeParameterExistsAs(const char*   parmName,
                              TreeNodePtr const&,
                              unsigned int  upToNodeId,
                              unsigned int  *foundInNodeId)
        {
            std::stringstream sstr;
            sstr
                << "Failed to query existence of parameter '" << parmName << "': "
                << "Not implemented for the requested type.";
            throw sys::Exception(sstr.str().c_str(), "Parameter Existence Test");
        }

        template<typename ValueType> static
        bool
        nodeParameterExistsAs(unsigned int  parmId,
                              TreeNodePtr const&,
                              unsigned int  upToNodeId,
                              unsigned int  *foundInNodeId)
        {
            std::stringstream sstr;
            sstr
                << "Failed to query existence of parameter (ID = " << parmId << "): "
                << "Not implemented for the requested type.";
            throw sys::Exception(sstr.str().c_str(), "Parameter Existence Test");
        }

        template<typename ValueType> static
        bool
        getNodeParameter(const parm::BuiltIn& parm,
                         ValueType&,
                         TreeNodePtr const&,
                         unsigned int upToNodeId)
        {
            std::stringstream sstr;
            sstr
                << "Failed to query value of built-in parameter '" << parm.c_str() << "': "
                << "Not implemented for the requested type.";
            throw sys::Exception(sstr.str().c_str(), "Parameter Value Getter");
        }

        template<typename ValueType> static
        bool
        getNodeParameter(const char* parmName,
                         ValueType&,
                         const ValueType&,
                         TreeNodePtr const&,
                         unsigned int upToNodeId)
        {
            std::stringstream sstr;
            sstr
                << "Failed to query value of parameter '" << parmName << "': "
                << "Not implemented for the requested type.";
            throw sys::Exception(sstr.str().c_str(), "Parameter Value Getter");
        }

        template<typename ValueType> static
        bool
        getNodeParameter(unsigned int parmId,
                         ValueType&,
                         const ValueType&,
                         TreeNodePtr const&,
                         unsigned int upToNodeId)
        {
            std::stringstream sstr;
            sstr
                << "Failed to query value of parameter (ID = " << parmId << "): "
                << "Not implemented for the requested type.";
            throw sys::Exception(sstr.str().c_str(), "Parameter Value Getter");
        }

        template<typename ValueType> static
        unsigned int
        setNodeParameter(const parm::BuiltIn& parm,
                         const ValueType&,
                         TreeNodePtr const&)
        {
            std::stringstream sstr;
            sstr
                << "Failed to set value of built-in parameter '" << parm.c_str() << "': "
                << "Not implemented for the requested type.";
            throw sys::Exception(sstr.str().c_str(), "Parameter Value Setter");
        }

        template<typename ValueType> static
        unsigned int
        setNodeParameter(const char* parmName,
                         const ValueType&,
                         TreeNodePtr const&)
        {
            std::stringstream sstr;
            sstr
                << "Failed to set value of parameter '" << parm.c_str() << "': "
                << "Not implemented for the requested type.";
            throw sys::Exception(sstr.str().c_str(), "Parameter Value Setter");
        }

        template<typename ValueType> static
        void
        updateNodeParameter(unsigned int parmId,
                            const ValueType&,
                            TreeNodePtr const&)
        {
            std::stringstream sstr;
            sstr
                << "Failed to update value of parameter (ID = " << parmId << "): "
                << "Not implemented for the requested type.";
            throw sys::Exception(sstr.str().c_str(), "Parameter Value Setter");
        }

        template<typename ValueType> static
        unsigned int
        connectNodeParameters(ScenePtr const&,
                              unsigned int  masterNodeId,
                              const char*   masterParmName,
                              unsigned int  slaveNodeId,
                              const char*   slaveParmName,
                              int           priority)
        {
            std::stringstream sstr;
            sstr
                << "Failed to connect parameter '" << masterParmName << "' "
                << "to parameter '" << slaveParmName << "': "
                << "Not implemented for the requested type.";
            throw sys::Exception(sstr.str().c_str(), "Parameter Connection");
        }

        SPECIALIZE_PARM_TYPE_DECL(bool)
        SPECIALIZE_PARM_TYPE_DECL(unsigned int)
        SPECIALIZE_PARM_TYPE_DECL(int)
        SPECIALIZE_PARM_TYPE_DECL(char)
        SPECIALIZE_PARM_TYPE_DECL(size_t)
        SPECIALIZE_PARM_TYPE_DECL(math::Vector2i)
        SPECIALIZE_PARM_TYPE_DECL(math::Vector3i)
        SPECIALIZE_PARM_TYPE_DECL(math::Vector4i)
        SPECIALIZE_PARM_TYPE_DECL(float)
        SPECIALIZE_PARM_TYPE_DECL(math::Vector2f)
        SPECIALIZE_PARM_TYPE_DECL(math::Vector4f)
        SPECIALIZE_PARM_TYPE_DECL(math::Affine3f)
        SPECIALIZE_PARM_TYPE_DECL(std::string)
        SPECIALIZE_PARM_TYPE_DECL(xchg::IdSet)
        SPECIALIZE_PARM_TYPE_DECL(xchg::DataStream)
        SPECIALIZE_PARM_TYPE_DECL(xchg::BoundingBox)
        SPECIALIZE_PARM_TYPE_DECL(std::shared_ptr<xchg::Data>)
        SPECIALIZE_PARM_TYPE_DECL(xchg::ObjectPtr)

    protected:
        static
        unsigned int
        getId(const parm::BuiltIn&);

    public:
        bool
        getWorldBounds(unsigned int, xchg::BoundingBox&) const;

        bool
        getWorldBounds(const xchg::IdSet&, xchg::BoundingBox&) const;

#if defined(FLIRT_LOG_ENABLED)
    public:
        void
        logNodeParameters(unsigned int) const;
#endif //defined(FLIRT_LOG_ENABLED)
    };
}
