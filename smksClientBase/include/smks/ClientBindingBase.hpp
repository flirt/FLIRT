// ========================================================================= //
// Copyright 2016-2018 SUPAMONKS_STUDIO                                      //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
//                                                                           //
// ========================================================================= //
#pragma once

#include <memory>
#include <smks/xchg/NodeClass.hpp>
#include "smks/sys/configure_clientbase.hpp"

namespace osg
{
    class Object;
}

namespace smks
{
    namespace xchg
    {
        class ParmEvent;
    }
    namespace scn
    {
        class TreeNode;
    }

    class ClientBase;

    //<! bindings are classes whose primary goal is to monitor
    // and process any parameter event coming from the node they are
    // associated with.
    class FLIRT_CLIENTBASE_EXPORT ClientBindingBase
    {
    public:
        typedef std::shared_ptr<ClientBindingBase>  Ptr;
        typedef std::weak_ptr<ClientBindingBase>    WPtr;
    protected:
        typedef std::weak_ptr<ClientBase>       ClientWPtr;
        typedef std::weak_ptr<scn::TreeNode>    DataNodeWPtr;

    private:
        class PImpl;
        class ParmObserver;
    private:
        PImpl *_pImpl;

    protected:
        ClientBindingBase(ClientWPtr const&,
                    DataNodeWPtr const&);

        virtual
        void
        build(Ptr const&);

    public:
        virtual
        ~ClientBindingBase();

        ClientWPtr const&
        client() const;

        DataNodeWPtr const&
        dataNode() const;

        unsigned int
        id() const;

        xchg::NodeClass
        nodeClass() const;

        virtual
        void
        erase();

        virtual
        const osg::Object*
        asOsgObject() const
        {
            return nullptr;
        }

        virtual
        osg::Object*
        asOsgObject()
        {
            return nullptr;
        }

        //<! bindings are also made aware of *other* node creation and deletion
        // in order to anticipate node link creation and deletion
        virtual
        void
        handleNodeCreated(unsigned int)
        { }

        virtual
        void
        handleNodeDeleted(unsigned int)
        { }

    protected:
        virtual
        void
        handle(const xchg::ParmEvent&) = 0;
    };
}
