// ========================================================================= //
// Copyright 2016-2018 SUPAMONKS_STUDIO                                      //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
//                                                                           //
// ========================================================================= //
#pragma once

#define SPECIALIZE_ASSIGN_TYPE_DECL(_TYPE_)         \
    template<> static                               \
    unsigned int                                    \
    createAssign <_TYPE_> (const char*,             \
                           const _TYPE_&,           \
                           const char*,             \
                           TreeNodePtr const&);

#define SPECIALIZE_ASSIGN_TYPE_IMPL(_TYPE_)                                     \
    template<> inline                                                           \
    unsigned int                                                                \
    ClientBase::createAssign <_TYPE_> (const char*                  parmName,   \
                                       const _TYPE_&                parmValue,  \
                                       const char*                  name,       \
                                       scn::TreeNode::Ptr const&    parent)     \
    {                                                                           \
        scn::ParmAssign<_TYPE_>::Ptr const& node =                              \
            scn::ParmAssign<_TYPE_>::create(parmName,                           \
                                            parmValue,                          \
                                            name,                               \
                                            parent);                            \
        return node->id();                                                      \
    }
