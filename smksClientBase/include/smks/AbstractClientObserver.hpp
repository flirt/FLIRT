// ========================================================================= //
// Copyright 2016-2018 SUPAMONKS_STUDIO                                      //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
//                                                                           //
// ========================================================================= //
#pragma once

#include <memory>

namespace smks
{
    namespace xchg
    {
        class ParmEvent;
        class SceneEvent;
        class ClientEvent;
    }

    class ClientBase;

    class AbstractClientObserver
    {
    public:
        typedef std::shared_ptr<AbstractClientObserver> Ptr;
    public:
        virtual
        ~AbstractClientObserver()
        { }

        virtual
        void
        handle(ClientBase&,
               const xchg::ClientEvent&) = 0;

        virtual
        void
        handle(ClientBase&,
               const xchg::SceneEvent&) = 0;

        // callback invoked when client's scene root recieves
        // parameter-related event.
        // WARNING: said parameter event may not have been emitted
        // by the scene itself, and just pass through it.
        virtual
        void
        handleSceneParmEvent(ClientBase&,
                             const xchg::ParmEvent&) = 0;
    };
}
