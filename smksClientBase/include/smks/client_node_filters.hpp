// ========================================================================= //
// Copyright 2016-2018 SUPAMONKS_STUDIO                                      //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
//                                                                           //
// ========================================================================= //
#pragma once

#include <smks/parm/BuiltIn.hpp>

#include "smks/AbstractClientNodeFilter.hpp"

namespace smks
{
    ////////////////////////////////
    // struct CompoundClientNodeFilter
    ////////////////////////////////
    struct CompoundClientNodeFilter:
        public AbstractClientNodeFilter
    {
    public:
        typedef std::shared_ptr<CompoundClientNodeFilter> Ptr;

    private:
        std::vector<AbstractClientNodeFilter::Ptr> _filters;

    public:
        static inline
        Ptr
        create()
        {
            Ptr ptr(new CompoundClientNodeFilter());
            return ptr;
        }

    protected:
        CompoundClientNodeFilter():
            _filters()
        { }

    public:
        inline
        bool
        operator()(unsigned int         node,
                   const ClientBase&    client) const
        {
            for (size_t i = 0; i < _filters.size(); ++i)
                if (_filters[i]->operator()(node, client) == false)
                    return false;
            return true;
        }

        inline
        void
        add(AbstractClientNodeFilter::Ptr const& filter)
        {
            if (!filter ||
                filter.get() == this)
                return;
            for (size_t i = 0; i < _filters.size(); ++i)
                if (filter.get() == _filters[i].get())
                    return;
            _filters.push_back(filter);
        }

        inline
        void
        remove(AbstractClientNodeFilter::Ptr const& filter)
        {
            for (size_t i = 0; i < _filters.size(); ++i)
                if (filter.get() == _filters[i].get())
                {
                    std::swap(_filters[i], _filters.back());
                    _filters.pop_back();
                    return;
                }
        }
    };

    ///////////////////////////
    // struct FilterClientNodeByClass
    ///////////////////////////
    struct FilterClientNodeByClass:
        public AbstractClientNodeFilter
    {
    public:
        typedef std::shared_ptr<FilterClientNodeByClass> Ptr;

    private:
        const xchg::NodeClass _query;

    public:
        static inline
        Ptr
        create(xchg::NodeClass query)
        {
            Ptr ptr(new FilterClientNodeByClass(query));
            return ptr;
        }

    protected:
        explicit
        FilterClientNodeByClass(xchg::NodeClass query):
            _query(query)
        { }

    public:
        bool
        operator()(unsigned int         node,
                   const ClientBase&    client) const
        {
            const xchg::NodeClass cl = client.getNodeClass(node);
            return (_query & cl) != 0 &&
                (~_query & cl) == 0;
        }
    };

    /////////////////////////////////
    // struct FilterClientNodeByBuiltIn
    /////////////////////////////////
    template<typename ValueType>
    struct FilterClientNodeByBuiltIn:
        public AbstractClientNodeFilter
    {
    public:
        typedef std::shared_ptr<FilterClientNodeByBuiltIn> Ptr;

    private:
        const parm::BuiltIn*    _builtIn;   // does not own it
        const ValueType         _parmValue;

    public:
        static inline
        Ptr
        create(const parm::BuiltIn& builtIn,
               const ValueType&     parmValue)
        {
            Ptr ptr(new FilterClientNodeByBuiltIn(builtIn, parmValue));
            return ptr;
        }

    protected:
        FilterClientNodeByBuiltIn(const parm::BuiltIn&  builtIn,
                                  const ValueType&      parmValue):
            _builtIn    (&builtIn),
            _parmValue  (parmValue)
        {
#if defined(_DEBUG)
            if (!_builtIn->compatibleWith<ValueType>())
                throw sys::Exception(
                    "Inconsistent built-in based filter definition.",
                    "Built-In Based Node Filter");
#endif // defined(_DEBUG)
        }

    public:
        inline
        bool
        operator()(unsigned int node,
                   const ClientBase& client) const
        {
            ValueType parmValue;
            client.getNodeParameter<ValueType>(*_builtIn, parmValue, node);
            return xchg::equal<ValueType>(parmValue, _parmValue);
        }
    };

    /////////////////////////////
    // class FilterClientNodeByName
    /////////////////////////////
    class FLIRT_CLIENTBASE_EXPORT FilterClientNodeByName:
        public AbstractClientNodeFilter
    {
    public:
        typedef std::shared_ptr<FilterClientNodeByName> Ptr;
    private:
        class PImpl;
    private:
        PImpl* _pImpl;
    public:
        static inline
        Ptr
        create(const char*  name,
               bool         isQueryRegex,
               bool         useFullName = false)
        {
            Ptr ptr(new FilterClientNodeByName(name, isQueryRegex, useFullName));
            return ptr;
        }
    protected:
        FilterClientNodeByName(const char*  query,
                               bool         isQueryRegex,
                               bool         useFullName);
    public:
        ~FilterClientNodeByName();
    protected:
        bool
        operator()(unsigned int node,
                   const ClientBase&) const;
    };
}
