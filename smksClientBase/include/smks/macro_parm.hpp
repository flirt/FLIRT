// ========================================================================= //
// Copyright 2016-2018 SUPAMONKS_STUDIO                                      //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
//                                                                           //
// ========================================================================= //
#pragma once

#define SPECIALIZE_PARM_TYPE_DECL(_CTYPE_)                                                                                                                      \
    template <> static bool         nodeParameterExistsAs   <_CTYPE_> (const char*,  TreeNodePtr const&, unsigned int, unsigned int*);                          \
    template <> static bool         nodeParameterExistsAs   <_CTYPE_> (unsigned int, TreeNodePtr const&, unsigned int, unsigned int*);                          \
    template <> static unsigned int setNodeParameter        <_CTYPE_> (const char*,             const _CTYPE_&,             TreeNodePtr const&);                \
    template <> static unsigned int setNodeParameter        <_CTYPE_> (const parm::BuiltIn&,    const _CTYPE_&,             TreeNodePtr const&);                \
    template <> static void         updateNodeParameter     <_CTYPE_> (unsigned int,            const _CTYPE_&,             TreeNodePtr const&);                \
    template <> static bool         getNodeParameter        <_CTYPE_> (const char*,             _CTYPE_&, const _CTYPE_&,   TreeNodePtr const&, unsigned int);  \
    template <> static bool         getNodeParameter        <_CTYPE_> (unsigned int,            _CTYPE_&, const _CTYPE_&,   TreeNodePtr const&, unsigned int);  \
    template <> static bool         getNodeParameter        <_CTYPE_> (const parm::BuiltIn&,    _CTYPE_&,                   TreeNodePtr const&, unsigned int);  \
    template <> static unsigned int connectNodeParameters   <_CTYPE_> (ScenePtr const&, unsigned int, const char*, unsigned int, const char*, int);

#define SPECIALIZE_PARM_TYPE_IMPL(_CTYPE_)                                                      \
    template <>                                                                                 \
    bool                                                                                        \
    ClientBase::nodeParameterExistsAs <_CTYPE_> (const char*                parmName,           \
                                                 scn::TreeNode::Ptr const&  fromNode,           \
                                                 unsigned int               upToNodeId,         \
                                                 unsigned int               *foundInNodeId)     \
    {                                                                                           \
        scn::TreeNode::Ptr foundInNode = nullptr;                                               \
        if (fromNode &&                                                                         \
            fromNode->parameterExistsAs <_CTYPE_> (parmName, upToNodeId, &foundInNode))         \
        {                                                                                       \
            if (foundInNodeId)                                                                  \
                *foundInNodeId = (foundInNode ? foundInNode->id() : 0);                         \
            return true;                                                                        \
        }                                                                                       \
        if (foundInNodeId)                                                                      \
            *foundInNodeId = 0;                                                                 \
        return false;                                                                           \
    }                                                                                           \
                                                                                                \
    template <>                                                                                 \
    bool                                                                                        \
    ClientBase::nodeParameterExistsAs <_CTYPE_> (unsigned int               parmId,             \
                                                 scn::TreeNode::Ptr const&  fromNode,           \
                                                 unsigned int               upToNodeId,         \
                                                 unsigned int               *foundInNodeId)     \
    {                                                                                           \
        scn::TreeNode::Ptr foundInNode = nullptr;                                               \
        if (fromNode &&                                                                         \
            fromNode->parameterExistsAs <_CTYPE_> (parmId, upToNodeId, &foundInNode))           \
        {                                                                                       \
            if (foundInNodeId)                                                                  \
                *foundInNodeId = (foundInNode ? foundInNode->id() : 0);                         \
            return true;                                                                        \
        }                                                                                       \
        if (foundInNodeId)                                                                      \
            *foundInNodeId = 0;                                                                 \
        return false;                                                                           \
    }                                                                                           \
                                                                                                \
    template <>                                                                                 \
    bool                                                                                        \
    ClientBase::getNodeParameter <_CTYPE_> (const parm::BuiltIn&        parm,                   \
                                            _CTYPE_&                    parmValue,              \
                                            scn::TreeNode::Ptr const&   fromNode,               \
                                            unsigned int                upToNodeId)             \
    {                                                                                           \
        if (fromNode)                                                                           \
            return fromNode->getParameter <_CTYPE_> (parm, parmValue, upToNodeId);              \
        if (!parm.defaultValue <_CTYPE_> (parmValue))                                           \
            parmValue = _CTYPE_();                                                              \
        return false;                                                                           \
    }                                                                                           \
                                                                                                \
    template <>                                                                                 \
    bool                                                                                        \
    ClientBase::getNodeParameter <_CTYPE_> (const char*                 parmName,               \
                                            _CTYPE_&                    parmValue,              \
                                            const _CTYPE_&              defaultValue,           \
                                            scn::TreeNode::Ptr const&   fromNode,               \
                                            unsigned int                upToNodeId)             \
    {                                                                                           \
        if (fromNode)                                                                           \
            return fromNode->getParameter <_CTYPE_> (                                           \
                parmName, parmValue, defaultValue, upToNodeId);                                 \
        parm::BuiltIn::Ptr const& builtIn = parm::builtIns().find(parmName);                    \
        if (builtIn == nullptr || !builtIn->defaultValue <_CTYPE_> (parmValue))                 \
            parmValue = defaultValue;                                                           \
        return false;                                                                           \
    }                                                                                           \
                                                                                                \
    template <>                                                                                 \
    bool                                                                                        \
    ClientBase::getNodeParameter <_CTYPE_> (unsigned int                parmId,                 \
                                            _CTYPE_&                    parmValue,              \
                                            const _CTYPE_&              defaultValue,           \
                                            scn::TreeNode::Ptr const&   fromNode,               \
                                            unsigned int                upToNodeId)             \
    {                                                                                           \
        if (fromNode)                                                                           \
            return fromNode->getParameter <_CTYPE_> (                                           \
                parmId, parmValue, defaultValue, upToNodeId);                                   \
        parm::BuiltIn::Ptr const& builtIn = parm::builtIns().find(parmId);                      \
        if (builtIn == nullptr || !builtIn->defaultValue <_CTYPE_> (parmValue))                 \
            parmValue = defaultValue;                                                           \
        return false;                                                                           \
    }                                                                                           \
                                                                                                \
    template <>                                                                                 \
    unsigned int                                                                                \
    ClientBase::setNodeParameter <_CTYPE_> (const parm::BuiltIn&        parm,                   \
                                            const _CTYPE_&              parmValue,              \
                                            scn::TreeNode::Ptr const&   node)                   \
    {                                                                                           \
        return node ? node->setParameter <_CTYPE_> (parm, parmValue) : 0;                       \
    }                                                                                           \
                                                                                                \
    template <>                                                                                 \
    unsigned int                                                                                \
    ClientBase::setNodeParameter <_CTYPE_> (const char*                 parmName,               \
                                            const _CTYPE_&              parmValue,              \
                                            scn::TreeNode::Ptr const&   node)                   \
    {                                                                                           \
        return node ? node->setParameter <_CTYPE_> (parmName, parmValue) : 0;                   \
    }                                                                                           \
                                                                                                \
    template <>                                                                                 \
    void                                                                                        \
    ClientBase::updateNodeParameter <_CTYPE_> (unsigned int                 parmId,             \
                                               const _CTYPE_&               parmValue,          \
                                               scn::TreeNode::Ptr const&    node)               \
    {                                                                                           \
        if (node)                                                                               \
            node->updateParameter <_CTYPE_> (parmId, parmValue);                                \
    }                                                                                           \
                                                                                                \
    template<>                                                                                  \
    unsigned int                                                                                \
    ClientBase::connectNodeParameters <_CTYPE_> (scn::Scene::Ptr const& scene,                  \
                                                 unsigned int           masterNodeId,           \
                                                 const char*            masterParmName,         \
                                                 unsigned int           slaveNodeId,            \
                                                 const char*            slaveParmName,          \
                                                 int                    priority)               \
    {                                                                                           \
        return scene                                                                            \
            ? scene->connect <_CTYPE_> (                                                        \
                masterNodeId, masterParmName, slaveNodeId, slaveParmName, priority)             \
            : 0;                                                                                \
    }
