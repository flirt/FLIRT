// ========================================================================= //
// Copyright 2016-2018 SUPAMONKS_STUDIO                                      //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
//                                                                           //
// ========================================================================= //
#include "smks/ClientBase.hpp"
#include "std/to_string_client.hpp"

namespace smks
{
    static inline
    std::ostream&
    to_string(std::ostream&         out,
              const ClientBase&,
              unsigned int          node,
              const std::string&    indent,
              bool                  lastChild);
}

inline
std::ostream&
smks::to_string(std::ostream&       out,
                const ClientBase&   client,
                unsigned int        node,
                const std::string&  indent,
                bool                lastChild)
{
    out << indent;

    std::string str = indent;
    if (lastChild)
    {
        out << "`--";
        str += "  ";
    }
    else
    {
        out << "|--";
        str += "|  ";
    }
    out << " " << client.getNodeName(node) << "\n";

    const size_t numChildren = client.getNodeNumChildren(node);
    for (size_t i = 0; i < numChildren; ++i)
    {
        const unsigned int child = client.getNodeChild(node, i);
        if (child)
            smks::to_string(out, client, child, str, i == numChildren - 1);
    }

    return out;
}

std::ostream&
std::to_string(std::ostream&            out,
               const smks::ClientBase&  client)
{
    return smks::to_string(out, client, client.sceneId(), "", true);
}
