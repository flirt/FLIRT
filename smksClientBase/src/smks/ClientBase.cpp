// ========================================================================= //
// Copyright 2016-2018 SUPAMONKS_STUDIO                                      //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
//                                                                           //
// ========================================================================= //
#include <iostream>
#include <deque>
#include <boost/unordered_map.hpp>

#include <std/to_string_xchg.hpp>

#include <smks/parm/BuiltIn.hpp>
#include <smks/xchg/VectorPtr.hpp>
#include <smks/xchg/IdSet.hpp>
#include <smks/xchg/DataStream.hpp>
#include <smks/xchg/Data.hpp>
#include <smks/xchg/ObjectPtr.hpp>
#include <smks/xchg/BoundingBox.hpp>
#include <smks/xchg/SceneEvent.hpp>
#include <smks/xchg/ParmEvent.hpp>
#include <smks/xchg/ClientEvent.hpp>
#include <smks/xchg/AbstractProgressCallbacks.hpp>
#include <smks/xchg/render_pass.hpp>

#include <smks/sys/Exception.hpp>
#include <smks/sys/ResourceDatabase.hpp>
#include <smks/sys/Log.hpp>

#include <smks/scn/TreeNode.hpp>
#include <smks/scn/Scene.hpp>
#include <smks/scn/TreeNodeVisitor.hpp>
#include <smks/scn/TreeNodeSelector.hpp>
#include <smks/scn/ParmAssign.hpp>
#include <smks/scn/Archive.hpp>
#include <smks/scn/Camera.hpp>
#include <smks/scn/Xform.hpp>
#include <smks/scn/Material.hpp>
#include <smks/scn/SubSurface.hpp>
#include <smks/scn/Texture.hpp>
#include <smks/scn/Light.hpp>
#include <smks/scn/FrameBuffer.hpp>
#include <smks/scn/Denoiser.hpp>
#include <smks/scn/ToneMapper.hpp>
#include <smks/scn/Renderer.hpp>
#include <smks/scn/RenderPass.hpp>
#include <smks/scn/Integrator.hpp>
#include <smks/scn/SamplerFactory.hpp>
#include <smks/scn/PixelFilter.hpp>
#include <smks/scn/RenderAction.hpp>
#include <smks/scn/IdAssign.hpp>
#include <smks/scn/AbstractSceneObserver.hpp>
#include <smks/scn/AbstractParmObserver.hpp>

#include "smks/ClientBase.hpp"
#include "smks/ClientBindingBase.hpp"
#include "smks/AbstractClientNodeFilter.hpp"
#include "smks/AbstractClientObserver.hpp"
#include "smks/AbstractClientNodeObserver.hpp"

namespace smks
{
    /////////////////////////////////
    // class ClientBase::SceneObserver
    /////////////////////////////////
    class ClientBase::SceneObserver:
        public scn::AbstractSceneObserver
    {
    public:
        typedef std::shared_ptr<SceneObserver> Ptr;
    private:
        ClientBase& _self;
    public:
        static inline
        Ptr
        create(ClientBase& self)
        {
            Ptr ptr(new SceneObserver(self));
            return ptr;
        }
    protected:
        explicit inline
        SceneObserver(ClientBase& self):
            _self(self)
        { }

        inline
        void
        handle(scn::Scene&, const xchg::SceneEvent&);
    };

    //////////////////////////////////////
    // class ClientBase::SceneParmObserver
    //////////////////////////////////////
    class ClientBase::SceneParmObserver:
        public scn::AbstractParmObserver
    {
    public:
        typedef std::shared_ptr<SceneParmObserver> Ptr;
    private:
        ClientBase& _self;
    public:
        static inline
        Ptr
        create(ClientBase& self)
        {
            Ptr ptr(new SceneParmObserver(self));
            return ptr;
        }
    protected:
        explicit
        SceneParmObserver(ClientBase& self):
            _self(self)
        { }

        inline
        void
        handle(scn::TreeNode&, const xchg::ParmEvent&);
    };

    /////////////////////////////////////
    // class ClientBase::ParmObserverWrap
    /////////////////////////////////////
    class ClientBase::ParmObserverWrap:
        public scn::AbstractParmObserver
    {
    public:
        typedef std::shared_ptr<ParmObserverWrap> Ptr;
    private:
        typedef AbstractClientNodeObserver Wrapped;
    private:
        ClientBase&  _self;
        Wrapped::Ptr _observer;
    public:
        static inline
        Ptr
        create(ClientBase&         self,
               Wrapped::Ptr const& wrapped)
        {
            Ptr ptr(new ParmObserverWrap(self, wrapped));
            return ptr;
        }
    protected:
        ParmObserverWrap(ClientBase&         self,
                         Wrapped::Ptr const& observer):
            _self       (self),
            _observer   (observer)
        { }
    public:
        inline
        Wrapped::Ptr const&
        observer() const
        {
            return _observer;
        }
        inline
        void
        handle(scn::TreeNode& node, const xchg::ParmEvent& evt)
        {
            if (_observer)
                _observer->handle(_self, node.id(), evt);
        }
    };
    /////////////////////////////////////////

    /////////////////////////
    // class ClientBase::PImpl
    /////////////////////////
    class ClientBase::PImpl
    {
    protected:
        typedef boost::unordered_map<unsigned int, ClientBindingBase::Ptr> BindingMap;
        typedef xchg::VectorPtr<AbstractClientObserver>                    ClientObservers;
        typedef std::vector<ParmObserverWrap::Ptr>                         ParmObserverWraps;
        typedef boost::unordered_map<unsigned int, ParmObserverWraps>      ParmObserverWrapMap;

    private:
        ClientBase&                     _self;

        ClientBase::WPtr                _wptr;  //<! useful to create shared pointers from instance
        scn::Scene::Ptr                 _scene;
        BindingMap                      _bindings;

        SceneObserver::Ptr              _sceneObserver;     //<! observer of the data scene
        SceneParmObserver::Ptr          _sceneParmObserver; //<! observer of the data scene's main parameter set

        std::deque<xchg::ClientEvent>   _pendingClientEvents;
        std::deque<xchg::SceneEvent>    _pendingSceneEvents;
        std::deque<xchg::ParmEvent>     _pendingParmEvents;

        ClientObservers                 _extClientObservers;    //<! external client observers on instance
        ParmObserverWrapMap             _extParmObservers;      //<! external parameter observers

        sys::ResourceDatabase::Ptr      _resources;

    public:
        explicit
        PImpl(ClientBase& self):
            _self               (self),
            _wptr               (),
            _scene              (nullptr),
            _bindings           (),
            _sceneObserver      (SceneObserver::create(self)),
            _sceneParmObserver  (SceneParmObserver::create(self)),
            _pendingClientEvents(),
            _pendingSceneEvents (),
            _pendingParmEvents  (),
            _extClientObservers (),
            _extParmObservers   (),
            _resources          (nullptr)
        {
            _extClientObservers.reserve(8);
        }

        inline
        ~PImpl()
        {
            dispose();

            _sceneObserver      = nullptr;
            _sceneParmObserver  = nullptr;
            _pendingParmEvents  .clear();
            _pendingSceneEvents .clear();
            _pendingClientEvents.clear();
            _extClientObservers .clear();
            _extParmObservers   .clear();

            _resources = nullptr;
            _wptr.reset();
        }

        inline
        void
        build(ClientBase::Ptr const& ptr)
        {
            assert(ptr);
            _wptr = ptr;
        }

        inline
        void
        dispose()
        {
            setScene(nullptr);
            assert(!_scene);
            assert(_bindings.empty());
        }

        inline
        ClientBase::WPtr const&
        self()
        {
            return _wptr;
        }

        inline
        void
        setScene(scn::Scene::Ptr const&);

        inline
        scn::Scene::Ptr const&
        scene() const
        {
            return _scene;
        }

        inline
        sys::ResourceDatabase::Ptr const&
        getOrCreateResourceDatabase();

        inline
        void
        setSharedResourceDatabase(sys::ResourceDatabase::Ptr const& value)
        {
            _resources = value;
        }

        inline
        sys::ResourceDatabase*
        getResourceDatabase()
        {
            return _resources.get();
        }

        inline
        const sys::ResourceDatabase*
        getResourceDatabase() const
        {
            return _resources.get();
        }

        inline
        void
        getIds(xchg::IdSet&,
               bool         accumulate,
               AbstractClientNodeFilter::Ptr const&,
               size_t       numRegexes,
               const char** regexes,
               unsigned int startId,
               bool         upwards) const;

        inline
        void
        getBindingKeys(xchg::IdSet&) const;

        inline
        ClientBindingBase::Ptr
        getBinding(unsigned int id) const;

        inline
        unsigned int
        registerBinding(ClientBindingBasePtr const&);
        inline
        void
        deregisterBinding(unsigned int);

        inline
        bool
        getTimeRange(float& minTime, float& maxTime) const;

        inline
        void
        registerObserver(AbstractClientObserver::Ptr const&);
        inline
        void
        deregisterObserver(AbstractClientObserver::Ptr const&);

        inline
        void
        registerObserver(AbstractClientNodeObserver::Ptr const&,
                         unsigned int node);
        inline
        void
        deregisterObserver(AbstractClientNodeObserver::Ptr const&,
                           unsigned int node);
    private:
        inline
        ParmObserverWrap::Ptr
        getWrap(AbstractClientNodeObserver::Ptr const&,
                unsigned int node) const;

    public:
        inline
        void
        handleSceneEvent(scn::Scene&, const xchg::SceneEvent&);

        inline
        void
        handleSceneParmEvent(const xchg::ParmEvent&);

#if defined(FLIRT_LOG_ENABLED)
        inline
        void
        logNodeParameters(unsigned int) const;
#endif // defined(FLIRT_LOG_ENABLED)
    private:
        inline
        void
        handleNodeCreated(unsigned int nodeId,
                          unsigned int parentId);

        inline
        void
        handleNodeDeleted(unsigned int nodeId,
                          unsigned int parentId);

        inline
        void
        destroyBindingsFrom(scn::TreeNode::Ptr const&);

        //inline
        //void
        //destroyBindingsFrom(ClientBindingBase::Ptr&);

        inline
        void
        deliver(const xchg::ClientEvent&);
    };
}

namespace smks
{
SPECIALIZE_PARM_TYPE_IMPL(bool)
SPECIALIZE_PARM_TYPE_IMPL(unsigned int)
SPECIALIZE_PARM_TYPE_IMPL(int)
SPECIALIZE_PARM_TYPE_IMPL(char)
SPECIALIZE_PARM_TYPE_IMPL(size_t)
SPECIALIZE_PARM_TYPE_IMPL(smks::math::Vector2i)
SPECIALIZE_PARM_TYPE_IMPL(smks::math::Vector3i)
SPECIALIZE_PARM_TYPE_IMPL(smks::math::Vector4i)
SPECIALIZE_PARM_TYPE_IMPL(float)
SPECIALIZE_PARM_TYPE_IMPL(smks::math::Vector2f)
SPECIALIZE_PARM_TYPE_IMPL(smks::math::Vector4f)
SPECIALIZE_PARM_TYPE_IMPL(smks::math::Affine3f)
SPECIALIZE_PARM_TYPE_IMPL(std::string)
SPECIALIZE_PARM_TYPE_IMPL(smks::xchg::BoundingBox)
SPECIALIZE_PARM_TYPE_IMPL(smks::xchg::IdSet)
SPECIALIZE_PARM_TYPE_IMPL(smks::xchg::DataStream)
SPECIALIZE_PARM_TYPE_IMPL(smks::xchg::Data::Ptr)
SPECIALIZE_PARM_TYPE_IMPL(smks::xchg::ObjectPtr)

SPECIALIZE_ASSIGN_TYPE_IMPL(bool)
SPECIALIZE_ASSIGN_TYPE_IMPL(unsigned int)
SPECIALIZE_ASSIGN_TYPE_IMPL(int)
SPECIALIZE_ASSIGN_TYPE_IMPL(size_t)
SPECIALIZE_ASSIGN_TYPE_IMPL(float)
SPECIALIZE_ASSIGN_TYPE_IMPL(std::string)
}

using namespace smks;

/////////////////////////////////
// class ClientBase::SceneObserver
/////////////////////////////////
void
ClientBase::SceneObserver::handle(scn::Scene&             scene,
                                  const xchg::SceneEvent& evt)
{
    _self.handleSceneEvent(scene, evt); //-> call virtual handler
}

//////////////////////////////////////
// class ClientBase::SceneParmObserver
//////////////////////////////////////
void
ClientBase::SceneParmObserver::handle(scn::TreeNode&         node,
                                      const xchg::ParmEvent& evt)
{
    assert(node.id() == _self.sceneId());
    _self.handleSceneParmEvent(evt);    //-> call virtual handler
}

/////////////////////////
// class ClientBase::PImpl
/////////////////////////
void
ClientBase::PImpl::setScene(scn::Scene::Ptr const& scene)
{
    ///////////////////////////////
    // class BindingCreationVisitor
    ///////////////////////////////
    class BindingCreationVisitor:
        public scn::TreeNodeVisitor
    {
    private:
        ClientBase& _self;
    public:
        explicit inline
        BindingCreationVisitor(ClientBase& self):
            scn::TreeNodeVisitor(scn::TreeNodeVisitor::DESCENDING),
            _self               (self)
        { }

        virtual inline
        void
        apply(scn::TreeNode& n)
        {
            _self.registerBinding(
                _self.createBinding(
                    n.self()));
            traverse(n);
        }
    };
    ///////////////////////////////

    //--------------------------------------------------------
    // remove bindings
    if (_scene)
        destroyBindingsFrom(_scene);    // bindings are destroyed recursively (depth-first traversal)

    // bindings should be empty at this point though...
    while (!_bindings.empty())
    {
        ClientBindingBase::Ptr const& bind = _bindings.begin()->second;
        if (bind)
            destroyBindingsFrom(bind->dataNode().lock());
    }

    //--------------------------------------------------------
    // unset previous scene
    if (_scene)
    {
        //###################################
        deliver(xchg::ClientEvent::sceneUnset(_scene->id()));
        //###################################
        _self.deregisterGUIEventCallbacks(*_scene);
        _scene->TreeNode::deregisterObserver(_sceneParmObserver);
        _scene->deregisterObserver(_sceneObserver);
        _scene = nullptr;
    }
    //--------------------------------------------------------
    // set new scene
    if (scene)
    {
        BindingCreationVisitor visitor(_self);

        _scene = scene;
        _scene->accept(visitor); //<! fills _bindings and set _osgRoot
        _scene->registerObserver(_sceneObserver);
        _scene->TreeNode::registerObserver(_sceneParmObserver);
        _self.registerGUIEventCallbacks(*scene);
        //##################################
        deliver(xchg::ClientEvent::sceneSet(scene->id()));
        //##################################
    }
}

void
ClientBase::PImpl::destroyBindingsFrom(scn::TreeNode::Ptr const& dataNode)
{
    if (!dataNode)
        return;

    FLIRT_LOG(LOG(DEBUG)
        << "destroying binding from node '" << dataNode->name() << "' "
        << "(ID = " << dataNode->id() << ", "
        << "type = " << std::to_string(dataNode->nodeClass()) << ") "
        << "client = " << (void*)this << "...";)

    // bindings stemming from associated node's children must be destroyed in a depth-first fashion
    for (size_t i = 0; i < dataNode->numChildren(); ++i)
        destroyBindingsFrom(dataNode->child(i));

    //---
    // notify other bindings of the imminent node deletion
    for (BindingMap::iterator it = _bindings.begin(); it != _bindings.end(); ++it)
        if (it->first != dataNode->id())
            it->second->handleNodeDeleted(dataNode->id());
    //---

    ClientBindingBase::Ptr const& bind = getBinding(dataNode->id());
    //---
    if (bind)
    {
        _self.destroyBinding(bind);
        bind->erase();
    }
    //---
    _self.deregisterBinding(dataNode->id());

    FLIRT_LOG(LOG(DEBUG)
        << "binding from node '" << dataNode->name() << "' "
        << "(ID = " << dataNode->id() << ", "
        << "type = " << std::to_string(dataNode->nodeClass()) << ") "
        << "client = " << (void*)this << " destroyed.";)

}

unsigned int
ClientBase::PImpl::registerBinding(ClientBindingBase::Ptr const& bind)
{
    if (!bind ||
        bind->id() == 0)
        return 0;

    //##################################
    _bindings.insert(std::pair<unsigned int, ClientBindingBase::Ptr>(bind->id(), bind));
    deliver(xchg::ClientEvent::bindingCreated(bind->id()));
    //##################################
    //---
    // notify other bindings of the recent node creation
    for (BindingMap::iterator it = _bindings.begin(); it != _bindings.end(); ++it)
        if (it->first != bind->id())
            it->second->handleNodeCreated(bind->id());
    //---

    return bind->id();
}

void
ClientBase::PImpl::deregisterBinding(unsigned int id)
{
    BindingMap::iterator const& it = _bindings.find(id);

    ////---
    //// notify other bindings of the imminent node deletion
    //for (BindingMap::iterator it = _bindings.begin(); it != _bindings.end(); ++it)
    //  if (it->first != id)
    //      it->second->handleNodeDeleted(id);
    ////---
    if (it != _bindings.end())
    {
        //##################################
        _bindings.erase(it);
        deliver(xchg::ClientEvent::bindingDeleted(id));
        //##################################
    }
}

void
ClientBase::PImpl::getBindingKeys(xchg::IdSet& keys) const
{
    keys.clear();
    for (BindingMap::const_iterator it = _bindings.begin(); it != _bindings.end(); ++it)
        keys.insert(it->first);
}

ClientBindingBase::Ptr
ClientBase::PImpl::getBinding(unsigned int id) const
{
    BindingMap::const_iterator const& it = _bindings.find(id);

    return it != _bindings.end()
        ? it->second
        : nullptr;
}

void
ClientBase::PImpl::getIds(xchg::IdSet&                         ids,
                          bool                                 accumulate,
                          AbstractClientNodeFilter::Ptr const& filter,
                          size_t                               numRegexes,
                          const char**                         regexes,
                          unsigned int                         startId,
                          bool                                 upwards) const
{
    ////////////////////////////
    // struct NodeFilterWrap
    ////////////////////////////
    struct NodeFilterWrap:
        public scn::AbstractTreeNodeFilter
    {
    public:
        typedef std::shared_ptr<NodeFilterWrap> Ptr;

    private:
        const AbstractClientNodeFilter::Ptr _filter;
        const ClientBase&               _client;

    public:
        static
        Ptr
        create(AbstractClientNodeFilter::Ptr const& filter,
               const ClientBase&                    client)
        {
            Ptr ptr(new NodeFilterWrap(filter, client));
            return ptr;
        }

    protected:
        NodeFilterWrap(AbstractClientNodeFilter::Ptr const& filter,
                       const ClientBase&                client):
            _filter (filter),
            _client (client)
        { }

        inline
        bool
        operator()(const scn::TreeNode& n) const
        {
            return _filter &&
                (*_filter)(n.id(), _client);
        }
    };
    ////////////////////////////

    if (!_scene)
        return;

    scn::Scene::IdGetterOptions options;

    options.accumulate  (accumulate);
    options.filter      (filter ? NodeFilterWrap::create(filter, _self) : nullptr);
    for (size_t i = 0; i < numRegexes; ++i)
        if (regexes[i])
            options.addRegex(regexes[i]);
    options.fromId      (startId);
    options.upwards     (upwards);

    _scene->getIds(ids, &options);
}

void
ClientBase::PImpl::registerObserver(AbstractClientObserver::Ptr const& obs)
{
    _extClientObservers.add(obs);
}

void
ClientBase::PImpl::deregisterObserver(AbstractClientObserver::Ptr const& obs)
{
    _extClientObservers.remove(obs);
}

void
ClientBase::PImpl::registerObserver(AbstractClientNodeObserver::Ptr const&  obs,
                                    unsigned int                            nodeId)
{
    if (!obs)
        return;

    scn::TreeNode::Ptr const& node = _self.getNode(nodeId);
    if (!node)
        return;

    ParmObserverWrapMap::const_iterator const& it = _extParmObservers.find(nodeId);
    if (it == _extParmObservers.end())
        _extParmObservers.insert(
            std::pair<unsigned int, ParmObserverWraps>(nodeId, ParmObserverWraps()));
    else
    {
        // abort if observer is already registered to node
        const ParmObserverWraps& wraps = it->second;
        for (size_t i = 0; i < wraps.size(); ++i)
            if (wraps[i] &&
                wraps[i]->observer().get() == obs.get())
                return;
    }

    ParmObserverWrap::Ptr const& wrap = ParmObserverWrap::create(_self, obs);

    _extParmObservers[nodeId].push_back(wrap);
    node->registerObserver(wrap);
}

void
ClientBase::PImpl::deregisterObserver(AbstractClientNodeObserver::Ptr const& obs,
                                      unsigned int                           nodeId)
{
    if (!obs)
        return;

    scn::TreeNode::Ptr const& node = _self.getNode(nodeId);
    if (!node)
        return;

    ParmObserverWrap::Ptr wrap = nullptr;

    ParmObserverWrapMap::iterator const& it = _extParmObservers.find(nodeId);
    if (it != _extParmObservers.end())
    {
        ParmObserverWraps& wraps = it->second;
        for (size_t i = 0; i < wraps.size(); ++i)
            if (wraps[i] &&
                wraps[i]->observer().get() == obs.get())
            {
                wrap = wraps[i];    // observer was indeed registered to node
                std::swap(wraps[i], wraps.back());
                wraps.pop_back();
                break;
            }
        if (wraps.empty())
            _extParmObservers.erase(it);
    }

    if (wrap)
        node->deregisterObserver(wrap);
}

void
ClientBase::PImpl::deliver(const xchg::ClientEvent& evt)
{
    assert(!evt.empty());
    for (size_t i = 0; i < _pendingClientEvents.size(); ++i)
        if (_pendingClientEvents[i] == evt)
            return; // event being already stored in 'queue'

    _pendingClientEvents.push_back(evt);    // MUTEXME
    if (_pendingClientEvents.size() > 1)    //<! event delivery must be delayed
        return;

    while (!_pendingClientEvents.empty())
    {
        //----------------------------------
        // notify all observers
        for (size_t i = 0; i < _extClientObservers.size(); ++i)
            if (_extClientObservers[i])
                _extClientObservers[i]
                    ->handle(_self, _pendingClientEvents.front());
        //----------------------------------
        _pendingClientEvents.pop_front();   //<! top event has been processed, can pop it now
    }
}

///////////////////////
// scene event handling
///////////////////////
void
ClientBase::PImpl::handleSceneEvent(scn::Scene&             scene,
                                    const xchg::SceneEvent& evt)
{
    assert(!evt.empty());
    for (size_t i = 0; i < _pendingSceneEvents.size(); ++i)
        if (_pendingSceneEvents[i] == evt)
            return; // event being already stored in 'queue'

    _pendingSceneEvents.push_back(evt); // MUTEXME
    if (_pendingSceneEvents.size() > 1) //<! event delivery must be delayed
        return;

    while (!_pendingSceneEvents.empty())
    {
        //---------------------------------------------------------------
        // first, notify external observers before actually removing data
        for (size_t i = 0; i < _extClientObservers.size(); ++i)
        {
            assert(_extClientObservers[i]);
            _extClientObservers[i]
                ->handle(_self, _pendingSceneEvents.front());
        }

        switch (evt.type())
        {
        case xchg::SceneEvent::NODE_CREATED:
            handleNodeCreated(
                _pendingSceneEvents.front().node(),
                _pendingSceneEvents.front().parent());
            break;
        case xchg::SceneEvent::NODE_DELETED:
            handleNodeDeleted(
                _pendingSceneEvents.front().node(),
                _pendingSceneEvents.front().parent());
            break;
        default:
            break;
        }
        //---------------------------------------------------------------
        _pendingSceneEvents.pop_front();    //<! top event has been processed, can pop it now
    }
}

void
ClientBase::PImpl::handleNodeCreated(unsigned int nodeId,
                                     unsigned int parentId)
{
    if (nodeId == 0 ||
        !_scene ||
        !_scene->hasDescendant(nodeId))
        return;

    _self.registerBinding(
        _self.createBinding(
            _scene->descendantById(nodeId)));
}

void
ClientBase::PImpl::handleNodeDeleted(unsigned int nodeId,
                                     unsigned int parentId)
{
    // destroy binding associated with deleted node
    {
        BindingMap::iterator it = _bindings.find(nodeId);
        if (it != _bindings.end())
            destroyBindingsFrom(it->second->dataNode().lock());
    }

    // deregister all parameter observers targetting the node
    {
        ParmObserverWrapMap::iterator const& it = _extParmObservers.find(nodeId);
        if (it != _extParmObservers.end())
        {
            while (true)
            {
                ParmObserverWraps& wraps = it->second;
                if (wraps.empty())
                    break;
                deregisterObserver(wraps.front()->observer(), nodeId);
            }
            _extParmObservers.erase(it);
        }
    }
}

//////////////////////////
// node parameter handling
//////////////////////////
void
ClientBase::PImpl::handleSceneParmEvent(const xchg::ParmEvent& evt)
{
    for (size_t i = 0; i < _pendingParmEvents.size(); ++i)
        if (_pendingParmEvents[i] == evt)
            return; // event being already stored in 'queue'

    _pendingParmEvents.push_back(evt);  // MUTEXME
    if (_pendingParmEvents.size() > 1)  //<! event delivery must be delayed
        return;

    while (!_pendingParmEvents.empty())
    {
        //---------------------------------------------------------------
        // first, notify external observers before actually removing data
        for (size_t i = 0; i < _extClientObservers.size(); ++i)
        {
            assert(_extClientObservers[i]);
            _extClientObservers[i]
                ->handleSceneParmEvent(_self, _pendingParmEvents.front());
        }
        // could do more here...
        //---------------------------------------------------------------
        _pendingParmEvents.pop_front(); //<! top event has been processed, can pop it now
    }
}

sys::ResourceDatabase::Ptr const&
ClientBase::PImpl::getOrCreateResourceDatabase()
{
    if (!_resources)
    {
        std::stringstream sstr;
        sstr << "resources <- client[" << (void*)&_self << "]";
        _resources = sys::ResourceDatabase::create(sstr.str().c_str());
    }

    return _resources;
}

//////////////////
// class ClientBase
//////////////////
ClientBase::ClientBase():
    _pImpl(new PImpl(*this))
{ }

ClientBase::~ClientBase()
{
    dispose();  //<! is itself virtual
    delete _pImpl;
}

void
ClientBase::build(ClientBase::Ptr const& ptr)
{
    _pImpl->build(ptr);
}

void
ClientBase::dispose()
{
    _pImpl->dispose();
}

void
ClientBase::setScene(scn::Scene::Ptr const& scene)
{
    _pImpl->setScene(scene);
}

ClientBase::WPtr const&
ClientBase::self()
{
    return _pImpl->self();
}

scn::Scene::Ptr const&
ClientBase::scene() const
{
    return _pImpl->scene();
}

unsigned int
ClientBase::sceneId() const
{
    return scene()
        ? scene()->id()
        : 0;
}

sys::ResourceDatabase::Ptr const&
ClientBase::getOrCreateResourceDatabase()
{
    return _pImpl->getOrCreateResourceDatabase();
}

sys::ResourceDatabase*
ClientBase::getResourceDatabase()
{
    return _pImpl->getResourceDatabase();
}

const sys::ResourceDatabase*
ClientBase::getResourceDatabase() const
{
    return _pImpl->getResourceDatabase();
}

void
ClientBase::setSharedResourceDatabase(sys::ResourceDatabase::Ptr const& value)
{
    _pImpl->setSharedResourceDatabase(value);
}

////////////////////////////
// node creation/destruction
////////////////////////////
unsigned int
ClientBase::createDataNode(const char*  name,
                           unsigned int parentId)
{
    scn::TreeNode::Ptr const& node = scn::TreeNode::create(
        name,
        getNode(parentId));
    return node
        ? node->id()
        : 0;
}

unsigned int
ClientBase::createArchive(const char*   filePath,
                          unsigned int  parentId)
{
    scn::Archive::Ptr const& node = scn::Archive::create(
        filePath,
        getNode(parentId));
    return node
        ? node->id()
        : 0;
}

unsigned int
ClientBase::createXform(const char* name,
                        unsigned int    parentId)
{
    scn::Xform::Ptr const& node = scn::Xform::create(
        name,
        getNode(parentId));
    return node
        ? node->id()
        : 0;
}


unsigned int
ClientBase::createCamera(const char*    name,
                         unsigned int parentId)
{
    scn::Camera::Ptr const& node = scn::Camera::create(
        name,
        getNode(parentId));
    return node
        ? node->id()
        : 0;
}

unsigned int
ClientBase::createSelector(const char*  regex,
                           const char*  name,
                           unsigned int parentId)
{
    scn::TreeNodeSelector::Ptr const& node = scn::TreeNodeSelector::create(
        regex,
        name,
        getNode(parentId));
    return node
        ? node->id()
        : 0;
}

unsigned int
ClientBase::createMaterial(const char*  code,
                           const char*  name,
                           unsigned int parentId)
{
    scn::Material::Ptr const& node = scn::Material::create(
        code,
        name,
        getNode(parentId));
    return node
        ? node->id()
        : 0;
}

unsigned int
ClientBase::createSubSurface(const char*    code,
                             const char*    name,
                             unsigned int   parentId)
{
    scn::SubSurface::Ptr const& node = scn::SubSurface::create(
        code,
        name,
        getNode(parentId));
    return node
        ? node->id()
        : 0;
}

unsigned int
ClientBase::createTexture(const char*   code,
                          const char*   name,
                          unsigned int  parentId)
{
    scn::Texture::Ptr const& node = scn::Texture::create(
        code,
        name,
        getNode(parentId));
    return node
        ? node->id()
        : 0;
}

unsigned int
ClientBase::createLight(const char*  code,
                        const char*  name,
                        unsigned int parentId)
{
    scn::Light::Ptr const& node = scn::Light::create(
        code,
        name,
        getNode(parentId));
    return node
        ? node->id()
        : 0;
}

unsigned int
ClientBase::createFrameBuffer(const char*   code,
                              const char*   name,
                              unsigned int  parentId)
{
    scn::FrameBuffer::Ptr const& node = scn::FrameBuffer::create(
        code,
        name,
        getNode(parentId));
    return node
        ? node->id()
        : 0;
}

unsigned int
ClientBase::createDenoiser(const char*  code,
                           const char*  name,
                           unsigned int parentId)
{
    scn::Denoiser::Ptr const& node = scn::Denoiser::create(
        code,
        name,
        getNode(parentId));
    return node
        ? node->id()
        : 0;
}

unsigned int
ClientBase::createToneMapper(const char*    code,
                             const char*    name,
                             unsigned int   parentId)
{
    scn::ToneMapper::Ptr const& node = scn::ToneMapper::create(
        code,
        name,
        getNode(parentId));
    return node
        ? node->id()
        : 0;
}

unsigned int
ClientBase::createRenderer(const char*  code,
                           const char*  name,
                           unsigned int parentId)
{
    scn::Renderer::Ptr const& node = scn::Renderer::create(
        code,
        name,
        getNode(parentId));
    return node
        ? node->id()
        : 0;
}

unsigned int
ClientBase::createFlatRenderPass(const char*    code,
                                 const char*    name,
                                 unsigned int   parentId)
{
    // first check the specified code corresponds to a valid render pass code
    bool ok = false;
    for (size_t t = 0; t < static_cast<size_t>(xchg::NUM_PASS_TYPES); ++t)
        ok = ok || code == std::to_string(static_cast<xchg::FlatRenderPassType>(t));
    if (!ok)
    {
        std::stringstream sstr;
        sstr
            << "Code '" << code << "' is not a valid flat render pass code. "
            << "Abort render pass node creation.";
        FLIRT_LOG(LOG(DEBUG) << sstr.str();)
        std::cerr << sstr.str() << std::endl;

        return 0;
    }

    scn::RenderPass::Ptr const& node = scn::RenderPass::create(
        code,
        name,
        getNode(parentId));
    return node
        ? node->id()
        : 0;
}

unsigned int
ClientBase::createLightPathExpressionPass(const char*   lpe,
                                          const char*   name,
                                          unsigned int  parentId)
{
    scn::RenderPass::Ptr const& node = scn::RenderPass::create(
        xchg::LPE_PASS_NAME,
        name,
        getNode(parentId));
    node->setParameter<std::string>(parm::regex(), lpe);
    return node
        ? node->id()
        : 0;
}

unsigned int
ClientBase::createShapeIdCoveragePass(const char*   name,
                                      unsigned int  parentId)
{
    scn::RenderPass::Ptr const& node = scn::RenderPass::create(
        xchg::IDCOVERAGE_PASS_NAME,
        name,
        getNode(parentId));
    return node
        ? node->id()
        : 0;
}

unsigned int
ClientBase::createIntegrator(const char*    code,
                             const char*    name,
                             unsigned int   parentId)
{
    scn::Integrator::Ptr const& node = scn::Integrator::create(
        code,
        name,
        getNode(parentId));
    return node
        ? node->id()
        : 0;
}

unsigned int
ClientBase::createSamplerFactory(const char*    code,
                                 const char*    name,
                                 unsigned int   parentId)
{
    scn::SamplerFactory::Ptr const& node = scn::SamplerFactory::create(
        code,
        name,
        getNode(parentId));
    return node
        ? node->id()
        : 0;
}

unsigned int
ClientBase::createPixelFilter(const char*   code,
                              const char*   name,
                              unsigned int  parentId)
{
    scn::PixelFilter::Ptr const& node = scn::PixelFilter::create(
        code,
        name,
        getNode(parentId));
    return node
        ? node->id()
        : 0;
}

unsigned int
ClientBase::createRenderAction(const char*  name,
                               unsigned int parentId)
{
    scn::RenderAction::Ptr const& node = scn::RenderAction::create(
        name,
        getNode(parentId));
    return node
        ? node->id()
        : 0;
}

unsigned int
ClientBase::createIdAssign(const char*  parmName,
                           unsigned int parmValue,
                           const char*  name,
                           unsigned int parentId)
{
    scn::IdAssign::Ptr const& node = scn::IdAssign::create(
        parmName,
        parmValue,
        name,
        getNode(parentId));
    return node
        ? node->id()
        : 0;
}

void
ClientBase::destroyDataNode(unsigned int id)
{
    scn::TreeNode::Ptr node = id > 0
        ? getNode(id)
        : nullptr;

    if (!node)
    {
        std::stringstream sstr;
        sstr << "Invalid node ID = " << id << ".";
        throw sys::Exception(sstr.str().c_str(), "Data Node Destruction");
    }

    scn::TreeNode::destroy(node);
}


unsigned int
ClientBase::registerBinding(ClientBindingBasePtr const& bind)
{
    return _pImpl->registerBinding(bind);
}

void
ClientBase::deregisterBinding(unsigned int id)
{
    _pImpl->deregisterBinding(id);
}

///////////////
// node queries
///////////////
bool
ClientBase::nodeExists(unsigned int id) const
{
    return getNode(id) != nullptr;
}

scn::TreeNode::Ptr
ClientBase::getNode(unsigned int id) const
{
    scn::Scene::Ptr const& scene = _pImpl->scene();

    if (id == 0)
    {
        std::stringstream sstr;
        sstr
            << "WARNING: No valid node ID specified. "
            << "Scene will be used instead.";
        FLIRT_LOG(LOG(DEBUG) << sstr.str();)
        std::cerr << sstr.str() << std::endl;

        return scene;
    }
    if (scene && scene->hasDescendant(id))
        return scene->descendantById(id).lock();
    return nullptr;
}

const char*
ClientBase::getNodeName(unsigned int id) const
{
    scn::TreeNode::Ptr const& node = getNode(id);

    return node
        ? node->name()
        : "";
}

const char*
ClientBase::getNodeFullName(unsigned int id) const
{
    scn::TreeNode::Ptr const& node = getNode(id);

    return node
        ? node->fullName()
        : "";
}

unsigned int
ClientBase::getNodeParent(unsigned int id) const
{
    scn::TreeNode::Ptr const& node      = getNode(id);
    scn::TreeNode::Ptr const& parent    = node ? node->parent().lock() : nullptr;

    return parent
        ? parent->id()
        : 0;
}

xchg::NodeClass
ClientBase::getNodeClass(unsigned int id) const
{
    scn::TreeNode::Ptr const& node = getNode(id);

    return node
        ? node->nodeClass()
        : xchg::NodeClass::NONE;
}

size_t
ClientBase::getNodeNumChildren(unsigned int id) const
{
    scn::TreeNode::Ptr const& node = getNode(id);

    return node
        ? node->numChildren()
        : 0;
}

unsigned int
ClientBase::getNodeChild(unsigned int   id,
                         size_t     childIdx) const
{
    scn::TreeNode::Ptr const& node = getNode(id);

    return node && childIdx < node->numChildren()
        ? node->child(childIdx)->id()
        : 0;
}

void
ClientBase::getCurrentNodeParameters(xchg::IdSet& parmIds,
                                     unsigned int id) const
{
    scn::TreeNode::Ptr const& node = getNode(id);

    if (node)
        node->getCurrentParameters(parmIds);
    else
        parmIds.clear();
}

void
ClientBase::getRelevantNodeParameters(xchg::IdSet& parmIds,
                                      unsigned int nodeId) const
{
    scn::TreeNode::Ptr const& node = getNode(nodeId);

    if (node)
        node->getRelevantParameters(parmIds);
    else
        parmIds.clear();
}

bool
ClientBase::getNodeParameterInfo(unsigned int           parmId,
                                 xchg::ParameterInfo&   parmInfo,
                                 unsigned int           nodeId) const
{
    parmInfo.name       = nullptr;
    parmInfo.typeinfo   = nullptr;
    parmInfo.flags      = parm::NO_PARM_FLAG;

    scn::TreeNode::Ptr const& node = getNode(nodeId);

    return node ? node->getParameterInfo(parmId, parmInfo) : false;
}

bool
ClientBase::getNodeParameterInfo(const char*            parmName,
                                 xchg::ParameterInfo&   parmInfo,
                                 unsigned int           nodeId) const
{
    parmInfo.name       = nullptr;
    parmInfo.typeinfo   = nullptr;
    parmInfo.flags      = parm::NO_PARM_FLAG;

    scn::TreeNode::Ptr const& node = getNode(nodeId);

    return node
        ? node->getParameterInfo(parmName, parmInfo)
        : false;
}

bool
ClientBase::isNodeParameterWritable(const char*     parmName,
                                    unsigned int    nodeId) const
{
    scn::TreeNode::Ptr const& node = getNode(nodeId);

    return node
        ? node->isParameterWritable(parmName)
        : false;
}

bool
ClientBase::isNodeParameterWritable(unsigned int parmId,
                                    unsigned int nodeId) const
{
    scn::TreeNode::Ptr const& node = getNode(nodeId);

    return node
        ? node->isParameterWritable(parmId)
        : false;
}

bool
ClientBase::isNodeParameterReadable(const char*     parmName,
                                    unsigned int    nodeId) const
{
    scn::TreeNode::Ptr const& node = getNode(nodeId);

    return node
        ? node->isParameterReadable(parmName)
        : false;
}

bool
ClientBase::isNodeParameterReadable(unsigned int parmId,
                                    unsigned int nodeId) const
{
    scn::TreeNode::Ptr const& node = getNode(nodeId);

    return node
        ? node->isParameterReadable(parmId)
        : false;
}

size_t
ClientBase::getNumLinksBetweenNodes(unsigned int targetId,
                                    unsigned int sourceId) const
{
    scn::TreeNode::Ptr const& target = getNode(targetId);

    return target
        ? target->getNumLinksFrom(sourceId)
        : 0;
}

void
ClientBase::getLinksBetweenNodes(unsigned int targetId,
                                 unsigned int sourceId,
                                 xchg::IdSet& ret) const
{
    scn::TreeNode::Ptr const& target = getNode(targetId);

    if (target)
        target->getLinksFrom(sourceId, ret);
    else
        ret.clear();
}

bool
ClientBase::isLinkBetweenNodes(unsigned int parmId,
                               unsigned int targetId,
                               unsigned int sourceId) const
{
    scn::TreeNode::Ptr const& target = getNode(targetId);

    return target
        ? target->isLinkFrom(parmId, sourceId)
        : false;
}

void
ClientBase::getNodeLinkSources(unsigned int nodeId,
                               xchg::IdSet& ret) const
{
    scn::TreeNode::Ptr const& node = getNode(nodeId);

    if (node)
        node->getLinkSources(ret);
    else
        ret.clear();
}

void
ClientBase::getNodeLinkTargets(unsigned int nodeId,
                               xchg::IdSet& ret) const
{
    scn::TreeNode::Ptr const& node = getNode(nodeId);

    if (node)
        node->getLinkTargets(ret);
    else
        ret.clear();
}

bool
ClientBase::nodeParameterExists(const char*     parmName,
                                unsigned int    fromNodeId,
                                unsigned int    upToNodeId,
                                unsigned int    *foundInNodeId) const
{
    scn::TreeNode::Ptr const& fromNode  = getNode(fromNodeId);
    scn::TreeNode::Ptr foundInNode      = nullptr;

    if (fromNode &&
        fromNode->parameterExists(parmName, upToNodeId, &foundInNode))
    {
        if (foundInNodeId)
            *foundInNodeId = (foundInNode ? foundInNode->id() : 0);
        return true;
    }
    if (foundInNodeId)
        *foundInNodeId = 0;
    return false;
}

bool
ClientBase::nodeParameterExists(unsigned int parmId,
                                unsigned int fromNodeId,
                                unsigned int upToNodeId,
                                unsigned int *foundInNodeId) const
{
    scn::TreeNode::Ptr const& fromNode  = getNode(fromNodeId);
    scn::TreeNode::Ptr foundInNode      = nullptr;

    if (fromNode &&
        fromNode->parameterExists(parmId, upToNodeId, &foundInNode))
    {
        if (foundInNodeId)
            *foundInNodeId = (foundInNode ? foundInNode->id() : 0);
        return true;
    }
    if (foundInNodeId)
        *foundInNodeId = 0;
    return false;
}

bool
ClientBase::nodeParameterExistsAsId(const char*     parmName,
                                    unsigned int    fromNodeId,
                                    unsigned int    upToNodeId,
                                    unsigned int    *foundInNodeId) const
{
    scn::TreeNode::Ptr const& fromNode  = getNode(fromNodeId);
    scn::TreeNode::Ptr foundInNode      = nullptr;

    if (fromNode &&
        fromNode->parameterExistsAsId(parmName, upToNodeId, &foundInNode))
    {
        if (foundInNodeId)
            *foundInNodeId = (foundInNode ? foundInNode->id() : 0);
        return true;
    }
    if (foundInNodeId)
        *foundInNodeId = 0;
    return false;
}

bool
ClientBase::nodeParameterExistsAsId(unsigned int parmId,
                                    unsigned int fromNodeId,
                                    unsigned int upToNodeId,
                                    unsigned int *foundInNodeId) const
{
    scn::TreeNode::Ptr const& fromNode  = getNode(fromNodeId);
    scn::TreeNode::Ptr foundInNode      = nullptr;

    if (fromNode &&
        fromNode->parameterExistsAsId(parmId, upToNodeId, &foundInNode))
    {
        if (foundInNodeId)
            *foundInNodeId = (foundInNode ? foundInNode->id() : 0);
        return true;
    }
    if (foundInNodeId)
        *foundInNodeId = 0;
    return false;
}

bool
ClientBase::nodeParameterExistsAsSwitch(const char*     parmName,
                                        unsigned int    fromNodeId,
                                        unsigned int    upToNodeId,
                                        unsigned int    *foundInNodeId) const
{
    scn::TreeNode::Ptr const& fromNode  = getNode(fromNodeId);
    scn::TreeNode::Ptr foundInNode      = nullptr;

    if (fromNode &&
        fromNode->parameterExistsAsSwitch(parmName, upToNodeId, &foundInNode))
    {
        if (foundInNodeId)
            *foundInNodeId = (foundInNode ? foundInNode->id() : 0);
        return true;
    }
    if (foundInNodeId)
        *foundInNodeId = 0;
    return false;
}

bool
ClientBase::nodeParameterExistsAsSwitch(unsigned int parmId,
                                        unsigned int fromNodeId,
                                        unsigned int upToNodeId,
                                        unsigned int *foundInNodeId) const
{
    scn::TreeNode::Ptr const& fromNode  = getNode(fromNodeId);
    scn::TreeNode::Ptr foundInNode      = nullptr;

    if (fromNode &&
        fromNode->parameterExistsAsSwitch(parmId, upToNodeId, &foundInNode))
    {
        if (foundInNodeId)
            *foundInNodeId = (foundInNode ? foundInNode->id() : 0);
        return true;
    }
    if (foundInNodeId)
        *foundInNodeId = 0;
    return false;
}

unsigned int
ClientBase::setNodeParameterId(
    const char*     parmName,
    unsigned int    sourceId,
    unsigned int    nodeId)
{
    scn::TreeNode::Ptr const& node = getNode(nodeId);

    return node
        ? node->setParameterId(parmName, sourceId)
        : 0;
}

unsigned int
ClientBase::setNodeParameterTrigger(const char*     parmName,
                                    unsigned int    nodeId)
{
    scn::TreeNode::Ptr const& node = getNode(nodeId);

    return node
        ? node->setParameterTrigger(parmName)
        : 0;
}

unsigned int
ClientBase::setNodeParameterSwitch(const char*  parmName,
                                   bool         state,
                                   unsigned int nodeId)
{
    scn::TreeNode::Ptr const& node = getNode(nodeId);

    return node
        ? node->setParameterSwitch(parmName, state)
        : 0;
}

unsigned int
ClientBase::connectIdNodeParameters(unsigned int    masterNodeId,
                                    const char*     masterParmName,
                                    unsigned int    slaveNodeId,
                                    const char*     slaveParmName,
                                    int             priority)
{
    return _pImpl->scene()
        ? _pImpl->scene()->connectId(
            masterNodeId,
            masterParmName,
            slaveNodeId,
            slaveParmName,
            priority)
        : 0;
}

bool
ClientBase::hasNodeParameterConnection(unsigned int cnxId) const
{
    return _pImpl->scene()
        ? _pImpl->scene()->hasConnection(cnxId)
        : 0;
}

bool
ClientBase::hasIdNodeParameterConnection(unsigned int cnxId) const
{
    return _pImpl->scene()
        ? _pImpl->scene()->hasIdConnection(cnxId)
        : 0;
}

void
ClientBase::disconnectNodeParameters(unsigned int cnxId)
{
    if (_pImpl->scene())
        _pImpl->scene()->disconnect(cnxId);
}

void
ClientBase::disconnectIdNodeParameters(unsigned int cnxId)
{
    if (_pImpl->scene())
        _pImpl->scene()->disconnectId(cnxId);
}

scn::ParmConnectionBase::Ptr
ClientBase::getNodeParameterConnection(unsigned int cnxId) const
{
    return _pImpl->scene() && _pImpl->scene()->hasConnection(cnxId)
        ? _pImpl->scene()->connectionById(cnxId)
        : nullptr;
}

void
ClientBase::getBindingKeys(xchg::IdSet& keys) const
{
    _pImpl->getBindingKeys(keys);
}

ClientBindingBase::Ptr
ClientBase::getBinding(unsigned int id) const
{
    return _pImpl->getBinding(id);
}

void
ClientBase::getIds(xchg::IdSet&                         ids,
                   bool                                 accumulate,
                   AbstractClientNodeFilter::Ptr const& filter,
                   size_t                               numRegexes,
                   const char**                         regexes,
                   unsigned int                         startId,
                   bool                                 upwards) const
{
    _pImpl->getIds(
        ids, accumulate, filter, numRegexes, regexes, startId, upwards);
}

void
ClientBase::registerObserver(AbstractClientObserver::Ptr const& obs)
{
    _pImpl->registerObserver(obs);
}

void
ClientBase::deregisterObserver(AbstractClientObserver::Ptr const& obs)
{
    _pImpl->deregisterObserver(obs);
}

void
ClientBase::registerObserver(AbstractClientNodeObserver::Ptr const& obs,
                             unsigned int                           nodeId)
{
    _pImpl->registerObserver(obs, nodeId);
}

void
ClientBase::deregisterObserver(AbstractClientNodeObserver::Ptr const&   obs,
                               unsigned int                         nodeId)
{
    _pImpl->deregisterObserver(obs, nodeId);
}

void
ClientBase::notifyProgressBeginning(const char* str, size_t totalSteps)
{
    if (_pImpl->scene())
        _pImpl->scene()->notifyProgressBeginning(str, totalSteps);
}

bool
ClientBase::notifyProgressProceeding(size_t currentStep)
{
    return _pImpl->scene()
        ? _pImpl->scene()->notifyProgressProceeding(currentStep)
        : false;
}

void
ClientBase::notifyProgressEnd(const char* str)
{
    if (_pImpl->scene())
        _pImpl->scene()->notifyProgressEnd(str);
}

void
ClientBase::registerProgressCallbacks(xchg::AbstractProgressCallbacks::Ptr const& callbacks)
{
    if (_pImpl->scene())
        _pImpl->scene()->registerProgressCallbacks(callbacks);
}

void
ClientBase::deregisterProgressCallbacks(xchg::AbstractProgressCallbacks::Ptr const& callbacks)
{
    if (_pImpl->scene())
        _pImpl->scene()->deregisterProgressCallbacks(callbacks);
}

// virtual
void
ClientBase::handleSceneEvent(scn::Scene&                scene,
                             const xchg::SceneEvent&    evt)
{
    _pImpl->handleSceneEvent(scene, evt);
}

// virtual
void
ClientBase::handleSceneParmEvent(const xchg::ParmEvent& evt)
{
    _pImpl->handleSceneParmEvent(evt);
}

void
ClientBase::unsetNodeParameter(const parm::BuiltIn& parm,
                               unsigned int         nodeId)
{
    scn::TreeNode::Ptr node = getNode(nodeId);
    if (node)
        node->unsetParameter(parm);
}

void
ClientBase::unsetNodeParameter(const char*  parmName,
                               unsigned int nodeId)
{
    scn::TreeNode::Ptr node = getNode(nodeId);
    if (node)
        node->unsetParameter(parmName);
}

void
ClientBase::unsetNodeParameter(unsigned int parmId,
                               unsigned int nodeId)
{
    scn::TreeNode::Ptr node = getNode(nodeId);
    if (node)
        node->unsetParameter(parmId);
}

bool
ClientBase::getWorldBounds(unsigned int         nodeId,
                           xchg::BoundingBox&   ret) const
{
    ret.clear();

    scn::TreeNode::Ptr const& node = getNode(nodeId);
    if (!node)
        return false;

    xchg::BoundingBox bounds;
    if (!node->getParameter<xchg::BoundingBox>(parm::boundingBox(), bounds))
        return false;
    if (!bounds.valid())
        return false;

    math::Affine3f worldXfm;
    if (node->getParameter<math::Affine3f>(parm::worldTransform(), worldXfm))
    {
        const bool isIdentity = xchg::equal<math::Affine3f>(worldXfm, math::Affine3f::Identity());
        if (!isIdentity)
            for (int k = 0; k < 8; ++k)
                ret += worldXfm * bounds.corner(k);
        else
            ret = bounds;
    }
    else
        ret = bounds;

    return ret.valid();
}

bool
ClientBase::getWorldBounds(const xchg::IdSet&   nodes,
                           xchg::BoundingBox&   ret) const
{
    ret.clear();
    for (xchg::IdSet::const_iterator id = nodes.begin();
        id != nodes.end();
        ++id)
    {
        xchg::BoundingBox bounds;

        if (getWorldBounds(*id, bounds))
            ret += bounds;
    }
    return ret.valid();
}

// static
unsigned int
ClientBase::getId(const parm::BuiltIn& parm)
{
    return parm.id(); // function useful for decoupling module
}
