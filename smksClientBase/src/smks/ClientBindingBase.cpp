// ========================================================================= //
// Copyright 2016-2018 SUPAMONKS_STUDIO                                      //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
//                                                                           //
// ========================================================================= //
#include <iostream>

#include <smks/sys/Log.hpp>
#include <smks/xchg/ParmEvent.hpp>
#include <smks/scn/TreeNode.hpp>
#include <smks/scn/AbstractParmObserver.hpp>

#include "smks/ClientBase.hpp"
#include "smks/ClientBindingBase.hpp"

namespace smks
{
    //////////////////////////////////
    // class ClientBindingBase::ParmObserver
    //////////////////////////////////
    class ClientBindingBase::ParmObserver:
        public scn::AbstractParmObserver
    {
    public:
        typedef std::shared_ptr<ParmObserver> Ptr;
    private:
        ClientBindingBase& _self;
    public:
        static inline
        Ptr
        create(ClientBindingBase& self)
        {
            Ptr ptr(new ParmObserver(self));
            return ptr;
        }
    protected:
        explicit inline
        ParmObserver(ClientBindingBase& self):
            _self(self)
        { }
    public:
        inline
        void
        handle(scn::TreeNode&, const xchg::ParmEvent&);
    };

    ///////////////////////////
    // class ClientBindingBase::PImpl
    ///////////////////////////
    class ClientBindingBase::PImpl
    {
    private:
        const ClientBase::WPtr      _client;
        const scn::TreeNode::WPtr   _dataNode;
        ParmObserver::Ptr           _observer;
        // observer attached to data node and redirecting all
        // parameter events to the binding itself

    public:
        inline
        PImpl(ClientBindingBase&                    self,
              ClientBase::WPtr const&       client,
              scn::TreeNode::WPtr const&    dataNode):
            _client     (client),
            _dataNode   (dataNode),
            _observer   (ParmObserver::create(self))
        { }

        inline
        ClientBase::WPtr const&
        client() const
        {
            return _client;
        }

        inline
        scn::TreeNode::WPtr const&
        dataNode() const
        {
            return _dataNode;
        }

        inline
        unsigned int
        id() const
        {
            scn::TreeNode::Ptr const& dataNode = _dataNode.lock();
            return dataNode ? dataNode->id() : 0;
        }

        inline
        xchg::NodeClass
        nodeClass() const
        {
            scn::TreeNode::Ptr const& dataNode = _dataNode.lock();
            return dataNode ? dataNode->nodeClass() : xchg::NodeClass::NONE;
        }


        inline
        void
        build(ClientBindingBase::Ptr const& self)
        {
            scn::TreeNode::Ptr const& dataNode = _dataNode.lock();
            if (dataNode)
                dataNode->registerObserver(_observer);
        }

        inline
        void
        erase()
        {
            scn::TreeNode::Ptr const& dataNode = _dataNode.lock();
            if (dataNode)
                dataNode->deregisterObserver(_observer);
        }
    };
    /////////////////////////
}


using namespace smks;

void
ClientBindingBase::ParmObserver::handle(scn::TreeNode&          node,
                                  const xchg::ParmEvent&    evt)
{
    assert(node.id() == _self.id());
    _self.handle(evt); //-> call virtual function
}

////////////////////
// class ClientBindingBase
////////////////////
ClientBindingBase::ClientBindingBase(ClientBase::WPtr const&    client,
                         scn::TreeNode::WPtr const& dataNode):
    _pImpl(new PImpl(*this, client, dataNode))
{ }

ClientBindingBase::~ClientBindingBase()
{
    delete _pImpl;
}

void
ClientBindingBase::build(Ptr const& self)
{
    return _pImpl->build(self);
}

void
ClientBindingBase::erase()
{
    _pImpl->erase();
}

ClientBase::WPtr const&
ClientBindingBase::client() const
{
    return _pImpl->client();
}

scn::TreeNode::WPtr const&
ClientBindingBase::dataNode() const
{
    return _pImpl->dataNode();
}

unsigned int
ClientBindingBase::id() const
{
    return _pImpl->id();
}

xchg::NodeClass
ClientBindingBase::nodeClass() const
{
    return _pImpl->nodeClass();
}
