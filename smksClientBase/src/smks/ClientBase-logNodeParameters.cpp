// ========================================================================= //
// Copyright 2016-2018 SUPAMONKS_STUDIO                                      //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
//                                                                           //
// ========================================================================= //

#include <std/to_string_xchg.hpp>

#include <smks/sys/Log.hpp>
#include <smks/scn/TreeNode.hpp>
#include "smks/ClientBase.hpp"

using namespace smks;

#if defined(FLIRT_LOG_ENABLED)
void
ClientBase::logNodeParameters(unsigned int nodeId) const
{
    if (nodeId == 0)
        return;

    std::stringstream   sstr;
    xchg::IdSet         parms;

    scn::TreeNode::Ptr const& node = getNode(nodeId);
    sstr << "'" << node->fullName() << "'";
    node->getCurrentParameters(parms);
    if (parms.empty())
        sstr << "\n\t- no parameter set";
    for (xchg::IdSet::const_iterator id = parms.begin(); id != parms.end(); ++id)
    {
        xchg::ParameterInfo parmInfo;
        const bool          found = node->getParameterInfo(*id, parmInfo);
        if (!found)
            continue;
        assert(parmInfo);

        bool            parm1b = false;
        char            parm1c = 0;
        int             parm1i = 0;
        unsigned int    parm1ui = 0;
        size_t          parm1l = 0;
        std::string     parm1s;
        float           parm1f = 0.0f;
        math::Vector2i  parm2i(math::Vector2i::Zero());
        math::Vector3i  parm3i(math::Vector3i::Zero());
        math::Vector4i  parm4i(math::Vector4i::Zero());
        math::Vector2f  parm2f(math::Vector2f::Zero());
        math::Vector4f  parm4f(math::Vector4f::Zero());
        math::Affine3f  parmXfm(math::Affine3f::Identity());
        xchg::IdSet     parmIds;

        sstr << "\n\t- '" << parmInfo.name << "' ";

        if (*parmInfo.typeinfo == typeid(bool) &&
            node->getParameter<bool>(*id, parm1b))
            sstr << "=\t" << (parm1b ? "true" : "false");
        else if (*parmInfo.typeinfo == typeid(char) &&
            node->getParameter<char>(*id, parm1c))
            sstr << "=\t" << static_cast<int>(parm1c);
        else if (*parmInfo.typeinfo == typeid(int) &&
            node->getParameter<int>(*id, parm1i))
            sstr << "=\t" << parm1i;
        else if (*parmInfo.typeinfo == typeid(unsigned int) &&
            node->getParameter<unsigned int>(*id, parm1ui))
            sstr << "=\t" << parm1ui;
        else if (*parmInfo.typeinfo == typeid(size_t) &&
            node->getParameter<size_t>(*id, parm1l))
            sstr << "=\t" << parm1l;
        else if (*parmInfo.typeinfo == typeid(std::string) &&
            node->getParameter<std::string>(*id, parm1s))
            sstr << "=\t\'" << parm1s << "'";
        else if (*parmInfo.typeinfo == typeid(float) &&
            node->getParameter<float>(*id, parm1f))
            sstr << "=\t" << parm1f;
        else if (*parmInfo.typeinfo == typeid(math::Vector2i) &&
            node->getParameter<math::Vector2i>(*id, parm2i))
            sstr << "=\t" << parm2i.transpose();
        else if (*parmInfo.typeinfo == typeid(math::Vector3i) &&
            node->getParameter<math::Vector3i>(*id, parm3i))
            sstr << "=\t" << parm3i.transpose();
        else if (*parmInfo.typeinfo == typeid(math::Vector4i) &&
            node->getParameter<math::Vector4i>(*id, parm4i))
            sstr << "=\t" << parm4i.transpose();
        else if (*parmInfo.typeinfo == typeid(math::Vector2f) &&
            node->getParameter<math::Vector2f>(*id, parm2f))
            sstr << "=\t" << parm2f.transpose();
        else if (*parmInfo.typeinfo == typeid(math::Vector4f) &&
            node->getParameter<math::Vector4f>(*id, parm4f))
            sstr << "=\t" << parm4f.transpose();
        else if (*parmInfo.typeinfo == typeid(math::Affine3f) &&
            node->getParameter<math::Affine3f>(*id, parmXfm))
            sstr << "=\t\n" << parmXfm.matrix();
        else if (*parmInfo.typeinfo == typeid(xchg::IdSet) &&
            node->getParameter<xchg::IdSet>(*id, parmIds))
            sstr << "=\t{ " << parmIds << "}";

        sstr << " " << std::to_string(parmInfo.flags);
    }
    LOG(DEBUG) << sstr.str();
}
#endif //defined(FLIRT_LOG_ENABLED)
