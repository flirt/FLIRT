// ========================================================================= //
// Copyright 2016-2018 SUPAMONKS_STUDIO                                      //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
//                                                                           //
// ========================================================================= //
#include <smks/util/string/Regex.hpp>
#include "smks/client_node_filters.hpp"

namespace smks
{
    class FilterClientNodeByName::PImpl
    {
    private:
        const char* const           _query;
        util::string::Regex::Ptr    _regex;
        const bool                  _useFullName;
    public:
        PImpl(const char*   query,
              bool          isQueryRegex,
              bool          useFullName):
            _query      (query),
            _regex      (isQueryRegex ? util::string::Regex::create(query) : nullptr),
            _useFullName(useFullName)
        { }

        inline
        bool
        operator()(unsigned int n, const ClientBase& client) const
        {
            const char* name = _useFullName
                ? client.getNodeFullName(n)
                : client.getNodeName(n);

            return _regex
                ? _regex->match(name)
                : strcmp(name, _query) == 0;
        }
    };
}

using namespace smks;

FilterClientNodeByName::FilterClientNodeByName(const char*  query,
                                               bool         isQueryRegex,
                                               bool         useFullName):
    _pImpl(new PImpl(query, isQueryRegex, useFullName))
{ }

FilterClientNodeByName::~FilterClientNodeByName()
{
    delete _pImpl;
}

bool
FilterClientNodeByName::operator()(unsigned int         n,
                                   const ClientBase&    client) const
{
    return (*_pImpl)(n, client);
}
