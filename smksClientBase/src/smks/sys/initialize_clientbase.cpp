// ========================================================================= //
// Copyright 2016-2018 SUPAMONKS_STUDIO                                      //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
//                                                                           //
// ========================================================================= //
#include <smks/sys/Log.hpp>

#if defined(FLIRT_LOG_ENABLED)
INITIALIZE_NULL_EASYLOGGINGPP;
#endif // defined(FLIRT_LOG_ENABLED)

#include "smks/sys/initialize_clientbase.hpp"

namespace smks { namespace sys
{
    static bool _smksClientBaseInitialized = false;
}
}

void
smks::sys::initializeSmksClientBase()
{
    if (_smksClientBaseInitialized)
        return;

#if defined(FLIRT_LOG_ENABLED)
    el::Helpers::setStorage(smks::sys::sharedLoggingRepository());
#endif //defined(FLIRT_LOG_ENABLED)

    _smksClientBaseInitialized = true;
}

void
smks::sys::finalizeSmksClientBase()
{
    if (!_smksClientBaseInitialized)
        return;

#if defined(FLIRT_LOG_ENABLED)
    el::Helpers::setStorage(nullptr);
#endif //defined(FLIRT_LOG_ENABLED)

    _smksClientBaseInitialized = false;
}

bool
smks::sys::smksClientInitialized()
{
    return _smksClientBaseInitialized;
}
