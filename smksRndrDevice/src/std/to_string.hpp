// ========================================================================= //
// Copyright 2016-2018 SUPAMONKS_STUDIO                                      //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
//                                                                           //
// ========================================================================= //
#pragma once

#include <smks/math/types.hpp>
#include <smks/xchg/BoundingBox.hpp>
#include <smks/xchg/DataStream.hpp>
#include <smks/xchg/ObjectPtr.hpp>
#include <smks/xchg/IdSet.hpp>
#include "../smks/rndr/renderer/FlatRenderPassType.hpp"
#include "../smks/rndr/api/AccumulationOp.hpp"

namespace std
{
    string
    to_string(const type_info&);

    string
    to_string(smks::rndr::AccumulationOp);
}
