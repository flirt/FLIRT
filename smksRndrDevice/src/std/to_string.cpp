// ========================================================================= //
// Copyright 2016-2018 SUPAMONKS_STUDIO                                      //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
//                                                                           //
// ========================================================================= //
#include "to_string.hpp"

std::string
std::to_string(const type_info& info)
{
    if (info == typeid(bool))                                   return "bool";
    else if (info == typeid(unsigned int))                      return "uint";
    else if (info == typeid(size_t))                            return "size_t";
    else if (info == typeid(char))                              return "char";
    else if (info == typeid(int))                               return "int";
    else if (info == typeid(smks::math::Vector2i))              return "int2";
    else if (info == typeid(smks::math::Vector3i))              return "int3";
    else if (info == typeid(smks::math::Vector4i))              return "int4";
    else if (info == typeid(float))                             return "float";
    else if (info == typeid(smks::math::Vector2f))              return "float2";
    else if (info == typeid(smks::math::Vector4f))              return "float4";
    else if (info == typeid(smks::math::Affine3f))              return "affine3";
    else if (info == typeid(std::string))                       return "string";
    else if (info == typeid(smks::xchg::BoundingBox))           return "bounds";
    else if (info == typeid(smks::xchg::DataStream))            return "datastream";
    else if (info == typeid(std::shared_ptr<smks::xchg::Data>)) return "data.ptr";
    else if (info == typeid(smks::xchg::IdSet))                 return "ids";
    else if (info == typeid(smks::xchg::ObjectPtr))             return "object";
    else                                                        return "?";
}

std::string
std::to_string(smks::rndr::AccumulationOp op)
{
    switch (op)
    {
    case smks::rndr::ACCUMULATION_AVG:  return "AVG";
    case smks::rndr::ACCUMULATION_MIN:  return "MIN";
    case smks::rndr::ACCUMULATION_MAX:  return "MAX";
    default:                            return "?";
    }
}
