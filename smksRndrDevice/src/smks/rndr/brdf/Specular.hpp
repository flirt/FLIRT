// ========================================================================= //
// Modification copyright 2016-2018 SUPAMONKS_STUDIO                         //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
// _________________________________________________________________________ //
// Copyright 2009-2013 Intel Corporation                                     //
//                                                                           //
// Licensed under the Apache License, Version 2.0 (the "License");           //
// you may not use this file except in compliance with the License.          //
// You may obtain a copy of the License at                                   //
//                                                                           //
//     http://www.apache.org/licenses/LICENSE-2.0                            //
//                                                                           //
// Unless required by applicable law or agreed to in writing, software       //
// distributed under the License is distributed on an "AS IS" BASIS,         //
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  //
// See the License for the specific language governing permissions and       //
// limitations under the License.                                            //
//                                                                           //
// ========================================================================= //
#pragma once

#include "BRDF.hpp"
#include "optics.hpp"

namespace smks { namespace rndr
{
    /*! Specular Phong BRDF. The cosine of the angle between incoming
    *  ray direction and reflection direction is raised to some power
    *  to approximate a glossy reflection. */
    class Specular:
        public BRDF
    {
    private:
        /*! The reflectivity parameter. The range is [0,1] where 0 means
        *  no reflection at all, and 1 means full reflection. */
        math::Vector4f _R;

        /*! The exponent that determines the glossiness. The range is
        *  [0,infinity[ where 0 means a diffuse surface, and the
        *  specularity increases towards infinity. */
        float _expn;

    public:

        /*! Specular BRDF constructor. */
        __forceinline
        Specular(const math::Vector4f& R, float expn):
            BRDF    (GLOSSY_REFLECTION_BRDF),
            _R      (R),
            _expn   (expn)
        { }

        __forceinline
        operator math::Vector4f() const
        {
            return _R;
        }

        __forceinline
        math::Vector4f eval(const math::Vector4f&       wo,
                            const DifferentialGeometry& dg,
                            const math::Vector4f&       wi) const
        {
            const math::Vector4f& r = reflect(wo, dg.Ns).value;

            if (math::dot3(r, wi) < 0.0f)
                return math::Vector4f::Zero();

            return _R * (_expn + 2.0f) * static_cast<float>(sys::one_over_two_pi) * math::pow(math::dot3(r, wi), _expn) * math::clamp(math::dot3(wi, dg.Ns));
        }

        math::Vector4f
        sample(const math::Vector4f&        wo,
               const DifferentialGeometry&  dg,
               Sample<math::Vector4f>&      wi,
               const math::Vector2f&        s) const
        {
            wi = powerCosineSampleHemisphere(s.x(), s.y(), reflect(wo, dg.Ns).value, _expn);
            return eval(wo, dg, wi.value);
        }

        float
        pdf(const math::Vector4f&       wo,
            const DifferentialGeometry& dg,
            const math::Vector4f&       wi) const
        {
            return powerCosineSampleHemispherePDF(wi, reflect(wo, dg.Ns).value, _expn);
        }
    };
}
}
