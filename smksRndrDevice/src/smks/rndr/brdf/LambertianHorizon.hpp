// ========================================================================= //
// Modification copyright 2016-2018 SUPAMONKS_STUDIO                         //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
// _________________________________________________________________________ //
// Copyright 2009-2013 Intel Corporation                                     //
//                                                                           //
// Licensed under the Apache License, Version 2.0 (the "License");           //
// you may not use this file except in compliance with the License.          //
// You may obtain a copy of the License at                                   //
//                                                                           //
//     http://www.apache.org/licenses/LICENSE-2.0                            //
//                                                                           //
// Unless required by applicable law or agreed to in writing, software       //
// distributed under the License is distributed on an "AS IS" BASIS,         //
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  //
// See the License for the specific language governing permissions and       //
// limitations under the License.                                            //
//                                                                           //
// ========================================================================= //
#pragma once

#include "BRDF.hpp"

namespace smks { namespace rndr
{
    /*! Lambertian BRDF. A lambertian surface is a surface that reflects
    *  the same intensity independent of the viewing direction. The
    *  BRDF has a reflectivity parameter that determines the color of
    *  the surface. */
    class LambertianHorizon:
        public BRDF
    {
    private:
        const math::Vector4f _dx;
        const math::Vector4f _dy;
        const math::Vector4f _dz;

        /*! The reflectivity parameter. The vale 0 means no reflection,
        *  and 1 means full reflection. */
        const math::Vector4f _R;

        const float _nhx;   //<! premultuplied by strength
        const float _phx;   //<! premultuplied by strength
        const float _nhy;   //<! premultuplied by strength
        const float _phy;   //<! premultuplied by strength

        const float _blend;
        const float _rBlend;

    public:
        /*! Lambertian BRDF constructor. This is a diffuse reflection BRDF. */
        __forceinline
        LambertianHorizon(const math::Vector4f& dx,
                          const math::Vector4f& dy,
                          const math::Vector4f& dz,
                          const math::Vector4f& R,
                          float                 nhx,
                          float                 phx,
                          float                 nhy,
                          float                 phy,
                          float                 blend,
                          float                 strength):
            BRDF        (DIFFUSE_REFLECTION_BRDF),
            _dx         (dx),
            _dy         (dy),
            _dz         (dz),
            _R          (R),
            _nhx        (nhx * strength),
            _phx        (phx * strength),
            _nhy        (nhy * strength),
            _phy        (phy * strength),
            _blend      (blend),
            _rBlend     (_blend > 0.0f ? math::rcp(_blend) : 0.0f)
        { }

        __forceinline
        operator math::Vector4f() const
        {
            return _R;
        }

        __forceinline
        math::Vector4f
        eval(const math::Vector4f&          wo,
             const DifferentialGeometry&    dg,
             const math::Vector4f&          wi) const
        {
            if (wi.dot(dg.Ng) <= 0.0f)
                return math::Vector4f::Zero();

            const float cThetaI = wi.dot(dg.Ns);

            const float znx = math::clamp(cThetaI - _nhx + _blend)* _rBlend;
            const float wnx = math::sqr(math::clamp(wi.dot(-_dx)));

            const float zpx = math::clamp(cThetaI - _phx + _blend)* _rBlend;
            const float wpx = math::sqr(math::clamp(wi.dot(_dx)));

            const float zny = math::clamp(cThetaI - _nhy + _blend)* _rBlend;
            const float wny = math::sqr(math::clamp(wi.dot(-_dy)));

            const float zpy = math::clamp(cThetaI - _phy + _blend)* _rBlend;
            const float wpy = math::sqr(math::clamp(wi.dot(_dy)));

            const float w = (wnx + wpx + wny + wpy) * 0.5f;
            if (w == 0.0f)
                return _R * static_cast<float>(sys::one_over_pi) * math::clamp(wi.dot(_dz));

            const float z = (wnx * znx + wpx * zpx + wny * zny + wpy * zpy) * 0.5f * math::rcp(w);
            return z * _R * static_cast<float>(sys::one_over_pi) * math::clamp(wi.dot(_dz));
        }

        math::Vector4f
        sample(const math::Vector4f&        wo,
               const DifferentialGeometry&  dg,
               Sample<math::Vector4f>&      wi,
               const math::Vector2f&        s) const
        {
            wi = cosineSampleHemisphere(s.x(), s.y(), _dz);
            return eval(wo, dg, wi.value);
        }

        float
        pdf(const math::Vector4f&       wo,
            const DifferentialGeometry& dg,
            const math::Vector4f&       wi) const
        {
            return cosineSampleHemispherePDF(wi, _dz);
        }
    };
}
}
