// ========================================================================= //
// Copyright 2016-2018 SUPAMONKS_STUDIO                                      //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
//                                                                           //
// ========================================================================= //
#pragma once

#include "BRDF.hpp"

namespace smks
{
    namespace rndr
    {
        struct RadianceClosure
        {
            ALIGNED_CLASS

            math::Vector4f  diffuseReflection;
            math::Vector4f  diffuseTransmission;
            math::Vector4f  glossyReflection;
            math::Vector4f  glossyTransmission;
            math::Vector4f  singularReflection;
            math::Vector4f  singularTransmission;

        public:
            explicit __forceinline
            RadianceClosure(const math::Vector4f& c = math::Vector4f::Zero()):
                diffuseReflection   (c),
                diffuseTransmission (c),
                glossyReflection    (c),
                glossyTransmission  (c),
                singularReflection  (c),
                singularTransmission(c)
            { }

            __forceinline
            RadianceClosure(const math::Vector4f& diffuseReflection,
                            const math::Vector4f& diffuseTransmission,
                            const math::Vector4f& glossyReflection,
                            const math::Vector4f& glossyTransmission,
                            const math::Vector4f& singularReflection,
                            const math::Vector4f& singularTransmission):
                diffuseReflection   (diffuseReflection),
                diffuseTransmission (diffuseTransmission),
                glossyReflection    (glossyReflection),
                glossyTransmission  (glossyTransmission),
                singularReflection  (singularReflection),
                singularTransmission(singularTransmission)
            { }


            __forceinline
            void
            set(const math::Vector4f& c)
            {
                diffuseReflection       = c;
                diffuseTransmission     = c;
                glossyReflection        = c;
                glossyTransmission      = c;
                singularReflection      = c;
                singularTransmission    = c;
            }

            __forceinline
            void
            add(BRDFType t, const math::Vector4f& c)
            {
                if      (t & DIFFUSE_REFLECTION_BRDF)       diffuseReflection       += c;
                else if (t & DIFFUSE_TRANSMISSION_BRDF)     diffuseTransmission     += c;
                else if (t & GLOSSY_REFLECTION_BRDF)        glossyReflection        += c;
                else if (t & GLOSSY_TRANSMISSION_BRDF)      glossyTransmission      += c;
                else if (t & SINGULAR_REFLECTION_BRDF)      singularReflection      += c;
                else if (t & SINGULAR_TRANSMISSION_BRDF)    singularTransmission    += c;
            }

            __forceinline
            RadianceClosure&
            operator+=(const RadianceClosure& other)
            {
                diffuseReflection       += other.diffuseReflection;
                diffuseTransmission     += other.diffuseTransmission;
                glossyReflection        += other.glossyReflection;
                glossyTransmission      += other.glossyTransmission;
                singularReflection      += other.singularReflection;
                singularTransmission    += other.singularTransmission;
                return *this;
            }

            __forceinline
            RadianceClosure
            operator*(float weight) const
            {
                return RadianceClosure(
                    weight * diffuseReflection,
                    weight * diffuseTransmission,
                    weight * glossyReflection,
                    weight * glossyTransmission,
                    weight * singularReflection,
                    weight * singularTransmission);
            }
        };
    }

    namespace math
    {
        __forceinline
        bool
        isnan(const rndr::RadianceClosure& c)
        {
            return isnan(c.diffuseReflection)   ||
                isnan(c.diffuseTransmission)    ||
                isnan(c.glossyReflection)       ||
                isnan(c.glossyTransmission)     ||
                isnan(c.singularReflection)     ||
                isnan(c.singularTransmission);
        }
    }
}
