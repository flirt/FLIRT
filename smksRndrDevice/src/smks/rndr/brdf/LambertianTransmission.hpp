// ========================================================================= //
// Copyright 2016-2018 SUPAMONKS_STUDIO                                      //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
//                                                                           //
// ========================================================================= //
#pragma once

#include "BRDF.hpp"

namespace smks { namespace rndr
{
    /*! Lambertian BRDF. A lambertian surface is a surface that reflects
    *  the same intensity independent of the viewing direction. The
    *  BRDF has a reflectivity parameter that determines the color of
    *  the surface. */
    class LambertianTransmission:
        public BRDF
    {
    private:
        const math::Vector4f    _N;
        /*! The reflectivity parameter. The vale 0 means no transmission,
        *  and 1 means full transmission. */
        const math::Vector4f    _T;

    public:
        /*! Lambertian BRDF constructor. This is a diffuse reflection BRDF. */
        __forceinline
        LambertianTransmission(const math::Vector4f& N,
                               const math::Vector4f& T):
            BRDF(DIFFUSE_TRANSMISSION_BRDF),
            _N  (N),
            _T  (T)
        { }

        __forceinline
        operator math::Vector4f() const
        {
            return _T;
        }

        __forceinline
        math::Vector4f
        eval(const math::Vector4f&          wo,
             const DifferentialGeometry&    dg,
             const math::Vector4f&          wi) const
        {
            if (wi.dot(-dg.Ng) <= 0.0f)
                return math::Vector4f::Zero();
            return _T * static_cast<float>(sys::one_over_pi) * math::clamp(wi.dot(-_N));
        }

        math::Vector4f
        sample(const math::Vector4f&        wo,
               const DifferentialGeometry&  dg,
               Sample<math::Vector4f>&      wi,
               const math::Vector2f&        s) const
        {
            wi = cosineSampleHemisphere(s.x(), s.y(), -_N);
            return eval(wo, dg, wi.value);
        }

        float
        pdf(const math::Vector4f& wo, const DifferentialGeometry& dg, const math::Vector4f& wi) const
        {
            return cosineSampleHemispherePDF(wi, -_N);
        }
    };
}
}
