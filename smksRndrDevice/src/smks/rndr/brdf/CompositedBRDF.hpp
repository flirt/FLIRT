// ========================================================================= //
// Modification copyright 2016-2018 SUPAMONKS_STUDIO                         //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
// _________________________________________________________________________ //
// Copyright 2009-2013 Intel Corporation                                     //
//                                                                           //
// Licensed under the Apache License, Version 2.0 (the "License");           //
// you may not use this file except in compliance with the License.          //
// You may obtain a copy of the License at                                   //
//                                                                           //
//     http://www.apache.org/licenses/LICENSE-2.0                            //
//                                                                           //
// Unless required by applicable law or agreed to in writing, software       //
// distributed under the License is distributed on an "AS IS" BASIS,         //
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  //
// See the License for the specific language governing permissions and       //
// limitations under the License.                                            //
//                                                                           //
// ========================================================================= //
#pragma once

#include <smks/math/matrixAlgo.hpp>
#include "BRDF.hpp"
#include "../brdf/RadianceClosure.hpp"

/*! Helper macro that allocates memory in the composited BRDF and
*  performs an inplace new of the BRDF to create. */
#ifndef NEW_BRDF
#define NEW_BRDF(BRDFS, ...) \
    new (BRDFS.alloc(sizeof(__VA_ARGS__))) __VA_ARGS__
#endif

namespace smks { namespace rndr
{
    /*! Composited BRDF deals as container of individual BRDF
    *  components. It contains storage where its BRDF components are
    *  allocated inside. It has to be aligned, because the BRDF
    *  components might use SSE code. */
    class __align(64) CompositedBRDF
    {
    private:
        enum { MAX_BYTES = 2048 * sizeof(float) };  /*! maximal number of bytes for BRDF storage */
        enum { MAX_BRDF = 16 };                     /*! maximal number of BRDF components */
        enum { MAX_CHUNKS = 4 };

    private:
        /*! Data storage. Has to be at the beginning of the class due to alignment. */
        char        _data[MAX_BYTES];   //!< Storage for BRDF components
        size_t      _numBytes;          //!< Number of bytes occupied in storage
        /*! BRDF list */
        const BRDF* _BRDFs[MAX_BRDF];   //!< pointers to BRDF components
        size_t      _totalBRDFs;        //!< number of stored BRDF components

        size_t      _numBRDFs[MAX_CHUNKS];
        float       _weights[MAX_CHUNKS];
        char        _numChunks;

    public:
        /*! Composited BRDF constructor. */
        __forceinline
        CompositedBRDF():
            _numBytes   (0),
            _totalBRDFs (0),
            _numChunks  (0)
        { }
    private:
        // non-copyable
        CompositedBRDF(const CompositedBRDF&);
        CompositedBRDF& operator=(const CompositedBRDF&);
    public:
        /*! Allocates data for new BRDF component. Data gets aligned by 64
        *  bytes. */
        __forceinline
        void*
        alloc(size_t size)
        {
            void* p     = _data + _numBytes;
            _numBytes   = (_numBytes + size + 63) & (size_t(-64));
            if (_numBytes > MAX_BYTES)
                throw sys::Exception(
                    "Out of memory.",
                    "BRDF Allocation");
            return p;
        }

        /*! Adds a new BRDF to the list of BRDFs */
        // WARNING: Fails silently when the composited BRDF
        // comes from a former blend operation.
        __forceinline
        void
        add(const BRDF* brdf)
        {
            if (_numChunks == 0)
                addComponent(brdf);

#if defined(_DEBUG)
            {
                if (_numChunks > 0)
                {
                    std::stringstream sstr;
                    sstr
                        << "Cannot add component to composited BRDF: "
                        << "already comes from blend.";
                    FLIRT_LOG(LOG(DEBUG) << sstr.str();)
                    std::cerr << sstr.str() << std::endl;
                }
            }
#endif // defined(_DEBUG)
        }
    private:
        __forceinline
        void
        addComponent(const BRDF* brdf)
        {
            if (_totalBRDFs < MAX_BRDF)
                _BRDFs[_totalBRDFs++] = brdf;
        }
        __forceinline
        float
        addChunk(float weight, size_t numBRDFs)
        {
            if (_numChunks < MAX_CHUNKS)
            {
                _weights[_numChunks] = weight;
                _numBRDFs[_numChunks] = numBRDFs;
                ++_numChunks;

                return weight;
            }
            return 0.0f;
        }

    public:
        void
        blend(size_t, const CompositedBRDF*, const float* weights);

    private:
        /*! Returns the number of used BRDF components. */
        __forceinline
        size_t
        size() const
        {
            return _totalBRDFs;
        }

    private:
        ///////////////////
        // struct BRDFArray
        ///////////////////
        struct BRDFArray
        {
            const BRDF* const*  ptr;
            size_t              size;
        public:
            BRDFArray(const BRDF* const* ptr, size_t size):
                ptr(ptr), size(size)
            { }
        };
        ///////////////////
    public:
        /*! Determine if the composited BRDF contains a component of the specified type. */
        inline
        bool
        has(BRDFType type) const
        {
            if (_numChunks == 0)
                return has(BRDFArray(_BRDFs, _totalBRDFs), type);
            //---
            const BRDF* const* BRDFs = _BRDFs;
            for (size_t n = 0; n < _numChunks; ++n)
            {
                const size_t numBRDFs = _numBRDFs[n];
                if (_weights[n] > 1e-6f &&
                    has(BRDFArray(BRDFs, numBRDFs), type))
                    return true;
                BRDFs += numBRDFs;
            }
            return false;
        }
    private:
        static inline
        bool
        has(const BRDFArray&    BRDFs,
            BRDFType            type)
        {
            for (size_t i = 0; i < BRDFs.size; ++i)
                if (BRDFs.ptr[i]->type() & type)
                    return true;
            return false;
        }

    public:
        inline
        math::Vector4f
        color(BRDFType type) const
        {
            if (_numChunks == 0)
                return color(BRDFArray(_BRDFs, _totalBRDFs), type);
            //---
            math::Vector4f c = math::Vector4f::Zero();

            const BRDF* const* BRDFs = _BRDFs;
            for (size_t n = 0; n < _numChunks; ++n)
            {
                const size_t numBRDFs = _numBRDFs[n];
                if (_weights[n] > 1e-6f)
                    c += color(BRDFArray(BRDFs, numBRDFs), type) *
                        _weights[n];
                BRDFs += numBRDFs;
            }
            return c;
        }
    private:
        static inline
        math::Vector4f
        color(const BRDFArray&  BRDFs,
              BRDFType          type)
        {
            math::Vector4f c = math::Vector4f::Zero();
            for (size_t i = 0; i < BRDFs.size; ++i)
                if (BRDFs.ptr[i]->type() & type)
                    c += BRDFs.ptr[i]->color();
            return c;
        }

    public:
        /*! Evaluates all BRDF components. */
        math::Vector4f
        eval(const math::Vector4f&          wo,
             const DifferentialGeometry&    dg,
             const math::Vector4f&          wi,
             BRDFType                       type) const
        {
            if (_numChunks == 0)
                return eval(BRDFArray(_BRDFs, _totalBRDFs),
                            wo, dg, wi, type);
            //---
            math::Vector4f c = math::Vector4f::Zero();

            const BRDF* const* BRDFs = _BRDFs;
            for (size_t n = 0; n < _numChunks; ++n)
            {
                const size_t numBRDFs = _numBRDFs[n];
                if (_weights[n] > 1e-6f)
                    c += eval(BRDFArray(BRDFs, numBRDFs),
                              wo, dg, wi, type) *
                        _weights[n];
                BRDFs += numBRDFs;
            }
            return c;
        }
    private:
        math::Vector4f
        eval(const BRDFArray&               BRDFs,
             const math::Vector4f&          wo,
             const DifferentialGeometry&    dg,
             const math::Vector4f&          wi,
             BRDFType                       type) const
        {
            math::Vector4f c = math::Vector4f::Zero();
            for (size_t i = 0; i < BRDFs.size; ++i)
                if (BRDFs.ptr[i]->type() & type)
                    c += BRDFs.ptr[i]->eval(wo, dg, wi);
            return c;
        }

    public:
        void
        eval(const math::Vector4f&          wo,
             const DifferentialGeometry&    dg,
             const math::Vector4f&          wi,
             BRDFType                       type,
             Sample<math::Vector4f>&        ret) const
        {
            if (_numChunks == 0)
            {
                eval(BRDFArray(_BRDFs, _totalBRDFs),
                     wo, dg, wi, type, ret);
                return;
            }
            //---
            ret.value.setConstant(0.0f);
            ret.pdf = 0.0f;

            const BRDF* const* BRDFs = _BRDFs;
            for (size_t n = 0; n < _numChunks; ++n)
            {
                const size_t numBRDFs = _numBRDFs[n];
                if (_weights[n] > 1e-6f)
                {
                    Sample<math::Vector4f> sample;
                    eval(BRDFArray(BRDFs, numBRDFs),
                         wo, dg, wi, type, sample);
                    ret += sample * _weights[n];
                }
                BRDFs += numBRDFs;
            }
        }
    private:
        static
        void
        eval(const BRDFArray&               BRDFs,
             const math::Vector4f&          wo,
             const DifferentialGeometry&    dg,
             const math::Vector4f&          wi,
             BRDFType                       type,
             Sample<math::Vector4f>&        ret)
        {
            ret.value.setConstant(0.0f);
            ret.pdf = 0.0f;

            size_t num = 0;
            for (size_t i = 0; i < BRDFs.size; ++i)
            {
                const BRDF* brdf = BRDFs.ptr[i];
                assert(brdf);
                if (brdf->type() & type)
                {
                    ret.value   += brdf->eval(wo, dg, wi);
                    ret.pdf     += brdf->pdf(wo, dg, wi);
                    ++num;
                }
            }
            if (num > 0)
                ret.pdf *= math::rcp(static_cast<float>(num));
        }

    public:
        void
        eval(const math::Vector4f&          wo,
             const DifferentialGeometry&    dg,
             const math::Vector4f&          wi,
             BRDFType                       type,
             Sample<RadianceClosure>&       ret) const
        {
            if (_numChunks == 0)
            {
                eval(BRDFArray(_BRDFs, _totalBRDFs),
                     wo, dg, wi, type, ret);
                return;
            }
            //---
            ret.value.set(math::Vector4f::Zero());
            ret.pdf = 0.0f;

            const BRDF* const* BRDFs = _BRDFs;
            for (size_t n = 0; n < _numChunks; ++n)
            {
                const size_t numBRDFs = _numBRDFs[n];
                if (_weights[n] > 1e-6f)
                {
                    Sample<RadianceClosure> sample;
                    eval(BRDFArray(BRDFs, numBRDFs),
                         wo, dg, wi, type, sample);
                    ret += sample * _weights[n];
                }
                BRDFs += numBRDFs;
            }
        }
    private:
        static
        void
        eval(const BRDFArray&               BRDFs,
             const math::Vector4f&          wo,
             const DifferentialGeometry&    dg,
             const math::Vector4f&          wi,
             BRDFType                       type,
             Sample<RadianceClosure>&       ret)
        {
            ret.value.set(math::Vector4f::Zero());
            ret.pdf = 0.0f;

            size_t num = 0;
            for (size_t i = 0; i < BRDFs.size; ++i)
            {
                const BRDF* brdf = BRDFs.ptr[i];
                assert(brdf);
                if (brdf->type() & type)
                {
                    ret.value.add(brdf->type(), brdf->eval(wo, dg, wi));
                    ret.pdf += brdf->pdf(wo, dg, wi);
                    ++num;
                }
            }
            if (num > 0)
                ret.pdf *= math::rcp(static_cast<float>(num));
        }

    private:
        /////////////////////
        // struct BRDFDistribution
        /////////////////////
        struct BRDFDistribution
        {
            /*! probability distribution to sample between BRDF components */
            float   f[MAX_BRDF];
            float   sumF;
            /*! stores sampling of each BRDF component */
            math::Vector4f          color[MAX_BRDF];
            Sample<math::Vector4f>  wi   [MAX_BRDF];
            BRDFType                type [MAX_BRDF];
            size_t                  num;
        public:
            BRDFDistribution():
                sumF(0.0f), num(0)
            { }
        };
        /////////////////////
    public:
        /*! Sample the composited BRDF. We are evaluating all BRDF
        *  components and then importance sampling one of them. */
        math::Vector4f
        sample(const math::Vector4f&        wo,                 /*!< Direction light is reflected into.                    */
               const DifferentialGeometry&  dg,                 /*!< Shade location on a surface to sample the BRDF at.    */
               Sample<math::Vector4f>&      wi_o,               /*!< Returns sampled incoming light direction and PDF.     */
               BRDFType&                    type_o,             /*!< Returns the type flags of dist component.          */
               const math::Vector2f&        s,                  /*!< Sample locations for BRDF are provided by the caller. */
               float                        ss,                 /*!< Sample to select the BRDF component.                  */
               const BRDFType&              type = ALL_BRDF)    /*!< The type of BRDF components to consider.              */ const
        {
            /*! probability distribution to sample between BRDF components */
            /*! stores sampling of each BRDF component */
            BRDFDistribution dist;

            /*! sample each BRDF component and build probability distribution */
            if (_numChunks == 0)
                addBRDFsToPDF(
                    BRDFArray(_BRDFs, _totalBRDFs), 1.0f,
                    wo, dg, s, type,
                    dist);
            else
            {
                const BRDF* const* BRDFs = _BRDFs;
                for (size_t n = 0; n < _numChunks; ++n)
                {
                    const size_t numBRDFs = _numBRDFs[n];
                    if (_weights[n] > 1e-6f)
                        addBRDFsToPDF(
                            BRDFArray(BRDFs, numBRDFs), _weights[n],
                            wo, dg, s, type,
                            dist);
                    BRDFs += numBRDFs;
                }
            }

            /*! exit if we did not find any valid component */
            if (dist.num == 0)
            {
                wi_o = Sample<math::Vector4f>(
                    math::Vector4f::Zero(), 0.0f);
                type_o = BRDFType::NO_BRDF;

                return math::Vector4f::Zero();
            }

            normalizePDF(dist);
            const size_t sIdx = samplePDF(ss, dist);

            wi_o = Sample<math::Vector4f>(
                dist.wi[sIdx].value,
                dist.wi[sIdx].pdf * dist.f[sIdx]);
            type_o = dist.type[sIdx];

            return dist.color[sIdx];
        }
    private:
        static
        void
        addBRDFsToPDF(
            const BRDFArray&            BRDFs,
            float                       weight,
            const math::Vector4f&       wo, /*!< Direction light is reflected into.                    */
            const DifferentialGeometry& dg, /*!< Shade location on a surface to sample the BRDF at.    */
            const math::Vector2f&       s,  /*!< Sample locations for BRDF are provided by the caller. */
            const BRDFType&             type,
            BRDFDistribution&           ret)
        {
            for (size_t i = 0; i < BRDFs.size; ++i)
            {
                assert(ret.num < MAX_BRDF);

                const BRDF* brdf = BRDFs.ptr[i];
                if (!(brdf->type() & type))
                    continue;

                Sample<math::Vector4f>  wi;
                math::Vector4f          color = brdf->sample(wo, dg, wi, s);
                assert(!math::isnan(color));

                if (math::neq3(color, math::Vector4f::Zero()) &&
                    wi.pdf > 0.0f)
                {
                    if (weight < 0.9999999f)
                        color *= weight;
                    const float f = math::add_xyz(color) *
                        math::rcp(wi.pdf);

                    ret.type    [ret.num]   = brdf->type();
                    ret.color   [ret.num]   = color;
                    ret.wi      [ret.num]   = wi;
                    ret.f       [ret.num]   = f;
                    ret.sumF                += f;

                    ++ret.num;
                }
            }
        }
        static inline
        void
        normalizePDF(BRDFDistribution& dist)
        {
            /*! normalize distribution */
            const float rSumF = dist.sumF > 0.0f
                ? math::rcp(dist.sumF)
                : 0.0f;
            for (size_t i = 0; i < dist.num; ++i)
                dist.f[i] *= rSumF;
        }
        static inline
        size_t
        samplePDF(float ss, const BRDFDistribution& dist)
        {
            /*! compute accumulated distribution */
            float d[MAX_BRDF];

            d[0] = dist.f[0];
            for (size_t i = 1; i < dist.num - 1; ++i)
                d[i] = d[i-1] + dist.f[i];
            d[dist.num - 1] = 1.0f;

            /*! sample distribution */
            size_t i = 0;
            while (i < dist.num - 1 && ss > d[i])
                ++i;
            return i;
        }
    };
}
}
