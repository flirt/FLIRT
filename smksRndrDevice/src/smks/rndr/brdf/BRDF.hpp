// ========================================================================= //
// Modification copyright 2016-2018 SUPAMONKS_STUDIO                         //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
// _________________________________________________________________________ //
// Copyright 2009-2013 Intel Corporation                                     //
//                                                                           //
// Licensed under the Apache License, Version 2.0 (the "License");           //
// you may not use this file except in compliance with the License.          //
// You may obtain a copy of the License at                                   //
//                                                                           //
//     http://www.apache.org/licenses/LICENSE-2.0                            //
//                                                                           //
// Unless required by applicable law or agreed to in writing, software       //
// distributed under the License is distributed on an "AS IS" BASIS,         //
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  //
// See the License for the specific language governing permissions and       //
// limitations under the License.                                            //
//                                                                           //
// ========================================================================= //
#pragma once

#include "BRDFType.hpp"
#include "../shape/DifferentialGeometry.hpp"
#include "../sampler/Sample.hpp"
#include "../sampler/ShapeSampler.hpp"

namespace smks { namespace rndr
{
    /*! BRDF interface definition.  A BRDF can be evaluated and sampled, and the sampling PDF       */
    /*! evaluated.  Unlike the definition in the literature, our BRDFs contain a cosine term.       */
    /*! For example, the diffuse BRDF in our system is "a / pi * cos(wi, N)" versus "a / pi".       */
    /*! This makes the formulae for reflection and refraction BRDFs more natural.  This also        */
    /*! adds consistency since the sampling functionality of the BRDF class would have to           */
    /*! include sampling of the cosine term anyway.  As an optimization, evaluation of the          */
    /*! cosine term can be skipped in case it is handled through sampling.                          */
    class BRDF
    {
    private:
        const BRDFType _type; /*! BRDF type hint to the integrator */

    public:
        explicit __forceinline
        BRDF(BRDFType type):
            _type(type)
        { }

        virtual
        ~BRDF()
        { }

        __forceinline
        BRDFType
        type() const
        {
            return _type;
        }

        //<! intrinsic (lighting-free) main color of the BRDF
        virtual
        math::Vector4f
        color() const
        {
            return math::Vector4f::Zero();
        }

        /*! evaluate the BRDF */
        virtual
        math::Vector4f
        eval(const math::Vector4f&          wo, /*! outgoing light direction    */
             const DifferentialGeometry&    dg, /*! shade location on a surface */
             const math::Vector4f&          wi) /*! incoming light direction    */ const
        {
            /*! specular BRDFs cannot be evaluated and should return zero */
            return math::Vector4f::Zero();
        }

        /*! sample the BRDF */
        virtual
        math::Vector4f
        sample(const math::Vector4f &       wo, /*! outgoing light direction        */
               const DifferentialGeometry&  dg, /*! shade location on a surface     */
               Sample<math::Vector4f>&      wi, /*! sampled light direction and PDF */
               const math::Vector2f&        s)  /*! sample location given by caller */ const
        {
            /*! by default we perform a cosine weighted hemisphere sampling */
            wi = cosineSampleHemisphere(s.x(), s.y(), dg.Ns);
            return eval(wo, dg, wi.value);
        }

        /*! evaluate sampling PDF */
        virtual
        float
        pdf(const math::Vector4f &      wo, /*! outgoing light direction    */
            const DifferentialGeometry& dg, /*! shade location on a surface */
            const math::Vector4f &      wi) /*! incoming light direction    */ const
        {
            /*! return the probability density */
            return cosineSampleHemispherePDF(wi, dg.Ns);
        }
    };
}
}

