// ========================================================================= //
// Modification copyright 2016-2018 SUPAMONKS_STUDIO                         //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
// _________________________________________________________________________ //
// Copyright 2009-2013 Intel Corporation                                     //
//                                                                           //
// Licensed under the Apache License, Version 2.0 (the "License");           //
// you may not use this file except in compliance with the License.          //
// You may obtain a copy of the License at                                   //
//                                                                           //
//     http://www.apache.org/licenses/LICENSE-2.0                            //
//                                                                           //
// Unless required by applicable law or agreed to in writing, software       //
// distributed under the License is distributed on an "AS IS" BASIS,         //
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  //
// See the License for the specific language governing permissions and       //
// limitations under the License.                                            //
//                                                                           //
// ========================================================================= //
#pragma once

#include <smks/xchg/macro_enum.hpp>

namespace smks
{
    namespace rndr
    {
        /*! BRDF type can be used as a hint to the integrator to help pick the best integration method  */
        // WARNING: values must not overlap RayType values
        enum BRDFType
        {
            ALL_BRDF                    = 0xFFFFFFFF,    /*! all BRDF components for the given surface        */
            NO_BRDF                     = 0x00000000,    /*! no BRDF components are set for the given surface */
            DIFFUSE_BRDF                = 0x000F000F,    /*! all diffuse BRDFs for the given surface          */
            DIFFUSE_REFLECTION_BRDF     = 0x00000001,    /*! fully diffuse reflection BRDF                    */
            DIFFUSE_TRANSMISSION_BRDF   = 0x00010000,    /*! fully diffuse transmission BRDF                  */
            //JIMENEZ_BRDF              = 0x02000002,    /*! all Jimenez BRDFs for the given surface          */
            //JIMENEZ_REFLECTION_BRDF   = 0x00000002,    /*! diffuse reflectance to be blurred [Jimenez 2009] */
            //JIMENEZ_TRANSMISSION_BRDF = 0x02000000,    /*! approximate diffuse transmission  [Jimenez 2010] */
            GLOSSY_BRDF                 = 0x00F000F0,    /*! all glossy BRDFs for the given surface           */
            GLOSSY_REFLECTION_BRDF      = 0x00000010,    /*! semi-specular reflection BRDF                    */
            GLOSSY_TRANSMISSION_BRDF    = 0x00100000,    /*! semi-specular transmission BRDF                  */
            SINGULAR_BRDF               = 0x0F000F00,    /*! all specular BRDFs for the given surface         */
            SINGULAR_REFLECTION_BRDF    = 0x00000100,    /*! perfect specular reflection BRDF                 */
            SINGULAR_TRANSMISSION_BRDF  = 0x01000000,    /*! perfect specular transmission BRDF               */
            REFLECTION_BRDF             = 0x00000FFF,    /*! all reflection BRDFs for the given surface       */
            TRANSMISSION_BRDF           = 0x0FFF0000     /*! all transmission BRDFs for the given surface     */
        };
        ENUM_BITWISE_OPS(BRDFType)
    }
}
