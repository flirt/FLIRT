// ========================================================================= //
// Modification copyright 2016-2018 SUPAMONKS_STUDIO                         //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
// _________________________________________________________________________ //
// Copyright 2009-2013 Intel Corporation                                     //
//                                                                           //
// Licensed under the Apache License, Version 2.0 (the "License");           //
// you may not use this file except in compliance with the License.          //
// You may obtain a copy of the License at                                   //
//                                                                           //
//     http://www.apache.org/licenses/LICENSE-2.0                            //
//                                                                           //
// Unless required by applicable law or agreed to in writing, software       //
// distributed under the License is distributed on an "AS IS" BASIS,         //
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  //
// See the License for the specific language governing permissions and       //
// limitations under the License.                                            //
//                                                                           //
// ========================================================================= //
#pragma once

#include "BRDF.hpp"
#include "optics.hpp"

namespace smks { namespace rndr
{
    /*! BRDF of the transmission part of a thin dielectricum. Supports a
    *  color of the dielectric medium. */
    class ThinDielectricTransmission :
        public BRDF
    {
    private:
        float           _eta;       /*! Relative refraction index etai/etat of both media. */
        math::Vector4f  _logT;      /*! Logarithm of volumetric transmission coefficient. */
        float           _thickness; /*! Thickness of the medium. */

    public:

        /*! Thin dielectric transmission BRDF constructor. \param etai is
        *  the refraction index of the medium the incident ray travels in
        *  \param etat is the refraction index of the opposite medium
        *  \param T is volumetric transmission coefficient \param
        *  thickness is the thickness of the medium */
        __forceinline
        ThinDielectricTransmission(float                    etai,
                                   float                    etat,
                                   const math::Vector4f&    T,
                                   float                    thickness):
            BRDF        (SINGULAR_TRANSMISSION_BRDF),
            _eta        (etai * math::rcp(etat)),
            _logT       (math::log(T)),
            _thickness  (thickness)
        { }

        __forceinline
        operator math::Vector4f() const
        {
            return math::exp(_logT);
        }

        __forceinline
        math::Vector4f
        eval(const math::Vector4f&          wo,
             const DifferentialGeometry&    dg,
             const math::Vector4f&          wi) const
        {
            return math::Vector4f::Zero();
        }

        math::Vector4f
        sample(const math::Vector4f&        wo,
               const DifferentialGeometry&  dg,
               Sample<math::Vector4f>&      wi,
               const math::Vector2f&        s) const
        {
            wi.value    = -wo;
            wi.pdf      = 1.0f;

            float cTheta = math::clamp(math::dot3(wo, dg.Ns));

            if (cTheta <= 0.0f)
                return math::Vector4f::Zero();

            float alpha = _thickness * math::rcp(cTheta);

            return math::exp(_logT * alpha) * (1.0f - fresnelDielectric(cTheta, _eta));
        }

        float
        pdf(const math::Vector4f&       wo,
            const DifferentialGeometry& dg,
            const math::Vector4f&       wi) const
        {
            return 0.0f;
        }
    };
}
}
