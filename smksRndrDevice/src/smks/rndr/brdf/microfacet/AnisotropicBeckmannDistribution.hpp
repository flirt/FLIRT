// ========================================================================= //
// Modification copyright 2016-2018 SUPAMONKS_STUDIO                         //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
// _________________________________________________________________________ //
// Copyright 2009-2013 Intel Corporation                                     //
//                                                                           //
// Licensed under the Apache License, Version 2.0 (the "License");           //
// you may not use this file except in compliance with the License.          //
// You may obtain a copy of the License at                                   //
//                                                                           //
//     http://www.apache.org/licenses/LICENSE-2.0                            //
//                                                                           //
// Unless required by applicable law or agreed to in writing, software       //
// distributed under the License is distributed on an "AS IS" BASIS,         //
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  //
// See the License for the specific language governing permissions and       //
// limitations under the License.                                            //
//                                                                           //
// ========================================================================= //
#pragma once

#include <smks/sys/constants.hpp>
#include <smks/math/math.hpp>
#include <smks/math/types.hpp>

#include "../../sampler/Sample.hpp"

namespace smks { namespace rndr
{
    /*! Anisotropic Beckmann microfacet distribution. */
    class AnisotropicBeckmannDistribution
    {
    private:
        const math::Vector4f    dx;         //!< x-direction of the distribution.
        const float             sigmaX;     //!< Standard deviation of distribution in X direction.
        const float             rSigmaX;    //!< Reciprocal standard deviation of distribution in X direction.
        const math::Vector4f    dy;         //!< y-direction of the distribution.
        const float             sigmaY;     //!< Standard deviation of distribution in Y direction.
        const float             rSigmaY;    //!< Reciprocal standard deviation of distribution in X direction.
        const math::Vector4f    dz;         //!< z-direction of the distribution.
        const float             norm;       //!< Normalization constant.

    public:

        /*! Distribution constructor. */
        __forceinline
        AnisotropicBeckmannDistribution(const math::Vector4f& dx, float sigmaX, const math::Vector4f& dy, float sigmaY, const math::Vector4f& dz):
            dx      (dx),
            sigmaX  (sigmaX),
            rSigmaX (math::rcp(sigmaX)),
            dy      (dy),
            sigmaY  (sigmaY),
            rSigmaY (math::rcp(sigmaY)),
            dz      (dz),
            norm    (static_cast<float>(sys::two_pi) * sigmaX * sigmaY)
        {}

        /*! Evaluates the distribution. \param wh is the half vector */
        __forceinline
        float
        eval(const math::Vector4f& wh) const
        {
            const float cPhiR   = wh.dot(dx);
            const float sPhiR   = wh.dot(dy);
            const float R       = math::sqr(cPhiR) + math::sqr(sPhiR);
            const float cTheta  = wh.dot(dz);
            const float c2Theta = math::sqr(cTheta);
            const float s2Theta = 1 - c2Theta;
            if (R == 0.0f)
                return math::rcp(norm * math::sqr(c2Theta));
            const float rSigma2 = (math::sqr(cPhiR * rSigmaX) + math::sqr(sPhiR * rSigmaY)) * math::rcp(R);
            return math::exp(-rSigma2 * s2Theta * math::rcp(c2Theta)) * math::rcp(norm * math::sqr(c2Theta));
        }

        /*! Samples the distribution. \param s is the sample location
        *  provided by the caller. */
        __forceinline
        Sample<math::Vector4f>
        sample(const math::Vector2f& s) const
        {
            const float phi     = static_cast<float>(sys::two_pi) * s.x();
            const float sPhiR   = sigmaY * math::sin(phi);
            const float cPhiR   = sigmaX * math::cos(phi);
            const float rcpR    = math::rsqrt(math::sqr(sPhiR) + math::sqr(cPhiR));
            const float sPhi    = sPhiR * rcpR;
            const float cPhi    = cPhiR * rcpR;
            const float rSigma2 = math::sqr(cPhi * rSigmaX) + math::sqr(sPhi * rSigmaY);
            const float sThetaT = math::sqrt(-math::log(s.y()));
            const float cThetaT = math::sqrt(rSigma2);
            const float rT      = math::rsqrt(math::sqr(cThetaT) + math::sqr(sThetaT));
            const float cTheta  = cThetaT * rT;
            const float c2Theta = math::sqr(cTheta);
            const float sTheta  = sThetaT * rT;
            const float s2Theta = math::sqr(sTheta);
            const float pdf     = math::exp(-rSigma2 * s2Theta * math::rcp(c2Theta)) * math::rcp(norm * cTheta * c2Theta);
            const math::Vector4f& wh = cPhi * sTheta * dx + sPhi * sTheta * dy + cTheta * dz;
            return Sample<math::Vector4f>(wh, pdf);
        }

        /*! Evaluates the sampling PDF. \param wh is the direction to
        *  evaluate the PDF for \returns the probability density */
        __forceinline
        float
        pdf(const math::Vector4f& wh)  const
        {
            const float cPhiR  = wh.dot(dx);
            const float sPhiR  = wh.dot(dy);
            const float cTheta = wh.dot(dz);
            if (cTheta < 0.0f)
                return 0.0f;
            const float R = math::sqr(cPhiR) + math::sqr(sPhiR);
            if (R == 0.0f)
                return math::rcp(norm * cTheta * math::sqr(cTheta));
            const float rSigma2 = (math::sqr(cPhiR*rSigmaX)+math::sqr(sPhiR*rSigmaY))*math::rcp(R);
            const float c2Theta = math::sqr(cTheta);
            const float s2Theta = 1-c2Theta;
            return math::exp(-rSigma2 * s2Theta * math::rcp(c2Theta)) * math::rcp(norm * cTheta * c2Theta);
        }
    };
}
}

