// ========================================================================= //
// Modification copyright 2016-2018 SUPAMONKS_STUDIO                         //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
// _________________________________________________________________________ //
// Copyright 2009-2013 Intel Corporation                                     //
//                                                                           //
// Licensed under the Apache License, Version 2.0 (the "License");           //
// you may not use this file except in compliance with the License.          //
// You may obtain a copy of the License at                                   //
//                                                                           //
//     http://www.apache.org/licenses/LICENSE-2.0                            //
//                                                                           //
// Unless required by applicable law or agreed to in writing, software       //
// distributed under the License is distributed on an "AS IS" BASIS,         //
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  //
// See the License for the specific language governing permissions and       //
// limitations under the License.                                            //
//                                                                           //
// ========================================================================= //
#pragma once

#include <smks/sys/constants.hpp>
#include <smks/math/math.hpp>
#include <smks/math/types.hpp>

#include "../../sampler/Sample.hpp"

namespace smks { namespace rndr
{
    /*! Anisotropic power cosine microfacet distribution. */
    class AnisotropicPowerCosineDistribution
    {
    private:
        const math::Vector4f    _dx;    //!< x-direction of the distribution.
        const float             _nx;    //!< Glossiness in x direction with range [0,infinity[ where 0 is a diffuse surface.
        const math::Vector4f    _dy;    //!< y-direction of the distribution.
        const float             _ny;    //!< Exponent that determines the glossiness in y direction.
        const math::Vector4f    _dz;    //!< z-direction of the distribution.
        const float             _norm1; //!< Normalization constant for calculating the pdf for sampling.
        const float             _norm2; //!< Normalization constant for calculating the distribution.

    public:

        /*! Anisotropic power cosine distribution constructor. */
        __forceinline
        AnisotropicPowerCosineDistribution(const math::Vector4f& dx, float nx, const math::Vector4f& dy, float ny, const math::Vector4f& dz):
            _dx(dx),
            _nx(nx),
            _dy(dy),
            _ny(ny),
            _dz(dz),
            _norm1(math::sqrt((_nx+1)*(_ny+1)) * static_cast<float>(sys::one_over_two_pi)),
            _norm2(math::sqrt((_nx+2)*(_ny+2)) * static_cast<float>(sys::one_over_two_pi))
        {
        }

        /*! Evaluates the power cosine distribution. \param wh is the half
        *  vector */
        __forceinline
            float
            eval(const math::Vector4f& wh) const
        {
            const float cPhiH   = math::dot3(wh, _dx);
            const float sPhiH   = math::dot3(wh, _dy);
            const float cThetaH = math::dot3(wh, _dz);
            const float R       = math::sqr(cPhiH)+math::sqr(sPhiH);

            if (R == 0.0f)
                return _norm2;

            const float n = (_nx*math::sqr(cPhiH)+_ny*math::sqr(sPhiH))*math::rcp(R);

            return _norm2 * math::pow(math::abs(cThetaH), n);
        }

        /*! Samples the distribution. \param s is the sample location
        *  provided by the caller. */
        __forceinline
        Sample<math::Vector4f>
        sample(const math::Vector2f& s) const
        {
            const float phi     = static_cast<float>(sys::two_pi) * s.x();
            const float sPhi0   = math::sqrt(_nx+1) * math::sin(phi);
            const float cPhi0   = math::sqrt(_ny+1) * math::cos(phi);
            const float norm    = math::rsqrt(math::sqr(sPhi0) + math::sqr(cPhi0));
            const float sPhi    = sPhi0 *norm;
            const float cPhi    = cPhi0 * norm;
            const float n       = _nx * math::sqr(cPhi) + _ny * math::sqr(sPhi);
            const float cTheta  = math::pow(s.y(), math::rcp(n+1));
            const float sTheta  = math::cos2sin(cTheta);
            const float pdf     = _norm1 * math::pow(cTheta,n);
            const math::Vector4f    h   (cPhi * sTheta, sPhi * sTheta, cTheta, 0.0f);
            const math::Vector4f&   wh  = h.x() * _dx + h.y() * _dy + h.z() * _dz;

            return Sample<math::Vector4f>(wh.normalized(), pdf);
        }

        /*! Evaluates the sampling PDF. \param wh is the direction to
        *  evaluate the PDF for \returns the probability density */
        __forceinline
        float
        pdf(const math::Vector4f& wh) const
        {
            const float cPhiH   = math::dot3(wh, _dx);
            const float sPhiH   = math::dot3(wh, _dy);
            const float cThetaH = math::dot3(wh, _dz);
            const float R = math::sqr(cPhiH) + math::sqr(sPhiH);

            if (R == 0.0f)
                return _norm1;

            const float n = (_nx * math::sqr(cPhiH) + _ny * math::sqr(sPhiH))
                * math::rcp(R);

            return _norm1 * math::pow(math::abs(cThetaH), n);
        }

        __forceinline
        const math::Vector4f&
        dz() const
        {
            return _dz;
        }
    };
}
}
