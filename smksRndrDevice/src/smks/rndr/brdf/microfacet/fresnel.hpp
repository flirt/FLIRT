// ========================================================================= //
// Modification copyright 2016-2018 SUPAMONKS_STUDIO                         //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
// _________________________________________________________________________ //
// Copyright 2009-2013 Intel Corporation                                     //
//                                                                           //
// Licensed under the Apache License, Version 2.0 (the "License");           //
// you may not use this file except in compliance with the License.          //
// You may obtain a copy of the License at                                   //
//                                                                           //
//     http://www.apache.org/licenses/LICENSE-2.0                            //
//                                                                           //
// Unless required by applicable law or agreed to in writing, software       //
// distributed under the License is distributed on an "AS IS" BASIS,         //
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  //
// See the License for the specific language governing permissions and       //
// limitations under the License.                                            //
//                                                                           //
// ========================================================================= //
#pragma once

#include <smks/sys/constants.hpp>
#include <smks/math/math.hpp>
#include <smks/math/types.hpp>
#include "../optics.hpp"

namespace smks { namespace rndr
{
    /*! Fresnel term for perfect reflection. */
    class FresnelNone
    {
    public:
        /*! Evaluation simply returns white. */
        __forceinline
        math::Vector4f
        eval(float) const
        {
            return math::Vector4f::Ones();
        }
    };


    /*! Constant fresnel term for perfect reflection. */
    class FresnelConstant
    {
    private:
        const math::Vector4f c;  //!< constant reflectivity

    public:
        /*! Constant fresnel term constructor. \param c is the constant
        *  reflectivity to return. */
        __forceinline
        FresnelConstant(math::Vector4f c):
            c(c)
        { }

        /*! Evaluation simply returns constant. */
        __forceinline
        math::Vector4f
        eval(float cTheta) const
        {
            return c;
        }
    };

    /*! Schlick approximation of fresnel term of a dielectric
    *  surface. */
    class SchlickDielectric
    {
    private:
        const float f0;  //!< reflectivity of the material along the normal vector

    public:
        /*! Fresnel term constructor. \param f0 is the reflectivity of the
        *  material along the normal vector. */
        __forceinline
        SchlickDielectric(float f0):
            f0(f0)
        { }

        /*! Evaluates the fresnel term. \param cTheta is the cosine
        *  between the facet normal (half vector) and the viewing
        *  vector. */
        __forceinline
        math::Vector4f
        eval(float cTheta) const
        {
            const float b  = 1.0f - cTheta;
            const float bb = b * b;
            return math::Vector4f::Constant(f0 + (1.0f - f0) * bb * bb * b);
        }
    };

    /*! Fresnel term of a dielectric surface. A dielectric surface is
    *  for instance glass or water. */
    class FresnelDielectric
    {
    private:
        const float etai;   /*! refraction index of the medium the incident ray travels in */
        const float etat;   /*! refraction index of the medium the outgoing transmission rays travels in */

    public:

        /*! Dielectric fresnel term constructor. \param etai is the
        *  refraction index of the medium the incident ray travels in
        *  \param etat is the refraction index of the opposite medium */
        __forceinline
        FresnelDielectric(float etai, float etat):
            etai(etai), etat(etat)
        { }

        /*! Evaluates the fresnel term. \param cTheta is the cosine
        *  between the facet normal (half vector) and the viewing
        *  vector. */
        __forceinline
        math::Vector4f
        eval(float cTheta) const
        {
            return math::Vector4f::Constant(fresnelDielectric(cTheta, etai * math::rcp(etat)));
        }
    };

    /*! Fresnel term for a metal surface. */
    class FresnelConductor
    {
    private:
        const math::Vector4f eta;  //!< Real part of refraction index
        const math::Vector4f k;    //!< Imaginary part of refraction index

    public:

        /*! Conductor fresnel term constructor. \param eta is the real part of
        *  the refraction index \param k is the imaginary part of the
        *  refraction index */
        __forceinline
        FresnelConductor(const math::Vector4f& eta, const math::Vector4f& k):
            eta(eta), k(k)
        { }

        /*! Evaluates the fresnel term. \param cTheta is the cosine
        *  between the facet normal (half vector) and the viewing
        *  vector. */
        //__forceinline math::Vector4f eval(float cTheta) const { return fresnelConductor(cTheta,eta,k); }
        __forceinline
        math::Vector4f
        eval(float cTheta) const
        {
            return fresnelConductorExact(cTheta, eta, k);
        }
    };
}
}
