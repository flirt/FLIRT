// ========================================================================= //
// Modification copyright 2016-2018 SUPAMONKS_STUDIO                         //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
// _________________________________________________________________________ //
// Copyright 2009-2013 Intel Corporation                                     //
//                                                                           //
// Licensed under the Apache License, Version 2.0 (the "License");           //
// you may not use this file except in compliance with the License.          //
// You may obtain a copy of the License at                                   //
//                                                                           //
//     http://www.apache.org/licenses/LICENSE-2.0                            //
//                                                                           //
// Unless required by applicable law or agreed to in writing, software       //
// distributed under the License is distributed on an "AS IS" BASIS,         //
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  //
// See the License for the specific language governing permissions and       //
// limitations under the License.                                            //
//                                                                           //
// ========================================================================= //
#pragma once

#include <smks/sys/constants.hpp>
#include <smks/math/math.hpp>
#include <smks/math/matrixAlgo.hpp>
#include <smks/math/types.hpp>

#include "../../sampler/Sample.hpp"

namespace smks { namespace rndr
{
    /*! Beckmann microfacet distribution. */
    class BeckmannDistribution
    {
    private:
        const float             _sigma;     //!< Standard deviation of distribution.
        const float             _rSigma;    //!< Reciprocal standard deviation of distribution.
        const float             _rSigma2;
        const math::Vector4f    _dz;        //!< z-direction of the distribution.
        const float             _norm;      //!< Normalization constant.

    public:

        /*! Distribution constructor. */
        __forceinline
        BeckmannDistribution(float sigma, const math::Vector4f& dz):
            _sigma  (sigma),
            _rSigma (math::rcp(sigma)),
            _rSigma2(math::rcp(sigma * sigma)),
            _dz     (dz),
            _norm   (static_cast<float>(sys::two_pi) * sigma * sigma)
        { }

        /*! Evaluates the distribution. \param wh is the half vector */
        __forceinline
        float
        eval(const math::Vector4f& wh) const
        {
            const float cTheta  = math::dot3(wh, _dz);
            const float c2Theta = math::sqr(cTheta);
            const float c4Theta = math::sqr(c2Theta);
            if (c4Theta < 1e-6f)
                return 0.0f;

            const float t2Theta = (1.0f - c2Theta) * math::rcp(c2Theta);

            return math::rcp(_norm * c4Theta) * math::exp(- t2Theta * _rSigma2);
        }

        /*! Samples the distribution. \param s is the sample location
        *  provided by the caller. */
        __forceinline
        Sample<math::Vector4f>
        sample(const math::Vector2f& s) const
        {
            assert(!(s.x() < 0.0f) && s.x() < 1.0f);
            assert(!(s.y() < 0.0f) && s.y() < 1.0f);

            const float phi     = static_cast<float>(sys::two_pi) * s.x();
            const float cPhi    = math::cos(phi);
            const float sPhi    = math::sin(phi);

            const float t2Theta = - math::sqr(_sigma) * math::log(1.0f - s.y());
            const float cTheta  = math::rsqrt(1.0f + t2Theta);
            const float c2Theta = math::sqr(cTheta);
            const float sTheta  = math::sqrt(1.0f - c2Theta);

            const float pdf     = math::rcp(_norm * cTheta * c2Theta) * (1.0f - s.y());

            const math::Vector4f& wh = math::transformVectorInFrame(_dz,
                math::Vector4f(cPhi * sTheta, sPhi * sTheta, cTheta, 0.0f));

            assert(!math::isnan(wh));
            assert(!math::isnan(pdf));

            return Sample<math::Vector4f>(wh, pdf);
        }

        /*! Evaluates the sampling PDF. \param wh is the direction to
        *  evaluate the PDF for \returns the probability density */
        __forceinline
        float
        pdf(const math::Vector4f& wh) const
        {
            const float cTheta = math::dot3(wh, _dz);
            if (cTheta < 1e-6f)
                return 0.0f;

            return eval(wh) * cTheta;
        }
    };
}
}
