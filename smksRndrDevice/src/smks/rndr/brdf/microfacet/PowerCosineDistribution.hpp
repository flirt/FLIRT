// ========================================================================= //
// Modification copyright 2016-2018 SUPAMONKS_STUDIO                         //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
// _________________________________________________________________________ //
// Copyright 2009-2013 Intel Corporation                                     //
//                                                                           //
// Licensed under the Apache License, Version 2.0 (the "License");           //
// you may not use this file except in compliance with the License.          //
// You may obtain a copy of the License at                                   //
//                                                                           //
//     http://www.apache.org/licenses/LICENSE-2.0                            //
//                                                                           //
// Unless required by applicable law or agreed to in writing, software       //
// distributed under the License is distributed on an "AS IS" BASIS,         //
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  //
// See the License for the specific language governing permissions and       //
// limitations under the License.                                            //
//                                                                           //
// ========================================================================= //
#pragma once

#include <smks/sys/constants.hpp>
#include <smks/math/math.hpp>
#include <smks/math/matrixAlgo.hpp>
#include <smks/math/types.hpp>

#include "../../sampler/Sample.hpp"

namespace smks { namespace rndr
{
    /*! Power cosine microfacet distribution. */
    class PowerCosineDistribution
    {
    private:
        const float             _n;      //!< Glossiness with range [0,infinity[ where 0 is a diffuse surface.
        const math::Vector4f    _dz;     //!< z-direction of the distribution.
        const float             _norm1;  //!< Normalization constant for calculating the pdf for sampling.
        const float             _norm2;  //!< Normalization constant for calculating the distribution.
    public:

        /*! Power cosine distribution constructor. */
        __forceinline
        PowerCosineDistribution(float n, const math::Vector4f& dz) :
            _n      (n),
            _dz     (dz),
            _norm1  ((n + 1.0f) * static_cast<float>(sys::one_over_two_pi)),
            _norm2  ((n + 2.0f) * static_cast<float>(sys::one_over_two_pi))
        {
            assert(!(n < 0.0f));
        }

        /*! Evaluates the power cosine distribution. \param wh is the half
        *  vector */
        __forceinline
        float
        eval(const math::Vector4f& wh) const
        {
            const float cTheta = math::dot3(wh, _dz);
            return _norm2 * math::pow(math::abs(cTheta), _n);
        }

        /*! Samples the distribution. \param s is the sample location
        *  provided by the caller. */
        __forceinline
        Sample<math::Vector4f>
        sample(const math::Vector2f& s) const
        {
            const float phi     = static_cast<float>(sys::two_pi) * s.x();
            const float cPhi    = math::cos(phi);
            const float sPhi    = math::sin(phi);
            const float cTheta  = math::pow(s.y(), math::rcp(_n + 1.0f));
            const float sTheta  = math::cos2sin(cTheta);

            return Sample<math::Vector4f>(
                math::transformVectorInFrame(_dz, math::Vector4f(cPhi * sTheta, sPhi * sTheta, cTheta, 0.0f)),
                _norm1 * math::pow(cTheta, _n));
        }

        /*! Evaluates the sampling PDF. \param wh is the direction to
        *  evaluate the PDF for \returns the probability density */
        __forceinline
        float
        pdf(const math::Vector4f& wh) const
        {
            const float cTheta = math::dot3(wh, _dz);
            return _norm1 * math::pow(math::abs(cTheta), _n);
        }
    };
}
}
