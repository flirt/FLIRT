// ========================================================================= //
// Modification copyright 2016-2018 SUPAMONKS_STUDIO                         //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
// _________________________________________________________________________ //
// Copyright 2009-2013 Intel Corporation                                     //
//                                                                           //
// Licensed under the Apache License, Version 2.0 (the "License");           //
// you may not use this file except in compliance with the License.          //
// You may obtain a copy of the License at                                   //
//                                                                           //
//     http://www.apache.org/licenses/LICENSE-2.0                            //
//                                                                           //
// Unless required by applicable law or agreed to in writing, software       //
// distributed under the License is distributed on an "AS IS" BASIS,         //
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  //
// See the License for the specific language governing permissions and       //
// limitations under the License.                                            //
//                                                                           //
// ========================================================================= //
#pragma once

#include <smks/sys/constants.hpp>
#include <smks/math/math.hpp>
#include <smks/math/types.hpp>

#include "../sampler/Sample.hpp"

/*! \file optics.h This file collects all operations of light waves
*  relevant for rendering, such as: computing reflection and
*  refraction directions, or fresnel terms for different kind of
*  media. */

namespace smks { namespace rndr
{
    /*! Reflects a viewing vector V at a normal N. */
    __forceinline
    Sample<math::Vector4f>
    reflect(const math::Vector4f& V, const math::Vector4f& N)
    {
        assert(math::isNormalized3(V));
        assert(math::isNormalized3(N));
        //---
        return Sample<math::Vector4f>((2.0f * math::dot3(V, N)) * N - V, 1.0f);
    }

    /*! Reflects a viewing vector V at a normal N. Cosine between V
    *  and N is given as input. */
    __forceinline
    Sample<math::Vector4f>
    reflect(const math::Vector4f& V, const math::Vector4f& N, float cosi)
    {
        assert(math::isNormalized3(V));
        assert(math::isNormalized3(N));
        //---
        return Sample<math::Vector4f>((2.0f * cosi) * N - V, 1.0f);
    }

    /*! Mirror a viewing vector V at a normal N. */
    __forceinline
    math::Vector4f
    mirror(const math::Vector4f& V, const math::Vector4f& N)
    {
        assert(math::isNormalized3(V));
        assert(math::isNormalized3(N));
        //---
        return V - (2.0f * math::dot3(V, N)) * N;
    }

    /*! Refracts a viewing vector V at a normal N using the relative
    *  refraction index eta. Eta is refraction index of outside medium
    *  (where N points into) divided by refraction index of the inside
    *  medium. The vectors V and N have to point towards the same side
    *  of the surface. */
    __forceinline
    Sample<math::Vector4f>
    refract(const math::Vector4f& V, const math::Vector4f& N, float eta)
    {
        assert(math::isNormalized3(V));
        assert(math::isNormalized3(N));
        //---
        const float cosi    = math::dot3(V, N);
        const float sqrEta  = math::sqr(eta);
        const float cost2   = 1.0f - sqrEta * (1.0f - math::sqr(cosi));
        if (cost2 < 0.0f)
            return Sample<math::Vector4f>(math::Vector4f::Zero(), 0.0f);

        const float cost = math::sqrt(cost2);
        return Sample<math::Vector4f>(eta * (cosi * N - V) - cost * N, sqrEta);
    }

    /*! Refracts a viewing vector V at a normal N using the relative
    *  refraction index eta. Eta is refraction index of outside medium
    *  (where N points into) divided by refraction index of the inside
    *  medium. The vectors V and N have to point towards the same side
    *  of the surface. The cosine between V and N is given as input. */
    __forceinline
    Sample<math::Vector4f>
    refract(const math::Vector4f& V, const math::Vector4f& N, float eta, float cosi)
    {
        assert(math::isNormalized3(V));
        assert(math::isNormalized3(N));
        //---
        const float sqrEta  = math::sqr(eta);
        const float cost2   = 1.0f - sqrEta * (1.0f - math::sqr(cosi));
        if (cost2 < 0.0f)
            return Sample<math::Vector4f>(math::Vector4f::Zero(), 0.0f);

        const float cost = math::sqrt(cost2);
        return Sample<math::Vector4f>(eta * (cosi * N - V) - cost * N, sqrEta);
    }

    /*!Refracts a viewing vector V at a normal N using the relative
    *  refraction index eta. Eta is refraction index of outside medium
    *  (where N points into) divided by refraction index of the inside
    *  medium. The vectors V and N have to point towards the same side
    *  of the surface. The cosine between V and N is given as input and
    *  the cosine of -N and transmission ray is computed as output. */
    __forceinline
    Sample<math::Vector4f>
    refract(const math::Vector4f& V, const math::Vector4f& N, float eta, float cosi, float& cost)
    {
        assert(math::isNormalized3(V));
        assert(math::isNormalized3(N));
        //---
        const float sqrEta  = math::sqr(eta);
        const float k       = 1.0f - sqrEta * (1.0f - math::sqr(cosi));
        if (k < 0.0f)
        {
            cost = 0.0f;
            return Sample<math::Vector4f>(math::Vector4f::Zero(), 0.0f);
        }
        cost = math::sqrt(k);
        return Sample<math::Vector4f>(eta * (cosi * N - V) - cost * N, sqrEta);
    }

    /*! Computes fresnel coefficient for a media interface with ouside
    *  refraction index of etai and inside refraction index of
    *  etat. Both cosines have to be positive. */
    __forceinline
    float
    fresnelDielectric(float cosi, float cost, float etai, float etat)
    {
        const float Rper = (etai * cosi - etat * cost) * math::rcp(etai * cosi + etat * cost);
        const float Rpar = (etat * cosi - etai * cost) * math::rcp(etat * cosi + etai * cost);
        return 0.5f * (math::sqr(Rpar) + math::sqr(Rper));
    }

    /*! Computes fresnel coefficient for media interface with relative
    *  refraction index eta. Eta is the outside refraction index
    *  divided by the inside refraction index. Both cosines have to be
    *  positive. */
    __forceinline
    float
    fresnelDielectric(float cosi, float cost, float eta)
    {
        const float Rper = (eta * cosi -       cost) * math::rcp(eta * cosi +       cost);
        const float Rpar = (      cosi - eta * cost) * math::rcp(      cosi + eta * cost);
        return 0.5f * (math::sqr(Rpar) + math::sqr(Rper));
    }

    /*! Computes fresnel coefficient for media interface with relative
    *  refraction index eta. Eta is the outside refraction index
    *  divided by the inside refraction index. The cosine has to be
    *  positive. */
    __forceinline
    float
    fresnelDielectric(float cosi, float eta)
    {
        assert(eta > 0.0f);
        assert(!(cosi < 0.0f));
        //---
        const float k = 1.0f - math::sqr(eta) * (1.0f - math::sqr(cosi));
        if (k < 0.0f)
            return 1.0f;

        const float cost = math::sqrt(k);

        return fresnelDielectric(cosi, cost, eta);
    }

    /*! Computes fresnel coefficient for conductor medium with complex
    *  refraction index (eta,k). The cosine has to be positive. */
    __forceinline
    math::Vector4f
    fresnelConductor(float cosi, const math::Vector4f& eta, const math::Vector4f& k)
    {
        const math::Vector4f& tmp   = eta.cwiseProduct(eta) + k.cwiseProduct(k);
        const math::Vector4f& Rpar  = ( tmp * cosi*cosi - 2.0f*eta*cosi + math::Vector4f::Ones() )
            .cwiseQuotient( tmp * cosi*cosi + 2.0f*eta*cosi + math::Vector4f::Ones() );
        const math::Vector4f& Rper  = ( tmp - 2.0f*eta*cosi + math::Vector4f::Constant(cosi*cosi) )
            .cwiseQuotient( tmp + 2.0f*eta*cosi + math::Vector4f::Constant(cosi*cosi) );

        return 0.5f * (Rpar + Rper);
    }

    __forceinline
    math::Vector4f
    fresnelConductorExact(float cosi, const math::Vector4f& eta, const math::Vector4f& k)
    {
        const float cosi2   = math::sqr(cosi);
        const float sini2   = 1.0f - cosi2;
        const float sini4   = math::sqr(sini2);

        const math::Vector4f&   temp1   = eta.cwiseProduct(eta) - k.cwiseProduct(k) - math::Vector4f::Constant(sini2);
        const math::Vector4f&   a2pb2   = (temp1.cwiseProduct(temp1) + k.cwiseProduct(k).cwiseProduct(eta).cwiseProduct(eta) * 4.0f).cwiseSqrt();
        const math::Vector4f&   a       = ((a2pb2 + temp1) * 0.5f).cwiseSqrt();

        const math::Vector4f&   term1   = a2pb2 + math::Vector4f::Constant(cosi2);
        const math::Vector4f&   term2   = a * (2.0f * cosi);

        const math::Vector4f&   Rs2     = ( term1 - term2 ).cwiseQuotient( term1 + term2 );

        const math::Vector4f&   term3   = a2pb2 * cosi2 + math::Vector4f::Constant(sini4);
        const math::Vector4f&   term4   = term2 * sini2;

        const math::Vector4f&   Rp2     = Rs2.cwiseProduct( term3 - term4 ).cwiseQuotient( term3 + term4 );

        return 0.5f * (Rp2 + Rs2);
    }
}
}

