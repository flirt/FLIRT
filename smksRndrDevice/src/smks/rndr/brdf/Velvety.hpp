// ========================================================================= //
// Modification copyright 2016-2018 SUPAMONKS_STUDIO                         //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
// _________________________________________________________________________ //
// Copyright 2009-2013 Intel Corporation                                     //
//                                                                           //
// Licensed under the Apache License, Version 2.0 (the "License");           //
// you may not use this file except in compliance with the License.          //
// You may obtain a copy of the License at                                   //
//                                                                           //
//     http://www.apache.org/licenses/LICENSE-2.0                            //
//                                                                           //
// Unless required by applicable law or agreed to in writing, software       //
// distributed under the License is distributed on an "AS IS" BASIS,         //
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  //
// See the License for the specific language governing permissions and       //
// limitations under the License.                                            //
//                                                                           //
// ========================================================================= //
#pragma once

#include "BRDF.hpp"
#include "optics.hpp"

namespace smks { namespace rndr
{
    /*! Velvety BRDF. For materials with horizon scattering (velvet, peach, etc.)
    *  The BRDF has a reflectance parameter that determines the color of
    *  the surface and a parameter to determine the falloff of horizon scattering. */
    class Velvety:
        public BRDF
    {
    private:
        /*! The reflectance parameter. The vale 0 means no reflection,
        *  and 1 means full reflection. */
        const math::Vector4f _R;

        /*! The falloff of horizon scattering. 0 no falloff,
        *  and inf means maximum falloff. */
        const float _f;

    public:
        /*! Velvety BRDF constructor. This is a diffuse reflection BRDF. */
        __forceinline
        Velvety(const math::Vector4f& R,
                float f):
            BRDF(DIFFUSE_REFLECTION_BRDF),
            _R  (R),
            _f  (f)
        { }

        __forceinline
        operator math::Vector4f() const
        {
            return _R;
        }

        __forceinline
        math::Vector4f
        eval(const math::Vector4f&          wo,
             const DifferentialGeometry&    dg,
             const math::Vector4f&          wi) const
        {
            const float cThetaO         = math::max(0.0f, math::dot3(wo, (dg.Ns)));
            const float cThetaI         = math::max(0.0f, math::dot3(wi, (dg.Ns)));
            const float horizonScatter  = math::pow(math::cos2sin(cThetaO), _f);

            return _R * horizonScatter * cThetaI * static_cast<float>(sys::one_over_pi);
        }

        __forceinline
        math::Vector4f
        sample(const math::Vector4f&        wo,
               const DifferentialGeometry&  dg,
               Sample<math::Vector4f>&      wi,
               const math::Vector2f&        s) const
        {
            wi = cosineSampleHemisphere(s.x(), s.y(), dg.Ns);
            return eval(wo, dg, wi.value);
        }

        __forceinline
        float
        pdf(const math::Vector4f&       wo,
            const DifferentialGeometry& dg,
            const math::Vector4f&       wi) const
        {
            return cosineSampleHemispherePDF(wi, dg.Ns);
        }
    };
}
}
