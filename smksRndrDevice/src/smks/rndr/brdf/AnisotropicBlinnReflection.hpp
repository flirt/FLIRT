// ========================================================================= //
// Modification copyright 2016-2018 SUPAMONKS_STUDIO                         //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
// _________________________________________________________________________ //
// Copyright 2009-2013 Intel Corporation                                     //
//                                                                           //
// Licensed under the Apache License, Version 2.0 (the "License");           //
// you may not use this file except in compliance with the License.          //
// You may obtain a copy of the License at                                   //
//                                                                           //
//     http://www.apache.org/licenses/LICENSE-2.0                            //
//                                                                           //
// Unless required by applicable law or agreed to in writing, software       //
// distributed under the License is distributed on an "AS IS" BASIS,         //
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  //
// See the License for the specific language governing permissions and       //
// limitations under the License.                                            //
//                                                                           //
// ========================================================================= //
#pragma once

#include "BRDF.hpp"
#include "optics.hpp"
#include "microfacet/AnisotropicPowerCosineDistribution.hpp"

namespace smks { namespace rndr
{
    class AnisotropicBlinnReflection:
        public BRDF
    {
    private:
        const math::Vector4f                        _C;
        const AnisotropicPowerCosineDistribution    _D;

    public:
        __forceinline
        AnisotropicBlinnReflection(const math::Vector4f&    C,
                                   const math::Vector4f&    dx,
                                   float                    nx,
                                   const math::Vector4f&    dy,
                                   float                    ny,
                                   const math::Vector4f&    dz):
            BRDF(GLOSSY_REFLECTION_BRDF),
            _C  (C),
            _D  (dx, nx, dy, ny, dz)
        { }

        __forceinline
        math::Vector4f
        eval(const math::Vector4f&          wo,
             const DifferentialGeometry&    dg,
             const math::Vector4f&          wi) const
        {
            const float cThetaI = math::dot3(wi, _D.dz());

            return cThetaI > 0.0f
                ? ( _C * _D.eval((wo + wi).normalized()) * cThetaI ).eval()
                : math::Vector4f::Zero();
        }

        math::Vector4f
        sample(const math::Vector4f&        wo,
               const DifferentialGeometry&  dg,
               Sample<math::Vector4f>&      wi,
               const math::Vector2f&        s) const
        {
            const Sample<math::Vector4f>& wh = _D.sample(s);

            wi.value    = reflect(wo, wh.value).value;
            wi.pdf      = wh.pdf;

            return _C * _D.eval(wh.value) * math::abs(math::dot3(wi.value, _D.dz()));
        }

        float
        pdf(const math::Vector4f&       wo,
            const DifferentialGeometry& dg,
            const math::Vector4f&       wi) const
        {
            return math::dot3(wi, _D.dz()) > 0.0f
                ? _D.pdf((wo + wi).normalized())
                : 0.0f;
        }
    };
}
}
