// ========================================================================= //
// Modification copyright 2016-2018 SUPAMONKS_STUDIO                         //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
// _________________________________________________________________________ //
// Copyright 2009-2013 Intel Corporation                                     //
//                                                                           //
// Licensed under the Apache License, Version 2.0 (the "License");           //
// you may not use this file except in compliance with the License.          //
// You may obtain a copy of the License at                                   //
//                                                                           //
//     http://www.apache.org/licenses/LICENSE-2.0                            //
//                                                                           //
// Unless required by applicable law or agreed to in writing, software       //
// distributed under the License is distributed on an "AS IS" BASIS,         //
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  //
// See the License for the specific language governing permissions and       //
// limitations under the License.                                            //
//                                                                           //
// ========================================================================= //
#pragma once

#include "BRDF.hpp"
#include "optics.hpp"

namespace smks { namespace rndr
{
    /*! Dielectric layer BRDF model. Models a _ground BRDF covered by a
    *  dielectric layer. The model works by refracting the incoming and
    *  outgoing light direction into the medium and then evaluating the
    *  _ground BRDF. The model also supports a transmission coefficient
    *  of the dielectricum. */
    template<typename GroundBRDF>
    class DielectricLayer :
        public BRDF
    {
    private:
        const math::Vector4f    _T;         //!< Transmission coefficient of dielectricum
        const float             _etait;     //!< Relative refraction index etai/etat of both media
        const float             _etati;     //!< relative refraction index etat/etai of both media
        const GroundBRDF        _ground;    //!< the BRDF of the _ground layer

    public:
        /*! Dielectric layer BRDF constructor. \param T is the
        *  transmission coefficient of the dielectricum \param etai is
        *  the refraction index of the medium the incident ray travels in
        *  \param etat is the refraction index of the opposite medium \param _ground is the _ground BRDF to use */
        __forceinline
        DielectricLayer(const math::Vector4f&   T,
                        float                   etai,
                        float                   etat,
                        const GroundBRDF&       ground):
            BRDF    (ground.type()),
            _T      (T),
            _etait  (etai * math::rcp(etat)),
            _etati  (etat * math::rcp(etai)),
            _ground (ground)
        { }

        __forceinline
        operator math::Vector4f() const
        {
            return static_cast<math::Vector4f>(_ground);
        }

        __forceinline
        math::Vector4f
        eval(const math::Vector4f&          wo,
             const DifferentialGeometry&    dg,
             const math::Vector4f&          wi) const
        {
            const float cThetaO = math::dot3(wo, dg.Ns);
            const float cThetaI = math::dot3(wi, dg.Ns);

            if (cThetaI <= 0.0f || cThetaO <= 0.0f)
                return math::Vector4f::Zero();

            float cThetaO1, cThetaI1;
            const math::Vector4f&   wo1 = refract(wo, dg.Ns, _etait, cThetaO, cThetaO1).value;
            const math::Vector4f&   wi1 = refract(wi, dg.Ns, _etait, cThetaI, cThetaI1).value;
            const float             Fi  = 1.0f - fresnelDielectric(cThetaI, cThetaI1, _etait);
            const float             Fo  = 1.0f - fresnelDielectric(cThetaO, cThetaO1, _etait);
            const math::Vector4f&   Fg  = _ground.eval(-wo1, dg, -wi1);

            return Fo * _T.cwiseProduct(Fg).cwiseProduct(_T) * Fi;
        }

        math::Vector4f
        sample(const math::Vector4f&        wo,
               const DifferentialGeometry&  dg,
               Sample<math::Vector4f>&      wi,
               const math::Vector2f&        s) const
        {
            /*! refract ray into medium */
            const float cThetaO = math::dot3(wo, dg.Ns);
            if (cThetaO <= 0.0f)
                return math::Vector4f::Zero();

            float cThetaO1;
            Sample<math::Vector4f> wo1 = refract(wo, dg.Ns, _etait, cThetaO, cThetaO1);

            /*! sample ground BRDF */
            Sample<math::Vector4f> wi1(math::Vector4f::Zero());
            math::Vector4f Fg = _ground.sample(-wo1.value, dg, wi1, s);

            /*! refract ray out of medium */
            const float cThetaI1 = math::dot3(wi1.value, dg.Ns);
            if (cThetaI1 <= 0.0f)
                return math::Vector4f::Zero();

            float cThetaI;
            Sample<math::Vector4f> wi0 = refract(-wi1.value, -dg.Ns, _etati, cThetaI1, cThetaI);
            if (wi0.pdf == 0.0f)
                return math::Vector4f::Zero();

            /*! accumulate contribution of path */
            wi = Sample<math::Vector4f>(wi0.value, wi1.pdf);
            float Fi = 1.0f - fresnelDielectric(cThetaI, cThetaI1, _etait);
            float Fo = 1.0f - fresnelDielectric(cThetaO, cThetaO1, _etait);

            return Fo * _T.cwiseProduct(Fg).cwiseProduct(_T) * Fi;
        }

        float
        pdf(const math::Vector4f& wo, const DifferentialGeometry& dg, const math::Vector4f& wi) const
        {
            const float cThetaO = math::dot3(wo, dg.Ns);
            if (cThetaO < 0.0f)
                return 0.0f;

            const Sample<math::Vector4f>& woPrime = refract(wo, dg.Ns, _etait, cThetaO);
            if (woPrime.pdf == 0.0f)
                return 0.0f;

            const float cThetaI = math::dot3(wi, dg.Ns);
            if (cThetaI < 0.0f)
                return 0.0f;

            const Sample<math::Vector4f>& wiPrime = refract(wi, dg.Ns, _etait, cThetaI);
            if (wiPrime.pdf == 0.0f)
                return 0.0f;

            return _ground.pdf(-woPrime.value, dg, -wiPrime.value);

            //float cThetaO = dot(wo,dg.Ns);
            //if (cThetaO <= 0.0f) return 0.0f;
            //Sample<math::Vector4f> wo1 = refract(wo,dg.Ns,_etait,cThetaO);
            //Sample<math::Vector4f> wi1(math::Vector4f::Zero()); float p = _ground.pdf(-wo1.value,dg,wi1);
            //Sample<math::Vector4f> wi0 = refract(-wi1.value,-dg.Ns,_etati);
            //if (wi0.pdf == 0.0f) return math::Vector4f::Zero();
            //return p;
        }
    };
}
}
