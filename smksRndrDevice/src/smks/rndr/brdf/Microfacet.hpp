// ========================================================================= //
// Modification copyright 2016-2018 SUPAMONKS_STUDIO                         //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
// _________________________________________________________________________ //
// Copyright 2009-2013 Intel Corporation                                     //
//                                                                           //
// Licensed under the Apache License, Version 2.0 (the "License");           //
// you may not use this file except in compliance with the License.          //
// You may obtain a copy of the License at                                   //
//                                                                           //
//     http://www.apache.org/licenses/LICENSE-2.0                            //
//                                                                           //
// Unless required by applicable law or agreed to in writing, software       //
// distributed under the License is distributed on an "AS IS" BASIS,         //
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  //
// See the License for the specific language governing permissions and       //
// limitations under the License.                                            //
//                                                                           //
// ========================================================================= //
#pragma once

#include "BRDF.hpp"
#include "optics.hpp"
#include "microfacet/fresnel.hpp"
#include "microfacet/PowerCosineDistribution.hpp"
#include "microfacet/BeckmannDistribution.hpp"
#include "microfacet/AnisotropicPowerCosineDistribution.hpp"
#include "microfacet/AnisotropicBeckmannDistribution.hpp"

namespace smks { namespace rndr
{
    /*! Microfacet BRDF model. The class is templated over the fresnel
    *  term and microfacet _distribution to use. */
    template<typename Fresnel, typename Distribution>
    class Microfacet:
        public BRDF
    {
    private:
        /*! Reflectivity of the microfacets. The range is [0,1] where 0
        *  means no reflection at all, and 1 means full reflection. */
        const math::Vector4f    _R;
        const Fresnel           _fresnel;       /*! Fresnel term to use. */
        const Distribution      _distribution;  /*! Microfacet _distribution to use. */

    public:

        /*! Microfacet BRDF constructor. This is a glossy BRDF. */
        __forceinline
        Microfacet(const math::Vector4f&    R,
                   const Fresnel&           fresnel,
                   const Distribution&      distribution):
            BRDF            (GLOSSY_REFLECTION_BRDF),
            _R              (R),
            _fresnel        (fresnel),
            _distribution   (distribution)
        { }

        __forceinline
        operator math::Vector4f() const
        {
            return _R;
        }

        __forceinline
        math::Vector4f
        eval(const math::Vector4f&          wo,
             const DifferentialGeometry&    dg,
             const math::Vector4f&          wi) const
        {
            if (math::dot3(wi, dg.Ng) <= 0.0f)
                return math::Vector4f::Zero();

            const float cThetaO = math::dot3(wo, dg.Ns);
            const float cThetaI = math::dot3(wi, dg.Ns);
            if (cThetaI <= 0.0f || cThetaO <= 0.0f)
                return math::Vector4f::Zero();

            const math::Vector4f wh = (wi + wo).normalized();
            const float cThetaH     = math::dot3(wh, dg.Ns);
            const float cTheta      = math::dot3(wi, wh); // = dot(wo, wh);
            const math::Vector4f F  = _fresnel.eval(cTheta);
            const float D = _distribution.eval(wh);
            const float G = math::min(
                1.0f,
                2.0f * cThetaH * cThetaO * math::rcp(cTheta),
                2.0f * cThetaH * cThetaI * math::rcp(cTheta));

            return _R.cwiseProduct(F) * D * G * math::rcp(4.0f * cThetaO);
        }

        math::Vector4f
        sample(const math::Vector4f&        wo,
               const DifferentialGeometry&  dg,
               Sample<math::Vector4f>&      wi,
               const math::Vector2f&        s) const
        {
            if (math::dot3(wo, dg.Ns) <= 0.0f)
                return math::Vector4f::Zero();

            const Sample<math::Vector4f>& wh = _distribution.sample(s);

            wi.value    = reflect(wo, wh.value).value;
            wi.pdf      = wh.pdf * math::rcp(4.0f * math::abs(math::dot3(wo, wh.value)));

            if (math::dot3(wi.value, dg.Ns) <= 0.0f)
                return math::Vector4f::Zero();

            return eval(wo, dg, wi.value);
        }

        float
        pdf(const math::Vector4f&       wo,
            const DifferentialGeometry& dg,
            const math::Vector4f&       wi) const
        {
            const math::Vector4f& wh = (wo + wi).normalized();
            return _distribution.pdf(wh) * math::rcp(4.0f * math::abs(math::dot3(wo, wh)));
        }
    };
}
}
