// ========================================================================= //
// Modification copyright 2016-2018 SUPAMONKS_STUDIO                         //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
// _________________________________________________________________________ //
// Copyright 2009-2013 Intel Corporation                                     //
//                                                                           //
// Licensed under the Apache License, Version 2.0 (the "License");           //
// you may not use this file except in compliance with the License.          //
// You may obtain a copy of the License at                                   //
//                                                                           //
//     http://www.apache.org/licenses/LICENSE-2.0                            //
//                                                                           //
// Unless required by applicable law or agreed to in writing, software       //
// distributed under the License is distributed on an "AS IS" BASIS,         //
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  //
// See the License for the specific language governing permissions and       //
// limitations under the License.                                            //
//                                                                           //
// ========================================================================= //
#include "CompositedBRDF.hpp"
#include "BRDF.hpp"

using namespace smks;

void
rndr::CompositedBRDF::blend(size_t                  numInputs,
                             const CompositedBRDF*  inputs,
                             const float*           weights)
{
    // reset the composited BRDFs
    _numBytes   = 0;
    _totalBRDFs = 0;
    _numChunks  = 0;

    float sumWeights = 0.0f;    // used to normalized weights

    for (size_t n = 0; n < numInputs; ++n)
    {
        const CompositedBRDF& input = inputs[n];

        // copy any data allocated on input composited BRDF
        char* dst = nullptr;
        if (input._numBytes > 0)
        {
            dst = reinterpret_cast<char*>(
                alloc(input._numBytes));
            memcpy(
                dst,
                input._data,
                sizeof(char) * input._numBytes);
        }

        int offset = 0; // memory offset on composite BRDF's allocated data
        for (size_t i = 0; i < input._totalBRDFs; ++i)
        {
            addComponent(
                input._numBytes > 0 &&
                (offset = reinterpret_cast<const char*>(input._BRDFs[i]) - input._data) < input._numBytes
                ? reinterpret_cast<BRDF*>(dst + offset) // input BRDF allocated on the composited BRDF
                : input._BRDFs[i]);
        }

        // compute new chunks' weights
        if (input._numChunks == 0)
            sumWeights += addChunk(
                    weights[n],
                    input._totalBRDFs);
        else
            for (char j = 0; j < input._numChunks; ++j)
                sumWeights += addChunk(
                    input._weights[j] * weights[n],
                    input._numBRDFs[j]);
    }

    // normalize all chunks' weights
    if (math::abs(sumWeights - 1.0f) > 1e-5f)
    {
        const float rSumWeights = sumWeights > 1e-6f
            ? math::rcp(sumWeights)
            : 0.0f;
        for (size_t n = 0; n < _numChunks; ++n)
            _weights[n] *= rSumWeights;
    }
}
