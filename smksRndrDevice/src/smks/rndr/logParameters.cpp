// ========================================================================= //
// Copyright 2016-2018 SUPAMONKS_STUDIO                                      //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
//                                                                           //
// ========================================================================= //
#include <smks/sys/Log.hpp>
#include <smks/parm/Container.hpp>
#include <smks/xchg/IdSet.hpp>
#include <smks/xchg/BoundingBox.hpp>

#include "../../std/to_string.hpp"
#include "logParameters.hpp"

namespace smks { namespace rndr
{
    static
    std::ostream&
    logParameter(std::ostream&, const parm::Container&, unsigned int, const char* = "");
}
}

using namespace smks;

// static
std::ostream&
rndr::logParameter(std::ostream&            out,
                    const parm::Container&  container,
                    unsigned int            parm,
                    const char*             parmPrefix)
{
    typedef std::pair<const char*, const std::type_info*> ParmInfo;

    const ParmInfo& info = container.getInfo(parm);
    if (!info.first || !info.second)
        return out;

    out << "\n-";
    if (strlen(parmPrefix) > 0)
        out << "[" << parmPrefix << "] ";
    out << "'" << info.first <<  "' (" << std::to_string(*info.second) << ") ";

    if      (container.existsAs<bool>(parm))                out << container.get<bool>(parm);
    else if (container.existsAs<char>(parm))                out << static_cast<int>(container.get<char>(parm));
    else if (container.existsAs<unsigned int>(parm))        out << container.get<unsigned int>(parm);
    else if (container.existsAs<size_t>(parm))              out << container.get<size_t>(parm);
    else if (container.existsAs<int>(parm))                 out << container.get<int>(parm);
    else if (container.existsAs<math::Vector2i>(parm))      out << "(" << container.get<math::Vector2i>(parm).transpose() << ")";
    else if (container.existsAs<math::Vector3i>(parm))      out << "(" << container.get<math::Vector3i>(parm).transpose() << ")";
    else if (container.existsAs<math::Vector4i>(parm))      out << "(" << container.get<math::Vector4i>(parm).transpose() << ")";
    else if (container.existsAs<float>(parm))               out << container.get<float>(parm);
    else if (container.existsAs<math::Vector2f>(parm))      out << "(" << container.get<math::Vector2f>(parm).transpose() << ")";
    else if (container.existsAs<math::Vector4f>(parm))      out << "(" << container.get<math::Vector4f>(parm).transpose() << ")";
    else if (container.existsAs<math::Affine3f>(parm))      out << "\n" << container.get<math::Affine3f>(parm).matrix();
    else if (container.existsAs<std::string>(parm))         out << "'" << container.get<std::string>(parm) << "'";
    else if (container.existsAs<xchg::DataStream>(parm))    out << container.get<xchg::DataStream>(parm);
    else if (container.existsAs<xchg::BoundingBox>(parm))   out << container.get<xchg::BoundingBox>(parm);
    else if (container.existsAs<xchg::IdSet>(parm))         out << container.get<xchg::IdSet>(parm);
    else if (container.existsAs<xchg::Data::Ptr>(parm) && container.get<xchg::Data::Ptr>(parm))
                                                            out << *(container.get<xchg::Data::Ptr>(parm));
    return out;
}

std::ostream&
rndr::logParameters(std::ostream&           out,
                     const parm::Container& container,
                     const char*            parmPrefix)
{
    const size_t    numKeys = container.size();
    unsigned int*   keys    = new unsigned int[numKeys];

    container.getKeys(numKeys, keys);
    for (size_t i = 0; i < numKeys; ++i)
        logParameter(out, container, keys[i], parmPrefix);
    delete[] keys;

    if (container.size() > 0)
        out << "\n";

    return out;
}

std::ostream&
rndr::logParameters(std::ostream&           out,
                     const parm::Container& container,
                     const xchg::IdSet&     dirties)
{
    const size_t    numKeys = container.size();
    unsigned int*   keys    = new unsigned int[numKeys];

    container.getKeys(numKeys, keys);
    for (int b = 0; b < 2; ++b)
        for (size_t i = 0; i < numKeys; ++i)
        {
            const unsigned int  parm    = keys[i];
            const bool          isDirty = dirties.find(parm) != dirties.end();
            if (isDirty != (b==0))
                logParameter(out, container, parm, isDirty ? "DIRTY" : "");
        }
    delete[] keys;

    for (smks::xchg::IdSet::const_iterator id = dirties.begin(); id != dirties.end(); ++id)
        if (!container.hasKey(*id))
            out << "\n- [UNSET] " << *id;

    if (container.size() > 0 || !dirties.empty())
        out << "\n";

    return out;
}

void
rndr::logParameters(unsigned int            scnId,
                     const parm::Container& container,
                     const char*            containerName,
                     const char*            parmPrefix)
{
#if defined(FLIRT_LOG_ENABLED)

    std::stringstream sstr;

    sstr << "\n"
        << container.size() << " parameter(s) "
        << "in container '" << containerName << "' "
        << "of rt object ID = " << scnId;
    logParameters(sstr, container, parmPrefix);

    LOG(DEBUG) << sstr.str();
#endif // defined(FLIRT_LOG_ENABLED)
}

void
rndr::logParameters(unsigned int            scnId,
                     const parm::Container& container,
                     const char*            containerName,
                     const xchg::IdSet&     dirties)
{
#if defined(FLIRT_LOG_ENABLED)
    std::stringstream sstr;

    sstr << "\n"
        << container.size() << " parameter(s) "
        << "in container '" << containerName << "' "
        << "of rt object ID = " << scnId;
    if (!dirties.empty())
        sstr << ", " << dirties.size() << " of which dirty";
    logParameters(sstr, container, dirties);

    LOG(DEBUG) << sstr.str();
#endif //defined(FLIRT_LOG_ENABLED)
}

