// ========================================================================= //
// Modification copyright 2016-2018 SUPAMONKS_STUDIO                         //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
// _________________________________________________________________________ //
// Copyright 2009-2013 Intel Corporation                                     //
//                                                                           //
// Licensed under the Apache License, Version 2.0 (the "License");           //
// you may not use this file except in compliance with the License.          //
// You may obtain a copy of the License at                                   //
//                                                                           //
//     http://www.apache.org/licenses/LICENSE-2.0                            //
//                                                                           //
// Unless required by applicable law or agreed to in writing, software       //
// distributed under the License is distributed on an "AS IS" BASIS,         //
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  //
// See the License for the specific language governing permissions and       //
// limitations under the License.                                            //
//                                                                           //
// ========================================================================= //
#include <smks/parm/BuiltIns.hpp>
#include <smks/math/math.hpp>
#include <smks/img/OutputImage.hpp>

#include "GammaCorrectionToneMapper.hpp"

using namespace smks;

rndr::GammaCorrectionToneMapper::GammaCorrectionToneMapper(unsigned int scnId, const CtorArgs& args):
    ToneMapper  (scnId, args),
    _gamma      (0.0f),
    _rGamma     (0.0f),
    _vignetting (false),
    //-----------
    _width      (0),
    _height     (0)
{
    dispose();
}

void
rndr::GammaCorrectionToneMapper::dispose()
{
    getBuiltIn<float>   (parm::gamma(),         _gamma);
    getBuiltIn<bool>    (parm::vignetting(),    _vignetting);

    _rGamma = math::rcp(_gamma);
}

void
rndr::GammaCorrectionToneMapper::commitFrameState(const parm::Container& parms, const xchg::IdSet& dirties)
{
    logParameters(scnId(), parms, "builtIns", dirties);
    logParameters(scnId(), userParameters(), "userParms", "USER");
    //---
    for (xchg::IdSet::const_iterator id = dirties.begin(); id != dirties.end(); ++id)
    {
        if (*id == parm::gamma().id())
        {
            getBuiltIn<float>(parm::gamma(), _gamma, &parms);
            _rGamma = math::rcp(_gamma);
        }
        else if (*id == parm::vignetting().id())
            getBuiltIn<bool>(parm::vignetting(), _vignetting, &parms);
    }
}

// virtual
void
rndr::GammaCorrectionToneMapper::getMetadata(img::OutputImage& frame) const
{
    frame.setMetadata("tonemapper/gamma",       _gamma);
    frame.setMetadata("tonemapper/vignetting",  _vignetting);
}

void
rndr::GammaCorrectionToneMapper::initialize(const FrameBuffer& framebuffer)
{
    _width  = framebuffer.width();
    _height = framebuffer.height();
}

void
rndr::GammaCorrectionToneMapper::apply(const math::Vector4f*    iRGBA,
                                         math::Vector4f*        oRGBA) const
{
    FLIRT_LOG(LOG(DEBUG) << "apply gamma-correction tone mapping...";)
    assert(_width > 0);
    assert(_height > 0);
    assert(iRGBA);
    assert(oRGBA);

    const bool  doGamma = math::abs(_gamma - 1.0f) > 1e-6f;
    const float hWidth  = 0.5f * static_cast<float>(_width);
    const float hHeight = 0.5f * static_cast<float>(_height);
    const float rHWidth = math::rcp(hWidth);

#pragma omp parallel for collapse(2)
    for (size_t y = 0; y < _height; y++)
        for (size_t x = 0; x < _width; x++)
        {
            const size_t    p       = x + _width * y;
            math::Vector4f  rgba    = iRGBA[p];
            const float     alpha   = rgba.w();

            if (doGamma)
                rgba = math::pow(rgba, _rGamma);
            if (_vignetting)
            {
                const float d = math::sqrt(math::sqr(x-hWidth) + math::sqr(y-hHeight)) * rHWidth;
                const float f = math::cos(0.5f * d);
                rgba *= (f*f*f);    // pow(f,3)
            }

            oRGBA[p]        = rgba;
            oRGBA[p].w()    = alpha;
        }
}
