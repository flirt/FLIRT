// ========================================================================= //
// Copyright 2016-2018 SUPAMONKS_STUDIO                                      //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
//                                                                           //
// ========================================================================= //
#pragma once

#include "ToneMapper.hpp"
#include "../api/PixelData.hpp"
#include "../api/FrameBuffer.hpp"

namespace smks
{
    namespace img
    {
        class OpenColorIOMapper;
    };
    namespace rndr
    {
        /*! OpenColorIO-based lookup table tonemapper. */
        class OpenColorIOMapper:
            public ToneMapper
        {
            VALID_RTOBJECT
        public:
            typedef sys::Ref<OpenColorIOMapper> Ptr;
        private:
            typedef std::shared_ptr<img::OpenColorIOMapper> MapperPtr;
        private:
            const ClientWPtr    _client;
        protected:
            MapperPtr           _mapper;
        private:
            size_t              _width;
            size_t              _height;

            CREATE_RTOBJECT(OpenColorIOMapper, ToneMapper)
        protected:
            OpenColorIOMapper(unsigned int scnId, const CtorArgs&);

        public:
            void
            getMetadata(img::OutputImage&) const;

            void
            initialize(const FrameBuffer&);

            void
            apply(const math::Vector4f*, math::Vector4f*) const;

            void
            dispose();

            void
            commitFrameState(const parm::Container&, const xchg::IdSet&);

            inline
            void
            beginSubframeCommits(size_t)
            { }

            inline
            void
            commitSubframeState(size_t, const parm::Container&)
            { }

            inline
            void
            endSubframeCommits()
            { }

            inline
            bool
            perSubframeParameter(unsigned int) const
            {
                return false;
            }

        protected:
            void
            setConfig(const std::string&);
        };
    }
}
