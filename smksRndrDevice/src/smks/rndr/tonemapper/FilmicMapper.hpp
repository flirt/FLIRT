// ========================================================================= //
// Copyright 2016-2018 SUPAMONKS_STUDIO                                      //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
//                                                                           //
// ========================================================================= //
#pragma once

#include <smks/img/OpenColorIOMapper.hpp>
#include "OpenColorIOMapper.hpp"

namespace smks { namespace rndr
{
    class FilmicMapper:
        public OpenColorIOMapper
    {
        VALID_RTOBJECT
    public:
        typedef sys::Ref<FilmicMapper> Ptr;

        CREATE_RTOBJECT(FilmicMapper, ToneMapper)
    protected:
        FilmicMapper(unsigned int scnId, const CtorArgs& args):
            OpenColorIOMapper(scnId, args)
        {
            setConfig("ocio/filmic-blender-master/config_smks.ocio");
            _mapper->inputColorspace("Linear");
            _mapper->displayView("sRGB / BT.709", getDisplayView(0));
        }

    public:
        inline
        void
        commitFrameState(const parm::Container& parms, const xchg::IdSet& dirties)
        {
            logParameters(scnId(), parms, "builtIns", dirties);
            logParameters(scnId(), userParameters(), "userParms", "USER");
            //---
            char    contrastLevel   = -1;
            float   exposureFstops  = 0.0f;
            for (xchg::IdSet::const_iterator id = dirties.begin(); id != dirties.end(); ++id)
            {
                if (*id == parm::contrastLevel().id())
                    getBuiltIn<char>    (parm::contrastLevel(),     contrastLevel,      &parms);
                else if (*id == parm::exposureFstops().id())
                    getBuiltIn<float>   (parm::exposureFstops(),    exposureFstops,     &parms);
            }
            if (contrastLevel != -1)
                _mapper->displayView("sRGB / BT.709", getDisplayView(contrastLevel));
            _mapper->exposureFstops(exposureFstops);

            FLIRT_LOG(LOG(DEBUG)
                << "\n--------------------"
                << "\nFilmic mapper (ID = " << scnId() << ")\n"
                << "\n\t- config = '" << _mapper->configPath() << "'"
                << "\n\t- input color space = '" << _mapper->inputColorspace() << "'"
                << "\n\t- display: '" << _mapper->display() << "' '" << _mapper->displayView() << "'"
                << "\n\t- exposure f-stops = " << _mapper->exposureFstops()
                << "\n--------------------";)
        }

        inline
        void
        beginSubframeCommits(size_t)
        { }

        inline
        void
        commitSubframeState(size_t, const parm::Container&)
        { }

        inline
        void
        endSubframeCommits()
        { }

        inline
        bool
        perSubframeParameter(unsigned int) const
        {
            return false;
        }

    private:
        static inline
        const char*
        getDisplayView(char contrastLevel)
        {
            switch(contrastLevel)
            {
            default:    return "Filmic Base Contrast";
            case 1:     return "Filmic Very Low Contrast";
            case 2:     return "Filmic Low Contrast";
            case 3:     return "Filmic Medium Low Contrast";
            case 4:     return "Filmic Medium High Contrast";
            case 5:     return "Filmic High Contrast";
            case 6:     return "Filmic Very High Contrast";
            }
        }
    };
}
}
