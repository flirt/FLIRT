// ========================================================================= //
// Copyright 2016-2018 SUPAMONKS_STUDIO                                      //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
//                                                                           //
// ========================================================================= //
#include <smks/sys/Log.hpp>
#include <smks/sys/ResourceDatabase.hpp>
#include <smks/sys/TextResource.hpp>
#include <smks/img/OutputImage.hpp>
#include <smks/img/OpenColorIOMapper.hpp>
#include <smks/ClientBase.hpp>
#include "OpenColorIOMapper.hpp"

using namespace smks;

rndr::OpenColorIOMapper::OpenColorIOMapper(unsigned int scnId, const CtorArgs& args):
    ToneMapper  (scnId, args),
    _client     (args.client),
    //----------
    _mapper     (img::OpenColorIOMapper::create()),
    //----------
    _width      (0),
    _height     (0)
{
    dispose();
}

void
rndr::OpenColorIOMapper::dispose()
{
    _mapper->configPath("");    // will remove all settings
}

void
rndr::OpenColorIOMapper::commitFrameState(const parm::Container& parms, const xchg::IdSet& dirties)
{
    logParameters(scnId(), parms, "builtIns", dirties);
    logParameters(scnId(), userParameters(), "userParms", "USER");
    //---
    std::string config;
    std::string inputColorspace;
    std::string display;
    std::string displayView;
    float       exposureFstops = 0.0f;
    for (xchg::IdSet::const_iterator id = dirties.begin(); id != dirties.end(); ++id)
    {
        if (*id == parm::config().id())                 getBuiltIn<std::string> (parm::config(),            config,             &parms);
        else if (*id == parm::inputColorspace().id())   getBuiltIn<std::string> (parm::inputColorspace(),   inputColorspace,    &parms);
        else if (*id == parm::displayName().id())       getBuiltIn<std::string> (parm::displayName(),       display,            &parms);
        else if (*id == parm::displayViewName().id())   getBuiltIn<std::string> (parm::displayViewName(),   displayView,        &parms);
        else if (*id == parm::exposureFstops().id())    getBuiltIn<float>       (parm::exposureFstops(),    exposureFstops,     &parms);
    }

    if (!config.empty())
        setConfig(config);
    if (!inputColorspace.empty())
        _mapper->inputColorspace(inputColorspace.c_str());
    if (!display.empty() || !displayView.empty())
        _mapper->displayView(display.c_str(), displayView.c_str());
    _mapper->exposureFstops(exposureFstops);

    FLIRT_LOG(LOG(DEBUG)
        << "\n--------------------"
        << "\nOpenColorIO mapper (ID = " << scnId() << ")\n"
        << "\n\t- config = '" << _mapper->configPath() << "'"
        << "\n\t- input color space = '" << _mapper->inputColorspace() << "'"
        << "\n\t- display: '" << _mapper->display() << "' '" << _mapper->displayView() << "'"
        << "\n\t- exposure f-stops = " << _mapper->exposureFstops()
        << "\n--------------------";)
}

void
rndr::OpenColorIOMapper::setConfig(const std::string& path)
{
    ClientBase::Ptr const&              client      = _client.lock();
    sys::ResourceDatabase::Ptr const&   resources   = client ? client->getOrCreateResourceDatabase() : nullptr;

    const size_t    BUFFER_SIZE = 512;
    char            BUFFER      [BUFFER_SIZE];

    if (resources &&
        resources->getOrCreatePathFinder()->resolveFileName(path.c_str(), BUFFER, BUFFER_SIZE))
        _mapper->configPath(BUFFER);
}

// virtual
void
rndr::OpenColorIOMapper::getMetadata(img::OutputImage& frame) const
{
    std::stringstream sstr;
    sstr << "ocio_mapper_" << scnId();

    frame.setMetadata((sstr.str() + "/config").c_str(),         _mapper->configPath());
    frame.setMetadata((sstr.str() + "/source").c_str(),         _mapper->inputColorspace());
    frame.setMetadata((sstr.str() + "/display").c_str(),        _mapper->display());
    frame.setMetadata((sstr.str() + "/displayView").c_str(),    _mapper->displayView());
}

void
rndr::OpenColorIOMapper::initialize(const FrameBuffer& framebuffer)
{
    _width  = framebuffer.width();
    _height = framebuffer.height();
}

void
rndr::OpenColorIOMapper::apply(const math::Vector4f*    iRGBA,
                                math::Vector4f*         oRGBA) const
{
    assert(_width > 0);
    assert(_height > 0);
    assert(iRGBA);
    assert(oRGBA);

    const float*    iPtr = &iRGBA[0].x();
    float*          oPtr = &oRGBA[0].x();
    _mapper->applyRGBA(_width, _height, iPtr, oPtr);
}
