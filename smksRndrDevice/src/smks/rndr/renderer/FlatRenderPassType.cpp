// ========================================================================= //
// Copyright 2016-2018 SUPAMONKS_STUDIO                                      //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
//                                                                           //
// ========================================================================= //
#include <smks/sys/Exception.hpp>
#include "FlatRenderPassType.hpp"

using namespace smks;

size_t
rndr::requiredDepth(xchg::FlatRenderPassType typ)
{
    switch (typ)
    {
    case xchg::FlatRenderPassType::FACING_RATIO_PASS:
    case xchg::FlatRenderPassType::SHADOW_BIAS_PASS:
        return 1;

    case xchg::FlatRenderPassType::TEXCOORDS_PASS:
#if defined(_DEBUG)
    case xchg::FlatRenderPassType::DEBUG_RAY_TEXCOORDS_PASS:
#endif // defined(_DEBUG)
        return 2;

    case xchg::FlatRenderPassType::AMBIENT_OCCLUSION_PASS:
    case xchg::FlatRenderPassType::POSITION_PASS:
    case xchg::FlatRenderPassType::EYE_POSITION_PASS:
    case xchg::FlatRenderPassType::NORMAL_PASS:
    case xchg::FlatRenderPassType::EYE_NORMAL_PASS:
    case xchg::FlatRenderPassType::TANGENT_PASS:
    case xchg::FlatRenderPassType::EYE_TANGENT_PASS:
    case xchg::FlatRenderPassType::SUBSURFACE_PASS:
    case xchg::FlatRenderPassType::SHADOWS_PASS:
    case xchg::FlatRenderPassType::DIFFUSE_COLOR_PASS:
    case xchg::FlatRenderPassType::GLOSSY_COLOR_PASS:
    case xchg::FlatRenderPassType::SINGULAR_COLOR_PASS:
#if defined(_DEBUG)
    case xchg::FlatRenderPassType::DEBUG_RAY_GEOM_NORMAL_PASS:
    case xchg::FlatRenderPassType::DEBUG_HIT_GEOM_NORMAL_PASS:
    case xchg::FlatRenderPassType::DEBUG_HIT_SHAD_NORMAL_PASS:
#endif // defined(_DEBUG)
        return 4;

    default:
        throw sys::Exception("Invalid type.", "Flat Render Pass Depth Request");
        break;
    }
}

rndr::AccumulationOp
rndr::accumulationOp(xchg::FlatRenderPassType typ)
{
    switch (typ)
    {
    case xchg::FlatRenderPassType::SHADOW_BIAS_PASS:
        return ACCUMULATION_MIN;

    case xchg::FlatRenderPassType::FACING_RATIO_PASS:
    case xchg::FlatRenderPassType::AMBIENT_OCCLUSION_PASS:
    case xchg::FlatRenderPassType::TEXCOORDS_PASS:
    case xchg::FlatRenderPassType::POSITION_PASS:
    case xchg::FlatRenderPassType::EYE_POSITION_PASS:
    case xchg::FlatRenderPassType::NORMAL_PASS:
    case xchg::FlatRenderPassType::EYE_NORMAL_PASS:
    case xchg::FlatRenderPassType::TANGENT_PASS:
    case xchg::FlatRenderPassType::EYE_TANGENT_PASS:
    case xchg::FlatRenderPassType::SUBSURFACE_PASS:
    case xchg::FlatRenderPassType::SHADOWS_PASS:
    case xchg::FlatRenderPassType::DIFFUSE_COLOR_PASS:
    case xchg::FlatRenderPassType::GLOSSY_COLOR_PASS:
    case xchg::FlatRenderPassType::SINGULAR_COLOR_PASS:
#if defined(_DEBUG)
    case xchg::FlatRenderPassType::DEBUG_RAY_TEXCOORDS_PASS:
    case xchg::FlatRenderPassType::DEBUG_RAY_GEOM_NORMAL_PASS:
    case xchg::FlatRenderPassType::DEBUG_HIT_GEOM_NORMAL_PASS:
    case xchg::FlatRenderPassType::DEBUG_HIT_SHAD_NORMAL_PASS:
#endif // defined(_DEBUG)
        return ACCUMULATION_AVG;

    default:
        throw sys::Exception("Invalid type.", "Flat Render Pass Accumulation Mode Request");
        break;
    }
}
