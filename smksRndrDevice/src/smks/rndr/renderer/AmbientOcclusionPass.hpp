// ========================================================================= //
// Copyright 2016-2018 SUPAMONKS_STUDIO                                      //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
//                                                                           //
// ========================================================================= //
#pragma once

#include <smks/parm/BuiltIns.hpp>
#include <smks/img/OutputImage.hpp>

#include "FlatRenderPass.hpp"

namespace smks { namespace rndr
{
    class AmbientOcclusionPass:
        public FlatRenderPass<xchg::AMBIENT_OCCLUSION_PASS>
    {
    public:
        typedef sys::Ref<AmbientOcclusionPass> Ptr;
    private:
        size_t      _numSamples;
        float       _maxDistance;

        std::string _userColorName; // name of the user parameter used to determine the color of scene primitive
        size_t      _userColorIdx;  // index of the user parameter getter

        CREATE_RTOBJECT(AmbientOcclusionPass, RenderPass)
    protected:
        AmbientOcclusionPass(unsigned int scnId, const CtorArgs&);
    public:
        ~AmbientOcclusionPass();

        void
        dispose();

        void
        commitFrameState(const parm::Container&, const xchg::IdSet&);

        void
        requestUserParametersFromPrimitives(parm::Getters&);

        inline
        size_t
        numSamples() const
        {
            return _numSamples;
        }

        inline
        float
        maxDistance() const
        {
            return _maxDistance;
        }

        inline
        size_t
        userColorIdx() const
        {
            return _userColorIdx;
        }

        inline
        void
        getMetadata(img::OutputImage& frame) const
        {
            frame.setMetadata("numAmbientOcclusionSamples",     _numSamples);
            frame.setMetadata("maxAmbientOcclusionDistance",    _maxDistance);
        }

        inline
        AmbientOcclusionPass*
        asAmbientOcclusionPass()
        {
            return this;
        }

        inline
        const AmbientOcclusionPass*
        asAmbientOcclusionPass() const
        {
            return this;
        }
    };
}
}
