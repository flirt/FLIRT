// ========================================================================= //
// Modification copyright 2016-2018 SUPAMONKS_STUDIO                         //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
// _________________________________________________________________________ //
// Copyright 2009-2013 Intel Corporation                                     //
//                                                                           //
// Licensed under the Apache License, Version 2.0 (the "License");           //
// you may not use this file except in compliance with the License.          //
// You may obtain a copy of the License at                                   //
//                                                                           //
//     http://www.apache.org/licenses/LICENSE-2.0                            //
//                                                                           //
// Unless required by applicable law or agreed to in writing, software       //
// distributed under the License is distributed on an "AS IS" BASIS,         //
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  //
// See the License for the specific language governing permissions and       //
// limitations under the License.                                            //
//                                                                           //
// ========================================================================= //
#pragma once

#include <smks/sys/sync/Mutex.hpp>
#include <smks/xchg/shutter.hpp>

#include "Renderer.hpp"

namespace smks
{
    namespace rndr
    {
        class DefaultRenderer:
            public Renderer
        {
            VALID_RTOBJECT
        public:
            typedef sys::Ref<DefaultRenderer> Ptr;

        private:
            class RenderJob;
            friend class RenderJob;

        private:
            const ClientWPtr            _client;

            sys::Ref<Integrator>        _integrator;
            sys::Ref<SamplerFactory>    _samplerFactory;
            sys::Ref<PixelFilter>       _pixelFilter;
            size_t                      _numSubframes;
            int                         _iteration;

            bool                        _accumulate;
            int                         _xMin, _xMax, _yMin, _yMax;
            xchg::ShutterType           _shutterType;
            float                       _shutter;

            bool                        _showProgress;
            sys::sync::MutexSys         _mutex;
            size_t                      _stepCount;

            CREATE_RTOBJECT(DefaultRenderer, Renderer)
        protected:
            DefaultRenderer(unsigned int scnId, const CtorArgs&);

        public:
            __forceinline
            void
            integrator(sys::Ref<Integrator> const& value)
            {
                _integrator = value;
            }

            __forceinline
            sys::Ref<Integrator>&
            integrator()
            {
                return _integrator;
            }

            __forceinline
            sys::Ref<Integrator> const&
            integrator() const
            {
                return _integrator;
            }

            __forceinline
            void
            samplerFactory(sys::Ref<SamplerFactory> const& value)
            {
                _samplerFactory = value;
            }

            __forceinline
            sys::Ref<SamplerFactory>&
            samplerFactory()
            {
                return _samplerFactory;
            }

            __forceinline
            sys::Ref<SamplerFactory> const&
            samplerFactory() const
            {
                return _samplerFactory;
            }

            __forceinline
            void
            filter(sys::Ref<PixelFilter> const& value)
            {
                _pixelFilter = value;
            }

            __forceinline
            sys::Ref<PixelFilter> const&
            filter() const
            {
                return _pixelFilter;
            }

            __forceinline
            bool
            showProgress() const
            {
                return _showProgress;
            }

            //__forceinline
            //xchg::ShutterType
            //shutterType() const
            //{
            //  return _shutterType;
            //}

            //__forceinline
            //float
            //shutterBias() const
            //{
            //  return _shutter;
            //}

            /*! Allows the request optional user parameters prior scene primitive extraction. */
            void
            requestUserParametersFromPrimitives(parm::Getters&);

            void
            renderFrame(sys::Ref<Camera> const&,
                        sys::Ref<Scene> const&,
                        sys::Ref<FrameBuffer>&,
                        size_t numSubframes);

            void
            dispose();

            void
            commitFrameState(const parm::Container&, const xchg::IdSet&);

            inline
            void
            beginSubframeCommits(size_t)
            { }

            inline
            void
            commitSubframeState(size_t, const parm::Container&)
            { }

            inline
            void
            endSubframeCommits()
            { }

            inline
            bool
            perSubframeParameter(unsigned int) const
            {
                return false;
            }

            void
            getMetadata(img::OutputImage&) const;

        protected:
            bool
            getRenderWindow(int& xMin, int& yMin, int& xMax, int& yMax) const
            {
                xMin = _xMin;
                yMin = _yMin;
                xMax = _xMax;
                yMax = _yMax;

                return xMin != -1 &&
                    yMin != -1 &&
                    xMax != -1 &&
                    yMax != -1 &&
                    xMin <= xMax &&
                    yMin <= yMax;
            }

            __forceinline bool accumulate() const { return _accumulate; }

            __forceinline
            void
            postIncrementStepCount(size_t& stepCount)   // should be called by RenderJobs only
            {
                sys::sync::Lock<sys::sync::MutexSys> lock(_mutex);
                //---
                stepCount = _stepCount;
                ++_stepCount;
            }
        };
    }
}
