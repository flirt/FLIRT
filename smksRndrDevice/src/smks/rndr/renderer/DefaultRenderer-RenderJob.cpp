// ========================================================================= //
// Modification copyright 2016-2018 SUPAMONKS_STUDIO                         //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
// _________________________________________________________________________ //
// Copyright 2009-2013 Intel Corporation                                     //
//                                                                           //
// Licensed under the Apache License, Version 2.0 (the "License");           //
// you may not use this file except in compliance with the License.          //
// You may obtain a copy of the License at                                   //
//                                                                           //
//     http://www.apache.org/licenses/LICENSE-2.0                            //
//                                                                           //
// Unless required by applicable law or agreed to in writing, software       //
// distributed under the License is distributed on an "AS IS" BASIS,         //
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  //
// See the License for the specific language governing permissions and       //
// limitations under the License.                                            //
//                                                                           //
// ========================================================================= //
#include <smks/math/math.hpp>
#include <smks/sys/platform.hpp>
#include <smks/ClientBase.hpp>

#include "DefaultRenderer-RenderJob.hpp"
#include "../integrator/Integrator.hpp"
#include "../integrator/IntegratorState.hpp"
#include "../sampler/SamplerFactory.hpp"
#include "../sampler/PrecomputedSample.hpp"
#include "../camera/Camera.hpp"
#include "../api/Scene.hpp"
#include "../api/Ray.hpp"
#include "../api/FrameBuffer.hpp"
#include "../api/PixelLayout.hpp"
#include "../api/PixelData.hpp"
#include "../filter/PixelFilter.hpp"
#include "../../parm/Getters.hpp"

using namespace smks;

rndr::DefaultRenderer::RenderJob::RenderJob(ClientBase::WPtr const& cl,
                                            DefaultRenderer::Ptr    renderer,
                                            Camera::Ptr const&      camera,
                                            Scene::Ptr const&       scene,
                                            FrameBuffer::Ptr        frameBuffer,
                                            int                     iteration):
    _client     (cl),
    _renderer   (renderer),
    _camera     (camera),
    _scene      (scene),
    _frameBuffer(frameBuffer),
    _iteration  (iteration),
    _rWidth     (math::rcp(static_cast<float>(frameBuffer->width()))),
    _rHeight    (math::rcp(static_cast<float>(frameBuffer->height()))),
    _numTilesX  ((frameBuffer->width()  + TILE_SIZE - 1) / TILE_SIZE),
    _numTilesY  ((frameBuffer->height() + TILE_SIZE - 1) / TILE_SIZE),
    _time0      (0.0),
    _tileId     (0),
    _numRays    (0),
    _task       ()
{
    const size_t numTiles = _numTilesX * _numTilesY;

    frameBuffer->startRendering(numTiles);

    if (renderer->showProgress())
    {
        ClientBase::Ptr const& client = _client.lock();
        if (client)
            client->notifyProgressBeginning("render", numTiles);
    }

    assert(_renderer->integrator());
    assert(_renderer->samplerFactory());
    assert(_renderer->filter());

    _frameBuffer                ->configure(*_renderer.cast<Renderer>());
    _frameBuffer                ->initialize(*_renderer.cast<Renderer>());
    _renderer->integrator()     ->initialize(_scene, _frameBuffer);

    _renderer->samplerFactory() ->deleteSamples();
    _renderer->integrator()     ->requestSamples(_renderer->samplerFactory(), _scene);
    _renderer->samplerFactory() ->initializeSamples(*_scene, _iteration, _renderer->filter());

    //this->framebuffer = swapchain->buffer();
    //this->framebuffer->startRendering(numTilesX*numTilesY);
    //if (renderer->showProgress) new (&progress) smks::sys::Progress(numTilesX*numTilesY);

    //if (renderer->showProgress) progress.start();
    //renderer->samplers->reset();
    //renderer->integrator->requestSamples(renderer->samplers, scene);
    //renderer->samplers->init(iteration,renderer->filter);
    //--------------------------------

#if 1
    sys::TaskScheduler::EventSync   event;
    sys::TaskScheduler::Task        task(&event, _renderTile, this, sys::TaskScheduler::getNumThreads(), _finish, this, "render::tile");

    sys::TaskScheduler::addTask(-1, sys::TaskScheduler::GLOBAL_BACK, &task);
    event.sync();
#else
    new (&task) TaskScheduler::Task (NULL, _renderTile, this, sys::TaskScheduler::getNumThreads(), _finish, this, "render::tile");
    TaskScheduler::addTask(-1, TaskScheduler::GLOBAL_BACK,&task);
#endif
}

void
rndr::DefaultRenderer::RenderJob::finish(size_t                     threadIndex,
                                          size_t                        threadCount,
                                          sys::TaskScheduler::Event*    event)
{
    if (_renderer->showProgress())
    {
        ClientBase::Ptr const& client = _client.lock();
        if (client)
            client->notifyProgressEnd("render");
    }

    const double dt = sys::getSeconds() - _time0;

    /*! print fps, render time, and rays per second */
    std::ostringstream stream;

    stream << "render  ";
    stream.setf(std::ios::fixed, std::ios::floatfield);
    stream.precision(2);
    stream << math::rcp(dt) << " fps, ";
    stream.precision(0);
    stream << dt * 1000.0f << " ms, ";
    stream.precision(3);
    stream << _numRays / dt * 1e-6 << " mrps";

    std::cout << stream.str() << std::endl;

    //--------------------------------
    // compute final RGBA values by applying denoiser and tonemapper
    _frameBuffer->finishFrame();
    //--------------------------------

    delete this;
}

void
rndr::DefaultRenderer::RenderJob::renderTile(size_t                     threadIndex,
                                              size_t                        threadCount,
                                              size_t                        taskIndex,
                                              size_t                        taskCount,
                                              sys::TaskScheduler::Event*    event)
{
    IntegratorComputeContext*   context = _renderer->integrator()->createComputeContext(*_frameBuffer);
    IntegratorState             state;
    ClientBase::Ptr const&      client  = _client.lock();

    if (taskIndex == taskCount-1)
        _time0 = sys::getSeconds();

    int xMin = -1;
    int xMax = -1;
    int yMin = -1;
    int yMax = -1;
    const bool renderWindow = _renderer->getRenderWindow(
        xMin, yMin, xMax, yMax);

    const size_t    numTiles = _numTilesX * _numTilesY;
    PixelData       pData/*, pDataAccum, pDataNorm*/;

    pData = _frameBuffer->initializePixelData(pData);
    //pDataAccum    = _frameBuffer->initializePixelData(pDataAccum);
    //pDataNorm     = _frameBuffer->initializePixelData(pDataNorm);

    while (true)
    {
        /*! pick a new tile */
        size_t tile = _tileId++;
        if (tile >= numTiles)
            break;

        /*! reseed random number generator */
        const int tileX = static_cast<int>((tile % _numTilesX) * TILE_SIZE);
        const int tileY = static_cast<int>((tile / _numTilesX) * TILE_SIZE);

        state.rng = sys::Random(tileX * 91711 + tileY * 81551);

        for (size_t dy = 0; dy < TILE_SIZE; ++dy)
        {
            size_t y = tileY + dy;
            if (y >= _frameBuffer->height())
                continue;

            for (size_t dx = 0; dx < TILE_SIZE; ++dx)
            {
                size_t x = tileX + dx;
                if (x >= _frameBuffer->width())
                    continue;

                const int set = state.rng.getInt(_renderer->samplerFactory()->numSampleSets());

                //pDataAccum.clear();
                if (!renderWindow ||
                    (xMin <= x && x <= xMax &&
                     yMin <= y && y <= yMax))
                {
                    if (_renderer->accumulate() == false)
                        _frameBuffer->clearPixel(x, y);

                    for (size_t s = 0; s < _renderer->samplerFactory()->numSamplesPerPixel(); ++s)
                    {
                        const PrecomputedSample& samp = _renderer->samplerFactory()->sample(set, s);
                        const math::Vector2f pixel (
                            ( static_cast<float>(x) + samp.pixel.x() )  *_rWidth,
                            ( static_cast<float>(y) + samp.pixel.y() )  *_rHeight );

                        // update integrator state
                        state.sample        = &samp;
                        state.raster        = math::Vector2i(x, y);
                        state.pixel         = pixel;
                        state.pixelSampleId = s;
                        _camera->getWorldToEyeSpaceMatrix(samp.time, state.worldToEye);

                        // generate primary ray from the camera
                        Ray primary;

                        _camera->ray(samp.time, pixel, samp.lens, primary);
                        primary.time = samp.time;

                        // approximate rendering equation using integrator
                        pData = _renderer->integrator()->eval(primary, _scene, pData, state, context);
                        //pDataAccum += pData;
                        _frameBuffer->addPixelSample(x, y, s, pData);   // pData is consumed at this point
                    }
                }
                // accumulate and average pixel data
                _frameBuffer->normalizePixel(x, y);
                //pDataNorm = _frameBuffer->update(x, y,
                //  static_cast<float>(_renderer->samplerFactory()->numSamplesPerPixel()), // weight
                //  pDataAccum,
                //  _renderer->accumulate(),
                //  pDataNorm);
                //
                //pDataNorm = _toneMapper->eval(pDataNorm, x, y, *_frameBuffer);

                //// save normalized, tonemapped pixel data to framebuffer
                //_frameBuffer->set(x, y, pDataNorm);
            }
        }

        /*! print progress bar */
        bool proceed = true;

        if (_renderer->showProgress())
        {
            size_t stepCount;

            _renderer->postIncrementStepCount(stepCount);
            proceed = client->notifyProgressProceeding(stepCount);
        }
        _frameBuffer->finishTile(); //<! mark one more tile as finished

        if (!proceed)
            break;
    }
    _numRays += state.numRays; //<! we access the atomic ray counter only once per tile

    delete context;
}
