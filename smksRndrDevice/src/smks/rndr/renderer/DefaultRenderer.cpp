// ========================================================================= //
// Modification copyright 2016-2018 SUPAMONKS_STUDIO                         //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
// _________________________________________________________________________ //
// Copyright 2009-2013 Intel Corporation                                     //
//                                                                           //
// Licensed under the Apache License, Version 2.0 (the "License");           //
// you may not use this file except in compliance with the License.          //
// You may obtain a copy of the License at                                   //
//                                                                           //
//     http://www.apache.org/licenses/LICENSE-2.0                            //
//                                                                           //
// Unless required by applicable law or agreed to in writing, software       //
// distributed under the License is distributed on an "AS IS" BASIS,         //
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  //
// See the License for the specific language governing permissions and       //
// limitations under the License.                                            //
//                                                                           //
// ========================================================================= //
#include <smks/sys/Log.hpp>
#include <smks/parm/BuiltIns.hpp>
#include <smks/img/OutputImage.hpp>

#include "DefaultRenderer.hpp"
#include "../integrator/Integrator.hpp"
#include "../sampler/SamplerFactory.hpp"
#include "../filter/PixelFilter.hpp"
#include "../api/FrameBuffer.hpp"
#include "../tonemapper/ToneMapper.hpp"
#include "../api/Scene.hpp"
#include "DefaultRenderer-RenderJob.hpp"

using namespace smks;

rndr::DefaultRenderer::DefaultRenderer(unsigned int scnId, const CtorArgs& args):
    Renderer        (scnId, args),
    _client         (args.client),
    _integrator     (sys::null),
    _samplerFactory (sys::null),
    _pixelFilter    (sys::null),
    _iteration      (0),
    _accumulate     (false),
    _xMin           (-1),
    _xMax           (-1),
    _yMin           (-1),
    _yMax           (-1),
    _shutterType    (xchg::NO_SHUTTER),
    _shutter        (1.0f),
    _showProgress   (false),
    _stepCount      (0)
{
    dispose();
}

void
rndr::DefaultRenderer::dispose()
{
    getBuiltIn<bool>(parm::accumulate(),    _accumulate);
    getBuiltIn<bool>(parm::showProgress(),  _showProgress);
    getBuiltIn<int> (parm::xMin(),          _xMin);
    getBuiltIn<int> (parm::xMax(),          _xMax);
    getBuiltIn<int> (parm::yMin(),          _yMin);
    getBuiltIn<int> (parm::yMax(),          _yMax);

    std::string parmStr;
    getBuiltIn<std::string> (parm::shutterType(),   parmStr);
    getBuiltIn<float>       (parm::shutter(),       _shutter);
    _shutterType = xchg::getShutterType(parmStr.c_str());
}

void
rndr::DefaultRenderer::commitFrameState(const parm::Container& parms, const xchg::IdSet& dirties)
{
    logParameters(scnId(), parms, "builtIns", dirties);
    logParameters(scnId(), userParameters(), "userParms", "USER");
    //---
    std::string parmStr;
    for (xchg::IdSet::const_iterator id = dirties.begin(); id != dirties.end(); ++id)
        if      (*id == parm::showProgress  ().id())    getBuiltIn<bool>    (parm::showProgress(),  _showProgress,  &parms);
        else if (*id == parm::accumulate    ().id())    getBuiltIn<bool>    (parm::accumulate(),    _accumulate,    &parms);
        else if (*id == parm::xMin          ().id())    getBuiltIn<int>     (parm::xMin(),          _xMin,          &parms);
        else if (*id == parm::xMax          ().id())    getBuiltIn<int>     (parm::xMax(),          _xMax,          &parms);
        else if (*id == parm::yMin          ().id())    getBuiltIn<int>     (parm::yMin(),          _yMin,          &parms);
        else if (*id == parm::yMax          ().id())    getBuiltIn<int>     (parm::yMax(),          _yMax,          &parms);
        else if (*id == parm::shutter       ().id())    getBuiltIn<float>   (parm::shutter(),       _shutter,   &parms);
        else if (*id == parm::shutterType   ().id())
        {
            getBuiltIn<std::string>(parm::shutterType(), parmStr, &parms);
            _shutterType = xchg::getShutterType(parmStr.c_str());
        }

    FLIRT_LOG(LOG(DEBUG)
        << "\n--------------------\n"
        << "default renderer (ID = " << scnId() << ")\n"
        << "\n\t- accumulate ? " << _accumulate
        << "\n\t- show progress ? " << _showProgress
        << "\n\t- x in [" << _xMin << ", " << _xMax << "]"
        << "\n\t- y in [" << _yMin << ", " << _yMax << "]"
        << "\n\t- shutter: " << std::to_string(_shutterType) << " bias= " << _shutter
        << "\n--------------------";)
}

void
rndr::DefaultRenderer::requestUserParametersFromPrimitives(parm::Getters& getters)
{
    if (integrator())
        integrator()->requestUserParametersFromPrimitives(getters);
}

void
rndr::DefaultRenderer::renderFrame(sys::Ref<Camera> const&  camera,
                                    sys::Ref<Scene> const&  scene,
                                    sys::Ref<FrameBuffer>&  framebuffer,
                                    size_t                  numSubframes)
{
    if (camera              == sys::null ||
        scene               == sys::null ||
        scene->rtcScene()   == nullptr   ||
        framebuffer         == sys::null)
        return;

    FLIRT_LOG(LOG(DEBUG)
        << "\n------------\nrender frame\n"
        << "\n\t- # subframes = " << numSubframes
        << "\n\t- integrator = " << _integrator.ptr()
        << "\n\t- sampler factory = " << _samplerFactory.ptr()
        << "\n\t- pixel filter = " << _pixelFilter.ptr()
        << "\n\t- accumulate ? " << _accumulate
        << "\n\t- show progress ? " << _showProgress
        << "\n\t- x in [" << _xMin << ", " << _xMax << "]"
        << "\n\t- y in [" << _yMin << ", " << _yMax << "]"
        << "\n\t- shutter: type = " << std::to_string(_shutterType) << "\tvalue = " << _shutter
        << "\n------------";)

    if (scene->totalNumLights() == 0)
        std::cerr << "WARNING: no active light found in the scene." << std::endl;
    {
        int xMin, yMin, xMax, yMax;
        if (getRenderWindow(xMin, yMin, xMax, yMax))
            std::cout
                << "Rendering confined in window "
                << "(" << xMin << ", " << yMin << ")"
                << " -> "
                << "(" << xMax << ", " << yMax << ")"
                << std::endl;
    }


    if (!_accumulate)
    {
        framebuffer->clear();
        _iteration = 0;
    }
    _stepCount = 0;
    new RenderJob(_client, this, camera, scene, framebuffer, _iteration);
    ++_iteration;
}

// virtual
void
rndr::DefaultRenderer::getMetadata(img::OutputImage& frame) const
{
    if (_integrator)
        _integrator->getMetadata(frame);
    if (_samplerFactory)
        _samplerFactory->getMetadata(frame);
    if (_pixelFilter)
        _pixelFilter->getMetadata(frame);
    frame.setMetadata("renderer/shutterType",   std::to_string(_shutterType));
    frame.setMetadata("renderer/shutter",       _shutter);
}
