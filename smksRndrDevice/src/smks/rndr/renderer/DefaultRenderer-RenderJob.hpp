// ========================================================================= //
// Modification copyright 2016-2018 SUPAMONKS_STUDIO                         //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
// _________________________________________________________________________ //
// Copyright 2009-2013 Intel Corporation                                     //
//                                                                           //
// Licensed under the Apache License, Version 2.0 (the "License");           //
// you may not use this file except in compliance with the License.          //
// You may obtain a copy of the License at                                   //
//                                                                           //
//     http://www.apache.org/licenses/LICENSE-2.0                            //
//                                                                           //
// Unless required by applicable law or agreed to in writing, software       //
// distributed under the License is distributed on an "AS IS" BASIS,         //
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  //
// See the License for the specific language governing permissions and       //
// limitations under the License.                                            //
//                                                                           //
// ========================================================================= //
#pragma once

#include <vector>
#include <smks/sys/TaskScheduler.hpp>
#include "DefaultRenderer.hpp"

namespace smks
{
    class ClientBase;

    namespace rndr
    {
        struct Ray;
        class DefaultRenderer::RenderJob
        {
        private:
            typedef std::weak_ptr<ClientBase> ClientWPtr;
        private:
            const ClientWPtr            _client;

            sys::Ref<DefaultRenderer>   _renderer;
            sys::Ref<Camera> const&     _camera;
            sys::Ref<Scene> const&      _scene;
            sys::Ref<FrameBuffer>       _frameBuffer;
            int                         _iteration;

            float                       _rWidth;        //!< Reciprocal width of framebuffer.
            float                       _rHeight;       //!< Reciprocal height of framebuffer.
            size_t                      _numTilesX;     //!< Number of tiles in x direction.
            size_t                      _numTilesY;     //!< Number of tiles in y direction.

            double                      _time0;         //!< start time of rendering
            sys::sync::Atomic           _tileId;        //!< ID of current tile
            sys::sync::Atomic           _numRays;       //!< for counting number of shoot rays
            sys::TaskScheduler::Task    _task;

        public:
            RenderJob(ClientWPtr const&,
                      sys::Ref<DefaultRenderer>,
                      sys::Ref<Camera> const&,
                      sys::Ref<Scene> const&,
                      sys::Ref<FrameBuffer>,
                      int iteration);

        private:
            TASK_RUN_FUNCTION       (RenderJob, renderTile);    /*! start functon */
            TASK_COMPLETE_FUNCTION  (RenderJob, finish);        /*! finish function */
        };
    }
}
