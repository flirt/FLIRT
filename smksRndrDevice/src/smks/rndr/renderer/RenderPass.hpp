// ========================================================================= //
// Copyright 2016-2018 SUPAMONKS_STUDIO                                      //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
//                                                                           //
// ========================================================================= //
#pragma once

#include <smks/parm/BuiltIns.hpp>
#include "../ObjectBase.hpp"

namespace smks
{
    namespace parm
    {
        class Getters;
    }
    namespace rndr
    {
        class FlatRenderPassBase;
        class LightPathExpressionPass;
        class ShapeIdCoveragePass;

        class RenderPass:
            public ObjectBase
        {
        public:
            typedef sys::Ref<RenderPass> Ptr;
        private:
            bool _storeAsHalf;

        protected:
            RenderPass(unsigned int scnId, const CtorArgs& args):
                ObjectBase  (scnId, args),
                _storeAsHalf(false)
            {
                dispose();
            }

        public:
            inline
            ~RenderPass()
            {
                dispose();
            }

            inline
            bool
            storeAsHalf() const
            {
                return _storeAsHalf;
            }

            virtual inline
            void
            dispose()
            {
                getBuiltIn<bool>(parm::storeAsHalf(), _storeAsHalf);
            }

            virtual inline
            void
            commitFrameState(const parm::Container& parms, const xchg::IdSet& dirties)
            {
                logParameters(scnId(), parms, "builtIns", dirties);
                logParameters(scnId(), userParameters(), "userParms", "USER");
                //---
                for (xchg::IdSet::const_iterator id = dirties.begin(); id != dirties.end(); ++id)
                    if (*id == parm::storeAsHalf().id())
                        getBuiltIn<bool>(parm::storeAsHalf(), _storeAsHalf, &parms);
            }

            inline
            void
            beginSubframeCommits(size_t)
            { }

            inline
            void
            commitSubframeState(size_t, const parm::Container&)
            { }

            inline
            void
            endSubframeCommits()
            { }

            inline
            bool
            perSubframeParameter(unsigned int) const
            {
                return false;
            }

            /*! Allows the request optional user parameters prior scene primitive extraction. */
            virtual
            void
            requestUserParametersFromPrimitives(parm::Getters&) = 0;

            virtual inline
            FlatRenderPassBase*
            asFlatRenderPass()
            {
                return nullptr;
            }

            virtual inline
            const FlatRenderPassBase*
            asFlatRenderPass() const
            {
                return nullptr;
            }

            virtual inline
            LightPathExpressionPass*
            asLightPathExpressionPass()
            {
                return nullptr;
            }

            virtual inline
            const LightPathExpressionPass*
            asLightPathExpressionPass() const
            {
                return nullptr;
            }

            virtual inline
            ShapeIdCoveragePass*
            asShapeIdCoveragePass()
            {
                return nullptr;
            }

            virtual inline
            const ShapeIdCoveragePass*
            asShapeIdCoveragePass() const
            {
                return nullptr;
            }
        };
    }
}
