// ========================================================================= //
// Copyright 2016-2018 SUPAMONKS_STUDIO                                      //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
//                                                                           //
// ========================================================================= //
#pragma once

#include <smks/parm/BuiltIns.hpp>
#include "RenderPass.hpp"

namespace smks { namespace rndr
{
    class ShapeIdCoveragePass:
        public RenderPass
    {
        VALID_RTOBJECT
    public:
        typedef sys::Ref<ShapeIdCoveragePass> Ptr;
    private:
        size_t _maxCount;

        CREATE_RTOBJECT(ShapeIdCoveragePass, RenderPass)
    protected:
        ShapeIdCoveragePass(unsigned int scnId, const CtorArgs& args):
            RenderPass  (scnId, args),
            _maxCount   (0)
        {
            dispose();
        }

    public:
        inline
        size_t
        maxCount() const
        {
            return _maxCount;
        }

        inline
        void
        dispose()
        {
            getBuiltIn<size_t>(parm::maxCount(), _maxCount);
            //---
            RenderPass::dispose();
        }

        inline
        void
        commitFrameState(const parm::Container& parms, const xchg::IdSet& dirties)
        {
            RenderPass::commitFrameState(parms, dirties);
            //---
            for (xchg::IdSet::const_iterator id = dirties.begin(); id != dirties.end(); ++id)
                if (*id == parm::maxCount().id())
                    getBuiltIn<size_t>(parm::maxCount(), _maxCount, &parms);
        }

        void
        requestUserParametersFromPrimitives(parm::Getters&)
        { }

        inline
        ShapeIdCoveragePass*
        asShapeIdCoveragePass()
        {
            return this;
        }

        inline
        const ShapeIdCoveragePass*
        asShapeIdCoveragePass() const
        {
            return this;
        }
    };
}
}
