// ========================================================================= //
// Copyright 2016-2018 SUPAMONKS_STUDIO                                      //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
//                                                                           //
// ========================================================================= //
#pragma once

#include <smks/parm/BuiltIns.hpp>
#include "RenderPass.hpp"

/*
* This class implements a render pass isolating the contribution
* of specific rays traced throughout the scene based on the type
* of interactions they had at each bounce.
* Most common light path expressions are:
* - "LDE" : direct diffuse component
* - "LSE" : direct specular component
* - "L.*DDE" : direct interreflection component
* - "LD{1,2}E" : first two diffuse bounces
* - "LS.*DE" : caustics
*
*/
namespace smks { namespace rndr
{
    class LightPathExpressionPass:
        public RenderPass
    {
        VALID_RTOBJECT
    public:
        typedef sys::Ref<LightPathExpressionPass> Ptr;
    private:
        std::string _regex;

        CREATE_RTOBJECT(LightPathExpressionPass, RenderPass)
    protected:
        LightPathExpressionPass(unsigned int scnId, const CtorArgs& args):
            RenderPass  (scnId, args),
            _regex      ()
        {
            dispose();
        }

    public:
        inline
        const std::string&
        regex() const
        {
            return _regex;
        }

        inline
        void
        dispose()
        {
            getBuiltIn<std::string>(parm::regex(), _regex);
            //---
            RenderPass::dispose();
        }

        inline
        void
        commitFrameState(const parm::Container& parms, const xchg::IdSet& dirties)
        {
            RenderPass::commitFrameState(parms, dirties);
            //---
            for (xchg::IdSet::const_iterator id = dirties.begin(); id != dirties.end(); ++id)
                if (*id == parm::regex().id())
                    getBuiltIn<std::string>(parm::regex(), _regex, &parms);
        }

        void
        requestUserParametersFromPrimitives(parm::Getters&)
        { }

        inline
        LightPathExpressionPass*
        asLightPathExpressionPass()
        {
            return this;
        }

        inline
        const LightPathExpressionPass*
        asLightPathExpressionPass() const
        {
            return this;
        }
    };
}
}
