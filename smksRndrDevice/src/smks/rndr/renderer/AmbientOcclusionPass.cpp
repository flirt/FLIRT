// ========================================================================= //
// Copyright 2016-2018 SUPAMONKS_STUDIO                                      //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
//                                                                           //
// ========================================================================= //
#include "AmbientOcclusionPass.hpp"
#include "../../parm/Getters.hpp"
#include <smks/sys/Log.hpp>

using namespace smks;

rndr::AmbientOcclusionPass::AmbientOcclusionPass(unsigned int scnId, const CtorArgs& args):
    FlatRenderPass  (scnId, args),
    _numSamples     (16),
    _maxDistance    (sys::inf),
    //---------------
    _userColorName  (),
    _userColorIdx   (parm::Getters::INVALID_IDX)
{
    dispose();
}

rndr::AmbientOcclusionPass::~AmbientOcclusionPass()
{
    dispose();
}

void
rndr::AmbientOcclusionPass::dispose()
{
    getBuiltIn<size_t>      (parm::numSamples(),    _numSamples);
    getBuiltIn<float>       (parm::maxDistance(),   _maxDistance);
    getBuiltIn<std::string> (parm::userColorName(), _userColorName);
    _userColorIdx = parm::Getters::INVALID_IDX;
    //---
    FlatRenderPass<xchg::AMBIENT_OCCLUSION_PASS>::dispose();
}

void
rndr::AmbientOcclusionPass::commitFrameState(const parm::Container& parms, const xchg::IdSet& dirties)
{
    FlatRenderPass<xchg::AMBIENT_OCCLUSION_PASS>::commitFrameState(parms, dirties);
    //---
    for (xchg::IdSet::const_iterator id = dirties.begin(); id != dirties.end(); ++id)
    {
        if      (*id == parm::numSamples().id())    getBuiltIn<size_t>      (parm::numSamples(),    _numSamples,    &parms);
        else if (*id == parm::maxDistance().id())   getBuiltIn<float>       (parm::maxDistance(),   _maxDistance,   &parms);
        else if (*id == parm::userColorName().id()) getBuiltIn<std::string> (parm::userColorName(), _userColorName, &parms);
    }
    //---
    FLIRT_LOG(LOG(DEBUG)
        << "\n--------------------\n"
        << "ambient occlusion (ID = " << scnId() << ")\n"
        << "\n\t- # samples = " << _numSamples
        << "\n\t- max distance = " << _maxDistance
        << "\n\t- user color name = '" << _userColorName << "'"
        << "\n--------------------";)
}

void
rndr::AmbientOcclusionPass::requestUserParametersFromPrimitives(parm::Getters& getters)
{
    // request user color for all primitives
    _userColorIdx = !_userColorName.empty()
        ? getters.append<math::Vector4f>(_userColorName.c_str())
        : parm::Getters::INVALID_IDX;
    //---
    FLIRT_LOG(LOG(DEBUG)
        << "\n--------------------\n"
        << "ambient occlusion (ID = " << scnId() << ")\n"
        << "\n\t- user color getter index = " << _userColorIdx
        << "\n--------------------";)
}
