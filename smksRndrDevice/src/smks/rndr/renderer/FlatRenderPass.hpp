// ========================================================================= //
// Copyright 2016-2018 SUPAMONKS_STUDIO                                      //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
//                                                                           //
// ========================================================================= //
#pragma once

#include "RenderPass.hpp"
#include "FlatRenderPassType.hpp"
#include "../../../std/to_string.hpp"

namespace smks { namespace rndr
{
    class AmbientOcclusionPass;

    ///////////////////////////
    // class FlatRenderPassBase
    ///////////////////////////
    class FlatRenderPassBase:
        public RenderPass
    {
    public:
        typedef sys::Ref<FlatRenderPassBase> Ptr;

    protected:
        FlatRenderPassBase(unsigned int scnId, const CtorArgs& args):
            RenderPass(scnId, args)
        { }

    public:
        virtual
        xchg::FlatRenderPassType
        type() const = 0;

        inline
        FlatRenderPassBase*
        asFlatRenderPass()
        {
            return this;
        }

        inline
        const FlatRenderPassBase*
        asFlatRenderPass() const
        {
            return this;
        }

        virtual inline
        AmbientOcclusionPass*
        asAmbientOcclusionPass()
        {
            return nullptr;
        }

        virtual inline
        const AmbientOcclusionPass*
        asAmbientOcclusionPass() const
        {
            return nullptr;
        }
    };

    /////////////////////////////////
    // class FlatRenderPass<PassType>
    /////////////////////////////////
    template <int PassType>
    class FlatRenderPass:
        public FlatRenderPassBase
    {
        VALID_RTOBJECT
    public:
        typedef sys::Ref<FlatRenderPass> Ptr;

        CREATE_RTOBJECT(FlatRenderPass<PassType>, RenderPass)
    protected:
        FlatRenderPass(unsigned int scnId, const CtorArgs& args):
            FlatRenderPassBase(scnId, args)
        { }

    public:
        inline
        xchg::FlatRenderPassType
        type() const
        {
            return static_cast<xchg::FlatRenderPassType>(PassType);
        }

        void
        requestUserParametersFromPrimitives(parm::Getters&)
        { }
    };
}
}
