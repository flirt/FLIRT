// ========================================================================= //
// Copyright 2016-2018 SUPAMONKS_STUDIO                                      //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
//                                                                           //
// ========================================================================= //
#include <cassert>

#include <smks/sys/Log.hpp>
#include <smks/math/matrixAlgo.hpp>
#include <smks/math/bounds.hpp>

#include "DifferentialGeometry.hpp"
#include "../api/Ray.hpp"
#include "HairGeometry-VertexInterpolator.hpp"

using namespace smks;

rndr::HairGeometry::VertexInterpolator::VertexInterpolator():
    SubframeInterpolatorBase(),
    _worldGBufferSeqs   (_NUM_GBUFFERS),
    _worldBoundsMinSeq  (),
    _worldBoundsMaxSeq  (),
    _rtIndices          (nullptr),
    _rtPathIds          (nullptr),
    _rtScalpTexcoords   (),
    //-----------------------
    _updates            ()
{ }

void
rndr::HairGeometry::VertexInterpolator::disposeSubframeData()
{
    for (size_t buf = 0; buf < _worldGBufferSeqs.size(); ++buf)
    {
        _worldGBufferSeqs[buf].clear();
        _worldGBufferSeqs[buf].shrink_to_fit();
    }
    _worldBoundsMinSeq.clear();
    _worldBoundsMinSeq.shrink_to_fit();
    _worldBoundsMaxSeq.clear();
    _worldBoundsMaxSeq.shrink_to_fit();
}

void
rndr::HairGeometry::VertexInterpolator::disposeFrameData()
{
    _rtIndices = nullptr;
    _rtPathIds = nullptr;
    _rtScalpTexcoords.clear();
}

void
rndr::HairGeometry::VertexInterpolator::resizeSubframeData(size_t numSubframes)
{
    for (size_t buf = 0; buf < _worldGBufferSeqs.size(); ++buf)
        _worldGBufferSeqs[buf].resize(numSubframes, nullptr);
    _worldBoundsMinSeq.resize(numSubframes, math::Vector4f::Constant(FLT_MAX));
    _worldBoundsMaxSeq.resize(numSubframes, math::Vector4f::Constant(-FLT_MAX));
}

void
rndr::HairGeometry::VertexInterpolator::setSubframeData(
    size_t                   subframe,
    int                      currentSampleIdx,
    const math::Affine3f&    worldTransform,
    const xchg::DataStream&  rtPositions,
    const xchg::DataStream&  rtWidths,
    const xchg::DataStream&  rtNormals,
    const xchg::BoundingBox& localBounds)
{
    assert(subframe < numSubframes());
    FLIRT_LOG(LOG(DEBUG)
        << "hair geometry vertex interpolation: "
        << subframe << " / " << numSubframes() << "\n"
        << "\t- current sample = " << currentSampleIdx << "\n"
        << "\t- positions = " << rtPositions << "\n"
        << "\t- widths = " << rtWidths << "\n"
        << "\t- normals   = " << rtNormals << "\n"
        << "\t- local bounds = " << localBounds << "\n"
        << "\t- world xfm = \n " << worldTransform.matrix();)

    const size_t prvNumVertices = numVertices();

    // control points' positions and radii
    xchg::LockedDataStream rtPositions_;
    if (rtPositions.lock(rtPositions_))
    {
        GBufferPtr& buffer = _worldGBufferSeqs[GBUFFER_VERTEX][subframe];

        buffer = createGBuffer();
        math::transformPoints(worldTransform, rtPositions_, *buffer);
        updateWidths(worldTransform, rtWidths, *buffer);
    }
    else
        _worldGBufferSeqs[GBUFFER_VERTEX][subframe] = nullptr;

    // scalp normals
    xchg::LockedDataStream rtNormals_;
    if (rtNormals.lock(rtNormals_))
    {
        GBufferPtr& buffer = _worldGBufferSeqs[GBUFFER_SCALP_NORMAL][subframe];

        buffer = createGBuffer();
        math::transformDirections(worldTransform, rtNormals_, *buffer);
    }
    else
        _worldGBufferSeqs[GBUFFER_SCALP_NORMAL][subframe] = nullptr;

    // do this only upon position/radius change
    bool verticesChanged = true;
    if (verticesChanged)
    {
        _updates.setReuploadVertexBuffer(static_cast<uint8_t>(subframe));

        if (localBounds.valid())
            math::transform(
                worldTransform,
                localBounds,
                _worldBoundsMinSeq[subframe],
                _worldBoundsMaxSeq[subframe]);
        else if (_worldGBufferSeqs[GBUFFER_VERTEX][subframe])
        {
            const math::Vector4fv& worldPositions = *(_worldGBufferSeqs[GBUFFER_VERTEX][subframe]);

            math::min(worldPositions, _worldBoundsMinSeq[subframe]);
            math::max(worldPositions, _worldBoundsMaxSeq[subframe]);
        }
        else
        {
            _worldBoundsMinSeq[subframe].setConstant(FLT_MAX);
            _worldBoundsMaxSeq[subframe].setConstant(-FLT_MAX);
        }

        int iStart, iEnd;
        getInvalidatedTabEntryRange(subframe, iStart, iEnd);
        for (int i = iStart; i <= iEnd; ++i)
            invalidateTabEntry(i);
    }
    if (numVertices() != prvNumVertices)
        _updates.setRebuildGeometry();
}

// static
void
rndr::HairGeometry::VertexInterpolator::updateWidths(
    const math::Affine3f&   worldTransform,
    const xchg::DataStream& rtWidths,
    math::Vector4fv&        buffer)
{
    xchg::LockedDataStream rtWidths_;
    if (!rtWidths.lock(rtWidths_) ||
        rtWidths_.numElements() != buffer.size())
        return;

    const float sc = math::scaling(worldTransform);
    for (size_t i = 0; i < buffer.size(); ++i)
        buffer[i].w() = sc * rtWidths_.getPtr<float>(i)[0];
}

void
rndr::HairGeometry::VertexInterpolator::setIndices(xchg::Data::Ptr const& value)
{
    if (value == _rtIndices)
        return;

    const size_t prvNumCurves = numCurves();

    _rtIndices = value;
    //---
    invalidateAllTabEntries();
    if (numCurves() != prvNumCurves)
        _updates.setRebuildGeometry();
    else
        _updates.setReuploadIndexBuffer();
}

void
rndr::HairGeometry::VertexInterpolator::setPathIds(xchg::Data::Ptr const& value)
{
    _rtPathIds = value;
}

void
rndr::HairGeometry::VertexInterpolator::setScalpTexCoords(const xchg::DataStream& value)
{
    value.lock(_rtScalpTexcoords);
}

bool
rndr::HairGeometry::VertexInterpolator::getWorldBounds(
    math::Vector4f& minBounds,
    math::Vector4f& maxBounds) const
{
    assert(numSubframes() > 0);
    math::min(_worldBoundsMinSeq, minBounds);
    math::max(_worldBoundsMaxSeq, maxBounds);

#if defined(FLIRT_LOG_ENABLED)
    {
        std::stringstream sstr;
        for (size_t n = 0; n < numSubframes(); ++n)
            sstr
                << "[" << n << "]\tbounds = "
                << "(" << _worldBoundsMinSeq[n].transpose() << ") -> "
                << "(" << _worldBoundsMaxSeq[n].transpose() << ")\n";
        sstr << "=>\t(" << minBounds.transpose() << ") -> (" << maxBounds.transpose() << ")";
        LOG(DEBUG) << sstr.str();
    }
#endif // defined(FLIRT_LOG_ENABLED)

    return !math::anyGreaterThan(minBounds, maxBounds);
}

const void*
rndr::HairGeometry::VertexInterpolator::getVertexBuffer(
    size_t  subframe,
    size_t& byteOffset,
    size_t& byteStride,
    size_t& size) const
{
    assert(subframe < numSubframes());
    GBufferPtr const& buffer = _worldGBufferSeqs[GBUFFER_VERTEX][subframe];

    byteOffset  = 0;
    byteStride  = sizeof(math::Vector4f);
    size        = buffer ? buffer->size() : 0;

    return buffer && !buffer->empty()
        ? reinterpret_cast<const void*>(&buffer->front())
        : nullptr;
}

const void*
rndr::HairGeometry::VertexInterpolator::getIndexBuffer(
    size_t& byteOffset,
    size_t& byteStride,
    size_t& size) const
{
    byteOffset  = 0;
    byteStride  = sizeof(unsigned int);
    size        = _rtIndices ? _rtIndices->size() : 0;

    return _rtIndices
        ? reinterpret_cast<const void*>(_rtIndices->getPtr<unsigned int>())
        : nullptr;
}

void
rndr::HairGeometry::VertexInterpolator::postIntersect(
    RTCScene,
    const Ray&              ray,
    DifferentialGeometry&   dg) const
{
    // get subframe indices and weight for time interpolation
    size_t  curSubframe = 0;
    size_t  nxtSubframe = 0;
    float   blend       = 0.0f;

    if (numSubframes() > 1)
        getSubframes(ray.time, curSubframe, nxtSubframe, blend);

    // apply Bezier interpolation for computing hair position and radius
    math::Vector4fv     cv  (4, math::Vector4f::UnitW());
    const unsigned int  vId = _rtIndices->get<unsigned int>(ray.primID);
    assert(vId >= 0 && vId+3 < numVertices());

    if (curSubframe == nxtSubframe)
    {
        const math::Vector4fv& vertices = *_worldGBufferSeqs[GBUFFER_VERTEX][curSubframe];
        for (size_t i = 0; i < 4; ++i)
            cv[i] = vertices[vId+i];
    }
    else
    {
        const GBufferSequence& verticesSeq = _worldGBufferSeqs[GBUFFER_VERTEX];
        const math::Vector4fv& curVertices = *verticesSeq[curSubframe];
        const math::Vector4fv& nxtVertices = *verticesSeq[nxtSubframe];
        for (size_t i = 0; i < 4; ++i)
        {
            const math::Vector4f& cur = curVertices[vId+i];
            const math::Vector4f& nxt = nxtVertices[vId+i];
            cv[i] = (cur + blend * (nxt - cur));
        }
    }
    const math::Vector4f& cv10  = cv[1] - cv[0];
    const math::Vector4f& cv21  = cv[2] - cv[1];
    const math::Vector4f& cv30  = cv[3] - cv[0];
    const math::Vector4f& v     = cv[0] +
        ray.u * (3.0f * cv10 +
            ray.u * (3.0f * (cv21 - cv10) +
                ray.u * (cv30 - 3.0f * cv21)));

    // compute tangent plane
    dg.Tx = math::normalized3(ray.Ng); // actually tangent along the curve, cf. bezier_intersector1.h: 94
    dg.Ty = math::normalized3(ray.dir.cross3(dg.Tx));
    dg.Ng = dg.Tx.cross3(dg.Ty);

    // position and shading normal
    dg.P            = v; dg.P.w() = 1.0f; //dg.P = ray.org + ray.tfar * ray.dir;
    dg.Ns           = dg.Ng;
    dg.time         = ray.time;
    dg.hairRadius   = v.w();
    dg.error        = math::max<float>(
        math::abs(ray.tfar),
        math::max_xyz(dg.P.cwiseAbs()));

    // if available, use the scalp's texture coordinates
    if (_rtScalpTexcoords.numElements())
    {
        assert(_rtPathIds);
        assert(ray.primID >= 0 && ray.primID < static_cast<int>(_rtPathIds->size()));

        const unsigned int  pathID      = _rtPathIds->get<unsigned int>(ray.primID);
        const float*        texcoords   = _rtScalpTexcoords.getPtr<float>(pathID);

        dg.st.x() = texcoords[0];
        dg.st.y() = texcoords[1];
    }
    else
    {
        dg.st.x() = ray.u;
        dg.st.y() = ray.v;
    }
}
