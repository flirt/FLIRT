// ========================================================================= //
// Modification copyright 2016-2018 SUPAMONKS_STUDIO                         //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
// _________________________________________________________________________ //
// Copyright 2009-2013 Intel Corporation                                     //
//                                                                           //
// Licensed under the Apache License, Version 2.0 (the "License");           //
// you may not use this file except in compliance with the License.          //
// You may obtain a copy of the License at                                   //
//                                                                           //
//     http://www.apache.org/licenses/LICENSE-2.0                            //
//                                                                           //
// Unless required by applicable law or agreed to in writing, software       //
// distributed under the License is distributed on an "AS IS" BASIS,         //
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  //
// See the License for the specific language governing permissions and       //
// limitations under the License.                                            //
//                                                                           //
// ========================================================================= //
#include <cassert>
#include "RtcGeomUpdates.hpp"

namespace smks { namespace rndr
{
    static
    void
    setBit(uint64_t& bitset, size_t i, bool value)
    {
        assert(i < 64);
        bitset = value
            ? bitset | (1i64 << i)
            : bitset & (~(1i64 << i));
    }
    static
    bool
    getBit(uint64_t bitset, size_t i)
    {
        assert(i < 64);
        return (bitset & (1i64 << i)) != 0;
    }
}
}

using namespace smks;

rndr::RtcGeomUpdates::RtcGeomUpdates()
{
    clear();
}
rndr::RtcGeomUpdates::RtcGeomUpdates(const RtcGeomUpdates& other)
{
    copy(other);
}

rndr::RtcGeomUpdates&
rndr::RtcGeomUpdates::operator=(const RtcGeomUpdates& other)
{
    copy(other);
    return *this;
}

void
rndr::RtcGeomUpdates::clear()
{
    _flags = 0;
    for (int i = 0; i < NUM_VERTEX_BUFFER_BATCHES; ++i)
        _vertexBufferFlags[i] = 0;
}
void
rndr::RtcGeomUpdates::copy(const RtcGeomUpdates& other)
{
    _flags = other._flags;
    for (int i = 0; i < NUM_VERTEX_BUFFER_BATCHES; ++i)
        _vertexBufferFlags[i] = other._vertexBufferFlags[i];
}

void
rndr::RtcGeomUpdates::setRebuildGeometry()
{
    _flags = -1;
    for (int i = 0; i < NUM_VERTEX_BUFFER_BATCHES; ++i)
        _vertexBufferFlags[i] = -1;
    // everything must be reuploaded whenever the geometry must be rebuilt
}
bool
rndr::RtcGeomUpdates::getRebuildGeometry() const
{
    return getBit(_flags, REBUILD_GEOM_BIT);
}

void
rndr::RtcGeomUpdates::setReuploadVertexBuffer(uint8_t idx)
{
    setBit(_vertexBufferFlags[idx / 64], idx % 64, true);
}
bool
rndr::RtcGeomUpdates::getReuploadVertexBuffer(uint8_t idx) const
{
    return getBit(_vertexBufferFlags[idx / 64], idx % 64);
}

void
rndr::RtcGeomUpdates::setReuploadIndexBuffer()
{
    setBit(_flags, REUPLOAD_INDEX_BUFFER_BIT, true);
}
bool
rndr::RtcGeomUpdates::getReuploadIndexBuffer() const
{
    return getBit(_flags, REUPLOAD_INDEX_BUFFER_BIT);
}

void
rndr::RtcGeomUpdates::setReuploadFaceBuffer()
{
    setBit(_flags, REUPLOAD_FACE_BUFFER_BIT, true);
}
bool
rndr::RtcGeomUpdates::getReuploadFaceBuffer() const
{
    return getBit(_flags, REUPLOAD_FACE_BUFFER_BIT);
}

void
rndr::RtcGeomUpdates::setReuploadLevelBuffer()
{
    setBit(_flags, REUPLOAD_LEVEL_BUFFER_BIT, true);
}
bool
rndr::RtcGeomUpdates::getReuploadLevelBuffer() const
{
    return getBit(_flags, REUPLOAD_LEVEL_BUFFER_BIT);
}
