// ========================================================================= //
// Modification copyright 2016-2018 SUPAMONKS_STUDIO                         //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
// _________________________________________________________________________ //
// Copyright 2009-2013 Intel Corporation                                     //
//                                                                           //
// Licensed under the Apache License, Version 2.0 (the "License");           //
// you may not use this file except in compliance with the License.          //
// You may obtain a copy of the License at                                   //
//                                                                           //
//     http://www.apache.org/licenses/LICENSE-2.0                            //
//                                                                           //
// Unless required by applicable law or agreed to in writing, software       //
// distributed under the License is distributed on an "AS IS" BASIS,         //
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  //
// See the License for the specific language governing permissions and       //
// limitations under the License.                                            //
//                                                                           //
// ========================================================================= //
#pragma once

#include "Shape.hpp"

namespace smks
{
    namespace xchg
    {
        class Data;
    }

    namespace rndr
    {
        class TriangleMesh:
            public Shape
        {
        private:
            class VertexInterpolator;

            VALID_RTOBJECT
        public:
            typedef sys::Ref<TriangleMesh>      Ptr;
        protected:
            typedef std::shared_ptr<xchg::Data> DataPtr;

        private:
            VertexInterpolator* _interpolator;
            char                _curGeomFlags;

            CREATE_RTOBJECT(TriangleMesh, Shape)
        protected:
            TriangleMesh(unsigned int scnId, const CtorArgs&);
        public:
            ~TriangleMesh();

            void
            postIntersect(RTCScene, const Ray&, DifferentialGeometry&) const;
            void
            sample(RTCScene, int    geomId,
                   const math::Vector2f&,
                   float            time,
                   math::Vector4f&  p,
                   math::Vector4f&  n,
                   float&           pdf) const;
            float
            pdf(float) const;

            unsigned    // returns RTC geometry index
            extract(RTCScene);
            bool
            getWorldBounds(math::Vector4f&, math::Vector4f&) const;

            void
            dispose();

            bool
            perSubframeParameter(unsigned int) const;

            void
            beginSubframeCommits(size_t);
            void
            commitSubframeState(size_t, const parm::Container&);
            void
            endSubframeCommits();

            void
            commitFrameState(const parm::Container&, const xchg::IdSet&);

            __forceinline
            TriangleMesh*
            asTriangleMesh()
            {
                return this;
            }

            __forceinline
            const TriangleMesh*
            asTriangleMesh() const
            {
                return this;
            }
        };
    }
}
