// ========================================================================= //
// Copyright 2016-2018 SUPAMONKS_STUDIO                                      //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
//                                                                           //
// ========================================================================= //
#include <cassert>

#include <smks/sys/Log.hpp>
#include <smks/math/matrixAlgo.hpp>
#include <smks/math/bounds.hpp>

#include "../sampler/ShapeSampler.hpp"
#include "DifferentialGeometry.hpp"
#include "SubdivisionMesh-VertexInterpolator.hpp"

using namespace smks;

rndr::SubdivisionMesh::VertexInterpolator::VertexInterpolator():
    SubframeInterpolatorBase(),
    _worldVerticesSeq   (),
    _vertexStride       (0),
    _offsetPosition     (INVALID_OFFSET),
    _offsetNormal       (INVALID_OFFSET),
    _worldBoundsMinSeq  (),
    _worldBoundsMaxSeq  (),
    _rtCounts           (nullptr),
    _1stFaceVertex      (),
    _rtIndices          (nullptr),
    _subdivisionLevel   (1),
    _rtTexcoords        (),
    _rTotalArea         (),
    _areaDistribution   (),
    //-----------------------
    _updates            ()
{ }

void
rndr::SubdivisionMesh::VertexInterpolator::disposeSubframeData()
{
    _worldVerticesSeq   .clear();
    _worldVerticesSeq   .shrink_to_fit();
    _worldBoundsMinSeq  .clear();
    _worldBoundsMinSeq  .shrink_to_fit();
    _worldBoundsMaxSeq  .clear();
    _worldBoundsMaxSeq  .shrink_to_fit();
}

void
rndr::SubdivisionMesh::VertexInterpolator::disposeFrameData()
{
    _rtCounts           = nullptr;
    _1stFaceVertex.clear();
    _1stFaceVertex.shrink_to_fit();
    _rtIndices          = nullptr;
    _subdivisionLevel   = 1;
    _rtTexcoords.clear();
}

void
rndr::SubdivisionMesh::VertexInterpolator::disposePrecomputedTables()
{
    _rTotalArea         .clear();
    _rTotalArea         .shrink_to_fit();
    _areaDistribution   .clear();
    _areaDistribution   .shrink_to_fit();
}

void
rndr::SubdivisionMesh::VertexInterpolator::resizeSubframeData(size_t numSubframes)
{
    _worldVerticesSeq   .resize(numSubframes, nullptr);
    _worldBoundsMinSeq  .resize(numSubframes, math::Vector4f::Constant(FLT_MAX));
    _worldBoundsMaxSeq  .resize(numSubframes, math::Vector4f::Constant(-FLT_MAX));
}

void
rndr::SubdivisionMesh::VertexInterpolator::resizePrecomputedTables(size_t tabSize)
{
    _rTotalArea         .resize(tabSize);
    _areaDistribution   .resize(tabSize);
}

void
rndr::SubdivisionMesh::VertexInterpolator::setSubframeData(
    size_t                      subframe,
    int                         currentSampleIdx,
    const math::Affine3f&       worldTransform,
    const xchg::DataStream&     rtPositions,
    const xchg::DataStream&     rtNormals,
    const xchg::BoundingBox&    localBounds)
{
    assert(subframe < numSubframes());
    FLIRT_LOG(LOG(DEBUG)
        << "subdivision mesh vertex interpolation: "
        << subframe << " / " << numSubframes() << "\n"
        << "\t- current sample = " << currentSampleIdx << "\n"
        << "\t- positions = " << rtPositions << "\n"
        << "\t- normals   = " << rtNormals << "\n"
        << "\t- local bounds = " << localBounds << "\n"
        << "\t- world xfm = \n " << worldTransform.matrix();)

    xchg::LockedDataStream  rtPositions_;
    xchg::LockedDataStream  rtNormals_;
    math::Vector4fv         worldPositions;
    math::Vector4fv         worldNormals;

    const size_t prvNumVertices = numVertices();

    size_t vertexStride     = 0;
    size_t offsetPosition   = INVALID_OFFSET;
    size_t offsetNormal     = INVALID_OFFSET;

    if (rtPositions.lock(rtPositions_))
    {
        math::transformPoints(worldTransform, rtPositions_, worldPositions);
        offsetPosition = vertexStride;
        ++vertexStride;

        if (rtNormals.lock(rtNormals_))
        {
            math::transformDirections(worldTransform, rtNormals_, worldNormals);
            offsetNormal = vertexStride;
            ++vertexStride;
        }
    }

    if (subframe == 0)
    {
        _vertexStride   = vertexStride;
        _offsetPosition = offsetPosition;
        _offsetNormal   = offsetNormal;
    }
    else if (vertexStride != _vertexStride ||
        offsetPosition != _offsetPosition ||
        offsetNormal != _offsetNormal)
        throw sys::Exception(
            "Vertex attribute layout changing between subframes.",
            "Subdivision Mesh Subframe Data");

    if (worldPositions.size())
    {
        GBufferPtr      buffer  = createGBuffer();
        math::Vector4f* vec     = nullptr;

        assert(_vertexStride > 0);
        buffer->resize(worldPositions.size() * _vertexStride, math::Vector4f::Zero());

        vec = &buffer->front() + _offsetPosition;
        for (size_t i = 0; i < worldPositions.size(); ++i)
        {
            *vec = worldPositions[i];
            vec += _vertexStride;
        }
        if (worldNormals.size() == worldPositions.size())
        {
            vec = &buffer->front() + _offsetNormal;
            for (size_t i = 0; i < worldNormals.size(); ++i)
            {
                *vec = worldNormals[i];
                vec += _vertexStride;
            }
        }
        _worldVerticesSeq[subframe] = buffer;
    }
    else
        _worldVerticesSeq[subframe] = nullptr;

    // do this only upon position change
    bool positionsChanged = true;
    if (positionsChanged)
    {
        _updates.setReuploadVertexBuffer(static_cast<uint8_t>(subframe));

        if (localBounds.valid())
            math::transform(
                worldTransform,
                localBounds,
                _worldBoundsMinSeq[subframe],
                _worldBoundsMaxSeq[subframe]);
        else if (worldPositions.size())
        {
            math::min(worldPositions, _worldBoundsMinSeq[subframe]);
            math::max(worldPositions, _worldBoundsMaxSeq[subframe]);
        }
        else
        {
            _worldBoundsMinSeq[subframe].setConstant(FLT_MAX);
            _worldBoundsMaxSeq[subframe].setConstant(-FLT_MAX);
        }

        int iStart, iEnd;
        getInvalidatedTabEntryRange(subframe, iStart, iEnd);
        for (int i = iStart; i <= iEnd; ++i)
            invalidateTabEntry(i);
    }
    if (numVertices() != prvNumVertices)
        _updates.setRebuildGeometry();
}

void
rndr::SubdivisionMesh::VertexInterpolator::setCounts(xchg::Data::Ptr const& value)
{
    if (value == _rtCounts)
        return;

    const size_t prvNumFaces = numFaces();

    _rtCounts = value;

    // precompute face first vertex ID mapping for face-varying interpolation
    unsigned int offset = 0;

    _1stFaceVertex.resize(_rtCounts->size());
    for (size_t f = 0; f < _rtCounts->size(); ++f)
    {
        _1stFaceVertex[f]   = offset;
        offset              += _rtCounts->get<unsigned int>(f);
    }
    //---
    invalidateAllTabEntries();
    if (numFaces() != prvNumFaces)
        _updates.setRebuildGeometry();
    else
        _updates.setReuploadFaceBuffer();
}

void
rndr::SubdivisionMesh::VertexInterpolator::setIndices(xchg::Data::Ptr const& value)
{
    if (value == _rtIndices)
        return;

    const size_t prvNumEdges = numEdges();

    _rtIndices = value;
    //---
    invalidateAllTabEntries();
    if (numEdges() != prvNumEdges)
        _updates.setRebuildGeometry();
    else
        _updates.setReuploadIndexBuffer();
}

void
rndr::SubdivisionMesh::VertexInterpolator::setSubdivisionLevel(unsigned int value)
{
    if (value == _subdivisionLevel)
        return;

    _subdivisionLevel = value;
    _updates.setReuploadLevelBuffer();
}

void
rndr::SubdivisionMesh::VertexInterpolator::setTexCoords(const xchg::DataStream& value)
{
    value.lock(_rtTexcoords);
}

void
rndr::SubdivisionMesh::VertexInterpolator::precomputeTabEntry(
    size_t tabEntry,
    size_t subframe)
{
    if (!isTabEntryValid(tabEntry))
    {
        assert(_worldVerticesSeq[subframe]);
        computeAreaDistribution(
            *_worldVerticesSeq[subframe],
            _areaDistribution[tabEntry],
            _rTotalArea[tabEntry]);
    }
}

void
rndr::SubdivisionMesh::VertexInterpolator::precomputeTabEntry(
    size_t  tabEntry,
    size_t  curSubframe,
    size_t  nxtSubframe,
    float   subframeBlend)
{
    if (!isTabEntryValid(tabEntry))
    {
        assert(_worldVerticesSeq[curSubframe]);
        assert(_worldVerticesSeq[nxtSubframe]);
        computeAreaDistribution(
            *_worldVerticesSeq[curSubframe],
            *_worldVerticesSeq[nxtSubframe],
            subframeBlend,
            _areaDistribution[tabEntry],
            _rTotalArea[tabEntry]);
    }
}

void
rndr::SubdivisionMesh::VertexInterpolator::computeAreaDistribution(
    const math::Vector4fv&  worldVertices0,
    const math::Vector4fv&  worldVertices1,
    float                   w1,
    Distribution1D&         areaDistribution,
    float&                  rTotalArea) const
{
    math::Vector4fv worldVertices;

    math::lerp(worldVertices0, worldVertices1, w1, worldVertices);
    computeAreaDistribution(worldVertices, areaDistribution, rTotalArea);
}

void
rndr::SubdivisionMesh::VertexInterpolator::computeAreaDistribution(
    const math::Vector4fv&  worldVertices,
    Distribution1D&         areaDistribution,
    float&                  rTotalArea) const
{
    areaDistribution.dispose();
    rTotalArea = 0.0f;

    const size_t numPrims = this->numFaces();

    if (numPrims == 0 ||
        numEdges() == 0 ||
        worldVertices.empty() ||
        _offsetPosition == INVALID_OFFSET ||
        _vertexStride == 0)
        return;

    float*  primWeights     = new float[numPrims];
    float   totalArea       = 0.0f;
    size_t  prim1stVertex   = 0;

    const math::Vector4f*   worldPositions  = &worldVertices.front() + _offsetPosition;
    const unsigned int*     counts          = _rtCounts->getPtr<unsigned int>();
    const unsigned int*     indices         = _rtIndices->getPtr<unsigned int>();

    for (size_t f = 0; f < numPrims; ++f)
    {
        // compute the area of the polygonal primitive
        float area = 0.0f;

        const unsigned int* face    = &indices[prim1stVertex];
        const unsigned int  count   = _rtCounts->get<unsigned int>(f);
        assert(count > 2);

        const math::Vector4f& P0 = worldPositions[face[0] * _vertexStride];
        for (size_t i = 2; i < count; ++i)
        {
            const math::Vector4f& Pim1  = worldPositions[face[i-1] * _vertexStride];
            const math::Vector4f& Pi    = worldPositions[face[i] * _vertexStride];

            area += (P0 - Pim1).cross3(Pi - P0).norm(); // must then be divided by 2
        }
        primWeights[f]  = area;
        totalArea       += area;
        prim1stVertex   += count;
    }
    totalArea *= 0.5f;
    if (totalArea > 1e-6f)
    {
        areaDistribution.initialize(primWeights, numPrims);
        rTotalArea = math::rcp(totalArea);
    }
    delete[] primWeights;
}

bool
rndr::SubdivisionMesh::VertexInterpolator::getWorldBounds(
    math::Vector4f& minBounds,
    math::Vector4f& maxBounds) const
{
    assert(numSubframes() > 0);
    math::min(_worldBoundsMinSeq, minBounds);
    math::max(_worldBoundsMaxSeq, maxBounds);

#if defined(FLIRT_LOG_ENABLED)
    {
        std::stringstream sstr;
        for (size_t n = 0; n < numSubframes(); ++n)
            sstr
                << "[" << n << "]\tbounds = "
                << "(" << _worldBoundsMinSeq[n].transpose() << ") -> "
                << "(" << _worldBoundsMaxSeq[n].transpose() << ")\n";
        sstr << "=>\t(" << minBounds.transpose() << ") -> (" << maxBounds.transpose() << ")";
        LOG(DEBUG) << sstr.str();
    }
#endif // defined(FLIRT_LOG_ENABLED)

    return !math::anyGreaterThan(minBounds, maxBounds);
}

const void*
rndr::SubdivisionMesh::VertexInterpolator::getVertexBuffer(
    size_t  subframe,
    size_t& byteOffset,
    size_t& byteStride,
    size_t& size) const
{
    assert(subframe < numSubframes());
    GBufferPtr const& buffer = _worldVerticesSeq[subframe];

    byteOffset  = 0;
    byteStride  = sizeof(math::Vector4f);
    size        = buffer ? buffer->size() : 0;

    return buffer && !buffer->empty()
        ? reinterpret_cast<const void*>(&buffer->front())
        : nullptr;
}

const void*
rndr::SubdivisionMesh::VertexInterpolator::getFaceBuffer(
    size_t& byteOffset,
    size_t& byteStride,
    size_t& size) const
{
    byteOffset  = 0;
    byteStride  = sizeof(unsigned int);
    size        = _rtCounts ? _rtCounts->size() : 0;

    return _rtCounts
        ? reinterpret_cast<const void*>(_rtCounts->getPtr<unsigned int>())
        : nullptr;
}

const void*
rndr::SubdivisionMesh::VertexInterpolator::getIndexBuffer(
    size_t& byteOffset,
    size_t& byteStride,
    size_t& size) const
{
    byteOffset  = 0;
    byteStride  = sizeof(unsigned int);
    size        = _rtIndices ? _rtIndices->size() : 0;

    return _rtIndices
        ? reinterpret_cast<const void*>(_rtIndices->getPtr<unsigned int>())
        : nullptr;
}

void
rndr::SubdivisionMesh::VertexInterpolator::postIntersect(
    RTCScene                rtcScene,
    const Ray&              ray,
    DifferentialGeometry&   dg) const
{
    assert(areAllTabEntriesValid());

    // get subframe indices and weight for time interpolation
    size_t  curSubframe = 0;
    size_t  nxtSubframe = 0;
    float   blend       = 0.0f;

    if (numSubframes() > 1)
        getSubframes(ray.time, curSubframe, nxtSubframe, blend);

    interpolate(
        curSubframe,
        rtcScene, ray.geomID, ray.primID, ray.u, ray.v,
        dg.P, dg.Ns, dg.Tx);

    if (curSubframe != nxtSubframe)
    {
        math::Vector4f nxtPos, nxtNrml, nxtTgx;

        interpolate(
            nxtSubframe,
            rtcScene, ray.geomID, ray.primID, ray.u, ray.v,
            nxtPos, nxtNrml, nxtTgx);

        dg.P  = blend * (nxtPos - dg.P).eval();
        dg.Ns = math::normalized3(blend * (nxtNrml - dg.Ns).eval());
        dg.Tx = math::normalized3(blend * (nxtTgx - dg.Tx).eval());
    }

    dg.Ty = dg.Tx.cross3(dg.Ns);

    dg.Ng = math::normalized3(ray.Ng);
    if (dg.Ng.dot(dg.Ns) < 0.0f)
        dg.Ng = -dg.Ng;

    dg.time       = ray.time;
    dg.hairRadius = -1.0f;
    dg.error      = math::max<float>(
        math::abs(ray.tfar),
        math::max_xyz(dg.P.cwiseAbs()));

    // interpolation of face-varying data
    if (_rtTexcoords.numElements())
    {
        assert(_1stFaceVertex.size() == _rtCounts->size());
        assert(ray.primID < _1stFaceVertex.size());
        const uint32 idx = _1stFaceVertex[ray.primID];

        const unsigned int count = _rtCounts->get<unsigned int>(ray.primID);    // number of face vertices
        assert(count == 3 || count == 4);

        // get interpolation vertex weights
        const float* texcoords[4] = {
            _rtTexcoords.getPtr<float>(idx),
            _rtTexcoords.getPtr<float>(idx + 1),
            _rtTexcoords.getPtr<float>(idx + 2),
            nullptr
        };
        if (count == 4)
        {
            const float     uv = ray.u * ray.v;
            math::Vector4f  weights(
                1.0f - ray.u - ray.v + uv,
                ray.u - uv,
                uv,
                ray.v - uv);

            texcoords[3] = _rtTexcoords.getPtr<float>(idx + 3);
            dg.st.x() = weights.dot(math::Vector4f(
                texcoords[0][0], texcoords[1][0], texcoords[2][0], texcoords[3][0]));
            dg.st.y() = weights.dot(math::Vector4f(
                texcoords[0][1], texcoords[1][1], texcoords[2][1], texcoords[3][1]));
        }
        else // count == 3
        {
            math::Vector4f weights(
                1.0f - ray.u - ray.v,
                ray.u,
                ray.v,
                0.0f);

            dg.st.x() = weights.dot(math::Vector4f(
                texcoords[0][0], texcoords[1][0], texcoords[2][0], 0.0f));
            dg.st.y() = weights.dot(math::Vector4f(
                texcoords[0][1], texcoords[1][1], texcoords[2][1], 0.0f));
        }
    }
    else
    {
        dg.st.x() = ray.u;
        dg.st.y() = ray.v;
    }
}

void
rndr::SubdivisionMesh::VertexInterpolator::sample(
    RTCScene                rtcScene,
    int                     geomId,
    const math::Vector2f&   s,
    float                   time,
    math::Vector4f&         p,
    math::Vector4f&         n,
    float&                  pdf) const
{
    assert(areAllTabEntriesValid());

    // get subframe indices and weight for time interpolation
    size_t  curSubframe = 0;
    size_t  nxtSubframe = 0;
    float   blend       = 0.0f;

    if (numSubframes() > 1)
        getSubframes(time, curSubframe, nxtSubframe, blend);

    // get precomputed table entry index
    const size_t tabIndex = getTabEntryIndex(time);

    // pick a primitive
    const Distribution1D&   primAreas   = _areaDistribution[tabIndex];
    const size_t            primId      = math::min<size_t>(
        static_cast<size_t>(primAreas.sample(s.y()).value),
        primAreas.size()-1);

    // sample the primitive for position and normal
    math::Vector4f tgx;

    interpolate(
        curSubframe,
        rtcScene, geomId, primId, s.x(), s.y(),
        p, n, tgx);
    if (curSubframe != nxtSubframe)
    {
        math::Vector4f nxtPos, nxtNrml, nxtTgx;

        interpolate(
            nxtSubframe,
            rtcScene, geomId, primId, s.x(), s.y(),
            nxtPos, nxtNrml, tgx);
        p   = blend * (nxtPos - p).eval();
        n   = math::normalized3(blend * (nxtNrml - n).eval());
    }
    pdf = _rTotalArea[tabIndex];
}

void
rndr::SubdivisionMesh::VertexInterpolator::interpolate(
    size_t          subframe,
    RTCScene        rtcScene,
    int             geomId,
    size_t          primId,
    float           u,
    float           v,
    math::Vector4f& pos,
    math::Vector4f& nrml,
    math::Vector4f& tgx) const
{
    assert(subframe < numSubframes());
    assert(primId < numFaces());
    assert(_vertexStride > 0);
    assert(_offsetPosition != INVALID_OFFSET);

    struct Vertex { ALIGNED_CLASS math::Vector4f p, n; } V, dVdu, dVdv;

    RTCBufferType typ = static_cast<RTCBufferType>(RTC_VERTEX_BUFFER0 + subframe);

    rtcInterpolate(
        rtcScene, geomId, primId, u, v, typ,
        V.p.data(), dVdu.p.data(), dVdv.p.data(),
        _vertexStride << 2);    // vertex stride is in float4

    pos = V.p;
    tgx = math::normalized3(dVdu.p);
    if (_offsetNormal != INVALID_OFFSET)
        nrml = math::normalized3(V.n);
    else
        nrml = math::normalized3(dVdv.p.cross3(dVdu.p));
}
