// ========================================================================= //
// Modification copyright 2016-2018 SUPAMONKS_STUDIO                         //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
// _________________________________________________________________________ //
// Copyright 2009-2013 Intel Corporation                                     //
//                                                                           //
// Licensed under the Apache License, Version 2.0 (the "License");           //
// you may not use this file except in compliance with the License.          //
// You may obtain a copy of the License at                                   //
//                                                                           //
//     http://www.apache.org/licenses/LICENSE-2.0                            //
//                                                                           //
// Unless required by applicable law or agreed to in writing, software       //
// distributed under the License is distributed on an "AS IS" BASIS,         //
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  //
// See the License for the specific language governing permissions and       //
// limitations under the License.                                            //
//                                                                           //
// ========================================================================= //
#pragma once

#include <stdint.h>

namespace smks { namespace rndr
{
    class RtcGeomUpdates
    {
    private:
        enum { NUM_VERTEX_BUFFER_BATCHES = 2 };
        enum
        {
            REBUILD_GEOM_BIT            = 0,
            REUPLOAD_INDEX_BUFFER_BIT   = 1,
            REUPLOAD_FACE_BUFFER_BIT    = 2,
            REUPLOAD_LEVEL_BUFFER_BIT   = 3
        };
    private:
        uint64_t _flags;
        uint64_t _vertexBufferFlags[NUM_VERTEX_BUFFER_BATCHES];
    public:
        RtcGeomUpdates();
        RtcGeomUpdates(const RtcGeomUpdates&);

        RtcGeomUpdates&
        operator=(const RtcGeomUpdates&);

        void
        clear();

        void
        setRebuildGeometry();
        bool
        getRebuildGeometry() const;

        void
        setReuploadVertexBuffer(uint8_t);
        bool
        getReuploadVertexBuffer(uint8_t) const;

        void
        setReuploadIndexBuffer();
        bool
        getReuploadIndexBuffer() const;

        void
        setReuploadFaceBuffer();
        bool
        getReuploadFaceBuffer() const;

        void
        setReuploadLevelBuffer();
        bool
        getReuploadLevelBuffer() const;

    private:
        void
        copy(const RtcGeomUpdates&);
    };
}
}
