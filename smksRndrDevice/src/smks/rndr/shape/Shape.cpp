// ========================================================================= //
// Modification copyright 2016-2018 SUPAMONKS_STUDIO                         //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
// _________________________________________________________________________ //
// Copyright 2009-2013 Intel Corporation                                     //
//                                                                           //
// Licensed under the Apache License, Version 2.0 (the "License");           //
// you may not use this file except in compliance with the License.          //
// You may obtain a copy of the License at                                   //
//                                                                           //
//     http://www.apache.org/licenses/LICENSE-2.0                            //
//                                                                           //
// Unless required by applicable law or agreed to in writing, software       //
// distributed under the License is distributed on an "AS IS" BASIS,         //
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  //
// See the License for the specific language governing permissions and       //
// limitations under the License.                                            //
//                                                                           //
// ========================================================================= //
#include <bitset>
#include <smks/parm/BuiltIns.hpp>
#include <smks/xchg/DataStream.hpp>
#include <smks/sys/Log.hpp>
#include <std/to_string_xchg.hpp>

#include "Shape.hpp"

using namespace smks;

rndr::Shape::Shape(unsigned int scnId, const CtorArgs& args):
    ObjectBase              (scnId, args),
    _rtcGeomId              (RTC_INVALID_GEOMETRY_ID),
    _updates                (UpdateFlags::NO_UPDATE),
    //-----------------------
    _globallyVisible        (1),
    _globalNumSamples       (0),
    _globalInitialization   (xchg::ACTIVE),
    _numIndexBufferSamples  (0),
    _localBounds            (),
    _illumMask              (0),
    _rayVisibility          (VISIBLE_TO_ALL),
    _recievesShadows        (false),
    _shadowBias             (0.0f)
{
    dispose();
}

// virtual
void
rndr::Shape::dispose()
{
    bool                boolean;
    math::Affine3f      xfm;
    char                chr;
    size_t              size;
    xchg::BoundingBox   bounds;
    char    state;

    getBuiltIn<char>(parm::globalInitialization(), state);
    updateGlobalInitialization(state);

    getBuiltIn<char>(parm::globallyVisible(), chr);
    updateGloballyVisible(chr);

    getBuiltIn<size_t>(parm::globalNumSamples(), size);
    updateGlobalNumSamples(size);

    getBuiltIn<size_t>(parm::numIndexBufferSamples(), size);
    updateNumIndexBufferSamples(size);

    getBuiltIn<int>     (parm::illumMask(),         _illumMask);
    getBuiltIn<float>   (parm::shadowBias(),        _shadowBias);
    getBuiltIn<bool>    (parm::receivesShadows(),   _recievesShadows);

    getBuiltIn<bool>(parm::castsShadows(), boolean);
    updateRayVisibility(boolean, VISIBLE_TO_SHADOW_RAYS);

    getBuiltIn<bool>(parm::primaryVisibility(), boolean);
    updateRayVisibility(boolean, VISIBLE_TO_PRIMARY_RAYS);

    getBuiltIn<bool>(parm::visibleInReflections(), boolean);
    updateRayVisibility(boolean, VISIBLE_TO_REFLECTION_RAYS);

    getBuiltIn<bool>(parm::visibleInRefractions(), boolean);
    updateRayVisibility(boolean, VISIBLE_TO_TRANSMISSION_RAYS);
}

// virtual
void
rndr::Shape::commitFrameState(const parm::Container& parms, const xchg::IdSet& dirties)
{
    logParameters(scnId(), parms, "builtIns", dirties);
    logParameters(scnId(), userParameters(), "userParms", "USER");
    //---
    for (xchg::IdSet::const_iterator id = dirties.begin(); id != dirties.end(); ++id)
    {
        bool                boolean;
        math::Affine3f      xfm;
        char                chr;
        size_t              size;
        xchg::BoundingBox   bounds;
        char    state;

        /*if (*id == parm::worldTransform().id())
        {
            getBuiltIn<math::Affine3f>(parm::worldTransform(), xfm, &parms);
            updateWorldTransform(xfm);
        }
        else */if (*id == parm::globallyVisible().id())
        {
            getBuiltIn<char>(parm::globallyVisible(), chr, &parms);
            updateGloballyVisible(chr);
        }
        else if (*id == parm::globalInitialization().id())
        {
            getBuiltIn<char>(parm::globalInitialization(), state, &parms);
            updateGlobalInitialization(state);
        }
        else if (*id == parm::globalNumSamples().id())
        {
            getBuiltIn<size_t>(parm::globalNumSamples(), size, &parms);
            updateGlobalNumSamples(size);
        }
        else if (*id == parm::numIndexBufferSamples().id())
        {
            getBuiltIn<size_t>(parm::numIndexBufferSamples(), size, &parms);
            updateNumIndexBufferSamples(size);
        }
        else if (*id == parm::illumMask().id())
        {
            getBuiltIn<int>(parm::illumMask(), _illumMask, &parms);
        }
        else if (*id == parm::shadowBias().id())
        {
            getBuiltIn<float>(parm::shadowBias(), _shadowBias, &parms);
        }
        else if (*id == parm::receivesShadows().id())
        {
            getBuiltIn<bool>(parm::receivesShadows(), _recievesShadows, &parms);
        }
        else if (*id == parm::castsShadows().id())
        {
            getBuiltIn<bool>(parm::castsShadows(), boolean, &parms);
            updateRayVisibility(boolean, VISIBLE_TO_SHADOW_RAYS);
        }
        else if (*id == parm::primaryVisibility().id())
        {
            getBuiltIn<bool>(parm::primaryVisibility(), boolean, &parms);
            updateRayVisibility(boolean, VISIBLE_TO_PRIMARY_RAYS);
        }
        else if (*id == parm::visibleInReflections().id())
        {
            getBuiltIn<bool>(parm::visibleInReflections(), boolean, &parms);
            updateRayVisibility(boolean, VISIBLE_TO_REFLECTION_RAYS);
        }
        else if (*id == parm::visibleInRefractions().id())
        {
            getBuiltIn<bool>(parm::visibleInRefractions(), boolean, &parms);
            updateRayVisibility(boolean, VISIBLE_TO_TRANSMISSION_RAYS);
        }
    }

    FLIRT_LOG(LOG(DEBUG)
        << "\n----------------"
        << "\nshape attributes (ID = " << scnId() << ")\n"
        << "\n\t- globally visible = " << static_cast<int>(_globallyVisible)
        << "\n\t- global initialization = " << std::to_string(_globalInitialization)
        << "\n\t- global # samples = " << _globalNumSamples
        << "\n\t- # index samples = " << _numIndexBufferSamples
        << "\n\t- illum mask = " << std::bitset<32>(_illumMask)
        << "\n\t- recieves shadows ? " << _recievesShadows
        << "\n\t- shadow bias = " << _shadowBias
        << "\n\t- ray visibility = " << std::bitset<32>(_rayVisibility)
        << "\n----------------";)
}

void
rndr::Shape::updateGloballyVisible(char value)
{
    _globallyVisible = value;
}

void
rndr::Shape::updateGlobalInitialization(char value)
{
    if (_globalInitialization != value)
        _updates = _updates | UpdateFlags::REBUILD_PIMITIVE;
    _globalInitialization = value;
}

void
rndr::Shape::updateGlobalNumSamples(size_t value)
{
    const bool wasStatic    = _globalNumSamples == 1;
    const bool isStatic     = value == 1;
    if (isStatic != wasStatic)
        _updates = _updates | UpdateFlags::REBUILD_PIMITIVE;

    _globalNumSamples = value;
}

void
rndr::Shape::updateNumIndexBufferSamples(size_t value)
{
    const bool wasStaticTopo    = _numIndexBufferSamples == 1;
    const bool isStaticTopo     = value == 1;
    if (isStaticTopo != wasStaticTopo)
        _updates = _updates | UpdateFlags::REBUILD_PIMITIVE;

    _numIndexBufferSamples = value;
}

RTCGeometryFlags
rndr::Shape::getRtcGeomFlags() const
{
    return _numIndexBufferSamples > 1
        ? RTC_GEOMETRY_DYNAMIC
        : (_globalNumSamples > 1
            ? RTC_GEOMETRY_DEFORMABLE
            : RTC_GEOMETRY_STATIC);
}
