// ========================================================================= //
// Copyright 2016-2018 SUPAMONKS_STUDIO                                      //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
//                                                                           //
// ========================================================================= //
#pragma once

#include <smks/xchg/macro_enum.hpp>

namespace smks { namespace rndr
{
    enum UpdateFlags
    {
        NO_UPDATE                           = 0,
        //--- rtc buffers
        REBUILD_PIMITIVE                    = 0x00000001,
        UPDATE_INDEX_BUFFER                 = 0x00000002,
        UPDATE_VERTEX_BUFFER                = 0x00000004,
        UPDATE_VERTEX_BUFFER0               = 0x00000004,
        UPDATE_VERTEX_BUFFER1               = 0x00000008,
        UPDATE_USER_VERTEX_BUFFER           = 0x00000010,
        UPDATE_USER_VERTEX_BUFFER0          = 0x00000010,
        UPDATE_USER_VERTEX_BUFFER1          = 0x00000020,
        UPDATE_FACE_BUFFER                  = 0x00000040,
        UPDATE_LEVEL_BUFFER                 = 0x00000080,
        UPDATE_EDGE_CREASE_INDEX_BUFFER     = 0x00000100,
        UPDATE_EDGE_CREASE_WEIGHT_BUFFER    = 0x00000200,
        UPDATE_VERTEX_CREASE_INDEX_BUFFER   = 0x00000400,
        UPDATE_VERTEX_CREASE_WEIGHT_BUFFER  = 0x00000800,
        UPDATE_HOLE_BUFFER                  = 0x00001000,
        //--- device buffers
        UPDATE_NORMAL_BUFFER                = 0x00002000,
        UPDATE_NORMAL_BUFFER0               = 0x00002000,
        UPDATE_NORMAL_BUFFER1               = 0x00004000,
        UPDATE_TEXCOORDS_BUFFER             = 0x00008000,
        UPDATE_TEXCOORDS_BUFFER0            = 0x00008000,
        UPDATE_TEXCOORDS_BUFFER1            = 0x00010000,
        UPDATE_TANGENTSX_BUFFER             = 0x00020000,
        UPDATE_TANGENTSX_BUFFER0            = 0x00020000,
        UPDATE_TANGENTSY_BUFFER             = 0x00040000,
        UPDATE_TANGENTSY_BUFFER0            = 0x00040000,
        UPDATE_TANGENTSY_BUFFER1            = 0x00080000,
    };
    ENUM_BITWISE_OPS(UpdateFlags)
}
}
