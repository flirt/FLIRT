// ========================================================================= //
// Modification copyright 2016-2018 SUPAMONKS_STUDIO                         //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
// _________________________________________________________________________ //
// Copyright 2009-2013 Intel Corporation                                     //
//                                                                           //
// Licensed under the Apache License, Version 2.0 (the "License");           //
// you may not use this file except in compliance with the License.          //
// You may obtain a copy of the License at                                   //
//                                                                           //
//     http://www.apache.org/licenses/LICENSE-2.0                            //
//                                                                           //
// Unless required by applicable law or agreed to in writing, software       //
// distributed under the License is distributed on an "AS IS" BASIS,         //
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  //
// See the License for the specific language governing permissions and       //
// limitations under the License.                                            //
//                                                                           //
// ========================================================================= //
#pragma once

#include <smks/math/types.hpp>
#include "../shape/Shape.hpp"

namespace smks
{
    namespace parm
    {
        class Values;
    }
    namespace rndr
    {
        class AreaLight;
        class Material;
        class SubSurface;

        struct DifferentialGeometry
        {
            const Shape*            shape;      //!< pointer to shape of hit shape instance
            const AreaLight*        light;      //!< pointer to area light of hit shape instance
            const Material*         material;   //!< pointer to material of hit shape instance
            const SubSurface*       subsurface; //!< pointer to sub surface of hit shape instance
            math::Vector4f          P;          //!< Hit location in world coordinates.
            math::Vector4f          Tx;         //!< Tangent in x direction
            math::Vector4f          Ty;         //!< Tangent in y direction
            math::Vector4f          Ng;         //!< Normalized geometry normal.
            mutable math::Vector4f  Ns;         //!< Normalized shading normal.
            math::Vector2f          st;         //!< Hit location in surface parameter space.
            float                   error;      //!< Intersection error factor.
            float                   hairRadius; //!< Size of the intersected hair's section.
            int                     illumMask;
            float                   time;       //!< time associated with intersecting ray (useful for motion blur)
            const parm::Values*     userParms;  //!< prefetched map of all requested user parameters (owned by intersected primitive)

        public:
            DifferentialGeometry():
                shape       (nullptr),
                material    (nullptr),
                light       (nullptr),
                subsurface  (nullptr),
                P           (math::Vector4f::Zero()),
                Tx          (math::Vector4f::Zero()),
                Ty          (math::Vector4f::Zero()),
                Ng          (math::Vector4f::Zero()),
                Ns          (math::Vector4f::Zero()),
                st          (math::Vector2f::Zero()),
                error       (0.0f),
                hairRadius  (-1.0f),
                illumMask   (-1),
                time        (0.0f),
                userParms   (nullptr)
            { }
        };
    }
}
