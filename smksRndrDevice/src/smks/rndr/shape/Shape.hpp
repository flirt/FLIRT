// ========================================================================= //
// Modification copyright 2016-2018 SUPAMONKS_STUDIO                         //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
// _________________________________________________________________________ //
// Copyright 2009-2013 Intel Corporation                                     //
//                                                                           //
// Licensed under the Apache License, Version 2.0 (the "License");           //
// you may not use this file except in compliance with the License.          //
// You may obtain a copy of the License at                                   //
//                                                                           //
//     http://www.apache.org/licenses/LICENSE-2.0                            //
//                                                                           //
// Unless required by applicable law or agreed to in writing, software       //
// distributed under the License is distributed on an "AS IS" BASIS,         //
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  //
// See the License for the specific language governing permissions and       //
// limitations under the License.                                            //
//                                                                           //
// ========================================================================= //
#pragma once

#include <embree2/rtcore.h>
#include <smks/sys/Log.hpp>
#include <smks/xchg/BoundingBox.hpp>
#include "../api/Ray.hpp"
#include "../ObjectBase.hpp"
#include "ShapeRayVisibility.hpp"
#include "UpdateFlags.hpp"

namespace smks
{
    namespace xchg
    {
        class DataStream;
    }
    namespace rndr
    {
        struct  Ray;
        struct  DifferentialGeometry;
        class   TriangleMesh;
        class   SubdivisionMesh;
        class   HairGeometry;

        class Shape:
            public ObjectBase
        {
        public:
            typedef sys::Ref<Shape> Ptr;

        protected:
            unsigned            _rtcGeomId;
            UpdateFlags         _updates;
            //------------------
            // up-to-date node parameters
            char                _globallyVisible;
            char                _globalInitialization;
            size_t              _globalNumSamples;      //<! static or dynamic
            size_t              _numIndexBufferSamples; //<! number of topology changes during animation
            xchg::BoundingBox   _localBounds;           //<! bounding box in object space
            int                 _illumMask;             //!< bit mask which light we're interested in
            ShapeRayVisibility  _rayVisibility;
            bool                _recievesShadows;
            float               _shadowBias;            //<! per object shadow bias

        protected:
            Shape(unsigned int scnId, const CtorArgs&);

        public:
            __forceinline
            unsigned
            rtcGeomId() const
            {
                return _rtcGeomId;
            }

            virtual
            unsigned    // returns RTC geometry index
            extract(RTCScene) = 0;

            virtual
            bool
            getWorldBounds(math::Vector4f&, math::Vector4f&) const = 0;

            virtual
            void
            postIntersect(RTCScene, const Ray&, DifferentialGeometry&) const = 0;

            inline
            void
            deleteGeometry(RTCScene rtcScene)
            {
                if (_rtcGeomId != RTC_INVALID_GEOMETRY_ID)
                {
                    assert(rtcScene);
                    FLIRT_LOG(LOG(DEBUG)
                        << "rtcDeleteGeometry (shape ID = " << scnId() << ") <- rtc ID = " << _rtcGeomId;)
                    rtcDeleteGeometry(rtcScene, _rtcGeomId);
                }
                _rtcGeomId = RTC_INVALID_GEOMETRY_ID;
            }

            virtual
            void
            sample(RTCScene, int geomId,
                   const math::Vector2f&,
                   float time,
                   math::Vector4f& p,
                   math::Vector4f& n,
                   float& pdf) const
            {
                throw sys::Exception(
                    "Only supported for triangle and subdivision meshes.",
                    "Shape Sampling");
            }

            // <! Only for surfaces
            virtual
            float
            pdf(float time) const
            {
                return 0.0f;
            }

            virtual
            void
            dispose();

            virtual
            void
            commitFrameState(const parm::Container&, const xchg::IdSet&);

            inline
            bool
            ready() const
            {
                return _globallyVisible != 0 &&
                    _globalInitialization == xchg::ACTIVE;
            }

            __forceinline
            int
            illumMask() const
            {
                return _illumMask;
            }
            __forceinline
            ShapeRayVisibility
            rayVisibility() const
            {
                return _rayVisibility;
            }
            __forceinline
            bool
            recievesShadows() const
            {
                return _recievesShadows;
            }
            __forceinline
            float
            shadowBias() const
            {
                return _shadowBias;
            }
        protected:
            __forceinline
            char
            globalInitialization() const
            {
                return _globalInitialization;
            }
            __forceinline
            char
            globallyVisible() const
            {
                return _globallyVisible;
            }
            RTCGeometryFlags
            getRtcGeomFlags() const;
        public:
            virtual __forceinline
            TriangleMesh*
            asTriangleMesh()
            {
                return nullptr;
            }

            virtual __forceinline
            const TriangleMesh*
            asTriangleMesh() const
            {
                return nullptr;
            }

            virtual __forceinline
            SubdivisionMesh*
            asSubdivisionMesh()
            {
                return nullptr;
            }

            virtual __forceinline
            const SubdivisionMesh*
            asSubdivisionMesh() const
            {
                return nullptr;
            }

            virtual __forceinline
            HairGeometry*
            asHairGeometry()
            {
                return nullptr;
            }

            virtual __forceinline
            const HairGeometry*
            asHairGeometry() const
            {
                return nullptr;
            }

            //--- intersection/occlusion callbacks ----------------
            virtual __forceinline bool intersected(Ray&)    const { return false; }
            virtual __forceinline bool occluded   (Ray& ray)const { ray.transparency.setConstant(0.0f); return false; }
            //-----------------------------------------------------
        private:
            void
            updateGloballyVisible(char);
            void
            updateGlobalInitialization(char);
            void
            updateGlobalNumSamples(size_t);
            void
            updateNumIndexBufferSamples(size_t);

            inline
            void
            updateRayVisibility(bool value, ShapeRayVisibility flag)
            {
                _rayVisibility = value ? (_rayVisibility | flag) : (_rayVisibility & ~flag);
            }
        };
    }
}
