// ========================================================================= //
// Copyright 2016-2018 SUPAMONKS_STUDIO                                      //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
//                                                                           //
// ========================================================================= //
#pragma once

#include <smks/xchg/macro_enum.hpp>
#include "../api/RayType.hpp"
#include "../brdf/BRDFType.hpp"

namespace smks { namespace rndr
{
    enum ShapeRayVisibility
    {
        VISIBLE_TO_NONE                 = 0x00000000,
        VISIBLE_TO_ALL                  = 0xFFFFFFFF,
        VISIBLE_TO_PRIMARY_RAYS         = static_cast<int>(VISIBLE_TO_ALL) & static_cast<int>(PRIMARY_RAY),
        VISIBLE_TO_SHADOW_RAYS          = static_cast<int>(VISIBLE_TO_ALL) & static_cast<int>(SHADOW_RAY),
        VISIBLE_TO_REFLECTION_RAYS      = static_cast<int>(VISIBLE_TO_ALL) & static_cast<int>(REFLECTION_BRDF),
        VISIBLE_TO_TRANSMISSION_RAYS    = static_cast<int>(VISIBLE_TO_ALL) & static_cast<int>(TRANSMISSION_BRDF)
    };
    ENUM_BITWISE_OPS(ShapeRayVisibility)
}
}
