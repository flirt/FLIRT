// ========================================================================= //
// Copyright 2016-2018 SUPAMONKS_STUDIO                                      //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
//                                                                           //
// ========================================================================= //
#pragma once

#include "Shape.hpp"

namespace smks
{
    namespace xchg
    {
        class Data;
    }

    namespace rndr
    {
        class HairGeometry:
            public Shape
        {
        private:
            class VertexInterpolator;

            VALID_RTOBJECT
        public:
            typedef sys::Ref<HairGeometry>      Ptr;
        protected:
            typedef std::shared_ptr<xchg::Data> DataPtr;

        private:
            VertexInterpolator* _interpolator;
            char                _curGeomFlags;

            CREATE_RTOBJECT(HairGeometry, Shape)
        protected:
            HairGeometry(unsigned int scnId, const CtorArgs&);
        public:
            ~HairGeometry();

            unsigned    // returns RTC geometry index
            extract(RTCScene);
            bool
            getWorldBounds(math::Vector4f&, math::Vector4f&) const;

            void
            postIntersect(RTCScene, const Ray&, DifferentialGeometry&) const;

            void
            dispose();

            bool
            perSubframeParameter(unsigned int) const;

            void
            beginSubframeCommits(size_t);
            void
            commitSubframeState(size_t, const parm::Container&);
            void
            endSubframeCommits();

            void
            commitFrameState(const parm::Container&, const xchg::IdSet&);

            __forceinline
            HairGeometry*
            asHairGeometry()
            {
                return this;
            }

            __forceinline
            const HairGeometry*
            asHairGeometry() const
            {
                return this;
            }
        };
    }
}
