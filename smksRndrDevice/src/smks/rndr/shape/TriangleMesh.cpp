// ========================================================================= //
// Modification copyright 2016-2018 SUPAMONKS_STUDIO                         //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
// _________________________________________________________________________ //
// Copyright 2009-2013 Intel Corporation                                     //
//                                                                           //
// Licensed under the Apache License, Version 2.0 (the "License");           //
// you may not use this file except in compliance with the License.          //
// You may obtain a copy of the License at                                   //
//                                                                           //
//     http://www.apache.org/licenses/LICENSE-2.0                            //
//                                                                           //
// Unless required by applicable law or agreed to in writing, software       //
// distributed under the License is distributed on an "AS IS" BASIS,         //
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  //
// See the License for the specific language governing permissions and       //
// limitations under the License.                                            //
//                                                                           //
// ========================================================================= //
#include <cassert>

#include <smks/sys/Log.hpp>
#include <smks/xchg/Data.hpp>
#include <smks/xchg/DataStream.hpp>
#include <smks/parm/BuiltIns.hpp>
#include <smks/parm/BuiltIn.hpp>

#include "TriangleMesh.hpp"
#include "TriangleMesh-VertexInterpolator.hpp"

using namespace smks;

rndr::TriangleMesh::TriangleMesh(unsigned int scnId, const CtorArgs& args):
    Shape           (scnId, args),
    _interpolator   (new VertexInterpolator()),
    _curGeomFlags   (-1)
{
    dispose();
}

rndr::TriangleMesh::~TriangleMesh()
{
    dispose();
    delete _interpolator;
}

void
rndr::TriangleMesh::dispose()
{
    _interpolator->dispose();
    _curGeomFlags = -1;
    //---
    Shape::dispose();
}

void
rndr::TriangleMesh::postIntersect(
    RTCScene                rtcScene,
    const Ray&              ray,
    DifferentialGeometry&   dg) const
{
    _interpolator->postIntersect(rtcScene, ray, dg);
}

void
rndr::TriangleMesh::sample(RTCScene             rtcScene,
                            int                     geomId,
                            const math::Vector2f&   s,
                            float                   time,
                            math::Vector4f&         p,
                            math::Vector4f&         n,
                            float&                  pdf) const
{
    _interpolator->sample(rtcScene, geomId, s, time, p, n, pdf);
}

float
rndr::TriangleMesh::pdf(float time) const
{
    return _interpolator->pdf(time);
}

unsigned    // returns RTC geometry index
rndr::TriangleMesh::extract(RTCScene rtcScene)
{
    RtcGeomUpdates updates;
    _interpolator->getAndFlushUpdates(updates);
    //---
    if (updates.getRebuildGeometry())
        Shape::deleteGeometry(rtcScene);

    // do not perform useless updates if invisible or not initialized
    if (globalInitialization() != xchg::ACTIVE)
    {
        FLIRT_LOG(LOG(DEBUG) << "triangle mesh (shape ID = " << scnId() << ") not initialized.";)
        return _rtcGeomId;
    }
    if (!globallyVisible())
    {
        if (_rtcGeomId != RTC_INVALID_GEOMETRY_ID)
        {
            FLIRT_LOG(LOG(DEBUG) << "triangle mesh (shape ID = " << scnId() << ", rtc ID = " << _rtcGeomId << ") disabled.";)
            rtcDisable(rtcScene, _rtcGeomId);
        }
        return _rtcGeomId;
    }
    // abandon geometry creation if mandatory buffers are empty
    if (_interpolator->numSubframes() == 0 ||
        _interpolator->numVertices() == 0 ||
        _interpolator->numTriangles() == 0 )
        return _rtcGeomId;

    if (_rtcGeomId == RTC_INVALID_GEOMETRY_ID ||
        _curGeomFlags != getRtcGeomFlags())
    {
        // (re)build primitive
        _rtcGeomId = rtcNewTriangleMesh(
            rtcScene,
            getRtcGeomFlags(),
            _interpolator->numTriangles(),
            _interpolator->numVertices(),
            _interpolator->numSubframes());
        _curGeomFlags = getRtcGeomFlags();

        FLIRT_LOG(LOG(DEBUG)
            << "rtcNewTriangleMesh (shape ID = " << scnId() << ") -> rtc ID = " << _rtcGeomId << ":"
            << "\n\t- geom flags = " << getRtcGeomFlags()
            << "\n\t- # triangles = " << _interpolator->numTriangles()
            << "\n\t- # vertices = " << _interpolator->numVertices()
            << "\n\t- # subframes = " << _interpolator->numSubframes();)
    }
    assert(_rtcGeomId >= 0);

    const void* buffer      = nullptr;
    size_t      byteOffset  = 0;
    size_t      byteStride  = 0;
    size_t      size        = 0;

    if (updates.getReuploadIndexBuffer())
    {
        buffer = _interpolator->getIndexBuffer(byteOffset, byteStride, size);
        assert(buffer && byteStride > 0);
        rtcSetBuffer2   (rtcScene, _rtcGeomId, RTC_INDEX_BUFFER, buffer, byteOffset, byteStride);
        rtcUpdateBuffer (rtcScene, _rtcGeomId, RTC_INDEX_BUFFER);
    }
    for (size_t n = 0; n < _interpolator->numSubframes(); ++n)
        if (updates.getReuploadVertexBuffer(n))
        {
            RTCBufferType type  = static_cast<RTCBufferType>(RTC_VERTEX_BUFFER0+n);
            buffer              = _interpolator->getVertexBuffer(n, byteOffset, byteStride, size);
            assert(buffer && byteStride > 0);
            rtcSetBuffer2   (rtcScene, _rtcGeomId, type, buffer, byteOffset, byteStride);
            rtcUpdateBuffer (rtcScene, _rtcGeomId, type);
        }
    rtcEnable   (rtcScene, _rtcGeomId);
    rtcSetMask  (rtcScene, _rtcGeomId, rayVisibility());

    return _rtcGeomId;
}

bool
rndr::TriangleMesh::getWorldBounds(math::Vector4f& minBounds,
                                    math::Vector4f& maxBounds) const
{
    return _interpolator->getWorldBounds(minBounds, maxBounds);
}

bool
rndr::TriangleMesh::perSubframeParameter(unsigned int parmId) const
{
    return parmId == parm::currentSampleIdx().id() ||
        parmId == parm::worldTransform().id() ||
        parmId == parm::rtVertices()    .id() ||
        parmId == parm::rtPositions()   .id() ||
        parmId == parm::rtNormals()     .id() ||
        parmId == parm::rtTangentsX()   .id() ||
        parmId == parm::rtTangentsY()   .id() ||
        parmId == parm::boundingBox()   .id();
}

void
rndr::TriangleMesh::beginSubframeCommits(size_t numSubframes)
{
    _interpolator->setNumSubframes(numSubframes);
}

void
rndr::TriangleMesh::commitSubframeState(size_t                 subframe,
                                        const parm::Container& parms)
{
    int                 currentSampleIdx;
    math::Affine3f      worldTransform;
    xchg::DataStream    rtPositions;
    xchg::DataStream    rtNormals;
    xchg::DataStream    rtTangentsX;
    xchg::DataStream    rtTangentsY;
    xchg::BoundingBox   localBounds;

    getBuiltIn<int>                 (parm::currentSampleIdx(),  currentSampleIdx,   &parms);
    getBuiltIn<math::Affine3f>      (parm::worldTransform(),    worldTransform,     &parms);
    getBuiltIn<xchg::DataStream>    (parm::rtPositions(),       rtPositions,        &parms);
    getBuiltIn<xchg::DataStream>    (parm::rtNormals(),         rtNormals,          &parms);
    getBuiltIn<xchg::DataStream>    (parm::rtTangentsX(),       rtTangentsX,        &parms);
    getBuiltIn<xchg::DataStream>    (parm::rtTangentsY(),       rtTangentsY,        &parms);
    getBuiltIn<xchg::BoundingBox>   (parm::boundingBox(),       localBounds,        &parms);

    _interpolator->setSubframeData(
        subframe, currentSampleIdx, worldTransform,
        rtPositions, rtNormals, rtTangentsX, rtTangentsY,
        localBounds);
}

void
rndr::TriangleMesh::endSubframeCommits()
{ }

void
rndr::TriangleMesh::commitFrameState(const parm::Container& parms,
                                     const xchg::IdSet&     dirties)
{
    Shape::commitFrameState(parms, dirties);
    //---
    xchg::Data::Ptr     parmData;
    xchg::DataStream    parmStream;

    for (xchg::IdSet::const_iterator id = dirties.begin(); id != dirties.end(); ++id)
    {
        if (*id == parm::rtIndices().id())
        {
            getBuiltIn<xchg::Data::Ptr>(parm::rtIndices(), parmData, &parms);
            _interpolator->setIndices(parmData);
        }
        else if (*id == parm::rtTexcoords().id())
        {
            getBuiltIn<xchg::DataStream>(parm::rtTexcoords(), parmStream, &parms);
            _interpolator->setTexCoords(parmStream);
        }
    }
    //---
    _interpolator->precomputeTables();
}
