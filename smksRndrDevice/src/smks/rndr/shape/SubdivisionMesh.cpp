// ========================================================================= //
// Modification copyright 2016-2018 SUPAMONKS_STUDIO                         //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
// _________________________________________________________________________ //
// Copyright 2009-2013 Intel Corporation                                     //
//                                                                           //
// Licensed under the Apache License, Version 2.0 (the "License");           //
// you may not use this file except in compliance with the License.          //
// You may obtain a copy of the License at                                   //
//                                                                           //
//     http://www.apache.org/licenses/LICENSE-2.0                            //
//                                                                           //
// Unless required by applicable law or agreed to in writing, software       //
// distributed under the License is distributed on an "AS IS" BASIS,         //
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  //
// See the License for the specific language governing permissions and       //
// limitations under the License.                                            //
//                                                                           //
// ========================================================================= //
#include <cassert>

#include <smks/sys/Log.hpp>
#include <smks/xchg/Data.hpp>
#include <smks/xchg/DataStream.hpp>
#include <smks/parm/BuiltIns.hpp>
#include <smks/parm/BuiltIn.hpp>

#include "SubdivisionMesh.hpp"
#include "SubdivisionMesh-VertexInterpolator.hpp"

using namespace smks;

rndr::SubdivisionMesh::SubdivisionMesh(unsigned int scnId, const CtorArgs& args):
    Shape           (scnId, args),
    _interpolator   (new VertexInterpolator()),
    _curGeomFlags   (-1)
{
    dispose();
}

rndr::SubdivisionMesh::~SubdivisionMesh()
{
    dispose();
    delete _interpolator;
}

void
rndr::SubdivisionMesh::dispose()
{
    _interpolator->dispose();
    _curGeomFlags = -1;
    //---
    Shape::dispose();
}

void
rndr::SubdivisionMesh::postIntersect(RTCScene rtcScene, const Ray& ray, DifferentialGeometry& dg) const
{
    _interpolator->postIntersect(rtcScene, ray, dg);
}

void
rndr::SubdivisionMesh::sample(RTCScene                  rtcScene,
                               int                      geomId,
                               const math::Vector2f&    s,
                               float                    time,
                               math::Vector4f&          p,
                               math::Vector4f&          n,
                               float&                   pdf) const
{
    _interpolator->sample(rtcScene, geomId, s, time, p, n, pdf);
}

float
rndr::SubdivisionMesh::pdf(float time) const
{
    return _interpolator->pdf(time);
}

unsigned    // returns RTC geometry index
rndr::SubdivisionMesh::extract(RTCScene rtcScene)
{
    RtcGeomUpdates updates;
    _interpolator->getAndFlushUpdates(updates);
    //---
    if (updates.getRebuildGeometry())
        Shape::deleteGeometry(rtcScene);

    // do not perform useless updates if invisible or not initialized
    if (globalInitialization() != xchg::ACTIVE)
    {
        FLIRT_LOG(LOG(DEBUG) << "subdivision mesh (shape ID = " << scnId() << ") not initialized.";)
        return _rtcGeomId;
    }
    if (!globallyVisible())
    {
        if (_rtcGeomId != RTC_INVALID_GEOMETRY_ID)
        {
            FLIRT_LOG(LOG(DEBUG) << "subdivision mesh (shape ID = " << scnId() << ", rtc ID = " << _rtcGeomId << ") disabled.";)
            rtcDisable(rtcScene, _rtcGeomId);
        }
        return _rtcGeomId;
    }
    // abandon geometry creation if mandatory buffers are empty
    if (_interpolator->numSubframes() == 0 ||
        _interpolator->numVertices() == 0 ||
        _interpolator->numFaces() == 0 ||
        _interpolator->numEdges() == 0)
        return _rtcGeomId;

    if (_rtcGeomId == RTC_INVALID_GEOMETRY_ID||
        _curGeomFlags != getRtcGeomFlags())
    {
        // (re)build primitive
        const size_t numEdgeCreases     = 0;
        const size_t numVertexCreases   = 0;
        const size_t numHoles           = 0;

        _rtcGeomId = rtcNewSubdivisionMesh(
            rtcScene,
            getRtcGeomFlags(),
            _interpolator->numFaces(),
            _interpolator->numEdges(),
            _interpolator->numVertices(),
            numEdgeCreases, numVertexCreases, numHoles,
            _interpolator->numSubframes());
        _curGeomFlags = getRtcGeomFlags();

        FLIRT_LOG(LOG(DEBUG)
            << "rtcNewSubdivisionMesh (shape ID = " << scnId() << ") -> rtc ID = " << _rtcGeomId << ":"
            << "\n\t- geom flags = " << getRtcGeomFlags()
            << "\n\t- # faces = " << _interpolator->numFaces()
            << "\n\t- # edges = " << _interpolator->numEdges()
            << "\n\t- # vertices = " << _interpolator->numVertices()
            << "\n\t- # edge creases = " << numEdgeCreases
            << "\n\t- # vertex creases = " << numVertexCreases
            << "\n\t- # holes = " << numHoles
            << "\n\t- # subframes = " << _interpolator->numSubframes();)
    }
    assert(_rtcGeomId >= 0);

    const void* buffer      = nullptr;
    size_t      byteOffset  = 0;
    size_t      byteStride  = 0;
    size_t      size        = 0;

    if (updates.getReuploadFaceBuffer())
    {
        buffer = _interpolator->getFaceBuffer(byteOffset, byteStride, size);
        assert(buffer && byteStride > 0);
        rtcSetBuffer2   (rtcScene, _rtcGeomId, RTC_FACE_BUFFER, buffer, byteOffset, byteStride);
        rtcUpdateBuffer (rtcScene, _rtcGeomId, RTC_FACE_BUFFER);
    }
    if (updates.getReuploadIndexBuffer())
    {
        buffer = _interpolator->getIndexBuffer(byteOffset, byteStride, size);
        assert(buffer && byteStride > 0);
        rtcSetBuffer2   (rtcScene, _rtcGeomId, RTC_INDEX_BUFFER, buffer, byteOffset, byteStride);
        rtcUpdateBuffer (rtcScene, _rtcGeomId, RTC_INDEX_BUFFER);
    }
    if (updates.getReuploadLevelBuffer())
    {
        // RTC_LEVEL_BUFFER: tessellation level for each or the edges of each face, making a total of numEdges values
        const float edgeLevel   = _interpolator->getEdgeLevel();
        float*      levelBuffer = static_cast<float*>(
            rtcMapBuffer(rtcScene, _rtcGeomId, RTC_LEVEL_BUFFER));

        for (size_t i = 0; i < _interpolator->numEdges(); ++i)
            levelBuffer[i] = edgeLevel;
        rtcUnmapBuffer(rtcScene, _rtcGeomId, RTC_LEVEL_BUFFER);
    }
    for (size_t n = 0; n < _interpolator->numSubframes(); ++n)
        if (updates.getReuploadVertexBuffer(n))
        {
            RTCBufferType type  = static_cast<RTCBufferType>(RTC_VERTEX_BUFFER0+n);
            buffer              = _interpolator->getVertexBuffer(n, byteOffset, byteStride, size);
            assert(buffer && byteStride > 0);
            rtcSetBuffer2   (rtcScene, _rtcGeomId, type, buffer, byteOffset, byteStride);
            rtcUpdateBuffer (rtcScene, _rtcGeomId, type);
        }
    rtcEnable   (rtcScene, _rtcGeomId);
    rtcSetMask  (rtcScene, _rtcGeomId, rayVisibility());

    return _rtcGeomId;

    //if (_updates & UpdateFlags::UPDATE_FACE_BUFFER)
    //{
    //  unsigned int* buffer = const_cast<unsigned int*>(_rtCounts->getPtr<unsigned int>());    // buffer shared with the scene data
    //  rtcSetBuffer    (rtcScene, _rtcGeomId, RTC_FACE_BUFFER, buffer, 0, sizeof(unsigned int));
    //  rtcUpdateBuffer (rtcScene, _rtcGeomId, RTC_FACE_BUFFER);
    //}

    //if (_updates & UpdateFlags::UPDATE_INDEX_BUFFER)
    //{
    //  unsigned int* buffer = const_cast<unsigned int*>(_rtIndices->getPtr<unsigned int>());   // buffer shared with the scene data
    //  rtcSetBuffer    (rtcScene, _rtcGeomId, RTC_INDEX_BUFFER, buffer, 0, sizeof(unsigned int));
    //  rtcUpdateBuffer (rtcScene, _rtcGeomId, RTC_INDEX_BUFFER);
    //}

    //if (_updates & UpdateFlags::UPDATE_VERTEX_BUFFER)
    //{
    //  _vertexStride   = 0;
    //  _offsetPosition = INVALID_OFFSET;
    //  _offsetNormal   = INVALID_OFFSET;

    //  if (_rtPositions)
    //  {
    //      _offsetPosition = _vertexStride;
    //      ++_vertexStride;
    //  }
    //  if (_rtNormals && _rtNormals.numElements() == _rtPositions.numElements())
    //  {
    //      _offsetNormal = _vertexStride;
    //      ++_vertexStride;
    //  }

    //  // compute world-space positions
    //  _bufVertices.resize(_vertexStride * _rtPositions.numElements());
    //  {
    //      //math::Vector4f p; // FIX ME
    //      //for (size_t i = 0, j = _offsetPosition; i < _rtPositions.numElements(); ++i, j += _vertexStride)
    //      //{
    //      //  memcpy(p.data(), _rtPositions.getPtr<float>(i), sizeof(float) * 3);
    //      //  p.w() = 1.0f;

    //      //  math::transformPoint(_worldTransform.data(), p.data(), _bufVertices[j].data());
    //      //}
    //  }
    //  // compute world-space normals
    //  if (_offsetNormal != INVALID_OFFSET)
    //  {
    //      //const math::Affine3f& normalMat = math::normalMatrix(_worldTransform);
    //      //math::Vector4f    n;
    //      //for (size_t i = 0, j = _offsetNormal; i < _rtNormals.numElements(); ++i, j += _vertexStride)
    //      //{
    //      //  memcpy(n.data(), _rtNormals.getPtr<float>(i), sizeof(float) * 3);
    //      //  n.w() = 0.0f;

    //      //  math::transformVector(normalMat.data(), n.data(), _bufVertices[j].data());
    //      //  _bufVertices[j] = math::normalize3(_bufVertices[j]);
    //      //}
    //  }

    //  assert(!_bufVertices.empty());
    //  rtcSetBuffer    (rtcScene, _rtcGeomId, RTC_VERTEX_BUFFER, &_bufVertices.front(), 0, _vertexStride * sizeof(math::Vector4f));
    //  rtcUpdateBuffer (rtcScene, _rtcGeomId, RTC_VERTEX_BUFFER);
    //}

    //if (_updates & UpdateFlags::UPDATE_TEXCOORDS_BUFFER)
    //{
    //  if (_rtTexcoords && _rtTexcoords.numElements() > 0)
    //  {
    //      assert(_rtFaceVertices && !_rtFaceVertices->empty());

    //      _bufTexcoords.resize(_rtTexcoords.numElements());
    //      memset(&_bufTexcoords.front(), 0, sizeof(math::Vector4f) * _rtTexcoords.numElements());
    //      for (size_t i = 0; i < _rtTexcoords.numElements(); ++i)
    //          memcpy(_bufTexcoords[i].data(), _rtTexcoords.getPtr<float>(i), sizeof(float) << 1);
    //  }
    //  else
    //      _bufTexcoords.clear();
    //}

    //if (_updates & UpdateFlags::UPDATE_LEVEL_BUFFER)
    //{
    //  // RTC_LEVEL_BUFFER: tessellation level for each or the edges of each face, making a total of numEdges values
    //  const float edgeLevel   = static_cast<float>(1 << std::max(static_cast<int>(_subdivisionLevel), 1));
    //  float*      levelBuffer = static_cast<float*>(rtcMapBuffer(rtcScene, _rtcGeomId, RTC_LEVEL_BUFFER));

    //  for (size_t i = 0; i < _rtIndices->size(); ++i)
    //      levelBuffer[i] = edgeLevel;
    //  rtcUnmapBuffer(rtcScene, _rtcGeomId, RTC_LEVEL_BUFFER);
    //}

    //const bool facevaryingInterpolate = !_bufTexcoords.empty();
    //if (facevaryingInterpolate &&
    //  _1stFaceVertex.empty())
    //{
    //  // precompute face first vertex ID mapping for face-varying interpolation
    //  _1stFaceVertex.resize(_rtCounts->size());
    //  unsigned int offset = 0;
    //  for (size_t f = 0; f < _rtCounts->size(); ++f)
    //  {
    //      _1stFaceVertex[f]   = offset;
    //      offset              += _rtCounts->get<unsigned int>(f);
    //  }
    //  if (offset != _rtIndices->size())
    //      throw sys::Exception("Face indexing issue with subdivision surface ID = " + std::to_string(scnId()) + "'.", "Subdivision Surface Extraction");
    //}

    ////if (math::anyGreaterThan(_worldBoundsMin, _worldBoundsMax))
    ////{
    ////    if (_localBounds.valid())
    ////    {
    ////        xchg::BoundingBox worldBounds;

    ////        math::transformBounds(_worldTransform, _localBounds, worldBounds);
    ////        memcpy(_worldBoundsMin.data(), &worldBounds.minX, sizeof(float) * 3);
    ////        memcpy(_worldBoundsMax.data(), &worldBounds.maxX, sizeof(float) * 3);
    ////    }
    ////    else if (!_bufVertices.empty())
    ////    {
    ////        size_t idx = _offsetPosition;
    ////        _worldBoundsMin = _bufVertices[idx];
    ////        _worldBoundsMax = _bufVertices[idx];
    ////        idx += _vertexStride;
    ////        for (; idx < _bufVertices.size(); idx += _vertexStride)
    ////        {
    ////            _worldBoundsMin = _worldBoundsMin.cwiseMin(_bufVertices[idx]);
    ////            _worldBoundsMax = _worldBoundsMax.cwiseMax(_bufVertices[idx]);
    ////        }
    ////    }
    ////    _worldBoundsMin.w() = 1.0f;
    ////    _worldBoundsMax.w() = 1.0f;

    ////    FLIRT_LOG(LOG(DEBUG) << "worldspace bounding box (subdivmesh ID = " << scnId() << ") = "
    ////        << "( " << _worldBoundsMin.transpose() << " ) -> "
    ////        << "( " << _worldBoundsMax.transpose() << " )"; )
    ////}

    //if (_areaDistribution.empty())
    //  updateAreaDistribution();

    //assert(!_bufVertices.empty());
    //
    //rtcEnable (rtcScene, _rtcGeomId);
    //rtcSetMask    (rtcScene, _rtcGeomId, _rayVisibility);

    //_updates = UpdateFlags::NO_UPDATE;

    //return _rtcGeomId;
}

bool
rndr::SubdivisionMesh::getWorldBounds(math::Vector4f&   minBounds,
                                       math::Vector4f&  maxBounds) const
{
    return _interpolator->getWorldBounds(minBounds, maxBounds);
}

bool
rndr::SubdivisionMesh::perSubframeParameter(unsigned int parmId) const
{
    return parmId == parm::currentSampleIdx().id() ||
        parmId == parm::worldTransform().id() ||
        parmId == parm::rtVertices()    .id() ||
        parmId == parm::rtPositions()   .id() ||
        parmId == parm::rtNormals()     .id() ||
        parmId == parm::boundingBox()   .id();
}

void
rndr::SubdivisionMesh::beginSubframeCommits(size_t numSubframes)
{
    _interpolator->setNumSubframes(numSubframes);
}

void
rndr::SubdivisionMesh::commitSubframeState(size_t                   subframe,
                                            const parm::Container&  parms)
{
    int                 currentSampleIdx;
    math::Affine3f      worldTransform;
    xchg::DataStream    rtPositions;
    xchg::DataStream    rtNormals;
    xchg::BoundingBox   localBounds;

    getBuiltIn<int>                 (parm::currentSampleIdx(),  currentSampleIdx,   &parms);
    getBuiltIn<math::Affine3f>      (parm::worldTransform(),    worldTransform,     &parms);
    getBuiltIn<xchg::DataStream>    (parm::rtPositions(),       rtPositions,        &parms);
    getBuiltIn<xchg::DataStream>    (parm::rtNormals(),         rtNormals,          &parms);
    getBuiltIn<xchg::BoundingBox>   (parm::boundingBox(),       localBounds,        &parms);

    _interpolator->setSubframeData(
        subframe, currentSampleIdx, worldTransform,
        rtPositions, rtNormals,
        localBounds);
}

void
rndr::SubdivisionMesh::endSubframeCommits()
{ }

void
rndr::SubdivisionMesh::commitFrameState(const parm::Container&  parms,
                                         const xchg::IdSet&     dirties)
{
    Shape::commitFrameState(parms, dirties);
    //---
    xchg::Data::Ptr     parmData;
    xchg::DataStream    parmStream;

    for (xchg::IdSet::const_iterator id = dirties.begin(); id != dirties.end(); ++id)
    {
        if (*id == parm::rtCounts().id())
        {
            getBuiltIn<xchg::Data::Ptr>(parm::rtCounts(), parmData, &parms);
            _interpolator->setCounts(parmData);
        }
        else if (*id == parm::rtIndices().id())
        {
            getBuiltIn<xchg::Data::Ptr>(parm::rtIndices(), parmData, &parms);
            _interpolator->setIndices(parmData);
        }
        else if (*id == parm::rtTexcoords().id())
        {
            getBuiltIn<xchg::DataStream>(parm::rtTexcoords(), parmStream, &parms);
            _interpolator->setTexCoords(parmStream);
        }
    }
    //---
    _interpolator->precomputeTables();
}

//void
//rndr::SubdivisionMesh::updateSubdivisionLevel(unsigned int value)
//{
//  if (_subdivisionLevel != value)
//      _updates = _updates | UpdateFlags::UPDATE_LEVEL_BUFFER;
//  _subdivisionLevel = value;
//}
//
//void
//rndr::SubdivisionMesh::updateRtIndices(DataPtr const& value)
//{
//  if (_rtIndices) // compare with previous value
//  {
//      if (value &&
//          value->size() != _rtIndices->size())
//          _updates = _updates | UpdateFlags::REBUILD_PIMITIVE;
//  }
//  _updates    = _updates | UpdateFlags::UPDATE_INDEX_BUFFER | UpdateFlags::UPDATE_LEVEL_BUFFER;
//  _rtIndices  = value;
//}
//
//void
//rndr::SubdivisionMesh::updateRtCounts(DataPtr const& value)
//{
//  if (_rtCounts)
//  {
//      if (value &&
//          value->size() != _rtCounts->size())
//          _updates = _updates | UpdateFlags::REBUILD_PIMITIVE;
//  }
//  _updates    = _updates | UpdateFlags::UPDATE_FACE_BUFFER;
//  _rtCounts   = value;
//  _1stFaceVertex.clear(); // will be computed during extraction only if face-varying data is required
//}
//
//void
//rndr::SubdivisionMesh::updateRtVertices(DataPtr const& value)
//{
//  _updates    = _updates | UpdateFlags::UPDATE_VERTEX_BUFFER;
//  _rtVertices = value;
//  //dirtyWorldBounds();
//}
//
//void
//rndr::SubdivisionMesh::updateRtFaceVertices(DataPtr const& value)
//{
//  _updates        = _updates | UpdateFlags::UPDATE_TEXCOORDS_BUFFER;
//  _rtFaceVertices = value;
//}
//
//void
//rndr::SubdivisionMesh::updateRtPositions(const xchg::DataStream& value)
//{
//  if (_rtPositions)
//  {
//      if (value &&
//          value.numElements() != _rtPositions.numElements())
//          _updates = _updates | UpdateFlags::REBUILD_PIMITIVE;
//  }
//  _updates        = _updates | UpdateFlags::UPDATE_VERTEX_BUFFER;
//  _rtPositions    = value;
//}
//
//void
//rndr::SubdivisionMesh::updateRtNormals(const xchg::DataStream& value)
//{
//  if (_rtNormals)
//  {
//      if (value &&
//          value.numElements() != _rtNormals.numElements())
//          _updates = _updates | UpdateFlags::REBUILD_PIMITIVE;
//  }
//  _updates    = _updates | UpdateFlags::UPDATE_VERTEX_BUFFER;
//  _rtNormals  = value;
//}
//
//void
//rndr::SubdivisionMesh::updateRtTexcoords(const xchg::DataStream& value)
//{
//  _updates        = _updates | UpdateFlags::UPDATE_TEXCOORDS_BUFFER;
//  _rtTexcoords    = value;
//}
//
//void
//rndr::SubdivisionMesh::updateAreaDistribution()
//{
//  dirtyAreaDistribution();
//
//  if (_bufVertices.empty()    ||
//      !_rtCounts  || _rtCounts->empty() ||
//      !_rtIndices || _rtIndices->empty() )
//      return;
//
//  const math::Vector4f* pos       = &_bufVertices.front() + _offsetPosition;
//  const unsigned int* counts      = _rtCounts->getPtr<unsigned int>();
//  const unsigned int* indices     = _rtIndices->getPtr<unsigned int>();
//
//  const size_t    numPrims        = _rtCounts->size();
//  float*          primWeights     = new float[numPrims];
//  float           totalArea       = 0.0f;
//  size_t          face1stVertex   = 0;
//
//  for (size_t f = 0; f < numPrims; ++f)
//  {
//      // compute the area of the polygonal primitive
//      float area = 0.0f;
//
//      const unsigned int  count   = _rtCounts->get<unsigned int>(f);
//      const unsigned int* face    = &indices[face1stVertex];
//      assert(count > 2);
//      const math::Vector4f& P0    = pos[face[0] * _vertexStride];
//      for (size_t i = 2; i < counts[f]; ++i)
//      {
//          const math::Vector4f& Pim1  = pos[face[i-1] * _vertexStride];
//          const math::Vector4f& Pi    = pos[face[i] * _vertexStride];
//
//          area += (P0 - Pim1).cross3(Pi - P0).norm(); // must then be divided by 2
//      }
//
//      primWeights[f]  = area;
//      totalArea       += area;
//      face1stVertex   += count;
//  }
//  totalArea *= 0.5f;
//
//  if (totalArea > 0.0f)
//  {
//      _areaDistribution.initialize(primWeights, numPrims);
//      _rTotalArea = math::rcp(totalArea);
//
//      FLIRT_LOG(LOG(DEBUG) << "area distribution updated for subdivision surface ID = " << scnId() << "\t total area = " << totalArea;)
//  }
//  delete[] primWeights;
//}
