// ========================================================================= //
// Modification copyright 2016-2018 SUPAMONKS_STUDIO                         //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
// _________________________________________________________________________ //
// Copyright 2009-2013 Intel Corporation                                     //
//                                                                           //
// Licensed under the Apache License, Version 2.0 (the "License");           //
// you may not use this file except in compliance with the License.          //
// You may obtain a copy of the License at                                   //
//                                                                           //
//     http://www.apache.org/licenses/LICENSE-2.0                            //
//                                                                           //
// Unless required by applicable law or agreed to in writing, software       //
// distributed under the License is distributed on an "AS IS" BASIS,         //
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  //
// See the License for the specific language governing permissions and       //
// limitations under the License.                                            //
//                                                                           //
// ========================================================================= //
#pragma once

#include "Shape.hpp"

namespace smks
{
    namespace xchg
    {
        class Data;
    }

    namespace rndr
    {
        class SubdivisionMesh:
            public Shape
        {
        private:
            class VertexInterpolator;

            VALID_RTOBJECT
        public:
            typedef sys::Ref<SubdivisionMesh>   Ptr;
        protected:
            typedef std::shared_ptr<xchg::Data> DataPtr;
        private:
            enum { INVALID_OFFSET = -1 };

        private:
            VertexInterpolator* _interpolator;
            char                _curGeomFlags;

            //// up-to-date node parameters
            //unsigned int              _subdivisionLevel;
            //DataPtr                       _rtIndices;
            //DataPtr                       _rtCounts;
            //std::vector<unsigned int> _1stFaceVertex;     //<! ID of each face's first vertex (useful when performing face interpolation, depends on rtCounts)
            //DataPtr                       _rtFaceVertices;
            //DataPtr                       _rtVertices;
            //xchg::DataStream          _rtPositions;
            //xchg::DataStream          _rtNormals;
            //xchg::DataStream          _rtTexcoords;
            //// local data buffers (most of them in world space)
            //Distribution1D                _areaDistribution;  //<! indexed by primitive index
            //float                     _rTotalArea;        //<! reciprocal of total surface area
            //math::Vector4fv               _bufVertices;       //<! shared with embree
            //size_t                        _vertexStride;
            //size_t                        _offsetPosition;
            //size_t                        _offsetNormal;
            //math::Vector4fv               _bufTexcoords;

            CREATE_RTOBJECT(SubdivisionMesh, Shape)
        protected:
            SubdivisionMesh(unsigned int scnId, const CtorArgs&);
        public:
            ~SubdivisionMesh();

            unsigned    // returns RTC geometry index
            extract(RTCScene);
            bool
            getWorldBounds(math::Vector4f&, math::Vector4f&) const;

            void
            postIntersect(RTCScene, const Ray&, DifferentialGeometry&) const;

            void
            sample(RTCScene, int    geomId,
                   const math::Vector2f&,
                   float            time,
                   math::Vector4f&  p,
                   math::Vector4f&  n,
                   float&           pdf) const;

            float
            pdf(float) const;

            void
            dispose();

            bool
            perSubframeParameter(unsigned int) const;

            void
            beginSubframeCommits(size_t);
            void
            commitSubframeState(size_t, const parm::Container&);
            void
            endSubframeCommits();

            void
            commitFrameState(const parm::Container&, const xchg::IdSet&);

            __forceinline
            SubdivisionMesh*
            asSubdivisionMesh()
            {
                return this;
            }

            __forceinline
            const SubdivisionMesh*
            asSubdivisionMesh() const
            {
                return this;
            }
        private:
            //void updateSubdivisionLevel   (unsigned int);
            //void updateRtIndices      (DataPtr const&);
            //void updateRtCounts           (DataPtr const&);
            //void updateRtFaceVertices (DataPtr const&);
            //void updateRtVertices     (DataPtr const&);
            //void updateRtPositions        (const xchg::DataStream&);
            //void updateRtNormals      (const xchg::DataStream&);
            //void updateRtTexcoords        (const xchg::DataStream&);

            //void dirtyAreaDistribution();
            //void updateAreaDistribution();
        };
    }
}
