// ========================================================================= //
// Copyright 2016-2018 SUPAMONKS_STUDIO                                      //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
//                                                                           //
// ========================================================================= //
#pragma once

#include <smks/xchg/DataStream.hpp>

#include "../sampler/Distribution1d.hpp"
#include "TriangleMesh.hpp"
#include "RtcGeomUpdates.hpp"
#include "../SubframeInterpolatorBase.hpp"

namespace smks { namespace rndr
{
    class TriangleMesh::VertexInterpolator:
        public SubframeInterpolatorBase
    {
    private:
        typedef std::shared_ptr<math::Vector4fv>    GBufferPtr;
        typedef std::vector<GBufferPtr>             GBufferSequence;
    private:
        enum GeometricBuffer
        {
            GBUFFER_POSITION = 0,
            GBUFFER_NORMAL,
            GBUFFER_TANGENT_X,
            GBUFFER_TANGENT_Y,
            _NUM_GBUFFERS
        };

    private:
        // per subframe data
        std::vector<GBufferSequence>    _worldGBufferSeqs;  //<! [gbuffer][subframe] -> std::shared_ptr<data>
        math::Vector4fv                 _worldBoundsMinSeq; //<! [subframe] -> min position in world space
        math::Vector4fv                 _worldBoundsMaxSeq; //<! [subframe] -> max position in world space
        // per frame data
        DataPtr                         _rtIndices;
        xchg::LockedDataStream          _rtTexcoords;
        // precomputed tables
        std::vector<float>              _rTotalArea;
        std::vector<Distribution1D>     _areaDistribution;

        RtcGeomUpdates                  _updates;

    public:
        VertexInterpolator();

    private:
        void
        disposeSubframeData();
        void
        disposeFrameData();
        void
        disposePrecomputedTables();

        void
        resizeSubframeData(size_t);
        void
        resizePrecomputedTables(size_t);

        void
        precomputeTabEntry(size_t tabEntry, size_t subframe);
        void
        precomputeTabEntry(size_t tabEntry, size_t curSubframe, size_t nxtSubframe, float);

    public:
        void
        setSubframeData(size_t,
                        int currentSampleIdx,
                        const math::Affine3f&,
                        const xchg::DataStream& rtPositions,
                        const xchg::DataStream& rtNormals,
                        const xchg::DataStream& rtTangentsX,
                        const xchg::DataStream& rtTangentsY,
                        const xchg::BoundingBox&);
        void
        setIndices(xchg::Data::Ptr const&);
        void
        setTexCoords(const xchg::DataStream&);

        bool
        getWorldBounds(math::Vector4f&, math::Vector4f&) const;

    private:
        virtual inline
        bool
        isTabEntryValid(size_t i) const
        {
            assert(i < tabSize());
            return !(_rTotalArea[i] < 0.0f);
        }
        virtual inline
        void
        invalidateTabEntry(size_t i)
        {
            assert(i < tabSize());
            _rTotalArea[i] = -1.0f;
        }

        void
        computeAreaDistribution(const math::Vector4fv&  pos0,
                                const math::Vector4fv&  pos1,
                                float   w1,
                                Distribution1D&,
                                float&  rTotalArea) const;
        void
        computeAreaDistribution(const math::Vector4fv&,
                                Distribution1D&,
                                float&  rTotalArea) const;
    public:
        __forceinline
        void
        getAndFlushUpdates(RtcGeomUpdates& updates)
        {
            updates = _updates;
            _updates.clear();
        }

        __forceinline
        size_t
        numTriangles() const
        {
            return _rtIndices
                ? _rtIndices->size() / 3
                : 0;
        }
        __forceinline
        size_t
        numVertices() const
        {
            return has(GBUFFER_POSITION)
                ? _worldGBufferSeqs[GBUFFER_POSITION].front()->size()
                : 0;
        }

        const void*
        getVertexBuffer(size_t, size_t& byteOffset, size_t& byteStride, size_t& size) const;
        const void*
        getIndexBuffer(size_t& byteOffset, size_t& byteStride, size_t& size) const;

        void
        postIntersect(RTCScene, const Ray&, DifferentialGeometry&) const;

        void
        sample(RTCScene, int geomId, const math::Vector2f&, float time,
               math::Vector4f& p, math::Vector4f& n, float& pdf) const;

        __forceinline
        float   //<! nearest precomputed table entry
        pdf(float time) const
        {
            assert(areAllTabEntriesValid());
            return _rTotalArea[getTabEntryIndex(time)];
        }

    private:
        __forceinline
        bool
        has(GeometricBuffer gbuffer) const
        {
            const GBufferSequence& bufferSeq = _worldGBufferSeqs[gbuffer];
            return !bufferSeq.empty() && bufferSeq.front() && !bufferSeq.front()->empty();
        }
        __forceinline
        const unsigned int*
        getPrimitiveIndices(int primId) const
        {
            assert(_rtIndices);
            assert(primId >= 0 && primId < numTriangles());
            return _rtIndices->getPtr<unsigned int>() + 3 * primId;
        }

    private:
        static
        GBufferPtr
        createGBuffer()
        {
            GBufferPtr buffer(new math::Vector4fv());
            return buffer;
        }
    };
}
}
