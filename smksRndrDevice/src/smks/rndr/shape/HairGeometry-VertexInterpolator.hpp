// ========================================================================= //
// Copyright 2016-2018 SUPAMONKS_STUDIO                                      //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
//                                                                           //
// ========================================================================= //
#pragma once

#include <smks/xchg/DataStream.hpp>
#include <smks/math/types.hpp>

#include "HairGeometry.hpp"
#include "RtcGeomUpdates.hpp"
#include "../SubframeInterpolatorBase.hpp"

namespace smks { namespace rndr
{
    class HairGeometry::VertexInterpolator:
        public SubframeInterpolatorBase
    {
    private:
        typedef std::shared_ptr<math::Vector4fv>    GBufferPtr;
        typedef std::vector<GBufferPtr>             GBufferSequence;
    private:
        enum GeometricBuffer
        {
            GBUFFER_VERTEX = 0,
            GBUFFER_SCALP_NORMAL,
            _NUM_GBUFFERS
        };

    private:
        // per subframe data
        std::vector<GBufferSequence>    _worldGBufferSeqs;  //<! [gbuffer][subframe] -> std::shared_ptr<data>
        math::Vector4fv                 _worldBoundsMinSeq; //<! [subframe] -> min position in world space
        math::Vector4fv                 _worldBoundsMaxSeq; //<! [subframe] -> max position in world space
        // per frame data
        DataPtr                         _rtIndices;
        DataPtr                         _rtPathIds;
        xchg::LockedDataStream          _rtScalpTexcoords;

        RtcGeomUpdates                  _updates;

    public:
        VertexInterpolator();

    private:
        void
        disposeSubframeData();
        void
        disposeFrameData();
        inline
        void
        disposePrecomputedTables()
        { }

        void
        resizeSubframeData(size_t);
        inline
        void
        resizePrecomputedTables(size_t)
        { }

        inline
        void
        precomputeTabEntry(size_t tabEntry, size_t subframe)
        { }
        inline
        void
        precomputeTabEntry(size_t tabEntry, size_t curSubframe, size_t nxtSubframe, float)
        { }

    public:
        void
        setSubframeData(size_t,
                        int currentSampleIdx,
                        const math::Affine3f&,
                        const xchg::DataStream& rtPositions,
                        const xchg::DataStream& rtWidths,
                        const xchg::DataStream& rtNormals,
                        const xchg::BoundingBox&);
        void
        setIndices(xchg::Data::Ptr const&);
        void
        setPathIds(xchg::Data::Ptr const&);
        void
        setScalpTexCoords(const xchg::DataStream&);

        bool
        getWorldBounds(math::Vector4f&, math::Vector4f&) const;

    private:
        virtual inline
        bool
        isTabEntryValid(size_t i) const
        {
            return false;
        }
        virtual inline
        void
        invalidateTabEntry(size_t i)
        { }

    public:
        __forceinline
        void
        getAndFlushUpdates(RtcGeomUpdates& updates)
        {
            updates = _updates;
            _updates.clear();
        }

        __forceinline
        size_t
        numCurves() const
        {
            return _rtIndices
                ? _rtIndices->size()
                : 0;
        }
        __forceinline
        size_t
        numVertices() const
        {
            return has(GBUFFER_VERTEX)
                ? _worldGBufferSeqs[GBUFFER_VERTEX].front()->size()
                : 0;
        }

        const void*
        getVertexBuffer(size_t, size_t& byteOffset, size_t& byteStride, size_t& size) const;
        const void*
        getIndexBuffer(size_t& byteOffset, size_t& byteStride, size_t& size) const;

        void
        postIntersect(RTCScene, const Ray&, DifferentialGeometry&) const;

    private:
        __forceinline
        bool
        has(GeometricBuffer gbuffer) const
        {
            const GBufferSequence& bufferSeq = _worldGBufferSeqs[gbuffer];
            return !bufferSeq.empty() && bufferSeq.front() && !bufferSeq.front()->empty();
        }

    private:
        static
        GBufferPtr
        createGBuffer()
        {
            GBufferPtr buffer(new math::Vector4fv());
            return buffer;
        }
        static
        void
        updateWidths(const math::Affine3f&, const xchg::DataStream&, math::Vector4fv&);
    };
}
}
