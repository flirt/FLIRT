// ========================================================================= //
// Copyright 2016-2018 SUPAMONKS_STUDIO                                      //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
//                                                                           //
// ========================================================================= //
#include <cassert>

#include <smks/sys/Log.hpp>
#include <smks/math/matrixAlgo.hpp>
#include <smks/math/bounds.hpp>

#include "../sampler/ShapeSampler.hpp"
#include "DifferentialGeometry.hpp"
#include "TriangleMesh-VertexInterpolator.hpp"

using namespace smks;

rndr::TriangleMesh::VertexInterpolator::VertexInterpolator():
    SubframeInterpolatorBase(),
    _worldGBufferSeqs   (_NUM_GBUFFERS),
    _worldBoundsMinSeq  (),
    _worldBoundsMaxSeq  (),
    _rtIndices          (nullptr),
    _rtTexcoords        (),
    _rTotalArea         (),
    _areaDistribution   (),
    //-----------------------
    _updates            ()
{ }

void
rndr::TriangleMesh::VertexInterpolator::disposeSubframeData()
{
    for (size_t buf = 0; buf < _worldGBufferSeqs.size(); ++buf)
    {
        _worldGBufferSeqs[buf].clear();
        _worldGBufferSeqs[buf].shrink_to_fit();
    }
    _worldBoundsMinSeq.clear();
    _worldBoundsMinSeq.shrink_to_fit();
    _worldBoundsMaxSeq.clear();
    _worldBoundsMaxSeq.shrink_to_fit();
}

void
rndr::TriangleMesh::VertexInterpolator::disposeFrameData()
{
    _rtIndices = nullptr;
    _rtTexcoords.clear();
}

void
rndr::TriangleMesh::VertexInterpolator::disposePrecomputedTables()
{
    _rTotalArea         .clear();
    _rTotalArea         .shrink_to_fit();
    _areaDistribution   .clear();
    _areaDistribution   .shrink_to_fit();
}

void
rndr::TriangleMesh::VertexInterpolator::resizeSubframeData(size_t numSubframes)
{
    for (size_t buf = 0; buf < _worldGBufferSeqs.size(); ++buf)
        _worldGBufferSeqs[buf].resize(numSubframes, nullptr);
    _worldBoundsMinSeq.resize(numSubframes, math::Vector4f::Constant(FLT_MAX));
    _worldBoundsMaxSeq.resize(numSubframes, math::Vector4f::Constant(-FLT_MAX));
}

void
rndr::TriangleMesh::VertexInterpolator::resizePrecomputedTables(size_t tabSize)
{
    _rTotalArea         .resize(tabSize);
    _areaDistribution   .resize(tabSize);
}

void
rndr::TriangleMesh::VertexInterpolator::setSubframeData(
    size_t                      subframe,
    int                         currentSampleIdx,
    const math::Affine3f&       worldTransform,
    const xchg::DataStream&     rtPositions,
    const xchg::DataStream&     rtNormals,
    const xchg::DataStream&     rtTangentsX,
    const xchg::DataStream&     rtTangentsY,
    const xchg::BoundingBox&    localBounds)
{
    assert(subframe < numSubframes());
    FLIRT_LOG(LOG(DEBUG)
        << "triangle mesh vertex interpolation: "
        << subframe << " / " << numSubframes() << "\n"
        << "\t- current sample = " << currentSampleIdx << "\n"
        << "\t- positions = " << rtPositions << "\n"
        << "\t- normals   = " << rtNormals << "\n"
        << "\t- tangentsX = " << rtTangentsX << "\n"
        << "\t- tangentsY = " << rtTangentsY << "\n"
        << "\t- local bounds = " << localBounds << "\n"
        << "\t- world xfm = \n " << worldTransform.matrix();)

    //--------------------------------------------------------------------------------------
#define XFM_STREAM(_GBUFFER_, _STREAM_, _XFM_FUNC_)                                         \
    {                                                                                       \
        xchg::LockedDataStream locked;                                                      \
        if (_STREAM_.lock(locked))                                                          \
        {                                                                                   \
            _worldGBufferSeqs[_GBUFFER_][subframe] = createGBuffer();                       \
            _XFM_FUNC_(worldTransform, locked, *_worldGBufferSeqs[_GBUFFER_][subframe]);    \
        }                                                                                   \
        else                                                                                \
            _worldGBufferSeqs[_GBUFFER_][subframe] = nullptr;                               \
    }
    //--------------------------------------------------------------------------------------

    const size_t prvNumVertices = numVertices();

    XFM_STREAM(GBUFFER_POSITION,    rtPositions,    math::transformPoints)
    XFM_STREAM(GBUFFER_NORMAL,      rtNormals,      math::transformDirections)
    XFM_STREAM(GBUFFER_TANGENT_X,   rtTangentsX,    math::transformDirections)
    XFM_STREAM(GBUFFER_TANGENT_Y,   rtTangentsY,    math::transformDirections)

    // do this only upon position change
    bool positionsChanged = true;
    if (positionsChanged)
    {
        _updates.setReuploadVertexBuffer(static_cast<uint8_t>(subframe));

        if (localBounds.valid())
            math::transform(
                worldTransform,
                localBounds,
                _worldBoundsMinSeq[subframe],
                _worldBoundsMaxSeq[subframe]);
        else if (_worldGBufferSeqs[GBUFFER_POSITION][subframe])
        {
            const math::Vector4fv& worldPositions = *(_worldGBufferSeqs[GBUFFER_POSITION][subframe]);

            math::min(worldPositions, _worldBoundsMinSeq[subframe]);
            math::max(worldPositions, _worldBoundsMaxSeq[subframe]);
        }
        else
        {
            _worldBoundsMinSeq[subframe].setConstant(FLT_MAX);
            _worldBoundsMaxSeq[subframe].setConstant(-FLT_MAX);
        }

        int iStart, iEnd;
        getInvalidatedTabEntryRange(subframe, iStart, iEnd);
        for (int i = iStart; i <= iEnd; ++i)
            invalidateTabEntry(i);
    }
    if (numVertices() != prvNumVertices)
        _updates.setRebuildGeometry();
}

void
rndr::TriangleMesh::VertexInterpolator::setIndices(xchg::Data::Ptr const& value)
{
    if (value == _rtIndices)
        return;

    const size_t prvNumTriangles = numTriangles();

    _rtIndices = value;
    //---
    invalidateAllTabEntries();
    if (numTriangles() != prvNumTriangles)
        _updates.setRebuildGeometry();
    else
        _updates.setReuploadIndexBuffer();
}

void
rndr::TriangleMesh::VertexInterpolator::setTexCoords(const xchg::DataStream& value)
{
    value.lock(_rtTexcoords);
}

void
rndr::TriangleMesh::VertexInterpolator::precomputeTabEntry(
    size_t tabEntry,
    size_t subframe)
{
    GBufferSequence& worldPositionsSeq = _worldGBufferSeqs[GBUFFER_POSITION];

    if (!isTabEntryValid(tabEntry))
    {
        GBufferPtr const& worldPositions = worldPositionsSeq[subframe];
        if (worldPositions)
            computeAreaDistribution(
                *worldPositions,
                _areaDistribution[tabEntry],
                _rTotalArea[tabEntry]);
    }
}

void
rndr::TriangleMesh::VertexInterpolator::precomputeTabEntry(
    size_t  tabEntry,
    size_t  curSubframe,
    size_t  nxtSubframe,
    float   subframeBlend)
{
    GBufferSequence& worldPositionsSeq = _worldGBufferSeqs[GBUFFER_POSITION];

    if (!isTabEntryValid(tabEntry))
    {
        GBufferPtr const& worldPositions0 = worldPositionsSeq[curSubframe];
        GBufferPtr const& worldPositions1 = worldPositionsSeq[nxtSubframe];
        if (worldPositions0 &&
            worldPositions1)
            computeAreaDistribution(
                *worldPositions0,
                *worldPositions1,
                subframeBlend,
                _areaDistribution[tabEntry],
                _rTotalArea[tabEntry]);
    }
}

void
rndr::TriangleMesh::VertexInterpolator::computeAreaDistribution(
    const math::Vector4fv&  worldPositions0,
    const math::Vector4fv&  worldPositions1,
    float                   w1,
    Distribution1D&         areaDistribution,
    float&                  rTotalArea) const
{
    math::Vector4fv worldPositions;

    math::lerp(worldPositions0, worldPositions1, w1, worldPositions);
    computeAreaDistribution(worldPositions, areaDistribution, rTotalArea);
}

void
rndr::TriangleMesh::VertexInterpolator::computeAreaDistribution(
    const math::Vector4fv&  worldPositions,
    Distribution1D&         areaDistribution,
    float&                  rTotalArea) const
{
    areaDistribution.dispose();
    rTotalArea = 0.0f;

    if (worldPositions.empty() ||
        !_rtIndices)
        return;
    assert(_rtIndices->size() % 3 == 0);

    const size_t        numPrims    = _rtIndices->size() / 3;
    const unsigned int* tri         = _rtIndices->getPtr<unsigned int>();
    float*              primWeights = new float[numPrims];
    float               totalArea   = 0.0f;

    for (size_t f = 0; f < numPrims; ++f)
    {
        // compute the area of the polygonal primitive
        const math::Vector4f&   P0      = worldPositions[tri[0]];
        const math::Vector4f&   P1      = worldPositions[tri[1]];
        const math::Vector4f&   P2      = worldPositions[tri[2]];
        const float             area    = 0.5f * (P0 - P1).cross3(P2 - P0).norm();

        primWeights[f]  = area;
        totalArea       += area;
        tri             += 3;
    }
    if (totalArea > 1e-6f)
    {
        areaDistribution.initialize(primWeights, numPrims);
        rTotalArea = math::rcp(totalArea);
    }
    delete[] primWeights;
}

bool
rndr::TriangleMesh::VertexInterpolator::getWorldBounds(
    math::Vector4f& minBounds,
    math::Vector4f& maxBounds) const
{
    assert(numSubframes() > 0);
    math::min(_worldBoundsMinSeq, minBounds);
    math::max(_worldBoundsMaxSeq, maxBounds);

#if defined(FLIRT_LOG_ENABLED)
    {
        std::stringstream sstr;
        for (size_t n = 0; n < numSubframes(); ++n)
            sstr
                << "[" << n << "]\tbounds = "
                << "(" << _worldBoundsMinSeq[n].transpose() << ") -> "
                << "(" << _worldBoundsMaxSeq[n].transpose() << ")\n";
        sstr << "=>\t(" << minBounds.transpose() << ") -> (" << maxBounds.transpose() << ")";
        LOG(DEBUG) << sstr.str();
    }
#endif // defined(FLIRT_LOG_ENABLED)

    return !math::anyGreaterThan(minBounds, maxBounds);
}

const void*
rndr::TriangleMesh::VertexInterpolator::getVertexBuffer(
    size_t  subframe,
    size_t& byteOffset,
    size_t& byteStride,
    size_t& size) const
{
    assert(subframe < numSubframes());
    GBufferPtr const& buffer = _worldGBufferSeqs[GBUFFER_POSITION][subframe];

    byteOffset  = 0;
    byteStride  = sizeof(math::Vector4f);
    size        = buffer ? buffer->size() : 0;

    return buffer && !buffer->empty()
        ? reinterpret_cast<const void*>(&buffer->front())
        : nullptr;
}

const void*
rndr::TriangleMesh::VertexInterpolator::getIndexBuffer(
    size_t& byteOffset,
    size_t& byteStride,
    size_t& size) const
{
    byteOffset  = 0;
    byteStride  = 3 * sizeof(unsigned int);
    size        = _rtIndices ? _rtIndices->size() / 3 : 0;

    return _rtIndices
        ? reinterpret_cast<const void*>(_rtIndices->getPtr<unsigned int>())
        : nullptr;
}

void
rndr::TriangleMesh::VertexInterpolator::postIntersect(
    RTCScene,
    const Ray&              ray,
    DifferentialGeometry&   dg) const
{
    assert(areAllTabEntriesValid());

    // get subframe indices and weight for time interpolation
    size_t  curSubframe = 0;
    size_t  nxtSubframe = 0;
    float   blend       = 0.0f;

    if (numSubframes() > 1)
        getSubframes(ray.time, curSubframe, nxtSubframe, blend);

    dg.P          = ray.org + ray.tfar * ray.dir;
    dg.Ng         = math::normalized3(ray.Ng);
    dg.time       = ray.time;
    dg.hairRadius = -1.0f;
    dg.error      = math::max<float>(
        math::abs(ray.tfar),
        math::max_xyz(dg.P.cwiseAbs()));

    const unsigned int* tri = getPrimitiveIndices(ray.primID);
    math::Vector4fv     texcoords(3, math::Vector4f::Zero());

    // interpolate texture coordinates across primitive
    if (_rtTexcoords.numElements())
        for (size_t i = 0; i < 3; ++i)
            memcpy(texcoords[i].data(), _rtTexcoords.getPtr<float>(i), sizeof(float) << 1);
    else
    {
        // [0] = (0, 0) // [1] = (1, 0) // [2] = (0, 1)
        texcoords[1].x() = 1.0f;
        texcoords[2].y() = 1.0f;
    }
    const math::Vector4f& st = texcoords[0] +
        ray.u * (texcoords[1] - texcoords[0]) +
        ray.v * (texcoords[2] - texcoords[0]);
    dg.st.x() = st.x();
    dg.st.y() = st.y();

    // interpolate shading normal/x-tangent/y-tangent across primitive
    GeometricBuffer typ[]       = { GBUFFER_NORMAL, GBUFFER_TANGENT_X,  GBUFFER_TANGENT_X };
    math::Vector4f* vec[]       = { &dg.Ns,         &dg.Tx,             &dg.Ty };
    float           sqrLen[]    = { 0.0f,           0.0f,               0.0f };
#define SQR_LEN_NORMAL      sqrLen[0]
#define SQR_LEN_TANGENT_X   sqrLen[1]
#define SQR_LEN_TANGENT_Y   sqrLen[2]

    for (int k = 0; k < 3; ++k)
    {
        if (!has(typ[k]))
            continue;

        math::Vector4fv triVecs(3, math::Vector4f::Zero());
        if (curSubframe == nxtSubframe)
        {
            const math::Vector4fv& vecs = *_worldGBufferSeqs[typ[k]][curSubframe];
            for (size_t i = 0; i < 3; ++i)
                triVecs[i] = vecs[tri[i]];
        }
        else
        {
            const GBufferSequence& vecsSeq = _worldGBufferSeqs[typ[k]];
            const math::Vector4fv& curVecs = *vecsSeq[curSubframe];
            const math::Vector4fv& nxtVecs = *vecsSeq[nxtSubframe];
            for (size_t i = 0; i < 3; ++i)
            {
                const math::Vector4f& cur = curVecs[tri[i]];
                const math::Vector4f& nxt = nxtVecs[tri[i]];
                triVecs[i] = math::normalized3(cur + blend * (nxt - cur));
            }
        }
        math::Vector4f& vec_k       = *vec[k];
        float&          sqrLen_k    = sqrLen[k];

        vec_k = triVecs[0] +
            ray.u * (triVecs[1] - triVecs[0]) +
            ray.v * (triVecs[2] - triVecs[0]);
        sqrLen_k = math::dot3(vec_k, vec_k);    // squaredNorm();
        if (sqrLen_k > 0.0f)
            vec_k *= math::rsqrt(sqrLen_k);     // normalize vector
    }

    if (SQR_LEN_NORMAL < 1e-3f)
        dg.Ns = dg.Ng;
    else if (math::dot3(dg.Ns, dg.Ng) < 0.0f)
        dg.Ns = -dg.Ns;

    if (SQR_LEN_TANGENT_X < 1e-3f ||
        SQR_LEN_TANGENT_Y < 1e-3f)
    {
        math::Vector4fv pos(3, math::Vector4f::UnitW());
        if (curSubframe == nxtSubframe)
        {
            const math::Vector4fv& worldPositions = *_worldGBufferSeqs[GBUFFER_POSITION][curSubframe];
            for (size_t i = 0; i < 3; ++i)
                pos[i] = worldPositions[tri[i]];
        }
        else
        {
            const GBufferSequence& worldPositionsSeq = _worldGBufferSeqs[GBUFFER_POSITION];
            const math::Vector4fv& curWorldPositions = *worldPositionsSeq[curSubframe];
            const math::Vector4fv& nxtWorldPositions = *worldPositionsSeq[nxtSubframe];
            for (size_t i = 0; i < 3; ++i)
            {
                const math::Vector4f& cur = curWorldPositions[tri[i]];
                const math::Vector4f& nxt = nxtWorldPositions[tri[i]];
                pos[i] = cur + blend * (nxt - cur);
            }
        }
        const math::Vector4f& dPdu  = pos[1] - pos[0];
        const math::Vector4f& dPdv  = pos[2] - pos[0];
        const math::Vector4f& dstdu = texcoords[1] - texcoords[0];  // (dsdu, dtdu, 0, 0)
        const math::Vector4f& dstdv = texcoords[2] - texcoords[0];  // (dsdv, dtdv, 0, 0)

        if (SQR_LEN_TANGENT_X < 1e-3f)
        {
            math::Vector4f  dPds    = dPdu * dstdv.y() - dPdv * dstdu.y();
            const float     sqrLen  = math::dot3(dPds, dPds); //.squaredNorm();

            if (sqrLen > 1e-3f)
            {
                dPds *= math::rsqrt(sqrLen);
                dg.Tx = math::normalized3(dPds - math::dot3(dPds, dg.Ns) * dg.Ns);
            }
            else
                dg.Tx.setZero();
        }
        if (SQR_LEN_TANGENT_Y < 1e-3f)
        {
            math::Vector4f  dPdt    = dPdv * dstdv.x() - dPdu * dstdv.x();
            const float     sqrLen  = math::dot3(dPdt, dPdt); //.squaredNorm();

            if (sqrLen > 1e-3f)
            {
                dPdt *= math::rsqrt(sqrLen);
                dg.Ty = math::normalized3(dPdt - math::dot3(dPdt, dg.Ns) * dg.Ns);
            }
            else
                dg.Ty.setZero();
        }
    }
}

void
rndr::TriangleMesh::VertexInterpolator::sample(RTCScene,
                                               int,    // geomId
                                               const math::Vector2f&   s,
                                               float                   time,
                                               math::Vector4f&         p,
                                               math::Vector4f&         n,
                                               float&                  pdf) const
{
    assert(areAllTabEntriesValid());

    // get subframe indices and weight for time interpolation
    size_t  curSubframe = 0;
    size_t  nxtSubframe = 0;
    float   blend       = 0.0f;

    if (numSubframes() > 1)
        getSubframes(time, curSubframe, nxtSubframe, blend);

    // get precomputed table entry index
    const size_t tabIndex = getTabEntryIndex(time);

    // pick a primitive
    const Distribution1D&   primAreas   = _areaDistribution[tabIndex];
    const size_t            primId      = math::min<size_t>(
        static_cast<size_t>(primAreas.sample(s.y()).value),
        primAreas.size()-1);

    // sample the primitive for position and normal
    const unsigned int* tri = getPrimitiveIndices(primId);
    math::Vector4fv     pos (3, math::Vector4f::UnitW());
    math::Vector4fv     nrml(3, math::Vector4f::Zero());

    assert(has(GBUFFER_POSITION));
    const bool hasNormals = has(GBUFFER_NORMAL);
    if (curSubframe == nxtSubframe)
    {
        const math::Vector4fv& worldPositions = *_worldGBufferSeqs[GBUFFER_POSITION][curSubframe];
        for (size_t i = 0; i < 3; ++i)
            pos[i] = worldPositions[tri[i]];
        if (hasNormals)
        {
            const math::Vector4fv& worldNormals = *_worldGBufferSeqs[GBUFFER_NORMAL][curSubframe];
            for (size_t i = 0; i < 3; ++i)
                nrml[i] = worldNormals[tri[i]];
        }
    }
    else
    {
        // interpolate if motion blur required
        const GBufferSequence& worldPositionsSeq = _worldGBufferSeqs[GBUFFER_POSITION];
        const math::Vector4fv& curWorldPositions = *worldPositionsSeq[curSubframe];
        const math::Vector4fv& nxtWorldPositions = *worldPositionsSeq[nxtSubframe];
        for (size_t i = 0; i < 3; ++i)
        {
            const math::Vector4f& cur = curWorldPositions[tri[i]];
            const math::Vector4f& nxt = nxtWorldPositions[tri[i]];
            pos[i] = cur + blend * (nxt - cur);
        }
        if (hasNormals)
        {
            const GBufferSequence& worldNormalsSeq = _worldGBufferSeqs[GBUFFER_NORMAL];
            const math::Vector4fv& curWorldNormals = *worldNormalsSeq[curSubframe];
            const math::Vector4fv& nxtWorldNormals = *worldNormalsSeq[nxtSubframe];
            for (size_t i = 0; i < 3; ++i)
            {
                const math::Vector4f& cur = curWorldNormals[tri[i]];
                const math::Vector4f& nxt = nxtWorldNormals[tri[i]];
                nrml[i] = math::normalized3(cur + blend * (nxt - cur));
            }
        }
    }
    p   = uniformSampleTriangle(s.x(), s.y(), pos[0], pos[1], pos[2]);
    n   = hasNormals
        ? math::normalized3(uniformSampleTriangle(s.x(), s.y(), nrml[0], nrml[1], nrml[2]))
        : math::normalized3((pos[0] - pos[1]).cross3(pos[2] - pos[0]));
    pdf = _rTotalArea[tabIndex];
}
