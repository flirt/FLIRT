// ========================================================================= //
// Modification copyright 2016-2018 SUPAMONKS_STUDIO                         //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
// _________________________________________________________________________ //
// Copyright 2009-2013 Intel Corporation                                     //
//                                                                           //
// Licensed under the Apache License, Version 2.0 (the "License");           //
// you may not use this file except in compliance with the License.          //
// You may obtain a copy of the License at                                   //
//                                                                           //
//     http://www.apache.org/licenses/LICENSE-2.0                            //
//                                                                           //
// Unless required by applicable law or agreed to in writing, software       //
// distributed under the License is distributed on an "AS IS" BASIS,         //
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  //
// See the License for the specific language governing permissions and       //
// limitations under the License.                                            //
//                                                                           //
// ========================================================================= //
#pragma once

#include <embree2/rtcore.h>

#include <smks/sys/platform.hpp>
#include <smks/sys/constants.hpp>
#include <smks/sys/Exception.hpp>
#include <smks/math/types.hpp>

#include "RayType.hpp"

namespace smks { namespace rndr
{
    struct __align(16) Ray
    {
        /*******************************************************************************/
        //!< ray specification
        math::Vector4f  org;            //!< Ray origin
        math::Vector4f  dir;            //!< Ray direction
        float           tnear;          //!< Start of ray segment
        float           tfar;           //!< End of ray segment = ray length = hit distance
        float           time;           //!< Time of this ray for motion blur.
        int             mask;           //!< used to mask out objects during traversal
        //!< hit data
        math::Vector4f  Ng;             //!< Unnormalized geometry normal
        float           u;              //!< Barycentric u coordinate of hit
        float           v;              //!< Barycentric v coordinate of hit
        int             geomID;         //!< geometry ID
        int             primID;         //!< primitive ID
        int             instID;         //!< instance ID
        /*******************************************************************************/
        int             orgGeomID;      //<! ID of the geometry the ray is emitted from
        int             orgPrimID;      //<! ID of the primitive the ray is emitted from
        int             orgInstID;      //<! ID of the instance the ray is emitted from
        /*******************************************************************************/
        math::Vector4f  transparency;   //<! Transparency updated by shadow rays
    public:
        __forceinline
        Ray():
            /************************************/
            org             (math::Vector4f::Zero()),
            dir             (math::Vector4f::Zero()),
            tnear           (sys::zero),
            tfar            (sys::inf),
            time            (sys::zero),
            mask            (PRIMARY_RAY),
            Ng              (math::Vector4f::Zero()),
            u               (sys::zero),
            v               (sys::zero),
            geomID          (RTC_INVALID_GEOMETRY_ID),
            primID          (RTC_INVALID_GEOMETRY_ID),
            instID          (RTC_INVALID_GEOMETRY_ID),
            /*************************************/
            orgGeomID       (RTC_INVALID_GEOMETRY_ID), // must be left unchanged
            orgPrimID       (RTC_INVALID_GEOMETRY_ID), // must be left unchanged
            orgInstID       (RTC_INVALID_GEOMETRY_ID), // must be left unchanged
            /*************************************/
            transparency    (math::Vector4f::Ones())
        { }

        __forceinline
        Ray(int                     mask,
            const math::Vector4f&   org,
            const math::Vector4f&   dir,
            float                   tnear   = sys::zero,
            float                   tfar    = sys::inf):
            /************************************/
            org             (org),
            dir             (dir),
            tnear           (tnear),
            tfar            (tfar),
            time            (sys::zero),
            mask            (mask),
            Ng              (math::Vector4f::Zero()),
            u               (sys::zero),
            v               (sys::zero),
            geomID          (RTC_INVALID_GEOMETRY_ID),
            primID          (RTC_INVALID_GEOMETRY_ID),
            instID          (RTC_INVALID_GEOMETRY_ID),
            /************************************/
            orgGeomID       (RTC_INVALID_GEOMETRY_ID),
            orgPrimID       (RTC_INVALID_GEOMETRY_ID),
            orgInstID       (RTC_INVALID_GEOMETRY_ID),
            /*************************************/
            transparency    (math::Vector4f::Ones())
        {
            if (tnear > tfar)
                throw sys::Exception(
                    "Incorrect ray parameter bounds: tnear > tfar.",
                    "Ray Creation");
        }

        __forceinline
        Ray(int                     mask,
            const math::Vector4f&   org,
            const math::Vector4f&   dir,
            float                   tnear,
            float                   tfar,
            const Ray&              previousRay):
            /************************************/
            org             (org),
            dir             (dir),
            tnear           (tnear),
            tfar            (tfar),
            time            (previousRay.time),
            mask            (mask),
            Ng              (math::Vector4f::Zero()),
            u               (sys::zero),
            v               (sys::zero),
            geomID          (RTC_INVALID_GEOMETRY_ID),
            primID          (RTC_INVALID_GEOMETRY_ID),
            instID          (RTC_INVALID_GEOMETRY_ID),
            /************************************/
            orgGeomID       (previousRay.geomID),   // must be left unchanged
            orgPrimID       (previousRay.primID),   // must be left unchanged
            orgInstID       (previousRay.instID),   // must be left unchanged
            /*************************************/
            transparency    (math::Vector4f::Ones())
        {
            if (tnear > tfar)
                throw sys::Exception(
                    "Incorrect ray parameter bounds: tnear > tfar.",
                    "Ray Creation");
        }

        // did ray hit something?
        __forceinline
        operator bool() const
        {
            return geomID != RTC_INVALID_GEOMETRY_ID;
        }
    };

    inline
    std::ostream&
    operator<<(std::ostream& out, const Ray& ray)
    {
        return out
            << "( org = (" << ray.org.transpose() << "), dir = (" << ray.dir.transpose() << ")"
            << ", tnear = " << ray.tnear << ", tfar = " << ray.tfar
            << ", time = " << ray.time
            << ", geomID = " << ray.geomID << ", primID = " << ray.primID << ", instID = " << ray.instID
            <<  ", u = " << ray.u <<  ", v = " << ray.v << ", Ng = " << ray.Ng
            << ", orgGeomID = " << ray.orgGeomID << ", orgPrimID = " << ray.orgPrimID << ", orgInstID = " << ray.orgInstID
            << ", transparency = (" << ray.transparency.transpose() << ")"
            << " )";
    }
}
}
