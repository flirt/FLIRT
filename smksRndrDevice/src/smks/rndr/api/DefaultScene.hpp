// ========================================================================= //
// Modification copyright 2016-2018 SUPAMONKS_STUDIO                         //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
// _________________________________________________________________________ //
// Copyright 2009-2013 Intel Corporation                                     //
//                                                                           //
// Licensed under the Apache License, Version 2.0 (the "License");           //
// you may not use this file except in compliance with the License.          //
// You may obtain a copy of the License at                                   //
//                                                                           //
//     http://www.apache.org/licenses/LICENSE-2.0                            //
//                                                                           //
// Unless required by applicable law or agreed to in writing, software       //
// distributed under the License is distributed on an "AS IS" BASIS,         //
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  //
// See the License for the specific language governing permissions and       //
// limitations under the License.                                            //
//                                                                           //
// ========================================================================= //
#pragma once

#include <boost/unordered_map.hpp>
#include <embree2/rtcore.h>

#include <smks/math/matrixAlgo.hpp>

#include "Scene.hpp"
#include "Ray.hpp"
#include "Primitive.hpp"
#include "../../parm/Values.hpp"

namespace smks { namespace rndr
{
    class DefaultScene:
        public Scene
    {
        VALID_RTOBJECT
    private:
        typedef boost::unordered_map<size_t, Primitive*> PrimitiveMap;
        typedef std::vector<const Primitive*>            RTCIdToPrimitive;

    private:
        PrimitiveMap                         _primitives;        //<! does not own primitives (device does)
        RTCScene                             _rtcScene;
        RTCIdToPrimitive                     _rtcIdToPrimitive;
        std::vector<const Light*>            _allLights;         //<! does not own lights
        std::vector<const EnvironmentLight*> _environmentLights; //<! does not own environment lights
        math::Vector4f                       _worldBoundsMin;    //<! bounding box in world space
        math::Vector4f                       _worldBoundsMax;    //<! bounding box in world space
    CREATE_RTOBJECT(DefaultScene, Scene)
    protected:
        DefaultScene(unsigned int scnId, const CtorArgs&);
    public:
        void
        dispose();

        inline
        bool
        perSubframeParameter(unsigned int) const
        {
            return false;
        }

        inline
        void
        beginSubframeCommits(size_t)
        { }
        inline
        void
        commitSubframeState(size_t, const parm::Container&)
        { }
        inline
        void
        endSubframeCommits()
        { }

        void
        commitFrameState(const parm::Container&, const xchg::IdSet&);

        void
        add(Primitive*);

        void
        remove(Primitive*);

        void
        extractPrimitives(RTCDevice, const parm::Getters&);

        void
        postIntersect(const Ray& ray, DifferentialGeometry& dg) const
        {
            const Primitive* prim = getPrimitive(ray);
            if (prim)
                prim->postIntersect(_rtcScene, ray, dg);
        }

        __forceinline
        RTCScene
        rtcScene() const
        {
            return _rtcScene;
        }

        __forceinline
        size_t
        totalNumLights() const
        {
            return _allLights.size();
        }

        __forceinline
        const Light*
        light(size_t idx) const
        {
            assert(idx < _allLights.size());
            return _allLights[idx];
        }

        __forceinline
        size_t
        numEnvironmentLights() const
        {
            return _environmentLights.size();
        }

        __forceinline
        const EnvironmentLight*
        environmentLight(size_t idx) const
        {
            assert(idx < _environmentLights.size());
            return _environmentLights[idx];
        }

        __forceinline
        bool
        getWorldBounds(math::Vector4f& minBounds, math::Vector4f& maxBounds) const
        {
            minBounds = _worldBoundsMin;
            maxBounds = _worldBoundsMax;
            return !math::anyGreaterThan(minBounds, maxBounds);
        }

        __forceinline
        void
        handleIntersected(Ray& ray) const
        {
            bool                handled = false;
            const Primitive*    prim    = getPrimitive(ray);
            if (prim)
            {
                // filters are directly called by rtcIntersect (hence come 1st)
                assert(math::isVector3f(ray.dir));
                ray.Ng.w() = 0.0f;

                // filter function priority: material > subsurface > shape
                if (prim->material())
                    handled |= prim->material()->intersected(ray);
                if (prim->subSurface())
                    handled |= handled || prim->subSurface()->intersected(ray);
                if (prim->shape())
                    handled |= handled || prim->shape()->intersected(ray);
            }
        }

        __forceinline
        void
        handleOccluded(Ray& ray) const
        {
            bool                handled = false;
            const Primitive*    prim    = getPrimitive(ray);
            if (prim)
            {
                // filter function priority: material > subsurface > shape
                if (prim->material())
                {
                    if (prim->material()->isTransparentForShadowRays())
                        ray.geomID = RTC_INVALID_GEOMETRY_ID;
                    else
                        handled |= prim->material()->occluded(ray);
                }
                if (prim->subSurface())
                    handled |= prim->subSurface()->occluded(ray);
                if (prim->shape())
                    handled |= prim->shape()->occluded(ray);
            }
        }

        void
        getMetadata(img::OutputImage&) const;

    private:
        __forceinline
        const Primitive*
        getPrimitive(const Ray& ray) const
        {
            if (!ray)
                return nullptr;
            assert(ray.geomID < _rtcIdToPrimitive.size());
            return _rtcIdToPrimitive[ray.geomID];
        }

        void
        add(const Light*);

        void
        add(const EnvironmentLight*);

        void
        remove(const Light*);

        void
        remove(const EnvironmentLight*);

        void
        dirtyWorldBounds();
    };
}
}
