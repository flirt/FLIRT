// ========================================================================= //
// Modification copyright 2016-2018 SUPAMONKS_STUDIO                         //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
// _________________________________________________________________________ //
// Copyright 2009-2013 Intel Corporation                                     //
//                                                                           //
// Licensed under the Apache License, Version 2.0 (the "License");           //
// you may not use this file except in compliance with the License.          //
// You may obtain a copy of the License at                                   //
//                                                                           //
//     http://www.apache.org/licenses/LICENSE-2.0                            //
//                                                                           //
// Unless required by applicable law or agreed to in writing, software       //
// distributed under the License is distributed on an "AS IS" BASIS,         //
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  //
// See the License for the specific language governing permissions and       //
// limitations under the License.                                            //
//                                                                           //
// ========================================================================= //
#pragma once

#include "../ObjectBase.hpp"
#include "../../sys/FullHandle.hpp"

namespace smks
{
    namespace img
    {
        class OutputImage;
    }
    namespace parm
    {
        class Getters;
    }
    namespace rndr
    {
        class Denoiser;
        class ToneMapper;
        class RenderPass;
        class Renderer;
        class PixelLayout;
        class PixelData;

        /*! Interface to different framebuffers. */
        class FrameBuffer:
            public ObjectBase
        {
        public:
            typedef sys::Ref<FrameBuffer>   Ptr;
        private:
            typedef sys::Ref<RenderPass>    RenderPassPtr;

        protected:
            FrameBuffer(unsigned int scnId, const CtorArgs& args):
                ObjectBase(scnId, args)
            { }

        public:
            virtual void                        denoiser(sys::Ref<Denoiser> const&) = 0;
            virtual sys::Ref<Denoiser>&         denoiser()                          = 0;
            virtual sys::Ref<Denoiser> const&   denoiser() const                    = 0;

            virtual void                        toneMapper(sys::Ref<ToneMapper> const&) = 0;
            virtual sys::Ref<ToneMapper>&       toneMapper()                            = 0;
            virtual sys::Ref<ToneMapper> const& toneMapper() const                      = 0;

            virtual
            size_t
            width() const = 0;

            virtual
            size_t
            height() const = 0;

            virtual
            std::shared_ptr<PixelLayout>
            layout() const = 0;

            virtual
            void
            getFrame(img::OutputImage&) const = 0;

            virtual
            const float*
            getFrameRGBA() const = 0;

            virtual
            const float*
            getFrameRawRGBA() const = 0;

            virtual
            size_t
            maxIdCoverageLayers() const = 0;

            virtual
            PixelData&
            initializePixelData(PixelData&) const = 0;

            virtual
            void
            add(RenderPassPtr const&) = 0;

            virtual
            void
            remove(RenderPassPtr const&) = 0;

            /*! Allows the request optional user parameters prior scene primitive extraction. */
            virtual
            void
            requestUserParametersFromPrimitives(parm::Getters&) = 0;

            virtual
            void
            initialize(const Renderer&) = 0;

            /*! Allows the framebuffer to configure the renderer (for specific renderpasses typically). */
            virtual
            void
            configure(Renderer&) const = 0;

            //<! clears content of buffers without deallocating memory
            virtual
            void
            clear() = 0;

            virtual
            void
            clearPixel(size_t x, size_t y) = 0;
            virtual
            void
            addPixelSample(size_t x, size_t y, size_t s, PixelData&) = 0; //<! WARNING: pixel data is consumed
            virtual
            void
            normalizePixel(size_t x, size_t y) = 0;

            //<! call to registered denoiser and tonemapper
            virtual
            void
            finishFrame() = 0;

            //virtual
            //void
            //set(size_t x, size_t y, const PixelData&) = 0;

            //virtual
            //PixelData&
            //update(size_t x, size_t y, float weight, const PixelData&, bool accumulate, PixelData&) = 0;

            /*! signals the framebuffer that rendering starts */
            virtual
            void
            startRendering(size_t numTiles = 1) = 0;

            /*! register a tile as finished */
            virtual
            bool
            finishTile(size_t numTiles = 1) = 0;

            /*! wait for rendering to finish */
            virtual
            void
            wait() = 0;
        };
    }
}
