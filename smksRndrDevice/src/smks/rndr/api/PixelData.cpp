// ========================================================================= //
// Copyright 2016-2018 SUPAMONKS_STUDIO                                      //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
//                                                                           //
// ========================================================================= //
#include "PixelData.hpp"

using namespace smks;

rndr::PixelData::PixelData():
    _layout         (),
    _data           (),
    _numLPEMatches  (),
    _shapeIdCoverage(nullptr)
{ }

rndr::PixelData::PixelData(const PixelData& other):
    _layout         (),
    _data           (),
    _numLPEMatches  (),
    _shapeIdCoverage(nullptr)
{
    copy(other);
}

rndr::PixelData::PixelData(PixelLayout::Ptr const& L, bool withIdCoverage):
    _layout         (),
    _data           (),
    _numLPEMatches  (),
    _shapeIdCoverage(nullptr)
{
    initialize(L, withIdCoverage);
}

rndr::PixelData::~PixelData()
{
    dispose();
}

void
rndr::PixelData::initialize(PixelLayout::Ptr const& L, bool withIdCoverage)
{
    dispose();

    _layout = L;

    PixelLayout::Ptr const& layout = _layout.lock();

    if (!layout)
        throw sys::Exception("Invalid pixel layout.", "Pixel Data Initialization");
    if (!static_cast<bool>(*layout))
        layout->commitFrameState();
    if (!static_cast<bool>(*layout))
        throw sys::Exception("Failed to commitFrameState pixel layout.", "Pixel Data Initialization");

    _data           .resize(layout->stride());
    _numLPEMatches  .resize(layout->numLPE(), 0);
    if (withIdCoverage)
        _shapeIdCoverage = new ShapeIdCoverageMap();
    clear();
}

