// ========================================================================= //
// Modification copyright 2016-2018 SUPAMONKS_STUDIO                         //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
// _________________________________________________________________________ //
// Copyright 2009-2013 Intel Corporation                                     //
//                                                                           //
// Licensed under the Apache License, Version 2.0 (the "License");           //
// you may not use this file except in compliance with the License.          //
// You may obtain a copy of the License at                                   //
//                                                                           //
//     http://www.apache.org/licenses/LICENSE-2.0                            //
//                                                                           //
// Unless required by applicable law or agreed to in writing, software       //
// distributed under the License is distributed on an "AS IS" BASIS,         //
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  //
// See the License for the specific language governing permissions and       //
// limitations under the License.                                            //
//                                                                           //
// ========================================================================= //
#pragma once

#include <smks/sys/Ref.hpp>
#include <smks/sys/aligned_allocation.hpp>
#include <smks/sys/sync/Condition.hpp>

#include "FrameBuffer.hpp"
#include "PixelLayout.hpp"
#include "PixelData.hpp"
#include "PixelDataBuffer.hpp"
#include "../denoiser/Denoiser.hpp"
#include "../tonemapper/ToneMapper.hpp"
#include "../logParameters.hpp"

namespace smks
{
    namespace parm
    {
        class Container;
    }
    namespace xchg
    {
        class IdSet;
    }

    namespace rndr
    {
        class RenderPass;
        class Renderer;

        class DefaultFrameBuffer:
            public FrameBuffer
        {
            VALID_RTOBJECT
        public:
            typedef sys::Ref<DefaultFrameBuffer>    Ptr;
        private:
            typedef sys::Ref<RenderPass>            RenderPassPtr;
            typedef std::vector<RenderPassPtr>      RenderPasses;

        private:
            sys::Ref<Denoiser>      _denoiser;
            sys::Ref<ToneMapper>    _tonemapper;
            RenderPasses            _renderPasses;          //<! does NOT own data
            //----------------------
            size_t                  _width;
            size_t                  _height;
            PixelLayout::Ptr        _layout;
            size_t                  _maxIdCoverageLayers;
            bool                    _storeRawRgba;
            bool                    _storeRgbaAsHalf;
            //----------------------
            float*                  _accumWeights;
            PixelDataBuffer         _accumBuffer;
            PixelDataBuffer         _pdataBuffer;           //<! normalized pixel data (RGBA + render passes)
            //----------------------
            size_t                  _remainingTiles;        //!< number of tiles that are not rendered yet
            sys::sync::MutexSys     _mutex;                 //!< mutex to protect access to remainingTiles
            sys::sync::ConditionSys _condition;             //!< condition to signal threads waiting for render to finish

            CREATE_RTOBJECT(DefaultFrameBuffer, FrameBuffer)
        protected:
            DefaultFrameBuffer(unsigned int scnId, const CtorArgs&);
        public:
            ~DefaultFrameBuffer();

            //<! dispose will not deregister any render pass
            void
            dispose();

            void
            commitFrameState(const parm::Container&, const xchg::IdSet&);

            inline
            void
            beginSubframeCommits(size_t)
            { }

            inline
            void
            commitSubframeState(size_t, const parm::Container&)
            { }

            inline
            void
            endSubframeCommits()
            { }

            inline
            bool
            perSubframeParameter(unsigned int) const
            {
                return false;
            }

            __forceinline
            size_t
            width() const
            {
                return _width;
            }

            __forceinline
            size_t
            height() const
            {
                return _height;
            }

            __forceinline
            PixelLayout::Ptr
            layout() const
            {
                return _layout;
            }

            void
            getFrame(img::OutputImage&) const;

            __forceinline
            const float*
            getFrameRGBA() const
            {
                return !_pdataBuffer.empty() && _layout && static_cast<bool>(*_layout)
                    ? _pdataBuffer.dataChannel(_layout->offsetRGBA())
                    : nullptr;
            }

            __forceinline
            const float*
            getFrameRawRGBA() const
            {
                return !_pdataBuffer.empty() && _layout && static_cast<bool>(*_layout)
                    ? _pdataBuffer.dataChannel(_layout->offsetRawRGBA())
                    : nullptr;
            }

            __forceinline
            size_t
            maxIdCoverageLayers() const
            {
                return _maxIdCoverageLayers;
            }

            PixelData&
            initializePixelData(PixelData&) const;

            __forceinline
            void
            denoiser(sys::Ref<Denoiser> const& value)
            {
                _denoiser = value;
            }
            __forceinline
            sys::Ref<Denoiser>&
            denoiser()
            {
                return _denoiser;
            }
            __forceinline
            sys::Ref<Denoiser> const&
            denoiser() const
            {
                return _denoiser;
            }

            __forceinline
            void
            toneMapper(sys::Ref<ToneMapper> const& value)
            {
                _tonemapper = value;
            }
            __forceinline
            sys::Ref<ToneMapper>&
            toneMapper()
            {
                return _tonemapper;
            }
            __forceinline
            sys::Ref<ToneMapper> const&
            toneMapper() const
            {
                return _tonemapper;
            }

            //<! pointer must remain valid between addition and removal
            void
            add(RenderPassPtr const&);
            void
            remove(RenderPassPtr const&);

            //<! Allows the request optional user parameters prior scene primitive extraction.
            void
            requestUserParametersFromPrimitives(parm::Getters&);

            void
            initialize(const Renderer&);
            void
            configure(Renderer&) const;

            //<! clears content of buffers without deallocating memory
            void
            clear();

            __forceinline
            void
            clearPixel(size_t x, size_t y)
            {
                assert(_accumWeights);
                assert(!_accumBuffer.empty());
                assert(_layout);
                const size_t index = getPixelIndex(x, y);

                _accumWeights[index] = 0;
                _accumBuffer.clearPixel(index, *_layout);
                //------------
                if (_denoiser)
                    _denoiser->clearPixel(index);
                //------------
            }
            __forceinline
            void
            addPixelSample(size_t x, size_t y, size_t s, PixelData& pData)
            {
                assert(_accumWeights);
                assert(!_accumBuffer.empty());
                const size_t index = getPixelIndex(x, y);

                //------------
                if (_denoiser)
                    _denoiser->addPixelSample(index, s, pData);
                //------------
                ++_accumWeights[index];
                _accumBuffer.addPixel(index, pData);    // WARNING: pData is consumed by function
            }
            __forceinline
            void
            normalizePixel(size_t x, size_t y)
            {
                assert(_accumWeights);
                assert(!_accumBuffer.empty());
                assert(!_pdataBuffer.empty());
                const size_t    index   = getPixelIndex(x, y);
                const int       weight  = _accumWeights[index];

                PixelDataBuffer::scaleAndCopyPixel(
                    _pdataBuffer,
                    _accumBuffer,
                    *_layout,
                    getPixelIndex(x, y),
                    weight > 0 ? math::rcp(static_cast<float>(weight)) : 0.0f);
            }

            void
            finishFrame();

            ////<! returns normalized pixel data
            //__forceinline
            //PixelData&
            //update(size_t x, size_t y, float weight, const PixelData& pData, bool accumulate, PixelData& pdataOut)
            //{
            //  if (_accumWeights == nullptr ||
            //      _accumBuffer.empty())
            //      return pdataOut;

            //  const size_t index = getPixelIndex(x, y);
            //  if (!accumulate)
            //  {
            //      _accumWeights[index]    += weight;
            //      pdataOut = _accumBuffer.get(index, pdataOut);   // pdataOut serves as temporary
            //      pdataOut += pData;
            //      _accumBuffer.set(index, pdataOut);
            //  }
            //  else
            //  {
            //      _accumWeights[index]    = weight;
            //      _accumBuffer.set(index, pData);
            //      pdataOut = pData;
            //  }
            //  pdataOut *= weight > 0.0f ? math::rcp(_accumWeights[index]) : 0.0f;

            //  return pdataOut;
            //}

            //void
            //set(size_t x, size_t y, const PixelData& pData)
            //{
            //  if (!_pdataBuffer.empty())
            //      _pdataBuffer.set(getPixelIndex(x, y), pData);
            //}

            /*! signals the framebuffer that rendering starts */
            inline
            void
            startRendering(size_t numTiles)
            {
                sys::sync::Lock<sys::sync::MutexSys> lock(_mutex);
                //---
                _remainingTiles = numTiles;
            }

            /*! register a tile as finished */
            inline
            bool
            finishTile(size_t numTiles)
            {
                sys::sync::Lock<sys::sync::MutexSys> lock(_mutex);
                //---
                _remainingTiles -= numTiles;
                if (_remainingTiles == 0)
                {
                    _condition.broadcast();
                    return true;
                }
                return false;
            }

            /*! wait for rendering to finish */
            inline
            void
            wait()
            {
                sys::sync::Lock<sys::sync::MutexSys> lock(_mutex);
                //---
                while (_remainingTiles != 0)
                    _condition.wait(_mutex);
            }

            void
            getMetadata(img::OutputImage&) const;

        protected:
            __forceinline
            size_t
            getPixelIndex(size_t x, size_t y) const
            {
                assert(x < _width);
                assert(y < _height);
                return x + _width * y;
            }
        };
    }
}
