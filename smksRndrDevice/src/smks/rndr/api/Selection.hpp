// ========================================================================= //
// Copyright 2016-2018 SUPAMONKS_STUDIO                                      //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
//                                                                           //
// ========================================================================= //
#pragma once

#include <smks/parm/BuiltIns.hpp>
#include "../ObjectBase.hpp"

namespace smks { namespace rndr
{
    class Selection:
        public ObjectBase
    {
        VALID_RTOBJECT
    public:
        typedef sys::Ref<Selection> Ptr;

    private:
        xchg::IdSet _selection;

    protected:
        Selection(unsigned int scnId, const CtorArgs& args):
            ObjectBase  (scnId, args),
            _selection  ()
        {
            dispose();
        }

    public:
        inline
        void
        dispose()
        {
            getBuiltIn<xchg::IdSet>(parm::selection(), _selection);
        }

        inline
        void
        commitFrameState(const parm::Container& parms, const xchg::IdSet& dirties)
        {
            logParameters(scnId(), parms, "builtIns", dirties);
            logParameters(scnId(), userParameters(), "userParms", "USER");
            //---
            for (xchg::IdSet::const_iterator id = dirties.begin(); id != dirties.end(); ++id)
                if (*id == parm::selection().id())
                    getBuiltIn<xchg::IdSet>(parm::selection(), _selection, &parms);
        }

        inline
        void
        beginSubframeCommits(size_t)
        { }

        inline
        void
        commitSubframeState(size_t, const parm::Container&)
        { }

        inline
        void
        endSubframeCommits()
        { }

        inline
        bool
        perSubframeParameter(unsigned int) const
        {
            return false;
        }

        inline
        const xchg::IdSet&
        selection() const
        {
            return _selection;
        }
    };
}
}
