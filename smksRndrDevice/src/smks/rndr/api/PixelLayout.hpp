// ========================================================================= //
// Copyright 2016-2018 SUPAMONKS_STUDIO                                      //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
//                                                                           //
// ========================================================================= //
#pragma once

#include <memory>
#include <vector>
#include <cassert>

#include <smks/xchg/IdSet.hpp>
#include <smks/xchg/Collection.hpp>
#include <std/to_string_xchg.hpp>
#include <smks/util/string/Regex.hpp>

#include "../renderer/RenderPass.hpp"
#include "../../../std/to_string.hpp"

namespace Json
{
    class Value;
}

namespace smks { namespace rndr
{
    class RenderPass;

    //<! class that stores only the layout information of any framebuffer's pixel.
    // it does NOT contain any actual color data.
    class PixelLayout
    {
    public:
        typedef std::shared_ptr<PixelLayout>    Ptr;
        typedef std::weak_ptr<PixelLayout>      WPtr;
    public:
        enum { INVALID_INDEX=-1 };
        enum { INVALID_OFFSET=-1 };

    private:
        ////////////////////////////////
        // struct PixelLayout::FlatEntry
        ////////////////////////////////
        struct FlatEntry
        {
            size_t                          offset;         //<! expressed in number of floats
            std::vector<RenderPass::Ptr>    renderpasses;   //<! keep track of renderpass node(s) that may have triggered registration
        public:
            inline
            FlatEntry():
                offset(INVALID_OFFSET), renderpasses()
            { }

            inline
            operator bool() const
            {
                return offset != INVALID_OFFSET;
            }

            friend inline
            std::ostream&
            operator<<(std::ostream& out, const FlatEntry& e)
            {
                out
                    << "offset = " << e.offset << " float(s)"
                    << "\trenderpasses = { ";
                for (size_t i = 0; i < e.renderpasses.size(); ++i)
                    if (e.renderpasses[i])
                        out << e.renderpasses[i]->scnId() << " ";
                return out << "}";
            }
        };

        ///////////////////////////////
        // struct PixelLayout::EntryLPE
        ///////////////////////////////
        class EntryLPE
        {
        private:
            const size_t                    _id;            //<! index in collection, used to determine offset in _data
        public:
            const util::string::Regex::Ptr  regex;
            const unsigned int              regexId;        //<! in order to prevent regex duplicates
            std::vector<RenderPass::Ptr>    renderpasses;   //<! keep track of renderpass node(s) that may have triggered registration
        public:
            EntryLPE(xchg::Collection<EntryLPE>&    entries,
                     const std::string&             regex):
                _id         (entries.add(this)),
                regex       (util::string::Regex::create(regex.c_str())),
                regexId     (util::string::getId(regex.c_str())),
                renderpasses()
            { }

            inline size_t id() const { return _id; }

            friend inline
            std::ostream&
            operator<<(std::ostream& out, const EntryLPE& e)
            {
                out
                    << "ID = " << e._id
                    << "\tregex = " << e.regex->c_str() << " "
                    << "(regex ID = " << e.regexId << ")"
                    << "\trenderpasses = { ";
                for (size_t i = 0; i < e.renderpasses.size(); ++i)
                    if (e.renderpasses[i])
                        out << e.renderpasses[i]->scnId() << " ";
                return out << "}";
            }
        };

    private:
        FlatEntry                   _requestedFlat[xchg::NUM_PASS_TYPES];
        xchg::Collection<EntryLPE>  _requestedLPE;
        //--------------------------
        size_t  _stride;
        size_t  _offsetRGBA;    //<! offset expressed as a number of floats
        size_t  _offsetRawRGBA; //<! offset expressed as a number of floats
        size_t  _startIndex[NUM_ACCUMULATION_OPS];  //<! last value = pixel stride
        size_t  _numVector4[NUM_ACCUMULATION_OPS];

    public:
        static
        Ptr
        create();

    protected:
        PixelLayout();
    private:
        // non-copyable
        PixelLayout(const PixelLayout&);
        PixelLayout& operator=(const PixelLayout&);

    public:
        ~PixelLayout();

        void
        registerRenderPass(RenderPass::Ptr const&);

        void
        deregisterRenderPass(RenderPass::Ptr const&);

        size_t  // returns pixel stride
        commitFrameState();
    private:
        bool
        haveVector4PassesAnOffsetMultipleOf4() const;
        bool
        doFlatPassesPartitionLayout() const;
    public:
        void
        dispose();

        inline
        operator bool() const
        {
            return !isDirty();
        }

        bool
        operator==(const PixelLayout&) const;

    private:
        inline
        bool
        isDirty() const
        {
            return _stride == 0; // vertex stride should at least be 1 (for RGBA)
        }

        inline
        void
        dirty()
        {
            _stride         = 0;
            _offsetRGBA     = INVALID_OFFSET;
            _offsetRawRGBA  = INVALID_OFFSET;
            for (size_t i = 0; i < static_cast<size_t>(xchg::NUM_PASS_TYPES); ++i)
                _requestedFlat[i].offset = INVALID_OFFSET;
            for (size_t i = 0; i < static_cast<size_t>(NUM_ACCUMULATION_OPS); ++i)
            {
                _startIndex[i] = INVALID_INDEX;
                _numVector4[i] = 0;
            }
        }

    public:
        //<! expressed in float4
        __forceinline
        size_t
        startIndex(AccumulationOp op) const
        {
            assert(op != UNKNOWN_ACCUMULATION_OP && op != NUM_ACCUMULATION_OPS);
            return _startIndex[static_cast<size_t>(op)];
        }

        //<! expressed in float4
        __forceinline
        size_t
        numVector4(AccumulationOp op) const
        {
            assert(op != UNKNOWN_ACCUMULATION_OP && op != NUM_ACCUMULATION_OPS);
            return _numVector4[static_cast<size_t>(op)];
        }

        //<! expressed in float4
        __forceinline
        size_t
        stride() const
        {
            return _stride;
        }

        ////////////////////////////////////////////
        // check whether renderpasses are registered
        ////////////////////////////////////////////
        __forceinline
        bool
        has(xchg::FlatRenderPassType typ) const
        {
            assert(typ != xchg::UNKNOWN_PASS && typ != xchg::NUM_PASS_TYPES);
            return !_requestedFlat[static_cast<size_t>(typ)].renderpasses.empty();  //<! the flat renderpass has been requested
        }

        __forceinline
        bool
        hasLPE() const
        {
            return !_requestedLPE.empty();
        }

        ///////////////////////////////////////////////////////
        // get renderpasses' offsets (offsets in a float array)
        ///////////////////////////////////////////////////////
        __forceinline
        size_t
        offsetRGBA() const
        {
            return _offsetRGBA;
        }

        __forceinline
        size_t
        offsetRawRGBA() const
        {
            return _offsetRawRGBA;
        }

        __forceinline
        size_t
        offset(xchg::FlatRenderPassType typ) const
        {
            assert(typ != xchg::UNKNOWN_PASS && typ != xchg::NUM_PASS_TYPES);
            assert(!isDirty());
            return has(typ) ? _requestedFlat[static_cast<size_t>(typ)].offset : INVALID_OFFSET;
        }

        __forceinline
        const std::vector<RenderPass::Ptr>&
        renderPasses(xchg::FlatRenderPassType typ) const
        {
            assert(typ != xchg::UNKNOWN_PASS && typ != xchg::NUM_PASS_TYPES);
            return _requestedFlat[static_cast<size_t>(typ)].renderpasses;
        }

        /////////////////////////////////
        // light path expression matching
        /////////////////////////////////
        __forceinline
        size_t
        numLPE() const
        {
            return _requestedLPE.size();
        }

        __forceinline
        bool
        matchLPE(size_t i, const std::string& str) const
        {
            assert(i < _requestedLPE.size());
            return _requestedLPE.get(i)->regex->match(str.c_str());
        }

        __forceinline
        const char*
        regexLPE(size_t i) const
        {
            assert(i < _requestedLPE.size());
            return _requestedLPE.get(i)->regex->c_str();
        }

        __forceinline
        size_t
        offsetLPE(size_t i) const
        {
            assert(i < _requestedLPE.size());
            return (i << 2);    //<! global data layout starts with the LPE
        }

        __forceinline
        const std::vector<RenderPass::Ptr>&
        renderPassesLPE(size_t i) const
        {
            assert(i < _requestedLPE.size());
            return _requestedLPE.get(i)->renderpasses;
        }

        void
        getLPEName(size_t, std::string&) const;

        void
        getLPERegexManifest(Json::Value&) const;

        AccumulationOp
        op(size_t offset) const;

        friend inline
        std::ostream&
        operator<<(std::ostream& out, const PixelLayout& l)
        {
            out
                << "stride = " << l._stride
                << "\nrange per accumulation operation:";
            for (size_t o = 0; o < static_cast<size_t>(NUM_ACCUMULATION_OPS); ++o)
                out << "\n\t- " << std::to_string(static_cast<AccumulationOp>(o)) << "\t"
                    << "[" << l.startIndex(static_cast<AccumulationOp>(o)) << "]"
                    << "\t-> # vector4 = " << l.numVector4(static_cast<AccumulationOp>(o));

            out
                << "\n'RGBA': offset = " << l._offsetRGBA << " float(s)"
                << "\n'raw RGBA': offset = " << l._offsetRawRGBA << " float(s)";
            for (size_t t = 0; t < static_cast<size_t>(xchg::NUM_PASS_TYPES); ++t)
            {
                if (!l.has(static_cast<xchg::FlatRenderPassType>(t)))
                    continue;
                out << "\n'" << std::to_string(static_cast<xchg::FlatRenderPassType>(t)) << "':"
                    << "\t" << l._requestedFlat[t];
            }
            for (size_t i = 0; i < l._requestedLPE.size(); ++i)
                out << "\n" << *l._requestedLPE.get(i);

            return out;
        }

    private:
        EntryLPE*
        getEntryLPE(unsigned int regexId);

        static
        void
        add(RenderPass::Ptr const&, std::vector<RenderPass::Ptr>&);

        static
        void
        remove(RenderPass::Ptr const&, std::vector<RenderPass::Ptr>&);
    };
}
}
