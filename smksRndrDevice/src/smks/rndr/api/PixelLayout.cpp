// ========================================================================= //
// Copyright 2016-2018 SUPAMONKS_STUDIO                                      //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
//                                                                           //
// ========================================================================= //
#include <iostream>
#include <sstream>
#include <smks/util/string/getId.hpp>
#include <smks/util/string/Regex.hpp>
#include <json/value.h>

#include "PixelLayout.hpp"
#include "../renderer/FlatRenderPass.hpp"
#include "../renderer/LightPathExpressionPass.hpp"

using namespace smks;

// static
rndr::PixelLayout::Ptr
rndr::PixelLayout::create()
{
    Ptr ptr(new PixelLayout);
    return ptr;
}

rndr::PixelLayout::PixelLayout():
    _requestedLPE               ()
{
    dirty();
}

rndr::PixelLayout::~PixelLayout()
{
    dispose();
}

void
rndr::PixelLayout::dispose()
{
    dirty();
    for (size_t t = 0; t < static_cast<size_t>(xchg::NUM_PASS_TYPES); ++t)
        _requestedFlat[t].renderpasses.clear();
    _requestedLPE.clear();
}

void
rndr::PixelLayout::registerRenderPass(sys::Ref<RenderPass> const& renderpass)
{
    if (!renderpass)
        return;

    if (renderpass->asFlatRenderPass())
    {
        const xchg::FlatRenderPassType typ = renderpass->asFlatRenderPass()->type();

        assert(typ != xchg::UNKNOWN_PASS && typ != xchg::NUM_PASS_TYPES);
        if (!has(typ))
            dirty();
        add(renderpass, _requestedFlat[static_cast<size_t>(typ)].renderpasses);
    }

    else if (renderpass->asLightPathExpressionPass())
    {
        const std::string& regex = renderpass->asLightPathExpressionPass()->regex();
        if (regex.empty())
            return;

        EntryLPE* entryLPE = getEntryLPE(util::string::getId(regex.c_str()));
        if (entryLPE == nullptr)
        {
            entryLPE = new EntryLPE(_requestedLPE, regex);  // owned by the collection from now on
            dirty();
        }
        add(renderpass, entryLPE->renderpasses);
    }
}

void
rndr::PixelLayout::deregisterRenderPass(sys::Ref<RenderPass> const& renderpass)
{
    if (!renderpass)
        return;

    if (renderpass->asFlatRenderPass())
    {
        const xchg::FlatRenderPassType typ = renderpass->asFlatRenderPass()->type();

        assert(typ != xchg::UNKNOWN_PASS && typ != xchg::NUM_PASS_TYPES);
        remove(renderpass, _requestedFlat[static_cast<size_t>(typ)].renderpasses);
    }

    else if (renderpass->asLightPathExpressionPass())
    {
        const std::string& regex = renderpass->asLightPathExpressionPass()->regex();
        if (regex.empty())
            return;

        EntryLPE* entryLPE = getEntryLPE(util::string::getId(regex.c_str()));
        if (entryLPE)
            remove(renderpass, entryLPE->renderpasses);
    }
}

rndr::PixelLayout::EntryLPE*
rndr::PixelLayout::getEntryLPE(unsigned int regexId)
{
    for (size_t i = 0; i < _requestedLPE.size(); ++i)
        if (regexId == _requestedLPE.get(i)->regexId)
            return _requestedLPE.get(i);
    return nullptr;
}

// static
void
rndr::PixelLayout::add(RenderPass::Ptr const&           renderpass,
                        std::vector<RenderPass::Ptr>&   renderpasses)
{
    if (!renderpass)
        return;
    for (size_t i = 0; i < renderpasses.size(); ++i)
        if (renderpasses[i] &&
            renderpasses[i]->scnId() == renderpass->scnId())
            return;
    renderpasses.push_back(renderpass);
}

// static
void
rndr::PixelLayout::remove(RenderPass::Ptr const&            renderpass,
                           std::vector<RenderPass::Ptr>&    renderpasses)
{
    if (!renderpass)
        return;
    for (size_t i = 0; i < renderpasses.size(); ++i)
        if (renderpasses[i] &&
            renderpasses[i]->scnId() == renderpass->scnId())
        {
            std::swap(renderpasses[i], renderpasses.back());
            break;
        }
    renderpasses.pop_back();
}

void
rndr::PixelLayout::getLPEName(size_t i, std::string& ret) const
{
    assert(i < _requestedLPE.size());
    ret = "LPE_" + std::to_string(_requestedLPE.get(i)->regexId);
}

void
rndr::PixelLayout::getLPERegexManifest(Json::Value& root) const
{
    root.clear();
    for (size_t i = 0; i < _requestedLPE.size(); ++i)
    {
        std::string name;

        getLPEName(i, name);
        root[name] = std::string(_requestedLPE.get(i)->regex->c_str());
    }
}

size_t
rndr::PixelLayout::commitFrameState()
{
    if (!isDirty())
        return _stride; // no need to do anything

    size_t numFloats[NUM_ACCUMULATION_OPS];

    // clean-up
    memset(numFloats,   0,  sizeof(size_t) * static_cast<size_t>(NUM_ACCUMULATION_OPS));
    memset(_numVector4, 0,  sizeof(size_t) * static_cast<size_t>(NUM_ACCUMULATION_OPS));
    for (size_t t = 0; t < static_cast<size_t>(xchg::NUM_PASS_TYPES); ++t)
        _requestedFlat[t].offset = 0;

    // light path expressions first (all in average accumulation mode)
    //-----------------------
    numFloats[ACCUMULATION_AVG] = (_requestedLPE.size() << 2);  // RGBA values

    // RGBA (average accumulation mode)
    //-----
    _offsetRGBA = numFloats[ACCUMULATION_AVG]; // expressed in floats
    numFloats[ACCUMULATION_AVG] += 4;
    // Final RGBA (average accumulation mode)
    //-----------
    _offsetRawRGBA = numFloats[ACCUMULATION_AVG]; // expressed in floats
    numFloats[ACCUMULATION_AVG] += 4;

    assert(_offsetRGBA % 4 == 0);   //  aligned in memory to Vector4 boundary
    assert(_offsetRawRGBA % 4 == 0);    //  aligned in memory to Vector4 boundary

    // process flat renderpasses in size decreasing order
    //         -----------------
    // recording local offset and updating the number of necessary vector4
    const size_t currentDepth[] = { 4, 2, 1 };
    for (size_t n = 0; n < sizeof(currentDepth) / sizeof(size_t); ++n)
    {
        for (size_t t = 0; t < static_cast<size_t>(xchg::NUM_PASS_TYPES); ++t)
        {
            const xchg::FlatRenderPassType typ = static_cast<xchg::FlatRenderPassType>(t);
            if (!has(typ))
                continue;

            const size_t depth = requiredDepth(typ);
            if (depth != currentDepth[n])
                continue;

            const AccumulationOp op = accumulationOp(typ);

            _requestedFlat[t].offset    = numFloats[op];
            numFloats[op]               += depth;
        }
    }

    // compute the number of data chunks (vector4f) per accumulation mode
    for (int op = 0; op < NUM_ACCUMULATION_OPS; ++op)
    {
        _numVector4[op] = 0;
        while (numFloats[op] > (_numVector4[op] << 2))
            ++_numVector4[op];
    }

    // compute start indices and complete pixel stride
    _stride = 0;
    for (size_t o = 0; o < static_cast<size_t>(NUM_ACCUMULATION_OPS); ++o)
    {
        _startIndex[o]  = _stride;
        _stride         += _numVector4[o];
    }

    // compute flat renderpasses' global offsets
    for (size_t t = 0; t < static_cast<size_t>(xchg::NUM_PASS_TYPES); ++t)
    {
        xchg::FlatRenderPassType typ = static_cast<xchg::FlatRenderPassType>(t);
        if (has(typ))
        {
            const AccumulationOp op = accumulationOp(typ);
            _requestedFlat[t].offset += (_startIndex[op] << 2); // expressed in floats
            assert(_requestedFlat[t].offset + requiredDepth(typ) <= (_stride << 2));
        }
    }
    assert(!isDirty());
    if (!haveVector4PassesAnOffsetMultipleOf4())
        throw sys::Exception(
            "At least one pass rendering float4 data "
            "has an incorrect offset in pixel layout.",
            "Pixel Layout Commit");
    if (!doFlatPassesPartitionLayout())
        throw sys::Exception(
            "Render passes do not correctly partition pixel layout "
            "(either out-of-bounds or overwriting one another).",
            "Pixel Layout Commit");

    return _stride;
}

bool
rndr::PixelLayout::haveVector4PassesAnOffsetMultipleOf4() const
{
    if (_offsetRGBA % 4 ||
        _offsetRawRGBA % 4)
        return false;
    for (size_t t = 0; t < static_cast<size_t>(xchg::NUM_PASS_TYPES); ++t)
    {
        xchg::FlatRenderPassType typ = static_cast<xchg::FlatRenderPassType>(t);
        if (has(typ) &&
            requiredDepth(typ) > 2 &&
            _requestedFlat[t].offset % 4)
            return false;
    }
    return true;
}

bool
rndr::PixelLayout::doFlatPassesPartitionLayout() const
{
    if (_stride > 0)
    {
        size_t  numFloats   = _stride << 2; // # vector4f -> # floats
        bool*   used        = new bool[numFloats];
        memset(used, 0, sizeof(used));
        for (size_t t = 0; t < static_cast<size_t>(xchg::NUM_PASS_TYPES); ++t)
        {
            const size_t depth = requiredDepth(static_cast<xchg::FlatRenderPassType>(t));
            for (size_t idx = _requestedFlat[t].offset; idx < depth; ++idx)
                if (idx >= numFloats || // out-of-bounds
                    used[idx])  // overwrite other pass
                {
                    delete[] used;
                    return false;
                }
        }
        delete[] used;
    }
    return true;
}

rndr::AccumulationOp
rndr::PixelLayout::op(size_t offset) const
{
    if (!isDirty())
        for (size_t o = 0; o < static_cast<size_t>(NUM_ACCUMULATION_OPS); ++o)
            if ((_startIndex[o] << 2) <= offset &&
                offset < ((_startIndex[o] + _numVector4[o]) << 2))
                return static_cast<AccumulationOp>(o);

    return UNKNOWN_ACCUMULATION_OP;
}

bool
rndr::PixelLayout::operator==(const PixelLayout& other) const
{
    if (_stride         != other._stride        ||
        _offsetRGBA     != other._offsetRGBA    ||
        _offsetRawRGBA  != other._offsetRawRGBA)
        return false;
    for (int i = 0; i < NUM_ACCUMULATION_OPS; ++i)
        if (_startIndex[i] != other._startIndex[i] ||
            _numVector4[i] != other._numVector4[i])
            return false;
    for (int i = 0; i < xchg::NUM_PASS_TYPES; ++i)
        if (_requestedFlat[i].offset != other._requestedFlat[i].offset)
            return false;

    xchg::IdSet ids1, ids2;
    for (size_t id = 0; id < _requestedLPE.size(); ++id)
        if (_requestedLPE.get(id))
            ids1.insert(id);
    for (size_t id = 0; id < other._requestedLPE.size(); ++id)
        if (other._requestedLPE.get(id))
            ids2.insert(id);
    if (!xchg::equal<xchg::IdSet>(ids1, ids2))
        return false;

    for (xchg::IdSet::const_iterator id = ids1.begin(); id != ids1.end(); ++id)
    {
        const EntryLPE& lpe1 = *_requestedLPE.get(*id);
        const EntryLPE& lpe2 = *other._requestedLPE.get(*id);
        if (lpe1.regexId != lpe2.regexId)
            return false;
    }
    return true;
}
