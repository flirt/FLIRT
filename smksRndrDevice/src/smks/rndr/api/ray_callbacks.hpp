// ========================================================================= //
// Copyright 2016-2018 SUPAMONKS_STUDIO                                      //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
//                                                                           //
// ========================================================================= //
#pragma once

#include "Scene.hpp"
#include "Ray.hpp"

namespace smks { namespace rndr
{
    static __forceinline
    bool
    selfIntersects(Ray& ray)
    {
        return ray.orgGeomID != RTC_INVALID_GEOMETRY_ID &&
            ray.orgPrimID != RTC_INVALID_GEOMETRY_ID &&
            ray.geomID  == ray.orgGeomID &&
            ray.primID  == ray.orgPrimID;
    }


    static
    void
    handleIntersected(void* userDataPtr, Ray& ray)
    {
        if (selfIntersects(ray))
        {
            ray.geomID = RTC_INVALID_GEOMETRY_ID;
            return;
        }

        Scene* scene = static_cast<Scene*>(userDataPtr);

        if (scene)
            scene->handleIntersected(ray);
    }

    static
    void
    handleOccluded(void* userDataPtr, Ray& ray)
    {
        if (selfIntersects(ray))
        {
            ray.geomID = RTC_INVALID_GEOMETRY_ID;
            return;
        }

        Scene* scene = static_cast<Scene*>(userDataPtr);

        if (scene)
            scene->handleOccluded(ray);
    }
}
}
