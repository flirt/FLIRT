// ========================================================================= //
// Modification copyright 2016-2018 SUPAMONKS_STUDIO                         //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
// _________________________________________________________________________ //
// Copyright 2009-2013 Intel Corporation                                     //
//                                                                           //
// Licensed under the Apache License, Version 2.0 (the "License");           //
// you may not use this file except in compliance with the License.          //
// You may obtain a copy of the License at                                   //
//                                                                           //
//     http://www.apache.org/licenses/LICENSE-2.0                            //
//                                                                           //
// Unless required by applicable law or agreed to in writing, software       //
// distributed under the License is distributed on an "AS IS" BASIS,         //
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  //
// See the License for the specific language governing permissions and       //
// limitations under the License.                                            //
//                                                                           //
// ========================================================================= //
#pragma once

#include <smks/sys/Ref.hpp>
#include <smks/xchg/Collection.hpp>
#include <smks/sys/Exception.hpp>

#include "Ray.hpp"
#include "../shape/DifferentialGeometry.hpp"
#include "../shape/Shape.hpp"
#include "../light/Light.hpp"
#include "../material/Material.hpp"
#include "../subsurface/SubSurface.hpp"
#include "../../parm/Values.hpp"

#include "../../../std/to_string.hpp"

namespace smks { namespace rndr
{
    enum  PrimitiveType { SHAPE_PRIMITIVE = 0, LIGHT_PRIMITIVE };

    class Primitive
    {
    private:
        const size_t        _id;
        const PrimitiveType _type;
        Shape::Ptr          _shape;
        Light::Ptr          _light;
        Material::Ptr       _material;
        SubSurface::Ptr     _subSurface;
        parm::Values::Ptr   _userParms;
    public:
        inline
        Primitive(PrimitiveType typ, xchg::Collection<Primitive>& prims):
            _id         (prims.add(this)),
            _type       (typ),
            _shape      (nullptr),
            _light      (nullptr),
            _material   (nullptr),
            _subSurface (nullptr),
            _userParms  (nullptr)
        { }

    private:
        // non-copyable
        Primitive(const Primitive&);
        Primitive& operator=(const Primitive&);

    public:
        __forceinline
        void
        postIntersect(RTCScene rtcScene, const Ray& ray, DifferentialGeometry& dg) const
        {
            dg.shape        = _shape.ptr();
            dg.light        = reinterpret_cast<const AreaLight*>(_light.ptr());
            dg.material     = _material.ptr();
            dg.subsurface   = _subSurface.ptr();
            dg.userParms    = _userParms ? _userParms.get() : nullptr;
            //---
            assert(dg.shape);
            dg.shape->postIntersect(rtcScene, ray,dg);
        }

        inline
        PrimitiveType
        type() const
        {
            return _type;
        }

        inline
        size_t
        id() const
        {
            return _id;
        }

        inline void shape       (Shape::Ptr         const& value) { _shape      = value; }
        inline void light       (Light::Ptr         const& value) { _light      = value; }
        inline void material    (Material::Ptr      const& value) { _material   = value; }
        inline void subSurface  (SubSurface::Ptr    const& value) { _subSurface = value; }

        inline Shape::Ptr       shape()      const { return _shape;         }
        inline Light::Ptr       light()      const { return _light;         }
        inline Material::Ptr    material()   const { return _material;      }
        inline SubSurface::Ptr  subSurface() const { return _subSurface;    }

        void
        prefetchUserParameters(const parm::Getters&);

        template<typename ParmType> inline
        bool
        getUserParameter(size_t idx, ParmType& value, const ParmType& defaultValue) const
        {
            if (!_userParms)
            {
                defaultValue = value;
                return false;
            }
            return _userParms->get(idx, value, defaultValue);
        }

        friend inline
        std::ostream&
        operator<<(std::ostream& out, const Primitive& p)
        {
            return out << "{ ID = " << p._id
                << " '" << (p._type == SHAPE_PRIMITIVE ? "shape" : (p._type == LIGHT_PRIMITIVE ? "light" : "?"))  << "'"
                << (p._shape        ? (" shape = " + std::to_string(p._shape->scnId()))             : "")
                << (p._light        ? (" light = " + std::to_string(p._light->scnId()))             : "")
                << (p._material     ? (" material = " + std::to_string(p._material->scnId()))       : "")
                << (p._subSurface   ? (" subsurface = " + std::to_string(p._subSurface->scnId()))   : "")
                << " }";
        }
    };
}
}
