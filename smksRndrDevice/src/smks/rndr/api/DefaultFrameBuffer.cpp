// ========================================================================= //
// Modification copyright 2016-2018 SUPAMONKS_STUDIO                         //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
// _________________________________________________________________________ //
// Copyright 2009-2013 Intel Corporation                                     //
//                                                                           //
// Licensed under the Apache License, Version 2.0 (the "License");           //
// you may not use this file except in compliance with the License.          //
// You may obtain a copy of the License at                                   //
//                                                                           //
//     http://www.apache.org/licenses/LICENSE-2.0                            //
//                                                                           //
// Unless required by applicable law or agreed to in writing, software       //
// distributed under the License is distributed on an "AS IS" BASIS,         //
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  //
// See the License for the specific language governing permissions and       //
// limitations under the License.                                            //
//                                                                           //
// ========================================================================= //
#include <json/value.h>
#include <json/writer.h>
#include <smks/parm/BuiltIns.hpp>
#include <smks/img/OutputImage.hpp>
#include <smks/img/ScaleOffsetPixelMapping.hpp>

#include "DefaultFrameBuffer.hpp"
#include "../renderer/Renderer.hpp"
#include "../renderer/FlatRenderPass.hpp"
#include "../renderer/AmbientOcclusionPass.hpp"
#include "../renderer/LightPathExpressionPass.hpp"
#include "../renderer/ShapeIdCoveragePass.hpp"
#include "../integrator/DefaultIntegrator.hpp"
#include "../denoiser/Denoiser.hpp"
#include "../tonemapper/ToneMapper.hpp"

namespace smks { namespace rndr
{
    static inline
    bool
    storeAsHalf(const std::vector<RenderPass::Ptr>& passes)
    {
        for (size_t i = 0; i < passes.size(); ++i)
            if (passes[i] && passes[i]->storeAsHalf())
                return true;
        return false;
    }
}
}

using namespace smks;

rndr::DefaultFrameBuffer::DefaultFrameBuffer(unsigned int scnId, const CtorArgs& args):
    FrameBuffer             (scnId, args),
    _denoiser               (sys::null),
    _tonemapper             (sys::null),
    _renderPasses           (),
    //-----------------------
    _width                  (),
    _height                 (),
    _layout                 (PixelLayout::create()),
    _maxIdCoverageLayers    (0),
    _storeRawRgba           (false),
    _storeRgbaAsHalf        (false),
    _accumWeights           (nullptr),
    _accumBuffer            (),
    _pdataBuffer            (),
    _remainingTiles         (0),
    _mutex                  (),
    _condition              ()
{
    _renderPasses.reserve(8);
    dispose();
}

rndr::DefaultFrameBuffer::~DefaultFrameBuffer()
{
    dispose();
    _renderPasses.clear();
}

void
rndr::DefaultFrameBuffer::add(RenderPass::Ptr const& pass)
{
    if (!pass)
        return;
    for (size_t i = 0; i < _renderPasses.size(); ++i)
        if (_renderPasses[i] &&
            _renderPasses[i]->scnId() == pass->scnId())
            return; // render pass already added

    _renderPasses.push_back(pass);
    FLIRT_LOG(LOG(DEBUG) << "add rtRenderPass (ptr= " << pass << " ID= " << pass->scnId() << ") to FrameBufferHandle (ptr= " << this << " ID= " << scnId() << ") .");
}

void
rndr::DefaultFrameBuffer::remove(RenderPass::Ptr const& pass)
{
    if (!pass)
        return;
    for (size_t i = 0; i < _renderPasses.size(); ++i)
        if (_renderPasses[i] &&
            _renderPasses[i]->scnId() == pass->scnId())
        {
            std::swap(_renderPasses[i], _renderPasses.back());
            _renderPasses.pop_back();
            FLIRT_LOG(LOG(DEBUG) << "remove rtRenderPass (ptr= " << pass << " ID= " << pass->scnId() << ") from FrameBufferHandle (ptr= " << this << " ID= " << scnId() << ") .");
            return;
        }
}

void
rndr::DefaultFrameBuffer::requestUserParametersFromPrimitives(parm::Getters& getters)
{
    for (size_t i = 0; i < _renderPasses.size(); ++i)
        if (_renderPasses[i])
            _renderPasses[i]->requestUserParametersFromPrimitives(getters);
}

void
rndr::DefaultFrameBuffer::initialize(const Renderer& renderer)
{
    if (_denoiser)
        _denoiser->initialize(*this, renderer);
    if (_tonemapper)
        _tonemapper->initialize(*this);
}

void
rndr::DefaultFrameBuffer::configure(Renderer& renderer) const
{
    for (RenderPasses::const_iterator it = _renderPasses.begin(); it != _renderPasses.end(); ++it)
        if (*it &&
            (*it)->asFlatRenderPass() &&
            (*it)->asFlatRenderPass()->asAmbientOcclusionPass())
        {
            const AmbientOcclusionPass* aoPass = (*it)->asFlatRenderPass()->asAmbientOcclusionPass();
            assert(aoPass);

            DefaultIntegrator::Ptr integrator = renderer.integrator().cast<DefaultIntegrator>();
            if (integrator)
                integrator->accept(*aoPass);
            break;
        }
}

rndr::PixelData&
rndr::DefaultFrameBuffer::initializePixelData(PixelData& pData) const
{
    pData.initialize(_layout, _maxIdCoverageLayers > 0);
    return pData;
}

//<! clears content of buffers without deallocating memory
void
rndr::DefaultFrameBuffer::clear()
{
    if (_accumWeights)
        memset(_accumWeights, 0, sizeof(float) * _width * _height);
    _accumBuffer.clear();
    _pdataBuffer.clear();
}

//<! dispose will not deregister any render pass
void
rndr::DefaultFrameBuffer::dispose()
{
    _remainingTiles = 0;

    // deallocate any preallocated data
    if (_accumWeights)
        delete[] _accumWeights;
    _accumWeights = nullptr;

    _accumBuffer.dispose();
    _pdataBuffer.dispose();

    getBuiltIn<size_t>  (parm::width(),             _width);
    getBuiltIn<size_t>  (parm::height(),            _height);
    getBuiltIn<bool>    (parm::storeRawRgba(),      _storeRawRgba);
    getBuiltIn<bool>    (parm::storeRgbaAsHalf(),   _storeRgbaAsHalf);

    _layout->dispose();
    _maxIdCoverageLayers = 0;
}

void
rndr::DefaultFrameBuffer::commitFrameState(const parm::Container& parms, const xchg::IdSet& dirties)
{
    logParameters(scnId(), parms, "builtIns", dirties);
    logParameters(scnId(), userParameters(), "userParms", "USER");
    //---
    // keep track of framebuffer's last state
    const size_t prvResolution      = _width * _height;
    const size_t prvDataStride      = _layout && static_cast<bool>(*_layout)
        ? _layout->stride()
        : 0;
    const size_t prvMaxIdCoverage   = _maxIdCoverageLayers;

    // account for all registered render passes
    for (size_t i = 0; i < _renderPasses.size(); ++i)
        if (_renderPasses[i])
        {
            RenderPass::Ptr const& renderPass = _renderPasses[i];
            assert(renderPass);
            FLIRT_LOG(LOG(DEBUG) << "renderpass ID = " << renderPass->scnId() << " in framebuffer.";)

            if (renderPass->asShapeIdCoveragePass())
            {
                const size_t maxCount = renderPass->asShapeIdCoveragePass()->maxCount();
                if (maxCount > 0 &&
                    _maxIdCoverageLayers < maxCount)
                    _maxIdCoverageLayers = maxCount;
            }
            else
                _layout->registerRenderPass(renderPass);    // flat renderpass + LPE renderpass
        }
    _layout->commitFrameState();
    assert(_layout->stride() > 0);

    for (xchg::IdSet::const_iterator id = dirties.begin(); id != dirties.end(); ++id)
    {
        if      (*id == parm::width().id())             getBuiltIn<size_t>  (parm::width(),             _width,             &parms);
        else if (*id == parm::height().id())            getBuiltIn<size_t>  (parm::height(),            _height,            &parms);
        else if (*id == parm::storeRawRgba().id())      getBuiltIn<bool>    (parm::storeRawRgba(),      _storeRawRgba,      &parms);
        else if (*id == parm::storeRgbaAsHalf().id())   getBuiltIn<bool>    (parm::storeRgbaAsHalf(),   _storeRgbaAsHalf,   &parms);
    }

    FLIRT_LOG(LOG(DEBUG)
        << "\n--------------------------"
        << "\nframebuffer's pixel layout\n"
        << "\n" << *_layout << "\n"
        << "\n\t- width = " << _width
        << "\n\t- height = " << _height
        << "\n\t- id/coverage max#layers = " << _maxIdCoverageLayers
        << "\n\t- store raw RGBA ? " << _storeRawRgba
        << "\n\t- store RGBA as half ? " << _storeRgbaAsHalf
        << "\n--------------------------";)

    // state allocated memory only if needed memory amount differs
    const size_t resolution = _width * _height;
    if (resolution              != prvResolution ||
        _layout->stride()       != prvDataStride ||
        _maxIdCoverageLayers    != prvMaxIdCoverage)
    {
        // deallocate previous memory pool
        if (_accumWeights)
            delete[] _accumWeights;
        _accumWeights = nullptr;

        _accumBuffer.dispose();
        _pdataBuffer.dispose();

        if (resolution > 0 && _layout->stride() > 0)
        {
            // finally, allocate and initialize new memory pool
            FLIRT_LOG(LOG(DEBUG)
                << "-> framebuffer allocation:"
                << "\twidth = " << _width
                << "\theight = " << _height
                << "\tdata stride = " << _layout->stride()
                << "\tid/coverage max#layers = " << _maxIdCoverageLayers;)

            _accumWeights = new float[resolution];
            memset(_accumWeights, 0, sizeof(float) * resolution);

            _accumBuffer.initialize(_width, _height, _layout->stride(), _maxIdCoverageLayers);
            _pdataBuffer.initialize(_width, _height, _layout->stride(), _maxIdCoverageLayers);

            clear();
        }
    }
}

void
rndr::DefaultFrameBuffer::finishFrame()
{
    const size_t resolution = _width * _height;
    if (resolution == 0)
        return;

    assert(_layout);
    assert(_layout->offsetRGBA() % 4 == 0);

    math::Vector4fv         buffer;
    const math::Vector4f*   rawRGBA = reinterpret_cast<const math::Vector4f*>   (_pdataBuffer.dataChannel(_layout->offsetRawRGBA()));
    math::Vector4f*         RGBA    = reinterpret_cast<math::Vector4f*>         (_pdataBuffer.dataChannel(_layout->offsetRGBA()));
    assert(rawRGBA);
    assert(RGBA);

    // extract denoised frame data and use it to initialize final RGBA channels from _pdataBuffer
    if (_denoiser)
    {
        //std::cout << "\n\n###\ndenoiser has been deactivated\n###\n\n" << std::endl;
        _denoiser->apply(rawRGBA, RGBA);
    }

    // copy current final RGBA values to buffer and run tone mapping color transform
    if (_tonemapper)
    {
        // std::cout << "\n\n###\ntonemapper has been deactivated\n###\n\n" << std::endl;
        if (buffer.empty())
            buffer.resize(_width * _height);
        memcpy(&buffer.front().x(), RGBA, sizeof(math::Vector4f) * resolution);

        _tonemapper->apply(&buffer.front(), RGBA);
    }

}

void
rndr::DefaultFrameBuffer::getFrame(img::OutputImage& frame) const
{
    frame.clear();
    frame.setResolution(_width, _height);

    // main RGBA
    for (size_t k = 0; k < 4; ++k)
        frame.addRGBAChannel(k, _pdataBuffer.dataChannel(_layout->offsetRGBA()+k), 4, _storeRgbaAsHalf);

    // raw RGB (prior denoising and tonemapping)
    if (_storeRawRgba)
    {
        const unsigned int layer = frame.addLayer(3, "raw");
        for (size_t k = 0; k < 3; ++k)
            frame.addLayerChannel(layer, k, _pdataBuffer.dataChannel(_layout->offsetRawRGBA()+k), 4, _storeRgbaAsHalf);
    }

    // flat render passes
    for (size_t t = 0; t < xchg::NUM_PASS_TYPES; ++t)
    {
        const xchg::FlatRenderPassType  typ     = static_cast<xchg::FlatRenderPassType>(t);
        img::AbstractPixelMapping::Ptr  func    = nullptr;
        if (typ == xchg::NORMAL_PASS ||
            typ == xchg::EYE_NORMAL_PASS
#if defined(_DEBUG)
            || typ == xchg::DEBUG_RAY_GEOM_NORMAL_PASS
            || typ == xchg::DEBUG_HIT_GEOM_NORMAL_PASS
            || typ == xchg::DEBUG_HIT_SHAD_NORMAL_PASS
#endif // defined(_DEBUG)
            )
        {
            const float scale[4] = { 0.5f, 0.5f, 0.5f, 1.0f };
            const float offset[4] = { 0.5f, 0.5f, 0.5f, 0.0f };
            func = img::ScaleOffsetPixelMapping::create(4, scale, offset);
        }

        const std::string&  name    = std::to_string(typ);
        const size_t        depth   = requiredDepth(typ);
        const size_t        offset  = _layout->offset(typ);
        if (offset == PixelLayout::INVALID_OFFSET)
            continue;

        const bool          asHalf  = storeAsHalf(_layout->renderPasses(typ));
        const unsigned int  layer   = frame.addLayer(depth, name.c_str(), func);
        for (size_t k = 0; k < depth; ++k)
            frame.addLayerChannel(layer, k, _pdataBuffer.dataChannel(offset+k), 4, asHalf);
    }

    // light path expressions
    for (size_t i = 0; i < _layout->numLPE(); ++i)
    {
        std::string name;
        const bool  asHalf = storeAsHalf(_layout->renderPassesLPE(i));

        _layout->getLPEName(i, name);
        const unsigned int layer = frame.addLayer(3, name.c_str());
        for (size_t k = 0; k < 3; ++k)
            frame.addLayerChannel(layer, k, _pdataBuffer.dataChannel(_layout->offsetLPE(i)+k), 4, asHalf);
    }

    // shape ID-coverage
    if (_maxIdCoverageLayers > 0)
    {
        assert(_pdataBuffer.maxIdCoverageLayers() == _maxIdCoverageLayers);

        unsigned int    layerId         = -1;
        unsigned int    layerCoverage   = -1;
        const bool      asHalf          = false;
        frame.addIdCoverageLayers(_maxIdCoverageLayers, layerId, layerCoverage);
        for (size_t i = 0; i < _maxIdCoverageLayers; ++i)
        {
            frame.addLayerChannel(layerId,          i, _pdataBuffer.shapeIdData(i),         _maxIdCoverageLayers);
            frame.addLayerChannel(layerCoverage,    i, _pdataBuffer.shapeCoverageData(i),   _maxIdCoverageLayers, asHalf);
        }
    }
}

void
rndr::DefaultFrameBuffer::getMetadata(img::OutputImage& frame) const
{
    frame.setMetadata("idCoverage#Layers", _maxIdCoverageLayers);
    for (size_t i = 0; i < _renderPasses.size(); ++i)
        _renderPasses[i]->getMetadata(frame);

    Json::Value         json;
    Json::FastWriter    writer;

    _layout->getLPERegexManifest(json);
    if (!json.empty())
        frame.setMetadata("LPEManifest", writer.write(json));

    if (_denoiser)
        _denoiser->getMetadata(frame);
    if (_tonemapper)
        _tonemapper->getMetadata(frame);
}
