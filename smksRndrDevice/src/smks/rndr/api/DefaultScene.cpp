// ========================================================================= //
// Modification copyright 2016-2018 SUPAMONKS_STUDIO                         //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
// _________________________________________________________________________ //
// Copyright 2009-2013 Intel Corporation                                     //
//                                                                           //
// Licensed under the Apache License, Version 2.0 (the "License");           //
// you may not use this file except in compliance with the License.          //
// You may obtain a copy of the License at                                   //
//                                                                           //
//     http://www.apache.org/licenses/LICENSE-2.0                            //
//                                                                           //
// Unless required by applicable law or agreed to in writing, software       //
// distributed under the License is distributed on an "AS IS" BASIS,         //
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  //
// See the License for the specific language governing permissions and       //
// limitations under the License.                                            //
//                                                                           //
// ========================================================================= //

#include <algorithm>
#include <smks/sys/Log.hpp>

#include <smks/xchg/IdSet.hpp>
#include <smks/parm/Container.hpp>
#include <smks/img/OutputImage.hpp>

#include "../logParameters.hpp"
#include "DefaultScene.hpp"
#include "Primitive.hpp"
#include "ray_callbacks.hpp"
#include "../light/Light.hpp"
#include "../light/MeshLight.hpp"
#include "../light/EnvironmentLight.hpp"
#include "../../parm/Getters.hpp"

using namespace smks;

rndr::DefaultScene::DefaultScene(unsigned int scnId, const CtorArgs& args):
    Scene               (scnId, args),
    _primitives         (),
    _rtcScene           (nullptr),
    _rtcIdToPrimitive   (),
    _allLights          (),
    _environmentLights  (),
    _worldBoundsMin     (),
    _worldBoundsMax     ()
{
    dirtyWorldBounds();
    dispose();
}

void
rndr::DefaultScene::dispose()
{
    dirtyWorldBounds();

    _allLights.clear();
    _allLights.shrink_to_fit();

    _environmentLights.clear();
    _environmentLights.shrink_to_fit();

    while (!_primitives.empty())
        remove(_primitives.begin()->second);

    if (_rtcScene)
        rtcDeleteScene(_rtcScene);
    _rtcScene = nullptr;
}

void
rndr::DefaultScene::dirtyWorldBounds()
{
    _worldBoundsMin.setConstant(FLT_MAX);
    _worldBoundsMax.setConstant(-FLT_MAX);
}

void
rndr::DefaultScene::commitFrameState(const parm::Container&   parms,
                                   const xchg::IdSet&       dirties)
{
    logParameters(scnId(), parms, "builtIns", dirties);
    logParameters(scnId(), userParameters(), "userParms", "USER");
}

void
rndr::DefaultScene::add(Primitive* prim)
{
    if (prim == nullptr)
        return;

    PrimitiveMap::const_iterator const& it = _primitives.find(prim->id());
    if (it != _primitives.end())
    {
        std::stringstream sstr;
        sstr << "Primitive (ID = " << prim->id() << ") already added to scene.";
        throw sys::Exception(sstr.str().c_str(), "Primitive Added to FLIRT Scene");
    }

    _primitives.insert(std::pair<size_t, Primitive*>(prim->id(), prim));
    FLIRT_LOG(LOG(DEBUG)
        << "primitive ID = " << prim->id()
        << "\t" << *prim << " "
        << "(ptr = " << prim << ") added to scene.";)
}

void
rndr::DefaultScene::add(const Light* light)
{
    if (light == nullptr ||
        !light->ready())
        return;
    for (size_t i = 0; i < _allLights.size(); ++i)
        if (_allLights[i] == light)
            return;
    _allLights.push_back(light);
    FLIRT_LOG(LOG(DEBUG) << "light (ptr = " << light << ") added to scene.";)
    add(light->asEnvironmentLight());
}

void
rndr::DefaultScene::add(const EnvironmentLight* light)
{
    if (light == nullptr ||
        !light->ready())
        return;
    for (size_t i = 0; i < _environmentLights.size(); ++i)
        if (_environmentLights[i] == light)
            return;
    _environmentLights.push_back(light);
    FLIRT_LOG(LOG(DEBUG) << "environment light (ptr = " << light << ") added to scene.";)
}

void
rndr::DefaultScene::remove(Primitive* prim)
{
    if (prim == nullptr)
        return;

    {
        std::stringstream sstr;
        sstr << "primitives in scene = { ";
        for (PrimitiveMap::const_iterator it = _primitives.begin(); it != _primitives.end(); ++it)
            sstr << it->first << " ";
        sstr << "}";
        FLIRT_LOG(LOG(DEBUG) << sstr.str();)
    }

    FLIRT_LOG(LOG(DEBUG)
        << "removing primitive " << *prim << " "
        << "(ptr = " << prim << ") from scene...";)

    if (prim->type() == SHAPE_PRIMITIVE)
    {
        if (prim->shape() &&
            prim->shape()->rtcGeomId() != RTC_INVALID_GEOMETRY_ID)
        {
            _rtcIdToPrimitive[prim->shape()->rtcGeomId()] = nullptr;
            prim->shape()->deleteGeometry(_rtcScene);
        }
    }

    PrimitiveMap::const_iterator const& it = _primitives.find(prim->id());
    if (it == _primitives.end())
    {
        std::stringstream sstr;
        sstr << "Primitive (ID = " << prim->id() << ") not found in scene.";
        throw sys::Exception(sstr.str().c_str(), "Primitive Removed from FLIRT Scene");
    }

    remove(prim->light().ptr());
    _primitives.erase(it);
    FLIRT_LOG(LOG(DEBUG) << "primitive " << *prim << " (ptr = " << prim << ") removed from scene.";)
}

void
rndr::DefaultScene::remove(const Light* light)
{
    if (light == nullptr)
        return;
    remove(light->asEnvironmentLight());
    for (size_t i = 0; i < _allLights.size(); ++i)
        if (_allLights[i] == light)
        {
            std::swap(_allLights[i], _allLights.back());
            _allLights.pop_back();
            FLIRT_LOG(LOG(DEBUG) << "light (ptr = " << light << ") removed from scene.";)

            return;
        }
}

void
rndr::DefaultScene::remove(const EnvironmentLight* light)
{
    if (light == nullptr)
        return;
    for (size_t i = 0; i < _environmentLights.size(); ++i)
        if (_environmentLights[i] == light)
        {
            std::swap(_environmentLights[i], _environmentLights.back());
            _environmentLights.pop_back();
            FLIRT_LOG(LOG(DEBUG) << "environment light (ptr = " << light << ") removed from scene.";)

            return;
        }
}

void
rndr::DefaultScene::extractPrimitives(RTCDevice               rtcDevice,
                                     const parm::Getters&   userParms)
{
    _rtcIdToPrimitive   .clear();
    _allLights          .clear();
    _environmentLights  .clear();
    dirtyWorldBounds();

#if defined(FLIRT_LOG_ENABLED)
    {
        std::stringstream sstr;
        userParms.print(sstr);
        FLIRT_LOG(LOG(DEBUG) << sstr.str();)
    }
#endif //defined(FLIRT_LOG_ENABLED)

    // create scene if not done already
    if (_rtcScene == nullptr)
        _rtcScene = rtcDeviceNewScene(rtcDevice, RTC_SCENE_DYNAMIC, RTC_INTERSECT1 | RTC_INTERPOLATE);

    if (!_primitives.empty())
    {
        // extract all shape primitives
        _rtcIdToPrimitive   .reserve(_primitives.size());
        _allLights          .reserve(_primitives.size());
        _environmentLights  .reserve(_primitives.size());

        for (PrimitiveMap::const_iterator it = _primitives.begin(); it != _primitives.end(); ++it)
        {
            const Primitive* prim = it->second;
            if (prim == nullptr)
                continue;

            if (prim->type() == SHAPE_PRIMITIVE)
            {
                if (!prim->shape())
                    throw sys::Exception("Invalid shape referenced by shape primitive.", "Scene Primitive Extraction");

                const unsigned rtcGeomId = prim->shape()->extract(_rtcScene);
                if (rtcGeomId != RTC_INVALID_GEOMETRY_ID)
                {
                    // register intersection/occlusion callbacks
                    rtcSetIntersectionFilterFunction(_rtcScene, rtcGeomId, (RTCFilterFunc)&smks::rndr::handleIntersected);
                    rtcSetOcclusionFilterFunction   (_rtcScene, rtcGeomId, (RTCFilterFunc)&smks::rndr::handleOccluded);
                    rtcSetUserData                  (_rtcScene, rtcGeomId, this);

                    // update the scene's bounding box
                    math::Vector4f shapeWorldBoundsMin, shapeWorldBoundsMax;

                    if (prim->shape()->getWorldBounds(shapeWorldBoundsMin, shapeWorldBoundsMax))
                    {
                        _worldBoundsMin = _worldBoundsMin.cwiseMin(shapeWorldBoundsMin);
                        _worldBoundsMax = _worldBoundsMax.cwiseMax(shapeWorldBoundsMax);
                    }

                    // embree-generated indices described a as compact as possible sequence.
                    if (rtcGeomId >= _rtcIdToPrimitive.size())
                        _rtcIdToPrimitive.resize(rtcGeomId + 1, nullptr);
                    _rtcIdToPrimitive[rtcGeomId] = prim;
                }
            }
            else if (prim->type() == LIGHT_PRIMITIVE)
            {
                if (!prim->light())
                    throw sys::Exception("Invalid light referenced by light primitive.", "Scene Primitive Extraction");

                add(prim->light().ptr());
            }
        }

        for (PrimitiveMap::iterator it = _primitives.begin(); it != _primitives.end(); ++it)
            if (it->second)
                it->second->prefetchUserParameters(userParms);
    }
    rtcCommit(_rtcScene);

#if defined(FLIRT_LOG_ENABLED)
    {
        std::stringstream stream;
        stream << "\n---------------------------\n" << _rtcIdToPrimitive.size() << " intersectable primitive(s)\n";
        for (size_t i = 0; i < _rtcIdToPrimitive.size(); ++i)
            if (_rtcIdToPrimitive[i])
                stream
                    << "\n\t- rtc ID = " << i
                    << "\t-> shape ID = " << _rtcIdToPrimitive[i]->shape()->scnId()
                    << "\tlight ID = " << (_rtcIdToPrimitive[i]->light() ? _rtcIdToPrimitive[i]->light()->scnId() : 0)
                    << "\tmaterial ID = " << (_rtcIdToPrimitive[i]->material() ? _rtcIdToPrimitive[i]->material()->scnId() : 0)
                    << "\tsubsurface ID = " << (_rtcIdToPrimitive[i]->subSurface() ? _rtcIdToPrimitive[i]->subSurface()->scnId() : 0);

        stream << "\n\n" << _allLights.size() << " scene light(s)";
        for (size_t i = 0; i < _allLights.size(); ++i)
            if (_allLights[i])
                stream << "\t" << _allLights[i]->scnId();

        stream << "\n\n" << _environmentLights.size() << " environment light(s)\t";
        for (size_t i = 0; i < _environmentLights.size(); ++i)
            if (_environmentLights[i])
                stream << "\t" << _environmentLights[i]->scnId();

        stream << "\n\nworldspace bounding box = "
            << "( " << _worldBoundsMin.transpose() << " ) -> "
            << "( " << _worldBoundsMax.transpose() << " )";
        stream << "\n--------------------------";
        LOG(DEBUG) << stream.str();
    }
#endif // FLIRT_LOG_ENABLED
}

void
rndr::DefaultScene::getMetadata(img::OutputImage& frame) const
{
    frame.setMetadata("scene/numPrimitives",    _primitives.size());
    frame.setMetadata("scene/totalNumLights",   _allLights.size());
    frame.setMetadata("scene/numEnvLights",     _environmentLights.size());
}
