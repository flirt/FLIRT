// ========================================================================= //
// Copyright 2016-2018 SUPAMONKS_STUDIO                                      //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
//                                                                           //
// ========================================================================= //
#pragma once

#include "PixelLayout.hpp"
#include "PixelData.hpp"

namespace smks { namespace rndr
{
    //<! class storing data, and shape ID-coverage pairs for each pixel.
    // address of a pixel's channels:
    //      @(x, y, i)  = x + width * (y + height * i)
    //                  = (x + width * y)   + width * height * i
    //                  = index             + resolution * i
    //
    class PixelDataBuffer
    {
        ALIGNED_CLASS
    private:
        size_t          _width;
        size_t          _height;
        size_t          _resolution;            //<! width * height
        size_t          _dataStride;
        size_t          _maxIdCoverageLayers;
        //------------------
        math::Vector4fv _data;                  //<! size = width * height * data stride
        unsigned int*   _shapeIds;              //<! size = width * height * max # shape id-coverage pairs (no shape ID duplicate)
        float*          _shapeCoverages;        //<! size = width * height * max # shape id-coverage pairs

    public:
        PixelDataBuffer();
        ~PixelDataBuffer();

        void
        dispose();

        void
        initialize(size_t width, size_t height, size_t dataStride, size_t maxIdCoverageLayers);

        __forceinline
        bool
        empty() const
        {
            return _resolution == 0;
        }

        //<! clears content of buffers without deallocating memory
        void
        clear();

        __forceinline
        const float*
        dataChannel(size_t offset) const
        {
            const size_t i = offset >> 2;
            const size_t j = offset & 0x3;
            assert(i < _data.size());
            return !_data.empty()
                ? reinterpret_cast<const float*>(&_data[_resolution * i].x()) + j
                : nullptr;
        }

        __forceinline
        float*
        dataChannel(size_t offset)
        {
            const size_t i = offset >> 2;
            const size_t j = offset & 0x3;
            assert(i < _data.size());
            return !_data.empty()
                ? reinterpret_cast<float*>(&_data[_resolution * i].x()) + j
                : nullptr;
        }

        __forceinline
        size_t
        maxIdCoverageLayers() const
        {
            return _maxIdCoverageLayers;
        }

        __forceinline
        const unsigned int*
        shapeIdData(size_t i) const
        {
            assert(i < _maxIdCoverageLayers);
            return _shapeIds + i;
        }

        __forceinline
        const float*
        shapeCoverageData(size_t i) const
        {
            assert(i < _maxIdCoverageLayers);
            return _shapeCoverages + i;
        }

        //<! set pixel content at the specified pixel position in the buffer.
        __forceinline
        void
        clearPixel(size_t index, const PixelLayout& layout)
        {
            const size_t iStartAVG = layout.startIndex(static_cast<AccumulationOp>(ACCUMULATION_AVG));
            const size_t iStartMIN = layout.startIndex(static_cast<AccumulationOp>(ACCUMULATION_MIN));
            const size_t iStartMAX = layout.startIndex(static_cast<AccumulationOp>(ACCUMULATION_MAX));
            assert(iStartAVG    == 0);
            assert(iStartMIN    == iStartAVG + layout.numVector4(static_cast<AccumulationOp>(ACCUMULATION_AVG)));
            assert(iStartMAX    == iStartMIN + layout.numVector4(static_cast<AccumulationOp>(ACCUMULATION_MIN)));
            assert(_dataStride  == iStartMAX + layout.numVector4(static_cast<AccumulationOp>(ACCUMULATION_MAX)));

            // vectorized data
            size_t j = index;
            for (size_t i = iStartAVG; i < iStartMIN; ++i, j+=_resolution)
                _data[j].setZero();             // average accumulation
            for (size_t i = iStartMIN; i < iStartMAX; ++i, j+=_resolution)
                _data[j].setConstant(FLT_MAX);  // min accumulation
            for (size_t i = iStartMAX; i < _dataStride; ++i, j+=_resolution)
                _data[j].setConstant(-FLT_MAX); // max accumulation

            // shape ID-coverage data
            if (_maxIdCoverageLayers > 0)
            {
                // shape ID/coverage pairs
                const size_t    offset          = index * _maxIdCoverageLayers;
                unsigned int*   pixIds          = _shapeIds + offset;
                float*          pixCoverages    = _shapeCoverages + offset;

                memset(pixIds,          0, sizeof(unsigned int) * _maxIdCoverageLayers);
                memset(pixCoverages,    0, sizeof(float) * _maxIdCoverageLayers);
            }
        }

        //<! add pixel content at the specified pixel position in the buffer.
        // WARNING: passed pixel data instance is invalidated after the end of the function.
        __forceinline
        void
        addPixel(size_t index, PixelData& pData)
        {
            assert(index < _resolution);
            assert(pData.LPEnormalized());

            PixelLayout::Ptr const& layout = pData._layout.lock();
            assert(layout);
            assert(_dataStride == pData._data.size());

            const size_t iStartAVG = layout->startIndex(static_cast<AccumulationOp>(ACCUMULATION_AVG));
            const size_t iStartMIN = layout->startIndex(static_cast<AccumulationOp>(ACCUMULATION_MIN));
            const size_t iStartMAX = layout->startIndex(static_cast<AccumulationOp>(ACCUMULATION_MAX));
            assert(iStartAVG    == 0);
            assert(iStartMIN    == iStartAVG + layout->numVector4(static_cast<AccumulationOp>(ACCUMULATION_AVG)));
            assert(iStartMAX    == iStartMIN + layout->numVector4(static_cast<AccumulationOp>(ACCUMULATION_MIN)));
            assert(_dataStride  == iStartMAX + layout->numVector4(static_cast<AccumulationOp>(ACCUMULATION_MAX)));

            // vectorized data
            size_t j = index;
            for (size_t i = iStartAVG; i < iStartMIN; ++i, j+=_resolution)
                _data[j] += pData._data[i];                     // average accumulation
            for (size_t i = iStartMIN; i < iStartMAX; ++i, j+=_resolution)
                _data[j] = _data[j].cwiseMin(pData._data[i]);   // min accumulation
            for (size_t i = iStartMAX; i < _dataStride; ++i, j+=_resolution)
                _data[j] = _data[j].cwiseMax(pData._data[i]);   // max accumulation

            // shape ID/coverage pairs
            if (pData._shapeIdCoverage)
            {
                PixelData::ShapeIdCoverageMap& m = *pData._shapeIdCoverage;

                const size_t    offset          = index * _maxIdCoverageLayers;
                unsigned int*   pixIds          = _shapeIds + offset;
                float*          pixCoverages    = _shapeCoverages + offset;

                // add existing pairs to passed pixel data
                for (size_t i = 0; i < _maxIdCoverageLayers; ++i)
                    if (pixIds[i] > 0)
                        m[pixIds[i]] += pixCoverages[i];
                    else
                        break;
                // copy updated pixel data to buffer
                memset(pixIds, 0, sizeof(unsigned int) * _maxIdCoverageLayers);
                size_t i = 0;
                for (PixelData::ShapeIdCoverageMap::const_iterator it = m.begin(); it != m.end(); ++it, ++i)
                {
                    pixIds[i]       = it->first;
                    pixCoverages[i] = it->second;
                    if (i == _maxIdCoverageLayers)
                        break;
                }
            }
            ///////////////
            pData.clear();  //<! necessary because the shape ID -> coverage mapping has been corrupted
            ///////////////
        }

        //<! scale by the specified amount the pixel data from source buffer and copy it to destination buffer
        static
        void
        scaleAndCopyPixel(PixelDataBuffer&          dst,
                          const PixelDataBuffer&    src,
                          const PixelLayout&        layout,
                          size_t                    index,
                          float                     weight)
        {
            assert(dst._resolution == src._resolution);
            assert(layout.stride() == src._dataStride);
            assert(layout.stride() == dst._dataStride);
            assert(dst._maxIdCoverageLayers == src._maxIdCoverageLayers);
            assert(index < src._resolution);

            // vectorized data
            const size_t iStartAVG  = layout.startIndex(static_cast<AccumulationOp>(ACCUMULATION_AVG));
            const size_t iEndAVG    = iStartAVG + layout.numVector4(static_cast<AccumulationOp>(ACCUMULATION_AVG));

            for (size_t i = 0, j = index; i < layout.stride(); ++i, j+=src._resolution)
                dst._data[j] = i >= iStartAVG && i < iEndAVG
                    ? src._data[j] * weight // average accumulation
                    : src._data[j];         // min/max accumulation

            // shape ID/coverage
            const size_t offset = index * src._maxIdCoverageLayers;
            memcpy(dst._shapeIds + offset,          src._shapeIds + offset,         sizeof(unsigned int) * src._maxIdCoverageLayers);
            memcpy(dst._shapeCoverages + offset,    src._shapeCoverages + offset,   sizeof(float) * src._maxIdCoverageLayers);

            for (size_t i = 0, j = offset; i < src._maxIdCoverageLayers; ++i, ++j)
                dst._shapeCoverages[j] *= weight;
        }

        //<! scale pixel content by specified amount and returns result
        __forceinline
        PixelData&
        getPixel(size_t index, PixelData& pData) const
        {
            assert(index < _resolution);

            PixelLayout::Ptr const& layout = pData._layout.lock();
            assert(layout);
            assert(_dataStride == pData._data.size());

            const size_t iStartAVG = layout->startIndex(static_cast<AccumulationOp>(ACCUMULATION_AVG));
            const size_t iStartMIN = layout->startIndex(static_cast<AccumulationOp>(ACCUMULATION_MIN));
            const size_t iStartMAX = layout->startIndex(static_cast<AccumulationOp>(ACCUMULATION_MAX));
            assert(iStartAVG    == 0);
            assert(iStartMIN    == iStartAVG + layout->numVector4(static_cast<AccumulationOp>(ACCUMULATION_AVG)));
            assert(iStartMAX    == iStartMIN + layout->numVector4(static_cast<AccumulationOp>(ACCUMULATION_MIN)));
            assert(_dataStride  == iStartMAX + layout->numVector4(static_cast<AccumulationOp>(ACCUMULATION_MAX)));
        }
    };
}
}
