// ========================================================================= //
// Copyright 2016-2018 SUPAMONKS_STUDIO                                      //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
//                                                                           //
// ========================================================================= //
#include <smks/sys/Log.hpp>
#include <smks/img/AbstractPixelMapping.hpp>
#include "PixelDataBuffer.hpp"

using namespace smks;

rndr::PixelDataBuffer::PixelDataBuffer():
    _width              (0),
    _height             (0),
    _resolution         (0),
    _dataStride         (0),
    _maxIdCoverageLayers(0),
    _data               (),
    _shapeIds           (nullptr),
    _shapeCoverages     (nullptr)
{ }

rndr::PixelDataBuffer::~PixelDataBuffer()
{
    dispose();
}

void
rndr::PixelDataBuffer::dispose()
{
    _data.clear();
    _data.shrink_to_fit();

    if (_shapeIds)
        delete[] _shapeIds;
    _shapeIds = nullptr;

    if (_shapeCoverages)
        delete[] _shapeCoverages;
    _shapeCoverages = nullptr;

    _width                  = 0;
    _height                 = 0;
    _resolution             = 0;
    _dataStride             = 0;
    _maxIdCoverageLayers    = 0;
}

void
rndr::PixelDataBuffer::initialize(size_t width, size_t height, size_t dataStride, size_t maxIdCoverageLayers)
{
    FLIRT_LOG(LOG(DEBUG)
        << "\n--------------------------------"
        << "\npixel data buffer initialization\n"
        << "\twidth = " << width
        << "\theight = " << height
        << "\tstride = " << dataStride
        << "\tmax # id-cov = " << maxIdCoverageLayers
        << "\n-------------------------------";)

    dispose();

    _width                  = width;
    _height                 = height;
    _resolution             = _width * _height;
    _dataStride             = dataStride;
    _maxIdCoverageLayers    = maxIdCoverageLayers;

    _data.resize(_resolution * _dataStride, math::Vector4f::Zero());

    if (_resolution > 0 && _maxIdCoverageLayers > 0)
    {
        _shapeIds       = new unsigned int  [_resolution * _maxIdCoverageLayers];
        _shapeCoverages = new float         [_resolution * _maxIdCoverageLayers];
    }
    clear();
}

//<! clears content of buffers without deallocating memory
void
rndr::PixelDataBuffer::clear()
{
    if (!_data.empty())
        memset(&_data.front(), 0, sizeof(math::Vector4f) * _data.size());
    if (_resolution > 0 && _maxIdCoverageLayers > 0)
    {
        memset(_shapeIds,       0, sizeof(unsigned int) * _resolution * _maxIdCoverageLayers);
        memset(_shapeCoverages, 0, sizeof(float) * _resolution * _maxIdCoverageLayers);
    }
}
