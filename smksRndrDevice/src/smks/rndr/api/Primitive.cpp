// ========================================================================= //
// Modification copyright 2016-2018 SUPAMONKS_STUDIO                         //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
// _________________________________________________________________________ //
// Copyright 2009-2013 Intel Corporation                                     //
//                                                                           //
// Licensed under the Apache License, Version 2.0 (the "License");           //
// you may not use this file except in compliance with the License.          //
// You may obtain a copy of the License at                                   //
//                                                                           //
//     http://www.apache.org/licenses/LICENSE-2.0                            //
//                                                                           //
// Unless required by applicable law or agreed to in writing, software       //
// distributed under the License is distributed on an "AS IS" BASIS,         //
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  //
// See the License for the specific language governing permissions and       //
// limitations under the License.                                            //
//                                                                           //
// ========================================================================= //
#include <sstream>
#include "Primitive.hpp"
#include "../../parm/Getters.hpp"
#include <smks/sys/Log.hpp>

using namespace smks;

void
rndr::Primitive::prefetchUserParameters(const parm::Getters& getters)
{
    _userParms = parm::Values::create();
    // user parameter values are accumulated within data holder
    if (_shape)
        getters.extractFrom(_shape->userParameters(), *_userParms);
    if (_light)
        getters.extractFrom(_light->userParameters(), *_userParms);
    if (_material)
        getters.extractFrom(_material->userParameters(), *_userParms);
    if (_subSurface)
        getters.extractFrom(_subSurface->userParameters(), *_userParms);

    if (static_cast<bool>(*_userParms) == false)
        _userParms = nullptr;

    //---
#if defined(FLIRT_LOG_ENABLED)
    if (_userParms)
    {
        std::stringstream sstr;
        _userParms->print(sstr << "Primitive ID = " << _id << ":\t");
        LOG(DEBUG) << sstr.str();
    }
#endif // defined(FLIRT_LOG_ENABLED)
}
