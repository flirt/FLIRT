// ========================================================================= //
// Copyright 2016-2018 SUPAMONKS_STUDIO                                      //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
//                                                                           //
// ========================================================================= //
#pragma once

#include <cassert>
#include <memory>

#include <boost/unordered_map.hpp>
#include <smks/sys/aligned_allocation.hpp>
#include <smks/xchg/IdSet.hpp>
#include <smks/math/types.hpp>
#include <smks/math/matrixAlgo.hpp>
#include <smks/util/string/Regex.hpp>

#include "../renderer/FlatRenderPassType.hpp"
#include "../integrator/LightPathExpression.hpp"
#include "PixelLayout.hpp"

namespace smks { namespace rndr
{
    //////////////////
    // class PixelData
    //////////////////
    class PixelData
    {
        //ALIGNED_CLASS
        friend class PixelDataBuffer;
    private:
        typedef boost::unordered_map<unsigned int, float> ShapeIdCoverageMap;

    private:
        PixelLayout::WPtr           _layout;            //<! owned specified by framebuffer. indicates pixel data memory layout.
        //------------------
        math::Vector4fv             _data;              //<! size = layout.stride
        std::vector<unsigned int>   _numLPEMatches;     //<! size = layout.# LPE
        ShapeIdCoverageMap*         _shapeIdCoverage;   //<! if null, no shape ID-coverage pairs are saved

    public:
        PixelData();
        PixelData(const PixelData&);
        PixelData(PixelLayout::Ptr const&, bool withIdCoverage);

        ~PixelData();

        inline
        PixelData&
        operator=(const PixelData& other)
        {
            copy(other);
            return *this;
        }

        //<! memory allocation/deallocation
        void
        initialize(PixelLayout::Ptr const&, bool withIdCoverage);

        __forceinline
        operator bool() const
        {
            PixelLayout::Ptr const& layout = _layout.lock();
            return layout && static_cast<bool>(*layout) && !_data.empty();
        }

        __forceinline
        void
        dispose()
        {
            _layout.reset();

            if (_shapeIdCoverage) delete _shapeIdCoverage;
            _shapeIdCoverage = nullptr;

            _numLPEMatches.clear(); _numLPEMatches.shrink_to_fit();
            _data.clear();          _data.shrink_to_fit();
        }

    private:
        __forceinline
        void
        copy(const PixelData& other)
        {
            if (this == &other)
                return;

            dispose();
            _layout         = other._layout;
            _data           = other._data;
            _numLPEMatches  = other._numLPEMatches;
            if (other._shapeIdCoverage)
            {
                _shapeIdCoverage    = new ShapeIdCoverageMap();
                *_shapeIdCoverage   = *other._shapeIdCoverage;
            }
        }

    public:

        __forceinline
        bool
        has(xchg::FlatRenderPassType typ) const
        {
            PixelLayout::Ptr const& layout = _layout.lock();
            return layout
                ? layout->has(typ)
                : false;
        }

        __forceinline
        bool
        hasLPE() const
        {
            PixelLayout::Ptr const& layout = _layout.lock();
            return layout
                ? layout->hasLPE()
                : false;
        }

        __forceinline
        bool
        shapeIdCoverageRegistered() const
        {
            return _shapeIdCoverage != nullptr;
        }

        //<! clear pixel values
        __forceinline
        void
        clear()
        {
            if (!static_cast<bool>(*this))
                return;

            PixelLayout::Ptr const& layout = _layout.lock();
            assert(layout);

            // average accumulation
            size_t iStart   = layout->startIndex(static_cast<AccumulationOp>(ACCUMULATION_AVG));
            size_t iEnd     = iStart + layout->numVector4(static_cast<AccumulationOp>(ACCUMULATION_AVG));
            for (size_t i = iStart; i < iEnd; ++i)
                _data[i].setConstant(0.0f);

            // min accumulation
            iStart  = layout->startIndex(static_cast<AccumulationOp>(ACCUMULATION_MIN));
            iEnd    = iStart + layout->numVector4(static_cast<AccumulationOp>(ACCUMULATION_MIN));
            for (size_t i = iStart; i < iEnd; ++i)
                _data[i].setConstant(FLT_MAX);

            // max accumulation
            iStart  = layout->startIndex(static_cast<AccumulationOp>(ACCUMULATION_MAX));
            iEnd    = iStart + layout->numVector4(static_cast<AccumulationOp>(ACCUMULATION_MAX));
            for (size_t i = iStart; i < iEnd; ++i)
                _data[i].setConstant(-FLT_MAX);

            if (!_numLPEMatches.empty())
                memset(&_numLPEMatches.front(), 0, sizeof(unsigned int) * _numLPEMatches.size());

            // shape ID-coverage
            if (_shapeIdCoverage)
                _shapeIdCoverage->clear();
        }

        __forceinline
        math::Vector4f&
        RGBA()
        {
            assert(static_cast<bool>(*this));
            PixelLayout::Ptr const& layout = _layout.lock();
            assert(layout->offsetRGBA() % 4 == 0);
            return _data[layout->offsetRGBA() >> 2];
        }

        __forceinline
        const math::Vector4f&
        RGBA() const
        {
            assert(static_cast<bool>(*this));
            PixelLayout::Ptr const& layout = _layout.lock();
            assert(layout->offsetRGBA() % 4 == 0);
            return _data[layout->offsetRGBA() >> 2];
        }

        __forceinline
        math::Vector4f&
        rawRGBA()
        {
            assert(static_cast<bool>(*this));
            PixelLayout::Ptr const& layout = _layout.lock();
            assert(layout->offsetRawRGBA() % 4 == 0);
            return _data[layout->offsetRawRGBA() >> 2];
        }

        __forceinline
        const math::Vector4f&
        rawRGBA() const
        {
            assert(static_cast<bool>(*this));
            PixelLayout::Ptr const& layout = _layout.lock();
            assert(layout->offsetRawRGBA() % 4 == 0);
            return _data[layout->offsetRawRGBA() >> 2];
        }

        __forceinline
        void
        set(xchg::FlatRenderPassType typ, float x)
        {
            const size_t offset = _layout.lock()->offset(typ);
            if (offset == PixelLayout::INVALID_OFFSET)
                return;
            float* data = &_data.front().x() + offset;
            *data       = x;
        }

        __forceinline
        void
        set(xchg::FlatRenderPassType typ, float x, float y)
        {
            const size_t offset = _layout.lock()->offset(typ);
            if (offset == PixelLayout::INVALID_OFFSET)
                return;
            float* data = &_data.front().x() + offset;
            data[0]     = x;
            data[1]     = y;
        }

        __forceinline
        void
        set(xchg::FlatRenderPassType typ, const math::Vector4f& value)
        {
            const size_t offset = _layout.lock()->offset(typ);
            if (offset == PixelLayout::INVALID_OFFSET)
                return;
            assert(offset % 4 == 0);
            _data[offset >> 2] = value;
        }

        __forceinline
        void
        add(xchg::FlatRenderPassType typ, float x)
        {
            const size_t offset = _layout.lock()->offset(typ);
            if (offset == PixelLayout::INVALID_OFFSET)
                return;
            assert(_layout.lock()->op(offset) == ACCUMULATION_AVG);
            float* data = &_data.front().x() + offset;
            *data       += x;
        }

        __forceinline
        void
        add(xchg::FlatRenderPassType typ, float x, float y)
        {
            const size_t offset = _layout.lock()->offset(typ);
            if (offset == PixelLayout::INVALID_OFFSET)
                return;
            assert(_layout.lock()->op(offset) == ACCUMULATION_AVG);
            float* data = &_data.front().x() + offset;
            data[0]     += x;
            data[1]     += y;
        }

        __forceinline
        void
        add(xchg::FlatRenderPassType typ, const math::Vector4f& value)
        {
            const size_t offset = _layout.lock()->offset(typ);
            if (offset == PixelLayout::INVALID_OFFSET)
                return;
            assert(_layout.lock()->op(offset) == ACCUMULATION_AVG);
            assert(offset % 4 == 0);
            _data[offset >> 2] += value;
        }

        __forceinline
        void
        min(xchg::FlatRenderPassType typ, float x)
        {
            const size_t offset = _layout.lock()->offset(typ);
            if (offset == PixelLayout::INVALID_OFFSET)
                return;
            assert(_layout.lock()->op(offset) == ACCUMULATION_MIN);
            float* data = &_data.front().x() + offset;
            *data       = math::min(*data, x);
        }

        __forceinline
        void
        min(xchg::FlatRenderPassType typ, float x, float y)
        {
            const size_t offset = _layout.lock()->offset(typ);
            if (offset == PixelLayout::INVALID_OFFSET)
                return;
            assert(_layout.lock()->op(offset) == ACCUMULATION_MIN);
            float* data = &_data.front().x() + offset;
            data[0]     = math::min(data[0], x);
            data[1]     = math::min(data[1], y);
        }

        __forceinline
        void
        min(xchg::FlatRenderPassType typ, const math::Vector4f& value)
        {
            const size_t offset = _layout.lock()->offset(typ);
            if (offset == PixelLayout::INVALID_OFFSET)
                return;
            assert(_layout.lock()->op(offset) == ACCUMULATION_MIN);
            assert(offset % 4 == 0);
            math::Vector4f& data    = _data[offset >> 2];
            data                    = data.cwiseMin(value);
        }

        __forceinline
        void
        max(xchg::FlatRenderPassType typ, float x)
        {
            const size_t offset = _layout.lock()->offset(typ);
            if (offset == PixelLayout::INVALID_OFFSET)
                return;
            assert(_layout.lock()->op(offset) == ACCUMULATION_MAX);
            float* data = &_data.front().x() + offset;
            *data       = math::max(*data, x);
        }

        __forceinline
        void
        max(xchg::FlatRenderPassType typ, float x, float y)
        {
            const size_t offset = _layout.lock()->offset(typ);
            if (offset == PixelLayout::INVALID_OFFSET)
                return;
            assert(_layout.lock()->op(offset) == ACCUMULATION_MAX);
            float* data = &_data.front().x() + offset;
            data[0]     = math::max(data[0], x);
            data[1]     = math::max(data[1], y);
        }

        __forceinline
        void
        max(xchg::FlatRenderPassType typ, const math::Vector4f& value)
        {
            const size_t offset = _layout.lock()->offset(typ);
            if (offset == PixelLayout::INVALID_OFFSET)
                return;
            assert(_layout.lock()->op(offset) == ACCUMULATION_MAX);
            assert(offset % 4 == 0);
            math::Vector4f& data    = _data[offset >> 2];
            data                    = data.cwiseMax(value);
        }

        __forceinline
        void
        matchLPE(const LightPathExpression& lpe, const math::Vector4f& contrib)
        {
            PixelLayout::Ptr const& layout = _layout.lock();
            for (size_t i = 0; i < layout->numLPE(); ++i)
                if (layout->matchLPE(i, lpe))
                {
                    const size_t offset = layout->offsetLPE(i);
                    assert(layout->op(offset) == ACCUMULATION_AVG);
                    assert(offset % 4 == 0);
                    _data[offset >> 2] += contrib;
                    ++_numLPEMatches[i];
                }
        }

        __forceinline
        void
        normalizeLPE()
        {
            assert(static_cast<bool>(*this));
            PixelLayout::Ptr const& layout = _layout.lock();
            if (!layout->hasLPE())
                return;
            for (size_t i = 0; i < layout->numLPE(); ++i)
                if (_numLPEMatches[i] > 1)
                    _data[i] *= math::rcp(static_cast<float>(_numLPEMatches[i]));
            memset(&_numLPEMatches.front(), 0, sizeof(unsigned int) * layout->numLPE());
        }
        __forceinline
        bool
        LPEnormalized() const
        {
            PixelLayout::Ptr const& layout = _layout.lock();
            if (layout)
                for (size_t i = 0; i < layout->numLPE(); ++i)
                    if (_numLPEMatches[i] > 0)
                        return false;
            return true;
        }

        __forceinline
        void
        add(unsigned int shapeId, float shapeCoverage)
        {
            if (_shapeIdCoverage == nullptr || shapeId == 0)
                return;
            ShapeIdCoverageMap::iterator const& it = _shapeIdCoverage->find(shapeId);

            if (it != _shapeIdCoverage->end())
                it->second += shapeCoverage;
            else
                _shapeIdCoverage->insert(std::pair<unsigned int, float>(shapeId, shapeCoverage));
        }

        __forceinline
        PixelData&
        operator+=(const PixelData& other)
        {
            if (!PixelData::compatible(*this, other))
                return *this;

            PixelLayout::Ptr const& layout = _layout.lock();
            assert(layout);

            // average accumulation
            size_t iStart   = layout->startIndex(static_cast<AccumulationOp>(ACCUMULATION_AVG));
            size_t iEnd     = iStart + layout->numVector4(static_cast<AccumulationOp>(ACCUMULATION_AVG));
            for (size_t i = iStart; i < iEnd; ++i)
                _data[i] += other._data[i];

            // min accumulation
            iStart  = layout->startIndex(static_cast<AccumulationOp>(ACCUMULATION_MIN));
            iEnd    = iStart + layout->numVector4(static_cast<AccumulationOp>(ACCUMULATION_MIN));
            for (size_t i = iStart; i < iEnd; ++i)
                _data[i] = _data[i].cwiseMin(other._data[i]);

            // max accumulation
            iStart  = layout->startIndex(static_cast<AccumulationOp>(ACCUMULATION_MAX));
            iEnd    = iStart + layout->numVector4(static_cast<AccumulationOp>(ACCUMULATION_MAX));
            for (size_t i = iStart; i < iEnd; ++i)
                _data[i] = _data[i].cwiseMax(other._data[i]);

            for (size_t i = 0; i < layout->numLPE(); ++i)
                _numLPEMatches[i] += other._numLPEMatches[i];

            // shape ID-coverage
            if (_shapeIdCoverage)
            {
                assert(other._shapeIdCoverage);
                for (ShapeIdCoverageMap::const_iterator it = other._shapeIdCoverage->begin();
                    it != other._shapeIdCoverage->begin();
                    ++it)
                    add(it->first, it->second);
            }

            return *this;
        }

        __forceinline
        PixelData&
        operator*=(float k)
        {
            if (!static_cast<bool>(*this))
                return *this;

            PixelLayout::Ptr const& layout = _layout.lock();
            assert(layout);

            // average accumulation
            size_t iStart   = layout->startIndex(static_cast<AccumulationOp>(ACCUMULATION_AVG));
            size_t iEnd     = iStart + layout->numVector4(static_cast<AccumulationOp>(ACCUMULATION_AVG));
            for (size_t i = iStart; i < iEnd; ++i)
                _data[i] *= k;

            // shape ID-coverage
            if (_shapeIdCoverage)
                for (ShapeIdCoverageMap::iterator it = _shapeIdCoverage->begin();
                    it != _shapeIdCoverage->end();
                    ++it)
                    it->second *= k;

            return *this;
        }

    private:
        static __forceinline
        bool
        compatible(const PixelData& a, const PixelData& b)
        {
            PixelLayout::Ptr const& layoutA = a._layout.lock();
            PixelLayout::Ptr const& layoutB = b._layout.lock();
            return layoutA && layoutB &&
                layoutA.get() == layoutB.get() &&
                (a._shapeIdCoverage != nullptr) == (b._shapeIdCoverage != nullptr);
        }
    };
}
}
