// ========================================================================= //
// Modification copyright 2016-2018 SUPAMONKS_STUDIO                         //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
// _________________________________________________________________________ //
// Copyright 2009-2013 Intel Corporation                                     //
//                                                                           //
// Licensed under the Apache License, Version 2.0 (the "License");           //
// you may not use this file except in compliance with the License.          //
// You may obtain a copy of the License at                                   //
//                                                                           //
//     http://www.apache.org/licenses/LICENSE-2.0                            //
//                                                                           //
// Unless required by applicable law or agreed to in writing, software       //
// distributed under the License is distributed on an "AS IS" BASIS,         //
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  //
// See the License for the specific language governing permissions and       //
// limitations under the License.                                            //
//                                                                           //
// ========================================================================= //
#pragma once

#include <embree2/rtcore.h>
#include "../ObjectBase.hpp"

namespace smks
{
    namespace parm
    {
        class Getters;
    }
    namespace rndr
    {
        class Primitive;
        class Light;
        class EnvironmentLight;
        struct Ray;
        struct DifferentialGeometry;

        class Scene:
            public ObjectBase
        {
        public:
            typedef sys::Ref<Scene> Ptr;

        protected:
            Scene(unsigned int scnId, const CtorArgs& args):
                ObjectBase(scnId, args)
            { }

        public:
            virtual
            void
            add(Primitive*) = 0;

            virtual
            void
            remove(Primitive*) = 0;

            virtual
            void
            extractPrimitives(RTCDevice, const parm::Getters& userParms) = 0;

            virtual
            void
            postIntersect(const Ray&, DifferentialGeometry&) const = 0;

            virtual
            RTCScene
            rtcScene() const = 0;

            virtual
            size_t
            totalNumLights() const = 0;

            virtual
            const Light*
            light(size_t) const = 0;

            virtual
            size_t
            numEnvironmentLights() const = 0;

            virtual
            const EnvironmentLight*
            environmentLight(size_t) const = 0;

            virtual
            bool
            getWorldBounds(math::Vector4f&, math::Vector4f&) const = 0;

            //--- intersection/occlusion callbacks ---------------------
            virtual __forceinline void handleIntersected(Ray&) const = 0;
            virtual __forceinline void handleOccluded   (Ray&) const = 0;
            //----------------------------------------------------------
        };
    }
}
