// ========================================================================= //
// Copyright 2016-2018 SUPAMONKS_STUDIO                                      //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
//                                                                           //
// ========================================================================= //
#pragma once

#include <smks/sys/Ref.hpp>
#include <smks/sys/aligned_allocation.hpp>
#include <smks/sys/Exception.hpp>
#include <smks/sys/Log.hpp>
#include <smks/parm/BuiltIn.hpp>
#include <smks/parm/Container.hpp>

#include "logParameters.hpp"

//-------------------------------------------------
#define VALID_RTOBJECT                              \
    public:                                         \
        inline bool valid() const { return true; }
//--------------------------------------------------------------
#define CREATE_RTOBJECT(_TYPE_, _BASE_TYPE_)                    \
    public:                                                     \
    static inline                                               \
    sys::Ref<_BASE_TYPE_>                                       \
    create(unsigned int     scnId,                              \
           const CtorArgs&  args)                               \
    {                                                           \
        sys::Ref<_BASE_TYPE_> ptr (new _TYPE_(scnId, args));    \
        return ptr;                                             \
    }
//-------------------------------------------------------------

namespace smks
{
    namespace parm
    {
        class Container;
    }
    namespace xchg
    {
        class IdSet;
    }
    namespace img
    {
        class OutputImage;
    }

    class ClientBase;

    namespace rndr
    {
        /////////////////
        // class ObjectBase
        /////////////////
        //<! base class for all rendering device entities declared and parameterized in the scene.
        class ObjectBase:
            public sys::RefCount
        {
            ALIGNED_CLASS
        public:
            //////////////////
            // struct CtorArgs
            //////////////////
            struct CtorArgs
            {
                parm::Container::Ptr        userParms;
                std::weak_ptr<ClientBase>   client;
            };
            //////////////////
        public:
            typedef sys::Ref<ObjectBase>        Ptr;
        protected:
            typedef std::weak_ptr<ClientBase>   ClientWPtr;

        private:
            const unsigned int      _scnId;     //<! ID of the corresponding node in scene
            parm::Container::Ptr    _userParms;
        protected:
            ObjectBase(unsigned int     scnId,
                       const CtorArgs&  args):
                _scnId      (scnId),
                _userParms  (args.userParms)
            {
                if (!_userParms)
                    throw sys::Exception(
                        "Invalid user parameter container.",
                        "Rt Object Construction");
            }

        public:
            virtual inline
            ~ObjectBase()
            { }

            inline
            unsigned int
            scnId() const
            {
                return _scnId;
            }

            inline
            const parm::Container&
            userParameters() const
            {
                return *_userParms;
            }

            //<! function indicating the instance's validity
            virtual
            bool
            valid() const = 0;

            //<! function in charge of deallocating all memory belonging to the instance
            virtual
            void
            dispose() = 0;

            //<! function indicating whether the specified parameter is committed on a subframe basis
            virtual
            bool
            perSubframeParameter(unsigned int parmId) const = 0;

            virtual
            void
            beginSubframeCommits(size_t numSubframes) = 0;

            virtual
            void
            commitSubframeState(size_t subframe,
                                const parm::Container&) = 0;

            virtual
            void
            endSubframeCommits() = 0;

            //<! function in charge of updating instance's parameters and allocating memory
            virtual
            void
            commitFrameState(const parm::Container&,
                             const xchg::IdSet&) = 0;

            virtual
            void
            getMetadata(img::OutputImage&) const
            { }
        };

        template<typename ValueType> static inline
        ValueType&
        getBuiltIn(const parm::BuiltIn&     builtIn,
                   ValueType&               value,
                   const parm::Container*   parms = nullptr)
        {
#if defined(_DEBUG)
            if (!builtIn.compatibleWith<ValueType>())
            {
                std::stringstream sstr;
                sstr
                    << "Incorrect type for built-in parameter "
                    << "'" << builtIn.c_str() << "'.";
                throw sys::Exception(sstr.str().c_str(), "Rt Object Built-In Getter");
            }
#endif // defined(_DEBUG)

            if (parms && parms->existsAs<ValueType>(builtIn.id()))
                value = parms->get<ValueType>(builtIn.id());
            else if (!builtIn.defaultValue(value))
                value = ValueType();

            return value;
        }
    }
}
