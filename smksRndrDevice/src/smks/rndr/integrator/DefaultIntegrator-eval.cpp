// ========================================================================= //
// Modification copyright 2016-2018 SUPAMONKS_STUDIO                         //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
// _________________________________________________________________________ //
// Copyright 2009-2013 Intel Corporation                                     //
//                                                                           //
// Licensed under the Apache License, Version 2.0 (the "License");           //
// you may not use this file except in compliance with the License.          //
// You may obtain a copy of the License at                                   //
//                                                                           //
//     http://www.apache.org/licenses/LICENSE-2.0                            //
//                                                                           //
// Unless required by applicable law or agreed to in writing, software       //
// distributed under the License is distributed on an "AS IS" BASIS,         //
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  //
// See the License for the specific language governing permissions and       //
// limitations under the License.                                            //
//                                                                           //
// ========================================================================= //
#include <smks/math/math.hpp>

#include "DefaultIntegrator.hpp"
#include "DefaultIntegrator-ComputeContext.hpp"
#include "LightPath.hpp"
#include "IntegratorState.hpp"
#include "../api/PixelData.hpp"
#include "../api/PixelLayout.hpp"
#include "../api/Scene.hpp"
#include "../api/Ray.hpp"
#include "../api/offsetRay.hpp"
#include "../light/AreaLight.hpp"
#include "../light/EnvironmentLight.hpp"
#include "../sampler/PrecomputedSample.hpp"
#include "../sampler/LightSample.hpp"
#include "../shape/Shape.hpp"
#include "../shape/DifferentialGeometry.hpp"
#include "../material/Medium.hpp"
#include "../material/Material.hpp"
#include "../subsurface/SubSurface.hpp"
#include "../brdf/CompositedBRDF.hpp"
#include "../../parm/Getters.hpp"
#include "../../parm/Values.hpp"

using namespace smks;

rndr::PixelData&
rndr::DefaultIntegrator::eval(Ray&                          ray,
                               Scene::Ptr const&            scene,
                               PixelData&                   pixel,
                               IntegratorState&             state,
                               IntegratorComputeContext*    contxt)
{
    ComputeContext* context = reinterpret_cast<ComputeContext*>(contxt);
    math::Vector4f  aoColor;    // temporary only relevant when ambient occlusion is activated

    assert(context);
    context->clear();
    pixel.clear();

    DifferentialGeometry    dg;
    LightPath               lp; // all fields explicitly initialized
    math::Vector4f&         pathL                           = pixel.rawRGBA();
    math::Vector4f          pathThroughput                  = math::Vector4f::Ones();
    Medium                  lastMedium                      = Medium::createVacuum();
    bool                    singularBounce                  = false;                    // when last bounce is not caused by singular bsdf
    bool                    trackShapeIdCoverage            = pixel.shapeIdCoverageRegistered();
    int                     lightForDirectSampleIDOffset    = _lightForDirectSampleId;

    // perform first intersection
    assert(ray.mask == PRIMARY_RAY);
    /**********************************************/
    rtcIntersect(scene->rtcScene(), (RTCRay&)ray);
    scene->postIntersect(ray, dg);
    ++state.numRays;
    /**********************************************/
    if (context->lpe())
        context->lpe()->push(LPE_CAMERA);

    while(true)
    {
        if (lp.rayDepth >= _maxRayDepth)
            break;
        if (math::max_xyz(pathThroughput) < _minRayContribution)
            break;

        math::Vector4f  rayL    = math::Vector4f::Zero();
        math::Vector4f  wo      = - ray.dir;

        //------------------------------------------------------------------------------------
        // ENVIRONMENT (nothing hit)
        //------------
        if (!ray)
        {
            /*
            if (_backplate &&
            (lp.rayDepth == 0 || onlyTransmission))
            rayL += _backplate->get(
            clamp(static_cast<int>(state.pixel.x * _backplate->width ), 0, static_cast<int>(_backplate->width )-1),
            clamp(static_cast<int>(state.pixel.y * _backplate->height), 0, static_cast<int>(_backplate->height)-1));

            else
            */

            if (lp.rayDepth == 0 || singularBounce)
                for (size_t i = 0; i < scene->numEnvironmentLights(); ++i)
                    rayL += scene->environmentLight(i)->Le(wo);
            if (_clamp2ndaryRays && !(ray.mask & PRIMARY_RAY))
                clampContribution(rayL);

            assert(!math::isnan(rayL));
            pathL += rayL.cwiseProduct(pathThroughput);

            if (context->lpe())
                matchLPE(pixel, pathL, context->lpe()->push(LPE_BACKGROUND));

            break; // quit recursion
        }
        assert(ray);
        assert(dg.shape);

        lp.rayLength    = ray.tfar;
        lp.pathLength   += lp.rayLength;

#if defined(FLIRT_TRACK_SHAPEIDS)
        if (dg.shape)
            state.shapeIDsAlongRay[lp.rayDepth] = dg.shape->scnId();
#endif // defined(FLIRT_TRACK_SHAPEIDS)

#if defined(DEBUG_AOV)
        if (aov && (ray.mask & PRIMARY_RAY))
        {
            aov->update(_aovHandles.debug_rayUv, ray.u, ray.v);
            if (_aovHandles.debug_rayGeomNormals)
                aov->update(_aovHandles.debug_rayGeomNormals, embree::normalize(ray.Ng), false);    // set
            aov->update(_aovHandles.debug_hitGeomNormals, dg.Ng, false);    // set
            aov->update(_aovHandles.debug_hitShadNormals, dg.Ns, false);    // set
        }
#endif // defined(DEBUG_AOV)

        // front-facing normals
        float facingRatio = math::dot3(dg.Ng, wo);
        if (facingRatio < 0.0f)
        {
            dg.Ng       = - dg.Ng;
            dg.Ns       = - dg.Ns;
            facingRatio = - facingRatio;
        }

        // intersected with emitter
        if (dg.light &&
            (lp.rayDepth == 0 || singularBounce))
        {
            rayL += dg.light->Le(dg, wo);
            if (_clamp2ndaryRays && !(ray.mask & PRIMARY_RAY))
                clampContribution(rayL);
        }

        // intersected with subsurface scattering object
        if (dg.subsurface)
        {
            rayL += dg.subsurface->Lo(dg, wo, lp, *scene);
            if (_clamp2ndaryRays && !(ray.mask & PRIMARY_RAY))
                clampContribution(rayL);
        }

        // shade surface
        CompositedBRDF brdfs;

        if (dg.material)
            dg.material->shade(ray, lp, lastMedium, dg, brdfs);

        // initialize 1st bounce AOV
        if (ray.mask & PRIMARY_RAY)
        {
            // set visible surfaces' renderpass data
            pixel.set(xchg::TEXCOORDS_PASS,             dg.st.x(), dg.st.y());
            pixel.set(xchg::POSITION_PASS,              dg.P);
            if (pixel.has(xchg::EYE_POSITION_PASS))
                pixel.set(xchg::EYE_POSITION_PASS,      math::transformPoint(state.worldToEye, dg.P));
            pixel.set(xchg::NORMAL_PASS,                dg.Ns);
            if (pixel.has(xchg::EYE_NORMAL_PASS))
                pixel.set(xchg::EYE_NORMAL_PASS,        math::transformVector(state.worldToEye, dg.Ns));
            pixel.set(xchg::TANGENT_PASS,               dg.Tx);
            if (pixel.has(xchg::EYE_TANGENT_PASS))
                pixel.set(xchg::EYE_TANGENT_PASS,       math::transformVector(state.worldToEye, dg.Tx));
            pixel.set(xchg::FACING_RATIO_PASS,          facingRatio);
            if (pixel.has(xchg::AMBIENT_OCCLUSION_PASS))
            {
                computeAmbientOcclusion(ray, dg, scene, state, context, aoColor);
                pixel.set(xchg::AMBIENT_OCCLUSION_PASS, aoColor);
            }
            if (pixel.has(xchg::DIFFUSE_COLOR_PASS))
                pixel.set(xchg::DIFFUSE_COLOR_PASS,     brdfs.color(DIFFUSE_BRDF));
            if (pixel.has(xchg::GLOSSY_COLOR_PASS))
                pixel.set(xchg::GLOSSY_COLOR_PASS,      brdfs.color(GLOSSY_BRDF));
            if (pixel.has(xchg::SINGULAR_COLOR_PASS))
                pixel.set(xchg::SINGULAR_COLOR_PASS,    brdfs.color(SINGULAR_BRDF));
        }

        //------------------------------------------------------------------------------------
        // NEXT EVENT ESTIMATION
        //----------------------
        const bool useDirectLighting = scene->totalNumLights() > 0 &&
            brdfs.has(~SINGULAR_BRDF);

        if (useDirectLighting)
        {
            // determine shadow ray offset and t-bounds
            const float shadowBias = math::max(1e-4f,
                dg.shape->asHairGeometry()
                ? 1.1f * dg.hairRadius
                : (dg.shape->shadowBias() < 1e-6f
                    ? _shadowBias
                    : dg.shape->shadowBias()));

            for (size_t i = 0; i < _numLightsForDirect; ++i)
            {
                const size_t lightId = _lightForDirectSampleId >= 0
                    ? static_cast<size_t>(floorf(scene->totalNumLights() * state.sample->getFloat1(lightForDirectSampleIDOffset + i)))
                    : i;

                if ((scene->light(lightId)->illumMask() & dg.illumMask) == 0)
                    continue;

                // Either use precomputed samples for the light or sample light now.
                LightSample ls;

                if (scene->light(lightId)->precompute())
                    ls   = state.sample->getLightSample(_precomputedLightSampleId[lightId]);
                else
                {
                    const math::Vector2f& s = state.sample->getFloat2(_lightSampleId);
                    ls.L = scene->light(lightId)->sample(scene->rtcScene(), dg, ls.wi, ls.tmax, s);
                }

                assert(!math::isnan(ls.wi));
                assert(!math::isnan(ls.L));

                // ignore math::Vector4f::Zero() radiance or illumination from the back [dot(dg.Ns,math::Vector4f(ls.wi)) <= 0.0f) ?].
                if (math::neq3(ls.L, math::Vector4f::Zero()) &&
                    ls.wi.pdf > 0.0f)
                {
                    // evaluate BRDF
                    Sample<RadianceClosure> cs;

                    brdfs.eval(wo, dg, ls.wi.value, ~SINGULAR_BRDF, cs);
                    assert(!math::isnan(cs));

                    if (cs.pdf > 0.0f &&
                        (math::neq3(cs.value.diffuseReflection,     math::Vector4f::Zero()) ||
                         math::neq3(cs.value.glossyReflection,      math::Vector4f::Zero()) ||
                         math::neq3(cs.value.diffuseTransmission,   math::Vector4f::Zero()) ||
                         math::neq3(cs.value.glossyTransmission,    math::Vector4f::Zero())))
                    {
                        math::Vector4f shadowColor = math::Vector4f::Ones();

                        if (dg.shape->recievesShadows())
                        {
                            // Test for shadows.
                            Ray shadowRay(SHADOW_RAY, dg.P, ls.wi.value, shadowBias, ls.tmax * (1.0f - 1e-3f), ray);

#if defined(_DEBUG)
                            /**
                            Ray ray3 = shadowRay;
                            rtcIntersect(scene->scene(), (RTCRay&)ray3);
                            if (ray3)
                            {
                            std::cout
                            << "[(" << ray3.geomID0 << " " << ray3.primID0 << " " << ray3.instID0 << ")]\t-X-> "
                            << "[(" << ray3.geomID << " " << ray3.primID << " " << ray3.instID << ")]\ttfar = " << ray3.tfar << std::endl;

                            //const rndr::SubdivisionSurface* subdiv = dynamic_cast<const rndr::SubdivisionSurface*>(dg.shape);
                            //if (subdiv)
                            //  subdiv->printPatch(ray.id1);

                            //DifferentialGeometry dg__3;
                            //scene->postIntersect(ray3, dg__3);
                            //const rndr::SubdivisionSurface* subdiv3 = dynamic_cast<const rndr::SubdivisionSurface*>(dg__3.shape);
                            //if (subdiv3)
                            //  subdiv3->printPatch(ray3.id1);

                            //std::cout << "\nP = " << dg.P << "\ntfar = " << ray3.tfar << "\nQ = " << dg.P + ray3.dir * ray3.tfar << std::endl;
                            }
                            **/
#endif // defined(_DEBUG_)

                            /* * * * * * * * * * * * * * * * * * * * * * */
                            rtcOccluded(scene->rtcScene(), (RTCRay&)shadowRay);
                            ++state.numShadowRays;
                            /* * * * * * * * * * * * * * * * * * * * * * */
                            shadowColor = shadowRay.transparency;

                            if ((ray.mask & PRIMARY_RAY) &&
                                shadowRay)
                            {
                                pixel.add(xchg::SHADOWS_PASS,       _rNumLightsForDirect);  // accumulate
                                pixel.min(xchg::SHADOW_BIAS_PASS,   shadowRay.tfar);        // accumulate
                            }
                        }

                        //if (!shadowRay)
                        if (math::neq3(shadowColor, math::Vector4f::Zero()))
                        {
                            const float brdfPdf = scene->light(lightId)->shape()
                                ? cs.pdf
                                : 0.0f;

                            ls.wi.pdf   *= _emitterPdf;
                            ls.L        *= math::rcp(ls.wi.pdf);
                            ls.L        *= miWeight(ls.wi.pdf, brdfPdf);
                            ls.L        = ls.L.cwiseProduct(shadowColor);
                            if (_clamp2ndaryRays)
                                clampContribution(ls.L);

                            if (context->lpe())
                            {
                                const LPEEventType lightEvt = scene->light(lightId)->shape()
                                    ? LPE_EMITTER
                                    : (scene->light(lightId)->asEnvironmentLight() ? LPE_BACKGROUND : LPE_LIGHT);

                                // direct diffuse reflection
                                matchLPE(pixel, pathL + ls.L.cwiseProduct(cs.value.diffuseReflection),
                                    context->lpe()->push(LPE_SURFACE_REFLECTION, LPE_DIFFUSE).push(lightEvt))
                                    .pop().pop();
                                // direct glossy reflection
                                matchLPE(pixel, pathL + ls.L.cwiseProduct(cs.value.glossyReflection),
                                    context->lpe()->push(LPE_SURFACE_REFLECTION, LPE_GLOSSY).push(lightEvt))
                                    .pop().pop();
                                // direct diffuse transmission
                                matchLPE(pixel, pathL + ls.L.cwiseProduct(cs.value.diffuseTransmission),
                                    context->lpe()->push(LPE_SURFACE_TRANSMISSION, LPE_DIFFUSE).push(lightEvt))
                                    .pop().pop();
                                // direct glossy transmission
                                matchLPE(pixel, pathL + ls.L.cwiseProduct(cs.value.glossyTransmission),
                                    context->lpe()->push(LPE_SURFACE_TRANSMISSION, LPE_GLOSSY).push(lightEvt))
                                    .pop().pop();
                            }

                            rayL += ls.L.cwiseProduct(
                                cs.value.diffuseReflection +
                                cs.value.glossyReflection +
                                cs.value.diffuseTransmission +
                                cs.value.glossyTransmission);
                        }
                    }
                }
            } // for (size_t i = 0; i < _numLightsForDirect; ++i)
        } // useDirectLighting

        //------------------------------------------------------------------------------------
        // BSDF SAMPLING
        //--------------
        Ray                     ray2;
        DifferentialGeometry    dg2;
        Medium                  nextMedium  = lastMedium;
        bool                    hitEmitter  = false;
        {
            Sample<math::Vector4f>  wi;
            BRDFType                type;
            const math::Vector2f&   s           = state.sample->getFloat2(_firstScatterSampleId     + lp.rayDepth);
            float                   ss          = state.sample->getFloat1(_firstScatterTypeSampleId + lp.rayDepth);
            math::Vector4f          bsdfColor   = brdfs.sample(wo, dg, wi, type, s, ss);

            assert(!math::isnan(wi));
            assert(math::isVector3f(wi.value));
            assert(!math::isnan(bsdfColor));

            singularBounce = (type & SINGULAR_BRDF) != 0;
            if (context->lpe())
                context->lpe()->push(type);

            // appending matte ID sample for current pixel
            if (trackShapeIdCoverage)
            {
                if (type & TRANSMISSION_BRDF)
                    context->pushShapeIdCoverage(dg.shape->scnId(), 1.0f - math::add_xyz(bsdfColor) * 0.333334f);
                else
                {
                    context->pushShapeIdCoverage(dg.shape->scnId(), 1.0f);
                    trackShapeIdCoverage = false;
                }
            }

            if (context)
                context->pushAlpha(type & TRANSMISSION_BRDF
                    ? 1.0f - math::add_xyz(bsdfColor) * 0.333334f
                    : 1.0f);

            if (type & TRANSMISSION_BRDF)
                nextMedium = dg.material->nextMedium(lastMedium); // Tracking medium if we hit a medium interface.

            if (wi.pdf > 0.0f)
            {
                math::Vector4f mediumAttenuation = math::neq3(lastMedium.transmission, math::Vector4f::Ones())
                    ? math::pow(lastMedium.transmission, ray.tfar)
                    : math::Vector4f::Ones();

                if (math::neq3(bsdfColor, math::Vector4f::Zero()))
                {
                    bsdfColor *= math::rcp(wi.pdf);

                    // Compute simple volumetric effect.
                    if (math::neq3(mediumAttenuation, math::Vector4f::Ones()))
                        bsdfColor = bsdfColor.cwiseProduct(mediumAttenuation);

                    // slightly offset ray's origin
                    dg.P = dg.shape && dg.shape->asHairGeometry()
                        ? offsetRay(dg.P, wi.value.dot(dg.Ns) > 0   ? dg.Ns : -dg.Ns,   1.1f * dg.hairRadius)
                        : offsetRay(dg.P, type & REFLECTION_BRDF    ? dg.Ns : -dg.Ns);

                    // shoot another ray by sampling the bsdf
                    ray2 = Ray(SECONDARY_RAY, dg.P, wi.value, 1e-3f, sys::inf, ray);
                    /*********************************************/
                    rtcIntersect(scene->rtcScene(), (RTCRay&)ray2);
                    scene->postIntersect(ray2, dg2);
                    ++state.numRays;

                    /*********************************************/

                    math::Vector4f  lightColor  = math::Vector4f::Zero();
                    float           lightPdf    = 0.0f;
                    if (ray2)
                    {
                        ++lp.rayDepth;

                        // hit emitter ?
                        if (dg2.light)
                        {
                            lightColor  = dg2.light->Le (dg2, -ray2.dir);
                            lightPdf    = dg2.light->pdf(dg2, dg);

                            hitEmitter  = true;
                            if (context->lpe())
                                context->lpe()->push(LPE_EMITTER);
                        }
                    }
                    else if (singularBounce)
                    {
                        /*
                        if (_backplate &&
                        onlyTransmission)
                        {
                        lightColor  = _backplate->get(
                        clamp(static_cast<int>(state.pixel.x * _backplate->width ), 0, static_cast<int>(_backplate->width )-1),
                        clamp(static_cast<int>(state.pixel.y * _backplate->height), 0, static_cast<int>(_backplate->height)-1));
                        lightPdf    = 1.0f;
                        hitEmitter  = true;
                        }
                        else
                        */
                        if (scene->numEnvironmentLights() > 0)
                        {
                            for (size_t i = 0; i < scene->numEnvironmentLights(); ++i)
                            {
                                lightColor  += scene->environmentLight(i)->Le(-ray2.dir);
                                lightPdf    += scene->environmentLight(i)->pdf(dg2, -ray2.dir);
                            }
                            hitEmitter  = true;
                            if (context->lpe())
                                context->lpe()->push(LPE_BACKGROUND);
                        }

                    }

                    assert(!math::isnan(lightColor));
                    assert(!math::isnan(lightPdf));

                    if (hitEmitter &&
                        lightPdf > 0.0f)
                    {
                        //if (singularBounce)
                        //  lightPdf = 0.0f;

                        rayL += bsdfColor.cwiseProduct(lightColor) * miWeight(wi.pdf, lightPdf);
                        if (_clamp2ndaryRays)
                            clampContribution(rayL);

                        if (context->lpe())
                            matchLPE(pixel, pathL + rayL.cwiseProduct(pathThroughput), *context->lpe())
                                .pop();
                    }
                }
            }
            assert(!math::isnan(rayL));

            pathL           += rayL         .cwiseProduct(pathThroughput);
            pathThroughput  = pathThroughput.cwiseProduct(bsdfColor);

        } // end of BSDF SAMPLING


        //------------------------------------------------------------------------------------
        // INDIRECT LIGHTING
        //------------------

        if (!ray2 ||
            hitEmitter)
        {
            if (dg2.light &&
                context)
                context->pushAlpha(1.0f);

            // appending matte ID sample for current pixel (early recursion quit)
            if (trackShapeIdCoverage && ray2)
            {
                assert(dg2.shape);
                context->pushShapeIdCoverage(dg2.shape->scnId(), 1.0f);
                trackShapeIdCoverage = false;
            }

            break; // quit recursion
        }

        ray         = ray2;
        dg          = dg2;
        lastMedium  = nextMedium;
    }
    // normalize cummulative contributions of all light path expression by the reciprocal of the number of matches.
    if (context->lpe())
        pixel.normalizeLPE();

    state.sumRayDepths += lp.rayDepth;
    assert(!math::isnan(pathL));

    return context->finalize(state, pixel); // handles alpha accumulation, light path expression normalization, copy to RGBA, backplate
}

__forceinline // static
float
rndr::DefaultIntegrator::miWeight(float pdfA, float pdfB)
{
    pdfA *= pdfA;
    pdfB *= pdfB;
    return pdfA * math::rcp(pdfA + pdfB);
}

__forceinline
math::Vector4f&
rndr::DefaultIntegrator::clampContribution(math::Vector4f& L) const
{
    const float avgL = math::add_xyz(L) * 0.333334f;

    if (avgL > _maxRayIntensity)
        L *= _maxRayIntensity * math::rcp(avgL);

    return L;
}

__forceinline // static
rndr::LightPathExpression&
rndr::DefaultIntegrator::matchLPE(PixelData& pData, const math::Vector4f& contrib, LightPathExpression& lpe)
{
    pData.matchLPE(lpe, contrib);
    return lpe;
}

//------------------------------------------------------------------------------------
// AMBIENT OCCLUSION (renderpass)
//------------------
void
rndr::DefaultIntegrator::computeAmbientOcclusion(const Ray&                 primaryRay,
                                                  const DifferentialGeometry&   dg,
                                                  Scene::Ptr const&             scene,
                                                  IntegratorState&              state,
                                                  ComputeContext*               contxt,
                                                  math::Vector4f&               ret) const
{
    // if specific user parameter has been designated to be used as
    // 'ambient occlusion color' on intersected primitive, then use
    // visibility as a scaling factor applied to it.
    getAmbientOcclusionColor(dg, ret);

    if (_ambientOcclusionDeactivated || !primaryRay)
        return; // full visibility

    assert(scene);
    assert(dg.shape);

    float visibility = 0.0f; // visibility = ratio of UNOCCLUDED rays

    // get intersected primitive's user parameter to be used as its color
    if (_ambientOcclusionColorIdx != parm::Getters::INVALID_IDX &&
        dg.userParms)
    {
        math::Vector4f aoColor;
        dg.userParms->get<math::Vector4f>(
            _ambientOcclusionColorIdx,
            aoColor,
            math::Vector4f::Ones());
    }


    // determine shadow ray offset and t-bounds
    math::Vector4f  P       = dg.P;
    float           tnear   = 1e-3f;
    if (dg.shape->asHairGeometry())
        tnear = 1.1f * dg.hairRadius;
    else
        P = offsetRay(dg.P, dg.Ns);

    const int sampleID = _ambientOcclusionSampleId + state.pixelSampleId * _numAmbientOcclusionRays;
    for (size_t i = 0; i < _numAmbientOcclusionRays; ++i)
    {
        math::Affine3f  xfm;

        math::frame(dg.Ns, xfm);
        const math::Vector4f& dir = math::transformVector(xfm, state.sample->getDirection(sampleID + i).value);

        Ray ambientOcclusionRay(SECONDARY_RAY, dg.P, dir, tnear, _maxAmbientOcclusionDistance, primaryRay);
        /* * * * * * * * * * * * * * * * * * * * * * */
        rtcOccluded(scene->rtcScene(), (RTCRay&)ambientOcclusionRay);
        ++state.numAmbientOcclusionRays;
        /* * * * * * * * * * * * * * * * * * * * * * */

        if (!ambientOcclusionRay)
            visibility += _rNumAmbientOcclusionRays;
    }
    // since directional samples are drawn according to a cosine-weighted pdf, there is
    // no need to account for the cosine term in the ambient occlusion definition
    // (importance sampling).

    if (math::abs(visibility - 1.0f) > 1e-3f)
        ret *= visibility;  // incorporate actual ambient occlusion to returned color
}

void
rndr::DefaultIntegrator::getAmbientOcclusionColor(
    const DifferentialGeometry& dg,
    math::Vector4f&             ret) const
{
    ret.setOnes();  // white by default

    // get intersected primitive's user parameter to be used as its color
    if (_ambientOcclusionColorIdx != parm::Getters::INVALID_IDX && dg.userParms)
        dg.userParms->get<math::Vector4f>(
            _ambientOcclusionColorIdx,
            ret,
            math::Vector4f::Ones());
}
