// ========================================================================= //
// Modification copyright 2016-2018 SUPAMONKS_STUDIO                         //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
// _________________________________________________________________________ //
// Copyright 2009-2013 Intel Corporation                                     //
//                                                                           //
// Licensed under the Apache License, Version 2.0 (the "License");           //
// you may not use this file except in compliance with the License.          //
// You may obtain a copy of the License at                                   //
//                                                                           //
//     http://www.apache.org/licenses/LICENSE-2.0                            //
//                                                                           //
// Unless required by applicable law or agreed to in writing, software       //
// distributed under the License is distributed on an "AS IS" BASIS,         //
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  //
// See the License for the specific language governing permissions and       //
// limitations under the License.                                            //
//                                                                           //
// ========================================================================= //
#pragma once

#include <smks/sys/Random.hpp>
#include <smks/math/types.hpp>

namespace smks { namespace rndr
{
    struct PrecomputedSample;

    //<! Each render job owns a separate integrator state instance.
    struct IntegratorState
    {
    public:
        sys::Random                 rng;                        //<! random number generator
        //--- current pixel --------
        const PrecomputedSample*    sample;                     //<! current sampler used to generate (pseudo) random numbers.
        math::Vector2i              raster;                     //<! current pixel's raster position
        math::Vector2f              pixel;                      //<! current pixel's normalized position
        size_t                      pixelSampleId;              //<! index of the current pixel sample.
        math::Affine3f              worldToEye;                 //<! camera's world- to eye-space transform.
        //--- accumulators----------
        size_t                      numRays;                    //<! Used to count the number of rays shot.
        size_t                      numShadowRays;              //<! Used to count the number of shadow rays shot.
        size_t                      numAmbientOcclusionRays;    //<! Used to count the number of ambient occlusion rays shot.
        size_t                      numRouletteKills;
        size_t                      sumRayDepths;

    public:
        inline
        IntegratorState():
            rng                     (),
            sample                  (nullptr),
            raster                  (0, 0),
            pixel                   (0.0f, 0.0f),
            pixelSampleId           (-1),
            worldToEye              (math::Affine3f::Identity()),
            numRays                 (0),
            numShadowRays           (0),
            numAmbientOcclusionRays (0),
            numRouletteKills        (0),
            sumRayDepths            (0)
        { }
    };
}
}
