// ========================================================================= //
// Modification copyright 2016-2018 SUPAMONKS_STUDIO                         //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
// _________________________________________________________________________ //
// Copyright 2009-2013 Intel Corporation                                     //
//                                                                           //
// Licensed under the Apache License, Version 2.0 (the "License");           //
// you may not use this file except in compliance with the License.          //
// You may obtain a copy of the License at                                   //
//                                                                           //
//     http://www.apache.org/licenses/LICENSE-2.0                            //
//                                                                           //
// Unless required by applicable law or agreed to in writing, software       //
// distributed under the License is distributed on an "AS IS" BASIS,         //
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  //
// See the License for the specific language governing permissions and       //
// limitations under the License.                                            //
//                                                                           //
// ========================================================================= //
#pragma once

#include <smks/xchg/Image.hpp>
#include "Integrator.hpp"

namespace smks { namespace rndr
{
    class   PixelData;
    class   LightPathExpression;
    struct  DifferentialGeometry;
    class   AmbientOcclusionPass;

    class DefaultIntegrator:
        public Integrator
    {
        VALID_RTOBJECT
    public:
        typedef sys::Ref<DefaultIntegrator> Ptr;
    private:
        typedef enum { MAX_NUM_LIGHTS_FOR_DIRECT = 16 };
        typedef enum { INVALID_SAMPLE_ID = -1 };

    private:
        class ComputeContext;

    private:
        size_t              _maxRayDepth;
        size_t              _maxNumLightsForDirect;         //<! upper bound on the number of lights involved in direct illumination (deactivated if equals to 0)
        float               _minRayContribution;            //<! intensity threshold below which ray is stopped
        float               _maxRayIntensity;               //<! clamping factor used to circumvent fireflies (deactivated if equals to 0)
        xchg::Image::WPtr   _backplate;
        // scene/framebuffer related parameters and precomputations
        size_t              _numLightsForDirect;
        float               _rNumLightsForDirect;
        float               _emitterPdf;                    //<! probability to pick a specific emitter from the scene
        float               _rEmitterPdf;
        float               _shadowBias;
        bool                _clamp2ndaryRays;               //<! true when the max ray intensity threshold has been set
        size_t              _numAmbientOcclusionRays;       //<! number of rays used to compute ambient occlusion
        float               _rNumAmbientOcclusionRays;
        float               _maxAmbientOcclusionDistance;   //<! maximal distance at which ambient occlusion rays travel in the scene (computation deactivated if = 0)
        size_t              _ambientOcclusionColorIdx;
        bool                _ambientOcclusionDeactivated;
        // percomputed random sample indices
        int                 _lightSampleId;                 //!< 2D random variable to sample the light source.
        std::vector<int>    _precomputedLightSampleId;      //!< ID of precomputed light samples for lights that need precomputations (indexed by light ID).
        int                 _firstScatterSampleId;          //!< 2D random variable to sample the BRDF.
        int                 _firstScatterTypeSampleId;      //!< 1D random variable to sample the BRDF type to choose.
        int                 _lightForDirectSampleId;        //!< 1D random variables used to pick lights for direct illumination (size = # lights for direct * max depth)
        int                 _ambientOcclusionSampleId;      //!< random direction

        CREATE_RTOBJECT(DefaultIntegrator, Integrator)
    protected:
        DefaultIntegrator(unsigned int scnId, const CtorArgs&);

    public:
        void
        initialize(sys::Ref<Scene> const&, sys::Ref<FrameBuffer> const&);

        /*! Allows the request optional user parameters prior scene primitive extraction. */
        void
        requestUserParametersFromPrimitives(parm::Getters&);
        /*! Allows the integrator to register required samples at the sampler. */
        void
        requestSamples(sys::Ref<SamplerFactory>&, sys::Ref<Scene> const&);

        void
        accept(const AmbientOcclusionPass&);

        PixelData&
        eval(Ray&, sys::Ref<Scene> const&, PixelData&, IntegratorState&, IntegratorComputeContext*);

        IntegratorComputeContext*
        createComputeContext(const FrameBuffer&);

        void
        dispose();

        void
        commitFrameState(const parm::Container&, const xchg::IdSet&);

        inline
        void
        beginSubframeCommits(size_t)
        { }

        inline
        void
        commitSubframeState(size_t, const parm::Container&)
        { }

        inline
        void
        endSubframeCommits()
        { }

        inline
        bool
        perSubframeParameter(unsigned int) const
        {
            return false;
        }

        __forceinline
        size_t
        maxHitsPerRay() const
        {
            return _maxRayDepth;
        }

        void
        getMetadata(img::OutputImage&) const;
    private:
        void
        disposeSampleIds();

        static __forceinline
        float
        miWeight(float, float);

        __forceinline
        math::Vector4f&
        clampContribution(math::Vector4f&) const;

        static __forceinline
        LightPathExpression&
        matchLPE(PixelData&, const math::Vector4f&, LightPathExpression&);

        void
        computeAmbientOcclusion(const Ray&,
                                const DifferentialGeometry&,
                                sys::Ref<Scene> const&,
                                IntegratorState&,
                                ComputeContext*,
                                math::Vector4f&) const;
        void
        getAmbientOcclusionColor(const DifferentialGeometry&,
                                 math::Vector4f&) const;
    };
}
}
