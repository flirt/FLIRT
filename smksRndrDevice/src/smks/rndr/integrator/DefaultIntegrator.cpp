// ========================================================================= //
// Modification copyright 2016-2018 SUPAMONKS_STUDIO                         //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
// _________________________________________________________________________ //
// Copyright 2009-2013 Intel Corporation                                     //
//                                                                           //
// Licensed under the Apache License, Version 2.0 (the "License");           //
// you may not use this file except in compliance with the License.          //
// You may obtain a copy of the License at                                   //
//                                                                           //
//     http://www.apache.org/licenses/LICENSE-2.0                            //
//                                                                           //
// Unless required by applicable law or agreed to in writing, software       //
// distributed under the License is distributed on an "AS IS" BASIS,         //
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  //
// See the License for the specific language governing permissions and       //
// limitations under the License.                                            //
//                                                                           //
// ========================================================================= //
#include <smks/sys/Log.hpp>
#include <smks/parm/BuiltIns.hpp>
#include <smks/math/math.hpp>
#include <smks/img/OutputImage.hpp>

#include "DefaultIntegrator.hpp"
#include "DefaultIntegrator-ComputeContext.hpp"
#include "../sampler/SamplerFactory.hpp"
#include "../api/Scene.hpp"
#include "../light/Light.hpp"
#include "../renderer/AmbientOcclusionPass.hpp"
#include "../../parm/Getters.hpp"


using namespace smks;

rndr::DefaultIntegrator::DefaultIntegrator(unsigned int scnId, const CtorArgs& args):
    Integrator                  (scnId, args),
    _maxRayDepth                (0),
    _maxNumLightsForDirect      (0),
    _minRayContribution         (0.0f),
    _maxRayIntensity            (0.0f),
    _backplate                  (),
    //---------------------------
    _numLightsForDirect         (0),
    _rNumLightsForDirect        (0.0f),
    _emitterPdf                 (1.0f),
    _rEmitterPdf                (1.0f),
    _shadowBias                 (-1.0f),
    _clamp2ndaryRays            (_maxRayIntensity > 0.0f),
    _numAmbientOcclusionRays    (0),
    _rNumAmbientOcclusionRays   (0.0f),
    _maxAmbientOcclusionDistance(0.0f),
    _ambientOcclusionColorIdx   (parm::Getters::INVALID_IDX),
    _ambientOcclusionDeactivated(true),
    //---------------------------
    _lightSampleId              (INVALID_SAMPLE_ID),
    _precomputedLightSampleId   (),
    _firstScatterSampleId       (INVALID_SAMPLE_ID),
    _firstScatterTypeSampleId   (INVALID_SAMPLE_ID),
    _lightForDirectSampleId     (INVALID_SAMPLE_ID),
    _ambientOcclusionSampleId   (INVALID_SAMPLE_ID)
{
    dispose();
}

void
rndr::DefaultIntegrator::initialize(sys::Ref<Scene> const&          scene,
                                     sys::Ref<FrameBuffer> const&   frameBuffer)
{
    assert(scene);
    assert(frameBuffer);

    _numLightsForDirect     = _maxNumLightsForDirect > 0
        ? std::min(scene->totalNumLights(), _maxNumLightsForDirect)
        : std::min(scene->totalNumLights(), static_cast<size_t>(MAX_NUM_LIGHTS_FOR_DIRECT));
    _rNumLightsForDirect    = _numLightsForDirect > 0
        ? math::rcp(static_cast<float>(_numLightsForDirect))
        : 0.0f;

    if (scene->totalNumLights() > 0 && _numLightsForDirect > 0)
    {
        _emitterPdf     = static_cast<float>(_numLightsForDirect) / static_cast<float>(scene->totalNumLights());
        _rEmitterPdf    = math::rcp(_emitterPdf);
    }

    // ambient occlusion parameters are set by the framebuffer itself with the FrameBuffer::configure(Renderer&) function.

    // compute shadow bias thanks to the scene's bounds
    const float     SHADOW_BIAS_RATIO = 0.0005f;
    math::Vector4f  minBounds, maxBounds;

    _shadowBias = scene->getWorldBounds(minBounds, maxBounds)
        ? math::max<float>(1e-4f, math::max_xyz(maxBounds - minBounds) * 0.25f * SHADOW_BIAS_RATIO)
        : SHADOW_BIAS_RATIO;
}

void
rndr::DefaultIntegrator::requestUserParametersFromPrimitives(parm::Getters& getters)
{ }

void
rndr::DefaultIntegrator::requestSamples(sys::Ref<SamplerFactory>&   samplerFactory,
                                         sys::Ref<Scene> const&     scene)
{
    assert(samplerFactory);
    assert(scene);

    FLIRT_LOG(LOG(DEBUG)
        << "\n--------------------------"
        << "\nintegrator sample requests\n"
        << "\n\t- max ray depth = " << _maxRayDepth
        << "\n\t- max # lights for direct = " << _maxNumLightsForDirect
        << "\n\t- min ray contrib = " << _minRayContribution
        << "\n\t- max ray intensity = " << _maxRayIntensity
        << "\n\n\t- # lights for direct = " << _numLightsForDirect
        << "\n\t- emitter pdf = " << _emitterPdf << "\t(rcp = " << _rEmitterPdf << ")"
        << "\n\t- shadow bias = " << _shadowBias
        << "\n\nambient occlusion\n"
        << "\n\t- # samples = " << _numAmbientOcclusionRays << "\t(rcp = " << _rNumAmbientOcclusionRays << ")"
        << "\n\t- max distance = " << _maxAmbientOcclusionDistance
        << "\n-------------------------";)

    disposeSampleIds();

    _lightSampleId              = samplerFactory->request2D(1);
    _precomputedLightSampleId.resize(scene->totalNumLights());
    for (size_t i = 0; i < scene->totalNumLights(); ++i)
        _precomputedLightSampleId[i] = scene->light(i)->precompute()
            ? samplerFactory->requestLightSample(_lightSampleId, scene->light(i))
            : INVALID_SAMPLE_ID;
    _firstScatterSampleId       = samplerFactory->request2D(_maxRayDepth);
    _firstScatterTypeSampleId   = samplerFactory->request1D(_maxRayDepth);
    _lightForDirectSampleId     = _numLightsForDirect > 0 && _numLightsForDirect < scene->totalNumLights()
        ? samplerFactory->request1D(_maxRayDepth * _numLightsForDirect)
        : INVALID_SAMPLE_ID;
    _ambientOcclusionSampleId   = _numAmbientOcclusionRays > 0
        ? samplerFactory->requestDirections(samplerFactory->numSamplesPerPixel() * _numAmbientOcclusionRays)
        : INVALID_SAMPLE_ID;
}

void
rndr::DefaultIntegrator::accept(const AmbientOcclusionPass& aoPass)
{
    _numAmbientOcclusionRays        = aoPass.numSamples();
    _maxAmbientOcclusionDistance    = aoPass.maxDistance();
    _ambientOcclusionColorIdx       = aoPass.userColorIdx();

    _rNumAmbientOcclusionRays       = _numAmbientOcclusionRays > 0
        ? math::rcp(static_cast<float>(_numAmbientOcclusionRays))
        : 0.0f;
    _ambientOcclusionDeactivated    = _numAmbientOcclusionRays == 0 || _maxAmbientOcclusionDistance < 1e-6f;
}

rndr::IntegratorComputeContext*
rndr::DefaultIntegrator::createComputeContext(const FrameBuffer& frameBuffer)
{
    return new ComputeContext(_maxRayDepth, _backplate.lock(), frameBuffer);
}

void
rndr::DefaultIntegrator::dispose()
{
    _backplate.reset();
    getBuiltIn<size_t>  (parm::maxRayDepth(),           _maxRayDepth);
    getBuiltIn<size_t>  (parm::numLightsForDirect(),    _maxNumLightsForDirect);
    getBuiltIn<float>   (parm::minRayContribution(),    _minRayContribution);
    getBuiltIn<float>   (parm::maxRayIntensity(),       _maxRayIntensity);

    _numLightsForDirect     = 0;
    _rNumLightsForDirect    = 0.0f;
    _emitterPdf             = 1.0f;
    _rEmitterPdf            = 1.0f;
    _shadowBias             = -1.0f;

    _numAmbientOcclusionRays        = 0;
    _rNumAmbientOcclusionRays       = 0.0f;
    _maxAmbientOcclusionDistance    = 0.0f;
    _ambientOcclusionDeactivated    = true;
    _ambientOcclusionColorIdx       = parm::Getters::INVALID_IDX;

    _clamp2ndaryRays    = _maxRayIntensity > 0.0f;

    disposeSampleIds();
}

void
rndr::DefaultIntegrator::disposeSampleIds()
{
    _lightSampleId              = INVALID_SAMPLE_ID;
    _precomputedLightSampleId.clear();
    _precomputedLightSampleId.shrink_to_fit();
    _firstScatterSampleId       = INVALID_SAMPLE_ID;
    _firstScatterTypeSampleId   = INVALID_SAMPLE_ID;
    _lightForDirectSampleId     = INVALID_SAMPLE_ID;
    _ambientOcclusionSampleId   = INVALID_SAMPLE_ID;
}

void
rndr::DefaultIntegrator::commitFrameState(const parm::Container& parms, const xchg::IdSet& dirties)
{
    logParameters(scnId(), parms, "builtIns", dirties);
    logParameters(scnId(), userParameters(), "userParms", "USER");
    //---
    for (xchg::IdSet::const_iterator id = dirties.begin(); id != dirties.end(); ++id)
        if      (*id == parm::maxRayDepth       ().id())    getBuiltIn<size_t>  (parm::maxRayDepth(),           _maxRayDepth,           &parms);
        else if (*id == parm::numLightsForDirect().id())    getBuiltIn<size_t>  (parm::numLightsForDirect(),    _maxNumLightsForDirect, &parms);
        else if (*id == parm::minRayContribution().id())    getBuiltIn<float>   (parm::minRayContribution(),    _minRayContribution,    &parms);
        else if (*id == parm::maxRayIntensity   ().id())    getBuiltIn<float>   (parm::maxRayIntensity(),       _maxRayIntensity,       &parms);
        else if (*id == parm::backplate         ().id())
        {
            xchg::ObjectPtr obj;
            getBuiltIn<xchg::ObjectPtr>(parm::backplate(), obj, &parms);
            _backplate = obj.shared_ptr<xchg::Image>();
        }

    _clamp2ndaryRays = _maxRayIntensity > 0.0f;
}

// virtual
void
rndr::DefaultIntegrator::getMetadata(img::OutputImage& frame) const
{
    frame.setMetadata("defaultIntegrator/maxRayDepth",              _maxRayDepth);
    frame.setMetadata("defaultIntegrator/maxNumLightsForDirect",    _maxNumLightsForDirect);
    frame.setMetadata("defaultIntegrator/minRayContribution",       _minRayContribution);
    frame.setMetadata("defaultIntegrator/maxRayIntensity",          _maxRayIntensity);
}
