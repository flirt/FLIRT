// ========================================================================= //
// Copyright 2016-2018 SUPAMONKS_STUDIO                                      //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
//                                                                           //
// ========================================================================= //
#pragma once

#include <string>
#include "../brdf/BRDFType.hpp"
#include "../light/Light.hpp"

namespace smks { namespace rndr
{
    enum LPEEventType
    {
        LPE_CAMERA                  = 'C',
        LPE_SURFACE_REFLECTION      = 'R',
        LPE_SURFACE_TRANSMISSION    = 'T',
        LPE_VOLUME                  = 'V',
        LPE_LIGHT                   = 'L',
        LPE_EMITTER                 = 'O',
        LPE_BACKGROUND              = 'B'
    };

    enum LPEEventScatteringType
    {
        LPE_DIFFUSE     = 'D',
        LPE_GLOSSY      = 'G',
        LPE_SINGULAR    = 'S',
        LPE_STRAIGHT    = 's',
        LPE_UNDEFINED   = 'x'
    };

    class LightPathExpression
    {
    private:
        size_t                  _maxCount;
        size_t                  _count;
        LPEEventType*           _event;
        LPEEventScatteringType* _eventScattering;
        std::string             _str;

    public:
        inline
        LightPathExpression():
            _maxCount       (0),
            _count          (0),
            _event          (nullptr),
            _eventScattering(nullptr),
            _str            ()
        { }

        explicit inline
        LightPathExpression(size_t maxCount):
            _maxCount       (0),
            _count          (0),
            _event          (nullptr),
            _eventScattering(nullptr),
            _str            ()
        {
            initialize(maxCount);
        }

        inline
        LightPathExpression(const LightPathExpression& other):
            _maxCount       (0),
            _count          (0),
            _event          (nullptr),
            _eventScattering(nullptr),
            _str            ()
        {
            copy(other);
        }

        inline
        ~LightPathExpression()
        {
            dispose();
        }

        inline
        LightPathExpression&
        operator=(const LightPathExpression& other)
        {
            if (this != &other)
                copy(other);
            return *this;
        }

        inline
        void
        dispose()
        {
            if (_event)             delete[] _event;            _event              = nullptr;
            if (_eventScattering)   delete[] _eventScattering;  _eventScattering    = nullptr;
            _str.clear(); _str.shrink_to_fit();
            _count      = 0;
            _maxCount   = 0;
        }

        inline
        void
        initialize(size_t maxCount)
        {
            dispose();
            if (maxCount == 0)
                return;
            _maxCount           = maxCount;
            _event              = new LPEEventType              [_maxCount];
            _eventScattering    = new LPEEventScatteringType    [_maxCount];

            memset(_event,              0,  sizeof(LPEEventType)            * _maxCount);
            memset(_eventScattering,    0,  sizeof(LPEEventScatteringType)  * _maxCount);

            _str.reserve((_maxCount << 2) + 1);
        }

    private:
        inline
        void
        copy(const LightPathExpression& other)
        {
            initialize(other._maxCount);
            _maxCount   = other._maxCount;
            _count      = other._count;
            memcpy(_event,              other._event,           sizeof(LPEEventType)            * _maxCount);
            memcpy(_eventScattering,    other._eventScattering, sizeof(LPEEventScatteringType)  * _maxCount);
            _str        = other._str;
        }

    public:
        inline
        LightPathExpression&
        clear()
        {
            _count = 0;
            _str.clear();
            return *this;
        }

        inline
        LightPathExpression&
        push(BRDFType brdf)
        {
            if      (brdf & DIFFUSE_REFLECTION_BRDF)    return push(LPE_SURFACE_REFLECTION,     LPE_DIFFUSE);
            else if (brdf & DIFFUSE_TRANSMISSION_BRDF)  return push(LPE_SURFACE_TRANSMISSION,   LPE_DIFFUSE);
            else if (brdf & GLOSSY_REFLECTION_BRDF)     return push(LPE_SURFACE_REFLECTION,     LPE_GLOSSY);
            else if (brdf & GLOSSY_TRANSMISSION_BRDF)   return push(LPE_SURFACE_TRANSMISSION,   LPE_GLOSSY);
            else if (brdf & SINGULAR_REFLECTION_BRDF)   return push(LPE_SURFACE_REFLECTION,     LPE_SINGULAR);
            else if (brdf & SINGULAR_TRANSMISSION_BRDF) return push(LPE_SURFACE_TRANSMISSION,   LPE_SINGULAR);
            else                                        return *this;
        }

        static inline
        LPEEventType
        getEvent(const Light& light)
        {
            if (light.shape())                      return LPE_EMITTER;
            else if (light.asEnvironmentLight())    return LPE_BACKGROUND;
            else                                    return LPE_LIGHT;
        }

        inline
        LightPathExpression&
        push(const Light& light)
        {
            return push(getEvent(light));
        }

        inline
        LightPathExpression&
        push(LPEEventType evt)
        {
            if (_count < _maxCount)
            {
                _event          [_count]    = evt;
                _eventScattering[_count]    = LPE_UNDEFINED;
                _str.push_back(static_cast<char>(evt));
                ++_count;
            }
            return *this;
        }

        inline
        LightPathExpression&
        push(LPEEventType evt, LPEEventScatteringType sct)
        {
            if (_count < _maxCount)
            {
                _event          [_count] = evt;
                _eventScattering[_count] = sct;
                _str.push_back('<');
                _str.push_back(static_cast<char>(evt));
                _str.push_back(static_cast<char>(sct));
                _str.push_back('>');
                ++_count;
            }
            return *this;
        }

        inline
        LightPathExpression&
        pop()
        {
            if (_count > 0)
            {
                if (_eventScattering[_count - 1] == LPE_UNDEFINED)
                    _str.pop_back();
                else
                {
                    _str.pop_back();
                    _str.pop_back();
                    _str.pop_back();
                    _str.pop_back();
                }
                --_count;
            }
            return *this;
        }

        inline
        operator std::string() const
        {
            return _str;
        }
    };
}
}
