// ========================================================================= //
// Copyright 2016-2018 SUPAMONKS_STUDIO                                      //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
//                                                                           //
// ========================================================================= //
#pragma once

#include <vector>
#include <smks/img/AbstractImage.hpp>

#include "IntegratorState.hpp"
#include "DefaultIntegrator.hpp"
#include "LightPathExpression.hpp"
#include "../api/FrameBuffer.hpp"
#include "../api/PixelData.hpp"

namespace smks { namespace rndr
{
    class DefaultIntegrator::ComputeContext:
        public IntegratorComputeContext
    {
    private:
        struct IdCoveragePair{ unsigned int id; float coverage; };
        typedef std::vector<IdCoveragePair> IdCoveragePairs;

    private:
        const img::AbstractImage*   _backplate; //<! does not own data
        std::vector<float>          _alpha;
        IdCoveragePairs             _shapeIdCoverage;
        LightPathExpression         *_lpe;

    public:
        __forceinline
        ComputeContext(unsigned int             maxRayDepth,
                       xchg::Image::Ptr const&  backplate,
                       const FrameBuffer&       frameBuffer):
            _backplate      (backplate ? backplate->getImage() : nullptr),
            _alpha          (),
            _shapeIdCoverage(),
            _lpe            (nullptr)
        {
            if (maxRayDepth > 0)
            {
                _alpha          .reserve(maxRayDepth);
                _shapeIdCoverage.reserve(maxRayDepth);
            }
            if (frameBuffer.maxIdCoverageLayers() > 0)
                _lpe = new LightPathExpression(maxRayDepth + 1);
        }
    private:
        // non-copyable
        ComputeContext(const ComputeContext&);
        ComputeContext& operator=(const ComputeContext&);

    public:
        __forceinline
        ~ComputeContext()
        {
            if (_lpe) delete _lpe;
        }

    public:
        __forceinline
        void
        clear()
        {
            _alpha          .clear();
            _shapeIdCoverage.clear();
            if (_lpe) _lpe  ->clear();
        }

        __forceinline
        void
        pushAlpha(float a)
        {
            _alpha.push_back(a);
        }

        __forceinline
        void
        pushShapeIdCoverage(unsigned int id, float coverage)
        {
            _shapeIdCoverage.push_back(IdCoveragePair());
            _shapeIdCoverage.back().id          = id;
            _shapeIdCoverage.back().coverage    = coverage;
        }

        __forceinline
        LightPathExpression*
        lpe() const
        {
            return _lpe;
        }

        __forceinline
        rndr::PixelData&
        finalize(const IntegratorState& state,
                 rndr::PixelData&       pixelData)
        {
            // - back-to-front alpha accumulation
            float alpha = _alpha.empty() ? 0.0f : 1.0f;
            if (_alpha.size() >= 2)
            {
                alpha = _alpha.back();
                for (int i = _alpha.size() - 2; i >= 0; --i)
                    alpha = alpha * (1.0f - _alpha[i]) + _alpha[i];
            }
            // - backplate
            if (alpha < 1.0f && _backplate)
            {
                const math::Vector4f& back = _backplate->pixel(
                    math::Vector2i(state.pixel.x() * _backplate->width(), state.pixel.y() * _backplate->height()),
                    false);
                pixelData.rawRGBA() *= alpha;
                pixelData.rawRGBA() += back * back[3] * (1.0f - alpha);
            }
            pixelData.rawRGBA().w() = alpha;
            // - record all shape id/coverage pairs
            for (size_t i = 0; i < _shapeIdCoverage.size(); ++i)
                pixelData.add(_shapeIdCoverage[i].id, _shapeIdCoverage[i].coverage);
            // - copy from raw RGBA to RGBA
            pixelData.RGBA() = pixelData.rawRGBA();
            return pixelData;
        }
    };
}
}
