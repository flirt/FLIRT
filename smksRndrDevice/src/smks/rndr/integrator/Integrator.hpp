// ========================================================================= //
// Modification copyright 2016-2018 SUPAMONKS_STUDIO                         //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
// _________________________________________________________________________ //
// Copyright 2009-2013 Intel Corporation                                     //
//                                                                           //
// Licensed under the Apache License, Version 2.0 (the "License");           //
// you may not use this file except in compliance with the License.          //
// You may obtain a copy of the License at                                   //
//                                                                           //
//     http://www.apache.org/licenses/LICENSE-2.0                            //
//                                                                           //
// Unless required by applicable law or agreed to in writing, software       //
// distributed under the License is distributed on an "AS IS" BASIS,         //
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  //
// See the License for the specific language governing permissions and       //
// limitations under the License.                                            //
//                                                                           //
// ========================================================================= //
#pragma once

#include "../ObjectBase.hpp"

namespace smks
{
    namespace parm
    {
        class Getters;
    }
    namespace rndr
    {
        class   SamplerFactory;
        class   Scene;
        class   FrameBuffer;
        class   PixelData;
        struct  Ray;
        struct  IntegratorState;
        class   IntegratorComputeContext { };   // opaque structure (defined par integrator)

        class Integrator:
            public ObjectBase
        {
        public:
            typedef sys::Ref<Integrator> Ptr;

        protected:
            Integrator(unsigned int scnId, const CtorArgs& args):
                ObjectBase(scnId, args)
            { }

        public:
            virtual
            void
            initialize(sys::Ref<Scene> const&,
                       sys::Ref<FrameBuffer> const&) = 0;

            /*! Allows the request optional user parameters prior scene primitive extraction. */
            virtual
            void
            requestUserParametersFromPrimitives(parm::Getters&) = 0;
            /*! Allows the integrator to register required samples at the sampler. */
            virtual
            void
            requestSamples(sys::Ref<SamplerFactory>&,
                           sys::Ref<Scene> const&) = 0;

            virtual
            PixelData&
            eval(Ray&, sys::Ref<Scene> const&, PixelData&, IntegratorState&, IntegratorComputeContext* = nullptr) = 0;

            virtual
            size_t
            maxHitsPerRay() const = 0;

            virtual
            IntegratorComputeContext*
            createComputeContext(const FrameBuffer&) = 0;
        };
    }
}
