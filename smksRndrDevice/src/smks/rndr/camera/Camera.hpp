// ========================================================================= //
// Modification copyright 2016-2018 SUPAMONKS_STUDIO                         //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
// _________________________________________________________________________ //
// Copyright 2009-2013 Intel Corporation                                     //
//                                                                           //
// Licensed under the Apache License, Version 2.0 (the "License");           //
// you may not use this file except in compliance with the License.          //
// You may obtain a copy of the License at                                   //
//                                                                           //
//     http://www.apache.org/licenses/LICENSE-2.0                            //
//                                                                           //
// Unless required by applicable law or agreed to in writing, software       //
// distributed under the License is distributed on an "AS IS" BASIS,         //
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  //
// See the License for the specific language governing permissions and       //
// limitations under the License.                                            //
//                                                                           //
// ========================================================================= //
#pragma once

#include <smks/sys/platform.hpp>
#include "../ObjectBase.hpp"

namespace smks
{
    namespace img
    {
        class OutputImage;
    }

    namespace rndr
    {
        struct Ray;

        class Camera:
            public ObjectBase
        {
        public:
            typedef sys::Ref<Camera> Ptr;

        protected:
            Camera(unsigned int scnId, const CtorArgs& args):
                ObjectBase(scnId, args)
            { }

        public:
            //<! Computes a primary ray for a pixel.
            virtual
            void
            ray(float                   time,   //!< Time expressed as a fraction of the duration of a frame.
                const math::Vector2f&   pixel,  //!< The pixel location on the on image plane in the range from 0 to 1.
                const math::Vector2f&   sample, //!< The lens sample in [0,1) for depth of field.
                Ray&) const = 0;

            virtual
            void
            getWorldToEyeSpaceMatrix(float, math::Affine3f&) const = 0;

            virtual
            void
            getMetadata(img::OutputImage&) const = 0;

            virtual
            bool
            ready() const = 0;
        protected:
            virtual
            void
            build() = 0;

            static inline
            void
            build(Camera& camera)
            {
                camera.build();
            }
        };
    }
}
