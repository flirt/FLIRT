// ========================================================================= //
// Copyright 2016-2018 SUPAMONKS_STUDIO                                      //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
//                                                                           //
// ========================================================================= //
#pragma once

#include <cassert>
#include <vector>

#include "../SubframeInterpolatorBase.hpp"
#include "PinHoleCamera.hpp"

namespace smks { namespace rndr
{
    class PinHoleCamera::MatrixInterpolator:
        public SubframeInterpolatorBase
    {
    private:
        // per subframe data
        std::vector<size_t> _worldXfmHash;
        math::Vector4fv     _worldTranslate;
        math::Quaternionfv  _worldRotate;
        math::Vector4fv     _worldScale;
        std::vector<float>  _yFieldOfView;  //<! in radians
        // per frame data
        float               _xAspectRatio;
        // precomputed tables
        math::Affine3fv     _worldToEye;    //<! size = tabSize
        math::Affine3fv     _eyeToWorld;    //<! size = tabSize
        math::Affine3fv     _pixelToWorld;  //<! size = tabSize

    public:
        MatrixInterpolator();

    protected:
        virtual
        void
        disposeSubframeData();
        virtual inline
        void
        disposeFrameData()
        { }
        virtual
        void
        disposePrecomputedTables();

        virtual
        void
        resizeSubframeData(size_t);
        virtual
        void
        resizePrecomputedTables(size_t);

        virtual
        void
        precomputeTabEntry(size_t tabEntry, size_t subframe);
        virtual
        void
        precomputeTabEntry(size_t tabEntry, size_t curSubframe, size_t nxtSubframe, float);

    protected:
        virtual
        std::ostream&
        printSubframeData(std::ostream&) const;
        virtual
        std::ostream&
        printTabEntries(std::ostream&) const;

    public:
        void
        setSubframeWorldTransformAndFieldOfView(size_t, const math::Affine3f&, float);  //<! vertical FOV in degrees
        void
        setAspectRatio(float);  //<! horizontal aspect ratio

        __forceinline
        const math::Affine3f&   //<! nearest precomputed table entry
        getWorldToEyeSpaceMatrix(float time) const
        {
            assert(areAllTabEntriesValid());
            return _worldToEye[getTabEntryIndex(time)];
        }
        __forceinline
        const math::Affine3f&   //<! nearest precomputed table entry
        getEyeToWorldSpaceMatrix(float time) const
        {
            assert(areAllTabEntriesValid());
            return _eyeToWorld[getTabEntryIndex(time)];
        }
        __forceinline
        const math::Affine3f&   //<! nearest precomputed table entry
        getPixelToWorldSpaceMatrix(float time) const
        {
            assert(areAllTabEntriesValid());
            return _pixelToWorld[getTabEntryIndex(time)];
        }

    protected:
        virtual inline
        bool
        isTabEntryValid(size_t i) const
        {
            assert(i < tabSize());
            return isValid(_worldToEye[i]) &&
                isValid(_eyeToWorld[i]) &&
                isValid(_pixelToWorld[i]);
        }

        virtual inline
        void
        invalidateTabEntry(size_t i)
        {
            assert(i < tabSize());
            invalidate(_worldToEye[i]);
            invalidate(_eyeToWorld[i]);
            invalidate(_pixelToWorld[i]);
        }

        const math::Affine3f&
        pixelToWorldSpaceMatrix(size_t i) const
        {
            assert(i < tabSize());
            return _pixelToWorld[i];
        }

    private:
        static __forceinline
        void
        invalidate(math::Affine3f& xfm)
        {
            xfm.matrix().data()[15] = 0.0f;
        }

        static __forceinline
        bool
        isValid(const math::Affine3f& xfm)
        {
            return math::abs(xfm.matrix().data()[15] - 1.0f) < 1e-6f;
        }

        static
        void
        computeWorldToEye(const math::Vector4f& translate,
                          const math::Quaternionf&,
                          const math::Vector4f& scale,
                          math::Affine3f& worldToEye,
                          math::Affine3f& eyeToWorld);
        static
        void
        computePixelToWorld(const math::Affine3f& eyeToWorld,
                            float yFieldOfView,
                            float xAspectRatio,
                            math::Affine3f&);
    };
}
}
