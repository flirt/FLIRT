// ========================================================================= //
// Copyright 2016-2018 SUPAMONKS_STUDIO                                      //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
//                                                                           //
// ========================================================================= //
#include <smks/sys/Log.hpp>
#include <smks/parm/BuiltIns.hpp>

#include "DepthOfFieldCamera-MatrixInterpolator.hpp"

using namespace smks;

rndr::DepthOfFieldCamera::MatrixInterpolator::MatrixInterpolator():
    PinHoleCamera::MatrixInterpolator(),
    _focalDistance      (),
    _worldFocalDistance ()
{
    invalidateAllTabEntries();
}

void
rndr::DepthOfFieldCamera::MatrixInterpolator::disposeSubframeData()
{
    PinHoleCamera::MatrixInterpolator::disposeSubframeData();
    //---
    _focalDistance      .clear();
    _focalDistance      .shrink_to_fit();
}

void
rndr::DepthOfFieldCamera::MatrixInterpolator::disposePrecomputedTables()
{
    PinHoleCamera::MatrixInterpolator::disposePrecomputedTables();
    //---
    _worldFocalDistance .clear();
    _worldFocalDistance .shrink_to_fit();
}

void
rndr::DepthOfFieldCamera::MatrixInterpolator::resizeSubframeData(size_t numSubframes)
{
    PinHoleCamera::MatrixInterpolator::resizeSubframeData(numSubframes);
    //---
    // get default value
    float parm1f;

    getBuiltIn<float>(parm::focalDistance(), parm1f);
    _focalDistance.resize(numSubframes, parm1f);
}

void
rndr::DepthOfFieldCamera::MatrixInterpolator::resizePrecomputedTables(size_t tabSize)
{
    PinHoleCamera::MatrixInterpolator::resizePrecomputedTables(tabSize);
    //---
    _worldFocalDistance.resize(tabSize);
}

void
rndr::DepthOfFieldCamera::MatrixInterpolator::setSubframeFocalDistance(
    size_t subframe, float focalDistance)
{
    assert(subframe < numSubframes());
    FLIRT_LOG(LOG(DEBUG)
        << "DOF camera matrix interpolation: "
        << subframe << " / " << numSubframes() << ": "
        << "focal dist = " << focalDistance;)

    if (math::abs(focalDistance - _focalDistance[subframe]) < 1e-4f)
        return;

    int iStart, iEnd;

    _focalDistance[subframe] = focalDistance;
    getInvalidatedTabEntryRange(subframe, iStart, iEnd);
    for (int i = iStart; i <= iEnd; ++i)
        invalidate(_worldFocalDistance[i]);
}

void
rndr::DepthOfFieldCamera::MatrixInterpolator::precomputeTabEntry(
    size_t tabEntry,
    size_t subframe)
{
    PinHoleCamera::MatrixInterpolator::precomputeTabEntry(tabEntry, subframe);
    //---
    if (!isValid(_worldFocalDistance[tabEntry]))
        _worldFocalDistance[tabEntry] = computeWorldFocalDistance(
            _focalDistance[subframe],
            pixelToWorldSpaceMatrix(tabEntry));
}

void
rndr::DepthOfFieldCamera::MatrixInterpolator::precomputeTabEntry(
    size_t  tabEntry,
    size_t  curSubframe,
    size_t  nxtSubframe,
    float   subframeBlend)
{
    PinHoleCamera::MatrixInterpolator::precomputeTabEntry(tabEntry, curSubframe, nxtSubframe, subframeBlend);
    //---
    if (!isValid(_worldFocalDistance[tabEntry]))
        _worldFocalDistance[tabEntry] = computeWorldFocalDistance(
            (1.0f - subframeBlend) * _focalDistance[curSubframe] + subframeBlend * _focalDistance[nxtSubframe],
            pixelToWorldSpaceMatrix(tabEntry));
}

// static
float
rndr::DepthOfFieldCamera::MatrixInterpolator::computeWorldFocalDistance(
    float                   focalDistance,
    const math::Affine3f&   pixelToWorld)
{
    const float len = math::transformVector(
        pixelToWorld,
        math::Vector4f(0.5f, 0.5f, 1.0f, 0.0f)).norm();
    return len > 0.0f
        ? focalDistance * math::rcp(len)
        : 0.0f;
}

std::ostream&
rndr::DepthOfFieldCamera::MatrixInterpolator::print(std::ostream& out) const
{
    out
        << "DOF camera matrix interpolation: "
        << numSubframes() << " subframe(s) "
        << "-> tabsize = " << tabSize() << "\n";
    out << "focal distance = [ ";
    for (size_t i = 0; i < numSubframes(); ++i)
        out << _focalDistance[i] << " ";
    out << "]";
    return out;
}
