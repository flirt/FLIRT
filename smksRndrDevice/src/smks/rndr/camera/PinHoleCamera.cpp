// ========================================================================= //
// Modification copyright 2016-2018 SUPAMONKS_STUDIO                         //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
// _________________________________________________________________________ //
// Copyright 2009-2013 Intel Corporation                                     //
//                                                                           //
// Licensed under the Apache License, Version 2.0 (the "License");           //
// you may not use this file except in compliance with the License.          //
// You may obtain a copy of the License at                                   //
//                                                                           //
//     http://www.apache.org/licenses/LICENSE-2.0                            //
//                                                                           //
// Unless required by applicable law or agreed to in writing, software       //
// distributed under the License is distributed on an "AS IS" BASIS,         //
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  //
// See the License for the specific language governing permissions and       //
// limitations under the License.                                            //
//                                                                           //
// ========================================================================= //
#include <algorithm>

#include <smks/sys/Log.hpp>
#include <smks/math/matrixAlgo.hpp>
#include <smks/math/math.hpp>
#include <smks/parm/BuiltIns.hpp>
#include <smks/img/OutputImage.hpp>
#include <std/to_string_xchg.hpp>

#include "PinHoleCamera.hpp"
#include "PinHoleCamera-MatrixInterpolator.hpp"

using namespace smks;

rndr::PinHoleCamera::PinHoleCamera(unsigned int scnId, const CtorArgs& args):
    Camera                  (scnId, args),
    _interpolator           (nullptr),
    _globalInitialization   (xchg::ACTIVE)
{
    FLIRT_LOG(LOG(DEBUG) << "constructs pinhole camera (node ID = " << scnId << ")";)
}

void
rndr::PinHoleCamera::build()
{
    _interpolator = new PinHoleCamera::MatrixInterpolator();
    dispose();
}

rndr::PinHoleCamera::~PinHoleCamera()
{
    delete _interpolator;
}

// virtual
void
rndr::PinHoleCamera::dispose()
{
    _interpolator->dispose();
    getBuiltIn<char>(parm::globalInitialization(), _globalInitialization);
}

// virtual
bool
rndr::PinHoleCamera::perSubframeParameter(unsigned int parmId) const
{
    return parmId == parm::worldTransform().id() ||
        parmId == parm::yFieldOfViewD().id();
}

// virtual
void
rndr::PinHoleCamera::beginSubframeCommits(size_t numSubframes)
{
    _interpolator->setNumSubframes(numSubframes);
}

// virtual
void
rndr::PinHoleCamera::commitSubframeState(size_t                 subframe,
                                          const parm::Container&    parms)
{
    math::Affine3f  worldTransform;
    float           yFieldOfViewD;

    getBuiltIn<math::Affine3f>  (parm::worldTransform(),    worldTransform, &parms);
    getBuiltIn<float>           (parm::yFieldOfViewD(),     yFieldOfViewD,  &parms);
    _interpolator->setSubframeWorldTransformAndFieldOfView(
        subframe, worldTransform, yFieldOfViewD);
}

// virtual
void
rndr::PinHoleCamera::endSubframeCommits()
{
#if defined(FLIRT_LOG_ENABLED)
#endif // defined(FLIRT_LOG_ENABLED)
}

// virtual
void
rndr::PinHoleCamera::commitFrameState(const parm::Container&    parms,
                                       const xchg::IdSet&       dirties)
{
    logParameters(scnId(), parms, "builtIns", dirties);
    logParameters(scnId(), userParameters(), "userParms", "USER");
    //---
    float parm1f = 0.0f;

    for (xchg::IdSet::const_iterator id = dirties.begin(); id != dirties.end(); ++id)
    {
        if (*id == parm::xAspectRatio().id())
        {
            getBuiltIn<float>(parm::xAspectRatio(), parm1f,  &parms);
            _interpolator->setAspectRatio(parm1f);
        }
    }
    //---
    _interpolator->precomputeTables();
}

void
rndr::PinHoleCamera::getWorldToEyeSpaceMatrix(float time, math::Affine3f& xfm) const
{
    xfm = _interpolator->getWorldToEyeSpaceMatrix(time);
}

void
rndr::PinHoleCamera::getEyeToWorldSpaceMatrix(float time, math::Affine3f& xfm) const
{
    xfm = _interpolator->getEyeToWorldSpaceMatrix(time);
}

void
rndr::PinHoleCamera::getPixelToWorldSpaceMatrix(float time, math::Affine3f& xfm) const
{
    xfm = _interpolator->getPixelToWorldSpaceMatrix(time);
}

// virtual
void
rndr::PinHoleCamera::getMetadata(img::OutputImage& frame) const
{
    math::Affine3f xfm;

    getWorldToEyeSpaceMatrix(0.0f, xfm);
    frame.setMetadata("worldToEyeSpaceMatrix", xfm);
}
