// ========================================================================= //
// Copyright 2016-2018 SUPAMONKS_STUDIO                                      //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
//                                                                           //
// ========================================================================= //
#include <smks/math/hash.hpp>
#include <smks/sys/constants.hpp>
#include <smks/sys/Log.hpp>
#include <smks/parm/BuiltIns.hpp>

#include "../ObjectBase.hpp"
#include "PinHoleCamera-MatrixInterpolator.hpp"

using namespace smks;

rndr::PinHoleCamera::MatrixInterpolator::MatrixInterpolator():
    SubframeInterpolatorBase(),
    _worldXfmHash   (),
    _worldTranslate (),
    _worldRotate    (),
    _worldScale     (),
    _yFieldOfView   (),
    _xAspectRatio   (0.0f),
    _worldToEye     (),
    _eyeToWorld     (),
    _pixelToWorld   ()
{
    invalidateAllTabEntries();
}

// virtual
void
rndr::PinHoleCamera::MatrixInterpolator::disposeSubframeData()
{
    _worldXfmHash   .clear();
    _worldXfmHash   .shrink_to_fit();
    _worldTranslate .clear();
    _worldTranslate .shrink_to_fit();
    _worldRotate    .clear();
    _worldRotate    .shrink_to_fit();
    _worldScale     .clear();
    _worldScale     .shrink_to_fit();
    _yFieldOfView   .clear();
    _yFieldOfView   .shrink_to_fit();

    getBuiltIn<float>(parm::xAspectRatio(), _xAspectRatio);
}

// virtual
void
rndr::PinHoleCamera::MatrixInterpolator::disposePrecomputedTables()
{
    _worldToEye     .clear();
    _worldToEye     .shrink_to_fit();
    _eyeToWorld     .clear();
    _eyeToWorld     .shrink_to_fit();
    _pixelToWorld   .clear();
    _pixelToWorld   .shrink_to_fit();
}

// virtual
void
rndr::PinHoleCamera::MatrixInterpolator::resizeSubframeData(size_t numSubframes)
{
    // get default values
    math::Affine3f      parmXfm;
    float               parm1f;
    math::Vector4f      translate, scale;
    math::Quaternionf   rotate;

    getBuiltIn<math::Affine3f>  (parm::worldTransform(),    parmXfm);
    getBuiltIn<float>           (parm::yFieldOfViewD(),     parm1f);
    math::decompose(parmXfm, translate, rotate, scale);

    _worldXfmHash   .resize(numSubframes, 0);
    _worldTranslate .resize(numSubframes, translate);
    _worldRotate    .resize(numSubframes, rotate);
    _worldScale     .resize(numSubframes, scale);
    _yFieldOfView   .resize(numSubframes, 0.0f);
}

// virtual
void
rndr::PinHoleCamera::MatrixInterpolator::resizePrecomputedTables(size_t tabSize)
{
    _worldToEye     .resize(tabSize);
    _eyeToWorld     .resize(tabSize);
    _pixelToWorld   .resize(tabSize);
}


void
rndr::PinHoleCamera::MatrixInterpolator::setSubframeWorldTransformAndFieldOfView(
    size_t                  subframe,
    const math::Affine3f&   worldTransform,
    float                   yFieldOfViewD)
{
    assert(subframe < numSubframes());
    FLIRT_LOG(LOG(DEBUG)
        << "pinhole camera matrix interpolation: "
        << subframe << " / " << numSubframes() << "\n"
        << "\t- y-fov = " << yFieldOfViewD << " (degrees)\n"
        << "\t- world xfm = \n " << worldTransform.matrix();)

    bool invalidateWorldToEye   = false;
    bool invalidatePixelToWorld = false;

    const size_t    worldXfmHash    = math::hash(worldTransform);
    const float     yFieldOfView    = yFieldOfViewD * static_cast<float>(sys::degrees_to_radians);

    if (_worldXfmHash[subframe] != worldXfmHash)
    {
        _worldXfmHash[subframe] = worldXfmHash;
        math::decompose(
            worldTransform,
            _worldTranslate[subframe],
            _worldRotate[subframe],
            _worldScale[subframe]);
        //---
        invalidateWorldToEye    = true;
        invalidatePixelToWorld  = true;
    }

    if (math::abs(_yFieldOfView[subframe] - yFieldOfView) > 1e-4f)
    {
        _yFieldOfView[subframe] = yFieldOfView;
        //---
        invalidatePixelToWorld  = true;
    }

    if (invalidateWorldToEye || invalidatePixelToWorld)
    {
        int iStart, iEnd;

        getInvalidatedTabEntryRange(subframe, iStart, iEnd);
        if (invalidateWorldToEye)
            for (int i = iStart; i <= iEnd; ++i)
                invalidateTabEntry(i);  // -> pixel to world matrices are invalidated too
        else if (invalidatePixelToWorld)
            for (int i = iStart; i <= iEnd; ++i)
                invalidate(_pixelToWorld[i]);
    }
}

void
rndr::PinHoleCamera::MatrixInterpolator::setAspectRatio(float value)
{
    if (math::abs(_xAspectRatio - value) < 1e-4f)
        return;
    _xAspectRatio = value;
    //---
    for (size_t i = 0; i < _pixelToWorld.size(); ++i)
        invalidate(_pixelToWorld[i]);
}

// virtual
std::ostream&
rndr::PinHoleCamera::MatrixInterpolator::printSubframeData(std::ostream& out) const
{
    out
        << "pinhole camera matrix interpolation: "
        << numSubframes() << " subframe(s) "
        << "-> tabsize = " << tabSize() << "\n"
        << "---\n"
        << "- x-aspect ratio = " << _xAspectRatio << "\n"
        << "---\n";
    for (size_t i = 0; i < numSubframes(); ++i)
        out
            << "--- subframe #" << i << " ---\n"
            << "\t- y-fov = " << _yFieldOfView[i] << "\n"
            << "\t- xfm hash = " << _worldXfmHash[i] << "\n"
            << "\t- translate = (" << _worldTranslate[i].transpose() << ")\n"
            << "\t- rotate =\n" << _worldRotate[i].matrix() << "\n"
            << "\t- scale = (" << _worldScale[i].transpose() << ")\n";
    out << "\n### invalid table entries: {";
    for (size_t i = 0; i < tabSize(); ++i)
        if (!isTabEntryValid(i))
            out << " " << i;
    out << " }\n";
    return out;
}

// virtual
std::ostream&
rndr::PinHoleCamera::MatrixInterpolator::printTabEntries(std::ostream& out) const
{
    out
        << "pinhole camera matrix interpolation: "
        << numSubframes() << " subframe(s) "
        << "-> tabsize = " << tabSize() << "\n";
    for (size_t i = 0; i < tabSize(); ++i)
        out
            << "--- table entry #" << i << " ---\n"
            << "\t- world->eye =\n" << _worldToEye[i].matrix() << "\n"
            << "\t- eye->world =\n" << _eyeToWorld[i].matrix() << "\n"
            << "\t- pixel->world =\n" << _pixelToWorld[i].matrix() << "\n";
    return out;
}

void
rndr::PinHoleCamera::MatrixInterpolator::precomputeTabEntry(
    size_t tabEntry,
    size_t subframe)
{
    if (!isValid(_worldToEye[tabEntry]))
        computeWorldToEye(
            _worldTranslate[subframe],
            _worldRotate[subframe],
            _worldScale[subframe],
            _worldToEye[tabEntry],
            _eyeToWorld[tabEntry]);

    if (!isValid(_pixelToWorld[tabEntry]))
        computePixelToWorld(
            _eyeToWorld[tabEntry],
            _yFieldOfView[subframe],
            _xAspectRatio,
            _pixelToWorld[tabEntry]);
}

void
rndr::PinHoleCamera::MatrixInterpolator::precomputeTabEntry(
    size_t tabEntry,
    size_t curSubframe,
    size_t nxtSubframe,
    float  subframeBlend)
{
    const float w1 = subframeBlend;
    const float w0 = 1.0f - w1;

    if (!isValid(_worldToEye[tabEntry]))
        computeWorldToEye(
            w0 * _worldTranslate[curSubframe] + w1 * _worldTranslate[nxtSubframe],
            _worldRotate[curSubframe].slerp(w1, _worldRotate[nxtSubframe]).normalized(),
            w0 * _worldScale[curSubframe] + w1 * _worldScale[nxtSubframe],
            _worldToEye[tabEntry],
            _eyeToWorld[tabEntry]);

    if (!isValid(_pixelToWorld[tabEntry]))
        computePixelToWorld(
            _eyeToWorld[tabEntry],
            w0 * _yFieldOfView[curSubframe] + w1 * _yFieldOfView[nxtSubframe],
            _xAspectRatio,
            _pixelToWorld[tabEntry]);
}

// static
void
rndr::PinHoleCamera::MatrixInterpolator::computeWorldToEye(const math::Vector4f&        translate,
                                                            const math::Quaternionf&    rotate,
                                                            const math::Vector4f&       scale,
                                                            math::Affine3f&             worldToEye,
                                                            math::Affine3f&             eyeToWorld)
{
    math::compose(eyeToWorld, translate, rotate, scale);
    worldToEye = eyeToWorld.inverse();
}

// static
void
rndr::PinHoleCamera::MatrixInterpolator::computePixelToWorld(const math::Affine3f&  eyeToWorld,
                                                              float                 yFieldOfView,
                                                              float                 xAspectRatio,
                                                              math::Affine3f&       pixelToWorld)
{
    const math::Vector4f& w = math::transformVector(
        eyeToWorld,
        0.5f * math::Vector4f(-xAspectRatio, -1.0f, -math::rcp(math::tan(0.5f * yFieldOfView)), 0.0f));

    pixelToWorld.matrix().col(0) << eyeToWorld.matrix().col(0) * xAspectRatio;
    pixelToWorld.matrix().col(1) << eyeToWorld.matrix().col(1);
    pixelToWorld.matrix().col(2) << w;
    pixelToWorld.matrix().col(3) << eyeToWorld.matrix().col(3);
    pixelToWorld.makeAffine();
}


