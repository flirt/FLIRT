// ========================================================================= //
// Copyright 2016-2018 SUPAMONKS_STUDIO                                      //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
//                                                                           //
// ========================================================================= //
#pragma once

#include "DepthOfFieldCamera.hpp"
#include "PinHoleCamera-MatrixInterpolator.hpp"

namespace smks { namespace rndr
{
    class DepthOfFieldCamera::MatrixInterpolator:
        public PinHoleCamera::MatrixInterpolator
    {
    private:
        // per subframe data
        std::vector<float>  _focalDistance;
        // precomputed tables
        std::vector<float>  _worldFocalDistance;
    public:
        MatrixInterpolator();

    protected:
        void
        disposeSubframeData();
        void
        disposePrecomputedTables();

        void
        resizeSubframeData(size_t);
        void
        resizePrecomputedTables(size_t);

        void
        precomputeTabEntry(size_t tabEntry, size_t subframe);
        void
        precomputeTabEntry(size_t tabEntry, size_t curSubframe, size_t nxtSubframe, float);
    public:
        void
        setSubframeFocalDistance(size_t, float);

    protected:
        inline
        bool
        isTabEntryValid(size_t i) const
        {
            return
                PinHoleCamera::MatrixInterpolator::isTabEntryValid(i) &&
                isValid(_worldFocalDistance[i]);
        }

        inline
        void
        invalidateTabEntry(size_t i)
        {
            PinHoleCamera::MatrixInterpolator::invalidateTabEntry(i);
            //---
            invalidate(_worldFocalDistance[i]);
        }

        inline
        bool
        isValid(float worldFocalDistance) const
        {
            return !(worldFocalDistance < 0.0f);
        }
        inline
        void
        invalidate(float& worldFocalDistance)
        {
            worldFocalDistance = -1.0f;
        }
    private:
        std::ostream&
        print(std::ostream&) const;
    private:
        static
        float
        computeWorldFocalDistance(float, const math::Affine3f&);
    };
}
}
