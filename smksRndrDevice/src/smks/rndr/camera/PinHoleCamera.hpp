// ========================================================================= //
// Modification copyright 2016-2018 SUPAMONKS_STUDIO                         //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
// _________________________________________________________________________ //
// Copyright 2009-2013 Intel Corporation                                     //
//                                                                           //
// Licensed under the Apache License, Version 2.0 (the "License");           //
// you may not use this file except in compliance with the License.          //
// You may obtain a copy of the License at                                   //
//                                                                           //
//     http://www.apache.org/licenses/LICENSE-2.0                            //
//                                                                           //
// Unless required by applicable law or agreed to in writing, software       //
// distributed under the License is distributed on an "AS IS" BASIS,         //
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  //
// See the License for the specific language governing permissions and       //
// limitations under the License.                                            //
//                                                                           //
// ========================================================================= //
#pragma once

#include <smks/math/matrixAlgo.hpp>
#include "Camera.hpp"
#include "../api/Ray.hpp"

namespace smks { namespace rndr
{
    class PinHoleCamera:
        public Camera
    {
    protected:
        class MatrixInterpolator;

        VALID_RTOBJECT
    protected:
        MatrixInterpolator* _interpolator;
    private:
        char                _globalInitialization;

    public:
        static inline
        sys::Ref<Camera>
        create(unsigned int scnId, const CtorArgs& args)
        {
            sys::Ref<Camera> ptr (new PinHoleCamera(scnId, args));
            Camera::build(*ptr);
            return ptr;
        }
    protected:
        PinHoleCamera(unsigned int scnId, const CtorArgs&);

        virtual
        void
        build();
    public:
        ~PinHoleCamera();   // deallocate interpolator

        virtual
        void
        dispose();

        virtual
        bool
        perSubframeParameter(unsigned int) const;

        virtual
        void
        beginSubframeCommits(size_t);
        virtual
        void
        commitSubframeState(size_t, const parm::Container&);
        virtual
        void
        endSubframeCommits();
        virtual
        void
        commitFrameState(const parm::Container&, const xchg::IdSet&);

        virtual inline
        void
        ray(float                   time,       //!< Time expressed as a fraction of the duration of a frame.
            const math::Vector2f&   pixel,      //!< The pixel location on the on image plane in the range from 0 to 1.
            const math::Vector2f&   sample,     //!< The lens sample in [0,1) for depth of field.
            Ray&                    ray) const
        {
            math::Affine3f pixelToWorld;

            getPixelToWorldSpaceMatrix(time, pixelToWorld);

            const math::Vector4f&   rayOrg = pixelToWorld.matrix().col(3);
            math::Vector4f          rayDir = math::transformVector(
                pixelToWorld,
                math::Vector4f(pixel.x(), pixel.y(), 1.0f, 0.0f));
            math::normalize3(rayDir);   // the ray direction must be normalized, not much for the intersection computation, but for shading

            new (&ray) Ray(RayType::PRIMARY_RAY, rayOrg, rayDir);
        }

        void
        getWorldToEyeSpaceMatrix(float, math::Affine3f&) const;

        inline
        bool
        ready() const
        {
            return _globalInitialization == xchg::ACTIVE;
        }

        void
        getMetadata(img::OutputImage&) const;

    protected:
        void
        getEyeToWorldSpaceMatrix(float, math::Affine3f&) const;
        void
        getPixelToWorldSpaceMatrix(float, math::Affine3f&) const;

        size_t
        tabSize() const;
        const math::Affine3f&
        pixelToWorldSpaceMatrix(size_t) const;
    };
}
}
