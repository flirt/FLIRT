// ========================================================================= //
// Modification copyright 2016-2018 SUPAMONKS_STUDIO                         //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
// _________________________________________________________________________ //
// Copyright 2009-2013 Intel Corporation                                     //
//                                                                           //
// Licensed under the Apache License, Version 2.0 (the "License");           //
// you may not use this file except in compliance with the License.          //
// You may obtain a copy of the License at                                   //
//                                                                           //
//     http://www.apache.org/licenses/LICENSE-2.0                            //
//                                                                           //
// Unless required by applicable law or agreed to in writing, software       //
// distributed under the License is distributed on an "AS IS" BASIS,         //
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  //
// See the License for the specific language governing permissions and       //
// limitations under the License.                                            //
//                                                                           //
// ========================================================================= //
#include <cassert>

#include <smks/sys/Log.hpp>
#include <smks/math/matrixAlgo.hpp>
#include <smks/parm/BuiltIns.hpp>

#include "DepthOfFieldCamera.hpp"
#include "DepthOfFieldCamera-MatrixInterpolator.hpp"

using namespace smks;

rndr::DepthOfFieldCamera::DepthOfFieldCamera(unsigned int scnId, const CtorArgs& args):
    PinHoleCamera   (scnId, args),
    _lensRadius     (0.0f),
    _focalDistance  ()
{
    FLIRT_LOG(LOG(DEBUG) << "constructs depth-of-field camera (node ID = " << scnId << ")";)
}

void
rndr::DepthOfFieldCamera::build()
{
    _interpolator = new DepthOfFieldCamera::MatrixInterpolator();
    dispose();
}

void
rndr::DepthOfFieldCamera::dispose()
{
    getBuiltIn<float>(parm::lensRadius(), _lensRadius);
    _focalDistance.clear();
    _focalDistance.shrink_to_fit();
    //---
    PinHoleCamera::dispose();
}

bool
rndr::DepthOfFieldCamera::perSubframeParameter(unsigned int parmId) const
{
    return PinHoleCamera::perSubframeParameter(parmId) ||
        parmId == parm::focalDistance().id();
}

void
rndr::DepthOfFieldCamera::beginSubframeCommits(size_t numSubframes)
{
    PinHoleCamera::beginSubframeCommits(numSubframes);
    //---
    float focalDistance;

    getBuiltIn<float>(parm::focalDistance(), focalDistance);
    _focalDistance.resize(numSubframes, focalDistance);
}

void
rndr::DepthOfFieldCamera::commitSubframeState(size_t                    subframe,
                                               const parm::Container&   parms)
{
    PinHoleCamera::commitSubframeState(subframe, parms);
    //---
    getBuiltIn<float>(parm::focalDistance(), _focalDistance[subframe]);
}

void
rndr::DepthOfFieldCamera::commitFrameState(const parm::Container&   parms,
                                            const xchg::IdSet&      dirties)
{
    PinHoleCamera::commitFrameState(parms, dirties);
    //---
    for (xchg::IdSet::const_iterator id = dirties.begin(); id != dirties.end(); ++id)
    {
        if (*id == parm::lensRadius().id())
            getBuiltIn<float>(parm::lensRadius(), _lensRadius, &parms);
    }
}
