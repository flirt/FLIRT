// ========================================================================= //
// Modification copyright 2016-2018 SUPAMONKS_STUDIO                         //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
// _________________________________________________________________________ //
// Copyright 2009-2013 Intel Corporation                                     //
//                                                                           //
// Licensed under the Apache License, Version 2.0 (the "License");           //
// you may not use this file except in compliance with the License.          //
// You may obtain a copy of the License at                                   //
//                                                                           //
//     http://www.apache.org/licenses/LICENSE-2.0                            //
//                                                                           //
// Unless required by applicable law or agreed to in writing, software       //
// distributed under the License is distributed on an "AS IS" BASIS,         //
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  //
// See the License for the specific language governing permissions and       //
// limitations under the License.                                            //
//                                                                           //
// ========================================================================= //
#pragma once

#include "PinHoleCamera.hpp"
#include "../sampler/ShapeSampler.hpp"

namespace smks { namespace rndr
{
    class DepthOfFieldCamera:
        public PinHoleCamera
    {
    private:
        class MatrixInterpolator;

        VALID_RTOBJECT
    private:
        float               _lensRadius;    //!< radius of the lens
        std::vector<float>  _focalDistance; //!< distance of the focal plane

    public:
        static inline
        sys::Ref<Camera>
        create(unsigned int scnId, const CtorArgs& args)
        {
            sys::Ref<Camera> ptr (new DepthOfFieldCamera(scnId, args));
            Camera::build(*ptr);
            return ptr;
        }
    protected:
        DepthOfFieldCamera(unsigned int scnId, const CtorArgs&);

        virtual
        void
        build();
    public:
        void
        dispose();

        bool
        perSubframeParameter(unsigned int) const;
        void
        beginSubframeCommits(size_t);
        void
        commitSubframeState(size_t, const parm::Container&);
        void
        commitFrameState(const parm::Container&, const xchg::IdSet&);

        inline
        void
        ray(float                   time,       //!< Time expressed as a fraction of the duration of a frame.
            const math::Vector2f&   pixel,      //!< The pixel location on the on image plane in the range from 0 to 1.
            const math::Vector2f&   sample,     //!< The lens sample in [0,1) for depth of field.
            Ray&                    ray) const
        {
            math::Affine3f localToWorld, pixelToWorld;

            getEyeToWorldSpaceMatrix    (time, localToWorld);
            getPixelToWorldSpaceMatrix  (time, pixelToWorld);

            const math::Vector4f&   rayOrg  = pixelToWorld.matrix().col(3);
            const math::Vector2f&   lens    = uniformSampleDisk(sample, _lensRadius);
            const math::Vector4f&   begin   = math::transformPoint(
                localToWorld,
                math::Vector4f(lens.x(), lens.y(), 0.0f, 1.0f));
            const math::Vector4f&   end     = rayOrg +
                getFocalDistance(time) * math::transformVector(
                    pixelToWorld,
                    math::Vector4f(pixel.x(), 1.0f - pixel.y(), 1.0f, 0.0f));

            new (&ray) Ray(RayType::PRIMARY_RAY, begin, (end - begin).normalized());
        }

    protected:
        inline
        float
        getFocalDistance(float time) const
        {
            assert(!_focalDistance.empty());
            const size_t idx = _focalDistance.size() == 1
                ? 0
                : static_cast<size_t>(
                    math::clamp<int>(
                        static_cast<int>(math::floor(time * _focalDistance.size())),
                        0, _focalDistance.size()-1));
            return _focalDistance[idx];
        }
    };
}
}
