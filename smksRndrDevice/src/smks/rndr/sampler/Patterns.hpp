// ========================================================================= //
// Modification copyright 2016-2018 SUPAMONKS_STUDIO                         //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
// _________________________________________________________________________ //
// Copyright 2009-2013 Intel Corporation                                     //
//                                                                           //
// Licensed under the Apache License, Version 2.0 (the "License");           //
// you may not use this file except in compliance with the License.          //
// You may obtain a copy of the License at                                   //
//                                                                           //
//     http://www.apache.org/licenses/LICENSE-2.0                            //
//                                                                           //
// Unless required by applicable law or agreed to in writing, software       //
// distributed under the License is distributed on an "AS IS" BASIS,         //
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  //
// See the License for the specific language governing permissions and       //
// limitations under the License.                                            //
//                                                                           //
// ========================================================================= //
#pragma once

#include <vector>

#include <smks/math/math.hpp>
#include <smks/math/types.hpp>
#include <smks/xchg/Array2D.hpp>
#include <smks/sys/Permutation.hpp>

namespace smks { namespace sys
{
    /*! Create a set of n jittered 1D samples, using the provided
    *  random number generator. */
    __forceinline
    void
    jittered(float* samples, uint32 n, Random& rng)
    {
        assert(n > 0);
        const float scale   = math::rcp(static_cast<float>(n));
        Permutation perm    (n, rng);

        for (uint32 i = 0; i < n; i++)
            samples[perm[i]] = std::min(1.0f - 1e-6f, (static_cast<float>(i) + rng.getFloat()) * scale);
    }

    /*! Create a set of n multi-jittered 2D samples, using the provided
    *  random number generator. */
    __forceinline
    void
    multiJittered(math::Vector2f* samples, uint32 N, Random& rng)
    {
        assert(N > 0);

        uint32 b = (uint32)sqrtf(static_cast<float>(N));
        if (b*b < N)
            b++;

        xchg::Array2D<math::Vector2f>   grid    (b, b);
        std::vector<uint32>             numbers (b);
        for (uint32 i = 0; i < b; i++)
            numbers[i] = i;
        assert(!numbers.empty());

        for (uint32 i = 0; i < b; i++)
        {
            shuffle(numbers.size(), &numbers.front(), rng);
            for (uint32 j = 0; j < b; j++)
                static_cast<math::Vector2f**>(grid)[i][j][0] = std::min(1.0f - 1e-6f,
                    static_cast<float>(i) / static_cast<float>(b) + (numbers[j] + rng.getFloat()) / static_cast<float>(b*b));
        }

        for (uint32 i = 0; i < b; i++)
        {
            shuffle(numbers.size(), &numbers.front(), rng);
            for (uint32 j = 0; j < b; j++)
                static_cast<math::Vector2f**>(grid)[j][i][1] = std::min(1.0f - 1e-6f,
                    static_cast<float>(i) / static_cast<float>(b) + (numbers[j] + rng.getFloat()) / static_cast<float>(b*b));
        }

        Permutation permutation(N, rng);
        for (uint32 n = 0; n < N; n++)
        {
            uint32 np   = permutation[n];
            samples[n]  = grid.get(np / b, np % b);
        }
    }
}
}

