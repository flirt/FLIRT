// ========================================================================= //
// Modification copyright 2016-2018 SUPAMONKS_STUDIO                         //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
// _________________________________________________________________________ //
// Copyright 2009-2013 Intel Corporation                                     //
//                                                                           //
// Licensed under the Apache License, Version 2.0 (the "License");           //
// you may not use this file except in compliance with the License.          //
// You may obtain a copy of the License at                                   //
//                                                                           //
//     http://www.apache.org/licenses/LICENSE-2.0                            //
//                                                                           //
// Unless required by applicable law or agreed to in writing, software       //
// distributed under the License is distributed on an "AS IS" BASIS,         //
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  //
// See the License for the specific language governing permissions and       //
// limitations under the License.                                            //
//                                                                           //
// ========================================================================= //
#pragma once

#include "SamplerFactory.hpp"

namespace smks { namespace rndr
{
    class MultijitteredSamplerFactory:
        public SamplerFactory
    {
        VALID_RTOBJECT
    private:
        size_t                      _numSampleSets;             //!< Number of precomputed sample sets.
        size_t                      _numSamplesPerPixel;        //!< Number of samples per pixel.

        size_t                      _numRequestedSamples1D;     //!< Number of additional 1D samples per pixel sample.
        size_t                      _numRequestedSamples2D;     //!< Number of additional 2D samples per pixel sample.
        size_t                      _numRequestedLightSamples;  //!< Number of precomputed light samples per pixel sample.
        size_t                      _numRequestedDirSamples;    //!< Number of precomputed direction samples per pixel sample.
        std::vector<const Light*>   _lights;                    //!< References to light sources. (owned by the device)
        std::vector<size_t>         _lightBaseSamples;          //!< Base samples for light sample precomputation.

        size_t                      _iteration;                 //!< Current iteration.
        PrecomputedSample**         _samples;                   //!< All precomputed samples.

        CREATE_RTOBJECT(MultijitteredSamplerFactory, SamplerFactory)
    protected:
        MultijitteredSamplerFactory(unsigned int scnId, const CtorArgs&);

    public:
        size_t
        numSampleSets() const;

        size_t
        numSamplesPerPixel() const;

        /*! Request additional 1D samples per pixel sample. */
        size_t
        request1D(size_t);

        /*! Request additional 2D samples per pixel sample. */
        size_t
        request2D(size_t);

        /*! Request additional light sample for specified light. */
        size_t
        requestLightSample(size_t baseSample, const Light*);

        /*! Request additional hemispheric direction samples per pixel sample. */
        size_t
        requestDirections(size_t);

        /*! Reset the sampler factory. Delete all precomputed samples. */
        void
        deleteSamples();

        /*! Initialize the factory for a given iteration and precompute all samples. */
        void
        initializeSamples(const Scene&, size_t iteration, sys::Ref<PixelFilter> const&);

        const PrecomputedSample&
        sample(size_t setIndex, size_t sampleIndex) const;

        void
        dispose();

        void
        commitFrameState(const parm::Container&, const xchg::IdSet&);

        inline
        void
        beginSubframeCommits(size_t)
        { }

        inline
        void
        commitSubframeState(size_t, const parm::Container&)
        { }

        inline
        void
        endSubframeCommits()
        { }

        inline
        bool
        perSubframeParameter(unsigned int) const
        {
            return false;
        }

        void
        getMetadata(img::OutputImage&) const;
    };
}
}
