// ========================================================================= //
// Modification copyright 2016-2018 SUPAMONKS_STUDIO                         //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
// _________________________________________________________________________ //
// Copyright 2009-2013 Intel Corporation                                     //
//                                                                           //
// Licensed under the Apache License, Version 2.0 (the "License");           //
// you may not use this file except in compliance with the License.          //
// You may obtain a copy of the License at                                   //
//                                                                           //
//     http://www.apache.org/licenses/LICENSE-2.0                            //
//                                                                           //
// Unless required by applicable law or agreed to in writing, software       //
// distributed under the License is distributed on an "AS IS" BASIS,         //
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  //
// See the License for the specific language governing permissions and       //
// limitations under the License.                                            //
//                                                                           //
// ========================================================================= //
#pragma once

#include "Sample.hpp"

namespace smks { namespace rndr
{
    /*! 1D probability distribution. The probability distribution
    *  function (PDF) can be initialized with arbitrary data and be
    *  sampled. */
    class Distribution1D
    {
    private:
        size_t  _size;  //!< Number of elements in the PDF
        float*  _pdf;   //!< Probability distribution function
        float*  _cdf;   //!< Cumulative distribution function (required for sampling)

    public:
        /*! Default construction. */
        Distribution1D();
        Distribution1D(const Distribution1D&);
        /*! Construction from distribution array f. */
        Distribution1D(const float*, size_t);

        /*! Destruction. */
        ~Distribution1D();

        Distribution1D&
        operator=(const Distribution1D&);

    private:
        void
        copy(const Distribution1D&);

    public:
        void
        dispose();

        inline
        bool
        empty() const
        {
            return _size == 0;
        }

        inline
        size_t
        size() const
        {
            return _size;
        }

        /*! Initialized the PDF and CDF arrays. */
        void
        initialize(const float*, const size_t);

        /*! Draws a sample from the distribution. \param u is a random
        *  number to use for sampling */
        Sample<float>
        sample(float u) const;

        /*! Returns the probability density a sample would be drawn from location p. */
        float
        pdf(float) const;

        /*! read back the x-cdf and x-pdf data for given y (for SPMD
        device offload). cdf is of size '_size+1', pdf of size
        '_size' */
        void
        readback(float* cdf, float* pdf);
    };
}
}
