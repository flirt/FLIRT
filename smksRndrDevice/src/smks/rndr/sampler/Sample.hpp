// ========================================================================= //
// Modification copyright 2016-2018 SUPAMONKS_STUDIO                         //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
// _________________________________________________________________________ //
// Copyright 2009-2013 Intel Corporation                                     //
//                                                                           //
// Licensed under the Apache License, Version 2.0 (the "License");           //
// you may not use this file except in compliance with the License.          //
// You may obtain a copy of the License at                                   //
//                                                                           //
//     http://www.apache.org/licenses/LICENSE-2.0                            //
//                                                                           //
// Unless required by applicable law or agreed to in writing, software       //
// distributed under the License is distributed on an "AS IS" BASIS,         //
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  //
// See the License for the specific language governing permissions and       //
// limitations under the License.                                            //
//                                                                           //
// ========================================================================= //
#pragma once

#include <smks/sys/constants.hpp>
#include <smks/sys/aligned_allocation.hpp>

namespace smks
{
    namespace rndr
    {
        /*! Sample representation. A sample has a location and a probability
        *  density it got sampled with. */
        template<typename ValueType>
        struct Sample
        {
            ALIGNED_CLASS
        public:
            ValueType   value;  //!< location of the sample
            float       pdf;    //!< probability density of the sample
        public:
            __forceinline
            Sample():
                value   (),
                pdf     (0.0f)
            { }

            __forceinline
            Sample(const ValueType& value, float pdf = sys::one):
                value   (value),
                pdf     (pdf)
            { }

            __forceinline
            Sample(const Sample& other):
                value   (other.value),
                pdf     (other.pdf)
            { }

            __forceinline
            Sample&
            operator=(const Sample& other)
            {
                value   = other.value;
                pdf     = other.pdf;
                return *this;
            }

            __forceinline
            Sample&
            operator+=(const Sample& other)
            {
                value   += other.value;
                pdf     += other.pdf;
                return *this;
            }

            __forceinline
            Sample
            operator*(float weight) const
            {
                return Sample(
                    value * weight,
                    pdf * weight);
            }

            friend __forceinline
            std::ostream&
            operator<<(std::ostream& out, const Sample<ValueType>& s)
            {
                return out << "( " << s.value << "\tpdf = " << s.pdf << " )";
            }
        };
    }

    namespace math
    {
        template<typename ValueType> __forceinline
        bool
        isnan(const rndr::Sample<ValueType>& s)
        {
            return isnan(s.value) || isnan(s.pdf);
        }
    }
}
