// ========================================================================= //
// Modification copyright 2016-2018 SUPAMONKS_STUDIO                         //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
// _________________________________________________________________________ //
// Copyright 2009-2013 Intel Corporation                                     //
//                                                                           //
// Licensed under the Apache License, Version 2.0 (the "License");           //
// you may not use this file except in compliance with the License.          //
// You may obtain a copy of the License at                                   //
//                                                                           //
//     http://www.apache.org/licenses/LICENSE-2.0                            //
//                                                                           //
// Unless required by applicable law or agreed to in writing, software       //
// distributed under the License is distributed on an "AS IS" BASIS,         //
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  //
// See the License for the specific language governing permissions and       //
// limitations under the License.                                            //
//                                                                           //
// ========================================================================= //
#pragma once

#include <smks/sys/Ref.hpp>
#include <smks/math/types.hpp>

#include "Distribution1d.hpp"

namespace smks { namespace rndr
{
    /*! 2D probability distribution. The probability distribution
    *  function (PDF) can be initialized with arbitrary data and be
    *  sampled. */
    class Distribution2D:
        public sys::RefCount
    {
    public:
        typedef sys::Ref<Distribution2D> Ptr;
    private:
        size_t          _width;     //!< Number of elements in x direction
        size_t          _height;    //!< Number of elements in y direction
        Distribution1D  _yDist;     //!< Distribution to select between rows
        Distribution1D* _xDists;    //!< One 1D Distribution per row

    public:
        /*! Default construction. */
        Distribution2D();
        Distribution2D(const Distribution2D&);
        /*! Construction from 2D distribution array f. */
        Distribution2D(const float** f, size_t width, size_t height);

        /*! Destruction. */
        ~Distribution2D();

        Distribution2D&
        operator=(const Distribution2D&);

    private:
        void
        copy(const Distribution2D&);

    public:
        void
        dispose();

        inline
        bool
        empty() const
        {
            return _width == 0 || _height == 0;
        }

        /*! Initialized the PDF and CDF arrays. */
        void
        initialize(const float** f, size_t width, size_t height);

        /*! Draws a sample from the distribution. \param u is a pair of
        *  random numbers to use for sampling */
        Sample<math::Vector2f>
        sample(const math::Vector2f& u) const;

        /*! Returns the probability density a sample would be drawn from
        *  location p. */
        float
        pdf(const math::Vector2f& p) const;

        /*! read back the y-cdf and y-pdf data (for SPMD device
        offload). cdf_y is of size 'height+1', pdf_y of size 'height'  */
        inline
        void
        readback(float *cdf_y, float *pdf_y)
        {
            _yDist.readback(cdf_y, pdf_y);
        }

        /*! read back the x-cdf and x-pdf data for given y (for SPMD
        device offload). cdf_x is of size 'width+1', pdf_x of size
        'width' */
        inline
        void
        readback(size_t y, float *cdf_x, float *pdf_x)
        {
            assert(y < _height);
            _xDists[y].readback(cdf_x, pdf_x);
        }
    };
}
}

