// ========================================================================= //
// Modification copyright 2016-2018 SUPAMONKS_STUDIO                         //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
// _________________________________________________________________________ //
// Copyright 2009-2013 Intel Corporation                                     //
//                                                                           //
// Licensed under the Apache License, Version 2.0 (the "License");           //
// you may not use this file except in compliance with the License.          //
// You may obtain a copy of the License at                                   //
//                                                                           //
//     http://www.apache.org/licenses/LICENSE-2.0                            //
//                                                                           //
// Unless required by applicable law or agreed to in writing, software       //
// distributed under the License is distributed on an "AS IS" BASIS,         //
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  //
// See the License for the specific language governing permissions and       //
// limitations under the License.                                            //
//                                                                           //
// ========================================================================= //
#include <algorithm>

#include <smks/math/math.hpp>

#include "Distribution1d.hpp"

using namespace smks;

rndr::Distribution1D::Distribution1D():
    _size   (0),
    _pdf    (nullptr),
    _cdf    (nullptr)
{ }

rndr::Distribution1D::Distribution1D(const Distribution1D& other):
    _size   (0),
    _pdf    (nullptr),
    _cdf    (nullptr)
{
    copy(other);
}

rndr::Distribution1D::Distribution1D(const float* f, const size_t size):
    _size   (0),
    _pdf    (nullptr),
    _cdf    (nullptr)
{
    initialize(f, size);
}

rndr::Distribution1D::~Distribution1D()
{
    dispose();
}

rndr::Distribution1D&
rndr::Distribution1D::operator=(const Distribution1D& other)
{
    if (this != &other)
        copy(other);
    return *this;
}

void
rndr::Distribution1D::copy(const Distribution1D& other)
{
    dispose();

    if (other._size == 0)
        return;

    _size = other._size;
    if (other._pdf)
    {
        _pdf = new float[other._size];
        memcpy(_pdf, other._pdf, sizeof(float) * other._size);
    }
    if (other._cdf)
    {
        _cdf = new float[other._size + 1];
        memcpy(_cdf, other._cdf, sizeof(float) * (other._size + 1));
    }
}

void
rndr::Distribution1D::dispose()
{
    if (_pdf)
        delete[] _pdf;
    _pdf = nullptr;
    if (_cdf)
        delete[] _cdf;
    _cdf = nullptr;
    _size = 0;
}

void
rndr::Distribution1D::initialize(const float* f, size_t size)
{
    dispose();
    if (size == 0)
        return;

    /*! create arrays */
    _size   = size;
    _pdf    = new float[_size];
    _cdf    = new float[_size+1];

    /*! accumulate the function f */
    _cdf[0] = 0.0f;
    for (size_t i = 1; i < _size+1; ++i)
        _cdf[i] = _cdf[i-1] + f[i-1];

    /*! compute reciprocal sum */
    float rcpSum = fabsf(_cdf[_size]) < 1e-6f
        ? 0.0f
        : math::rcp(_cdf[_size]);

    /*! normalize the probability distribution and cumulative distribution */
    for (size_t i = 1; i<_size+1; i++)
    {
        _pdf[i-1]   = f[i-1] * rcpSum * _size;
        _cdf[i]     *= rcpSum;
    }
    _cdf[_size] = 1.0f;
}

rndr::Sample<float>
rndr::Distribution1D::sample(float u) const
{
    assert(_size > 0);
    assert(_cdf);
    assert(_pdf);
    /*! coarse sampling of the distribution */
    float*  pointer     = std::upper_bound(_cdf, _cdf + _size, u);
    int     index       = math::clamp<int>(
        static_cast<int>(pointer - _cdf - 1),
        0,
        static_cast<int>(_size - 1));

    /*! refine sampling linearly by assuming the distribution being a step function */
    float fraction = (u - _cdf[index]) * math::rcp(_cdf[index+1] - _cdf[index]);

    return Sample<float>(static_cast<float>(index) + fraction, _pdf[index]);
}

float
rndr::Distribution1D::pdf(float p) const
{
    assert(_size > 0);
    assert(_pdf);
    return _pdf[ math::clamp<int>(
        static_cast<int>(p * _size),
        0,
        static_cast<int>(_size - 1) ) ];
}

void
rndr::Distribution1D::readback(float* cdf, float* pdf)
{
    assert(_size > 0);
    assert(_cdf);
    assert(_pdf);
    memcpy(cdf, _cdf, sizeof(float) * (_size + 1));
    memcpy(pdf, _pdf, sizeof(float) * _size);
}

