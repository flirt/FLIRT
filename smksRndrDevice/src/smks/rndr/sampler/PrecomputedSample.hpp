// ========================================================================= //
// Modification copyright 2016-2018 SUPAMONKS_STUDIO                         //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
// _________________________________________________________________________ //
// Copyright 2009-2013 Intel Corporation                                     //
//                                                                           //
// Licensed under the Apache License, Version 2.0 (the "License");           //
// you may not use this file except in compliance with the License.          //
// You may obtain a copy of the License at                                   //
//                                                                           //
//     http://www.apache.org/licenses/LICENSE-2.0                            //
//                                                                           //
// Unless required by applicable law or agreed to in writing, software       //
// distributed under the License is distributed on an "AS IS" BASIS,         //
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  //
// See the License for the specific language governing permissions and       //
// limitations under the License.                                            //
//                                                                           //
// ========================================================================= //
#pragma once

#include "LightSample.hpp"

namespace smks { namespace rndr
{
    struct PrecomputedSample
    {
        ALIGNED_CLASS
    public:
        math::Vector2f          pixel;          //!< Sample location inside the pixel. [0.5;0.5] is the pixel center.
        float                   time;           //!< time sample for motion blur.
        math::Vector2f          lens;           //!< 2D lens sample for depth of field.
        float                   *samples1D;     //!< Additional 1D samples requested by the integrator.
        math::Vector2f          *samples2D;     //!< Additional 2D samples requested by the integrator.
        Sample<math::Vector4f>  *dirSamples;    //!< Additional directions sampled on the hemisphere following a cosine weighted distribution.
        LightSample             *lightSamples;  //!< Precomputed light samples.
        //math::Vector2i            imageSize;      //!< Dimesions of the entire image.

    public:
        __forceinline
        PrecomputedSample():
            pixel       (math::Vector2f::Zero()),
            time        (0.0f),
            lens        (math::Vector2f::Zero()),
            samples1D(  nullptr),
            samples2D   (nullptr),
            dirSamples  (nullptr),
            lightSamples(nullptr)
        { }

    private:
        // non-copyable
        PrecomputedSample(const PrecomputedSample&);
        PrecomputedSample& operator=(const PrecomputedSample&);

    public:
        __forceinline
        float
        getFloat1(size_t idx) const
        {
            return samples1D[idx];
        }

        __forceinline
        const math::Vector2f&
        getFloat2(size_t idx) const
        {
            return samples2D[idx];
        }

        __forceinline
        const Sample<math::Vector4f>&
        getDirection(size_t idx) const
        {
            return dirSamples[idx];
        }

        __forceinline
        const LightSample&
        getLightSample(size_t idx) const
        {
            return lightSamples[idx];
        }
    };
}
}
