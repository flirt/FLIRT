// ========================================================================= //
// Modification copyright 2016-2018 SUPAMONKS_STUDIO                         //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
// _________________________________________________________________________ //
// Copyright 2009-2013 Intel Corporation                                     //
//                                                                           //
// Licensed under the Apache License, Version 2.0 (the "License");           //
// you may not use this file except in compliance with the License.          //
// You may obtain a copy of the License at                                   //
//                                                                           //
//     http://www.apache.org/licenses/LICENSE-2.0                            //
//                                                                           //
// Unless required by applicable law or agreed to in writing, software       //
// distributed under the License is distributed on an "AS IS" BASIS,         //
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  //
// See the License for the specific language governing permissions and       //
// limitations under the License.                                            //
//                                                                           //
// ========================================================================= //
#include <smks/math/math.hpp>
#include "Distribution2d.hpp"

using namespace smks;

rndr::Distribution2D::Distribution2D():
    _width  (0),
    _height (0),
    _yDist  (),
    _xDists (nullptr)
{ }

rndr::Distribution2D::Distribution2D(const Distribution2D& other):
    _width  (0),
    _height (0),
    _yDist  (),
    _xDists (nullptr)
{
    copy(other);
}

rndr::Distribution2D::Distribution2D(const float** f, size_t width, size_t height):
    _width  (0),
    _height (0),
    _yDist  (),
    _xDists (nullptr)
{
    initialize(f, width, height);
}

rndr::Distribution2D::~Distribution2D()
{
    dispose();
}

rndr::Distribution2D&
rndr::Distribution2D::operator=(const Distribution2D& other)
{
    if (this != &other)
        copy(other);
    return *this;
}

void
rndr::Distribution2D::copy(const Distribution2D& other)
{
    dispose();

    if (other._width == 0 ||
        other._height == 0)
        return;

    _width  = other._width;
    _height = other._height;
    _yDist  = other._yDist;
    _xDists = new Distribution1D[other._height];
    for (size_t y = 0; y < other._height; ++y)
        _xDists[y] = other._xDists[y];
}

void
rndr::Distribution2D::dispose()
{
    if (_xDists)
        delete[] _xDists;
    _xDists = nullptr;
    _yDist.dispose();
    _width  = 0;
    _height = 0;
}

void
rndr::Distribution2D::initialize(const float** f, size_t w, size_t h)
{
    dispose();
    if (w == 0 || h == 0)
        return;

    /*! create arrays */
    if (_xDists)
        delete[] _xDists;
    _width      = w;
    _height     = h;
    _xDists     = new Distribution1D[_height];

    float* fy   = new float[_height];

    /*! compute y distribution and initialize row distributions */
    for (size_t y = 0; y < _height; ++y)
    {
        /*! accumulate row to compute y distribution */
        fy[y] = 0.0f;
        for (size_t x = 0; x < _width; ++x)
            fy[y] += f[y][x];

        /*! initialize distribution for current row */
        _xDists[y].initialize(f[y], _width);
    }

    /*! initializes the y distribution */
    _yDist.initialize(fy, _height);
    delete[] fy;
}

rndr::Sample<math::Vector2f>
rndr::Distribution2D::sample(const math::Vector2f& u) const
{
    /*! use u.y to sample a row */
    const Sample<float>&    sy  = _yDist.sample(u.y());
    const int               y   = math::clamp<int>(
        static_cast<int>(sy.value),
        0,
        static_cast<int>(_height-1));

    /*! use u.x to sample inside the row */
    const Sample<float>&    sx  = _xDists[y].sample(u.x());
    return Sample<math::Vector2f>(
        math::Vector2f(sx.value, sy.value),
        sx.pdf * sy.pdf);
}

float
rndr::Distribution2D::pdf(const math::Vector2f& p) const
{
    const int idx = math::clamp<int>(
        static_cast<int>(p.y() * _height),
        0,
        static_cast<int>(_height-1));
    return _xDists[idx].pdf(p.x()) * _yDist.pdf(p.y());
}

