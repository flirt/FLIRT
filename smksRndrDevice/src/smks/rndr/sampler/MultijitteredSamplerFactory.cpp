// ========================================================================= //
// Modification copyright 2016-2018 SUPAMONKS_STUDIO                         //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
// _________________________________________________________________________ //
// Copyright 2009-2013 Intel Corporation                                     //
//                                                                           //
// Licensed under the Apache License, Version 2.0 (the "License");           //
// you may not use this file except in compliance with the License.          //
// You may obtain a copy of the License at                                   //
//                                                                           //
//     http://www.apache.org/licenses/LICENSE-2.0                            //
//                                                                           //
// Unless required by applicable law or agreed to in writing, software       //
// distributed under the License is distributed on an "AS IS" BASIS,         //
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  //
// See the License for the specific language governing permissions and       //
// limitations under the License.                                            //
//                                                                           //
// ========================================================================= //
#include <smks/math/math.hpp>
#include <smks/parm/BuiltIns.hpp>
#include <smks/xchg/shutter.hpp>
#include <smks/sys/Random.hpp>
#include <smks/sys/Exception.hpp>
#include <smks/img/OutputImage.hpp>

#include "PrecomputedSample.hpp"
#include "Patterns.hpp"
#include "MultijitteredSamplerFactory.hpp"
#include "ShapeSampler.hpp"
#include "../light/Light.hpp"
#include "../shape/Shape.hpp"
#include "../api/Scene.hpp"
#include "../filter/PixelFilter.hpp"

using namespace smks;

rndr::MultijitteredSamplerFactory::MultijitteredSamplerFactory(unsigned int scnId, const CtorArgs& args):
    SamplerFactory              (scnId, args),
    _numSampleSets              (0),
    _numSamplesPerPixel         (0),
    _numRequestedSamples1D      (0),
    _numRequestedSamples2D      (0),
    _numRequestedLightSamples   (0),
    _numRequestedDirSamples     (0),
    _lights                     (),
    _lightBaseSamples           (),
    _iteration                  (0),
    _samples                    (nullptr)
{
    dispose();
}

size_t
rndr::MultijitteredSamplerFactory::numSampleSets() const
{
    return _numSampleSets;
}

size_t
rndr::MultijitteredSamplerFactory::numSamplesPerPixel() const
{
    return _numSamplesPerPixel;
}

size_t
rndr::MultijitteredSamplerFactory::request1D(size_t num)
{
    const size_t idx = _numRequestedSamples1D;
    _numRequestedSamples1D += num;
    deleteSamples();

    return idx;
}

size_t
rndr::MultijitteredSamplerFactory::request2D(size_t num)
{
    const size_t idx = _numRequestedSamples2D;
    _numRequestedSamples2D += num;
    deleteSamples();

    return idx;
}

size_t
rndr::MultijitteredSamplerFactory::requestLightSample(size_t baseSample, const Light* light)
{
    if (light == nullptr)
        throw sys::Exception("Invalid light.", "Light Sample Request");

    const size_t idx = _numRequestedLightSamples;
    _lightBaseSamples   .push_back(baseSample);
    _lights             .push_back(light);
    ++_numRequestedLightSamples;
    deleteSamples();

    return idx;
}

size_t
rndr::MultijitteredSamplerFactory::requestDirections(size_t num)
{
    const size_t idx = _numRequestedDirSamples;
    _numRequestedDirSamples += num;
    deleteSamples();

    return idx;
}

const rndr::PrecomputedSample&
rndr::MultijitteredSamplerFactory::sample(size_t setIndex, size_t sampleIndex) const
{
    assert(setIndex     < _numSampleSets);
    assert(sampleIndex  < _numSamplesPerPixel);
    assert(_samples != nullptr);

    return _samples[setIndex][sampleIndex];
}

void
rndr::MultijitteredSamplerFactory::initializeSamples(const Scene& scene, size_t iteration, PixelFilter::Ptr const& filter)
{
    FLIRT_LOG(LOG(DEBUG)
        << "\n----------------------------"
        << "\nrandom sample initialization\n"
        << "\n\t- # sample sets = " << _numSampleSets
        << "\n\t- # samples / pixel = " << _numSamplesPerPixel
        << "\n\t- # requested 1d samples = " << _numRequestedSamples1D
        << "\n\t- # requested 2d samples = " << _numRequestedSamples2D
        << "\n\t- # requested light samples = " << _numRequestedLightSamples
        << "\n\t- # requested directions = " << _numRequestedDirSamples
        << "\n---------------------------";)

    _iteration = iteration;
    if (_samples)
        return; // already precomputed
    if (_numSampleSets == 0 ||
        _numSamplesPerPixel == 0)
        return;

    if (_numSamplesPerPixel != (1i64 << __bsf(_numSamplesPerPixel)))
        throw sys::Exception("Number of samples per pixel have to be a power of two.", "Sample Precomputation");

    _samples = new PrecomputedSample* [_numSampleSets];

    const size_t    chunkSize       = math::max(_numSamplesPerPixel, static_cast<size_t>(64));
    const size_t    currentChunk    = (iteration * _numSamplesPerPixel) / chunkSize;
    const size_t    offset          = (iteration * _numSamplesPerPixel) % chunkSize;

    sys::Random     rng;

    rng.setSeed(currentChunk * 5897);

    math::Vector2f  *tmp_pixel      = new math::Vector2f    [chunkSize];
    float           *tmp_time       = new float             [chunkSize];
    math::Vector2f  *tmp_lens       = new math::Vector2f    [chunkSize];
    float           *tmp_samples1D  = new float             [chunkSize];
    math::Vector2f  *tmp_samples2D  = new math::Vector2f    [chunkSize];

    for (size_t set = 0; set < _numSampleSets; set++)
    {
        _samples[set]   = new PrecomputedSample[_numSamplesPerPixel];

        /*! Generate pixel and lens samples. */
        sys::multiJittered  (tmp_pixel, chunkSize, rng);
        sys::jittered       (tmp_time,  chunkSize, rng);
        sys::multiJittered  (tmp_lens,  chunkSize, rng);

        for (size_t s = 0; s < _numSamplesPerPixel; s++)
        {
            _samples[set][s].pixel          = tmp_pixel [offset + s];
            if (filter)
                _samples[set][s].pixel      = filter->sample(_samples[set][s].pixel) + math::Vector2f(0.5f, 0.5f);
            _samples[set][s].time           = tmp_time  [offset + s];
            _samples[set][s].lens           = tmp_lens  [offset + s];

            _samples[set][s].samples1D      = new float                 [_numRequestedSamples1D];
            _samples[set][s].samples2D      = new math::Vector2f        [_numRequestedSamples2D];
            _samples[set][s].lightSamples   = new LightSample           [_numRequestedLightSamples];
            _samples[set][s].dirSamples     = new Sample<math::Vector4f>[_numRequestedDirSamples];

            memset(_samples[set][s].dirSamples, 0, sizeof(Sample<math::Vector4f>) * _numRequestedDirSamples);
        }

        /*! Generate requested 1D samples. */
        for (size_t d = 0; d < _numRequestedSamples1D; d++)
        {
            sys::jittered(tmp_samples1D, chunkSize, rng);
            for (size_t s = 0; s < _numSamplesPerPixel; s++)
                _samples[set][s].samples1D[d] = tmp_samples1D[offset + s];
        }

        /*! Generate 2D samples. */
        for (size_t d = 0; d < _numRequestedSamples2D; d++)
        {
            sys::multiJittered(tmp_samples2D, chunkSize, rng);
            for (size_t s = 0; s < _numSamplesPerPixel; s++)
                _samples[set][s].samples2D[d] = tmp_samples2D[offset + s];
        }

        /*! Generate light samples. */
        for (size_t d = 0; d < _numRequestedLightSamples; d++)
            for (size_t s = 0; s < _numSamplesPerPixel; s++)
            {
                LightSample             ls;
                DifferentialGeometry    dg;

                ls.L = _lights[d]->sample(scene.rtcScene(), dg, ls.wi, ls.tmax, _samples[set][s].samples2D[_lightBaseSamples[d]]);
                _samples[set][s].lightSamples[d] = ls;
            }

        /*! Generate directional samples. */
        for (size_t d = 0; d < _numRequestedDirSamples; ++d)
        {
            sys::multiJittered(tmp_samples2D, chunkSize, rng);
            for (size_t s = 0; s < _numSamplesPerPixel; s++)
            {
                const math::Vector2f& sample    = tmp_samples2D[offset + s];
                _samples[set][s].dirSamples[d]  = cosineSampleHemisphere(sample.x(), sample.y());
            }
        }
    }

    delete[] tmp_pixel;
    delete[] tmp_time;
    delete[] tmp_lens;
    delete[] tmp_samples1D;
    delete[] tmp_samples2D;

#if defined(FLIRT_LOG_ENABLED)
    for (size_t set = 0; set < _numSampleSets; set++)
        for (size_t s = 0; s < _numSamplesPerPixel; s++)
        {
            std::stringstream           sstr1d;
            std::stringstream           sstr2d;
            const PrecomputedSample&    sample = _samples[set][s];

            for (size_t j = 0; j < _numRequestedSamples1D; ++j)
                sstr1d << sample.samples1D[j] << " ";
            for (size_t j = 0; j < _numRequestedSamples2D; ++j)
                sstr2d << "(" << sample.samples2D[j].transpose() << ") ";

            LOG(DEBUG) << "precomputed sample[" << set << "][" << s << "]"
                << "\n\t- pixel\t= (" << sample.pixel.transpose() << ")"
                << "\n\t- time\t= " << sample.time
                << "\n\t- lens\t= (" << sample.lens.transpose() << ")"
                << "\n\t- samples 1d\t= { " << sstr1d.str() << "}"
                << "\n\t- samples 2d\t= { " << sstr2d.str() << "}";
        }
#endif // defined(FLIRT_LOG_ENABLED)
}

void
rndr::MultijitteredSamplerFactory::dispose()
{
    deleteSamples();

    _numRequestedSamples1D      = 0;
    _numRequestedSamples2D      = 0;
    _numRequestedDirSamples     = 0;
    _numRequestedLightSamples   = 0;

    getBuiltIn<size_t>      (parm::numSampleSets(), _numSampleSets);
    getBuiltIn<size_t>      (parm::numSamples(),    _numSamplesPerPixel);
}

void
rndr::MultijitteredSamplerFactory::commitFrameState(const parm::Container& parms, const xchg::IdSet& dirties)
{
    logParameters(scnId(), parms, "builtIns", dirties);
    logParameters(scnId(), userParameters(), "userParms", "USER");
    //---
    size_t  numSampleSets       = _numSampleSets;
    size_t  numSamplesPerPixel  = _numSamplesPerPixel;

    for (xchg::IdSet::const_iterator id = dirties.begin(); id != dirties.end(); ++id)
        if      (*id == parm::numSampleSets().id()) getBuiltIn<size_t>  (parm::numSampleSets(), numSampleSets,      &parms);
        else if (*id == parm::numSamples().id())    getBuiltIn<size_t>  (parm::numSamples(),    numSamplesPerPixel, &parms);

    // decide whether samples must be reset
    if (numSampleSets       != _numSampleSets ||
        numSamplesPerPixel  != _numSamplesPerPixel)
        deleteSamples();

    _numSampleSets      = numSampleSets;
    _numSamplesPerPixel = numSamplesPerPixel;
}

void
rndr::MultijitteredSamplerFactory::deleteSamples()
{
    if (_samples)
    {
        for (size_t set = 0; set < _numSampleSets; ++set)
        {
            for (size_t s = 0; s < _numSamplesPerPixel; ++s)
            {
                delete[] _samples[set][s].samples1D;
                delete[] _samples[set][s].samples2D;
                delete[] _samples[set][s].dirSamples;
                delete[] _samples[set][s].lightSamples;
            }
            delete[] _samples[set];
        }
        delete[] _samples;
    }
    _samples = nullptr;
}

// virtual
void
rndr::MultijitteredSamplerFactory::getMetadata(img::OutputImage& frame) const
{
    frame.setMetadata("multijitteredSampler/numSampleSets",         _numSampleSets);
    frame.setMetadata("multijitteredSampler/numSamplesPerPixel",    _numSamplesPerPixel);
}
