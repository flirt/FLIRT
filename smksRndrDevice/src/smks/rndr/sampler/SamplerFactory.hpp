// ========================================================================= //
// Modification copyright 2016-2018 SUPAMONKS_STUDIO                         //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
// _________________________________________________________________________ //
// Copyright 2009-2013 Intel Corporation                                     //
//                                                                           //
// Licensed under the Apache License, Version 2.0 (the "License");           //
// you may not use this file except in compliance with the License.          //
// You may obtain a copy of the License at                                   //
//                                                                           //
//     http://www.apache.org/licenses/LICENSE-2.0                            //
//                                                                           //
// Unless required by applicable law or agreed to in writing, software       //
// distributed under the License is distributed on an "AS IS" BASIS,         //
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  //
// See the License for the specific language governing permissions and       //
// limitations under the License.                                            //
//                                                                           //
// ========================================================================= //
#pragma once

#include "../ObjectBase.hpp"

namespace smks { namespace rndr
{
    class   Scene;
    class   PixelFilter;
    class   Light;
    struct  PrecomputedSample;

    class SamplerFactory:
        public ObjectBase
    {
    public:
        typedef sys::Ref<SamplerFactory> Ptr;

    protected:
        SamplerFactory(unsigned int scnId, const CtorArgs& args):
            ObjectBase(scnId, args)
        { }

    public:
        virtual
        size_t
        numSampleSets() const = 0;

        virtual
        size_t
        numSamplesPerPixel() const = 0;

        /*! Request additional 1D samples per pixel sample. */
        virtual
        size_t
        request1D(size_t = 1) = 0;

        /*! Request additional 2D samples per pixel sample. */
        virtual
        size_t
        request2D(size_t = 1) = 0;

        /*! Request additional light sample for specified light. */
        virtual
        size_t
        requestLightSample(size_t baseSample, const Light*) = 0;

        /*! Request additional hemispheric direction samples per pixel sample. */
        virtual
        size_t
        requestDirections(size_t = 1) = 0;

        /*! Reset the sampler factory. Delete all precomputed samples. */
        virtual
        void
        deleteSamples() = 0;

        /*! Initialize the factory for a given iteration and precompute all samples. */
        virtual
        void
        initializeSamples(const Scene&, size_t iteration = 0, sys::Ref<PixelFilter> const& = nullptr) = 0;

        virtual
        const PrecomputedSample&
        sample(size_t setIndex, size_t sampleIndex) const = 0;
    };
}
}
