// ========================================================================= //
// Modification copyright 2016-2018 SUPAMONKS_STUDIO                         //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
// _________________________________________________________________________ //
// Copyright 2009-2013 Intel Corporation                                     //
//                                                                           //
// Licensed under the Apache License, Version 2.0 (the "License");           //
// you may not use this file except in compliance with the License.          //
// You may obtain a copy of the License at                                   //
//                                                                           //
//     http://www.apache.org/licenses/LICENSE-2.0                            //
//                                                                           //
// Unless required by applicable law or agreed to in writing, software       //
// distributed under the License is distributed on an "AS IS" BASIS,         //
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  //
// See the License for the specific language governing permissions and       //
// limitations under the License.                                            //
//                                                                           //
// ========================================================================= //
#pragma once

#include <smks/math/types.hpp>
#include <smks/math/math.hpp>
#include <smks/math/MatrixAlgo.hpp>

#include "Sample.hpp"

/*! \file shapesampler.h Implements sampling functions for different
*  geometric shapes. */

namespace smks { namespace rndr
{
    ////////////////////////////////////////////////////////////////////////////////
    /// Sampling of Sphere
    ////////////////////////////////////////////////////////////////////////////////

    /*! Uniform sphere sampling. */
    __forceinline
    Sample<math::Vector4f>
    uniformSampleSphere(float u, float v)
    {
        const float phi         = static_cast<float>(sys::two_pi) * u;
        const float cTheta  = 1.0f - 2.0f * v;
        const float sTheta  = 2.0f * math::sqrt(v * (1.0f - v));

        return Sample<math::Vector4f>(
            math::Vector4f(math::cos(phi) * sTheta, math::sin(phi) * sTheta, cTheta, 0.0f),
            static_cast<float>(sys::one_over_four_pi));
    }

    /*! Computes the probability density for the uniform shere sampling. */
    __forceinline
    float
    uniformSampleSpherePDF()
    {
        return static_cast<float>(sys::one_over_four_pi);
    }

    /*! Cosine weighted sphere sampling. Up direction is the z direction. */
    __forceinline
    Sample<math::Vector4f>
    cosineSampleSphere(float u, float v)
    {
        const float phi     = static_cast<float>(sys::two_pi) * u;
        const float vv      = 2.0f * (v - 0.5f);
        const float cTheta  = math::sign(vv) * math::sqrt(math::abs(vv));
        const float sTheta  = math::cos2sin(cTheta);
        return Sample<math::Vector4f>(
            math::Vector4f(math::cos(phi) * sTheta, math::sin(phi) * sTheta, cTheta, 0.0f),
            2.0f*cTheta*static_cast<float>(sys::one_over_pi));
    }

    /*! Computes the probability density for the cosine weighted sphere sampling. */
    __forceinline
    float
    cosineSampleSpherePDF(const math::Vector4f& s)
    {
        return 2.0f * math::abs(s.z()) * static_cast<float>(sys::one_over_pi);
    }

    /*! Cosine weighted sphere sampling. Up direction is provided as argument. */
    __forceinline
    Sample<math::Vector4f>
    cosineSampleSphere(float u, float v, const math::Vector4f& N)
    {
        Sample<math::Vector4f> s = cosineSampleSphere(u,v);
        return Sample<math::Vector4f>(
            math::transformVectorInFrame(N, s.value),
            s.pdf);
    }

    /*! Computes the probability density for the cosine weighted sphere sampling. */
    __forceinline
    float
    cosineSampleSpherePDF(const math::Vector4f& s, const math::Vector4f& N)
    {
        return 2.0f * math::abs(math::dot3(s, N)) * static_cast<float>(sys::one_over_pi);
    }

    ////////////////////////////////////////////////////////////////////////////////
    /// Sampling of Hemisphere
    ////////////////////////////////////////////////////////////////////////////////

    /*! Uniform hemisphere sampling. Up direction is the z direction. */
    __forceinline
    Sample<math::Vector4f>
    uniformSampleHemisphere(float u, float v)
    {
        const float phi = static_cast<float>(sys::two_pi) * u;
        const float cTheta = v, sTheta = math::cos2sin(v);
        return Sample<math::Vector4f>(
            math::Vector4f(math::cos(phi) * sTheta, math::sin(phi) * sTheta, cTheta, 0.0f),
            static_cast<float>(sys::one_over_two_pi));
    }

    /*! Computes the probability density for the uniform hemisphere sampling. */
    __forceinline
    float
    uniformSampleHemispherePDF(const math::Vector4f& s)
    {
        return math::select<float>(s.z() < 0.0f,
            0.0f,
            static_cast<float>(sys::one_over_two_pi));
    }

    /*! Uniform hemisphere sampling. Up direction is provided as argument. */
    __forceinline
    Sample<math::Vector4f>
    uniformSampleHemisphere(float u, float v, const math::Vector4f& N)
    {
        Sample<math::Vector4f> s = uniformSampleHemisphere(u,v);
        return Sample<math::Vector4f>(
            math::transformVectorInFrame(N, s.value),
            s.pdf);
    }

    /*! Computes the probability density for the uniform hemisphere sampling. */
    __forceinline
    float
    uniformSampleHemispherePDF(const math::Vector4f& s, const math::Vector4f& N)
    {
        return math::select<float>(math::dot3(s, N) < 0.0f,
            0.0f,
            static_cast<float>(sys::one_over_two_pi));
    }

    /*! Cosine weighted hemisphere sampling. Up direction is the z direction. */
    __forceinline
    Sample<math::Vector4f>
    cosineSampleHemisphere(float u, float v)
    {
        const float phi = static_cast<float>(sys::two_pi) * u;
        const float cTheta = math::sqrt(v), sTheta = math::sqrt(1.0f - v);
        return Sample<math::Vector4f>(
            math::Vector4f(math::cos(phi) * sTheta, math::sin(phi) * sTheta, cTheta, 0.0f),
            cTheta*static_cast<float>(sys::one_over_pi));
    }

    /*! Computes the probability density for the cosine weighted hemisphere sampling. */
    __forceinline
    float
    cosineSampleHemispherePDF(const math::Vector4f& s)
    {
        return math::select<float>(s.z() < 0.0f,
            0.0f,
            s.z() * static_cast<float>(sys::one_over_pi));
    }

    /*! Cosine weighted hemisphere sampling. Up direction is provided as argument. */
    __forceinline
    Sample<math::Vector4f>
    cosineSampleHemisphere(float u, float v, const math::Vector4f& N)
    {
        Sample<math::Vector4f> s = cosineSampleHemisphere(u,v);
        return Sample<math::Vector4f>(
            math::transformVectorInFrame(N, s.value),
            s.pdf);
    }

    /*! Computes the probability density for the cosine weighted hemisphere sampling. */
    __forceinline
    float
    cosineSampleHemispherePDF(const math::Vector4f& s, const math::Vector4f& N)
    {
        return math::select<float>(math::dot3(s, N) < 0.0f,
            0.0f,
            math::dot3(s, N) * static_cast<float>(sys::one_over_pi));
    }

    /*! Samples hemisphere with power cosine distribution. Up direction
    *  is the z direction. */
    __forceinline
    Sample<math::Vector4f>
    powerCosineSampleHemisphere(float u, float v, float exp)
    {
        const float phi = static_cast<float>(sys::two_pi) * u;
        const float cTheta = math::pow(v, math::rcp(exp+1));
        const float sTheta = math::cos2sin(cTheta);
        return Sample<math::Vector4f>(
            math::Vector4f(math::cos(phi) * sTheta, math::sin(phi) * sTheta, cTheta, 0.0f),
            (exp + 1.0f) * powf(cTheta, exp) * static_cast<float>(sys::one_over_two_pi));
    }

    /*! Computes the probability density for the power cosine sampling of the hemisphere. */
    __forceinline
    float
    powerCosineSampleHemispherePDF(const math::Vector4f& s, float exp)
    {
        return math::select<float>(s.z() < 0.0f,
            0.0f,
            (exp + 1.0f) * math::pow(s.z(), exp) * static_cast<float>(sys::one_over_two_pi));
    }

    /*! Samples hemisphere with power cosine distribution. Up direction
    *  is provided as argument. */
    __forceinline
    Sample<math::Vector4f>
    powerCosineSampleHemisphere(float u, float v, const math::Vector4f& N, float exp)
    {
        Sample<math::Vector4f> s = powerCosineSampleHemisphere(u,v,exp);
        return Sample<math::Vector4f>(
            math::transformVectorInFrame(N, s.value),
            s.pdf);
    }

    /*! Computes the probability density for the power cosine sampling of the hemisphere. */
    __forceinline
    float
    powerCosineSampleHemispherePDF(const math::Vector4f& s, const math::Vector4f& N, float exp)
    {
        const float cTheta = math::dot3(s, N);
        return math::select<float>(cTheta < 0.0f,
            0.0f,
            (exp + 1.0f)*math::pow(cTheta, exp) * static_cast<float>(sys::one_over_two_pi));
    }

    ////////////////////////////////////////////////////////////////////////////////
    /// Sampling of Spherical Cone
    ////////////////////////////////////////////////////////////////////////////////

    /*! Uniform sampling of spherical cone. Cone direction is the z
    *  direction. */
    __forceinline
    Sample<math::Vector4f>
    uniformSampleCone(float u, float v, float angle)
    {
        const float phi = static_cast<float>(sys::two_pi) * u;
        const float cTheta = 1.0f - v * (1.0f - math::cos(angle));
        const float sTheta = math::cos2sin(cTheta);
        return Sample<math::Vector4f>(
            math::Vector4f(math::cos(phi) * sTheta, math::sin(phi) * sTheta, cTheta, 0.0f),
            math::rcp(static_cast<float>(sys::four_pi) * math::sqr(math::sin(0.5f * angle))));
    }

    /*! Computes the probability density of uniform spherical cone sampling. */
    __forceinline
    float
    uniformSampleConePDF(const math::Vector4f& s, float angle)
    {
        return math::select<float>(s.z() < math::cos(angle),
            0.0f,
            math::rcp(static_cast<float>(sys::four_pi) * math::sqr(math::sin(0.5f * angle))));
    }

    /*! Uniform sampling of spherical cone. Cone direction is provided as argument. */
    __forceinline
    Sample<math::Vector4f>
    uniformSampleCone(float u, float v, float angle, const math::Vector4f& N)
    {
        Sample<math::Vector4f> s = uniformSampleCone(u,v,angle);
        return Sample<math::Vector4f>(
            math::transformVectorInFrame(N, s.value),
            s.pdf);
    }

    /*! Computes the probability density of uniform spherical cone sampling. */
    __forceinline
    float
    uniformSampleConePDF(const math::Vector4f& s, float angle, const math::Vector4f& N)
    {
        return math::select<float>(math::dot3(s, N) < math::cos(angle),
            0.0f,
            math::rcp(static_cast<float>(sys::four_pi) * math::sqr(math::sin(0.5f * angle))));
    }

    ////////////////////////////////////////////////////////////////////////////////
    /// Sampling of Triangle
    ////////////////////////////////////////////////////////////////////////////////

    /*! Uniform sampling of triangle. */
    __forceinline
    math::Vector4f
    uniformSampleTriangle(float u, float v, const math::Vector4f& A, const math::Vector4f& B, const math::Vector4f& C)
    {
        float su = math::sqrt(u);
        return math::Vector4f(C + (1.0f - su) * (A - C) + (v * su) * (B - C));
    }

    ////////////////////////////////////////////////////////////////////////////////
    /// Sampling of Disk
    ////////////////////////////////////////////////////////////////////////////////

    /*! Uniform sampling of disk. */
    __forceinline
    math::Vector2f
    uniformSampleDisk(const math::Vector2f& sample, float radius)
    {
        const float r       = radius * math::sqrt(sample.x());
        const float theta   = static_cast<float>(sys::two_pi) * sample.y();
        return math::Vector2f(r * math::cos(theta), r * math::sin(theta));
    }
}
}
