// ========================================================================= //
// Modification copyright 2016-2018 SUPAMONKS_STUDIO                         //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
// _________________________________________________________________________ //
// Copyright 2009-2013 Intel Corporation                                     //
//                                                                           //
// Licensed under the Apache License, Version 2.0 (the "License");           //
// you may not use this file except in compliance with the License.          //
// You may obtain a copy of the License at                                   //
//                                                                           //
//     http://www.apache.org/licenses/LICENSE-2.0                            //
//                                                                           //
// Unless required by applicable law or agreed to in writing, software       //
// distributed under the License is distributed on an "AS IS" BASIS,         //
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  //
// See the License for the specific language governing permissions and       //
// limitations under the License.                                            //
//                                                                           //
// ========================================================================= //
#pragma once

#include <smks/sys/Log.hpp>
#include <smks/math/math.hpp>
#include <smks/xchg/Array2D.hpp>
#include "PixelFilter.hpp"

using namespace smks;

rndr::PixelFilter::PixelFilter(unsigned int scnId, const CtorArgs& args):
    ObjectBase      (scnId, args),
    _radius         (0.0f),
    _width          (0.0f),
    _height         (0.0f),
    _border         (0),
    _tableSize      (0),
    _rTableSize     (0.0f),
    _distribution   ()
{ }

void
rndr::PixelFilter::initialize(float radius, float width, float height, int border, unsigned int tableSize)
{
    FLIRT_LOG(LOG(DEBUG)
        << "\n---------------------"
        << "\nfilter initialization\n"
        << "\n\t- radius = " << radius
        << "\n\t- width = " << width
        << "\n\t- height = " << height
        << "\n\t- border = " << border
        << "\n\t- table size = " << tableSize
        << "\n---------------------";)

    _distribution.dispose();

    // store parameters
    _radius     = radius;
    _width      = width;
    _height     = height;
    _border     = border;
    _tableSize  = tableSize;
    _rTableSize = _tableSize > 0 ? _rTableSize  = math::rcp(static_cast<float>(_tableSize)) : 0.0f;

    if (_tableSize == 0)
        return;

    /*! Initializes the automatic importance sampling feature. */
    xchg::Array2D<float>    absValues   (_tableSize, _tableSize);
    const float             rTableSize  = math::rcp(static_cast<float>(_tableSize));

    for (uint32 x = 0; x < _tableSize; ++x)
    {
        for (uint32 y = 0; y < _tableSize; ++y)
        {
            const math::Vector2f pos(
                (x + 0.5f) * _rTableSize * _width   - _width    * 0.5f,
                (y + 0.5f) * _rTableSize * _height  - _height   * 0.5f);
            absValues.set(x, y, fabsf(eval(pos)));
        }
    }
    _distribution.initialize(absValues, _tableSize, _tableSize);
}

/*! Draws a filter sample. */
math::Vector2f
rndr::PixelFilter::sample(const math::Vector2f& uv) const
{
    Sample<math::Vector2f> s = _distribution.sample(uv);
    s.value.x() = s.value.x() * _rTableSize * _width  - _width  * 0.5f;
    s.value.y() = s.value.y() * _rTableSize * _height - _height * 0.5f;

    return s.value;
}
