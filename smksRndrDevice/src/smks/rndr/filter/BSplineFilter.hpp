// ========================================================================= //
// Modification copyright 2016-2018 SUPAMONKS_STUDIO                         //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
// _________________________________________________________________________ //
// Copyright 2009-2013 Intel Corporation                                     //
//                                                                           //
// Licensed under the Apache License, Version 2.0 (the "License");           //
// you may not use this file except in compliance with the License.          //
// You may obtain a copy of the License at                                   //
//                                                                           //
//     http://www.apache.org/licenses/LICENSE-2.0                            //
//                                                                           //
// Unless required by applicable law or agreed to in writing, software       //
// distributed under the License is distributed on an "AS IS" BASIS,         //
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  //
// See the License for the specific language governing permissions and       //
// limitations under the License.                                            //
//                                                                           //
// ========================================================================= //
#pragma once

#include <smks/parm/BuiltIns.hpp>
#include "PixelFilter.hpp"

namespace smks { namespace rndr
{
    class BSplineFilter:
        public PixelFilter
    {
        VALID_RTOBJECT
        CREATE_RTOBJECT(BSplineFilter, PixelFilter)
    protected:
        /*! Constructs a box filter of specified half width. */
        BSplineFilter(unsigned int scnId, const CtorArgs& args):
            PixelFilter(scnId, args)
        {
            initialize(
                2.0f,   // radius
                4.0f,   // width
                4.0f,   // height
                2);     // border
        }

    public:
        inline
        float
        eval(const math::Vector2f& distToCenter) const
        {
            const float d = distToCenter.norm();
            if (d > 2.0f)
                return 0.0f;
            else if (d < 1.0f)
            {
                const float t = 1.0f - d;
                return ((((-3.0f * t) + 3.0f) * t + 3.0f) * t + 1.0f) / 6.0f;
            }
            else
            {
                float t = 2.0f - d;
                return t * t * t / 6.0f;
            }
        }

        inline
        void
        dispose()
        { }

        inline
        void
        commitFrameState(const parm::Container& parms, const xchg::IdSet& dirties)
        {
            logParameters(scnId(), parms, "builtIns", dirties);
            logParameters(scnId(), userParameters(), "userParms", "USER");
        }

        inline
        void
        beginSubframeCommits(size_t)
        { }

        inline
        void
        commitSubframeState(size_t, const parm::Container&)
        { }

        inline
        void
        endSubframeCommits()
        { }

        inline
        bool
        perSubframeParameter(unsigned int) const
        {
            return false;
        }
    };
}
}
