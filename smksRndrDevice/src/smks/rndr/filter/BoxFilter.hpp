// ========================================================================= //
// Modification copyright 2016-2018 SUPAMONKS_STUDIO                         //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
// _________________________________________________________________________ //
// Copyright 2009-2013 Intel Corporation                                     //
//                                                                           //
// Licensed under the Apache License, Version 2.0 (the "License");           //
// you may not use this file except in compliance with the License.          //
// You may obtain a copy of the License at                                   //
//                                                                           //
//     http://www.apache.org/licenses/LICENSE-2.0                            //
//                                                                           //
// Unless required by applicable law or agreed to in writing, software       //
// distributed under the License is distributed on an "AS IS" BASIS,         //
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  //
// See the License for the specific language governing permissions and       //
// limitations under the License.                                            //
//                                                                           //
// ========================================================================= //
#pragma once

#include <smks/parm/BuiltIns.hpp>
#include "PixelFilter.hpp"

namespace smks { namespace rndr
{
    class BoxFilter:
        public PixelFilter
    {
        VALID_RTOBJECT
        CREATE_RTOBJECT(BoxFilter, PixelFilter)
    private:
        float _halfWidth; //!< Half the width of the box filter

    protected:
        /*! Constructs a box filter of specified half width. */
        BoxFilter(unsigned int scnId, const CtorArgs& args):
            PixelFilter (scnId, args),
            _halfWidth  (0.0f)
        {
            dispose();
        }

    public:
        inline
        float
        eval(const math::Vector2f& distToCenter) const
        {
            return fabsf(distToCenter.x()) > _halfWidth || fabsf(distToCenter.y()) > _halfWidth
                ? 0.0f
                : 1.0f;
        }

        inline
        void
        dispose()
        {
            getBuiltIn<float>(parm::halfWidth(), _halfWidth);
        }

        inline
        void
        commitFrameState(const parm::Container& parms, const xchg::IdSet& dirties)
        {
            float halfWidth = _halfWidth;

            for (xchg::IdSet::const_iterator id = dirties.begin(); id != dirties.end(); ++id)
                if (*id == parm::halfWidth().id())
                    getBuiltIn<float>(parm::halfWidth(), _halfWidth, &parms);

            if (fabsf(halfWidth - _halfWidth) > 1e-6f)
            {
                _halfWidth = halfWidth;
                initialize(
                    1.414213562f * _halfWidth,          // radius
                    2.0f * _halfWidth,                  // width
                    2.0f * _halfWidth,                  // height
                    uint32(ceil(_halfWidth - 0.5f)));   // border
            }
        }

        inline
        void
        beginSubframeCommits(size_t)
        { }

        inline
        void
        commitSubframeState(size_t, const parm::Container&)
        { }

        inline
        void
        endSubframeCommits()
        { }

        inline
        bool
        perSubframeParameter(unsigned int) const
        {
            return false;
        }
    };
}
}
