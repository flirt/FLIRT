// ========================================================================= //
// Copyright 2016-2018 SUPAMONKS_STUDIO                                      //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
//                                                                           //
// ========================================================================= //
#pragma once

#include "../ObjectBase.hpp"

namespace smks { namespace rndr
{
    struct  DifferentialGeometry;
    struct  Ray;
    struct  LightPath;
    class   Scene;

    class SubSurface:
        public ObjectBase
    {
    public:
        typedef sys::Ref<SubSurface> Ptr;

    protected:
        inline
            SubSurface(unsigned int scnId, const CtorArgs& args):
            ObjectBase(scnId, args)
        { }

    public:
        virtual
        math::Vector4f
        Lo(const DifferentialGeometry&,
           const math::Vector4f& wo,    //<! outgoing direction
           const LightPath&,
           const Scene&) const = 0;

        virtual
        void
        assignTextureToParameter(unsigned int parmId, sys::Ref<Texture> const&) = 0;

        //--- intersection/occlusion callbacks ------------
        virtual __forceinline bool intersected(Ray&) const { return false; }
        virtual __forceinline bool occluded   (Ray&) const { return false; }
        //-------------------------------------------------
    };
}
}
