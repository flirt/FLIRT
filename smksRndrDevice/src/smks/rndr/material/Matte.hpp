// ========================================================================= //
// Modification copyright 2016-2018 SUPAMONKS_STUDIO                         //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
// _________________________________________________________________________ //
// Copyright 2009-2013 Intel Corporation                                     //
//                                                                           //
// Licensed under the Apache License, Version 2.0 (the "License");           //
// you may not use this file except in compliance with the License.          //
// You may obtain a copy of the License at                                   //
//                                                                           //
//     http://www.apache.org/licenses/LICENSE-2.0                            //
//                                                                           //
// Unless required by applicable law or agreed to in writing, software       //
// distributed under the License is distributed on an "AS IS" BASIS,         //
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  //
// See the License for the specific language governing permissions and       //
// limitations under the License.                                            //
//                                                                           //
// ========================================================================= //
#pragma once

#include "Material.hpp"
#include "../brdf/Lambertian.hpp"

namespace smks { namespace rndr
{
    /*! Implements a diffuse material. */
    class Matte:
        public Material
    {
        VALID_RTOBJECT
    private:
        ColorOrTexture  _reflectance;       /*! Diffuse reflectance of the surface. The range is from 0 (black) to 1 (white). */
        //--------------
        Lambertian*     _lambertianBRDF;    // precomputed only if reflectance is constant

        CREATE_RTOBJECT(Matte, Material)
    protected:
        Matte(unsigned int scnId, const CtorArgs& args):
            Material        (scnId, args),
            _reflectance    (),
            //--------------
            _lambertianBRDF (nullptr)
        {
            dispose();
        }
    public:
        ~Matte()
        {
            dispose();
        }

        void
        shade(const Ray&            ray,
              const LightPath&      lp,
              const Medium&         medium,
              DifferentialGeometry& dg,
              CompositedBRDF&       brdfs) const
        {
            if (!applyNormalMap(dg, -ray.dir))
                return;

            brdfs.add(
                _lambertianBRDF
                ? _lambertianBRDF
                : NEW_BRDF(brdfs, Lambertian) (
                    _reflectance.get(dg.st)));
        }

        void
        assignTextureToParameter(unsigned int parmId, Texture::Ptr const& tex)
        {
            Material::assignTextureToParameter(parmId, tex);
            //---
            if (parmId == parm::reflectanceMap().id())
            {
                _reflectance.texture = tex;
                disposeLambertianBRDF();
            }
        }

        void
        commitFrameState(const parm::Container& parms, const xchg::IdSet& dirties)
        {
            Material::commitFrameState(parms, dirties);
            //---
            for (xchg::IdSet::const_iterator id = dirties.begin(); id != dirties.end(); ++id)
                if (*id == parm::reflectance().id())
                {
                    getBuiltIn<math::Vector4f>(parm::reflectance(), _reflectance.value, &parms);
                    disposeLambertianBRDF();
                }

            if (_lambertianBRDF == nullptr && !_reflectance.texture)
                _lambertianBRDF = new Lambertian(_reflectance.value);

            FLIRT_LOG(LOG(DEBUG)
                << "\n--------------------"
                << "\nmatte material (ID = " << scnId() << ")"
                << "\n\t- reflectance = ( " << _reflectance << " )"
                << "\n--------------------";)
        }

        void
        dispose()
        {
            getBuiltIn<math::Vector4f>(parm::reflectance(), _reflectance.value);
            _reflectance.texture = sys::null;
            //---
            disposeLambertianBRDF();
            //---
            Material::dispose();
        }

    private:
        void
        disposeLambertianBRDF()
        {
            if (_lambertianBRDF)
                delete _lambertianBRDF;
            _lambertianBRDF = nullptr;
        }
    };
}
}
