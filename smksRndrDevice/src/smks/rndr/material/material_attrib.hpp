// ========================================================================= //
// Copyright 2016-2018 SUPAMONKS_STUDIO                                      //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
//                                                                           //
// ========================================================================= //
#pragma once

#include <smks/math/types.hpp>
#include "../texture/Texture.hpp"

namespace smks { namespace rndr
{
    ////////////////////////
    // struct FloatOrTexture
    ////////////////////////
    struct FloatOrTexture
    {
        float           value;
        Texture::Ptr    texture;

    public:
        explicit inline
        FloatOrTexture(float val = 0.0f):
            value   (val),
            texture (sys::null)
        { }

        __forceinline
        float
        get(const math::Vector2f& uv) const
        {
            return texture
                ? texture->get(uv).x()
                : value;
        }

        __forceinline
        void
        reset(float val = 0.0f)
        {
            value   = val;
            texture = sys::null;
        }

        friend std::ostream&
        operator<<(std::ostream& out, const FloatOrTexture& x)
        {
            return out
                << "float = " << x.value << "\t"
                << "texture ID = " << (x.texture ? x.texture->scnId() : 0);
        }
    };

    ////////////////////////
    // struct ColorOrTexture
    ////////////////////////
    struct ColorOrTexture
    {
        ALIGNED_CLASS
        math::Vector4f  value;
        Texture::Ptr    texture;

    public:
        explicit inline
        ColorOrTexture(const math::Vector4f& val = math::Vector4f::Zero()):
            value   (val),
            texture (sys::null)
        { }

        __forceinline
        math::Vector4f
        get(const math::Vector2f& uv) const
        {
            return texture
                ? texture->get(uv)
                : value;
        }

        __forceinline
        void
        reset(const math::Vector4f& val = math::Vector4f::Zero())
        {
            value   = val;
            texture = sys::null;
        }

        friend std::ostream&
        operator<<(std::ostream& out, const ColorOrTexture& x)
        {
            return out
                << "color = ( " << x.value.transpose() << " )\t"
                << "texture ID = " << (x.texture ? x.texture->scnId() : 0);
        }
    };
}
}
