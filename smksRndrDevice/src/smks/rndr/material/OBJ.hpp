// ========================================================================= //
// Modification copyright 2016-2018 SUPAMONKS_STUDIO                         //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
// _________________________________________________________________________ //
// Copyright 2009-2013 Intel Corporation                                     //
//                                                                           //
// Licensed under the Apache License, Version 2.0 (the "License");           //
// you may not use this file except in compliance with the License.          //
// You may obtain a copy of the License at                                   //
//                                                                           //
//     http://www.apache.org/licenses/LICENSE-2.0                            //
//                                                                           //
// Unless required by applicable law or agreed to in writing, software       //
// distributed under the License is distributed on an "AS IS" BASIS,         //
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  //
// See the License for the specific language governing permissions and       //
// limitations under the License.                                            //
//                                                                           //
// ========================================================================= //
#pragma once

#include "Material.hpp"
#include "../brdf/Transmission.hpp"
#include "../brdf/Lambertian.hpp"
#include "../brdf/Specular.hpp"

namespace smks { namespace rndr
{
    /*! material which models a subset of the features of the OBJ material format */
    class OBJ:
        public Material
    {
        VALID_RTOBJECT
    protected:
        FloatOrTexture  _d;     /*! opacity: 0 (transparent), 1 (opaque)                */
        ColorOrTexture  _Kd;    /*! diffuse  reflectance: 0 (none), 1 (full)            */
        ColorOrTexture  _Ks;    /*! specular reflectance: 0 (none), 1 (full)            */
        FloatOrTexture  _Ns;    /*! specular exponent: 0 (diffuse), infinity (specular) */

        CREATE_RTOBJECT(OBJ, Material)
    protected:
        OBJ(unsigned int scnId, const CtorArgs& args):
            Material(scnId, args),
            _d      (1.0f),
            _Kd     (math::Vector4f::Ones()),
            _Ks     (math::Vector4f::Zero()),
            _Ns     (1.0f)
        { }
    public:
        ~OBJ()
        {
            dispose();
        }

        void
        shade(const Ray&            ray,
              const LightPath&      lp,
              const Medium&         medium,
              DifferentialGeometry& dg,
              CompositedBRDF&       brdfs) const
        {
            if (!applyNormalMap(dg, -ray.dir))
                return;

            /*! transmission */
            const float d = _d.get(dg.st);
            if (d < 1.0f)
                brdfs.add(
                    NEW_BRDF(brdfs, Transmission) (math::Vector4f::Constant(1.0f - d)));

            /*! diffuse component */
            const math::Vector4f& kd = d * _Kd.get(dg.st);
            if (math::neq3(kd, math::Vector4f::Zero()))
                brdfs.add(
                    NEW_BRDF(brdfs, Lambertian) (kd));

            /*! specular exponent */
            const float Ns = _Ns.get(dg.st);

            /*! specular component */
            const math::Vector4f& Ks = d * _Ks.get(dg.st);
            if (math::neq3(Ks, math::Vector4f::Zero()))
                brdfs.add(
                    NEW_BRDF(brdfs, Specular) (Ks, Ns));
        }

        void
        assignTextureToParameter(unsigned int parmId, Texture::Ptr const& tex)
        {
            Material::assignTextureToParameter(parmId, tex);
            //---
            if      (parmId == parm::diffuseReflectanceMap().id())  _Kd.texture = tex;
            else if (parmId == parm::opacityMap().id())             _d.texture  = tex;
            else if (parmId == parm::specularReflectanceMap().id()) _Ks.texture = tex;
            else if (parmId == parm::specularExponentMap().id())        _Ns.texture = tex;
        }

        void
        commitFrameState(const parm::Container& parms, const xchg::IdSet& dirties)
        {
            Material::commitFrameState(parms, dirties);
            //---
            for (xchg::IdSet::const_iterator id = dirties.begin(); id != dirties.end(); ++id)
                if      (*id == parm::opacity().id())               getBuiltIn<float>           (parm::opacity(),               _d.value,   &parms);
                else if (*id == parm::diffuseReflectance().id())    getBuiltIn<math::Vector4f>  (parm::diffuseReflectance(),    _Kd.value,  &parms);
                else if (*id == parm::specularReflectance().id())   getBuiltIn<math::Vector4f>  (parm::specularReflectance(),   _Ks.value,  &parms);
                else if (*id == parm::specularExponent().id())      getBuiltIn<float>           (parm::specularExponent(),      _Ns.value,  &parms);

            FLIRT_LOG(LOG(DEBUG)
                << "\n--------------------\n"
                << "OBJ material (ID = " << scnId() << ")\n"
                << "\n\t- Kd = ( " << _Kd << " )"
                << "\n\t- Ks = ( " << _Ks << " )"
                << "\n\t- Ns = ( " << _Ns << " )"
                << "\n\t- d = ( " << _d << " )"
                <<"\n--------------------";)
        }

        void
        dispose()
        {
            getBuiltIn<float>(parm::opacity(), _d.value);
            _d.texture = sys::null;

            getBuiltIn<math::Vector4f>(parm::diffuseReflectance(), _Kd.value);
            _Kd.texture = sys::null;

            getBuiltIn<math::Vector4f>(parm::specularReflectance(), _Ks.value);
            _Ks.texture = sys::null;

            getBuiltIn<float>(parm::specularExponent(), _Ns.value);
            _Ns.texture = sys::null;
            //---
            Material::dispose();
        }
    };
}
}

