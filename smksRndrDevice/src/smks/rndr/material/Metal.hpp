// ========================================================================= //
// Modification copyright 2016-2018 SUPAMONKS_STUDIO                         //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
// _________________________________________________________________________ //
// Copyright 2009-2013 Intel Corporation                                     //
//                                                                           //
// Licensed under the Apache License, Version 2.0 (the "License");           //
// you may not use this file except in compliance with the License.          //
// You may obtain a copy of the License at                                   //
//                                                                           //
//     http://www.apache.org/licenses/LICENSE-2.0                            //
//                                                                           //
// Unless required by applicable law or agreed to in writing, software       //
// distributed under the License is distributed on an "AS IS" BASIS,         //
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  //
// See the License for the specific language governing permissions and       //
// limitations under the License.                                            //
//                                                                           //
// ========================================================================= //
#pragma once

#include "Material.hpp"
#include "../brdf/Conductor.hpp"
#include "../brdf/Microfacet.hpp"
#include "../brdf/microfacet/fresnel.hpp"
#include "../brdf/microfacet/PowerCosineDistribution.hpp"

namespace smks { namespace rndr
{
    /*! Implements a rough metal material. The metal is modelled by a
    *  microfacet BRDF with a fresnel term for metals and a power
    *  cosine distribution for different roughness of the material. */
    class Metal:
        public Material
    {
        VALID_RTOBJECT
    private:
        typedef Microfacet<FresnelConductor,PowerCosineDistribution > MicrofacetMetal;
    private:
        ColorOrTexture  _reflectance;   //!< Reflectivity of the metal
        math::Vector4f  _eta;           //!< Real part of refraction index
        math::Vector4f  _k;             //!< Imaginary part of refraction index
        FloatOrTexture  _roughness;     //!< Roughness parameter. The range goes from 0 (specular) to 1 (diffuse).
        //--------------
        Conductor*      _conductorBRDF; //!< precomputed only if reflectance is constant.
        float           _rRoughness;    //!< Reciprocal roughness parameter. (only valid if roughness constant)

        CREATE_RTOBJECT(Metal, Material)
    protected:
        Metal(unsigned int scnId, const CtorArgs& args):
            Material        (scnId, args),
            _reflectance    (),
            _eta            (math::Vector4f::Zero()),
            _k              (math::Vector4f::Zero()),
            _roughness      (),
            _conductorBRDF  (nullptr),
            _rRoughness     (-1.0f)
        {
            dispose();
        }
    public:
        ~Metal()
        {
            dispose();
        }

        void
        shade(const Ray&            ray,
              const LightPath&      lp,
              const Medium&         medium,
              DifferentialGeometry& dg,
              CompositedBRDF&       brdfs) const
        {
            if (!applyNormalMap(dg, -ray.dir))
                return;

            const float roughness = _roughness.get(dg.st);
            if (roughness < 1e-6f)
            {
                /*! handle the special case of a specular metal through a special BRDF */
                brdfs.add(
                    _conductorBRDF
                    ? _conductorBRDF
                    : NEW_BRDF(brdfs, Conductor) (
                        _reflectance.get(dg.st), _eta, _k));
            }
            else
            {
                /*! otherwise use the microfacet BRDF model */
                const float rRoughness = !_roughness.texture
                    ? _rRoughness
                    : math::rcp(roughness);

                brdfs.add(
                    NEW_BRDF(brdfs, MicrofacetMetal) (
                        _reflectance.get(dg.st),
                        FresnelConductor(_eta, _k),
                        PowerCosineDistribution(rRoughness, dg.Ns)));
            }
        }

        void
        assignTextureToParameter(unsigned int parmId, Texture::Ptr const& tex)
        {
            Material::assignTextureToParameter(parmId, tex);
            //---
            if (parmId == parm::reflectanceMap().id())
            {
                _reflectance.texture = tex;
                disposeConductorBRDF();
            }
            else if (parmId == parm::roughnessMap().id())
                _roughness.texture = tex;
        }

        void
        commitFrameState(const parm::Container& parms, const xchg::IdSet& dirties)
        {
            Material::commitFrameState(parms, dirties);
            //---
            for (xchg::IdSet::const_iterator id = dirties.begin(); id != dirties.end(); ++id)
                if (*id == parm::reflectance().id())
                {
                    getBuiltIn<math::Vector4f>(parm::reflectance(), _reflectance.value, &parms);
                    disposeConductorBRDF();
                }
                else if (*id == parm::IORreal().id())
                {
                    getBuiltIn<math::Vector4f>(parm::IORreal(), _eta, &parms);
                    disposeConductorBRDF();
                }
                else if (*id == parm::IORimag().id())
                {
                    getBuiltIn<math::Vector4f>(parm::IORimag(), _k, &parms);
                    disposeConductorBRDF();
                }
                else if (*id == parm::roughness().id())
                {
                    getBuiltIn<float>(parm::roughness(), _roughness.value, &parms);
                    _rRoughness = -1.0f;
                }

            if (_conductorBRDF == nullptr && !_reflectance.texture)
                _conductorBRDF = new Conductor(_reflectance.value, _eta, _k);
            if (_rRoughness < 0.0f && !_roughness.texture)
                _rRoughness = _roughness.value > 0.0f
                    ? math::rcp(_roughness.value)
                    : 0.0f;

            FLIRT_LOG(LOG(DEBUG)
                << "\n--------------------"
                << "\nmetal material (ID = " << scnId() << ")\n"
                << "\n\t- reflectance = ( " << _reflectance << " )"
                << "\n\t- eta = (" << _eta.transpose() << ")"
                << "\n\t- k = (" << _k.transpose() << ")"
                << "\n\t- roughness = ( " << _roughness << " )"
                << "\n--------------------";)
        }

        void
        dispose()
        {
            getBuiltIn<math::Vector4f>(parm::reflectance(), _reflectance.value);
            _reflectance.texture = sys::null;

            getBuiltIn<math::Vector4f>(parm::IORreal(), _eta);
            getBuiltIn<math::Vector4f>(parm::IORimag(), _k);

            getBuiltIn<float>(parm::roughness(), _roughness.value);
            _roughness.texture = sys::null;
            //---
            disposeConductorBRDF();
            _rRoughness     = -1.0f;
            //---
            Material::dispose();
        }

    private:
        void
        disposeConductorBRDF()
        {
            if (_conductorBRDF)
                delete _conductorBRDF;
            _conductorBRDF = nullptr;
        }
    };
}
}
