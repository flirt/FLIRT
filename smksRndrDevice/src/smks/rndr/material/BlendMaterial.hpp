// ========================================================================= //
// Modification copyright 2016-2018 SUPAMONKS_STUDIO                         //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
// _________________________________________________________________________ //
// Copyright 2009-2013 Intel Corporation                                     //
//                                                                           //
// Licensed under the Apache License, Version 2.0 (the "License");           //
// you may not use this file except in compliance with the License.          //
// You may obtain a copy of the License at                                   //
//                                                                           //
//     http://www.apache.org/licenses/LICENSE-2.0                            //
//                                                                           //
// Unless required by applicable law or agreed to in writing, software       //
// distributed under the License is distributed on an "AS IS" BASIS,         //
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  //
// See the License for the specific language governing permissions and       //
// limitations under the License.                                            //
//                                                                           //
// ========================================================================= //
#pragma once

#include "Material.hpp"

namespace smks { namespace rndr
{
    class BlendMaterial:
        public Material
    {
        VALID_RTOBJECT
    protected:
        enum { MAX_INPUTS=2 };
        Material::Ptr   _inputs[MAX_INPUTS];    //<! blended materials
        FloatOrTexture  _weight;                //<! blending factor in [0, 1]

        CREATE_RTOBJECT(BlendMaterial, Material)
    protected:
        BlendMaterial(unsigned int scnId, const CtorArgs& args):
            Material(scnId, args)
        {
            dispose();
        }
    public:
        ~BlendMaterial()
        {
            dispose();
        }

        void
        shade(const Ray&            ray,
              const LightPath&      lp,
              const Medium&         medium,
              DifferentialGeometry& dg,
              CompositedBRDF&       brdfs) const
        {
            if (!applyNormalMap(dg, -ray.dir))
                return;

            CompositedBRDF  inputsBRDFs [MAX_INPUTS];
            float           weights     [MAX_INPUTS];

            for (int i = 0; i < MAX_INPUTS; ++i)
                if (_inputs[i])
                    _inputs[i]->shade(
                        ray, lp, medium, dg, inputsBRDFs[i]);
            weights[1] = _weight.get(dg.st);
            weights[0] = 1.0f - weights[1];
            brdfs.blend(MAX_INPUTS, inputsBRDFs, weights);
        }

        void
        assignTextureToParameter(unsigned int parmId, Texture::Ptr const& tex)
        {
            Material::assignTextureToParameter(parmId, tex);
            //---
            if (parmId == parm::weightMap().id())
                _weight.texture = tex;
        }

        void
        setInput(Material::Ptr input, size_t idx)
        {
            if (idx >= MAX_INPUTS)
                throw sys::Exception(
                    "Exceed number of allowed inputs",
                    "Blend Material Input Setter");
            _inputs[idx] = input;
        }

        void
        commitFrameState(const parm::Container& parms, const xchg::IdSet& dirties)
        {
            Material::commitFrameState(parms, dirties);
            //---
            for (xchg::IdSet::const_iterator id = dirties.begin(); id != dirties.end(); ++id)
                if (*id == parm::weight().id())
                    getBuiltIn<float>(parm::weight(), _weight.value, &parms);

#if defined (FLIRT_LOG_ENABLED)
            {
                std::stringstream sstr;

                sstr
                    << "\n--------------------"
                    << "\nblend material (ID = " << scnId() << ")\n";
                for (int n = 0; n < MAX_INPUTS; ++n)
                    sstr
                        << "\n\t- input[" << n << "] = "
                        << (_inputs[n] ? _inputs[n]->scnId() : 0);
                sstr << "\n\t- weight = " << _weight;
                LOG(DEBUG) << sstr.str();
            }
#endif // defined (FLIRT_LOG_ENABLED)
        }

        void
        dispose()
        {
            memset(_inputs, 0, sizeof(Material::Ptr) * MAX_INPUTS);

            getBuiltIn<float>(parm::weight(), _weight.value);
            _weight.texture = sys::null;
            //---
            Material::dispose();
        }
    };
}
}
