// ========================================================================= //
// Modification copyright 2016-2018 SUPAMONKS_STUDIO                         //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
// _________________________________________________________________________ //
// Copyright 2009-2013 Intel Corporation                                     //
//                                                                           //
// Licensed under the Apache License, Version 2.0 (the "License");           //
// you may not use this file except in compliance with the License.          //
// You may obtain a copy of the License at                                   //
//                                                                           //
//     http://www.apache.org/licenses/LICENSE-2.0                            //
//                                                                           //
// Unless required by applicable law or agreed to in writing, software       //
// distributed under the License is distributed on an "AS IS" BASIS,         //
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  //
// See the License for the specific language governing permissions and       //
// limitations under the License.                                            //
//                                                                           //
// ========================================================================= //
#pragma once

#include "Material.hpp"
#include "../brdf/Translucence.hpp"

namespace smks { namespace rndr
{
    class Translucent:
        public Material
    {
        VALID_RTOBJECT
    protected:
        ColorOrTexture  _translucent;
        //--------------
        Translucence*   _transmissionBRDF; // precomputed only if translucent is constant

        CREATE_RTOBJECT(Translucent, Material)
    protected:
        Translucent(unsigned int scnId, const CtorArgs& args):
            Material            (scnId, args),
            _translucent        (math::Vector4f::Zero()),
            //-------------------
            _transmissionBRDF   (nullptr)
        {
            dispose();
        }
    public:
        ~Translucent()
        {
            dispose();
        }

        void
        shade(const Ray&            ray,
              const LightPath&      lp,
              const Medium&         medium,
              DifferentialGeometry& dg,
              CompositedBRDF&       brdfs) const
        {
            if (!applyNormalMap(dg, -ray.dir))
                return;

            brdfs.add(
                _transmissionBRDF
                ? _transmissionBRDF
                : NEW_BRDF(brdfs, Translucence) (
                    _translucent.get(dg.st)));
        }

        void
        assignTextureToParameter(unsigned int parmId, Texture::Ptr const& tex)
        {
            Material::assignTextureToParameter(parmId, tex);
            //---
            if (parmId == parm::transmissionMap().id())
            {
                _translucent.texture = tex;
                disposeTransmissionBRDF();
            }
        }

        void
        commitFrameState(const parm::Container& parms, const xchg::IdSet& dirties)
        {
            Material::commitFrameState(parms, dirties);
            //---
            for (xchg::IdSet::const_iterator id = dirties.begin(); id != dirties.end(); ++id)
                if (*id == parm::transmission().id())
                {
                    getBuiltIn<math::Vector4f>(parm::transmission(), _translucent.value, &parms);
                    disposeTransmissionBRDF();
                }

            if (_transmissionBRDF == nullptr && !_translucent.texture)
                _transmissionBRDF = new Translucence(_translucent.value);

            FLIRT_LOG(LOG(DEBUG)
                << "\n--------------------"
                << "\ntranslucent material (ID = " << scnId() << ")\n"
                << "\n\t- translucent = ( " << _translucent<< " )"
                << "\n--------------------";)
        }

        void
        dispose()
        {
            getBuiltIn<math::Vector4f>(parm::transmission(), _translucent.value);
            _translucent.texture = sys::null;
            //---
            disposeTransmissionBRDF();
            //---
            Material::dispose();
        }

    private:
        void
        disposeTransmissionBRDF()
        {
            if (_transmissionBRDF)
                delete _transmissionBRDF;
            _transmissionBRDF = nullptr;
        }
    };
}
}
