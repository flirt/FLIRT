// ========================================================================= //
// Copyright 2016-2018 SUPAMONKS_STUDIO                                      //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
//                                                                           //
// ========================================================================= //
#pragma once

#include "Material.hpp"
#include "../brdf/Lambertian.hpp"
#include "../brdf/Translucence.hpp"
#include "../brdf/Microfacet.hpp"
#include "../brdf/microfacet/fresnel.hpp"
#include "../brdf/microfacet/BeckmannDistribution.hpp"
#include "../integrator/LightPath.hpp"

namespace smks { namespace rndr
{
    class TranslucentSS:
        public Material
    {
        VALID_RTOBJECT
    private:
        typedef Microfacet<FresnelDielectric, BeckmannDistribution> Gloss;
    private:
        ColorOrTexture      _diffuse;
        ColorOrTexture      _glossy;
        ColorOrTexture      _translucent;
        float               _eta;                   /*<! relative refractive index = 1 /  inside refractive index */
        FloatOrTexture      _roughness;
        FloatOrTexture      _fresnelWeight;
        FloatOrTexture      _translucentWeight;
        FloatOrTexture      _rayLengthFalloff;
        //------------------
        float               _rEta;
        float               _rSqrRayLengthFalloff;  /*<! precomputed only if ray length fall off is constant*/

        CREATE_RTOBJECT(TranslucentSS, Material)
    protected:
        TranslucentSS(unsigned int scnId, const CtorArgs& args):
            Material                (scnId, args),
            _diffuse                (math::Vector4f::Zero()),
            _glossy                 (math::Vector4f::Zero()),
            _translucent            (math::Vector4f::Zero()),
            _eta                    (0.0f),
            _roughness              (0.0f),
            _fresnelWeight          (0.0f),
            _translucentWeight      (0.0f),
            _rayLengthFalloff       (0.0f),
            //----------------------
            _rEta                   (-1.0f),
            _rSqrRayLengthFalloff   (-1.0f)
        {
            dispose();
        }
    public:
        ~TranslucentSS()
        {
            dispose();
        }

        void
        shade(const Ray&            ray,
              const LightPath&      lp,
              const Medium&         medium,
              DifferentialGeometry& dg,
              CompositedBRDF&       brdfs) const
        {
            if (!applyNormalMap(dg, -ray.dir))
                return;

            // 1. mix diffuse and translucent BRDFs
            const float cosThetaI   = math::dot3(dg.Ns, -ray.dir);
            float translucentWeight = lp.rayDepth > 0
                ? ( !_rayLengthFalloff.texture
                    ? math::exp(- math::sqr(ray.tfar) * _rSqrRayLengthFalloff                   )
                    : math::exp(- math::sqr(ray.tfar * math::rcp(_rayLengthFalloff.get(dg.st))) ) )
                : 1.0f; //no attenuation for camera rays

            const float fresnel = cosThetaI > 0.0f
                ? fresnelDielectric(cosThetaI, _rEta)
                : 0.0f;

            translucentWeight   *= math::min(1.0f, _translucentWeight.get(dg.st) + _fresnelWeight.get(dg.st) * fresnel);

            // float diffuseWeight  = 1.0f - translucentWeight;

            // 2. mix result with glossy BRDFs
            // float glossyWeight   = 0.5f;
            translucentWeight   *= 0.5f;
            // diffuseWeight        *= 0.5f;

            brdfs.add(
                NEW_BRDF(brdfs, Lambertian) (/*diffuseWeight **/ _diffuse.get(dg.st)));
            brdfs.add(
                NEW_BRDF(brdfs, Translucence) (translucentWeight * _translucent.get(dg.st)));
            brdfs.add(
                NEW_BRDF(brdfs, Gloss) (
                    /*glossyWeight **/ _glossy.get(dg.st),
                    FresnelDielectric(1.0f, _eta),
                    BeckmannDistribution(_roughness.get(dg.st), dg.Ns)));
        }

        void
        assignTextureToParameter(unsigned int parmId, Texture::Ptr const& tex)
        {
            Material::assignTextureToParameter(parmId, tex);
            //---
            if      (parmId == parm::diffuseReflectanceMap().id())  _diffuse.texture            = tex;
            else if (parmId == parm::glossyReflectanceMap().id())   _glossy.texture             = tex;
            else if (parmId == parm::diffuseTransmissionMap().id()) _translucent.texture        = tex;
            else if (parmId == parm::roughnessMap().id())           _roughness.texture          = tex;
            else if (parmId == parm::fresnelWeightMap().id())       _fresnelWeight.texture      = tex;
            else if (parmId == parm::transmissivityMap().id())      _translucentWeight.texture  = tex;
            else if (parmId == parm::falloffMap().id())
            {
                _rayLengthFalloff.texture   = tex;
                _rSqrRayLengthFalloff       = -1.0f;
            }
        }

        void
        commitFrameState(const parm::Container& parms, const xchg::IdSet& dirties)
        {
            Material::commitFrameState(parms, dirties);
            //---
            for (xchg::IdSet::const_iterator id = dirties.begin(); id != dirties.end(); ++id)
                if      (*id == parm::diffuseReflectance().id())    getBuiltIn<math::Vector4f>  (parm::diffuseReflectance(),    _diffuse.value,             &parms);
                else if (*id == parm::glossyReflectance().id())     getBuiltIn<math::Vector4f>  (parm::glossyReflectance(),     _glossy.value,              &parms);
                else if (*id == parm::diffuseTransmission().id())   getBuiltIn<math::Vector4f>  (parm::diffuseTransmission(),   _translucent.value,         &parms);
                else if (*id == parm::roughness().id())             getBuiltIn<float>           (parm::roughness(),             _roughness.value,           &parms);
                else if (*id == parm::fresnelWeight().id())         getBuiltIn<float>           (parm::fresnelWeight(),         _fresnelWeight.value,       &parms);
                else if (*id == parm::transmissivity().id())        getBuiltIn<float>           (parm::transmissivity(),        _translucentWeight.value,   &parms);
                else if (*id == parm::IOR().id())
                {
                    getBuiltIn<float>(parm::IOR(), _eta,    &parms);
                    _rEta = _eta > 0.0f ? math::rcp(_eta) : 0.0f;
                }
                else if (*id == parm::falloff().id())
                {
                    getBuiltIn<float>(parm::falloff(), _rayLengthFalloff.value, &parms);
                    _rSqrRayLengthFalloff = -1.0f;
                }

            if (_rSqrRayLengthFalloff < 0.0f &&
                !_rayLengthFalloff.texture)
                _rSqrRayLengthFalloff = _rayLengthFalloff.value > 0.0f ? math::sqr(math::rcp(_rayLengthFalloff.value)) : 0.0f;

            FLIRT_LOG(LOG(DEBUG)
                << "\n--------------------"
                << "\ntranslucentSS material (ID = " << scnId() << ")\n"
                << "\n\t- diffuse = ( " << _diffuse << " )"
                << "\n\t- glossy = ( " << _glossy << " )"
                << "\n\t- translucent = ( " << _translucent << " )"
                << "\n\t- eta = " << _eta
                << "\n\t- roughness = ( " << _roughness << " )"
                << "\n\t- fresnel weight = ( " << _fresnelWeight << " )"
                << "\n\t- translucent weight = ( " << _translucentWeight << " )"
                << "\n\t- ray length falloff = ( " << _rayLengthFalloff << " )"
                << "\n--------------------";)
        }

        void
        dispose()
        {
            getBuiltIn<math::Vector4f>(parm::diffuseReflectance(), _diffuse.value);
            _diffuse.texture = sys::null;

            getBuiltIn<math::Vector4f>(parm::glossyReflectance(), _glossy.value);
            _glossy.texture = sys::null;

            getBuiltIn<math::Vector4f>(parm::diffuseTransmission(), _translucent.value);
            _translucent.texture = sys::null;

            getBuiltIn<float>(parm::roughness(), _roughness.value);
            _roughness.texture = sys::null;

            getBuiltIn<float>(parm::fresnelWeight(), _fresnelWeight.value);
            _fresnelWeight.texture = sys::null;

            getBuiltIn<float>(parm::transmissivity(), _translucentWeight.value);
            _translucentWeight.texture = sys::null;

            getBuiltIn<float>(parm::IOR(), _eta);

            getBuiltIn<float>(parm::falloff(), _rayLengthFalloff.value);
            _rayLengthFalloff.texture = sys::null;
            //---
            _rEta                   = -1.0f;
            _rSqrRayLengthFalloff   = -1.0f;
            //---
            Material::dispose();
        }
    };
}
}
