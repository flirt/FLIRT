// ========================================================================= //
// Modification copyright 2016-2018 SUPAMONKS_STUDIO                         //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
// _________________________________________________________________________ //
// Copyright 2009-2013 Intel Corporation                                     //
//                                                                           //
// Licensed under the Apache License, Version 2.0 (the "License");           //
// you may not use this file except in compliance with the License.          //
// You may obtain a copy of the License at                                   //
//                                                                           //
//     http://www.apache.org/licenses/LICENSE-2.0                            //
//                                                                           //
// Unless required by applicable law or agreed to in writing, software       //
// distributed under the License is distributed on an "AS IS" BASIS,         //
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  //
// See the License for the specific language governing permissions and       //
// limitations under the License.                                            //
//                                                                           //
// ========================================================================= //
#pragma once

#include "Material.hpp"
#include "../brdf/DielectricReflection.hpp"
#include "../brdf/DielectricTransmission.hpp"

namespace smks { namespace rndr
{
    /*! Implements a dielectric material such as glass. */
    class Dielectric:
        public Material
    {
        VALID_RTOBJECT
    private:
        Medium                  _mediumOutside;         //!< Outside medium.
        Medium                  _mediumInside;          //!< Inside medium.
        //----------------------
        DielectricReflection*   _reflectionInOutBRDF;   //!< Reflection component for inside to outside transition.
        DielectricReflection*   _reflectionOutInBRDF;   //!< Reflection component for outside to inside transition.
        DielectricTransmission* _transmissionInOutBRDF; //!< Transmission component for inside to outside transition.
        DielectricTransmission* _transmissionOutInBRDF; //!< Transmission component for outside to inside transition.

        CREATE_RTOBJECT(Dielectric, Material)
    protected:
        Dielectric(unsigned int scnId, const CtorArgs& args):
            Material                (scnId, args),
            _mediumOutside          (),
            _mediumInside           (),
            //----------------------
            _reflectionInOutBRDF    (nullptr),
            _reflectionOutInBRDF    (nullptr),
            _transmissionInOutBRDF  (nullptr),
            _transmissionOutInBRDF  (nullptr)
        {
            dispose();
        }
    public:
        ~Dielectric()
        {
            dispose();
        }

        void
        shade(const Ray&            ray,
              const LightPath&      lp,
              const Medium&         medium,
              DifferentialGeometry& dg,
              CompositedBRDF&       brdfs) const
        {
            if (!applyNormalMap(dg, -ray.dir))
                return;

            assert(_reflectionInOutBRDF  );
            assert(_reflectionOutInBRDF  );
            assert(_transmissionInOutBRDF);
            assert(_transmissionOutInBRDF);

            /*! the ray transitions from outside to inside */
            if (medium == _mediumOutside)
            {
                brdfs.add(_reflectionOutInBRDF);
                brdfs.add(_transmissionOutInBRDF);
            }

            /*! the ray transitions from inside to outside */
            else
            {
                brdfs.add(_reflectionInOutBRDF);
                brdfs.add(_transmissionInOutBRDF);
            }
        }

        __forceinline // virtual
        const Medium&
        nextMedium(const Medium& current) const
        {
            return current == _mediumInside
                ? _mediumOutside
                : _mediumInside;
        }

        void
        commitFrameState(const parm::Container& parms, const xchg::IdSet& dirties)
        {
            Material::commitFrameState(parms, dirties);
            //---
            for (xchg::IdSet::const_iterator id = dirties.begin(); id != dirties.end(); ++id)
                if (*id == parm::intIOR().id())
                {
                    getBuiltIn<float>(parm::intIOR(), _mediumInside.eta, &parms);
                    disposeBRDF();
                }
                else if (*id == parm::intTransmission().id())
                    getBuiltIn<math::Vector4f>(parm::intTransmission(), _mediumInside.transmission, &parms);
                else if (*id == parm::extIOR().id())
                {
                    getBuiltIn<float>(parm::extIOR(), _mediumOutside.eta, &parms);
                    disposeBRDF();
                }
                else if (*id == parm::extTransmission().id())
                    getBuiltIn<math::Vector4f>(parm::extTransmission(), _mediumOutside.transmission, &parms);

            if (_reflectionInOutBRDF == nullptr)
                _reflectionInOutBRDF    = new DielectricReflection  (_mediumInside.eta,  _mediumOutside.eta);
            if (_reflectionOutInBRDF == nullptr)
                _reflectionOutInBRDF    = new DielectricReflection  (_mediumOutside.eta, _mediumInside.eta);
            if (_transmissionInOutBRDF == nullptr)
                _transmissionInOutBRDF  = new DielectricTransmission(_mediumInside.eta,  _mediumOutside.eta);
            if (_transmissionOutInBRDF == nullptr)
                _transmissionOutInBRDF  = new DielectricTransmission(_mediumOutside.eta, _mediumInside.eta);

            FLIRT_LOG(LOG(DEBUG)
                << "\n--------------------"
                << "\ndielectric material (ID = " << scnId() << ")\n"
                << "\n\t- medium outside transmission = ( " << _mediumOutside.transmission.transpose() << " )"
                << "\n\t- medium outside eta = " << _mediumOutside.eta
                << "\n\t- medium inside transmission = ( " << _mediumInside.transmission.transpose() << " )"
                << "\n\t- medium inside eta = " << _mediumInside.eta
                << "\n--------------------";)
        }

        void
        dispose()
        {
            getBuiltIn<float>           (parm::intIOR(),            _mediumInside.eta);
            getBuiltIn<math::Vector4f>  (parm::intTransmission(),   _mediumInside.transmission);
            getBuiltIn<float>           (parm::extIOR(),            _mediumOutside.eta);
            getBuiltIn<math::Vector4f>  (parm::extTransmission(),   _mediumOutside.transmission);
            //---
            disposeBRDF();
            //---
            Material::dispose();
        }

    private:
        void
        disposeBRDF()
        {
            if (_reflectionInOutBRDF)
                delete _reflectionInOutBRDF;
            _reflectionInOutBRDF = nullptr;
            if (_reflectionOutInBRDF)
                delete _reflectionOutInBRDF;
            _reflectionOutInBRDF = nullptr;
            if (_transmissionInOutBRDF)
                delete _transmissionInOutBRDF;
            _transmissionInOutBRDF = nullptr;
            if (_transmissionOutInBRDF)
                delete _transmissionOutInBRDF;
            _transmissionOutInBRDF = nullptr;
        }
    };
}
}
