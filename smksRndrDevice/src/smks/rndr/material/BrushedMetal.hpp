// ========================================================================= //
// Modification copyright 2016-2018 SUPAMONKS_STUDIO                         //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
// _________________________________________________________________________ //
// Copyright 2009-2013 Intel Corporation                                     //
//                                                                           //
// Licensed under the Apache License, Version 2.0 (the "License");           //
// you may not use this file except in compliance with the License.          //
// You may obtain a copy of the License at                                   //
//                                                                           //
//     http://www.apache.org/licenses/LICENSE-2.0                            //
//                                                                           //
// Unless required by applicable law or agreed to in writing, software       //
// distributed under the License is distributed on an "AS IS" BASIS,         //
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  //
// See the License for the specific language governing permissions and       //
// limitations under the License.                                            //
//                                                                           //
// ========================================================================= //
#pragma once

#include <smks/math/math.hpp>

#include "Material.hpp"
#include "../brdf/Conductor.hpp"
#include "../brdf/Microfacet.hpp"
#include "../brdf/microfacet/fresnel.hpp"
#include "../brdf/microfacet/AnisotropicPowerCosineDistribution.hpp"

namespace smks { namespace rndr
{
    /*! Implements a brushed metal material. The metal is modelled by a
    *  microfacet BRDF with a fresnel term for metals and a power
    *  cosine distribution for different roughness of the material. */
    class BrushedMetal:
        public Material
    {
        VALID_RTOBJECT
    protected:
        typedef Microfacet<FresnelConductor,AnisotropicPowerCosineDistribution > MicrofacetBRDF;
    protected:
        ColorOrTexture  _reflectance;   //!< Reflectivity of the metal
        math::Vector4f  _eta;           //!< Real part of refraction index
        math::Vector4f  _k;             //!< Imaginary part of refraction index
        FloatOrTexture  _roughnessX;    //!< Roughness parameter in X direction. The range goes from 0 (specular) to 1 (diffuse).
        FloatOrTexture  _roughnessY;    //!< Roughness parameter in Y direction. The range goes from 0 (specular) to 1 (diffuse).
        //--------------
        Conductor*      _conductorBRDF;
        float           _rRoughnessX;   //!< Reciprocal roughness parameter in X direction. (only valid if roughnessX is constant)
        float           _rRoughnessY;   //!< Reciprocal roughness parameter in Y direction. (only valid if roughnessY is constant)

        CREATE_RTOBJECT(BrushedMetal, Material)
    protected:
        BrushedMetal(unsigned int scnId, const CtorArgs& args):
            Material        (scnId, args),
            _reflectance    (math::Vector4f::Zero()),
            _eta            (math::Vector4f::Zero()),
            _k              (math::Vector4f::Zero()),
            _roughnessX     (0.0f),
            _roughnessY     (0.0f),
            //---------------
            _conductorBRDF  (nullptr),
            _rRoughnessX    (-1.0f),
            _rRoughnessY    (-1.0f)
        {
            dispose();
        }
    public:
        ~BrushedMetal()
        {
            dispose();
        }

        void
        shade(const Ray&            ray,
              const LightPath&      lp,
              const Medium&         medium,
              DifferentialGeometry& dg,
              CompositedBRDF&       brdfs) const
        {
            if (!applyNormalMap(dg, -ray.dir))
                return;

            const float roughnessX  = _roughnessX.get(dg.st);
            const float roughnessY  = _roughnessY.get(dg.st);

            if (roughnessX < 1e-6f ||
                roughnessY < 1e-6f)
            {
                /*! handle the special case of a specular metal through a special BRDF */
                brdfs.add(
                    _conductorBRDF
                    ? _conductorBRDF
                    : NEW_BRDF(brdfs, Conductor) (
                        _reflectance.get(dg.st), _eta, _k));
                return;
            }

            const float rRoughnessX = !_roughnessX.texture ? math::rcp(roughnessX) : _rRoughnessX;
            const float rRoughnessY = !_roughnessY.texture ? math::rcp(roughnessY) : _rRoughnessY;

            brdfs.add(
                NEW_BRDF(brdfs, MicrofacetBRDF) (
                    _reflectance.get(dg.st),
                    FresnelConductor(_eta, _k),
                    AnisotropicPowerCosineDistribution(
                        dg.Tx, rRoughnessX, dg.Ty, rRoughnessY, dg.Ns)));
        }

        void
        assignTextureToParameter(unsigned int parmId, Texture::Ptr const& tex)
        {
            Material::assignTextureToParameter(parmId, tex);
            //---
            if (parmId == parm::reflectanceMap().id())
            {
                _reflectance.texture = tex;
                disposeConductorBRDF();
            }
            else if (parmId == parm::roughnessXMap().id())
            {
                _roughnessX.texture = tex;
                _rRoughnessX = -1.0f;
            }
            else if (parmId == parm::roughnessYMap().id())
            {
                _roughnessY.texture = tex;
                _rRoughnessY = -1.0f;
            }
        }

        void
        commitFrameState(const parm::Container& parms, const xchg::IdSet& dirties)
        {
            Material::commitFrameState(parms, dirties);
            //---
            for (xchg::IdSet::const_iterator id = dirties.begin(); id != dirties.end(); ++id)
                if (*id == parm::reflectance().id())
                {
                    getBuiltIn<math::Vector4f>(parm::reflectance(), _reflectance.value, &parms);
                    disposeConductorBRDF();
                }
                else if (*id == parm::IORreal().id())
                {
                    getBuiltIn<math::Vector4f>(parm::IORreal(), _eta, &parms);
                    disposeConductorBRDF();
                }
                else if (*id == parm::IORimag().id())
                {
                    getBuiltIn<math::Vector4f>(parm::IORimag(), _k, &parms);
                    disposeConductorBRDF();
                }
                else if (*id == parm::roughnessX().id())
                {
                    getBuiltIn<float>(parm::roughnessX(), _roughnessX.value, &parms);
                    _rRoughnessX = -1.0f;
                }
                else if (*id == parm::roughnessY().id())
                {
                    getBuiltIn<float>(parm::roughnessY(), _roughnessY.value, &parms);
                    _rRoughnessY = -1.0f;
                }

            if (_conductorBRDF == nullptr && !_reflectance.texture)
                _conductorBRDF = new Conductor(_reflectance.value, _eta, _k);
            if (_rRoughnessX < 0.0f && !_roughnessX.texture)
                _rRoughnessX = _roughnessX.value > 0.0f ? math::rcp(_roughnessX.value) : 0.0f;
            if (_rRoughnessY < 0.0f && !_roughnessY.texture)
                _rRoughnessY = _roughnessY.value > 0.0f ? math::rcp(_roughnessY.value) : 0.0f;

            FLIRT_LOG(LOG(DEBUG)
                << "\n--------------------"
                << "\nbrushed metal material (ID = " << scnId() << ")\n"
                << "\n\t- reflectance = ( " << _reflectance << " )"
                << "\n\t- eta = " << _eta.transpose()
                << "\n\t- k = " << _k.transpose()
                << "\n\t- roughness-x = ( " << _roughnessX << " )"
                << "\n\t- roughness-y = ( " << _roughnessY << " )"
                << "\n--------------------";)
        }

        void
        dispose()
        {
            getBuiltIn<math::Vector4f>(parm::reflectance(), _reflectance.value);
            _reflectance.texture = sys::null;

            getBuiltIn<math::Vector4f>(parm::IORreal(), _eta);
            getBuiltIn<math::Vector4f>(parm::IORimag(), _k);

            getBuiltIn<float>(parm::roughnessX(), _roughnessX.value);
            _roughnessX.texture = sys::null;

            getBuiltIn<float>(parm::roughnessY(), _roughnessY.value);
            _roughnessY.texture = sys::null;
            //---
            disposeConductorBRDF();
            _rRoughnessX    = -1.0f;
            _rRoughnessY    = -1.0f;
            //---
            Material::dispose();
        }

    private:
        void
        disposeConductorBRDF()
        {
            if (_conductorBRDF)
                delete _conductorBRDF;
            _conductorBRDF = nullptr;
        }
    };
}
}
