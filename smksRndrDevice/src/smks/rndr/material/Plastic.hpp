// ========================================================================= //
// Modification copyright 2016-2018 SUPAMONKS_STUDIO                         //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
// _________________________________________________________________________ //
// Copyright 2009-2013 Intel Corporation                                     //
//                                                                           //
// Licensed under the Apache License, Version 2.0 (the "License");           //
// you may not use this file except in compliance with the License.          //
// You may obtain a copy of the License at                                   //
//                                                                           //
//     http://www.apache.org/licenses/LICENSE-2.0                            //
//                                                                           //
// Unless required by applicable law or agreed to in writing, software       //
// distributed under the License is distributed on an "AS IS" BASIS,         //
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  //
// See the License for the specific language governing permissions and       //
// limitations under the License.                                            //
//                                                                           //
// ========================================================================= //
#pragma once

#include "Material.hpp"
#include "../brdf/Microfacet.hpp"
#include "../brdf/microfacet/fresnel.hpp"
#include "../brdf/DielectricLayer.hpp"
#include "../brdf/DielectricReflection.hpp"
#include "../brdf/Lambertian.hpp"

namespace smks { namespace rndr
{
    /*! Implements a plastic material. A dielectric layer over a diffuse
    *  surface is modeled through a dieletric layer BRDF for the
    *  covered diffuse part, and a microfacet BRDF for the rough
    *  dielectric surface. */
    class Plastic:
        public Material
    {
        VALID_RTOBJECT
    private:
        typedef Microfacet<FresnelDielectric,PowerCosineDistribution > MicrofacetPlastic;
    private:
        ColorOrTexture                  _pigmentColor;      //!< math::Vector4f of the diffuse layer.
        float                           _eta;               //!< Refraction index of the dielectric layer.
        FloatOrTexture                  _roughness;         //!< Roughness parameter. The range goes from 0 (specular) to 1 (diffuse).
        //-----------------------------
        DielectricLayer<Lambertian>*    _diffuseBRDF;       //!< Precomputed only if pigment color is constant
        DielectricReflection*           _reflectionBRDF;
        float                           _rRoughness;        //!< Reciprocal roughness parameter. (only if roughness is constant)

        CREATE_RTOBJECT(Plastic, Material)
    protected:
        Plastic(unsigned int scnId, const CtorArgs& args):
            Material        (scnId, args),
            _pigmentColor   (math::Vector4f::Zero()),
            _eta            (0.0f),
            _roughness      (0.0f),
            //--------------
            _diffuseBRDF    (nullptr),
            _reflectionBRDF (nullptr),
            _rRoughness     (-1.0f)
        {
            dispose();
        }
    public:
        ~Plastic()
        {
            dispose();
        }

        void
        shade(const Ray&            ray,
              const LightPath&      lp,
              const Medium&         medium,
              DifferentialGeometry& dg,
              CompositedBRDF&       brdfs) const
        {
            if (!applyNormalMap(dg, -ray.dir))
                return;

            /*! the dielectric layer that models the covered diffuse part */
            brdfs.add(
                _diffuseBRDF
                ? _diffuseBRDF
                : NEW_BRDF(brdfs, DielectricLayer<Lambertian>) (
                    math::Vector4f::Ones(),
                    1.0f,
                    _eta,
                    Lambertian(_pigmentColor.get(dg.st))));

            const float roughness = _roughness.get(dg.st);

            if (roughness < 1e-6f)
            {
                /*! use dielectric reflection in case of a specular surface */
                assert(_reflectionBRDF);
                brdfs.add(_reflectionBRDF);
            }
            else
            {
                /*! otherwise use the microfacet BRDF to model the rough surface */
                const float rRoughness = !_roughness.texture ? _rRoughness : math::rcp(roughness);
                brdfs.add(
                    NEW_BRDF(brdfs, MicrofacetPlastic) (
                        math::Vector4f::Ones(),
                        FresnelDielectric(1.0f, _eta),
                        PowerCosineDistribution(rRoughness, dg.Ns)));
            }
        }

        void
        assignTextureToParameter(unsigned int parmId, Texture::Ptr const& tex)
        {
            Material::assignTextureToParameter(parmId, tex);
            //---
            if (parmId == parm::diffuseReflectanceMap().id())
            {
                _pigmentColor.texture = tex;
                disposeDiffuseBRDF();
            }
            else if (parmId == parm::roughnessMap().id())
            {
                _roughness.texture = tex;
                _rRoughness = -1.0f;
            }
        }

        void
        commitFrameState(const parm::Container& parms, const xchg::IdSet& dirties)
        {
            Material::commitFrameState(parms, dirties);
            //---
            for (xchg::IdSet::const_iterator id = dirties.begin(); id != dirties.end(); ++id)
                if (*id == parm::diffuseReflectance().id())
                {
                    getBuiltIn<math::Vector4f>(parm::diffuseReflectance(), _pigmentColor.value, &parms);
                    disposeDiffuseBRDF();
                }
                else if (*id == parm::IOR().id())
                {
                    getBuiltIn<float>(parm::IOR(), _eta, &parms);
                    disposeDiffuseBRDF();
                    disposeReflectionBRDF();
                }
                else if (*id == parm::roughness().id())
                {
                    getBuiltIn<float>(parm::roughness(), _roughness.value, &parms);
                    _rRoughness = -1.0f;
                }

            if (_diffuseBRDF == nullptr && !_pigmentColor.texture)
                _diffuseBRDF = new DielectricLayer<Lambertian>(math::Vector4f::Ones(), 1.0f, _eta, Lambertian(_pigmentColor.value));
            if (_reflectionBRDF == nullptr)
                _reflectionBRDF = new DielectricReflection(1.0f, _eta);
            _rRoughness = !_roughness.texture ? math::rcp(_roughness.value) : -1.0f;

            FLIRT_LOG(LOG(DEBUG)
                << "\n--------------------"
                << "\nplastic material (ID = " << scnId() << ")\n"
                << "\n\t- pigment color = ( " << _pigmentColor << " )"
                << "\n\t- eta = " << _eta
                << "\n\t- roughness = ( " << _roughness << " )"
                << "\n--------------------";)
        }

        void
        dispose()
        {
            getBuiltIn<math::Vector4f>(parm::diffuseReflectance(), _pigmentColor.value);
            _pigmentColor.texture = sys::null;

            getBuiltIn<float>(parm::IOR(), _eta);

            getBuiltIn<float>(parm::roughness(), _roughness.value);
            _roughness.texture = sys::null;
            //---
            disposeDiffuseBRDF();
            disposeReflectionBRDF();
            _rRoughness = -1.0f;
            //---
            Material::dispose();
        }

    private:
        void
        disposeDiffuseBRDF()
        {
            if (_diffuseBRDF)
                delete _diffuseBRDF;
            _diffuseBRDF = nullptr;
        }

        void
        disposeReflectionBRDF()
        {
            if (_reflectionBRDF)
                delete _reflectionBRDF;
            _reflectionBRDF = nullptr;
        }
    };
}
}
