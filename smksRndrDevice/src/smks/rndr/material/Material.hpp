// ========================================================================= //
// Modification copyright 2016-2018 SUPAMONKS_STUDIO                         //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
// _________________________________________________________________________ //
// Copyright 2009-2013 Intel Corporation                                     //
//                                                                           //
// Licensed under the Apache License, Version 2.0 (the "License");           //
// you may not use this file except in compliance with the License.          //
// You may obtain a copy of the License at                                   //
//                                                                           //
//     http://www.apache.org/licenses/LICENSE-2.0                            //
//                                                                           //
// Unless required by applicable law or agreed to in writing, software       //
// distributed under the License is distributed on an "AS IS" BASIS,         //
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  //
// See the License for the specific language governing permissions and       //
// limitations under the License.                                            //
//                                                                           //
// ========================================================================= //
#pragma once

#include <smks/parm/BuiltIns.hpp>

#include "../ObjectBase.hpp"
#include "../brdf/CompositedBRDF.hpp"
#include "material_attrib.hpp"
#include "Medium.hpp"

namespace smks { namespace rndr
{
    struct  LightPath;
    class   Texture;

    class Material:
        public ObjectBase
    {
    public:
        typedef sys::Ref<Material> Ptr;

    private:
        Texture::Ptr _normalMap;

    protected:
        Material(unsigned int scnId, const CtorArgs& args):
            ObjectBase(scnId, args)
        { }

    public:
        //<! Shades a location and and returns a composited BRDF
        virtual
        void
        shade(const Ray&,               /*!< The ray arriving at the point to shade.    */
              const LightPath&,
              const Medium&,            /*!< The medium this ray travels inside.        */
              DifferentialGeometry&,    /*!< The point to shade on a surface.           */
              CompositedBRDF&)          /*!< Container for generated BRDF components.   */ const = 0;

        //<! Tracks the medium when crossing the surface.
        virtual __forceinline
        const Medium&
        nextMedium(const Medium& current) const
        {
            return current; // by default, no media interface is being crossed.
        }

        virtual __forceinline
        bool
        isTransparentForShadowRays() const
        {
            return false;
        }

        //<! Parameterizes material property with textures
        virtual __forceinline
        void
        assignTextureToParameter(unsigned int parmId, Texture::Ptr const& tex)
        {
            if (parmId == parm::normalMap().id())
                _normalMap = tex;
        }

        virtual __forceinline
        void
        setInput(Material::Ptr, size_t idx)
        { }

        //--- intersection/occlusion callbacks -------------
        virtual __forceinline bool intersected(Ray&) const { return false; }
        virtual __forceinline bool occluded   (Ray&) const { return false; }
        //--------------------------------------------------

        virtual inline
        void
        dispose()
        {
            _normalMap = sys::null;
        }

        virtual inline
        void
        commitFrameState(const parm::Container& parms, const xchg::IdSet& dirties)
        {
            logParameters(scnId(), parms, "builtIns", dirties);
            logParameters(scnId(), userParameters(), "userParms", "USER");
        }

        inline
        void
        beginSubframeCommits(size_t)
        { }

        inline
        void
        commitSubframeState(size_t, const parm::Container&)
        { }

        inline
        void
        endSubframeCommits()
        { }

        inline
        bool
        perSubframeParameter(unsigned int) const
        {
            return false;
        }

    protected:
        //<! Perturbs normal vector in accordance to normal map. Returns true if shading computations can proceed.
        bool
        applyNormalMap(DifferentialGeometry& dg, const math::Vector4f& wo) const
        {
            if (!_normalMap)
                return true;
            const math::Vector4f& n = 2.0f * _normalMap->get(dg.st) - math::Vector4f::Ones(); // perturbed normal in tangent space
            // n.w() = 0.0f; // w is not involved in following computations

            const float cTheta = dg.Ng.dot(wo);
            assert(!(cTheta < 0.0f));

            dg.Ns   = (n.x() * dg.Tx + n.y() * dg.Ty + n.z() * dg.Ns)   .normalized();  // perturbed normal in world space
            dg.Tx   = (dg.Tx - dg.Ns * dg.Tx.dot(dg.Ns))                .normalized();  // project Tx on perturbed normal plane
            dg.Ty   = (dg.Ty - dg.Ns * dg.Ty.dot(dg.Ns))                .normalized();  // project Ty on perturbed normal plane

            return !(cTheta * dg.Ns.dot(wo) < 0.0f);
        }
    };
}
}
