// ========================================================================= //
// Modification copyright 2016-2018 SUPAMONKS_STUDIO                         //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
// _________________________________________________________________________ //
// Copyright 2009-2013 Intel Corporation                                     //
//                                                                           //
// Licensed under the Apache License, Version 2.0 (the "License");           //
// you may not use this file except in compliance with the License.          //
// You may obtain a copy of the License at                                   //
//                                                                           //
//     http://www.apache.org/licenses/LICENSE-2.0                            //
//                                                                           //
// Unless required by applicable law or agreed to in writing, software       //
// distributed under the License is distributed on an "AS IS" BASIS,         //
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  //
// See the License for the specific language governing permissions and       //
// limitations under the License.                                            //
//                                                                           //
// ========================================================================= //
#pragma once

#include "Material.hpp"
#include "../brdf/DielectricReflection.hpp"
#include "../brdf/ThinDielectricTransmission.hpp"

namespace smks { namespace rndr
{
    /*! Implements a thin dielectricum material. The model uses a
    *  dielectric reflection BRDF and thin dielectric transmission
    *  BRDF. */
    class ThinDielectric:
        public Material
    {
        VALID_RTOBJECT
    protected:
        ColorOrTexture              _transmission;      //!< Transmission coefficient of material.
        float                       _eta;               //!< Refraction index of material.
        FloatOrTexture              _thickness;         //!< Thickness of material layer.
        //--------------------------
        DielectricReflection*       _reflectionBRDF;
        ThinDielectricTransmission* _transmissionBRDF;  // precomputed only if transmission and thickness are constant

        CREATE_RTOBJECT(ThinDielectric, Material)
    protected:
        ThinDielectric(unsigned int scnId, const CtorArgs& args):
            Material            (scnId, args),
            _transmission       (math::Vector4f::Zero()),
            _eta                (0.0f),
            _thickness          (0.0f),
            //------------------
            _reflectionBRDF     (nullptr),
            _transmissionBRDF   (nullptr)
        {
            dispose();
        }
    public:
        ~ThinDielectric()
        {
            dispose();
        }

        __forceinline
        bool
        isTransparentForShadowRays() const
        {
            return true;
        }

        void
        shade(const Ray&            ray,
              const LightPath&      lp,
              const Medium&         medium,
              DifferentialGeometry& dg,
              CompositedBRDF&       brdfs) const
        {
            if (!applyNormalMap(dg, -ray.dir))
                return;

            assert(_reflectionBRDF);
            brdfs.add(_reflectionBRDF);
            brdfs.add(
                _transmissionBRDF
                ? _transmissionBRDF
                : NEW_BRDF(brdfs, ThinDielectricTransmission) (
                    1.0f, _eta, _transmission.get(dg.st), _thickness.get(dg.st)));
        }

        void
        assignTextureToParameter(unsigned int parmId, Texture::Ptr const& tex)
        {
            Material::assignTextureToParameter(parmId, tex);
            //---
            if (parmId == parm::transmissionMap().id())
            {
                _transmission.texture = tex;
                disposeTransmissionBRDF();
            }
            else if (parmId == parm::thicknessMap().id())
            {
                _thickness.texture = tex;
                disposeTransmissionBRDF();
            }
        }

        void
        commitFrameState(const parm::Container& parms, const xchg::IdSet& dirties)
        {
            Material::commitFrameState(parms, dirties);
            //---
            for (xchg::IdSet::const_iterator id = dirties.begin(); id != dirties.end(); ++id)
                if (*id == parm::transmission().id())
                {
                    getBuiltIn<math::Vector4f>(parm::transmission(), _transmission.value, &parms);
                    disposeTransmissionBRDF();
                }
                else if (*id == parm::IOR().id())
                {
                    getBuiltIn<float>(parm::IOR(), _eta, &parms);
                    disposeTransmissionBRDF();
                    disposeReflectionBRDF();
                }
                else if (*id == parm::thickness().id())
                {
                    getBuiltIn<float>(parm::thickness(), _thickness.value, &parms);
                    disposeTransmissionBRDF();
                }

            if (_reflectionBRDF == nullptr)
                _reflectionBRDF = new DielectricReflection(1.0f, _eta);
            if (_transmissionBRDF == nullptr &&
                !_transmission.texture &&
                !_thickness.texture)
                _transmissionBRDF = new ThinDielectricTransmission(1.0f, _eta, _transmission.value, _thickness.value);

            FLIRT_LOG(LOG(DEBUG)
                << "\n--------------------"
                << "\nthin dielectric material (ID = " << scnId() << ")\n"
                << "\n\t- transmission = ( " << _transmission << " )"
                << "\n\t- eta = " << _eta
                << "\n\t- thickness = ( " << _thickness << " )"
                << "\n--------------------";)
        }

        void
        dispose()
        {
            getBuiltIn<math::Vector4f>(parm::transmission(), _transmission.value);
            _transmission.texture = sys::null;

            getBuiltIn<float>(parm::IOR(), _eta);

            getBuiltIn<float>(parm::thickness(), _thickness.value);
            _thickness.texture = sys::null;
            //---
            disposeReflectionBRDF();
            disposeTransmissionBRDF();
            //---
            Material::dispose();
        }

    private:
        void
        disposeReflectionBRDF()
        {
            if (_reflectionBRDF)
                delete _reflectionBRDF;
            _reflectionBRDF = nullptr;
        }

        void
        disposeTransmissionBRDF()
        {
            if (_transmissionBRDF)
                delete _transmissionBRDF;
            _transmissionBRDF = nullptr;
        }
    };
}
}
