// ========================================================================= //
// Copyright 2016-2018 SUPAMONKS_STUDIO                                      //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
//                                                                           //
// ========================================================================= //
#pragma once

#include "Material.hpp"
#include "../brdf/Lambertian.hpp"

namespace smks { namespace rndr
{
    class EyeMaterial:
        public Material
    {
        VALID_RTOBJECT
    private:
        ////////////////
        // struct Inputs
        ////////////////
        struct Inputs
        {
            ALIGNED_CLASS
            //---
            math::Vector4f center, front, up;
            math::Vector4f globeColor, hilightColor, irisColor;
            math::Vector2f hilightCoordsD, irisDimensions, pupilDimensions;
            float eyeRadius, corneaIOR;
            float hilightAngSizeD, hilightSmoothness;
            float irisAngSizeD, irisRotationD, irisDepth, irisEdgeBlur, irisEdgeOffset, irisNormalSmoothness;
            float pupilBlur, pupilSize;
            bool usedAsPoint;
            //---
        public:
            Inputs();

            void
            defaults();
        };
        ////////////////////////
        // struct Precomputation
        ////////////////////////
        struct Precomputation
        {
            ALIGNED_CLASS
            //---
            math::Vector4f front, up, side; //<! rotated iris frame axes (in world space)
            math::Vector4f hilightDir;      //<! highlightDir direction (in world space)
            float rCorneaIOR;
            float irisRWidth, irisRHeight;
            float irisConeTipFrontOffset;   //<! iris cone tip offset along front axis
            float irisConeCAng2;            //<! iris cone half angle (as squared cosine)
            float irisConeRLength;
            float pupilWidthHeight;
            float hilightCAngSteps[2];              //<! steps controlling transition between hilightDir and surface (globe/iris) colors
            float irisAngStepsForEyeColor[2];       //<! steps controlling transition between global and iris colors
            float irisAngStepsForNormalBevel[2];    //<! steps controlling mixing factor between globe and iris normal vectors
        private:
            // non-copyable
            Precomputation(const Precomputation&);
            Precomputation& operator=(const Precomputation&);
        public:
            Precomputation();

            void
            reset();
            void
            initialize(const Inputs&);
        };
        ////////////////////////
    protected:
        Inputs          _in;
        Precomputation  _pc;

        CREATE_RTOBJECT(EyeMaterial, Material)
    public:
        EyeMaterial(unsigned int scnId, const CtorArgs& args):
            Material(scnId, args),
            _in     (),
            _pc     ()
        {
            dispose();
        }
        inline
        ~EyeMaterial()
        {
            dispose();
        }

        void
        shade(const Ray&            ray,
              const LightPath&      lp,
              const Medium&         medium,
              DifferentialGeometry& dg,
              CompositedBRDF&       brdfs) const
        {
            brdfs.add(
                NEW_BRDF(brdfs, Lambertian) (
                    getShadingColor(dg.P, dg.Ns, -ray.dir)));
        }

        void
        commitFrameState(const parm::Container& parms, const xchg::IdSet& dirties)
        {
            Material::commitFrameState(parms, dirties);
            //---
            for (xchg::IdSet::const_iterator id = dirties.begin(); id != dirties.end(); ++id)
                if      (*id == parm::center().id())                    getBuiltIn<math::Vector4f>  (parm::center(),                    _in.center,                 &parms);
                else if (*id == parm::front().id())                     getBuiltIn<math::Vector4f>  (parm::front(),                     _in.front,                  &parms);
                else if (*id == parm::up().id())                        getBuiltIn<math::Vector4f>  (parm::up(),                        _in.up,                     &parms);
                else if (*id == parm::eyeGlobeColor().id())             getBuiltIn<math::Vector4f>  (parm::eyeGlobeColor(),             _in.globeColor,             &parms);
                else if (*id == parm::eyeHighlightColor().id())         getBuiltIn<math::Vector4f>  (parm::eyeHighlightColor(),         _in.hilightColor,           &parms);
                else if (*id == parm::eyeIrisColor().id())              getBuiltIn<math::Vector4f>  (parm::eyeIrisColor(),              _in.irisColor,              &parms);
                else if (*id == parm::eyeHighlightCoordsD().id())       getBuiltIn<math::Vector2f>  (parm::eyeHighlightCoordsD(),       _in.hilightCoordsD,         &parms);
                else if (*id == parm::eyeIrisDimensions().id())         getBuiltIn<math::Vector2f>  (parm::eyeIrisDimensions(),         _in.irisDimensions,         &parms);
                else if (*id == parm::eyePupilDimensions().id())        getBuiltIn<math::Vector2f>  (parm::eyePupilDimensions(),        _in.pupilDimensions,        &parms);
                else if (*id == parm::eyeRadius().id())                 getBuiltIn<float>           (parm::eyeRadius(),                 _in.eyeRadius,              &parms);
                else if (*id == parm::eyeCorneaIOR().id())              getBuiltIn<float>           (parm::eyeCorneaIOR(),              _in.corneaIOR,              &parms);
                else if (*id == parm::eyeHighlightAngSizeD().id())      getBuiltIn<float>           (parm::eyeHighlightAngSizeD(),      _in.hilightAngSizeD,        &parms);
                else if (*id == parm::eyeHighlightSmoothness().id())    getBuiltIn<float>           (parm::eyeHighlightSmoothness(),    _in.hilightSmoothness,      &parms);
                else if (*id == parm::eyeIrisAngSizeD().id())           getBuiltIn<float>           (parm::eyeIrisAngSizeD(),           _in.irisAngSizeD,           &parms);
                else if (*id == parm::eyeIrisRotationD().id())          getBuiltIn<float>           (parm::eyeIrisRotationD(),          _in.irisRotationD,          &parms);
                else if (*id == parm::eyeIrisDepth().id())              getBuiltIn<float>           (parm::eyeIrisDepth(),              _in.irisDepth,              &parms);
                else if (*id == parm::eyeIrisEdgeBlur().id())           getBuiltIn<float>           (parm::eyeIrisEdgeBlur(),           _in.irisEdgeBlur,           &parms);
                else if (*id == parm::eyeIrisEdgeOffset().id())         getBuiltIn<float>           (parm::eyeIrisEdgeOffset(),         _in.irisEdgeOffset,         &parms);
                else if (*id == parm::eyeIrisNormalSmoothness().id())   getBuiltIn<float>           (parm::eyeIrisNormalSmoothness(),   _in.irisNormalSmoothness,   &parms);
                else if (*id == parm::eyePupilBlur().id())              getBuiltIn<float>           (parm::eyePupilBlur(),              _in.pupilBlur,              &parms);
                else if (*id == parm::eyePupilSize().id())              getBuiltIn<float>           (parm::eyePupilSize(),              _in.pupilSize,              &parms);
                else if (*id == parm::usedAsPoint().id())               getBuiltIn<bool>            (parm::usedAsPoint(),               _in.usedAsPoint,            &parms);

            FLIRT_LOG(LOG(DEBUG)
                << "\n--------------------\n"
                << "eye material (ID = " << scnId() << ")\n"
                << "\n\t- center = ( " << _in.center.transpose() << " )"
                << "\n\t- front = ( " << _in.front.transpose() << " )"
                << "\n\t- up = ( " << _in.up.transpose() << " )"
                << "\n\t- used as point ? " << _in.usedAsPoint
                << "\n\t- eye radius = " << _in.eyeRadius
                << "\n\t- cornea IOR = " << _in.corneaIOR
                << "\n\t- iris color = ( " << _in.irisColor.transpose() << " )"
                << "\n\t- iris rotation = " << _in.irisRotationD
                << "\n\t- iris ang size = " << _in.irisAngSizeD
                << "\n\t- iris depth = " << _in.irisDepth
                << "\n\t- iris dims = ( " << _in.irisDimensions.transpose() << " )"
                << "\n\t- iris edge blur = " << _in.irisEdgeBlur
                << "\n\t- iris edge offset = " << _in.irisEdgeOffset
                << "\n\t- iris normal smoothness = " << _in.irisNormalSmoothness
                << "\n\t- pupil dims = ( " << _in.pupilDimensions.transpose() << " )"
                << "\n\t- pupil size = " << _in.pupilSize
                << "\n\t- pupil blur = " << _in.pupilBlur
                << "\n\t- hilight ang size = " << _in.hilightAngSizeD
                << "\n\t- hilight color = ( " << _in.hilightColor.transpose() << " )"
                << "\n\t- hilight sphr coords = ( " << _in.hilightCoordsD.transpose() << " )"
                << "\n\t- hilight smoothness = " << _in.hilightSmoothness
                << "\n\t- globe color = ( " << _in.globeColor.transpose() << " )"
                << "\n--------------------";)

            _pc.initialize(_in);
        }

        void
        dispose()
        {
            _in.defaults();
            //---
            Material::dispose();
        }
    private:
        math::Vector4f
        getShadingColor(const math::Vector4f&,          // position (in world space)
                        const math::Vector4f&,          // shading normal (in world space)
                        const math::Vector4f&) const;   // incident direction (in world space)
    };
}
}
