// ========================================================================= //
// Copyright 2016-2018 SUPAMONKS_STUDIO                                      //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
//                                                                           //
// ========================================================================= //
#pragma once

#include "Material.hpp"
#include "../brdf/Microfacet.hpp"
#include "../brdf/microfacet/fresnel.hpp"
#include "../brdf/microfacet/PowerCosineDistribution.hpp"
#include "../brdf/microfacet/BeckmannDistribution.hpp"

namespace smks { namespace rndr
{
    class MicrofacetMaterial:
        public Material
    {
        VALID_RTOBJECT
    private:
        enum FresnelType
        {
            NO_FRESNEL          = 0x0000,
            CONSTANT_FRESNEL    = 0x0001,
            DIELECTRIC_FRESNEL  = 0x0010,
            CONDUCTOR_FRESNEL   = 0x0100,
            SCHLICK_FRESNEL     = 0x1000
        };
        enum NDFType
        {
            POWERCOSINE_NDF     = 0x000,
            BECKMANN_NDF        = 0x001
        };

    private:
        FresnelType     _fresnelType;
        NDFType         _ndfType;

        math::Vector4f  _reflectance;   //!< Reflectivity of the metal

        math::Vector4f  _C;             // for constant
        float           _etaInside;     // for dielectric
        float           _etaOutside;
        math::Vector4f  _eta;           // for conductor
        math::Vector4f  _k;

        float           _roughness;     //!< Roughness parameter. The range goes from 0 (specular) to 1 (diffuse).
        //--------------
        float           _f0;            // for Schlick
        float           _rRoughness;    //!< Reciprocal roughness parameter

        CREATE_RTOBJECT(MicrofacetMaterial, Material)
    protected:
        MicrofacetMaterial(unsigned int scnId, const CtorArgs& args):
            Material    (scnId, args),
            _fresnelType(),
            _ndfType    (),
            _reflectance(math::Vector4f::Zero()),
            _C          (math::Vector4f::Zero()),
            _etaInside  (0.0f),
            _etaOutside (0.0f),
            _eta        (math::Vector4f::Zero()),
            _k          (math::Vector4f::Zero()),
            _roughness  (0.0f),
            //----------
            _f0         (-1.0f),
            _rRoughness (-1.0f)
        {
            dispose();
        }
    public:
        ~MicrofacetMaterial()
        {
            dispose();
        }

        void
        shade(const Ray&            ray,
              const LightPath&      lp,
              const Medium&         medium,
              DifferentialGeometry& dg,
              CompositedBRDF&       brdfs) const
        {
            if (!applyNormalMap(dg, -ray.dir))
                return;

            BRDF *brdf = nullptr;
            if (_ndfType == NDFType::BECKMANN_NDF)
            {
                if (_fresnelType == FresnelType::CONSTANT_FRESNEL)
                    brdf = NEW_BRDF(brdfs, Microfacet<FresnelConstant, BeckmannDistribution>) (
                        _reflectance,
                        FresnelConstant(_C),
                        BeckmannDistribution(_roughness, dg.Ns));
                else if (_fresnelType == FresnelType::DIELECTRIC_FRESNEL)
                    brdf = NEW_BRDF(brdfs, Microfacet<FresnelDielectric, BeckmannDistribution>) (
                        _reflectance,
                        FresnelDielectric(_etaOutside, _etaInside),
                        BeckmannDistribution(_roughness, dg.Ns));
                else if (_fresnelType == FresnelType::CONDUCTOR_FRESNEL)
                    brdf = NEW_BRDF(brdfs, Microfacet<FresnelConductor, BeckmannDistribution>) (
                        _reflectance,
                        FresnelConductor(_eta, _k),
                        BeckmannDistribution(_roughness, dg.Ns));
                else if (_fresnelType == FresnelType::SCHLICK_FRESNEL)
                    brdf = NEW_BRDF(brdfs, Microfacet<SchlickDielectric, BeckmannDistribution>) (
                        _reflectance,
                        SchlickDielectric(_f0),
                        BeckmannDistribution(_roughness, dg.Ns));
                else
                    brdf = NEW_BRDF(brdfs, Microfacet<FresnelNone, BeckmannDistribution>) (
                        _reflectance,
                        FresnelNone(),
                        BeckmannDistribution(_roughness, dg.Ns));
            }
            else
            {
                if (_fresnelType == FresnelType::CONSTANT_FRESNEL)
                    brdf = NEW_BRDF(brdfs, Microfacet<FresnelConstant, PowerCosineDistribution>) (
                        _reflectance,
                        FresnelConstant(_C),
                        PowerCosineDistribution(_rRoughness, dg.Ns));
                else if (_fresnelType == FresnelType::DIELECTRIC_FRESNEL)
                    brdf = NEW_BRDF(brdfs, Microfacet<FresnelDielectric, PowerCosineDistribution>) (
                        _reflectance,
                        FresnelDielectric(_etaInside, _etaOutside),
                        PowerCosineDistribution(_rRoughness, dg.Ns));
                else if (_fresnelType == FresnelType::CONDUCTOR_FRESNEL)
                    brdf = NEW_BRDF(brdfs, Microfacet<FresnelConductor, PowerCosineDistribution>) (
                        _reflectance,
                        FresnelConductor(_eta, _k),
                        PowerCosineDistribution(_rRoughness, dg.Ns));
                else if (_fresnelType == FresnelType::SCHLICK_FRESNEL)
                    brdf = NEW_BRDF(brdfs, Microfacet<SchlickDielectric, PowerCosineDistribution>) (
                        _reflectance,
                        SchlickDielectric(_f0),
                        PowerCosineDistribution(_rRoughness, dg.Ns));
                else
                    brdf = NEW_BRDF(brdfs, Microfacet<FresnelNone, PowerCosineDistribution>) (
                        _reflectance,
                        FresnelNone(),
                        PowerCosineDistribution(_rRoughness, dg.Ns));
            }
            assert(brdf);
            brdfs.add(brdf);
        }

        void
        commitFrameState(const parm::Container& parms, const xchg::IdSet& dirties)
        {
            Material::commitFrameState(parms, dirties);
            //---
            for (xchg::IdSet::const_iterator id = dirties.begin(); id != dirties.end(); ++id)
                if      (*id == parm::reflectance().id())   getBuiltIn<math::Vector4f>  (parm::reflectance(),   _reflectance,   &parms);
                else if (*id == parm::intIOR().id())        getBuiltIn<float>           (parm::intIOR(),            _etaInside,     &parms);
                else if (*id == parm::extIOR().id())        getBuiltIn<float>           (parm::extIOR(),            _etaOutside,    &parms);
                else if (*id == parm::IORreal().id())       getBuiltIn<math::Vector4f>  (parm::IORreal(),       _eta,           &parms);
                else if (*id == parm::IORimag().id())       getBuiltIn<math::Vector4f>  (parm::IORimag(),       _k,             &parms);
                else if (*id == parm::fresnelValue().id())
                {
                    getBuiltIn<math::Vector4f>(parm::fresnelValue(), _C, &parms);
                    _f0 = math::add_xyz(_C) * 0.333334f;
                }
                else if (*id == parm::roughness().id())
                {
                    getBuiltIn<float>(parm::roughness(), _roughness, &parms);
                    if (_roughness < 1e-2f)
                        _roughness = 1e-2f;
                    _rRoughness = math::rcp(_roughness);
                }
                else if (*id == parm::fresnelName().id())
                {
                    std::string str;

                    getBuiltIn<std::string>(parm::fresnelName(), str, &parms);
                    _fresnelType = getFresnelType(str);
                }
                else if (*id == parm::NDFName().id())
                {
                    std::string str;

                    getBuiltIn<std::string>(parm::NDFName(), str, &parms);
                    _ndfType = getNDFType(str);
                }

            FLIRT_LOG(LOG(DEBUG)
                << "\n-------------------"
                << "\nmicrofacet material (ID = " << scnId() << ")\n"
                << "\n\t- fresnel type = " << _fresnelType
                << "\n\t- NDF type = " << _ndfType
                << "\n\t- reflectance = ( " << _reflectance.transpose() << " )"
                << "\n\t- C = ( " << _C.transpose() << " )"
                << "\n\t- eta inside = " << _etaInside
                << "\n\t- eta outside = " << _etaOutside
                << "\n\t- eta = ( " << _eta.transpose() << " )"
                << "\n\t- k = ( " << _k.transpose() << " )"
                << "\n\t- roughness = " << _roughness
                << "\n-------------------";)
        }

        void
        dispose()
        {
            std::string str;

            getBuiltIn<math::Vector4f>  (parm::reflectance(),   _reflectance);
            getBuiltIn<float>           (parm::intIOR(),        _etaInside);
            getBuiltIn<float>           (parm::extIOR(),        _etaOutside);
            getBuiltIn<math::Vector4f>  (parm::IORreal(),       _eta);
            getBuiltIn<math::Vector4f>  (parm::IORimag(),       _k);
            getBuiltIn<math::Vector4f>  (parm::fresnelValue(),  _C);
            getBuiltIn<float>           (parm::roughness(),     _roughness);

            getBuiltIn<std::string>(parm::fresnelName(), str);
            _fresnelType = getFresnelType(str);

            getBuiltIn<std::string>(parm::NDFName(), str);
            _ndfType = getNDFType(str);
            //---
            _f0          = -1.0f;
            _rRoughness  = -1.0f;
            //---
            Material::dispose();
        }

    private:
        static
        FresnelType
        getFresnelType(const std::string& str)
        {
            if      (str == "constant")     return CONSTANT_FRESNEL;
            else if (str == "dielectric")   return DIELECTRIC_FRESNEL;
            else if (str == "conductor")    return CONDUCTOR_FRESNEL;
            else if (str == "schlick")      return SCHLICK_FRESNEL;
            else                            return NO_FRESNEL;
        }

        static
        NDFType
        getNDFType(const std::string& str)
        {
            if (str == "beckmann")  return BECKMANN_NDF;
            else                    return POWERCOSINE_NDF;
        }
    };
}
}
