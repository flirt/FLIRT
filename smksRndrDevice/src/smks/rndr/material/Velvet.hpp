// ========================================================================= //
// Modification copyright 2016-2018 SUPAMONKS_STUDIO                         //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
// _________________________________________________________________________ //
// Copyright 2009-2013 Intel Corporation                                     //
//                                                                           //
// Licensed under the Apache License, Version 2.0 (the "License");           //
// you may not use this file except in compliance with the License.          //
// You may obtain a copy of the License at                                   //
//                                                                           //
//     http://www.apache.org/licenses/LICENSE-2.0                            //
//                                                                           //
// Unless required by applicable law or agreed to in writing, software       //
// distributed under the License is distributed on an "AS IS" BASIS,         //
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  //
// See the License for the specific language governing permissions and       //
// limitations under the License.                                            //
//                                                                           //
// ========================================================================= //
#pragma once

#include "Material.hpp"
#include "../brdf/Minnaert.hpp"
#include "../brdf/Velvety.hpp"

namespace smks { namespace rndr
{
    /*! Implements a velvet material. */
    class Velvet:
        public Material
    {
        VALID_RTOBJECT
    protected:
        ColorOrTexture  _reflectance;               /*! Diffuse reflectance of the surface. The range is from 0 (black) to 1 (white). */
        FloatOrTexture  _backScattering;            /*! Amount of back scattering. The range is from 0 (no backscattering) to inf (maximum back scattering). */
        ColorOrTexture  _horizonScatteringColor;    /*! math::Vector4f of horizon scattering. */
        FloatOrTexture  _horizonScatteringFalloff;  /*! Fall-off of horizon scattering. */
        //--------------
        Minnaert*       _minneartBRDF;
        Velvety*        _velvetyBRDF;

        CREATE_RTOBJECT(Velvet, Material)
    protected:
        Velvet(unsigned int scnId, const CtorArgs& args):
            Material                    (scnId, args),
            _reflectance                (math::Vector4f::Zero()),
            _backScattering             (0.0f),
            _horizonScatteringColor     (math::Vector4f::Zero()),
            _horizonScatteringFalloff   (0.0f),
            //---------------------------
            _minneartBRDF               (nullptr),
            _velvetyBRDF                (nullptr)
        {
            dispose();
        }
    public:
        ~Velvet()
        {
            dispose();
        }

        void
        shade(const Ray&            ray,
              const LightPath&      lp,
              const Medium&         medium,
              DifferentialGeometry& dg,
              CompositedBRDF&       brdfs) const
        {
            if (!applyNormalMap(dg, -ray.dir))
                return;

            brdfs.add(
                _minneartBRDF
                ? _minneartBRDF
                : NEW_BRDF(brdfs, Minnaert) (
                    _reflectance.get(dg.st),
                    _backScattering.get(dg.st)));
            brdfs.add(
                _velvetyBRDF
                ? _velvetyBRDF
                : NEW_BRDF(brdfs, Velvety) (
                    _horizonScatteringColor.get(dg.st),
                    _horizonScatteringFalloff.get(dg.st)));
        }

        void
        assignTextureToParameter(unsigned int parmId, Texture::Ptr const& tex)
        {
            Material::assignTextureToParameter(parmId, tex);
            //---
            if (parmId == parm::reflectanceMap().id())
            {
                _reflectance.texture = tex;
                disposeMinneartBRDF();
            }
            else if (parmId == parm::backScatteringMap().id())
            {
                _backScattering.texture = tex;
                disposeMinneartBRDF();
            }
            else if (parmId == parm::horizonScatteringColorMap().id())
            {
                _horizonScatteringColor.texture = tex;
                disposeVelvetyBRDF();
            }
            else if (parmId == parm::horizonScatteringFalloffMap().id())
            {
                _horizonScatteringFalloff.texture = tex;
                disposeVelvetyBRDF();
            }
        }

        void
        commitFrameState(const parm::Container& parms, const xchg::IdSet& dirties)
        {
            Material::commitFrameState(parms, dirties);
            //---
            for (xchg::IdSet::const_iterator id = dirties.begin(); id != dirties.end(); ++id)
                if      (*id == parm::reflectance().id())               getBuiltIn<math::Vector4f>  (parm::reflectance(),               _reflectance.value,                 &parms);
                else if (*id == parm::backScattering().id())            getBuiltIn<float>           (parm::backScattering(),            _backScattering.value,              &parms);
                else if (*id == parm::horizonScatteringColor().id())    getBuiltIn<math::Vector4f>  (parm::horizonScatteringColor(),    _horizonScatteringColor.value,      &parms);
                else if (*id == parm::horizonScatteringFalloff().id())  getBuiltIn<float>           (parm::horizonScatteringFalloff(),  _horizonScatteringFalloff.value,    &parms);

            if (_minneartBRDF == nullptr &&
                !_reflectance.texture &&
                !_backScattering.texture)
                _minneartBRDF = new Minnaert(_reflectance.value, _backScattering.value);
            if (_velvetyBRDF == nullptr &&
                !_horizonScatteringColor.texture &&
                !_horizonScatteringFalloff.texture)
                _velvetyBRDF = new Velvety(_horizonScatteringColor.value, _horizonScatteringFalloff.value);

            FLIRT_LOG(LOG(DEBUG)
                << "\n--------------------"
                << "\nvelvet material (ID = " << scnId() << ")\n"
                << "\n\t- reflectance = ( " << _reflectance << " )"
                << "\n\t- backscattering = ( " << _backScattering << " )"
                << "\n\t- horizon scattering color = ( " << _horizonScatteringColor << " )"
                << "\n\t- horizon scattering falloff = ( " << _horizonScatteringFalloff << " )"
                << "\n--------------------";)
        }

        void
        dispose()
        {
            getBuiltIn<math::Vector4f>(parm::reflectance(), _reflectance.value);
            _reflectance.texture = sys::null;

            getBuiltIn<float>(parm::backScattering(), _backScattering.value);
            _backScattering.texture = sys::null;

            getBuiltIn<math::Vector4f>(parm::horizonScatteringColor(), _horizonScatteringColor.value);
            _horizonScatteringColor.texture = sys::null;

            getBuiltIn<float>(parm::horizonScatteringFalloff(), _horizonScatteringFalloff.value);
            _horizonScatteringFalloff.texture = sys::null;
            //---
            disposeMinneartBRDF();
            disposeVelvetyBRDF();
            //---
            Material::dispose();
        }

    private:
        void
        disposeMinneartBRDF()
        {
            if (_minneartBRDF)
                delete _minneartBRDF;
            _minneartBRDF = nullptr;
        }

        void
        disposeVelvetyBRDF()
        {
            if (_velvetyBRDF)
                delete _velvetyBRDF;
            _velvetyBRDF = nullptr;
        }
    };
}
}
