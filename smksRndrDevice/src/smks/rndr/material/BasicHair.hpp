// ========================================================================= //
// Modification copyright 2016-2018 SUPAMONKS_STUDIO                         //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
// _________________________________________________________________________ //
// Copyright 2009-2013 Intel Corporation                                     //
//                                                                           //
// Licensed under the Apache License, Version 2.0 (the "License");           //
// you may not use this file except in compliance with the License.          //
// You may obtain a copy of the License at                                   //
//                                                                           //
//     http://www.apache.org/licenses/LICENSE-2.0                            //
//                                                                           //
// Unless required by applicable law or agreed to in writing, software       //
// distributed under the License is distributed on an "AS IS" BASIS,         //
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  //
// See the License for the specific language governing permissions and       //
// limitations under the License.                                            //
//                                                                           //
// ========================================================================= //
#pragma once

#include "Material.hpp"
#include "../brdf/AnisotropicBlinnReflection.hpp"
#include "../brdf/AnisotropicBlinnTransmission.hpp"

namespace smks { namespace rndr
{
    class BasicHair:
        public Material
    {
        VALID_RTOBJECT
    protected:
        float           _roughnessX;
        float           _roughnessY;
        float           _reflectivity;  //<! normalized trade-off weight between reflectance and transmittance
        math::Vector4f  _color;
        bool            _transparentShadows;
        //--------------
        math::Vector4f* _reflectance;   // precomputed only if reflectance is constant
        math::Vector4f* _transmittance; // precomputed only if reflectance is constant

        CREATE_RTOBJECT(BasicHair, Material)
    protected:
        BasicHair(unsigned int scnId, const CtorArgs& args):
            Material            (scnId, args),
            _roughnessX         (0.0f),
            _roughnessY         (0.0f),
            _reflectivity       (0.2f),
            _color              (math::Vector4f::Zero()),
            _transparentShadows (false),
            _reflectance        (nullptr),
            _transmittance      (nullptr)
        {
            dispose();
        }
    public:
        ~BasicHair()
        {
            dispose();
        }

        void
        shade(const Ray&            ray,
              const LightPath&      lp,
              const Medium&         medium,
              DifferentialGeometry& dg,
              CompositedBRDF&       brdfs) const
        {
            assert(_reflectance);
            assert(_transmittance);

            brdfs.add(
                NEW_BRDF(brdfs, AnisotropicBlinnReflection) (
                    *_reflectance, dg.Tx, _roughnessX, dg.Ty, _roughnessY, dg.Ns));
            brdfs.add(
                NEW_BRDF(brdfs, AnisotropicBlinnTransmission) (
                    *_transmittance, dg.Tx, _roughnessX, dg.Ty, _roughnessY, dg.Ns));
        }

        __forceinline
        void
        handleOccluded(Ray& ray) const
        {
            if (_transparentShadows)
            {
                assert(_transmittance);
                ray.transparency = ray.transparency.cwiseProduct(*_transmittance);
                if (ray.transparency != math::Vector4f::Zero())
                    ray.geomID = RTC_INVALID_GEOMETRY_ID;
            }
        }


        void
        commitFrameState(const parm::Container& parms, const xchg::IdSet& dirties)
        {
            Material::commitFrameState(parms, dirties);
            //---
            for (xchg::IdSet::const_iterator id = dirties.begin(); id != dirties.end(); ++id)
                if      (*id == parm::roughnessX().id())            getBuiltIn<float>   (parm::roughnessX(),            _roughnessX,            &parms);
                else if (*id == parm::roughnessY().id())            getBuiltIn<float>   (parm::roughnessY(),            _roughnessY,            &parms);
                else if (*id == parm::transparentShadows().id())    getBuiltIn<bool>    (parm::transparentShadows(),    _transparentShadows,    &parms);
                else if (*id == parm::reflectivity().id())
                {
                    getBuiltIn<float>(parm::reflectivity(), _reflectivity, &parms);
                    disposeReflectance();
                    disposeTransmittance();
                }
                else if (*id == parm::diffuseReflectance().id())
                {
                    getBuiltIn<math::Vector4f>(parm::diffuseReflectance(), _color, &parms);
                    disposeReflectance();
                    disposeTransmittance();
                }

            if (_reflectance == nullptr)
                _reflectance = new math::Vector4f(_color * _reflectivity);
            if (_transmittance == nullptr)
                _transmittance = new math::Vector4f(_color * (1.0f - _reflectivity));

            FLIRT_LOG(LOG(DEBUG)
                << "\n--------------------\n"
                << "basic hair material (ID = " << scnId() << ")\n"
                << "\n\t- roughness-x = " << _roughnessX
                << "\n\t- roughness-y = " << _roughnessY
                << "\n\t- reflectivity = " << _reflectivity
                << "\n\t- color = ( " << _color.transpose() << " )"
                << "\n\t- transparent shadows ? " << _transparentShadows
                << "\n--------------------";)
        }

        void
        dispose()
        {
            getBuiltIn<float>           (parm::roughnessX(),            _roughnessX);
            getBuiltIn<float>           (parm::roughnessY(),            _roughnessY);
            getBuiltIn<bool>            (parm::transparentShadows(),    _transparentShadows);
            getBuiltIn<float>           (parm::reflectivity(),          _reflectivity);
            getBuiltIn<math::Vector4f>  (parm::diffuseReflectance(),    _color);
            //---
            disposeReflectance();
            disposeTransmittance();
            //---
            Material::dispose();
        }

    private:
        void
        disposeReflectance()
        {
            if (_reflectance)
                delete _reflectance;
            _reflectance = nullptr;
        }

        void
        disposeTransmittance()
        {
            if (_transmittance)
                delete _transmittance;
            _transmittance = nullptr;
        }
    };
}
}
