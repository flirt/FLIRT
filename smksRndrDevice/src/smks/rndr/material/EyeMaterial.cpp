// ========================================================================= //
// Copyright 2016-2018 SUPAMONKS_STUDIO                                      //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
//                                                                           //
// ========================================================================= //
#include <smks/parm/BuiltIn.hpp>
#include <smks/math/math.hpp>
#include <smks/math/matrixAlgo.hpp>
#include "../brdf/optics.hpp"

#include "EyeMaterial.hpp"

namespace smks { namespace math
{
    // Evaluate ray to cone intersection
    // V: the cone tip
    // A: the cone axis
    // a2: the cone angle as cosine(angle)^2
    // P: the ray origin
    // D: the ray direction
    // out H: the intersection point
    // out N: the intersection normal
    void
    rayToCone(const math::Vector4f& V,
              const math::Vector4f& A,
              float a2,
              const math::Vector4f& P,
              const math::Vector4f& D,
              math::Vector4f& H,
              math::Vector4f& N)
    {
        assert(math::isNormalized3(A));

        // line cone intersection as stated in
        // http://www.geometrictools.com/Documentation/IntersectionLineCone.pdf
        const float Axy = A.x() * A.y();
        const float Axz = A.x() * A.z();
        const float Ayz = A.y() * A.z();
        const math::Vector4f M1     = math::Vector4f (sqr(A.x()) - a2, Axy, Axz, 0.0f);
        const math::Vector4f M2     = math::Vector4f (Axy, sqr(A.y()) - a2, Ayz, 0.0f);
        const math::Vector4f M3     = math::Vector4f (Axz, Ayz, sqr(A.z()) - a2, 0.0f);
        const math::Vector4f delta  = P - V;
        const math::Vector4f DM     = math::Vector4f (dot3(D, M1), dot3(D, M2), dot3(D, M3), 0.0f);
        const float c2 = dot3 (DM, D);
        const float c1 = dot3 (DM, delta);
        const float c0 = dot3 (math::Vector4f (dot3(delta, M1), dot3(delta, M2), dot3(delta, M3), 0.0f), delta);
        const float d = sqr(c1) - c0 * c2;
        const float l = d > 1e-6f
            ? (-c1 - sqrt (d)) * rcp(c2)
            : 0.0f;

        // location of the intersection between the ray and the cone
        H = P + l * D;

        // normal at the intersection between the ray and the cone
        math::Vector4f tipToIntersection = math::normalized3(H-V);
        const math::Vector4f binormal = A.cross3(tipToIntersection);
        N = tipToIntersection.cross3(binormal);
        math::normalize3(N);
    }
}
}

using namespace smks;

/////////////////////////////
// struct EyeMaterial::Inputs
/////////////////////////////
rndr::EyeMaterial::Inputs::Inputs()
{
    defaults();
}

void
rndr::EyeMaterial::Inputs::defaults()
{
    parm::center()                  .defaultValue<math::Vector4f>(center);
    parm::front()                   .defaultValue<math::Vector4f>(front);
    parm::up()                      .defaultValue<math::Vector4f>(up);
    parm::eyeGlobeColor()           .defaultValue<math::Vector4f>(globeColor);
    parm::eyeHighlightColor()       .defaultValue<math::Vector4f>(hilightColor);
    parm::eyeIrisColor()            .defaultValue<math::Vector4f>(irisColor);
    parm::eyeHighlightCoordsD()     .defaultValue<math::Vector2f>(hilightCoordsD);
    parm::eyeIrisDimensions()       .defaultValue<math::Vector2f>(irisDimensions);
    parm::eyePupilDimensions()      .defaultValue<math::Vector2f>(pupilDimensions);
    parm::eyeRadius()               .defaultValue<float>(eyeRadius);
    parm::eyeCorneaIOR()            .defaultValue<float>(corneaIOR);
    parm::eyeHighlightAngSizeD()    .defaultValue<float>(hilightAngSizeD);
    parm::eyeHighlightSmoothness()  .defaultValue<float>(hilightSmoothness);
    parm::eyeIrisAngSizeD()         .defaultValue<float>(irisAngSizeD);
    parm::eyeIrisRotationD()        .defaultValue<float>(irisRotationD);
    parm::eyeIrisDepth()            .defaultValue<float>(irisDepth);
    parm::eyeIrisEdgeBlur()         .defaultValue<float>(irisEdgeBlur);
    parm::eyeIrisEdgeOffset()       .defaultValue<float>(irisEdgeOffset);
    parm::eyeIrisNormalSmoothness() .defaultValue<float>(irisNormalSmoothness);
    parm::eyePupilBlur()            .defaultValue<float>(pupilBlur);
    parm::eyePupilSize()            .defaultValue<float>(pupilSize);
    parm::usedAsPoint()             .defaultValue<bool>(usedAsPoint);
}

/////////////////////////////////////
// struct EyeMaterial::Precomputation
/////////////////////////////////////
rndr::EyeMaterial::Precomputation::Precomputation()
{
    reset();
}
void
rndr::EyeMaterial::Precomputation::reset()
{
    front       = math::Vector4f::UnitZ();
    up          = math::Vector4f::UnitY();
    side        = front.cross3(up);
    hilightDir  = front;
    rCorneaIOR  = 1.0f;
    irisRWidth  = 1.0f;
    irisRHeight = 1.0f;
    irisConeTipFrontOffset  = 0.0f;
    irisConeCAng2       = 1.0f;
    irisConeRLength     = 1.0f;
    pupilWidthHeight    = 1.0f;
    hilightCAngSteps[0] = 1.0f;
    hilightCAngSteps[1] = 1.0f;
    irisAngStepsForEyeColor[0] = 0.0f;
    irisAngStepsForEyeColor[1] = 0.0f;
    irisAngStepsForNormalBevel[0]   = 0.0f;
    irisAngStepsForNormalBevel[1]   = 0.0f;
}
void
rndr::EyeMaterial::Precomputation::initialize(const Inputs& in)
{
    // compute the rotated iris frame's axes in world space
    front   = in.front;

    math::Vector4f up_ = in.up;
    if (in.usedAsPoint )
    {
        front   -= in.center;
        up_     -= in.center;
    }
    math::normalize3(front);
    math::normalize3(up_);
    math::Vector4f side_ = front.cross3(up_);
    up_ = side_.cross3(front);

    const float irisRotation    = math::deg2rad(in.irisRotationD);
    const float irisCRotation   = math::cos(irisRotation);
    const float irisSRotation   = math::sin(irisRotation);

    up      = irisCRotation * up_ - irisSRotation * side_;
    side    = irisSRotation * up_ + irisCRotation * side_;
    //---
    // compute the hilightDir direction in world space
    const float irisAngSize     = math::deg2rad(in.irisAngSizeD);
    const float hilightAngSize  = math::deg2rad(in.hilightAngSizeD);
    const float hlightPolar     = irisAngSize + math::deg2rad(in.hilightCoordsD.x());
    const float hlightAzimuth   = math::deg2rad(in.hilightCoordsD.y());
    const float hilightCPolar   = math::cos(hlightPolar);
    const float hilightSPolar   = math::sin(hlightPolar);
    const float hilightCAzimuth = math::cos(hlightAzimuth);
    const float hilightSAzimuth = math::sin(hlightAzimuth);

    hilightDir  = hilightCPolar * front
        + hilightSPolar * hilightCAzimuth * up
        + hilightSPolar * hilightSAzimuth * side;
    math::normalize3(hilightDir);
    //---
    rCorneaIOR  = math::rcp(in.corneaIOR);
    assert(in.irisDimensions.x() > 0.0f);
    assert(in.irisDimensions.y() > 0.0f);
    irisRWidth  = math::rcp(in.irisDimensions.x());
    irisRHeight = math::rcp(in.irisDimensions.y());
    //---
    // iris cone
    assert(in.irisDepth > 1e-6f);
    const float irisConeTAng = math::sin(irisAngSize) * math::rcp(in.irisDepth);
    irisConeCAng2           = 1.0f / (1.0f + math::sqr(irisConeTAng));
    irisConeTipFrontOffset  = in.eyeRadius * (math::cos(irisAngSize) - in.irisDepth);
    assert(in.eyeRadius > 1e-6f);
    assert(in.irisDepth > 1e-6f);
    irisConeRLength         = math::sqrt(irisConeCAng2) * math::rcp(in.eyeRadius * in.irisDepth);
    //---
    pupilWidthHeight    = in.pupilDimensions.x() * in.pupilDimensions.y();
    //---
    hilightCAngSteps[0] = math::cos(hilightAngSize);
    hilightCAngSteps[1] = math::cos(hilightAngSize * (1.0f - in.hilightSmoothness));
    //---
    irisAngStepsForEyeColor[0]  = in.irisEdgeOffset + irisAngSize * (1.0f-in.irisEdgeBlur);
    irisAngStepsForEyeColor[1]  = in.irisEdgeOffset + irisAngSize;
    //---
    irisAngStepsForNormalBevel[0]   = irisAngSize * (1.0f-in.irisNormalSmoothness);
    irisAngStepsForNormalBevel[1]   = irisAngSize * (1.0f+in.irisNormalSmoothness);
}

/////////////////////////////////////
// struct EyeMaterial::Precomputation
/////////////////////////////////////
math::Vector4f
rndr::EyeMaterial::getShadingColor(const math::Vector4f&    P,          // position (in world space)
                                    const math::Vector4f&   Ns,         // shading normal (in world space)
                                    const math::Vector4f&   I) const    // incident direction (in world space)
{
    assert(math::isNormalized3(Ns));
    assert(math::isNormalized3(I));

    // get rotated iris frame in world space
    assert(math::isNormalized3(_pc.front));
    assert(math::isNormalized3(_pc.up));
    assert(math::isNormalized3(_pc.side));

    // offset center
    math::Vector4f center   (_in.center - (_in.eyeRadius - math::len3(_in.center - P)) * _pc.front);
    math::Vector4f dP       (math::normalized3(P - center));

    // express hilightDir direction in world space
    assert(math::isNormalized3(_pc.hilightDir));
    const float cHilight = math::smoothstep(
            _pc.hilightCAngSteps[0],
            _pc.hilightCAngSteps[1],
            math::dot3(_pc.hilightDir, dP));
    //return math::Vector4f(cHilight, cHilight, cHilight, 1.0f);

    math::Vector4f hilightColor(math::Vector4f::Zero());
    if (_in.hilightAngSizeD > 1e-1f)
        hilightColor = math::smoothstep(
            _pc.hilightCAngSteps[0],
            _pc.hilightCAngSteps[1],
            math::dot3(_pc.hilightDir, dP))
            * _in.hilightColor;

    // express dP in iris space in order to account for the iris deformation along its axes
    float dpIrisFront = math::dot3(dP, _pc.front);
    float dpIrisUp    = math::dot3(dP, _pc.up);
    float dpIrisSide  = math::dot3(dP, _pc.side);
    dpIrisSide  *= _pc.irisRWidth;
    dpIrisUp    *= _pc.irisRHeight;
    dP = dpIrisFront * _pc.front + dpIrisUp * _pc.up + dpIrisSide * _pc.side;
    math::normalize3(dP);

    const float alpha = math::acos(math::dot3(dP, _pc.front));

    // refract direction across the cornea
    Sample<math::Vector4f> refracted = refract(I, Ns, _pc.rCorneaIOR);
    if (refracted.pdf < 1e-6f)
        return math::Vector4f::Zero();
    assert(math::isNormalized3(refracted.value));

    math::Vector4f irisConeP, irisConeN;
    math::Vector4f irisConeTip = center + _pc.irisConeTipFrontOffset * _pc.front;
    math::rayToCone(irisConeTip, _pc.front, _pc.irisConeCAng2,
        P, refracted.value,
        irisConeP, irisConeN);
    if (math::dot3(irisConeN, Ns) < 0.0f)
        irisConeN  -irisConeN;
    assert(math::isNormalized3(irisConeN));

    const float     distToIrisEdge = math::max(0.0f, 1.0f - math::len3(irisConeP - irisConeTip) * _pc.irisConeRLength);
    math::Vector4f  dIrisConeP = irisConeP - center;
    const float     irisAng = math::atan2(-math::dot3(dIrisConeP, _pc.side), -math::dot3(dIrisConeP, _pc.up));
    const float     pupilEllipse = _pc.pupilWidthHeight
        * math::rsqrt(math::sqr(_in.pupilDimensions.x() * math::cos(irisAng)) + math::sqr(_in.pupilDimensions.y() * math::sin(irisAng)));
    const float     pupilDissolve = math::smoothstep(
        1.0f - _in.pupilBlur,
        1.0f + _in.pupilBlur,
        distToIrisEdge * math::rcp(1.0f - _in.pupilSize * pupilEllipse));

    const float globeBleed = math::smoothstep(_pc.irisAngStepsForEyeColor[0], _pc.irisAngStepsForEyeColor[1], alpha);

    // compute the bevelled normal
    const float bevel = math::smoothstep(_pc.irisAngStepsForNormalBevel[0], _pc.irisAngStepsForNormalBevel[1], alpha);
    const math::Vector4f normal = math::normalized3(math::mix(irisConeN, Ns, bevel));

    // determine final color
    const float illum = 0.5f * (math::max(0.0f, math::dot3(_pc.hilightDir, normal)) + math::max(0.0f, math::dot3(I, normal)));

    return (illum * globeBleed) * _in.globeColor
        +  (illum * (1.0f - globeBleed) * (1.0f - pupilDissolve)) * _in.irisColor
        + hilightColor;
}

