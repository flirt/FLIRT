// ========================================================================= //
// Modification copyright 2016-2018 SUPAMONKS_STUDIO                         //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
// _________________________________________________________________________ //
// Copyright 2009-2013 Intel Corporation                                     //
//                                                                           //
// Licensed under the Apache License, Version 2.0 (the "License");           //
// you may not use this file except in compliance with the License.          //
// You may obtain a copy of the License at                                   //
//                                                                           //
//     http://www.apache.org/licenses/LICENSE-2.0                            //
//                                                                           //
// Unless required by applicable law or agreed to in writing, software       //
// distributed under the License is distributed on an "AS IS" BASIS,         //
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  //
// See the License for the specific language governing permissions and       //
// limitations under the License.                                            //
//                                                                           //
// ========================================================================= //
#pragma once

#include "Material.hpp"
#include "../brdf/Microfacet.hpp"
#include "../brdf/microfacet/fresnel.hpp"
#include "../brdf/microfacet/PowerCosineDistribution.hpp"
#include "../brdf/DielectricLayer.hpp"
#include "../brdf/DielectricReflection.hpp"
#include "../brdf/Lambertian.hpp"

namespace smks { namespace rndr
{
    /*! Implements a car paint BRDF. Models a dielectric layer over a
    *  diffuse ground layer. Additionally the ground layer may contain
    *  metallic glitter. */
    class MetallicPaint:
        public Material
    {
        VALID_RTOBJECT
    protected:
        typedef Microfacet<FresnelConductor,PowerCosineDistribution> MicrofacetGlitter;
    protected:
        ColorOrTexture                  _layerColor;
        float                           _layerEta;
        ColorOrTexture                  _glitterColor;
        FloatOrTexture                  _glitterSpread;
        //------------------------------
        const FresnelConductor          _fresnelAluminium;
        DielectricReflection*           _reflectionBRDF;    //!< Precomputed dielectric reflection component.
        DielectricLayer<Lambertian>*    _paintBRDF;         //!< Precomputed diffuse layer covered by dielectricum (only if shadeColor is constant).

        CREATE_RTOBJECT(MetallicPaint, Material)
    protected:
        MetallicPaint(unsigned int scnId, const CtorArgs& args):
            Material            (scnId, args),
            _layerColor         (),
            _layerEta           (0.0f),
            _glitterColor       (),
            _glitterSpread      (),
            //------------------
            _fresnelAluminium   (math::Vector4f::Constant(0.62f), math::Vector4f::Constant(4.8f)),
            _reflectionBRDF     (nullptr),
            _paintBRDF          (nullptr)
        {
            dispose();
        }
    public:
        ~MetallicPaint()
        {
            dispose();
        }

        void
        shade(const Ray&            ray,
              const LightPath&      lp,
              const Medium&         medium,
              DifferentialGeometry& dg,
              CompositedBRDF&       brdfs) const
        {
            if (!applyNormalMap(dg, -ray.dir))
                return;

            assert(_reflectionBRDF);
            brdfs.add(_reflectionBRDF);
            brdfs.add(
                _paintBRDF
                ? _paintBRDF
                : NEW_BRDF(brdfs, DielectricLayer<Lambertian>) (
                    math::Vector4f::Ones(),
                    1.0f,
                    _layerEta,
                    Lambertian(_layerColor.get(dg.st))));

            const math::Vector4f&   glitterColor    = _glitterColor.get(dg.st);
            const float             glitterSpread   = _glitterSpread.get(dg.st);

            if (glitterSpread > 0.0f &&
                math::neq3(glitterColor, math::Vector4f::Zero()))
                /*! Use the fresnel relfectance of Aluminium for the flakes. Modulate with glitterColor. */
                brdfs.add(NEW_BRDF(brdfs, DielectricLayer<MicrofacetGlitter>) (
                    math::Vector4f::Ones(),
                    1.0f,
                    _layerEta,
                    MicrofacetGlitter(
                        glitterColor,
                        _fresnelAluminium,
                        PowerCosineDistribution(math::rcp(glitterSpread), dg.Ns))));
        }

        void
        assignTextureToParameter(unsigned int parmId, Texture::Ptr const& tex)
        {
            Material::assignTextureToParameter(parmId, tex);
            //---
            if (parmId == parm::layerColorMap().id())
            {
                _layerColor.texture = tex;
                disposePaintBRDF();
            }
            else if (parmId == parm::glitterColorMap().id())
                _glitterColor.texture = tex;
            else if (parmId == parm::glitterSpreadMap().id())
                _glitterSpread.texture = tex;
        }

        void
        commitFrameState(const parm::Container& parms, const xchg::IdSet& dirties)
        {
            Material::commitFrameState(parms, dirties);
            //---
            for (xchg::IdSet::const_iterator id = dirties.begin(); id != dirties.end(); ++id)
                if (*id == parm::layerColor().id())
                {
                    getBuiltIn<math::Vector4f>(parm::layerColor(), _layerColor.value, &parms);
                    disposePaintBRDF();
                }
                else if (*id == parm::layerIOR().id())
                {
                    getBuiltIn<float>(parm::layerIOR(), _layerEta, &parms);
                    disposeReflectionBRDF();
                    disposePaintBRDF();
                }
                else if (*id == parm::glitterColor().id())
                    getBuiltIn<math::Vector4f>(parm::glitterColor(), _glitterColor.value, &parms);
                else if (*id == parm::glitterSpread().id())
                    getBuiltIn<float>(parm::glitterSpread(), _glitterSpread.value, &parms);

            if (_reflectionBRDF == nullptr)
                _reflectionBRDF = new DielectricReflection(1.0f, _layerEta);
            if (_paintBRDF == nullptr && !_layerColor.texture)
                _paintBRDF = new DielectricLayer<Lambertian>(math::Vector4f::Ones(), 1.0f, _layerEta, Lambertian(_layerColor.value));

            FLIRT_LOG(LOG(DEBUG)
                << "\n--------------------"
                << "\nmetallic paint material (ID = " << scnId() << ")\n"
                << "\n\t- layer color = ( " << _layerColor << " )"
                << "\n\t- layer eta = " << _layerEta
                << "\n\t- glitter color = ( " << _glitterColor << " )"
                << "\n\t- glitter spread = ( " << _glitterSpread << " )"
                << "\n--------------------";)
        }

        void
        dispose()
        {
            getBuiltIn<math::Vector4f>(parm::layerColor(), _layerColor.value);
            _layerColor.texture = sys::null;

            getBuiltIn<float>(parm::layerIOR(), _layerEta);

            getBuiltIn<math::Vector4f>(parm::glitterColor(), _glitterColor.value);
            _glitterColor.texture = sys::null;

            getBuiltIn<float>(parm::glitterSpread(), _glitterSpread.value);
            _glitterSpread.texture = sys::null;
            //---
            disposeReflectionBRDF();
            disposePaintBRDF();
            //---
            Material::dispose();
        }

    private:
        void
        disposeReflectionBRDF()
        {
            if (_reflectionBRDF)
                delete _reflectionBRDF;
            _reflectionBRDF = nullptr;
        }

        void
        disposePaintBRDF()
        {
            if (_paintBRDF)
                delete _paintBRDF;
            _paintBRDF = nullptr;
        }
    };
}
}
