// ========================================================================= //
// Modification copyright 2016-2018 SUPAMONKS_STUDIO                         //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
// _________________________________________________________________________ //
// Copyright 2009-2013 Intel Corporation                                     //
//                                                                           //
// Licensed under the Apache License, Version 2.0 (the "License");           //
// you may not use this file except in compliance with the License.          //
// You may obtain a copy of the License at                                   //
//                                                                           //
//     http://www.apache.org/licenses/LICENSE-2.0                            //
//                                                                           //
// Unless required by applicable law or agreed to in writing, software       //
// distributed under the License is distributed on an "AS IS" BASIS,         //
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  //
// See the License for the specific language governing permissions and       //
// limitations under the License.                                            //
//                                                                           //
// ========================================================================= //
#pragma once

#include <smks/math/types.hpp>
#include <smks/math/math.hpp>
#include <smks/img/AbstractImage.hpp>

#include "../ObjectBase.hpp"

namespace smks
{
    namespace xchg
    {
        class Image;
        class TileImages;
    }

    namespace rndr
    {
        class Texture:
            public ObjectBase
        {
            VALID_RTOBJECT
        public:
            typedef sys::Ref<Texture> Ptr;
        private:
            typedef std::weak_ptr<xchg::Image>      ImageWPtr;
            typedef std::weak_ptr<xchg::TileImages> TileImagesWPtr;
        private:
            math::Vector4f* _scale;
            math::Vector2f* _scaleUV;
            math::Vector2f* _offsetUV;

            ImageWPtr       _image;
            TileImagesWPtr  _tileImages;

        protected:
            Texture(unsigned int scnId, const CtorArgs&);
        public:
            virtual
            ~Texture();

            /*! Returns the color for a surface point p. \param p is the
            *  location to query the color for. The range is 0 to 1. */
            __forceinline
            math::Vector4f
            get(const math::Vector2f& UV) const
            {
                math::Vector2f              localUV;
                const img::AbstractImage*   image = getImage(UV, localUV);

                if (_scaleUV)
                    localUV = localUV.cwiseProduct(*_scaleUV);
                if (_offsetUV)
                    localUV += *_offsetUV;

                math::Vector4f ret = sample(image, localUV);

                if (_scale)
                    ret = ret.cwiseProduct(*_scale);

                return ret;
            }

        protected:
            // warning: must also map input query to returned image's frame
            const img::AbstractImage*
            getImage(const math::Vector2f& UV, math::Vector2f& localUV) const;

            virtual
            math::Vector4f
            sample(const img::AbstractImage*, const math::Vector2f&) const = 0;

        public:
            virtual
            void
            dispose();

            virtual
            void
            commitFrameState(const parm::Container&, const xchg::IdSet&);

            virtual inline
            void
            beginSubframeCommits(size_t)
            { }

            virtual inline
            void
            commitSubframeState(size_t, const parm::Container&)
            { }

            virtual inline
            void
            endSubframeCommits()
            { }

            virtual inline
            bool
            perSubframeParameter(unsigned int) const
            {
                return false;
            }
        };
    }
}
