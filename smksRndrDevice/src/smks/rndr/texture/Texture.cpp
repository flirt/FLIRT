// ========================================================================= //
// Modification copyright 2016-2018 SUPAMONKS_STUDIO                         //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
// _________________________________________________________________________ //
// Copyright 2009-2013 Intel Corporation                                     //
//                                                                           //
// Licensed under the Apache License, Version 2.0 (the "License");           //
// you may not use this file except in compliance with the License.          //
// You may obtain a copy of the License at                                   //
//                                                                           //
//     http://www.apache.org/licenses/LICENSE-2.0                            //
//                                                                           //
// Unless required by applicable law or agreed to in writing, software       //
// distributed under the License is distributed on an "AS IS" BASIS,         //
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  //
// See the License for the specific language governing permissions and       //
// limitations under the License.                                            //
//                                                                           //
// ========================================================================= //
#include <smks/sys/Log.hpp>
#include <smks/parm/BuiltIns.hpp>
#include <smks/xchg/Image.hpp>
#include <smks/xchg/TileImages.hpp>
#include <smks/ClientBase.hpp>

#include "Texture.hpp"

using namespace smks;

rndr::Texture::Texture(unsigned int scnId, const CtorArgs& args):
    ObjectBase  (scnId, args),
    _scale      (nullptr),
    _scaleUV    (nullptr),
    _offsetUV   (nullptr),
    _image      (),
    _tileImages ()
{
    dispose();
}

// virtual
rndr::Texture::~Texture()
{
    dispose();
}

// virtual
void
rndr::Texture::dispose()
{
    _image      .reset();
    _tileImages .reset();

    if (_offsetUV)  delete _offsetUV;
    if (_scaleUV)   delete _scaleUV;
    if (_scale)     delete _scale;
    _offsetUV   = nullptr;
    _scaleUV    = nullptr;
    _scale      = nullptr;
}

// virtual
void
rndr::Texture::commitFrameState(const parm::Container& parms, const xchg::IdSet& dirties)
{
    logParameters(scnId(), parms, "builtIns", dirties);
    logParameters(scnId(), userParameters(), "userParms", "USER");
    //---
    xchg::ObjectPtr obj;

    for (xchg::IdSet::const_iterator id = dirties.begin(); id != dirties.end(); ++id)
        if (*id == parm::image().id())
        {
            getBuiltIn<xchg::ObjectPtr>(parm::image(), obj, &parms);
            _image = obj.shared_ptr<xchg::Image>();
        }
        else if (*id == parm::tileImages().id())
        {
            getBuiltIn<xchg::ObjectPtr>(parm::tileImages(), obj, &parms);
            _tileImages = obj.shared_ptr<xchg::TileImages>();
        }
        else if (*id == parm::transformUV().id())
        {
            math::Vector4f transformUV;

            if (_offsetUV)  delete _offsetUV;
            if (_scaleUV)   delete _scaleUV;
            _offsetUV   = nullptr;
            _scaleUV    = nullptr;

            getBuiltIn<math::Vector4f>(parm::transformUV(), transformUV, &parms);

            if (math::abs(transformUV.x() - 1.0f) > 1e-6f &&
                math::abs(transformUV.y() - 1.0f) > 1e-6f)
                _scaleUV = new math::Vector2f(transformUV.x(), transformUV.y());

            if (math::abs(transformUV.z()) > 1e-6f &&
                math::abs(transformUV.w()) > 1e-6f)
                _offsetUV = new math::Vector2f(transformUV.z(), transformUV.w());
        }
        else if (*id == parm::scale().id())
        {
            math::Vector4f scale;

            if (_scale)
                delete _scale;
            _scale = nullptr;

            getBuiltIn<math::Vector4f>(parm::scale(), scale, &parms);

            if (math::abs(scale.x() - 1.0f) < 1e-6f &&
                math::abs(scale.y() - 1.0f) < 1e-6f &&
                math::abs(scale.z() - 1.0f) < 1e-6f)
                continue;

            _scale = new math::Vector4f(scale);
        }

#if defined(FLIRT_LOG_ENABLED)
    {
        std::stringstream sstr;
        sstr << "\n----------\ntexture ID = " << scnId();
        if (_scaleUV)
            sstr << "\n\t- scale UV = " << _scaleUV->transpose();
        if (_offsetUV)
            sstr << "\n\t- offset UV = " << _offsetUV->transpose();
        if (_scale)
            sstr << "\n\t- scale = " << _scale->transpose();
        sstr << "\n----------";
        LOG(DEBUG) << sstr.str();
    }
#endif // defined(FLIRT_LOG_ENABLED)
}

const img::AbstractImage*
rndr::Texture::getImage(const math::Vector2f&   UV,
                         math::Vector2f&        localUV) const
{
    xchg::TileImages::Ptr const& tileImages = _tileImages.lock();
    if (tileImages)
        return tileImages->getImage(UV, localUV);

    xchg::Image::Ptr const& image = _image.lock();
    if (image)
    {
        localUV = UV;
        return image->getImage();
    }
    return nullptr;
}
