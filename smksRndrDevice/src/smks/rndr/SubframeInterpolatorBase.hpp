// ========================================================================= //
// Copyright 2016-2018 SUPAMONKS_STUDIO                                      //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
//                                                                           //
// ========================================================================= //
#pragma once

#include <cassert>
#include <smks/sys/Log.hpp>
#include <smks/math/math.hpp>

namespace smks { namespace rndr
{
    class SubframeInterpolatorBase
    {
    protected:
        enum { TAB_SIZE=8 };
    private:
        size_t  _numSubframes, _tabSize;
        float   _subframeToTabEntry;    //<! tabsize / (#subframes-1)

    public:
        SubframeInterpolatorBase():
            _numSubframes(0), _tabSize(0)
        { }
        virtual
        ~SubframeInterpolatorBase()
        { }

        inline
        void
        dispose()
        {
            disposeSubframeData();
            disposeFrameData();
            disposePrecomputedTables();
        }
    protected:
        virtual void disposeSubframeData() = 0;
        virtual void disposeFrameData() = 0;
        virtual void disposePrecomputedTables() = 0;

    public:
        void
        setNumSubframes(size_t numSubframes)
        {
            if (numSubframes == 0 ||
                numSubframes == this->numSubframes())
                return;

            _numSubframes       = numSubframes;
            _tabSize            = 1;
            _subframeToTabEntry = 0.0f;
            if (_numSubframes > 1)
            {
                _tabSize            = math::max<size_t>(TAB_SIZE, _numSubframes);
                _subframeToTabEntry = static_cast<float>(_tabSize) / static_cast<float>(_numSubframes-1);
            }

            resizeSubframeData(numSubframes);
            resizePrecomputedTables(_tabSize);
            invalidateAllTabEntries();
        }
    protected:
        virtual void resizeSubframeData(size_t) = 0;
        virtual void resizePrecomputedTables(size_t) = 0;

    public:
        void
        precomputeTables()
        {
#if defined(FLIRT_LOG_ENABLED)
                {
                    std::stringstream sstr;
                    printSubframeData(sstr);
                    LOG(DEBUG) << sstr.str();
                }
#endif // defined(FLIRT_LOG_ENABLED)
            if (_tabSize == 0)
                return;

            // compute only invalid table entries
            if (_tabSize == 1)
            {
                assert(_numSubframes == 1);
                if (!isTabEntryValid(0))
                    precomputeTabEntry(0, 0);
            }
            else
            {
                const float tnStep  = static_cast<float>(_numSubframes) / static_cast<float>(_tabSize-1);
                float       tn      = 0.0f;
                for (size_t i = 0; i < _tabSize; ++i)
                {
                    const int n0 = math::min<int>(_numSubframes-1, static_cast<int>(math::floor(tn)));
                    const int n1 = math::min<int>(_numSubframes-1, n0 + 1);

                    if (n0 == n1)
                        precomputeTabEntry(i, n0);
                    else
                        precomputeTabEntry(i, n0, n1, tn - static_cast<float>(n0));
                    tn += tnStep;
                }
            }
#if defined(FLIRT_LOG_ENABLED)
                {
                    std::stringstream sstr;
                    printTabEntries(sstr);
                    LOG(DEBUG) << sstr.str();
                }
#endif // defined(FLIRT_LOG_ENABLED)
        }
    protected:
        virtual void precomputeTabEntry(size_t tabEntry, size_t subframe) = 0;
        virtual void precomputeTabEntry(size_t tabEntry, size_t subframe0, size_t subframe1, float) = 0;
    protected:
        virtual std::ostream& printSubframeData(std::ostream& out) const { return out; }
        virtual std::ostream& printTabEntries(std::ostream& out) const { return out; }
    public:
        __forceinline
        size_t
        numSubframes() const
        {
            return _numSubframes;
        }
    protected:
        __forceinline
        size_t
        tabSize() const
        {
            return _tabSize;
        }

        __forceinline
        void
        getSubframes(float      time,
                     size_t&    curSubframe,
                     size_t&    nxtSubframe,
                     float&     subframeBlend) const
        {
            assert(_numSubframes > 1);  // if #subframes == 1 then calling this function is a waste of time
            const float t = time * _numSubframes;
            const float n = math::floor(t);
            curSubframe     = math::clamp<size_t>   (_numSubframes-1, static_cast<size_t>(n));
            nxtSubframe     = math::min<size_t>     (_numSubframes-1, curSubframe+1);
            subframeBlend   = t - n;
        }

        __forceinline
        size_t
        getTabEntryIndex(float time) const
        {
            assert(_tabSize > 0);
            return _tabSize == 1
                ? 0
                : static_cast<size_t>(
                    math::clamp<int>(
                        static_cast<int>(math::floor(time * _tabSize)),
                        0, _tabSize-1));
        }
        inline
        void
        getInvalidatedTabEntryRange(size_t subframe, int& iStart, int& iEnd) const
        {
            assert(_tabSize > 0);
            iStart  = math::clamp<int>(
                static_cast<int>((subframe-1) * _subframeToTabEntry),
                0, static_cast<int>(_tabSize-1));
            iEnd    = math::clamp<int>(
                static_cast<int>((subframe+1) * _subframeToTabEntry),
                0, static_cast<int>(_tabSize-1));
        }

        inline
        void
        invalidateAllTabEntries()
        {
            for (size_t i = 0; i < _tabSize; ++i)
                invalidateTabEntry(i);
        }
        inline
        bool
        areAllTabEntriesValid() const
        {
            for (size_t i = 0; i < tabSize(); ++i)
                if (!isTabEntryValid(i))
                    return false;
            return true;
        }

        virtual bool isTabEntryValid(size_t) const = 0;
        virtual void invalidateTabEntry(size_t) = 0;
    };
}
}
