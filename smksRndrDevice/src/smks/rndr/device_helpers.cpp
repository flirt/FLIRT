// ========================================================================= //
// Copyright 2016-2018 SUPAMONKS_STUDIO                                      //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
//                                                                           //
// ========================================================================= //
#include <smks/sys/Log.hpp>
#include <smks/sys/initialize_img.hpp>

#include "smks/rndr/device_helpers.hpp"
#include "Device.hpp"

#if defined(FLIRT_LOG_ENABLED)
//INITIALIZE_NULL_EXTERN_EASYLOGGINGPP;
INITIALIZE_NULL_EASYLOGGINGPP;
#endif // defined(FLIRT_LOG_ENABLED)

namespace smks { namespace rndr
{
    static
    AbstractDevice*
    createDevice(const char* jsonCfg,
                 std::shared_ptr<ClientBase> const&);

    static
    void
    destroyDevice(AbstractDevice*);
}
}

using namespace smks;

rndr::AbstractDevice*
create(const char*                          jsonCfg,
       std::shared_ptr<ClientBase> const&   client)
{
    return rndr::createDevice(jsonCfg, client);
}

void
destroy(rndr::AbstractDevice* device)
{
    rndr::destroyDevice(device);
}

// static
rndr::AbstractDevice*
rndr::createDevice(const char*                        jsonCfg,
                   std::shared_ptr<ClientBase> const& client)
{
#if defined(FLIRT_LOG_ENABLED)
    el::Helpers::setStorage(smks::sys::sharedLoggingRepository());
#endif //defined(FLIRT_LOG_ENABLED)

    sys::initializeSmksImg();

    return Device::create(jsonCfg, client);
}

// static
void
rndr::destroyDevice(rndr::AbstractDevice* device)
{
    if (dynamic_cast<Device*>(device))
        Device::destroy(
            dynamic_cast<Device*>(device));

    sys::finalizeSmksImg();

#if defined(FLIRT_LOG_ENABLED)
    el::Helpers::setStorage(nullptr);
#endif //defined(FLIRT_LOG_ENABLED)
}
