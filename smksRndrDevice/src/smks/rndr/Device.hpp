// ========================================================================= //
// Modification copyright 2016-2018 SUPAMONKS_STUDIO                         //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
// _________________________________________________________________________ //
// Copyright 2009-2013 Intel Corporation                                     //
//                                                                           //
// Licensed under the Apache License, Version 2.0 (the "License");           //
// you may not use this file except in compliance with the License.          //
// You may obtain a copy of the License at                                   //
//                                                                           //
//     http://www.apache.org/licenses/LICENSE-2.0                            //
//                                                                           //
// Unless required by applicable law or agreed to in writing, software       //
// distributed under the License is distributed on an "AS IS" BASIS,         //
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  //
// See the License for the specific language governing permissions and       //
// limitations under the License.                                            //
//                                                                           //
// ========================================================================= //
#pragma once

#include <memory>
#include <boost/unordered_map.hpp>
#include <boost/unordered_set.hpp>

#include <embree2/rtcore.h>

#include <smks/rndr/AbstractDevice.hpp>
#include <smks/xchg/Collection.hpp>
#include <smks/sys/sync/Mutex.hpp>

#include "api/Primitive.hpp"

namespace smks
{
    class ClientBase;
    namespace rndr
    {
        class Device:
            public AbstractDevice
        {
        public:
            ////////////////
            // struct Config
            ////////////////
            struct Config
            {
                unsigned int numThreads;
                std::string  rtCoreConfig;
            public:
                Config();
                explicit Config(const char*);
            private:
                void setDefaults();
            };
            ////////////////
        private:
            typedef std::shared_ptr<ClientBase>           ClientPtr;
            typedef std::weak_ptr<ClientBase>             ClientWPtr;
            typedef boost::unordered_set<Handle>          Handles;
            typedef boost::unordered_map<Handle, Handles> HandleToHandles;

        private:
            Config                      _cfg;
            ClientWPtr                  _client;

            RTCDevice                   _rtcDevice;
            xchg::Collection<Primitive> _prims;
            HandleToHandles             _primToScenes;
            HandleToHandles             _sceneToPrims;

            sys::sync::MutexSys         _mutex;

        public:
            static inline
            Device*
            create(const char*      jsonCfg,
                   ClientPtr const& client)
            {
                Device* ptr = new Device();
                ptr->build(jsonCfg, client);
                return ptr;
            }

            static inline
            void
            destroy(Device* ptr)
            {
                if (ptr)
                {
                    ptr->dispose();
                    delete ptr;
                }
            }

        protected:
            Device();
        public:
            ~Device();
        protected:
            //////////////////////////
            // initialization
            //////////////////////////
            void
            build(const char* jsonCfg,
                  std::shared_ptr<ClientBase> const&);

            void
            dispose();
        public:
            //////////////////////////
            // handle reference counts
            //////////////////////////
            void
            incRef(Handle);

            void
            decRef(Handle);

            unsigned int
            getScnId(Handle) const;

            //////////////////
            // handle creators
            //////////////////
            SceneHandle
            newScene(const char*    type,
                     unsigned int   scnId);

            CameraHandle
            newCamera(const char*   type,
                      unsigned int  scnId);

            MaterialHandle
            newMaterial(const char*     type,
                        unsigned int    scnId);

            SubSurfaceHandle
            newSubSurface(const char*   type,
                          unsigned int  scnId);

            TextureHandle
            newTexture(const char*  type,
                       unsigned int scnId);

            ShapeHandle
            newShape(const char*    type,
                     unsigned int   scnId);

            LightHandle
            newLight(const char*    type,
                     unsigned int   scnId);

            SelectionHandle
            newSelection(const char*    type,
                         unsigned int   scnId);

            DenoiserHandle
            newDenoiser(const char*     type,
                        unsigned int    scnId);

            ToneMapperHandle
            newToneMapper(const char*   type,
                          unsigned int  scnId);

            RenderPassHandle
            newRenderPass(const char*   type,
                          unsigned int  scnId);

            RendererHandle
            newRenderer(const char*     type,
                        unsigned int    scnId);

            FrameBufferHandle
            newFrameBuffer(const char*  type,
                           unsigned int scnId);

            IntegratorHandle
            newIntegrator(const char*   type,
                          unsigned int  scnId);

            SamplerFactoryHandle
            newSamplerFactory(const char*   type,
                              unsigned int  scnId);

            PixelFilterHandle
            newPixelFilter(const char*  type,
                           unsigned int scnId);

            ///////////////////////////////
            // general link/unlink handlers
            ///////////////////////////////
            bool
            link(unsigned int parmId, Handle target, Handle source);

            bool
            unlink(unsigned int parmId, Handle target, Handle source);

            /////////////////////////////////
            // scene object parameter setting
            /////////////////////////////////

            unsigned int setBool        (Handle, const char*, const bool&);
            unsigned int setChar        (Handle, const char*, const char&);
            unsigned int setUInt        (Handle, const char*, const unsigned int&);
            unsigned int setUInt64      (Handle, const char*, const size_t&);
            unsigned int setInt1        (Handle, const char*, const int&);
            unsigned int setInt2        (Handle, const char*, const math::Vector2i&);
            unsigned int setInt3        (Handle, const char*, const math::Vector3i&);
            unsigned int setInt4        (Handle, const char*, const math::Vector4i&);
            unsigned int setFloat1      (Handle, const char*, const float&);
            unsigned int setFloat2      (Handle, const char*, const math::Vector2f&);
            unsigned int setFloat4      (Handle, const char*, const math::Vector4f&);
            unsigned int setTransform   (Handle, const char*, const math::Affine3f&);
            unsigned int setString      (Handle, const char*, const char*);
            unsigned int setIdSet       (Handle, const char*, const xchg::IdSet&);
            unsigned int setBounds      (Handle, const char*, const xchg::BoundingBox&);
            unsigned int setDataStream  (Handle, const char*, const xchg::DataStream&);
            unsigned int setData        (Handle, const char*, const std::shared_ptr<xchg::Data>&);
            unsigned int setObjectPtr   (Handle, const char*, const xchg::ObjectPtr&);
            void         unset          (Handle, unsigned int);

            void
            dispose(Handle);

            void
            destroy(Handle);

            void
            commitFrameState(Handle);

            bool
            perSubframeParameter(Handle,
                                 unsigned int parmId) const;

            void
            beginSubframeCommits(Handle,
                                 size_t numSubframes);

            void
            commitSubframeState(Handle,
                                size_t subframe);

            void
            endSubframeCommits(Handle);

            ///////////////////
            // scene primitives
            ///////////////////
            PrimitiveHandle
            newPrimitive(const char*);

            void
            destroy(PrimitiveHandle);

            void
            add(SceneHandle,
                PrimitiveHandle);

            void
            remove(SceneHandle,
                   PrimitiveHandle);

            void
            setShape(PrimitiveHandle,
                     ShapeHandle);

            void
            setLight(PrimitiveHandle,
                     LightHandle);

            void
            setMaterial(PrimitiveHandle,
                        MaterialHandle);

            void
            setSubSurface(PrimitiveHandle,
                          SubSurfaceHandle);

            /////////////////////////
            // material configuration
            /////////////////////////
            void
            setInput(MaterialHandle,
                     MaterialHandle rtInput,
                     size_t     idx);

            ////////////////////////////
            // framebuffer configuration
            ////////////////////////////
            void
            setDenoiser(FrameBufferHandle,
                        DenoiserHandle);

            void
            setToneMapper(FrameBufferHandle,
                          ToneMapperHandle);

            void
            add(FrameBufferHandle,
                RenderPassHandle);

            void
            remove(FrameBufferHandle,
                   RenderPassHandle);

            ////////////////////////
            // shading configuration
            ////////////////////////
            void
            setupMaterial(MaterialHandle,
                          unsigned int parmId,
                          TextureHandle);

            void
            setupSubSurface(SubSurfaceHandle,
                            unsigned int parmId,
                            TextureHandle);

            /////////////////////////
            // renderer configuration
            /////////////////////////
            void
            setIntegrator(RendererHandle,
                          IntegratorHandle);

            void
            setSamplerFactory(RendererHandle,
                              SamplerFactoryHandle);

            void
            setFilter(RendererHandle,
                      PixelFilterHandle);

            ///////////////
            // render calls
            ///////////////
            void
            renderFrame(RendererHandle,
                        SceneHandle,
                        CameraHandle,
                        FrameBufferHandle,
                        size_t numSubframes);

            void
            getFrame(FrameBufferHandle,
                     img::OutputImage&);

            const float*
            getFrameData(FrameBufferHandle,
                         size_t& width,
                         size_t& height);

            void
            getMetadata(FrameBufferHandle,
                        img::OutputImage&);

            void
            getMetadata(SceneHandle,
                        img::OutputImage&);

            void
            getMetadata(RendererHandle,
                        img::OutputImage&);

            void
            getMetadata(ToneMapperHandle,
                        img::OutputImage&);

            void
            getMetadata(CameraHandle,
                        img::OutputImage&);

        private:
            template<typename RtObjectType> inline
            sys::Ref<RtObjectType>
            getInstance(Handle rtHandle) const
            {
                return rtHandle &&
                    reinterpret_cast< sys::InstanceHandle<ObjectBase>* >(rtHandle) &&
                    reinterpret_cast< sys::InstanceHandle<ObjectBase>* >(rtHandle)->instance() &&
                    reinterpret_cast< sys::InstanceHandle<ObjectBase>* >(rtHandle)->instance()->valid()
                    ? reinterpret_cast< sys::InstanceHandle<RtObjectType>* >(rtHandle)->instance()
                    : nullptr;
            }
        };
    }
}
