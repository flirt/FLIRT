// ========================================================================= //
// Copyright 2016-2018 SUPAMONKS_STUDIO                                      //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
//                                                                           //
// ========================================================================= //
#pragma once

#include "../ObjectBase.hpp"
#include "../api/PixelLayout.hpp"

namespace smks { namespace rndr
{
    class PixelData;
    class FrameBuffer;
    class Renderer;

    class Denoiser:
        public ObjectBase
    {
    public:
        typedef sys::Ref<Denoiser> Ptr;

    protected:
        Denoiser(unsigned int scnId, const CtorArgs& args):
            ObjectBase(scnId, args)
        { }

    public:
        virtual
        void
        initialize(const FrameBuffer&, const Renderer&) = 0;

        virtual
        void
        clearPixel(size_t pixelIndex) = 0;  //<! pixelIndex is expected to be (x + width * y)

        virtual
        void
        addPixelSample(size_t pixelIndex, size_t sampleIndex, const PixelData&) = 0; //<! pixelIndex is expected to be (x + width * y)

        //<! returns the denoised RGBA channel of the complete frame
        virtual
        void
        apply(const math::Vector4f*, math::Vector4f*) const = 0;
    };
}
}
