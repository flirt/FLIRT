// ========================================================================= //
// Copyright 2016-2018 SUPAMONKS_STUDIO                                      //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
//                                                                           //
// ========================================================================= //
#include "Denoiser.hpp"

using namespace smks;

////<! Initialize post processing with frame info (pixel count and content).
//void
//rndr::Denoiser::initialize(size_t                 width,
//                          size_t                  height,
//                          size_t                  numSamplesPerPixel,
//                          PixelLayout::Ptr const& pixelLayout,
//                          bool                    accumulate)
//{
//  if (width == 0 || height == 0)
//      throw sys::Exception("Invalid frame dimensions.", "Denoiser Initialization");
//  if (numSamplesPerPixel == 0)
//      throw sys::Exception("Invalid number of samples per pixel.", "Denoiser Initialization");
//  if (!pixelLayout)
//      throw sys::Exception("Invalid pixel layout.", "Denoiser Initialization");
//
//  bool reallocateCache = _width != width ||
//      _height != height ||
//      _numSamplesPerPixel != numSamplesPerPixel ||
//      !_pixelLayout.lock() ||
//      !_pixelLayout.lock()->operator==(*pixelLayout);
//
//  _width              = width;
//  _height             = height;
//  _numSamplesPerPixel = numSamplesPerPixel;
//  _pixelLayout        = pixelLayout;
//
//  if (reallocateCache)
//      allocateCache();
//  if (!accumulate)
//      clearCache();
//}
