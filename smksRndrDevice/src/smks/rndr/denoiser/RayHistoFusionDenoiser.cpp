// ========================================================================= //
// Copyright 2016-2018 SUPAMONKS_STUDIO                                      //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
//                                                                           //
// ========================================================================= //
#include <libdenoising.h>

#include <smks/math/math.hpp>
#include <smks/math/matrixAlgo.hpp>
#include <smks/img/OutputImage.hpp>
#include "RayHistoFusionDenoiser.hpp"
#include "../api/FrameBuffer.hpp"
#include "../api/PixelData.hpp"

using namespace smks;

rndr::RayHistoFusionDenoiser::RayHistoFusionDenoiser(unsigned int scnId, const CtorArgs& args):
    Denoiser                (scnId, args),
    _numBinsPerChannel      (0),
    _numMipmapLevels        (0),
    _maxPatchDistance       (0.0f),
    _minPatchMatchCount     (0),
    _patchHalfSize          (0),
    _searchWindowHalfSize   (0),
    _rGamma                 (1.0f),
    //----------------------
    _width              (0),
    _height             (0),
    _resolution         (0),
    _resolution_numBins (0),
    _rayHistoValues     (nullptr),
    _rayHistoCount      (nullptr),
    //----------------------
    _numRayHistoLayers  (0),
    _rayHistoLayers     (nullptr)
{ }

rndr::RayHistoFusionDenoiser::~RayHistoFusionDenoiser()
{
    deallocateRayHistos();
}

void
rndr::RayHistoFusionDenoiser::initialize(const FrameBuffer& framebuffer,
                                          const Renderer&       renderer)
{
    const bool dirtyRayHistos = _width != framebuffer.width() ||
        _height != framebuffer.height();
    //---
    _width      = framebuffer.width();
    _height     = framebuffer.height();
    _resolution = _width * _height;

    if (dirtyRayHistos)
        deallocateRayHistos();
    allocateRayHistos();
}

void
rndr::RayHistoFusionDenoiser::clearPixel(size_t pixelIndex)
{
    assert(pixelIndex < _resolution);
    assert(_rayHistoLayers);
    for (size_t b = 0; b < _numRayHistoLayers; ++b)
        _rayHistoLayers[b][pixelIndex] = 0.0f;
}

void
rndr::RayHistoFusionDenoiser::addPixelSample(size_t         pixelIndex,
                                              size_t            sampleIndex,
                                              const PixelData&  pData)
{
    static const float MAX_VALUE            = 2.5f;
    static const float CLAMP_VALUE          = 2.0f;
    static const float R_MAX_VALUE          = math::rcp(MAX_VALUE);
    static const float R_CLAMP_VALUE_MIN1   = math::rcp(CLAMP_VALUE - 1.0f);

    assert(pixelIndex < _resolution);

    // transform RGB values before embedding them into the ray histograms
    math::Vector4f rgba = pData.rawRGBA();
    if (math::abs(_rGamma - 1.0f) > 1e-6f)
        rgba = math::pow(rgba, _rGamma);
    rgba = (R_MAX_VALUE * rgba).cwiseMin(CLAMP_VALUE);

    size_t firstChannelBin = 0;
    size_t __bins[NUM_CHANNELS];
    for (size_t k = 0; k < NUM_CHANNELS; ++k, firstChannelBin += _numBinsPerChannel)
    {
        size_t  lowerBin        = 0;
        size_t  upperBin        = 0;
        float   lowerBinWeight  = 1.0f;
        float   upperBinWeight  = 0.0f;

        const float value       = rgba[k];
        const float bin         = value * static_cast<float>(_numBinsPerChannel-2);

        lowerBin = static_cast<size_t>(bin);
        if (lowerBin < _numBinsPerChannel-2)
            upperBinWeight  = bin - static_cast<float>(lowerBin);
        else    // value is out of bounds
        {
            lowerBin        = _numBinsPerChannel-2;
            upperBinWeight  = (value - 1.0f) * R_CLAMP_VALUE_MIN1;
        }
        upperBin        = lowerBin+1;
        lowerBinWeight  = 1.0f - upperBinWeight;

        // update histogram bins
        _rayHistoLayers[firstChannelBin + lowerBin][pixelIndex] += lowerBinWeight;
        _rayHistoLayers[firstChannelBin + upperBin][pixelIndex] += upperBinWeight;

        __bins[k] = firstChannelBin + lowerBin;
    }
    // update histogram count
    _rayHistoCount[pixelIndex] += 1.0f;
}

void
rndr::RayHistoFusionDenoiser::apply(const math::Vector4f* iRGBA, math::Vector4f* oRGBA) const
{
    FLIRT_LOG(LOG(DEBUG) << "apply ray-histogram-fusion denoising...";)
    assert(_resolution > 0);
    assert(iRGBA);
    assert(oRGBA);

    // allocate memory for input and output image data
    float* iData                    = new float[_resolution * NUM_CHANNELS];
    float* oData                    = new float[_resolution * NUM_CHANNELS];
    float* iChannels[NUM_CHANNELS]  = { iData,  iData + _resolution,    iData + (_resolution << 1) };
    float* oChannels[NUM_CHANNELS]  = { oData,  oData + _resolution,    oData + (_resolution << 1) };

    // shuffle raw RGB data (RGBA, RGBA, RGBA) -> RRR, GGG, BBB
    for (size_t i = 0; i < _resolution; ++i)
        for (size_t k = 0; k < NUM_CHANNELS; ++k)
            iChannels[k][i] = iRGBA[i][k];

    rhf_multiscale(
        _patchHalfSize,
        _searchWindowHalfSize,
        _maxPatchDistance,
        _minPatchMatchCount,
        _numMipmapLevels,
        _rayHistoLayers,
        iChannels,
        oChannels,
        nullptr, // will simply transfer alpha, otherwise must predivide by it causing numerical precision issues
        NUM_CHANNELS,
        _width,
        _height,
        _numRayHistoLayers);

    // shuffle denoised RGB data RRR, GGG, BBB -> (RGBA, RGBA, RGBA)
    // transfer raw alpha channel
    for (size_t i = 0; i < _resolution; ++i)
    {
        for (size_t k = 0; k < NUM_CHANNELS; ++k)
            oRGBA[i][k] = oChannels[k][i];
        oRGBA[i][3] = iRGBA[i][3];
    }

    delete[] iData;
    delete[] oData;
}

void
rndr::RayHistoFusionDenoiser::dispose()
{
    float gamma = 1.0f;

    deallocateRayHistos();
    getBuiltIn<size_t>  (parm::numHistoBins(),          _numBinsPerChannel);
    getBuiltIn<size_t>  (parm::numMipmapLevels(),       _numMipmapLevels);
    getBuiltIn<float>   (parm::maxPatchDistance(),      _maxPatchDistance);
    getBuiltIn<size_t>  (parm::minPatchMatchCount(),    _minPatchMatchCount);
    getBuiltIn<size_t>  (parm::patchHalfSize(),         _patchHalfSize);
    getBuiltIn<size_t>  (parm::searchWindowHalfSize(),  _searchWindowHalfSize);
    getBuiltIn<float>   (parm::gamma(),                 gamma);
    assert(gamma > 0.0f);
    _rGamma     = math::rcp(gamma);
    _width      = 0;
    _height     = 0;
    _resolution = 0;
    //---
    //Denoiser::dispose();
}

void
rndr::RayHistoFusionDenoiser::commitFrameState(const parm::Container&   parms,
                                      const xchg::IdSet&        dirties)
{
    logParameters(scnId(), parms, "builtIns", dirties);
    logParameters(scnId(), userParameters(), "userParms", "USER");
    //---

    size_t numHistoBins = _numBinsPerChannel;

    for (xchg::IdSet::const_iterator id = dirties.begin(); id != dirties.end(); ++id)
        if (*id == parm::numHistoBins().id())
            getBuiltIn<size_t>(parm::numHistoBins(), _numBinsPerChannel, &parms);
        else if (*id == parm::numMipmapLevels().id())
            getBuiltIn<size_t>(parm::numMipmapLevels(), _numMipmapLevels, &parms);
        else if (*id == parm::maxPatchDistance().id())
            getBuiltIn<float>(parm::maxPatchDistance(), _maxPatchDistance, &parms);
        else if (*id == parm::minPatchMatchCount().id())
            getBuiltIn<size_t>(parm::minPatchMatchCount(), _minPatchMatchCount, &parms);
        else if (*id == parm::patchHalfSize().id())
            getBuiltIn<size_t>(parm::patchHalfSize(), _patchHalfSize, &parms);
        else if (*id == parm::searchWindowHalfSize().id())
            getBuiltIn<size_t>(parm::searchWindowHalfSize(), _searchWindowHalfSize, &parms);
        else if (*id == parm::gamma().id())
        {
            float gamma = 1.0f;

            getBuiltIn<float>(parm::gamma(), gamma, &parms);
            assert(gamma > 0.0f);
            _rGamma = math::rcp(gamma);
        }

    if (numHistoBins != _numBinsPerChannel)
        deallocateRayHistos();

    FLIRT_LOG(LOG(DEBUG)
        << "\n-----------"
        << "\nray histogram fusion denoiser ID = " << scnId() << "\n"
        << "\n\t- # histo bins per color channel = " << _numBinsPerChannel
        << "\n\t- # mipmap levels = " << _numMipmapLevels
        << "\n\t- max patch distance = " << _maxPatchDistance
        << "\n\t- min # patch matches = " << _minPatchMatchCount
        << "\n\t- patch half size = " << _patchHalfSize
        << "\n\t- search window half size = " << _searchWindowHalfSize
        << "\n\t- 1/gamma = " << _rGamma
        << "\n-----------";)
}

// virtual
void
rndr::RayHistoFusionDenoiser::getMetadata(img::OutputImage& frame) const
{
    const float gamma = _rGamma > 1e-6f ? math::rcp(_rGamma) : 0.0f;
    frame.setMetadata("rhf/#histoBins",             _numBinsPerChannel);
    frame.setMetadata("rhf/#mipmapLevels",          _numMipmapLevels);
    frame.setMetadata("rhf/maxPatchDistance",       _maxPatchDistance);
    frame.setMetadata("rhf/minPatchMatchCount",     _minPatchMatchCount);
    frame.setMetadata("rhf/patchHalfSize",          _patchHalfSize);
    frame.setMetadata("rhf/searchWindowHalfSize",   _searchWindowHalfSize);
    frame.setMetadata("rhf/gamma",                  gamma);
}

void
rndr::RayHistoFusionDenoiser::deallocateRayHistos()
{
    if (_rayHistoValues)
        delete[] _rayHistoValues;
    if (_rayHistoCount)
        delete[] _rayHistoCount;
    if (_rayHistoLayers)
        delete[] _rayHistoLayers;
    _rayHistoValues     = nullptr;
    _rayHistoCount      = nullptr;
    _rayHistoLayers     = nullptr;
    _numRayHistoLayers  = 0;
    _resolution_numBins = 0;
}

void
rndr::RayHistoFusionDenoiser::allocateRayHistos()
{
    deallocateRayHistos();
    //---
    if (_numBinsPerChannel > 1 && _resolution > 0)
    {
        const size_t numBins = _numBinsPerChannel * NUM_CHANNELS;

        _rayHistoValues = new float[_resolution * numBins];
        _rayHistoCount  = new float[_resolution];
        memset(_rayHistoValues, 0,  sizeof(float) * _resolution * numBins);
        memset(_rayHistoCount,  0,  sizeof(float) * _resolution);

        // prepare the complete ray histogram redirection structure
        _numRayHistoLayers  = numBins + 1;
        _rayHistoLayers     = new float*[_numRayHistoLayers];

        float* rayHistoValues = _rayHistoValues;
        for (size_t b = 0; b < numBins; ++b)
        {
            _rayHistoLayers[b]  = rayHistoValues;
            rayHistoValues      += _resolution;
        }
        _rayHistoLayers[numBins] = _rayHistoCount;
    }
}
