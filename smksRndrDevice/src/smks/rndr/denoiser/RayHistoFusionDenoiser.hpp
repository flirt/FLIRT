// ========================================================================= //
// Copyright 2016-2018 SUPAMONKS_STUDIO                                      //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
//                                                                           //
// ========================================================================= //
#pragma once

#include "Denoiser.hpp"

namespace smks { namespace rndr
{
    class RayHistoFusionDenoiser:
        public Denoiser
    {
        VALID_RTOBJECT
    private:
        size_t  _numBinsPerChannel;
        size_t  _numMipmapLevels;
        float   _maxPatchDistance;
        size_t  _minPatchMatchCount;
        size_t  _patchHalfSize;
        size_t  _searchWindowHalfSize;
        float   _rGamma;
        //----------------------------
        size_t  _width;
        size_t  _height;
        size_t  _resolution;
        size_t  _resolution_numBins;
        float*  _rayHistoValues;    //<! size = width * height * #bins * NUM_CHANNELS
        //<! index(x, y, bin, channel) = x + width * (y + height * (bin + #bins * channel))
        float*  _rayHistoCount;     //<! size = width * height
        //----------------------------
        size_t  _numRayHistoLayers;
        float** _rayHistoLayers;
    private:
        enum { NUM_CHANNELS = 3 };

        CREATE_RTOBJECT(RayHistoFusionDenoiser, Denoiser)
    protected:
        RayHistoFusionDenoiser(unsigned int scnId, const CtorArgs&);
    public:
        ~RayHistoFusionDenoiser();

        void
        initialize(const FrameBuffer&, const Renderer&);

        void
        clearPixel(size_t pixelIndex);
        void
        addPixelSample(size_t pixelIndex, size_t sampleIndex, const PixelData&);

        void
        apply(const math::Vector4f*, math::Vector4f*) const;

        void
        dispose();

        void
        commitFrameState(const parm::Container&, const xchg::IdSet&);

        inline
        void
        beginSubframeCommits(size_t)
        { }

        inline
        void
        commitSubframeState(size_t, const parm::Container&)
        { }

        inline
        void
        endSubframeCommits()
        { }

        inline
        bool
        perSubframeParameter(unsigned int) const
        {
            return false;
        }

        void
        getMetadata(img::OutputImage&) const;
    private:
        void
        deallocateRayHistos();
        void
        allocateRayHistos();
    };
}
}
