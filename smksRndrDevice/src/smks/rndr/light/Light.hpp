// ========================================================================= //
// Modification copyright 2016-2018 SUPAMONKS_STUDIO                         //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
// _________________________________________________________________________ //
// Copyright 2009-2013 Intel Corporation                                     //
//                                                                           //
// Licensed under the Apache License, Version 2.0 (the "License");           //
// you may not use this file except in compliance with the License.          //
// You may obtain a copy of the License at                                   //
//                                                                           //
//     http://www.apache.org/licenses/LICENSE-2.0                            //
//                                                                           //
// Unless required by applicable law or agreed to in writing, software       //
// distributed under the License is distributed on an "AS IS" BASIS,         //
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  //
// See the License for the specific language governing permissions and       //
// limitations under the License.                                            //
//                                                                           //
// ========================================================================= //
#pragma once

#include <smks/parm/BuiltIns.hpp>
#include "../ObjectBase.hpp"
#include "../sampler/Sample.hpp"
#include "../shape/DifferentialGeometry.hpp"

namespace smks { namespace rndr
{
    class Shape;
    class AreaLight;
    class EnvironmentLight;

    /*! Interface to different lights. A light can be evaluated,
    *  sampled, and the sampling PDF be evaluated. */
    class Light:
        public ObjectBase
    {
    public:
        typedef sys::Ref<Light> Ptr;

    protected:
        int _illumMask;

    protected:
        Light(unsigned int scnId, const CtorArgs&);

    public:
        /*! Evaluates the radiance that would arrive at a given location
        *  from a given direction assuming no blocking geometry. \returns
        *  the emitted radiance. */
        virtual inline
        math::Vector4f
        eval(const DifferentialGeometry&,   /*!< The shade point that is illuminated.    */
             const math::Vector4f& wi)      /*!< The direction the light is coming from. */ const
        {
            return math::Vector4f::Zero();
        }

        virtual inline
        sys::Ref<Shape>
        shape() const
        {
            return sys::null;
        }

        /*! Samples the light for a point to shade. \returns the radiance
        *  arriving from the sampled direction. */
        virtual inline
        math::Vector4f
        sample(RTCScene,
               const DifferentialGeometry&,     /*!< The shade point that is illuminated. */
               Sample<math::Vector4f>&  wi,     /*!< Returns the sampled direction including PDF.*/
               float&                   tMax,   /*!< Returns the distance of the light. */
               const math::Vector2f&)           /*!< Sample locations are provided by the caller. */ const
        {
            return math::Vector4f::Zero();
        }

        /*! Evaluates the probability distribution function used by the
        *  sampling function of the light for a shade location and
        *  direction. \returns the probability density */
        virtual inline
        float
        pdf(const DifferentialGeometry&,    /*!< The shade location to compute the PDF for. */
            const math::Vector4f& wi)       /*!< The direction to compute the PDF for. */ const
        {
            return sys::zero;
        }

        /*! Indicates that the sampling of the light is expensive and the
        *  integrator should presample the light. */
        virtual inline
        bool
        precompute() const
        {
            return false;
        }

        //<! indicates that the light is actually active and emits irradiance in the scene
        virtual
        bool
        ready() const = 0;

        virtual inline
        void
        add(sys::Ref<Shape> const&)
        { }

        virtual inline
        void
        remove(sys::Ref<Shape> const&)
        { }

        __forceinline
        int
        illumMask() const
        {
            return _illumMask;
        }

        virtual
        void
        dispose();

        virtual
        void
        commitFrameState(const parm::Container&, const xchg::IdSet&);

        virtual inline
        void
        beginSubframeCommits(size_t)
        { }

        virtual inline
        void
        commitSubframeState(size_t, const parm::Container&)
        { }

        virtual inline
        void
        endSubframeCommits()
        { }

        virtual inline
        bool
        perSubframeParameter(unsigned int) const
        {
            return false;
        }

        virtual inline
        AreaLight*
        asAreaLight()
        {
            return nullptr;
        }
        virtual inline
        const AreaLight*
        asAreaLight() const
        {
            return nullptr;
        }

        virtual inline
        EnvironmentLight*
        asEnvironmentLight()
        {
            return nullptr;
        }
        virtual inline
        const EnvironmentLight*
        asEnvironmentLight() const
        {
            return nullptr;
        }
    };
}
}
