// ========================================================================= //
// Copyright 2016-2018 SUPAMONKS_STUDIO                                      //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
//                                                                           //
// ========================================================================= //
#pragma once

#include <smks/math/math.hpp>
#include "AreaLight.hpp"
#include "../shape/Shape.hpp"
#include "../sampler/ShapeSampler.hpp"

namespace smks { namespace rndr
{
    class MeshLight:
        public AreaLight
    {
        VALID_RTOBJECT
    private:
        math::Vector4f  _L;     //!< Radiance (W/(m^2*sr))
        Shape::Ptr      _shape;

        CREATE_RTOBJECT(MeshLight, Light)
    protected:
        MeshLight(unsigned int scnId, const CtorArgs& args):
            AreaLight   (scnId, args),
            _L          (math::Vector4f::Zero()),
            _shape      (sys::null)
        { }
    public:
        ~MeshLight()
        {
            dispose();
        }

        __forceinline
        math::Vector4f
        Le(const DifferentialGeometry& dg, const math::Vector4f& wo) const
        {
            assert(_shape);
            return math::dot3(dg.Ns, wo) < 0.0f ? math::Vector4f::Zero() : _L;
        }

        __forceinline
        math::Vector4f
        eval(const DifferentialGeometry& dg, const math::Vector4f& wi) const
        {
            assert(_shape);
            return Le(dg, -wi);
        }

        inline
        math::Vector4f
        sample(RTCScene rtcScene, const DifferentialGeometry& dg, Sample<math::Vector4f>& wi, float& tmax, const math::Vector2f& s) const
        {
            assert(_shape);
            math::Vector4f  pos, normal;
            float           pdf;

            _shape->sample(rtcScene, _shape->rtcGeomId(), s, dg.time, pos, normal, pdf);

            const math::Vector4f&   d       = pos - dg.P;
            const float             dDotN   = math::dot3(d, normal);

            if (dDotN >= -1e-9f)
                return math::Vector4f::Zero();

            tmax        = math::len3(d);
            wi.value    = d * math::rcp(tmax);
            wi.pdf      = pdf * (tmax * tmax * tmax * math::rcp(math::abs(dDotN))); // d is not normalized

            return _L;
        }

        __forceinline
        float
        pdf(const DifferentialGeometry& onLight,
            const DifferentialGeometry& dg) const
        {
            const math::Vector4f&   d       = onLight.P - dg.P;
            const float             dDotNl  = math::dot3(d, onLight.Ns);

            if (dDotNl < 0.0f)
            {
                const float dist    = math::len3(d);
                const float pdf     = _shape->pdf(onLight.time);
                return pdf * dist * dist * dist * math::rcp(math::abs(dDotNl));
            }
            else
                return 0.0f;
        }

        inline
        void
        add(Shape::Ptr const& shape)
        {
            assert(shape);
            assert(!_shape);
            _shape = shape;
        }

        inline
        void
        remove(Shape::Ptr const& shape)
        {
            assert(shape);
            assert(_shape == shape);
            _shape = sys::null;
        }

        Shape::Ptr
        shape() const
        {
            return _shape;
        }

        inline
        void
        shape(Shape::Ptr const& value)
        {
            _shape = value;
        }

        inline
        void
        commitFrameState(const parm::Container& parms, const xchg::IdSet& dirties)
        {
            Light::commitFrameState(parms, dirties);
            //---
            bool updateDirection = false;

            for (xchg::IdSet::const_iterator id = dirties.begin(); id != dirties.end(); ++id)
                if (*id == parm::radiance().id())
                    getBuiltIn<math::Vector4f>(parm::radiance(), _L, &parms);

            if (!_shape)
                std::cerr << "WARNING: no shape is attached to mesh light ID = " << scnId() << "." << std::endl;

            FLIRT_LOG(LOG(DEBUG)
                << "\n-----------"
                << "\nmesh light ID = " << scnId() << "\n"
                << "\n\t- radiance = " << _L.transpose()
                << "\n\t- shape ID = " << (_shape ? _shape->scnId() : 0)
                << "\n-----------";)
        }

        inline
        void
        dispose()
        {
            getBuiltIn<math::Vector4f>(parm::radiance(), _L);
            _shape = sys::null;
            //---
            Light::dispose();
        }

        inline
        bool
        ready() const
        {
            return _shape && _shape->ready() && math::max_xyz(_L.cwiseAbs()) > 0.0f;
        }

        inline
        MeshLight*
        asMeshLight()
        {
            return this;
        }
        inline
        const MeshLight*
        asMeshLight() const
        {
            return this;
        }
    };
}
}
