// ========================================================================= //
// Modification copyright 2016-2018 SUPAMONKS_STUDIO                         //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
// _________________________________________________________________________ //
// Copyright 2009-2013 Intel Corporation                                     //
//                                                                           //
// Licensed under the Apache License, Version 2.0 (the "License");           //
// you may not use this file except in compliance with the License.          //
// You may obtain a copy of the License at                                   //
//                                                                           //
//     http://www.apache.org/licenses/LICENSE-2.0                            //
//                                                                           //
// Unless required by applicable law or agreed to in writing, software       //
// distributed under the License is distributed on an "AS IS" BASIS,         //
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  //
// See the License for the specific language governing permissions and       //
// limitations under the License.                                            //
//                                                                           //
// ========================================================================= //
#pragma once

#include <smks/math/math.hpp>
#include "Light.hpp"

namespace smks { namespace rndr
{
    class PointLight:
        public Light
    {
        VALID_RTOBJECT
    private:
        math::Vector4f _P;  //!< Position of the point light
        math::Vector4f _I;  //!< Radiant intensity (W/sr)

        CREATE_RTOBJECT(PointLight, Light)
    protected:
        PointLight(unsigned int scnId, const CtorArgs& args):
            Light   (scnId, args),
            _P      (math::Vector4f::Zero()),
            _I      (math::Vector4f::Zero())
        {
            dispose();
        }
    public:
        ~PointLight()
        {
            dispose();
        }

        __forceinline
        math::Vector4f
        sample(RTCScene, const DifferentialGeometry& dg, Sample<math::Vector4f>& wi, float& tmax, const math::Vector2f&) const
        {
            const math::Vector4f&   d           = _P - dg.P;
            const float             distance    = math::len3(d);

            wi.value    = d * math::rcp(distance);
            wi.pdf      = math::sqr(distance);
            tmax        = distance;

            return _I;
        }

        __forceinline
        float
        pdf(const DifferentialGeometry&, const math::Vector4f&) const
        {
            return 0.0f;
        }

        inline
        void
        commitFrameState(const parm::Container& parms, const xchg::IdSet& dirties)
        {
            Light::commitFrameState(parms, dirties);
            //---
            for (xchg::IdSet::const_iterator id = dirties.begin(); id != dirties.end(); ++id)
                if (*id == parm::worldTransform().id())
                {
                    math::Affine3f xfm;

                    getBuiltIn<math::Affine3f>(parm::worldTransform(), xfm, &parms);
                    _P = math::transformPoint(xfm, math::Vector4f::UnitW());
                }
                else if (*id == parm::intensity().id())
                    getBuiltIn<math::Vector4f>(parm::intensity(), _I, &parms);

            FLIRT_LOG(LOG(DEBUG)
                << "\n-----------"
                << "\npoint light ID = " << scnId() << "\n"
                << "\n\t- world position = " << _P.transpose()
                << "\n\t- intensity = " << _I.transpose()
                << "\n-----------";)
        }

        inline
        void
        dispose()
        {
            math::Affine3f xfm;

            getBuiltIn<math::Vector4f>(parm::intensity(),       _I);
            getBuiltIn<math::Affine3f>(parm::worldTransform(),  xfm);
            _P = math::transformPoint(xfm, math::Vector4f::UnitW());
            //---
            Light::dispose();
        }

        inline
        bool
        ready() const
        {
            return math::max_xyz(_I.cwiseAbs()) > 0.0f;
        }
    };
}
}
