// ========================================================================= //
// Modification copyright 2016-2018 SUPAMONKS_STUDIO                         //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
// _________________________________________________________________________ //
// Copyright 2009-2013 Intel Corporation                                     //
//                                                                           //
// Licensed under the Apache License, Version 2.0 (the "License");           //
// you may not use this file except in compliance with the License.          //
// You may obtain a copy of the License at                                   //
//                                                                           //
//     http://www.apache.org/licenses/LICENSE-2.0                            //
//                                                                           //
// Unless required by applicable law or agreed to in writing, software       //
// distributed under the License is distributed on an "AS IS" BASIS,         //
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  //
// See the License for the specific language governing permissions and       //
// limitations under the License.                                            //
//                                                                           //
// ========================================================================= //
#pragma once

#include <smks/sys/sync/Mutex.hpp>
#include <smks/math/math.hpp>
#include <smks/xchg/Image.hpp>
#include <smks/img/AbstractImage.hpp>
#include "EnvironmentLight.hpp"
#include "../sampler/Distribution2d.hpp"

namespace smks
{
    namespace xchg
    {
        class Image;
    }
    namespace rndr
    {
        /*! Implements a texture mapped environment light. */
        class HDRILight:
            public EnvironmentLight
        {
            VALID_RTOBJECT
        private:
            mutable sys::sync::MutexSys _mutex;

            xchg::Image::WPtr   _pixels;        //!< The image mapped to the environment.
            math::Vector4f      _intensity;     //!< Scaling factor for the image.
            math::Affine3f      _localToWorld;  //!< Transformation from light space into world space
            //----------------------
            float               _rWidth;
            float               _rHeight;
            Distribution2D::Ptr _distribution;  //!< The 2D distribution used to importance sample the image.
            math::Affine3f      _worldToLocal;  //!< Transformation from light space into world space

            CREATE_RTOBJECT(HDRILight, Light)
        protected:
            HDRILight(unsigned int scnId, const CtorArgs&);
        public:
            __forceinline
            math::Vector4f
            Le(const math::Vector4f& wo_) const
            {
                xchg::Image::Ptr const&     pixels  = _pixels.lock();
                const img::AbstractImage*   image   = pixels ? pixels->getImage() : nullptr;
                if (image == nullptr)
                    return math::Vector4f::Ones();

                const math::Vector4f&   wi  = math::transformVector(_worldToLocal, -wo_);
                const float theta   = math::acos(math::clamp(wi.y(), -1.0f, 1.0f));
                float       phi     = math::atan2(-wi.z(), -wi.x());

                if (phi < 0.0f)
                    phi += static_cast<float>(sys::two_pi);


                const float     u       = 1.0f - (phi * static_cast<float>(sys::one_over_two_pi));
                const float     uWidth  = image->width() * u;
                const size_t    x       = math::clamp(static_cast<size_t>(uWidth), static_cast<size_t>(0), image->width()-1);
                size_t          xNext   = x + 1;
                const float     v       = theta * static_cast<float>(sys::one_over_pi);
                const float     vHeight = image->height() *
#if defined(FLIRT_NO_YFLIPPED_IMG_LOADING)
                    v;
#else
                    (1.0f - v);
#endif // defined()
                const size_t    y       = math::clamp(static_cast<size_t>(vHeight), static_cast<size_t>(0), image->height()-1);
                size_t          yNext   = y + 1;

                if (xNext == image->width())
                    xNext = 0;
                if (yNext == image->height())
                    yNext = image->height() - 1;

                const float     a       = uWidth - x;
                const float     b       = vHeight - y;
                const float     ab      = a * b;

                const bool noClamping = true;
                const math::Vector4f& c0    = image->pixel(math::Vector2i(x,     y    ), noClamping);
                const math::Vector4f& c1    = image->pixel(math::Vector2i(xNext, y    ), noClamping);
                const math::Vector4f& c2    = image->pixel(math::Vector2i(xNext, yNext), noClamping);
                const math::Vector4f& c3    = image->pixel(math::Vector2i(x,     yNext), noClamping);

                return ((1.0f - a - b + ab) * c0 + (a - ab) * c1 + ab * c2 + (b - ab) * c3)
                    .cwiseProduct(_intensity);
            }

            __forceinline
            math::Vector4f
            eval(const DifferentialGeometry&, const math::Vector4f& wi_) const
            {
                return Le(-wi_);
            }

            __forceinline
            math::Vector4f
            sample(RTCScene,
                   const DifferentialGeometry&,
                   Sample<math::Vector4f>&  wi_,
                   float&                   tmax,
                   const math::Vector2f&    sample) const
            {
                xchg::Image::Ptr const&     pixels  = _pixels.lock();
                // lazy loading of the image and 2d distribution precomputation
                const img::AbstractImage*   image   = pixels ? pixels->getImage() : nullptr;
                if (image == nullptr)
                    return math::Vector4f::Zero();
                if (_distribution->empty())
                {
                    sys::sync::Lock<sys::sync::MutexSys> lock(_mutex);
                    const_cast<HDRILight*>(this)->computeDistribution();
                }

                const Sample<math::Vector2f>& pix = _distribution->sample(sample);
                const float theta   = static_cast<float>(sys::pi) * pix.value.y() * _rHeight;
                const float sTheta  = math::sin(theta);
                const float phi     = static_cast<float>(sys::two_pi) * (1.0f - pix.value.x() * _rWidth);

                wi_.value   = math::transformVector(
                    _localToWorld,
                    math::Vector4f(-sTheta * math::cos(phi), math::cos(theta), - sTheta * math::sin(phi), 0.0f));
                wi_.pdf     = pix.pdf * math::rcp(static_cast<float>(sys::two_pi) * static_cast<float>(sys::pi) * sTheta);
                tmax        = static_cast<float>(sys::inf);

                return image->pixel(pix.value.cast<int>())
                    .cwiseProduct(_intensity);
            }

            __forceinline
            float
            pdf(const DifferentialGeometry& dg,
                const math::Vector4f&       wi_) const
            {
                const math::Vector4f& wi    = math::transformVector(_worldToLocal, wi_);
                const float theta   = math::acos(math::clamp(wi.y(), -1.0f, 1.0f));
                const float sTheta  = math::sin(theta);

                if (abs(sTheta) < 1e-6f)
                    return 0.0f;

                float phi = math::atan2(-wi.z(), -wi.x());
                if (phi < 0.0f)
                    phi += static_cast<float>(sys::two_pi);

                const math::Vector2f uv(
                    1.0f - phi * static_cast<float>(sys::one_over_two_pi),
                    theta * static_cast<float>(sys::one_over_pi));

                return _distribution->pdf(uv) *
                    math::rcp(static_cast<float>(sys::two_pi) * static_cast<float>(sys::pi) * sTheta);
            }

            inline
            bool
            precompute() const
            {
                return true;
            }

            inline
            bool
            ready() const
            {
                return !_pixels.expired() &&
                    math::add_xyz(_intensity.cwiseAbs()) > 0.0f;
            }

            void
            dispose();

            void
            commitFrameState(const parm::Container&, const xchg::IdSet&);
        private:
            void
            computeDistribution();
        };
    }
}
