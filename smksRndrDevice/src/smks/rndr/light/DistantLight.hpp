// ========================================================================= //
// Modification copyright 2016-2018 SUPAMONKS_STUDIO                         //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
// _________________________________________________________________________ //
// Copyright 2009-2013 Intel Corporation                                     //
//                                                                           //
// Licensed under the Apache License, Version 2.0 (the "License");           //
// you may not use this file except in compliance with the License.          //
// You may obtain a copy of the License at                                   //
//                                                                           //
//     http://www.apache.org/licenses/LICENSE-2.0                            //
//                                                                           //
// Unless required by applicable law or agreed to in writing, software       //
// distributed under the License is distributed on an "AS IS" BASIS,         //
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  //
// See the License for the specific language governing permissions and       //
// limitations under the License.                                            //
//                                                                           //
// ========================================================================= //
#pragma once

#include <smks/math/math.hpp>
#include "EnvironmentLight.hpp"
#include "../sampler/ShapeSampler.hpp"

namespace smks { namespace rndr
{
    /*! Implements a distant light. The distant light illuminates from
    *  infinity from a cone of directions. This simulates the light
    *  field of a far away big object like the sun. */
    class DistantLight:
        public EnvironmentLight
    {
        VALID_RTOBJECT
    private:
        math::Vector4f  _wo;        //!< Negative light direction
        math::Vector4f  _L;         //!< Radiance (W/(m^2*sr))
        float           _halfAngle; //!< Half illumination angle
        //--------------
        float           _cHalfAngle;

        CREATE_RTOBJECT(DistantLight, Light)
    protected:
        DistantLight(unsigned int scnId, const CtorArgs& args):
            EnvironmentLight(scnId, args),
            _wo             (math::Vector4f::Zero()),
            _L              (math::Vector4f::Zero()),
            _halfAngle      (0.0f),
            //--------------
            _cHalfAngle     (0.0f)
        {
            dispose();
        }
    public:
        ~DistantLight()
        {
            dispose();
        }

        __forceinline
        math::Vector4f
        Le(const math::Vector4f&) const
        {
            return math::Vector4f::Zero();
        }

        __forceinline
        math::Vector4f
        eval(const DifferentialGeometry&, const math::Vector4f& wi) const
        {
            if (math::dot3(wi, _wo) >= _cHalfAngle)
                return _L;
            return math::Vector4f::Zero();
        }

        __forceinline
        math::Vector4f
        sample(RTCScene, const DifferentialGeometry& dg, Sample<math::Vector4f>& wi, float& tmax, const math::Vector2f& s) const
        {
            wi      = uniformSampleCone(s.x(), s.y(), _halfAngle, _wo);
            tmax    = static_cast<float>(sys::inf);
            return _L;
        }

        __forceinline
        float
        pdf(const DifferentialGeometry&, const math::Vector4f& wi) const
        {
            return uniformSampleConePDF(wi, _halfAngle, _wo);
        }

        inline
        void
        commitFrameState(const parm::Container& parms, const xchg::IdSet& dirties)
        {
            Light::commitFrameState(parms, dirties);
            //---
            bool updateDirection = false;

            for (xchg::IdSet::const_iterator id = dirties.begin(); id != dirties.end(); ++id)
                if (*id == parm::worldTransform().id())
                {
                    math::Affine3f xfm;

                    getBuiltIn<math::Affine3f>(parm::worldTransform(), xfm, &parms);
                    _wo = math::transformVector(xfm, -(-math::Vector4f::UnitZ())); // forward = -Z, but negative direction
                    math::normalize3(_wo);
                }
                else if (*id == parm::radiance().id())
                    getBuiltIn<math::Vector4f>(parm::radiance(), _L, &parms);
                else if (*id == parm::halfAngleD().id())
                {
                    float halfAngleD;

                    getBuiltIn<float>(parm::halfAngleD(), halfAngleD, &parms);
                    _halfAngle = math::deg2rad(halfAngleD);
                }

            if (_cHalfAngle < -1.0f)
                _cHalfAngle = math::cos(_halfAngle);

            FLIRT_LOG(LOG(DEBUG)
                << "\n-----------"
                << "\ndistant light ID = " << scnId() << "\n"
                << "\n\t- world direction = " << _wo.transpose()
                << "\n\t- half angle (rad) = " << _halfAngle
                << "\n\t- radiance = " << _L.transpose()
                << "\n-----------";)
        }

        inline
        void
        dispose()
        {
            math::Affine3f  xfm;
            float           halfAngleD;

            getBuiltIn<math::Vector4f>  (parm::radiance(),          _L);
            getBuiltIn<float>           (parm::halfAngleD(),        halfAngleD);
            getBuiltIn<math::Affine3f>  (parm::worldTransform(),    xfm);
            _wo = math::transformVector(xfm, -(-math::Vector4f::UnitZ())); // forward = -Z, but negative direction
            math::normalize3(_wo);
            _halfAngle  = math::deg2rad(halfAngleD);
            //---
            _cHalfAngle = -2.0f;
            //---
            Light::dispose();
        }

        inline
        bool
        ready() const
        {
            return math::max_xyz(_L.cwiseAbs()) > 0.0f;
        }
    };
}
}
