// ========================================================================= //
// Modification copyright 2016-2018 SUPAMONKS_STUDIO                         //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
// _________________________________________________________________________ //
// Copyright 2009-2013 Intel Corporation                                     //
//                                                                           //
// Licensed under the Apache License, Version 2.0 (the "License");           //
// you may not use this file except in compliance with the License.          //
// You may obtain a copy of the License at                                   //
//                                                                           //
//     http://www.apache.org/licenses/LICENSE-2.0                            //
//                                                                           //
// Unless required by applicable law or agreed to in writing, software       //
// distributed under the License is distributed on an "AS IS" BASIS,         //
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  //
// See the License for the specific language governing permissions and       //
// limitations under the License.                                            //
//                                                                           //
// ========================================================================= //
#include <smks/math/matrixAlgo.hpp>
#include <smks/xchg/Array2D.hpp>

#include "HDRILight.hpp"

using namespace smks;

rndr::HDRILight::HDRILight(unsigned int scnId, const CtorArgs& args):
    EnvironmentLight(scnId, args),
    _pixels         (),
    _intensity      (math::Vector4f::Zero()),
    _localToWorld   (math::Affine3f::Identity()),
    _rWidth         (0.0f),
    _rHeight        (0.0f),
    _distribution   (new Distribution2D()),
    _worldToLocal   (math::Affine3f::Identity())
{
    dispose();
}

// virtual
void
rndr::HDRILight::dispose()
{
    getBuiltIn<math::Affine3f>  (parm::worldTransform(),    _localToWorld);
    getBuiltIn<math::Vector4f>  (parm::intensity(),         _intensity);

    _pixels.reset();
    _worldToLocal   = _localToWorld.inverse();
    _rWidth         = 0.0f;
    _rHeight        = 0.0f;
    _distribution->dispose();
    //---
    EnvironmentLight::dispose();
}

// virtual
void
rndr::HDRILight::commitFrameState(const parm::Container& parms, const xchg::IdSet& dirties)
{
    EnvironmentLight::commitFrameState(parms, dirties);
    //---
    xchg::ObjectPtr obj;

    for (xchg::IdSet::const_iterator id = dirties.begin(); id != dirties.end(); ++id)
    {
        if (*id == parm::image().id())
        {
            getBuiltIn<xchg::ObjectPtr>(parm::image(), obj, &parms);
            _pixels = obj.shared_ptr<xchg::Image>();
        }
        else if (*id == parm::worldTransform().id())
        {
            getBuiltIn<math::Affine3f>(parm::worldTransform(), _localToWorld, &parms);
            _worldToLocal = _localToWorld.inverse();
        }
        else if (*id == parm::intensity().id())
            getBuiltIn<math::Vector4f>(parm::intensity(), _intensity, &parms);
    }

    FLIRT_LOG(LOG(DEBUG)
        << "\n----------"
        << "\nhdri light ID = " << scnId() << "\n"
        << "\n\t- local->world = " << _localToWorld.matrix()
        << "\n\t- world->local = " << _worldToLocal.matrix()
        << "\n\t- color = " << _intensity.transpose()
        << "\n----------";)
}

void
rndr::HDRILight::computeDistribution()
{
    _rWidth     = 0.0f;
    _rHeight    = 0.0f;
    _distribution->dispose();

    xchg::Image::Ptr const& pixels = _pixels.lock();
    if (!pixels)
        return;

    const img::AbstractImage* image = pixels->getImage();
    if (image == nullptr)
        return;

    const size_t width  = image->width();
    const size_t height = image->height();
    if (width == 0 || height == 0)
        return;

    _rWidth     = math::rcp(static_cast<float>(width));
    _rHeight    = math::rcp(static_cast<float>(height));

    const float k = static_cast<float>(sys::pi) * _rHeight;

    xchg::Array2D<float> importance(height, width);
    math::Vector2i p(math::Vector2i::Zero());
    for (p.y() = 0; p.y() < static_cast<int>(height); ++p.y())
        for (p.x() = 0; p.x() < static_cast<int>(width); ++p.x())
            importance.set(
                p.y(), p.x(),
                sinf(k * (p.y() + 0.5f)) * math::add_xyz(image->pixel(p)));

    _distribution = new Distribution2D(importance, width, height);
}
