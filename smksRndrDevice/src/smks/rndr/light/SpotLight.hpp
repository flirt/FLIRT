// ========================================================================= //
// Modification copyright 2016-2018 SUPAMONKS_STUDIO                         //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
// _________________________________________________________________________ //
// Copyright 2009-2013 Intel Corporation                                     //
//                                                                           //
// Licensed under the Apache License, Version 2.0 (the "License");           //
// you may not use this file except in compliance with the License.          //
// You may obtain a copy of the License at                                   //
//                                                                           //
//     http://www.apache.org/licenses/LICENSE-2.0                            //
//                                                                           //
// Unless required by applicable law or agreed to in writing, software       //
// distributed under the License is distributed on an "AS IS" BASIS,         //
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  //
// See the License for the specific language governing permissions and       //
// limitations under the License.                                            //
//                                                                           //
// ========================================================================= //
#pragma once

#include <smks/math/math.hpp>
#include <smks/math/matrixAlgo.hpp>
#include "Light.hpp"

namespace smks { namespace rndr
{
    class SpotLight:
        public Light
    {
        VALID_RTOBJECT
    private:
        math::Vector4f  _P;         //!< Position of the spot light
        math::Vector4f  _D;         //!< Negative light direction of the spot light
        math::Vector4f  _I;         //!< Radiant intensity (W/sr)
        float           _cAngleMin;
        float           _cAngleMax; //!< Linear falloff region
        //--------------
        float           _rAngleRange;

        CREATE_RTOBJECT(SpotLight, Light)
    protected:
        SpotLight(unsigned int scnId, const CtorArgs& args):
            Light       (scnId, args),
            _P          (math::Vector4f::Zero()),
            _D          (math::Vector4f::Zero()),
            _I          (math::Vector4f::Zero()),
            _cAngleMin  (0.0f),
            _cAngleMax  (0.0f),
            //-----------
            _rAngleRange(0.0f)
        {
            dispose();
        }
    public:
        ~SpotLight()
        {
            dispose();
        }

        inline
        math::Vector4f
        sample(RTCScene, const DifferentialGeometry& dg, Sample<math::Vector4f>& wi, float& tmax, const math::Vector2f&) const
        {
            const math::Vector4f&   d           = _P - dg.P;
            const float             distance    = math::len3(d);

            wi.value    = d * math::rcp(distance);
            wi.pdf      = math::sqr(distance);
            tmax        = distance;

            const float cAngle = math::dot3(wi.value, _D);

            if (_cAngleMax < _cAngleMin)    // in falloff region
                return _I * math::clamp((cAngle - _cAngleMax) * _rAngleRange);
            else if (_cAngleMin < cAngle)   // inside spot
                return _I;
            else
                return math::Vector4f::Zero();
        }

        __forceinline
        float
        pdf(const DifferentialGeometry&, const math::Vector4f&) const
        {
            return 0.0f;
        }

        inline
        void
        commitFrameState(const parm::Container& parms, const xchg::IdSet& dirties)
        {
            Light::commitFrameState(parms, dirties);
            //---
            for (xchg::IdSet::const_iterator id = dirties.begin(); id != dirties.end(); ++id)
                if (*id == parm::worldTransform().id())
                {
                    math::Affine3f xfm;

                    getBuiltIn<math::Affine3f>  (parm::worldTransform(), xfm, &parms);
                    _P  = math::transformPoint  (xfm, math::Vector4f::UnitW());
                    _D  = math::transformVector (xfm, -(-math::Vector4f::UnitZ())); // forward = -Z, but negative direction
                    math::normalize3(_D);
                }
                else if (*id == parm::intensity().id())
                    getBuiltIn<math::Vector4f>(parm::intensity(), _I, &parms);
                else if (*id == parm::minAngleD().id())
                {
                    float angD = 0.0f;

                    getBuiltIn<float>(parm::minAngleD(), angD, &parms);
                    _cAngleMin = math::cos(0.5f * math::deg2rad(angD));
                    _rAngleRange = -1.0f;
                }
                else if (*id == parm::maxAngleD().id())
                {
                    float angD = 0.0f;

                    getBuiltIn<float>(parm::maxAngleD(), angD, &parms);
                    _cAngleMax = math::cos(0.5f * math::deg2rad(angD));
                    _rAngleRange = -1.0f;
                }

            if (_rAngleRange < 0.0f &&
                _cAngleMax < _cAngleMin)
                _rAngleRange = math::rcp(_cAngleMin - _cAngleMax);

            FLIRT_LOG(LOG(DEBUG)
                << "\n-----------"
                << "\nspot light ID = " << scnId() << "\n"
                << "\n\t- world position = " << _P.transpose()
                << "\n\t- world (negative) direction = " << _D.transpose()
                << "\n\t- intensity = " << _I.transpose()
                << "\n\t- cos(min angle) = " << _cAngleMin
                << "\n\t- cos(max angle) = " << _cAngleMax
                << "\n-----------";)
        }

        inline
        void
        dispose()
        {
            float           angD = 0.0f;
            math::Affine3f  xfm;

            getBuiltIn<float>(parm::minAngleD(), angD);
            _cAngleMin = math::cos(0.5f * math::deg2rad(angD));

            getBuiltIn<float>(parm::maxAngleD(), angD);
            _cAngleMax = math::cos(0.5f * math::deg2rad(angD));

            getBuiltIn<math::Vector4f>(parm::intensity(), _I);
            getBuiltIn<math::Affine3f>(parm::worldTransform(), xfm);
            _P  = math::transformPoint  (xfm, math::Vector4f::UnitW());
            _D  = math::transformVector (xfm, -(-math::Vector4f::UnitZ())); // forward = -Z, but negative direction
            math::normalize3(_D);
            //---
            _rAngleRange = _cAngleMax < _cAngleMin
                ? math::rcp(_cAngleMin - _cAngleMax)
                : -1.0f;
            //---
            Light::dispose();
        }

        inline
        bool
        ready() const
        {
            return math::max_xyz(_I.cwiseAbs()) > 0.0f;
        }
    };
}
}
