// ========================================================================= //
// Modification copyright 2016-2018 SUPAMONKS_STUDIO                         //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
// _________________________________________________________________________ //
// Copyright 2009-2013 Intel Corporation                                     //
//                                                                           //
// Licensed under the Apache License, Version 2.0 (the "License");           //
// you may not use this file except in compliance with the License.          //
// You may obtain a copy of the License at                                   //
//                                                                           //
//     http://www.apache.org/licenses/LICENSE-2.0                            //
//                                                                           //
// Unless required by applicable law or agreed to in writing, software       //
// distributed under the License is distributed on an "AS IS" BASIS,         //
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  //
// See the License for the specific language governing permissions and       //
// limitations under the License.                                            //
//                                                                           //
// ========================================================================= //
#pragma once

#include <smks/math/math.hpp>
#include <smks/math/matrixAlgo.hpp>
#include "Light.hpp"

namespace smks { namespace rndr
{
    class DirectionalLight:
        public Light
    {
        VALID_RTOBJECT
    private:
        math::Vector4f  _wo;    //!< *negative* light direction
        math::Vector4f  _E;     //!< Irradiance (W/m^2)

        CREATE_RTOBJECT(DirectionalLight, Light)
    protected:
        DirectionalLight(unsigned int scnId, const CtorArgs& args):
            Light   (scnId, args),
            _wo     (math::Vector4f::Zero()),
            _E      (math::Vector4f::Zero())
        {
            dispose();
        }
    public:
        ~DirectionalLight()
        {
            dispose();
        }

        __forceinline
        math::Vector4f
        sample(RTCScene, const DifferentialGeometry&, Sample<math::Vector4f>& wi, float& tmax, const math::Vector2f&) const
        {
            wi.value    = _wo;
            wi.pdf      = 1.0f;
            tmax        = static_cast<float>(sys::inf);

            return _E;
        }

        __forceinline
        float
        pdf(const DifferentialGeometry&, const math::Vector4f&) const
        {
            return 0.0f;
        }

        inline
        void
        commitFrameState(const parm::Container& parms, const xchg::IdSet& dirties)
        {
            Light::commitFrameState(parms, dirties);
            //---
            for (xchg::IdSet::const_iterator id = dirties.begin(); id != dirties.end(); ++id)
                if (*id == parm::worldTransform().id())
                {
                    math::Affine3f xfm;

                    getBuiltIn<math::Affine3f>(parm::worldTransform(),  xfm, &parms);
                    _wo = math::transformVector(xfm, -(-math::Vector4f::UnitZ())); // forward = -Z, but negative direction
                    math::normalize3(_wo);
                }
                else if (*id == parm::irradiance().id())
                    getBuiltIn<math::Vector4f>(parm::irradiance(), _E, &parms);

            FLIRT_LOG(LOG(DEBUG)
                << "\n-----------"
                << "\ndirectional light ID = " << scnId() << "\n"
                << "\n\t- world direction = " << _wo.transpose()
                << "\n\t- irradiance = " << _E.transpose()
                << "\n-----------";)
        }

        inline
        void
        dispose()
        {
            math::Affine3f xfm;

            getBuiltIn<math::Vector4f>(parm::irradiance(),      _E);
            getBuiltIn<math::Affine3f>(parm::worldTransform(),  xfm);
            _wo = math::transformVector(xfm, -(-math::Vector4f::UnitZ()));  // forward = -Z, but negative direction
            math::normalize3(_wo);
            //---
            Light::dispose();
        }

        inline
        bool
        ready() const
        {
            return math::max_xyz(_E.cwiseAbs()) > 0.0f;
        }
    };
}
}
