// ========================================================================= //
// Modification copyright 2016-2018 SUPAMONKS_STUDIO                         //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
// _________________________________________________________________________ //
// Copyright 2009-2013 Intel Corporation                                     //
//                                                                           //
// Licensed under the Apache License, Version 2.0 (the "License");           //
// you may not use this file except in compliance with the License.          //
// You may obtain a copy of the License at                                   //
//                                                                           //
//     http://www.apache.org/licenses/LICENSE-2.0                            //
//                                                                           //
// Unless required by applicable law or agreed to in writing, software       //
// distributed under the License is distributed on an "AS IS" BASIS,         //
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  //
// See the License for the specific language governing permissions and       //
// limitations under the License.                                            //
//                                                                           //
// ========================================================================= //
#pragma once

#include <smks/math/math.hpp>
#include "EnvironmentLight.hpp"
#include "../sampler/ShapeSampler.hpp"

namespace smks { namespace rndr
{
    class AmbientLight:
        public EnvironmentLight
    {
        VALID_RTOBJECT
    private:
        math::Vector4f _L;  //!< Radiance (W/(m^2*sr))

        CREATE_RTOBJECT(AmbientLight, Light)
    protected:
        AmbientLight(unsigned int scnId, const CtorArgs& args):
            EnvironmentLight(scnId, args),
            _L              (math::Vector4f::Zero())
        {
            dispose();
        }
    public:
        inline
        ~AmbientLight()
        {
            dispose();
        }

        __forceinline
        math::Vector4f
        Le(const math::Vector4f&) const
        {
            return _L;
        }

        __forceinline
        math::Vector4f
        eval(const DifferentialGeometry&, const math::Vector4f&) const
        {
            return _L;
        }

        __forceinline
        math::Vector4f
        sample(RTCScene, const DifferentialGeometry& dg, Sample<math::Vector4f>& wi, float& tmax, const math::Vector2f& s) const
        {
            wi      = cosineSampleHemisphere(s.x(), s.y(), dg.Ns);
            tmax    = static_cast<float>(sys::inf);
            assert(math::dot3(wi.value, dg.Ns) > 0.0f);
            return _L;
        }

        __forceinline
        float
        pdf(const DifferentialGeometry& dg, const math::Vector4f& wi) const
        {
            return cosineSampleHemispherePDF(wi, dg.Ns);
        }

        inline
        void
        commitFrameState(const parm::Container& parms, const xchg::IdSet& dirties)
        {
            Light::commitFrameState(parms, dirties);
            //---
            for (xchg::IdSet::const_iterator id = dirties.begin(); id != dirties.end(); ++id)
                if (*id == parm::radiance().id())
                    getBuiltIn<math::Vector4f>(parm::radiance(), _L, &parms);

            FLIRT_LOG(LOG(DEBUG)
                << "\n-----------"
                << "\nambient light ID = " << scnId() << "\n"
                << "\n\t- radiance = " << _L.transpose()
                << "\n-----------";)
        }

        inline
        void
        dispose()
        {
            getBuiltIn<math::Vector4f>(parm::radiance(), _L);
            //---
            Light::dispose();
        }

        inline
        bool
        ready() const
        {
            return math::max_xyz(_L.cwiseAbs()) > 0.0f;
        }
    };
}
}
