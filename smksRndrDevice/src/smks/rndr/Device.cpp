// ========================================================================= //
// Modification copyright 2016-2018 SUPAMONKS_STUDIO                         //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
// _________________________________________________________________________ //
// Copyright 2009-2013 Intel Corporation                                     //
//                                                                           //
// Licensed under the Apache License, Version 2.0 (the "License");           //
// you may not use this file except in compliance with the License.          //
// You may obtain a copy of the License at                                   //
//                                                                           //
//     http://www.apache.org/licenses/LICENSE-2.0                            //
//                                                                           //
// Unless required by applicable law or agreed to in writing, software       //
// distributed under the License is distributed on an "AS IS" BASIS,         //
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  //
// See the License for the specific language governing permissions and       //
// limitations under the License.                                            //
//                                                                           //
// ========================================================================= //
#include <smks/sys/Exception.hpp>
#include <smks/sys/Log.hpp>
#include <smks/sys/TaskScheduler.hpp>

#include <smks/parm/BuiltInMap.hpp>
#include <smks/xchg/NodeCode.hpp>
#include <smks/xchg/NodeCodes.hpp>
#include <smks/xchg/DataStream.hpp>
#include <std/to_string_xchg.hpp>
#include <smks/ClientBase.hpp>
#include <smks/img/OutputImage.hpp>

#include "../sys/FullHandle.hpp"
#include "Device.hpp"
#include "ObjectBase.hpp"
#include "InvalidObject.hpp"

#include "api/DefaultScene.hpp"
#include "api/Selection.hpp"
#include "camera/DepthOfFieldCamera.hpp"
#include "shape/TriangleMesh.hpp"
#include "shape/HairGeometry.hpp"
#include "shape/SubdivisionMesh.hpp"
#include "light/AmbientLight.hpp"
#include "light/PointLight.hpp"
#include "light/DirectionalLight.hpp"
#include "light/DistantLight.hpp"
#include "light/SpotLight.hpp"
#include "light/HDRILight.hpp"
#include "light/MeshLight.hpp"
#include "material/BasicHair.hpp"
#include "material/BrushedMetal.hpp"
#include "material/Dielectric.hpp"
#include "material/ThinDielectric.hpp"
#include "material/Metal.hpp"
#include "material/MetallicPaint.hpp"
#include "material/Matte.hpp"
#include "material/OBJ.hpp"
#include "material/Mirror.hpp"
#include "material/Plastic.hpp"
#include "material/Translucent.hpp"
#include "material/TranslucentSS.hpp"
#include "material/Velvet.hpp"
#include "material/MicrofacetMaterial.hpp"
#include "material/EyeMaterial.hpp"
#include "material/BlendMaterial.hpp"
#include "subsurface/SubSurface.hpp"
#include "texture/NearestTexture.hpp"
#include "renderer/DefaultRenderer.hpp"
#include "api/DefaultFrameBuffer.hpp"
#include "renderer/FlatRenderPass.hpp"
#include "renderer/AmbientOcclusionPass.hpp"
#include "renderer/LightPathExpressionPass.hpp"
#include "renderer/ShapeIdCoveragePass.hpp"
#include "integrator/DefaultIntegrator.hpp"
#include "sampler/MultijitteredSamplerFactory.hpp"
#include "filter/BoxFilter.hpp"
#include "filter/BSplineFilter.hpp"
#include "tonemapper/GammaCorrectionToneMapper.hpp"
#include "tonemapper/OpenColorIOMapper.hpp"
#include "tonemapper/FilmicMapper.hpp"
#include "denoiser/RayHistoFusionDenoiser.hpp"
#include "../parm/Getters.hpp"

#define LOCK_MUTEX sys::sync::Lock<sys::sync::MutexSys> lock(_mutex);

//-----------------------------------------------------------------------------------------
#define IMPL_SETTER(_CTYPE_, _SETTER_)                                                      \
    unsigned int                                                                            \
    Device::_SETTER_(Handle hdl,                                                            \
                              const char* name,                                             \
                              const _CTYPE_& value)                                         \
    {                                                                                       \
        sys::sync::Lock<sys::sync::MutexSys> lock(_mutex);                                  \
                                                                                            \
        if (hdl == nullptr)                                                                 \
        {                                                                                   \
            std::stringstream sstr;                                                         \
            sstr << "Failed to set parameter '" << name << "': invalid handle.";            \
            throw sys::Exception(sstr.str().c_str(), "Rendering Device Parameter Setter");  \
        }                                                                                   \
                                                                                            \
        _CTYPE_                     validValue  = value;                                    \
        parm::BuiltIn::Ptr const&   builtIn     = parm::builtIns().find(name);              \
                                                                                            \
        if (builtIn &&                                                                      \
            !builtIn->validate<_CTYPE_>(value, parm::NO_PARM_FLAG, validValue))             \
        {                                                                                   \
            std::stringstream sstr;                                                         \
            sstr << "Failed to set parameter '" << name << "': invalid value.";             \
            throw sys::Exception(sstr.str().c_str(), "Rendering Device Parameter Setter");  \
        }                                                                                   \
                                                                                            \
        parm::Container& parms = builtIn                                                    \
            ? reinterpret_cast<sys::AbstractHandle*>(hdl)->builtInParameters()              \
            : reinterpret_cast<sys::AbstractHandle*>(hdl)->userParameters();                \
        return parms.set<_CTYPE_>(name, validValue);                                        \
    }
//-----------------------------------------------------------------------------------------

namespace smks { namespace rndr
{
    IMPL_SETTER(bool,               setBool)
    IMPL_SETTER(char,               setChar)
    IMPL_SETTER(unsigned int,       setUInt)
    IMPL_SETTER(size_t,             setUInt64)
    IMPL_SETTER(math::Vector2i,     setInt2)
    IMPL_SETTER(math::Vector3i,     setInt3)
    IMPL_SETTER(math::Vector4i,     setInt4)
    IMPL_SETTER(float,              setFloat1)
    IMPL_SETTER(math::Vector2f,     setFloat2)
    IMPL_SETTER(math::Vector4f,     setFloat4)
    IMPL_SETTER(math::Affine3f,     setTransform)
    IMPL_SETTER(xchg::IdSet,        setIdSet)
    IMPL_SETTER(xchg::BoundingBox,  setBounds)
    IMPL_SETTER(xchg::DataStream,   setDataStream)
    IMPL_SETTER(xchg::Data::Ptr,    setData)
    IMPL_SETTER(xchg::ObjectPtr,    setObjectPtr)

    static
    void
    throwError(RTCError, const char*);
}
}

using namespace smks;

// static
void
rndr::throwError(RTCError code, const char* msg)
{
    if (code == RTC_NO_ERROR)
        return;

    std::stringstream sstr;
    switch (code)
    {
        case RTC_NO_ERROR:          sstr << "[NO ERROR]";           break;
        case RTC_UNKNOWN_ERROR:     sstr << "[UNKNOWN ERROR]";      break;
        case RTC_INVALID_ARGUMENT:  sstr << "[INVALID ARGUMENT]";   break;
        case RTC_INVALID_OPERATION: sstr << "[INVALID OPERATION]";  break;
        case RTC_OUT_OF_MEMORY:     sstr << "[OUT OF MEMORY]";      break;
        case RTC_UNSUPPORTED_CPU:   sstr << "[UNSUPPORTED CPU]";    break;
        case RTC_CANCELLED:         sstr << "[CANCELLED]";          break;
        default:                    sstr << "[?]";                  break;
    }
    if (strlen(msg) == 0)
        sstr << ": " << msg;

    throw sys::Exception(sstr.str().c_str(), "Embree Error");
}

////////////////////
// class Device
////////////////////
rndr::Device::Device():
    _cfg         (),
    _client      (),
    _rtcDevice   (nullptr),
    _prims       (),
    _primToScenes(),
    _sceneToPrims(),
    _mutex       ()
{ }

rndr::Device::~Device()
{
    dispose();
}

//////////////////////////
// initialization
//////////////////////////
void
rndr::Device::build(const char*             jsonCfg,
                    ClientBase::Ptr const&  client)
{
    dispose();

    _cfg        = Config(jsonCfg);
    _client     = client;
    _rtcDevice  = rtcNewDevice(_cfg.rtCoreConfig.c_str());

    rtcDeviceSetErrorFunction   (_rtcDevice, throwError);
    rtcDeviceSetParameter1i     (_rtcDevice, RTC_SOFTWARE_CACHE_SIZE, 100 * 1024 * 1024);
    sys::TaskScheduler::create  (_cfg.numThreads);

    FLIRT_LOG(LOG(DEBUG)
        << "Rendering device created (ptr= " << this << "):\n"
        << "\t- # threads = " << _cfg.numThreads << "\n"
        << "\t- RT core config = '" << _cfg.rtCoreConfig << "'";)
}

void
rndr::Device::dispose()
{
    sys::TaskScheduler::destroy();

    _prims.clear();
    _primToScenes.clear();
    _sceneToPrims.clear();

    if (_rtcDevice)
        rtcDeleteDevice(_rtcDevice);
    _rtcDevice = nullptr;

    _client.reset();
    _cfg = Config();
}

///////////////////////
// scene object handles
///////////////////////
unsigned int
rndr::Device::getScnId(Handle hdl) const
{
    const sys::AbstractHandle* handle =
        reinterpret_cast<const sys::AbstractHandle*>(hdl);
    return handle ? handle->scnId() : 0;
}

rndr::AbstractDevice::SceneHandle
rndr::Device::newScene(const char* type, unsigned int scnId)
{
    LOCK_MUTEX
    //---
    if (scnId == 0)
        throw sys::Exception(
            "Invalid scene ID.",
            "New Scene From Rendering Device");

    SceneHandle ret = reinterpret_cast<SceneHandle>(
        new sys::FullHandle<DefaultScene, Scene>(scnId, _client));

    FLIRT_LOG(LOG(DEBUG)
        << "SceneHandle of type '" << type << "' created (ptr= " << ret << ")"
        << "\t" << (getInstance<Scene>(ret) ? "valid" : "INVALID");)

    return ret;
}

rndr::AbstractDevice::CameraHandle
rndr::Device::newCamera(const char* type, unsigned int scnId)
{
    LOCK_MUTEX
    //---
    if (scnId == 0)
        throw sys::Exception(
            "Invalid scene ID.",
            "New Camera From Rendering Device");

    CameraHandle        ret     = nullptr;
    const unsigned int  code    = util::string::getId(type);

    if (code == xchg::code::pinhole().id())
        ret = reinterpret_cast<CameraHandle>(
            new sys::FullHandle<PinHoleCamera, Camera>(scnId, _client));
    else if (code == xchg::code::depthOfField().id())
        ret = reinterpret_cast<CameraHandle>(
            new sys::FullHandle<DepthOfFieldCamera, Camera>(scnId, _client));
    else
    {
        std::stringstream sstr;
        sstr << "Unrecognized code '" << type << "'.";
        throw sys::Exception(sstr.str().c_str(), "New Camera From Rendering Device");
    }

    FLIRT_LOG(LOG(DEBUG)
        << "CameraHandle of type '" << type << "' created (ptr= " << ret << ")"
        << "\t" << (getInstance<Camera>(ret) ? "valid" : "INVALID");)

    return ret;
}

rndr::AbstractDevice::MaterialHandle
rndr::Device::newMaterial(const char* type, unsigned int scnId)
{
    LOCK_MUTEX
    //---
    if (scnId == 0)
        throw sys::Exception(
            "Invalid scene ID.",
            "New Material From Rendering Device");

    MaterialHandle ret = reinterpret_cast<MaterialHandle>(
        new sys::FullHandle<InvalidObject, ObjectBase>(scnId, _client));
    const unsigned int code = util::string::getId(type);

    if (code == xchg::code::brushedMetal().id())
        ret = reinterpret_cast<MaterialHandle>(
            new sys::FullHandle<BrushedMetal, Material>(scnId, _client));
    else if (code == xchg::code::metal().id())
        ret = reinterpret_cast<MaterialHandle>(
            new sys::FullHandle<Metal, Material>(scnId, _client));
    else if (code == xchg::code::metallicPaint().id())
        ret = reinterpret_cast<MaterialHandle>(
            new sys::FullHandle<MetallicPaint, Material>(scnId, _client));
    else if (code == xchg::code::dielectric().id() ||
             code == xchg::code::glass().id())
        ret = reinterpret_cast<MaterialHandle>(
            new sys::FullHandle<Dielectric, Material>(scnId, _client));
    else if (code == xchg::code::thinDielectric().id() ||
             code == xchg::code::thinGlass().id())
        ret = reinterpret_cast<MaterialHandle>(
            new sys::FullHandle<ThinDielectric, Material>(scnId, _client));
    else if (code == xchg::code::matte().id())
        ret = reinterpret_cast<MaterialHandle>(
            new sys::FullHandle<Matte, Material>(scnId, _client));
    else if (code == xchg::code::plastic().id())
        ret = reinterpret_cast<MaterialHandle>(
            new sys::FullHandle<Plastic, Material>(scnId, _client));
    else if (code == xchg::code::mirror().id())
        ret = reinterpret_cast<MaterialHandle>(
            new sys::FullHandle<Mirror, Material>(scnId, _client));
    else if (code == xchg::code::velvet().id())
        ret = reinterpret_cast<MaterialHandle>(
            new sys::FullHandle<Velvet, Material>(scnId, _client));
    else if (code == xchg::code::translucent().id())
        ret = reinterpret_cast<MaterialHandle>(
            new sys::FullHandle<Translucent, Material>(scnId, _client));
    else if (code == xchg::code::translucentSS().id())
        ret = reinterpret_cast<MaterialHandle>(
            new sys::FullHandle<TranslucentSS, Material>(scnId, _client));
    else if (code == xchg::code::obj().id())
        ret = reinterpret_cast<MaterialHandle>(
            new sys::FullHandle<OBJ, Material>(scnId, _client));
    else if (code == xchg::code::basicHair().id())
        ret = reinterpret_cast<MaterialHandle>(
            new sys::FullHandle<BasicHair, Material>(scnId, _client));
    else if (code == xchg::code::eye().id())
        ret = reinterpret_cast<MaterialHandle>(
            new sys::FullHandle<EyeMaterial, Material>(scnId, _client));
    else if (code == xchg::code::microfacet().id())
        ret = reinterpret_cast<MaterialHandle>(
            new sys::FullHandle<MicrofacetMaterial, Material>(scnId, _client));
    else if (code == xchg::code::blend().id())
        ret = reinterpret_cast<MaterialHandle>(
        new sys::FullHandle<BlendMaterial, Material>(scnId, _client));
    else if (strlen(type))
    {
        std::stringstream sstr;
        sstr << "Unrecognized code '" << type << "'.";
        throw sys::Exception(sstr.str().c_str(), "New Material From Rendering Device");
    }

    FLIRT_LOG(LOG(DEBUG)
            << "MaterialHandle of type '" << type << "' created (ptr= " << ret << ")"
            << "\t" << (getInstance<Material>(ret) ? "valid" : "INVALID");)

    return ret;
}

rndr::AbstractDevice::SubSurfaceHandle
rndr::Device::newSubSurface(const char* type, unsigned int scnId)
{
    LOCK_MUTEX
    //---
    if (scnId == 0)
        throw sys::Exception(
            "Invalid scene ID.",
            "New SubSurface From Rendering Device");

    SubSurfaceHandle ret = reinterpret_cast<SubSurfaceHandle>(
        new sys::FullHandle<InvalidObject, ObjectBase>(scnId, _client));

    FLIRT_LOG(LOG(DEBUG)
        << "SubSurfaceHandle of type '" << type << "' created (ptr= " << ret << ")"
        << "\t" << (getInstance<SubSurface>(ret) ? "valid" : "INVALID");)

    return ret;
}

rndr::AbstractDevice::TextureHandle
rndr::Device::newTexture(const char* type, unsigned int scnId)
{
    LOCK_MUTEX
    //---
    if (scnId == 0)
        throw sys::Exception(
            "Invalid scene ID.",
            "New Texture From Rendering Device");

    TextureHandle ret = reinterpret_cast<TextureHandle>(
        new sys::FullHandle<InvalidObject, ObjectBase>(scnId, _client));
    const unsigned int code = util::string::getId(type);

    if (code == xchg::code::nearest().id())
        ret = reinterpret_cast<TextureHandle>(
            new sys::FullHandle<NearestTexture, Texture>(scnId, _client));
    else if (strlen(type))
    {
        std::stringstream sstr;
        sstr << "Unrecognized code '" << type << "'.";
        throw sys::Exception(sstr.str().c_str(), "New Texture From Rendering Device");
    }

    FLIRT_LOG(LOG(DEBUG)
        << "TextureHandle of type '" << type << "' created (ptr= " << ret << ")"
        << "\t" << (getInstance<Texture>(ret) ? "valid" : "INVALID");)

    return ret;
}

rndr::AbstractDevice::ShapeHandle
rndr::Device::newShape(const char* type, unsigned int scnId)
{
    LOCK_MUTEX
    //---
    if (scnId == 0)
        throw sys::Exception(
            "Invalid scene ID.",
            "New Shape From Rendering Device");

    ShapeHandle ret = nullptr;

    if (strcmp(type, "triangleMesh") == 0)
        ret = reinterpret_cast<ShapeHandle>(
            new sys::FullHandle<TriangleMesh, Shape>(scnId, _client));
    else if (strcmp(type, "hairGeometry") == 0)
        ret = reinterpret_cast<ShapeHandle>(
            new sys::FullHandle<HairGeometry, Shape>(scnId, _client));
    else if (strcmp(type, "subdivisionMesh") == 0)
        ret = reinterpret_cast<ShapeHandle>(
            new sys::FullHandle<SubdivisionMesh, Shape>(scnId, _client));
    else
    {
        std::stringstream sstr;
        sstr << "Unrecognized code '" << type << "'.";
        throw sys::Exception(sstr.str().c_str(), "New Shape From Rendering Device");
    }

    FLIRT_LOG(LOG(DEBUG)
        << "ShapeHandle of type '" << type << "' created (ptr= " << ret << ")"
        << "\t" << (getInstance<Shape>(ret) ? "valid" : "INVALID");)

    return ret;
}

rndr::AbstractDevice::LightHandle
rndr::Device::newLight(const char* type, unsigned int scnId)
{
    LOCK_MUTEX
    //---
    if (scnId == 0)
        throw sys::Exception(
            "Invalid scene ID.",
            "New Light From Rendering Device");

    LightHandle ret = reinterpret_cast<LightHandle>(
        new sys::FullHandle<InvalidObject, ObjectBase>(scnId, _client));
    const unsigned int code = util::string::getId(type);

    if (code == xchg::code::point().id())
        ret = reinterpret_cast<LightHandle>(
            new sys::FullHandle<PointLight, Light>(scnId, _client));
    else if (code == xchg::code::directional().id())
        ret = reinterpret_cast<LightHandle>(
            new sys::FullHandle<DirectionalLight, Light>(scnId, _client));
    else if (code == xchg::code::ambient().id())
        ret = reinterpret_cast<LightHandle>(
            new sys::FullHandle<AmbientLight, Light>(scnId, _client));
    else if (code == xchg::code::distant().id())
        ret = reinterpret_cast<LightHandle>(
            new sys::FullHandle<DistantLight, Light>(scnId, _client));
    else if (code == xchg::code::spot().id())
        ret = reinterpret_cast<LightHandle>(
            new sys::FullHandle<SpotLight, Light>(scnId, _client));
    else if (code == xchg::code::hdri().id())
        ret = reinterpret_cast<LightHandle>(
            new sys::FullHandle<HDRILight, Light>(scnId, _client));
    else if (code == xchg::code::mesh().id())
        ret = reinterpret_cast<LightHandle>(
            new sys::FullHandle<MeshLight, Light>(scnId, _client));
    else if (strlen(type))
    {
        std::stringstream sstr;
        sstr << "Unrecognized code '" << type << "'.";
        throw sys::Exception(sstr.str().c_str(), "New Light From Rendering Device");
    }

    FLIRT_LOG(LOG(DEBUG)
        << "LightHandle of type '" << type << "' created (ptr= " << ret << ")"
        << "\t" << (getInstance<Light>(ret) ? "valid" : "INVALID");)
    return ret;
}

rndr::AbstractDevice::SelectionHandle
rndr::Device::newSelection(const char* type, unsigned int scnId)
{
    LOCK_MUTEX
    //---
    if (scnId == 0)
        throw sys::Exception(
            "Invalid scene ID.",
            "New Selection From Rendering Device");

    SelectionHandle ret = reinterpret_cast<SelectionHandle>(
        new sys::FullHandle<InvalidObject, ObjectBase>(scnId, _client));

    FLIRT_LOG(LOG(DEBUG)
        << "SelectionHandle of type '" << type << "' created (ptr= " << ret << ")"
        << "\t" << (getInstance<Selection>(ret) ? "valid" : "INVALID");)

    return ret;
}

rndr::AbstractDevice::RenderPassHandle
rndr::Device::newRenderPass(const char* type, unsigned int scnId)
{
    LOCK_MUTEX
    //---
    if (scnId == 0)
        throw sys::Exception(
            "Invalid scene ID.",
            "New RenderPass From Rendering Device");

    RenderPassHandle ret = reinterpret_cast<RenderPassHandle>(
        new sys::FullHandle<InvalidObject, ObjectBase>(scnId, _client));
    const unsigned int code = util::string::getId(type);

    if (code == xchg::code::lightPath().id())
        ret = reinterpret_cast<RenderPassHandle>(
            new sys::FullHandle<LightPathExpressionPass, RenderPass>(scnId, _client));
    else if (code == xchg::code::idCoverage().id())
        ret = reinterpret_cast<RenderPassHandle>(
            new sys::FullHandle<ShapeIdCoveragePass, RenderPass>(scnId, _client));
    else if (code == xchg::code::ambientOcclusion().id())
        ret = reinterpret_cast<RenderPassHandle>(
            new sys::FullHandle<AmbientOcclusionPass, RenderPass>(scnId, _client));
    else if (code == xchg::code::position().id())
        ret = reinterpret_cast<RenderPassHandle>(
            new sys::FullHandle<FlatRenderPass<xchg::POSITION_PASS>, RenderPass>(scnId, _client));
    else if (code == xchg::code::eyePosition().id())
        ret = reinterpret_cast<RenderPassHandle>(
            new sys::FullHandle<FlatRenderPass<xchg::EYE_POSITION_PASS>, RenderPass>(scnId, _client));
    else if (code == xchg::code::normal().id())
        ret = reinterpret_cast<RenderPassHandle>(
            new sys::FullHandle<FlatRenderPass<xchg::NORMAL_PASS>, RenderPass>(scnId, _client));
    else if (code == xchg::code::eyeNormal().id())
        ret = reinterpret_cast<RenderPassHandle>(
            new sys::FullHandle<FlatRenderPass<xchg::EYE_NORMAL_PASS>, RenderPass>(scnId, _client));
    else if (code == xchg::code::tangent().id())
        ret = reinterpret_cast<RenderPassHandle>(
            new sys::FullHandle<FlatRenderPass<xchg::TANGENT_PASS>, RenderPass>(scnId, _client));
    else if (code == xchg::code::eyeTangent().id())
        ret = reinterpret_cast<RenderPassHandle>(
            new sys::FullHandle<FlatRenderPass<xchg::EYE_TANGENT_PASS>, RenderPass>(scnId, _client));
    else if (code == xchg::code::texcoords().id())
        ret = reinterpret_cast<RenderPassHandle>(
            new sys::FullHandle<FlatRenderPass<xchg::TEXCOORDS_PASS>, RenderPass>(scnId, _client));
    else if (code == xchg::code::facingRatio().id())
        ret = reinterpret_cast<RenderPassHandle>(
            new sys::FullHandle<FlatRenderPass<xchg::FACING_RATIO_PASS>, RenderPass>(scnId, _client));
    else if (code == xchg::code::subsurface().id())
        ret = reinterpret_cast<RenderPassHandle>(
            new sys::FullHandle<FlatRenderPass<xchg::SUBSURFACE_PASS>, RenderPass>(scnId, _client));
    else if (code == xchg::code::shadows().id())
        ret = reinterpret_cast<RenderPassHandle>(
            new sys::FullHandle<FlatRenderPass<xchg::SHADOWS_PASS>, RenderPass>(scnId, _client));
    else if (code == xchg::code::shadowBias().id())
        ret = reinterpret_cast<RenderPassHandle>(
            new sys::FullHandle<FlatRenderPass<xchg::SHADOW_BIAS_PASS>, RenderPass>(scnId, _client));
    else if (code == xchg::code::diffuseColor().id())
        ret = reinterpret_cast<RenderPassHandle>(
            new sys::FullHandle<FlatRenderPass<xchg::DIFFUSE_COLOR_PASS>, RenderPass>(scnId, _client));
    else if (code == xchg::code::glossyColor().id())
        ret = reinterpret_cast<RenderPassHandle>(
            new sys::FullHandle<FlatRenderPass<xchg::GLOSSY_COLOR_PASS>, RenderPass>(scnId, _client));
    else if (code == xchg::code::singularColor().id())
        ret = reinterpret_cast<RenderPassHandle>(
            new sys::FullHandle<FlatRenderPass<xchg::SINGULAR_COLOR_PASS>, RenderPass>(scnId, _client));
#if defined(_DEBUG)
    else if (code == xchg::code::debugRayTexcoords().id())
        ret = reinterpret_cast<RenderPassHandle>(
            new sys::FullHandle<FlatRenderPass<xchg::DEBUG_RAY_TEXCOORDS_PASS>, RenderPass>(scnId, _client));
    else if (code == xchg::code::debugRayGeomNormal().id())
        ret = reinterpret_cast<RenderPassHandle>(
            new sys::FullHandle<FlatRenderPass<xchg::DEBUG_RAY_GEOM_NORMAL_PASS>, RenderPass>(scnId, _client));
    else if (code == xchg::code::debugHitGeomNormal().id())
        ret = reinterpret_cast<RenderPassHandle>(
            new sys::FullHandle<FlatRenderPass<xchg::DEBUG_HIT_GEOM_NORMAL_PASS>, RenderPass>(scnId, _client));
    else if (code == xchg::code::debugHitShadNormal().id())
        ret = reinterpret_cast<RenderPassHandle>(
            new sys::FullHandle<FlatRenderPass<xchg::DEBUG_HIT_SHAD_NORMAL_PASS>, RenderPass>(scnId, _client));
#endif // defined(_DEBUG)
    else if (strlen(type))
    {
        std::stringstream sstr;
        sstr << "Unrecognized code '" << type << "'.";
        throw sys::Exception(sstr.str().c_str(), "New RenderPass From Rendering Device");
    }

    FLIRT_LOG(LOG(DEBUG)
        << "RenderPassHandle of type '" << type << "' created (ptr= " << ret << ")"
        << "\t" << (getInstance<RenderPass>(ret) ? "valid" : "INVALID");)

    return ret;
}

rndr::AbstractDevice::RendererHandle
rndr::Device::newRenderer(const char* type, unsigned int scnId)
{
    LOCK_MUTEX
    //---
    if (scnId == 0)
        throw sys::Exception(
            "Invalid scene ID.",
            "New Renderer From Rendering Device");

    RendererHandle ret = reinterpret_cast<RendererHandle>(
        new sys::FullHandle<InvalidObject, ObjectBase>(scnId, _client));
    const unsigned int code = util::string::getId(type);

    if (code == xchg::code::default().id())
        ret = reinterpret_cast<RendererHandle>(
            new sys::FullHandle<DefaultRenderer, Renderer>(scnId, _client));
    else if (strlen(type))
    {
        std::stringstream sstr;
        sstr << "Unrecognized code '" << type << "'.";
        throw sys::Exception(sstr.str().c_str(), "New Renderer From Rendering Device");
    }

    FLIRT_LOG(LOG(DEBUG)
        << "RendererHandle of type '" << type << "' created (ptr= " << ret << ")"
        << "\t" << (getInstance<Renderer>(ret) ? "valid" : "INVALID");)

    return ret;
}

rndr::AbstractDevice::FrameBufferHandle
rndr::Device::newFrameBuffer(const char* type, unsigned int scnId)
{
    LOCK_MUTEX
    //---
    if (scnId == 0)
        throw sys::Exception(
            "Invalid scene ID.",
            "New Framebuffer From Rendering Device");

    FrameBufferHandle ret = reinterpret_cast<FrameBufferHandle>(
        new sys::FullHandle<InvalidObject, ObjectBase>(scnId, _client));
    const unsigned int code = util::string::getId(type);

    if (code == xchg::code::default().id())
        ret = reinterpret_cast<FrameBufferHandle>(
            new sys::FullHandle<DefaultFrameBuffer, FrameBuffer>(scnId, _client));
    else if (strlen(type))
    {
        std::stringstream sstr;
        sstr << "Unrecognized code '" << type << "'.";
        throw sys::Exception(sstr.str().c_str(), "New Framebuffer From Rendering Device");
    }

    FLIRT_LOG(LOG(DEBUG)
        << "FrameBufferHandle of type '" << type << "' created (ptr= " << ret << ")"
        << "\t" << (getInstance<FrameBuffer>(ret) ? "valid" : "INVALID");)

    return ret;
}

rndr::AbstractDevice::IntegratorHandle
rndr::Device::newIntegrator(const char* type, unsigned int scnId)
{
    LOCK_MUTEX
    //---
    if (scnId == 0)
        throw sys::Exception(
            "Invalid scene ID.",
            "New Integrator From Rendering Device");

    IntegratorHandle ret = reinterpret_cast<IntegratorHandle>(
        new sys::FullHandle<InvalidObject, ObjectBase>(scnId, _client));
    const unsigned int code = util::string::getId(type);

    if (code == xchg::code::default().id())
        ret = reinterpret_cast<IntegratorHandle>(
            new sys::FullHandle<DefaultIntegrator, Integrator>(scnId, _client));
    else if (strlen(type))
    {
        std::stringstream sstr;
        sstr << "Unrecognized code '" << type << "'.";
        throw sys::Exception(sstr.str().c_str(), "New Integrator From Rendering Device");
    }

    FLIRT_LOG(LOG(DEBUG)
        << "IntegratorHandle of type '" << type << "' created (ptr= " << ret << ")"
        << "\t" << (getInstance<Integrator>(ret) ? "valid" : "INVALID");)

    return ret;
}

rndr::AbstractDevice::SamplerFactoryHandle
rndr::Device::newSamplerFactory(const char* type, unsigned int scnId)
{
    LOCK_MUTEX
    //---
    if (scnId == 0)
        throw sys::Exception(
            "Invalid scene ID.",
            "New SamplerFactory From Rendering Device");

    SamplerFactoryHandle ret = reinterpret_cast<SamplerFactoryHandle>(
        new sys::FullHandle<InvalidObject, ObjectBase>(scnId, _client));
    const unsigned int code = util::string::getId(type);

    if (code == xchg::code::multijittered().id())
        ret = reinterpret_cast<SamplerFactoryHandle>(
            new sys::FullHandle<MultijitteredSamplerFactory, SamplerFactory>(scnId, _client));
    else if (strlen(type))
    {
        std::stringstream sstr;
        sstr << "Unrecognized code '" << type << "'.";
        throw sys::Exception(sstr.str().c_str(), "New SamplerFactory From Rendering Device");
    }

    FLIRT_LOG(LOG(DEBUG)
        << "SamplerFactoryHandle of type '" << type << "' created (ptr= " << ret << ")"
        << "\t" << (getInstance<SamplerFactory>(ret) ? "valid" : "INVALID");)

    return ret;
}

rndr::AbstractDevice::DenoiserHandle
rndr::Device::newDenoiser(const char* type, unsigned int scnId)
{
    LOCK_MUTEX
    //---
    if (scnId == 0)
        throw sys::Exception(
            "Invalid scene ID.",
            "New Denoiser From Rendering Device");

    DenoiserHandle ret = reinterpret_cast<DenoiserHandle>(
        new sys::FullHandle<InvalidObject, ObjectBase>(scnId, _client));
    const unsigned int code = util::string::getId(type);

    if (code == xchg::code::rayHistoFusion().id())
        ret = reinterpret_cast<DenoiserHandle>(
            new sys::FullHandle<RayHistoFusionDenoiser, Denoiser>(scnId, _client));
    else if (strlen(type))
    {
        std::stringstream sstr;
        sstr << "Unrecognized code '" << type << "'.";
        throw sys::Exception(sstr.str().c_str(), "New Denoiser From Rendering Device");
    }

    FLIRT_LOG(LOG(DEBUG)
        << "DenoiserHandle of type '" << type << "' created (ptr= " << ret << ")"
        << "\t" << (getInstance<Denoiser>(ret) ? "valid" : "INVALID");)

    return ret;
}

rndr::AbstractDevice::ToneMapperHandle
rndr::Device::newToneMapper(const char* type, unsigned int scnId)
{
    LOCK_MUTEX
    //---
    if (scnId == 0)
        throw sys::Exception(
            "Invalid scene ID.",
            "New Tonemapper From Rendering Device");

    ToneMapperHandle ret = reinterpret_cast<ToneMapperHandle>(
        new sys::FullHandle<InvalidObject, ObjectBase>(scnId, _client));
    const unsigned int code = util::string::getId(type);

    if (code == xchg::code::gammaCorrection().id())
        ret = reinterpret_cast<ToneMapperHandle>(
            new sys::FullHandle<GammaCorrectionToneMapper, ToneMapper>(scnId, _client));
    else if (code == xchg::code::ocio().id())
        ret = reinterpret_cast<ToneMapperHandle>(
            new sys::FullHandle<OpenColorIOMapper, ToneMapper>(scnId, _client));
    else if (code == xchg::code::filmic().id())
        ret = reinterpret_cast<ToneMapperHandle>(
            new sys::FullHandle<FilmicMapper, ToneMapper>(scnId, _client));
    else if (strlen(type))
    {
        std::stringstream sstr;
        sstr << "Unrecognized code '" << type << "'.";
        throw sys::Exception(sstr.str().c_str(), "New Tonemapper From Rendering Device");
    }

    FLIRT_LOG(LOG(DEBUG)
        << "ToneMapperHandle of type '" << type << "' created (ptr= " << ret << ")"
        << "\t" << (getInstance<ToneMapper>(ret) ? "valid" : "INVALID");)

    return ret;
}

rndr::AbstractDevice::PixelFilterHandle
rndr::Device::newPixelFilter(const char* type, unsigned int scnId)
{
    LOCK_MUTEX
    //---
    if (scnId == 0)
        throw sys::Exception(
            "Invalid scene ID.",
            "New PixelFilter From Rendering Device");

    PixelFilterHandle ret = reinterpret_cast<PixelFilterHandle>(
        new sys::FullHandle<InvalidObject, ObjectBase>(scnId, _client));
    const unsigned int code = util::string::getId(type);

    if (code == xchg::code::box().id())
        ret = reinterpret_cast<PixelFilterHandle>(
            new sys::FullHandle<BoxFilter, PixelFilter>(scnId, _client));
    else if (code == xchg::code::bspline().id())
        ret = reinterpret_cast<PixelFilterHandle>(
            new sys::FullHandle<BSplineFilter, PixelFilter>(scnId, _client));
    else if (strlen(type))
    {
        std::stringstream sstr;
        sstr << "Unrecognized code '" << type << "'.";
        throw sys::Exception(sstr.str().c_str(), "New PixelFilter From Rendering Device");
    }

    FLIRT_LOG(LOG(DEBUG)
        << "PixelFilterHandle of type '" << type << "' created (ptr= " << ret << ")"
        << "\t" << (getInstance<PixelFilter>(ret) ? "valid" : "INVALID");)

    return ret;
}

bool
rndr::Device::link(unsigned int parmId,
                   Handle       targetHdl,
                   Handle       sourceHdl)
{
    LOCK_MUTEX
    //---
    const size_t targetId = targetHdl
        ? reinterpret_cast<sys::AbstractHandle*>(targetHdl)->scnId()
        : 0;
    const size_t sourceId = sourceHdl
        ? reinterpret_cast<sys::AbstractHandle*>(sourceHdl)->scnId()
        : 0;

    FLIRT_LOG(LOG(DEBUG)
        << "rendering device handles linked:\n"
        << "\t- " << targetId << " --<-- " << sourceId << "\n"
        << "\t- parm ID = " << parmId;)

    return false;
}

bool
rndr::Device::unlink(unsigned int   parmId,
                     Handle         targetHdl,
                     Handle         sourceHdl)
{
    LOCK_MUTEX
    //---
    const size_t targetId = targetHdl
        ? reinterpret_cast<sys::AbstractHandle*>(targetHdl)->scnId()
        : 0;
    const size_t sourceId = sourceHdl
        ? reinterpret_cast<sys::AbstractHandle*>(sourceHdl)->scnId()
        : 0;

    FLIRT_LOG(LOG(DEBUG)
        << "rendering device handles unlinked:\n"
        << "\t- " << targetId << " --x-- " << sourceId << "\n"
        << "\t- parm ID = " << parmId;)

    return false;
}

void
rndr::Device::incRef(Handle hdl)
{
    LOCK_MUTEX
    //---
    if (hdl == nullptr)
        throw sys::Exception(
            "Invalid handle.",
            "Increment Reference Counter");

    reinterpret_cast<sys::AbstractHandle*>(hdl)->refInc();
}

void
rndr::Device::decRef(Handle hdl)
{
    LOCK_MUTEX
    //---
    if (hdl == nullptr)
        throw sys::Exception(
            "Invalid handle.",
            "Decrement Reference Counter");

    reinterpret_cast<sys::AbstractHandle*>(hdl)->refDec();
}

unsigned int
rndr::Device::setInt1(Handle hdl, const char* name, const int& value)
{
    LOCK_MUTEX
    //---
    if (hdl == nullptr)
    {
        std::stringstream sstr;
        sstr << "Failed to set parameter '" << name << "': invalid handle.";
        throw sys::Exception(sstr.str().c_str(), "Rendering Device Parameter Setter");
    }

    int                         validInt    = value;
    parm::BuiltIn::Ptr const&   builtIn     = parm::builtIns().find(name);

    if (builtIn)
    {
        if (!builtIn->validate<int>(value, parm::NO_PARM_FLAG, validInt))
        {
            char validChar = static_cast<char>(value);

            if (!builtIn->validate<char>(value, parm::NO_PARM_FLAG, validChar))
            {
                std::stringstream sstr;
                sstr << "Failed to set parameter '" << name << "': failed to validate value.";
                throw sys::Exception(sstr.str().c_str(), "Rendering Device Parameter Setter");
            }
        }
    }

    parm::Container& parms = builtIn
        ? reinterpret_cast<sys::AbstractHandle*>(hdl)->builtInParameters()
        : reinterpret_cast<sys::AbstractHandle*>(hdl)->userParameters();
    return parms.set<int>(name, validInt);
}

unsigned int
rndr::Device::setString(Handle hdl, const char* name, const char* value)
{
    LOCK_MUTEX
    //---
    if (hdl == nullptr)
    {
        std::stringstream sstr;
        sstr << "Failed to set parameter '" << name << "': invalid handle.";
        throw sys::Exception(sstr.str().c_str(), "Rendering Device Parameter Setter");
    }
    if (name == nullptr)
        return 0;

    std::string                 validValue  (value);
    parm::BuiltIn::Ptr const&   builtIn     = parm::builtIns().find(name);

    if (builtIn &&
        !builtIn->validate<std::string>(value, parm::NO_PARM_FLAG, validValue))
    {
        std::stringstream sstr;
        sstr << "Failed to set parameter '" << name << "': failed to validate value.";
        throw sys::Exception(sstr.str().c_str(), "Rendering Device Parameter Setter");
    }

    parm::Container& parms = builtIn
        ? reinterpret_cast<sys::AbstractHandle*>(hdl)->builtInParameters()
        : reinterpret_cast<sys::AbstractHandle*>(hdl)->userParameters();
    return parms.set<std::string>(name, validValue);
}

void
rndr::Device::unset(Handle hdl, unsigned int parmId)
{
    LOCK_MUTEX
    //---
    if (hdl == nullptr)
        throw sys::Exception(
            "Invalid handle.",
            "Rendering Device Parameter Unsetter");

    parm::Container& parms = parm::builtIns().find(parmId)
        ? reinterpret_cast<sys::AbstractHandle*>(hdl)->builtInParameters()
        : reinterpret_cast<sys::AbstractHandle*>(hdl)->userParameters();
    parms.unset(parmId);
}

void
rndr::Device::commitFrameState(Handle hdl)
{
    LOCK_MUTEX
    //---
    if (hdl == nullptr)
        throw sys::Exception(
            "Invalid handle.",
            "Commit Rendering Device Handle's Frame State");

    reinterpret_cast<sys::AbstractHandle*>(hdl)
        ->commitFrameState();
}

void
rndr::Device::beginSubframeCommits(Handle hdl, size_t numSubframes)
{
    LOCK_MUTEX
    //---
    if (hdl == nullptr)
        throw sys::Exception(
            "Invalid handle.",
            "Begin Rendering Device Handle's Subframe Commits");

    reinterpret_cast<sys::AbstractHandle*>(hdl)
        ->beginSubframeCommits(numSubframes);
}

void
rndr::Device::commitSubframeState(Handle hdl, size_t subframe)
{
    LOCK_MUTEX
    //---
    if (hdl == nullptr)
        throw sys::Exception(
            "Invalid handle.",
            "Commit Rendering Device Handle's Subframe State");

    reinterpret_cast<sys::AbstractHandle*>(hdl)
        ->commitSubframeState(subframe);
}

void
rndr::Device::endSubframeCommits(Handle hdl)
{
    LOCK_MUTEX
    //---
    if (hdl == nullptr)
        throw sys::Exception(
            "Invalid handle.",
            "End Rendering Device Handle's Subframe Commits");

    reinterpret_cast<sys::AbstractHandle*>(hdl)
        ->endSubframeCommits();
}

bool
rndr::Device::perSubframeParameter(Handle hdl, unsigned int parmId) const
{
    return hdl
        ? reinterpret_cast<sys::AbstractHandle*>(hdl)
            ->perSubframeParameter(parmId)
        : false;
}

void
rndr::Device::dispose(Handle hdl)
{
    LOCK_MUTEX
    //---
    if (hdl == nullptr)
        throw sys::Exception(
            "Invalid handle.",
            "Dispose Rendering Device Handle");

    FLIRT_LOG(LOG(DEBUG) << "dispose Handle (ptr= " << hdl << ").");
    reinterpret_cast<sys::AbstractHandle*>(hdl)
        ->dispose();
}

void
rndr::Device::destroy(Handle hdl)
{
    LOCK_MUTEX
    //---
    if (hdl == nullptr)
        throw sys::Exception(
            "Invalid handle.",
            "Destroy Rendering Device Handle");

    FLIRT_LOG(LOG(DEBUG) << "destroy Handle (ptr= " << hdl << ").");
    dispose(hdl);
    {
        // if the handle is a scene then remove all primitives from it beforehand
        HandleToHandles::iterator const& it = _sceneToPrims.find(hdl);
        if (it != _sceneToPrims.end())
        {
            SceneHandle sceneHdl = reinterpret_cast<SceneHandle>(hdl);
            if (sceneHdl)
            {
                Handles& primHdls = it->second;
                while (!primHdls.empty())
                {
                    PrimitiveHandle primHdl = reinterpret_cast<PrimitiveHandle>(*primHdls.begin());
                    if (primHdl)
                        remove(sceneHdl, primHdl);
                    else
                        primHdls.erase(primHdls.begin());
                }
            }
            _sceneToPrims.erase(it);
        }
    }
}

rndr::AbstractDevice::PrimitiveHandle
rndr::Device::newPrimitive(const char* type)
{
    LOCK_MUTEX
    //---
    PrimitiveHandle ret = nullptr;

    if (strcmp(type, "shape") == 0)
        ret = reinterpret_cast<AbstractDevice::PrimitiveHandle>(
            new Primitive(SHAPE_PRIMITIVE, _prims));
    else if (strcmp(type, "light") == 0)
        ret = reinterpret_cast<AbstractDevice::PrimitiveHandle>(
            new Primitive(LIGHT_PRIMITIVE, _prims));
    else
    {
        std::stringstream sstr;
        sstr << "Unsupported primitive type '" << type << "'.";
        throw sys::Exception(sstr.str().c_str(), "New Primitive From Rendering Device");
    }
    FLIRT_LOG(LOG(DEBUG) << "newPrimitive of type '" << type << "' created (ptr= " << ret << ").";)
    return ret;
}

void
rndr::Device::destroy(PrimitiveHandle primHdl)
{
    LOCK_MUTEX
    //---
    if (primHdl == nullptr)
        throw sys::Exception(
            "Invalid primitive handle.",
            "Destroy Rendering Device Primitive");

    FLIRT_LOG(LOG(DEBUG) << "destroy PrimitiveHandle (ptr= " << primHdl << ").");
    Primitive* prim = reinterpret_cast<Primitive*>(primHdl);

    HandleToHandles::iterator const& it = _primToScenes.find(primHdl);
    if (it != _primToScenes.end())
    {
        Handles& sceneHdls = it->second;
        while (!sceneHdls.empty())
        {
            SceneHandle sceneHdl = reinterpret_cast<SceneHandle>(*sceneHdls.begin());
            if (sceneHdl)
                remove(sceneHdl, primHdl);
            else
                sceneHdls.erase(sceneHdls.begin());
        }
        _primToScenes.erase(it);
    }
    _prims.remove(prim);    // delete primitive
}

void
rndr::Device::add(SceneHandle       sceneHdl,
                  PrimitiveHandle   primHdl)
{
    LOCK_MUTEX
    //---
    if (sceneHdl == nullptr)
        throw sys::Exception(
            "Invalid scene handle.",
            "Remove Scene Primitive");
    if (primHdl == nullptr)
        throw sys::Exception(
            "Invalid primitive handle.",
            "Remove Scene Primitive");

    Scene::Ptr scene    = getInstance<Scene>(sceneHdl);
    Primitive* prim     = reinterpret_cast<Primitive*>(primHdl);
    assert(prim);
    if (scene)
    {
        scene->add(prim);
        _sceneToPrims[sceneHdl] .insert(primHdl);
        _primToScenes[primHdl]  .insert(sceneHdl);
    }
}

void
rndr::Device::remove(SceneHandle        sceneHdl,
                     PrimitiveHandle    primHdl)
{
    LOCK_MUTEX
    //---
    if (sceneHdl == nullptr)
        throw sys::Exception(
            "Invalid scene handle.",
            "Remove Scene Primitive");
    if (primHdl == nullptr)
        throw sys::Exception(
            "Invalid primitive handle.",
            "Remove Scene Primitive");

    Scene::Ptr  scene   = getInstance<Scene>(sceneHdl);
    Primitive*  prim    = reinterpret_cast<Primitive*>(primHdl);
    assert(prim);
    if (scene)
    {
        HandleToHandles::iterator it;

        // update the primitive->scenes map
        it  = _primToScenes.find(primHdl);
        if (it!= _primToScenes.end())
            it->second.erase(sceneHdl);
        // update the scene->primitives map
        it  = _sceneToPrims.find(sceneHdl);
        if (it != _sceneToPrims.end())
            it->second.erase(primHdl);

        scene->remove(prim);
    }
}

void
rndr::Device::setShape(PrimitiveHandle  primHdl,
                       ShapeHandle      shapeHdl)
{
    LOCK_MUTEX
    //---
    if (primHdl == nullptr)
        throw sys::Exception(
            "Invalid primitive handle.",
            "Primitive Shape Setter");

    Primitive* prim = reinterpret_cast<Primitive*>(primHdl);
    assert(prim);

    Shape::Ptr shape = getInstance<Shape>(shapeHdl);

    prim->shape(shape);
    if (prim->light() &&
        prim->light()->asAreaLight() &&
        prim->light()->asAreaLight()->asMeshLight())
        prim->light()->asAreaLight()->asMeshLight()->shape(shape);
}
void
rndr::Device::setLight(PrimitiveHandle  primHdl,
                       LightHandle      lightHdl)
{
    LOCK_MUTEX
    //---
    if (primHdl == nullptr)
        throw sys::Exception(
            "Invalid primitive handle.",
            "Primitive Shape Setter");

    Primitive* prim = reinterpret_cast<Primitive*>(primHdl);
    assert(prim);

    Light::Ptr light = getInstance<Light>(lightHdl);
    prim->light(light);
}
void
rndr::Device::setMaterial(PrimitiveHandle   primHdl,
                          MaterialHandle    materialHdl)
{
    LOCK_MUTEX
    //---
    if (primHdl == nullptr)
        throw sys::Exception(
            "Invalid primitive handle.",
            "Primitive Shape Setter");

    Primitive* prim = reinterpret_cast<Primitive*>(primHdl);
    assert(prim);

    Material::Ptr material = getInstance<Material>(materialHdl);
    prim->material(material);
}
void
rndr::Device::setSubSurface(PrimitiveHandle     primHdl,
                            SubSurfaceHandle    subsurfaceHdl)
{
    LOCK_MUTEX
    //---
    if (primHdl == nullptr)
        throw sys::Exception(
            "Invalid primitive handle.",
            "Primitive Shape Setter");

    Primitive* prim = reinterpret_cast<Primitive*>(primHdl);
    assert(prim);

    SubSurface::Ptr subSurface = getInstance<SubSurface>(subsurfaceHdl);
    prim->subSurface(subSurface);
}

void
rndr::Device::setInput(MaterialHandle   targetHdl,
                       MaterialHandle   inputHdl,
                       size_t           idx)
{
    LOCK_MUTEX
    //---
    if (targetHdl == nullptr)
        throw sys::Exception(
            "Invalid target material handle.",
            "Material Input Setter");

    Material::Ptr target    = getInstance<Material>(targetHdl);
    Material::Ptr input     = getInstance<Material>(inputHdl);

    target->setInput(input, idx);
}

void
rndr::Device::setDenoiser(FrameBufferHandle frameBufferHdl,
                          DenoiserHandle    denoiserHdl)
{
    LOCK_MUTEX
    //---
    FrameBuffer::Ptr    framebuffer = getInstance<FrameBuffer>  (frameBufferHdl);
    Denoiser::Ptr       denoiser    = getInstance<Denoiser>     (denoiserHdl); // can be null

    if (framebuffer)
        framebuffer->denoiser(denoiser);
}

void
rndr::Device::setToneMapper(FrameBufferHandle   frameBufferHdl,
                            ToneMapperHandle    toneMapperHdl)
{
    LOCK_MUTEX
    //---
    FrameBuffer::Ptr    framebuffer = getInstance<FrameBuffer>  (frameBufferHdl);
    ToneMapper::Ptr     tonemapper  = getInstance<ToneMapper>   (toneMapperHdl); // can be null

    if (framebuffer)
        framebuffer->toneMapper(tonemapper);
}

void
rndr::Device::add(FrameBufferHandle frameBufferHdl,
                  RenderPassHandle  renderPassHdl)
{
    LOCK_MUTEX
    //---
    if (frameBufferHdl == nullptr)
        throw sys::Exception(
            "Invalid framebuffer handle.",
            "Add RenderPass to FrameBuffer");
    if (renderPassHdl == nullptr)
        throw sys::Exception(
            "Invalid renderpass handle.",
            "Add RenderPass to FrameBuffer");

    FrameBuffer::Ptr    frameBuffer = getInstance<FrameBuffer>  (frameBufferHdl);
    RenderPass::Ptr     renderPass  = getInstance<RenderPass>   (renderPassHdl);

    if (frameBuffer &&
        renderPass)
        frameBuffer->add(renderPass);
}

void
rndr::Device::remove(FrameBufferHandle  frameBufferHdl,
                     RenderPassHandle   renderPassHdl)
{
    LOCK_MUTEX
    //---
    if (frameBufferHdl == nullptr)
        throw sys::Exception(
            "Invalid framebuffer handle.",
            "Remove RenderPass from FrameBuffer");
    if (renderPassHdl == nullptr)
        throw sys::Exception(
            "Invalid renderpass handle.",
            "Remove RenderPass from FrameBuffer");

    FLIRT_LOG(LOG(DEBUG)
        << "remove renderPassHdl (ptr= " << renderPassHdl << ") "
        << "from FrameBufferHandle (ptr= " << frameBufferHdl << ") .");

    FrameBuffer::Ptr    frameBuffer = getInstance<FrameBuffer>  (frameBufferHdl);
    RenderPass::Ptr     renderPass  = getInstance<RenderPass>   (renderPassHdl);

    if (frameBuffer &&
        renderPass)
        frameBuffer->remove(renderPass);
}

void
rndr::Device::setSamplerFactory(RendererHandle          rendererHdl,
                                SamplerFactoryHandle    samplerFactoryHdl)
{
    LOCK_MUTEX
    //---
    Renderer::Ptr       renderer        = getInstance<Renderer>         (rendererHdl);
    SamplerFactory::Ptr samplerFactory  = getInstance<SamplerFactory>   (samplerFactoryHdl);    // can be null

    if (renderer)
        renderer->samplerFactory(samplerFactory);
}

void
rndr::Device::setIntegrator(RendererHandle      rendererHdl,
                            IntegratorHandle    integratorHdl)
{
    LOCK_MUTEX
    //---
    Renderer::Ptr   renderer    = getInstance<Renderer>     (rendererHdl);
    Integrator::Ptr integrator  = getInstance<Integrator>   (integratorHdl); // can be null

    if (renderer)
        renderer->integrator(integrator);
}

void
rndr::Device::setFilter(RendererHandle      rendererHdl,
                        PixelFilterHandle   pixelFilterHdl)
{
    LOCK_MUTEX
    //---
    Renderer::Ptr       renderer    = getInstance<Renderer>     (rendererHdl);
    PixelFilter::Ptr    filter      = getInstance<PixelFilter>  (pixelFilterHdl); // can be null

    if (renderer)
        renderer->filter(filter);
}

void
rndr::Device::setupMaterial(MaterialHandle  materialHdl,
                            unsigned int    parmId,
                            TextureHandle   textureHdl)
{
    LOCK_MUTEX
    //---
    Material::Ptr   material    = getInstance<Material> (materialHdl);
    Texture::Ptr    texture     = getInstance<Texture>  (textureHdl); // can be null

    if (material)
        material->assignTextureToParameter(parmId, texture);
}

void
rndr::Device::setupSubSurface(SubSurfaceHandle  subsurfaceHdl,
                              unsigned int      parmId,
                              TextureHandle     textureHdl)
{
    LOCK_MUTEX
    //---
    SubSurface::Ptr subsurface  = getInstance<SubSurface>   (subsurfaceHdl);
    Texture::Ptr    texture     = getInstance<Texture>      (textureHdl); // can be null

    if (subsurface)
        subsurface->assignTextureToParameter(parmId, texture);
}

void
rndr::Device::renderFrame(RendererHandle    rendererHdl,
                          SceneHandle       sceneHdl,
                          CameraHandle      cameraHdl,
                          FrameBufferHandle frameBufferHdl,
                          size_t            numSubframes)
{
    if (numSubframes == 0)
        return;

    LOCK_MUTEX
    //---
    Renderer::Ptr       renderer    = getInstance<Renderer>     (rendererHdl);
    Scene::Ptr          scene       = getInstance<Scene>        (sceneHdl);
    Camera::Ptr         camera      = getInstance<Camera>       (cameraHdl);
    FrameBuffer::Ptr    framebuffer = getInstance<FrameBuffer>  (frameBufferHdl);

    if (!renderer)
        throw sys::Exception(
            "Invalid renderer handle.",
            "Render Frame");
    if (!scene)
        throw sys::Exception(
            "Invalid scene handle.",
            "Render Frame");
    if (!camera)
        throw sys::Exception(
            "Invalid camera handle.",
            "Render Frame");
    if (!framebuffer)
        throw sys::Exception(
            "Invalid framebuffer handle.",
            "Render Frame");

    // gather all user parameter requests from renderer, framebuffer, etc...
    parm::Getters userParmGetters;

    renderer    ->requestUserParametersFromPrimitives(userParmGetters);
    framebuffer ->requestUserParametersFromPrimitives(userParmGetters);
    scene       ->extractPrimitives(_rtcDevice, userParmGetters);

    renderer->renderFrame(camera, scene, framebuffer, numSubframes);
}

void
rndr::Device::getFrame(FrameBufferHandle frameBufferHdl,
                       img::OutputImage& frame)
{
    LOCK_MUTEX
    //---
    FrameBuffer::Ptr frameBuffer = getInstance<FrameBuffer>(frameBufferHdl);

    if (frameBuffer)
        frameBuffer->getFrame(frame);
    else
        frame.clear();
}

const float*
rndr::Device::getFrameData(FrameBufferHandle    frameBufferHdl,
                           size_t&              width,
                           size_t&              height)
{
    LOCK_MUTEX
    //---
    width   = 0;
    height  = 0;

    FrameBuffer::Ptr frameBuffer = getInstance<FrameBuffer>(frameBufferHdl);
    if (!frameBuffer)
        return nullptr;

    width   = frameBuffer->width();
    height  = frameBuffer->height();
    return frameBuffer->getFrameRGBA();
}

void
rndr::Device::getMetadata(FrameBufferHandle frameBufferHdl,
                          img::OutputImage& frame)
{
    LOCK_MUTEX
    //---
    FrameBuffer::Ptr frameBuffer = getInstance<FrameBuffer>(frameBufferHdl);

    if (frameBuffer)
        frameBuffer->getMetadata(frame);
}

void
rndr::Device::getMetadata(SceneHandle       sceneHdl,
                          img::OutputImage& frame)
{
    LOCK_MUTEX
    //---
    Scene::Ptr scene = getInstance<Scene>(sceneHdl);

    if (scene)
        scene->getMetadata(frame);
}

void
rndr::Device::getMetadata(RendererHandle    rendererHdl,
                          img::OutputImage& frame)
{
    LOCK_MUTEX
    //---
    Renderer::Ptr renderer = getInstance<Renderer>(rendererHdl);

    if (renderer)
        renderer->getMetadata(frame);
}

void
rndr::Device::getMetadata(ToneMapperHandle  toneMapperHdl,
                          img::OutputImage& frame)
{
    LOCK_MUTEX
    //---
    ToneMapper::Ptr toneMapper = getInstance<ToneMapper>(toneMapperHdl);

    if (toneMapper)
        toneMapper->getMetadata(frame);
}

void
rndr::Device::getMetadata(CameraHandle      cameraHdl,
                          img::OutputImage& frame)
{
    LOCK_MUTEX
    //---
    Camera::Ptr camera = getInstance<Camera>(cameraHdl);

    if (camera)
        camera->getMetadata(frame);
}
