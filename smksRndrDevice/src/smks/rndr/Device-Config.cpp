// ========================================================================= //
// Copyright 2016-2018 SUPAMONKS_STUDIO                                      //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
//                                                                           //
// ========================================================================= //
#include <json/json.h>
#include <smks/xchg/cfg.hpp>
#include <smks/sys/Exception.hpp>

#include "Device.hpp"

using namespace smks;

void
rndr::Device::Config::setDefaults()
{
    numThreads = 0;
    rtCoreConfig.clear();
}

rndr::Device::Config::Config()
{
    setDefaults();
}

// explicit
rndr::Device::Config::Config(const char* json)
{
    setDefaults();

    if (strlen(json) == 0)
        return;

    Json::Value  root;
    Json::Reader reader;

    if (!reader.parse(json, root))
    {
        std::stringstream sstr;
        sstr
            << "Failed to parse Json configuration: " << reader.getFormatedErrorMessages();
        throw sys::Exception(sstr.str().c_str(), "Rendering Device Configuration");
    }

    numThreads   = root.get(xchg::cfg::num_threads,    numThreads).asUInt();
    rtCoreConfig = root.get(xchg::cfg::rt_core_config, rtCoreConfig).asString();
}
