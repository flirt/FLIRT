// ========================================================================= //
// Copyright 2016-2018 SUPAMONKS_STUDIO                                      //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
//                                                                           //
// ========================================================================= //
#pragma once

#include <cassert>
#include <vector>
#include <smks/parm/Any.hpp>

namespace smks { namespace parm
{
    class Values
    {
        friend class Getters;
    public:
        typedef std::shared_ptr<Values> Ptr;
    private:
        std::vector<Any*> _data;
    public:
        static
        Ptr
        create()
        {
            Ptr ptr(new Values);
            return ptr;
        }
    public:
        Values();
        Values(const Values&);
        ~Values();
        Values& operator=(const Values&);
    private:
        void
        copy(const Values&);
    public:
        template<typename ParmType>
        bool
        get(size_t idx, ParmType& value, const ParmType& defaultValue) const
        {
            value = defaultValue;

            if (idx >= _data.size() ||
                !_data[idx] ||
                _data[idx]->type() != typeid(ParmType))
                return false;

            value = *Any::cast<ParmType>(_data[idx]);
            return true;
        }

        operator bool() const
        {
            return numValues() > 0;
        }

        size_t
        size() const
        {
            return _data.size();
        }

        size_t
        numValues() const
        {
            size_t ret = 0;
            for (size_t i = 0; i < _data.size(); ++i)
                ret += _data[i] ? 1 : 0;
            return ret;
        }

        void
        dispose();
        void
        resize(size_t);

        std::ostream&
        print(std::ostream&) const;
    };
}
}
