// ========================================================================= //
// Copyright 2016-2018 SUPAMONKS_STUDIO                                      //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
//                                                                           //
// ========================================================================= //
#include <smks/math/types.hpp>
#include <smks/xchg/IdSet.hpp>
#include <smks/xchg/DataStream.hpp>
#include <smks/xchg/BoundingBox.hpp>
#include <smks/xchg/Data.hpp>
#include <smks/xchg/ObjectPtr.hpp>
#include "Values.hpp"

using namespace smks;

parm::Values::Values():
    _data()
{ }

parm::Values::Values(const Values& other):
    _data()
{
    copy(other);
}

parm::Values&
parm::Values::operator=(const Values& other)
{
    copy(other);
    return *this;
}

parm::Values::~Values()
{
    dispose();
}

void
parm::Values::copy(const Values& other)
{
    resize(other.size());
    for (size_t i = 0; i < other._data.size(); ++i)
        if (other._data[i])
            _data[i] = new Any(*other._data[i]);
}

void
parm::Values::dispose()
{
    for (size_t i = 0; i < _data.size(); ++i)
        if (_data[i])
            delete _data[i];
    _data.clear();
}

void
parm::Values::resize(size_t value)
{
    dispose();
    _data.resize(value, nullptr);
}

std::ostream&
parm::Values::print(std::ostream& out) const
{
    out << numValues() << " user parameter value(s)";
    for (size_t i = 0; i < _data.size(); ++i)
    {
        const Any* data = _data[i];
        if (!data)
            continue;
        out << "\n\t- [" << i << "]\t";
        if      (data->type() == typeid(bool))              out << *parm::Any::cast<bool>(data);
        else if (data->type() == typeid(char))              out << static_cast<int>(*parm::Any::cast<unsigned int>(data));
        else if (data->type() == typeid(int))               out << *parm::Any::cast<int>(data);
        else if (data->type() == typeid(unsigned int))      out << *parm::Any::cast<unsigned int>(data);
        else if (data->type() == typeid(size_t))            out << *parm::Any::cast<size_t>(data);
        else if (data->type() == typeid(float))             out << *parm::Any::cast<float>(data);
        else if (data->type() == typeid(math::Vector2i))    out << "(" << parm::Any::cast<math::Vector2i>(data)->transpose() << ")";
        else if (data->type() == typeid(math::Vector3i))    out << "(" << parm::Any::cast<math::Vector3i>(data)->transpose() << ")";
        else if (data->type() == typeid(math::Vector4i))    out << "(" << parm::Any::cast<math::Vector4i>(data)->transpose() << ")";
        else if (data->type() == typeid(math::Vector2f))    out << "(" << parm::Any::cast<math::Vector2f>(data)->transpose() << ")";
        else if (data->type() == typeid(math::Vector4f))    out << "(" << parm::Any::cast<math::Vector4f>(data)->transpose() << ")";
        else if (data->type() == typeid(math::Affine3f))    out << "\n" << parm::Any::cast<math::Affine3f>(data)->matrix();
        else if (data->type() == typeid(std::string))       out << "'" << *parm::Any::cast<std::string>(data) << "'";
        else if (data->type() == typeid(xchg::IdSet))       out << *parm::Any::cast<xchg::IdSet>(data);
        else if (data->type() == typeid(xchg::DataStream))  out << *parm::Any::cast<xchg::DataStream>(data);
        else if (data->type() == typeid(xchg::BoundingBox)) out << *parm::Any::cast<xchg::BoundingBox>(data);
        else if (data->type() == typeid(xchg::ObjectPtr))   out << "object ptr";
        else if (data->type() == typeid(xchg::Data::Ptr))   out << *parm::Any::cast<xchg::Data::Ptr>(data);
        else                                                out << "??";
    }
    return out;
}
