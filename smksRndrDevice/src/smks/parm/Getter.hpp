// ========================================================================= //
// Copyright 2016-2018 SUPAMONKS_STUDIO                                      //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
//                                                                           //
// ========================================================================= //
#pragma once

#include <memory>
#include <smks/util/string/getId.hpp>
#include <smks/parm/Container.hpp>

namespace smks { namespace parm
{
    ///////////////////////////
    // class AbstractGetter
    ///////////////////////////
    class AbstractGetter
    {
    public:
        typedef std::shared_ptr<AbstractGetter> Ptr;
    public:
        virtual
        ~AbstractGetter()
        { }

        virtual
        const char*
        name() const = 0;

        virtual
        unsigned int
        id() const = 0;

        virtual
        const std::type_info&
        type() const = 0;

        virtual
        Any*
        extractFrom(const Container&) const = 0;
    };
    ///////////////////////////

    ///////////////////////////
    // class Getter<T>
    ///////////////////////////
    template <typename ParmType>
    class Getter:
        public AbstractGetter
    {
    private:
        const std::string   _name;
        const unsigned int  _id;
    public:
        static
        Ptr
        create(const char* name)
        {
            Ptr ptr(new Getter(name));
            return ptr;
        }
    protected:
        explicit
        Getter(const char* name):
        _name(name), _id(util::string::getId(name))
        { }
    public:
        const char*
        name() const
        {
            return _name.c_str();
        }

        unsigned int
        id() const
        {
            return _id;
        }

        const std::type_info&
        type() const
        {
            return typeid(ParmType);
        }

        Any*
        extractFrom(const Container& parms) const
        {
            return parms.existsAs<ParmType>(_id)
                ? new Any(parms.get<ParmType>(_id))
                : nullptr;
        }
    };
    ///////////////////////////
}
}
