// ========================================================================= //
// Copyright 2016-2018 SUPAMONKS_STUDIO                                      //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
//                                                                           //
// ========================================================================= //
#include <cassert>
#include <sstream>
#include <smks/sys/Exception.hpp>
#include <smks/sys/Log.hpp>
#include "../../std/to_string.hpp"
#include "Getters.hpp"
#include "Values.hpp"

using namespace smks;

parm::Getters::Getters():
    _getters()
{ }

void
parm::Getters::clear()
{
    _getters.clear();
}

void
parm::Getters::extractFrom(const parm::Container& parms, Values& ret) const
{
    if (ret.size() != _getters.size())
        ret.resize(_getters.size());
    for (size_t i = 0; i < _getters.size(); ++i)
        if (!ret._data[i])
            ret._data[i] = _getters[i]->extractFrom(parms);
}

size_t
parm::Getters::append(AbstractGetter::Ptr const& getter)
{
    assert(getter);
    for (size_t i = 0; i < _getters.size(); ++i)
        if (_getters[i]->id() == getter->id())
        {
            if (strcmp(_getters[i]->name(), getter->name()) ||
                _getters[i]->type() != getter->type())
            {
                std::stringstream sstr;
                sstr
                    << "Conflicting user attributes name or type between "
                    << "'" << _getters[i]->name() << "' "
                    << "(type= " << std::to_string(_getters[i]->type()) << ") and "
                    << "'" << getter->name() << "' "
                    << "(type= " << std::to_string(getter->type()) << ").";
                throw sys::Exception(sstr.str().c_str(), "User Attribute Getter Addition");
            }
            return i;
        }
    const size_t idx = _getters.size();
    _getters.push_back(getter);

    return idx;
}

std::ostream&
parm::Getters::print(std::ostream& out) const
{
    out << _getters.size() << " user parameter getter(s)";
    for (size_t i = 0; i < _getters.size(); ++i)
        if (_getters[i])
            out
                << "\n\t- [" << i << "]\t"
                << "parameter name '" << _getters[i]->name() << "' "
                << "(ID = " << _getters[i]->id() << ", "
                << "type = " << std::to_string(_getters[i]->type()) << ")";
    return out;
}
