// ========================================================================= //
// Copyright 2016-2018 SUPAMONKS_STUDIO                                      //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
//                                                                           //
// ========================================================================= //
#pragma once

#include <vector>
#include "Getter.hpp"

namespace smks { namespace parm
{
    class Container;
    class Values;

    class Getters
    {
    public:
        enum {INVALID_IDX=-1};
    private:
        std::vector<AbstractGetter::Ptr> _getters;
    public:
        Getters();

        void
        clear();

        operator bool() const
        {
            return !_getters.empty();
        }

        //<! register a new explicitly typed, user-defined parameter for getting
        template<typename ParmType>
        size_t
        append(const char* name)
        {
            return append(Getter<ParmType>::create(name));
        }
    private:
        size_t
        append(AbstractGetter::Ptr const&);
    public:
        //<! add values to unassigned requested user parameters only
        void
        extractFrom(const parm::Container&, Values&) const;

        std::ostream&
        print(std::ostream&) const;
    };
}
}
