// ========================================================================= //
// Modification copyright 2016-2018 SUPAMONKS_STUDIO                         //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
// _________________________________________________________________________ //
// Copyright 2009-2013 Intel Corporation                                     //
//                                                                           //
// Licensed under the Apache License, Version 2.0 (the "License");           //
// you may not use this file except in compliance with the License.          //
// You may obtain a copy of the License at                                   //
//                                                                           //
//     http://www.apache.org/licenses/LICENSE-2.0                            //
//                                                                           //
// Unless required by applicable law or agreed to in writing, software       //
// distributed under the License is distributed on an "AS IS" BASIS,         //
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  //
// See the License for the specific language governing permissions and       //
// limitations under the License.                                            //
//                                                                           //
// ========================================================================= //
#pragma once

#include <smks/sys/Ref.hpp>

namespace smks
{
    namespace parm
    {
        class Container;
    }

    namespace sys
    {
        class AbstractHandle:
            public RefCount
        {
        protected:
            /*! Start counting with 1, as each created handle is owned by the
            application. */
            AbstractHandle():
                RefCount(1)
            { }

        public:
            virtual
            ~AbstractHandle()
            { }

            virtual
            unsigned int
            scnId() const = 0;

            virtual
            parm::Container&
            builtInParameters() = 0;
            virtual
            parm::Container&
            userParameters() = 0;
            virtual
            const parm::Container&
            builtInParameters() const = 0;
            virtual
            const parm::Container&
            userParameters() const = 0;

            virtual
            void
            dispose() = 0;

            virtual
            bool
            perSubframeParameter(unsigned int parmId) const = 0;
            virtual
            void
            beginSubframeCommits(size_t numSubframes) = 0;
            virtual
            void
            commitSubframeState(size_t subframe) = 0;
            virtual
            void
            endSubframeCommits() = 0;

            virtual
            void
            commitFrameState() = 0;
        };
    }
}
