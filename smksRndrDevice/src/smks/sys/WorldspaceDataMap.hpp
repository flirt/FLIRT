// ========================================================================= //
// Copyright 2016-2018 SUPAMONKS_STUDIO                                      //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
//                                                                           //
// ========================================================================= //
#pragma once

#include <functional>
#include <boost/unordered_map.hpp>
#include <smks/math/types.hpp>

namespace smks { namespace sys
{
    // worldspace computations are stored in associative containers and
    // indexed by their corresponding sample index and world transform matrix.
    struct WorldspaceDataKey
    {
        unsigned int    sample;         // sample index corresponding to objectspace data
        math::Affine3f  worldTransform; // object's local-to-world transform
    };

    // equality predicate used by associative containers
    struct WorldspaceDataKeyEqual:
        public std::binary_function<WorldspaceDataKey, WorldspaceDataKey, bool>
    {
        inline
        bool
        operator()(const WorldspaceDataKey& a, const WorldspaceDataKey& b) const
        {
            return a.sample == b.sample &&
                a.worldTransform.isApprox(b.worldTransform);
        }
    };

    // hash function used by associative containers
    struct WorldspaceDataKeyHash:
        std::unary_function<WorldspaceDataKey, std::size_t>
    {
        size_t
        operator()(const WorldspaceDataKey& k) const;
    };

    typedef boost::unordered_map<WorldspaceDataKey, WorldspaceDataKeyHash, WorldspaceDataKeyEqual> WorldspaceDataMap;
}
}
