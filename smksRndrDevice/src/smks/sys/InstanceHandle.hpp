// ========================================================================= //
// Modification copyright 2016-2018 SUPAMONKS_STUDIO                         //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
// _________________________________________________________________________ //
// Copyright 2009-2013 Intel Corporation                                     //
//                                                                           //
// Licensed under the Apache License, Version 2.0 (the "License");           //
// you may not use this file except in compliance with the License.          //
// You may obtain a copy of the License at                                   //
//                                                                           //
//     http://www.apache.org/licenses/LICENSE-2.0                            //
//                                                                           //
// Unless required by applicable law or agreed to in writing, software       //
// distributed under the License is distributed on an "AS IS" BASIS,         //
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  //
// See the License for the specific language governing permissions and       //
// limitations under the License.                                            //
//                                                                           //
// ========================================================================= //
#pragma once

#include <smks/xchg/ParmEvent.hpp>
#include <smks/xchg/IdSet.hpp>
#include <smks/parm/Container.hpp>

#include "AbstractHandle.hpp"

namespace smks { namespace sys
{
    template<typename BaseType>
    class InstanceHandle:
        public AbstractHandle
    {
    protected:
        /////////////////////////////////////
        // class InstanceHandle::ParmObserver
        /////////////////////////////////////
        class ParmObserver:
            public parm::AbstractObserver
        {
        public:
            typedef std::shared_ptr<ParmObserver> Ptr;
        private:
            InstanceHandle& _handle;
        public:
            static inline
            Ptr
            create(InstanceHandle& handle)
            {
                Ptr ptr(new ParmObserver(handle));
                return ptr;
            }
        private:
            explicit
            ParmObserver(InstanceHandle& handle):
                _handle(handle)
            { }
            inline
            void
            handle(unsigned int parmId, parm::Event typ)
            {
                _handle.handleParmEvent(parmId, typ);
            }
        };
        ///////////////////////////////////////

    protected:
        Ref<BaseType>               _instance;
        parm::Container::Ptr        _userParms;
    private:
        parm::Container::Ptr        _parms; //<! only built-in parameters
        parm::AbstractObserver::Ptr _parmObs;
        xchg::IdSet                 _perFrameDirtyParms;
    public:
        InstanceHandle():
            AbstractHandle      (),
            _instance           (nullptr),
            _userParms          (parm::Container::create()),
            _parms              (parm::Container::create()),
            _parmObs            (ParmObserver::create(*this)),
            _perFrameDirtyParms ()
        {
            _parms->registerObserver(_parmObs);
        }

    public:
        ~InstanceHandle()
        {
            _parms->deregisterObserver(_parmObs);
        }

        inline
        Ref<BaseType> const&
        instance() const
        {
            return _instance;
        }

        inline
        unsigned int
        scnId() const
        {
            return _instance
                ? _instance->scnId()
                : 0;
        }

        inline
        parm::Container&
        builtInParameters()
        {
            return *_parms;
        }
        inline
        parm::Container&
        userParameters()
        {
            return *_userParms;
        }
        inline
        const parm::Container&
        builtInParameters() const
        {
            return *_parms;
        }
        inline
        const parm::Container&
        userParameters() const
        {
            return *_userParms;
        }

        inline
        void
        dispose()
        {
            if (_instance)
                _instance->dispose();
        }

        inline
        bool
        perSubframeParameter(unsigned int parmId) const
        {
            return _instance
                ? _instance->perSubframeParameter(parmId)
                : false;
        }
        inline
        void
        beginSubframeCommits(size_t numSubframes)
        {
            if (_instance)
                _instance->beginSubframeCommits(numSubframes);
        }
        inline
        void
        commitSubframeState(size_t subframe)
        {
            if (_instance)
                _instance->commitSubframeState(subframe, *_parms);
        }
        inline
        void
        endSubframeCommits()
        {
            if (_instance)
                _instance->endSubframeCommits();
        }

        inline
        void
        commitFrameState()
        {
            if (_instance)
                _instance->commitFrameState(*_parms, _perFrameDirtyParms);
            _perFrameDirtyParms.clear();
        }

    private:
        inline
        void
        handleParmEvent(unsigned int parmId, parm::Event typ)
        {
            if (!perSubframeParameter(parmId))
                _perFrameDirtyParms.insert(parmId);
        }
    };
}
}
