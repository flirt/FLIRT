-- ========================================================================= --
-- Copyright 2016-2018 SUPAMONKS_STUDIO                                      --
-- Author: Pierre-Edouard Landes <pel@supamonks.com>                         --
--                                                                           --
-- This program is free software: you can redistribute it and/or modify      --
-- it under the terms of the GNU Lesser General Public License as            --
-- published by the Free Software Foundation, either version 3 of the        --
-- License, or (at your option) any later version.                           --
--                                                                           --
-- This program is distributed in the hope that it will be useful,           --
-- but WITHOUT ANY WARRANTY; without even the implied warranty of            --
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             --
-- GNU Lesser General Public License for more details.                       --
--                                                                           --
-- You should have received a copy of the GNU Lesser General Public License  --
-- along with this program.  If not, see <https://www.gnu.org/licenses/>.    --
--                                                                           --
-- ========================================================================= --


smks.module.uses_rndr_device = function ()

    smks.module.links(
        smks.module.name.rndr_device,
        smks.module.kind.rndr_device)

end


smks.module.copy_rndr_device_to_targetdir = function()

    smks.module.copy_to_targetdir(
        smks.module.name.rndr_device)

    smks.module.copy_clientbase_to_targetdir()

    smks.dep.embree.copy_to_targetdir()

end


project(smks.module.name.rndr_device)

    language 'C++'
    smks.module.set_kind(smks.module.kind.rndr_device)

    files
    {
        'include/**.hpp',
        'src/**.hpp',
        'src/**.cpp'
    }

    defines
    {
        'FLIRT_RNDR_DEVICE_DLL',
    }
    includedirs
    {
        'include',
    }

    smks.dep.boost.includes()
    smks.dep.embree.links()
    smks.dep.json.links()

    smks.contrib_rhf.links()

    smks.module.uses_log()
    smks.module.uses_strid()
    smks.module.uses_util()
    smks.module.uses_sys()
    smks.module.uses_math()
    smks.module.uses_img()
    smks.module.uses_asset()
    smks.module.uses_xchg()
    smks.module.uses_clientbase()
    smks.module.includes(smks.module.name.parm)
    smks.module.includes(smks.module.name.rndr_device_api)
