// ========================================================================= //
// Copyright 2016-2018 SUPAMONKS_STUDIO                                      //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
//                                                                           //
// ========================================================================= //
#pragma once

#include <memory>
#include "smks/sys/configure_rndr_device.hpp"

namespace smks
{
    class ClientBase;
    namespace rndr
    {
        class AbstractDevice;
    }
}

extern "C"
{
    FLIRT_RNDR_DEVICE_EXPORT
    smks::rndr::AbstractDevice*
    create(const char* jsonCfg,
           std::shared_ptr<smks::ClientBase> const&);

    FLIRT_RNDR_DEVICE_EXPORT
    void
    destroy(smks::rndr::AbstractDevice*);
}
