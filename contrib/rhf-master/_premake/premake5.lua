-- ========================================================================= --
-- Copyright 2016-2018 SUPAMONKS_STUDIO                                      --
-- Author: Pierre-Edouard Landes <pel@supamonks.com>                         --
--                                                                           --
-- This program is free software: you can redistribute it and/or modify      --
-- it under the terms of the GNU Lesser General Public License as            --
-- published by the Free Software Foundation, either version 3 of the        --
-- License, or (at your option) any later version.                           --
--                                                                           --
-- This program is distributed in the hope that it will be useful,           --
-- but WITHOUT ANY WARRANTY; without even the implied warranty of            --
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             --
-- GNU Lesser General Public License for more details.                       --
--                                                                           --
-- You should have received a copy of the GNU Lesser General Public License  --
-- along with this program.  If not, see <https://www.gnu.org/licenses/>.    --
--                                                                           --
-- ========================================================================= --

smks.contrib_rhf = {}


smks.contrib_rhf.links = function()

    local contrib_rhf_path = function(...)

        return smks.repo_path('contrib', 'rhf-master', ...)

    end

    filter {}
    includedirs
    {
        contrib_rhf_path(),
    }
    libdirs
    {
        contrib_rhf_path(smks.compiler(), 'lib', '%{cfg.buildcfg}'),
    }
    links
    {
        'contrib_rhf',
    }

end


project 'contrib_rhf'

    language 'C++'
    smks.module.set_kind('StaticLib')

    files
    {
        '../libauxiliar.h',
        '../libauxiliar.cpp',
        '../libdenoising.h',
        '../libdenoising.cpp',
    }
    includedirs
    {
        '..',
    }


-- project 'test_rhf'

--     language 'C++'
--     smks.module.set_kind('ConsoleApp')

--     files
--     {
--         '../io_exr.h',
--         '../io_exr.cpp',
--         '../io_png.h',
--         '../io_png.c',
--         '../rhf.cpp',
--     }
--     includedirs
--     {
--         '..',
--     }

--     smks.dep.zlib.links()
--     smks.dep.lpng.links()
--     smks.dep.openexr.links()

--     smks.contrib_rhf.links()

--     smks.dep.zlib.copy_to_targetdir()
--     smks.dep.lpng.copy_to_targetdir()
