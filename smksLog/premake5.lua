-- ========================================================================= --
-- Copyright 2016-2018 SUPAMONKS_STUDIO                                      --
-- Author: Pierre-Edouard Landes <pel@supamonks.com>                         --
--                                                                           --
-- This program is free software: you can redistribute it and/or modify      --
-- it under the terms of the GNU Lesser General Public License as            --
-- published by the Free Software Foundation, either version 3 of the        --
-- License, or (at your option) any later version.                           --
--                                                                           --
-- This program is distributed in the hope that it will be useful,           --
-- but WITHOUT ANY WARRANTY; without even the implied warranty of            --
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             --
-- GNU Lesser General Public License for more details.                       --
--                                                                           --
-- You should have received a copy of the GNU Lesser General Public License  --
-- along with this program.  If not, see <https://www.gnu.org/licenses/>.    --
--                                                                           --
-- ========================================================================= --


smks.module.uses_log = function ()

    smks.module.links(
        smks.module.name.log,
        smks.module.kind.log)

    filter 'configurations:Debug'
        defines
        {
            'FLIRT_LOG_ENABLED',
            '_ELPP_THREAD_SAFE',
            'ELPP_DISABLE_DEFAULT_CRASH_HANDLING',
        }
        smks.dep.easylogging.includes()
    filter {}

end


smks.module.copy_log_to_targetdir = function()

    smks.module.copy_to_targetdir(
        smks.module.name.log)

end


project(smks.module.name.log)

    language 'C++'
    smks.module.set_kind(
        smks.module.kind.log)

    files
    {
        'include/**.hpp',
        'src/**.hpp',
        'src/**.cpp',
    }
    defines
    {
        'FLIRT_LOG_DLL',
        '_ELPP_THREAD_SAFE',
        'ELPP_DISABLE_DEFAULT_CRASH_HANDLING',
    }
    includedirs
    {
        'include',
    }
    filter 'configurations:Debug'
        defines
        {
            'FLIRT_LOG_ENABLED',
        }
    filter {}

    smks.dep.easylogging.includes()
