// ========================================================================= //
// Copyright 2016-2018 SUPAMONKS_STUDIO                                      //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
//                                                                           //
// ========================================================================= //
#include "smks/sys/Log.hpp"

#if defined(FLIRT_LOG_ENABLED)

#define ELPP_AS_DLL // Tells Easylogging++ that it's used for DLL
#define ELPP_EXPORT_SYMBOLS // Tells Easylogging++ to export symbols

INITIALIZE_EASYLOGGINGPP

namespace smks
{
    namespace sys
    {
        el::base::type::StoragePointer
        sharedLoggingRepository()
        {
            return el::Helpers::storage();
        }

        void
        initializeLogging(int argc, char** argv)
        {
            el::Helpers::setStorage(sharedLoggingRepository());
            START_EASYLOGGINGPP(argc, argv);
        }

        void
        redirectLogging(const std::string& fileName)
        {
            if (fileName.empty())
                return;

            el::Configurations defaultConf;

            defaultConf.setToDefault();
            defaultConf.setGlobally(el::ConfigurationType::ToStandardOutput, "false");
            defaultConf.setGlobally(el::ConfigurationType::ToFile, "true");
            defaultConf.setGlobally(el::ConfigurationType::Filename, fileName);

            el::Loggers::reconfigureLogger("default", defaultConf);
        }
    }
}

#endif //defined(FLIRT_LOG_ENABLED)

void
smks::sys::emptySmksLogSymbol()
{
}
