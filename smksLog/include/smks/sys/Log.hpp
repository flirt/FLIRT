// ========================================================================= //
// Copyright 2016-2018 SUPAMONKS_STUDIO                                      //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
//                                                                           //
// ========================================================================= //
#pragma once

#if defined(_MSC_VER) || defined(__CYGWIN__) || defined(__MINGW32__) || defined( __BCPLUSPLUS__)  || defined( __MWERKS__)
    #if defined(FLIRT_LOG_DLL)
        #define FLIRT_LOG_EXPORT __declspec(dllexport)
    #else
        #define FLIRT_LOG_EXPORT /*__declspec(dllimport)*/
    #endif
#else
    #define FLIRT_LOG_EXPORT
#endif

#if defined(FLIRT_LOG_ENABLED)

#include <easylogging++.h>
#undef near
#undef far

// extern declaration that should be used in static libraries
#define INITIALIZE_NULL_EXTERN_EASYLOGGINGPP\
    namespace el {\
    namespace base {\
        extern el::base::type::StoragePointer   elStorage;\
        extern el::base::debug::CrashHandler    elCrashHandler;\
        }\
    }\


namespace smks { namespace sys
{
    FLIRT_LOG_EXPORT
    el::base::type::StoragePointer
    sharedLoggingRepository();

    FLIRT_LOG_EXPORT
    void
    initializeLogging(int argc, char** argv);

    FLIRT_LOG_EXPORT
    void
    redirectLogging(const std::string&);
}
}

#define FLIRT_LOG(l) l

#else // defined(FLIRT_LOG_ENABLED)

#define FLIRT_LOG(l)

#endif // defined(FLIRT_LOG_ENABLED)

namespace smks { namespace sys
{
    FLIRT_LOG_EXPORT
    void
    emptySmksLogSymbol();
}
}
