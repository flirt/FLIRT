// ========================================================================= //
// Modification copyright 2016-2018 SUPAMONKS_STUDIO                         //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
// _________________________________________________________________________ //
// Copyright 2009-2013 Intel Corporation                                     //
//                                                                           //
// Licensed under the Apache License, Version 2.0 (the "License");           //
// you may not use this file except in compliance with the License.          //
// You may obtain a copy of the License at                                   //
//                                                                           //
//     http://www.apache.org/licenses/LICENSE-2.0                            //
//                                                                           //
// Unless required by applicable law or agreed to in writing, software       //
// distributed under the License is distributed on an "AS IS" BASIS,         //
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  //
// See the License for the specific language governing permissions and       //
// limitations under the License.                                            //
//                                                                           //
// ========================================================================= //
#include <vector>

#include "smks/sys/Thread.hpp"
#include "smks/sys/TaskScheduler.hpp"
#include "smks/sys/sysinfo.hpp"
#include "smks/sys/Exception.hpp"
#include "smks/sys/delay.hpp"
#include "TaskSchedulerSys.hpp"
#include "TaskSchedulerMIC.hpp"

namespace smks { namespace sys
{
    /*static*/ TaskScheduler* TaskScheduler::instance = NULL;

    ////////////////
    // struct Thread
    ////////////////
    /* initialization structure for threads */
    struct Thread
    {
    public:
        size_t          threadIndex;
        size_t          threadCount;
        TaskScheduler*  scheduler;

    public:
        Thread (size_t threadIndex, size_t threadCount, TaskScheduler* scheduler):
            threadIndex(threadIndex), threadCount(threadCount), scheduler(scheduler)
        { }
    };

    /////////////////////////////
    // class TaskScheduler::PImpl
    /////////////////////////////
    /* thread handling */
    class TaskScheduler::PImpl
    {
    protected:
        TaskScheduler   *const  _self;
        //----------------------
        volatile bool           _terminateThreads;
        std::vector<thread_t>   _threads;
        size_t                  _numThreads;
        Event**                 _thread2event;

    public:
        explicit inline
        PImpl(TaskScheduler* self):
            _self               (self),
            _terminateThreads   (false),
            _threads            (),
            _numThreads         (0),
            _thread2event       (NULL)
        {
            if (_self == nullptr)
                throw sys::Exception("Invalid task scheduler handle.", "Task Scheduler Construction");
        }

        inline
        bool
        terminateThreads() const
        {
            return _terminateThreads;
        }

        inline
        void
        terminateThreads(bool value)
        {
            _terminateThreads = value;
        }

        inline
        size_t
        numThreads() const
        {
            return _numThreads;
        }

        inline
        void
        thread2event(ssize_t threadIndex, Event* value)
        {
            if (threadIndex < 0 || threadIndex >= (ssize_t)_numThreads)
                throw sys::Exception("Invalid thread index.", "Task Scheduler Thread Event Setter");
            _thread2event[threadIndex] = value;
        }

        inline
        Event*
        thread2event(ssize_t threadIndex) const
        {
            if (threadIndex < 0 || threadIndex >= (ssize_t)_numThreads)
                throw sys::Exception("Invalid thread index.", "Task Scheduler Thread Event Getter");
            return _thread2event[threadIndex];
        }

        inline
        void
        createThreads(size_t numThreads_in)
        {
            _numThreads = numThreads_in;
#if defined(__MIC__)
            if (_numThreads == 0) _numThreads = getNumberOfLogicalThreads()-4;
#else
            if (_numThreads == 0) _numThreads = getNumberOfLogicalThreads();
#endif
            /* this mapping is only required as ISPC does not propagate task groups */
            _thread2event = new Event*[_numThreads];
            memset(_thread2event, 0, _numThreads * sizeof(Event*));

            /* generate all threads */
            for (size_t t=0; t<_numThreads; t++)
            {
                _threads.push_back(
                    createThread((thread_func)threadFunction, new Thread(t, _numThreads, _self), 4 * 1024 * 1024, t));
            }
        }

        inline
        void
        destroyThreads ()
        {
            _self->terminate();

            for (size_t i=0; i<_threads.size(); i++)
                join(_threads[i]);
            _threads.clear();

            delete[] _thread2event;
            _thread2event       = NULL;
            _terminateThreads   = false;
        }
    };

    //---------------------------------------------------------------------
    //--------------------------------------------------------------------

    // static
    void TaskScheduler::create(size_t numThreads)
    {
        if (instance)
            throw sys::Exception("FLIRT threads already running.", "Scheduler Thread Creation");

#if defined(__MIC__)
#if 0
        /* enable fast spinning tasking system */
        instance = new TaskSchedulerMIC;
        std::cout << "DONE: TaskSchedulerMIC" << std::endl << std::flush;
        sleep(1);
#else
        /* enable slower pthreads tasking system */
        printf("WARNING: taskscheduler.cpp: Using pthreads tasking system active on MIC! Expect reduced rendering performance.\n");
        instance = new TaskSchedulerSys;
#endif
#else
        /* enable fast pthreads tasking system */
        instance = new TaskSchedulerSys;
#endif
#if 1
        instance->createThreads(numThreads);
#else
        instance->createThreads(1);
        std::cout << "WARNING: Using only a single thread." << std::endl;
#endif
    }

    // static
    size_t
    TaskScheduler::getNumThreads()
    {
        if (!instance)
            throw sys::Exception("FLIRT tasks not running.", "Scheduler Thread Count Getter");
        return instance->numThreads();
    }

    // static
    void
    TaskScheduler::addTask(ssize_t threadIndex, QUEUE queue, Task* task)
    {
        if (!instance)
            throw sys::Exception("FLIRT tasks not running.", "Add Task to Scheduler");
        instance->add(threadIndex,queue,task);
    }

    // static
    void
    TaskScheduler::destroy()
    {
        if (instance)
        {
            instance->destroyThreads();
            delete instance;
            instance = NULL;
        }
    }

    // static
    TaskScheduler::Event*
    TaskScheduler::getISPCEvent(ssize_t threadIndex)
    {
        if (!instance)
            throw sys::Exception("FLIRT tasks not running.", "ISPC Thread Event Getter");
        return instance->thread2event(threadIndex);
    }

    // static
    void
    TaskScheduler::threadFunction(void* ptr)
    try
    {
        Thread* thread = (Thread*) ptr;
        thread->scheduler->run(thread->threadIndex,thread->threadCount);
        delete thread;
    }
    catch (const std::exception& e)
    {
        std::cerr << "\n\n-----\nERROR\n-----\n" << e.what() << std::endl;
        delay(10000);
        exit(1);
    }

    //---------------------------------------------------------------------
    //---------------------------------------------------------------------

    TaskScheduler::TaskScheduler ():
        _pImpl(new PImpl(this))
    { }

    TaskScheduler::~TaskScheduler ()
    {
        delete _pImpl;
    }

    bool
    TaskScheduler::terminateThreads() const
    {
        return _pImpl->terminateThreads();
    }

    void
    TaskScheduler::terminateThreads(bool value)
    {
        _pImpl->terminateThreads(value);
    }

    size_t
    TaskScheduler::numThreads() const
    {
        return _pImpl->numThreads();
    }

    void
    TaskScheduler::thread2event(ssize_t threadIndex, Event* value) const
    {
        _pImpl->thread2event(threadIndex, value);
    }

    TaskScheduler::Event*
    TaskScheduler::thread2event(ssize_t threadIndex) const
    {
        return _pImpl->thread2event(threadIndex);
    }

    void
    TaskScheduler::createThreads(size_t numThreads_in)
    {
        _pImpl->createThreads(numThreads_in);
    }

    void
    TaskScheduler::destroyThreads ()
    {
        _pImpl->destroyThreads();
    }
}
}
