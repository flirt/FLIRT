// ========================================================================= //
// Modification copyright 2016-2018 SUPAMONKS_STUDIO                         //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
// _________________________________________________________________________ //
// Copyright 2009-2013 Intel Corporation                                     //
//                                                                           //
// Licensed under the Apache License, Version 2.0 (the "License");           //
// you may not use this file except in compliance with the License.          //
// You may obtain a copy of the License at                                   //
//                                                                           //
//     http://www.apache.org/licenses/LICENSE-2.0                            //
//                                                                           //
// Unless required by applicable law or agreed to in writing, software       //
// distributed under the License is distributed on an "AS IS" BASIS,         //
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  //
// See the License for the specific language governing permissions and       //
// limitations under the License.                                            //
//                                                                           //
// ========================================================================= //
#pragma once

#include "smks/sys/TaskScheduler.hpp"
#include "smks/sys/sync/Mutex.hpp"

namespace smks { namespace sys
{
    /*! Task scheduler using active synchronization. */
    class TaskSchedulerMIC:
        public TaskScheduler
    {
    public:
        enum { NUM_TASKS = 4 * 1024 };
    private:
        sync::Atomic        nextScheduleIndex;  /*! next index in the task queue where we'll insert a live task */
        Task* volatile      tasks[NUM_TASKS];   //!< queue of tasks
        volatile atomic_t   locks[NUM_TASKS];

        /*! construction */
        TaskSchedulerMIC();

    private:
        /*! adds a task to the specified task queue */
        void
        add(ssize_t threadIndex, QUEUE queue, Task* task);

        /*! thread function */
        void
        run(size_t threadIndex, size_t threadCount);

        /*! sets the terminate thread variable */
        void
        terminate();
    };
}
}
