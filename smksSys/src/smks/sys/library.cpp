// ========================================================================= //
// Modification copyright 2016-2018 SUPAMONKS_STUDIO                         //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
// _________________________________________________________________________ //
// Copyright 2009-2013 Intel Corporation                                     //
//                                                                           //
// Licensed under the Apache License, Version 2.0 (the "License");           //
// you may not use this file except in compliance with the License.          //
// You may obtain a copy of the License at                                   //
//                                                                           //
//     http://www.apache.org/licenses/LICENSE-2.0                            //
//                                                                           //
// Unless required by applicable law or agreed to in writing, software       //
// distributed under the License is distributed on an "AS IS" BASIS,         //
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  //
// See the License for the specific language governing permissions and       //
// limitations under the License.                                            //
//                                                                           //
// ========================================================================= //
#include <boost/filesystem.hpp>

#include "smks/sys/library.hpp"
#include "smks/sys/sysinfo.hpp"

////////////////////////////////////////////////////////////////////////////////
/// Windows Platform
////////////////////////////////////////////////////////////////////////////////

#if defined(__WIN32__)

#define WIN32_LEAN_AND_MEAN
#include <windows.h>

namespace smks { namespace sys
{
    ///* opens a shared library */
    lib_t
    openLibrary(const char* libName,
                const char* binPath)
    {
        using namespace boost;

        filesystem::path libPath(binPath);

        libPath.remove_leaf() /= libName;
        libPath = libPath.replace_extension("dll");
        if (filesystem::exists(libPath) &&
            filesystem::is_regular_file(libPath))
        {
            // add binary directory to windows search paths
            filesystem::path libDir = libPath;
            libDir.remove_leaf();
            SetDllDirectory(libDir.string().c_str());

            HANDLE handle = LoadLibrary(libPath.string().c_str());
            return lib_t(handle);
        }
        else
        {
            std::cerr << "Failed to find '" << libPath.c_str() << "'.";
            return lib_t(0);
        }
    }

    /* returns address of a symbol from the library */
    void*
    getSymbol(lib_t       lib,
              const char* sym)
    {
        return GetProcAddress(HMODULE(lib), sym);
    }

    /* closes the shared library */
    void
    closeLibrary(lib_t lib)
    {
        FreeLibrary(HMODULE(lib));
    }
} }
#endif

////////////////////////////////////////////////////////////////////////////////
/// Unix Platform
////////////////////////////////////////////////////////////////////////////////

#if defined(__UNIX__)

#include <dlfcn.h>

namespace smks { namespace sys
{
    /* opens a shared library */
    lib_t
    openLibrary(const char* file)
    {
#if defined(__MACOSX__)
        std::string fullName = "lib" + std::string(file) + ".dylib";
#else
        std::string fullName = "lib" + std::string(file) + ".so";
#endif
        FileName executable = getExecutableFileName();
        void* lib = dlopen((executable.path() + fullName).c_str(),RTLD_NOW);
        if (lib == NULL) throw std::runtime_error(dlerror());
        return lib_t(lib);
    }

    lib_t
    openLibrary(const char* libName,
                const char* binPath)
    {
        throw std::runtime_error("openLibrary(libName, binPath) not implemented for current platform.");
    }

    /* returns address of a symbol from the library */
    void*
    getSymbol(lib_t         lib,
              const char*   sym)
    {
        return dlsym(lib, sym);
    }

    /* closes the shared library */
    void
    closeLibrary(lib_t lib)
    {
        dlclose(lib);
    }
} }
#endif
