-- ========================================================================= --
-- Modification copyright 2016-2018 SUPAMONKS_STUDIO                         --
-- Author: Pierre-Edouard Landes <pel@supamonks.com>                         --
--                                                                           --
-- This program is free software: you can redistribute it and/or modify      --
-- it under the terms of the GNU Lesser General Public License as            --
-- published by the Free Software Foundation, either version 3 of the        --
-- License, or (at your option) any later version.                           --
--                                                                           --
-- This program is distributed in the hope that it will be useful,           --
-- but WITHOUT ANY WARRANTY; without even the implied warranty of            --
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             --
-- GNU Lesser General Public License for more details.                       --
--                                                                           --
-- You should have received a copy of the GNU Lesser General Public License  --
-- along with this program.  If not, see <https://www.gnu.org/licenses/>.    --
-- _________________________________________________________________________ --
-- Copyright 2009-2013 Intel Corporation                                     --
--                                                                           --
-- Licensed under the Apache License, Version 2.0 (the "License");           --
-- you may not use this file except in compliance with the License.          --
-- You may obtain a copy of the License at                                   --
--                                                                           --
--     http://www.apache.org/licenses/LICENSE-2.0                            --
--                                                                           --
-- Unless required by applicable law or agreed to in writing, software       --
-- distributed under the License is distributed on an "AS IS" BASIS,         --
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  --
-- See the License for the specific language governing permissions and       --
-- limitations under the License.                                            --
--                                                                           --
-- ========================================================================= --


smks.module.uses_sys = function ()

    smks.module.links(
        smks.module.name.sys,
        smks.module.kind.sys)

end


smks.module.copy_sys_to_targetdir = function()

    smks.module.copy_to_targetdir(
        smks.module.name.sys)

end


project(smks.module.name.sys)

    language 'C++'
    smks.module.set_kind(
        smks.module.kind.sys)

    characterset ('MBCS')

    files
    {
        'include/**.hpp',
        'src/**.hpp',
        'src/**.cpp',
    }
    defines
    {
        'FLIRT_SYS_DLL',
    }
    includedirs
    {
        'include',
    }

    filter 'configurations:Release'
        defines
        {
            'INTEL_SUITE_VERSION=PE120_196',
        }
    filter {}

    smks.dep.boost.includes()
    smks.dep.boost.links_filesystem()
