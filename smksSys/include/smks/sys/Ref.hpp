// ========================================================================= //
// Modification copyright 2016-2018 SUPAMONKS_STUDIO                         //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
// _________________________________________________________________________ //
// Copyright 2009-2013 Intel Corporation                                     //
//                                                                           //
// Licensed under the Apache License, Version 2.0 (the "License");           //
// you may not use this file except in compliance with the License.          //
// You may obtain a copy of the License at                                   //
//                                                                           //
//     http://www.apache.org/licenses/LICENSE-2.0                            //
//                                                                           //
// Unless required by applicable law or agreed to in writing, software       //
// distributed under the License is distributed on an "AS IS" BASIS,         //
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  //
// See the License for the specific language governing permissions and       //
// limitations under the License.                                            //
//                                                                           //
// ========================================================================= //
#pragma once

#include "smks/sys/constants.hpp"
#include "smks/sys/sync/Atomic.hpp"

namespace smks { namespace sys
{
    class RefCount
    {
    private:
        sync::Atomic _refCounter;

    public:
        RefCount(int val = 0) : _refCounter(val) {}
        virtual ~RefCount() {};

        virtual void refInc() { _refCounter++; }
        virtual void refDec() { if (--_refCounter == 0) delete this; }
    };

    ////////////////////////////////////////////////////////////////////////////////
    /// Reference to single object
    ////////////////////////////////////////////////////////////////////////////////

    template<typename Type>
    class Ref
    {
    protected:
        Type* const _ptr;

        ////////////////////////////////////////////////////////////////////////////////
        /// Constructors, Assignment & Cast Operators
        ////////////////////////////////////////////////////////////////////////////////
    public:
        __forceinline Ref( void )               : _ptr(NULL)        {}
        __forceinline Ref(NullTy)               : _ptr(NULL)        {}
        __forceinline Ref( const Ref& input )   : _ptr(input._ptr)  { if ( _ptr ) _ptr->refInc();   }
        __forceinline Ref( Type* const input )  : _ptr(input)       { if ( _ptr ) _ptr->refInc();   }
        __forceinline ~Ref( void )                                  { if (_ptr) _ptr->refDec();     }

        __forceinline Ref& operator =( const Ref& input )
        {
            if ( input._ptr ) input._ptr->refInc();
            if (_ptr) _ptr->refDec();
            *(Type**)&_ptr = input._ptr;
            return *this;
        }

        __forceinline Ref& operator =( NullTy )
        {
            if (_ptr) _ptr->refDec();
            *(Type**)&_ptr = NULL;
            return *this;
        }

        __forceinline Type*         ptr()       { return _ptr; }
        __forceinline const Type*   ptr() const { return _ptr; }

        __forceinline operator bool( void ) const { return _ptr != NULL; }

        __forceinline const Type& operator  *( void ) const { return *_ptr; }
        __forceinline       Type& operator  *( void )       { return *_ptr; }
        __forceinline const Type* operator ->( void ) const { return  _ptr; }
        __forceinline       Type* operator ->( void )       { return  _ptr; }

        template<typename TypeOut>
        __forceinline       Ref<TypeOut> cast()       { return Ref<TypeOut>(static_cast<TypeOut*>(_ptr)); }
        template<typename TypeOut>
        __forceinline const Ref<TypeOut> cast() const { return Ref<TypeOut>(static_cast<TypeOut*>(_ptr)); }

        template<typename TypeOut>
        __forceinline       Ref<TypeOut> dynamicCast()       { return Ref<TypeOut>(dynamic_cast<TypeOut*>(_ptr)); }
        template<typename TypeOut>
        __forceinline const Ref<TypeOut> dynamicCast() const { return Ref<TypeOut>(dynamic_cast<TypeOut*>(_ptr)); }
    };

    template<typename Type> __forceinline  bool operator < ( const Ref<Type>& a, const Ref<Type>& b ) { return a.ptr() <  b.ptr() ; }

    template<typename Type> __forceinline  bool operator ==( const Ref<Type>& a, NullTy             ) { return a.ptr()  == NULL  ;      }
    template<typename Type> __forceinline  bool operator ==( NullTy            , const Ref<Type>& b ) { return NULL     == b.ptr() ;    }
    template<typename Type> __forceinline  bool operator ==( const Ref<Type>& a, const Ref<Type>& b ) { return a.ptr()  == b.ptr() ;    }

    template<typename Type> __forceinline  bool operator !=( const Ref<Type>& a, NullTy             ) { return a.ptr()  != NULL  ;      }
    template<typename Type> __forceinline  bool operator !=( NullTy            , const Ref<Type>& b ) { return NULL     != b.ptr() ;    }
    template<typename Type> __forceinline  bool operator !=( const Ref<Type>& a, const Ref<Type>& b ) { return a.ptr()  != b.ptr() ;    }
} }

