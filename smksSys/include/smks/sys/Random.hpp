// ========================================================================= //
// Modification copyright 2016-2018 SUPAMONKS_STUDIO                         //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
// _________________________________________________________________________ //
// Copyright 2009-2013 Intel Corporation                                     //
//                                                                           //
// Licensed under the Apache License, Version 2.0 (the "License");           //
// you may not use this file except in compliance with the License.          //
// You may obtain a copy of the License at                                   //
//                                                                           //
//     http://www.apache.org/licenses/LICENSE-2.0                            //
//                                                                           //
// Unless required by applicable law or agreed to in writing, software       //
// distributed under the License is distributed on an "AS IS" BASIS,         //
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  //
// See the License for the specific language governing permissions and       //
// limitations under the License.                                            //
//                                                                           //
// ========================================================================= //
#pragma once

#include "smks/sys/constants.hpp"

namespace smks { namespace sys
{
    class Random
    {
    private:
        int _seed;
        int _state;
        int _table[32];

    public:

        explicit __forceinline
        Random(const int seed = 27)
        {
            setSeed(seed);
        }

        __forceinline
        Random(const Random& other)
        {
            copy(other);
        }

        __forceinline
        Random&
        operator=(const Random& other)
        {
            copy(other);
            return *this;
        }

    private:
        __forceinline
        void
        copy(const Random& other)
        {
            _seed   = other._seed;
            _state  = other._state;
            memcpy(_table, other._table, sizeof(int) * 32);
        }

    public:
        __forceinline
        void
        setSeed(const int s)
        {
            const int a = 16807;
            const int m = 2147483647;
            const int q = 127773;
            const int r = 2836;
            int j, k;

            if (s == 0)
                _seed = 1;
            else if (s < 0)
                _seed = -s;
            else
                _seed = s;

            for (j = 32+7; j >= 0; j--)
            {
                k = _seed / q;
                _seed = a*(_seed - k*q) - r*k;
                if (_seed < 0)
                    _seed += m;
                if (j < 32)
                    _table[j] = _seed;
            }
            _state = _table[0];
        }

        __forceinline
        int
        getInt()
        {
            const int a = 16807;
            const int m = 2147483647;
            const int q = 127773;
            const int r = 2836;

            int k = _seed / q;
            _seed = a*(_seed - k*q) - r*k;
            if (_seed < 0)
                _seed += m;
            int j = _state / (1 + (2147483647-1) / 32);
            _state = _table[j];
            _table[j] = _seed;

            return _state;
        }

        __forceinline
        int
        getInt(int limit)
        {
            return getInt() % limit;
        }

        __forceinline
        float
        getFloat()
        {
            const float s       = getInt() / 2147483647.0f;
            const float upper   = 1.0f - float(ulp);

            return s < upper ? s : upper;
        }

        __forceinline
        double
        getDouble()
        {
            const double s      = getInt() / 2147483647.0;
            const double upper  = 1.0  - double(ulp);

            return s < upper ? s : upper;
        }
    };

    template<typename ValueType> __forceinline
    void
    shuffle(size_t size, ValueType* values, Random& rng)
    {
        for (size_t i = 0; i < size; i++)
            std::swap(values[i], values[rng.getInt(static_cast<int>(size))]);
    }

}
}
