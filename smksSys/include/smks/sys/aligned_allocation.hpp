// ========================================================================= //
// Modification copyright 2016-2018 SUPAMONKS_STUDIO                         //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
// _________________________________________________________________________ //
// Copyright 2009-2013 Intel Corporation                                     //
//                                                                           //
// Licensed under the Apache License, Version 2.0 (the "License");           //
// you may not use this file except in compliance with the License.          //
// You may obtain a copy of the License at                                   //
//                                                                           //
//     http://www.apache.org/licenses/LICENSE-2.0                            //
//                                                                           //
// Unless required by applicable law or agreed to in writing, software       //
// distributed under the License is distributed on an "AS IS" BASIS,         //
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  //
// See the License for the specific language governing permissions and       //
// limitations under the License.                                            //
//                                                                           //
// ========================================================================= //
#pragma once

#include <cstdlib>
#include <smks/sys/Exception.hpp>

namespace smks { namespace sys
{
    inline
    void*
    alignedMalloc(size_t size, size_t align)
    {
        if (size == 0)
            return nullptr;

        char* base = (char*)malloc(size + align + sizeof(int));
        if (base == nullptr)
            throw Exception(
                "Failed to allocate memory.",
                "Aligned Memory Allocation");

        char*   unaligned   = base + sizeof(int);
        char*   aligned     = unaligned + align - ((size_t)unaligned & (align-1));
        ((int*)aligned)[-1] = (int)((size_t)aligned-(size_t)base);

        return aligned;
    }

    inline
    void
    alignedFree(const void* ptr)
    {
        if (ptr == nullptr)
            return;
        int ofs = ((int*)ptr)[-1];
        free((char*)ptr-ofs);
    }
} }

#define ALIGNED_CLASS_(align)                                                               \
public:                                                                                     \
    void* operator new(size_t size)     { return smks::sys::alignedMalloc(size,align); }    \
    void operator delete(void* ptr)     { smks::sys::alignedFree(ptr); }                    \
    void* operator new[](size_t size)   { return smks::sys::alignedMalloc(size,align); }    \
    void operator delete[](void* ptr)   { smks::sys::alignedFree(ptr); }

#define ALIGNED_CLASS ALIGNED_CLASS_(64)
