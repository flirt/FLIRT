// ========================================================================= //
// Modification copyright 2016-2018 SUPAMONKS_STUDIO                         //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
// _________________________________________________________________________ //
// Copyright 2009-2013 Intel Corporation                                     //
//                                                                           //
// Licensed under the Apache License, Version 2.0 (the "License");           //
// you may not use this file except in compliance with the License.          //
// You may obtain a copy of the License at                                   //
//                                                                           //
//     http://www.apache.org/licenses/LICENSE-2.0                            //
//                                                                           //
// Unless required by applicable law or agreed to in writing, software       //
// distributed under the License is distributed on an "AS IS" BASIS,         //
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  //
// See the License for the specific language governing permissions and       //
// limitations under the License.                                            //
//                                                                           //
// ========================================================================= //
#pragma once

#include "smks/sys/intrinsics.hpp"

#include "smks/sys/configure_sys.hpp"

namespace smks { namespace sys { namespace sync
{
    struct FLIRT_SYS_EXPORT Atomic
    {
    private:
        volatile atomic_t data;

    protected:
        Atomic( const Atomic& ); // don't implement
        Atomic& operator =( const Atomic& ); // don't implement

    public:
        __forceinline Atomic( void ) : data(0) {}
        __forceinline Atomic( const atomic_t data ) : data(data) {}
        __forceinline Atomic& operator =( const atomic_t input ) { data = input; return *this; }
        __forceinline operator atomic_t() const { return data; }

    public:
        __forceinline atomic_t sub( const atomic_t input ) { return atomic_add(&data,-input) - input; }
        __forceinline atomic_t add( const atomic_t input ) { return atomic_add(&data, input) + input; }
        __forceinline friend atomic_t operator +=( Atomic& value, const atomic_t input ) { return atomic_add(&value.data, input) + input; }
        __forceinline friend atomic_t operator ++( Atomic& value ) { return atomic_add(&value.data,  1) + 1; }
        __forceinline friend atomic_t operator --( Atomic& value ) { return atomic_add(&value.data, -1) - 1; }
        __forceinline friend atomic_t operator ++( Atomic& value, int ) { return atomic_add(&value.data,  1); }
        __forceinline friend atomic_t operator --( Atomic& value, int ) { return atomic_add(&value.data, -1); }
        __forceinline friend atomic_t cmpxchg    ( Atomic& value, const atomic_t v, const atomic_t c) { return atomic_cmpxchg(&value.data,v,c); }
    };
}
} }

