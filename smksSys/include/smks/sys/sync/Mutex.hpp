// ========================================================================= //
// Modification copyright 2016-2018 SUPAMONKS_STUDIO                         //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
// _________________________________________________________________________ //
// Copyright 2009-2013 Intel Corporation                                     //
//                                                                           //
// Licensed under the Apache License, Version 2.0 (the "License");           //
// you may not use this file except in compliance with the License.          //
// You may obtain a copy of the License at                                   //
//                                                                           //
//     http://www.apache.org/licenses/LICENSE-2.0                            //
//                                                                           //
// Unless required by applicable law or agreed to in writing, software       //
// distributed under the License is distributed on an "AS IS" BASIS,         //
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  //
// See the License for the specific language governing permissions and       //
// limitations under the License.                                            //
//                                                                           //
// ========================================================================= //
#pragma once

#include "smks/sys/platform.hpp"
#include "smks/sys/sync/Atomic.hpp"

#include "smks/sys/configure_sys.hpp"

namespace smks { namespace sys { namespace sync
{
    /*! system mutex */
    class FLIRT_SYS_EXPORT MutexSys
    {
        friend class ConditionSys;
    public:

        MutexSys( void );
        ~MutexSys( void );

        void lock( void );
        void unlock( void );

    protected:
        void* mutex;

        MutexSys( const MutexSys& );             // don't implement
        MutexSys& operator =( const MutexSys& ); // don't implement
    };

    /*! spinning mutex */
    class MutexActive
    {
    public:
        __forceinline MutexActive( void ) : flag(0) {}
        void lock  ( void );
        void unlock( void );
    protected:
        Atomic flag;

        MutexActive( const MutexActive& );             // don't implement
        MutexActive& operator =( const MutexActive& ); // don't implement
    };

    /*! safe mutex lock and unlock helper */
    template<typename Mutex>
    class Lock
    {
    public:
        Lock (Mutex& mutex) : mutex(mutex) { mutex.lock(); }
        ~Lock() { mutex.unlock(); }
    protected:
        Mutex& mutex;
        Lock( const Lock& );             // don't implement
        Lock& operator =( const Lock& ); // don't implement
    };
}
} }
