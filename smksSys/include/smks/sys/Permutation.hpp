// ========================================================================= //
// Modification copyright 2016-2018 SUPAMONKS_STUDIO                         //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
// _________________________________________________________________________ //
// Copyright 2009-2013 Intel Corporation                                     //
//                                                                           //
// Licensed under the Apache License, Version 2.0 (the "License");           //
// you may not use this file except in compliance with the License.          //
// You may obtain a copy of the License at                                   //
//                                                                           //
//     http://www.apache.org/licenses/LICENSE-2.0                            //
//                                                                           //
// Unless required by applicable law or agreed to in writing, software       //
// distributed under the License is distributed on an "AS IS" BASIS,         //
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  //
// See the License for the specific language governing permissions and       //
// limitations under the License.                                            //
//                                                                           //
// ========================================================================= //
#pragma once

#include <algorithm>
#include "smks/sys/Random.hpp"

namespace smks { namespace sys
{
    /*! Random permutation. */
    class Permutation
    {
    private:
        int     _elts;  //!< Size of the permutation.
        int*    _perm;  //!< Array storing the permutation.

    public:
        ///*! Creates a random permutation. Uses system random number generator. */
        //explicit inline
        //Permutation(int size)
        //{
        //  _elts = size;
        //  _perm = new int[_elts];
        //  for (int i=0; i<_elts; i++)
        //      _perm[i] = i;
        //  for (int i=0; i<_elts; i++)
        //      std::swap(_perm[i], _perm[random<uint32>() % _elts]);
        //}

        /*! Creates a random permutation. Random number generator is provided as argument. */
        Permutation(int size, Random& rng)
        {
            _elts = size;
            _perm = new int[_elts];
            for (int i=0; i<_elts; i++)
                _perm[i] = i;
            for (int i=0; i<_elts; i++)
                std::swap(_perm[i], _perm[rng.getInt(_elts)]);
        }

        /*! Destroys the permutation. */
        ~Permutation()
        {
            if (_perm)
                delete[] _perm;
            _perm = nullptr;
        }

        /*! Returns the size of the permutation. */
        __forceinline
        int
        size() const
        {
            return _elts;
        }

        /*! Returns the i'th element of the permutation. */
        __forceinline
        int
        operator[](int i) const
        {
            assert(i >= 0 && i < _elts);
            return _perm[i];
        }
    };
}
}
