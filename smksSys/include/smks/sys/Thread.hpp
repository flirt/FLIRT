// ========================================================================= //
// Modification copyright 2016-2018 SUPAMONKS_STUDIO                         //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
// _________________________________________________________________________ //
// Copyright 2009-2013 Intel Corporation                                     //
//                                                                           //
// Licensed under the Apache License, Version 2.0 (the "License");           //
// you may not use this file except in compliance with the License.          //
// You may obtain a copy of the License at                                   //
//                                                                           //
//     http://www.apache.org/licenses/LICENSE-2.0                            //
//                                                                           //
// Unless required by applicable law or agreed to in writing, software       //
// distributed under the License is distributed on an "AS IS" BASIS,         //
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  //
// See the License for the specific language governing permissions and       //
// limitations under the License.                                            //
//                                                                           //
// ========================================================================= //
#pragma once

#include "smks/sys/platform.hpp"

#include "smks/sys/configure_sys.hpp"

namespace smks { namespace sys
{
    /*! type for thread */
    typedef struct opaque_thread_t* thread_t;

    /*! signature of thread start function */
    typedef void (*thread_func)(void*);

    /*! creates a hardware thread running on specific logical thread */
    FLIRT_SYS_EXPORT thread_t createThread(thread_func f, void* arg, size_t stack_size = 0, ssize_t threadID = -1);

    /*! set affinity of the calling thread */
    FLIRT_SYS_EXPORT void setAffinity(ssize_t affinity);

    /*! the thread calling this function gets yielded */
    FLIRT_SYS_EXPORT void yield();

    /*! waits until the given thread has terminated */
    FLIRT_SYS_EXPORT void join(thread_t tid);

    /*! destroy handle of a thread */
    FLIRT_SYS_EXPORT void destroyThread(thread_t tid);

    /*! type for handle to thread local storage */
    typedef struct opaque_tls_t* tls_t;

    /*! creates thread local storage */
    FLIRT_SYS_EXPORT tls_t createTls();

    /*! set the thread local storage pointer */
    FLIRT_SYS_EXPORT void setTls(tls_t tls, void* const ptr);

    /*! return the thread local storage pointer */
    FLIRT_SYS_EXPORT void* getTls(tls_t tls);

    /*! destroys thread local storage identifier */
    FLIRT_SYS_EXPORT void destroyTls(tls_t tls);
}
}
