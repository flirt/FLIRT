-- ========================================================================= --
-- Copyright 2016-2018 SUPAMONKS_STUDIO                                      --
-- Author: Pierre-Edouard Landes <pel@supamonks.com>                         --
--                                                                           --
-- This program is free software: you can redistribute it and/or modify      --
-- it under the terms of the GNU Lesser General Public License as            --
-- published by the Free Software Foundation, either version 3 of the        --
-- License, or (at your option) any later version.                           --
--                                                                           --
-- This program is distributed in the hope that it will be useful,           --
-- but WITHOUT ANY WARRANTY; without even the implied warranty of            --
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             --
-- GNU Lesser General Public License for more details.                       --
--                                                                           --
-- You should have received a copy of the GNU Lesser General Public License  --
-- along with this program.  If not, see <https://www.gnu.org/licenses/>.    --
--                                                                           --
-- ========================================================================= --


smks.module.uses_view_gl = function()

    smks.module.links(
        smks.module.name.view_gl,
        smks.module.kind.view_gl)

    smks.dep.eigen.includes()
    smks.dep.osg.includes()

    smks.module.includes(smks.module.name.math)
    smks.module.includes(smks.module.name.img)
    smks.module.includes(smks.module.name.xchg)

end


smks.module.copy_view_gl_to_targetdir = function()

    smks.module.copy_to_targetdir(
        smks.module.name.view_gl)

    smks.module.copy_xchg_to_targetdir()

    smks.dep.osg.copy_to_targetdir()

    -- copy shaders alongside binaries in target directory
    local subdir = 'shader'
    filter {}
    postbuildcommands
    {
        smks.os.make_sub_targetdir(subdir),
        smks.os.copy_to_targetdir(
            smks.repo_path('smksViewGL', 'asset', 'shader', '*.glsl'),
            subdir),
    }

end


project(smks.module.name.view_gl)

    language 'C++'
    smks.module.set_kind(smks.module.kind.view_gl)

    files
    {
        'include/**.hpp',
        'src/**.hpp',
        'src/**.cpp',
    }
    defines
    {
        'FLIRT_VIEWGL_DLL',
    }
    includedirs
    {
        'include',
    }

    smks.dep.boost.includes()
    smks.dep.osg.links()

    smks.module.uses_log()
    smks.module.uses_strid()
    smks.module.uses_xchg()
    smks.module.uses_asset()
    smks.module.includes(smks.module.name.util)
    smks.module.includes(smks.module.name.img)
