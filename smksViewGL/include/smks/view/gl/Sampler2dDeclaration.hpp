// ========================================================================= //
// Copyright 2016-2018 SUPAMONKS_STUDIO                                      //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
//                                                                           //
// ========================================================================= //
#pragma once

#include "smks/view/gl/ProgramInputDeclaration.hpp"

namespace smks { namespace view { namespace gl
{
    class UniformInput;
    class Sampler2dInput;

    class FLIRT_VIEWGL_EXPORT Sampler2dDeclaration:
        public ProgramInputDeclaration
    {
        friend class Sampler2dInput;
    public:
        typedef std::shared_ptr<Sampler2dDeclaration>   Ptr;
    private:
        typedef std::shared_ptr<UniformDeclaration>     UniformDeclarationPtr;
        typedef std::shared_ptr<Sampler2dInput>         Sampler2dPtr;
    private:
        class PImpl;
    private:
        PImpl *_pImpl;

    public:
        static
        Ptr
        create(const char*);

    private:
        explicit
        Sampler2dDeclaration(const char*);
    public:
        ~Sampler2dDeclaration();

        Sampler2dPtr
        create(int unit) const;

    private:
        UniformDeclarationPtr const&
        uSampler() const;

        UniformDeclarationPtr const&
        uGLTransformUV() const;

        UniformDeclarationPtr const&
        uTransformUV() const;

        UniformDeclarationPtr const&
        uScale() const;

        UniformDeclarationPtr const&
        uValid() const;

        virtual inline const Sampler2dDeclaration* asSampler2dDeclaration() const { return this; }
    };
}
}
}
