// ========================================================================= //
// Copyright 2016-2018 SUPAMONKS_STUDIO                                      //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
//                                                                           //
// ========================================================================= //
#pragma once

#include "smks/sys/configure_view_gl.hpp"

namespace smks { namespace view { namespace gl
{
    class MaterialProgramBase;
    // points material programs
    FLIRT_VIEWGL_EXPORT_VAR const MaterialProgramBase& get_BasicPointsMaterial ();
    // curves material programs
    FLIRT_VIEWGL_EXPORT_VAR const MaterialProgramBase& get_BasicHairCurvesMaterial ();
    // polymesh material programs
    FLIRT_VIEWGL_EXPORT_VAR const MaterialProgramBase& get_BrushedMetalMeshMaterial ();
    FLIRT_VIEWGL_EXPORT_VAR const MaterialProgramBase& get_DielectricMeshMaterial ();
    FLIRT_VIEWGL_EXPORT_VAR const MaterialProgramBase& get_EyeMeshMaterial ();
    FLIRT_VIEWGL_EXPORT_VAR const MaterialProgramBase& get_MatteMeshMaterial ();
    FLIRT_VIEWGL_EXPORT_VAR const MaterialProgramBase& get_MetalMeshMaterial ();
    FLIRT_VIEWGL_EXPORT_VAR const MaterialProgramBase& get_MetallicPaintMeshMaterial ();
    FLIRT_VIEWGL_EXPORT_VAR const MaterialProgramBase& get_MirrorMeshMaterial ();
    FLIRT_VIEWGL_EXPORT_VAR const MaterialProgramBase& get_ObjMeshMaterial ();
    FLIRT_VIEWGL_EXPORT_VAR const MaterialProgramBase& get_PlasticMeshMaterial ();
    FLIRT_VIEWGL_EXPORT_VAR const MaterialProgramBase& get_ThinDielectricMeshMaterial ();
    FLIRT_VIEWGL_EXPORT_VAR const MaterialProgramBase& get_TranslucentMeshMaterial ();
    FLIRT_VIEWGL_EXPORT_VAR const MaterialProgramBase& get_TranslucentSSMeshMaterial ();
    FLIRT_VIEWGL_EXPORT_VAR const MaterialProgramBase& get_VelvetMeshMaterial ();
}
}
}

