// ========================================================================= //
// Copyright 2016-2018 SUPAMONKS_STUDIO                                      //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
//                                                                           //
// ========================================================================= //
#pragma once

#include "smks/sys/configure_view_gl.hpp"

namespace smks { namespace view { namespace gl
{
    class ProgramInputDeclaration;

    FLIRT_VIEWGL_EXPORT_VAR const ProgramInputDeclaration& uAntiAlias ();
    FLIRT_VIEWGL_EXPORT_VAR const ProgramInputDeclaration& uCenter ();
    FLIRT_VIEWGL_EXPORT_VAR const ProgramInputDeclaration& uClockTime ();    //<! in seconds
    FLIRT_VIEWGL_EXPORT_VAR const ProgramInputDeclaration& uColor ();
    FLIRT_VIEWGL_EXPORT_VAR const ProgramInputDeclaration& uDiffuseTexture ();
    FLIRT_VIEWGL_EXPORT_VAR const ProgramInputDeclaration& uEyeCorneaIOR ();
    FLIRT_VIEWGL_EXPORT_VAR const ProgramInputDeclaration& uEyeGlobeColor ();
    FLIRT_VIEWGL_EXPORT_VAR const ProgramInputDeclaration& uEyeHighlightAngSizeD ();
    FLIRT_VIEWGL_EXPORT_VAR const ProgramInputDeclaration& uEyeHighlightColor ();
    FLIRT_VIEWGL_EXPORT_VAR const ProgramInputDeclaration& uEyeHighlightSmoothness ();
    FLIRT_VIEWGL_EXPORT_VAR const ProgramInputDeclaration& uEyeHighlightCoordsD ();
    FLIRT_VIEWGL_EXPORT_VAR const ProgramInputDeclaration& uEyeIrisAngSizeD ();
    FLIRT_VIEWGL_EXPORT_VAR const ProgramInputDeclaration& uEyeIrisColor ();
    FLIRT_VIEWGL_EXPORT_VAR const ProgramInputDeclaration& uEyeIrisDepth ();
    FLIRT_VIEWGL_EXPORT_VAR const ProgramInputDeclaration& uEyeIrisDimensions ();
    FLIRT_VIEWGL_EXPORT_VAR const ProgramInputDeclaration& uEyeIrisEdgeBlur ();
    FLIRT_VIEWGL_EXPORT_VAR const ProgramInputDeclaration& uEyeIrisEdgeOffset ();
    FLIRT_VIEWGL_EXPORT_VAR const ProgramInputDeclaration& uEyeIrisNormalSmoothness ();
    FLIRT_VIEWGL_EXPORT_VAR const ProgramInputDeclaration& uEyeIrisRotationD ();
    FLIRT_VIEWGL_EXPORT_VAR const ProgramInputDeclaration& uEyePupilBlur ();
    FLIRT_VIEWGL_EXPORT_VAR const ProgramInputDeclaration& uEyePupilDimensions ();
    FLIRT_VIEWGL_EXPORT_VAR const ProgramInputDeclaration& uEyePupilSize ();
    FLIRT_VIEWGL_EXPORT_VAR const ProgramInputDeclaration& uEyeRadius ();
    FLIRT_VIEWGL_EXPORT_VAR const ProgramInputDeclaration& uFront ();
    FLIRT_VIEWGL_EXPORT_VAR const ProgramInputDeclaration& uIsBillboard ();
    FLIRT_VIEWGL_EXPORT_VAR const ProgramInputDeclaration& uLightDirection ();
    FLIRT_VIEWGL_EXPORT_VAR const ProgramInputDeclaration& uPrimaryColor ();
    FLIRT_VIEWGL_EXPORT_VAR const ProgramInputDeclaration& uPrimaryMap ();
    FLIRT_VIEWGL_EXPORT_VAR const ProgramInputDeclaration& uModelViewScale ();
    FLIRT_VIEWGL_EXPORT_VAR const ProgramInputDeclaration& uMotionExponent ();
    FLIRT_VIEWGL_EXPORT_VAR const ProgramInputDeclaration& uMotionSign ();
    FLIRT_VIEWGL_EXPORT_VAR const ProgramInputDeclaration& uObjectStateFlags ();
    FLIRT_VIEWGL_EXPORT_VAR const ProgramInputDeclaration& uObjectId ();
    FLIRT_VIEWGL_EXPORT_VAR const ProgramInputDeclaration& uPreviousModelView ();
    FLIRT_VIEWGL_EXPORT_VAR const ProgramInputDeclaration& uPreviousModelViewProjection ();
    FLIRT_VIEWGL_EXPORT_VAR const ProgramInputDeclaration& uPreviousProjection ();
    FLIRT_VIEWGL_EXPORT_VAR const ProgramInputDeclaration& uRecTextureSize ();
    FLIRT_VIEWGL_EXPORT_VAR const ProgramInputDeclaration& uScalelessModelView ();
    FLIRT_VIEWGL_EXPORT_VAR const ProgramInputDeclaration& uScaling ();
    FLIRT_VIEWGL_EXPORT_VAR const ProgramInputDeclaration& uSecondaryColor ();
    FLIRT_VIEWGL_EXPORT_VAR const ProgramInputDeclaration& uSecondaryMap ();
    FLIRT_VIEWGL_EXPORT_VAR const ProgramInputDeclaration& uSelectionColor ();
    FLIRT_VIEWGL_EXPORT_VAR const ProgramInputDeclaration& uTertiaryColor ();
    FLIRT_VIEWGL_EXPORT_VAR const ProgramInputDeclaration& uTertiaryMap ();
    FLIRT_VIEWGL_EXPORT_VAR const ProgramInputDeclaration& uTextColor ();
    FLIRT_VIEWGL_EXPORT_VAR const ProgramInputDeclaration& uTextGlyphs ();
    FLIRT_VIEWGL_EXPORT_VAR const ProgramInputDeclaration& uUp ();
    FLIRT_VIEWGL_EXPORT_VAR const ProgramInputDeclaration& uUsedAsPoint ();
    FLIRT_VIEWGL_EXPORT_VAR const ProgramInputDeclaration& uVertexAttributes ();
}
}
}
