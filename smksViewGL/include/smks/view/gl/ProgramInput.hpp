// ========================================================================= //
// Copyright 2016-2018 SUPAMONKS_STUDIO                                      //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
//                                                                           //
// ========================================================================= //
#pragma once

#include <memory>
#include "smks/sys/configure_view_gl.hpp"

namespace osg
{
    class Uniform;
    class Texture2D;
}
namespace smks { namespace view { namespace gl
{
    class ProgramInputDeclaration;
    class UniformInput;
    class Sampler2dInput;

    //////////////////////
    // struct ProgramInput
    //////////////////////
    class FLIRT_VIEWGL_EXPORT ProgramInput
    {
    public:
        typedef std::shared_ptr<ProgramInput> Ptr;
    protected:
        inline
        ProgramInput()
        { }
    private:
        // non-copyable
        ProgramInput(const ProgramInput&);
        ProgramInput& operator=(const ProgramInput&);
    public:
        virtual inline
        ~ProgramInput()
        { }

        virtual inline const UniformInput*      asUniformInput() const      { return nullptr; }
        virtual inline UniformInput*            asUniformInput()            { return nullptr; }
        virtual inline const Sampler2dInput*    asSampler2dInput() const    { return nullptr; }
        virtual inline Sampler2dInput*          asSampler2dInput()          { return nullptr; }
    };

    //////////////////////
    // struct UniformInput
    //////////////////////
    class FLIRT_VIEWGL_EXPORT UniformInput:
        public ProgramInput
    {
    public:
        typedef std::shared_ptr<UniformInput> Ptr;
    private:
        class PImpl;
    private:
        PImpl *_pImpl;
    public:
        static inline
        Ptr
        create(const ProgramInputDeclaration& decl)
        {
            Ptr ptr(new UniformInput(decl));
            return ptr;
        }
    protected:
        explicit
        UniformInput(const ProgramInputDeclaration&);
    public:
        ~UniformInput();

        osg::Uniform* data();

        virtual inline const UniformInput*  asUniformInput() const  { return this; }
        virtual inline UniformInput*        asUniformInput()        { return this; }
    };

    ////////////////////////
    // struct Sampler2dInput
    ////////////////////////
    class FLIRT_VIEWGL_EXPORT Sampler2dInput:
        public ProgramInput
    {
    public:
        typedef std::shared_ptr<Sampler2dInput> Ptr;
    private:
        class PImpl;
    private:
        PImpl *_pImpl;
    public:
        static inline
        Ptr
        create(const ProgramInputDeclaration& decl, int unit)
        {
            Ptr ptr(new Sampler2dInput(decl, unit));
            return ptr;
        }
    protected:
        Sampler2dInput(const ProgramInputDeclaration&, int unit);
    public:
        ~Sampler2dInput();

        osg::Uniform*   sampler2d       ();
        osg::Uniform*   glTransformUV   ();
        osg::Uniform*   transformUV     ();
        osg::Uniform*   scale           ();
        osg::Uniform*   valid           ();
        osg::Texture2D* texture2d       ();

        virtual inline const Sampler2dInput*    asSampler2dInput() const    { return this; }
        virtual inline Sampler2dInput*          asSampler2dInput()          { return this; }
    };
}
}
}
