// ========================================================================= //
// Copyright 2016-2018 SUPAMONKS_STUDIO                                      //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
//                                                                           //
// ========================================================================= //
#pragma once

#include "smks/view/gl/ProgramInputDeclaration.hpp"

namespace smks { namespace view { namespace gl
{
    class UniformInput;

    class FLIRT_VIEWGL_EXPORT UniformDeclaration:
        public ProgramInputDeclaration
    {
        friend class UniformInput;
    public:
        typedef std::shared_ptr<UniformDeclaration> Ptr;
    private:
        class PImpl;
    private:
        PImpl *_pImpl;

    public:
        static
        Ptr
        create(const char*,
               osg::Uniform::Type,
               osg::Object::DataVariance = osg::Object::UNSPECIFIED);
    private:
        UniformDeclaration(const char*,
                           osg::Uniform::Type,
                           osg::Object::DataVariance);
    public:
        ~UniformDeclaration();

        std::shared_ptr<UniformInput>
        createInstance() const;

        osg::Uniform::Type
        type() const;

        osg::Object::DataVariance
        dataVariance() const;
    private:
        osg::ref_ptr<osg::Uniform>
        createUniformData() const;
    public:
        virtual inline const UniformDeclaration* asUniformDeclaration() const { return this; }
    };
}
}
}
