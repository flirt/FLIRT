// ========================================================================= //
// Copyright 2016-2018 SUPAMONKS_STUDIO                                      //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
//                                                                           //
// ========================================================================= //
#pragma once

#include <string>
#include <memory>

#include "smks/view/gl/ProgramInput.hpp"
#include "smks/view/gl/uniform.hpp"
#include "smks/sys/configure_view_gl.hpp"

namespace osg
{
    class Uniform;
    class Program;
}

namespace smks
{
    namespace parm
    {
        class BuiltIn;
    }
    namespace xchg
    {
        class IdSet;
    }
    namespace sys
    {
        class ResourceDatabase;
    }

    namespace view { namespace gl
    {
        class ProgramInputDeclaration;
        class UniformDeclaration;
        class Sampler2dDeclaration;
        class VertexAttrib;
        class ProgramTemplate;

        class FLIRT_VIEWGL_EXPORT MaterialProgramBase
        {
        public:
            typedef std::shared_ptr<MaterialProgramBase> Ptr;
        private:
            class PImpl;
        private:
            PImpl *_pImpl;

        protected:
            MaterialProgramBase(const char* alias, const ProgramTemplate&);
        private:
            // non-copyable
            MaterialProgramBase(const MaterialProgramBase&);
            MaterialProgramBase& operator=(const MaterialProgramBase&);
        public:
            virtual ~MaterialProgramBase();
        protected:
            //-----------------------------
            virtual inline void build() = 0;
            //-----------------------------
            void
            parmAffectsInput(const parm::BuiltIn&, const ProgramInputDeclaration&);
        public:
            const char*
            alias() const;
            unsigned int
            id() const;
            const ProgramTemplate&
            progTemplate() const;

            const xchg::IdSet&
            getInputs() const;
            void
            getParmsAffectingInput(unsigned int, xchg::IdSet&) const;
            void
            getInputsAffectedByParm(unsigned int, xchg::IdSet&) const;
        };
    }
}
}
