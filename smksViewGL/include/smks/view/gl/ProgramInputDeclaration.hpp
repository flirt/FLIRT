// ========================================================================= //
// Copyright 2016-2018 SUPAMONKS_STUDIO                                      //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
//                                                                           //
// ========================================================================= //
#pragma once

#include <memory>
#include <osg/Uniform>

#include "smks/sys/configure_view_gl.hpp"

namespace smks { namespace view { namespace gl
{
    class UniformDeclaration;
    class Sampler2dDeclaration;

    class FLIRT_VIEWGL_EXPORT ProgramInputDeclaration
    {
    public:
        typedef std::shared_ptr<ProgramInputDeclaration> Ptr;
    private:
        class PImpl;
    private:
        PImpl *_pImpl;

    protected:
        explicit
        ProgramInputDeclaration(const char*);
    private:
        // non-copyable
        ProgramInputDeclaration(const ProgramInputDeclaration&);
        ProgramInputDeclaration& operator=(const ProgramInputDeclaration&);
    public:
        virtual
        ~ProgramInputDeclaration();

        unsigned int
        id() const;

        const char*
        name() const;

        virtual inline const UniformDeclaration*    asUniformDeclaration()      const { return nullptr; }
        virtual inline const Sampler2dDeclaration*  asSampler2dDeclaration()    const { return nullptr; }
    };
}
}
}
