// ========================================================================= //
// Copyright 2016-2018 SUPAMONKS_STUDIO                                      //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
//                                                                           //
// ========================================================================= //
#pragma once

#include <string>
#include <memory>

#include "smks/view/gl/ProgramInput.hpp"
#include "smks/view/gl/uniform.hpp"
#include "smks/sys/configure_view_gl.hpp"

namespace osg
{
    class Uniform;
    class Program;
}

namespace smks
{
    namespace sys
    {
        class ResourceDatabase;
    }

    namespace view { namespace gl
    {
        class ProgramInputDeclaration;
        class UniformDeclaration;
        class Sampler2dDeclaration;
        class VertexAttrib;

        class FLIRT_VIEWGL_EXPORT ProgramTemplate
        {
        public:
            typedef std::shared_ptr<ProgramTemplate> Ptr;
        private:
            class PImpl;
        private:
            PImpl *_pImpl;
        protected:
            ProgramTemplate(const char* vtx,
                            const char* tessctrl,
                            const char* tesseval,
                            const char* geom,
                            const char* frag,
                            const char* comp);
        private:
            // non-copyable
            ProgramTemplate(const ProgramTemplate&);
            ProgramTemplate& operator=(const ProgramTemplate&);
        public:
            virtual ~ProgramTemplate();

            const char*
            shader(int glShaderType) const;

            const char*
            key() const;

            unsigned int
            id() const;

            ProgramInput::Ptr   createInput     (const ProgramInputDeclaration&) const;
            UniformInput::Ptr   createUniform   (const ProgramInputDeclaration&,                osg::Object::DataVariance = osg::Object::UNSPECIFIED) const;
            UniformInput::Ptr   createUniform   (const ProgramInputDeclaration&, bool,          osg::Object::DataVariance = osg::Object::UNSPECIFIED) const;
            UniformInput::Ptr   createUniform   (const ProgramInputDeclaration&, unsigned int,  osg::Object::DataVariance = osg::Object::UNSPECIFIED) const;
            UniformInput::Ptr   createUniform   (const ProgramInputDeclaration&, int,           osg::Object::DataVariance = osg::Object::UNSPECIFIED) const;
            UniformInput::Ptr   createUniform   (const ProgramInputDeclaration&, const int*,    osg::Object::DataVariance = osg::Object::UNSPECIFIED) const;
            UniformInput::Ptr   createUniform   (const ProgramInputDeclaration&, float,         osg::Object::DataVariance = osg::Object::UNSPECIFIED) const;
            UniformInput::Ptr   createUniform   (const ProgramInputDeclaration&, const float*,  osg::Object::DataVariance = osg::Object::UNSPECIFIED) const;
        private:
            Sampler2dInput::Ptr createSampler2d (const ProgramInputDeclaration&) const;
            //----------------------------------------------------------------------------------------------
        public:
            virtual inline void finalize(osg::Program&) const = 0;
        protected:
            virtual inline bool isInputRelevant     (unsigned int inputId) const                = 0;
            virtual inline void setUniformDefaults  (unsigned int inputId, osg::Uniform&) const = 0;
            virtual inline int  getSampler2dTexUnit (unsigned int inputId) const                = 0;
            //----------------------------------------------------------------------------------------------
        };

        FLIRT_VIEWGL_EXPORT
        osg::ref_ptr<osg::Program>
        getOrCreateProgram(const ProgramTemplate&, std::shared_ptr<sys::ResourceDatabase> const&);
    }
}
}
