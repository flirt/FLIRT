// ========================================================================= //
// Copyright 2016-2018 SUPAMONKS_STUDIO                                      //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
//                                                                           //
// ========================================================================= //
#pragma once

#include <memory>
#include <osg/GLDefines>
#include <osg/Array>
#include "smks/sys/configure_view_gl.hpp"

namespace osg
{
    class Program;
    class Geometry;
    class GLExtensions;
}

namespace smks { namespace view { namespace gl
{
    class FLIRT_VIEWGL_EXPORT VertexAttrib
    {
    public:
        typedef std::shared_ptr<VertexAttrib> Ptr;
    private:
        class PImpl;
    private:
        PImpl *_pImpl;
    public:
        static
        Ptr
        create(const char*,
               unsigned int location,
               GLenum       glType,
               size_t       numComponents,
               bool);

    private:
        VertexAttrib(const char*,
                     unsigned int,
                     GLenum,
                     size_t numComponents,
                     bool);
        // non-copyable
        VertexAttrib(const VertexAttrib&);
        VertexAttrib& operator=(const VertexAttrib&);
    public:
        ~VertexAttrib();

        unsigned int
        id() const;

        void
        bindLocation(osg::Program&) const;
        void
        bindArray(osg::Geometry&, osg::Array*, osg::Array::Binding) const;
        void
        glEnableVertexAttribArray(const osg::GLExtensions*) const;
        void
        glVertexAttribPointer(const osg::GLExtensions*, GLsizei stride, void*) const;
        void
        glDisableVertexAttribArray(const osg::GLExtensions*) const;
    };
}
}
}
