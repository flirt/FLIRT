// ========================================================================= //
// Copyright 2016-2018 SUPAMONKS_STUDIO                                      //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
//                                                                           //
// ========================================================================= //
#pragma once

#include <memory>
#include <osg/ref_ptr>
#include <osg/buffered_value>

#include <osg/GLExtensions>
#include <osg/FrameBufferObject>

#include "smks/sys/configure_view_gl.hpp"

namespace smks
{
    namespace sys
    {
        class ResourceDatabase;
    }

    namespace view { namespace gl
    {
        class FLIRT_VIEWGL_EXPORT ContextExtensions
        {
        public:
            typedef std::shared_ptr<ContextExtensions> Ptr;
        private:
            class PImpl;
        private:
            PImpl* _pImpl;

        public:
            static inline
            Ptr
            create()
            {
                Ptr ptr(new ContextExtensions());
                return ptr;
            }

        protected:
            ContextExtensions();
        private:
            // non-copyable
            ContextExtensions(const ContextExtensions&);
            ContextExtensions& operator=(const ContextExtensions&);
        public:
            ~ContextExtensions();

            const osg::GLExtensions*
            getOrCreateGLExtension(unsigned int contextId);

            void
            unbindVertexArray();
        };

        FLIRT_VIEWGL_EXPORT
        ContextExtensions::Ptr const&
        getOrCreateContextExtensions(sys::ResourceDatabase&);

        FLIRT_VIEWGL_EXPORT
        ContextExtensions*
        getContextExtensions(sys::ResourceDatabase&);

        FLIRT_VIEWGL_EXPORT
        const ContextExtensions*
        getContextExtensions(const sys::ResourceDatabase&);
    }
}
}
