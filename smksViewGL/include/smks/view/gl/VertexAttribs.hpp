// ========================================================================= //
// Copyright 2016-2018 SUPAMONKS_STUDIO                                      //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
//                                                                           //
// ========================================================================= //
#pragma once

#include "smks/sys/configure_view_gl.hpp"

namespace smks { namespace view { namespace gl
{
    class VertexAttrib;

    FLIRT_VIEWGL_EXPORT_VAR const VertexAttrib& iColor ();
    FLIRT_VIEWGL_EXPORT_VAR const VertexAttrib& iPosition ();
    FLIRT_VIEWGL_EXPORT_VAR const VertexAttrib& iPreviousPosition ();
    FLIRT_VIEWGL_EXPORT_VAR const VertexAttrib& iWidth ();
}
}
}
