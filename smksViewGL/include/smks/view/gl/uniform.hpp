// ========================================================================= //
// Copyright 2016-2018 SUPAMONKS_STUDIO                                      //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
//                                                                           //
// ========================================================================= //
#pragma once

#include <osg/ref_ptr>
#include <osg/Object>

#include <smks/math/types.hpp>
#include "smks/sys/configure_view_gl.hpp"

namespace osg
{
    class Uniform;
}

namespace smks { namespace view { namespace gl
{
    FLIRT_VIEWGL_EXPORT void setUniformb (osg::Uniform&, bool);
    FLIRT_VIEWGL_EXPORT void setUniformi (osg::Uniform&, int);
    FLIRT_VIEWGL_EXPORT void setUniformiv(osg::Uniform&, const int*);
    FLIRT_VIEWGL_EXPORT void setUniformf (osg::Uniform&, float);
    FLIRT_VIEWGL_EXPORT void setUniformfv(osg::Uniform&, const float*);
    FLIRT_VIEWGL_EXPORT void setUniformui(osg::Uniform&, unsigned int);  //<! interpreted as RGBA8 color depending on the uniform's type
}
}
}
