// ========================================================================= //
// Copyright 2016-2018 SUPAMONKS_STUDIO                                      //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
//                                                                           //
// ========================================================================= //
#pragma once

#include "smks/sys/configure_view_gl.hpp"

namespace smks { namespace view { namespace gl
{
    class ProgramTemplate;

    FLIRT_VIEWGL_EXPORT_VAR const ProgramTemplate& get_BasicProgram ();
    FLIRT_VIEWGL_EXPORT_VAR const ProgramTemplate& get_BlinnPhongProgram ();
    FLIRT_VIEWGL_EXPORT_VAR const ProgramTemplate& get_EyeProgram ();
    FLIRT_VIEWGL_EXPORT_VAR const ProgramTemplate& get_CurvesProgram ();
    FLIRT_VIEWGL_EXPORT_VAR const ProgramTemplate& get_LineProgram ();
    FLIRT_VIEWGL_EXPORT_VAR const ProgramTemplate& get_ManipulatorProgram ();
    FLIRT_VIEWGL_EXPORT_VAR const ProgramTemplate& get_MotionProgram ();
    FLIRT_VIEWGL_EXPORT_VAR const ProgramTemplate& get_NormalProgram ();
    FLIRT_VIEWGL_EXPORT_VAR const ProgramTemplate& get_PointsProgram ();
    FLIRT_VIEWGL_EXPORT_VAR const ProgramTemplate& get_SceneObjLocatorProgram ();
    FLIRT_VIEWGL_EXPORT_VAR const ProgramTemplate& get_TexCoordProgram ();
    FLIRT_VIEWGL_EXPORT_VAR const ProgramTemplate& get_TextProgram ();
    FLIRT_VIEWGL_EXPORT_VAR const ProgramTemplate& get_TextureProgram ();
}
}
}
