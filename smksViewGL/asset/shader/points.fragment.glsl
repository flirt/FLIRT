// ========================================================================= //
// Copyright 2016-2018 SUPAMONKS_STUDIO                                      //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
//                                                                           //
// ========================================================================= //
#version 140
//---------------------------------

uniform float   uClockTime;
uniform vec3    uSelectionColor;
uniform ivec4   uObjectId;
uniform int     uObjectStateFlags;

uniform vec4    uColor;
//---------------------------------
out vec4 oColors[2];

float
getClockTimeBeat()
{
    float two_pi = 6.28318530718;
    return 0.5 * (1.0 + cos(uClockTime * two_pi));  //  1 pulsation per second
}

void
main(void)
{
    bool isSelected = (uObjectStateFlags & 1 << 0) != 0;
    vec3 color = isSelected ? mix(uSelectionColor, uColor.xyz, getClockTimeBeat()) : uColor.xyz;

    //###########################################
    oColors[0] = vec4(color, 1.0);
    oColors[1] = vec4(uObjectId / 255.0);
    //###########################################
}
