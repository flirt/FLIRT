// ========================================================================= //
// Copyright 2016-2018 SUPAMONKS_STUDIO                                      //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
//                                                                           //
// ========================================================================= //
#version 140

uniform sampler2D uDiffuseTexture;
uniform vec2      uRecTextureSize;
uniform bool      uAntiAlias = true;

in      vec4      vColor;
in      vec4      vTexCoord0;

out     vec4      oColor;

float
vignetting()
{
  float v = (vTexCoord0.x * vTexCoord0.y * (1.0 - vTexCoord0.x) * (1.0 - vTexCoord0.y)) / 0.0625;
  v       = pow(v, 0.15);

  return v;
}

float
getLuminance(vec4 rgba)
{
  vec4 LUMA = vec4(0.299, 0.587, 0.114, 0.0);

  return dot(LUMA, rgba);
}

vec4
getLumaNwNeSwSe(vec2 texCoord)
{
  return vec4(
    getLuminance(texture2D(uDiffuseTexture, texCoord + (vec2(-1.0, -1.0) * uRecTextureSize))),
    getLuminance(texture2D(uDiffuseTexture, texCoord + (vec2(+1.0, -1.0) * uRecTextureSize))),
    getLuminance(texture2D(uDiffuseTexture, texCoord + (vec2(-1.0, +1.0) * uRecTextureSize))),
    getLuminance(texture2D(uDiffuseTexture, texCoord + (vec2(+1.0, +1.0) * uRecTextureSize)))
  );
}

float
getLumaNwNeSwSeMax(vec4 LumaNwNeSwSe)
{
  vec2 tmp = max(LumaNwNeSwSe.xy, LumaNwNeSwSe.zw);

  return max(tmp.x, tmp.y);
}

float
getLumaNwNeSwSeMin(vec4 LumaNwNeSwSe)
{
  vec2 tmp = min(LumaNwNeSwSe.xy, LumaNwNeSwSe.zw);

  return min(tmp.x, tmp.y);
}

vec4
fxaa(vec2 texCoord)
{
  float   MIN_REDUCE          = 1.0 / 128.0; // prevents division by zero when normalizing smallest component of blur direction
  float   MUL_REDUCE          = 1.0 / 8.0; // bias direction reduction
  float   MAX_SPAN            = 8.0;

  vec4    lumaNwNeSwSe        = getLumaNwNeSwSe(texCoord);
  lumaNwNeSwSe.y              += 1.0 / 384.0;

  float   lumaMax             = getLumaNwNeSwSeMax(lumaNwNeSwSe);
  float   lumaMin             = getLumaNwNeSwSeMin(lumaNwNeSwSe);

  vec4    rgbaM               = texture2D(uDiffuseTexture, texCoord);
  float   lumaM               = getLuminance(rgbaM);
  float   lumaMaxM            = max(lumaMax, lumaM);
  float   lumaMinM            = min(lumaMin, lumaM);

  // if ((lumaMaxM - lumaMinM) < max(lumaMax * EDGE_THRESHOLD, MIN_EDGE_THRESHOLD))
  //   return rgbaM; // no edge

  // get blur direction
  // dx = - ((Nw + Ne) - (Sw + Se))
  // dy = (Nw + Sw) - (Ne + Se)

  vec2  dir = normalize(vec2(
    dot(lumaNwNeSwSe, vec4(-1.0, -1.0, 1.0, 1.0)),
    dot(lumaNwNeSwSe, vec4(1.0, -1.0, 1.0, -1.0))
  ));

  float dirReduce = max(MIN_REDUCE, MUL_REDUCE * dot(lumaNwNeSwSe, vec4(0.25)));

  dir   /= dirReduce + min(abs(dir.x), abs(dir.y));
  dir   = min(vec2(MAX_SPAN), max(vec2(-MAX_SPAN), dir));
  dir   *= uRecTextureSize;

  vec4  rgba1   = 0.5 * (
    texture2D(uDiffuseTexture, texCoord - dir / 6.0) +  // 1.0/3.0 - 0.5
    texture2D(uDiffuseTexture, texCoord + dir / 6.0)    // 2.0/3.0 - 0.5
  );
  vec4  rgba2   = 0.5 * rgba1 + 0.25 * (
    texture2D(uDiffuseTexture, texCoord - dir * 0.5) +  // 0.0/3.0 - 0.5
    texture2D(uDiffuseTexture, texCoord + dir * 0.5)    // 3.0/3.0 - 0.5
  );

  float lumaRgba2 = getLuminance(rgba2);

  return lumaMinM < lumaRgba2 && lumaRgba2 < lumaMaxM
  ? rgba2
  : rgba1;
}

void
main()
{
  oColor = uAntiAlias
    ? vColor * fxaa(vTexCoord0.xy)
    : vColor * texture2D(uDiffuseTexture, vTexCoord0.xy);

  // float v     = vignetting();
  // oColor.xyz  *= v;
  // oColor.a    = 1.0 - max(0.0, v - oColor.a);
}
