// ========================================================================= //
// Copyright 2016-2018 SUPAMONKS_STUDIO                                      //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
//                                                                           //
// ========================================================================= //
#version 120
#extension GL_EXT_geometry_shader4 : enable

uniform mat4 osg_ProjectionMatrix;

vec4
evalCubicBezier(float t, vec4 p0, vec4 p1, vec4 p2, vec4 p3)
{
    float one_minus_t = 1.0 - t;

    return one_minus_t * one_minus_t * one_minus_t * p0
        + 3.0 * one_minus_t * one_minus_t * t * p1
        + 3.0 * one_minus_t * t * t * p2
        + t * t * t * p3;
}

vec4
evalCubicBezierHorner(float t, vec4 p0, vec4 p1, vec4 p2, vec4 p3)
{
    vec4 p10 = p1 - p0;
    vec4 p21 = p2 - p1;
    vec4 p30 = p3 - p0;

    // bezier(t) = a*t*t*t + b*t*t + c*t + p0
    vec4 a = p30 - 3.0 * p21;
    vec4 b = 3.0 * (p21 - p10);
    vec4 c = 3.0 * p10;

    return p0 + t * (c + t * (b + t * a));
}

void
emitSliceVertices(vec4 pos, vec4 tgt, float width)
{
    vec4 nrml = normalize(vec4(tgt.y, -tgt.x, 0.0, 0.0));

    gl_Position = pos + width * nrml;
    EmitVertex();

    gl_Position = pos - width * nrml;
    EmitVertex();
}

void
evalCubicBezierFwdDifferences(vec4 position0,
                              vec4 position1,
                              vec4 position2,
                              vec4 position3,
                              float width0,
                              float width1,
                              float width2,
                              float width3)
{
    vec4 p0     = osg_ProjectionMatrix * position0;
    vec4 p10    = osg_ProjectionMatrix * (position1 - position0);
    vec4 p21    = osg_ProjectionMatrix * (position2 - position1);
    vec4 p30    = osg_ProjectionMatrix * (position3 - position0);

    float w0    = 0.5 * width0;
    float w10   = 0.5 * (width1 - width0);
    float w21   = 0.5 * (width2 - width1);
    float w30   = 0.5 * (width3 - width0);

    float h = 0.05; // 1 / 20

    // bezier(t) = Ba * t^3 + Bb * t^2 + Bc * t + p0
    vec4 Ba = p30 - 3.0 * p21;
    vec4 Bb = 3.0 * (p21 - p10);
    vec4 Bc = 3.0 * p10;
    // bezier(t + h) - bezier(t) = Bd * t^2 + Be * t + Bf
    vec4 Bd = h * 3.0 * Ba;
    vec4 Be = h * (2.0 * Bb + h * 3.0 * Ba);
    vec4 Bf = h * (Bc + h * (Bb + h * Ba));

    vec3 Wabc = vec3(
        w30 - 3.0 * w21,
        3.0 * (w21 - w10),
        3.0 * w10);
    // width(t + h) - width(t) = dot(Wdef, vec3(t^2, t, 1.0))
    vec3 Wdef = vec3(
        h * 3.0 * Wabc.x,
        h * (2.0 * Wabc.y + h * 3.0 * Wabc.x),
        h * (Wabc.z + h * (Wabc.y + h * Wabc.x)));

    // dBdt(t) = 3 * Ba * t^2 + 2 * Bb * t + Bc
    // dBdt(t + h) - dBdt(t) = Ta * t + Tb
    vec4 Ta = h * 6.0 * Ba;
    vec4 Tb = h * (2.0 * Bb + h * 3.0 * Ba);

    vec4    pos     = p0;
    vec4    tgt     = Bc;
    float   width   = w0;
    emitSliceVertices(pos, tgt, width);

    float t = 0.0;
    for (int i = 0; i < 20; ++i)
    {
        pos     += (Bf + t * (Be + t * Bd));
        tgt     +=  Tb + t * Ta;
        width   += dot(Wdef, vec3(t * t, t, 1.0));
        emitSliceVertices(pos, tgt, width);

        t += h;
    }
    EndPrimitive();
}

void
main(void)
{
    evalCubicBezierFwdDifferences(
        gl_PositionIn[0],
        gl_PositionIn[1],
        gl_PositionIn[2],
        gl_PositionIn[3],
        gl_PointSizeIn[0],
        gl_PointSizeIn[1],
        gl_PointSizeIn[2],
        gl_PointSizeIn[3]);
}
