// ========================================================================= //
// Copyright 2016-2018 SUPAMONKS_STUDIO                                      //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
//                                                                           //
// ========================================================================= //
#version 140

//---------------------------------

uniform mat4    osg_ModelViewMatrix;
uniform mat4    osg_ProjectionMatrix;
uniform mat3    osg_NormalMatrix;

uniform vec3    uLightDirection;
//---------------------------------
uniform int     uVertexAttributes;

uniform vec4    uPrimaryMap_glTransformUV;
uniform vec4    uPrimaryMap_transformUV;
uniform bool    uPrimaryMap_valid;
//---
uniform vec4    uSecondaryMap_glTransformUV;
uniform vec4    uSecondaryMap_transformUV;
uniform bool    uSecondaryMap_valid;
//---
uniform vec4    uTertiaryMap_glTransformUV;
uniform vec4    uTertiaryMap_transformUV;
uniform bool    uTertiaryMap_valid;
//---------------------------------

in  vec4    osg_Vertex;
in  vec3    osg_Normal;
in  vec2    osg_MultiTexCoord0;
//---------------------------------

out vec3    vNormal;            // in eye space
out vec3    vEyeVector;         // in eye space
out vec3    vLightDirection;    // in eye space
out vec4    vTexCoords_1_2;     // transformed texture coordinates for primary and secondary color maps
out vec2    vTexCoords_3;       // transformed texture coordinates for tertiary color map

/*
float
upwardSlope(float x)
{
    return x*x*(3.0 - 2.0*x);
}

float
downwardSlope(float x)
{
    return 1.0 + x*x*(2.0*x - 3.0);
}

vec4
getCurrentColor(float hasNormal)
{
    if (hasNormal > 0.5)
        return uMeshDiffuseColor;
    else
    {
        float PERIOD    = 2.0;
        float UP_TIME   = 0.3;
        float w         = mod(uClockTime, PERIOD) / PERIOD;

        w = w < UP_TIME
        ? upwardSlope(w / UP_TIME)
        : downwardSlope((w - UP_TIME) / (1.0 - UP_TIME));

        return w * uNoNormalColor + (1.0 - w) * uMeshDiffuseColor;
    }
}
*/
vec2
transformTexCoords(vec2 texCoords, bool map_valid, vec4 map_transformUV, vec4 map_glTransformUV)
{
    vec2 oTexCoords = texCoords;
    // user UV transform
    oTexCoords *= map_valid ? map_transformUV.xy : vec2(1.0);
    oTexCoords += map_valid ? map_transformUV.zw : vec2(0.0);
    // GL display UV transform
    oTexCoords *= map_valid ? map_glTransformUV.xy : vec2(1.0);
    oTexCoords += map_valid ? map_glTransformUV.zw : vec2(0.0);
    return oTexCoords;
}

void
main()
{
    bool hasNormals     = (uVertexAttributes & (1 << 1)) != 0;
    bool hasTexCoords   = (uVertexAttributes & (1 << 2)) != 0;

    vec4 pos = osg_ModelViewMatrix * osg_Vertex;    // in eye space

    vNormal = normalize(
        osg_NormalMatrix * (hasNormals ? osg_Normal : vec3(0.0, 1.0, 0.0)));
    vEyeVector = normalize(- pos.xyz);  // in eye space, from position to eye
    vLightDirection = normalize(mat3(osg_ModelViewMatrix) * uLightDirection);

    vTexCoords_1_2  = hasTexCoords ? vec4(osg_MultiTexCoord0, osg_MultiTexCoord0) : vec4(0.0);
    vTexCoords_3    = hasTexCoords ? osg_MultiTexCoord0 : vec2(0.0);
    // transform texture coordinates for valid color maps
    vTexCoords_1_2.xy   = transformTexCoords(vTexCoords_1_2.xy,     uPrimaryMap_valid,      uPrimaryMap_transformUV,    uPrimaryMap_glTransformUV);
    vTexCoords_1_2.zw   = transformTexCoords(vTexCoords_1_2.zw,     uSecondaryMap_valid,    uSecondaryMap_transformUV,  uSecondaryMap_glTransformUV);
    vTexCoords_3        = transformTexCoords(vTexCoords_3,          uTertiaryMap_valid,     uTertiaryMap_transformUV,   uTertiaryMap_glTransformUV);
    //###########################################
    gl_Position = osg_ProjectionMatrix * pos;
    //###########################################
}
