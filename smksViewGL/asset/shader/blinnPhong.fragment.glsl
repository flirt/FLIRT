// ========================================================================= //
// Copyright 2016-2018 SUPAMONKS_STUDIO                                      //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
//                                                                           //
// ========================================================================= //
#version 140
//---------------------------------

uniform float   uClockTime;
uniform vec3    uSelectionColor;
uniform ivec4   uObjectId;
uniform int     uObjectStateFlags;

uniform vec4        uPrimaryColor;
uniform sampler2D   uPrimaryMap;
uniform vec4        uPrimaryMap_scale;
uniform bool        uPrimaryMap_valid;
//---
uniform vec4        uSecondaryColor;
uniform sampler2D   uSecondaryMap;
uniform vec4        uSecondaryMap_scale;
uniform bool        uSecondaryMap_valid;
//---
uniform vec4        uTertiaryColor;
uniform sampler2D   uTertiaryMap;
uniform vec4        uTertiaryMap_scale;
uniform bool        uTertiaryMap_valid;
//---------------------------------

in  vec3    vNormal;            // in eye space
in  vec3    vEyeVector;         // in eye space
in  vec3    vLightDirection;    // in eye space
in  vec4    vTexCoords_1_2;     // transformed texture coordinates for primary and secondary color maps
in  vec2    vTexCoords_3;       // transformed texture coordinates for tertiary color map
//---------------------------------

out vec4    oColors[2];

float
sqr(float x)
{
    return x*x;
}

float
getClockTimeBeat()
{
    float two_pi = 6.28318530718;
    return 0.5 * (1.0 + cos(uClockTime * two_pi));  //  1 pulsation per second
}

void
main()
{
    vec3 normal     = normalize(vNormal);
    vec3 eyeVector  = normalize(vEyeVector);
    vec3 lightDir   = normalize(vLightDirection);

    vec4 primaryColor   = uPrimaryMap_valid     ? uPrimaryMap_scale * texture2D(uPrimaryMap, vTexCoords_1_2.xy)     : uPrimaryColor;
    vec4 secondaryColor = uSecondaryMap_valid   ? uSecondaryMap_scale * texture2D(uSecondaryMap, vTexCoords_1_2.zw)     : uSecondaryColor;
    vec4 tertiaryColor  = uTertiaryMap_valid    ? uTertiaryMap_scale * texture2D(uTertiaryMap, vTexCoords_3)        : uTertiaryColor;

    // lambertian shading
    float wrap = 0.25 * sqr(max(0.0, dot(normal, lightDir)) + 1.0); // Valve's wrap shading model

    // blinn-phong shading
    vec3 halfVec = normalize(eyeVector + lightDir);
    float specular = max(0.0, dot(normal, halfVec));
    specular = sqr(sqr(sqr(specular))); //  shininess = 8

    vec3 color = wrap * primaryColor.xyz + specular * secondaryColor.xyz;

    bool    isSelected = (uObjectStateFlags & 1 << 0) != 0;
    float   selectGlow = smoothstep(0.5, 0.75, max(0.0, dot(normal, eyeVector)));
    vec3    selectColor = mix(uSelectionColor, color, selectGlow);
    color = isSelected ? mix(selectColor, color, getClockTimeBeat()) : color;

    //###########################################
    oColors[0] = vec4(color, 1.0);
    oColors[1] = vec4(uObjectId / 255.0);
    //###########################################
}
