// ========================================================================= //
// Copyright 2016-2018 SUPAMONKS_STUDIO                                      //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
//                                                                           //
// ========================================================================= //
#version 140

uniform mat4    osg_ModelViewMatrix;
uniform mat4    osg_ProjectionMatrix;
uniform mat4    osg_ModelViewProjectionMatrix;
uniform mat3    osg_NormalMatrix;
uniform vec3    uLightPosition = vec3(0.5, 0.5, 0.5);

in      vec4    osg_Vertex;
in      vec3    osg_Normal;
//in    vec4    osg_Color;
//in    vec4    osg_MultiTexCoord0;

out     vec4    vPosition;  // in eye space
out     vec3    vNormal;    // in eye space
out     vec4    vLightPosition; // in eye space
//out   vec4    vColor;
//out   vec4    vTexCoord0;

void
main()
{
    vPosition       = osg_ModelViewMatrix * osg_Vertex;
    vNormal         = normalize(osg_NormalMatrix * osg_Normal);
    vLightPosition  = osg_ModelViewMatrix * vec4(uLightPosition, 1.0);
    // vColor       = osg_Color;
    // vTexCoord0   = osg_MultiTexCoord0;

    gl_Position = osg_ProjectionMatrix * vPosition;
}
