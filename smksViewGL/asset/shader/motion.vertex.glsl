// ========================================================================= //
// Copyright 2016-2018 SUPAMONKS_STUDIO                                      //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
//                                                                           //
// ========================================================================= //
#version 140

uniform mat4    osg_ModelViewMatrix;
uniform mat4    osg_ProjectionMatrix;
uniform mat4    osg_ModelViewProjectionMatrix;
uniform mat4    uPreviousModelView;
uniform mat4    uPreviousProjection;
uniform mat4    uPreviousModelViewProjection;

in      vec4    osg_Vertex;
in      vec3    iPreviousPosition;

out     vec4    vPreviousPosition; // clip coordinates
out     vec4    vCurrentPosition; // clip coordinates

void
main()
{
    vPreviousPosition   = uPreviousModelViewProjection  * vec4(iPreviousPosition, osg_Vertex.w);
    vCurrentPosition    = osg_ModelViewProjectionMatrix * osg_Vertex;

    //vPreviousPosition = uPreviousProjection * uPreviousModelView  * vec4(iPreviousPosition, osg_Vertex.w);
    //vCurrentPosition  = osg_ProjectionMatrix  * osg_ModelViewMatrix * osg_Vertex;

    gl_Position         = vCurrentPosition;
}
