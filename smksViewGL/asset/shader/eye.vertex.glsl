// ========================================================================= //
// Copyright 2016-2018 SUPAMONKS_STUDIO                                      //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
//                                                                           //
// ========================================================================= //
#version 140

//-------------------------------------
uniform mat4    osg_ModelViewMatrix;
uniform mat4    osg_ViewMatrix;
uniform mat4    osg_ProjectionMatrix;
uniform mat3    osg_NormalMatrix;
uniform float   uClockTime;

in  vec4    osg_Vertex;
in  vec3    osg_Normal;
in  vec2    osg_MultiTexCoord0;
//-------------------------------------
uniform vec4    uCenter;    // in world space
uniform vec4    uFront;     // in world space
uniform vec4    uUp;        // in world space
uniform bool    uUsedAsPoint;
uniform float   uEyeCorneaIOR;
uniform vec4    uEyeGlobeColor;
uniform float   uEyeHighlightAngSizeD;
uniform vec4    uEyeHighlightColor;
uniform float   uEyeHighlightSmoothness;
uniform vec2    uEyeHighlightCoordsD;
uniform float   uEyeIrisAngSizeD;
uniform vec4    uEyeIrisColor;
uniform float   uEyeIrisDepth;
uniform vec2    uEyeIrisDimensions;
uniform float   uEyeIrisEdgeBlur;
uniform float   uEyeIrisEdgeOffset;
uniform float   uEyeIrisNormalSmoothness;
uniform float   uEyeIrisRotationD;
uniform float   uEyePupilBlur;
uniform vec2    uEyePupilDimensions;
uniform float   uEyePupilSize;
uniform float   uEyeRadius;

out vec4    vVertex;    // in eye space
out vec3    vNormal;    // in eye space

out vec3    vFront;         // in eye space
out vec3    vUp;            // in eye space
out vec3    vSide;          // in eye space
out vec3    vHighlightDir;  // in eye space
out vec2    vIrisRDimensions;   // reciprocal
out vec3    vIrisConeTipOffset;
out vec2    vIrisConeShape;     // ( x: cos(ang)^2, y: rcp(length) )
out vec4    vPrecomp[2];        // [0]: ( xy: cos(ang) steps for inside/outside highlight, yz: ang steps for inside/outside iris )
                                // [1]: ( xy: ang steps for normal bevel, z: pupil width * pupil height, w: rcp(cornea IOR) )

void
computeIrisFrame(out vec3 front, out vec3 up, out vec3 side)
{
    // axes are expressed in world space, must transform them in eye space
    vec3 _up = vec3(0.0);
    if (uUsedAsPoint)
    {
        front   = (osg_ViewMatrix * vec4((uFront - uCenter).xyz, 0.0)).xyz;
        _up     = (osg_ViewMatrix * vec4((uUp - uCenter).xyz, 0.0)).xyz;
    }
    else
    {
        front   = (osg_ViewMatrix * vec4(uFront.xyz, 0.0)).xyz;
        _up     = (osg_ViewMatrix * vec4(uUp.xyz, 0.0)).xyz;
    }
    front = normalize(front);
    _up = normalize(_up);

    vec3 _side = cross(front, _up);

    float irisRotation = radians(uEyeIrisRotationD);
    float irisCRotation = cos(irisRotation);
    float irisSRotation = sin(irisRotation);

    up = irisCRotation * _up - irisSRotation * _side;
    side = irisSRotation * _up + irisCRotation * _side;
}

vec3
computeHighlightDirection(vec3 front, vec3 up, vec3 side)
{
    float hilightPolarAng       = radians(uEyeIrisAngSizeD + uEyeHighlightCoordsD.x);
    float hilightAzimuthAng     = radians(uEyeHighlightCoordsD.y);
    float cHilightPolarAng      = cos(hilightPolarAng);
    float sHilightPolarAng      = sin(hilightPolarAng);
    float cHilightAzimuthAng    = cos(hilightAzimuthAng);
    float sHilightAzimuthAng    = sin(hilightAzimuthAng);

    return normalize(cHilightPolarAng * front +
        sHilightPolarAng * (cHilightAzimuthAng * up + sHilightAzimuthAng * side));
}

void
computeIrisCone(vec3 front, out vec3 coneTipOffset, out vec2 coneShape)
{
    float irisAngSize = radians(uEyeIrisAngSizeD);
    float irisConeTAng = sin(irisAngSize) / uEyeIrisDepth;

    coneTipOffset = uEyeRadius * (cos(irisAngSize) - uEyeIrisDepth) * front;
    coneShape.x = 1.0 / (1.0 + irisConeTAng * irisConeTAng);
    coneShape.y = sqrt(coneShape.x) / (uEyeRadius * uEyeIrisDepth);
}

void
main()
{
    computeIrisFrame(vFront, vUp, vSide);
    vHighlightDir = computeHighlightDirection(vFront, vUp, vSide);
    vIrisRDimensions = 1.0 / uEyeIrisDimensions;
    computeIrisCone(vFront, vIrisConeTipOffset, vIrisConeShape);

    float hilightAngSize = radians(uEyeHighlightAngSizeD);
    float irisAngSize = radians(uEyeIrisAngSizeD);

    vPrecomp[0].x = cos(hilightAngSize);
    vPrecomp[0].y = cos(hilightAngSize * (1.0 - uEyeHighlightSmoothness));

    vPrecomp[0].z = uEyeIrisEdgeOffset + irisAngSize * (1.0 - uEyeIrisEdgeBlur);
    vPrecomp[0].w = uEyeIrisEdgeOffset + irisAngSize;

    vPrecomp[1].x = irisAngSize * (1.0 - uEyeIrisNormalSmoothness);
    vPrecomp[1].y = irisAngSize * (1.0 + uEyeIrisNormalSmoothness);

    vPrecomp[1].z = uEyePupilDimensions.x * uEyePupilDimensions.y;
    vPrecomp[1].w = 1.0 / uEyeCorneaIOR;

    //----------

    float hasNormal = step(1e-3, length(osg_Normal)); // {0, 1}

    vVertex     = osg_ModelViewMatrix * osg_Vertex;
    vNormal     = mix(vec3(0.0),        osg_NormalMatrix * normalize(osg_Normal), hasNormal);

    gl_Position = osg_ProjectionMatrix * vVertex;
}
