// ========================================================================= //
// Copyright 2016-2018 SUPAMONKS_STUDIO                                      //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
//                                                                           //
// ========================================================================= //
#version 140

uniform mat4    osg_ProjectionMatrix;

uniform mat4    uScalelessModelView;
uniform vec4    uModelViewScale;
uniform bool    uIsBillboard;

in      vec4    osg_Vertex;

mat4
getModelViewProjectionMatrix()
{
    mat4 modelView = uScalelessModelView;

    if (uIsBillboard)
    {
        modelView[0][0] = 1.0;
        modelView[0][1] = 0.0;
        modelView[0][2] = 0.0;

        modelView[1][0] = 0.0;
        modelView[1][1] = 1.0;
        modelView[1][2] = 0.0;

        modelView[2][0] = 0.0;
        modelView[2][1] = 0.0;
        modelView[2][2] = 1.0;
    }

    return osg_ProjectionMatrix * modelView;
}

void
main()
{
    mat4    modelViewProjection = getModelViewProjectionMatrix();
    vec4    clipPos             = modelViewProjection * osg_Vertex;
    float   scale               = 0.035 * clipPos.w * (length(uModelViewScale) * 0.57735027); // div by sqrt(3)

    gl_Position     = (clipPos - modelViewProjection[3]) * scale + modelViewProjection[3];
    gl_Position.z   = (clipPos.z / clipPos.w) * gl_Position.w;  // post perspective division Z must be preserved for avoiding clipping
}
