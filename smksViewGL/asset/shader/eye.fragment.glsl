// ========================================================================= //
// Copyright 2016-2018 SUPAMONKS_STUDIO                                      //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
//                                                                           //
// ========================================================================= //
#version 140

uniform mat4    osg_ViewMatrix;
//-----------------------------------
uniform float   uClockTime;
uniform vec3    uSelectionColor;
uniform ivec4   uObjectId;
uniform int     uObjectStateFlags;
//-----------------------------------
uniform vec4    uCenter;
uniform vec4    uFront;
uniform vec4    uUp;
uniform bool    uUsedAsPoint;
uniform float   uEyeCorneaIOR;
uniform vec4    uEyeGlobeColor;
uniform float   uEyeHighlightAngSizeD;
uniform vec4    uEyeHighlightColor;
uniform float   uEyeHighlightSmoothness;
uniform vec2    uEyeHighlightCoordsD;
uniform float   uEyeIrisAngSizeD;
uniform vec4    uEyeIrisColor;
uniform float   uEyeIrisDepth;
uniform vec2    uEyeIrisDimensions;
uniform float   uEyeIrisEdgeBlur;
uniform float   uEyeIrisEdgeOffset;
uniform float   uEyeIrisNormalSmoothness;
uniform float   uEyeIrisRotationD;
uniform float   uEyePupilBlur;
uniform vec2    uEyePupilDimensions;
uniform float   uEyePupilSize;
uniform float   uEyeRadius;

in vec4 vVertex;    // in eye space
in vec3 vNormal;    // in eye space

in vec3 vFront;         // in eye space
in vec3 vUp;            // in eye space
in vec3 vSide;          // in eye space
in vec3 vHighlightDir;  // in eye space
in vec2 vIrisRDimensions;   // reciprocal
in vec3 vIrisConeTipOffset;
in vec2 vIrisConeShape;     // ( x: cos(ang)^2, y: rcp(length) )
in vec4 vPrecomp[2];        // [0]: ( xy: cos(ang) steps for inside/outside highlight, yz: ang steps for inside/outside iris )
                            // [1]: ( xy: ang steps for normal bevel, z: pupil width * pupil height, w: rcp(cornea IOR) )

out vec4 oColors[2];


float
sqr(float x)
{
    return x * x;
}

float
getClockTimeBeat()
{
    float two_pi = 6.28318530718;
    return 0.5 * (1.0 + cos(uClockTime * two_pi));  //  1 pulsation per second
}

// Evaluate ray to cone intersection
// V: the cone tip
// A: the cone axis
// a2: the cone angle as cosine(angle)^2
// P: the ray origin
// D: the ray direction
// out H: the intersection point
// out N: the intersection normal
void
rayToCone (vec3 V, vec3 A, float a2, vec3 P, vec3 D, out vec3 H, out vec3 N)
{
    // line cone intersection as stated in
    // http://www.geometrictools.com/Documentation/IntersectionLineCone.pdf
    float   Axy = A.x * A.y;
    float   Axz = A.x * A.z;
    float   Ayz = A.y * A.z;
    vec3    M1 = vec3 (sqr(A.x) - a2, Axy, Axz);
    vec3    M2 = vec3 (Axy, sqr(A.y) - a2, Ayz);
    vec3    M3 = vec3 (Axz, Ayz, sqr(A.z) - a2);
    vec3    delta = P - V;

    vec3    DM = vec3 (dot (D, M1), dot (D, M2), dot (D, M3));
    float   c2 = dot (DM, D);
    float   c1 = dot (DM, delta);
    float   c0 = dot (vec3 (dot (delta, M1), dot (delta, M2), dot (delta, M3)), delta);

    float   d = sqr(c1) - c0 * c2;
    float   l = d > 0.0
        ? (-c1 - sqrt (d)) / c2
        : 0.0;
    H = P + l * D;

    vec3 binormal = cross(A, (H-V));
    N = normalize(cross(H-V, binormal));
}

void
main()
{
    vec3 incident   = normalize(vVertex.xyz);   // direction from eye to vertex
    vec3 normal     = normalize(vNormal);

    vec3 front          = normalize(vFront);
    vec3 up             = normalize(vUp);
    vec3 side           = normalize(vSide);
    vec3 highlightDir   = normalize(vHighlightDir);

    vec3    center          = (osg_ViewMatrix * vec4(uCenter.xyz, 1.0)).xyz;    // world space to eye space
    float   distToCenter    = length(center - vVertex.xyz);
    center -= (uEyeRadius - distToCenter) * front;

    vec3 toVertex       = vVertex.xyz - center;
    vec3 hilightColor   = uEyeHighlightAngSizeD > 0
        ? smoothstep(vPrecomp[0].x, vPrecomp[0].y, dot(highlightDir, normalize(toVertex))) * uEyeHighlightColor.xyz
        : vec3(0.0);

    // map vector from center to shaded vertex to the iris frame and account for iris deformation there
    float toVertex_front    = dot(toVertex, front);
    float toVertex_up       = dot(toVertex, up);
    float toVertex_side     = dot(toVertex, side);
    toVertex_side   *= vIrisRDimensions.x;
    toVertex_up     *= vIrisRDimensions.y;
    toVertex    = normalize(toVertex_front * front + toVertex_up * up + toVertex_side * side);
    float alpha = acos(dot(toVertex, front));

    // intersect the refracted ray with the iris cone
    float irisConeCAng2     = vIrisConeShape.x;
    float irisConeRLength   = vIrisConeShape.y;
    float rCorneaIOR        = vPrecomp[1].w;
    //---
    vec3 refracted      = refract(incident, normal, rCorneaIOR);
    vec3 irisConeTip    = center + vIrisConeTipOffset;

    vec3 irisConeP, irisConeN;
    rayToCone(irisConeTip, front, irisConeCAng2,
        vVertex.xyz, refracted,
        irisConeP, irisConeN);

    // compute elliptic pupil mask
    float pupilWidthHeight  = vPrecomp[1].z;
    //---
    float distToIrisEdge    = 1.0 - length(irisConeP - irisConeTip) * irisConeRLength;
    vec3  dIrisConeP        = irisConeP - irisConeTip; // - center (does not behave well)
    float irisAng           = atan(-dot(dIrisConeP, side), -dot(dIrisConeP, up));
    float pupilEllipse      = vPrecomp[1].z * inversesqrt(sqr(uEyePupilDimensions.x * cos(irisAng)) + sqr(uEyePupilDimensions.y * sin(irisAng)));
    float pupilDissolve     = smoothstep(1.0f - uEyePupilBlur, 1.0f + uEyePupilBlur, distToIrisEdge / (1.0f - uEyePupilSize * pupilEllipse));

    // determine final shaded color
    float globeBleed    = smoothstep(vPrecomp[0].z, vPrecomp[0].w, alpha);
    float bevel         = smoothstep(vPrecomp[1].x, vPrecomp[1].y, alpha);
    normal              = normalize(mix(irisConeN, normal, bevel));

    float   illum   = 0.5 * (max(0.0, dot(highlightDir, normal)) + max(0.0, dot(-incident, normal)));
    vec3    color   = illum * ( globeBleed * uEyeGlobeColor.xyz
        + (1.0 - globeBleed) * (1.0 - pupilDissolve) * uEyeIrisColor.xyz )
        + hilightColor;

    bool    isSelected = (uObjectStateFlags & 1 << 0) != 0;
    float   selectGlow = smoothstep(0.5, 0.75, max(0.0, dot(normal, -incident)));
    vec3    selectColor = mix(uSelectionColor, color, selectGlow);
    color = isSelected ? mix(selectColor, color, getClockTimeBeat()) : color;

    //###########################################
    oColors[0] = vec4(color, 1.0);
    oColors[1] = vec4(uObjectId / 255.0);
    //###########################################
}
