// ========================================================================= //
// Copyright 2016-2018 SUPAMONKS_STUDIO                                      //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
//                                                                           //
// ========================================================================= //
#version 140

uniform float   uMotionExponent;
uniform float   uMotionSign;

in      vec4    vPreviousPosition; // clip coordinates
in      vec4    vCurrentPosition; // clip coordinates

out     vec4    oColor;

void
main()
{
    float   motionSign      = uMotionSign / max(1e-6, abs(uMotionSign)); // = {-1 1}
    float   motionExponent  = max(1e-6, abs(uMotionExponent)); // > 0

    vec2    previousNDC     = vPreviousPosition.xy / vPreviousPosition.w; // in [-1 1]
    vec2    currentNDC      = vCurrentPosition.xy / vCurrentPosition.w; // in [-1 1]
    vec2    velocity        = (currentNDC - previousNDC) * motionSign; // in [-2 2]

    // oColor       = vec4(0.25 * velocity + 0.5, 0.0, 1.0);

    float   velocityMagn    = length(velocity); // in [0 2]
    vec2    velocityVec     = velocityMagn > 1e-6 ? normalize(velocity) : vec2(0.0);
    float   w               = step(1e-5, velocityMagn);

    oColor = vec4(
        w * (0.5 * velocityVec + 0.5), // motion direction
        pow(velocityMagn * 0.5, motionExponent), // motion strength
        1.0
    );

    //oColor = w * vec4(1.0, 0.0, 0.0, 1.0) + (1.0 - w) * vec4(0.0, 1.0, 0.0, 1.0);
    // oColor = clamp(
    //  vec4(0.5 * currentNDC + 0.5, (currentNDC.x * currentNDC.x + currentNDC.y * currentNDC.y)*0.5, 1.0),
    //  vec4(0.0),
    //  vec4(1.0)
    // );
}
