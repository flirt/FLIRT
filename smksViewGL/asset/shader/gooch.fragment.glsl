// ========================================================================= //
// Copyright 2016-2018 SUPAMONKS_STUDIO                                      //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
//                                                                           //
// ========================================================================= //
#version 140

uniform vec4    uWarmColor  = vec4(1.0, 0.0, 0.0, 1.0);
uniform vec4    uCoolColor  = vec4(0.0, 1.0, 0.0, 1.0);

in      vec4    vPosition;      // in eye space
in      vec3    vNormal;        // in eye space
in      vec4    vLightPosition; // in eye space

// in       vec4    vColor; // in eye space
// in       vec4    vTexCoord0; // in eye space

out     vec4    oColor;

vec4
getColor(float NdotL)
{
    return mix(uWarmColor, uCoolColor, abs(NdotL));
}

void
main()
{
    vec3    eyeVec      = normalize(-vPosition.xyz);
    vec3    lightVec    = normalize(vLightPosition.xyz - vPosition.xyz);
    lightVec = normalize(vec3(1.0, 1.0, -1.0));
    vec3    normal      = normalize(vNormal);
    vec3    halfVec     = normalize(lightVec + eyeVec);

    float   NdotL       = dot(normal, lightVec);
    float   spec        = pow(max(0.0, dot(normal, halfVec)), 32.0);

    oColor = uWarmColor;
    oColor = vec4(vec3(NdotL), 1.0);
    oColor = getColor(NdotL) + vec4(vec3(spec), 1.0);
}
