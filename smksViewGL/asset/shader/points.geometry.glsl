// ========================================================================= //
// Copyright 2016-2018 SUPAMONKS_STUDIO                                      //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
//                                                                           //
// ========================================================================= //
#version 120
#extension GL_EXT_geometry_shader4 : enable

uniform mat4 osg_ProjectionMatrix;

void
createParticle(vec4 position, float size)
{
    float   hSize       = 0.5 * size;
    vec4    pos         = osg_ProjectionMatrix * position;
    vec4    dirUp       = osg_ProjectionMatrix * vec4(0.0, hSize, 0.0, 0.0);
    vec4    dirLeft     = osg_ProjectionMatrix * vec4(hSize, 0.0, 0.0, 0.0);
    vec4    hDirUp      = 0.25 * dirUp;
    vec4    hDirLeft    = 0.25 * dirLeft;

    gl_Position = pos;                      EmitVertex();
    gl_Position = pos + hDirUp - hDirLeft;  EmitVertex();
    gl_Position = pos + dirUp;              EmitVertex();
    gl_Position = pos + hDirUp + hDirLeft;  EmitVertex();
    gl_Position = pos;                      EmitVertex();

    gl_Position = pos;                      EmitVertex();

    gl_Position = pos;                      EmitVertex();
    gl_Position = pos + hDirUp + hDirLeft;  EmitVertex();
    gl_Position = pos + dirLeft;            EmitVertex();
    gl_Position = pos - hDirUp + hDirLeft;  EmitVertex();
    gl_Position = pos;                      EmitVertex();

    gl_Position = pos;                      EmitVertex();

    gl_Position = pos;                      EmitVertex();
    gl_Position = pos - hDirUp + hDirLeft;  EmitVertex();
    gl_Position = pos - dirUp;              EmitVertex();
    gl_Position = pos - hDirUp - hDirLeft;  EmitVertex();
    gl_Position = pos;                      EmitVertex();

    gl_Position = pos;                      EmitVertex();

    gl_Position = pos;                      EmitVertex();
    gl_Position = pos - hDirUp - hDirLeft;  EmitVertex();
    gl_Position = pos - dirLeft;            EmitVertex();
    gl_Position = pos + hDirUp - hDirLeft;  EmitVertex();
    gl_Position = pos;                      EmitVertex();

    EndPrimitive();
}

void
main(void)
{
    createParticle(gl_PositionIn[0], gl_PointSizeIn[0]);
}
