// ========================================================================= //
// Copyright 2016-2018 SUPAMONKS_STUDIO                                      //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
//                                                                           //
// ========================================================================= //
#include <cassert>
#include <boost/unordered_map.hpp>
#include <string>
#include <osg/Uniform>
#include <osg/Program>

#include <smks/sys/Log.hpp>
#include <smks/sys/Exception.hpp>
#include <smks/sys/ResourceDatabase.hpp>
#include <smks/sys/TextResource.hpp>
#include <smks/util/string/getId.hpp>

#include "smks/view/gl/ProgramInput.hpp"
#include "smks/view/gl/ProgramInputDeclaration.hpp"
#include "smks/view/gl/UniformDeclaration.hpp"
#include "smks/view/gl/Sampler2dDeclaration.hpp"
#include "smks/view/gl/ProgramInputDeclarationMap.hpp"
#include "smks/view/gl/ProgramTemplate.hpp"

namespace smks { namespace view { namespace gl
{
    class ProgramTemplate::PImpl
    {
    private:
        typedef boost::unordered_map<int, std::string> ShaderFilenames;
    private:
        ShaderFilenames _shaders;   // osg::Shader::Type -> unresolved file path
        std::string     _key;
        unsigned int    _id;
    public:
        inline
        PImpl():
            _shaders(), _id(0), _key()
        { }

        inline
        void
        shader(int glShaderType, const std::string& value)
        {
            if (_id > 0)
                throw sys::Exception(
                    "Forbidden to add new shader to template.",
                    "Shader Addition to Program Template");
            if (value.empty() ||
                _shaders.find(glShaderType) != _shaders.end())
                return;
            _shaders.insert(std::pair<int, std::string>(glShaderType, value));
        }

        inline
        void
        forbidNewShader()
        {
            if (_id > 0)
                return;
        //-------------------------------------------------------------------------------
#define APPEND_GL_SHADER_PATH(_GL_SHADER_TYPE_, _PREFIX_)                                   \
            {                                                                               \
                ShaderFilenames::const_iterator const& it = _shaders.find(_GL_SHADER_TYPE_);\
                if (it != _shaders.end() && !it->second.empty())                            \
                    key << _PREFIX_ << ":" << it->second << ";";                            \
            }
        //-------------------------------------------------------------------------------
            std::stringstream key;

            APPEND_GL_SHADER_PATH(GL_VERTEX_SHADER,             "vtx")
            APPEND_GL_SHADER_PATH(GL_TESS_CONTROL_SHADER,       "tessctrl")
            APPEND_GL_SHADER_PATH(GL_TESS_EVALUATION_SHADER,    "tesseval")
            APPEND_GL_SHADER_PATH(GL_GEOMETRY_SHADER,           "geom")
            APPEND_GL_SHADER_PATH(GL_FRAGMENT_SHADER,           "frg")
            APPEND_GL_SHADER_PATH(GL_COMPUTE_SHADER,            "comp")

            _key = key.str();
            _id = util::string::getId(_key.c_str());
        }

        inline
        const char*
        shader(int glShaderType) const
        {
            ShaderFilenames::const_iterator const& it = _shaders.find(glShaderType);
            return it != _shaders.end()
                ? it->second.c_str()
                : "";
        }

        inline
        unsigned int
        id() const
        {
            return _id;
        }

        inline
        const std::string&
        key() const
        {
            return _key;
        }
    };
}
}
}

using namespace smks;

osg::ref_ptr<osg::Program>
view::gl::getOrCreateProgram(const ProgramTemplate& prog, sys::ResourceDatabase::Ptr const& resources)
{
    typedef osg::ref_ptr<osg::Program> ProgramPtr;

    if (!resources)
        return nullptr;

    ProgramPtr ret = nullptr;

    if (resources->stored<ProgramPtr>(prog.id()))
        ret = resources->take<ProgramPtr>(prog.id());
    else
    {
        //----------------------------------------------------------------------------------------------
#define LOAD_GL_SHADER_SOURCE(_GL_SHADER_TYPE_)                                                         \
        {                                                                                               \
            const char* shaderFileName = prog.shader(_GL_SHADER_TYPE_);                                 \
            if (strlen(shaderFileName) > 0)                                                             \
            {                                                                                           \
                const sys::TextResource* rsrc = resources->load<sys::TextResource>(shaderFileName);     \
                if (rsrc)                                                                               \
                    shaders.insert(std::pair<int, const sys::TextResource*>(_GL_SHADER_TYPE_, rsrc));   \
            }                                                                                           \
        }
        //----------------------------------------------------------------------------------------------

        typedef boost::unordered_map<int, const sys::TextResource*> ShaderResources;
        ShaderResources shaders;

        LOAD_GL_SHADER_SOURCE(GL_VERTEX_SHADER)
        LOAD_GL_SHADER_SOURCE(GL_TESS_CONTROL_SHADER)
        LOAD_GL_SHADER_SOURCE(GL_TESS_EVALUATION_SHADER)
        LOAD_GL_SHADER_SOURCE(GL_GEOMETRY_SHADER)
        LOAD_GL_SHADER_SOURCE(GL_FRAGMENT_SHADER)
        LOAD_GL_SHADER_SOURCE(GL_COMPUTE_SHADER)

        ret = new osg::Program();

        for (ShaderResources::const_iterator it = shaders.begin(); it != shaders.end(); ++it)
        {
            osg::Shader* shader = new osg::Shader(static_cast<osg::Shader::Type>(it->first), it->second->text());
            FLIRT_LOG(LOG(DEBUG)
                << "Loaded shader source from '" << it->second << "'"
                << "\n----------\n"
                << it->second->text()
                << "\n----------";)
            shader->setName(it->second->fileName());
            ret->addShader(shader); // takes ownership
        }
        prog.finalize(*ret); // set up vertex attributes and geometry shader settings

        const unsigned int rsrcId = resources->store<ProgramPtr>(prog.key(), ret);
        assert(rsrcId == prog.id());
    }
    assert(ret);
    return ret;
}

////////////////////////
// class ProgramTemplate
////////////////////////
view::gl::ProgramTemplate::ProgramTemplate(const char* vtx,
                                           const char* tessctrl,
                                           const char* tesseval,
                                           const char* geom,
                                           const char* frag,
                                           const char* comp):
    _pImpl(new PImpl())
{
    _pImpl->shader(osg::Shader::VERTEX,         vtx);
    _pImpl->shader(osg::Shader::TESSCONTROL,    tessctrl);
    _pImpl->shader(osg::Shader::TESSEVALUATION, tesseval);
    _pImpl->shader(osg::Shader::GEOMETRY,       geom);
    _pImpl->shader(osg::Shader::FRAGMENT,       frag);
    _pImpl->shader(osg::Shader::COMPUTE,        comp);
    _pImpl->forbidNewShader();  // computes key and id
}

view::gl::ProgramTemplate::~ProgramTemplate()
{
    delete _pImpl;
}

const char*
view::gl::ProgramTemplate::shader(int glShaderType) const
{
    return _pImpl->shader(glShaderType);
}

unsigned int
view::gl::ProgramTemplate::id() const
{
    return _pImpl->id();
}

const char*
view::gl::ProgramTemplate::key() const
{
    return _pImpl->key().c_str();
}

view::gl::ProgramInput::Ptr
view::gl::ProgramTemplate::createInput(const ProgramInputDeclaration& decl) const
{
    if (decl.asUniformDeclaration())
        return createUniform(*decl.asUniformDeclaration());
    else if (decl.asSampler2dDeclaration())
        return createSampler2d(*decl.asSampler2dDeclaration());
    else
        return nullptr;
}

view::gl::Sampler2dInput::Ptr
view::gl::ProgramTemplate::createSampler2d(const ProgramInputDeclaration& decl) const
{
    if (!isInputRelevant(decl.id()) ||
        decl.asSampler2dDeclaration() == nullptr)
        return nullptr;

    return decl.asSampler2dDeclaration()->create(getSampler2dTexUnit(decl.id()));
}

view::gl::UniformInput::Ptr
view::gl::ProgramTemplate::createUniform(const ProgramInputDeclaration& decl, osg::Object::DataVariance var) const
{
    if (!isInputRelevant(decl.id()) ||
        decl.asUniformDeclaration() == nullptr)
        return nullptr;

    UniformInput::Ptr const& ret = decl.asUniformDeclaration()->createInstance();
    assert(ret && ret->data());

    // program sets default values first
    setUniformDefaults(decl.id(), *ret->data());

    if (var != osg::Object::UNSPECIFIED &&
        ret->data())
        ret->data()->setDataVariance(var);
    return ret;
}

view::gl::UniformInput::Ptr
view::gl::ProgramTemplate::createUniform(const ProgramInputDeclaration& decl, bool v, osg::Object::DataVariance var) const
{
    UniformInput::Ptr const& ret = createUniform(decl, var);
    if (ret && ret->data())
        setUniformb(*ret->data(), v);
    return ret;
}

view::gl::UniformInput::Ptr
view::gl::ProgramTemplate::createUniform(const ProgramInputDeclaration& decl, unsigned int v, osg::Object::DataVariance var) const
{
    UniformInput::Ptr const& ret = createUniform(decl, var);
    if (ret && ret->data())
        setUniformui(*ret->data(), v);
    return ret;
}

view::gl::UniformInput::Ptr
view::gl::ProgramTemplate::createUniform(const ProgramInputDeclaration& decl, int v, osg::Object::DataVariance var) const
{
    UniformInput::Ptr const& ret = createUniform(decl, var);
    if (ret && ret->data())
        setUniformi(*ret->data(), v);
    return ret;
}

view::gl::UniformInput::Ptr
view::gl::ProgramTemplate::createUniform(const ProgramInputDeclaration& decl, const int* v, osg::Object::DataVariance var) const
{
    UniformInput::Ptr const& ret = createUniform(decl, var);
    if (ret && ret->data())
        setUniformiv(*ret->data(), v);
    return ret;
}

view::gl::UniformInput::Ptr
view::gl::ProgramTemplate::createUniform(const ProgramInputDeclaration& decl, float v, osg::Object::DataVariance var) const
{
    UniformInput::Ptr const& ret = createUniform(decl, var);
    if (ret && ret->data())
        setUniformf(*ret->data(), v);
    return ret;
}

view::gl::UniformInput::Ptr
view::gl::ProgramTemplate::createUniform(const ProgramInputDeclaration& decl, const float* v, osg::Object::DataVariance var) const
{
    UniformInput::Ptr const& ret = createUniform(decl, var);
    if (ret && ret->data())
        setUniformfv(*ret->data(), v);
    return ret;
}
