// ========================================================================= //
// Copyright 2016-2018 SUPAMONKS_STUDIO                                      //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
//                                                                           //
// ========================================================================= //
#include <osg/Uniform>

#include "smks/view/gl/ObjectStateFlags.hpp"
#include "smks/view/gl/VertexAttributeFlags.hpp"
#include "smks/view/gl/uniform.hpp"

namespace smks { namespace view { namespace gl
{
    static inline
    void
    addFlags(osg::Uniform& uniform, int f)
    {
        int curFlags = 0;
        if (!uniform.get(curFlags))
            return;
        //----------------------------
        int nxtFlags = curFlags | f;
        //----------------------------
        if (nxtFlags == curFlags)
            return;

        setUniformi(uniform, nxtFlags);
    }

    static inline
    void
    removeFlags(osg::Uniform& uniform, int f)
    {
        int curFlags = 0;
        if (!uniform.get(curFlags))
            return;
        //----------------------------
        int nxtFlags = curFlags & (~f);
        //----------------------------
        if (nxtFlags == curFlags)
            return;

        setUniformi(uniform, nxtFlags);
    }
}
}
}

using namespace smks;

//////////////////////////////////
// ObjectFlags
//////////////////////////////////
void
view::gl::add(osg::Uniform& uniform, ObjectStateFlags f)
{
    addFlags(uniform, f);
}

void
view::gl::remove(osg::Uniform& uniform, ObjectStateFlags f)
{
    removeFlags(uniform, f);
}

//////////////////////////////////
// VertexAttributeFlags
//////////////////////////////////
void
view::gl::add(osg::Uniform& uniform, VertexAttributeFlags f)
{
    addFlags(uniform, f);
}

void
view::gl::remove(osg::Uniform& uniform, VertexAttributeFlags f)
{
    removeFlags(uniform, f);
}
