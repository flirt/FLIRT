// ========================================================================= //
// Copyright 2016-2018 SUPAMONKS_STUDIO                                      //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
//                                                                           //
// ========================================================================= //
#include <smks/sys/Exception.hpp>

#include "smks/view/gl/ProgramInput.hpp"
#include "smks/view/gl/uniform.hpp"
#include "smks/view/gl/UniformDeclaration.hpp"
#include "smks/view/gl/Sampler2dDeclaration.hpp"

namespace smks { namespace view { namespace gl
{
    class Sampler2dDeclaration::PImpl
    {
    private:
        const UniformDeclaration::Ptr   _uSampler;
        const UniformDeclaration::Ptr   _uGLTransformUV;
        const UniformDeclaration::Ptr   _uTransformUV;
        const UniformDeclaration::Ptr   _uScale;
        const UniformDeclaration::Ptr   _uValid;
    public:
        explicit inline
        PImpl(const std::string& name):
            _uSampler       (UniformDeclaration::create(name.c_str(),                       osg::Uniform::SAMPLER_2D)),
            _uGLTransformUV (UniformDeclaration::create((name + "_glTransformUV").c_str(),  osg::Uniform::FLOAT_VEC4)),
            _uTransformUV   (UniformDeclaration::create((name + "_transformUV").c_str(),    osg::Uniform::FLOAT_VEC4)),
            _uScale         (UniformDeclaration::create((name + "_scale").c_str(),          osg::Uniform::FLOAT_VEC4)),
            _uValid         (UniformDeclaration::create((name + "_valid").c_str(),          osg::Uniform::BOOL))
        { }

        inline
        UniformDeclaration::Ptr const&
        uSampler() const
        {
            return _uSampler;
        }

        inline
        UniformDeclaration::Ptr const&
        uGLTransformUV() const
        {
            return _uGLTransformUV;
        }

        inline
        UniformDeclaration::Ptr const&
        uTransformUV() const
        {
            return _uTransformUV;
        }

        inline
        UniformDeclaration::Ptr const&
        uScale() const
        {
            return _uScale;
        }

        inline
        UniformDeclaration::Ptr const&
        uValid() const
        {
            return _uValid;
        }
    };
}
}
}

using namespace smks;

// static
view::gl::Sampler2dDeclaration::Ptr
view::gl::Sampler2dDeclaration::create(const char* name)
{
    Ptr ptr(new Sampler2dDeclaration(name));
    return ptr;
}

// explicit
view::gl::Sampler2dDeclaration::Sampler2dDeclaration(const char* name):
    ProgramInputDeclaration(name),
    _pImpl(new PImpl(name))
{ }

view::gl::Sampler2dDeclaration::~Sampler2dDeclaration()
{
    delete _pImpl;
}

view::gl::Sampler2dInput::Ptr
view::gl::Sampler2dDeclaration::create(int unit) const
{
    return Sampler2dInput::create(*this, unit);
}

view::gl::UniformDeclaration::Ptr const&
view::gl::Sampler2dDeclaration::uSampler() const
{
    return _pImpl->uSampler();
}

view::gl::UniformDeclaration::Ptr const&
view::gl::Sampler2dDeclaration::uGLTransformUV() const
{
    return _pImpl->uGLTransformUV();
}

view::gl::UniformDeclaration::Ptr const&
view::gl::Sampler2dDeclaration::uTransformUV() const
{
    return _pImpl->uTransformUV();
}

view::gl::UniformDeclaration::Ptr const&
view::gl::Sampler2dDeclaration::uScale() const
{
    return _pImpl->uScale();
}

view::gl::UniformDeclaration::Ptr const&
view::gl::Sampler2dDeclaration::uValid() const
{
    return _pImpl->uValid();
}
