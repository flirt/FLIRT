// ========================================================================= //
// Copyright 2016-2018 SUPAMONKS_STUDIO                                      //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
//                                                                           //
// ========================================================================= //
#include <osg/Uniform>
#include "smks/view/gl/ProgramInput.hpp"
#include "smks/view/gl/UniformDeclaration.hpp"
#include "smks/view/gl/ProgramInputDeclarations.hpp"

#include "ProgramTemplates.hpp"

using namespace smks;

view::gl::BlinnPhongProgram::BlinnPhongProgram():
    view::gl::ProgramTemplate("shader/blinnPhong.vertex.glsl",  // vertex shader
                              "",   // tesselation control
                              "",   // tesselation evaluation
                              "",   // geometry shader
                              "shader/blinnPhong.fragment.glsl",    // fragment shader
                              "")   // compute shader
{ }

void
view::gl::BlinnPhongProgram::finalize(osg::Program&) const
{ }

bool
view::gl::BlinnPhongProgram::isInputRelevant(unsigned int inputId) const
{
    return inputId == uObjectId().id() ||
        inputId == uObjectStateFlags().id() ||
        inputId == uVertexAttributes().id() ||
        inputId == uPrimaryColor().id() ||
        inputId == uPrimaryMap().id() ||
        inputId == uSecondaryColor().id() ||
        inputId == uSecondaryMap().id() ||
        inputId == uTertiaryColor().id() ||
        inputId == uTertiaryMap().id() ||
        inputId == uClockTime().id() ||
        inputId == uLightDirection().id() ||
        inputId == uSelectionColor().id();
}

void
view::gl::BlinnPhongProgram::setUniformDefaults(unsigned int inputId, osg::Uniform& u) const
{
    if      (inputId == uObjectId().id())           setUniformui(u, 0);
    else if (inputId == uObjectStateFlags().id())   setUniformi(u, 0);
    else if (inputId == uVertexAttributes().id())   setUniformi(u, 0);
    else if (inputId == uPrimaryColor().id())       setUniformui(u, -1);
    else if (inputId == uSecondaryColor().id())     setUniformfv(u, math::Vector4f::Zero().eval().data());
    else if (inputId == uTertiaryColor().id())      setUniformfv(u, math::Vector4f::Zero().eval().data());
    else if (inputId == uClockTime().id())          setUniformf(u, 0.0f);
    else if (inputId == uLightDirection().id())     setUniformfv(u, math::Vector4f::UnitZ().eval().data());
    else if (inputId == uSelectionColor().id())     setUniformfv(u, math::Vector4f(0.0f, 1.0f, 1.0f, 1.0f).data());
}

int
view::gl::BlinnPhongProgram::getSampler2dTexUnit(unsigned int inputId) const
{
    if      (inputId == uPrimaryMap().id())     return 0;
    else if (inputId == uSecondaryMap().id())   return 1;
    else if (inputId == uTertiaryMap().id())    return 2;
    return -1;
}
