// ========================================================================= //
// Copyright 2016-2018 SUPAMONKS_STUDIO                                      //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
//                                                                           //
// ========================================================================= //
#include <boost/unordered_map.hpp>
#include "smks/view/gl/ProgramTemplate.hpp"
#include "smks/view/gl/ProgramInput.hpp"
#include "smks/view/gl/ProgramInputDeclaration.hpp"
#include "smks/view/gl/ProgramInputDeclarationMap.hpp"
#include "smks/view/gl/DefaultInputs.hpp"

namespace smks { namespace view { namespace gl
{
    class DefaultInputs::PImpl
    {
    private:
        typedef boost::unordered_map<unsigned int, ProgramInput::Ptr>   ProgramInputs;      //<! input ID   -> input
        typedef boost::unordered_map<unsigned int, ProgramInputs>       ProgramInputMap;    //<! program ID -> program inputs
    private:
        ProgramInputMap _m;
    public:
        inline
        PImpl():
            _m()
        { }

        inline
        view::gl::ProgramInput::Ptr
        getOrCreateInput(const ProgramTemplate&, const ProgramInputDeclaration&);
    };

    DefaultInputs&
    defaultInputs()
    {
        static DefaultInputs* ptr = new DefaultInputs();    // never deallocated
        return *ptr;
    }
}
}
}

using namespace smks;

view::gl::DefaultInputs::DefaultInputs():
    _pImpl(new PImpl())
{ }

view::gl::DefaultInputs::~DefaultInputs()
{
    delete _pImpl;
}

view::gl::ProgramInput::Ptr
view::gl::DefaultInputs::getOrCreateInput(const ProgramTemplate&    prog,
                                          unsigned int              inId)
{
    ProgramInputDeclaration::Ptr const& decl = programInputDeclarations().find(inId);
    return decl
        ? getOrCreateInput(prog, *decl)
        : nullptr;
}

view::gl::ProgramInput::Ptr
view::gl::DefaultInputs::getOrCreateInput(const ProgramTemplate&            prog,
                                          const ProgramInputDeclaration&    decl)
{
    return _pImpl->getOrCreateInput(prog, decl);
}

view::gl::ProgramInput::Ptr
view::gl::DefaultInputs::PImpl::getOrCreateInput(const ProgramTemplate&         prog,
                                                 const ProgramInputDeclaration& decl)
{
    if (_m.find(prog.id()) == _m.end())
        _m.insert(std::pair<unsigned int, ProgramInputs>(prog.id(), ProgramInputs()));
    assert(_m.find(prog.id()) != _m.end());

    ProgramInputs&                          progInputs  = _m.find(prog.id())->second;
    ProgramInputs::const_iterator const&    itInputs    = progInputs.find(decl.id());

    if (itInputs != progInputs.end())
        return itInputs->second;

    ProgramInput::Ptr const& input = prog.createInput(decl);    // default value is imposed by program here!
    progInputs.insert(std::pair<unsigned int, ProgramInput::Ptr>(decl.id(), input));

    return input;
}
