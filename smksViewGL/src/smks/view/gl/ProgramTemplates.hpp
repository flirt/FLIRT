// ========================================================================= //
// Copyright 2016-2018 SUPAMONKS_STUDIO                                      //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
//                                                                           //
// ========================================================================= //
#pragma once

#include "smks/view/gl/ProgramTemplate.hpp"
#include "smks/view/gl/ProgramTemplates.hpp"
#include "smks/view/gl/ProgramTemplateMap.hpp"

    //-------------------------------------------------------------------------------------
#define DEF_PROG_TEMPLATE(_CLASS_)                                                          \
    class _CLASS_:                                                                          \
        public smks::view::gl::ProgramTemplate                                              \
    {                                                                                       \
    public:                                                                                 \
        typedef std::shared_ptr<_CLASS_> Ptr;                                               \
    public:                                                                                 \
        static inline Ptr create() { Ptr ptr(new _CLASS_()); return ptr; }                  \
    public:                                                                                 \
        _CLASS_();                                                                          \
    public:                                                                                 \
        void    finalize(osg::Program&) const;                                              \
    protected:                                                                              \
        bool    isInputRelevant     (unsigned int inputId) const;                           \
        void    setUniformDefaults  (unsigned int inputId, osg::Uniform&) const;            \
        int     getSampler2dTexUnit (unsigned int inputId) const;                           \
    };                                                                                      \
    /* define the global getter alonside the new class*/                                    \
    inline                                                                                  \
    const smks::view::gl::ProgramTemplate&                                                  \
    smks::view::gl::get_##_CLASS_ ()                                                        \
    {                                                                                       \
        static ProgramTemplate::Ptr ptr = programs().add(_CLASS_::create());                \
        return *ptr;                                                                        \
    }
    //------------------------------------------------------------------------------------

namespace smks { namespace view { namespace gl
{
    DEF_PROG_TEMPLATE(BasicProgram)
    DEF_PROG_TEMPLATE(BlinnPhongProgram)
    DEF_PROG_TEMPLATE(CurvesProgram)
    DEF_PROG_TEMPLATE(EyeProgram)
    DEF_PROG_TEMPLATE(LineProgram)
    DEF_PROG_TEMPLATE(ManipulatorProgram)
    DEF_PROG_TEMPLATE(MotionProgram)
    DEF_PROG_TEMPLATE(NormalProgram)
    DEF_PROG_TEMPLATE(PointsProgram)
    DEF_PROG_TEMPLATE(SceneObjLocatorProgram)
    DEF_PROG_TEMPLATE(TexCoordProgram)
    DEF_PROG_TEMPLATE(TextProgram)
    DEF_PROG_TEMPLATE(TextureProgram)
}
}
}
