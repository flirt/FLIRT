// ========================================================================= //
// Copyright 2016-2018 SUPAMONKS_STUDIO                                      //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
//                                                                           //
// ========================================================================= //
#include <osg/ref_ptr>
#include <osg/Texture2D>

#include "smks/view/gl/ProgramInputDeclaration.hpp"
#include "smks/view/gl/UniformDeclaration.hpp"
#include "smks/view/gl/Sampler2dDeclaration.hpp"
#include "smks/view/gl/ProgramInput.hpp"
#include "smks/view/gl/uniform.hpp"

namespace smks { namespace view { namespace gl
{
    ////////////////////////////
    // class UniformInput::PImpl
    ////////////////////////////
    class UniformInput::PImpl
    {
    private:
        osg::ref_ptr<osg::Uniform> _data;
    public:
        explicit inline
        PImpl(osg::ref_ptr<osg::Uniform> const& data):
            _data(data)
        {
            _data->setDataVariance(osg::Object::DYNAMIC);
        }

        inline osg::Uniform* data() { return _data.get(); }
    };

    //////////////////////////////
    // class Sampler2dInput::PImpl
    //////////////////////////////
    class Sampler2dInput::PImpl
    {
    private:
        UniformInput::Ptr               _sampler2d;
        UniformInput::Ptr               _glTransformUV;
        UniformInput::Ptr               _transformUV;
        UniformInput::Ptr               _scale;
        UniformInput::Ptr               _valid;
        osg::ref_ptr<osg::Texture2D>    _texture2d;
    public:
        inline
        PImpl(
            int                         unit,
            UniformInput::Ptr const&    sampler2d,
            UniformInput::Ptr const&    glTransformUV,
            UniformInput::Ptr const&    transformUV,
            UniformInput::Ptr const&    scale,
            UniformInput::Ptr const&    valid):
            _sampler2d      (sampler2d),
            _glTransformUV  (glTransformUV),
            _transformUV    (transformUV),
            _scale          (scale),
            _valid          (valid),
            _texture2d      (new osg::Texture2D())
        {
            if (_sampler2d && _sampler2d->data())
            {
                setUniformi(*_sampler2d->data(), unit);
                _sampler2d->data()->setDataVariance(osg::Object::STATIC);
            }
            if (_transformUV && _transformUV->data())
            {
                setUniformfv(*_transformUV->data(), math::Vector4f(1.0f, 1.0f, 0.0f, 0.0f).data());
                _transformUV->data()->setDataVariance(osg::Object::DYNAMIC);
            }
            if (_scale && _scale->data())
            {
                setUniformfv(*_scale->data(), math::Vector4f::Ones().eval().data());
                _scale->data()->setDataVariance(osg::Object::DYNAMIC);
            }
            if (_valid && _valid->data())
            {
                setUniformb(*_valid->data(), false);
                _valid->data()->setDataVariance(osg::Object::DYNAMIC);
            }
        }

        inline osg::Uniform*    sampler2d()     { return _sampler2d     ->data();   }
        inline osg::Uniform*    glTransformUV() { return _glTransformUV ->data();   }
        inline osg::Uniform*    transformUV()   { return _transformUV   ->data();   }
        inline osg::Uniform*    scale()         { return _scale         ->data();   }
        inline osg::Uniform*    valid()         { return _valid         ->data();   }
        inline osg::Texture2D*  texture2d()     { return _texture2d.get();          }
    };
}
}
}

using namespace smks;

/////////////////////
// class UniformInput
/////////////////////
view::gl::UniformInput::UniformInput(const ProgramInputDeclaration& decl):
    ProgramInput(),
    _pImpl(new PImpl(
        decl.asUniformDeclaration() ? decl.asUniformDeclaration()->createUniformData() : nullptr))
{ }

view::gl::UniformInput::~UniformInput()
{
    delete _pImpl;
}

osg::Uniform* view::gl::UniformInput::data() { return _pImpl->data(); }

///////////////////////
// class Sampler2dInput
///////////////////////
view::gl::Sampler2dInput::Sampler2dInput(const ProgramInputDeclaration& decl, int unit):
    ProgramInput(),
    _pImpl(new PImpl(
        unit,
        decl.asSampler2dDeclaration() ? decl.asSampler2dDeclaration()->uSampler()       ->createInstance()  : nullptr,
        decl.asSampler2dDeclaration() ? decl.asSampler2dDeclaration()->uGLTransformUV() ->createInstance()  : nullptr,
        decl.asSampler2dDeclaration() ? decl.asSampler2dDeclaration()->uTransformUV()   ->createInstance()  : nullptr,
        decl.asSampler2dDeclaration() ? decl.asSampler2dDeclaration()->uScale()         ->createInstance()  : nullptr,
        decl.asSampler2dDeclaration() ? decl.asSampler2dDeclaration()->uValid()         ->createInstance()  : nullptr))
{ }

view::gl::Sampler2dInput::~Sampler2dInput()
{
    delete _pImpl;
}

osg::Uniform*   view::gl::Sampler2dInput::sampler2d     ()  { return _pImpl->sampler2d();       }
osg::Uniform*   view::gl::Sampler2dInput::glTransformUV ()  { return _pImpl->glTransformUV();   }
osg::Uniform*   view::gl::Sampler2dInput::transformUV   ()  { return _pImpl->transformUV();     }
osg::Uniform*   view::gl::Sampler2dInput::scale         ()  { return _pImpl->scale();           }
osg::Uniform*   view::gl::Sampler2dInput::valid         ()  { return _pImpl->valid();           }
osg::Texture2D* view::gl::Sampler2dInput::texture2d     ()  { return _pImpl->texture2d();       }
