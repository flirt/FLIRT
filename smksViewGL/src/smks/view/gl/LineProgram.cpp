// ========================================================================= //
// Copyright 2016-2018 SUPAMONKS_STUDIO                                      //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
//                                                                           //
// ========================================================================= //
#include <osg/Uniform>
#include "smks/view/gl/ProgramInput.hpp"
#include "smks/view/gl/UniformDeclaration.hpp"
#include "smks/view/gl/ProgramInputDeclarations.hpp"

#include "ProgramTemplates.hpp"

using namespace smks;

view::gl::LineProgram::LineProgram():
    view::gl::ProgramTemplate("shader/line.vertex.glsl",    // vertex shader
                              "",   // tesselation control
                              "",   // tesselation evaluation
                              "",   // geometry shader
                              "shader/line.fragment.glsl",  // fragment shader
                              "")   // compute shader
{ }

void
view::gl::LineProgram::finalize(osg::Program&) const
{ }

bool
view::gl::LineProgram::isInputRelevant(unsigned int inputId) const
{
    return inputId == uObjectId().id() ||
        inputId == uColor().id();
}

void
view::gl::LineProgram::setUniformDefaults(unsigned int inputId, osg::Uniform& u) const
{
    if      (inputId == uObjectId().id())   setUniformui(u, 0);
    else if (inputId == uColor().id())      setUniformui(u, -1);
}

int
view::gl::LineProgram::getSampler2dTexUnit(unsigned int inputId) const
{
    return -1;
}
