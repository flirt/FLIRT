// ========================================================================= //
// Copyright 2016-2018 SUPAMONKS_STUDIO                                      //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
//                                                                           //
// ========================================================================= //
#include <osg/GLExtensions>
#include <osg/Geometry>
#include <osg/Program>

#include <smks/sys/Exception.hpp>
#include <smks/util/string/getId.hpp>
#include "smks/view/gl/VertexAttrib.hpp"

namespace smks { namespace view { namespace gl
{
    class VertexAttrib::PImpl
    {
    private:
        const std::string   _name;
        const unsigned int  _id;
        const unsigned int  _location;
        const GLenum        _glType;
        const size_t        _numComponents;
        const bool          _normalized;
    public:
        inline
        PImpl(const char*   name,
              unsigned int  location,
              GLenum        glType,
              size_t        numComponents,
              bool          normalized):
            _name               (name),
            _id                 (name && strlen(name) > 0 ? util::string::getId(name) : 0),
            _location           (location),
            _glType             (glType),
            _numComponents      (numComponents),
            _normalized         (normalized)
        {
            if (_id == 0)
                throw sys::Exception(
                    "Invalid name.",
                    "Vertex Attribute Construction");
        }

        inline
        unsigned int
        id() const
        {
            return _id;
        }

        inline
        void
        bindLocation(osg::Program& program) const
        {
            program.addBindAttribLocation(_name, _location);
        }

        inline
        void
        bindArray(osg::Geometry& geometry, osg::Array* data, osg::Array::Binding binding) const
        {
            geometry.setVertexAttribArray(_location, data, binding);
        }

        inline
        void
        glEnableVertexAttribArray(const osg::GLExtensions* ext) const
        {
            if (ext)
                ext->glEnableVertexAttribArray(_location);
        }

        inline
        void
        glVertexAttribPointer(const osg::GLExtensions* ext, GLsizei stride, void* ptr) const
        {
            if (ext)
                ext->glVertexAttribPointer(_location, _numComponents, _glType, _normalized, stride, ptr);
        }

        inline
        void
        glDisableVertexAttribArray(const osg::GLExtensions* ext) const
        {
            if (ext)
                ext->glDisableVertexAttribArray(_location);
        }
    };
}
}
}

using namespace smks;

// static
view::gl::VertexAttrib::Ptr
view::gl::VertexAttrib::create(const char*  name,
                               unsigned int location,
                               GLenum       glType,
                               size_t       numComponents,
                               bool         normalized)
{
    Ptr ptr(new VertexAttrib(name, location, glType, numComponents, normalized));
    return ptr;
}

view::gl::VertexAttrib::VertexAttrib(const char*    name,
                                     unsigned int   location,
                                     GLenum         glType,
                                     size_t         numComponents,
                                     bool           normalized):
    _pImpl(new PImpl(name, location, glType, numComponents, normalized))
{ }

view::gl::VertexAttrib::~VertexAttrib()
{
    delete _pImpl;
}

unsigned int
view::gl::VertexAttrib::id() const
{
    return _pImpl->id();
}

void
view::gl::VertexAttrib::bindLocation(osg::Program& program) const
{
    _pImpl->bindLocation(program);
}

void
view::gl::VertexAttrib::bindArray(osg::Geometry&        geometry,
                                  osg::Array*           data,
                                  osg::Array::Binding   binding) const
{
    _pImpl->bindArray(geometry, data, binding);
}

void
view::gl::VertexAttrib::glEnableVertexAttribArray(const osg::GLExtensions* ext) const
{
    _pImpl->glEnableVertexAttribArray(ext);
}

void
view::gl::VertexAttrib::glVertexAttribPointer(const osg::GLExtensions*  ext,
                                              GLsizei                   stride,
                                              void*                     ptr) const
{
    _pImpl->glVertexAttribPointer(ext, stride, ptr);
}

void
view::gl::VertexAttrib::glDisableVertexAttribArray(const osg::GLExtensions* ext) const
{
    _pImpl->glDisableVertexAttribArray(ext);
}
