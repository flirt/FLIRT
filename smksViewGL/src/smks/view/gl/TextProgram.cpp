// ========================================================================= //
// Copyright 2016-2018 SUPAMONKS_STUDIO                                      //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
//                                                                           //
// ========================================================================= //
#include <osg/Uniform>
#include "smks/view/gl/ProgramInput.hpp"
#include "smks/view/gl/UniformDeclaration.hpp"
#include "smks/view/gl/ProgramInputDeclarations.hpp"

#include "ProgramTemplates.hpp"

using namespace smks;

view::gl::TextProgram::TextProgram():
    view::gl::ProgramTemplate("shader/text.vertex.glsl",    // vertex shader
                              "",   // tesselation control
                              "",   // tesselation evaluation
                              "",   // geometry shader
                              "shader/text.fragment.glsl",  // fragment shader
                              "")   // compute shader
{ }

void
view::gl::TextProgram::finalize(osg::Program&) const
{ }

bool
view::gl::TextProgram::isInputRelevant(unsigned int inputId) const
{
    return inputId == uTextColor().id() ||
        inputId == uTextGlyphs().id();
}

void
view::gl::TextProgram::setUniformDefaults(unsigned int inputId, osg::Uniform& u) const
{
    if      (inputId == uTextColor().id())  setUniformui(u, -1);
    else if (inputId == uTextGlyphs().id()) setUniformi(u, 0);
}

int
view::gl::TextProgram::getSampler2dTexUnit(unsigned int inputId) const
{
    return -1;
}
