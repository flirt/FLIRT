// ========================================================================= //
// Copyright 2016-2018 SUPAMONKS_STUDIO                                      //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
//                                                                           //
// ========================================================================= //
#include <osg/Uniform>

#include <smks/sys/Exception.hpp>
#include <smks/sys/constants.hpp>
#include <smks/util/color/rgba8.hpp>

#include "smks/view/gl/uniform.hpp"

using namespace smks;

void view::gl::setUniformb(osg::Uniform& uniform, bool v)
{
    switch (uniform.getType())
    {
    case osg::Uniform::BOOL:    uniform.set(v); break;
    default:                    throw sys::Exception("Uniform type incompatible with value.", "Boolean Uniform Setter");
    }
    uniform.dirty();
}

void view::gl::setUniformi(osg::Uniform& uniform, int v)
{
    if (uniform.getType() == osg::Uniform::INT ||
        uniform.getType() == osg::Uniform::SAMPLER_2D)
        uniform.set(v);
    else
    {
        unsigned char rgba[4] = { 0, 0, 0, 0 };

        util::color::uintToUchar4(v, rgba);
        switch (uniform.getType())
        {
        case osg::Uniform::FLOAT_VEC3:
            uniform.set(osg::Vec3f(
                rgba[0] * static_cast<float>(sys::one_over_255),
                rgba[1] * static_cast<float>(sys::one_over_255),
                rgba[2] * static_cast<float>(sys::one_over_255)));
            break;
        case osg::Uniform::FLOAT_VEC4:
            uniform.set(osg::Vec4f(
                rgba[0] * static_cast<float>(sys::one_over_255),
                rgba[1] * static_cast<float>(sys::one_over_255),
                rgba[2] * static_cast<float>(sys::one_over_255),
                rgba[3] * static_cast<float>(sys::one_over_255)));
            break;
        default:
            throw sys::Exception("Uniform type incompatible with value.", "Unsigned Int Uniform Setter");
        }
    }
    uniform.dirty();
}

void view::gl::setUniformiv(osg::Uniform& uniform, const int* v)
{
    if (v == nullptr) throw sys::Exception("Invalid argument.", "Int Array Uniform Setter");
    switch (uniform.getType())
    {
    case osg::Uniform::INT:         uniform.set(*v);                     break;
    case osg::Uniform::INT_VEC2:    uniform.set(v[0], v[1]);             break;
    case osg::Uniform::INT_VEC3:    uniform.set(v[0], v[1], v[2]);       break;
    case osg::Uniform::INT_VEC4:    uniform.set(v[0], v[1], v[2], v[3]); break;
    default:                        throw sys::Exception("Uniform type incompatible with value.", "Int Array Uniform Setter");
    }
    uniform.dirty();
}

void view::gl::setUniformf(osg::Uniform& uniform, float v)
{
    switch (uniform.getType())
    {
    case osg::Uniform::FLOAT:   uniform.set(v); break;
    default:                    throw sys::Exception("Uniform type incompatible with value.", "Float Uniform Setter");
    }
    uniform.dirty();
}

void view::gl::setUniformfv(osg::Uniform& uniform, const float* v)
{
    if (v == nullptr) throw sys::Exception("Invalid argument.", "Float Array Uniform Setter");
    switch (uniform.getType())
    {
    case osg::Uniform::FLOAT:       uniform.set(*v);                                 break;
    case osg::Uniform::FLOAT_VEC2:  uniform.set(osg::Vec2f(v[0], v[1]));             break;
    case osg::Uniform::FLOAT_VEC3:  uniform.set(osg::Vec3f(v[0], v[1], v[2]));       break;
    case osg::Uniform::FLOAT_VEC4:  uniform.set(osg::Vec4f(v[0], v[1], v[2], v[3])); break;
    case osg::Uniform::FLOAT_MAT4:  uniform.set(osg::Matrixf(v));                    break;
    default:                        throw sys::Exception("Uniform type incompatible with value.", "Float Array Uniform Setter");
    }
    uniform.dirty();
}

void view::gl::setUniformui(osg::Uniform& uniform, unsigned int v)  //<! interpreted as RGBA8 color depending on the uniform's type
{
    if (uniform.getType() == osg::Uniform::UNSIGNED_INT)
        uniform.set(v);
    else
    {
        unsigned char rgba[4] = { 0, 0, 0, 0 };

        util::color::uintToUchar4(v, rgba);
        switch (uniform.getType())
        {
        case osg::Uniform::FLOAT_VEC3:
            uniform.set(osg::Vec3f(
                rgba[0] * static_cast<float>(sys::one_over_255),
                rgba[1] * static_cast<float>(sys::one_over_255),
                rgba[2] * static_cast<float>(sys::one_over_255)));
            break;
        case osg::Uniform::FLOAT_VEC4:
            uniform.set(osg::Vec4f(
                rgba[0] * static_cast<float>(sys::one_over_255),
                rgba[1] * static_cast<float>(sys::one_over_255),
                rgba[2] * static_cast<float>(sys::one_over_255),
                rgba[3] * static_cast<float>(sys::one_over_255)));
            break;
        case osg::Uniform::INT_VEC4:
            uniform.set(rgba[0], rgba[1], rgba[2], rgba[3]);
            break;
        default:
            throw sys::Exception("Uniform type incompatible with value.", "Unsigned Int Uniform Setter");
        }
    }
    uniform.dirty();
}
