// ========================================================================= //
// Copyright 2016-2018 SUPAMONKS_STUDIO                                      //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
//                                                                           //
// ========================================================================= //
#include <osg/Uniform>
#include <osg/Program>
#include <osg/PrimitiveSet>
#include "smks/view/gl/ProgramInput.hpp"
#include "smks/view/gl/UniformDeclaration.hpp"
#include "smks/view/gl/VertexAttrib.hpp"
#include "smks/view/gl/VertexAttribs.hpp"
#include "smks/view/gl/ProgramInputDeclarations.hpp"

#include "ProgramTemplates.hpp"

using namespace smks;

view::gl::ManipulatorProgram::ManipulatorProgram():
    view::gl::ProgramTemplate("shader/manipulator.vertex.glsl", // vertex shader
                              "",   // tesselation control
                              "",   // tesselation evaluation
                              "",   // geometry shader
                              "shader/manipulator.fragment.glsl",   // fragment shader
                              "")   // compute shader
{ }

void
view::gl::ManipulatorProgram::finalize(osg::Program& program) const
{
    // vertex attribute
    iPosition   ().bindLocation(program);
    iColor      ().bindLocation(program);
}

bool
view::gl::ManipulatorProgram::isInputRelevant(unsigned int inputId) const
{
    return false;
}

void
view::gl::ManipulatorProgram::setUniformDefaults(unsigned int inputId, osg::Uniform& u) const
{ }

int
view::gl::ManipulatorProgram::getSampler2dTexUnit(unsigned int inputId) const
{
    return -1;
}
