// ========================================================================= //
// Copyright 2016-2018 SUPAMONKS_STUDIO                                      //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
//                                                                           //
// ========================================================================= //
#include <boost/unordered_map.hpp>

#include <smks/util/string/getId.hpp>
#include <smks/xchg/IdObjectMap.hpp>
#include "smks/view/gl/ProgramInputDeclaration.hpp"
#include "smks/view/gl/ProgramInputDeclarationMap.hpp"

namespace smks { namespace view { namespace gl
{
    OBJECT_DICT_IMPL(ProgramInputDeclarationMap, ProgramInputDeclaration)

    ProgramInputDeclarationMap&
    programInputDeclarations()
    {
        static ProgramInputDeclarationMap* ptr = new ProgramInputDeclarationMap();  // never deallocated
        return *ptr;
    }
}
}
}
