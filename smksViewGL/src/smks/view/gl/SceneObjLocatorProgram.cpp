// ========================================================================= //
// Copyright 2016-2018 SUPAMONKS_STUDIO                                      //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
//                                                                           //
// ========================================================================= //
#include <osg/Uniform>
#include "smks/view/gl/ProgramInput.hpp"
#include "smks/view/gl/UniformDeclaration.hpp"
#include "smks/view/gl/ProgramInputDeclarations.hpp"

#include "ProgramTemplates.hpp"

using namespace smks;

view::gl::SceneObjLocatorProgram::SceneObjLocatorProgram():
    view::gl::ProgramTemplate("shader/screenObjectLine.vertex.glsl",    // vertex shader
                              "",   // tesselation control
                              "",   // tesselation evaluation
                              "",   // geometry shader
                              "shader/locator.fragment.glsl",   // fragment shader
                              "")   // compute shader
{ }

void
view::gl::SceneObjLocatorProgram::finalize(osg::Program&) const
{ }

bool
view::gl::SceneObjLocatorProgram::isInputRelevant(unsigned int inputId) const
{
    return inputId == uObjectId().id() ||
        inputId == uObjectStateFlags().id() ||
        inputId == uColor().id() ||
        inputId == uScalelessModelView().id() ||
        inputId == uModelViewScale().id() ||
        inputId == uIsBillboard().id() ||
        inputId == uClockTime().id() ||
        inputId == uSelectionColor().id();
}

void
view::gl::SceneObjLocatorProgram::setUniformDefaults(unsigned int inputId, osg::Uniform& u) const
{
    if      (inputId == uObjectId().id())           setUniformui(u, 0);
    else if (inputId == uObjectStateFlags().id())   setUniformi(u, 0);
    else if (inputId == uColor().id())              setUniformui(u, -1);
    else if (inputId == uScalelessModelView().id()) setUniformfv(u, math::Affine3f::Identity().data());
    else if (inputId == uModelViewScale().id())     setUniformfv(u, math::Vector4f::Ones().eval().data());
    else if (inputId == uIsBillboard().id())        setUniformb(u, false);
    else if (inputId == uClockTime().id())          setUniformf(u, 0.0f);
    else if (inputId == uSelectionColor().id())     setUniformfv(u, math::Vector4f(0.0f, 1.0f, 1.0f, 1.0f).data());
}

int
view::gl::SceneObjLocatorProgram::getSampler2dTexUnit(unsigned int inputId) const
{
    return -1;
}
