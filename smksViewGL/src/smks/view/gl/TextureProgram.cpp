// ========================================================================= //
// Copyright 2016-2018 SUPAMONKS_STUDIO                                      //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
//                                                                           //
// ========================================================================= //
#include <osg/Uniform>
#include "smks/view/gl/ProgramInput.hpp"
#include "smks/view/gl/UniformDeclaration.hpp"
#include "smks/view/gl/ProgramInputDeclarations.hpp"

#include "ProgramTemplates.hpp"

using namespace smks;

view::gl::TextureProgram::TextureProgram():
    view::gl::ProgramTemplate("shader/basic.vertex.glsl",   // vertex shader
                              "",   // tesselation control
                              "",   // tesselation evaluation
                              "",   // geometry shader
                              "shader/diffuseTexture.fragment.glsl",    // fragment shader
                              "")   // compute shader
{ }

void
view::gl::TextureProgram::finalize(osg::Program&) const
{ }

bool
view::gl::TextureProgram::isInputRelevant(unsigned int inputId) const
{
    return inputId == uDiffuseTexture().id() ||
        inputId == uRecTextureSize().id() ||
        inputId == uAntiAlias().id();
}

void
view::gl::TextureProgram::setUniformDefaults(unsigned int inputId, osg::Uniform& u) const
{
    if      (inputId == uDiffuseTexture().id()) setUniformi(u, 0);
    else if (inputId == uRecTextureSize().id()) setUniformfv(u, math::Vector4f::Ones().eval().data());
    else if (inputId == uAntiAlias().id())      setUniformb(u, false);
}

int
view::gl::TextureProgram::getSampler2dTexUnit(unsigned int inputId) const
{
    return -1;
}
