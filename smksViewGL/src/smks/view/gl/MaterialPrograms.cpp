// ========================================================================= //
// Copyright 2016-2018 SUPAMONKS_STUDIO                                      //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
//                                                                           //
// ========================================================================= //
#include <smks/parm/BuiltIn.hpp>
#include <smks/parm/BuiltIns.hpp>

#include "smks/view/gl/ProgramInputDeclarations.hpp"
#include "MaterialPrograms.hpp"

using namespace smks;

//------------------------------------------------
// points-dedicated shading programs
//------------------------------------------------

void
view::gl::BasicPointsMaterial::build()
{
    parmAffectsInput(parm::diffuseReflectance(),    uColor());
    parmAffectsInput(parm::eyeIrisColor(),          uColor());
    parmAffectsInput(parm::reflectance(),           uColor());
    parmAffectsInput(parm::layerColor(),            uColor());
    parmAffectsInput(parm::intTransmission(),       uColor());
    parmAffectsInput(parm::transmission(),          uColor());
}

//------------------------------------------------
// curves-dedicated shading programs
//------------------------------------------------

void
view::gl::BasicHairCurvesMaterial::build()
{
    parmAffectsInput(parm::diffuseReflectance(), uColor());
}

//------------------------------------------------
// polymesh-dedicated shading programs
//------------------------------------------------

void
view::gl::BrushedMetalMeshMaterial::build()
{
    parmAffectsInput(parm::reflectance(),       uPrimaryColor());
    parmAffectsInput(parm::reflectanceMap(),    uPrimaryMap());
}

void
view::gl::DielectricMeshMaterial::build()
{
    parmAffectsInput(parm::intTransmission(),   uPrimaryColor());

    parmAffectsInput(parm::extTransmission(),   uSecondaryColor());
}

void
view::gl::EyeMeshMaterial::build()
{
    parmAffectsInput(parm::center(),                    uCenter());
    parmAffectsInput(parm::front(),                     uFront());
    parmAffectsInput(parm::up(),                        uUp());
    parmAffectsInput(parm::usedAsPoint(),               uUsedAsPoint());
    parmAffectsInput(parm::eyeCorneaIOR(),              uEyeCorneaIOR());
    parmAffectsInput(parm::eyeGlobeColor(),             uEyeGlobeColor());
    parmAffectsInput(parm::eyeHighlightAngSizeD(),      uEyeHighlightAngSizeD());
    parmAffectsInput(parm::eyeHighlightColor(),         uEyeHighlightColor());
    parmAffectsInput(parm::eyeHighlightSmoothness(),    uEyeHighlightSmoothness());
    parmAffectsInput(parm::eyeHighlightCoordsD(),       uEyeHighlightCoordsD());
    parmAffectsInput(parm::eyeIrisAngSizeD(),           uEyeIrisAngSizeD());
    parmAffectsInput(parm::eyeIrisColor(),              uEyeIrisColor());
    parmAffectsInput(parm::eyeIrisDepth(),              uEyeIrisDepth());
    parmAffectsInput(parm::eyeIrisDimensions(),         uEyeIrisDimensions());
    parmAffectsInput(parm::eyeIrisEdgeBlur(),           uEyeIrisEdgeBlur());
    parmAffectsInput(parm::eyeIrisEdgeOffset(),         uEyeIrisEdgeOffset());
    parmAffectsInput(parm::eyeIrisNormalSmoothness(),   uEyeIrisNormalSmoothness());
    parmAffectsInput(parm::eyeIrisRotationD(),          uEyeIrisRotationD());
    parmAffectsInput(parm::eyePupilBlur(),              uEyePupilBlur());
    parmAffectsInput(parm::eyePupilDimensions(),        uEyePupilDimensions());
    parmAffectsInput(parm::eyePupilSize(),              uEyePupilSize());
    parmAffectsInput(parm::eyeRadius(),                 uEyeRadius());
}

void
view::gl::MatteMeshMaterial::build()
{
    parmAffectsInput(parm::reflectance(),       uPrimaryColor());
    parmAffectsInput(parm::reflectanceMap(),    uPrimaryMap());
}

void
view::gl::MetalMeshMaterial::build()
{
    parmAffectsInput(parm::reflectance(),       uPrimaryColor());
    parmAffectsInput(parm::reflectanceMap(),    uPrimaryMap());
}

void
view::gl::MetallicPaintMeshMaterial::build()
{
    parmAffectsInput(parm::layerColor(),    uPrimaryColor());
    parmAffectsInput(parm::layerColorMap(), uPrimaryMap());

    parmAffectsInput(parm::glitterColor(),      uSecondaryColor());
    parmAffectsInput(parm::glitterColorMap(),   uSecondaryMap());
}

void
view::gl::MirrorMeshMaterial::build()
{
    parmAffectsInput(parm::reflectance(),       uPrimaryColor());
    parmAffectsInput(parm::reflectanceMap(),    uPrimaryMap());
}

void
view::gl::ObjMeshMaterial::build()
{
    parmAffectsInput(parm::diffuseReflectance(),    uPrimaryColor());
    parmAffectsInput(parm::diffuseReflectanceMap(), uPrimaryMap());

    parmAffectsInput(parm::specularReflectance(),       uSecondaryColor());
    parmAffectsInput(parm::specularReflectanceMap(),    uSecondaryMap());
}

void
view::gl::PlasticMeshMaterial::build()
{
    parmAffectsInput(parm::diffuseReflectance(),    uPrimaryColor());
    parmAffectsInput(parm::diffuseReflectanceMap(), uPrimaryMap());
}

void
view::gl::ThinDielectricMeshMaterial::build()
{
    parmAffectsInput(parm::transmission(),      uPrimaryColor());
    parmAffectsInput(parm::transmissionMap(),   uPrimaryMap());
}

void
view::gl::TranslucentMeshMaterial::build()
{
    parmAffectsInput(parm::transmission(),      uPrimaryColor());
    parmAffectsInput(parm::transmissionMap(),   uPrimaryMap());
}

void
view::gl::TranslucentSSMeshMaterial::build()
{
    parmAffectsInput(parm::diffuseReflectance(),    uPrimaryColor());
    parmAffectsInput(parm::diffuseReflectanceMap(), uPrimaryMap());

    parmAffectsInput(parm::diffuseTransmission(),       uSecondaryColor());
    parmAffectsInput(parm::diffuseTransmissionMap(),    uSecondaryMap());

    parmAffectsInput(parm::glossyReflectance(),     uTertiaryColor());
    parmAffectsInput(parm::glossyReflectanceMap(),  uTertiaryMap());
}

void
view::gl::VelvetMeshMaterial::build()
{
    parmAffectsInput(parm::reflectance(),       uPrimaryColor());
    parmAffectsInput(parm::reflectanceMap(),    uPrimaryMap());

    parmAffectsInput(parm::horizonScatteringColor(),    uSecondaryColor());
    parmAffectsInput(parm::horizonScatteringColorMap(), uSecondaryMap());
}
