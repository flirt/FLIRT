// ========================================================================= //
// Copyright 2016-2018 SUPAMONKS_STUDIO                                      //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
//                                                                           //
// ========================================================================= //
#include <smks/sys/Exception.hpp>
#include "smks/view/gl/uniform.hpp"
#include "smks/view/gl/ProgramInput.hpp"
#include "smks/view/gl/UniformDeclaration.hpp"

namespace smks { namespace view { namespace gl
{
    class UniformDeclaration::PImpl
    {
    private:
        const osg::Uniform::Type        _type;
        const osg::Object::DataVariance _dataVariance;
    public:
        inline
        PImpl(osg::Uniform::Type        type,
              osg::Object::DataVariance dataVariance):
            _type           (type),
            _dataVariance   (dataVariance)
        {
            if (_type == osg::Uniform::UNDEFINED)
                throw sys::Exception(
                    "Invalid type.",
                    "Uniform Declaration Construction");
        }

        inline
        osg::Uniform::Type
        type() const
        {
            return _type;
        }

        inline
        osg::Object::DataVariance
        dataVariance() const
        {
            return _dataVariance;
        }
    };
}
}
}

using namespace smks;

// static
view::gl::UniformDeclaration::Ptr
view::gl::UniformDeclaration::create(const char*                name,
                                     osg::Uniform::Type         typ,
                                     osg::Object::DataVariance  dataVariance)
{
    Ptr ptr(new UniformDeclaration(name, typ, dataVariance));
    return ptr;
}

view::gl::UniformDeclaration::UniformDeclaration(const char*                name,
                                                 osg::Uniform::Type         typ,
                                                 osg::Object::DataVariance  dataVariance):
    ProgramInputDeclaration(name),
    _pImpl      (new PImpl(typ, dataVariance))
{ }

view::gl::UniformDeclaration::~UniformDeclaration()
{
    delete _pImpl;
}

osg::Uniform::Type
view::gl::UniformDeclaration::type() const
{
    return _pImpl->type();
}

osg::Object::DataVariance
view::gl::UniformDeclaration::dataVariance() const
{
    return _pImpl->dataVariance();
}

view::gl::UniformInput::Ptr
view::gl::UniformDeclaration::createInstance() const
{
    return UniformInput::create(*this);
}

osg::ref_ptr<osg::Uniform>
view::gl::UniformDeclaration::createUniformData() const
{
    osg::ref_ptr<osg::Uniform> uniform = new osg::Uniform(type(), name());

    switch (type())
    {
    case osg::Uniform::FLOAT:
        setUniformf(*uniform, 0.0f);
        break;
    case osg::Uniform::FLOAT_VEC2:
    case osg::Uniform::FLOAT_VEC3:
    case osg::Uniform::FLOAT_VEC4:
        setUniformfv(*uniform, math::Vector4f::Zero().eval().data());
        break;
    case osg::Uniform::INT:
    case osg::Uniform::SAMPLER_2D:
        setUniformi(*uniform, 0);
        break;
    case osg::Uniform::INT_VEC2:
    case osg::Uniform::INT_VEC3:
    case osg::Uniform::INT_VEC4:
        setUniformiv(*uniform, math::Vector4i::Zero().eval().data());
        break;
    case osg::Uniform::UNSIGNED_INT:
        setUniformui(*uniform, 0);
        break;
    case osg::Uniform::BOOL:
        setUniformb(*uniform, false);
        break;
    case osg::Uniform::FLOAT_MAT4:
        setUniformfv(*uniform, math::Affine3f::Identity().data());
        break;
    case osg::Uniform::UNDEFINED:
    default:
        {
            std::stringstream sstr;
            sstr << "Failed to initialize uniform '" << name() << "'.";
            throw sys::Exception(sstr.str().c_str(), "Uniform Creation From Declaration");
        }
    }

    if (dataVariance() != osg::Object::UNSPECIFIED)
        uniform->setDataVariance(dataVariance());

    return uniform;
}

