// ========================================================================= //
// Copyright 2016-2018 SUPAMONKS_STUDIO                                      //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
//                                                                           //
// ========================================================================= //
#include <cassert>
#include <smks/sys/ResourceDatabase.hpp>
#include "smks/view/gl/ContextExtensions.hpp"

namespace smks { namespace view { namespace gl
{
    const char* EXTENSIONS_ALIAS = "GLextensions";

    class ContextExtensions::PImpl
    {
    private:
        typedef osg::ref_ptr<osg::GLExtensions>         GLExtensionsPtr;
        typedef osg::buffered_value<GLExtensionsPtr>    BufferedGLExtensions;
    private:
        BufferedGLExtensions _contextGLExtensions;

    public:
        inline
        PImpl():
            _contextGLExtensions()
        { }

        inline
        const osg::GLExtensions*
        getOrCreateGLExtension(unsigned int contextId)
        {
            if (!_contextGLExtensions[contextId].valid())
                _contextGLExtensions[contextId] = new osg::GLExtensions(contextId);

            return _contextGLExtensions[contextId].get();
        }

        inline
        void
        unbindVertexArray()
        {
            for (unsigned int i = 0; i < _contextGLExtensions.size(); ++i)
                if (_contextGLExtensions[i].valid())
                    _contextGLExtensions[i]->glBindVertexArray(0);
        }
    };
}
}
}

using namespace smks;

view::gl::ContextExtensions::ContextExtensions():
    _pImpl(new PImpl())
{ }

view::gl::ContextExtensions::~ContextExtensions()
{
    delete _pImpl;
}

const osg::GLExtensions*
view::gl::ContextExtensions::getOrCreateGLExtension(unsigned int contextId)
{
    return _pImpl->getOrCreateGLExtension(contextId);
}

void
view::gl::ContextExtensions::unbindVertexArray()
{
    _pImpl->unbindVertexArray();
}

view::gl::ContextExtensions::Ptr const&
view::gl::getOrCreateContextExtensions(sys::ResourceDatabase& resources)
{
    if (!resources.stored<ContextExtensions::Ptr>(EXTENSIONS_ALIAS))
    {
        // create and store new context extensions
        ContextExtensions::Ptr ext = ContextExtensions::create();

        resources.store<ContextExtensions::Ptr>(EXTENSIONS_ALIAS, ext);
    }
    assert(resources.stored<ContextExtensions::Ptr>(EXTENSIONS_ALIAS));
    return resources.take<ContextExtensions::Ptr>(EXTENSIONS_ALIAS);
}

view::gl::ContextExtensions*
view::gl::getContextExtensions(sys::ResourceDatabase& resources)
{
    return resources.stored<ContextExtensions::Ptr>(EXTENSIONS_ALIAS)
        ? resources.take<ContextExtensions::Ptr>(EXTENSIONS_ALIAS).get()
        : nullptr;
}

const view::gl::ContextExtensions*
view::gl::getContextExtensions(const sys::ResourceDatabase& resources)
{
    return resources.stored<ContextExtensions::Ptr>(EXTENSIONS_ALIAS)
        ? resources.take<ContextExtensions::Ptr>(EXTENSIONS_ALIAS).get()
        : nullptr;
}
