// ========================================================================= //
// Copyright 2016-2018 SUPAMONKS_STUDIO                                      //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
//                                                                           //
// ========================================================================= //
#include <smks/sys/Exception.hpp>
#include <smks/util/string/getId.hpp>
#include "smks/view/gl/ProgramInputDeclaration.hpp"

namespace smks { namespace view { namespace gl
{
    class ProgramInputDeclaration::PImpl
    {
    private:
        const std::string   _name;
        const unsigned int  _id;
    public:
        explicit inline
        PImpl(const char* name):
            _name   (name),
            _id     (name && strlen(name) > 0 ? util::string::getId(name) : 0)
        {
            if (_id == 0)
                throw sys::Exception("Invalid name.", "Program Input Construction");
        }

        inline
        unsigned int
        id() const
        {
            return _id;
        }

        inline
        const char*
        name() const
        {
            return _name.c_str();
        }
    };
}
}
}

using namespace smks;

view::gl::ProgramInputDeclaration::ProgramInputDeclaration(const char* name):
    _pImpl(new PImpl(name))
{ }

view::gl::ProgramInputDeclaration::~ProgramInputDeclaration()
{
    delete _pImpl;
}

unsigned int
view::gl::ProgramInputDeclaration::id() const
{
    return _pImpl->id();
}

const char*
view::gl::ProgramInputDeclaration::name() const
{
    return _pImpl->name();
}

