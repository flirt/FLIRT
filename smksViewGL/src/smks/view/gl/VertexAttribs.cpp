// ========================================================================= //
// Copyright 2016-2018 SUPAMONKS_STUDIO                                      //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
//                                                                           //
// ========================================================================= //
#include "smks/view/gl/VertexAttrib.hpp"
#include "smks/view/gl/VertexAttribs.hpp"
#include "smks/view/gl/VertexAttribMap.hpp"

#define VERTEX_ATTRIB_DECL(_NAME_, _LOC_, _GLTYPE_, _NUM_COMPS_, _NORMALIZED_)                      \
    const VertexAttrib&                                                                             \
    _NAME_ ()                                                                                       \
    {                                                                                               \
        static VertexAttrib::Ptr ptr =                                                              \
            vertexAttribs().add(                                                                    \
                VertexAttrib::create("" #_NAME_ "", _LOC_, _GLTYPE_, _NUM_COMPS_, _NORMALIZED_));   \
        return *ptr;                                                                                \
    }

namespace smks { namespace view { namespace gl
{
    VERTEX_ATTRIB_DECL  (iColor,            2, GL_FLOAT, 4, false)
    VERTEX_ATTRIB_DECL  (iPosition,         1, GL_FLOAT, 4, false)
    VERTEX_ATTRIB_DECL  (iPreviousPosition, 6, GL_FLOAT, 3, false)
    VERTEX_ATTRIB_DECL  (iWidth,            6, GL_FLOAT, 1, false)
}
}
}
