// ========================================================================= //
// Copyright 2016-2018 SUPAMONKS_STUDIO                                      //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
//                                                                           //
// ========================================================================= //
#include <osg/Uniform>
#include "smks/view/gl/ProgramInput.hpp"
#include "smks/view/gl/UniformDeclaration.hpp"
#include "smks/view/gl/ProgramInputDeclarations.hpp"

#include "ProgramTemplates.hpp"

using namespace smks;

view::gl::EyeProgram::EyeProgram():
    view::gl::ProgramTemplate("shader/eye.vertex.glsl", // vertex shader
                              "",   // tesselation control
                              "",   // tesselation evaluation
                              "",   // geometry shader
                              "shader/eye.fragment.glsl",   // fragment shader
                              "")   // compute shader
{ }

void
view::gl::EyeProgram::finalize(osg::Program&) const
{ }

bool
view::gl::EyeProgram::isInputRelevant(unsigned int inputId) const
{
    return inputId == uObjectId().id() ||
        inputId == uObjectStateFlags().id() ||
        inputId == uCenter().id() ||
        inputId == uFront().id() ||
        inputId == uUp().id() ||
        inputId == uUsedAsPoint().id() ||
        inputId == uEyeCorneaIOR().id() ||
        inputId == uEyeGlobeColor().id() ||
        inputId == uEyeHighlightAngSizeD().id() ||
        inputId == uEyeHighlightColor().id() ||
        inputId == uEyeHighlightSmoothness().id() ||
        inputId == uEyeHighlightCoordsD().id() ||
        inputId == uEyeIrisAngSizeD().id() ||
        inputId == uEyeIrisColor().id() ||
        inputId == uEyeIrisDepth().id() ||
        inputId == uEyeIrisDimensions().id() ||
        inputId == uEyeIrisEdgeBlur().id() ||
        inputId == uEyeIrisEdgeOffset().id() ||
        inputId == uEyeIrisNormalSmoothness().id() ||
        inputId == uEyeIrisRotationD().id() ||
        inputId == uEyePupilBlur().id() ||
        inputId == uEyePupilDimensions().id() ||
        inputId == uEyePupilSize().id() ||
        inputId == uEyeRadius().id() ||
        inputId == uClockTime().id() ||
        inputId == uSelectionColor().id();
}

void
view::gl::EyeProgram::setUniformDefaults(unsigned int inputId, osg::Uniform& u) const
{
    if      (inputId == uObjectId().id())                   setUniformui(u, 0);
    else if (inputId == uObjectStateFlags().id())           setUniformi(u, 0);
    else if (inputId == uCenter().id())                     setUniformfv(u, math::Vector4f::UnitW().eval().data());
    else if (inputId == uFront().id())                      setUniformfv(u, math::Vector4f(0.0f, 0.0f, -5.0f, 1.0f).data());
    else if (inputId == uUp().id())                         setUniformfv(u, math::Vector4f(0.0f, 2.0f, 0.0f, 1.0f).data());
    else if (inputId == uUsedAsPoint().id())                setUniformb(u, true);
    else if (inputId == uEyeCorneaIOR().id())               setUniformf(u, 1.5f);
    else if (inputId == uEyeGlobeColor().id())              setUniformfv(u, math::Vector4f(0.7f, 0.7f, 0.7f, 1.0f).data());
    else if (inputId == uEyeHighlightAngSizeD().id())       setUniformf(u, 0.0f);
    else if (inputId == uEyeHighlightColor().id())          setUniformfv(u, math::Vector4f::Ones().eval().data());
    else if (inputId == uEyeHighlightSmoothness().id())     setUniformf(u, 0.0f);
    else if (inputId == uEyeHighlightCoordsD().id())        setUniformfv(u, math::Vector2f::Zero().eval().data());
    else if (inputId == uEyeIrisAngSizeD().id())            setUniformf(u, 57.0f);
    else if (inputId == uEyeIrisColor().id())               setUniformfv(u, math::Vector4f(0.0f, 0.3f, 0.3f, 1.0f).data());
    else if (inputId == uEyeIrisDepth().id())               setUniformf(u, 0.2f);
    else if (inputId == uEyeIrisDimensions().id())          setUniformfv(u, math::Vector2f::Ones().eval().data());
    else if (inputId == uEyeIrisEdgeBlur().id())            setUniformf(u, 0.0f);
    else if (inputId == uEyeIrisEdgeOffset().id())          setUniformf(u, 0.0f);
    else if (inputId == uEyeIrisNormalSmoothness().id())    setUniformf(u, 0.2f);
    else if (inputId == uEyeIrisRotationD().id())           setUniformf(u, 0.0f);
    else if (inputId == uEyePupilBlur().id())               setUniformf(u, 0.2f);
    else if (inputId == uEyePupilDimensions().id())         setUniformfv(u, math::Vector2f::Ones().eval().data());
    else if (inputId == uEyePupilSize().id())               setUniformf(u, 0.2f);
    else if (inputId == uEyeRadius().id())                  setUniformf(u, 1.0f);
    else if (inputId == uClockTime().id())                  setUniformf(u, 0.0f);
    else if (inputId == uSelectionColor().id())             setUniformfv(u, math::Vector4f(0.0f, 1.0f, 1.0f, 1.0f).data());
}

int
view::gl::EyeProgram::getSampler2dTexUnit(unsigned int inputId) const
{
    return -1;
}
