// ========================================================================= //
// Copyright 2016-2018 SUPAMONKS_STUDIO                                      //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
//                                                                           //
// ========================================================================= //
#include <osg/Uniform>
#include <osg/Program>
#include "smks/view/gl/ProgramInput.hpp"
#include "smks/view/gl/UniformDeclaration.hpp"
#include "smks/view/gl/VertexAttrib.hpp"
#include "smks/view/gl/VertexAttribs.hpp"
#include "smks/view/gl/ProgramInputDeclarations.hpp"

#include "ProgramTemplates.hpp"

using namespace smks;

view::gl::MotionProgram::MotionProgram():
    view::gl::ProgramTemplate("shader/motion.vertex.glsl",      // vertex shader
                              "",   // tesselation control
                              "",   // tesselation evaluation
                              "",   // geometry shader
                              "shader/motion.fragment.glsl",    // fragment shader
                              "")   // compute shader
{ }

void
view::gl::MotionProgram::finalize(osg::Program& program) const
{
    // vertex attribute
    iPreviousPosition().bindLocation(program);
}

bool
view::gl::MotionProgram::isInputRelevant(unsigned int inputId) const
{
    return inputId == uPreviousModelView().id() ||
        inputId == uPreviousProjection().id() ||
        inputId == uPreviousModelViewProjection().id() ||
        inputId == uMotionExponent().id() ||
        inputId == uMotionSign().id();
}

void
view::gl::MotionProgram::setUniformDefaults(unsigned int inputId, osg::Uniform& u) const
{
    if      (inputId == uPreviousModelView().id())              setUniformfv(u, math::Affine3f::Identity().data());
    else if (inputId == uPreviousProjection().id())             setUniformfv(u, math::Affine3f::Identity().data());
    else if (inputId == uPreviousModelViewProjection().id())    setUniformfv(u, math::Affine3f::Identity().data());
    else if (inputId == uMotionExponent().id())                 setUniformf(u, 0.0f);
    else if (inputId == uMotionSign().id())                     setUniformf(u, 0.0f);
}

int
view::gl::MotionProgram::getSampler2dTexUnit(unsigned int inputId) const
{
    return -1;
}
