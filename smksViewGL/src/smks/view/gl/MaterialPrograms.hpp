// ========================================================================= //
// Copyright 2016-2018 SUPAMONKS_STUDIO                                      //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
//                                                                           //
// ========================================================================= //
#pragma once

#include "smks/view/gl/MaterialProgramBase.hpp"
#include "smks/view/gl/MaterialPrograms.hpp"
#include "smks/view/gl/MaterialProgramMap.hpp"
#include "smks/view/gl/ProgramTemplates.hpp"

//-------------------------------------------------------------------------------------------
#define DEF_MATERIAL_PROG(_CLASS_, _ALIAS_, _TEMPLATE_)                                     \
    class _CLASS_: public smks::view::gl::MaterialProgramBase                               \
    {                                                                                       \
    public:                                                                                 \
        typedef std::shared_ptr<_CLASS_> Ptr;                                               \
    public:                                                                                 \
        static inline Ptr create() { Ptr ptr(new _CLASS_()); ptr->build(); return ptr; }    \
    protected:                                                                              \
        inline _CLASS_():                                                                   \
            smks::view::gl::MaterialProgramBase(_ALIAS_, _TEMPLATE_)                        \
        { }                                                                                 \
        void build();                                                                       \
    };                                                                                      \
    /* define global getter alongside the new class*/                                       \
    inline                                                                                  \
    const smks::view::gl::MaterialProgramBase&                                              \
    smks::view::gl::get_##_CLASS_ ()                                                        \
    {                                                                                       \
        static MaterialProgramBase::Ptr ptr = materials().add(_CLASS_::create());           \
        return *ptr;                                                                        \
    }
//-------------------------------------------------------------------------------------------

namespace smks { namespace view { namespace gl
{
    // points material programs
    DEF_MATERIAL_PROG(BasicPointsMaterial,          "basicPoints",      get_PointsProgram())
    // curves material programs
    DEF_MATERIAL_PROG(BasicHairCurvesMaterial,      "basicHair",        get_CurvesProgram())
    // polymesh material programs
    DEF_MATERIAL_PROG(BrushedMetalMeshMaterial,     "brushedMetal",     get_BlinnPhongProgram())
    DEF_MATERIAL_PROG(DielectricMeshMaterial,       "dielectric",       get_BlinnPhongProgram())
    DEF_MATERIAL_PROG(EyeMeshMaterial,              "eye",              get_EyeProgram())
    DEF_MATERIAL_PROG(MatteMeshMaterial,            "matte",            get_BlinnPhongProgram())
    DEF_MATERIAL_PROG(MetalMeshMaterial,            "metal",            get_BlinnPhongProgram())
    DEF_MATERIAL_PROG(MetallicPaintMeshMaterial,    "metallicPaint",    get_BlinnPhongProgram())
    DEF_MATERIAL_PROG(MirrorMeshMaterial,           "mirror",           get_BlinnPhongProgram())
    DEF_MATERIAL_PROG(ObjMeshMaterial,              "obj",              get_BlinnPhongProgram())
    DEF_MATERIAL_PROG(PlasticMeshMaterial,          "plastic",          get_BlinnPhongProgram())
    DEF_MATERIAL_PROG(ThinDielectricMeshMaterial,   "thinDielectric",   get_BlinnPhongProgram())
    DEF_MATERIAL_PROG(TranslucentMeshMaterial,      "translucent",      get_BlinnPhongProgram())
    DEF_MATERIAL_PROG(TranslucentSSMeshMaterial,    "translucentSS",    get_BlinnPhongProgram())
    DEF_MATERIAL_PROG(VelvetMeshMaterial,           "velvet",           get_BlinnPhongProgram())
}
}
}
