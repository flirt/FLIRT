// ========================================================================= //
// Copyright 2016-2018 SUPAMONKS_STUDIO                                      //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
//                                                                           //
// ========================================================================= //
#include <boost/unordered_map.hpp>

#include <osg/Program>

#include <smks/parm/BuiltIn.hpp>
#include <smks/xchg/IdSet.hpp>
#include <smks/util/string/getId.hpp>

#include "smks/view/gl/ProgramInputDeclaration.hpp"
#include "smks/view/gl/MaterialProgramBase.hpp"
#include "smks/view/gl/ProgramTemplate.hpp"

namespace smks { namespace view { namespace gl
{
    class MaterialProgramBase::PImpl
    {
    private:
        typedef boost::unordered_map<unsigned int, xchg::IdSet> IdSetMap;
    private:
        const std::string       _alias;
        const unsigned int      _id;
        const ProgramTemplate&  _progTemplate;
        xchg::IdSet             _inputs;
        IdSetMap                _parmsAffectingInput;   // uniform ID -> [ parameter ID ]*
    public:
        inline
        PImpl(const std::string& alias, const ProgramTemplate& progTemplate):
            _alias              (alias),
            _id                 (util::string::getId(alias.c_str())),
            _progTemplate       (progTemplate),
            _inputs             (),
            _parmsAffectingInput()
        { }

        inline
        const std::string&
        alias() const
        {
            return _alias;
        }

        inline
        unsigned int
        id() const
        {
            return _id;
        }

        inline
        const ProgramTemplate&
        progTemplate() const
        {
            return _progTemplate;
        }

        inline
        void
        parmAffectsInput(const parm::BuiltIn&           parm,
                         const ProgramInputDeclaration& decl)
        {
            _inputs.insert(decl.id());
            _parmsAffectingInput[decl.id()].insert(parm.id());
        }

        inline
        const xchg::IdSet&
        getInputs() const
        {
            return _inputs;
        }

        inline
        void
        getParmsAffectingInput(unsigned int input,
                               xchg::IdSet& parms) const
        {
            IdSetMap::const_iterator const& it = _parmsAffectingInput.find(input);
            if (it != _parmsAffectingInput.end())
                parms = it->second;
            else
                parms.clear();
        }

        inline
        void
        getInputsAffectedByParm(unsigned int parm,
                                xchg::IdSet& inputs) const
        {
            inputs.clear();
            for (xchg::IdSet::const_iterator id = _inputs.begin(); id != _inputs.end(); ++id)
            {
                IdSetMap::const_iterator const& it = _parmsAffectingInput.find(*id);
                if (it == _parmsAffectingInput.end() ||
                    it->second.find(parm) == it->second.end())
                    continue;
                inputs.insert(*id);
            }
        }
    };
}
}
}

using namespace smks;

////////////////////////////
// class MaterialProgramBase
////////////////////////////
view::gl::MaterialProgramBase::MaterialProgramBase(const char* alias, const ProgramTemplate& progTemplate):
    _pImpl(new PImpl(alias, progTemplate))
{ }

view::gl::MaterialProgramBase::~MaterialProgramBase()
{
    delete _pImpl;
}

const char*
view::gl::MaterialProgramBase::alias() const
{
    return _pImpl->alias().c_str();
}

unsigned int
view::gl::MaterialProgramBase::id() const
{
    return _pImpl->id();
}

const view::gl::ProgramTemplate&
view::gl::MaterialProgramBase::progTemplate() const
{
    return _pImpl->progTemplate();
}

void
view::gl::MaterialProgramBase::parmAffectsInput(const parm::BuiltIn&            parm,
                                                const ProgramInputDeclaration&  decl)
{
    _pImpl->parmAffectsInput(parm, decl);
}

void
view::gl::MaterialProgramBase::getParmsAffectingInput(unsigned int input, xchg::IdSet& parms) const
{
    return _pImpl->getParmsAffectingInput(input, parms);
}

const xchg::IdSet&
view::gl::MaterialProgramBase::getInputs() const
{
    return _pImpl->getInputs();
}

void
view::gl::MaterialProgramBase::getInputsAffectedByParm(unsigned int parm, xchg::IdSet& inputs) const
{
    return _pImpl->getInputsAffectedByParm(parm, inputs);
}
