// ========================================================================= //
// Copyright 2016-2018 SUPAMONKS_STUDIO                                      //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
//                                                                           //
// ========================================================================= //
#include "smks/view/gl/ProgramInputDeclaration.hpp"
#include "smks/view/gl/ProgramInputDeclarations.hpp"
#include "smks/view/gl/ProgramInputDeclarationMap.hpp"
#include "smks/view/gl/UniformDeclaration.hpp"
#include "smks/view/gl/Sampler2dDeclaration.hpp"

#define UNIFORM_DECL(_TYPE_, _NAME_, _VARIANCE_)                                \
    const ProgramInputDeclaration&                                              \
    _NAME_ ()                                                                   \
    {                                                                           \
        static ProgramInputDeclaration::Ptr ptr =                               \
            programInputDeclarations().add(                                     \
                UniformDeclaration::create("" #_NAME_ "", _TYPE_, _VARIANCE_)); \
        return *ptr;                                                            \
    }

#define SAMPLER2D_DECL(_NAME_)                                  \
    const ProgramInputDeclaration&                              \
    _NAME_ ()                                                   \
    {                                                           \
        static ProgramInputDeclaration::Ptr ptr =               \
            programInputDeclarations().add(                     \
                Sampler2dDeclaration::create("" #_NAME_ ""));   \
        return *ptr;                                            \
    }

namespace smks { namespace view { namespace gl
{
    UNIFORM_DECL    (osg::Uniform::BOOL,        uAntiAlias                      , osg::Object::STATIC)
    UNIFORM_DECL    (osg::Uniform::FLOAT_VEC4,  uCenter                         , osg::Object::DYNAMIC)
    UNIFORM_DECL    (osg::Uniform::FLOAT,       uClockTime                      , osg::Object::DYNAMIC)
    UNIFORM_DECL    (osg::Uniform::FLOAT_VEC4,  uColor                          , osg::Object::DYNAMIC)
    UNIFORM_DECL    (osg::Uniform::SAMPLER_2D,  uDiffuseTexture                 , osg::Object::DYNAMIC)
    UNIFORM_DECL    (osg::Uniform::FLOAT,       uEyeCorneaIOR                   , osg::Object::DYNAMIC)
    UNIFORM_DECL    (osg::Uniform::FLOAT_VEC4,  uEyeGlobeColor                  , osg::Object::DYNAMIC)
    UNIFORM_DECL    (osg::Uniform::FLOAT,       uEyeHighlightAngSizeD           , osg::Object::DYNAMIC)
    UNIFORM_DECL    (osg::Uniform::FLOAT_VEC4,  uEyeHighlightColor              , osg::Object::DYNAMIC)
    UNIFORM_DECL    (osg::Uniform::FLOAT,       uEyeHighlightSmoothness         , osg::Object::DYNAMIC)
    UNIFORM_DECL    (osg::Uniform::FLOAT_VEC2,  uEyeHighlightCoordsD            , osg::Object::DYNAMIC)
    UNIFORM_DECL    (osg::Uniform::FLOAT,       uEyeIrisAngSizeD                , osg::Object::DYNAMIC)
    UNIFORM_DECL    (osg::Uniform::FLOAT_VEC4,  uEyeIrisColor                   , osg::Object::DYNAMIC)
    UNIFORM_DECL    (osg::Uniform::FLOAT,       uEyeIrisDepth                   , osg::Object::DYNAMIC)
    UNIFORM_DECL    (osg::Uniform::FLOAT_VEC2,  uEyeIrisDimensions              , osg::Object::DYNAMIC)
    UNIFORM_DECL    (osg::Uniform::FLOAT,       uEyeIrisEdgeBlur                , osg::Object::DYNAMIC)
    UNIFORM_DECL    (osg::Uniform::FLOAT,       uEyeIrisEdgeOffset              , osg::Object::DYNAMIC)
    UNIFORM_DECL    (osg::Uniform::FLOAT,       uEyeIrisNormalSmoothness        , osg::Object::DYNAMIC)
    UNIFORM_DECL    (osg::Uniform::FLOAT,       uEyeIrisRotationD               , osg::Object::DYNAMIC)
    UNIFORM_DECL    (osg::Uniform::FLOAT,       uEyePupilBlur                   , osg::Object::DYNAMIC)
    UNIFORM_DECL    (osg::Uniform::FLOAT_VEC2,  uEyePupilDimensions             , osg::Object::DYNAMIC)
    UNIFORM_DECL    (osg::Uniform::FLOAT,       uEyePupilSize                   , osg::Object::DYNAMIC)
    UNIFORM_DECL    (osg::Uniform::FLOAT,       uEyeRadius                      , osg::Object::DYNAMIC)
    UNIFORM_DECL    (osg::Uniform::FLOAT_VEC4,  uFront                          , osg::Object::DYNAMIC)
    UNIFORM_DECL    (osg::Uniform::BOOL,        uIsBillboard                    , osg::Object::DYNAMIC)
    UNIFORM_DECL    (osg::Uniform::FLOAT_VEC3,  uLightDirection                 , osg::Object::DYNAMIC)
    UNIFORM_DECL    (osg::Uniform::FLOAT_VEC4,  uPrimaryColor                   , osg::Object::DYNAMIC)
    SAMPLER2D_DECL  (uPrimaryMap)
    UNIFORM_DECL    (osg::Uniform::FLOAT_VEC4,  uModelViewScale                 , osg::Object::DYNAMIC)
    UNIFORM_DECL    (osg::Uniform::FLOAT,       uMotionExponent                 , osg::Object::STATIC)
    UNIFORM_DECL    (osg::Uniform::FLOAT,       uMotionSign                     , osg::Object::STATIC)
    UNIFORM_DECL    (osg::Uniform::INT,         uObjectStateFlags               , osg::Object::DYNAMIC)
    UNIFORM_DECL    (osg::Uniform::INT_VEC4,    uObjectId                       , osg::Object::STATIC)
    UNIFORM_DECL    (osg::Uniform::FLOAT_MAT4,  uPreviousModelView              , osg::Object::DYNAMIC)
    UNIFORM_DECL    (osg::Uniform::FLOAT_MAT4,  uPreviousModelViewProjection    , osg::Object::DYNAMIC)
    UNIFORM_DECL    (osg::Uniform::FLOAT_MAT4,  uPreviousProjection             , osg::Object::DYNAMIC)
    UNIFORM_DECL    (osg::Uniform::FLOAT_VEC2,  uRecTextureSize                 , osg::Object::DYNAMIC)
    UNIFORM_DECL    (osg::Uniform::FLOAT_MAT4,  uScalelessModelView             , osg::Object::DYNAMIC)
    UNIFORM_DECL    (osg::Uniform::FLOAT,       uScaling                        , osg::Object::DYNAMIC)
    UNIFORM_DECL    (osg::Uniform::FLOAT_VEC4,  uSecondaryColor                 , osg::Object::DYNAMIC)
    SAMPLER2D_DECL  (uSecondaryMap)
    UNIFORM_DECL    (osg::Uniform::FLOAT_VEC3,  uSelectionColor                 , osg::Object::DYNAMIC)
    UNIFORM_DECL    (osg::Uniform::FLOAT_VEC4,  uTertiaryColor                  , osg::Object::DYNAMIC)
    SAMPLER2D_DECL  (uTertiaryMap)
    UNIFORM_DECL    (osg::Uniform::FLOAT_VEC4,  uTextColor                      , osg::Object::STATIC)
    UNIFORM_DECL    (osg::Uniform::SAMPLER_2D,  uTextGlyphs                     , osg::Object::DYNAMIC)
    UNIFORM_DECL    (osg::Uniform::FLOAT_VEC4,  uUp                             , osg::Object::DYNAMIC)
    UNIFORM_DECL    (osg::Uniform::BOOL,        uUsedAsPoint                    , osg::Object::DYNAMIC)
    UNIFORM_DECL    (osg::Uniform::INT,         uVertexAttributes               , osg::Object::STATIC)
}
}
}
