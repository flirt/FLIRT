// ========================================================================= //
// Copyright 2016-2018 SUPAMONKS_STUDIO                                      //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
//                                                                           //
// ========================================================================= //
#include <osg/Uniform>
#include <osg/Program>
#include <osg/PrimitiveSet>
#include "smks/view/gl/ProgramInput.hpp"
#include "smks/view/gl/UniformDeclaration.hpp"
#include "smks/view/gl/VertexAttrib.hpp"
#include "smks/view/gl/VertexAttribs.hpp"
#include "smks/view/gl/ProgramInputDeclarations.hpp"

#include "ProgramTemplates.hpp"

using namespace smks;

view::gl::CurvesProgram::CurvesProgram():
    view::gl::ProgramTemplate("shader/curves.vertex.glsl",  // vertex shader
                              "",   // tesselation control
                              "",   // tesselation evaluation
                              "shader/curves.geometry.glsl",    // geometry shader
                              "shader/curves.fragment.glsl",    // fragment shader
                              "")   // compute shader
{ }

void
view::gl::CurvesProgram::finalize(osg::Program& program) const
{
    // vertex attribute
    iWidth().bindLocation(program);
    // geometry shader settings
    program.setParameter(GL_GEOMETRY_INPUT_TYPE_EXT,    osg::PrimitiveSet::LINES_ADJACENCY);
    program.setParameter(GL_GEOMETRY_OUTPUT_TYPE_EXT,   osg::PrimitiveSet::TRIANGLE_STRIP);
    program.setParameter(GL_GEOMETRY_VERTICES_OUT_EXT,  42);
}

bool
view::gl::CurvesProgram::isInputRelevant(unsigned int inputId) const
{
    return inputId == uObjectId().id() ||
        inputId == uObjectStateFlags().id() ||
        inputId == uColor().id() ||
        inputId == uScaling().id() ||
        inputId == uVertexAttributes().id() ||
        inputId == uClockTime().id() ||
        inputId == uSelectionColor().id();
}

void
view::gl::CurvesProgram::setUniformDefaults(unsigned int inputId, osg::Uniform& u) const
{
    if      (inputId == uObjectId().id())           setUniformui(u, 0);
    else if (inputId == uObjectStateFlags().id())   setUniformi(u, 0);
    else if (inputId == uColor().id())              setUniformui(u, -1);
    else if (inputId == uScaling().id())            setUniformf(u, 1.0f);
    else if (inputId == uVertexAttributes().id())   setUniformi(u, 0);
    else if (inputId == uClockTime().id())          setUniformf(u, 0.0f);
    else if (inputId == uSelectionColor().id())     setUniformfv(u, math::Vector4f(0.0f, 1.0f, 1.0f, 1.0f).data());
}

int
view::gl::CurvesProgram::getSampler2dTexUnit(unsigned int inputId) const
{
    return -1;
}
