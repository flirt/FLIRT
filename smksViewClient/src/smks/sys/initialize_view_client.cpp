// ========================================================================= //
// Copyright 2016-2018 SUPAMONKS_STUDIO                                      //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
//                                                                           //
// ========================================================================= //
#include <smks/sys/Log.hpp>

#if defined(FLIRT_LOG_ENABLED)
INITIALIZE_NULL_EASYLOGGINGPP;
#endif // defined(FLIRT_LOG_ENABLED)

#include <smks/sys/initialize_view_gl.hpp>
#include <smks/sys/initialize_clientbase.hpp>
#include "smks/sys/initialize_view_client.hpp"

namespace smks { namespace sys
{
    static bool _smksViewClientInitialized = false;
}
}

void
smks::sys::initializeSmksViewClient()
{
    if (_smksViewClientInitialized)
        return;

#if defined(FLIRT_LOG_ENABLED)
    el::Helpers::setStorage(smks::sys::sharedLoggingRepository());
#endif //defined(FLIRT_LOG_ENABLED)

    initializeSmksViewGL();
    initializeSmksClientBase();

    _smksViewClientInitialized = true;
}

void
smks::sys::finalizeSmksViewClient()
{
    if (!_smksViewClientInitialized)
        return;

    finalizeSmksClientBase();
    finalizeSmksViewGL();

#if defined(FLIRT_LOG_ENABLED)
    el::Helpers::setStorage(nullptr);
#endif //defined(FLIRT_LOG_ENABLED)

    _smksViewClientInitialized = false;
}


bool
smks::sys::smksViewClientInitialized()
{
    return _smksViewClientInitialized;
}
