// ========================================================================= //
// Copyright 2016-2018 SUPAMONKS_STUDIO                                      //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
//                                                                           //
// ========================================================================= //
#include <iostream>
#include <osgViewer/Viewer>

#include "CullVisitor.hpp"
#include "XTransform.hpp"

using namespace smks;

view::CullVisitor::CullVisitor():
    osgUtil::CullVisitor()
{ }

view::CullVisitor::CullVisitor(const CullVisitor& other):
    osgUtil::CullVisitor(other)
{ }

// virtual
osgUtil::CullVisitor*
view::CullVisitor::clone() const
{
    return new CullVisitor(*this);
}

// virtual
void
view::CullVisitor::apply(osg::Transform& node)
{
    if (isCulled(node))
        return;

    const XTransform* xfm = dynamic_cast<const XTransform*>(&node);
    if (!xfm || xfm->inheritsXforms())
    {
        osgUtil::CullVisitor::apply(node);  // default behavior
        return;
    }
    //---
    // the following code handles the case of XTransform
    // nodes which do NOT inherit their transforms from
    // their parents (unless osg::Camera for view matrix).

    // push the culling mode.
    pushCurrentMask();

    // push the node's state.
    osg::StateSet* node_state = node.getStateSet();
    if (node_state) pushStateSet(node_state);

    osg::Matrixf viewMatrix;
    getCurrentViewMatrix(viewMatrix);

    osg::RefMatrix* matrix = createOrReuseMatrix(*getModelViewMatrix());
    node.computeLocalToWorldMatrix(*matrix, this);
    matrix->osg::Matrix::postMult(viewMatrix);
    pushModelViewMatrix(matrix, node.getReferenceFrame());

    handle_cull_callbacks_and_traverse(node);

    popModelViewMatrix();

    // pop the node's state off the render graph stack.
    if (node_state) popStateSet();

    // pop the culling mode.
    popCurrentMask();
}

void
view::CullVisitor::getCurrentViewMatrix(osg::Matrixf& mtx) const
{
    mtx.makeIdentity();

    const osgViewer::Viewer* view = dynamic_cast<const osgViewer::Viewer*>(
        getRenderInfo().getView());
    const osgGA::CameraManipulator* camManip = view
        ? view->getCameraManipulator()
        : nullptr;

    if (camManip)
        mtx = camManip->getInverseMatrix();
}
