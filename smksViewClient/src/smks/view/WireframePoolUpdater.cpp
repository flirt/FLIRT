// ========================================================================= //
// Copyright 2016-2018 SUPAMONKS_STUDIO                                      //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
//                                                                           //
// ========================================================================= //
#include <iostream>

#include <osg/Geode>

#include <smks/scn/TreeNode.hpp>
#include <smks/scn/PolyMesh.hpp>
#include <smks/parm/BuiltIn.hpp>
#include <smks/parm/BuiltIns.hpp>
#include <smks/ClientBase.hpp>
#include <smks/ClientBindingBase.hpp>

#include "WireframePoolUpdater.hpp"

using namespace smks;

// static
view::WireframePoolUpdater::Ptr
view::WireframePoolUpdater::create(ClientBase::WPtr const&      client,
                                   osg::Camera::BufferComponent colorIdAttachment)
{
    Ptr ptr(new WireframePoolUpdater(client, colorIdAttachment));
    return ptr;
}

view::WireframePoolUpdater::WireframePoolUpdater(ClientBase::WPtr const&        client,
                                                 osg::Camera::BufferComponent   colorIdAttachment):
    _client             (client),
    _objectIdAttachment (colorIdAttachment),
    _wireframeMap       (),
    _color              (1.0f, 1.0f, 1.0f, 1.0f)
{ }

void
view::WireframePoolUpdater::dispose()
{
    while(!_wireframeMap.empty())
        removeWireframeGeometry(_wireframeMap.begin()->first);
}

void
view::WireframePoolUpdater::setColor(const osg::Vec4f& value)
{
    _color = value;
}

// virtual
void
view::WireframePoolUpdater::handle(scn::TreeNode&,
                                   const xchg::ParmEvent& evt)
{
    if (evt.parm() != parm::glWireIndices().id())
        return;
    if (!evt.emittedByNode())
        return;

    switch (evt.type())
    {
    case smks::xchg::ParmEvent::UNKNOWN:
        break;
    case smks::xchg::ParmEvent::PARM_ADDED:
        addWireframeGeometry(evt.node());
        updateWireframeGeometry(evt.node());
        break;
    case smks::xchg::ParmEvent::PARM_CHANGED:
        updateWireframeGeometry(evt.node());
        break;
    case smks::xchg::ParmEvent::PARM_REMOVED:
        removeWireframeGeometry(evt.node());
        break;
    default:
        break;
    }
}

xchg::Data::Ptr
view::WireframePoolUpdater::getMeshWireIndices(unsigned int nodeId) const
{
    ClientBase::Ptr const&      client      = _client.lock();
    ClientBindingBase::Ptr const&       bind        = client ? client->getBinding(nodeId) : nullptr;
    scn::TreeNode::Ptr const&   dataNode    = bind ? bind->dataNode().lock() : nullptr;
    xchg::Data::Ptr             indices     = nullptr;

    if (dataNode ||
        !dataNode->is(xchg::NodeClass::MESH))
        dataNode->asSceneObject()->asDrawable()->asPolyMesh()
            ->getParameter<xchg::Data::Ptr>(parm::glWireIndices(), indices);

    return indices;
}

osg::Geode*
view::WireframePoolUpdater::getGeode(unsigned int nodeId)
{
    ClientBase::Ptr const&  client      = _client.lock();
    ClientBindingBase::Ptr const&   bind        = client ? client->getBinding(nodeId) : nullptr;
    osg::Object*            osgObject   = bind ? bind->asOsgObject() : nullptr;

    return osgObject && osgObject->asNode()
        ? osgObject->asNode()->asGeode()
        : nullptr;
}

void
view::WireframePoolUpdater::addWireframeGeometry(unsigned int nodeId)
{
    WireframeGeometryMap::const_iterator const& it = _wireframeMap.find(nodeId);
    if (it != _wireframeMap.end())
        return;

    osg::Geode* geode = getGeode(nodeId);
    if (!geode)
        return;

    WireframeGeometry::Ptr const& geom = WireframeGeometry::create(nodeId, _client, _objectIdAttachment);
    geom->setColor(_color);
    geode->addDrawable(geom.get());
    _wireframeMap.insert(std::pair<unsigned int, WireframeGeometry::Ptr>(nodeId, geom));
}

void
view::WireframePoolUpdater::updateWireframeGeometry(unsigned int nodeId)
{
    WireframeGeometryMap::const_iterator const& it = _wireframeMap.find(nodeId);
    if (it == _wireframeMap.end())
        return;

    xchg::Data::Ptr const& indices = getMeshWireIndices(nodeId);

    if (indices)
        it->second->setIndices(indices);
    else
        removeWireframeGeometry(nodeId);
}

void
view::WireframePoolUpdater::removeWireframeGeometry(unsigned int nodeId)
{
    WireframeGeometryMap::const_iterator const& it = _wireframeMap.find(nodeId);
    if (it == _wireframeMap.end())
        return;

    osg::Geode* geode = getGeode(nodeId);
    if (geode)
        geode->removeDrawable(it->second.get());
    _wireframeMap.erase(it);
}
