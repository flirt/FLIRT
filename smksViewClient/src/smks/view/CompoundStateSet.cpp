// ========================================================================= //
// Copyright 2016-2018 SUPAMONKS_STUDIO                                      //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
//                                                                           //
// ========================================================================= //
#include <cassert>
#include "CompoundStateSet.hpp"

using namespace smks;

view::CompoundStateSet::CompoundStateSet():
    osg::StateSet   (),
    _sets           ()
{ }

view::CompoundStateSet::~CompoundStateSet()
{
    osg::StateSet::clear();
    _sets.clear();
}

void
view::CompoundStateSet::add(const osg::StateSet* s)
{
    if (s == nullptr)
        return;
    for (size_t i = 0; i < _sets.size(); ++i)
        if (_sets[i] == s)
            return;
    _sets.push_back(s);
    reevaluate();
}

void
view::CompoundStateSet::remove(const osg::StateSet* s)
{
    if (s == nullptr)
        return;
    for (size_t i = 0; i < _sets.size(); ++i)
        if (_sets[i] == s)
        {
            std::swap(_sets[i], _sets.back());
            _sets.pop_back();
            reevaluate();
            break;
        }
}

void
view::CompoundStateSet::reevaluate()
{
    clear();

    // registered state sets are traversed in reverse order.
    for (int i = _sets.size() - 1; i >= 0; --i)
    {
        if (_sets[i] == nullptr)
            continue;

        // mode list
        {
            const osg::StateSet::ModeList& modeList = _sets[i]->getModeList();

            for (osg::StateSet::ModeList::const_iterator it = modeList.begin(); it != modeList.end(); ++it)
                osg::StateSet::setMode(it->first, it->second);
        }
        // attribute list
        {
            const osg::StateSet::AttributeList& attrList = _sets[i]->getAttributeList();

            for (osg::StateSet::AttributeList::const_iterator it = attrList.begin(); it != attrList.end(); ++it)
                osg::StateSet::setAttribute(it->second.first, it->second.second);
        }
        // texture mode list
        {
            const osg::StateSet::TextureModeList& textureModeList = _sets[i]->getTextureModeList();

            for (size_t unit = 0; unit < textureModeList.size(); ++unit)
            {
                const osg::StateSet::ModeList& modeList = textureModeList[unit];

                for (osg::StateSet::ModeList::const_iterator it = modeList.begin(); it != modeList.end(); ++it)
                    osg::StateSet::setTextureMode(unit, it->first, it->second);
            }
        }
        // texture attribute list
        {
            const osg::StateSet::TextureAttributeList& textureAttrList = _sets[i]->getTextureAttributeList();

            for (size_t unit = 0; unit < textureAttrList.size(); ++unit)
            {
                const osg::StateSet::AttributeList& attrList = textureAttrList[unit];

                for (osg::StateSet::AttributeList::const_iterator it = attrList.begin(); it != attrList.end(); ++it)
                    osg::StateSet::setTextureAttribute(unit, it->second.first, it->second.second);
            }
        }
        // uniform list
        {
            const osg::StateSet::UniformList& uniformList = _sets[i]->getUniformList();

            for (osg::StateSet::UniformList::const_iterator it = uniformList.begin(); it != uniformList.end(); ++it)
            {
                assert(it->first == it->second.first->getName());
                osg::StateSet::addUniform(it->second.first.get(), it->second.second);
            }
        }
    }
}
