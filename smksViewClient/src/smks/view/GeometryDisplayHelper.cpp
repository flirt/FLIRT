// ========================================================================= //
// Copyright 2016-2018 SUPAMONKS_STUDIO                                      //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
//                                                                           //
// ========================================================================= //
#include <iostream>
#include <osg/Texture2D>

#include <smks/sys/Log.hpp>
#include <smks/sys/Exception.hpp>
#include <smks/parm/BuiltIn.hpp>
#include <smks/parm/BuiltIns.hpp>
#include <smks/xchg/NodeCode.hpp>
#include <smks/xchg/NodeCodes.hpp>
#include <smks/util/string/getId.hpp>
#include <smks/ClientBase.hpp>
#include <smks/view/gl/ProgramInput.hpp>
#include <smks/view/gl/MaterialProgramBase.hpp>
#include <smks/view/gl/MaterialPrograms.hpp>
#include <smks/view/gl/DefaultInputs.hpp>

#include "StateAttributeProvider.hpp"
#include "GeometryDisplayHelper.hpp"

using namespace smks;

// static
view::GeometryDisplayHelper::Ptr
view::GeometryDisplayHelper::create(unsigned int            id,
                                    ClientBase::WPtr const& cl,
                                    osg::StateSet*          stateSet)
{
    Ptr ptr(new GeometryDisplayHelper(id, cl, stateSet));
    ptr->build(ptr);
    return ptr;
}

view::GeometryDisplayHelper::GeometryDisplayHelper(unsigned int             id,
                                                   ClientBase::WPtr const&  cl,
                                                   osg::StateSet*           stateSet):
    _client     (cl),
    _scnId      (id),
    _self       (),
    _program    (nullptr),
    _stateSet   (stateSet)
{
    ClientBase::Ptr const& client = _client.lock();
    if (!client)
        throw sys::Exception(
            "Invalid client.",
            "Geometry Display Helper Construction");
    if (!_stateSet)
        throw sys::Exception(
            "Invalid state set.",
            "Geometry Display Helper Construction");
}

void
view::GeometryDisplayHelper::build(Ptr const& ptr)
{
    _self = ptr;
    refreshProgram();
    assert(program());
}

void
view::GeometryDisplayHelper::setProvider(unsigned int               linkId,
                                         StateAttributeProvider*    provider)   //<! provider can be null
{
    Ptr const& self = _self.lock();
    if (!self)
        return;

    ProviderType typ = UNSPECIFIED_PROVIDER;
    if (linkId == parm::materialId().id())
        typ = PROVIDER_MATERIAL;
    else if (linkId == parm::subsurfaceId().id())
        typ = PROVIDER_SUBSURFACE;

    FLIRT_LOG(LOG(DEBUG)
        << "setting provider (" << provider << ") "
        << "as " << (typ == PROVIDER_MATERIAL ? "material" : (typ == PROVIDER_SUBSURFACE ? "subsurface" : "??"))
        << " @ geom display helper (" << this << ")";)

    if (typ == UNSPECIFIED_PROVIDER)
        return;

    StateAttributeProvider::Ptr prevProvider = nullptr;

    // deregister previous state attributes
    if (_providers[typ].lock(prevProvider) &&
        prevProvider.get() == provider)
        return; // no actual change

    //###############
    if (prevProvider)
        prevProvider->deregisterDisplayHelper(self);

    _providers[typ] = provider;

    if (provider)
        provider->registerDisplayHelper(self);
    //###############

    refreshProgram();
}

void
view::GeometryDisplayHelper::refreshProgram()
{
    setProgram(selectProgram());
}

void
    view::GeometryDisplayHelper::setProgram(const gl::MaterialProgramBase* prog)
{
    if (prog == program())
        return; // no actual change

    const gl::MaterialProgramBase* prvProgram = program();
    const gl::MaterialProgramBase* newProgram = prog;

    _program = prog;

    // have all associated providers account for the newly selected program,
    // and update their stored attributes
    for (size_t i = 0; i < NUM_PROVIDERS; ++i)
    {
        StateAttributeProvider::Ptr provider = nullptr;
        if (_providers[i].lock(provider))
            provider->handleDisplayHelperProgramChanged(_scnId, newProgram, prvProgram); // may call handleUniformCreated/handleUniformDestroyed
    }
    // find program's uniforms that could not be found in current providers and
    // add default uniforms instead
    if (program())
    {
        const xchg::IdSet& progInputs = program()->getInputs();
        for (xchg::IdSet::const_iterator inId = progInputs.begin(); inId != progInputs.end(); ++inId)
        {
            bool found = false;
            for (size_t i = 0; i < NUM_PROVIDERS; ++i)
            {
                StateAttributeProvider::Ptr provider = nullptr;
                found = found ||
                    (_providers[i].lock(provider) &&
                    provider->getInput(program()->id(), *inId));
            }
            if (!found)
            {
                //                                  #####################
                gl::ProgramInput::Ptr const& input = gl::defaultInputs().getOrCreateInput(program()->progTemplate(), *inId);
                            //                      #####################
                addInputToStateSet(input.get());
            }
        }
    }
}

const view::gl::MaterialProgramBase*
view::GeometryDisplayHelper::selectProgram() const
{
    ClientBase::Ptr const&  client          = _client.lock();
    std::string             providerCodes   [NUM_PROVIDERS];
    const xchg::NodeClass   geometryClass   = client
        ? client->getNodeClass(_scnId)
        : xchg::NONE;

    for (size_t i = 0; i < NUM_PROVIDERS; ++i)
    {
        StateAttributeProvider::Ptr provider = nullptr;

        if (_providers[i].lock(provider))
            provider->getCode(providerCodes[i]);
    }

    const view::gl::MaterialProgramBase* prog = selectProgram(geometryClass, providerCodes);
    assert(prog);

#if defined(FLIRT_LOG_ENABLED)
    {
        std::stringstream sstr;
        sstr << "geometry type: " << std::to_string(geometryClass) << "\tproviders = { ";
        for (size_t i = 0; i < NUM_PROVIDERS; ++i)
            sstr << "'" << providerCodes[i] << "' ";
        sstr << "}\n\t-> selected program: '" << prog->alias() << "'";
        LOG(DEBUG) << sstr.str();
    }
#endif // defined(FLIRT_LOG_ENABLED)

    return prog;
}

// static
const view::gl::MaterialProgramBase*
view::GeometryDisplayHelper::selectProgram(xchg::NodeClass      geometryClass,
                                           const std::string*   providerCodes)
{
    const view::gl::MaterialProgramBase* prog = nullptr;

    const unsigned int materialCode     = util::string::getId(providerCodes[PROVIDER_MATERIAL].c_str());
    const unsigned int subsurfaceCode   = util::string::getId(providerCodes[PROVIDER_SUBSURFACE].c_str());

    ////////// POLYMESH PROGRAM MATERIAL SELECTION //////////////////////////
    if (geometryClass & xchg::MESH)
    {
        if      (materialCode == xchg::code::brushedMetal()     .id()) prog = &gl::get_BrushedMetalMeshMaterial();
        else if (materialCode == xchg::code::dielectric()       .id())  prog = &gl::get_DielectricMeshMaterial();
        else if (materialCode == xchg::code::eye()              .id())  prog = &gl::get_EyeMeshMaterial();
        else if (materialCode == xchg::code::glass()            .id())  prog = &gl::get_DielectricMeshMaterial();
        else if (materialCode == xchg::code::matte()            .id())  prog = &gl::get_MatteMeshMaterial();
        else if (materialCode == xchg::code::metal()            .id())  prog = &gl::get_MetalMeshMaterial();
        else if (materialCode == xchg::code::metallicPaint()    .id())  prog = &gl::get_MetallicPaintMeshMaterial();
        else if (materialCode == xchg::code::mirror()           .id())  prog = &gl::get_MirrorMeshMaterial();
        else if (materialCode == xchg::code::obj()              .id())  prog = &gl::get_ObjMeshMaterial();
        else if (materialCode == xchg::code::plastic()          .id())  prog = &gl::get_PlasticMeshMaterial();
        else if (materialCode == xchg::code::thinDielectric()   .id())  prog = &gl::get_ThinDielectricMeshMaterial();
        else if (materialCode == xchg::code::thinGlass()        .id())  prog = &gl::get_ThinDielectricMeshMaterial();
        else if (materialCode == xchg::code::translucent()      .id())  prog = &gl::get_TranslucentMeshMaterial();
        else if (materialCode == xchg::code::translucentSS()    .id())  prog = &gl::get_TranslucentSSMeshMaterial();
        else if (materialCode == xchg::code::velvet()           .id())  prog = &gl::get_VelvetMeshMaterial();
        else                                                            prog = &gl::get_MatteMeshMaterial();
    }
    ////////// CURVES PROGRAM MATERIAL SELECTION //////////////////////////
    else if (geometryClass & xchg::CURVES)
        prog = &gl::get_BasicHairCurvesMaterial();
    ////////// POINTS PROGRAM MATERIAL SELECTION //////////////////////////
    else if (geometryClass & xchg::POINTS)
        prog = &gl::get_BasicPointsMaterial();

    if (prog == nullptr)
        throw sys::Exception("Failed to select shading program.", "Geometry Display Helper Program Selection");

    FLIRT_LOG(LOG(DEBUG)
        << "material code '" << providerCodes[PROVIDER_MATERIAL] << "' + "
        << "subsurface code '" << providerCodes[PROVIDER_SUBSURFACE] << "'"
        << "\t-> select '" << prog->alias() << "' shading program.";)

    return prog;
}

void
view::GeometryDisplayHelper::handleProviderInputAppears(unsigned int        providerId,
                                                        unsigned int        uId,
                                                        gl::ProgramInput*   created)
{
    if (program() == nullptr)
        return;

    const xchg::IdSet& progInputs = program()->getInputs();

    if (progInputs.find(uId) == progInputs.end())
        return;
    addInputToStateSet(created);
}

void
view::GeometryDisplayHelper::handleProviderInputDisappears(unsigned int         providerId,
                                                           unsigned int         uId,
                                                           gl::ProgramInput*    destroyed)
{
    // uniform is not relevant to current program, remove it from the stateset
    if (destroyed)
        removeInputFromStateSet(destroyed);

    if (program())
    {
        const xchg::IdSet& progInputs = program()->getInputs();

        if (progInputs.find(uId) != progInputs.end())
        {
            //-------------------------------------------------
            // uniform is relevant to current program,
            // we should look for it in other providers
            // and fallback to default value if necessary.
            gl::ProgramInput::Ptr input = nullptr;

            for (size_t i = 0; i < NUM_PROVIDERS; ++i)
            {
                StateAttributeProvider::Ptr provider = nullptr;
                if (!_providers[i].lock(provider) ||
                    provider->scnId() == providerId)
                    continue;

                input = provider->getInput(program()->id(), uId);
                if (input)
                    break;  // take the first replacement possible
            }
            if (!input) // fallback to default
                input = gl::defaultInputs().getOrCreateInput(program()->progTemplate(), uId);
            assert(input);
            addInputToStateSet(input.get());
            //-------------------------------------------------
            return;
        }
    }
}

void
view::GeometryDisplayHelper::addInputToStateSet(gl::ProgramInput* input)
{
    if (!input)
        return;
    osg::ref_ptr<osg::StateSet> stateSet = nullptr;
    if (!_stateSet.lock(stateSet))
        return;

    gl::UniformInput*   uniformInput = input->asUniformInput();
    gl::Sampler2dInput* samplerInput = input->asSampler2dInput();

    if (uniformInput &&
        uniformInput->data())
    {
        stateSet->addUniform(uniformInput->data());
        FLIRT_LOG(LOG(DEBUG)
            << "added uniform '" << uniformInput->data()->getName() << "' "
            << "(ptr = " << uniformInput->data() << ") "
            << "to geometry ID = " << _scnId;)
    }
    else if (samplerInput)
    {
        if (samplerInput->glTransformUV())
            stateSet->addUniform(samplerInput->glTransformUV());
        if (samplerInput->transformUV())
            stateSet->addUniform(samplerInput->transformUV());
        if (samplerInput->scale())
            stateSet->addUniform(samplerInput->scale());
        if (samplerInput->valid())
            stateSet->addUniform(samplerInput->valid());
        if (samplerInput->sampler2d())
        {
            int unit = -1;

            stateSet->addUniform(samplerInput->sampler2d());
            if (samplerInput->sampler2d()->get(unit) &&
                samplerInput->texture2d())
                stateSet->setTextureAttributeAndModes(unit, samplerInput->texture2d(), osg::StateAttribute::ON);
            FLIRT_LOG(LOG(DEBUG)
                << "added texture '" << samplerInput->sampler2d()->getName() << "' "
                << "(sampler uniform ptr = " << samplerInput->sampler2d()
                << ", texture unit = " << unit << ") "
                << "to geometry ID = " << _scnId;)
        }
    }
}

void
view::GeometryDisplayHelper::removeInputFromStateSet(gl::ProgramInput* input)
{
    if (!input)
        return;
    osg::ref_ptr<osg::StateSet> stateSet = nullptr;
    if (!_stateSet.lock(stateSet))
        return;

    gl::UniformInput*   uniformInput = input->asUniformInput();
    gl::Sampler2dInput* samplerInput = input->asSampler2dInput();

    if (uniformInput &&
        uniformInput->data())
    {
        stateSet->removeUniform(uniformInput->data());
        FLIRT_LOG(LOG(DEBUG)
            << "removed uniform '" << uniformInput->data()->getName() << "' "
            << "from geometry ID = " << _scnId;)
    }
    else if (samplerInput)
    {
        if (samplerInput->glTransformUV())
            stateSet->removeUniform(samplerInput->glTransformUV());
        if (samplerInput->transformUV())
            stateSet->removeUniform(samplerInput->transformUV());
        if (samplerInput->scale())
            stateSet->removeUniform(samplerInput->scale());
        if (samplerInput->valid())
            stateSet->removeUniform(samplerInput->valid());
        if (samplerInput->sampler2d())
        {
            int unit = -1;

            stateSet->removeUniform(samplerInput->sampler2d());
            if (samplerInput->sampler2d()->get(unit) &&
                samplerInput->texture2d())
                stateSet->removeAssociatedTextureModes(unit, samplerInput->texture2d());
            FLIRT_LOG(LOG(DEBUG)
                << "removed texture '" << samplerInput->sampler2d()->getName() << "' "
                << "(sampler uniform ptr = " << samplerInput->sampler2d()
                << ", texture unit = " << unit << ") "
                << "to geometry ID = " << _scnId;)
        }
    }
}
