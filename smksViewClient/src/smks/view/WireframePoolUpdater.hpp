// ========================================================================= //
// Copyright 2016-2018 SUPAMONKS_STUDIO                                      //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
//                                                                           //
// ========================================================================= //
#pragma once

#include <memory>
#include <boost/unordered_map.hpp>

#include <osg/Camera>

#include <smks/xchg/ParmEvent.hpp>
#include <smks/scn/AbstractParmObserver.hpp>

#include "WireframeGeometry.hpp"

namespace smks
{
    class ClientBase;
    namespace view
    {
        class WireframePoolUpdater:
            public scn::AbstractParmObserver
        {
        public:
            typedef std::shared_ptr<WireframePoolUpdater> Ptr;
        private:
            typedef std::weak_ptr<ClientBase>                                   ClientWPtr;
            typedef osg::ref_ptr<WireframeGeometry>                             WireframeGeometryPtr;
            typedef boost::unordered_map<unsigned int, WireframeGeometryPtr>    WireframeGeometryMap;

        private:
            const ClientWPtr                    _client;
            const osg::Camera::BufferComponent  _objectIdAttachment;

            WireframeGeometryMap                _wireframeMap;
            osg::Vec4f                          _color;

        public:
            static
            Ptr
            create(ClientWPtr const&,
                   osg::Camera::BufferComponent);

            void
            dispose();

            void
            setColor(const osg::Vec4f&);

        private:
            WireframePoolUpdater(ClientWPtr const&,
                                 osg::Camera::BufferComponent);

            virtual
            void
            handle(scn::TreeNode&, const xchg::ParmEvent&);

            void
            addWireframeGeometry(unsigned int);

            void
            updateWireframeGeometry(unsigned int);

            void
            removeWireframeGeometry(unsigned int);

            xchg::Data::Ptr
            getMeshWireIndices(unsigned int) const;

            osg::Geode*
            getGeode(unsigned int);
        };
    }
}
