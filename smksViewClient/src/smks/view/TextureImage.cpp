// ========================================================================= //
// Copyright 2016-2018 SUPAMONKS_STUDIO                                      //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
//                                                                           //
// ========================================================================= //
#include <osg/Texture2D>
#include <smks/sys/Log.hpp>
#include <smks/math/math.hpp>
#include <smks/img/UDIM.hpp>
#include <smks/parm/BuiltIns.hpp>
#include <smks/xchg/Image.hpp>
#include <smks/xchg/TileImages.hpp>
#include <smks/ClientBase.hpp>
#include <smks/view/gl/texture.hpp>
#include <smks/view/gl/uniform.hpp>
#include <smks/view/gl/ProgramInput.hpp>
#include <smks/view/Client.hpp>

#include "TextureImage.hpp"

namespace smks { namespace view
{
    static inline
    void
    getGLDataSize(size_t    width,
                  size_t    height,
                  size_t&   newWidth,
                  size_t&   newHeight,
                  size_t    max);
}
}

using namespace smks;

// static
view::TextureImage::Ptr
view::TextureImage::create(unsigned int id, ClientBase::WPtr const& client)
{
    Ptr ptr (new TextureImage(id, client));
    return ptr;
}

view::TextureImage::TextureImage(unsigned int id, ClientBase::WPtr const& cl):
    osg::Image      (),
    //---------------
    _client         (cl),
    _scnImage       (),
    _scnTimeImages  (),
    //---------------
    _glData         (nullptr),
    _ownsGLData     (false),
    _glTransformUV  (math::Vector4f(1.0f, 1.0f, 0.0f, 0.0f)),
    _transformUV    (math::Vector4f(1.0f, 1.0f, 0.0f, 0.0f)),
    _scale          (math::Vector4f::Ones()),
    _samplers       ()
{
    ClientBase::Ptr const& client = _client.lock();
    if (!client)
        throw sys::Exception(
            "Invalid client.",
            "TextureImage View Binding Construction");

    setName(client->getNodeName(id));
    invalidate();
}

view::TextureImage::~TextureImage()
{
    dispose();
}

void
view::TextureImage::dispose()
{
    invalidate();

    _samplers       .clear();
    _scnImage       .reset();
    _scnTimeImages  .reset();

    disposeGLData();
    setTransformUV  (math::Vector4f(1.0f, 1.0f, 0.0f, 0.0f));
    setScale        (math::Vector4f::Ones());
}

void
view::TextureImage::disposeGLData()
{
    if (_ownsGLData)
        delete _glData;
    _glData         = nullptr;
    _ownsGLData     = false;
    setGLTransformUV(math::Vector4f(1.0f, 1.0f, 0.0f, 0.0f));
}

void
view::TextureImage::registerSampler2dInput(gl::Sampler2dInput::Ptr const& added)
{
    if (!added)
        return;
    for (size_t i = 0; i < _samplers.size(); ++i)
    {
        gl::Sampler2dInput::Ptr const& sampler = _samplers[i].lock();
        if (sampler && added.get() == sampler.get())
            return; // already there
    }
    //-------------------------
    _samplers.push_back(added);
    //-------------------------
    // initialize new sampler's content
    if (added->glTransformUV())
        gl::setUniformfv(*added->glTransformUV(), _glTransformUV.data());
    if (added->transformUV())
        gl::setUniformfv(*added->transformUV(), _transformUV.data());
    if (added->scale())
        gl::setUniformfv(*added->scale(), _scale.data());
}

void
view::TextureImage::deregisterSampler2dInput(gl::Sampler2dInput::Ptr const& removed)
{
    if (!removed)
        return;
    // reset sampler's content
    if (removed->glTransformUV())
        gl::setUniformfv(*removed->glTransformUV(), math::Vector4f(1.0f, 1.0f, 0.0f, 0.0f).data());
    if (removed->transformUV())
        gl::setUniformfv(*removed->transformUV(), math::Vector4f(1.0f, 1.0f, 0.0f, 0.0f).data());
    if (removed->scale())
        gl::setUniformfv(*removed->scale(), math::Vector4f::Ones().eval().data());

    for (size_t i = 0; i < _samplers.size(); ++i)
    {
        gl::Sampler2dInput::Ptr const& sampler = _samplers[i].lock();
        if (sampler && removed.get() == sampler.get())
        {
            //-------------------------
            std::swap(_samplers[i], _samplers.back());
            _samplers.pop_back();
            //-------------------------
        }
    }
}

void
view::TextureImage::setImage(xchg::Image::Ptr const& value)
{
    _scnImage = value;
    _scnTimeImages.reset();
    invalidate();
}

void
view::TextureImage::setTileImages(xchg::TileImages::Ptr const& value)
{
    _scnImage.reset();
    _scnTimeImages = value;
    invalidate();
}

void
view::TextureImage::setGLTransformUV(const math::Vector4f& value)
{
    _glTransformUV = value;
    //---
    for (size_t i = 0; i < _samplers.size(); ++i)
    {
        gl::Sampler2dInput::Ptr const& sampler = _samplers[i].lock();
        if (sampler && sampler->glTransformUV())
            gl::setUniformfv(*sampler->glTransformUV(), value.data());
    }
}

void
view::TextureImage::setTransformUV(const math::Vector4f& value)
{
    _transformUV = value;
    //---
    for (size_t i = 0; i < _samplers.size(); ++i)
    {
        gl::Sampler2dInput::Ptr const& sampler = _samplers[i].lock();
        if (sampler && sampler->transformUV())
            gl::setUniformfv(*sampler->transformUV(), value.data());
    }
}

void
view::TextureImage::setScale(const math::Vector4f& value)
{
    _scale = value;
    //---
    for (size_t i = 0; i < _samplers.size(); ++i)
    {
        gl::Sampler2dInput::Ptr const& sampler = _samplers[i].lock();
        if (sampler && sampler->scale())
            gl::setUniformfv(*sampler->scale(), value.data());
    }
}

bool
view::TextureImage::valid() const
{
    return r() > 0 && s() > 0 && t() > 0;
}

void
view::TextureImage::invalidate()
{
    osg::Image::setImage(0, 0, 0, 0, 0, GL_FLOAT, nullptr, osg::Image::NO_DELETE);
    synchronizeSampler2dInputs();
}

void
view::TextureImage::loadGLData()
{
    if (valid() ||
        _samplers.empty())
        return;

    // the OSG image needs computing.
    xchg::Image::Ptr const& scnImage = _scnImage.lock();
    if (scnImage)
        loadGLData(*scnImage);
    else
    {
        xchg::TileImages::Ptr const& scnTileImages = _scnTimeImages.lock();
        if (scnTileImages)
            loadGLData(*scnTileImages);
    }
    synchronizeSampler2dInputs();
}

void
view::TextureImage::loadGLData(xchg::Image& scnImage)
{
    disposeGLData();
    //---
    const img::AbstractImage* image = scnImage.getImage();
    if (image == nullptr || image->width() == 0 || image->height() == 0 || image->data() == nullptr)
        return;

    Client::Ptr const&  client          = std::dynamic_pointer_cast<Client>(_client.lock());
    const unsigned int  glTextureSize   = client
        ? client->config().glTextureSize()
        : Client::Config().glTextureSize();

    size_t  width   = 0;
    size_t  height  = 0;
    getGLDataSize(image->width(),
                  image->height(),
                  width,
                  height,
                  glTextureSize);
    FLIRT_LOG(LOG(DEBUG)
        << "scale texture '" << getName() << "' content: "
        << image->width() << " x " << image->height()
        << "\t-> " << width << " x " << height;)
    if (width == 0 || height == 0)
        return;

    if (width == image->width() && height == image->height())
    {
        _glData     = const_cast<float*>(image->data());
        _ownsGLData = false;
    }
    else
    {
        // rescale the image either because the original file is too large or because its resolution is not a pozer of two.
        const size_t numFloats = (width * height) << 2;

        _glData     = new float[numFloats];
        _ownsGLData = true;
        memset(_glData, 0, sizeof(float) * numFloats);

        const float ratiox = static_cast<float>(image->width()) / static_cast<float>(width);
        const float ratioy = static_cast<float>(image->height())/ static_cast<float>(height);

        float*  optr    = _glData;
        float   fy      = 0.0f;
        for (size_t y = 0; y < height; ++y)
        {
            const int iy = static_cast<int>(math::floor(fy));

            float fx = 0.0f;
            for (size_t x = 0; x < width; ++x)
            {
                const int ix = static_cast<int>(math::floor(fx));

                const float* iptr = image->data() + ((ix + image->width() * iy) << 2);

                memcpy(optr, iptr, sizeof(float) << 2);
                optr    += 4;
                fx      += ratiox;
            }
            fy += ratioy;
        }
    }

    osg::Image::setImage(
        width,
        height,
        1,
        gl::toGL(image->pixelInternalFormat()),
        gl::toGL(image->pixelFormat()),
        GL_FLOAT,
        reinterpret_cast<unsigned char*>(_glData),
        osg::Image::NO_DELETE);
}

void
view::TextureImage::loadGLData(xchg::TileImages& tileImages)
{
    disposeGLData();
    //---
    math::Vector2i  minUDIM = math::Vector2i::Constant(INT_MAX);
    math::Vector2i  maxUDIM = math::Vector2i::Constant(-INT_MAX);
    math::Vector2iv validUDIM;

    validUDIM.reserve(32);
    math::Vector2i udim = math::Vector2i::Zero();
    for (udim[1] = 0; udim[1] < img::MAX_UDIM; ++udim[1])
        for (udim[0] = 0; udim[0] < img::MAX_UDIM; ++udim[0])
            if (tileImages.tileExists(udim))
            {
                minUDIM = minUDIM.cwiseMin(udim);
                maxUDIM = maxUDIM.cwiseMax(udim);
            }
    if ((minUDIM.array() > maxUDIM.array()).any())
    {
        FLIRT_LOG(LOG(DEBUG)
            << "failed to find any valid tile for " << tileImages.formattedFilePath();)
        return;
    }

    Client::Ptr const&  client          = std::dynamic_pointer_cast<Client>(_client.lock());
    const unsigned int  glUdimTileSize  = client
        ? client->config().glUdimTileSize()
        : Client::Config().glUdimTileSize();

    math::Vector2i  numTiles    = maxUDIM - minUDIM + math::Vector2i::Ones();
    assert((numTiles.array() > math::Vector2i::Zero().array()).all());

    size_t  width   = 0;
    size_t  height  = 0;
    getGLDataSize(numTiles.x() * glUdimTileSize,
                  numTiles.y() * glUdimTileSize,
                  width,
                  height,
                  glUdimTileSize);
    FLIRT_LOG(LOG(DEBUG)
        << "create texture atlas '" << getName() << "':"
        << "\t-> " << width << " x " << height;)
    if (width == 0 || height == 0)
        return;

    const size_t numFloats = (width * height) << 2;

    _glData     = new float[numFloats];
    _ownsGLData = true;
    memset(_glData, 0, sizeof(float) * numFloats);

    const size_t    tileWidth   = static_cast<size_t>(math::floor(static_cast<float>(width) / static_cast<float>(numTiles.x())));
    const size_t    tileHeight  = static_cast<size_t>(math::floor(static_cast<float>(height) / static_cast<float>(numTiles.y())));
    assert(tileWidth > 0);
    assert(tileHeight > 0);
    const float     rTileWidth  = 1.0f / static_cast<float>(tileWidth);
    const float     rTileHeight = 1.0f / static_cast<float>(tileHeight);

    size_t offsety = 0;
    for (udim[1] = minUDIM[1]; udim[1] <= maxUDIM[1]; ++udim[1])
    {
        size_t offsetx = 0;
        for (udim[0] = minUDIM[0]; udim[0] <= maxUDIM[0]; ++udim[0])
        {
            if (tileImages.tileExists(udim))
            {
                const img::AbstractImage* tile = tileImages.getImage(udim);
                if (tile && tile->width() > 0 && tile->height() > 0 && tile->data())
                {
                    const float ratiox = static_cast<float>(tile->width())  * rTileWidth;
                    const float ratioy = static_cast<float>(tile->height()) * rTileHeight;

                    float fy = 0.0f;
                    for (size_t y = 0; y < tileHeight; ++y)
                    {
                        const int iy = static_cast<int>(math::floor(fy));

                        float fx = 0.0f;
                        for (size_t x = 0; x < tileWidth; ++x)
                        {
                            const int ix = static_cast<int>(math::floor(fx));

                            const float*    iptr = tile->data() + (( ix             + tile->width() * iy)               << 2);
                            float*          optr = _glData      + (((x + offsetx)   + width         * (y + offsety))    << 2);

                            memcpy(optr, iptr, sizeof(float) << 2);
                            fx += ratiox;
                        }
                        fy += ratioy;
                    }
                }
            }
            offsetx += tileWidth;
        }
        offsety += tileHeight;
    }

    const float scalex = static_cast<float>(tileWidth)  /static_cast<float>(width);
    const float scaley = static_cast<float>(tileHeight) /static_cast<float>(height);
    setGLTransformUV(math::Vector4f(
        scalex,
        scaley,
        static_cast<float>(-minUDIM[0]) * scalex,
        static_cast<float>(-minUDIM[1]) * scaley));

    osg::Image::setImage(
        width,
        height,
        1,
        gl::toGL(img::RGBA_INT_FORMAT),
        gl::toGL(img::RGBA_FORMAT),
        GL_FLOAT,
        reinterpret_cast<unsigned char*>(_glData),
        osg::Image::NO_DELETE);
}

// static inline
void
view::getGLDataSize(size_t  width,
                    size_t  height,
                    size_t& newWidth,
                    size_t& newHeight,
                    size_t  max)
{
    assert(math::isp2(max));
    newWidth = width;
    newHeight = height;
    if (width > height && width > 0)
    {
        newWidth = math::min<size_t>(math::flp2(width), max);
        const float scaling = static_cast<float>(newWidth)/static_cast<float>(width);
        newHeight = math::flp2(static_cast<size_t>(scaling * height));
    }
    else if (height > 0)
    {
        newHeight = math::min<size_t>(math::flp2(height), max);
        const float scaling = static_cast<float>(newHeight)/static_cast<float>(height);
        newWidth = math::flp2(static_cast<size_t>(scaling * width));
    }
}

void
view::TextureImage::synchronizeSampler2dInputs()
{
    const bool ok = valid();
    for (size_t i = 0; i < _samplers.size(); ++i)
    {
        gl::Sampler2dInput::Ptr const& samplerInput = _samplers[i].lock();
        int unit = -1;
        if (!samplerInput)
            continue;
        if (samplerInput->valid())
            gl::setUniformb(*samplerInput->valid(), ok);
        if (samplerInput->texture2d())
            samplerInput->texture2d()->setImage(0, ok ? this : nullptr);
    }
}
