// ========================================================================= //
// Copyright 2016-2018 SUPAMONKS_STUDIO                                      //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
//                                                                           //
// ========================================================================= //
#include <iostream>
#include <smks/ClientBase.hpp>
#include <smks/xchg/IdSet.hpp>
#include <smks/parm/BuiltIn.hpp>
#include <smks/parm/BuiltIns.hpp>
#include "RenderActionGroup.hpp"

namespace smks { namespace view
{
    class RenderActionGroup::ProgressiveCallback:
        public osg::Callback
    {
    private:
        bool _active;
    public:
        inline
        ProgressiveCallback():
            osg::Callback   (),
            _active         (false)
        { }

        inline
        void
        active(bool value)
        {
            _active = value;
        }

        inline
        bool
        run(osg::Object* obj, osg::Object* data)
        {
            if (_active)
            {
                RenderActionGroup* group = dynamic_cast<RenderActionGroup*>(obj);
                if (group)
                    group->renderIteration();
            }
            return traverse(obj, data);
        }
    };
}
}

using namespace smks;

// static
view::RenderActionGroup::Ptr
view::RenderActionGroup::create(unsigned int id, ClientBase::WPtr const& client)
{
    Ptr ptr(new RenderActionGroup(id, client));
    return ptr;
}

view::RenderActionGroup::RenderActionGroup(unsigned int id, ClientBase::WPtr const& cl):
    osg::Group      (),
    _scnId          (id),
    _client         (cl),
    _updateCallback (new ProgressiveCallback())
{
    ClientBase::Ptr const& client = cl.lock();
    if (!client)
        throw sys::Exception("Invalid client.", "Render Action Node Construction");

    setName(client->getNodeName(id));
    addUpdateCallback(_updateCallback.get());
}

view::RenderActionGroup::~RenderActionGroup()
{
    removeUpdateCallback(_updateCallback.get());
}

void
view::RenderActionGroup::isProgressive(bool value)
{
    _updateCallback->active(value);
}

void
view::RenderActionGroup::renderIteration()
{
    ClientBase::Ptr const& client = _client.lock();
    if (!client ||
        _scnId == 0)
        return;

    // get iteration count from render action node
    size_t iteration = 0;

    client->getNodeParameter<size_t>(parm::iteration(), iteration, _scnId);

    // get all nodes involved in the rendering user settings
    unsigned int    frameBuffer     = 0;
    unsigned int    renderer        = 0;
    unsigned int    samplerFactory  = 0;
    unsigned int    denoiser        = 0;
    xchg::IdSet     renderPasses;

    client->getNodeParameter<unsigned int>(parm::frameBufferId(),   frameBuffer,    _scnId);
    client->getNodeParameter<unsigned int>(parm::rendererId(),      renderer,       _scnId);
    if (renderer)
        client->getNodeParameter<unsigned int>(parm::samplerFactoryId(), samplerFactory, renderer);
    if (frameBuffer)
    {
        client->getNodeParameter<unsigned int>  (parm::denoiserId(),    denoiser,       frameBuffer);
        client->getNodeParameter<xchg::IdSet>   (parm::renderPassIds(), renderPasses,   frameBuffer);
    }

    // push parameters that may clash with progressive rendering
    std::string prvFilePath;
    bool        prvAccumulate   = false;
    size_t      prvNumSamples   = 0;

    client->getNodeParameter<std::string>(parm::filePath(), prvFilePath, _scnId);
    if (renderer)
        client->getNodeParameter<bool>(parm::accumulate(), prvAccumulate, renderer);
    if (samplerFactory)
        client->getNodeParameter<size_t>(parm::numSamples(), prvNumSamples, samplerFactory);

    // prepare progressive rendering
    const bool      accumulate  = iteration > 0;
    const size_t    numSamples  = 1;

    if (!prvFilePath.empty())
        client->unsetNodeParameter(parm::filePath().id(), _scnId);
    if (renderer &&
        accumulate != prvAccumulate)
        client->setNodeParameter<bool>(parm::accumulate(), accumulate, renderer);
    if (samplerFactory &&
        numSamples != prvNumSamples)
        client->setNodeParameter<size_t>(parm::numSamples(), numSamples, samplerFactory);

    if (frameBuffer)
    {
        if (denoiser)
            client->unsetNodeParameter(parm::denoiserId().id(), frameBuffer);
        if (!renderPasses.empty())
            client->unsetNodeParameter(parm::renderPassIds().id(), frameBuffer);
    }

    //##############################
    // render and update framebuffer
    // (also cause render action to
    // increment its iteration count)
    client->setNodeParameter<char>(parm::rendering(), xchg::ACTIVE, _scnId);
    //##############################

    // pop saved parameters
    if (frameBuffer)
    {
        if (denoiser)
            client->setNodeParameterId(parm::denoiserId().c_str(), denoiser, frameBuffer);
        if (!renderPasses.empty())
            client->setNodeParameter<xchg::IdSet>(parm::renderPassIds().c_str(), renderPasses, frameBuffer);
    }

    if (samplerFactory &&
        numSamples != prvNumSamples)
        client->setNodeParameter<size_t>(parm::numSamples(), prvNumSamples, samplerFactory);
    if (renderer &&
        accumulate != prvAccumulate)
        client->setNodeParameter<bool>(parm::accumulate(), prvAccumulate, renderer);
    if (!prvFilePath.empty())
        client->setNodeParameter<std::string>(parm::filePath(), prvFilePath, _scnId);
}
