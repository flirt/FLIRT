// ========================================================================= //
// Copyright 2016-2018 SUPAMONKS_STUDIO                                      //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
//                                                                           //
// ========================================================================= //
#pragma once

#include <memory>
#include <osg/Geometry>

#include <smks/xchg/Data.hpp>
#include <smks/xchg/DataStream.hpp>
#include <smks/xchg/BoundingBox.hpp>
#include <smks/xchg/Buffered.hpp>

namespace osg
{
    class Program;
}

namespace smks
{
    class ClientBase;
    namespace xchg
    {
        class Data;
    }
    namespace view
    {
        namespace gl
        {
            class ContextExtensions;
            class UniformInput;
        }
        class GeometryDisplayHelper;

        class PolyMeshGeometry:
            public osg::Geometry
        {
            friend class UpdateCallback;
            friend class WireframeGeometry;
        public:
            typedef osg::ref_ptr<PolyMeshGeometry> Ptr;
        private:
            typedef std::weak_ptr<ClientBase>               ClientWPtr;
            typedef std::shared_ptr<gl::UniformInput>       UniformPtr;
            typedef std::shared_ptr<GeometryDisplayHelper>  DisplayHelperPtr;
        private:
            //////////////////////
            // struct BufferObject
            //////////////////////
            struct BufferObjects
            {
                GLuint  vbo, ebo;
                bool    dirtyVbo, dirtyEbo;
            public:
                inline BufferObjects(): vbo(0), ebo(0), dirtyVbo(true), dirtyEbo(true) { }
                inline operator bool() const { return vbo != 0 && ebo != 0; }
            };
            //////////////////////
            typedef xchg::Buffered<BufferObjects> PerContextBuffers;
            class UpdateCallback;
        private:
            const ClientWPtr            _client;

            DisplayHelperPtr            _displayHelper;
            unsigned int                _currentProgram;    //<! currently assigned program

            xchg::BoundingBox           _currentBounds;
            xchg::Data::WPtr            _currentVertices;
            xchg::DataStream            _currentPositions;
            xchg::DataStream            _currentNormals;
            xchg::DataStream            _currentTexcoords;
            xchg::Data::WPtr            _currentIndices;
            size_t                      _currentCount;

            mutable PerContextBuffers   _bufferObjects;

            UniformPtr                  _uObjectStateFlags;
            UniformPtr                  _uVertexAttributes;

        public:
            static
            Ptr
            create(unsigned int id, ClientWPtr const& client);

        private:
            PolyMeshGeometry(unsigned int id, ClientWPtr const& client);
            // non-copyable
            PolyMeshGeometry(const PolyMeshGeometry&);
            PolyMeshGeometry& operator=(const PolyMeshGeometry&);

        public:
            ~PolyMeshGeometry();

            void
            dispose();

            inline
            const GeometryDisplayHelper*
            displayHelper() const
            {
                return _displayHelper.get();
            }

            inline
            GeometryDisplayHelper*
            displayHelper()
            {
                return _displayHelper.get();
            }

            void
            getPositions(osg::Vec3Array&) const;

            inline virtual
            const osg::Node*
            node() const
            {
                return getNumParents() > 0 ? getParent(0) : nullptr;
            }

            inline virtual
            osg::Node*
            node()
            {
                return getNumParents() > 0 ? getParent(0) : nullptr;
            }

            //------------------------------------
            void
            setBound(const xchg::BoundingBox&);

            void
            setVertices(xchg::Data::Ptr const&);

            void
            setPositions(const xchg::DataStream&);

            void
            setNormals(const xchg::DataStream&);

            void
            setTexcoords(const xchg::DataStream&);

            void
            setIndices(xchg::Data::Ptr const&);

            void
            setSelected(bool);

            void
            setCount(size_t);

            void
            dirtyVertices();
            void
            dirtyIndices();

            virtual
            osg::BoundingBoxf
            computeBoundingBox() const;

            virtual
            void
            drawImplementation(osg::RenderInfo&) const;
        private:
            bool
            isProgramUpToDate() const;
            void
            loadProgram();

            bool
            bindVbo(osg::RenderInfo&, const osg::GLExtensions&) const;
            void
            unbindVbo(osg::RenderInfo&, const osg::GLExtensions&) const;
        };
    }
}
