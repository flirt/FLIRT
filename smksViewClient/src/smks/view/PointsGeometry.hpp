// ========================================================================= //
// Copyright 2016-2018 SUPAMONKS_STUDIO                                      //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
//                                                                           //
// ========================================================================= //
#pragma once

#include <memory>
#include <osg/Geometry>

#include <smks/xchg/Data.hpp>
#include <smks/xchg/DataStream.hpp>
#include <smks/xchg/BoundingBox.hpp>
#include <smks/xchg/Buffered.hpp>

namespace osg
{
    class Program;
}

namespace smks
{
    class ClientBase;
    namespace view
    {
        namespace gl
        {
            class ContextExtensions;
            class UniformInput;
        }
        class GeometryDisplayHelper;

        class PointsGeometry:
            public osg::Geometry
        {
            friend class UpdateCallback;
        public:
            typedef osg::ref_ptr<PointsGeometry> Ptr;
        private:
            typedef std::weak_ptr<ClientBase>               ClientWPtr;
            typedef std::shared_ptr<gl::UniformInput>       UniformPtr;
            typedef std::shared_ptr<GeometryDisplayHelper>  DisplayHelperPtr;
        private:
            //////////////////////
            // struct BufferObject
            //////////////////////
            struct BufferObject
            {
                GLenum  vbo;
                bool    dirty;
            public:
                inline BufferObject(): vbo(0), dirty(true) { }
                inline operator bool() const { return vbo != 0; }
            };
            //////////////////////
            typedef xchg::Buffered<BufferObject> PerContextBuffers;
            class UpdateCallback;
        private:
            const ClientWPtr            _client;

            DisplayHelperPtr            _displayHelper;
            unsigned int                _currentProgram;    //<! currently assigned program

            xchg::BoundingBox           _currentBounds;
            xchg::Data::WPtr            _currentVertices;
            xchg::DataStream            _currentPositions;
            xchg::DataStream            _currentWidths;

            mutable PerContextBuffers   _bufferObjects;

            UniformPtr                  _uScaling;  // automatically controlled by world transform
            UniformPtr                  _uObjectStateFlags;
            UniformPtr                  _uVertexAttributes;

        public:
            static
            Ptr
            create(unsigned int id, ClientWPtr const&);

        private:
            PointsGeometry(unsigned int id, ClientWPtr const&);
            // non-copyable
            PointsGeometry(const PointsGeometry&);
            PointsGeometry& operator=(const PointsGeometry&);

        public:
            ~PointsGeometry();

            void
            dispose();

            inline
            const GeometryDisplayHelper*
            displayHelper() const
            {
                return _displayHelper.get();
            }

            inline
            GeometryDisplayHelper*
            displayHelper()
            {
                return _displayHelper.get();
            }

            void
            setBound(const xchg::BoundingBox&);

            void
            setVertices(xchg::Data::Ptr const&);

            void
            setPositions(const xchg::DataStream&);

            void
            setWidths(const xchg::DataStream&);

            void
            setSelected(bool);

            void
            dirtyVertices();

            virtual
            osg::BoundingBoxf
            computeBoundingBox() const;

            virtual
            void
            drawImplementation(osg::RenderInfo&) const;
        private:
            bool
            isProgramUpToDate() const;
            void
            loadProgram();
        };
    }
}
