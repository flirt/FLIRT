// ========================================================================= //
// Copyright 2016-2018 SUPAMONKS_STUDIO                                      //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
//                                                                           //
// ========================================================================= //
#pragma once

#include <memory>
#include <osg/ref_ptr>
#include <smks/xchg/ParmEvent.hpp>
#include <smks/ClientBindingBase.hpp>

namespace osg
{
    class Node;
    class Projection;
    class Object;
}

namespace smks
{
    class ClientBase;
    namespace scn
    {
        class TreeNode;
        class SceneObject;
        class Drawable;
        class PolyMesh;
        class Curves;
        class Points;
        class Camera;
        class Xform;
        class Light;
        class Shader;
        class Material;
        class SubSurface;
        class Texture;
        class RtNode;
        class FrameBuffer;
        class GraphAction;
        class RenderAction;
    }
    namespace view
    {
        class StateAttributeProvider;
        class TextureImage;

        class Binding:
            public ClientBindingBase
        {
        public:
            typedef std::shared_ptr<Binding> Ptr;
        private:
            typedef std::weak_ptr<ClientBase>       ClientWPtr;
            typedef std::weak_ptr<scn::TreeNode>    TreeNodeWPtr;
            typedef osg::ref_ptr<osg::Object>       ObjectPtr;
        private:
            ObjectPtr _osgObject;
        public:
            static inline
            Ptr
            create(ClientWPtr const&    client,
                   TreeNodeWPtr const&  dataNode,
                   ObjectPtr const&     osgObject)
            {
                Ptr ptr(new Binding(client, dataNode, osgObject));
                ptr->build(ptr);
                return ptr;
            }
        private:
            Binding(ClientWPtr const&, TreeNodeWPtr const&, ObjectPtr const&);
        private:
            // virtual
            void
            build(Ptr const&);

            // parameter initialization
            //-------------------------
            void
            initTreeNodeParms(const scn::TreeNode&, osg::Object&) const;
            void
            initSceneObjectParms(const scn::SceneObject&, osg::Node&) const;
            void
            initDrawableParms(const scn::Drawable&, osg::Node&) const;
            void
            initPolyMeshParms(const scn::PolyMesh&, osg::Node&) const;
            void
            initCurvesParms(const scn::Curves&, osg::Node&) const;
            void
            initPointsParms(const scn::Points&, osg::Node&) const;
            void
            initCameraParms(const scn::Camera&, osg::Node&) const;
            void
            initXformParms(const scn::Xform&, osg::Node&) const;
            void
            initLightParms(const scn::Light&, osg::Node&) const;
            void
            initShaderParms(const scn::Shader&, osg::Object&) const;
            void
            initMaterialParms(const scn::Material&, StateAttributeProvider&) const;
            void
            initSubSurfaceParms(const scn::SubSurface&, StateAttributeProvider&) const;
            void
            initTextureParms(const scn::Texture&, TextureImage&) const;
            void
            initRtNodeParms(const scn::RtNode&, osg::Node&) const;
            void
            initFrameBufferParms(const scn::FrameBuffer&, osg::Node&) const;
            void
            initGraphActionParms(const scn::GraphAction&, osg::Node&) const;
            void
            initRenderActionParms(const scn::RenderAction&, osg::Node&) const;

            // virtual
            void
            handle(const xchg::ParmEvent&);

            // parameter update
            //-----------------
            bool
            handleTreeNodeParmsChanged(const scn::TreeNode&, unsigned int, xchg::ParmEvent::Type, osg::Object&) const;
            bool
            handleSceneObjectParmsChanged(const scn::SceneObject&, unsigned int, xchg::ParmEvent::Type, osg::Node&) const;
            bool
            handleDrawableParmsChanged(const scn::Drawable&, unsigned int, xchg::ParmEvent::Type, osg::Node&) const;
            bool
            handlePolyMeshParmsChanged(const scn::PolyMesh&, unsigned int, xchg::ParmEvent::Type, osg::Node&) const;
            bool
            handleCurvesParmsChanged(const scn::Curves&, unsigned int, xchg::ParmEvent::Type, osg::Node&) const;
            bool
            handlePointsParmsChanged(const scn::Points&, unsigned int, xchg::ParmEvent::Type, osg::Node&) const;
            bool
            handleCameraParmsChanged(const scn::Camera&, unsigned int, xchg::ParmEvent::Type, osg::Node&) const;
            bool
            handleXformParmsChanged(const scn::Xform&, unsigned int, xchg::ParmEvent::Type, osg::Node&) const;
            bool
            handleLightParmsChanged(const scn::Light&, unsigned int, xchg::ParmEvent::Type, osg::Node&) const;
            bool
            handleShaderParmsChanged(const scn::Shader&, unsigned int, xchg::ParmEvent::Type, osg::Object&) const;
            void
            handleMaterialParmsChanged(const scn::Material&, unsigned int, xchg::ParmEvent::Type, StateAttributeProvider&) const;
            void
            handleSubSurfaceParmsChanged(const scn::SubSurface&, unsigned int, xchg::ParmEvent::Type, StateAttributeProvider&) const;
            bool
            handleTextureParmsChanged(const scn::Texture&, unsigned int, xchg::ParmEvent::Type, TextureImage&) const;
            bool
            handleRtNodeParmsChanged(const scn::RtNode&, unsigned int, xchg::ParmEvent::Type, osg::Node&) const;
            bool
            handleFrameBufferParmsChanged(const scn::FrameBuffer&, unsigned int, xchg::ParmEvent::Type, osg::Node&) const;
            bool
            handleGraphActionParmsChanged(const scn::GraphAction&, unsigned int, xchg::ParmEvent::Type, osg::Node&) const;
            bool
            handleRenderActionParmsChanged(const scn::RenderAction&, unsigned int, xchg::ParmEvent::Type, osg::Node&) const;
        public:
            inline // virtual
            const osg::Object*
            asOsgObject() const
            {
                return _osgObject.get();
            }

            inline // virtual
            osg::Object*
            asOsgObject()
            {
                return _osgObject.get();
            }
        };
    }
}
