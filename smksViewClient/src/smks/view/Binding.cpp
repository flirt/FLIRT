// ========================================================================= //
// Copyright 2016-2018 SUPAMONKS_STUDIO                                      //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
//                                                                           //
// ========================================================================= //
#include <iostream>

#include <osg/Uniform>
#include <osg/Geode>
#include <osg/Projection>

#include <smks/sys/Log.hpp>
#include <smks/math/types.hpp>
#include <smks/xchg/ObjectPtr.hpp>
#include <smks/xchg/Image.hpp>
#include <smks/xchg/TileImages.hpp>
#include <smks/img/AbstractImage.hpp>
#include <smks/util/osg/NodeMask.hpp>
#include <smks/parm/BuiltIns.hpp>

#include <smks/scn/TreeNode.hpp>
#include <smks/scn/PolyMesh.hpp>
#include <smks/scn/Points.hpp>
#include <smks/scn/Curves.hpp>
#include <smks/scn/Camera.hpp>
#include <smks/scn/Xform.hpp>
#include <smks/scn/Light.hpp>
#include <smks/scn/Material.hpp>
#include <smks/scn/SubSurface.hpp>
#include <smks/scn/Texture.hpp>
#include <smks/scn/FrameBuffer.hpp>
#include <smks/scn/RenderAction.hpp>

#include <smks/view/gl/uniform.hpp>

#include "smks/view/Client.hpp"
#include "Binding.hpp"
#include "Geode.hpp"
#include "PolyMeshGeometry.hpp"
#include "CurvesGeometry.hpp"
#include "PointsGeometry.hpp"
#include "GeometryDisplayHelper.hpp"
#include "XTransform.hpp"
#include "LightLocator.hpp"
#include "CameraLocator.hpp"
#include "TextureImage.hpp"
#include "FrameBufferQuad.hpp"
#include "StateAttributeProvider.hpp"
#include "RenderActionGroup.hpp"

namespace smks { namespace view
{
    template<class GeomType> static inline
    bool
    setBoundingBox(osg::Node&, const xchg::BoundingBox&);
}
}

using namespace smks;

template<class GeomType> static inline
bool
view::setBoundingBox(osg::Node& osgNode, const xchg::BoundingBox& box)
{
    if (!box.valid())
        return false;

    Geode<GeomType>* geode = dynamic_cast<Geode<GeomType>*>(&osgNode);
    if (!geode)
        return false;

    geode->mainGeometry()->setBound(box);
    geode->dirtyBound();
    return true;
}

////////////////
// class Binding
////////////////
view::Binding::Binding(ClientBase::WPtr const&          client,
                       scn::TreeNode::WPtr const&       dataNode,
                       osg::ref_ptr<osg::Object> const& osgObject):
    ClientBindingBase   (client, dataNode),
    _osgObject  (osgObject)
{ }

// virtual
void
view::Binding::build(Ptr const& ptr)
{
    ClientBindingBase::build(ptr);
    //---
    scn::TreeNode::Ptr const& node = dataNode().lock();

    if (node && _osgObject)
        initTreeNodeParms(*node, *_osgObject.get());
}

///////////////////////////
// parameter initialization
///////////////////////////
void
view::Binding::initTreeNodeParms(const scn::TreeNode&   dataNode,
                                 osg::Object&           osgObject) const
{
    if (dataNode.asSceneObject())
    {
        osg::Node* osgNode = dynamic_cast<osg::Node*>(&osgObject);
        assert(osgNode);
        initSceneObjectParms(*dataNode.asSceneObject(), *osgNode);
    }
    else if (dataNode.asShader())
        initShaderParms(*dataNode.asShader(), osgObject);
    else if (dataNode.asRtNode())
    {
        osg::Node* osgNode = dynamic_cast<osg::Node*>(&osgObject);
        assert(osgNode);
        initRtNodeParms(*dataNode.asRtNode(), *osgNode);
    }
    else if (dataNode.asGraphAction())
    {
        osg::Node* osgNode = dynamic_cast<osg::Node*>(&osgObject);
        assert(osgNode);
        initGraphActionParms(*dataNode.asGraphAction(), *osgNode);
    }
}

void
view::Binding::initSceneObjectParms(const scn::SceneObject& dataNode,
                                    osg::Node&              osgNode) const
{
    if (dataNode.asDrawable())
        initDrawableParms(*dataNode.asDrawable(), osgNode);
    else if (dataNode.asCamera())
        initCameraParms(*dataNode.asCamera(), osgNode);
    else if (dataNode.asXform())
        initXformParms(*dataNode.asXform(), osgNode);
    else if (dataNode.asLight())
        initLightParms(*dataNode.asLight(), osgNode);

    char    visible = 0;
    char    initd   = xchg::INACTIVE;

    dataNode.getParameter<char>(parm::currentlyVisible(),   visible);
    dataNode.getParameter<char>(parm::initialization(),     initd);
    osgNode.setNodeMask(visible > 0 && initd == xchg::ACTIVE
        ? util::osg::VISIBLE_SCENE_ENTITY
        : util::osg::INVISIBLE_TO_ALL);
}

void
view::Binding::initDrawableParms(const scn::Drawable&   dataNode,
                                 osg::Node&             osgNode) const
{
    if (dataNode.asPolyMesh())
        initPolyMeshParms(*dataNode.asPolyMesh(), osgNode);
    else if (dataNode.asCurves())
        initCurvesParms(*dataNode.asCurves(), osgNode);
    else if (dataNode.asPoints())
        initPointsParms(*dataNode.asPoints(), osgNode);

    xchg::BoundingBox box = xchg::BoundingBox::UnitBox();
    if (dataNode.getParameter<xchg::BoundingBox>(parm::boundingBox(), box))
    {
        (setBoundingBox<PolyMeshGeometry>(osgNode, box) ||
        setBoundingBox<CurvesGeometry>  (osgNode, box) ||
        setBoundingBox<PointsGeometry>  (osgNode, box));
    }
}

void
view::Binding::initPolyMeshParms(const scn::PolyMesh&   dataNode,
                                 osg::Node&             osgNode) const
{
    PolyMeshGeode* geode = dynamic_cast<PolyMeshGeode*>(&osgNode);
    if (!geode)
        return;

    bool                parm1b      = false;
    size_t              parm1l      = 0;
    xchg::Data::Ptr     parmData    = nullptr;
    xchg::DataStream    parmStrm;

    dataNode.getParameter<xchg::Data::Ptr>  (parm::glVertices(),    parmData);  geode->mainGeometry()->setVertices  (parmData);
    dataNode.getParameter<xchg::DataStream> (parm::glPositions(),   parmStrm);  geode->mainGeometry()->setPositions (parmStrm);
    dataNode.getParameter<xchg::DataStream> (parm::glNormals(),     parmStrm);  geode->mainGeometry()->setNormals   (parmStrm);
    dataNode.getParameter<xchg::DataStream> (parm::glTexcoords(),   parmStrm);  geode->mainGeometry()->setTexcoords (parmStrm);
    dataNode.getParameter<xchg::Data::Ptr>  (parm::glIndices(),     parmData);  geode->mainGeometry()->setIndices   (parmData);
    dataNode.getParameter<size_t>           (parm::glCount(),       parm1l);    geode->mainGeometry()->setCount     (parm1l);
    dataNode.getParameter<bool>             (parm::selected(),      parm1b);    geode->mainGeometry()->setSelected  (parm1b);
}

void
view::Binding::initCurvesParms(const scn::Curves&   dataNode,
                               osg::Node&           osgNode) const
{
    CurvesGeode* geode = dynamic_cast<CurvesGeode*>(&osgNode);
    if (!geode)
        return;

    bool                parm1b      = false;
    size_t              parm1l      = 0;
    xchg::Data::Ptr     parmData    = nullptr;
    xchg::DataStream    parmStrm;

    dataNode.getParameter<xchg::Data::Ptr>  (parm::glVertices(),    parmData);  geode->mainGeometry()->setVertices  (parmData);
    dataNode.getParameter<xchg::DataStream> (parm::glPositions(),   parmStrm);  geode->mainGeometry()->setPositions (parmStrm);
    dataNode.getParameter<xchg::DataStream> (parm::glWidths(),      parmStrm);  geode->mainGeometry()->setWidths    (parmStrm);
    dataNode.getParameter<xchg::DataStream> (parm::glNormals(),     parmStrm);  geode->mainGeometry()->setNormals   (parmStrm);
    dataNode.getParameter<xchg::DataStream> (parm::glTexcoords(),   parmStrm);  geode->mainGeometry()->setTexcoords (parmStrm);
    dataNode.getParameter<xchg::Data::Ptr>  (parm::glIndices(),     parmData);  geode->mainGeometry()->setIndices   (parmData);
    dataNode.getParameter<size_t>           (parm::glCount(),       parm1l);    geode->mainGeometry()->setCount     (parm1l);
    dataNode.getParameter<bool>             (parm::selected(),      parm1b);    geode->mainGeometry()->setSelected  (parm1b);
}

void
view::Binding::initPointsParms(const scn::Points&   dataNode,
                               osg::Node&           osgNode) const
{
    PointsGeode* geode = dynamic_cast<PointsGeode*>(&osgNode);
    if (!geode)
        return;

    bool                parm1b      = false;
    xchg::Data::Ptr     parmData    = nullptr;
    xchg::DataStream    parmStrm;

    dataNode.getParameter<xchg::Data::Ptr>  (parm::vertices(),  parmData);  geode->mainGeometry()->setVertices  (parmData);
    dataNode.getParameter<xchg::DataStream> (parm::positions(), parmStrm);  geode->mainGeometry()->setPositions (parmStrm);
    dataNode.getParameter<xchg::DataStream> (parm::widths(),    parmStrm);  geode->mainGeometry()->setWidths    (parmStrm);
    dataNode.getParameter<bool>             (parm::selected(),  parm1b);    geode->mainGeometry()->setSelected  (parm1b);
}

void
view::Binding::initCameraParms(const scn::Camera&   dataNode,
                               osg::Node&           osgNode) const
{
    CameraLocator* cam = dynamic_cast<CameraLocator*>(&osgNode);
    if (!cam)
        return;

    bool parm1b = false;

    {
        float   yFieldOfViewD;
        float   xAspectRatio;
        float   zNear;
        float   zFar;

        dataNode.getParameter<float>(parm::yFieldOfViewD(), yFieldOfViewD);
        dataNode.getParameter<float>(parm::xAspectRatio(),  xAspectRatio);
        dataNode.getParameter<float>(parm::zNear(),         zNear);
        dataNode.getParameter<float>(parm::zFar(),          zFar);
        cam->setPerspective(yFieldOfViewD, xAspectRatio, zNear, zFar);
    }
    dataNode.getParameter<bool>(parm::selected(), parm1b);  cam->setSelected(parm1b);
}

void
view::Binding::initXformParms(const scn::Xform& dataNode,
                              osg::Node&        osgNode) const
{
    XTransform* trf = dynamic_cast<XTransform*>(&osgNode);
    if (!trf)
        return;

    bool inherits = true;

    dataNode.getParameter<bool>             (parm::inheritsTransforms(),    inherits);
    dataNode.getParameter<math::Affine3f>   (parm::transform(),         trf->accessMatrix());
    trf->inheritsXforms(inherits);
    trf->commitMatrix();
}

void
view::Binding::initLightParms(const scn::Light& dataNode,
                              osg::Node&        osgNode) const
{
    LightLocator* light = dynamic_cast<LightLocator*>(&osgNode);
    if (!light)
        return;

    bool            parm1b  = false;
    float           parm1f  = 0.0f;
    math::Affine3f  parmXfm (math::Affine3f::Identity());
    std::string     parmStr;

    dataNode.getParameter<std::string>      (parm::code(),          parmStr);   light->setCode(parmStr);
    dataNode.getParameter<math::Affine3f>   (parm::transform(),     parmXfm);   light->setTransform(parmXfm);
    dataNode.getParameter<float>            (parm::halfAngleD(),    parm1f);    light->setHalfAngleD(parm1f);
    dataNode.getParameter<float>            (parm::minAngleD(),     parm1f);    light->setMinAngleD(parm1f);
    dataNode.getParameter<float>            (parm::maxAngleD(),     parm1f);    light->setMaxAngleD(parm1f);
    dataNode.getParameter<bool>             (parm::selected(),      parm1b);    light->setSelected(parm1b);
}

void
view::Binding::initRtNodeParms(const scn::RtNode&   dataNode,
                               osg::Node&           osgNode) const
{
    if (dataNode.asFrameBuffer())
        initFrameBufferParms(*dataNode.asFrameBuffer(), osgNode);
}

void
view::Binding::initFrameBufferParms(const scn::FrameBuffer& dataNode,
                                    osg::Node&              osgNode) const
{
    FrameBufferQuad* quad = dynamic_cast<FrameBufferQuad*>(&osgNode);
    if (!quad)
        return;

    size_t          parm1l      = 0;
    xchg::Data::Ptr parmData    = nullptr;

    dataNode.getParameter<size_t>           (parm::width(),     parm1l);    quad->textureWidth(parm1l);
    dataNode.getParameter<size_t>           (parm::height(),    parm1l);    quad->textureHeight(parm1l);
    dataNode.getParameter<xchg::Data::Ptr>  (parm::frameData(), parmData);  quad->textureData(parmData);
    osgNode.setNodeMask(util::osg::INVISIBLE_TO_ALL);
}

void
view::Binding::initShaderParms(const scn::Shader&   dataNode,
                               osg::Object&         osgObject) const
{
    if (dataNode.asTexture())
    {
        TextureImage* texImage = dynamic_cast<TextureImage*>(&osgObject);
        assert(texImage);
        initTextureParms(*dataNode.asTexture(), *texImage);
    }
    else if (dataNode.asMaterial())
    {
        StateAttributeProvider* provider = dynamic_cast<StateAttributeProvider*>(&osgObject);
        assert(provider);
        initMaterialParms(*dataNode.asMaterial(), *provider);
    }
    else if (dataNode.asSubSurface())
    {
        StateAttributeProvider* provider = dynamic_cast<StateAttributeProvider*>(&osgObject);
        assert(provider);
        initSubSurfaceParms(*dataNode.asSubSurface(), *provider);
    }
}

void
view::Binding::initMaterialParms(const scn::Material&   dataNode,
                                 StateAttributeProvider&    provider) const
{ }

void
view::Binding::initSubSurfaceParms(const scn::SubSurface&   dataNode,
                                   StateAttributeProvider&  provider) const
{ }

void
view::Binding::initTextureParms(const scn::Texture& dataNode,
                                TextureImage&       texImage) const
{
    math::Vector4f  parm4f;
    xchg::ObjectPtr parmObj;

    dataNode.getParameter<math::Vector4f>   (parm::transformUV(),   parm4f);    texImage.setTransformUV(parm4f);
    dataNode.getParameter<math::Vector4f>   (parm::scale(),         parm4f);    texImage.setScale(parm4f);
    dataNode.getParameter<xchg::ObjectPtr>  (parm::image(),         parmObj);   texImage.setImage(parmObj.shared_ptr<xchg::Image>());
    dataNode.getParameter<xchg::ObjectPtr>  (parm::tileImages(),    parmObj);   texImage.setTileImages(parmObj.shared_ptr<xchg::TileImages>());
}

void
view::Binding::initGraphActionParms(const scn::GraphAction& dataNode,
                                    osg::Node&              osgNode) const
{
    if (dataNode.asRenderAction())
        initRenderActionParms(*dataNode.asRenderAction(), osgNode);
}

void
view::Binding::initRenderActionParms(const scn::RenderAction&   dataNode,
                                     osg::Node&                 osgNode) const
{
    RenderActionGroup* node = dynamic_cast<RenderActionGroup*>(&osgNode);
    if (!node)
        return;

    bool parm1b = false;

    dataNode.getParameter<bool>(parm::isProgressive(), parm1b); node->isProgressive(parm1b);
}

///////////////////
// parameter update
///////////////////
// virtual
void
view::Binding::handle(const xchg::ParmEvent& evt)
{
    if (!evt.emittedByNode())
        return;

    scn::TreeNode::Ptr const& node = dataNode().lock();

    if (!node || !_osgObject)
        return;

    // process common parameters first
    if (handleTreeNodeParmsChanged(*node, evt.parm(), evt.type(), *_osgObject))
        return;
}

bool
view::Binding::handleTreeNodeParmsChanged(const scn::TreeNode&  dataNode,
                                          unsigned int          parmId,
                                          xchg::ParmEvent::Type typ,
                                          osg::Object&          osgObject) const
{
    if (dataNode.asSceneObject())
    {
        osg::Node* osgNode = dynamic_cast<osg::Node*>(&osgObject);
        assert(osgNode);
        if (handleSceneObjectParmsChanged(*dataNode.asSceneObject(), parmId, typ, *osgNode))
            return true;
    }
    else if (dataNode.asShader())
        return handleShaderParmsChanged(*dataNode.asShader(), parmId, typ, osgObject);
    else if (dataNode.asRtNode())
    {
        osg::Node* osgNode = dynamic_cast<osg::Node*>(&osgObject);
        assert(osgNode);
        if (handleRtNodeParmsChanged(*dataNode.asRtNode(), parmId, typ, *osgNode))
            return true;
    }
    else if (dataNode.asGraphAction())
    {
        osg::Node* osgNode = dynamic_cast<osg::Node*>(&osgObject);
        assert(osgNode);
        if (handleGraphActionParmsChanged(*dataNode.asGraphAction(), parmId, typ, *osgNode))
            return true;
    }
    return false;
}

bool
view::Binding::handleSceneObjectParmsChanged(const scn::SceneObject&    dataNode,
                                             unsigned int               parmId,
                                             xchg::ParmEvent::Type      typ,
                                             osg::Node&                 osgNode) const
{
    if (dataNode.asDrawable() &&
        handleDrawableParmsChanged(*dataNode.asDrawable(), parmId, typ, osgNode))
        return true;
    else if (dataNode.asCamera() &&
        handleCameraParmsChanged(*dataNode.asCamera(), parmId, typ, osgNode))
        return true;
    else if (dataNode.asXform() &&
        handleXformParmsChanged(*dataNode.asXform(), parmId, typ, osgNode))
        return true;
    else if (dataNode.asLight() &&
        handleLightParmsChanged(*dataNode.asLight(), parmId, typ, osgNode))
        return true;
    else
    {
        if (parmId == parm::currentlyVisible()  .id() ||
            parmId == parm::initialization()    .id())
        {
            char    visible = 0;
            char    initd   = xchg::INACTIVE;

            dataNode.getParameter<char>(parm::currentlyVisible(),   visible);
            dataNode.getParameter<char>(parm::initialization(), initd);
            osgNode.setNodeMask(visible > 0 && initd == xchg::ACTIVE
                ? util::osg::VISIBLE_SCENE_ENTITY
                : util::osg::INVISIBLE_TO_ALL);
            return true;
        }
        return false;
    }
}

bool
view::Binding::handleCameraParmsChanged(const scn::Camera&      dataNode,
                                        unsigned int            parmId,
                                        xchg::ParmEvent::Type   typ,
                                        osg::Node&              osgNode) const
{
    CameraLocator* cam = dynamic_cast<CameraLocator*>(&osgNode);
    if (!cam)
        return false;

    bool parm1b = false;

    if (parmId == parm::yFieldOfViewD   ().id() ||
        parmId == parm::xAspectRatio    ().id() ||
        parmId == parm::zNear           ().id() ||
        parmId == parm::zFar            ().id())
    {
        float yFieldOfViewD;
        float xAspectRatio;
        float zNear;
        float zFar;

        dataNode.getParameter<float>(parm::yFieldOfViewD(), yFieldOfViewD);
        dataNode.getParameter<float>(parm::xAspectRatio (), xAspectRatio);
        dataNode.getParameter<float>(parm::zNear        (), zNear);
        dataNode.getParameter<float>(parm::zFar         (), zFar);
        cam->setPerspective(yFieldOfViewD, xAspectRatio, zNear, zFar);
        return true;
    }
    else if (parmId == parm::selected().id())
    {
        dataNode.getParameter<bool>(parm::selected(), parm1b);
        cam->setSelected(parm1b);
        return true;
    }

    return false;
}

bool
view::Binding::handleXformParmsChanged(const scn::Xform&        dataNode,
                                       unsigned int             parmId,
                                       xchg::ParmEvent::Type    typ,
                                       osg::Node&               osgNode) const
{
    XTransform* trf = dynamic_cast<XTransform*>(&osgNode);
    if (!trf)
        return false;

    bool updated = false;

    if (parmId == parm::transform().id())
    {
        dataNode.getParameter<math::Affine3f>(parm::transform(), trf->accessMatrix());
        updated = true;
    }
    else if (parmId == parm::inheritsTransforms().id())
    {
        bool parm1b = false;
        dataNode.getParameter<bool>(parm::inheritsTransforms(), parm1b);
        trf->inheritsXforms(parm1b);
        updated = true;
    }

    if (updated)
        trf->commitMatrix();

    return updated;
}

bool
view::Binding::handleLightParmsChanged(const scn::Light&        dataNode,
                                       unsigned int             parmId,
                                       xchg::ParmEvent::Type    typ,
                                       osg::Node&               osgNode) const
{
    LightLocator* light = dynamic_cast<LightLocator*>(&osgNode);
    if (!light)
        return false;

    bool            parm1b  = false;
    float           parm1f  = 0.0f;
    math::Affine3f  parmXfm (math::Affine3f::Identity());
    std::string     parmStr;

    if (parmId == parm::code().id())
    {
        dataNode.getParameter<std::string>(parm::code(), parmStr);
        light->setCode(parmStr);
        return true;
    }
    else if (parmId == parm::transform().id())
    {
        dataNode.getParameter<math::Affine3f>(parm::transform(), parmXfm);
        light->setTransform(parmXfm);
        return true;
    }
    else if (parmId == parm::halfAngleD().id())
    {
        dataNode.getParameter<float>(parm::halfAngleD(), parm1f);
        light->setHalfAngleD(parm1f);
        return true;
    }
    else if (parmId == parm::minAngleD().id())
    {
        dataNode.getParameter<float>(parm::minAngleD(), parm1f);
        light->setMinAngleD(parm1f);
        return true;
    }
    else if (parmId == parm::maxAngleD().id())
    {
        dataNode.getParameter<float>(parm::maxAngleD(), parm1f);
        light->setMaxAngleD(parm1f);
        return true;
    }
    else if (parmId == parm::selected().id())
    {
        dataNode.getParameter<bool>(parm::selected(), parm1b);
        light->setSelected(parm1b);
        return true;
    }
    return false;
}

bool
view::Binding::handleDrawableParmsChanged(const scn::Drawable&  dataNode,
                                          unsigned int          parmId,
                                          xchg::ParmEvent::Type typ,
                                          osg::Node&            osgNode) const
{
    if (dataNode.asPolyMesh() &&
        handlePolyMeshParmsChanged(*dataNode.asPolyMesh(), parmId, typ, osgNode))
        return true;
    else if (dataNode.asCurves() &&
        handleCurvesParmsChanged(*dataNode.asCurves(), parmId, typ, osgNode))
        return true;
    else if (dataNode.asPoints() &&
        handlePointsParmsChanged(*dataNode.asPoints(), parmId, typ, osgNode))
        return true;
    else
    {
        if (parmId == parm::boundingBox().id())
        {
            xchg::BoundingBox box = xchg::BoundingBox::UnitBox();

            dataNode.getParameter<xchg::BoundingBox>(parm::boundingBox(), box);
            if (setBoundingBox<PolyMeshGeometry>(osgNode, box) ||
                setBoundingBox<CurvesGeometry>  (osgNode, box) ||
                setBoundingBox<PointsGeometry>  (osgNode, box))
                return true;
        }
        else if (parmId == parm::materialId().id() ||
            parmId == parm::subsurfaceId().id())
        {
            GeometryDisplayHelper*  helper   = nullptr;
            StateAttributeProvider* provider = nullptr;

            if (dynamic_cast<PolyMeshGeode*>(&osgNode))
                helper = dynamic_cast<PolyMeshGeode*>(&osgNode)->mainGeometry()->displayHelper();
            else if (dynamic_cast<CurvesGeode*>(&osgNode))
                helper = dynamic_cast<CurvesGeode*>(&osgNode)->mainGeometry()->displayHelper();
            else if (dynamic_cast<PointsGeode*>(&osgNode))
                helper = dynamic_cast<PointsGeode*>(&osgNode)->mainGeometry()->displayHelper();

            unsigned int nodeId = 0;

            if (typ != xchg::ParmEvent::PARM_REMOVED)
            {
                unsigned int            nodeId      = 0;
                ClientBindingBase::Ptr      nodeBind    = nullptr;
                ClientBase::Ptr const&  cl          = client().lock();

                dataNode.getParameter<unsigned int>(parmId, nodeId, 0);
                nodeBind = cl->getBinding(nodeId);
                provider = nodeBind
                    ? dynamic_cast<StateAttributeProvider*>(nodeBind->asOsgObject())
                    : nullptr;
            }

            if (helper)
                helper->setProvider(parmId, provider);
        }
        return false;
    }
}

bool
view::Binding::handlePolyMeshParmsChanged(const scn::PolyMesh&  dataNode,
                                          unsigned int          parmId,
                                          xchg::ParmEvent::Type,
                                          osg::Node&            osgNode) const
{
    PolyMeshGeode*      geode   = dynamic_cast<PolyMeshGeode*>(&osgNode);
    PolyMeshGeometry*   geom    = geode ? geode->mainGeometry() : nullptr;
    if (!geom)
        return false;

    bool                parm1b      = false;
    size_t              parm1l      = 0;
    xchg::Data::Ptr     parmData    = nullptr;
    xchg::DataStream    parmStrm;

    if (parmId == parm::glVertices().id())
    {
        dataNode.getParameter<xchg::Data::Ptr>(parm::glVertices(), parmData);
        geom->setVertices(parmData);
        return true;
    }
    else if (parmId == parm::glPositions().id())
    {
        dataNode.getParameter<xchg::DataStream>(parm::glPositions(), parmStrm);
        geom->setPositions(parmStrm);
        return true;
    }
    else if (parmId == parm::glNormals().id())
    {
        dataNode.getParameter<xchg::DataStream>(parm::glNormals(), parmStrm);
        geom->setNormals(parmStrm);
        return true;
    }
    else if (parmId == parm::glTexcoords().id())
    {
        dataNode.getParameter<xchg::DataStream>(parm::glTexcoords(), parmStrm);
        geom->setTexcoords(parmStrm);
        return true;
    }
    else if (parmId == parm::glIndices().id())
    {
        dataNode.getParameter<xchg::Data::Ptr>(parm::glIndices(), parmData);
        geom->setIndices(parmData);
        return true;
    }
    else if (parmId == parm::glCount().id())
    {
        dataNode.getParameter<size_t>(parm::glCount(), parm1l);
        geom->setCount(parm1l);
        return true;
    }
    else if (parmId == parm::selected().id())
    {
        dataNode.getParameter<bool>(parm::selected(), parm1b);
        geom->setSelected(parm1b);
        return true;
    }
    else if (parmId == parm::currentSampleIdx().id())
    {
        // when caching is deactivated, memory is updated while the values of the
        // data/datastream parameters do not change. it is then necessary to dirty
        // vertex/index buffers by tracking changes in the 'currentSampleIdx'
        // parameter.
        geom->dirtyVertices();

        dataNode.getParameter<size_t>(parm::numIndexBufferSamples(), parm1l);
        if (parm1l > 1)
            geom->dirtyIndices();
        return true;
    }

    return false;
}

bool
view::Binding::handleCurvesParmsChanged(const scn::Curves&  dataNode,
                                        unsigned int        parmId,
                                        xchg::ParmEvent::Type,
                                        osg::Node&          osgNode) const
{
    CurvesGeode*    geode   = dynamic_cast<CurvesGeode*>(&osgNode);
    CurvesGeometry* geom    = geode ? geode->mainGeometry() : nullptr;
    if (!geom)
        return false;

    bool                parm1b      = false;
    size_t              parm1l      = 0;
    xchg::Data::Ptr     parmData    = nullptr;
    xchg::DataStream    parmStrm;

    if (parmId == parm::glVertices().id())
    {
        dataNode.getParameter<xchg::Data::Ptr>(parm::glVertices(), parmData);
        geom->setVertices(parmData);
        return true;
    }
    else if (parmId == parm::glPositions().id())
    {
        dataNode.getParameter<xchg::DataStream>(parm::glPositions(), parmStrm);
        geom->setPositions(parmStrm);
        return true;
    }
    else if (parmId == parm::glWidths().id())
    {
        dataNode.getParameter<xchg::DataStream>(parm::glWidths(), parmStrm);
        geom->setWidths(parmStrm);
        return true;
    }
    else if (parmId == parm::glNormals().id())
    {
        dataNode.getParameter<xchg::DataStream>(parm::glNormals(), parmStrm);
        geom->setNormals(parmStrm);
        return true;
    }
    else if (parmId == parm::glTexcoords().id())
    {
        dataNode.getParameter<xchg::DataStream>(parm::glTexcoords(), parmStrm);
        geom->setTexcoords(parmStrm);
        return true;
    }
    else if (parmId == parm::glIndices().id())
    {
        dataNode.getParameter<xchg::Data::Ptr>(parm::glIndices(), parmData);
        geom->setIndices(parmData);
        return true;
    }
    else if (parmId == parm::glCount().id())
    {
        dataNode.getParameter<size_t>(parm::glCount(), parm1l);
        geom->setCount(parm1l);
        return true;
    }
    else if (parmId == parm::selected().id())
    {
        dataNode.getParameter<bool>(parm::selected(), parm1b);
        geom->setSelected(parm1b);
        return true;
    }
    else if (parmId == parm::currentSampleIdx().id())
    {
        // when caching is deactivated, memory is updated while the values of the
        // data/datastream parameters do not change. it is then necessary to dirty
        // vertex/index buffers by tracking changes in the 'currentSampleIdx'
        // parameter.
        geom->dirtyVertices();
        dataNode.getParameter<size_t>(parm::numIndexBufferSamples(), parm1l);
        if (parm1l > 1)
            geom->dirtyIndices();
        return true;
    }

    return false;
}

bool
view::Binding::handlePointsParmsChanged(const scn::Points&  dataNode,
                                        unsigned int        parmId,
                                        xchg::ParmEvent::Type,
                                        osg::Node&          osgNode) const
{
    PointsGeode*    geode   = dynamic_cast<PointsGeode*>(&osgNode);
    PointsGeometry* geom    = geode ? geode->mainGeometry() : nullptr;
    if (!geom)
        return false;

    bool                parm1b      = false;
    xchg::Data::Ptr     parmData    = nullptr;
    xchg::DataStream    parmStrm;

    if (parmId == parm::vertices().id())
    {
        dataNode.getParameter<xchg::Data::Ptr>(parm::vertices(), parmData);
        geom->setVertices(parmData);
        return true;
    }
    else if (parmId == parm::positions().id())
    {
        dataNode.getParameter<xchg::DataStream>(parm::positions(), parmStrm);
        geom->setPositions(parmStrm);
        return true;
    }
    else if (parmId == parm::widths().id())
    {
        dataNode.getParameter<xchg::DataStream>(parm::widths(), parmStrm);
        geom->setWidths(parmStrm);
        return true;
    }
    else if (parmId == parm::selected().id())
    {
        dataNode.getParameter<bool>(parm::selected(), parm1b);
        geom->setSelected(parm1b);
        return true;
    }
    else if (parmId == parm::currentSampleIdx().id())
    {
        // when caching is deactivated, memory is updated while the values of the
        // data/datastream parameters do not change. it is then necessary to dirty
        // vertex/index buffers by tracking changes in the 'currentSampleIdx'
        // parameter.
        geom->dirtyVertices();
        return true;
    }

    return false;
}

bool
view::Binding::handleRtNodeParmsChanged(const scn::RtNode&      dataNode,
                                        unsigned int            parmId,
                                        xchg::ParmEvent::Type   typ,
                                        osg::Node&              osgNode) const
{
    if (dataNode.asFrameBuffer() &&
        handleFrameBufferParmsChanged(*dataNode.asFrameBuffer(), parmId, typ, osgNode))
        return true;
    return false;
}

bool
view::Binding::handleFrameBufferParmsChanged(const scn::FrameBuffer&    dataNode,
                                             unsigned int               parmId,
                                             xchg::ParmEvent::Type      typ,
                                             osg::Node&                 osgNode) const
{
    FrameBufferQuad* quad = dynamic_cast<FrameBufferQuad*>(&osgNode);
    if (!quad)
        return false;

    size_t              parm1l      = 0;
    xchg::Data::Ptr     parmData    = nullptr;

    if (parmId == parm::width().id())
    {
        dataNode.getParameter<size_t>(parm::width(), parm1l);
        quad->textureWidth(parm1l);
        return true;
    }
    else if (parmId == parm::height().id())
    {
        dataNode.getParameter<size_t>(parm::height(), parm1l);
        quad->textureHeight(parm1l);
        return true;
    }
    else if (parmId == parm::frameData().id())
    {
        dataNode.getParameter<xchg::Data::Ptr>(parm::frameData(), parmData);
        quad->textureData(parmData);
        return true;
    }
    return false;
}

bool
view::Binding::handleShaderParmsChanged(const scn::Shader&      dataNode,
                                        unsigned int            parmId,
                                        xchg::ParmEvent::Type   typ,
                                        osg::Object&            osgObject) const
{
    if (dataNode.asTexture())
    {
        TextureImage* texImage = dynamic_cast<TextureImage*>(&osgObject);
        assert(texImage);

        if (handleTextureParmsChanged(*dataNode.asTexture(), parmId, typ, *texImage))
            return true;
    }
    else if (dataNode.asMaterial() ||
        dataNode.asSubSurface())
    {
        StateAttributeProvider* provider = dynamic_cast<StateAttributeProvider*>(&osgObject);
        assert(provider);

        if (provider->handleParmChanged(parmId, typ))
            return true;
        if (parmId == parm::code().id())
        {
            provider->handleCodeChanged();
            return true;
        }
    }
    return false;
}

bool
view::Binding::handleTextureParmsChanged(const scn::Texture&    dataNode,
                                         unsigned int           parmId,
                                         xchg::ParmEvent::Type  typ,
                                         TextureImage&          texImage) const
{
    math::Vector4f  parm4f;
    xchg::ObjectPtr parmObj;

    if (parmId == parm::transformUV().id())
    {
        dataNode.getParameter<math::Vector4f>(parm::transformUV(), parm4f);
        texImage.setTransformUV(parm4f);
        return true;
    }
    else if (parmId == parm::scale().id())
    {
        dataNode.getParameter<math::Vector4f>(parm::scale(), parm4f);
        texImage.setScale(parm4f);
        return true;
    }
    else if (parmId == parm::image().id())
    {
        dataNode.getParameter<xchg::ObjectPtr>(parm::image(), parmObj);
        texImage.setImage(parmObj.shared_ptr<xchg::Image>());
        return true;
    }
    else if (parmId == parm::tileImages().id())
    {
        dataNode.getParameter<xchg::ObjectPtr>(parm::tileImages(), parmObj);
        texImage.setTileImages(parmObj.shared_ptr<xchg::TileImages>());
        return true;
    }
    return false;
}

bool
view::Binding::handleGraphActionParmsChanged(const scn::GraphAction&    dataNode,
                                             unsigned int               parmId,
                                             xchg::ParmEvent::Type      typ,
                                             osg::Node&                 osgNode) const
{
    if (dataNode.asRenderAction())
    {
        RenderActionGroup* node = dynamic_cast<RenderActionGroup*>(&osgNode);
        assert(node);

        if (handleRenderActionParmsChanged(*dataNode.asRenderAction(), parmId, typ, osgNode))
            return true;
    }
    return false;
}

bool
view::Binding::handleRenderActionParmsChanged(const scn::RenderAction&  dataNode,
                                              unsigned int              parmId,
                                              xchg::ParmEvent::Type     typ,
                                              osg::Node&                osgNode) const
{
    RenderActionGroup* node = dynamic_cast<RenderActionGroup*>(&osgNode);
    if (!node)
        return false;

    bool parm1b = false;

    if (parmId == parm::isProgressive().id())
    {
        dataNode.getParameter<bool>(parm::isProgressive(), parm1b);
        node->isProgressive(parm1b);
        return true;
    }
    return false;
}
