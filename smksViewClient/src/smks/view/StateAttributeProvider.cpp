// ========================================================================= //
// Copyright 2016-2018 SUPAMONKS_STUDIO                                      //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
//                                                                           //
// ========================================================================= //
#include <osg/Texture2D>

#include <smks/sys/Log.hpp>
#include <smks/xchg/ParmEvent.hpp>
#include <smks/parm/BuiltIn.hpp>
#include <smks/parm/BuiltIns.hpp>
#include <smks/parm/BuiltInMap.hpp>
#include <smks/scn/TreeNode.hpp>
#include <smks/ClientBase.hpp>
#include <smks/ClientBindingBase.hpp>
#include <smks/view/gl/MaterialProgramBase.hpp>
#include <smks/view/gl/MaterialProgramMap.hpp>
#include <smks/view/gl/UniformDeclaration.hpp>
#include <smks/view/gl/ProgramInputDeclarationMap.hpp>
#include <smks/view/gl/ProgramTemplate.hpp>


#include "TextureImage.hpp"
#include "GeometryDisplayHelper.hpp"
#include "StateAttributeProvider.hpp"

using namespace smks;

view::StateAttributeProvider::StateAttributeProvider(unsigned int id, ClientBase::WPtr const& cl):
    osg::Object (true),
    _scnId      (id),
    _client     (cl),
    //---------------
    _helpers    (),
    _progInputs ()
{
    ClientBase::Ptr const& client = _client.lock();
    if (!client)
        throw sys::Exception("Invalid client.", "State Attribute Provider Construction");

    setName(client->getNodeName(id));
}

void
view::StateAttributeProvider::dispose()
{
    // remove all attributes and notify listening helpers
    for (ProgramInputMap::iterator itInputs = _progInputs.begin(); itInputs != _progInputs.end(); ++itInputs)
    {
        const unsigned int progId = itInputs->first;
        while (!itInputs->second.empty())
        {
            const unsigned int inId = itInputs->second.begin()->first;
            destroyInput(progId, inId); // -> notify helpers
        }
    }

    // deregister all helpers
    while (!_helpers.empty())
    {
        GeometryDisplayHelper::Ptr const& helper = _helpers.begin()->second.lock();
        if (helper)
            deregisterDisplayHelper(helper);
        else
            break;
    }
    _progInputs .clear();
    _helpers    .clear();
}

void
view::StateAttributeProvider::getCode(std::string& value) const
{
    value.clear();

    ClientBase::Ptr const& client = _client.lock();

    if (client)
        client->getNodeParameter<std::string>(parm::code(), value, _scnId);
}

void
view::StateAttributeProvider::setTextureImage(unsigned int parmId, TextureImage* texImage)
{
    FLIRT_LOG(LOG(DEBUG)
        << "provider ID = " << _scnId << "\ttexture image <- ptr = " << texImage
        << " (parmID = " << parmId
        << ", name = '" << (texImage ? texImage->getName().c_str() : "") << "').";)

    for (Helpers::iterator it = _helpers.begin(); it != _helpers.end(); ++it)
    {
        GeometryDisplayHelper::Ptr const&   helper  = it->second.lock();
        const gl::MaterialProgramBase*      prog    = helper ? helper->program() : nullptr;
        if (prog == nullptr)
            continue;

        xchg::IdSet inIds;

        prog->getInputsAffectedByParm(parmId, inIds);
        for (xchg::IdSet::const_iterator inId = inIds.begin(); inId != inIds.end(); ++inId)
        {
            gl::Sampler2dInput::Ptr const& samplerInput = std::dynamic_pointer_cast<gl::Sampler2dInput>(getInput(prog->id(), *inId));
            // we should not create new sampler 2d program input here.
            // we only update the referenced texture image for existing ones.
            if (!samplerInput)
                continue;

            // have the previous texture image deregister current sampler input
            TextureImage* prvTexImage = dynamic_cast<TextureImage*>(samplerInput->texture2d()->getImage());
            if (prvTexImage)
                prvTexImage->deregisterSampler2dInput(samplerInput);

            if (texImage)
            {
                if (samplerInput->valid())
                    gl::setUniformb(*samplerInput->valid(), true);
                if (samplerInput->texture2d())
                    samplerInput->texture2d()->setImage(texImage);
                FLIRT_LOG(LOG(DEBUG)
                    << "sampler 2d input ID = " << *inId << " (provider ID = " << _scnId << ") "
                    << "<- valid texture image " << texImage->s() << " x " << texImage->t() << ".";)
            }
            else
            {
                if (samplerInput->valid())
                    gl::setUniformb(*samplerInput->valid(), false);
                if (samplerInput->texture2d())
                    samplerInput->texture2d()->setImage(nullptr);
                FLIRT_LOG(LOG(DEBUG)
                    << "sampler 2d input ID = " << *inId << " (provider ID = " << _scnId << ") "
                    << "<- no texture image.";)
            }

            // have the new texture image register current sampler input
            if (texImage)
                texImage->registerSampler2dInput(samplerInput);
        }
    }
}

view::gl::ProgramInput::Ptr
view::StateAttributeProvider::getInput(unsigned int progId, unsigned int inId) const
{
    ProgramInputMap::const_iterator const& itInputs = _progInputs.find(progId);
    if (itInputs == _progInputs.end())
        return nullptr;

    const ProgramInputs&                    inputs  = itInputs->second;
    ProgramInputs::const_iterator const&    itInput = inputs.find(inId);
    if (itInput == inputs.end())
        return nullptr;
    return itInput->second;
}

void
view::StateAttributeProvider::handleCodeChanged()
{
    for (Helpers::iterator it = _helpers.begin(); it != _helpers.end(); ++it)
    {
        GeometryDisplayHelper::Ptr const& helper = it->second.lock();
        if (helper)
            helper->refreshProgram();
    }
}

bool
view::StateAttributeProvider::handleParmChanged(unsigned int parmId, xchg::ParmEvent::Type typ)
{
    bool handled = false;
    for (Helpers::iterator it = _helpers.begin(); it != _helpers.end(); ++it)
    {
        GeometryDisplayHelper::Ptr const&   helper  = it->second.lock();
        const gl::MaterialProgramBase*      prog    = helper ? helper->program() : nullptr;
        if (prog == nullptr)
            continue;

        xchg::IdSet inIds;

        prog->getInputsAffectedByParm(parmId, inIds);
        for (xchg::IdSet::const_iterator inId = inIds.begin(); inId != inIds.end(); ++inId)
        {
            if (typ == xchg::ParmEvent::Type::PARM_ADDED ||
                typ == xchg::ParmEvent::Type::PARM_CHANGED)
            {
                gl::ProgramInput::Ptr const& input = getOrCreateInput(prog->id(), *inId);   // may notify helpers
                if (input)
                    updateInputFromParm(*input, parmId);
                handled = true;
            }
            else if (typ == xchg::ParmEvent::Type::PARM_REMOVED)
            {
                gl::ProgramInput::Ptr input = getInput(prog->id(), *inId);
                if (!input)
                    continue;

                // before complete deletion, make sure the provider does not
                // have another parameter that could replace the deleted one
                // and update the uniform.
                bool        updated = false;
                xchg::IdSet parmIds;

                prog->getParmsAffectingInput(*inId, parmIds);
                for (xchg::IdSet::const_iterator parmId = parmIds.begin(); parmId != parmIds.end(); ++parmId)
                    updated = updated || updateInputFromParm(*input, *parmId);
                if (!updated)
                    destroyInput(prog->id(), *inId);    // notifies helpers
                handled = true;
            }
        }
    }
    return handled;
}

void
view::StateAttributeProvider::registerDisplayHelper(GeometryDisplayHelper::Ptr const& helper)
{
    if (!helper)
        return;
    if (_helpers.find(helper->scnId()) != _helpers.end())
    {
        std::stringstream sstr;
        sstr
            << "Geometry display helper ID = " << helper->scnId()
            << " already registered by attribute provider ID = " << _scnId << ".";
        throw sys::Exception(sstr.str().c_str(), "Geometry Display Helper Registration");
    }
    _helpers.insert(std::pair<unsigned int, GeometryDisplayHelper::WPtr>(helper->scnId(), helper));
    //#####################
    if (helper->program())
        addProgramInputsToHelper(helper, helper->program()->id());
    //#####################
}

void
view::StateAttributeProvider::deregisterDisplayHelper(GeometryDisplayHelper::Ptr const& helper)
{
    if (!helper)
        return;
    if (_helpers.find(helper->scnId()) == _helpers.end())
    {
        std::stringstream sstr;
        sstr
            << "Geometry display helper ID = " << helper->scnId()
            << " never registered by attribute provider ID = " << _scnId << ".";
        throw sys::Exception(sstr.str().c_str(), "Geometry Display Helper Deregistration");
    }
    _helpers.erase(helper->scnId());
    //#####################
    if (helper->program())
        removeProgramInputsFromHelper(helper, helper->program()->id());
    //#####################
}

void
view::StateAttributeProvider::handleDisplayHelperProgramChanged(unsigned int                    helperId,
                                                                const gl::MaterialProgramBase*  newProgram,
                                                                const gl::MaterialProgramBase*  prvProgram)
{
    Helpers::const_iterator const& itHelper = _helpers.find(helperId);
    if (itHelper == _helpers.end())
        return; // updated display helper is not registered

    GeometryDisplayHelper::Ptr const& helper = itHelper->second.lock();
    if (!helper)
        return;

    if (prvProgram)
        removeProgramInputsFromHelper(helper, prvProgram->id());
    if (newProgram)
        addProgramInputsToHelper(helper, newProgram->id());
}

view::gl::ProgramInput::Ptr
view::StateAttributeProvider::getOrCreateInput(unsigned int progId, unsigned int inId)
{
    view::gl::ProgramInput::Ptr input = getInput(progId, inId);
    if (input)
        return input;

    gl::MaterialProgramBase::Ptr const&     prog = gl::materials().find(progId);
    gl::ProgramInputDeclaration::Ptr const& decl = gl::programInputDeclarations().find(inId);
    if (!prog || !decl)
        return nullptr;
    //---------------------------------------------------
    input = prog->progTemplate().createInput(*decl);    // create uniform
    if (!input)
        return nullptr;
    _progInputs[progId][inId] = input;
    //---------------------------------------------------
    // notify helpers
    for (Helpers::const_iterator it = _helpers.begin(); it != _helpers.end(); ++it)
    {
        GeometryDisplayHelper::Ptr const&   helper  = it->second.lock();
        const gl::MaterialProgramBase*      prog    = helper ? helper->program() : nullptr;

        if (prog && prog->id() == progId)
            helper->handleProviderInputAppears(_scnId, inId, input.get());
    }

    return input;
}

void
view::StateAttributeProvider::destroyInput(unsigned int progId, unsigned int inId)
{
    ProgramInputMap::iterator const& itInputs = _progInputs.find(progId);
    if (itInputs == _progInputs.end())
        return;

    ProgramInputs&                  inputs  = itInputs->second;
    ProgramInputs::iterator const&  itInput = inputs.find(inId);
    if (itInput == inputs.end())
        return;

    gl::ProgramInput::Ptr input = itInput->second;
    //------------------------------------
    inputs.erase(itInput); // remove input
    //------------------------------------
    // notify helpers
    for (Helpers::const_iterator it = _helpers.begin(); it != _helpers.end(); ++it)
    {
        GeometryDisplayHelper::Ptr const&   helper  = it->second.lock();
        const gl::MaterialProgramBase*      prog    = helper ? helper->program() : nullptr;

        if (prog && prog->id() == progId)
            helper->handleProviderInputDisappears(_scnId, inId, input.get());
    }
}

bool
view::StateAttributeProvider::updateInputFromParm(gl::ProgramInput& input,
                                                  unsigned int      parmId)
{
    ClientBase::Ptr const& client = _client.lock();
    if (!client ||
        !client->nodeParameterExists(parmId, _scnId))
        return false;

    gl::UniformInput*   uniformInput = input.asUniformInput();
    gl::Sampler2dInput* samplerInput = input.asSampler2dInput();

    if (uniformInput &&
        uniformInput->data())
    {
        bool            parm1b = false;
        char            parm1c = 0;
        int             parm1i = 0;
        unsigned int    parm1ui = 0;
        size_t          parm1ul = 0;
        math::Vector2i  parm2i = math::Vector2i::Zero();
        math::Vector3i  parm3i = math::Vector3i::Zero();
        math::Vector4i  parm4i = math::Vector4i::Zero();
        float           parm1f = 0.0f;
        math::Vector2f  parm2f = math::Vector2f::Zero();
        math::Vector4f  parm4f = math::Vector4f::Zero();
        math::Affine3f  parm4x4f = math::Affine3f::Identity();

        if (client->getNodeParameter<math::Vector4f>(parmId, parm4f, math::Vector4f::Zero(), _scnId))
            gl::setUniformfv(*uniformInput->data(), parm4f.data());
        else if (client->getNodeParameter<float>(parmId, parm1f, 0.0f, _scnId))
            gl::setUniformf(*uniformInput->data(), parm1f);
        else if (client->getNodeParameter<math::Affine3f>(parmId, parm4x4f, math::Affine3f::Identity(), _scnId))
            gl::setUniformfv(*uniformInput->data(), parm4x4f.data());
        else if (client->getNodeParameter<math::Vector2f>(parmId, parm2f, math::Vector2f::Zero(), _scnId))
            gl::setUniformfv(*uniformInput->data(), parm2f.data());
        else if (client->getNodeParameter<int>(parmId, parm1i, 0, _scnId))
            gl::setUniformi(*uniformInput->data(), parm1i);
        else if (client->getNodeParameter<unsigned int>(parmId, parm1ui, 0, _scnId))
            gl::setUniformui(*uniformInput->data(), parm1ui);
        else if (client->getNodeParameter<bool>(parmId, parm1b, false, _scnId))
            gl::setUniformb(*uniformInput->data(), parm1b);
        else if (client->getNodeParameter<math::Vector2i>(parmId, parm2i, math::Vector2i::Zero(), _scnId))
            gl::setUniformiv(*uniformInput->data(), parm2i.data());
        else if (client->getNodeParameter<math::Vector3i>(parmId, parm3i, math::Vector3i::Zero(), _scnId))
            gl::setUniformiv(*uniformInput->data(), parm3i.data());
        else if (client->getNodeParameter<math::Vector4i>(parmId, parm4i, math::Vector4i::Zero(), _scnId))
            gl::setUniformiv(*uniformInput->data(), parm4i.data());
        else if (client->getNodeParameter<size_t>(parmId, parm1ul, 0, _scnId))
            gl::setUniformui(*uniformInput->data(), parm1ul);
        else if (client->getNodeParameter<char>(parmId, parm1c, 0, _scnId))
            gl::setUniformi(*uniformInput->data(), parm1c);
        else
            return false;
        return true;
    }
    else if (samplerInput)
    {
        // SAMPLER 2D INPUT
        TextureImage*   texImage    = nullptr;
        unsigned int    texId       = 0;
        if (client->getNodeParameter<unsigned int>(parmId, texId, 0, _scnId))
        {
            ClientBindingBase::Ptr texBind = client->getBinding(texId);

            texImage = texBind
                ? reinterpret_cast<TextureImage*>(texBind->asOsgObject())
                : nullptr;
        }
        setTextureImage(parmId, texImage);
        return true;
    }
    else
        return false;
}

void
view::StateAttributeProvider::addProgramInputsToHelper(GeometryDisplayHelper::Ptr const&    helper,
                                                       unsigned int                         progId)
{
    if (!helper)
        return;

    ProgramInputMap::const_iterator itInputs = _progInputs.find(progId);
    if (itInputs == _progInputs.end())
    {
        ClientBase::Ptr const& client = _client.lock();
        if (!client)
            return;

        // initialize program inputs with current parameters
        gl::MaterialProgramBase::Ptr const& prog = gl::materials().find(progId);
        if (prog)
        {
            const xchg::IdSet& progInputs = prog->getInputs();
            for (xchg::IdSet::const_iterator inId = progInputs.begin(); inId != progInputs.end(); ++inId)
            {
                xchg::IdSet     parms;
                unsigned int    parmId = 0;

                prog->getParmsAffectingInput(*inId, parms);
                for (xchg::IdSet::const_iterator pId = parms.begin(); pId != parms.end(); ++pId)
                    if (client->nodeParameterExists(*pId, _scnId))
                        parmId = *pId;
                if (parmId > 0)
                {
                    gl::ProgramInput::Ptr const& input = getOrCreateInput(prog->id(), *inId);   // may notify helpers
                    if (input)
                        updateInputFromParm(*input, parmId);
                }
            }
        }
    }

    itInputs = _progInputs.find(progId);
    if (itInputs == _progInputs.end())
        return; // no uniform has been created given the current parameter set

    const ProgramInputs& inputs = itInputs->second;
    for (ProgramInputs::const_iterator itInput = inputs.begin(); itInput != inputs.end(); ++itInput)
        helper->handleProviderInputAppears(_scnId, itInput->first, itInput->second.get());
}

void
view::StateAttributeProvider::removeProgramInputsFromHelper(GeometryDisplayHelper::Ptr const&   helper,
                                                            unsigned int                        progId)
{
    if (!helper)
        return;

    ProgramInputMap::const_iterator const& itInputs = _progInputs.find(progId);
    if (itInputs == _progInputs.end())
        return;

    const ProgramInputs& inputs = itInputs->second;
    for (ProgramInputs::const_iterator itInput = inputs.begin(); itInput != inputs.end(); ++itInput)
        helper->handleProviderInputDisappears(_scnId, itInput->first, itInput->second.get());

    // remove program inputs that are not used by any helper anymore
    for (ProgramInputMap::iterator it = _progInputs.begin(); it != _progInputs.end(); )
        if (areProgramInputsUsed(it->first))
            ++it;
        else
        {
            const unsigned int  progId  = it->first;
            ProgramInputs&      inputs  = it->second;
            while (!inputs.empty())
            {
                const unsigned int inId = inputs.begin()->first;
                destroyInput(progId, inId);
            }
            it = _progInputs.erase(it);
        }
}

bool
view::StateAttributeProvider::areProgramInputsUsed(unsigned int progId) const
{
    for (Helpers::const_iterator it = _helpers.begin(); it != _helpers.end(); ++it)
    {
        GeometryDisplayHelper::Ptr const&   helper  = it->second.lock();
        const gl::MaterialProgramBase*      prog    = helper ? helper->program() : nullptr;

        if (prog && prog->id() == progId)
            return true;
    }
    return false;
}
