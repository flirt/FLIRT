// ========================================================================= //
// Copyright 2016-2018 SUPAMONKS_STUDIO                                      //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
//                                                                           //
// ========================================================================= //
#include <cassert>

#include <osg/RenderInfo>
#include <smks/sys/ResourceDatabase.hpp>
#include <smks/sys/Exception.hpp>
#include <smks/parm/BuiltIns.hpp>
#include <smks/view/gl/MaterialProgramBase.hpp>
#include <smks/view/gl/ProgramInput.hpp>
#include <smks/view/gl/ProgramTemplate.hpp>
#include <smks/view/gl/ProgramTemplates.hpp>
#include <smks/view/gl/ContextExtensions.hpp>
#include <smks/view/gl/ProgramInputDeclarations.hpp>
#include <smks/view/gl/UniformDeclaration.hpp>
#include <smks/util/color/rgba8.hpp>
#include <smks/util/osg/NodeMask.hpp>
#include <smks/util/osg/drawable.hpp>
#include <smks/ClientBase.hpp>
#include <smks/view/gl/uniform.hpp>
#include <smks/view/gl/ObjectStateFlags.hpp>
#include <smks/view/gl/VertexAttributeFlags.hpp>

#include "GeometryDisplayHelper.hpp"
#include "PolyMeshGeometry.hpp"

namespace smks { namespace view
{
    class PolyMeshGeometry::UpdateCallback:
        public osg::Callback
    {
    public:
        virtual inline
        bool
        run(osg::Object* object, osg::Object* data)
        {
            PolyMeshGeometry* geo = reinterpret_cast<PolyMeshGeometry*>(object);
            if (geo)
                geo->loadProgram();
            return traverse(object, data);
        }
    };
}
}

using namespace smks;

// static
view::PolyMeshGeometry::Ptr
view::PolyMeshGeometry::create(unsigned int id, ClientBase::WPtr const& client)
{
    Ptr ptr = new PolyMeshGeometry(id, client);
    return ptr;
}

view::PolyMeshGeometry::PolyMeshGeometry(unsigned int id, ClientBase::WPtr const& cl):
    osg::Geometry       (),
    _client             (cl),
    _displayHelper      (GeometryDisplayHelper::create(id, cl, getOrCreateStateSet())),
    _currentProgram     (0),
    //------------------
    _currentBounds      (xchg::BoundingBox::UnitBox()),
    _currentVertices    (),
    _currentPositions   (),
    _currentNormals     (),
    _currentTexcoords   (),
    _currentIndices     (),
    _currentCount       (0),
    //------------------
    _bufferObjects      (),
    _uObjectStateFlags  (gl::uObjectStateFlags().asUniformDeclaration()->createInstance()),
    _uVertexAttributes  (gl::uVertexAttributes().asUniformDeclaration()->createInstance())
{
    ClientBase::Ptr const& client = cl.lock();
    if (!client)
        throw sys::Exception("Invalid client.", "Polymesh Geometry Construction");

    setName(client->getNodeName(id));
    util::osg::setupRender(*this);

    osg::StateSet* stateSet = getStateSet();
    assert(stateSet);
    stateSet->setDataVariance(osg::Object::DYNAMIC);    // prevents update phase to commence before all draw phases finish
    addUpdateCallback(new UpdateCallback());            // assign program at update phase

    // add object id uniform
    gl::UniformInput::Ptr const& uObjectId = gl::uObjectId().asUniformDeclaration()->createInstance();

    gl::setUniformui(*uObjectId->data(),            id);
    gl::setUniformi (*_uObjectStateFlags->data(),   gl::NO_OBJSTATE);
    gl::setUniformi (*_uVertexAttributes->data(),   gl::NO_VATTRIB);

    stateSet->addUniform(uObjectId->data(), osg::StateAttribute::OVERRIDE);
    stateSet->addUniform(_uObjectStateFlags->data());
    stateSet->addUniform(_uVertexAttributes->data());
}

view::PolyMeshGeometry::~PolyMeshGeometry()
{
    dispose();
    _displayHelper = nullptr;
}

void
view::PolyMeshGeometry::getPositions(osg::Vec3Array& oPositions) const
{
    oPositions.clear();

    xchg::Data::Ptr const& currentVertices = _currentVertices.lock();
    if (currentVertices == nullptr || currentVertices->empty() || !_currentPositions)
        return;

    assert(_currentNormals.stride() > 0);
    const size_t numVertices = currentVertices->size() / _currentNormals.stride();
    assert(numVertices > 0);
    osg::Vec3f* oPosition = nullptr;

    oPositions.resize(numVertices);
    if (oPositions.empty())
        return;
    oPosition = &oPositions.front();
    for (size_t i = 0; i < numVertices; ++i)
    {
        const float* ptr = _currentPositions.getPtr<float>(i);

        oPosition->set(ptr[0], ptr[1], ptr[2]);
        ++oPosition;
    }
}

void
view::PolyMeshGeometry::dispose()
{
    ClientBase::Ptr const&  client      = _client.lock();
    sys::ResourceDatabase*  resources   = client ? client->getResourceDatabase() : nullptr;
    gl::ContextExtensions*  extensions  = resources ? gl::getContextExtensions(*resources) : nullptr;

    setDataVariance(Object::STATIC);
    setNodeMask(util::osg::INVISIBLE_TO_ALL);

    _currentBounds      = xchg::BoundingBox::UnitBox();
    _currentVertices    .reset();
    _currentPositions   .clear();
    _currentNormals     .clear();
    _currentTexcoords   .clear();
    _currentIndices     .reset();
    _currentCount       = 0;

    if (extensions)
        for (size_t contextId = 0; contextId < _bufferObjects.size(); ++contextId)
        {
            BufferObjects& buffers = _bufferObjects.get(contextId);
            if (buffers)
                extensions->getOrCreateGLExtension(contextId)->glDeleteBuffers(2, &buffers.vbo);
        }
    _bufferObjects.clear();

    _currentProgram = 0;
    gl::setUniformi(*_uVertexAttributes->data(), gl::NO_VATTRIB);
}

void
view::PolyMeshGeometry::setBound(const xchg::BoundingBox& box)
{
    _currentBounds = box.valid() ? box : xchg::BoundingBox::UnitBox();
    dirtyBound();
}

void
view::PolyMeshGeometry::setVertices(xchg::Data::Ptr const& value)
{
    _currentVertices = value;
    dirtyVertices();
}

void
view::PolyMeshGeometry::setPositions(const xchg::DataStream& value)
{
    _currentPositions   = value;
    dirtyVertices();
    if (value.numElements() > 0)
        gl::add(*_uVertexAttributes->data(), gl::VATTRIB_POSITION);
    else
        gl::remove(*_uVertexAttributes->data(), gl::VATTRIB_POSITION);
}

void
view::PolyMeshGeometry::setNormals(const xchg::DataStream& value)
{
    _currentNormals = value;
    dirtyVertices();
    if (value.numElements() > 0)
        gl::add(*_uVertexAttributes->data(), gl::VATTRIB_NORMAL);
    else
        gl::remove(*_uVertexAttributes->data(), gl::VATTRIB_NORMAL);
}

void
view::PolyMeshGeometry::setTexcoords(const xchg::DataStream& value)
{
    _currentTexcoords   = value;
    dirtyVertices();
    if (value.numElements() > 0)
        gl::add(*_uVertexAttributes->data(), gl::VATTRIB_TEXCOORD);
    else
        gl::remove(*_uVertexAttributes->data(), gl::VATTRIB_TEXCOORD);
}

void
view::PolyMeshGeometry::setIndices(xchg::Data::Ptr const& value)
{
    _currentIndices = value;
    dirtyIndices();
}

void
view::PolyMeshGeometry::setCount(size_t value)
{
    _currentCount = value;
    dirtyIndices();
}

void
view::PolyMeshGeometry::setSelected(bool value)
{
    if (value)
        gl::add(*_uObjectStateFlags->data(), gl::OBJSTATE_SELECTED);
    else
        gl::remove(*_uObjectStateFlags->data(), gl::OBJSTATE_SELECTED);
}

void
view::PolyMeshGeometry::dirtyVertices()
{
    for (size_t contextId = 0; contextId < _bufferObjects.size(); ++contextId)
        _bufferObjects.get(contextId).dirtyVbo = true;
}
void
view::PolyMeshGeometry::dirtyIndices()
{
    for (size_t contextId = 0; contextId < _bufferObjects.size(); ++contextId)
        _bufferObjects.get(contextId).dirtyEbo = true;
}

// virtual
osg::BoundingBoxf
view::PolyMeshGeometry::computeBoundingBox() const
{
    return osg::BoundingBoxf(
        _currentBounds.minX, _currentBounds.minY, _currentBounds.minZ,
        _currentBounds.maxX, _currentBounds.maxY, _currentBounds.maxZ);
}

// virtual
void
view::PolyMeshGeometry::drawImplementation(osg::RenderInfo& renderInfo) const
{
    if (!isProgramUpToDate())
        return;

    xchg::Data::Ptr const& currentVertices  = _currentVertices.lock();
    xchg::Data::Ptr const& currentIndices   = _currentIndices.lock();
    if (!currentVertices    || currentVertices->empty() ||
        !currentIndices     || currentIndices->empty() ||
        !_currentPositions)
        return;
    if (_currentCount != 3) // quads as drawing primitives are deprecated in core profile
        return;

    assert(!_currentNormals     || _currentNormals.numElements()    == _currentPositions.numElements());
    assert(!_currentTexcoords   || _currentTexcoords.numElements()  == _currentPositions.numElements());
    //---
    ClientBase::Ptr const&          client      = _client.lock();
    const sys::ResourceDatabase*    resources   = client ? client->getResourceDatabase() : nullptr;
    const gl::ContextExtensions*    extensions  = resources ? gl::getContextExtensions(*resources) : nullptr;
    if (!extensions)
        return;

    assert(renderInfo.getState());
    osg::State&                 state       = *renderInfo.getState();
    const unsigned int          contextId   = state.getContextID();
    BufferObjects&              buffers     = _bufferObjects.get(contextId);
    const osg::GLExtensions*    ext         = const_cast<gl::ContextExtensions*>(extensions)
        ->getOrCreateGLExtension(contextId);

    assert(ext);

    const bool      generateBuffers = !buffers;
    const GLuint    locPosition     = state.getVertexAlias()._location;
    const GLuint    locNormal       = state.getNormalAlias()._location;
    const GLuint    locTexCoords    = !state.getTexCoordAliasList().empty() ? state.getTexCoordAliasList()[0]._location : 0;

    if (generateBuffers)
        ext->glGenBuffers(2, &buffers.vbo);

    ext->glBindBuffer(GL_ARRAY_BUFFER,          buffers.vbo);
    ext->glBindBuffer(GL_ELEMENT_ARRAY_BUFFER,  buffers.ebo);

    if (generateBuffers)
    {
        ext->glVertexAttribPointer(
            locPosition,
            3,
            GL_FLOAT,
            false,
            sizeof(float) * _currentPositions.stride(),
            (GLvoid*)(sizeof(float) * _currentPositions.offset()));
        ext->glVertexAttribPointer(
            locNormal,
            3,
            GL_FLOAT,
            false,
            sizeof(float) * _currentNormals.stride(),
            (GLvoid*)(sizeof(float) * _currentNormals.offset()));
        ext->glVertexAttribPointer(
            locTexCoords,
            2,
            GL_FLOAT,
            false,
            sizeof(float) * _currentTexcoords.stride(),
            (GLvoid*)(sizeof(float) * _currentTexcoords.offset()));
    }

    if (buffers.dirtyVbo)
    {
        ext->glBufferData(
            GL_ARRAY_BUFFER,
            currentVertices->size() * sizeof(float),
            currentVertices->getPtr<float>(),
            GL_DYNAMIC_DRAW);
        buffers.dirtyVbo = false;
    }
    if (buffers.dirtyEbo)
    {
        ext->glBufferData(
            GL_ELEMENT_ARRAY_BUFFER,
            currentIndices->size() * sizeof(unsigned int)/* * _currentNumIndices*/,
            currentIndices->getPtr<unsigned int>(),
            GL_DYNAMIC_DRAW);
        buffers.dirtyEbo = false;
    }

    assert(buffers.vbo > 0 && buffers.ebo > 0 && !buffers.dirtyVbo && !buffers.dirtyEbo);

    ext->glEnableVertexAttribArray(locPosition);
    ext->glEnableVertexAttribArray(locNormal);
    ext->glEnableVertexAttribArray(locTexCoords);
    //----------------------------------------------------------------------
    assert(_currentCount == 3);
    glDrawElements(GL_TRIANGLES, currentIndices->size(), GL_UNSIGNED_INT, 0);
    //----------------------------------------------------------------------
    ext->glDisableVertexAttribArray(locPosition);
    ext->glDisableVertexAttribArray(locNormal);
    ext->glDisableVertexAttribArray(locTexCoords);
    ext->glBindBuffer(GL_ARRAY_BUFFER,          0);
    ext->glBindBuffer(GL_ELEMENT_ARRAY_BUFFER,  0);
}

bool
view::PolyMeshGeometry::bindVbo(osg::RenderInfo&            renderInfo,
                                const osg::GLExtensions&    ext) const
{
    assert(renderInfo.getState());
    osg::State&             state       = *renderInfo.getState();
    const unsigned int      contextId   = state.getContextID();
    const BufferObjects&    buffers     = _bufferObjects.get(contextId);

    if (!buffers.vbo || buffers.dirtyVbo || !_currentPositions)
        return false;

    const GLuint locPosition        = state.getVertexAlias()._location;
    const GLuint locNormal          = state.getNormalAlias()._location;
    const GLuint locTexCoords       = !state.getTexCoordAliasList().empty() ? state.getTexCoordAliasList()[0]._location : 0;

    ext.glBindBuffer(GL_ARRAY_BUFFER, buffers.vbo);
    {
        ext.glEnableVertexAttribArray(locPosition);
        ext.glVertexAttribPointer(locPosition, 3, GL_FLOAT, false, sizeof(float) * _currentPositions.stride(), (GLvoid*)(sizeof(float) * _currentPositions.offset()));
    }
    if (_currentNormals)
    {
        ext.glEnableVertexAttribArray(locNormal);
        ext.glVertexAttribPointer(locNormal, 3, GL_FLOAT, false, sizeof(float) * _currentNormals.stride(), (GLvoid*)(sizeof(float) * _currentNormals.offset()));
    }
    if (_currentTexcoords)
    {
        ext.glEnableVertexAttribArray(locTexCoords);
        ext.glVertexAttribPointer(locTexCoords, 2, GL_FLOAT, false, sizeof(float) * _currentTexcoords.stride(), (GLvoid*)(sizeof(float) * _currentTexcoords.offset()));
    }
    return true;
}

void
view::PolyMeshGeometry::unbindVbo(osg::RenderInfo&          renderInfo,
                                  const osg::GLExtensions&  ext) const
{
    assert(renderInfo.getState());

    osg::State&         state       = *renderInfo.getState();
    const unsigned int  contextId   = state.getContextID();

    const GLuint locPosition        = state.getVertexAlias()._location;
    const GLuint locNormal          = state.getNormalAlias()._location;
    const GLuint locTexCoords       = !state.getTexCoordAliasList().empty() ? state.getTexCoordAliasList()[0]._location : 0;

    ext.glDisableVertexAttribArray(locPosition);
    ext.glDisableVertexAttribArray(locNormal);
    ext.glDisableVertexAttribArray(locTexCoords);
    ext.glBindBuffer(GL_ARRAY_BUFFER, 0);
}

bool
view::PolyMeshGeometry::isProgramUpToDate() const
{
    return _displayHelper &&
        _displayHelper->program() &&
        _currentProgram == _displayHelper->program()->id();
}

void
view::PolyMeshGeometry::loadProgram()
{
    // lazily load and assign shading program at update stage
    if (isProgramUpToDate())
        return;
    if (_displayHelper == nullptr ||
        _displayHelper->program() == nullptr)
        return;

    ClientBase::Ptr const&              client      = _client.lock();
    sys::ResourceDatabase::Ptr const&   resources   = client ? client->getOrCreateResourceDatabase() : nullptr;
    if (!resources)
        return;

    gl::getOrCreateContextExtensions(*resources);
    osg::ref_ptr<osg::Program> const& program = gl::getOrCreateProgram(
        _displayHelper->program()->progTemplate(), resources);
    if (!program)
        return;

    assert(getStateSet());
    getStateSet()->setAttribute(program.get(), osg::StateAttribute::OVERRIDE);
    _currentProgram = _displayHelper->program()->id();
}
