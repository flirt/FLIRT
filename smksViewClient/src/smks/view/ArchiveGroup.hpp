// ========================================================================= //
// Copyright 2016-2018 SUPAMONKS_STUDIO                                      //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
//                                                                           //
// ========================================================================= //
#pragma once

#include <memory>
#include <osg/Group>
#include <smks/util/osg/NodeMask.hpp>

namespace smks
{
    class ClientBase;

    namespace view
    {
        class ArchiveGroup:
            public osg::Group
        {
        public:
            typedef osg::ref_ptr<ArchiveGroup> Ptr;

        public:
            static inline
            Ptr
            create(unsigned int id, std::weak_ptr<ClientBase> const& client)
            {
                Ptr ptr(new ArchiveGroup(id, client));
                return ptr;
            }

        private:
            inline
            ArchiveGroup(unsigned int id, std::weak_ptr<ClientBase> const&):
                osg::Group()
            { }
            // non-copyable
            ArchiveGroup(const ArchiveGroup&);
            ArchiveGroup& operator=(const ArchiveGroup&);

        public:
            inline
            void
            dispose()
            {
                setNodeMask(util::osg::INVISIBLE_TO_ALL);
            }
        };
    }
}
