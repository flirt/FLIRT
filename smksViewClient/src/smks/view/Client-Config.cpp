// ========================================================================= //
// Copyright 2016-2018 SUPAMONKS_STUDIO                                      //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
//                                                                           //
// ========================================================================= //
#include <sstream>
#include <json/json.h>

#include <smks/sys/Exception.hpp>
#include <smks/xchg/cfg.hpp>
#include <smks/math/math.hpp>

#include "smks/view/Client.hpp"

namespace smks { namespace view
{
    struct Client::Config::PImpl
    {
        std::string     deviceModule;
        unsigned int    glTextureSize;
        unsigned int    glUdimTileSize;
    };
}
}

using namespace smks;

void
view::Client::Config::setDefaults()
{
    _pImpl->deviceModule    = "SmksViewDevice";
    _pImpl->glTextureSize   = 512;
    _pImpl->glUdimTileSize  = 256;
}

view::Client::Config::Config():
    _pImpl(new PImpl())
{
    setDefaults();
}

view::Client::Config::Config(const Config& other):
    _pImpl(new PImpl())
{
    *_pImpl = *other._pImpl;
}

view::Client::Config&
view::Client::Config::operator=(const Config& other)
{
    *_pImpl = *other._pImpl;
    return *this;
}

// explicit
view::Client::Config::Config(const char* json):
    _pImpl(new PImpl())
{
    setDefaults();

    if (strlen(json) == 0)
        return;

    Json::Value  root;
    Json::Reader reader;

    if (!reader.parse(json, root))
    {
        std::stringstream sstr;
        sstr
            << "Failed to parse Json configuration: " << reader.getFormatedErrorMessages();
        throw sys::Exception(sstr.str().c_str(), "View Client Configuration");
    }

    _pImpl->deviceModule   = root.get(xchg::cfg::viewport_device,   _pImpl->deviceModule).asString();
    _pImpl->glTextureSize  = root.get(xchg::cfg::gl_texture_size,   _pImpl->glTextureSize) .asUInt();
    _pImpl->glUdimTileSize = root.get(xchg::cfg::gl_udim_tile_size, _pImpl->glUdimTileSize).asUInt();

    if (_pImpl->glTextureSize == 0 ||
        !math::isp2(_pImpl->glTextureSize))
        throw sys::Exception(
            "Maximum size of a GL texture must be a positive power of 2.",
            "View Client Configuration");
    if (_pImpl->glUdimTileSize == 0 ||
        !math::isp2(_pImpl->glUdimTileSize))
        throw sys::Exception(
            "Maximum size of a GL UDIM tile must be a positive power of 2.",
            "View Client Configuration");
}

view::Client::Config::~Config()
{
    delete _pImpl;
}

const char*  view::Client::Config::deviceModule() const   { return _pImpl->deviceModule.c_str(); }
unsigned int view::Client::Config::glTextureSize() const  { return _pImpl->glTextureSize; }
unsigned int view::Client::Config::glUdimTileSize() const { return _pImpl->glUdimTileSize; }
