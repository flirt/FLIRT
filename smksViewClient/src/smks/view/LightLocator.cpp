// ========================================================================= //
// Copyright 2016-2018 SUPAMONKS_STUDIO                                      //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
//                                                                           //
// ========================================================================= //
#include <osg/LineWidth>
#include <osg/Depth>
#include <osg/Geometry>

#include <smks/xchg/NodeCode.hpp>
#include <smks/xchg/NodeCodes.hpp>
#include <smks/xchg/Buffered.hpp>
#include <smks/math/matrixAlgo.hpp>
#include <smks/ClientBase.hpp>
#include <smks/view/gl/ProgramInput.hpp>
#include <smks/view/gl/ProgramTemplate.hpp>
#include <smks/view/gl/ProgramTemplates.hpp>
#include <smks/view/gl/ContextExtensions.hpp>
#include <smks/view/gl/ProgramInputDeclarations.hpp>
#include <smks/view/gl/ObjectStateFlags.hpp>
#include <smks/view/gl/uniform.hpp>
#include <smks/sys/Log.hpp>
#include <smks/sys/ResourceDatabase.hpp>
#include <smks/util/string/getId.hpp>
#include <smks/util/osg/geometry.hpp>
#include <smks/util/osg/NodeMask.hpp>
#include <smks/util/osg/basic_shapes.hpp>
#include <smks/util/osg/math.hpp>
#include <smks/util/osg/drawable.hpp>
#include <smks/util/color/rgba8.hpp>

#include "LightLocator.hpp"

namespace smks { namespace view
{
    class LightLocator::Geometry:
        public osg::Geometry
    {
        friend class UpdateCallback;
    public:
        typedef osg::ref_ptr<Geometry> Ptr;
    private:
        ////////////////////
        // struct BufferData
        ////////////////////
        struct BufferData
        {
            std::vector<float>          positions3f;
            std::vector<unsigned int>   lines;
        public:
            inline BufferData(): positions3f(), lines() { }
            inline void dirty() { positions3f.clear(); lines.clear(); }
            inline operator bool() const { return !positions3f.empty() && !lines.empty(); }
        };
        //////////////////////
        // struct BufferObjects
        //////////////////////
        struct BufferObjects
        {
            GLuint  vbo, ebo;
            bool    dirty;
        public:
            inline BufferObjects(): vbo(0), ebo(0), dirty(true) { }
            inline operator bool() const { return vbo != 0 && ebo != 0; }
        };
        //////////////////////
        typedef xchg::Buffered<BufferObjects> PerContextBuffers;
        class UpdateCallback;
    private:
        const ClientBase::WPtr      _client;
        //////////////////
        // characteristics
        //////////////////
        Type                        _type;
        float                       _halfAngleD;    //<! for distant lights
        float                       _minAngleD;     //<! for spot lights
        float                       _maxAngleD;     //<! for spot lights

        osg::BoundingBoxf           _precomputedBounds;
        BufferData                  _bufferData;

        mutable PerContextBuffers   _bufferObjects;

        gl::UniformInput::Ptr       _uObjectStateFlags;
        gl::UniformInput::Ptr       _uScalelessModelView;
        gl::UniformInput::Ptr       _uModelViewScale;
        gl::UniformInput::Ptr       _uIsBillboard;

    public:
        static inline
        Ptr
        create(unsigned int id, ClientBase::WPtr const&);
    protected:
        Geometry(unsigned int id, ClientBase::WPtr const&);

    public:
        ~Geometry();

        void
        dispose();

        inline
        Type
        type() const
        {
            return _type;
        }

        void
        type(Type);

        void
        setHalfAngleD(float);

        void
        setMinAngleD(float);

        void
        setMaxAngleD(float);

        inline
        void
        setSelected(bool);

        virtual
        osg::BoundingBoxf
        computeBoundingBox() const;

        virtual
        void
        drawImplementation(osg::RenderInfo&) const;

    private:
        inline
        void
        dirtyShape();

        void
        recomputeBufferData();
        void
        recomputePointLightBuffers(BufferData&) const;
        void
        recomputeDirectionalLightBuffers(BufferData&) const;
        void
        recomputeAmbientLightBuffers(BufferData&) const;
        void
        recomputeDistantLightBuffers(BufferData&) const;
        void
        recomputeSpotLightBuffers(BufferData&) const;
        void
        recomputeHDRILightBuffers(BufferData&) const;
        void
        recomputeMeshLightBuffers(BufferData&) const;

        inline
        bool
        isProgramUpToDate() const;
        inline
        void
        loadProgram();
    private:
        static
        void
        appendSpotCone(std::vector<float>&,
                       std::vector<unsigned int>&,
                       float    halfAngleD,
                       float    maxRadius,
                       float&   zDist,  //<! return distance along negative z axis
                       float&   radius,
                       size_t = NUM_CIRCLE_SEGMENTS);
    };

    ///////////////////////////////////////////////
    // class LightLocator::Geometry::UpdateCallback
    ///////////////////////////////////////////////
    class LightLocator::Geometry::UpdateCallback:
        public osg::Callback
    {
    public:
        virtual inline
        bool
        run(osg::Object* object, osg::Object* data)
        {
            Geometry* geo = reinterpret_cast<Geometry*>(object);
            if (geo)
            {
                geo->loadProgram();
                geo->recomputeBufferData();
            }
            return traverse(object, data);
        }
    };
}
}

using namespace smks;

///////////////////
// class LightLocator
///////////////////
// static
view::LightLocator::Ptr
view::LightLocator::create(unsigned int id, ClientBase::WPtr const& client)
{
    Ptr ptr = new LightLocator(id, client);
    return ptr;
}


view::LightLocator::LightLocator(unsigned int id, ClientBase::WPtr const& cl):
    osg::MatrixTransform(),
    _geometry           (Geometry::create(id, cl)),
    _xform              (math::Affine3f::Identity())
{
    addChild(_geometry);
    setName("__xform[" + _geometry->getName() + "]");
}

view::LightLocator::~LightLocator()
{
    dispose();
}

void
view::LightLocator::setCode(const std::string& code)
{
    const Type typ = getType(code.c_str());

    _geometry->type(typ);
    setNodeMask(typ == UNSPECIFIED_LIGHT_LOCATOR
        ? util::osg::INVISIBLE_TO_ALL
        : util::osg::VISIBLE_SCENE_ENTITY);
}

// static
view::LightLocator::Type
view::LightLocator::getType(const char* code)
{
    const unsigned int lightCode = util::string::getId(code);
    if      (lightCode == xchg::code::point()       .id())  return POINT_LIGHT_LOCATOR;
    else if (lightCode == xchg::code::directional() .id())  return DIRECTIONAL_LIGHT_LOCATOR;
    else if (lightCode == xchg::code::ambient()     .id())  return AMBIENT_LIGHT_LOCATOR;
    else if (lightCode == xchg::code::distant()     .id())  return DISTANT_LIGHT_LOCATOR;
    else if (lightCode == xchg::code::spot()        .id())  return SPOT_LIGHT_LOCATOR;
    else if (lightCode == xchg::code::hdri()        .id())  return HDRI_LIGHT_LOCATOR;
    else if (lightCode == xchg::code::mesh()        .id())  return MESH_LIGHT_LOCATOR;
    else
    {
        if (strlen(code) > 0)
            std::cerr << "Unknown light code '" << code << "'." << std::endl;
        return UNSPECIFIED_LIGHT_LOCATOR;
    }
}

void
view::LightLocator::setTransform(const math::Affine3f& value)
{
    _xform = value;
    commitMatrix();
}

void
view::LightLocator::setSelected(bool value)
{
    if (_geometry)
        _geometry->setSelected(value);
}


void
view::LightLocator::commitMatrix()
{
    _matrix.set(_xform.data());
    _inverseDirty = true;
    dirtyBound();
}

void
view::LightLocator::setHalfAngleD(float value)
{
    _geometry->setHalfAngleD(value);
}

void
view::LightLocator::setMinAngleD(float value)
{
    _geometry->setMinAngleD(value);
}

void
view::LightLocator::setMaxAngleD(float value)
{
    _geometry->setMaxAngleD(value);
}

void
view::LightLocator::dispose()
{
    _geometry->dispose();
}


/////////////////////////////
// class LightLocator::Geometry
/////////////////////////////
// static inline
view::LightLocator::Geometry::Ptr
view::LightLocator::Geometry::create(unsigned int id, ClientBase::WPtr const& cl)
{
    Ptr ptr = new Geometry(id, cl);
    return ptr;
}

view::LightLocator::Geometry::Geometry(unsigned int id, ClientBase::WPtr const& cl):
    osg::Geometry       (),
    _client             (cl),
    //--------------
    _type               (UNSPECIFIED_LIGHT_LOCATOR),
    _halfAngleD         (0.0f),
    _minAngleD          (0.0f),
    _maxAngleD          (0.0f),
    //--------------
    _precomputedBounds  (),
    _bufferData         (),
    _bufferObjects      (),
    _uObjectStateFlags  (gl::get_SceneObjLocatorProgram().createUniform(gl::uObjectStateFlags(), gl::NO_OBJSTATE)),
    _uScalelessModelView(gl::get_SceneObjLocatorProgram().createUniform(gl::uScalelessModelView())),
    _uModelViewScale    (gl::get_SceneObjLocatorProgram().createUniform(gl::uModelViewScale())),
    _uIsBillboard       (gl::get_SceneObjLocatorProgram().createUniform(gl::uIsBillboard(), false, osg::Object::DYNAMIC))
{
    ClientBase::Ptr const& client = cl.lock();
    if (!client)
        throw sys::Exception("Invalid client.", "Light Gizmo Geometry Construction");

    setName(client->getNodeName(id));
    util::osg::setupRender(*this);

    osg::StateSet* stateSet = getOrCreateStateSet();
    stateSet->setDataVariance(osg::Object::DYNAMIC);    // prevents update phase to commence before all draw phases finish
    addUpdateCallback(new UpdateCallback());            // assign program at update phase

    osg::LineWidth* lineWidth = new osg::LineWidth(2.0f);
    stateSet->setAttributeAndModes(lineWidth, osg::StateAttribute::ON);

    osg::Depth* depth = new osg::Depth(osg::Depth::ALWAYS);
    stateSet->setAttributeAndModes(depth, osg::StateAttribute::ON);

    gl::UniformInput::Ptr const& uObjectId  = gl::get_SceneObjLocatorProgram().createUniform(gl::uObjectId(), id);
    gl::UniformInput::Ptr const& uColor     = gl::get_SceneObjLocatorProgram().createUniform(
        gl::uColor(), math::Vector4f(0.6f, 0.15f, 0.0f, 1.0f).data(), osg::Object::STATIC);

    stateSet->addUniform(uObjectId->data(), osg::StateAttribute::OVERRIDE);
    stateSet->addUniform(_uObjectStateFlags->data());
    stateSet->addUniform(uColor->data());
    stateSet->addUniform(_uIsBillboard->data());
    stateSet->addUniform(_uScalelessModelView->data());
    stateSet->addUniform(_uModelViewScale->data());
}

view::LightLocator::Geometry::~Geometry()
{
    dispose();
}

void
view::LightLocator::Geometry::type(Type value)
{
    if (value != _type)
        dirtyShape();
    _type = value;
    view::gl::setUniformb(*_uIsBillboard->data(), _type == Type::AMBIENT_LIGHT_LOCATOR || _type == Type::POINT_LIGHT_LOCATOR);
}

void
view::LightLocator::Geometry::setHalfAngleD(float value)
{
    if (_type == DISTANT_LIGHT_LOCATOR &&
        math::abs(_halfAngleD - value) > 1e-6f)
        dirtyShape();
    _halfAngleD = value;
}

void
view::LightLocator::Geometry::setMinAngleD(float value)
{
    if (_type == SPOT_LIGHT_LOCATOR &&
        math::abs(_minAngleD - value) > 1e-6f)
        dirtyShape();
    _minAngleD  = value;
}

void
view::LightLocator::Geometry::setMaxAngleD(float value)
{
    if (_type == SPOT_LIGHT_LOCATOR &&
        math::abs(_maxAngleD - value) > 1e-6f)
        dirtyShape();
    _maxAngleD  = value;
}

void
view::LightLocator::Geometry::setSelected(bool value)
{
    if (value)
        gl::add(*_uObjectStateFlags->data(), gl::OBJSTATE_SELECTED);
    else
        gl::remove(*_uObjectStateFlags->data(), gl::OBJSTATE_SELECTED);
}

void
view::LightLocator::Geometry::dispose()
{
    ClientBase::Ptr const&  client      = _client.lock();
    sys::ResourceDatabase*  resources   = client ? client->getResourceDatabase() : nullptr;
    gl::ContextExtensions*  extensions  = resources ? gl::getContextExtensions(*resources) : nullptr;

    setNodeMask(util::osg::INVISIBLE_TO_ALL);

    _type       = UNSPECIFIED_LIGHT_LOCATOR;
    _minAngleD  = 0.0f;
    _maxAngleD  = 0.0f;

    _precomputedBounds.init();
    _bufferData.dirty();
    if (extensions)
        for (size_t contextId = 0; contextId < _bufferObjects.size(); ++contextId)
        {
            BufferObjects& buffers = _bufferObjects.get(contextId);
            if (buffers)
                extensions->getOrCreateGLExtension(contextId)->glDeleteBuffers(2, &buffers.vbo);
        }
}

// virtual
osg::BoundingBoxf
view::LightLocator::Geometry::computeBoundingBox() const
{
    return _precomputedBounds;
}

// virtual
void
view::LightLocator::Geometry::drawImplementation(osg::RenderInfo& renderInfo) const
{
    if (!isProgramUpToDate())
        return;

    if (!_bufferData)
        return;
    //---
    ClientBase::Ptr const&          client      = _client.lock();
    const sys::ResourceDatabase*    resources   = client ? client->getResourceDatabase() : nullptr;
    const gl::ContextExtensions*    extensions  = resources ? gl::getContextExtensions(*resources) : nullptr;
    if (!extensions)
        return;

    assert(renderInfo.getState());
    osg::State&                 state       = *renderInfo.getState();
    const unsigned int          contextId   = state.getContextID();
    BufferObjects&              buffers     = _bufferObjects.get(contextId);
    const osg::GLExtensions*    ext         = const_cast<gl::ContextExtensions*>(extensions)
        ->getOrCreateGLExtension(contextId);

    // decompose modelview in order to better handle scaling in vertex shader
    {
        math::Affine3f modelView, scalelessModelView;
        math::Vector4f modelViewScale;

        util::osg::convert(state.getModelViewMatrix(), modelView);
        math::removeScaling(modelView, scalelessModelView, &modelViewScale);
        view::gl::setUniformfv(*_uModelViewScale    ->data(), modelViewScale.data());
        view::gl::setUniformfv(*_uScalelessModelView->data(), scalelessModelView.data());
    }

    //const float curScaling    = math::scaling(curModelView);
    //float     prvScaling  = 1.0f;
    //_uScaling->get(prvScaling);
    //if (fabsf(curScaling - prvScaling) > 1e-3f)
    //{
    //  _uScaling->set(curScaling);
    //  _uScaling->dirty();
    //}

    assert(ext);

    const bool      generateBuffers = !buffers;
    const GLuint    locPosition     = state.getVertexAlias()._location;

    if (generateBuffers)
        ext->glGenBuffers(2, &buffers.vbo);

    ext->glBindBuffer(GL_ARRAY_BUFFER,          buffers.vbo);
    ext->glBindBuffer(GL_ELEMENT_ARRAY_BUFFER,  buffers.ebo);

    if (generateBuffers)
        ext->glVertexAttribPointer(locPosition, 3, GL_FLOAT, false, 3 * sizeof(float), 0);

    if (buffers.dirty)
    {
        ext->glBufferData(
            GL_ARRAY_BUFFER,
            _bufferData.positions3f.size() * sizeof(float),
            !_bufferData.positions3f.empty() ? &_bufferData.positions3f.front() : nullptr,
            GL_DYNAMIC_DRAW);
        ext->glBufferData(
            GL_ELEMENT_ARRAY_BUFFER,
            _bufferData.lines.size() * sizeof(unsigned int),
            !_bufferData.lines.empty() ? &_bufferData.lines.front() : nullptr,
            GL_DYNAMIC_DRAW);
        buffers.dirty = false;
    }

    ext->glEnableVertexAttribArray(locPosition);
    //--------------------------------------------------------------------
    glDrawElements(GL_LINES, _bufferData.lines.size(), GL_UNSIGNED_INT, 0);
    //--------------------------------------------------------------------
    ext->glDisableVertexAttribArray(locPosition);
    ext->glBindBuffer(GL_ARRAY_BUFFER,          0);
    ext->glBindBuffer(GL_ELEMENT_ARRAY_BUFFER,  0);
}

bool
view::LightLocator::Geometry::isProgramUpToDate() const
{
    return getStateSet() &&
        getStateSet()->getAttribute(osg::StateAttribute::PROGRAM);
}

void
view::LightLocator::Geometry::loadProgram()
{
    if (isProgramUpToDate())
        return;

    ClientBase::Ptr const&              client      = _client.lock();
    sys::ResourceDatabase::Ptr const&   resources   = client ? client->getOrCreateResourceDatabase() : nullptr;
    if (!resources)
        return;

    gl::getOrCreateContextExtensions(*resources);
    osg::ref_ptr<osg::Program> const& program = gl::getOrCreateProgram(
        gl::get_SceneObjLocatorProgram(), resources);
    if (!program)
        return;

    assert(getStateSet());
    getStateSet()->setAttribute(program.get(), osg::StateAttribute::OVERRIDE);
}

void
view::LightLocator::Geometry::dirtyShape()
{
    _bufferData.dirty();
    for (size_t i = 0; i < _bufferObjects.size(); ++i)
        _bufferObjects.get(i).dirty = true;
}

void
view::LightLocator::Geometry::recomputeBufferData()
{
    if (_bufferData)
        return;

    _bufferData.dirty();
    switch (_type)
    {
    default: break;
    case UNSPECIFIED_LIGHT_LOCATOR: break;
    case POINT_LIGHT_LOCATOR:       recomputePointLightBuffers      (_bufferData);  break;
    case DIRECTIONAL_LIGHT_LOCATOR: recomputeDirectionalLightBuffers(_bufferData);  break;
    case AMBIENT_LIGHT_LOCATOR:     recomputeAmbientLightBuffers    (_bufferData);  break;
    case DISTANT_LIGHT_LOCATOR:     recomputeDistantLightBuffers    (_bufferData);  break;
    case SPOT_LIGHT_LOCATOR:        recomputeSpotLightBuffers       (_bufferData);  break;
    case HDRI_LIGHT_LOCATOR:        recomputeHDRILightBuffers       (_bufferData);  break;
    case MESH_LIGHT_LOCATOR:        recomputeMeshLightBuffers       (_bufferData);  break;
    }

    // precompute the bounding box
    _precomputedBounds = util::osg::computeBounds(_bufferData.positions3f, 3);
    util::osg::symmetrizeBounds(_precomputedBounds);
    dirtyBound();
}

void
view::LightLocator::Geometry::recomputePointLightBuffers(BufferData& ret) const
{
    ret.positions3f .clear();
    ret.lines       .clear();

    ret.positions3f .reserve(3 * NUM_CIRCLE_SEGMENTS + 51);
    ret.lines       .reserve(2 * NUM_CIRCLE_SEGMENTS + 16);

    math::Affine3f xfm;

    util::osg::appendUnitDiskXY(ret.positions3f, ret.lines, nullptr, NUM_CIRCLE_SEGMENTS);
    util::osg::appendRaysXY(ret.positions3f, ret.lines, 4, 0.0f, 1.0f);
    math::composeEuler(xfm, math::Vector4f::UnitW(), math::XYZ, math::Vector4f(0.0f, 0.0f, 45.0f, 0.0f), math::Vector4f::Ones());
    util::osg::appendRaysXY(ret.positions3f, ret.lines, 4, 0.5f, 1.0f, xfm);
}

void
view::LightLocator::Geometry::recomputeDirectionalLightBuffers(BufferData& ret) const
{
    ret.positions3f .clear();
    ret.lines       .clear();

    ret.positions3f .reserve(60);
    ret.lines       .reserve(40);

    util::osg::appendForwardArrow(ret.positions3f, ret.lines, nullptr, 1.0f, 0.4f, 0.4f);
    for (size_t i = 0; i < 3; ++i)
    {
        math::Affine3f xfm, rot;
        float rz = 30.0f + i * 120.0f;

        xfm.setIdentity();
        xfm.translation()[1] = -0.5f;
        math::composeEuler(rot, math::Vector4f::UnitW(), math::XYZ, math::Vector4f(0.0f, 0.0f, rz, 0.0f));
        xfm = rot * xfm;
        util::osg::appendForwardArrow(ret.positions3f, ret.lines, nullptr, 1.0f, 0.2f, 0.2f, xfm);
    }
}

void
view::LightLocator::Geometry::recomputeAmbientLightBuffers(BufferData& ret) const
{
    ret.positions3f .clear();
    ret.lines       .clear();

    ret.positions3f .reserve(3 * NUM_CIRCLE_SEGMENTS + 51);
    ret.lines       .reserve(2 * NUM_CIRCLE_SEGMENTS + 16);

    math::Affine3f xfm;

    util::osg::appendUnitDiskXY(ret.positions3f, ret.lines, nullptr, NUM_CIRCLE_SEGMENTS);
    util::osg::appendRaysXY(ret.positions3f, ret.lines, 4, 0.5f, 1.0f);
    math::composeEuler(xfm, math::Vector4f::UnitW(), math::XYZ, math::Vector4f(0.0f, 0.0f, 45.0f, 0.0f), math::Vector4f::Ones());
    util::osg::appendRaysXY(ret.positions3f, ret.lines, 4, 0.5f, 1.0f, xfm);
}

void
view::LightLocator::Geometry::recomputeDistantLightBuffers(BufferData& ret) const
{
    ret.positions3f .clear();
    ret.lines       .clear();

    ret.positions3f .reserve(3 * NUM_CIRCLE_SEGMENTS + 63);
    ret.lines       .reserve(2 * NUM_CIRCLE_SEGMENTS + 40);

    const float ang = _halfAngleD * static_cast<float>(sys::degrees_to_radians);
    if (ang > 1e-6f)
    {
        math::Affine3f xfm, rot;
        const float tAng    = math::tan(ang);
        const float zDist   = math::min(0.75f, tAng > 1e-6f ? 0.5f / tAng : 0.0f);
        const float length  = math::sqrt(0.25f * (1.0f + 1.0f / (tAng * tAng)));

        math::composeEuler(xfm, math::Vector4f(0.0f, 0.0f, -zDist, 1.0f));
        util::osg::appendUnitDiskXY (ret.positions3f, ret.lines, nullptr, NUM_CIRCLE_SEGMENTS, xfm);

        for (size_t i = 0; i < 4; ++i)
        {
            math::Affine3f  rot;
            math::Vector4f  eulerD( i & 2 ? -_halfAngleD : _halfAngleD, 0.0f, i & 1 ? 90.0f : 0.0f, 0.0f);

            xfm.setIdentity();
            xfm.translation()[0] = 0.0f;
            xfm.translation()[1] = 0.0f;
            xfm.translation()[2] = -0.3f;
            math::composeEuler(rot, math::Vector4f::UnitW(), math::XYZ, eulerD);
            xfm = rot * xfm;
            util::osg::appendForwardArrow(ret.positions3f, ret.lines, nullptr, length, 0.2f, 0.2f, xfm);
        }
    }
    else
    {
        // no angle
        util::osg::appendUnitDiskXY (ret.positions3f, ret.lines, nullptr, NUM_CIRCLE_SEGMENTS);

        for (size_t i = 0; i < 4; ++i)
        {
            const float     sign = i & 1 ? -1.0f : 1.0f;
            const float     tx = sign * (i & 2 ? 0.5f : 0.0f);
            const float     ty = sign * (i & 2 ? 0.0f : 0.5f);
            const float     rz = i & 2 ? 90.0f : 0.0f;
            math::Affine3f  xfm;

            math::composeEuler(xfm, math::Vector4f(tx, ty, 0.5f, 1.0f), math::XYZ, math::Vector4f(0.0f, 0.0f, rz, 0.0f));
            util::osg::appendForwardArrow(ret.positions3f, ret.lines, nullptr, 1.0f, 0.2f, 0.2f, xfm);
        }
    }
}

// static
void
view::LightLocator::Geometry::appendSpotCone(std::vector<float>&        positions3f,
                                             std::vector<unsigned int>& lines,
                                             float                      halfAngleD,
                                             float                      maxRadius,
                                             float&                     zDist,
                                             float&                     radius,
                                             size_t                     numDiskSegments)
{
    zDist   = 0.75f;
    radius  = maxRadius;
    const float MIN_T_ANG = maxRadius / zDist;

    if (halfAngleD < 90.0f)
    {
        float tAng  = math::tan(halfAngleD * static_cast<float>(sys::degrees_to_radians));
        if (MIN_T_ANG < tAng)
            zDist = radius / tAng;
        else
            radius = zDist * tAng;
    }
    else
    {
        zDist   = 0.0f;
        radius  = maxRadius;
    }
    float sc = radius * 2.0f; // unit circle's radius = 0.5

    if (radius > 1e-6f)
    {
        math::Affine3f xfm;

        math::composeEuler(xfm, math::Vector4f(0.0f, 0.0f, -zDist, 1.0f), math::XYZ, math::Vector4f::Zero(), math::Vector4f(sc, sc, sc, 1.0f));
        util::osg::appendUnitDiskXY(positions3f, lines, nullptr, NUM_CIRCLE_SEGMENTS, xfm);
    }
    else
        zDist = 0.0f;
}

void
view::LightLocator::Geometry::recomputeSpotLightBuffers(BufferData& ret) const
{
    ret.positions3f .clear();
    ret.lines       .clear();

    // get correct angle range
    float minAng = math::clamp(_minAngleD, 0.0f, 90.0f);
    float maxAng = math::clamp(_maxAngleD, 0.0f, 90.0f);
    if (minAng > maxAng)
        std::swap(minAng, maxAng);

    // circles
    float minAngZDist   = 0.0f;
    float minAngRadius  = 0.0f;
    float maxAngZDist   = 0.0f;
    float maxAngRadius  = 0.0f;

    appendSpotCone(ret.positions3f, ret.lines, minAng, 0.4f, minAngZDist, minAngRadius, NUM_CIRCLE_SEGMENTS);
    appendSpotCone(ret.positions3f, ret.lines, maxAng, 0.5f, maxAngZDist, maxAngRadius, NUM_CIRCLE_SEGMENTS);

    // outer lines
    size_t numPositions = ret.positions3f.size() / 3;
    size_t v0           = numPositions;

    ret.positions3f.push_back(0.0f);
    ret.positions3f.push_back(0.0f);
    ret.positions3f.push_back(0.0f);
    ++numPositions;
    for (size_t i = 0; i < 4; ++i)
    {
        const float sign = i & 2 ? -1.0f : 1.0f;

        ret.lines.push_back(v0);
        ret.lines.push_back(numPositions);
        ret.positions3f.push_back(sign * (i & 1 ? maxAngRadius : 0.0f));
        ret.positions3f.push_back(sign * (i & 1 ? 0.0f : maxAngRadius));
        ret.positions3f.push_back(-maxAngZDist);
        ++numPositions;
    }

    // arrow
    float arrowLength = math::max(minAngZDist, maxAngZDist);
    if (arrowLength < 1e-6f)
        arrowLength = 1.0f;
    else
        arrowLength += 0.3f;
    util::osg::appendForwardArrow(ret.positions3f, ret.lines, nullptr, arrowLength, 0.2f, 0.2f);
}

void
view::LightLocator::Geometry::recomputeHDRILightBuffers(BufferData& ret) const
{
    ret.positions3f .clear();
    ret.lines       .clear();

    ret.positions3f .reserve(9 * NUM_CIRCLE_SEGMENTS + 9);
    ret.lines       .reserve(6 * NUM_CIRCLE_SEGMENTS);

    math::Affine3f xfm;

    util::osg::appendUnitDiskXY (ret.positions3f, ret.lines, nullptr, NUM_CIRCLE_SEGMENTS);

    math::composeEuler(xfm, math::Vector4f::UnitW(), math::XYZ, math::Vector4f(90.0f, 0.0f, 0.0f, 0.0f));
    util::osg::appendUnitDiskXY (ret.positions3f, ret.lines, nullptr, NUM_CIRCLE_SEGMENTS, xfm);

    math::composeEuler(xfm, math::Vector4f::UnitW(), math::XYZ, math::Vector4f(00.0f, 90.0f, 0.0f, 0.0f));
    util::osg::appendUnitDiskXY (ret.positions3f, ret.lines, nullptr, NUM_CIRCLE_SEGMENTS, xfm);
}

void
view::LightLocator::Geometry::recomputeMeshLightBuffers(BufferData& ret) const
{
    ret.positions3f .clear();
    ret.lines       .clear();
}
