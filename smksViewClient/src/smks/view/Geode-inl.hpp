// ========================================================================= //
// Copyright 2016-2018 SUPAMONKS_STUDIO                                      //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
//                                                                           //
// ========================================================================= //

template<class ViewGeomType> inline //static
osg::ref_ptr<view::Geode<ViewGeomType>>
view::Geode<ViewGeomType>::create(unsigned int id, ClientWPtr const& client)
{
    osg::ref_ptr<view::Geode<ViewGeomType>> ptr(new Geode<ViewGeomType>(id, client));
    return ptr;
}

template<class ViewGeomType> inline
view::Geode<ViewGeomType>::Geode(unsigned int id, ClientWPtr const& client):
    osg::Geode      (),
    _mainGeometry   (ViewGeomType::create(id, client))
{
    setName("__geode[" + _mainGeometry->getName() + "]");
    addDrawable(_mainGeometry.get());
}

template<class ViewGeomType> inline
void
view::Geode<ViewGeomType>::dispose()
{
    removeDrawables(0, getNumDrawables());
    _mainGeometry = nullptr;
}
