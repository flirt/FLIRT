// ========================================================================= //
// Copyright 2016-2018 SUPAMONKS_STUDIO                                      //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
//                                                                           //
// ========================================================================= //
#pragma once

#include <osg/Geode>

#include "PointsGeometry.hpp"
#include "CurvesGeometry.hpp"
#include "PolyMeshGeometry.hpp"

namespace smks
{
    class ClientBase;
    namespace view
    {
        template<class ViewGeomType>
        class Geode:
            public osg::Geode
        {
        private:
            typedef std::weak_ptr<ClientBase> ClientWPtr;
        private:
            osg::ref_ptr<ViewGeomType> _mainGeometry;
        public:
            static inline
            osg::ref_ptr<Geode<ViewGeomType>>
            create(unsigned int id, ClientWPtr const&);

        private:
            inline
            Geode(unsigned int id, ClientWPtr const&);
            // non-copyable
            Geode(const Geode&);
            Geode& operator=(const Geode&);

        public:
            inline
            void
            dispose();

            inline
            ViewGeomType*
            mainGeometry()
            {
                return _mainGeometry.get();
            }

            inline
            const ViewGeomType*
            mainGeometry() const
            {
                return _mainGeometry.get();
            }
        };

        typedef Geode<PointsGeometry>   PointsGeode;
        typedef Geode<CurvesGeometry>   CurvesGeode;
        typedef Geode<PolyMeshGeometry> PolyMeshGeode;
    }
#include "Geode-inl.hpp"
}
