// ========================================================================= //
// Copyright 2016-2018 SUPAMONKS_STUDIO                                      //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
//                                                                           //
// ========================================================================= //
#include <osg/LineWidth>
#include <osg/Depth>
#include <osg/Projection>
#include <osg/Geometry>

#include <smks/xchg/Buffered.hpp>
#include <smks/view/gl/UniformDeclaration.hpp>
#include <smks/view/gl/ObjectStateFlags.hpp>
#include <smks/view/gl/ProgramInput.hpp>
#include <smks/view/gl/ProgramInputDeclarations.hpp>
#include <smks/view/gl/ProgramTemplate.hpp>
#include <smks/view/gl/ProgramTemplates.hpp>
#include <smks/view/gl/ContextExtensions.hpp>
#include <smks/view/gl/uniform.hpp>

#include <std/to_string_osg.hpp>
#include <smks/ClientBase.hpp>
#include <smks/math/matrixAlgo.hpp>
#include <smks/sys/Log.hpp>
#include <smks/sys/ResourceDatabase.hpp>
#include <smks/util/osg/NodeMask.hpp>
#include <smks/util/osg/drawable.hpp>
#include <smks/util/osg/geometry.hpp>
#include <smks/util/osg/math.hpp>
#include <smks/util/color/rgba8.hpp>

#include "CameraLocator.hpp"

namespace smks { namespace view
{
    //////////////////////////////////
    // class CameraLocator::Projection
    //////////////////////////////////
    class CameraLocator::Projection:
        public osg::Projection
    {
    public:
        typedef osg::ref_ptr<Projection> Ptr;
    public:
        static inline
        Ptr
        create()
        {
            Ptr ptr(new Projection());
            return ptr;
        }
    private:
        inline
        Projection();
        // non-copyable
        Projection(const Projection&);
        Projection& operator=(const Projection&);
    public:
        inline
        void
        dispose();

        inline
        void
        setPerspective(float yFieldOfViewD, float xAspectRatio, float zNear, float zFar);
    };

    ////////////////////////////////
    // class CameraLocator::Geometry
    ////////////////////////////////
    // WARNING: This class will be shared among all camera locator instances.
    // It is NOT meant to store instance specific data such as object ID, or object state flags.
    class CameraLocator::Geometry:
        public osg::Geometry
    {
        friend class UpdateCallback;
    public:
        typedef osg::ref_ptr<Geometry> Ptr;
    private:
        enum PolylinePlane { XY = 0, ZY };
    private:
        ////////////////////
        // struct BufferData
        ////////////////////
        struct BufferData
        {
            std::vector<float>          positions3f;
            std::vector<unsigned int>   lines;
        public:
            inline BufferData(): positions3f(), lines() { }
            inline void dirty(){ positions3f.clear(); lines.clear(); }
            inline operator bool() const { return !positions3f.empty() && !lines.empty(); }
        };
        //////////////////////
        // struct BufferObjects
        //////////////////////
        struct BufferObjects
        {
            GLuint  vbo, ebo;
            bool    dirty;
        public:
            inline BufferObjects(): vbo(0), ebo(0), dirty(true) { }
            inline operator bool() const { return vbo != 0 && ebo != 0; }
        };
        //////////////////////
        typedef xchg::Buffered<BufferObjects> PerContextBuffers;
        class UpdateCallback;
    private:
        const ClientBase::WPtr      _client;

        osg::BoundingBoxf           _precomputedBounds;
        BufferData                  _bufferData;

        mutable PerContextBuffers   _bufferObjects;

        gl::UniformInput::Ptr       _uScalelessModelView;
        gl::UniformInput::Ptr       _uModelViewScale;
        gl::UniformInput::Ptr       _uObjectStateFlags;
    public:
        static inline
        Ptr
        create(unsigned int id, ClientBase::WPtr const& client)
        {
            Ptr ptr(new Geometry(id, client));
            return ptr;
        }
    public:
        inline
        Geometry(unsigned int id, ClientBase::WPtr const&);
    private:
        // non-copyable
        Geometry(const Geometry&);
        Geometry& operator=(const Geometry&);
    public:
        inline
        ~Geometry();

        inline
        void
        dispose();

        inline
        void
        setSelected(bool);

        inline
        osg::BoundingBoxf
        computeBoundingBox() const;

        virtual inline
        void
        drawImplementation(osg::RenderInfo&) const;
    private:
        inline
        bool
        isProgramUpToDate() const;
        inline
        void
        loadProgram();
    private:
        static
        void
        buildBuffers(BufferData&);
        static
        void
        addPolylines(PolylinePlane, const float*, size_t, float, float, BufferData&);
    };

    ///////////////////////////////////////////////
    // class CameraLocator::Geometry::UpdateCallback
    ///////////////////////////////////////////////
    class CameraLocator::Geometry::UpdateCallback:
        public osg::Callback
    {
    public:
        virtual inline
        bool
        run(osg::Object* object, osg::Object* data)
        {
            Geometry* geo = reinterpret_cast<Geometry*>(object);
            if (geo)
                geo->loadProgram();
            return traverse(object, data);
        }
    };
}
}

using namespace smks;

//////////////////////////////////
// class CameraLocator::Projection
//////////////////////////////////
view::CameraLocator::Projection::Projection():
    osg::Projection()
{ }

void
view::CameraLocator::Projection::dispose()
{
    _matrix.makeIdentity();
}

void
view::CameraLocator::Projection::setPerspective(float yFieldOfViewD, float xAspectRatio, float zNear, float zFar)
{
    _matrix.makePerspective(yFieldOfViewD, xAspectRatio, zNear, zFar);
}

///////////////////////////////////
// class CameraLocator::Geometry
///////////////////////////////////
view::CameraLocator::Geometry::Geometry(unsigned int id, ClientBase::WPtr const& cl):
    osg::Geometry       (),
    _client             (cl),
    _precomputedBounds  (),
    _bufferData         (),
    _bufferObjects      (),
    _uScalelessModelView(gl::get_SceneObjLocatorProgram().createUniform(gl::uScalelessModelView())),
    _uModelViewScale    (gl::get_SceneObjLocatorProgram().createUniform(gl::uModelViewScale())),
    _uObjectStateFlags  (gl::get_SceneObjLocatorProgram().createUniform(gl::uObjectStateFlags(), gl::NO_OBJSTATE))
{
    setName("__camera.geometry");
    util::osg::setupRender(*this);

    osg::StateSet* stateSet = getOrCreateStateSet();
    stateSet->setDataVariance(osg::Object::DYNAMIC);    // prevents update phase to commence before all draw phases finish
    addUpdateCallback(new UpdateCallback());            // assign program at update phase

    osg::LineWidth* lineWidth = new osg::LineWidth(2.0f);
    stateSet->setAttributeAndModes(lineWidth, osg::StateAttribute::ON);

    osg::Depth* depth = new osg::Depth(osg::Depth::ALWAYS);
    stateSet->setAttributeAndModes(depth, osg::StateAttribute::ON);

    gl::UniformInput::Ptr const& uObjectId      = gl::get_SceneObjLocatorProgram().createUniform(gl::uObjectId(), id);
    gl::UniformInput::Ptr const& uColor         = gl::get_SceneObjLocatorProgram().createUniform(
        gl::uColor(), math::Vector4f(0.0f, 0.2745f, 0.098f, 1.0f).data(), osg::Object::STATIC);
    gl::UniformInput::Ptr const& uIsBillboard   = gl::get_SceneObjLocatorProgram().createUniform(
        gl::uIsBillboard(), false, osg::Object::STATIC);

    stateSet->addUniform(uObjectId->data(), osg::StateAttribute::OVERRIDE);
    stateSet->addUniform(_uObjectStateFlags->data());
    stateSet->addUniform(uIsBillboard->data());
    stateSet->addUniform(uColor->data());
    stateSet->addUniform(_uScalelessModelView->data());
    stateSet->addUniform(_uModelViewScale->data());

    buildBuffers(_bufferData);
    _precomputedBounds = util::osg::computeBounds(&_bufferData.positions3f.front(),
                                                  _bufferData.positions3f.size(),
                                                  3);
    util::osg::symmetrizeBounds(_precomputedBounds);
}

view::CameraLocator::Geometry::~Geometry()
{
    dispose();
}

void
view::CameraLocator::Geometry::dispose()
{
    ClientBase::Ptr const&  client      = _client.lock();
    sys::ResourceDatabase*  resources   = client ? client->getResourceDatabase() : nullptr;
    gl::ContextExtensions*  extensions  = resources ? gl::getContextExtensions(*resources) : nullptr;

    setNodeMask(util::osg::INVISIBLE_TO_ALL);

    _precomputedBounds.init();
    _bufferData.dirty();
    if (extensions)
        for (size_t contextId = 0; contextId < _bufferObjects.size(); ++contextId)
        {
            BufferObjects& buffers = _bufferObjects.get(contextId);
            if (buffers)
                extensions->getOrCreateGLExtension(contextId)->glDeleteBuffers(2, &buffers.vbo);
        }
}

void
view::CameraLocator::Geometry::setSelected(bool value)
{
    if (value)
        gl::add(*_uObjectStateFlags->data(), gl::OBJSTATE_SELECTED);
    else
        gl::remove(*_uObjectStateFlags->data(), gl::OBJSTATE_SELECTED);
}

osg::BoundingBoxf
view::CameraLocator::Geometry::computeBoundingBox() const
{
    return _precomputedBounds;
}

void
view::CameraLocator::Geometry::drawImplementation(osg::RenderInfo& renderInfo) const
{
    if (!isProgramUpToDate())
        return;

    if (!_bufferData)
        return;
    //---
    ClientBase::Ptr const&          client      = _client.lock();
    const sys::ResourceDatabase*    resources   = client ? client->getResourceDatabase() : nullptr;
    const gl::ContextExtensions*    extensions  = resources ? gl::getContextExtensions(*resources) : nullptr;
    if (!extensions)
        return;

    assert(renderInfo.getState());
    osg::State&                 state       = *renderInfo.getState();
    const unsigned int          contextId   = state.getContextID();
    BufferObjects&              buffers     = _bufferObjects.get(contextId);
    const osg::GLExtensions*    ext         = const_cast<gl::ContextExtensions*>(extensions)
        ->getOrCreateGLExtension(contextId);

    // decompose modelview in order to better handle scaling in vertex shader
    {
        math::Affine3f modelView, scalelessModelView;
        math::Vector4f modelViewScale;

        util::osg::convert(state.getModelViewMatrix(),  modelView);
        math::removeScaling(modelView, scalelessModelView, &modelViewScale);
        view::gl::setUniformfv(*_uModelViewScale    ->data(),   modelViewScale.data());
        view::gl::setUniformfv(*_uScalelessModelView->data(),   scalelessModelView.data());
    }

    assert(ext);

    const bool      generateBuffers = !buffers;
    const GLuint    locPosition     = state.getVertexAlias()._location;

    if (generateBuffers)
        ext->glGenBuffers(2, &buffers.vbo);

    ext->glBindBuffer(GL_ARRAY_BUFFER,          buffers.vbo);
    ext->glBindBuffer(GL_ELEMENT_ARRAY_BUFFER,  buffers.ebo);

    if (generateBuffers)
        ext->glVertexAttribPointer(locPosition, 3, GL_FLOAT, false, 3 * sizeof(float), 0);

    if (buffers.dirty)
    {
        ext->glBufferData(
            GL_ARRAY_BUFFER,
            _bufferData.positions3f.size() * sizeof(float),
            !_bufferData.positions3f.empty() ? &_bufferData.positions3f.front() : nullptr,
            GL_DYNAMIC_DRAW);
        ext->glBufferData(
            GL_ELEMENT_ARRAY_BUFFER,
            _bufferData.lines.size() * sizeof(unsigned int),
            !_bufferData.lines.empty() ? &_bufferData.lines.front() : nullptr,
            GL_DYNAMIC_DRAW);
        buffers.dirty = true;
    }

    ext->glEnableVertexAttribArray(locPosition);
    //---------------------------------------------------------------------
    glDrawElements(GL_LINES, _bufferData.lines.size(), GL_UNSIGNED_INT, 0);
    //---------------------------------------------------------------------
    ext->glDisableVertexAttribArray(locPosition);
    ext->glBindBuffer(GL_ARRAY_BUFFER,          0);
    ext->glBindBuffer(GL_ELEMENT_ARRAY_BUFFER,  0);
}

bool
view::CameraLocator::Geometry::isProgramUpToDate() const
{
    return getStateSet() &&
        getStateSet()->getAttribute(osg::StateAttribute::PROGRAM);
}

void
view::CameraLocator::Geometry::loadProgram()
{
    if (isProgramUpToDate())
        return;

    ClientBase::Ptr const&              client      = _client.lock();
    sys::ResourceDatabase::Ptr const&   resources   = client ? client->getOrCreateResourceDatabase() : nullptr;
    if (!resources)
        return;

    gl::getOrCreateContextExtensions(*resources);
    osg::ref_ptr<osg::Program> const& program = gl::getOrCreateProgram(
        gl::get_SceneObjLocatorProgram(), resources);
    if (!program)
        return;

    assert(getStateSet());
    getStateSet()->setAttribute(program.get(), osg::StateAttribute::OVERRIDE);
}


// static
void
view::CameraLocator::Geometry::buildBuffers(BufferData& ret)
{
    ret.positions3f .clear(); ret.positions3f   .reserve(240);
    ret.lines       .clear(); ret.lines         .reserve(224);
    {
        const float zy[] = {
            1.35f, -.25f,   1.35f, .35f,    .45f, .35f,
            .45f, -.25f,    1.35f, -.25f
        };
        addPolylines(Geometry::ZY, zy, sizeof(zy) / sizeof(float), -0.15f, 0.15f, ret);
    }
    {
        const float zy[] = {
            1.15f, .2f,     .6f, .2f,   .6f, -.25f,
            1.25f, -.25f,   1.15f, .2f
        };
        addPolylines(Geometry::ZY, zy, sizeof(zy) / sizeof(float), 0.25f, 0.15f, ret);
    }
    {
        const float zy[] = {
            1.35f, -.2f,    1.475f, -.223f, 1.60f, -.17f,
            1.70f, -.06f,   1.75f, .075f,   1.73f, .23f,
            1.65f, .34f,    1.52f, .416f,   1.32f, .434f,
            1.25f, .6f,     1.17f, .7f,     1.035f, .765f,
            0.9f, .765f,    0.77f, .7f,     0.68f, .6f,
            0.63f, .48f,    0.64f, .35f
        };
        addPolylines(Geometry::ZY, zy, sizeof(zy) / sizeof(float), -0.15f, 0.15f, ret);
    }
    {
        const float xy[] = {
            -.15f, 0.0f,    -.1f, .1f,  0.0f, .15f,
            .1f, .1f,       .15f, 0.0f, .1f, -.1f,
            0.0f, -.15f,    -.1f, -.1f, -.15f, 0.0f
        };
        addPolylines(Geometry::XY, xy, sizeof(xy) / sizeof(float), 0.15f, 0.45f, ret);
    }
    {
        assert(ret.positions3f.size() % 3 == 0);
        const size_t vStart = ret.positions3f.size() / 3;
        for (size_t i = 0; i < 4; ++i)
        {
            ret.positions3f.push_back(i & 1 ? -.15f : .15f);
            ret.positions3f.push_back(i & 2 ? -.15f : .15f);
            ret.positions3f.push_back(.15f);

            ret.positions3f.push_back(i & 1 ? -.23f : .23f);
            ret.positions3f.push_back(i & 2 ? -.16f : .15f);
            ret.positions3f.push_back(0.0f);
        }
        const unsigned int lines[] = {
            0, 1, 2, 3, 4, 5, 6, 7,
            0, 2, 2, 6, 6, 4, 4, 0,
            1, 3, 3, 7, 7, 5, 5, 1 };
        for (size_t i = 0; i < sizeof(lines) / sizeof(unsigned int); ++i)
            ret.lines.push_back(vStart + lines[i]);
    }
}

void
view::CameraLocator::Geometry::addPolylines(PolylinePlane   plane,
                                            const float*    uv,
                                            size_t          size,
                                            float           wmin,
                                            float           wmax,
                                            BufferData&     ret)
{
    assert(ret.positions3f.size() % 3 == 0);
    assert(size % 2 == 0);

    size_t          numVertices = ret.positions3f.size() / 3;
    const size_t    N           = size >> 1;
    assert(N > 1);

    const float w[] = { wmin, wmax };
    for (size_t n = 0, i = 0; n < N; ++n, i+=2)
    {
        const float u   = uv[i];
        const float v   = uv[i+1];
        float xyz[3]    = { 0.0f, 0.0f, 0.0f };

        for (size_t j = 0; j < 2; ++j)
        {
            ret.lines.push_back(numVertices);
            switch (plane)
            {
            default:
            case Geometry::ZY: xyz[0] = w[j];   xyz[1] = v; xyz[2] = u;     break;
            case Geometry::XY: xyz[0] = u;      xyz[1] = v; xyz[2] = w[j];  break;
            }
            for (size_t k = 0; k < 3; ++k)
                ret.positions3f.push_back(xyz[k]);
            ++numVertices;
        }

        if (n < 1)
            continue;
        ret.lines.push_back(numVertices-1);
        ret.lines.push_back(numVertices-3);

        ret.lines.push_back(numVertices-2);
        ret.lines.push_back(numVertices-4);
    }
}

/////////////////////////
// class CameraLocator
/////////////////////////
view::CameraLocator::CameraLocator(unsigned int id, ClientBase::WPtr const& cl):
    osg::Group          (),
    _projection         (Projection::create()),
    _geometry           (Geometry::create(id, cl))
{
    ClientBase::Ptr const& client = cl.lock();
    if (!client)
        throw sys::Exception(
            "Invalid client.",
            "Camera Projection Construction");

    const std::string name (client->getNodeName(id));

    addChild(_geometry.get());
    addChild(_projection.get());

    _projection->setName(name);
    setName("__group[" + name + "]");
}

view::CameraLocator::~CameraLocator()
{
    dispose();
}

void
view::CameraLocator::dispose()
{
    assert(_projection);
    setNodeMask(util::osg::INVISIBLE_TO_ALL);
    _projection->dispose();
}

void
view::CameraLocator::setPerspective(float yFieldOfViewD, float xAspectRatio, float zNear, float zFar)
{
    assert(_projection);
    _projection->setPerspective(yFieldOfViewD, xAspectRatio, zNear, zFar);
    FLIRT_LOG(LOG(DEBUG)
        << "\n- vertical field of view (D) = " << yFieldOfViewD
        << "\n- horizontal aspect ratio = " << xAspectRatio
        << "\n- z in [" << zNear << ", " << zFar << "]"
        << "\n-> projection matrix ('" << getName()  << "') = \n" << std::to_string(_projection->getMatrix());)
}

void
view::CameraLocator::setSelected(bool value)
{
    if (_geometry)
        _geometry->setSelected(value);
}

osg::Projection*
view::CameraLocator::projection()
{
    return _projection.get();
}

const osg::Projection*
view::CameraLocator::projection() const
{
    return _projection.get();
}
