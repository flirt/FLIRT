// ========================================================================= //
// Copyright 2016-2018 SUPAMONKS_STUDIO                                      //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
//                                                                           //
// ========================================================================= //
#pragma once

#include <memory>
#include <osg/Group>

namespace smks
{
    class ClientBase;
    namespace view
    {
        class RenderActionGroup:
            public osg::Group
        {
            friend class ProgressiveCallback;
        public:
            typedef osg::ref_ptr<RenderActionGroup> Ptr;
        private:
            class ProgressiveCallback;
        private:
            typedef std::weak_ptr<ClientBase>           ClientWPtr;
            typedef osg::ref_ptr<ProgressiveCallback>   CallbackPtr;
        private:
            const unsigned int  _scnId;
            const ClientWPtr    _client;
            CallbackPtr         _updateCallback;

        public:
            static
            Ptr
            create(unsigned int id, ClientWPtr const& client);

        private:
            RenderActionGroup(unsigned int, ClientWPtr const&);
            // non-copyable
            RenderActionGroup(const RenderActionGroup&);
            RenderActionGroup& operator=(const RenderActionGroup&);

        public:
            ~RenderActionGroup();

            inline
            void
            dispose()
            { }

            void
            isProgressive(bool);

        private:
            void
            renderIteration();
        };
    }
}
