// ========================================================================= //
// Copyright 2016-2018 SUPAMONKS_STUDIO                                      //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
//                                                                           //
// ========================================================================= //
#pragma once

#include <memory>
#include <osg/observer_ptr>
#include <osg/StateSet>
#include <smks/xchg/NodeClass.hpp>

namespace smks
{
    class ClientBase;
    namespace scn
    {
        class TreeNode;
    }

    namespace view
    {
        class StateAttributeProvider;

        namespace gl
        {
            class MaterialProgramBase;
            class ProgramInput;
        };

        class GeometryDisplayHelper
        {
        public:
            typedef std::shared_ptr<GeometryDisplayHelper>  Ptr;
            typedef std::weak_ptr<GeometryDisplayHelper>    WPtr;
        private:
            typedef std::weak_ptr<ClientBase>                   ClientWPtr;
            typedef osg::observer_ptr<StateAttributeProvider>   ProviderWPtr;
            typedef osg::observer_ptr<osg::StateSet>            StateSetWPtr;
        private:
            enum ProviderType
            {
                UNSPECIFIED_PROVIDER    = -1,
                PROVIDER_MATERIAL       = 0,
                PROVIDER_SUBSURFACE,
                NUM_PROVIDERS,
            };
        private:
            const ClientWPtr                _client;
            const unsigned int              _scnId;
            WPtr                            _self;

            ProviderWPtr                    _providers[NUM_PROVIDERS];
            const gl::MaterialProgramBase*  _program;   //<! does not own it
            StateSetWPtr                    _stateSet;
        public:
            static
            Ptr
            create(unsigned int, ClientWPtr const&, osg::StateSet*);

        protected:
            GeometryDisplayHelper(unsigned int, ClientWPtr const&, osg::StateSet*);
        private:
            // non-copyable
            GeometryDisplayHelper(const GeometryDisplayHelper&);
            GeometryDisplayHelper& operator=(const GeometryDisplayHelper&);

        protected:
            void
            build(Ptr const&);

        public:
            inline
            unsigned int
            scnId() const
            {
                return _scnId;
            }
        private:
            const gl::MaterialProgramBase*
            selectProgram() const;

            //<! throws exception if program cannot be selected
            static
            const gl::MaterialProgramBase*
            selectProgram(xchg::NodeClass, const std::string*);

            void
            setProgram(const gl::MaterialProgramBase*);

        public:
            void
            setProvider(unsigned int linkId, StateAttributeProvider*);

            void
            refreshProgram();

            inline
            const gl::MaterialProgramBase*
            program() const
            {
                return _program;
            }

            void
            handleProviderInputAppears(unsigned int providerId, unsigned int inId, gl::ProgramInput*);
            void
            handleProviderInputDisappears(unsigned int providerId, unsigned int inId, gl::ProgramInput*);

        private:
            void
            addInputToStateSet(gl::ProgramInput*);
            void
            removeInputFromStateSet(gl::ProgramInput*);
        };
    }
}
