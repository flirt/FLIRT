// ========================================================================= //
// Copyright 2016-2018 SUPAMONKS_STUDIO                                      //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
//                                                                           //
// ========================================================================= //
#pragma once

#include <memory>
#include <boost/unordered_map.hpp>
#include <osg/StateSet>

#include <smks/xchg/ParmEvent.hpp>
#include <smks/math/types.hpp>
#include <smks/xchg/IdSet.hpp>

namespace smks
{
    class ClientBase;
    namespace view
    {
        namespace gl
        {
            class ProgramInput;
            class MaterialProgramBase;
        }
        class GeometryDisplayHelper;
        class TextureImage;

        class StateAttributeProvider:
            public osg::Object
        {
        public:
            typedef osg::ref_ptr<StateAttributeProvider>        Ptr;
            typedef osg::observer_ptr<StateAttributeProvider>   WPtr;
        private:
            typedef std::weak_ptr<ClientBase>                           ClientWPtr;
            typedef std::shared_ptr<GeometryDisplayHelper>              HelperPtr;
            typedef std::weak_ptr<GeometryDisplayHelper>                HelperWPtr;
            typedef std::shared_ptr<gl::ProgramInput>                   ProgramInputPtr;
            typedef boost::unordered_map<unsigned int, HelperWPtr>      Helpers;
            typedef boost::unordered_map<unsigned int, ProgramInputPtr> ProgramInputs;  // input ID -> input
            typedef boost::unordered_map<unsigned int, ProgramInputs>   ProgramInputMap;

        private:
            const unsigned int  _scnId;             //<! material/subsurface ID
            const ClientWPtr    _client;

            Helpers             _helpers;           //<! geometry ID    -> helper
            ProgramInputMap     _progInputs;        //<! program ID     -> program inputs

        public:
            static inline
            Ptr
            create(unsigned int id, ClientWPtr const& client)
            {
                Ptr ptr (new StateAttributeProvider(id, client));
                return ptr;
            }

        private:
            StateAttributeProvider(unsigned int, ClientWPtr const&);
            // non-copyable
            StateAttributeProvider(const StateAttributeProvider&);
            StateAttributeProvider& operator=(const StateAttributeProvider&);

        public:
            inline osg::Object* cloneType()                         const { throw; }
            inline osg::Object* clone(const osg::CopyOp& copyop)    const { throw; }
            inline const char*  libraryName()                       const { return "SmksVientClient"; }
            inline const char*  className()                         const { return "StateAttributeProvider"; }

            inline
            unsigned int
            scnId() const
            {
                return _scnId;
            }

        public:
            void
            dispose();

            void
            getCode(std::string&) const;

            ProgramInputPtr
            getInput(unsigned int progId, unsigned int inId) const;

            void
            handleCodeChanged();

            bool
            handleParmChanged(unsigned int parmId, xchg::ParmEvent::Type);

            void
            registerDisplayHelper(HelperPtr const&);

            void
            deregisterDisplayHelper(HelperPtr const&);

            void
            handleDisplayHelperProgramChanged(unsigned int                      helperId,
                                              const gl::MaterialProgramBase*    newProgram,
                                              const gl::MaterialProgramBase*    prvProgram);

            void
            setTextureImage(unsigned int parmId, TextureImage*);

        private:
            bool
            areProgramInputsUsed(unsigned int progId) const;

            std::shared_ptr<gl::ProgramInput>
            getOrCreateInput(unsigned int progId, unsigned int inId); // will notify helpers

            bool
            updateInputFromParm(gl::ProgramInput&, unsigned int parmId);

            void
            destroyInput(unsigned int progId, unsigned int inId); // will notify display helpers

            void
            addProgramInputsToHelper(HelperPtr const&, unsigned int progId);

            void
            removeProgramInputsFromHelper(HelperPtr const&, unsigned int progId);
        };
    }
}
