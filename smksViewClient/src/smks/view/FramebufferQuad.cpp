// ========================================================================= //
// Copyright 2016-2018 SUPAMONKS_STUDIO                                      //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
//                                                                           //
// ========================================================================= //
#include <osg/Uniform>
#include <osg/Texture2D>
#include <osg/Geometry>
#include <osg/Image>
#include <osg/MatrixTransform>

#include <smks/xchg/Data.hpp>
#include <smks/ClientBase.hpp>
#include <smks/view/gl/ProgramTemplate.hpp>
#include <smks/view/gl/ProgramTemplates.hpp>
#include <smks/view/gl/ProgramInput.hpp>
#include <smks/view/gl/ProgramInputDeclarations.hpp>

#include <smks/util/osg/texture.hpp>

#include "FrameBufferQuad.hpp"

#include <osg/Depth>

namespace smks { namespace view
{
    struct FrameBufferQuad::DrawableCallback:
        public osg::Drawable::DrawCallback
    {
    private:
        FrameBufferQuad::WPtr _self;
    public:
        explicit inline
        DrawableCallback(FrameBufferQuad* self):
            _self(self)
        { }

        virtual inline
        void
        drawImplementation(osg::RenderInfo& renderInfo, const osg::Drawable* drawable) const
        {
            FrameBufferQuad::Ptr self = nullptr;
            if (_self.lock(self))
                self->updateImage();
            drawable->drawImplementation(renderInfo);
        }
    };
}
}

using namespace smks;

// static
view::FrameBufferQuad::Ptr
view::FrameBufferQuad::create(unsigned int              id,
                              ClientBase::WPtr const&   client,
                              osg::GraphicsContext*     context,
                              unsigned int              textureUnit)
{
    Ptr ptr = new FrameBufferQuad(id, client, context, textureUnit);
    return ptr;
}

view::FrameBufferQuad::FrameBufferQuad(unsigned int             id,
                                       ClientBase::WPtr const&  cl,
                                       osg::GraphicsContext*    context,
                                       unsigned int             textureUnit):
    util::osg::ScreenQuadCamera (textureUnit),
    _image                      (new osg::Image()),
    _texture2D                  (util::osg::createRGBATexture(true, osg::Texture::NEAREST, osg::Texture::NEAREST)),
    _uRcpTextureSize            (gl::get_TextureProgram().createUniform(gl::uRecTextureSize())),
    //---
    _dirty          (false),
    _textureWidth   (0),
    _textureHeight  (0),
    _textureData    ()
{
    ClientBase::Ptr const& client = cl.lock();

    if (!client)
        throw sys::Exception("Invalid client.", "FrameBuffer Quad Construction");

    _texture2D->setName(std::string(client->getNodeName(id)) + ".texture");
    _texture2D->setDataVariance(Object::DYNAMIC);
    _texture2D->setImage(_image.get());

    // set up the screen quad's shading
    osg::ref_ptr<osg::Program> program = client
        ? gl::getOrCreateProgram(gl::get_TextureProgram(), client->getOrCreateResourceDatabase())
        : nullptr;
    if (!program.valid())
        throw sys::Exception("Failed to load texture shading program.", "FrameBuffer Quad Construction");

    gl::UniformInput::Ptr uDiffuseTexture   = gl::get_TextureProgram().createUniform(gl::uDiffuseTexture(), static_cast<int>(textureUnit));
    gl::UniformInput::Ptr uAntiAlias        = gl::get_TextureProgram().createUniform(gl::uAntiAlias(), false);

    quad()->getOrCreateStateSet()->setAttribute(program.get(), osg::StateAttribute::ON);
    quad()->getOrCreateStateSet()->addUniform(uDiffuseTexture   ->data());
    quad()->getOrCreateStateSet()->addUniform(uAntiAlias        ->data());
    quad()->getOrCreateStateSet()->addUniform(_uRcpTextureSize  ->data());
    quad()->getOrCreateStateSet()->setTextureAttributeAndModes(textureUnit, _texture2D.get(), osg::StateAttribute::ON);
    quad()->setDrawCallback(new DrawableCallback(this));

    if (context == nullptr)
        throw sys::Exception("Invalid graphics context.", "FrameBuffer Quad Construction");
    setGraphicsContext(context);
    setRenderOrder(POST_RENDER);    // after the MRT rendering
    //setClearMask(0); // clear nothing
    setClearColor(osg::Vec4f(1.0f, 0.0f, 0.0f, 1.0f));

    handleResize(context->getTraits()->x,
                 context->getTraits()->y,
                 context->getTraits()->width,
                 context->getTraits()->height);
}

view::FrameBufferQuad::~FrameBufferQuad()
{
    dispose();
}

void
view::FrameBufferQuad::dispose()
{
    textureWidth    (0);
    textureHeight   (0);
    textureData     (nullptr);
}

void
view::FrameBufferQuad::textureWidth(size_t value)
{
    _dirty          = _textureWidth != value;
    _textureWidth   = value;
}

void
view::FrameBufferQuad::textureHeight(size_t value)
{
    _dirty          = _textureHeight != value;
    _textureHeight  = value;
}

void
view::FrameBufferQuad::textureData(xchg::Data::Ptr const& value)
{
    xchg::Data::Ptr prv = _textureData.lock();

    _dirty          = prv == nullptr || !xchg::equal(prv, value);
    _textureData    = value;
}

void
view::FrameBufferQuad::updateImage()
{
    if (_dirty)
    {
        xchg::Data::Ptr const& data = _textureData.lock();
        if (data && data->type() != typeid(float))
            throw sys::Exception("Image data must be of floating type.", "Frame Buffer Quad Image Update");

        float* ptr = data
            ? const_cast<float*>(data->getPtr<float>())
            : nullptr;

        _image->setImage(_textureWidth, _textureHeight, 0, GL_RGBA, GL_RGBA, GL_FLOAT, reinterpret_cast<unsigned char*>(ptr), osg::Image::NO_DELETE);
        _texture2D->setTextureSize(_textureWidth, _textureHeight);

        const float rTexSize[2] = {
            _textureWidth > 0   ? 1.0f / static_cast<float>(_textureWidth)  : 0.0f,
            _textureHeight > 0  ? 1.0f / static_cast<float>(_textureHeight) : 0.0f
        };
        gl::setUniformfv(*_uRcpTextureSize->data(), rTexSize);
    }
    _dirty = false;
}
