// ========================================================================= //
// Copyright 2016-2018 SUPAMONKS_STUDIO                                      //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
//                                                                           //
// ========================================================================= //
#pragma once

#include <memory>
#include <osg/Group>

namespace smks { namespace view
{
    class Client;

    // This class must be exposed because it is the only OpenSceneGraph object
    // that will be manipulated by OSG handlers and callbacks. It must be completely
    // opaque though and will only allow for the access to the public Client class.

    class SceneGroup:
        public osg::Group
    {
    public:
        typedef osg::ref_ptr<SceneGroup> Ptr;
    private:
        typedef std::weak_ptr<ClientBase> ClientWPtr;
    public:
        static inline
        Ptr
        create(unsigned int id, ClientWPtr const& client)
        {
            Ptr ptr(new SceneGroup(id, client));
            return ptr;
        }

    private:
        inline
        SceneGroup(unsigned int id, ClientWPtr const&):
            osg::Group()
        {
            getOrCreateStateSet()->setMode(GL_CULL_FACE, osg::StateAttribute::ON);
        }

        inline
        ~SceneGroup()
        { }

        // non-copyable
        SceneGroup(const SceneGroup&);
        SceneGroup& operator=(const SceneGroup&);

    public:
        inline
        void
        dispose()
        { }
    };
}
}
