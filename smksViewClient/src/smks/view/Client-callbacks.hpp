// ========================================================================= //
// Copyright 2016-2018 SUPAMONKS_STUDIO                                      //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
//                                                                           //
// ========================================================================= //
#pragma once

#include <osgViewer/Viewer>

#include <smks/xchg/GUIEvents.hpp>
#include <smks/xchg/AbstractGUIEventCallback.hpp>
#include <smks/util/osg/KeyMap.hpp>

#include "smks/view/Client.hpp"

namespace smks { namespace view
{
    static inline
    unsigned int
    getModifiersOsg(const xchg::GUIInputEvent&);

    static inline
    unsigned int
    getButtonOsg(const xchg::MouseEvent&);

    static inline
    int
    getKeyOsg(const xchg::KeyEvent&);

    ////////////////////////////
    // struct MouseEventCallback
    ////////////////////////////
    struct Client::MouseEventCallback:
        public xchg::MouseEventCallback
    {
    protected:
        Client::WPtr _client;
    protected:
        MouseEventCallback(Client::Ptr const& client):
            xchg::MouseEventCallback(),
            _client(client)
        { }
    public:
        virtual
        void
        operator()(const xchg::MouseEvent&, osgGA::EventQueue&) = 0;

        inline
        void
        operator()(const xchg::MouseEvent& evt)
        {
            Client::Ptr const&  client = _client.lock();
            osgViewer::Viewer*  viewer = client ? client->device() : nullptr;
            osgGA::EventQueue*  events = viewer ? viewer->getEventQueue() : nullptr;
            if (events)
                this->operator()(evt, *events);
        }
    };

    /////////////////////////////////
    // struct MousePressEventCallback
    /////////////////////////////////
    struct Client::MousePressEventCallback:
        public Client::MouseEventCallback
    {
    public:
        typedef std::shared_ptr<MousePressEventCallback> Ptr;
    public:
        static inline
        Ptr
        create(Client::Ptr const& client)
        {
            Ptr ptr(new MousePressEventCallback(client));
            return ptr;
        }
    protected:
        explicit
        MousePressEventCallback(Client::Ptr const& client):
            MouseEventCallback(client)
        { }
    protected:
        inline
        void
        operator()(const xchg::MouseEvent& evt, osgGA::EventQueue& events)
        {
            // impossible to change openscene graph's Y mouse direction directly
            const int height = events.getCurrentEventState()->getWindowHeight();

            events.getCurrentEventState()->setModKeyMask(getModifiersOsg(evt));
            events.mouseButtonPress(static_cast<float>(evt.x()),
                                    static_cast<float>(height - evt.y()),
                                    getButtonOsg(evt));
        }
    };

    ////////////////////////////////
    // struct MouseMoveEventCallback
    ////////////////////////////////
    struct Client::MouseMoveEventCallback:
        public Client::MouseEventCallback
    {
    public:
        typedef std::shared_ptr<MouseMoveEventCallback> Ptr;
    public:
        static inline
        Ptr
        create(Client::Ptr const& client)
        {
            Ptr ptr(new MouseMoveEventCallback(client));
            return ptr;
        }
    protected:
        explicit
        MouseMoveEventCallback(Client::Ptr const& client):
            MouseEventCallback(client)
        { }
    protected:
        inline
        void
        operator()(const xchg::MouseEvent& evt, osgGA::EventQueue& events)
        {
            // impossible to change openscene graph's Y mouse direction directly
            const int height = events.getCurrentEventState()->getWindowHeight();

            events.getCurrentEventState()->setModKeyMask(getModifiersOsg(evt));
            events.mouseMotion(static_cast<float>(evt.x()),
                               static_cast<float>(height - evt.y()));
        }
    };

    ///////////////////////////////////
    // struct MouseReleaseEventCallback
    ///////////////////////////////////
    struct Client::MouseReleaseEventCallback:
        public Client::MouseEventCallback
    {
    public:
        typedef std::shared_ptr<MouseReleaseEventCallback> Ptr;
    public:
        static inline
        Ptr
        create(Client::Ptr const& client)
        {
            Ptr ptr(new MouseReleaseEventCallback(client));
            return ptr;
        }
    protected:
        explicit
        MouseReleaseEventCallback(Client::Ptr const& client):
            MouseEventCallback(client)
        { }
    protected:
        inline
        void
        operator()(const xchg::MouseEvent& evt, osgGA::EventQueue& events)
        {
            // impossible to change openscene graph's Y mouse direction directly
            const int height = events.getCurrentEventState()->getWindowHeight();

            events.getCurrentEventState()->setModKeyMask(getModifiersOsg(evt));
            events.mouseButtonRelease(static_cast<float>(evt.x()),
                                      static_cast<float>(height - evt.y()),
                                      getButtonOsg(evt));
        }
    };

    ////////////////////////////
    // struct WheelEventCallback
    ////////////////////////////
    struct Client::WheelEventCallback:
        public xchg::WheelEventCallback
    {
    public:
        typedef std::shared_ptr<WheelEventCallback> Ptr;
    protected:
        Client::WPtr _client;
    public:
        static inline
        Ptr
        create(Client::Ptr const& client)
        {
            Ptr ptr (new WheelEventCallback(client));
            return ptr;
        }
    protected:
        explicit
        WheelEventCallback(Client::Ptr const& client):
            xchg::WheelEventCallback(),
            _client                 (client)
        { }
    public:
        inline
        void
        operator()(const xchg::WheelEvent& evt)
        {
            Client::Ptr const&  client = _client.lock();
            osgViewer::Viewer*  viewer = client ? client->device() : nullptr;
            osgGA::EventQueue*  events = viewer ? viewer->getEventQueue() : nullptr;
            if (events)
            {
                events->getCurrentEventState()->setModKeyMask(getModifiersOsg(evt));
                events->mouseScroll(evt.delta() > 0
                    ? osgGA::GUIEventAdapter::SCROLL_UP
                    : osgGA::GUIEventAdapter::SCROLL_DOWN);
            }
        }
    };

    //////////////////////////
    // struct KeyEventCallback
    //////////////////////////
    struct Client::KeyEventCallback:
        public xchg::KeyEventCallback
    {
    protected:
        Client::WPtr _client;
    protected:
        explicit
        KeyEventCallback(Client::Ptr const& client):
            xchg::KeyEventCallback(),
            _client(client)
        { }
    public:
        virtual
        void
        operator()(const xchg::KeyEvent&, osgGA::EventQueue&) = 0;

        inline
        void
        operator()(const xchg::KeyEvent& evt)
        {
            Client::Ptr const&  client = _client.lock();
            osgViewer::Viewer*  viewer = client ? client->device() : nullptr;
            osgGA::EventQueue*  events = viewer ? viewer->getEventQueue() : nullptr;
            if (events)
                this->operator()(evt, *events);
        }
    protected:

    };

    ///////////////////////////////////
    // struct KeyPressEventCallback
    ///////////////////////////////////
    struct Client::KeyPressEventCallback:
        public Client::KeyEventCallback
    {
    public:
        typedef std::shared_ptr<KeyPressEventCallback> Ptr;
    public:
        static inline
        Ptr
        create(Client::Ptr const& client)
        {
            Ptr ptr(new KeyPressEventCallback(client));
            return ptr;
        }
    protected:
        explicit
        KeyPressEventCallback(Client::Ptr const& client):
            KeyEventCallback(client)
        { }
    protected:
        inline
        void
        operator()(const xchg::KeyEvent& evt, osgGA::EventQueue& events)
        {
            const int key = getKeyOsg(evt);

            events.getCurrentEventState()->setModKeyMask(getModifiersOsg(evt));
            events.keyPress(key, key);
        }
    };

    ///////////////////////////////////
    // struct KeyReleaseEventCallback
    ///////////////////////////////////
    struct Client::KeyReleaseEventCallback:
        public Client::KeyEventCallback
    {
    public:
        typedef std::shared_ptr<KeyReleaseEventCallback> Ptr;
    public:
        static inline
        Ptr
        create(Client::Ptr const& client)
        {
            Ptr ptr(new KeyReleaseEventCallback(client));
            return ptr;
        }
    protected:
        explicit
        KeyReleaseEventCallback(Client::Ptr const& client):
            KeyEventCallback(client)
        { }
    protected:
        inline
        void
        operator()(const xchg::KeyEvent& evt, osgGA::EventQueue& events)
        {
            const int key = getKeyOsg(evt);

            events.getCurrentEventState()->setModKeyMask(getModifiersOsg(evt));
            events.keyRelease(key, key);
        }
    };

    /////////////////////////////
    // struct ResizeEventCallback
    /////////////////////////////
    struct Client::ResizeEventCallback:
        public xchg::ResizeEventCallback
    {
    public:
        typedef std::shared_ptr<ResizeEventCallback> Ptr;
    private:
        Client::WPtr _client;
    public:
        static inline
        Ptr
        create(Client::Ptr const& client)
        {
            Ptr ptr(new ResizeEventCallback(client));
            return ptr;
        }
    protected:
        explicit
        ResizeEventCallback(Client::Ptr const& client):
            xchg::ResizeEventCallback   (),
            _client                     (client)
        { }
    public:
        inline
        void
        operator()(const xchg::ResizeEvent& evt)
        {
            Client::Ptr const&          client          = _client.lock();
            osg::Camera*                camera          = client && client->device() ? client->device()->getCamera() : nullptr;
            osgViewer::GraphicsWindow*  graphicsWindow  = camera ? dynamic_cast<osgViewer::GraphicsWindow*>(camera->getGraphicsContext()) : nullptr;
            osgGA::EventQueue*          events          = graphicsWindow ? graphicsWindow->getEventQueue() : nullptr;
            if (events)
            {
                const osg::GraphicsContext::Traits* traits = graphicsWindow->getTraits();

                events->windowResize(traits ? traits->x : 0,
                                     traits ? traits->y : 0,
                                     evt.width(),
                                     evt.height());
            }
        }
    };


    //////////////////////////////
    // struct PaintGLEventCallback
    //////////////////////////////
    struct Client::PaintGLEventCallback:
        public xchg::PaintGLEventCallback
    {
    public:
        typedef std::shared_ptr<PaintGLEventCallback> Ptr;
    private:
        Client::WPtr _client;
    public:
        static inline
        Ptr
        create(Client::Ptr const& client)
        {
            Ptr ptr(new PaintGLEventCallback(client));
            return ptr;
        }
    protected:
        explicit
        PaintGLEventCallback(Client::Ptr const& client):
            xchg::PaintGLEventCallback  (),
            _client                     (client)
        { }
    public:
        inline
        void
        operator()(const xchg::PaintGLEvent& evt)
        {
            Client::Ptr const& client = _client.lock();
            if (client &&
                client->device())
                client->device()->frame();
        }
    };

    //----------------------------------------------------------
    static inline
    unsigned int
    getModifiersOsg(const xchg::GUIInputEvent& evt)
    {
        unsigned int mods = 0;

        if (evt.keyboardModifiers() & xchg::SHIFT_MODIFIER)
            mods |= osgGA::GUIEventAdapter::MODKEY_SHIFT;
        if (evt.keyboardModifiers() & xchg::CTRL_MODIFIER)
            mods |= osgGA::GUIEventAdapter::MODKEY_CTRL;
        if (evt.keyboardModifiers() & xchg::ALT_MODIFIER)
            mods |= osgGA::GUIEventAdapter::MODKEY_ALT;

        return mods;
    }

    static inline
    unsigned int
    getButtonOsg(const xchg::MouseEvent& evt)
    {
        switch (evt.button())
        {
        case xchg::LEFT_BUTTON:     return 1;
        case xchg::RIGHT_BUTTON:    return 3;
        case xchg::MIDDLE_BUTTON:   return 2;
        default:                    return 0;
        }
    }

    static inline
    int
    getKeyOsg(const xchg::KeyEvent& evt)
    {
        int key = util::osg::keyMap().map(evt.key());
        if (key == 0 &&
            !evt.text().empty())
            key = *evt.text().c_str();
        return key;
    }
}
}
