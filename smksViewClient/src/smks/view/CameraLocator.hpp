// ========================================================================= //
// Copyright 2016-2018 SUPAMONKS_STUDIO                                      //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
//                                                                           //
// ========================================================================= //
#pragma once

#include <memory>
#include <osg/Group>

namespace smks
{
    class ClientBase;
    namespace view
    {
        namespace gl
        {
            class UniformInput;
        }
        class CameraLocator:
            public osg::Group
        {
        public:
            typedef osg::ref_ptr<CameraLocator> Ptr;
        private:
            class Projection;
            class Geometry;
        private:
            typedef std::shared_ptr<ClientBase>         ClientPtr;
            typedef std::weak_ptr<ClientBase>           ClientWPtr;
            typedef osg::ref_ptr<Projection>            ProjectionPtr;
            typedef osg::ref_ptr<Geometry>              GeometryPtr;
            typedef std::shared_ptr<gl::UniformInput>   UniformPtr;
        private:
            ProjectionPtr   _projection;
            GeometryPtr     _geometry;

        public:
            static inline
            Ptr
            create(unsigned int id, ClientWPtr const& client)
            {
                Ptr ptr(new CameraLocator(id, client));
                return ptr;
            }

        private:
            CameraLocator(unsigned int id, ClientWPtr const&);
            // non-copyable
            CameraLocator(const CameraLocator&);
            CameraLocator& operator=(const CameraLocator&);
        public:
            ~CameraLocator();

            void
            dispose();

            void
            setPerspective(float yFieldOfViewD, float xAspectRatio, float zNear, float zFar);

            void
            setSelected(bool);

            osg::Projection*
            projection();

            const osg::Projection*
            projection() const;
        };
    }
}

