// ========================================================================= //
// Copyright 2016-2018 SUPAMONKS_STUDIO                                      //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
//                                                                           //
// ========================================================================= //
#pragma once

#include <memory>
#include <osg/MatrixTransform>
#include <smks/math/types.hpp>

namespace smks
{
    class ClientBase;
    namespace view
    {
        class LightLocator:
            public osg::MatrixTransform
        {
        public:
            ////////////////////////
            // enum LightLocator::Type
            ////////////////////////
            enum Type
            {
                UNSPECIFIED_LIGHT_LOCATOR   = -1,
                POINT_LIGHT_LOCATOR         = 0,
                DIRECTIONAL_LIGHT_LOCATOR,
                AMBIENT_LIGHT_LOCATOR,
                DISTANT_LIGHT_LOCATOR,
                SPOT_LIGHT_LOCATOR,
                HDRI_LIGHT_LOCATOR,
                MESH_LIGHT_LOCATOR
            };
            ////////////////////////
        private:
            enum { NUM_CIRCLE_SEGMENTS = 32 };
        private:
            class Geometry;
        public:
            typedef osg::ref_ptr<LightLocator>      Ptr;
            typedef osg::observer_ptr<LightLocator> WPtr;
        private:
            typedef std::weak_ptr<ClientBase>       ClientWPtr;
            typedef osg::ref_ptr<Geometry>          GeometryPtr;
            typedef osg::ref_ptr<osg::Uniform>      UniformPtr;
        private:
            GeometryPtr     _geometry;
            //////////////////
            // local transform
            //////////////////
            math::Affine3f  _xform;

        public:
            static
            Ptr
            create(unsigned int id, ClientWPtr const&);

        private:
            LightLocator(unsigned int id, ClientWPtr const&);
            // non-copyable
            LightLocator(const LightLocator&);
            LightLocator& operator=(const LightLocator&);

        public:
            ~LightLocator();

            void
            dispose();

            void
            setCode(const std::string&);

            void
            setSelected(bool);

        private:
            static
            Type
            getType(const char* code);

        public:
            void
            setTransform(const math::Affine3f&);

        private:
            void
            commitMatrix();

        public:
            void
            setHalfAngleD(float);

            void
            setMinAngleD(float);

            void
            setMaxAngleD(float);
        };
    }
}
