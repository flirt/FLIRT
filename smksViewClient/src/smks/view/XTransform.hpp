// ========================================================================= //
// Copyright 2016-2018 SUPAMONKS_STUDIO                                      //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
//                                                                           //
// ========================================================================= //
#pragma once

#include <memory>
#include <osg/MatrixTransform>
#include <smks/math/types.hpp>
#include <smks/util/osg/NodeMask.hpp>

namespace smks
{
    class ClientBase;
    namespace view
    {
        class XTransform:
            public osg::MatrixTransform
        {
        public:
            typedef osg::ref_ptr<XTransform> Ptr;
        private:
            typedef std::weak_ptr<ClientBase> ClientWPtr;

        private:
            math::Affine3f  _xform;
            bool            _inheritsXforms;    //<! ignores ancestors' model transforms
            unsigned int    _currentCamera;     //<! needed to keep track of current view transform

        public:
            static inline
            Ptr
            create(unsigned int id, ClientWPtr const& client)
            {
                Ptr ptr (new XTransform(id, client));
                return ptr;
            }

        private:
            inline
            XTransform(unsigned int, ClientWPtr const&):
                osg::MatrixTransform(),
                _xform              (math::Affine3f::Identity()),
                _inheritsXforms     (true),
                _currentCamera      (0)
            { }
            // non-copyable
            XTransform(const XTransform&);
            XTransform& operator=(const XTransform&);

        public:
            inline
            void
            dispose()
            {
                setNodeMask(util::osg::INVISIBLE_TO_ALL);
                //---
                _xform.setIdentity();
                inheritsXforms(true);
                commitMatrix();
            }

            inline
            math::Affine3f&
            accessMatrix()
            {
                return _xform;
            }

            inline
            void
            inheritsXforms(bool value)
            {
                _inheritsXforms = value;
                setReferenceFrame(value ? RELATIVE_RF : ABSOLUTE_RF);
            }

            inline
            bool
            inheritsXforms() const
            {
                return _inheritsXforms;
            }

            inline
            void
            currentCamera(unsigned int value)
            {
                _currentCamera = value;
            }

            inline
            void
            commitMatrix()
            {
                _matrix.set(_xform.data());
                _inverseDirty = true;
                dirtyBound();
            }

            bool
            computeLocalToWorldMatrix(osg::Matrix& matrix, osg::NodeVisitor* nv) const
            {
                return MatrixTransform::computeLocalToWorldMatrix(matrix, nv);
                //if (_referenceFrame==RELATIVE_RF)
                //{
                //  matrix.preMult(_matrix);
                //}
                //else // absolute
                //{
                //  matrix = _matrix;
                //}
                //return true;
            }

            bool
            computeWorldToLocalMatrix(osg::Matrix& matrix, osg::NodeVisitor* nv) const
            {
                return MatrixTransform::computeWorldToLocalMatrix(matrix, nv);
                //const Matrix& inverse = getInverseMatrix();

                //if (_referenceFrame==RELATIVE_RF)
                //{
                //  matrix.postMult(inverse);
                //}
                //else // absolute
                //{
                //  matrix = inverse;
                //}
                //return true;
            }
        };
    }
}
