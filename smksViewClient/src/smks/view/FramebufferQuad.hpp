// ========================================================================= //
// Copyright 2016-2018 SUPAMONKS_STUDIO                                      //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
//                                                                           //
// ========================================================================= //
#pragma once

#include <memory>
#include <smks/util/osg/ScreenQuadCamera.hpp>

namespace osg
{
    class Texture2D;
    class Image;
}

namespace smks
{
    class ClientBase;
    namespace xchg
    {
        class Data;
    }
    namespace view
    {
        namespace gl
        {
            class UniformInput;
        }

        class FrameBufferQuad:
            public util::osg::ScreenQuadCamera
        {
            friend struct DrawableCallback;
        public:
            typedef osg::ref_ptr<FrameBufferQuad>       Ptr;
            typedef osg::observer_ptr<FrameBufferQuad>  WPtr;
        private:
            typedef std::weak_ptr<ClientBase>           ClientWPtr;
            typedef std::weak_ptr<xchg::Data>           DataWPtr;
            typedef osg::ref_ptr<osg::Texture2D>        Texture2DPtr;
            typedef std::shared_ptr<gl::UniformInput>   UniformPtr;
            typedef osg::ref_ptr<osg::Image>            ImagePtr;
        private:
            struct DrawableCallback;
        private:
            ImagePtr        _image;
            Texture2DPtr    _texture2D;
            UniformPtr      _uRcpTextureSize;

            bool        _dirty;
            size_t      _textureWidth;
            size_t      _textureHeight;
            DataWPtr    _textureData;

        public:
            static
            Ptr
            create(unsigned int id, ClientWPtr const&, osg::GraphicsContext*, unsigned int textureUnit = 0);

        private:
            FrameBufferQuad(unsigned int id, ClientWPtr const&, osg::GraphicsContext*, unsigned int textureUnit);
            // non-copyable
            FrameBufferQuad(const FrameBufferQuad&);
            FrameBufferQuad& operator=(const FrameBufferQuad&);

        public:
            ~FrameBufferQuad();

            void
            dispose();

            void
            textureWidth(size_t);

            void
            textureHeight(size_t);

            void
            textureData(std::shared_ptr<xchg::Data> const&);

        private:
            void
            updateImage();
        };
    }
}
