// ========================================================================= //
// Copyright 2016-2018 SUPAMONKS_STUDIO                                      //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
//                                                                           //
// ========================================================================= //
#include <iostream>

#include <smks/sys/Exception.hpp>
#include <smks/sys/ResourceDatabase.hpp>
#include <smks/math/matrixAlgo.hpp>
#include <smks/view/gl/MaterialProgramBase.hpp>
#include <smks/view/gl/ContextExtensions.hpp>
#include <smks/view/gl/VertexAttrib.hpp>
#include <smks/view/gl/VertexAttribs.hpp>
#include <smks/view/gl/UniformDeclaration.hpp>
#include <smks/view/gl/ProgramInputDeclaration.hpp>
#include <smks/view/gl/ProgramInputDeclarations.hpp>
#include <smks/view/gl/ProgramTemplate.hpp>
#include <smks/view/gl/ProgramTemplates.hpp>
#include <smks/view/gl/ObjectStateFlags.hpp>
#include <smks/view/gl/VertexAttributeFlags.hpp>
#include <smks/view/gl/uniform.hpp>
#include <smks/util/color/rgba8.hpp>
#include <smks/util/osg/NodeMask.hpp>
#include <smks/util/osg/math.hpp>
#include <smks/util/osg/drawable.hpp>
#include <smks/ClientBase.hpp>

#include "GeometryDisplayHelper.hpp"
#include "CurvesGeometry.hpp"

namespace smks { namespace view
{
    class CurvesGeometry::UpdateCallback:
        public osg::Callback
    {
    public:
        virtual inline
        bool
        run(osg::Object* object, osg::Object* data)
        {
            CurvesGeometry* geo = reinterpret_cast<CurvesGeometry*>(object);
            if (geo)
                geo->loadProgram();
            return traverse(object, data);
        }
    };
}
}

using namespace smks;

// static
view::CurvesGeometry::Ptr
view::CurvesGeometry::create(unsigned int id, ClientBase::WPtr const& client)
{
    Ptr ptr = new CurvesGeometry(id, client);
    return ptr;
}

view::CurvesGeometry::CurvesGeometry(unsigned int id, ClientBase::WPtr const& cl):
    Geometry            (),
    _client             (cl),
    _displayHelper      (GeometryDisplayHelper::create(id, cl, getOrCreateStateSet())),
    _currentProgram     (0),
    //-------------------
    _currentBounds      (xchg::BoundingBox::UnitBox()),
    _currentVertices    (),
    _currentPositions   (),
    _currentWidths      (),
    _currentNormals     (),
    _currentTexcoords   (),
    _currentIndices     (),
    _currentCount       (0),
    //-------------------
    _bufferObjects      (),
    _uScaling           (gl::uScaling().asUniformDeclaration()->createInstance()),
    _uObjectStateFlags  (gl::uObjectStateFlags().asUniformDeclaration()->createInstance()),
    _uVertexAttributes  (gl::uVertexAttributes().asUniformDeclaration()->createInstance())
{
    ClientBase::Ptr const& client = cl.lock();
    if (!client)
        throw sys::Exception(
            "Invalid client.",
            "Curves Geometry Construction");

    setName(client->getNodeName(id));
    util::osg::setupRender(*this);

    osg::StateSet* stateSet = getStateSet();
    assert(stateSet);
    stateSet->setDataVariance(osg::Object::DYNAMIC);    // prevents update phase to commence before all draw phases finish
    addUpdateCallback(new UpdateCallback());            // assign program at update phase

    // add object id uniform
    gl::UniformInput::Ptr const& uObjectId = gl::uObjectId().asUniformDeclaration()->createInstance();

    gl::setUniformui(*uObjectId->data(),            id);
    gl::setUniformf(*_uScaling->data(),             1.0f);
    gl::setUniformi(*_uObjectStateFlags->data(),    gl::NO_OBJSTATE);
    gl::setUniformi(*_uVertexAttributes->data(),    gl::NO_VATTRIB);

    stateSet->addUniform(uObjectId->data(), osg::StateAttribute::OVERRIDE);
    stateSet->addUniform(_uScaling->data());
    stateSet->addUniform(_uObjectStateFlags->data());
    stateSet->addUniform(_uVertexAttributes->data());
}

view::CurvesGeometry::~CurvesGeometry()
{
    dispose();
    _displayHelper = nullptr;
}

void
view::CurvesGeometry::dispose()
{
    ClientBase::Ptr const&  client      = _client.lock();
    sys::ResourceDatabase*  resources   = client ? client->getResourceDatabase() : nullptr;
    gl::ContextExtensions*  extensions  = resources ? gl::getContextExtensions(*resources) : nullptr;

    setDataVariance(Object::STATIC);
    setNodeMask(util::osg::INVISIBLE_TO_ALL);

    _currentBounds      = xchg::BoundingBox::UnitBox();
    _currentVertices    .reset();
    _currentPositions   .clear();
    _currentWidths      .clear();
    _currentNormals     .clear();
    _currentTexcoords   .clear();
    _currentIndices     .reset();
    _currentCount       = 0;

    if (extensions)
        for (size_t contextId = 0; contextId < _bufferObjects.size(); ++contextId)
        {
            BufferObjects& buffers = _bufferObjects.get(contextId);
            if (buffers)
                extensions->getOrCreateGLExtension(contextId)->glDeleteBuffers(2, &buffers.vbo);
        }
    _bufferObjects.clear();

    _currentProgram = 0;
    gl::setUniformi(*_uVertexAttributes->data(), gl::NO_VATTRIB);
}

void
view::CurvesGeometry::setBound(const xchg::BoundingBox& box)
{
    _currentBounds = box.valid() ? box : xchg::BoundingBox::UnitBox();
    dirtyBound();
}

void
view::CurvesGeometry::setVertices(xchg::Data::Ptr const& value)
{
    _currentVertices = value;
    dirtyVertices();
}

void
view::CurvesGeometry::setPositions(const xchg::DataStream& value)
{
    _currentPositions = value;
    dirtyVertices();
    if (value.numElements() > 0)
        gl::add(*_uVertexAttributes->data(), gl::VATTRIB_POSITION);
    else
        gl::remove(*_uVertexAttributes->data(), gl::VATTRIB_POSITION);
}

void
view::CurvesGeometry::setWidths(const xchg::DataStream& value)
{
    _currentWidths = value;
    dirtyVertices();
    if (value.numElements() > 0)
        gl::add(*_uVertexAttributes->data(), gl::VATTRIB_WIDTH);
    else
        gl::remove(*_uVertexAttributes->data(), gl::VATTRIB_WIDTH);
}

void
view::CurvesGeometry::setNormals(const xchg::DataStream& value)
{
    _currentNormals = value;
    dirtyVertices();
    if (value.numElements() > 0)
        gl::add(*_uVertexAttributes->data(), gl::VATTRIB_NORMAL);
    else
        gl::remove(*_uVertexAttributes->data(), gl::VATTRIB_NORMAL);
}

void
view::CurvesGeometry::setTexcoords(const xchg::DataStream& value)
{
    _currentTexcoords = value;
    dirtyVertices();
    if (value.numElements() > 0)
        gl::add(*_uVertexAttributes->data(), gl::VATTRIB_TEXCOORD);
    else
        gl::remove(*_uVertexAttributes->data(), gl::VATTRIB_TEXCOORD);
}

void
view::CurvesGeometry::setIndices(xchg::Data::Ptr const& value)
{
    _currentIndices = value;
    dirtyIndices();
}

void
view::CurvesGeometry::setCount(size_t value)
{
    _currentCount = value;
    dirtyIndices();
}

void
view::CurvesGeometry::setSelected(bool value)
{
    if (value)
        gl::add(*_uObjectStateFlags->data(), gl::OBJSTATE_SELECTED);
    else
        gl::remove(*_uObjectStateFlags->data(), gl::OBJSTATE_SELECTED);
}

void
view::CurvesGeometry::dirtyVertices()
{
    for (size_t contextId = 0; contextId < _bufferObjects.size(); ++contextId)
        _bufferObjects.get(contextId).dirtyVbo = true;
}

void
view::CurvesGeometry::dirtyIndices()
{
    for (size_t contextId = 0; contextId < _bufferObjects.size(); ++contextId)
        _bufferObjects.get(contextId).dirtyEbo = true;
}

// virtual
osg::BoundingBoxf
view::CurvesGeometry::computeBoundingBox() const
{
    return osg::BoundingBoxf(
        _currentBounds.minX, _currentBounds.minY, _currentBounds.minZ,
        _currentBounds.maxX, _currentBounds.maxY, _currentBounds.maxZ);
}

// virtual
void
view::CurvesGeometry::drawImplementation(osg::RenderInfo& renderInfo) const
{
    if (!isProgramUpToDate())
        return;

    xchg::Data::Ptr const& currentVertices  = _currentVertices.lock();
    xchg::Data::Ptr const& currentIndices   = _currentIndices.lock();
    if (!currentVertices    || currentVertices->empty() ||
        !currentIndices     || currentIndices->empty() ||
        !_currentPositions)
        return;
    assert(!_currentWidths      || _currentWidths.numElements()     == _currentPositions.numElements());
    assert(!_currentNormals     || _currentNormals.numElements()    == _currentPositions.numElements());
    assert(!_currentTexcoords   || _currentTexcoords.numElements()  == _currentPositions.numElements());
    //---
    ClientBase::Ptr const&          client      = _client.lock();
    const sys::ResourceDatabase*    resources   = client ? client->getResourceDatabase() : nullptr;
    const gl::ContextExtensions*    extensions  = resources ? gl::getContextExtensions(*resources) : nullptr;
    if (!extensions)
        return;

    assert(renderInfo.getState());
    osg::State&                 state       = *renderInfo.getState();
    const unsigned int          contextId   = state.getContextID();
    BufferObjects&              buffers     = _bufferObjects.get(contextId);
    const osg::GLExtensions*    ext         = const_cast<gl::ContextExtensions*>(extensions)
        ->getOrCreateGLExtension(contextId);

    math::Affine3f curModelView;

    util::osg::convert(state.getModelViewMatrix(), curModelView);

    const float curScaling  = math::scaling(curModelView);
    float       prvScaling  = 1.0f;
    _uScaling->data()->get(prvScaling);
    if (fabsf(curScaling - prvScaling) > 1e-3f)
    {
        _uScaling->data()->set(curScaling);
        _uScaling->data()->dirty();
    }

    assert(ext);

    const bool      generateBuffers = !buffers;
    const GLuint    loc             = state.getVertexAlias()._location;

    if (generateBuffers)
        ext->glGenBuffers(2, &buffers.vbo);

    ext->glBindBuffer(GL_ARRAY_BUFFER,          buffers.vbo);
    ext->glBindBuffer(GL_ELEMENT_ARRAY_BUFFER,  buffers.ebo);

    if (generateBuffers)
    {
        ext->glVertexAttribPointer(
            loc,
            3,
            GL_FLOAT,
            false,
            sizeof(float) * _currentPositions.stride(),
            (GLvoid*)(sizeof(float) * _currentPositions.offset()));
        if (_currentWidths)
            gl::iWidth().glVertexAttribPointer(
                ext,
                sizeof(float) * _currentWidths.stride(),
                (GLvoid*)(sizeof(float) * _currentWidths.offset()));
    }

    if (buffers.dirtyVbo)
    {
        ext->glBufferData(
            GL_ARRAY_BUFFER,
            currentVertices->size() * sizeof(float),
            currentVertices->getPtr<float>(),
            GL_DYNAMIC_DRAW);
        buffers.dirtyVbo = false;
    }
    if (buffers.dirtyEbo)
    {
        ext->glBufferData(
            GL_ELEMENT_ARRAY_BUFFER,
            currentIndices->size() * sizeof(unsigned int),
            currentIndices->getPtr<unsigned int>(),
            GL_DYNAMIC_DRAW);
        buffers.dirtyEbo = false;
    }

    assert(buffers.vbo > 0 && buffers.ebo > 0 && !buffers.dirtyVbo && !buffers.dirtyEbo);

    ext->glEnableVertexAttribArray(loc);
    if (_currentWidths)
        gl::iWidth().glEnableVertexAttribArray(ext);
    //--------------------------------------------------------------------------------
    glDrawElements(GL_LINES_ADJACENCY_EXT, currentIndices->size(), GL_UNSIGNED_INT, 0);
    //--------------------------------------------------------------------------------
    ext->glDisableVertexAttribArray(state.getVertexAlias()._location);
    gl::iWidth().glDisableVertexAttribArray(ext);
    ext->glBindBuffer(GL_ARRAY_BUFFER,          0);
    ext->glBindBuffer(GL_ELEMENT_ARRAY_BUFFER,  0);
}

bool
view::CurvesGeometry::isProgramUpToDate() const
{
    return _displayHelper &&
        _displayHelper->program() &&
        _currentProgram == _displayHelper->program()->id();
}

void
view::CurvesGeometry::loadProgram()
{
    // lazily load and assign shading program at update stage
    if (isProgramUpToDate())
        return;
    if (_displayHelper == nullptr ||
        _displayHelper->program() == nullptr)
        return;

    ClientBase::Ptr const&              client      = _client.lock();
    sys::ResourceDatabase::Ptr const&   resources   = client ? client->getOrCreateResourceDatabase() : nullptr;
    if (!resources)
        return;

    gl::getOrCreateContextExtensions(*resources);
    osg::ref_ptr<osg::Program> const& program = gl::getOrCreateProgram(
        _displayHelper->program()->progTemplate(), resources);
    if (!program)
        return;

    assert(getStateSet());
    getStateSet()->setAttribute(program.get(), osg::StateAttribute::OVERRIDE);
    _currentProgram = _displayHelper->program()->id();
}
