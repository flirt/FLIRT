// ========================================================================= //
// Copyright 2016-2018 SUPAMONKS_STUDIO                                      //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
//                                                                           //
// ========================================================================= //
#include <cassert>

#include <osg/Camera>
#include <osg/Uniform>
#include <osg/Depth>
#include <osg/LineWidth>

#include <smks/sys/Exception.hpp>
#include <smks/sys/ResourceDatabase.hpp>
#include <smks/sys/TextResource.hpp>
#include <smks/view/gl/ProgramTemplate.hpp>
#include <smks/view/gl/ProgramTemplates.hpp>
#include <smks/view/gl/ContextExtensions.hpp>
#include <smks/view/gl/ProgramInputDeclarations.hpp>
#include <smks/util/osg/texture.hpp>
#include <smks/util/osg/drawable.hpp>
#include <smks/ClientBase.hpp>
#include <smks/ClientBindingBase.hpp>

#include "WireframeGeometry.hpp"
#include "Geode.hpp"
#include "PolyMeshGeometry.hpp"

using namespace smks;

// static
view::WireframeGeometry::Ptr
view::WireframeGeometry::create(unsigned int                    id,
                                ClientBase::WPtr const&         client,
                                osg::Camera::BufferComponent    colorIdAttachment)
{
    Ptr ptr(new WireframeGeometry(id, client, colorIdAttachment));
    return ptr;
}

view::WireframeGeometry::WireframeGeometry(unsigned int                 id,
                                           ClientBase::WPtr const&      cl,
                                           osg::Camera::BufferComponent colorIdAttachment):
    Geometry(),
    _id                     (id),
    _client                 (cl),
    _extensions             (nullptr),
    _objectIdAttachment     (colorIdAttachment),
    _objectIdAttachmentGL   (util::osg::getGLenum(colorIdAttachment)),
    _objectIdTexture        (),
    _uColor                 (view::gl::get_LineProgram().createUniform(gl::uColor(), -1))
{
    ClientBase::Ptr const& client = _client.lock();
    if (client)
        _extensions = gl::getOrCreateContextExtensions(*client->getOrCreateResourceDatabase());
    if (!_extensions)
        throw sys::Exception("Failed to get GL extensions.", "Wireframe Geometry Construction");

    util::osg::setupRender(*this);

    osg::StateSet*          stateSet    = getOrCreateStateSet();
    const PolyMeshGeometry* mesh        = getPolyMeshGeometry();

    stateSet->setAttributeAndModes(loadProgram(), osg::StateAttribute::ON);

    stateSet->addUniform(_uColor->data());
    stateSet->setAttribute(new osg::Depth(osg::Depth::LESS, 0.0, 1.0, false));
    stateSet->setAttribute(new osg::LineWidth(1.5f));
    stateSet->setRenderBinDetails(osg::StateSet::TRANSPARENT_BIN, "RenderBin");

    if (mesh)
    {
        setDataVariance(mesh->getDataVariance());
        setVertexArray(const_cast<osg::Array*>(mesh->getVertexArray()));
    }
    else
        setDataVariance(mesh->getDataVariance());
}

osg::Program*
view::WireframeGeometry::loadProgram() const
{
    ClientBase::Ptr const&  client = _client.lock();
    osg::Program*           program = client
        ? gl::getOrCreateProgram(gl::get_LineProgram(), client->getOrCreateResourceDatabase()).get()
        : nullptr;

    if (program == nullptr)
        throw sys::Exception("Failed to initialize shading program.", "Line Shading Program Creation");

    return program;
}

void
view::WireframeGeometry::setIndices(xchg::Data::Ptr const& value)
{
    if (!value)
        return;

    osg::ref_ptr<osg::DrawElementsUInt> indices = nullptr;
    if (getNumPrimitiveSets() == 0)
    {
        indices = new osg::DrawElementsUInt();
        indices->setMode(GL_LINES);
        addPrimitiveSet(indices);
    }
    indices = dynamic_cast<osg::DrawElementsUInt*>(getPrimitiveSet(0));
    assert(indices);

    indices->asVector().resize(value->size());
    assert(indices->size() % 2 == 0);
    memcpy(&indices->asVector().front(), value->getPtr<unsigned int>(), sizeof(unsigned int) * value->size());

    indices->dirty();
}

const view::PolyMeshGeometry*
view::WireframeGeometry::getPolyMeshGeometry() const
{
    ClientBase::Ptr const& client = _client.lock();
    ClientBindingBase::Ptr const&   bind = client
        ? client->getBinding(_id)
        : nullptr;

    if (!bind ||
        !bind->asOsgObject() ||
        !bind->asOsgObject()->asNode() ||
        !bind->asOsgObject()->asNode()->asGeode())
        return nullptr;

    PolyMeshGeode* geode = dynamic_cast<PolyMeshGeode*>(
        bind->asOsgObject()->asNode()->asGeode());

    return geode
        ? geode->mainGeometry()
        : nullptr;
}

void
view::WireframeGeometry::setColor(const osg::Vec4f& value)
{
    _uColor->data()->set(value);
}

// virtual
osg::BoundingBoxf
view::WireframeGeometry::computeBoundingBox() const
{
    const PolyMeshGeometry* mesh = getPolyMeshGeometry();

    return mesh ? mesh->getBoundingBox() : osg::BoundingBoxf();
}

// virtual
void
view::WireframeGeometry::drawImplementation(osg::RenderInfo& renderInfo) const
{
    assert(renderInfo.getState());
    assert(_extensions);

    osg::State&         state       = *renderInfo.getState();
    const unsigned int  contextId   = state.getContextID();
    TextureObject&      objectIdTex = _objectIdTexture.get(contextId);

    if (objectIdTex.tex < 0)
    {
        const osg::Camera*                                      camera          = renderInfo.getCurrentCamera();
        const osg::Camera::BufferAttachmentMap&                 attachements    = camera->getBufferAttachmentMap();
        const osg::Camera::BufferAttachmentMap::const_iterator  foundAttachIt   = attachements.find(_objectIdAttachment);

        if (foundAttachIt != attachements.end())
        {
            const osg::Camera::Attachment&  attachment  = foundAttachIt->second;

            if (attachment._texture.valid())
                objectIdTex.tex = attachment._texture->getTextureObject(renderInfo.getContextID())->id();
        }
    }
    if (objectIdTex.tex < 0)
        return;

    if (getNumPrimitiveSets() != 1)
        return;

    const osg::DrawElementsUInt* indices = dynamic_cast<const osg::DrawElementsUInt*>(getPrimitiveSet(0));
    assert(indices->getMode() == GL_LINES);
    assert(indices->getType() == osg::PrimitiveSet::DrawElementsUIntPrimitiveType);

    const PolyMeshGeometry* mesh = getPolyMeshGeometry();
    if (!mesh)
        return;

    assert(renderInfo.getState());

    const osg::GLExtensions* ext = _extensions->getOrCreateGLExtension(contextId);
    if (ext == nullptr ||
        !ext->isFrameBufferObjectSupported)
        return;

    const bool vboBound = mesh->bindVbo(renderInfo, *ext);
    if (!vboBound)
        return;

    osg::GLBufferObject* ebo = indices->getOrCreateGLBufferObject(state.getContextID());

    ext->glFramebufferTexture2D(GL_FRAMEBUFFER_EXT, _objectIdAttachmentGL, GL_TEXTURE_2D, 0, 0);
    state.bindElementBufferObject(ebo);
    //-----------------------------------------------------------
    glDrawElements(
        GL_LINES,
        indices->getNumIndices(),
        GL_UNSIGNED_INT,
        (const GLvoid*)(ebo->getOffset(indices->getBufferIndex()))
    );
    //-----------------------------------------------------------
    state.unbindElementBufferObject();
    ext->glFramebufferTexture2D(GL_FRAMEBUFFER_EXT, _objectIdAttachmentGL, GL_TEXTURE_2D, objectIdTex.tex, 0);
    mesh->unbindVbo(renderInfo, *ext);
}
