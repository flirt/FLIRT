// ========================================================================= //
// Copyright 2016-2018 SUPAMONKS_STUDIO                                      //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
//                                                                           //
// ========================================================================= //
#include <iostream>
#include <std/to_string_xchg.hpp>
#include <boost/unordered_map.hpp>
#include <osg/observer_ptr>
#include <osg/Group>
#include <osg/Geode>
#include <osg/Geometry>
#include <osg/Program>
#include <osg/GraphicsContext>

#include <smks/xchg/IdSet.hpp>
#include <smks/xchg/SceneEvent.hpp>
#include <smks/xchg/ParmEvent.hpp>

#include <smks/sys/Exception.hpp>
#include <smks/sys/Log.hpp>

#include <smks/util/string/getId.hpp>
#include <smks/util/osg/UserData.hpp>

#include <smks/parm/BuiltIns.hpp>

#include <smks/scn/TreeNode.hpp>
#include <smks/scn/Archive.hpp>
#include <smks/scn/Scene.hpp>
#include <smks/scn/PolyMesh.hpp>
#include <smks/scn/Curves.hpp>
#include <smks/scn/Points.hpp>
#include <smks/scn/Xform.hpp>
#include <smks/scn/Camera.hpp>
#include <smks/scn/Material.hpp>
#include <smks/scn/SubSurface.hpp>
#include <smks/scn/Texture.hpp>
#include <smks/scn/IdAssign.hpp>
#include <smks/scn/TreeNodeVisitor.hpp>
#include <smks/scn/FrameBuffer.hpp>

#include <smks/view/gl/ContextExtensions.hpp>
#include <smks/view/AbstractDevice.hpp>

#include "smks/view/Client.hpp"
#include "Binding.hpp"
#include "CullVisitor.hpp"
#include "SceneGroup.hpp"
#include "ArchiveGroup.hpp"
#include "XTransform.hpp"
#include "CameraLocator.hpp"
#include "Geode.hpp"
#include "TextureImage.hpp"
#include "FrameBufferQuad.hpp"
#include "LightLocator.hpp"
#include "StateAttributeProvider.hpp"
#include "GeometryDisplayHelper.hpp"
#include "RenderActionGroup.hpp"
#include "DefaultGroup.hpp"
#include "WireframePoolUpdater.hpp"
#include "Client-callbacks.hpp"
#include "../sys/device_helpers.hpp"

namespace smks { namespace view
{
    //////////////
    // class PImpl
    //////////////
    class Client::PImpl
    {
    private:
        typedef osg::ref_ptr<osg::GraphicsContext> GraphicsContextPtr;

    private:
        Client&                             _self;
        const Config                        _cfg;
        osg::ref_ptr<osg::Group>            _root;
        AbstractDevice*                     _device;
        xchg::AbstractGUIEventCallback::Ptr _mousePressCallback;
        xchg::AbstractGUIEventCallback::Ptr _mouseMoveCallback;
        xchg::AbstractGUIEventCallback::Ptr _mouseReleaseCallback;
        xchg::AbstractGUIEventCallback::Ptr _wheelCallback;
        xchg::AbstractGUIEventCallback::Ptr _keyPressCallback;
        xchg::AbstractGUIEventCallback::Ptr _keyReleaseCallback;
        xchg::AbstractGUIEventCallback::Ptr _resizeCallback;
        xchg::AbstractGUIEventCallback::Ptr _paintGLCallback;
        WireframePoolUpdater::Ptr           _wireframes;

    public:
        PImpl(Client&     self,
              const char* jsonCfg):
            _self                   (self),
            _cfg                    (jsonCfg),
            _root                   (new osg::Group()),
            _device                 (nullptr),
            _mousePressCallback     (nullptr),
            _mouseMoveCallback      (nullptr),
            _mouseReleaseCallback   (nullptr),
            _wheelCallback          (nullptr),
            _keyPressCallback       (nullptr),
            _keyReleaseCallback     (nullptr),
            _resizeCallback         (nullptr),
            _paintGLCallback        (nullptr),
            _wireframes             (nullptr)
        {
            _root->setName("__client_root");
        }

        inline
        void
        build(Client::Ptr const&,
              const char* jsonCfg,
              osg::GraphicsContext*);

        inline
        void
        dispose();

        inline
        const osg::Group*
        root() const
        {
            return _root.get();
        }

        inline
        osg::Group*
        root()
        {
            return _root.get();
        }

        inline
        const view::Client::Config&
        config() const
        {
            return _cfg;
        }

        inline
        AbstractDevice*
        device()
        {
            return _device;
        }

        inline
        const AbstractDevice*
        device() const
        {
            return _device;
        }

        inline
        void
        getDrawablePositions(unsigned int, osg::Vec3Array&) const;

        inline
        void
        handleSceneEvent(scn::Scene&, const xchg::SceneEvent&);

        inline
        void
        handleSceneParmEvent(const xchg::ParmEvent&);

        inline
        ClientBindingBasePtr
        createBinding(scn::TreeNode::WPtr const&);

        inline
        void
        destroyBinding(ClientBindingBasePtr const&);

        inline
        void
        registerGUIEventCallbacks(scn::Scene& scene)
        {
            if (_mousePressCallback)
                scene.registerGUIEventCallback(xchg::MOUSE_PRESS_EVENT, _mousePressCallback);
            if (_mouseMoveCallback)
                scene.registerGUIEventCallback(xchg::MOUSE_MOVE_EVENT, _mouseMoveCallback);
            if (_mouseReleaseCallback)
                scene.registerGUIEventCallback(xchg::MOUSE_RELEASE_EVENT, _mouseReleaseCallback);
            if (_wheelCallback)
                scene.registerGUIEventCallback(xchg::WHEEL_EVENT, _wheelCallback);
            if (_keyPressCallback)
                scene.registerGUIEventCallback(xchg::KEY_PRESS_EVENT, _keyPressCallback);
            if (_keyReleaseCallback)
                scene.registerGUIEventCallback(xchg::KEY_RELEASE_EVENT, _keyReleaseCallback);
            if (_resizeCallback)
                scene.registerGUIEventCallback(xchg::RESIZE_EVENT, _resizeCallback);
            if (_paintGLCallback)
                scene.registerGUIEventCallback(xchg::PAINT_GL_EVENT, _paintGLCallback);
        }

        inline
        void
        deregisterGUIEventCallbacks(scn::Scene& scene)
        {
            if (_mousePressCallback)
                scene.deregisterGUIEventCallback(xchg::MOUSE_PRESS_EVENT, _mousePressCallback);
            if (_mouseMoveCallback)
                scene.deregisterGUIEventCallback(xchg::MOUSE_MOVE_EVENT, _mouseMoveCallback);
            if (_mouseReleaseCallback)
                scene.deregisterGUIEventCallback(xchg::MOUSE_RELEASE_EVENT, _mouseReleaseCallback);
            if (_wheelCallback)
                scene.deregisterGUIEventCallback(xchg::WHEEL_EVENT, _wheelCallback);
            if (_keyPressCallback)
                scene.deregisterGUIEventCallback(xchg::KEY_PRESS_EVENT, _keyPressCallback);
            if (_keyReleaseCallback)
                scene.deregisterGUIEventCallback(xchg::KEY_RELEASE_EVENT, _keyReleaseCallback);
            if (_resizeCallback)
                scene.deregisterGUIEventCallback(xchg::RESIZE_EVENT, _resizeCallback);
            if (_paintGLCallback)
                scene.deregisterGUIEventCallback(xchg::PAINT_GL_EVENT, _paintGLCallback);
        }

    private:
        inline
        scn::TreeNode::Ptr
        getParentDataNode(unsigned int) const;

        inline
        osg::Group*
        getClosestParentGroup(unsigned int) const;

        inline
        void
        updateLink(ClientBindingBase::Ptr const&, ClientBindingBase::Ptr const&, bool createLink);

        inline
        void
        updateTextureToProviderLink(ClientBindingBase::Ptr const&, ClientBindingBase::Ptr const&, bool createLink);

        inline
        void
        updateProviderToDrawableLink(ClientBindingBase::Ptr const&, ClientBindingBase::Ptr const&, bool createLink);

        static inline
        void
        getDrawableProviderIds(scn::TreeNode::Ptr const&, xchg::IdSet&);
    };
}
}

using namespace smks;

/////////////////////////
// class Client::PImpl
/////////////////////////
void
view::Client::PImpl::build(Client::Ptr const&       ptr,
                           const char*              jsonCfg,
                           osg::GraphicsContext*    graphicsContext)
{
    dispose();

    osgUtil::CullVisitor::prototype() = new CullVisitor();

    assert(ptr);
    _device = sys::createDeviceHelper(
        _cfg.deviceModule(),
        jsonCfg,
        ptr,
        _root,
        graphicsContext);
    _wireframes = WireframePoolUpdater::create(ptr, osg::Camera::COLOR_BUFFER1);

    if (graphicsContext)
    {
        _mousePressCallback     = MousePressEventCallback   ::create(ptr);
        _mouseMoveCallback      = MouseMoveEventCallback    ::create(ptr);
        _mouseReleaseCallback   = MouseReleaseEventCallback ::create(ptr);
        _wheelCallback          = WheelEventCallback        ::create(ptr);
        _keyPressCallback       = KeyPressEventCallback     ::create(ptr);
        _keyReleaseCallback     = KeyReleaseEventCallback   ::create(ptr);
        _resizeCallback         = ResizeEventCallback       ::create(ptr);
        _paintGLCallback        = PaintGLEventCallback      ::create(ptr);
    }
}

void
view::Client::PImpl::dispose()
{
    _mousePressCallback     = nullptr;
    _mouseMoveCallback      = nullptr;
    _mouseReleaseCallback   = nullptr;
    _wheelCallback          = nullptr;
    _keyPressCallback       = nullptr;
    _keyReleaseCallback     = nullptr;
    _resizeCallback         = nullptr;
    _paintGLCallback        = nullptr;

    if (_wireframes)
        _wireframes->dispose();
    _wireframes = nullptr;

    sys::destroyDeviceHelper(
        _cfg.deviceModule(),
        _device);
    _device = nullptr;
}

void
view::Client::PImpl::handleSceneEvent(scn::Scene&               scene,
                                      const xchg::SceneEvent&   evt)
{
    if (evt.type() == xchg::SceneEvent::NODES_LINKED ||
        evt.type() == xchg::SceneEvent::NODES_UNLINKED)
    {
        ClientBindingBase::Ptr const&   source      = _self.getBinding(evt.source());
        ClientBindingBase::Ptr const&   target      = _self.getBinding(evt.target());
        const bool              createLink  = evt.type() == xchg::SceneEvent::NODES_LINKED;

        updateLink(source, target, createLink);
    }
    else if (evt.type() == xchg::SceneEvent::NODE_DELETED)
    { }
}

void
view::Client::PImpl::updateLink(ClientBindingBase::Ptr const&   source,
                                ClientBindingBase::Ptr const&   target,
                                bool                            createLink)
{
    if (!source || !target)
        return;
    const xchg::NodeClass sourceCl = source->nodeClass();
    const xchg::NodeClass targetCl = target->nodeClass();

    if (sourceCl == xchg::NodeClass::TEXTURE &&
        (targetCl == xchg::NodeClass::MATERIAL || targetCl == xchg::NodeClass::SUBSURFACE))
        updateTextureToProviderLink(source, target, createLink);
    else if ((sourceCl == xchg::NodeClass::MATERIAL || sourceCl == xchg::NodeClass::SUBSURFACE) &&
        (targetCl == xchg::NodeClass::MESH || targetCl == xchg::NodeClass::CURVES || targetCl == xchg::NodeClass::POINTS))
        updateProviderToDrawableLink(source, target, createLink);
}

//////////////////////
// TEXTURE -> PROVIDER
//////////////////////
void
view::Client::PImpl::updateTextureToProviderLink(ClientBindingBase::Ptr const&  sourceBind,
                                                 ClientBindingBase::Ptr const&  targetBind,
                                                 bool                           createLink)
{
    assert(sourceBind && sourceBind->nodeClass() == xchg::NodeClass::TEXTURE);
    assert(targetBind && (targetBind->nodeClass() == xchg::NodeClass::MATERIAL ||
        targetBind->nodeClass() == xchg::NodeClass::SUBSURFACE));

    TextureImage*           texImage    = dynamic_cast<TextureImage*>           (sourceBind->asOsgObject());
    StateAttributeProvider* provider    = dynamic_cast<StateAttributeProvider*> (targetBind->asOsgObject());

    if (texImage && provider)
    {
        xchg::IdSet links;

        _self.getLinksBetweenNodes(targetBind->id(), sourceBind->id(), links);
        for (xchg::IdSet::const_iterator id = links.begin(); id != links.end(); ++id)
            provider->setTextureImage(*id, createLink ? texImage : nullptr);
    }
}


///////////////////////
// PROVIDER -> DRAWABLE
///////////////////////
// static inline
void
view::Client::PImpl::getDrawableProviderIds(scn::TreeNode::Ptr const&   node,
                                            xchg::IdSet&                providerIds)
{
    providerIds.clear();

    const scn::Drawable* drawable = node && node->asSceneObject()
        ? node->asSceneObject()->asDrawable()
        : nullptr;

    if (!drawable)
        return;

    unsigned int id = 0;

    if (drawable->getParameter<unsigned int>(parm::materialId   (), id))    providerIds.insert(id);
    if (drawable->getParameter<unsigned int>(parm::subsurfaceId (), id))    providerIds.insert(id);
}

void
view::Client::PImpl::updateProviderToDrawableLink(ClientBindingBase::Ptr const& sourceBind,
                                                  ClientBindingBase::Ptr const& targetBind,
                                                  bool                          createLink)
{
    if (!sourceBind || !targetBind)
        return;

    scn::TreeNode::Ptr const& target = targetBind->dataNode().lock();
    if (!target)
        return;

    unsigned int    link            = 0;
    const bool      asMaterial      = target->getParameter<unsigned int>(parm::materialId(),    link)   && link == sourceBind->id();
    const bool      asSubSurface    = target->getParameter<unsigned int>(parm::subsurfaceId(),  link)   && link == sourceBind->id();

    StateAttributeProvider* provider    = dynamic_cast<StateAttributeProvider*>(sourceBind->asOsgObject());
    GeometryDisplayHelper*  helper      = nullptr;

    if (targetBind->asOsgObject() &&
        targetBind->asOsgObject()->asNode() &&
        targetBind->asOsgObject()->asNode()->asGeode())
    {
        osg::Geode* geode = targetBind->asOsgObject()->asNode()->asGeode();
        if (dynamic_cast<PolyMeshGeode*>(geode))
            helper = dynamic_cast<PolyMeshGeode*>(geode)
                ->mainGeometry()->displayHelper();
        else if (dynamic_cast<CurvesGeode*>(geode))
            helper = dynamic_cast<CurvesGeode*>(geode)
                ->mainGeometry()->displayHelper();
        else if (dynamic_cast<PointsGeode*>(geode))
            helper = dynamic_cast<PointsGeode*>(geode)
                ->mainGeometry()->displayHelper();
    }

    if (provider && helper)
    {
        FLIRT_LOG(LOG(DEBUG)
            << (createLink ? "creating" : "destroying")
            << " link between provider ID = " << sourceBind->id()
            << " and drawable ID = " << targetBind->id()
            << "\n\tas material ? " << asMaterial
            << "\tas subsurface ? " << asSubSurface;)
        if (asMaterial)
            helper->setProvider(parm::materialId().id(),
                                createLink ? provider : nullptr);
        if (asSubSurface)
            helper->setProvider(parm::subsurfaceId().id(),
                                createLink ? provider : nullptr);
    }
}
//////////////////////

void
view::Client::PImpl::handleSceneParmEvent(const xchg::ParmEvent& evt)
{ }

ClientBindingBase::Ptr
view::Client::PImpl::createBinding(scn::TreeNode::WPtr const& n)
{
    scn::TreeNode::Ptr const& dataNode = n.lock();
    if (!dataNode)
        return nullptr;

    osg::GraphicsContext* context = _device->getCamera()->getGraphicsContext();

    //--------------------------------------------------
    // create the SMKS scene data node's OSG counterpart (may be an osg::Node, osg::StateAttribute, or osg::StateSet).
    osg::ref_ptr<osg::Object> osgObject = DefaultGroup::create(dataNode->id(), _self.self());

    if (dataNode->asScene())
        osgObject = SceneGroup::create(dataNode->id(), _self.self());

    else if (dataNode->asArchive())
        osgObject = ArchiveGroup::create(dataNode->id(), _self.self());

    else if (dataNode->asSceneObject())
    {
        const scn::SceneObject* obj = dataNode->asSceneObject();

        if (obj->asXform())
            osgObject = XTransform::create(dataNode->id(), _self.self());

        else if (obj->asCamera())
            osgObject = CameraLocator::create(dataNode->id(), _self.self());

        else if (obj->asDrawable())
        {
            const scn::Drawable* drw = obj->asDrawable();

            if (drw->asPolyMesh())
            {
                osgObject = PolyMeshGeode::create(dataNode->id(), _self.self());
                dataNode->registerObserver(_wireframes);
            }

            else if (drw->asCurves())
                osgObject = CurvesGeode::create(dataNode->id(), _self.self());

            else if (drw->asPoints())
                osgObject = PointsGeode::create(dataNode->id(), _self.self());
        }

        else if (obj->asLight())
            osgObject = LightLocator::create(dataNode->id(), _self.self());

    } // scene object
    else if (dataNode->asShader())
    {
        const scn::Shader* shader = dataNode->asShader();

        if (shader->asTexture())
            osgObject = TextureImage::create(dataNode->id(), _self.self());
        else if (shader->asMaterial())
            osgObject = StateAttributeProvider::create(dataNode->id(), _self.self());
        else if (shader->asSubSurface())
            osgObject = StateAttributeProvider::create(dataNode->id(), _self.self());
    }
    else if (dataNode->asRtNode())
    {
        const scn::RtNode* rtNode = dataNode->asRtNode();

        if (rtNode->asFrameBuffer())
            osgObject = FrameBufferQuad::create(dataNode->id(), _self.self(), context);
    }

    else if (dataNode->asGraphAction())
    {
        const scn::GraphAction* action = dataNode->asGraphAction();

        if (action->asRenderAction())
            osgObject = RenderActionGroup::create(dataNode->id(), _self.self());
    }

    osgObject->setUserData(new util::osg::UserData(dataNode->id()));
    osgObject->setName(dataNode->name());
    //--------------------------------------------------

    // if the OSG object is a node, then place it inside the OSG scene graph hierarchy.
    osg::Node* osgNode = dynamic_cast<osg::Node*>(osgObject.get());

    if (osgNode)
    {
        scn::TreeNode::Ptr const& parentNode = dataNode->parent().lock();

        if (parentNode)
        {
            osg::Group* osgParent = getClosestParentGroup(parentNode->id());

            if (!osgParent)
            {
                std::stringstream sstr;
                sstr
                    << "Failed to find group above view node "
                    << "associated with ID = " << parentNode->id()
                    << ".";
                throw sys::Exception(sstr.str().c_str(), "Node View Creation");
            }
            osgParent->addChild(osgNode);
        }
        else if (dataNode->asScene())
            _root->addChild(osgNode);
        else
            throw sys::Exception(
                "Only the scene is allowed not to have a parent.",
                "Node View Creation");
    }
    //--------------------------------------------------

    return Binding::create(_self.self(), n, osgObject);
}

void
view::Client::PImpl::destroyBinding(ClientBindingBase::Ptr const& bind)
{
    osg::Object*    osgObject   = bind ? bind->asOsgObject() : nullptr;
    osg::Node*      osgNode     = osgObject ? osgObject->asNode() : nullptr;

    if (osgNode)
    {
        // must remove node from the OSG scene graph hierarchy
        if (osgNode->getNumParents() > 0)
            osgNode->getParent(0)->removeChild(osgNode);

        if      (dynamic_cast<XTransform*>          (osgNode)) dynamic_cast<XTransform*>        (osgNode)->dispose();
        else if (dynamic_cast<PolyMeshGeode*>       (osgNode)) dynamic_cast<PolyMeshGeode*>     (osgNode)->dispose();
        else if (dynamic_cast<CurvesGeode*>         (osgNode)) dynamic_cast<CurvesGeode*>       (osgNode)->dispose();
        else if (dynamic_cast<PointsGeode*>         (osgNode)) dynamic_cast<PointsGeode*>       (osgNode)->dispose();
        else if (dynamic_cast<CameraLocator*>       (osgNode)) dynamic_cast<CameraLocator*>     (osgNode)->dispose();
        else if (dynamic_cast<ArchiveGroup*>        (osgNode)) dynamic_cast<ArchiveGroup*>      (osgNode)->dispose();
        else if (dynamic_cast<SceneGroup*>          (osgNode)) dynamic_cast<SceneGroup*>        (osgNode)->dispose();
        else if (dynamic_cast<DefaultGroup*>        (osgNode)) dynamic_cast<DefaultGroup*>      (osgNode)->dispose();
        else if (dynamic_cast<FrameBufferQuad*>     (osgNode)) dynamic_cast<FrameBufferQuad*>   (osgNode)->dispose();
        else if (dynamic_cast<LightLocator*>        (osgNode)) dynamic_cast<LightLocator*>      (osgNode)->dispose();
        else if (dynamic_cast<RenderActionGroup*>   (osgNode)) dynamic_cast<RenderActionGroup*> (osgNode)->dispose();
        else
        {
            std::stringstream sstr;
            sstr
                << "Failed to dispose of (node) binding "
                << "associated with ID = " << bind->id() << ".";
            throw sys::Exception(sstr.str().c_str(), "View Binding Destruction");
        }
    }
    else if (osgObject)
    {
        if      (dynamic_cast<StateAttributeProvider*>  (osgObject)) dynamic_cast<StateAttributeProvider*>  (osgObject)->dispose();
        else if (dynamic_cast<TextureImage*>            (osgObject)) dynamic_cast<TextureImage*>            (osgObject)->dispose();
        else
        {
            std::stringstream sstr;
            sstr
                << "Failed to destroy binding "
                << "associated with ID = " << bind->id() << ".";
            throw sys::Exception(sstr.str().c_str(), "View Binding Destruction");
        }
    }
}

osg::Group*
view::Client::PImpl::getClosestParentGroup(unsigned int parentId) const
{
    ClientBindingBase::Ptr const&   parentBind  = _self.getBinding(parentId);
    osg::Node*              parent      = parentBind && parentBind->asOsgObject()
        ? parentBind->asOsgObject()->asNode()
        : nullptr;

    if (!parent)
    {
        std::stringstream sstr;
        sstr
            << "No valid node view associated with ID = " << parentId << ".";
        throw sys::Exception(sstr.str().c_str(), "Get Closest Parent Group");
    }

    while (parent)
    {
        osg::Group* group = dynamic_cast<osg::Group*>(parent);
        if (group)
            return group;

        if (parent->getNumParents() == 0)
            return nullptr;
        else
        {
            assert(parent->getNumParents() == 1);
            parent = parent->getParent(0);
        }
    }
    return nullptr;
}

void
view::Client::PImpl::getDrawablePositions(unsigned int id, osg::Vec3Array& ret) const
{
    ret.clear();

    ClientBindingBase::Ptr const& bind  = _self.getBinding(id);
    const osg::Node*        osgNode = bind && bind->asOsgObject()
        ? bind->asOsgObject()->asNode()
        : nullptr;

    if (!osgNode)
        return;

    {
        const PolyMeshGeode* geode = dynamic_cast<const PolyMeshGeode*>(osgNode);
        if (geode)
        {
            assert(geode);
            assert(geode->mainGeometry());
            geode->mainGeometry()->getPositions(ret);
            return;
        }
    }
    {
        const CurvesGeode* geode = dynamic_cast<const CurvesGeode*>(osgNode);
        if (geode)
            throw sys::Exception(
                "Cannot currently get positions from curves view nodes.",
                "View Node Position Getter");
    }
    {
        const PointsGeode* geode = dynamic_cast<const PointsGeode*>(osgNode);
        if (geode)
            throw sys::Exception(
                "Cannot currently get positions from points view nodes.",
                "View Node Position Getter");
    }
    {
        std::stringstream sstr;
        sstr
            << "Node associated with ID = " << id
            << " is not a supported kind of drawable.";
        throw sys::Exception(sstr.str().c_str(), "View Node Position Getter");
    }
}

//////////////////
// class Client
//////////////////
// explicit
view::Client::Client(const char* jsonCfg):
    ClientBase  (),
    _pImpl      (new PImpl(*this, jsonCfg))
{ }

view::Client::~Client()
{
    dispose();
    delete _pImpl;
}

void
view::Client::build(Client::Ptr const&    ptr,
                    const char*           jsonCfg,
                    osg::GraphicsContext* graphicsContext)
{
    ClientBase::build(ptr);
    //---
    _pImpl->build(ptr, jsonCfg, graphicsContext);
}

void
view::Client::dispose()
{
    _pImpl->dispose();
    //---
    ClientBase::dispose();
}

const osg::Group*
view::Client::root() const
{
    return _pImpl->root();
}

osg::Group*
view::Client::root()
{
    return _pImpl->root();
}

const view::Client::Config&
view::Client::config() const
{
    return _pImpl->config();
}

view::AbstractDevice*
view::Client::device()
{
    return _pImpl->device();
}

const view::AbstractDevice*
view::Client::device() const
{
    return _pImpl->device();
}

view::gl::ContextExtensions::Ptr const&
view::Client::getOrCreateContextExtensions()
{
    sys::ResourceDatabase::Ptr const& resources = getOrCreateResourceDatabase();
    assert(resources);
    return gl::getOrCreateContextExtensions(*resources);
}

view::gl::ContextExtensions*
view::Client::getContextExtensions()
{
    sys::ResourceDatabase* resources = getResourceDatabase();
    return resources
        ? gl::getContextExtensions(*resources)
        : nullptr;
}

const view::gl::ContextExtensions*
view::Client::getContextExtensions() const
{
    const sys::ResourceDatabase* resources = getResourceDatabase();
    return resources
        ? gl::getContextExtensions(*resources)
        : nullptr;
}

void
view::Client::getDrawablePositions(unsigned int id, osg::Vec3Array& ret) const
{
    _pImpl->getDrawablePositions(id, ret);
}

// virtual
void
view::Client::handleSceneEvent(scn::Scene&              scene,
                               const xchg::SceneEvent&  evt)
{
    ClientBase::handleSceneEvent(scene, evt);
    //---
    _pImpl->handleSceneEvent(scene, evt);
}

// virtual
void
view::Client::handleSceneParmEvent(const xchg::ParmEvent& evt)
{
    ClientBase::handleSceneParmEvent(evt);
    //---
    _pImpl->handleSceneParmEvent(evt);
}

// virtual
ClientBindingBase::Ptr
view::Client::createBinding(scn::TreeNode::WPtr const& dataNode)
{
    return _pImpl->createBinding(dataNode);
}

// virtual
void
view::Client::destroyBinding(ClientBindingBase::Ptr const& bind)
{
    _pImpl->destroyBinding(bind);
}

void
view::Client::registerGUIEventCallbacks(scn::Scene& scene)
{
    _pImpl->registerGUIEventCallbacks(scene);
}

void
view::Client::deregisterGUIEventCallbacks(scn::Scene& scene)
{
    _pImpl->deregisterGUIEventCallbacks(scene);
}
