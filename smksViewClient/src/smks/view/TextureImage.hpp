// ========================================================================= //
// Copyright 2016-2018 SUPAMONKS_STUDIO                                      //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
//                                                                           //
// ========================================================================= //
#pragma once

#include <memory>
#include <osg/Image>
#include <boost/unordered_map.hpp>

#include <smks/img/AbstractImage.hpp>
#include <smks/sys/Exception.hpp>
#include <smks/sys/ImageResource.hpp>
#include <smks/sys/ResourceDatabase.hpp>

namespace smks
{
    class ClientBase;
    namespace xchg
    {
        class Image;
        class TileImages;
    }
    namespace view
    {
        namespace gl
        {
            class Sampler2dInput;
        }

        class TextureImage:
            public osg::Image
        {
        public:
            typedef osg::ref_ptr<TextureImage> Ptr;
        private:
            typedef std::weak_ptr<ClientBase>           ClientWPtr;
            typedef std::shared_ptr<gl::Sampler2dInput> SamplerPtr;
            typedef std::weak_ptr<gl::Sampler2dInput>   SamplerWPtr;
            typedef std::vector<SamplerWPtr>            Samplers;
        private:
            const ClientWPtr                _client;

            // image data directly loaded from file, accessible through the texture node's parameter set
            std::weak_ptr<xchg::Image>      _scnImage;
            std::weak_ptr<xchg::TileImages> _scnTimeImages;

            float*                          _glData;        //<! image display used for GL display (either downscaled or atlas version of the original)
            bool                            _ownsGLData;
            math::Vector4f                  _glTransformUV;
            math::Vector4f                  _transformUV;   //<! user-defined UV transform
            math::Vector4f                  _scale;

            Samplers                        _samplers;

        public:
            static
            Ptr
            create(unsigned int id, ClientWPtr const&);

        private:
            TextureImage(unsigned int id, ClientWPtr const&);
            // non-copyable
            TextureImage(const TextureImage&);
            TextureImage& operator=(const TextureImage&);

        public:
            ~TextureImage();

            void
            dispose();

            void
            registerSampler2dInput(SamplerPtr const&);

            void
            deregisterSampler2dInput(SamplerPtr const&);

            void
            setImage(std::shared_ptr<xchg::Image> const& = nullptr);

            void
            setTileImages(std::shared_ptr<xchg::TileImages> const& = nullptr);

            void
            setTransformUV(const math::Vector4f&);

            void
            setScale(const math::Vector4f&);

            bool
            valid() const;

            void
            loadGLData();

            void
            invalidate();

            virtual inline
            bool
            requiresUpdateCall() const
            {
                return true;
            }

            virtual inline
            void
            update(osg::NodeVisitor*)
            {
                if (!valid())
                    loadGLData();
            }

        protected:
            void
            disposeGLData();
            void
            setGLTransformUV(const math::Vector4f&);

            void
            loadGLData(xchg::Image&);
            void
            loadGLData(xchg::TileImages&);
            void
            synchronizeSampler2dInputs();
        };
    }
}
