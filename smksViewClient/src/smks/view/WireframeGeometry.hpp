// ========================================================================= //
// Copyright 2016-2018 SUPAMONKS_STUDIO                                      //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
//                                                                           //
// ========================================================================= //
#pragma once

#include <memory>
#include <osg/Geometry>

#include <smks/xchg/BoundingBox.hpp>
#include <smks/xchg/Data.hpp>
#include <smks/xchg/DataStream.hpp>
#include <smks/xchg/Buffered.hpp>

namespace smks
{
    class ClientBase;
    namespace view
    {
        namespace gl
        {
            class ContextExtensions;
            class UniformInput;
        }
        class PolyMeshGeometry;

        class WireframeGeometry:
            public osg::Geometry
        {
        public:
            typedef osg::ref_ptr<WireframeGeometry>         Ptr;
        private:
            typedef std::weak_ptr<ClientBase>               ClientWPtr;
            typedef std::shared_ptr<gl::UniformInput>       UniformPtr;
            typedef std::shared_ptr<gl::ContextExtensions>  ContextExtensionsPtr;
        private:
            ///////////////////////
            // struct TextureObject
            ///////////////////////
            struct TextureObject
            {
                int tex;
            public:
                inline TextureObject(): tex(-1) { }
                inline operator int() const { return tex; }
            };
            typedef xchg::Buffered<TextureObject> PerContextTexture;
        private:
            const unsigned int                  _id;

            const ClientWPtr                    _client;
            ContextExtensionsPtr                _extensions;

            const osg::Camera::BufferComponent  _objectIdAttachment;
            const GLenum                        _objectIdAttachmentGL;
            mutable PerContextTexture           _objectIdTexture; //<! -1 if unset
            UniformPtr                          _uColor;

        public:
            static
            Ptr
            create(unsigned int id, ClientWPtr const&, osg::Camera::BufferComponent);

        public:
            WireframeGeometry(unsigned int id, ClientWPtr const&, osg::Camera::BufferComponent);

            void
            setIndices(xchg::Data::Ptr const&);

            void
            setColor(const osg::Vec4f&);

            virtual
            osg::BoundingBoxf
            computeBoundingBox() const;

            virtual
            void
            drawImplementation(osg::RenderInfo&) const;

        private:
            osg::Program*
            loadProgram() const;

            const PolyMeshGeometry*
            getPolyMeshGeometry() const;
        };
    }
}
