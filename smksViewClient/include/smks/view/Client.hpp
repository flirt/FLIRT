// ========================================================================= //
// Copyright 2016-2018 SUPAMONKS_STUDIO                                      //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
//                                                                           //
// ========================================================================= //
#pragma once

#include <memory>
#include <osg/observer_ptr>
#include <osg/Array>

#include <smks/sys/Exception.hpp>
#include <smks/math/types.hpp>
#include <smks/xchg/NodeClass.hpp>
#include <smks/ClientBase.hpp>

#include "smks/sys/configure_view_client.hpp"

namespace osg
{
    class Group;
    class Program;
    class CameraProjection;
    class Uniform;
    class GraphicsContext;
}

namespace smks
{
    class ClientBindingBase;
    namespace xchg
    {
        class IdSet;
        class SceneEvent;
        class ParmEvent;
        template<class EventType> class AbstractObserver;
    }
    namespace scn
    {
        class TreeNode;
        class Scene;
    }
    namespace view
    {
        namespace gl
        {
            class ContextExtensions;
        }
        class ClientConfig;
        class AbstractDevice;

        class FLIRT_VIEW_CLIENT_EXPORT Client:
            public ClientBase
        {
        public:
            typedef std::shared_ptr<Client> Ptr;
            typedef std::weak_ptr<Client>   WPtr;
        private:
            typedef std::weak_ptr<scn::TreeNode>            TreeNodeWPtr;
            typedef std::shared_ptr<scn::Scene>             ScenePtr;
            typedef std::shared_ptr<ClientBindingBase>      ClientBindingBasePtr;
            typedef std::shared_ptr<gl::ContextExtensions>  ContextExtensionsPtr;
        public:
            ///////////////
            // class Config
            ///////////////
            class FLIRT_VIEW_CLIENT_EXPORT Config
            {
            public:
                const char*  deviceModule() const;
                unsigned int glTextureSize() const;
                unsigned int glUdimTileSize() const;
            private:
                struct PImpl;
            private:
                PImpl* _pImpl;
            public:
                Config();
                Config(const Config&);
                explicit Config(const char*);
                Config& operator=(const Config&);
                ~Config();
            private:
                void setDefaults();
            };
            ////////////////
        private:
            class PImpl;
            class SceneObserver;
            class ParmObserver;

            struct MouseEventCallback;
            struct MousePressEventCallback;
            struct MouseMoveEventCallback;
            struct MouseReleaseEventCallback;
            struct WheelEventCallback;
            struct ResizeEventCallback;
            struct KeyEventCallback;
            struct KeyPressEventCallback;
            struct KeyReleaseEventCallback;
            struct PaintGLEventCallback;

            class KeyMap;
        private:
            PImpl *_pImpl;

        public:
            static inline
            Ptr
            create(const char*           jsonCfg         = "",
                   osg::GraphicsContext* graphicsContext = nullptr)
            {
                Ptr ptr(new Client(jsonCfg));
                ptr->build(ptr, jsonCfg, graphicsContext);
                return ptr;
            }

            static inline
            void
            destroy(Ptr& ptr)
            {
                if (ptr)
                    ptr->dispose();
                ptr = nullptr;
            }

        protected:
            explicit
            Client(const char* jsonCfg);
        private:
            // non-copyable
            Client(const Client&);
            Client& operator=(const Client&);

        protected:
            virtual
            void
            build(Ptr const&,
                  const char* jsonCfg,
                  osg::GraphicsContext*);   //<! creates OSG viewer

            virtual
            void
            dispose();  //<! destroys OSG viewer

        public:
            ~Client();

            const Config&
            config() const;

            const osg::Group*
            root() const;

            osg::Group*
            root();

            AbstractDevice*
            device();

            const AbstractDevice*
            device() const;

            void
            getDrawablePositions(unsigned int,
                                 osg::Vec3Array&) const;

            ContextExtensionsPtr const&
            getOrCreateContextExtensions();

            gl::ContextExtensions*
            getContextExtensions();

            const gl::ContextExtensions*
            getContextExtensions() const;

        protected:
            virtual
            void
            handleSceneEvent(scn::Scene&, const xchg::SceneEvent&);

            virtual
            void
            handleSceneParmEvent(const xchg::ParmEvent&);

            virtual
            ClientBindingBasePtr
            createBinding(TreeNodeWPtr const&);

            virtual
            void
            destroyBinding(ClientBindingBasePtr const&);

            // register GUI event callbacks
            ///////////////////////////////
            void
            registerGUIEventCallbacks(scn::Scene&);

            void
            deregisterGUIEventCallbacks(scn::Scene&);
        };
    }
}
