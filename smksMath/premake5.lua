-- ========================================================================= --
-- Copyright 2016-2018 SUPAMONKS_STUDIO                                      --
-- Author: Pierre-Edouard Landes <pel@supamonks.com>                         --
--                                                                           --
-- This program is free software: you can redistribute it and/or modify      --
-- it under the terms of the GNU Lesser General Public License as            --
-- published by the Free Software Foundation, either version 3 of the        --
-- License, or (at your option) any later version.                           --
--                                                                           --
-- This program is distributed in the hope that it will be useful,           --
-- but WITHOUT ANY WARRANTY; without even the implied warranty of            --
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             --
-- GNU Lesser General Public License for more details.                       --
--                                                                           --
-- You should have received a copy of the GNU Lesser General Public License  --
-- along with this program.  If not, see <https://www.gnu.org/licenses/>.    --
--                                                                           --
-- ========================================================================= --


smks.module.uses_math = function ()

    smks.module.links(
        smks.module.name.math,
        smks.module.kind.math)

    smks.dep.eigen.includes()

    smks.module.includes(smks.module.name.xchg)

end


smks.module.copy_math_to_targetdir = function()

    smks.module.copy_to_targetdir(
        smks.module.name.math)

    smks.dep.tbb.copy_to_targetdir()

end


project(smks.module.name.math)

    language 'C++'
    smks.module.set_kind(
        smks.module.kind.math)

    files
    {
        'include/**.hpp',
        'src/**.cpp',
    }
    defines
    {
        'FLIRT_MATH_DLL',
    }
    includedirs
    {
        'include',
    }

    smks.dep.eigen.includes()
    smks.dep.boost.includes()
    smks.dep.ilmbase.links()
    smks.dep.tbb.links()

    smks.module.includes(smks.module.name.sys)
    smks.module.includes(smks.module.name.xchg)
