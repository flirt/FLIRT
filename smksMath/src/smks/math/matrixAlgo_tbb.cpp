// ========================================================================= //
// Copyright 2016-2018 SUPAMONKS_STUDIO                                      //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
//                                                                           //
// ========================================================================= //
#include "tbb/parallel_for.h"
#include "tbb/parallel_reduce.h"
#include "tbb/blocked_range.h"

#include <smks/xchg/DataStream.hpp>
#include "smks/math/matrixAlgo.hpp"

namespace smks { namespace math
{
    //////////////////////////////
    // struct TransformPointBody
    //////////////////////////////
    struct TransformPointBody
    {
        const Affine3f&         xfm;
        const xchg::DataStream& input;
        math::Vector4fv&        output;
    public:
        TransformPointBody(const Affine3f&          xfm,
                           const xchg::DataStream&  input,
                           math::Vector4fv&         output):
            xfm(xfm), input(input), output(output)
        { }

        void
        operator()(const tbb::blocked_range<int>& range) const
        {
            for (int i = range.begin(); i != range.end(); ++i)
            {
                const float* p = input.getPtr<float>(i);

                transformPoint(
                    xfm.data(),
                    math::Vector4f(p[0], p[1], p[2], 1.0f).data(),
                    output[i].data());
            }
        }
    };

    //////////////////////////////////
    // struct TransformDirectionBody
    //////////////////////////////////
    struct TransformDirectionBody
    {
        const Affine3f&         xfm;
        const xchg::DataStream& input;
        math::Vector4fv&        output;
    public:
        TransformDirectionBody(const Affine3f&          xfm,
                               const xchg::DataStream&  input,
                               math::Vector4fv&         output):
            xfm(xfm), input(input), output(output)
        { }

        void
        operator()(const tbb::blocked_range<int>& range) const
        {
            for (int i = range.begin(); i != range.end(); ++i)
            {
                const float* p = input.getPtr<float>(i);

                transformVector(
                    xfm.data(),
                    math::Vector4f(p[0], p[1], p[2], 0.0f).data(),
                    output[i].data());
                output[i] = math::normalize3(output[i]);
            }
        }
    };

    ////////////////////
    // struct LerpBody
    ////////////////////
    struct LerpBody
    {
        const math::Vector4fv&  input0;
        const math::Vector4fv&  input1;
        float                   w0, w1;
        math::Vector4fv&        output;
    public:
        LerpBody(const math::Vector4fv& input0,
                 const math::Vector4fv& input1,
                 float                  w1,
                 math::Vector4fv&       output):
            input0(input0), input1(input1),
            w0(1.0f - w1), w1(w1),
            output(output)
        { }

        void
        operator()(const tbb::blocked_range<int>& range) const
        {
            for (int i = range.begin(); i != range.end(); ++i)
                output[i] = w0 * input0[i] + w1 * input1[i];
        }
    };

    ////////////////////////
    // struct MinReduceBody
    ////////////////////////
    struct MinReduceBody
    {
        const math::Vector4fv&  input;
        math::Vector4f          output;
    public:
        explicit
        MinReduceBody(const math::Vector4fv& input):
            input(input), output(math::Vector4f::Constant(FLT_MAX))
        { }
        MinReduceBody(MinReduceBody& other, tbb::split):
            input(other.input), output(math::Vector4f::Constant(FLT_MAX))
        { }

        void
        operator()(const tbb::blocked_range<int>& range)
        {
            math::Vector4f tmp(output);
            for (int i = range.begin(); i != range.end(); ++i)
                tmp = input[i].cwiseMin(tmp);
            output = tmp;
        }
        void
        join(MinReduceBody& other)
        {
            output = other.output.cwiseMin(output);
        }
    };

    ////////////////////////
    // struct MaxReduceBody
    ////////////////////////
    struct MaxReduceBody
    {
        const math::Vector4fv&  input;
        math::Vector4f          output;
    public:
        explicit
        MaxReduceBody(const math::Vector4fv& input):
            input(input), output(math::Vector4f::Constant(-FLT_MAX))
        { }
        MaxReduceBody(MaxReduceBody& other, tbb::split):
            input(other.input), output(math::Vector4f::Constant(-FLT_MAX))
        { }

        void
        operator()(const tbb::blocked_range<int>& range)
        {
            math::Vector4f tmp(output);
            for (int i = range.begin(); i != range.end(); ++i)
                tmp = input[i].cwiseMax(tmp);
            output = tmp;
        }
        void
        join(MaxReduceBody& other)
        {
            output = other.output.cwiseMax(output);
        }
    };
}
}

using namespace smks;

void
math::transformPoints(const math::Affine3f&     xfm,
                      const xchg::DataStream&   stream,
                      math::Vector4fv&          pts)
{
    if (!stream || stream.numElements() == 0)
    {
        pts.clear();
        return;
    }

    TransformPointBody op(xfm, stream, pts);

    pts.resize(stream.numElements());
    tbb::parallel_for(tbb::blocked_range<int>(0, stream.numElements()), op);
}

void
math::transformDirections(const math::Affine3f&     xfm,
                          const xchg::DataStream&   stream,
                          math::Vector4fv&          dirs)
{
    if (!stream || stream.numElements() == 0)
    {
        dirs.clear();
        return;
    }

    TransformDirectionBody op(xfm, stream, dirs);

    dirs.resize(stream.numElements());
    tbb::parallel_for(tbb::blocked_range<int>(0, stream.numElements()), op);
}

void
math::lerp(const Vector4fv& x0, const Vector4fv& x1, float w1, Vector4fv& x)
{
    if (x0.empty())
    {
        x.clear();
        return;
    }
    assert(x1.size() == x0.size());

    LerpBody op(x0, x1, w1, x);

    x.resize(x0.size());
    tbb::parallel_for(tbb::blocked_range<int>(0, x0.size()), op);
}

void
math::max(const Vector4fv& pts, Vector4f& ret)
{
    MaxReduceBody op(pts);

    tbb::parallel_reduce(tbb::blocked_range<int>(0, pts.size()), op);
    ret = op.output;
}

void
math::min(const Vector4fv& pts, Vector4f& ret)
{
    MinReduceBody op(pts);

    tbb::parallel_reduce(tbb::blocked_range<int>(0, pts.size()), op);
    ret = op.output;
}
