// ========================================================================= //
// Copyright 2016-2018 SUPAMONKS_STUDIO                                      //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
//                                                                           //
// ========================================================================= //
#include <cassert>
#include "smks/math/math.hpp"
#include "smks/math/matrixAlgo.hpp"
#include "smks/math/types_eigen.hpp"

using namespace smks;

bool
xchg::areDirectionsEqual(const math::Vector4f& a, const math::Vector4f& b)
{
    assert(math::abs(math::len3(a) - 1.0f) < 1e-3f);
    assert(math::abs(math::len3(b) - 1.0f) < 1e-3f);
    return math::abs(math::dot3(a, b) - 1.0f) < 1e-4f;
}
