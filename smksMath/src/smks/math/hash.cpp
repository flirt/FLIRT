// ========================================================================= //
// Copyright 2016-2018 SUPAMONKS_STUDIO                                      //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
//                                                                           //
// ========================================================================= //
#include <iostream>
#include <cassert>
#include <boost/unordered_map.hpp>
#include "smks/math/math.hpp"
#include "smks/math/hash.hpp"

namespace smks { namespace math
{
    int
    ipow(int base, int exp)
    {
        assert(exp >= 0);
        int result = 1;
        while (exp)
        {
            if (exp & 1)
                result *= base;
            exp >>= 1;
            base *= base;
        }
        return result;
    }

    void
    hash_combine_translation(size_t& seed, const Eigen::Vector3f& vec3)
    {
        const int   pow10   = max<int>(0, 5 - static_cast<int>(log10f(vec3.cwiseAbs2().sum()))/2);
        const float mult10  = static_cast<float>(ipow(10, pow10));
        for (size_t n = 0; n < 3; ++n)
        {
            int icoeff = mult10 * vec3(n);
            std::cout << "transl[" << n << "] = " << icoeff << std::endl;
            boost::hash_combine(seed, icoeff);
        }
    }

    void
    hash_combine_linear(size_t& seed, const Eigen::Matrix3f& mat3)
    {
        const int   pow10   = max<int>(0, 5 - static_cast<int>(log10f(mat3.cwiseAbs2().sum()))/2);
        const float mult10  = static_cast<float>(ipow(10, pow10));
        for (size_t r = 0; r < 3; ++r)
            for (size_t c = 0; c < 3; ++c)
            {
                int icoeff = mult10 * mat3(r, c);
                std::cout << "linear(" << r << ", " << c << ") = " << icoeff << std::endl;
                boost::hash_combine(seed, icoeff);
            }
    }
}
}

using namespace smks;

size_t
math::hash(const Affine3f& xfm)
{
    size_t seed = 0;
    hash_combine(seed, xfm);
    return seed;
}

void
math::hash_combine(size_t& seed, const Affine3f& xfm)
{
    const int       pow10   = max<int>(0, 5 - static_cast<int>(log10f(xfm.matrix().cwiseAbs2().sum()))/2);
    const float     mult10  = static_cast<float>(ipow(10, pow10));
    Eigen::Matrix4i imat    ((mult10 * xfm.matrix()).cast<int>());

    for (size_t r = 0; r < 3; ++r)
        for (size_t c = 0; c < 4; ++c)
            boost::hash_combine(seed, imat(r, c));

    //hash_combine_translation(seed, xfm.translation());
    //hash_combine_linear(seed, xfm.linear());
}
