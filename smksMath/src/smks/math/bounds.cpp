// ========================================================================= //
// Copyright 2016-2018 SUPAMONKS_STUDIO                                      //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
//                                                                           //
// ========================================================================= //
#include "smks/math/math.hpp"
#include "smks/math/bounds.hpp"

using namespace smks;


bool
math::transform(const math::Affine3f&       xfm,
                const xchg::BoundingBox&    bounds,
                Vector4f&                   minBounds,
                Vector4f&                   maxBounds)
{
    if (!bounds.valid())
    {
        minBounds.setConstant(FLT_MAX);
        maxBounds.setConstant(-FLT_MAX);
        return false;
    }

    Vector4fv corners(8);

    for (size_t i = 0; i < 8; ++i)
        corners[i] = transformPoint(xfm, bounds.corner(i));
    min(corners, minBounds);
    max(corners, maxBounds);

    return !anyGreaterThan(minBounds, maxBounds);
}

bool
math::transform(const math::Affine3f&       xfm,
                const xchg::BoundingBox&    bounds,
                xchg::BoundingBox&          ret)
{
    math::Vector4f minBounds;
    math::Vector4f maxBounds;

    return transform(xfm, bounds, minBounds, maxBounds)
        ? ret.initialize(
            minBounds.x(), minBounds.y(), minBounds.z(),
            maxBounds.x(), maxBounds.y(), maxBounds.z())
        : false;
}
