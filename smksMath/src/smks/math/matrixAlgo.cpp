// ========================================================================= //
// Copyright 2016-2018 SUPAMONKS_STUDIO                                      //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
//                                                                           //
// ========================================================================= //
#include <ImathMatrixAlgo.h>
#include <smks/sys/Exception.hpp>
#include "smks/math/matrixAlgo.hpp"

using namespace smks;

math::Affine3f
math::normalMatrix(const Affine3f& xfm)
{
    Affine3f ret;

    ret.matrix().setZero();
    ret.matrix().block(0, 0, 3, 3) = xfm.linear().inverse().transpose();

    return ret;
}

float
math::scaling(const Affine3f& xfm)
{
    return powf(xfm.linear().determinant(), 1.0f/3.0f);
}

void
math::lookAt(Affine3f&          xfm,
             const Vector4f&    eyePosition,
             const Vector4f&    forwardVector,
             const Vector4f&    upVector,
             const Vector4f&    scale)
{
    Vector4f    F       = forwardVector;
    const float lenF    = math::len3(F);
    if (lenF > 1e-6f)
        F *= math::rcp(lenF);
    else
    {
        F = -math::Vector4f::UnitZ();
        std::cerr << "forward vector explicitly set to " << F.transpose() << " for computing look-at transform." << std::endl;
    }

    Vector4f    U       = upVector;
    const float lenU    = math::len3(U);
    if (lenU > 1e-6f)
        U *= math::rcp(lenU);
    else
    {
        U = -math::Vector4f::UnitZ();
        std::cerr << "up vector explicitly set to " << U.transpose() << " for computing look-at transform." << std::endl;
    }

    Vector4f S = math::normalized3(F.cross3(U));

    U = math::normalized3(S.cross3(F));
    xfm.matrix().col(0) << S[0], S[1], S[2], 0.0f;
    xfm.matrix().col(1) << U[0], U[1], U[2], 0.0f;
    xfm.matrix().col(2) << -F[0], -F[1], -F[2], 0.0f;
    xfm.matrix().col(3) << eyePosition[0], eyePosition[1], eyePosition[2], 1.0f;
    xfm.makeAffine();

    xfm *= Eigen::Scaling(scale[0], scale[1], scale[2]);
}

void
math::lookAtPoint(Affine3f&         xfm,
                  const Vector4f&   eyePosition,
                  const Vector4f&   targetPosition,
                  const Vector4f&   upVector,
                  const Vector4f&   scale)
{
    lookAt(xfm, eyePosition, targetPosition - eyePosition, upVector, scale);
}

void
math::composeEuler(Affine3f&        xfm,
                   const Vector4f&  translate,
                   RotateOrder      order,
                   const Vector4f&  rotateD,
                   const Vector4f&  scale)
{
    Eigen::Vector3f axis[3] = {
        Eigen::Vector3f::UnitX(),
        Eigen::Vector3f::UnitY(),
        Eigen::Vector3f::UnitZ() };

    unsigned int r[3];

    getRotationOrderShuffle(order, r);

    const float euler[3] = { rotateD[r[2]], rotateD[r[1]], rotateD[r[0]] };

    xfm = Eigen::Translation3f(translate.head(3)) *
        Eigen::AngleAxisf(euler[0] * static_cast<float>(sys::degrees_to_radians), axis[r[2]]) *
        Eigen::AngleAxisf(euler[1] * static_cast<float>(sys::degrees_to_radians), axis[r[1]]) *
        Eigen::AngleAxisf(euler[2] * static_cast<float>(sys::degrees_to_radians), axis[r[0]]) *
        Eigen::Scaling(scale.head(3));
}

void
math::compose(Affine3f&             xfm,
              const Vector4f&       translate,
              const Quaternionf&    rotate,
              const Vector4f&       scale)
{
    assert(math::abs(rotate.norm() - 1.0f) < 1e-6f);
    xfm = Eigen::Translation3f(translate.head(3)) *
        rotate *
        Eigen::Scaling(scale.head(3));
}

void
math::decompose(const Affine3f& xfm,
                Vector4f&       translate,
                RotateOrder     order,
                Vector4f&       rotateD,
                Vector4f&       scale,
                Vector4f*       forwardVector,
                Vector4f*       upVector)
{
    Eigen::Matrix3f rotate_, scale_;

    xfm.computeRotationScaling(&rotate_, &scale_);

    for (size_t i = 0; i < 3; ++i)
        scale[i] = scale_(i,i);
    scale[3] = 1.0f;

    unsigned int r[3];
    switch (order)
    {
    case RotateOrder::XYZ: r[0] = 0; r[1] = 1; r[2] = 2; break;
    case RotateOrder::YZX: r[0] = 1; r[1] = 2; r[2] = 0; break;
    case RotateOrder::ZXY: r[0] = 2; r[1] = 0; r[2] = 1; break;
    case RotateOrder::XZY: r[0] = 0; r[1] = 2; r[2] = 1; break;
    case RotateOrder::YXZ: r[0] = 1; r[1] = 0; r[2] = 2; break;
    case RotateOrder::ZYX: r[0] = 2; r[1] = 1; r[2] = 0; break;
    default:
        throw sys::Exception("Invalid rotation order.", "Xform Decomposition");
    }

    Eigen::Vector3f euler = rotate_.eulerAngles(r[2], r[1], r[0]);  // returned values in [0, pi] x [-pi, pi] x [-pi, pi]
    for (size_t i = 0; i < 3; ++i)
        rotateD[r[2-i]] = euler[i] * static_cast<float>(sys::radians_to_degrees);
    rotateD[3] = 1.0f;

    const Eigen::Vector3f&  translate_ = xfm.translation();
    memcpy(translate.data(), translate_.data(), sizeof(float) * 3);
    translate[3] = 1.0f;

    if (forwardVector)
    {
        memcpy(forwardVector->data(), rotate_.col(2).data(), sizeof(float) * 3);
        (*forwardVector)[3] = 0.0f;
        (*forwardVector)    *= -1.0f;
    }

    if (upVector)
    {
        memcpy(upVector->data(), rotate_.col(1).data(), sizeof(float) * 3);
        (*upVector)[3] = 0.0f;
        upVector->normalize();
    }
}

void
math::decompose(const Affine3f& xfm,
                Vector4f&       translate,
                Quaternionf&    rotate,
                Vector4f&       scale)
{
    Eigen::Matrix3f rotate_, scale_;

    xfm.computeRotationScaling(&rotate_, &scale_);

    for (size_t i = 0; i < 3; ++i)
        scale[i] = scale_(i,i);
    scale[3] = 1.0f;

    rotate = Quaternionf(rotate_).normalized();

    const Eigen::Vector3f&  translate_ = xfm.translation();
    memcpy(translate.data(), translate_.data(), sizeof(float) * 3);
    translate[3] = 1.0f;
}

// erases scaling from specified matrix
FLIRT_MATH_EXPORT
void
math::removeScaling(const Affine3f& xfm, Affine3f& scaleless, Vector4f* scale)
{
    Eigen::Matrix3f rotate, scale_;

    xfm.computeRotationScaling(&rotate, &scale_);

    scaleless.setIdentity();
    scaleless.translate(xfm.translation());
    scaleless.rotate(rotate);

    if (scale)
    {
        (*scale)[0] = scale_(0,0);
        (*scale)[1] = scale_(1,1);
        (*scale)[2] = scale_(2,2);
        (*scale)[3] = 1.0f;
    }
}

void
math::interpolateXform(const Affine3f&      xfm0,
                       const Affine3f&      xfm1,
                       std::vector<float>&  samples,
                       Affine3fv&           ret)
{
    ret.clear();
    if (samples.empty())
        return;

    // decompose the two matrices into interpolable components
    Imath::M44f     mat0, mat1;
    Imath::V3f      trl0, scl0, shr0;
    Imath::V3f      trl1, scl1, shr1;

    memcpy(mat0.getValue(), xfm0.data(), sizeof(float) << 4);
    memcpy(mat1.getValue(), xfm1.data(), sizeof(float) << 4);

    memcpy(trl0.getValue(), mat0[3], sizeof(float) * 3);
    memcpy(trl1.getValue(), mat1[3], sizeof(float) * 3);

    if (!Imath::extractAndRemoveScalingAndShear(mat0, scl0, shr0, true) ||
        !Imath::extractAndRemoveScalingAndShear(mat1, scl1, shr1, true))
        throw sys::Exception("Failed to decompose xforms.", "Xform Interpolation");

    const Imath::Quatf& rot0 = Imath::extractQuat<float>(mat0);
    const Imath::Quatf& rot1 = Imath::extractQuat<float>(mat1);

    ret.resize(samples.size());
    for (size_t i = 0; i < samples.size(); ++i)
    {
        const float w1 = std::max(0.0f, std::min(1.0f, samples[i]));
        const float w0 = 1.0f - w1;

        const Imath::Quatf& rot = Imath::slerpShortestArc<float>(rot0, rot1, w0);
        const Imath::V3f&   trl = w0 * trl0 + w1 * trl1;
        const Imath::V3f&   scl = w0 * scl0 + w1 * scl1;
        const Imath::V3f&   shr = w0 * shr0 + w1 * shr1;

        Imath::M44f matrix = rot.toMatrix44();
        matrix.shear(shr);
        matrix.scale(scl);
        memcpy(matrix[3], trl.getValue(), sizeof(float) * 3);

        memcpy(ret[i].data(), matrix.getValue(), sizeof(float) << 4);
    }
}
