// ========================================================================= //
// Copyright 2016-2018 SUPAMONKS_STUDIO                                      //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
//                                                                           //
// ========================================================================= //
#pragma once

#include <cmath>
#include <float.h>
#include <emmintrin.h>
#include <xmmintrin.h>

#include <smks/sys/constants.hpp>

namespace smks { namespace math
{
#if defined(__WIN32__)
    __forceinline bool finite ( float x ) { return _finite(x) != 0; }
#endif

    __forceinline bool isnan(float x)   { return x != x; }
    __forceinline bool isnan(double x)  { return x != x; }

#ifndef __MIC__
    __forceinline float rcp  ( float x )
    {
        const __m128 vx = _mm_set_ss(x);
        const __m128 r = _mm_rcp_ps(vx);
        return _mm_cvtss_f32(_mm_sub_ps(_mm_add_ps(r, r), _mm_mul_ps(_mm_mul_ps(r, r), vx)));
    }
    __forceinline float signmsk ( float x )
    {
        return _mm_cvtss_f32(_mm_and_ps(_mm_set_ss(x),_mm_castsi128_ps(_mm_set1_epi32(0x80000000))));
    }
    __forceinline float xorf( float x, float y )
    {
        return _mm_cvtss_f32(_mm_xor_ps(_mm_set_ss(x),_mm_set_ss(y)));
    }
    __forceinline float andf( float x, const unsigned y )
    {
        return _mm_cvtss_f32(_mm_and_ps(_mm_set_ss(x),_mm_castsi128_ps(_mm_set1_epi32(y))));
    }
    __forceinline float rsqrt( float x )
    {
        const __m128 a = _mm_set_ss(x);
        const __m128 r = _mm_rsqrt_ps(a);
        const __m128 c = _mm_add_ps(_mm_mul_ps(_mm_set_ps(1.5f, 1.5f, 1.5f, 1.5f), r),
            _mm_mul_ps(_mm_mul_ps(_mm_mul_ps(a, _mm_set_ps(-0.5f, -0.5f, -0.5f, -0.5f)), r), _mm_mul_ps(r, r)));
        return _mm_cvtss_f32(c);
    }
#else
    __forceinline float rcp  ( float x ) { return 1.0f/x; }
    __forceinline float signmsk ( float x ) { return cast_i2f(cast_f2i(x)&0x80000000); }
    __forceinline float xorf( float x, float y ) { return cast_i2f(cast_f2i(x) ^ cast_f2i(y)); }
    __forceinline float andf( float x, float y ) { return cast_i2f(cast_f2i(x) & cast_f2i(y)); }
    __forceinline float rsqrt( float x ) { return 1.0f/sqrtf(x); }
#endif

    __forceinline float abs  ( float x )            { return ::fabsf(x);        }
    __forceinline float sign ( float x )            { return x<0.0f?-1.0f:1.0f; }
    __forceinline float acos ( float x )            { return ::acosf (x);       }
    __forceinline float asin ( float x )            { return ::asinf (x);       }
    __forceinline float atan ( float x )            { return ::atanf (x);       }
    __forceinline float atan2( float y, float x )   { return ::atan2f(y, x);    }
    __forceinline float cos  ( float x )            { return ::cosf  (x);       }
    __forceinline float cosh ( float x )            { return ::coshf (x);       }
    __forceinline float exp  ( float x )            { return ::expf  (x);       }
    __forceinline float fmod ( float x, float y )   { return ::fmodf (x, y);    }
    __forceinline float log  ( float x )            { return ::logf  (x);       }
    __forceinline float log10( float x )            { return ::log10f(x);       }
    __forceinline float pow  ( float x, float y )   { return ::powf  (x, y);    }
    // rcp and rsqrt are optimized with SSEE2 instructions whenever possible.
    __forceinline float sin  ( float x )            { return ::sinf  (x);       }
    __forceinline float sinh ( float x )            { return ::sinhf (x);       }
    __forceinline float sqr  ( float x )            { return x * x;             }
    __forceinline float sqrt ( float x )            { return ::sqrtf (x);       }
    __forceinline float tan  ( float x )            { return ::tanf  (x);       }
    __forceinline float tanh ( float x )            { return ::tanhf (x);       }
    __forceinline float floor( float x )            { return ::floorf (x);      }
    __forceinline float ceil ( float x )            { return ::ceilf (x);       }

    __forceinline double abs  ( double x )              { return ::fabs(x);     }
    __forceinline double sign ( double x )              { return x<0?-1.0:1.0;  }
    __forceinline double acos ( double x )              { return ::acos (x);    }
    __forceinline double asin ( double x )              { return ::asin (x);    }
    __forceinline double atan ( double x )              { return ::atan (x);    }
    __forceinline double atan2( double y, double x )    { return ::atan2(y, x); }
    __forceinline double cos  ( double x )              { return ::cos  (x);    }
    __forceinline double cosh ( double x )              { return ::cosh (x);    }
    __forceinline double exp  ( double x )              { return ::exp  (x);    }
    __forceinline double fmod ( double x, double y )    { return ::fmod (x, y); }
    __forceinline double log  ( double x )              { return ::log  (x);    }
    __forceinline double log10( double x )              { return ::log10(x);    }
    __forceinline double pow  ( double x, double y )    { return ::pow  (x, y); }
    __forceinline double rcp  ( double x )              { return 1.0/x;         }
    __forceinline double rsqrt( double x )              { return 1.0/::sqrt(x); }
    __forceinline double sin  ( double x )              { return ::sin  (x);    }
    __forceinline double sinh ( double x )              { return ::sinh (x);    }
    __forceinline double sqr  ( double x )              { return x * x;         }
    __forceinline double sqrt ( double x )              { return ::sqrt (x);    }
    __forceinline double tan  ( double x )              { return ::tan  (x);    }
    __forceinline double tanh ( double x )              { return ::tanh (x);    }
    __forceinline double floor( double x )              { return ::floor (x);   }
    __forceinline double ceil ( double x )              { return ::ceil (x);    }

    template<typename T> __forceinline T min(const T& a, const T& b)                                     { return a<b? a:b; }
    template<typename T> __forceinline T min(const T& a, const T& b, const T& c)                         { return min(min(a,b),c); }
    template<typename T> __forceinline T min(const T& a, const T& b, const T& c, const T& d)             { return min(min(a,b),min(c,d)); }
    template<typename T> __forceinline T min(const T& a, const T& b, const T& c, const T& d, const T& e) { return min(min(min(a,b),min(c,d)),e); }

    template<typename T> __forceinline T max(const T& a, const T& b)                                     { return a<b? b:a; }
    template<typename T> __forceinline T max(const T& a, const T& b, const T& c)                         { return max(max(a,b),c); }
    template<typename T> __forceinline T max(const T& a, const T& b, const T& c, const T& d)             { return max(max(a,b),max(c,d)); }
    template<typename T> __forceinline T max(const T& a, const T& b, const T& c, const T& d, const T& e) { return max(max(max(a,b),max(c,d)),e); }

    template<typename T> __forceinline T clamp(const T& x, const T& lower = T(sys::zero), const T& upper = T(sys::one)) { return max(lower, min(x, upper)); }

    template<typename T> __forceinline T  deg2rad ( const T& x )  { return x * T(sys::degrees_to_radians); }
    template<typename T> __forceinline T  rad2deg ( const T& x )  { return x * T(sys::radians_to_degrees); }
    template<typename T> __forceinline T  sin2cos ( const T& x )  { return sqrt(max(T(sys::zero), T(sys::one)-x*x)); }
    template<typename T> __forceinline T  cos2sin ( const T& x )  { return sin2cos(x); }

#if defined(__AVX2__)
    __forceinline float madd  ( float a, float b, float c) { return _mm_cvtss_f32(_mm_fmadd_ss(_mm_set_ss(a),_mm_set_ss(b),_mm_set_ss(c))); }
    __forceinline float msub  ( float a, float b, float c) { return _mm_cvtss_f32(_mm_fmsub_ss(_mm_set_ss(a),_mm_set_ss(b),_mm_set_ss(c))); }
    __forceinline float nmadd ( float a, float b, float c) { return _mm_cvtss_f32(_mm_fnmadd_ss(_mm_set_ss(a),_mm_set_ss(b),_mm_set_ss(c))); }
    __forceinline float nmsub ( float a, float b, float c) { return _mm_cvtss_f32(_mm_fnmsub_ss(_mm_set_ss(a),_mm_set_ss(b),_mm_set_ss(c))); }
#else
    __forceinline float madd  ( float a, float b, float c) { return a*b+c; }
    __forceinline float msub  ( float a, float b, float c) { return a*b-c; }
    __forceinline float nmadd ( float a, float b, float c) { return -a*b-c;}
    __forceinline float nmsub ( float a, float b, float c) { return c-a*b; }
#endif

    /*! random functions */
    template<typename T> T          random() { return T(0); }
    template<> __forceinline int    random() { return int(rand()); }
    template<> __forceinline uint32 random() { return uint32(rand()); }
    template<> __forceinline float  random() { return random<uint32>()/static_cast<float>(RAND_MAX); }
    template<> __forceinline double random() { return random<uint32>()/static_cast<double>(RAND_MAX); }

    /*! selects */
    template<typename T> __forceinline  T       select(bool s, const T& t , const T& f) { return s ? t : f; }
    __forceinline                       __m128  select(__m128 mask, __m128 t, __m128 f) { return _mm_blendv_ps(f, t, mask); }

    __forceinline bool isp2(uint32_t x){ return (x & (x-1)) == 0; } //<! test whether x is a power of

    //<! find upper power of two
    inline
    uint32_t
    fup2(uint32_t x)
    {
        x--;
        x |= x >> 1;
        x |= x >> 2;
        x |= x >> 4;
        x |= x >> 8;
        x |= x >> 16;
        x++;
        return x;
    }

    //<! find lower power of two
    inline
    uint32_t
    flp2 (uint32_t x)
    {
        x |= x >> 1;
        x |= x >> 2;
        x |= x >> 4;
        x |= x >> 8;
        x |= x >> 16;
        return x - (x >> 1);
    }

    __forceinline
    float
    lerp(float a, float b, float x)
    {
        return a + x * (b-a);
    }

    //<! returns a smooth value between in the [0, 1] range.
    __forceinline
    float
    smoothstep(float a, float b, float x)
    {
        if (abs(a-b) < 1e-6f)
            return x < a ? 0.0f : 1.0f;
        const float t = clamp((x-a)*rcp(b-a));
        return t*t*(3.0f - 2.0f * t);
    }
} }

