// ========================================================================= //
// Modification copyright 2016-2018 SUPAMONKS_STUDIO                         //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
// _________________________________________________________________________ //
// Copyright 2013 John Burkardt                                              //
//                                                                           //
// This code is distributed under the GNU LGPL license.                      //
//                                                                           //
// Reference:                                                                //
//     George Marsaglia, Wai Wan Tsang,                                      //
//     The Ziggurat Method for Generating Random Variables,                  //
//     Journal of Statistical Software,                                      //
//     Volume 5, Number 8, October 2000, seven pages.                        //
//                                                                           //
// ========================================================================= //
#pragma once

#include <cstdint>
#include <smks/sys/configure_math.hpp>

namespace smks { namespace math { namespace ziggurat
{
    FLIRT_MATH_EXPORT
    uint32_t
    cong_seeded(uint32_t &jcong);

    FLIRT_MATH_EXPORT
    double
    cpu_time();

    FLIRT_MATH_EXPORT
    uint32_t
    kiss_seeded(uint32_t &jcong, uint32_t &jsr, uint32_t &w, uint32_t &z);

    FLIRT_MATH_EXPORT
    uint32_t
    mwc_seeded(uint32_t &w, uint32_t &z);

    FLIRT_MATH_EXPORT
    float
    r4_exp(uint32_t &jsr, uint32_t ke[256], float fe[256], float we[256]);

    FLIRT_MATH_EXPORT
    void
    r4_exp_setup(uint32_t ke[256], float fe[256], float we[256]);

    FLIRT_MATH_EXPORT
    float
    r4_nor(uint32_t &jsr, uint32_t kn[128], float fn[128], float wn[128]);

    FLIRT_MATH_EXPORT
    void
    r4_nor_setup(uint32_t kn[128], float fn[128], float wn[128]);

    FLIRT_MATH_EXPORT
    float
    r4_uni(uint32_t &jsr);

    FLIRT_MATH_EXPORT
    uint32_t
    shr3_seeded(uint32_t &jsr);

    FLIRT_MATH_EXPORT
    void
    timestamp();
}
}
}
