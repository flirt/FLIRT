// ========================================================================= //
// Copyright 2016-2018 SUPAMONKS_STUDIO                                      //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
//                                                                           //
// ========================================================================= //
#pragma once

//#include <smks/sys/constants.hpp>

#include "smks/sys/configure_math.hpp"
#include "smks/math/types.hpp"
#include "smks/math/math.hpp"
#include "smks/math/RotateOrder.hpp"
#include "smks/math/sse_special.hpp"

namespace smks {
    namespace xchg
    {
        class DataStream;
    }
    namespace math
    {
        __forceinline
        bool
        eq3(const Vector4f& a, const Vector4f& b)
        {
            return (_mm_movemask_ps(
                _mm_cmpeq_ps(
                    _mm_load_ps(a.data()),
                    _mm_load_ps(b.data()))) & 0x7) == 0x7;
        }

        __forceinline
        bool
        neq3(const Vector4f& a, const Vector4f& b)
        {
            return (_mm_movemask_ps(
                _mm_cmpneq_ps(
                    _mm_load_ps(a.data()),
                    _mm_load_ps(b.data()))) & 0x7) != 0;
        }

        __forceinline
        float
        dot3(const Vector4f& a, const Vector4f& b)
        {
#if defined(__SSE4_1__)
            return _mm_cvtss_f32(
                _mm_dp_ps(
                    _mm_load_ps(a.data()),
                    _mm_load_ps(b.data()),
                    0x7F));
#else
            return a.head<3>().dot(b.head<3>());
#endif // defined(__SSE4_1__)
        }

        __forceinline
        float
        len3(const Vector4f& a)
        {
            const float sqrLen3 = dot3(a,a);
            return sqrLen3 > 1e-6f
                ? sqrt(sqrLen3)
                : 0.0f;
        }

        __forceinline
        Vector4f&
        normalize3(Vector4f& a)
        {
            const float sqrLen3 = dot3(a,a);
            if (sqrLen3 > 1e-6f)
                a *= rsqrt(sqrLen3);
            return a;
        }

        __forceinline
        Vector4f
        normalized3(const Vector4f& a)
        {
            Vector4f a_(a);
            normalize3(a_);
            return a_;
        }

        __forceinline
        bool
        isNormalized3(const Vector4f& a, float eps=1e-1f)
        {
            return abs(sqrt(dot3(a,a)) - 1.0f) < eps;
        }

        __forceinline
        bool
        isVector3f(const Vector4f& v)
        {
            return abs(v.w()) < 1e-6f;
        }

        __forceinline
        bool
        isnan(const Vector2f& v)
        {
            return isnan(v.x()) || isnan(v.y());
        }

        __forceinline
        bool
        isnan(const Vector4f& v)
        {
            return isnan(v.x()) || isnan(v.y()) || isnan(v.z()) || isnan(v.w());
        }

        __forceinline
        bool
        anyGreaterThan(const Vector4f& a, const Vector4f& b)
        {
            return (a.array() > b.array()).any();
        }

        __forceinline
        void
        minFloat4(const float a[4], const float b[4], float ret[4]);

        __forceinline
        void
        maxFloat4(const float a[4], const float b[4], float ret[4]);

        __forceinline
        Vector4f
        min(const Vector4f& a, const Vector4f& b)
        {
            Vector4f ret;
            minFloat4(a.data(), b.data(), ret.data());
            return ret;
        }

        __forceinline
        Vector4f
        max(const Vector4f& a, const Vector4f& b)
        {
            Vector4f ret;
            maxFloat4(a.data(), b.data(), ret.data());
            return ret;
        }

        __forceinline
        Vector4f
        rsqrt(const Vector4f& a)
        {
            Vector4f ret;
            __m128 a128 = _mm_load_ps(a.data());
            __m128 r128 = _mm_rsqrt_ps(a128);
            _mm_store_ps(
                ret.data(),
                _mm_add_ps(_mm_mul_ps(_mm_set_ps1(1.5f), r128), _mm_mul_ps(_mm_mul_ps(_mm_mul_ps(a128, _mm_set_ps1(-0.5f)), r128), _mm_mul_ps(r128, r128))));
            return ret;
        }

        __forceinline
        Vector4f
        exp(const Vector4f& x)
        {
            Vector4f ret;
            _mm_store_ps(ret.data(), exp_ps(_mm_load_ps(x.data())));
            return ret;
        }

        __forceinline
        Vector4f
        log(const Vector4f& x)
        {
            Vector4f ret;
            _mm_store_ps(ret.data(), log_ps(_mm_load_ps(x.data())));
            return ret;
        }

        __forceinline
        Vector4f
        pow(const Vector4f& x, float y)
        {
            const float EPS = 1e-6f;

            // x^y approximated by exp(y * log(x))
            Vector4f ret;
            _mm_store_ps(ret.data(),
                exp_ps( _mm_mul_ps(
                    _mm_load_ps1(&y),
                    log_ps( _mm_max_ps(_mm_load_ps1(&EPS), _mm_load_ps(x.data())) )
                ) )
                );
            return ret;
        }

        __forceinline
        Vector4f
        pow(const Vector4f& x, const Vector4f& y)
        {
            const float EPS = 1e-6f;

            // x^y approximated by exp(y * log(x))
            Vector4f ret;
            _mm_store_ps(ret.data(),
                exp_ps( _mm_mul_ps(
                    _mm_load_ps(y.data()),
                    log_ps( _mm_max_ps(_mm_load_ps1(&EPS), _mm_load_ps(x.data())) )
                ) )
                );
            return ret;
        }

        __forceinline
        Vector2f
        floor(const math::Vector2f& x)
        {
            return Eigen::floor(x.array());
        }


        //<! constructs a coordinate frame form a normal.
        __forceinline
        void
        frame(const Vector4f& N, Affine3f& xfm)
        {
            const Vector4f& dx0 = Vector4f::UnitX().cross3(N);
            const Vector4f& dx1 = Vector4f::UnitY().cross3(N);
            const Vector4f& dx  = select<Vector4f>(math::dot3(dx0, dx0) > math::dot3(dx1, dx1), dx0, dx1).normalized();
            const Vector4f& dy  = N.cross3(dx).normalized();

            xfm.setIdentity();
            xfm.matrix().col(0) << dx;
            xfm.matrix().col(1) << dy;
            xfm.matrix().col(2) << N;
        }

        __forceinline
        void
        transform4x4Point(const float mat[16], const float p[4], float q[4]);

        //__forceinline
        //Vector4f
        //transform4x4Point(const Matrix4f& mat, const Vector4f& p)
        //{
        //  Vector4f q;
        //  transform4x4Point(mat.data(), p.data(), q.data());
        //  return q;
        //}

        // xfm parameter is expected to represent a column-major affine transform
        __forceinline
        void
        transformPoint(const float xfm[16], const float p[4], float q[4]);

        __forceinline
        Vector4f
        transformPoint(const Affine3f& xfm, const Vector4f& p)
        {
            Vector4f q;
            transformPoint(xfm.data(), p.data(), q.data());
            return q;
        }

        // xfm parameter is expected to represent a column-major affine transform
        __forceinline
        void
        transformVector(const float xfm[16], const float p[4], float q[4]);

        __forceinline
        Vector4f
        transformVector(const Affine3f& xfm, const Vector4f& p)
        {
            Vector4f q;
            transformVector(xfm.data(), p.data(), q.data());
            return q;
        }

        __forceinline
        Vector4f
        transformVectorInFrame(const Vector4f& frameNormal, const Vector4f& p)
        {
            Affine3f xfm;
            frame(frameNormal, xfm);
            return transformVector(xfm, p);
        }

        //---------------------------------------------
        // TBB-accelerated functions on vector4f arrays
        FLIRT_MATH_EXPORT
        void
        transformPoints(const Affine3f&, const xchg::DataStream&, Vector4fv&);
        FLIRT_MATH_EXPORT
        void
        transformDirections(const Affine3f&, const xchg::DataStream&, Vector4fv&);

        FLIRT_MATH_EXPORT
        void
        lerp(const Vector4fv&, const Vector4fv&, float, Vector4fv&);

        FLIRT_MATH_EXPORT
        void
        max(const Vector4fv&, Vector4f&);
        FLIRT_MATH_EXPORT
        void
        min(const Vector4fv&, Vector4f&);
        //---------------------------------------------

        // precompute the normal matrix as the transpose of the linear part's inverse
        FLIRT_MATH_EXPORT
        Affine3f
        normalMatrix(const Affine3f&);

        // returns the overall scaling induces by the specified affine transform
        FLIRT_MATH_EXPORT
        float
        scaling(const Affine3f&);

        // erases scaling from specified matrix
        FLIRT_MATH_EXPORT
        void
        removeScaling(const Affine3f&, Affine3f&, Vector4f* scale = nullptr);

        // produces a lookAt transform matrix with the following conventions:
        // - forward = (0, 0, -1)
        // - up = (0, 1, 0)
        FLIRT_MATH_EXPORT
        void
        lookAt(Affine3f&,
               const Vector4f&  eyePosition     = Vector4f::UnitW(),
               const Vector4f&  forwardVector   = -Vector4f::UnitZ(),
               const Vector4f&  upVector        = Vector4f::UnitY(),
               const Vector4f&  scale           = Vector4f::Ones());

        FLIRT_MATH_EXPORT
        void
        lookAtPoint(Affine3f&,
                    const Vector4f& eyePosition     = Vector4f::UnitW(),
                    const Vector4f& targetPosition  = Vector4f(0.0f, 0.0f, -1.0f, 1.0f),
                    const Vector4f& upVector        = Vector4f::UnitY(),
                    const Vector4f& scale           = Vector4f::Ones());

        FLIRT_MATH_EXPORT
        void
        composeEuler(Affine3f&,
                     const Vector4f& translate  = Vector4f::UnitW(),
                     RotateOrder                = RotateOrder::XYZ,
                     const Vector4f& rotateD    = Vector4f::UnitW(),
                     const Vector4f& scale      = Vector4f::Ones());

        FLIRT_MATH_EXPORT
        void
        compose(Affine3f&,
                const Vector4f&     translate   = Vector4f::UnitW(),
                const Quaternionf&              = Quaternionf::Identity(),
                const Vector4f&     scale       = Vector4f::Ones());

        FLIRT_MATH_EXPORT
        void
        decompose(const Affine3f&,
                  Vector4f&     translate,
                  RotateOrder,
                  Vector4f&     rotateD,
                  Vector4f&     scale,
                  Vector4f*     forwardVector   = nullptr,
                  Vector4f*     upVector        = nullptr);

        FLIRT_MATH_EXPORT
        void
        decompose(const Affine3f&,
                  Vector4f&     translate,
                  Quaternionf&  rotate,
                  Vector4f&     scale);

        FLIRT_MATH_EXPORT
        void
        interpolateXform(const Affine3f&, const Affine3f&, std::vector<float>& samples, Affine3fv&);

        ////////////////////////////////////////////////////////////////////////////////
        /// Reductions
        ////////////////////////////////////////////////////////////////////////////////

        __forceinline float add_xyz(const Vector4f& v)  { return v.x() + v.y() + v.z();     }
        __forceinline float mul_xyz(const Vector4f& v)  { return v.x() * v.y() * v.z();     }
        __forceinline float min_xyz(const Vector4f& v)  { return min(v.x(), v.y(), v.z());  }
        __forceinline float max_xyz(const Vector4f& v)  { return max(v.x(), v.y(), v.z());  }

        ////////////////////////////////////////////////////////////////////////////////
        __forceinline
        math::Vector4f
        mix(const math::Vector4f& a, const math::Vector4f& b, float x)
        {
            return a + x*(b-a);
        }

        template<int ArraySize> __forceinline
        bool
        equal(const float* a, const float* b, float precision = 1e-5f, float epsilon = 1e-6f)
        {
            for (int i = 0; i < ArraySize; ++i)
                if ((math::abs(a[i] - b[i]) < math::abs(epsilon + precision * b[i])) == false)
                    return false;
            return true;
        }
    }

#include "matrixAlgo-inl.hpp"
}
