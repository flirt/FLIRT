// ========================================================================= //
// Copyright 2016-2018 SUPAMONKS_STUDIO                                      //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
//                                                                           //
// ========================================================================= //
#pragma once

namespace smks { namespace math
{
    ///////////////////
    // enum RotateOrder
    ///////////////////
    enum RotateOrder { XYZ = 0, YZX, ZXY, XZY, YXZ, ZYX };

    inline
    void
    getRotationOrderShuffle(RotateOrder order, unsigned int r[3])
    {
        switch (order)
        {
        default:
        case RotateOrder::XYZ: r[0] = 0; r[1] = 1; r[2] = 2; break;
        case RotateOrder::YZX: r[0] = 1; r[1] = 2; r[2] = 0; break;
        case RotateOrder::ZXY: r[0] = 2; r[1] = 0; r[2] = 1; break;
        case RotateOrder::XZY: r[0] = 0; r[1] = 2; r[2] = 1; break;
        case RotateOrder::YXZ: r[0] = 1; r[1] = 0; r[2] = 2; break;
        case RotateOrder::ZYX: r[0] = 2; r[1] = 1; r[2] = 0; break;
        }
    }
}
}
