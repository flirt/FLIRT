// ========================================================================= //
// Copyright 2016-2018 SUPAMONKS_STUDIO                                      //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
//                                                                           //
// ========================================================================= //
#pragma once

#include <Eigen/Dense>
#include <Eigen/Geometry>
#include <Eigen/StdVector>

#include <smks/xchg/equal.hpp>
#include <smks/sys/configure_math.hpp>

namespace smks
{
    namespace math
    {
        typedef Eigen::Vector2i                                                     Vector2i;
        typedef Eigen::Vector3i                                                     Vector3i;
        typedef Eigen::Vector4i                                                     Vector4i;
        typedef Eigen::Vector2f                                                     Vector2f;
        typedef Eigen::Vector4f                                                     Vector4f;
        // typedef Eigen::Matrix<float, 4, 4, Eigen::ColMajor>                          Matrix4f;
        typedef Eigen::Transform<float, 3, Eigen::Affine, Eigen::ColMajor>          Affine3f;
        typedef Eigen::Quaternionf                                                  Quaternionf;
        typedef std::vector<Vector2i,   Eigen::aligned_allocator<Vector2i>>         Vector2iv;
        typedef std::vector<Vector4f,   Eigen::aligned_allocator<Vector4f>>         Vector4fv;
        typedef std::vector<Affine3f,   Eigen::aligned_allocator<Affine3f>>         Affine3fv;
        typedef std::vector<Quaternionf,    Eigen::aligned_allocator<Quaternionf>>  Quaternionfv;
        // typedef std::vector<Matrix4f,    Eigen::aligned_allocator<Matrix4f>>         Matrix4fv;

        template<typename DerivedA, typename DerivedB> inline
        bool allclose(const Eigen::DenseBase<DerivedA>&     a,
                      const Eigen::DenseBase<DerivedB>&     b,
                      const typename DerivedA::RealScalar&  precision = Eigen::NumTraits<typename DerivedA::RealScalar>::dummy_precision(),
                      const typename DerivedA::RealScalar&  epsilon = Eigen::NumTraits<typename DerivedA::RealScalar>::epsilon())
        {
          return ((a.derived() - b.derived()).array().abs()
                  < (epsilon + precision * b.derived().array().abs())).all();
        }
    }

    namespace xchg
    {

        // parameter type equality test
        template<> inline
        bool
        equal<math::Vector2f>(math::Vector2f const& left, math::Vector2f const& right)
        {
            return math::allclose<math::Vector2f>(left, right);
        }

        template<> inline
        bool
        equal<math::Vector4f>(math::Vector4f const& left, math::Vector4f const& right)
        {
            return math::allclose<math::Vector4f>(left, right);
        }

        template<> inline
        bool
        equal<math::Affine3f>(math::Affine3f const& left, math::Affine3f const& right)
        {
            return left.matrix().isApprox(right.matrix());
        }

        FLIRT_MATH_EXPORT
        bool
        areDirectionsEqual(const math::Vector4f&, const math::Vector4f&);
    }
}
