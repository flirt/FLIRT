// ========================================================================= //
// Copyright 2016-2018 SUPAMONKS_STUDIO                                      //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
//                                                                           //
// ========================================================================= //

__forceinline
void
math::transform4x4Point(const float m[16], const float p[4], float q[4])
{
    __m128 col1 = _mm_load_ps(m);
    __m128 col2 = _mm_load_ps(m + 4);
    __m128 col3 = _mm_load_ps(m + 8);
    __m128 col4 = _mm_load_ps(m + 12);

    _mm_store_ps(
        q,
        //---
        _mm_add_ps(
            _mm_mul_ps(col1, _mm_load_ps1(p)),
            _mm_add_ps(
                _mm_mul_ps(col2, _mm_load_ps1(p + 1)),
                _mm_add_ps(
                    _mm_mul_ps(col3, _mm_load_ps1(p + 2)),
                    _mm_mul_ps(col4, _mm_load_ps1(p + 3))))));
}

__forceinline
void
math::transformPoint(const float xfm[16], const float p[4], float q[4])
{
    assert(fabsf(p[3] - 1.0f) < 1e-6f);

    __m128 col1 = _mm_load_ps(xfm);
    __m128 col2 = _mm_load_ps(xfm + 4);
    __m128 col3 = _mm_load_ps(xfm + 8);
    __m128 col4 = _mm_load_ps(xfm + 12);

    _mm_store_ps(
        q,
        //---
        _mm_add_ps(
            _mm_mul_ps(col1, _mm_load_ps1(p)),
            _mm_add_ps(
                _mm_mul_ps(col2, _mm_load_ps1(p + 1)),
                _mm_add_ps(
                    _mm_mul_ps(col3, _mm_load_ps1(p + 2)),
                    col4))));
}

__forceinline
void
math::transformVector(const float xfm[16], const float p[4], float q[4])
{
    __m128 col1 = _mm_load_ps(xfm);
    __m128 col2 = _mm_load_ps(xfm + 4);
    __m128 col3 = _mm_load_ps(xfm + 8);
    __m128 col4 = _mm_load_ps(xfm + 12);

    _mm_store_ps(
        q,
        //---
        _mm_add_ps(
            _mm_mul_ps(col1, _mm_load_ps1(p)),
            _mm_add_ps(
                _mm_mul_ps(col2, _mm_load_ps1(p + 1)),
                _mm_mul_ps(col3, _mm_load_ps1(p + 2)))));
}

__forceinline
void
math::minFloat4(const float a[4], const float b[4], float ret[4])
{
    _mm_store_ps(
        ret,
        _mm_min_ps(
            _mm_load_ps(a),
            _mm_load_ps(b)));
}

__forceinline
void
math::maxFloat4(const float a[4], const float b[4], float ret[4])
{
    _mm_store_ps(
        ret,
        _mm_max_ps(
            _mm_load_ps(a),
            _mm_load_ps(b)));
}
