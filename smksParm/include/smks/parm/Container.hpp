// ========================================================================= //
// Copyright 2016-2018 SUPAMONKS_STUDIO                                      //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
//                                                                           //
// ========================================================================= //
#pragma once

#include <cassert>
#include <memory>
#include <string>
#include <vector>
#include <boost/unordered_map.hpp>

#include <smks/util/string/getId.hpp>
#include <smks/xchg/equal.hpp>

#include "Any.hpp"
#include "AbstractObserver.hpp"

namespace smks { namespace parm
{
    ///////////////////
    // class Container
    ///////////////////
    class Container
    {
    public:
        typedef std::shared_ptr<Container>          Ptr;
        typedef std::shared_ptr<const Container>    CPtr;
        typedef std::weak_ptr<Container>            WPtr;
        typedef std::weak_ptr<const Container>      CWPtr;

    private:
        //////////////////
        // struct NamedAny
        //////////////////
        struct NamedAny
        {
            const std::string   key;
            Any*                value;

        public:
            inline
            NamedAny(const std::string& key, Any* value):
                key     (key),
                value   (value)
            { }
        };
        //////////////////
    private:
        typedef boost::unordered_map<unsigned int, NamedAny>    ParmMap;
        typedef std::vector<AbstractObserver::Ptr>              Observers;
    private:
        ParmMap     _parms;
        Observers   _observers;

    public:
        static inline
        Ptr
        create()
        {
            Ptr ptr (new Container);
            return ptr;
        }

    protected:
        inline
        Container():
            _parms      (),
            _observers  ()
        {
            _observers.reserve(1);
        }

    private:
        // non-copyable
        Container(const Container&);
        Container& operator=(const Container&);
    public:
        ~Container()
        {
            clear();
            while (!_observers.empty())
                deregisterObserver(*_observers.begin());
        }

        inline
        void
        clear()
        {
            while(!_parms.empty())
                unset(_parms.begin()->first);
        }

        template<typename ValueType> inline
        bool
        existsAs(const std::string& parmName) const
        {
            return !parmName.empty()
                ? existsAs<ValueType>(util::string::getId(parmName.c_str()))
                : false;
        }

        template<typename ValueType> inline
        bool
        existsAs(unsigned int parmId) const
        {
            Any* parm = get(parmId);

            return parm && parm->type() == typeid(ValueType);
        }

        template<typename ValueType> inline
        const ValueType&
        get(const std::string& parmName) const
        {
            assert(!parmName.empty());
            Any* parm = get(util::string::getId(parmName.c_str()));
            assert(parm);
            return Any::cast<const ValueType&>(*parm);
        }

        template<typename ValueType> inline
        const ValueType&
        get(unsigned int parmId) const
        {
            Any* parm = get(parmId);
            assert(parm);
            return Any::cast<const ValueType&>(*parm);
        }

        template<typename ValueType> inline
        unsigned int //<! returns parameter id
        set(const char* parmName, const ValueType& parmValue)
        {
            return set<ValueType>(parmName, util::string::getId(parmName), parmValue);
        }

        template<typename ValueType> inline
        unsigned int //<! returns parameter id
        set(const char* parmName, unsigned int parmId, const ValueType& parmValue)
        {
            if (parmId == 0)
                return 0;
            assert(parmId == util::string::getId(parmName));
            if (existsAs<ValueType>(parmId))
                update<ValueType>(parmId, parmValue);
            else
            {
                if (remove(parmId))
                    deliver(parmId, Event::PARM_REMOVED);
                if (add<ValueType>(parmId, parmName, parmValue))
                    deliver(parmId, Event::PARM_ADDED);
                else
                    return 0;
            }
            return parmId;
        }

        template<typename ValueType> inline
        bool
        update(unsigned int parmId, const ValueType& parmValue)
        {
            Any* parm = get(parmId);

            if (parm == nullptr ||
                parm->type() != typeid(ValueType))
                return false;

            // update the value and notify change if any
            ValueType* ptr = Any::cast<ValueType>(parm);
            assert(ptr);
            if (!xchg::equal(*ptr, parmValue))
            {
                *ptr = parmValue;
                deliver(parmId, Event::PARM_CHANGED);
            }
            return true;
        }

        inline
        void
        unset(const std::string& parmName)
        {
            if (!parmName.empty())
                unset(util::string::getId(parmName.c_str()));
        }

        inline
        void
        unset(unsigned int parmId)
        {
            if (remove(parmId))
                deliver(parmId, Event::PARM_REMOVED);
        }

        inline
        bool
        getName(unsigned int parmId, std::string& parmName) const
        {
            parmName.clear();

            ParmMap::const_iterator const& it = _parms.find(parmId);
            if (it != _parms.end())
                parmName = it->second.key;

            return !parmName.empty();
        }

        inline
        bool
        hasKey(unsigned int id) const
        {
            return _parms.find(id) != _parms.end();
        }

        inline
        size_t
        size() const
        {
            return _parms.size();
        }

        inline
        size_t
        getKeys(size_t maxCount, unsigned int* keys) const
        {
            size_t n = 0;
            if (maxCount > 0 && keys)
            {
                memset(keys, 0, sizeof(unsigned int) * maxCount);
                for (ParmMap::const_iterator it = _parms.begin(); it != _parms.end(); ++it)
                    if (n < maxCount)
                        keys[n++] = it->first;
            }
            return n;
        }

        inline
        void
        registerObserver(AbstractObserver::Ptr const& observer)
        {
            if (observer == nullptr)
                return;
            for (size_t i = 0; i < _observers.size(); ++i)
                if (_observers[i].get() == observer.get())
                    return;
            _observers.push_back(observer);
        }

        inline
        void
        deregisterObserver(AbstractObserver::Ptr const& observer)
        {
            if (!observer)
                return;
            for (size_t i = 0; i < _observers.size(); ++i)
                if (_observers[i].get() == observer.get())
                {
                    std::swap(_observers[i], _observers.back());
                    _observers.pop_back();
                    break;
                }
        }

        inline
        std::pair<const char*, const std::type_info*>
        getInfo(unsigned int parmId) const
        {
            ParmMap::const_iterator const& it = _parms.find(parmId);

            return it != _parms.end() && it->second.value
                ? std::pair<const char*, const std::type_info*>(it->second.key.c_str(), &it->second.value->type())
                : std::pair<const char*, const std::type_info*>(nullptr, nullptr);
        }

        inline
        const std::type_info*
        getInfo(const char* parmName) const
        {
            const unsigned int              parmId  = util::string::getId(parmName);
            ParmMap::const_iterator const&  it      = _parms.find(parmId);

            return it != _parms.end() && it->second.value
                ? &it->second.value->type()
                : nullptr;
        }

    private:
        inline
        Any*
        get(unsigned int parmId) const
        {
            ParmMap::const_iterator const& it = _parms.find(parmId);

            return it != _parms.end() ? it->second.value : nullptr;
        }

        inline
        bool
        insert(unsigned int parmId, const std::string& parmName, Any* parmValue)
        {
            return _parms.insert(std::pair<unsigned int, NamedAny>(parmId, NamedAny(parmName, parmValue))).second;
        }

        template<typename ValueType> inline
        bool
        add(unsigned int parmId, const std::string& parmName, ValueType parmValue)
        {
            ParmMap::iterator const& it = _parms.find(parmId);

            if (it != _parms.end() ||
                parmName.empty())
                return false;

            assert(parmId == util::string::getId(parmName.c_str()));

            return insert(parmId, parmName, new Any(parmValue));
        }

        inline
        bool
        remove(unsigned int parmId)
        {
            ParmMap::iterator const& it = _parms.find(parmId);

            if (it == _parms.end())
                return false;
            delete it->second.value;
            _parms.erase(it);

            return true;
        }

        void
        deliver(unsigned int parmId, Event typ)
        {
            for (size_t i = 0; i < _observers.size(); ++i)
                if (_observers[i])
                    _observers[i]->handle(parmId, typ);
        }
    };
}
}
