// ========================================================================= //
// Copyright 2016-2018 SUPAMONKS_STUDIO                                      //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
//                                                                           //
// ========================================================================= //
#pragma once

#include <algorithm>
#include <typeinfo>

#include <smks/sys/aligned_allocation.hpp>

namespace smks { namespace parm
{
    ////////////
    // class Any
    ////////////
    class Any
    {
    private:
        template <typename T>
        struct RemoveReference
        {
            typedef T Type;
        };

        template <typename T>
        struct RemoveReference<T&>
        {
            typedef T Type;
        };

    private:
        ///////////////////////
        // class AbstractHolder
        ///////////////////////
        class AbstractHolder
        {
        public:
            virtual inline
            ~AbstractHolder()
            { }

            virtual
            const std::type_info&
            type() const = 0;

            virtual
            AbstractHolder*
            clone() const = 0;
        };
        ///////////////////////

    private:
        AbstractHolder* _content;

    private:
        //////////////////////////
        // class Holder<ValueType>
        //////////////////////////
        template <typename ValueType>
        class Holder :
            public AbstractHolder
        {
        public:
            ValueType held;

        public:
            explicit inline
            Holder(const ValueType& value) :
                held(value)
            { }

            inline
            void*
            operator new(size_t size)
            {
                assert(size == sizeof(Holder<ValueType>));
                return sys::alignedMalloc(size, 16);    // must guarantee 16-bit alignment for SSE2 instruction sets
            }

            inline
            void
            operator delete(void* ptr)
            {
                sys::alignedFree(ptr);
            }

        private:
            // non-copyable
            Holder(const Holder&);
            Holder& operator=(const Holder&);

        public:
            virtual inline
            const std::type_info&
            type() const
            {
                return typeid(ValueType);
            }

            virtual inline
            AbstractHolder*
            clone() const
            {
                return new Holder(held);
            }
        };
        //////////////////////////

    public:
        inline
        Any():
            _content(0)
        { }

        template <typename ValueType> inline
        Any(const ValueType & value) :
            _content(new Holder<ValueType>(value))
        { }

        inline
        Any(const Any& other) :
            _content(other._content ? other._content->clone() : 0)
        { }

        inline
        Any(Any&& other) :
            _content(other._content ? other._content : 0)
        {
            other._content = 0;
        }

        inline
        ~Any()
        {
            if (_content)
                delete _content;
            _content = 0;
        }

        inline
        Any&
        swap(Any& rhs)
        {
            std::swap(_content, rhs._content);
            return *this;
        }

        template <typename ValueType> inline
        Any&
        operator=(const ValueType& rhs)
        {
            Any(rhs).swap(*this);
            return *this;
        }

        inline
        Any&
        operator=(Any& rhs)
        {
            rhs.swap(*this);
            return *this;
        }

        inline
        Any&
        operator=(Any&& rhs)
        {
            _content        = rhs._content;
            rhs._content    = 0;
            return *this;
        }

        inline
        bool
        empty() const
        {
            return !_content;
        }

        inline
        const std::type_info&
        type() const
        {
            return _content ? _content->type() : typeid(void);
        }

        template<typename ValueType> inline
        bool
        compatibleWith() const
        {
            return _content ? _content->type() == typeid(ValueType) : false;
        }

        template <typename ValueType> static inline
        ValueType*
        cast(Any* operand)
        {
            return operand && operand->type() == typeid(ValueType)
                ? &static_cast<Any::Holder<ValueType> *>(operand->_content)->held
                : 0;
        }

        template <typename ValueType> static inline
        ValueType*
        unsafe_cast(Any* operand)
        {
            return operand
                ? &static_cast<Any::Holder<ValueType> *>(operand->_content)->held
                : 0;
        }

        template <typename ValueType> static inline
        const ValueType*
        cast(const Any* operand)
        {
            return Any::cast<ValueType>(const_cast<Any*>(operand));
        }

        template <typename ValueType> static inline
        const ValueType*
        unsafe_cast(const Any* operand)
        {
            return Any::unsafe_cast<ValueType>(const_cast<Any*>(operand));
        }

        template <typename ValueType> static inline
        ValueType
        cast(Any& operand)
        {
            typedef typename RemoveReference<ValueType>::Type NonRef;

            NonRef* result = cast<NonRef>(&operand);

            if (!result)
                throw std::bad_cast();

            return *result;
        }

        template <typename ValueType> static inline
        ValueType
        unsafe_cast(Any& operand)
        {
            typedef typename RemoveReference<ValueType>::Type NonRef;

            NonRef* result = unsafe_cast<NonRef>(&operand);

            if (!result)
                throw std::bad_cast();

            return *result;
        }

        template <typename ValueType> static inline
        ValueType
        cast(const Any& operand)
        {
            typedef typename RemoveReference<ValueType>::Type NonRef;

            return cast<const NonRef&>(const_cast<Any&>(operand));
        }

        template <typename ValueType> static inline
        ValueType
        unsafe_cast(const Any& operand)
        {
            typedef typename RemoveReference<ValueType>::Type NonRef;

            return unsafe_cast<const NonRef&>(const_cast<Any&>(operand));
        }
    };
}
}
