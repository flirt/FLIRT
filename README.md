
![Alt text](doc/flirt_logo.png "FLIRT Rendering Framework")

FLIRT — 3D Rendering Framework
==============================

## Presentation

FLIRT is an opensource 3D rendering framework providing:

* a dynamic, node-based scene management,

* an physically-based offline renderer,

* and a real-time viewport for scene inspection.

Developed in C++, FLIRT also enables users with scene editing and rendering scripting through the exposition of its API in Python.

Currently LGPL-licensed, FLIRT has been developed and tested mostly with Windows x64 platforms in mind, using the Visual Studio 2012 and Visual Studio 2015 compilers.

## Downloads

* **Prebuilt FLIRT package**
    * Visual Studio 2015 (VC14) for 64bit: [flirt-bin-msvc14.7z](https://spaces.hightail.com/space/DoUj5cczz9)

* **Prebuilt dependency package**
    * Visual Studio 2015 (VC14) for 64bit: [flirt-dep-msvc14.7z](https://spaces.hightail.com/space/DoUj5cczz9)

## Architecture

When it comes to its implementation and organization, the gist of the FLIRT project's architecture is the respect of the Model-View-Controller architecture principles. Everything thus revolves around three main, interdependent concepts:

* The **scene** (Model), that assumes the form of a hierarchy of nodes corresponding to 3D scene entities;

* **Clients** (Controllers), used to manipulate and query the scene content, as well as to observe its changes and propagate editing information;

* **Devices** (Views), used to propose specific visualizations of the scene content made accessible to them through their associated client.

### [Scene Representation and Management](doc/scene.md)

### [Controlling the Scene](doc/client.md)

### [Visualizing the Scene](doc/device.md)

## Content

Follows a brief description of the content of the different folders at the root of the repository.

### Framework

#### Primary Components

* **_pyflirt/**: stores the Python module exposing the complete framework's API
* **smksScn/**: implements the scene `smks::scn::Scene` and nodes deriving from `smks::scn::TreeNode`
* **smksParm/**: defines the node parameter container `smks::parm::Container` and its observer interface `smks::parm::AbstractObserver`
* **smksClientBase/**: common code for scene clients `smks::ClientBase` and bindings `smks::ClientBindingBase`
* **smksRndrClient/**: implements the rendering client `smks::rndr::Client`
* **smksRndrDeviceAPI/**: defines the rendering device API `smks::rndr::AbstractDevice`
* **smksRndrDevice/**: implements a raytracing rendering device `smks::rndr::Device`
* **smksViewClient/**: implements the viewport client `smks::view::Client`
* **smksViewDeviceAPI/**: defines the viewport device API `smks::view::AbstractDevice`
* **smksViewDevide/**: implements a viewport device `smks::view::Device`
* **smksXchg/**: stores all shared structures and enumerations used by other components
* **smksMath/**: defines numeric types and implements mathematic routines used by other components

#### Secondary Components
* **smksAsset/**: implements the external resource manager `smks::sys::ResourceDatabase`
* **smksImg/**: implements the image structure `smks::img::AbstractImage` and the I/O image code
* **smksSys/**: stores OS-specific structures, enumerations, and functions (e.g., threading, memory allocation)
* **smksLog/**: enables logging support for debugging
* **smksUtil/**: implements shared utility functions used by other components
* **smksViewCtrl/**: implements the OpenSceneGraph event handlers available to viewport devices
* **smksUtilOsg/**: implements the OpenSceneGraph-bound utility functions used by viewport clients and devices
* **smksViewGL**: defines and stores the OpenGL-bound concepts used by viewport clients and devices (e.g., shading program and uniform declarations)
* **smksStrId/**: implements the string hash function used throughout the whole framework

### Documentation
* **doc/**: stores all documentation resources locally
* External links:
    * [Python module API](https://flirt.gitlab.io/)
    * [List of all node classes and parameters](https://flirt.gitlab.io/reference_card.pdf)

### Build System

* **build/**: stores the premake scripts used by the build system (the `build/smks.dep.*.lua` files define the dependency paths expected by the system and should be edited to specify custom dependency locations).

### Executables

* **testsuite/**: contains test suites in both C++ and Python (using `googletest` and `unittest` respectively)
* **example/**: contains FLIRT examples in both C++ and Python

## [Building FLIRT](doc/building.md)

## Authors

* Pierre-Edouard LANDES (pel@supamonks.com)

Copyright 2016-2018 SUPAMONKS STUDIO

## Acknowledgments

Supervision by Damien 'Dee' COUREAU (dee@supamonks.com)

## License

The FLIRT project is licensed under the GNU Lesser General Public License. Refer to the LICENSE.txt file for details.

## Upcoming Features

- Reference implementation for subsurface scattering
- Geometry instancing
- Improved real-time preview