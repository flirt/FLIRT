// ========================================================================= //
// Copyright 2016-2018 SUPAMONKS_STUDIO                                      //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
//                                                                           //
// ========================================================================= //
#pragma once

#include <memory>
#include <smks/math/types.hpp>

#include "smks/img/enum.hpp"
#include "smks/sys/configure_img.hpp"

namespace smks { namespace img
{
    class AbstractImage
    {
    public:
        typedef std::shared_ptr<AbstractImage>  Ptr;

    public:
        //####################
        // API for ray-tracing
        virtual
        size_t
        width() const = 0;

        virtual
        size_t
        height() const = 0;

        virtual
        const math::Vector2i&
        resolution() const = 0;

        virtual
        math::Vector4f
        pixel(const math::Vector2i&, bool noClamping = false) const = 0;
        //####################

        virtual
        const float*
        data() const = 0;

        virtual
        size_t
        stride() const = 0; // expressed in floats

        virtual
        size_t
        numBytes() const = 0;

        virtual
        img::PixelFormat
        pixelFormat() const = 0;

        virtual
        img::PixelInternalFormat
        pixelInternalFormat() const = 0;
    };
}
}
