// ========================================================================= //
// Copyright 2016-2018 SUPAMONKS_STUDIO                                      //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
//                                                                           //
// ========================================================================= //
#pragma once

#include <iostream>
#include "smks/img/AbstractImage.hpp"
#include "smks/sys/configure_img.hpp"

namespace smks { namespace img
{
    class FLIRT_IMG_EXPORT Image:
        public AbstractImage
    {
    public:
        typedef std::shared_ptr<Image> Ptr;

    private:
        class PImpl;
    private:
        PImpl *_pImpl;

    public:
        static inline
        Ptr
        create(size_t               width,
               size_t               height,
               size_t               stride,
               PixelInternalFormat  internalFormat,
               PixelFormat          format,
               const float*         data,
               bool                 copy)
        {
            Ptr ptr(new Image());
            ptr->initialize(width, height, stride, internalFormat, format, data, copy);
            return ptr;
        }

        static inline
        Ptr
        create(const char* filePath)
        {
            Ptr ptr(new Image());
            ptr->initialize(filePath);
            return ptr;
        }

    protected:
        Image();
    private:
        // non-copyable
        Image(const Image&);
        Image& operator=(const Image&);
    public:
        ~Image();

        const math::Vector2i&
        resolution() const;

        size_t
        width() const;

        size_t
        height() const;

        size_t
        stride() const;

        const float*
        data() const;

        size_t
        numBytes() const;

        img::PixelFormat
        pixelFormat() const;

        img::PixelInternalFormat
        pixelInternalFormat() const;

        virtual
        math::Vector4f
        pixel(const math::Vector2i&, bool) const;   //<! clamp to texture dimensions

    protected:
        void
        dispose();

        void
        initialize(size_t, size_t, size_t, PixelInternalFormat, PixelFormat, const float*, bool);

        void
        initialize(const char*);
    };
}
}
