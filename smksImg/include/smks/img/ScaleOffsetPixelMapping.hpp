// ========================================================================= //
// Copyright 2016-2018 SUPAMONKS_STUDIO                                      //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
//                                                                           //
// ========================================================================= //
#pragma once

#include "smks/img/AbstractPixelMapping.hpp"

namespace smks { namespace img
{
    ///////////////////////
    // class ScaleOffsetPixelMapping
    ///////////////////////
    class FLIRT_IMG_EXPORT ScaleOffsetPixelMapping:
        public AbstractPixelMapping
    {
    public:
        typedef std::shared_ptr<ScaleOffsetPixelMapping> Ptr;
    private:
        class PImpl;
    private:
        PImpl* _pImpl;

    public:
        static inline
        Ptr
        create(float scale = 1.0f, float offset = 0.0f)
        {
            Ptr ptr(new ScaleOffsetPixelMapping(scale, offset));
            return ptr;
        }
        static inline
        Ptr
        create(size_t num, const float* scale, const float* offset)
        {
            Ptr ptr(new ScaleOffsetPixelMapping(num, scale, offset));
            return ptr;
        }

    protected:
        ScaleOffsetPixelMapping(float scale, float offset);
        ScaleOffsetPixelMapping(size_t, const float* scale, const float* offset);
    private:
        // non-copyable
        ScaleOffsetPixelMapping(const ScaleOffsetPixelMapping&);
        ScaleOffsetPixelMapping& operator=(const ScaleOffsetPixelMapping&);
    public:
        ~ScaleOffsetPixelMapping();

        void
        operator()(size_t x, size_t y, size_t numChannels, const float*, float*) const;
    };
}
}
