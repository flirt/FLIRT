// ========================================================================= //
// Copyright 2016-2018 SUPAMONKS_STUDIO                                      //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
//                                                                           //
// ========================================================================= //
#pragma once

#include <memory>
#include "smks/sys/configure_img.hpp"

namespace smks { namespace img
{
    class FLIRT_IMG_EXPORT OpenColorIOMapper
    {
    public:
        typedef std::shared_ptr<OpenColorIOMapper> Ptr;
    private:
        class PImpl;
    private:
        PImpl* _pImpl;
    public:
        static
        Ptr
        create();
    protected:
        OpenColorIOMapper();
    private:
        // non-copyable
        OpenColorIOMapper(const OpenColorIOMapper&);
        OpenColorIOMapper& operator=(const OpenColorIOMapper&);
    public:
        ~OpenColorIOMapper();

        const char*
        configPath() const;
        const char*
        inputColorspace() const;
        const char*
        display() const;
        const char*
        displayView() const;
        float
        exposureFstops() const;

        void
        configPath(const char*);    //<! expects resolved file path
        void
        inputColorspace(const char*);
        void
        displayView(const char* display, const char* displayView);
        void
        exposureFstops(float);

        void
        applyRGBA(size_t width, size_t height, const float*, float*) const; //<! expects tightly-packed RGBA data
    };
}
}
