// ========================================================================= //
// Copyright 2016-2018 SUPAMONKS_STUDIO                                      //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
//                                                                           //
// ========================================================================= //
#pragma once

#include <memory>
#include "smks/img/AbstractImage.hpp"

namespace smks { namespace img
{
    class AbstractPixelMapping;

    class FLIRT_IMG_EXPORT OutputImage
    {
    public:
        typedef std::shared_ptr<OutputImage> Ptr;
    private:
        typedef std::shared_ptr<AbstractPixelMapping> PixelMappingPtr;
    private:
        class PImpl;
    private:
        PImpl* _pImpl;

    public:
        static
        Ptr
        create();

    protected:
        OutputImage();

        void
        build(Ptr const&);

    private:
        // non-copyable
        OutputImage(const OutputImage&);
        OutputImage& operator=(const OutputImage&);

    public:
        ~OutputImage();

        void
        clear();

        void
        setResolution(size_t, size_t);

        void setMetadata(const char*, size_t);
        void setMetadata(const char*, float);
        void setMetadata(const char*, const math::Vector2f&);
        void setMetadata(const char*, const math::Vector4f&);
        void setMetadata(const char*, const math::Affine3f&);
        void setMetadata(const char*, const std::string&);
        void setMetadata(const char*, bool);

        bool getMetadata(const char*, size_t&)          const;
        bool getMetadata(const char*, float&)           const;
        bool getMetadata(const char*, math::Vector2f&)  const;
        bool getMetadata(const char*, math::Vector4f&)  const;
        bool getMetadata(const char*, math::Affine3f&)  const;
        bool getMetadata(const char*, std::string&)     const;
        bool getMetadata(const char*, bool&)            const;

        unsigned int    //<! returns layerID
        addLayer(size_t numChannels, const char* name = "", PixelMappingPtr const& = nullptr);
        void
        addIdCoverageLayers(size_t numChannels, unsigned int& layerId, unsigned int& layerCoverage);
        void
        addLayerChannel(unsigned int layerId, size_t channelIdx, const float* data, size_t stride, bool useHalf);
        void
        addLayerChannel(unsigned int layerId, size_t channelIdx, const unsigned int* data, size_t stride);
        void
        addRGBAChannel(size_t channelIdx, const float* data, size_t stride, bool useHalf);

        void
        saveToFile(const char*) const;
    };
}
}
