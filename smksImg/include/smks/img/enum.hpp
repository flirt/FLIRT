// ========================================================================= //
// Copyright 2016-2018 SUPAMONKS_STUDIO                                      //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
//                                                                           //
// ========================================================================= //
#pragma once

namespace smks { namespace img
{
    enum ColorChannel
    {
        UNSPECIFIED_COLOR_CHANNEL = -1,
        RED_CHANNEL,
        GREEN_CHANNEL,
        BLUE_CHANNEL,
        ALPHA_CHANNEL,
        NUM_COLOR_CHANNELS
    };

    enum PixelFormat
    {
        UNSPECIFIED_FORMAT = 0,
        RGBA_FORMAT
    };

    enum PixelInternalFormat
    {
        UNSPECIFIED_INT_FORMAT = 0,
        RGBA_INT_FORMAT
    };
}
}
