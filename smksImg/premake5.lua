-- ========================================================================= --
-- Copyright 2016-2018 SUPAMONKS_STUDIO                                      --
-- Author: Pierre-Edouard Landes <pel@supamonks.com>                         --
--                                                                           --
-- This program is free software: you can redistribute it and/or modify      --
-- it under the terms of the GNU Lesser General Public License as            --
-- published by the Free Software Foundation, either version 3 of the        --
-- License, or (at your option) any later version.                           --
--                                                                           --
-- This program is distributed in the hope that it will be useful,           --
-- but WITHOUT ANY WARRANTY; without even the implied warranty of            --
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             --
-- GNU Lesser General Public License for more details.                       --
--                                                                           --
-- You should have received a copy of the GNU Lesser General Public License  --
-- along with this program.  If not, see <https://www.gnu.org/licenses/>.    --
--                                                                           --
-- ========================================================================= --


smks.module.uses_img = function ()

    smks.module.links(
        smks.module.name.img,
        smks.module.kind.img)

    smks.dep.boost.includes()
    smks.dep.eigen.includes()

    smks.module.includes(smks.module.name.math)
    smks.module.includes(smks.module.name.xchg)

end


smks.module.copy_img_to_targetdir = function()

    smks.module.copy_to_targetdir(
        smks.module.name.img)

    smks.module.copy_log_to_targetdir()
    smks.module.copy_strid_to_targetdir()

    smks.dep.lpng.copy_to_targetdir()
    smks.dep.opencolorio.copy_to_targetdir()
    smks.dep.zlib.copy_to_targetdir()

end


project(smks.module.name.img)

    language 'C++'
    smks.module.set_kind(
        smks.module.kind.img)

    files
    {
        'include/**.hpp',
        'src/**.hpp',
        'src/**.cpp',
    }
    defines
    {
        'FLIRT_IMG_DLL',
        '__FLIRT_NO_YFLIPPED_IMG_LOADING',
    }
    includedirs
    {
        'include',
    }

    smks.dep.boost.includes()
    smks.dep.eigen.includes()
    smks.dep.openexr.links()
    smks.dep.zlib.links()
    smks.dep.lpng.links()
    smks.dep.jpeg.links()
    smks.dep.tiff.links()
    smks.dep.opencolorio.links()

    smks.module.includes(smks.module.name.sys)
    smks.module.includes(smks.module.name.util)
    smks.module.includes(smks.module.name.math)
    smks.module.includes(smks.module.name.xchg)
    smks.module.includes(smks.module.name.parm)
    smks.module.uses_log()
    smks.module.uses_strid()
