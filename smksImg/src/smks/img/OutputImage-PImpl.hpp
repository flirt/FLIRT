// ========================================================================= //
// Copyright 2016-2018 SUPAMONKS_STUDIO                                      //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
//                                                                           //
// ========================================================================= //
#pragma once

#include <smks/parm/Container.hpp>
#include <smks/util/string/getId.hpp>
#include <smks/util/string/case.hpp>

#include "smks/img/OutputImage.hpp"
#include "smks/img/AbstractPixelMapping.hpp"


namespace smks { namespace img
{
    static const char* LAYER_ID         = "ID";
    static const char* LAYER_COVERAGE   = "coverage";

    class AbstractPixelMapping;

    ///////////////////
    // enum ChannelType
    ///////////////////
    enum ChannelType
    {
        UNTYPED_CHANNEL = -1,
        UINT_CHANNEL    = 0,    // unsigned int (32 bit)
        HALF_CHANNEL    = 1,    // half (16 bit floating point)
        FLOAT_CHANNEL   = 2     // float (32 bit floating point)
    };

    /////////////////////
    // class LayerChannel
    /////////////////////
    class LayerChannel
    {
    public:
        typedef std::shared_ptr<LayerChannel>   Ptr;
        typedef std::weak_ptr<LayerChannel>     WPtr;
    private:
        const std::string   _name;
        const unsigned int  _id;
        const unsigned int  _layerId;
        const char *const   _data;
        const ChannelType   _type;
        const size_t        _stride;
    public:
        static inline
        Ptr
        create(const char* name, unsigned int layerId, const char* data, ChannelType type, size_t stride)
        {
            Ptr ptr(new LayerChannel(name, layerId, data, type, stride));
            return ptr;
        }
    protected:
        LayerChannel(const char* name, unsigned int layerId, const char* data, ChannelType type, size_t stride):
            _name   (name),
            _id     (util::string::getId(_name.c_str())),
            _layerId(layerId),
            _data   (data),
            _type   (type),
            _stride (stride)
        { }
    private:
        LayerChannel(const LayerChannel&);
        LayerChannel& operator=(const LayerChannel&);
    public:
        inline
        const std::string&
        name() const
        {
            return _name;
        }
        inline
        unsigned int
        id() const
        {
            return _id;
        }
        inline
        unsigned int
        layerId() const
        {
            return _layerId;
        }
        inline
        const char*
        data() const
        {
            return _data;
        }
        inline
        ChannelType
        type() const
        {
            return _type;
        }
        inline
        size_t
        stride() const
        {
            return _stride;
        }
    };

    //////////////
    // class Layer
    //////////////
    class Layer
    {
    public:
        typedef std::shared_ptr<Layer> Ptr;
    private:
        const std::string               _name;
        const unsigned int              _id;
        std::vector<LayerChannel::WPtr> _channels;
        const AbstractPixelMapping::Ptr _func;
    public:
        static inline
        Ptr
        create(size_t numChannels, const std::string& name, AbstractPixelMapping::Ptr const& func)
        {
            Ptr ptr(new Layer(numChannels, name, func));
            return ptr;
        }
    protected:
        inline
        Layer(size_t numChannels, const std::string& name, AbstractPixelMapping::Ptr const& func):
            _name       (name),
            _id         (util::string::getId(_name.c_str())),
            _channels   (numChannels),
            _func       (func)
        { }
    private:
        Layer(const Layer&);
        Layer& operator=(const Layer&);
    public:
        inline
        const std::string&
        name() const
        {
            return _name;
        }
        inline
        unsigned int
        id() const
        {
            return _id;
        }
        inline
        size_t
        numChannels() const
        {
            return _channels.size();
        }
        inline
        LayerChannel::WPtr const&
        channel(size_t idx) const
        {
            assert(idx < _channels.size());
            return _channels[idx];
        }
        inline
        LayerChannel::WPtr&
        channel(size_t idx)
        {
            assert(idx < _channels.size());
            return _channels[idx];
        }
        inline
        AbstractPixelMapping::Ptr const&
        func() const
        {
            return _func;
        }
    };

    ///////////////////////////
    // class OutputImage::PImpl
    ///////////////////////////
    class OutputImage::PImpl
    {
    private:
        typedef boost::unordered_map<unsigned int, Layer::Ptr>          LayerMap;           //<! layer name -> layer
        typedef boost::unordered_map<unsigned int, LayerChannel::Ptr>   LayerChannelMap;    //<! layer channel name -> layer chanel
    private:
        std::weak_ptr<OutputImage>  _self;

        size_t                  _width;
        size_t                  _height;
        parm::Container::Ptr    _metadata;
        LayerMap                _layers;
        LayerChannelMap         _layerChannels;

    public:
        PImpl();

        void
        build(Ptr const&);

        void
        clear();

        void
        setResolution(size_t, size_t);

        template <typename ValueType> inline
        void
        setMetadata(const char* name, const ValueType& value)
        {
            _metadata->set<ValueType>(name, value);
        }

        template <typename ValueType> inline
        bool
        getMetadata(const char* name, ValueType& value) const
        {
            if (!_metadata->existsAs<ValueType>(name))
                return false;
            value = _metadata->get<ValueType>(name);
            return true;
        }

        unsigned int
        addLayer(size_t numChannels, const char* name, PixelMappingPtr const&);
        void
        addLayerChannel(unsigned int layerId, size_t channelIdx, const char* data, ChannelType type, size_t stride);

        void
        saveToFile(const std::string&) const;

    private:
        const Layer*
        getLayer(const char*) const;
        LayerChannel::Ptr
        getLayerChannel(const char*) const;
        void
        getLayerChannelName(const Layer&, size_t channelIdx, std::string&) const;

        AbstractPixelMapping*   //<! callers takes ownership over returned memory
        getLayerTransform(const Layer&) const;

        void
        saveToEXRFile(const std::string&) const;
        void
        saveToPNGFile(const std::string&) const;

        static
        void
        getLayerFilePath(const std::string& filePath, const Layer&, std::string&);
        static
        void
        getLayerFilePath(const std::string& filePath, const std::string&, std::string&);
    };
}
}
