// ========================================================================= //
// Copyright 2016-2018 SUPAMONKS_STUDIO                                      //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
//                                                                           //
// ========================================================================= //
#pragma once

#include <smks/util/string/case.hpp>
#include "smks/img/Image.hpp"

namespace smks { namespace img
{
    class Image::PImpl
    {
    private:
        const float*        _data;
        math::Vector2i      _resolution;
        size_t              _stride;    //<! expressed in floats
        PixelInternalFormat _pixelInternalFormat;
        PixelFormat         _pixelFormat;
        size_t              _numBytes;
        bool                _ownsData;

    public:
        inline
        PImpl():
            _data               (nullptr),
            _resolution         (math::Vector2i::Zero()),
            _stride             (0),
            _pixelInternalFormat(img::UNSPECIFIED_INT_FORMAT),
            _pixelFormat        (img::UNSPECIFIED_FORMAT),
            _numBytes           (0),
            _ownsData           (false)
        { }

        inline
        ~PImpl()
        {
            if (_ownsData && _data)
                delete[] _data;
        }

        inline
        void
        dispose()
        {
            if (_ownsData && _data)
                delete[] _data;
            _data                   = nullptr;
            _resolution.setZero();
            _stride                 = 0;
            _pixelInternalFormat    = img::UNSPECIFIED_INT_FORMAT;
            _pixelFormat            = img::UNSPECIFIED_FORMAT;
            _numBytes               = 0;
            _ownsData               = false;
        }

        inline
        void
        initialize(size_t width, size_t height, size_t stride, PixelInternalFormat internalFormat, PixelFormat format, const float* data, bool copy)
        {
            dispose();

            _data                   = nullptr;
            _resolution             = math::Vector2i(static_cast<int>(width), static_cast<int>(height));
            _stride                 = stride;
            _pixelInternalFormat    = internalFormat;
            _pixelFormat            = format;
            _numBytes               = width * height * stride * sizeof(float);

            if (_numBytes > 0)
            {
                if (copy)
                {
                    float* newData = new float[_numBytes / sizeof(float)];
                    memcpy(newData, data, _numBytes);
                    _data       = newData;
                    _ownsData   = true;
                }
                else
                {
                    _data       = data;
                    _ownsData   = false;
                }
            }
            else
                _ownsData = false;
        }

        inline
        bool
        initialize(const std::string& filePath)
        {
            const std::string& ext = util::string::strlwr(filePath.substr(filePath.find_last_of('.')));

            if (strcmp(ext.c_str(), ".exr") == 0)
                return initializeFromEXR(filePath);
            else if (strcmp(ext.c_str(), ".jpg") == 0 ||
                strcmp(ext.c_str(), ".jpeg") == 0 ||
                strcmp(ext.c_str(), ".jpe") == 0 )
                return initializeFromJPG(filePath);
            else if (strcmp(ext.c_str(), ".png") == 0)
                return initializeFromPNG(filePath);
            else if (strcmp(ext.c_str(), ".tif") == 0 ||
                strcmp(ext.c_str(), ".tiff") == 0)
                return initializeFromTIF(filePath);
            else
            {
                std::cerr << "Failed to load image from '" << filePath << "': unsupported image extension '" << ext << "'." << std::endl;
                return false;
            }
        }

    private:
        bool
        initializeFromJPG(const std::string&);
        bool
        initializeFromPNG(const std::string&);
        bool
        initializeFromEXR(const std::string&);
        bool
        initializeFromTIF(const std::string&);

    public:
        inline
        const math::Vector2i&
        resolution() const
        {
            return _resolution;
        }

        inline
        size_t
        width() const
        {
            return static_cast<size_t>(_resolution.x());
        }

        inline
        size_t
        height() const
        {
            return static_cast<size_t>(_resolution.y());
        }

        inline
        size_t
        stride() const
        {
            return _stride;
        }

        inline
        size_t
        numBytes() const
        {
            return _numBytes;
        }

        inline
        img::PixelFormat
        pixelFormat() const
        {
            return _pixelFormat;
        }

        inline
        img::PixelInternalFormat
        pixelInternalFormat() const
        {
            return _pixelInternalFormat;
        }

        inline
        const float*
        data() const
        {
            return _data;
        }

        inline
        math::Vector4f
        pixel(const math::Vector2i& ij_, bool noClamping) const
        {
            const math::Vector2i& ij = noClamping
                ? ij_
                : ij_
                .cwiseMax(math::Vector2i::Zero())
                .cwiseMin(_resolution - math::Vector2i::Ones());

            math::Vector4f ret(math::Vector4f::Ones());

            memcpy(ret.data(), _data + (ij.x() + _resolution.x() * ij.y()) * _stride, sizeof(float) * _stride);

            return ret;
        }

    private:
        static inline
        size_t
        numFloats(PixelInternalFormat fmt)
        {
            switch (fmt)
            {
            default:
            case UNSPECIFIED_INT_FORMAT:    return 0;
            case RGBA_INT_FORMAT:           return 4;
            }
        }
    };
}
}
