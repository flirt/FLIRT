// ========================================================================= //
// Copyright 2016-2018 SUPAMONKS_STUDIO                                      //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
//                                                                           //
// ========================================================================= //
#include <tiffio.h>

// #include <smks/sys/constants.hpp>    // type redefinition clash with libTIFF
#include <smks/sys/Log.hpp>
#include "Image-PImpl.hpp"

using namespace smks;

bool
img::Image::PImpl::initializeFromTIF(const std::string& filePath)
{
    dispose();

    TIFF* file = TIFFOpen(filePath.c_str(), "r");
    if (!file)
    {
        std::cerr << "Failed to load TIFF image from file '" << filePath << "'." << std::endl;
        return false;
    }

    uint32_t width  = 0;
    uint32_t height = 0;

    TIFFGetField(file, TIFFTAG_IMAGEWIDTH, &width);
    TIFFGetField(file, TIFFTAG_IMAGELENGTH, &height);
    if (width == 0 ||
        height == 0)
    {
        std::cerr << "Invalid image resolution stored in file '" << filePath << "'." << std::endl;
        TIFFClose(file);

        return false;
    }
    FLIRT_LOG(LOG(DEBUG)
        << "loading TIFF image from '" << filePath
        << "'\t-> width = " << width
        << "\theight = " << height << ".";)

    const size_t    numPixels   = width * height;
    uint32_t*       raster      = new uint32_t[numPixels];
    const int       ok          = TIFFReadRGBAImage(file, width, height, raster, 0);
    // WARNING: 'ok' equals 0 when an error occured
    if (ok)
    {
        const float one_over_255 = 1.0f/255.0f;

        float* data = new float[numPixels << 2];
        for (size_t i = 0, j = 0; i < numPixels; ++i)
        {
            const uint32_t pix = raster[i];
            data[j++] = static_cast<float>(( pix        & 0x000000ff)) * one_over_255;
            data[j++] = static_cast<float>(((pix >> 8)  & 0x000000ff)) * one_over_255;
            data[j++] = static_cast<float>(((pix >> 16) & 0x000000ff)) * one_over_255;
            data[j++] = static_cast<float>(((pix >> 24) & 0x000000ff)) * one_over_255;
        }
        // set image content
        size_t              stride          = 4;
        PixelInternalFormat internalFormat  = img::RGBA_INT_FORMAT;
        PixelFormat         format          = img::RGBA_FORMAT;

        initialize(width, height, stride, internalFormat, format, data, true);
        delete[] data;
        delete[] raster;
    }
    else
    {
        std::cerr << "Failed to retrieve RGBA image data from file '" << filePath << "'." << std::endl;
        delete[] raster;
        TIFFClose(file);

        return false;
    }
    TIFFClose(file);

    return true;
}
