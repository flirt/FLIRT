// ========================================================================= //
// Copyright 2016-2018 SUPAMONKS_STUDIO                                      //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
//                                                                           //
// ========================================================================= //
#include <iostream>

#include <smks/sys/Log.hpp>
#include "OutputImage-PImpl.hpp"

using namespace smks;

img::OutputImage::PImpl::PImpl():
    _self           (),
    _width          (0),
    _height         (0),
    _metadata       (parm::Container::create()),
    _layers         (),
    _layerChannels  ()
{
    clear();
}

void
img::OutputImage::PImpl::build(Ptr const& ptr)
{
    _self = ptr;
}

void
img::OutputImage::PImpl::clear()
{
    _width  = 0;
    _height = 0;
    _metadata       ->clear();
    _layerChannels  .clear();
    _layers         .clear();

    const unsigned int layerId = addLayer(4, "", nullptr);  // default layer used for main RGBA channels
    assert(layerId == 0);
}

void
img::OutputImage::PImpl::setResolution(size_t width, size_t height)
{
    _width  = width;
    _height = height;
}

void
img::OutputImage::PImpl::saveToFile(const std::string& filePath) const
{
#if defined(FLIRT_LOG_ENABLED)
    {
        std::stringstream sstr;

        // layers and channels
        for (LayerMap::const_iterator it = _layers.begin(); it != _layers.end(); ++it)
        {
            sstr << "- layer '" << it->second->name() << "'\n";
            for (size_t i = 0; i < it->second->numChannels(); ++i)
            {
                LayerChannel::Ptr const& channel = it->second->channel(i).lock();
                if (channel)
                    sstr << "\t[" << i << "] '" << channel->name() << "'\tstride = " << channel->stride() << "\n";
            }
        }

        // metadata
        const size_t    numKeys = _metadata->size();
        unsigned int*   keys    = new unsigned int[numKeys];

        _metadata->getKeys(numKeys, keys);
        for (size_t i = 0; i < numKeys; ++i)
            if (keys[i] > 0)
            {
                std::string parmName;

                _metadata->getName(keys[i], parmName);
                sstr << "- metadata '" << parmName << "'\n";
            }
        LOG(DEBUG) << sstr.str();
        delete[] keys;
    }
#endif //defined(FLIRT_LOG_ENABLED)

    if (_width == 0 || _height == 0)
    {
        std::stringstream sstr;
        sstr
            << "Cannot save image '" << filePath << "': "
            << "incorrect resolution.";
        throw sys::Exception(sstr.str().c_str(), "Output Image Save");
    }
    if (getLayerChannel("R") == nullptr ||
        getLayerChannel("G") == nullptr ||
        getLayerChannel("B") == nullptr)
    {
        std::stringstream sstr;
        sstr
            << "Cannot save image '" << filePath << "': "
            << "missing at least one R, G, or B channel.";
        throw sys::Exception(sstr.str().c_str(), "Output Image Save");
    }

    const std::string& ext = util::string::strlwr(filePath.substr(filePath.find_last_of('.')));

    if (ext == ".exr")
        saveToEXRFile(filePath);
    else if (ext == ".png")
        saveToPNGFile(filePath);
    else
        std::cerr << "Failed to save image '" << filePath << "': unsupported image extension '" << ext << "'." << std::endl;
}

const img::Layer*
img::OutputImage::PImpl::getLayer(const char* name) const
{
    LayerMap::const_iterator const& it = _layers.find(util::string::getId(name));
    return it != _layers.end()
        ? it->second.get()
        : nullptr;
}

img::LayerChannel::Ptr
img::OutputImage::PImpl::getLayerChannel(const char* name) const
{
    LayerChannelMap::const_iterator const& it = _layerChannels.find(util::string::getId(name));
    return it != _layerChannels.end()
        ? it->second
        : nullptr;
}

void
img::OutputImage::PImpl::getLayerChannelName(const Layer& layer, size_t channelIdx, std::string& ret) const
{
    ret = layer.name();
    if (layer.numChannels() > 4 ||
        strcmp(layer.name().c_str(), LAYER_ID) == 0 ||
        strcmp(layer.name().c_str(), LAYER_COVERAGE) == 0)
    {
        if (!ret.empty())
            ret += '_';
        ret += std::to_string(channelIdx);
    }
    else if (layer.numChannels() > 1)
    {
        if (!ret.empty())
            ret += '.';
        ret += channelIdx == 0 ? 'R' : (channelIdx == 1 ? 'G' : (channelIdx == 2 ? 'B' : 'A'));
    }
}

unsigned int
img::OutputImage::PImpl::addLayer(size_t numChannels, const char* name, AbstractPixelMapping::Ptr const& func)
{
    if (numChannels == 0)
    {
        std::stringstream sstr;
        sstr << "Invalid number count for layer '" << name << "'";
        throw sys::Exception(sstr.str().c_str(), "Add Layer");
    }

    Layer::Ptr const& layer = Layer::create(numChannels, name, func);

    if (_layers.find(layer->id()) != _layers.end())
    {
        std::stringstream sstr;
        sstr << "Name collision between layers '" << name << "' and '" << layer->name() << "'.'";
        throw sys::Exception(sstr.str().c_str(), "Add Layer");
    }
    _layers.insert(std::pair<unsigned int, Layer::Ptr>(layer->id(), layer));

    return layer->id();
}

void
img::OutputImage::PImpl::addLayerChannel(unsigned int   layerId,
                                         size_t         channelIdx,
                                         const char*    data,
                                         ChannelType    type,
                                         size_t         stride)
{
    LayerMap::iterator const& it = _layers.find(layerId);
    if (it == _layers.end())
    {
        std::stringstream sstr;
        sstr
            << "Failed to find layer with ID = " << layerId << ".";
        throw sys::Exception(sstr.str().c_str(), "Add Layer Channel");
    }
    assert(it->second);

    std::string name;
    getLayerChannelName(*it->second, channelIdx, name);

    LayerChannel::Ptr const& channel = LayerChannel::create(name.c_str(), layerId, data, type, stride);

    if (_layerChannels.find(channel->id()) != _layerChannels.end())
    {
        std::stringstream sstr;
        sstr << "Name collision between layer channel '" << name << "' and '" << channel->name() << "'.'";
        throw sys::Exception(sstr.str().c_str(), "Add Layer Channel");
    }
    _layerChannels.insert(std::pair<unsigned int, LayerChannel::Ptr>(channel->id(), channel));
    it->second->channel(channelIdx) = channel;
}

// static
void
img::OutputImage::PImpl::getLayerFilePath(const std::string&    filePath,
                                          const Layer&          layer,
                                          std::string&          ret)
{
    std::string layerName = "-" + (layer.name().empty() ? "RGBA" : layer.name());
    getLayerFilePath(filePath, layerName, ret);
}

// static
void
img::OutputImage::PImpl::getLayerFilePath(const std::string&    filePath,
                                          const std::string&    layerName,
                                          std::string&          ret)
{
    assert(!filePath.empty());
    assert(!layerName.empty());
    ret = filePath;

    // remove directory path
    const size_t lastSep1 = filePath.find_last_of('/');
    const size_t lastSep2 = filePath.find_last_of('\\');
    size_t lastSep = lastSep1 == std::string::npos
        ? lastSep2
        : (lastSep2 == std::string::npos
            ? lastSep1
            : std::max(lastSep1, lastSep2));

    std::string fileName = filePath;
    if (lastSep != std::string::npos)
        fileName = filePath.substr(lastSep+1);

    // place layer name before the first dot
    size_t firstDot = fileName.find_first_of('.');
    if (firstDot != std::string::npos &&
        lastSep != std::string::npos)
        firstDot += (lastSep+1);

    if (firstDot != std::string::npos)
        ret.insert(firstDot, layerName);
}
