// ========================================================================= //
// Copyright 2016-2018 SUPAMONKS_STUDIO                                      //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
//                                                                           //
// ========================================================================= //
#include <png.h>

#include <smks/sys/constants.hpp>
#include <smks/sys/Log.hpp>
#include "Image-PImpl.hpp"

using namespace smks;

bool
img::Image::PImpl::initializeFromPNG(const std::string& filePath)
{
    dispose();

    FILE *file = fopen(filePath.c_str(), "rb");
    if (!file)
    {
        std::cerr << "Failed to find PNG image file '" << filePath << "'." << std::endl;
        return false;
    }

    png_byte header[8];   // 8 is the maximum size that can be checked
    fread(header, 1, 8, file);
    if (png_sig_cmp(header, 0, 8) != 0)
    {
        std::cerr << "File '" << filePath << "' is not recognized as a PNG image." << std::endl;
        fclose(file);
        return false;
    }

    png_structp png_ptr     = png_create_read_struct(PNG_LIBPNG_VER_STRING, NULL, NULL, NULL);
    png_infop   info_ptr    = png_create_info_struct(png_ptr);
    png_infop   end_info    = png_create_info_struct(png_ptr);

    // the code in this if statement gets called if libpng encounters an error
    if (setjmp(png_jmpbuf(png_ptr)))
    {
        png_destroy_read_struct(&png_ptr, &info_ptr, &end_info);
        fclose(file);
        return false;
    }

    png_init_io(png_ptr, file);         // init png reading
    png_set_sig_bytes(png_ptr, 8);      // let libpng know you already read the first 8 bytes
    png_read_info(png_ptr, info_ptr);   // read all the info up to the image data

    // variables to pass to get info
    png_uint_32 width, height;
    int bitDepth, colorType;

    // get info about png
    png_get_IHDR(png_ptr, info_ptr, &width, &height, &bitDepth, &colorType, NULL, NULL, NULL);
    FLIRT_LOG(LOG(DEBUG)
        << "loading PNG image from '" << filePath
        << "'\t-> width = " << width
        << "\theight = " << height
        << "\tbit depth = " << bitDepth
        << "\t color type = " << colorType << ".";)

    if (width == 0 ||
        height == 0 ||
        (colorType != PNG_COLOR_TYPE_RGB && colorType != PNG_COLOR_TYPE_RGBA))
    {
        png_destroy_read_struct(&png_ptr, &info_ptr, &end_info);
        fclose(file);
        return false;
    }
    const size_t    numChannels     = colorType == PNG_COLOR_TYPE_RGBA ? 4 : 3;

    png_read_update_info(png_ptr, info_ptr); // Update the png info struct.

    const size_t    rowbytes        = png_get_rowbytes(png_ptr, info_ptr); // Row size in bytes.
    png_bytep*      row_pointers    = new png_bytep[height];
    png_byte*       image_data      = new png_byte[rowbytes * height];
    png_byte*       row_pointer     = image_data;

    for (size_t y = 0; y < height; ++y)
    {
        row_pointers[y] = row_pointer;
        row_pointer     += rowbytes;
    }
    png_read_image(png_ptr, row_pointers); // read the png into image_data through row_pointers

    float* data = new float[width * height * 4];
    size_t i = 0;
    for (size_t y = 0; y < height; ++y)
    {
        row_pointer =
#if defined(FLIRT_NO_YFLIPPED_IMG_LOADING)
            row_pointers[y];
#else
            row_pointers[height-1-y];
#endif // defined(FLIRT_NO_YFLIPPED_IMG_LOADING)

        size_t j = 0;
        for (size_t x = 0; x < width; ++x)
            for (size_t k = 0; k < 4; ++k)
                data[i++] = k < numChannels
                ? static_cast<float>(row_pointer[j++]) * static_cast<float>(sys::one_over_255)
                : 1.0f;
    }

    delete[] image_data;
    delete[] row_pointers;
    png_destroy_read_struct(&png_ptr, &info_ptr, &end_info);
    fclose(file);

    // set image content
    size_t              stride          = 4;
    PixelInternalFormat internalFormat  = img::RGBA_INT_FORMAT;
    PixelFormat         format          = img::RGBA_FORMAT;

    initialize(width, height, stride, internalFormat, format, data, true);
    delete[] data;

    return true;
}
