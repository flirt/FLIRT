// ========================================================================= //
// Copyright 2016-2018 SUPAMONKS_STUDIO                                      //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
//                                                                           //
// ========================================================================= //
#include "Image-PImpl.hpp"

using namespace smks;

img::Image::Image():
    AbstractImage   (),
    _pImpl          (new PImpl())
{ }

img::Image::~Image()
{
    delete _pImpl;
}

const math::Vector2i&
img::Image::resolution() const
{
    return _pImpl->resolution();
}

size_t
img::Image::width() const
{
    return _pImpl->width();
}

size_t
img::Image::height() const
{
    return _pImpl->height();
}

size_t
img::Image::stride() const
{
    return _pImpl->stride();
}

const float*
img::Image::data() const
{
    return _pImpl->data();
}

size_t
img::Image::numBytes() const
{
    return _pImpl->numBytes();
}

img::PixelFormat
img::Image::pixelFormat() const
{
    return _pImpl->pixelFormat();
}

img::PixelInternalFormat
img::Image::pixelInternalFormat() const
{
    return _pImpl->pixelInternalFormat();
}

math::Vector4f
img::Image::pixel(const math::Vector2i& ij, bool noClamping) const
{
    return _pImpl->pixel(ij, noClamping);
}

void
img::Image::dispose()
{
    _pImpl->dispose();
}

void
img::Image::initialize(size_t width, size_t height, size_t stride, PixelInternalFormat internalFormat, PixelFormat format, const float* data, bool copy)
{
    _pImpl->initialize(width, height, stride, internalFormat, format, data, copy);
}

void
img::Image::initialize(const char* filePath)
{
    _pImpl->initialize(filePath);
}
