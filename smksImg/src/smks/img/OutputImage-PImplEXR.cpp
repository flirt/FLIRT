// ========================================================================= //
// Copyright 2016-2018 SUPAMONKS_STUDIO                                      //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
//                                                                           //
// ========================================================================= //
#include <smks/sys/Log.hpp>

#include <ImathMath.h>
#include <ImathVec.h>
#include <ImathMatrix.h>
#include <ImfRgbaFile.h>
#include <ImfArray.h>
#include <ImfChannelList.h>
#include <ImfOutputFile.h>
#include <ImfStandardAttributes.h>

#include "OutputImage-PImpl.hpp"

namespace smks { namespace img
{
    class Buffer
    {
    public:
        typedef std::shared_ptr<Buffer> Ptr;
    private:
        ChannelType _type;
        char*       _data;

    public:
        static inline
        Ptr
        create()
        {
            Ptr ptr(new Buffer());
            return ptr;
        }

    public:
        Buffer();

    private:
        // non-copyable
        Buffer(const Buffer&);
        Buffer& operator=(const Buffer&);
    public:
        inline
        ~Buffer();

        inline
        char*
        data()
        {
            return _data;
        }

        void
        initialize(ChannelType, size_t, size_t, const char*, size_t, size_t, bool flipY);

        inline
        void
        dispose();
    };
}
}

using namespace smks;

///////////////
// class Buffer
///////////////
img::Buffer::Buffer():
    _type   (UNTYPED_CHANNEL),
    _data   (nullptr)
{ }

img::Buffer::~Buffer()
{
    dispose();
}

void
img::Buffer::initialize(ChannelType type,
                        size_t      width,
                        size_t      height,
                        const char* data,
                        size_t      xStride,    // in bytes
                        size_t      yStride,    // in bytes
                        bool        flipY)
{
    dispose();

    const size_t resolution = width * height;
    if (resolution == 0 ||
        UNTYPED_CHANNEL == 0 ||
        data    == nullptr ||
        xStride == 0 ||
        yStride == 0)
        return;

    _type = type;

    size_t  oxStride    = 0;
    size_t  oyStride    = 0;

    switch (type)
    {
    default:
        break;
    case smks::img::UINT_CHANNEL:
        _data       = reinterpret_cast<char*>(new unsigned int[resolution]);
        oxStride    = sizeof(unsigned int);
        oyStride    = sizeof(unsigned int) * width;
        break;
    case smks::img::HALF_CHANNEL:
        _data       = reinterpret_cast<char*>(new half[resolution]);
        oxStride    = sizeof(half);
        oyStride    = sizeof(half) * width;
        break;
    case smks::img::FLOAT_CHANNEL:
        _data       = reinterpret_cast<char*>(new float[resolution]);
        oxStride    = sizeof(float);
        oyStride    = sizeof(float) * width;
        break;
    }

    for (size_t y = 0, yy = height - 1; y < height; ++y, --yy)
        for (size_t x = 0; x < width; ++x)
        {
            const char* iptr = data     + xStride * x   + yStride * y;
            char*       optr = _data    + oxStride * x  + oyStride * (flipY ? yy : y);
            switch (type)
            {
            default: break;
            case UINT_CHANNEL:  *reinterpret_cast<unsigned int*>(optr) = *reinterpret_cast<const unsigned int*>(iptr);
                break;
            case HALF_CHANNEL:  *reinterpret_cast<half*>(optr) = *reinterpret_cast<const float*>(iptr);
                break;
            case FLOAT_CHANNEL: *reinterpret_cast<float*>(optr) = *reinterpret_cast<const float*>(iptr);
                break;
            }
        }
}
///////////////

void
img::Buffer::dispose()
{
    if (_data)
    {
        switch (_type)
        {
        default:
        case img::UNTYPED_CHANNEL:
            throw sys::Exception("Impossible to deallocate untyped buffer.", "OpenEXR Buffer Disposal");
        case img::UINT_CHANNEL:
            delete reinterpret_cast<unsigned int*>(_data);
            break;
        case img::HALF_CHANNEL:
            delete reinterpret_cast<half*>(_data);
            break;
        case img::FLOAT_CHANNEL:
            delete reinterpret_cast<float*>(_data);
            break;
        }
    }
    _data   = nullptr;
    _type   = img::UNTYPED_CHANNEL;
}


void
img::OutputImage::PImpl::saveToEXRFile(const std::string& filePath) const
{
    const bool FLIP_Y = true;

    const size_t            resolution          = _width * _height;
    const float             pixelAspectRatio    = 1.0f;
    const Imath::V2f        screenWindowCenter  (0.0f, 0.0f);
    const float             screenWindowWidth   = 1.0f;
    const Imf::LineOrder    lineOrder           = Imf::INCREASING_Y; // top scan line first
    const Imf::Compression  compression         = Imf::ZIP_COMPRESSION;

    Imf::Header header(
        static_cast<int>(_width),
        static_cast<int>(_height),
        //getDataWindow(width, height, _data, 1e-6f),
        pixelAspectRatio,
        screenWindowCenter,
        screenWindowWidth,
        lineOrder,
        compression
    );
    Imf::FrameBuffer frameBuffer;

    //---------
    // METADATA
    //---------
    const size_t    numKeys = _metadata->size();
    unsigned int*   keys    = new unsigned int[numKeys];

    _metadata->getKeys(numKeys, keys);
    for (size_t i = 0; i < numKeys; ++i)
        if (keys[i] > 0)
        {
            const unsigned int  parmId = keys[i];
            std::string         parmName;

            _metadata->getName(keys[i], parmName);
            if (_metadata->existsAs<size_t>(parmId))
            {
                const size_t parmValue = _metadata->get<size_t>(parmId);
                header.insert(parmName, Imf::TypedAttribute<int>(static_cast<int>(parmValue)));
            }
            else if (_metadata->existsAs<float>(parmId))
            {
                const float parmValue = _metadata->get<float>(parmId);
                header.insert(parmName, Imf::TypedAttribute<float>(parmValue));
            }
            else if (_metadata->existsAs<math::Vector2f>(parmId))
            {
                const math::Vector2f& parmValue = _metadata->get<math::Vector2f>(parmId);
                header.insert(parmName, Imf::V2fAttribute(Imath::V2f(parmValue.x(), parmValue.y())));
            }
            else if (_metadata->existsAs<math::Vector4f>(parmId))
            {
                const math::Vector4f& parmValue = _metadata->get<math::Vector4f>(parmId);
                header.insert(parmName + ".xy", Imf::V2fAttribute(Imath::V2f(parmValue.x(), parmValue.y())));
                header.insert(parmName + ".zw", Imf::V2fAttribute(Imath::V2f(parmValue.z(), parmValue.w())));
            }
            else if (_metadata->existsAs<math::Affine3f>(parmId))
            {
                const math::Affine3f& parmValue = _metadata->get<math::Affine3f>(parmId);
                Imath::M44f m44f;

                memcpy(m44f.getValue(), parmValue.matrix().data(), sizeof(float) << 4);
                header.insert(parmName, Imf::M44fAttribute(m44f));
            }
            else if (_metadata->existsAs<std::string>(parmId))
            {
                const std::string& parmValue = _metadata->get<std::string>(parmId);
                header.insert(parmName, Imf::StringAttribute(parmValue));
            }
            else if (_metadata->existsAs<bool>(parmId))
            {
                const bool parmValue = _metadata->get<bool>(parmId);
                header.insert(parmName, Imf::StringAttribute(parmValue ? "true" : "false"));
            }
        }
    delete[] keys;

    //---------
    // CHANNELS
    //---------
    std::vector<Buffer::Ptr> buffers;

    buffers.reserve(_layerChannels.size());
    for (LayerChannelMap::const_iterator it = _layerChannels.begin(); it != _layerChannels.end(); ++it)
    {
        std::cout << "img::OutputImage::PImpl::saveToEXRFile: channel name " << it->second->name() << std::endl;
        header.channels().insert(
            it->second->name(),
            it->second->type() == UINT_CHANNEL ? Imf::UINT : (it->second->type() == FLOAT_CHANNEL ? Imf::FLOAT : Imf::HALF));

        if (it->second->type() == UINT_CHANNEL)
        {
            buffers.push_back(Buffer::create());
            buffers.back()->initialize(
                UINT_CHANNEL,
                _width,
                _height,
                it->second->data(),
                it->second->stride() * sizeof(unsigned int),
                it->second->stride() * _width * sizeof(unsigned int),
                FLIP_Y);

            frameBuffer.insert(
                it->second->name(),
                Imf::Slice(
                    Imf::UINT,
                    buffers.back()->data(),
                    sizeof(unsigned int),
                    sizeof(unsigned int) * _width));
        }
        else if (it->second->type() == FLOAT_CHANNEL)
        {
            buffers.push_back(Buffer::create());
            buffers.back()->initialize(
                FLOAT_CHANNEL,
                _width,
                _height,
                it->second->data(),
                it->second->stride() * sizeof(float),
                it->second->stride() * _width * sizeof(float),
                FLIP_Y);

            frameBuffer.insert(
                it->second->name(),
                Imf::Slice(
                    Imf::FLOAT,
                    buffers.back()->data(),
                    sizeof(float),
                    sizeof(float) * _width));
        }
        else if (it->second->type() == HALF_CHANNEL)
        {
            buffers.push_back(Buffer::create());
            buffers.back()->initialize(
                HALF_CHANNEL,
                _width,
                _height,
                it->second->data(),
                it->second->stride() * sizeof(float),
                it->second->stride() * _width * sizeof(float),
                FLIP_Y);

            frameBuffer.insert(
                it->second->name(),
                Imf::Slice(
                    Imf::HALF,
                    buffers.back()->data(),
                    sizeof(half),
                    sizeof(half) * _width));
        }
    }

    try
    {
        //-------------------
        // SAVE OpenEXR IMAGE
        //-------------------
        Imf::OutputFile file(filePath.c_str(), header);

        file.setFrameBuffer(frameBuffer);
        file.writePixels(static_cast<int>(_height));
    }
    catch (const std::exception &e)
    {
        std::stringstream sstr;
        sstr
            << "Unable to write layered image file '" << filePath << "': "
            << e.what();
        throw sys::Exception(sstr.str().c_str(), "Output EXR Image Save");
    }
}
