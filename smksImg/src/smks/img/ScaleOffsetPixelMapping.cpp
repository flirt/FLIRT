// ========================================================================= //
// Copyright 2016-2018 SUPAMONKS_STUDIO                                      //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
//                                                                           //
// ========================================================================= //
#include <cassert>
#include <memory>
#include "smks/img/ScaleOffsetPixelMapping.hpp"

namespace smks { namespace img
{
    class ScaleOffsetPixelMapping::PImpl
    {
    private:
        enum { UNCONSTRAINED_NUM_CHANNELS = -1 };
    private:
        size_t _num;
        float* _scale;
        float* _offset;
    public:
        inline
        PImpl(float scale, float offset):
            _num    (UNCONSTRAINED_NUM_CHANNELS),
            _scale  (new float[1]),
            _offset (new float[1])
        {
            _scale[0]   = scale;
            _offset[0]  = offset;
        }

        inline
        PImpl(size_t num, const float* scale, const float* offset):
            _num    (num > 0 ? num : UNCONSTRAINED_NUM_CHANNELS),
            _scale  (new float[num > 0 ? num : 1]),
            _offset (new float[num > 0 ? num : 1])
        {
            if (_num > 0)
            {
                memcpy(_scale, scale, sizeof(float) * _num);
                memcpy(_offset, offset, sizeof(float) * _num);
            }
            else
            {
                _scale[0]   = 1.0f;
                _offset[0]  = 0.0f;
            }
        }

        inline
        ~PImpl()
        {
            if (_scale)     delete[] _scale;
            if (_offset)    delete[] _offset;
        }

        inline
        void
        operator()(size_t x, size_t y, size_t numChannels, const float* iPixel, float* oPixel) const
        {
            if (_num == UNCONSTRAINED_NUM_CHANNELS)
                for (size_t i = 0; i < numChannels; ++i)
                    oPixel[i] = _offset[0] + _scale[0] * iPixel[i];
            else
            {
                assert(_num == numChannels);
                for (size_t i = 0; i < numChannels; ++i)
                    oPixel[i] = _offset[i] + _scale[i] * iPixel[i];
            }
        }
    };
}
}

using namespace smks;

img::ScaleOffsetPixelMapping::ScaleOffsetPixelMapping(float scale, float offset):
    _pImpl(new PImpl(scale, offset))
{ }

img::ScaleOffsetPixelMapping::ScaleOffsetPixelMapping(size_t num, const float* scale, const float* offset):
    _pImpl(new PImpl(num, scale, offset))
{ }

img::ScaleOffsetPixelMapping::~ScaleOffsetPixelMapping()
{
    delete _pImpl;
}

void
img::ScaleOffsetPixelMapping::operator()(size_t x, size_t y, size_t numChannels, const float* iPixel, float* oPixel) const
{
    return _pImpl->operator()(x, y, numChannels, iPixel, oPixel);
}
