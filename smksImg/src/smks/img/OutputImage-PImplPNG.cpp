// ========================================================================= //
// Copyright 2016-2018 SUPAMONKS_STUDIO                                      //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
//                                                                           //
// ========================================================================= //
#include <cassert>
#include <smks/sys/Log.hpp>
#include "saveToFile.hpp"
#include "OutputImage-PImpl.hpp"

using namespace smks;

void
img::OutputImage::PImpl::saveToPNGFile(const std::string& filePath) const
{
    if (filePath.empty() ||
        _layers.empty())
        return;

    // get global alpha channel
    LayerChannel::Ptr alpha = getLayerChannel("A");

    for (LayerMap::const_iterator it = _layers.begin(); it != _layers.end(); ++it)
    {
        if (!it->second)
            continue;

        const Layer& layer = *it->second;
        if (layer.numChannels() == 0 ||
            layer.numChannels() > 4 ||
            strcmp(layer.name().c_str(), LAYER_ID) == 0 ||
            strcmp(layer.name().c_str(), LAYER_COVERAGE) == 0)
            continue;

        LayerChannel::Ptr channels[4];
        if (layer.numChannels() == 1)
            for (size_t k = 0; k < 3; ++k)
                channels[k] = layer.channel(0).lock();
        else
            for (size_t k = 0; k < layer.numChannels(); ++k)
                channels[k] = layer.channel(k).lock();
        channels[3] = alpha;

        const float*    ptr[4];
        size_t          stride[4];
        for (size_t k = 0; k < 4; ++k)
            if (channels[k] &&
                (channels[k]->type() == img::FLOAT_CHANNEL || channels[k]->type() == img::HALF_CHANNEL))
            {
                ptr[k]      = reinterpret_cast<const float*>(channels[k]->data());
                stride[k]   = channels[k]->stride();
            }

        std::string layerFilePath;

        getLayerFilePath(filePath, layer, layerFilePath);
        ::img::saveToPNGFile(layerFilePath.c_str(), _width, _height, 4, ptr, stride, layer.func());
    }

    // save shape ID/coverage data
    {
        const Layer* layerId        = getLayer(LAYER_ID);
        const Layer* layerCoverage  = getLayer(LAYER_COVERAGE);
        const size_t resolution     = _width * _height;

        if (layerId && layerCoverage && resolution > 0)
        {
            typedef boost::unordered_map<unsigned int, float*> ShapeIdToMask;
            ShapeIdToMask   shapeMasks;

            const size_t numChannels = layerId->numChannels();
            assert(layerId->numChannels() == layerCoverage->numChannels());

            for (size_t i = 0; i < numChannels; ++i)
            {
                LayerChannel::Ptr const& channelId  = layerId->channel(i).lock();
                LayerChannel::Ptr const& channelCov = layerCoverage->channel(i).lock();
                if (!channelId || !channelCov)
                    continue;

                const unsigned int* dataId  = reinterpret_cast<const unsigned int*> (channelId->data());
                const float*        dataCov = reinterpret_cast<const float*>        (channelCov->data());
                const size_t        strdId  = channelId ->stride();
                const size_t        strdCov = channelCov->stride();
                assert(dataId && strdId > 0);
                assert(dataCov && strdCov > 0);

                for (size_t p = 0; p < resolution; ++p)
                {
                    const unsigned int  id  = *dataId;
                    const float         cov = *dataCov;

                    if (id > 0)
                    {
                        float* mask = nullptr;
                        ShapeIdToMask::iterator const& it = shapeMasks.find(id);
                        if (it == shapeMasks.end())
                        {
                            mask = new float[resolution];
                            memset(mask, 0, resolution * sizeof(float));
                            shapeMasks.insert(std::pair<unsigned int, float*>(id, mask));
                        }
                        else
                            mask = it->second;
                        assert(mask);
                        mask[p] = cov;
                    }

                    dataId  += strdId;
                    dataCov += strdCov;
                }
            }

            // save all mask in separate images and deallocate memory
            for (ShapeIdToMask::iterator it = shapeMasks.begin(); it != shapeMasks.end(); ++it)
            {
                const float* data   = it->second;
                const size_t stride = 1;
                std::string layerFilePath;

                getLayerFilePath(filePath, "-ID-" + std::to_string(it->first), layerFilePath);
                ::img::saveToPNGFile(layerFilePath.c_str(), _width, _height, 1, &data, &stride, nullptr);
                delete[] it->second;
            }
            shapeMasks.clear();
        }
    }
}
