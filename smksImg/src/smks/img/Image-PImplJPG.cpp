// ========================================================================= //
// Copyright 2016-2018 SUPAMONKS_STUDIO                                      //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
//                                                                           //
// ========================================================================= //
# define XMD_H /* This prevents libjpeg to redefine INT32 */

extern "C"
{
#include <jinclude.h>
#include <jpeglib.h>
#include <jerror.h>
}
#include <smks/sys/constants.hpp>
#include <smks/sys/Log.hpp>
#include "Image-PImpl.hpp"

using namespace smks;

bool
img::Image::PImpl::initializeFromJPG(const std::string& filePath)
{
    dispose();

    FILE* file  = fopen(filePath.c_str(), "rb");    // open the file
    if (!file)
    {
        std::cerr << "Failed to find JPEG image file '" << filePath << "'." << std::endl;
        return false;
    }

    struct jpeg_decompress_struct   info;   // for our jpeg info
    struct jpeg_error_mgr           err;    // the error handler

    info.err = jpeg_std_error(&err);    // set error manager
    jpeg_create_decompress(&info);      // create JPEG decompressor object
    jpeg_stdio_src(&info, file);        // specify data source
    jpeg_read_header(&info, TRUE);      // read jpeg file header
    info.out_color_space = JCS_RGB;     // set target image format
    jpeg_start_decompress(&info);       // decompress the file

    const JDIMENSION    width       = info.output_width;
    const JDIMENSION    height      = info.output_height;
    const int           numChannels = info.out_color_components;

    size_t              stride          = 4;
    PixelInternalFormat internalFormat  = img::RGBA_INT_FORMAT;
    PixelFormat         format          = img::RGBA_FORMAT;

    FLIRT_LOG(LOG(DEBUG)
        << "loading JPG image from '" << filePath
        << "'\t-> width = " << width
        << "\theight = " << height
        << "\t# channels = " << numChannels << ".";)

    if (width == 0 ||
        height == 0 ||
        (numChannels != 3 && numChannels != 4))
    {
        jpeg_destroy_decompress(&info);
        fclose(file);
        return false;
    }
    assert(numChannels <= 4);

    // temporary buffers used for decompression
    float*      data    = new float[(width * height) << 2]; // RGBA
    JSAMPLE*    row     = new JSAMPLE[width * numChannels];

    size_t y = 0;
    while (info.output_scanline < info.output_height)
    {
        jpeg_read_scanlines(&info, &row, 1);    // *pRows = row

        float* dataRow =
#if defined(FLIRT_NO_YFLIPPED_IMG_LOADING)
            data + ((y * width) << 2);
#else
            data + (((height - 1 - y) * width) << 2);
#endif // defined(FLIRT_NO_YFLIPPED_IMG_LOADING)

        size_t j = 0;
        for (size_t x = 0; x < width; ++x)
            for (size_t k = 0; k < 4; ++k)
            {
                *dataRow = k < numChannels
                    ? static_cast<float>(row[j++]) * static_cast<float>(sys::one_over_255)
                    : 1.0f;
                ++dataRow;
            }
        ++y;
    }
    jpeg_finish_decompress(&info);

    delete[] row;
    jpeg_destroy_decompress(&info);
    fclose(file);

    // set image content
    initialize(width, height, stride, internalFormat, format, data, true);
    delete[] data;

    return true;
}
