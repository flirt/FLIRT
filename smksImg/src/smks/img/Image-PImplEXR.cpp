// ========================================================================= //
// Copyright 2016-2018 SUPAMONKS_STUDIO                                      //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
//                                                                           //
// ========================================================================= //
#include <smks/sys/Log.hpp>

#include <ImfRgbaFile.h>
#include <ImfArray.h>

#include "Image-PImpl.hpp"

using namespace smks;

bool
img::Image::PImpl::initializeFromEXR(const std::string& filePath)
{
    dispose();

    Imf::RgbaInputFile  file(filePath.c_str());
    const Imath::Box2i& dw  = file.dataWindow();

    if (dw.isEmpty())
        return false;

    size_t              width           = dw.max.x - dw.min.x + 1;
    size_t              height          = dw.max.y - dw.min.y + 1;
    size_t              stride          = 4;
    PixelInternalFormat internalFormat  = img::RGBA_INT_FORMAT;
    PixelFormat         format          = img::RGBA_FORMAT;
    size_t              numBytes        = width * height * stride * sizeof(float);

    FLIRT_LOG(LOG(DEBUG)
        << "loading EXR image from '" << filePath
        << "'\t-> width = " << width
        << "\theight = " << height << ".";)


    Imf::Array2D<Imf::Rgba> pixels(height, width);
    file.setFrameBuffer(&pixels[0][0] - dw.min.x - dw.min.y * width, 1, width);
    file.readPixels(dw.min.y, dw.max.y);

    float* newData = new float[numBytes / sizeof(float)];

    if (file.lineOrder() == Imf::INCREASING_Y)
        for (size_t y = 0; y < height; ++y)
            for (size_t x = 0; x < width; ++x)
            {
#if defined(FLIRT_NO_YFLIPPED_IMG_LOADING)
                const Imf::Rgba& c = pixels[y][x];
#else
                const Imf::Rgba& c = pixels[height-1-y][x];
#endif // defined(FLIRT_NO_YFLIPPED_IMG_LOADING)
                float* dst = newData + ((x + width * y) << 2);

                dst[0] = c.r;
                dst[1] = c.g;
                dst[2] = c.b;
                dst[3] = c.a;
            }
    else
        for (size_t y = 0, yr = height - 1; y < height; ++y, --yr)
            for (size_t x = 0; x < width; ++x)
            {
                const Imf::Rgba& c =
#if defined(FLIRT_NO_YFLIPPED_IMG_LOADING)
                    pixels[y][x];
#else
                    pixels[height-1-y][x];
#endif // defined(FLIRT_NO_YFLIPPED_IMG_LOADING)

                float* dst = newData + ((x + width * yr) << 2);

                dst[0] = c.r;
                dst[1] = c.g;
                dst[2] = c.b;
                dst[3] = c.a;
            }

    initialize(width, height, stride, internalFormat, format, newData, true);
    delete[] newData;

    return true;
}
