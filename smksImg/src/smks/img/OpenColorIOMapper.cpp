// ========================================================================= //
// Copyright 2016-2018 SUPAMONKS_STUDIO                                      //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
//                                                                           //
// ========================================================================= //
#include <iostream>
#include <string>
#include <OpenColorIO/OpenColorIO.h>

#include <smks/sys/Log.hpp>
#include "smks/img/OpenColorIOMapper.hpp"

namespace smks { namespace img
{
    class OpenColorIOMapper::PImpl
    {
    private:
        enum { INVALID_IDX=-1 };
    private:
        std::string                     _configPath;
        OpenColorIO::ConstConfigRcPtr   _config;
        int                             _inputColorspace;
        int                             _display;
        int                             _displayView;
        float                           _exposureFstops;
    public:
        inline
        PImpl():
            _configPath     (),
            _config         (nullptr),
            _inputColorspace(INVALID_IDX),
            _display        (INVALID_IDX),
            _displayView    (INVALID_IDX),
            _exposureFstops (0.0f)
        { }

        inline
        const char*
        configPath() const
        {
            return _configPath.c_str();
        }
        inline
        const char*
        inputColorspace() const
        {
            return _config && _inputColorspace >=0 && _inputColorspace < _config->getNumColorSpaces()
                ? _config->getColorSpaceNameByIndex(_inputColorspace)
                : "";
        }
        inline
        const char*
        display() const
        {
            return _config && _display >=0 && _display < _config->getNumDisplays()
                ? _config->getDisplay(_display)
                : "";
        }
        inline
        const char*
        displayView() const
        {
            return _config && _displayView >= 0 && _displayView < _config->getNumViews(display())
                ? _config->getView(display(), _displayView)
                : "";
        }
        inline
        float
        exposureFstops() const
        {
            return _exposureFstops;
        }

        inline
        void
        configPath(const std::string&);
        inline
        void
        inputColorspace(const std::string&);
        inline
        void
        displayView(const std::string&, const std::string&);
        inline
        void
        exposureFstops(float value)
        {
            _exposureFstops = value;
        }

        inline
        void
        applyRGBA(size_t width, size_t height, const float*, float*) const;

    private:
        inline
        std::ostream&
        printConfig(std::ostream&) const;

        inline
        int
        getColorspaceIndex(const std::string&) const;
        inline
        int
        getDisplayIndex(const std::string&) const;
        inline
        int
        getDisplayViewIndex(const char*, const std::string&) const;
    };
}
}

using namespace smks;

/////////////////////////////////
// class OpenColorIOMapper::PImpl
/////////////////////////////////
void
img::OpenColorIOMapper::PImpl::configPath(const std::string& path)
{
    _configPath.clear();
    _config = nullptr;

    const char* prvInputColorSpace  = inputColorspace();
    const char* prvDisplay          = display();
    const char* prvDisplayView      = displayView();

    try
    {
        // create the OpenColorIO configuration from the specified file
        _config = OpenColorIO::Config::CreateFromFile(path.c_str());
        if (_config)
        {
            _configPath = path;

            // try to keep previous settings
            inputColorspace(prvInputColorSpace);
            displayView(prvDisplay, prvDisplayView);
            // print configuration content
            printConfig(std::cout) << std::endl;
        }
    }
    catch(const OpenColorIO::Exception& e)
    {
        if (!path.empty())
            std::cerr << "Failed to configure OpenColorIO mapper from file '" << path << "'.\nOpenColorIO error: " << e.what() << std::endl;

        _config = nullptr;
        _configPath.clear();
        _inputColorspace    = INVALID_IDX;
        _display            = INVALID_IDX;
        _displayView        = INVALID_IDX;
    }
}

void
img::OpenColorIOMapper::PImpl::inputColorspace(const std::string& inputColorspace)
{
    _inputColorspace = getColorspaceIndex(inputColorspace); // input colorspace as determined by users

    if (_inputColorspace == INVALID_IDX)
    {
        // try to fallback to scene's linear colorspace (valid configurations should propose one)
        OpenColorIO::ConstColorSpaceRcPtr linear = _config->getColorSpace(OpenColorIO::ROLE_SCENE_LINEAR);
        if (linear)
            _inputColorspace = getColorspaceIndex(linear->getName());
    }
}

void
img::OpenColorIOMapper::PImpl::displayView(const std::string& display, const std::string& displayView)
{
    _display = getDisplayIndex(display);    // display as determined by users
    if (_display == INVALID_IDX)
        _display = getDisplayIndex(_config->getDefaultDisplay());   // try to fallback to configuration's default display

    _displayView = getDisplayViewIndex(this->display(), displayView);   // display view as determined by users
    if (_displayView == INVALID_IDX)
        _displayView = getDisplayViewIndex(this->display(), _config->getDefaultView(this->display()));  // try to fallback to display's default view
}


int
img::OpenColorIOMapper::PImpl::getColorspaceIndex(const std::string& query) const
{
    if (!_config || query.empty())
        return INVALID_IDX;
    std::string q = query;
    std::transform(q.begin(), q.end(), q.begin(), ::tolower);
    for (int i = 0; i < _config->getNumColorSpaces(); ++i)
    {
        std::string name(_config->getColorSpaceNameByIndex(i));
        std::transform(name.begin(), name.end(), name.begin(), ::tolower);
        if (strcmp(q.c_str(), name.c_str()) == 0)
            return i;
    }
    return INVALID_IDX;
}
int
img::OpenColorIOMapper::PImpl::getDisplayIndex(const std::string& query) const
{
    if (!_config || query.empty())
        return INVALID_IDX;
    std::string q = query;
    std::transform(q.begin(), q.end(), q.begin(), ::tolower);
    for (int i = 0; i < _config->getNumDisplays(); ++i)
    {
        std::string name(_config->getDisplay(i));
        std::transform(name.begin(), name.end(), name.begin(), ::tolower);
        if (strcmp(q.c_str(), name.c_str()) == 0)
            return i;
    }
    return INVALID_IDX;
}
int
img::OpenColorIOMapper::PImpl::getDisplayViewIndex(const char* display, const std::string& query) const
{
    if (!_config || strlen(display) == 0 || query.empty())
        return INVALID_IDX;
    std::string q = query;
    std::transform(q.begin(), q.end(), q.begin(), ::tolower);
    for (int i = 0; i < _config->getNumViews(display); ++i)
    {
        std::string name(_config->getView(display,i));
        std::transform(name.begin(), name.end(), name.begin(), ::tolower);
        if (strcmp(q.c_str(), name.c_str()) == 0)
            return i;
    }
    return INVALID_IDX;
}

std::ostream&
img::OpenColorIOMapper::PImpl::printConfig(std::ostream& out) const
{
    //_config->getDefaultDisplay()
    if (!_config)
        return out;

    out << "\n-----\nOpenColorIO configuration from '" << _configPath << "'" << std::endl;

    float lumacoef[3];
    _config->getDefaultLumaCoefs(lumacoef);
    out << "luma coefficients = (" << lumacoef[0] << "\t" << lumacoef[1] << "\t" << lumacoef[2] << ")\n";

    const int numColorspaces    = _config->getNumColorSpaces();
    const int numDisplays       = _config->getNumDisplays();
    if (numColorspaces > 0)
    {
        out << numColorspaces << " colorspace" << (numColorspaces > 1 ? "s" : "") << "\n";
        for (int i = 0; i < _config->getNumColorSpaces(); ++i)
            out << "  [" << i << "]\t'" << _config->getColorSpaceNameByIndex(i) << "'\n";
    }
    if (numDisplays > 0)
    {
        out << numDisplays << " display" << (numDisplays > 1 ? "s" : "") << "\n";
        for (int i = 0; i < _config->getNumDisplays(); ++i)
        {
            const char* display         = _config->getDisplay(i);
            const int   numDisplayViews = _config->getNumViews(display);
            out << "  [" << i << "]\t'" << display << "'\n  " << numDisplayViews << " view" << (numDisplayViews > 1 ? "s" : "") << "\n";
            for (int j = 0; j < numDisplayViews; ++j)
                out << "    [" << j << "]\t'" << _config->getView(display, j) << "'\n";
        }
    }
    out << "-----\n";
    return out;
}

void
img::OpenColorIOMapper::PImpl::applyRGBA(size_t         width,
                                         size_t         height,
                                         const float*   iRGBA,
                                         float*         oRGBA) const
{
    // initialize output data with input data (no transform)
    memcpy(oRGBA, iRGBA, sizeof(float) * width * height << 2);

    if (_config == nullptr)
    {
        std::cerr << "Abort color transform: No OpenColorIO configuration set." << std::endl;
        return;
    }
    if (_inputColorspace == INVALID_IDX)
    {
        std::cerr << "Abort color transform: No valid input OpenColorIO colorspace set." << std::endl;
        return;
    }
    if (_display == INVALID_IDX || _displayView == INVALID_IDX)
    {
        std::cerr << "Abort color transform: No valid OpenColorIO display and/or view." << std::endl;
        return;
    }

    FLIRT_LOG(LOG(DEBUG) << "apply OpenColorIO transform"
        << "\n\t- input color space = '" << inputColorspace() << "'"
        << "\n\t- exposure f-stops = " << _exposureFstops
        << "\n\t- display = '" << display() << "'.'" << displayView() << "'";)

    OpenColorIO::DisplayTransformRcPtr trf = OpenColorIO::DisplayTransform::Create();

    trf->setInputColorSpaceName(inputColorspace());

    // Add an fstop exposure control (in SCENE_LINEAR)
    OpenColorIO::CDLTransformRcPtr cc   = OpenColorIO::CDLTransform::Create();
    const float gain                    = powf(2.0f, _exposureFstops);
    const float slope3f[]               = { gain, gain, gain };
    cc->setSlope(slope3f);
    trf->setLinearCC(cc);

    // 'channelHot' controls which channels are viewed.
    int channelHot[4] = { 1, 1, 1, 1 };  // show rgba
    float lumacoef[3];
    _config->getDefaultLumaCoefs(lumacoef);

    float m44[16];
    float offset[4];
    OpenColorIO::MatrixTransform::View(m44, offset, channelHot, lumacoef);
    OpenColorIO::MatrixTransformRcPtr swizzle = OpenColorIO::MatrixTransform::Create();
    swizzle->setValue(m44, offset);
    trf->setChannelView(swizzle);

    trf->setDisplay(display());
    trf->setView(displayView());

    // get OpenColorIO processor and apply it to (mutable) output image data
    OpenColorIO::ConstProcessorRcPtr    proc = _config->getProcessor(trf);
    OpenColorIO::PackedImageDesc        img (oRGBA, width, height, 4);

    proc->apply(img);
}

//////////////////////////
// class OpenColorIOMapper
//////////////////////////
// static
img::OpenColorIOMapper::Ptr
img::OpenColorIOMapper::create()
{
    Ptr ptr(new OpenColorIOMapper());
    return ptr;
}

img::OpenColorIOMapper::OpenColorIOMapper():
    _pImpl(new PImpl())
{ }
img::OpenColorIOMapper::~OpenColorIOMapper()
{
    delete _pImpl;
}

const char*
img::OpenColorIOMapper::configPath() const
{
    return _pImpl->configPath();
}
const char*
img::OpenColorIOMapper::inputColorspace() const
{
    return _pImpl->inputColorspace();
}
const char*
img::OpenColorIOMapper::display() const
{
    return _pImpl->display();
}
const char*
img::OpenColorIOMapper::displayView() const
{
    return _pImpl->displayView();
}
float
img::OpenColorIOMapper::exposureFstops() const
{
    return _pImpl->exposureFstops();
}

void
img::OpenColorIOMapper::configPath(const char* value)
{
    return _pImpl->configPath(value);
}
void
img::OpenColorIOMapper::inputColorspace(const char* value)
{
    return _pImpl->inputColorspace(value);
}
void
img::OpenColorIOMapper::displayView(const char* display, const char* displayView)
{
    return _pImpl->displayView(display, displayView);
}
void
img::OpenColorIOMapper::exposureFstops(float value)
{
    return _pImpl->exposureFstops(value);
}

void
img::OpenColorIOMapper::applyRGBA(size_t width, size_t height, const float* iRGBA, float* oRGBA) const
{
    return _pImpl->applyRGBA(width, height, iRGBA, oRGBA);
}
