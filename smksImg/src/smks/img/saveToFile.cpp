// ========================================================================= //
// Copyright 2016-2018 SUPAMONKS_STUDIO                                      //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
//                                                                           //
// ========================================================================= //
#include <cassert>
#include <iostream>
#include <png.h>

#include <smks/math/math.hpp>
#include "smks/img/AbstractPixelMapping.hpp"
#include "saveToFile.hpp"

namespace smks { namespace img
{
    static inline
    bool
    inputsOK(size_t width, size_t height, size_t, const float**, const size_t*);
}
}

using namespace smks;

void
img::saveToPNGFile(const char*                      path,
                   size_t                           width,
                   size_t                           height,
                   size_t                           iNumChannels,
                   const float**                    iPtr,
                   const size_t*                    iStride,
                   AbstractPixelMapping::Ptr const& func)
{
    if (!inputsOK(width, height, iNumChannels, iPtr, iStride))
    {
        std::cerr << "Failed to save PNG image file '" << path << "': incorrect input data." << std::endl;
        return;
    }
    assert(iNumChannels <= 4);

    FILE* file = fopen(path, "wb");
    if (!file)
    {
        std::cerr << "Failed to open PNG image file '" << path << "' for writing." << std::endl;
        return;
    }

    const int   bitDepth        = 8;
    int         colorType       = iNumChannels == 4 ? PNG_COLOR_MASK_COLOR | PNG_COLOR_MASK_ALPHA : PNG_COLOR_MASK_COLOR;
    size_t      oNumChannels    = colorType == PNG_COLOR_MASK_COLOR ? 3 : 4;

    png_structp png_ptr = png_create_write_struct(PNG_LIBPNG_VER_STRING, NULL, NULL, NULL);
    png_init_io(png_ptr, file);

    png_infop info_ptr = png_create_info_struct(png_ptr);

    png_set_IHDR(png_ptr, info_ptr,
                 static_cast<int>(width), static_cast<int>(height),
                 bitDepth, colorType,
                 PNG_INTERLACE_NONE,
                 PNG_COMPRESSION_TYPE_DEFAULT,
                 PNG_FILTER_TYPE_DEFAULT);
    png_write_info(png_ptr, info_ptr);
    // To remove the alpha channel for PNG_COLOR_TYPE_RGB format,
    // Use png_set_filler().
    //png_set_filler(png_ptr, 0, PNG_FILLER_AFTER);

    // prepare image data
    png_byte* oData = new png_byte[width * height * oNumChannels];
    memset(oData, 0, sizeof(png_byte) * width * height * oNumChannels);

    const size_t    rowSize = width * oNumChannels;
    png_bytep*      rows    = new png_bytep[height];
    for (size_t y = 0; y < height; ++y)
        rows[y] = oData + y * rowSize;

    const float** ptr = new const float*[iNumChannels];
    for (size_t k = 0; k < iNumChannels; ++k)
        ptr[k] = iPtr[k];

    float*      preTrfPixel     = new float[iNumChannels];
    float*      postTrfPixel    = new float[iNumChannels];
    png_byte*   pixData         = new png_byte[iNumChannels];
    for (size_t y = 0; y < height; ++y)
    {
        png_bytep oRow = rows[height-1-y];
        for (size_t x = 0; x < width; ++x)
        {
            for (size_t k = 0; k < iNumChannels; ++k)
            {
                preTrfPixel[k]  = *ptr[k];
                postTrfPixel[k] = *ptr[k];
            }
            if (func)
                (*func)(x, y, iNumChannels, preTrfPixel, postTrfPixel);
            for (size_t k = 0; k < iNumChannels; ++k)
                pixData[k] = static_cast<png_byte>(math::clamp(
                    static_cast<int>(math::floor(postTrfPixel[k] * 256.0f)),
                    0, 255));
            //---
            for (size_t k = 0; k < oNumChannels; ++k)
            {
                if (k < 3)
                    oRow[k] = k < iNumChannels
                    ? pixData[k]
                    : (iNumChannels == 1 ? pixData[0] : 0);
                else
                    oRow[k] = 4 <= iNumChannels ? pixData[k] : 255; // alpha channel

            }
            oRow += oNumChannels;
            //---
            for (size_t k = 0; k < iNumChannels; ++k)
                ptr[k] += iStride[k];
        }
    }
    delete[] pixData;
    delete[] preTrfPixel;
    delete[] postTrfPixel;
    delete[] ptr;

    png_write_image(png_ptr, rows);
    png_write_end(png_ptr, NULL);

    delete[] oData;
    delete[] rows;
    png_destroy_write_struct(&png_ptr, &info_ptr);
    fclose(file);
}

// static
bool
img::inputsOK(size_t width, size_t height, size_t numChannels, const float** ptr, const size_t* stride)
{
    if (width == 0 || height == 0 ||
        numChannels == 0 || numChannels > 4 ||
        ptr == nullptr ||
        stride == nullptr)
        return false;
    for (size_t i = 0; i < numChannels; ++i)
        if (ptr[i] == nullptr || stride[i] == 0)
            return false;
    return true;
}
