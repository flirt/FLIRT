// ========================================================================= //
// Copyright 2016-2018 SUPAMONKS_STUDIO                                      //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
//                                                                           //
// ========================================================================= //
#include "smks/img/AbstractPixelMapping.hpp"
#include "OutputImage-PImpl.hpp"

using namespace smks;

// static
img::OutputImage::Ptr
img::OutputImage::create()
{
    Ptr ptr(new OutputImage());
    ptr->build(ptr);
    return ptr;
}


img::OutputImage::OutputImage():
    _pImpl(new PImpl())
{ }

img::OutputImage::~OutputImage()
{
    delete _pImpl;
}

void
img::OutputImage::clear()
{
    _pImpl->clear();
}

void
img::OutputImage::build(Ptr const& ptr)
{
    _pImpl->build(ptr);
}

void
img::OutputImage::setResolution(size_t width, size_t height)
{
    _pImpl->setResolution(width, height);
}

void img::OutputImage::setMetadata(const char* name, size_t                 value) { _pImpl->setMetadata<size_t>        (name, value); }
void img::OutputImage::setMetadata(const char* name, float                  value) { _pImpl->setMetadata<float>         (name, value); }
void img::OutputImage::setMetadata(const char* name, const math::Vector2f&  value) { _pImpl->setMetadata<math::Vector2f>(name, value); }
void img::OutputImage::setMetadata(const char* name, const math::Vector4f&  value) { _pImpl->setMetadata<math::Vector4f>(name, value); }
void img::OutputImage::setMetadata(const char* name, const math::Affine3f&  value) { _pImpl->setMetadata<math::Affine3f>(name, value); }
void img::OutputImage::setMetadata(const char* name, const std::string&     value) { _pImpl->setMetadata<std::string>   (name, value); }
void img::OutputImage::setMetadata(const char* name, bool                   value) { _pImpl->setMetadata<bool>          (name, value); }

bool img::OutputImage::getMetadata(const char* name, size_t&            value)  const { return _pImpl->getMetadata<size_t>          (name, value); }
bool img::OutputImage::getMetadata(const char* name, float&             value)  const { return _pImpl->getMetadata<float>           (name, value); }
bool img::OutputImage::getMetadata(const char* name, math::Vector2f&    value)  const { return _pImpl->getMetadata<math::Vector2f>  (name, value); }
bool img::OutputImage::getMetadata(const char* name, math::Vector4f&    value)  const { return _pImpl->getMetadata<math::Vector4f>  (name, value); }
bool img::OutputImage::getMetadata(const char* name, math::Affine3f&    value)  const { return _pImpl->getMetadata<math::Affine3f>  (name, value); }
bool img::OutputImage::getMetadata(const char* name, std::string&       value)  const { return _pImpl->getMetadata<std::string>     (name, value); }
bool img::OutputImage::getMetadata(const char* name, bool&              value)  const { return _pImpl->getMetadata<bool>            (name, value); }

unsigned int
img::OutputImage::addLayer(size_t                           numChannels,
                           const char*                      name,
                           AbstractPixelMapping::Ptr const& func)
{
    if (strcmp(name, LAYER_ID) == 0 ||
        strcmp(name, LAYER_COVERAGE) == 0)
    {
        std::stringstream sstr;
        sstr << "'" << name << "' is a reserved layer name.";
        throw sys::Exception(sstr.str().c_str(), "Add Layer");
    }
    return _pImpl->addLayer(numChannels, name, func);
}
void
img::OutputImage::addIdCoverageLayers(size_t numChannels, unsigned int& layerId, unsigned int& layerCoverage)
{
    layerId         = _pImpl->addLayer(numChannels, LAYER_ID, nullptr);
    layerCoverage   = _pImpl->addLayer(numChannels, LAYER_COVERAGE, nullptr);
}

void
img::OutputImage::addLayerChannel(unsigned int layerId, size_t channelIdx, const float* data, size_t stride, bool useHalf)
{
    _pImpl->addLayerChannel(layerId, channelIdx, reinterpret_cast<const char*>(data), useHalf ? ChannelType::HALF_CHANNEL : ChannelType::FLOAT_CHANNEL, stride);
}
void
img::OutputImage::addLayerChannel(unsigned int layerId, size_t channelIdx, const unsigned int* data, size_t stride)
{
    _pImpl->addLayerChannel(layerId, channelIdx, reinterpret_cast<const char*>(data), ChannelType::UINT_CHANNEL, stride);
}
void
img::OutputImage::addRGBAChannel(size_t channelIdx, const float* data, size_t stride, bool useHalf)
{
    const unsigned int layerId = 0;
    addLayerChannel(layerId, channelIdx, data, stride, useHalf);
}

void
img::OutputImage::saveToFile(const char* filePath) const
{
    _pImpl->saveToFile(filePath);
}
