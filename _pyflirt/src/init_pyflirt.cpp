// ========================================================================= //
// Copyright 2016-2018 SUPAMONKS_STUDIO                                      //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
//                                                                           //
// ========================================================================= //
#include <smks/sys/initialize_xchg.hpp>
#include "smks/py/_pyflirt.hpp"
#include "smks/py/NodeClass.hpp"
#include "smks/py/ParmType.hpp"
#include "smks/py/ParmEvent.hpp"
#include "smks/py/SceneEvent.hpp"
#include "smks/py/ActionState.hpp"
#include "smks/py/BuiltIns.hpp"
#include "smks/py/ManipulatorFlags.hpp"

#include <iostream>

PyMODINIT_FUNC
init_pyflirt(void)
{
    smks::sys::initializeSmksXchg();    //<! initializes built-in parameters and node codes

    PyObject *module, *module_dict;

    module = Py_InitModule3("_pyflirt", smks::py::_pyflirt_methods, smks::py::_pyflirt_doc);
    if (module == nullptr)
        return;

    module_dict = PyModule_GetDict(module);

    // prepare the class attributes of the enum-like classes
    smks::py::NodeClass_Type        .tp_dict    = smks::py::NodeClass_tp_dict();
    smks::py::ParmType_Type         .tp_dict    = smks::py::ParmType_tp_dict();
    smks::py::ParmEvent_Type        .tp_dict    = smks::py::ParmEvent_tp_dict();
    smks::py::SceneEvent_Type       .tp_dict    = smks::py::SceneEvent_tp_dict();
    smks::py::BuiltIns_Type         .tp_dict    = smks::py::BuiltIns_tp_dict();
    smks::py::ActionState_Type      .tp_dict    = smks::py::ActionState_tp_dict();
    smks::py::ManipulatorFlags_Type .tp_dict    = smks::py::ManipulatorFlags_tp_dict();

    if (smks::py::NodeClass_Type        .tp_dict == nullptr || PyType_Ready(&smks::py::NodeClass_Type)          < 0 ||
        smks::py::ParmType_Type         .tp_dict == nullptr || PyType_Ready(&smks::py::ParmType_Type)           < 0 ||
        smks::py::ParmEvent_Type        .tp_dict == nullptr || PyType_Ready(&smks::py::ParmEvent_Type)          < 0 ||
        smks::py::SceneEvent_Type       .tp_dict == nullptr || PyType_Ready(&smks::py::SceneEvent_Type)         < 0 ||
        smks::py::BuiltIns_Type         .tp_dict == nullptr || PyType_Ready(&smks::py::BuiltIns_Type)           < 0 ||
        smks::py::ActionState_Type      .tp_dict == nullptr || PyType_Ready(&smks::py::ActionState_Type)        < 0 ||
        smks::py::ManipulatorFlags_Type .tp_dict == nullptr || PyType_Ready(&smks::py::ManipulatorFlags_Type)   < 0)
    {
        Py_DECREF(module);
        return;
    }

    // add python error handling
    smks::py::g_pyflirtError = PyErr_NewException("_pyflirt.Error", nullptr, nullptr);
    PyDict_SetItemString(module_dict, "Error", smks::py::g_pyflirtError);

    PyDict_SetItemString(module_dict, "NodeClass",        reinterpret_cast<PyObject*>(&smks::py::NodeClass_Type));
    PyDict_SetItemString(module_dict, "ParmType",         reinterpret_cast<PyObject*>(&smks::py::ParmType_Type));
    PyDict_SetItemString(module_dict, "ParmEvent",        reinterpret_cast<PyObject*>(&smks::py::ParmEvent_Type));
    PyDict_SetItemString(module_dict, "SceneEvent",       reinterpret_cast<PyObject*>(&smks::py::SceneEvent_Type));
    PyDict_SetItemString(module_dict, "BuiltIns",         reinterpret_cast<PyObject*>(&smks::py::BuiltIns_Type));
    PyDict_SetItemString(module_dict, "ActionState",      reinterpret_cast<PyObject*>(&smks::py::ActionState_Type));
    PyDict_SetItemString(module_dict, "ManipulatorFlags", reinterpret_cast<PyObject*>(&smks::py::ManipulatorFlags_Type));
}
