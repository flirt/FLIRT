// ========================================================================= //
// Copyright 2016-2018 SUPAMONKS_STUDIO                                      //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
//                                                                           //
// ========================================================================= //
#pragma once

#include "_pyflirt.hpp"
#include <memory>
#include "ClientSceneObserver.hpp"
#include "NodeParmObserver.hpp"
#include "ProgressCallbacks.hpp"

namespace smks { namespace py
{
    ////////////////////////////////
    // class AbstractCallableWrapper
    ////////////////////////////////
    class AbstractCallableWrapper
    {
    public:
        typedef std::shared_ptr<AbstractCallableWrapper> Ptr;

    public:
        virtual inline
        ~AbstractCallableWrapper()
        { }

        virtual inline
        ClientSceneObserver::Ptr
        getClientSceneObserver()
        {
            return nullptr;
        }

        virtual inline
        NodeParmObserver::Ptr
        getNodeParmObserver()
        {
            return nullptr;
        }

        virtual inline
        ProgressCallbacks::Ptr
        getProgressCallbacks()
        {
            return nullptr;
        }
    };

    AbstractCallableWrapper::Ptr
    createAndWrapClientSceneObserver(PyObject*);

    AbstractCallableWrapper::Ptr
    createAndWrapNodeParmObserver(PyObject*, unsigned int);

    AbstractCallableWrapper::Ptr
    createAndWrapProgressCallbacks(PyObject*, PyObject*, PyObject*);


    ///////////////////////////////////
    // class ClientSceneObserverWrapper
    ///////////////////////////////////
    class ClientSceneObserverWrapper:
        public AbstractCallableWrapper
    {
    private:
        ClientSceneObserver::Ptr _wrapped;
    public:
        static inline
        Ptr
        create(PyObject* func)
        {
            Ptr ptr(new ClientSceneObserverWrapper(func));
            return ptr;
        }
    protected:
        explicit
        ClientSceneObserverWrapper(PyObject*);
    public:
        inline
        ClientSceneObserver::Ptr
        getClientSceneObserver()
        {
            return _wrapped;
        }
    };

    ////////////////////////////////
    // class NodeParmObserverWrapper
    ////////////////////////////////
    class NodeParmObserverWrapper:
        public AbstractCallableWrapper
    {
    private:
        NodeParmObserver::Ptr _wrapped;
    public:
        static inline
        Ptr
        create(PyObject* func, unsigned int node)
        {
            Ptr ptr(new NodeParmObserverWrapper(func, node));
            return ptr;
        }
    protected:
        NodeParmObserverWrapper(PyObject*, unsigned int);
    public:
        inline
        NodeParmObserver::Ptr
        getNodeParmObserver()
        {
            return _wrapped;
        }
    };

    /////////////////////////////////
    // class ProgressCallablesWrapper
    /////////////////////////////////
    class ProgressCallablesWrapper:
        public AbstractCallableWrapper
    {
    private:
        ProgressCallbacks::Ptr _wrapped;
    public:
        static inline
        Ptr
        create(PyObject* beginFunc, PyObject* proceedFunc, PyObject* endFunc)
        {
            Ptr ptr (new ProgressCallablesWrapper(beginFunc, proceedFunc, endFunc));
            return ptr;
        }
    protected:
        ProgressCallablesWrapper(PyObject*, PyObject*, PyObject*);
    public:
        inline
        ProgressCallbacks::Ptr
        getProgressCallbacks()
        {
            return _wrapped;
        }
    };
}
}
