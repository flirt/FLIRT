// ========================================================================= //
// Copyright 2016-2018 SUPAMONKS_STUDIO                                      //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
//                                                                           //
// ========================================================================= //
#include <smks/xchg/ParmEvent.hpp>
#include <std/to_string_xchg.hpp>
#include "ParmEvent.hpp"
#include "ret_build.hpp"

using namespace smks;

// extern
const char* const
py::ParmEvent_doc =
{
    "Parameter event type enumeration. \n"
};

// extern
void
py::ParmEvent_dealloc(ParmEvent* self)
{
    self->ob_type->tp_free((PyObject*)self);
}

// extern
PyObject*
py::ParmEvent_tp_dict()
{
    PyObject* tp_dict = PyDict_New();

    insertInDict<int>(tp_dict, "UNKNOWN", xchg::ParmEvent::UNKNOWN      );
    insertInDict<int>(tp_dict, "ADDED"  , xchg::ParmEvent::PARM_ADDED   );
    insertInDict<int>(tp_dict, "CHANGED", xchg::ParmEvent::PARM_CHANGED );
    insertInDict<int>(tp_dict, "REMOVED", xchg::ParmEvent::PARM_REMOVED );

    return tp_dict;
}

// extern
PyObject*
py::ParmEvent_new(PyTypeObject *type, PyObject *args, PyObject *kwds)
{
    ParmEvent *self;

    self = reinterpret_cast<ParmEvent*>(type->tp_alloc(type, 0));
    if (self != nullptr)
    { }

    return (PyObject *)self;
}

// extern
int
py::ParmEvent_init(ParmEvent *self, PyObject *args, PyObject *kwds)
{
    return 0;
}


// extern
const char* const
py::doc::ParmEvent_str =
{
    "Return a string representing the type of the parameter event. \n\n"
    "\t :param type: type of parameter event \n"
    "\t :returns: string representing the type of the parameter event \n"
};

// extern
PyObject*
py::ParmEvent_str(PyObject *self, PyObject *args, PyObject *kwds)
{
    static char*    argnames[] = { "type", nullptr };
    int             tmp = 0;

    if (!PyArg_ParseTupleAndKeywords(args, kwds, "i", argnames, &tmp))
        return nullptr;

    xchg::ParmEvent::Type typ = static_cast<xchg::ParmEvent::Type>(tmp);
    return build<std::string>(std::to_string(typ));
}
