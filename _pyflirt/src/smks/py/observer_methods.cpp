// ========================================================================= //
// Copyright 2016-2018 SUPAMONKS_STUDIO                                      //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
//                                                                           //
// ========================================================================= //
#include "_pyflirt.hpp"
#include <iostream>
#include <sstream>
#include <smks/sys/Log.hpp>

#include "Context.hpp"
#include "ControlCenter.hpp"
#include "AbstractCallableWrapper.hpp"
#include "ClientSceneObserver.hpp"
#include "NodeParmObserver.hpp"
#include "ProgressCallbacks.hpp"
#include "ret_build.hpp"

using namespace smks;


const char* const py::doc::registerSceneObserver =
{
    "Register an observer used to handle scene events. \n\n"
    "\t :param func: callable used to handle scene events, signature: ``def func(type, info_dict)`` \n"
    "\t :returns: ID of the registered observer \n"
};

PyObject*
py::_registerSceneObserver(PyObject* self, PyObject* args, PyObject* kwds)
{
    static char*    argnames[] = { "func", nullptr };
    PyObject*       func;

    if (!PyArg_ParseTupleAndKeywords(args, kwds, "O", argnames, &func))
        return nullptr;

    assert(g_context);
    const size_t                        obsId   = g_context->callableIdGenerator++;
    AbstractCallableWrapper::Ptr const& wrap    = createAndWrapClientSceneObserver(func);

    g_context->callableWrappers.insert(
        std::pair<size_t, AbstractCallableWrapper::Ptr>(obsId, wrap));
    assert(wrap->getClientSceneObserver());
    g_context->controlCenter->registerObserver(
        wrap->getClientSceneObserver());

    return build<size_t>(obsId);
}

const char* const py::doc::registerNodeParameterObserver =
{
    "Register an observer used to handle a given node's parameter events. \n\n"
    "\t :param func: callable used to handle parameter events, signature: ``def func(type, info_dict)`` \n"
    "\t :param node: ID of the node to observe (default 0: uses the scene's root) \n"
    "\t :returns: ID of the registered observer \n"
};

PyObject*
py::_registerNodeParameterObserver(PyObject* self, PyObject* args, PyObject* kwds)
{
    static char*    argnames[] = { "func", "node", nullptr };
    PyObject*       func;
    unsigned int    nodeId = 0;

    if (!PyArg_ParseTupleAndKeywords(args, kwds, "O|I", argnames, &func, &nodeId))
        return nullptr;

    assert(g_context);
    const size_t                        obsId   = g_context->callableIdGenerator++;
    AbstractCallableWrapper::Ptr const& wrap    = createAndWrapNodeParmObserver(func, nodeId);

    g_context->callableWrappers.insert(std::pair<size_t, AbstractCallableWrapper::Ptr>(obsId, wrap));
    assert(wrap->getNodeParmObserver());
    g_context->controlCenter->registerObserver(
        wrap->getNodeParmObserver(), nodeId);

    return build<size_t>(obsId);
}


const char* const py::doc::deregisterObserver =
{
    "Deregister the given observer. \n\n"
    "\t :param obs: ID of the observer \n"
};

PyObject*
py::_deregisterObserver(PyObject* self, PyObject* args, PyObject* kwds)
{
    static char*    argnames[]  = { "obs", nullptr };
    size_t          obsId       = 0;

    if (!PyArg_ParseTupleAndKeywords(args, kwds, "K", argnames, &obsId))
        return nullptr;

    assert(g_context);

    Context::CallWrapperMap::iterator const& it = g_context->callableWrappers.find(obsId);

    if (it != g_context->callableWrappers.end())
    {
        ClientSceneObserver::Ptr const& sceneObs = it->second->getClientSceneObserver();

        if (sceneObs)
            g_context->controlCenter->deregisterObserver(sceneObs);
        else
        {
            NodeParmObserver::Ptr const& parmObs = it->second->getNodeParmObserver();

            if (parmObs)
                g_context->controlCenter->deregisterObserver(parmObs, parmObs->node());
            else
            {
                std::stringstream sstr;
                sstr << "Unrecognized observer type for callable ID = " << obsId << ".";
                FLIRT_LOG(LOG(DEBUG) << sstr.str();)
                throw sys::Exception(sstr.str().c_str(), "Observer Deregistration");
            }
        }
        g_context->callableWrappers.erase(it);
    }
    else
    {
        std::stringstream sstr;
        sstr << "Failed to find callable ID = " << obsId << ".";
        FLIRT_LOG(LOG(DEBUG) << sstr.str();)
        throw sys::Exception(sstr.str().c_str(), "Observer Deregistration");
    }

    Py_RETURN_NONE;
}

const char* const py::doc::registerProgressCallbacks =
{
    "Register callbacks used to monitor the progress of processes such as rendering. \n\n"
    "\t :param begin: callable called at the beginning of processes, "
    "signature: ``def begin(process_name, total_num_steps)`` \n"
    "\t :param proceed: callable called during the course of processes "
    "and returning whether to resume them or not, "
    "signature: ``proceed(current_step) -> bool`` \n"
    "\t :param end: callable called at the end of processes, "
    "signature: ``end(process_name)`` \n"
    "\t :returns: ID of the registered progress callbacks \n"
};

PyObject*
py::_registerProgressCallbacks(PyObject* self, PyObject* args, PyObject* kwds)
{
    static char* argnames[] = { "begin", "proceed", "end", nullptr };

    PyObject*   beginFunc   = nullptr;
    PyObject*   proceedFunc = nullptr;
    PyObject*   endFunc     = nullptr;
    if (!PyArg_ParseTupleAndKeywords(args, kwds, "OOO", argnames, &beginFunc, &proceedFunc, &endFunc))
        return nullptr;

    assert(g_context);
    const size_t                        prgId   = g_context->callableIdGenerator++;
    AbstractCallableWrapper::Ptr const& wrap    = createAndWrapProgressCallbacks(beginFunc, proceedFunc, endFunc);

    g_context->callableWrappers.insert(
        std::pair<size_t, AbstractCallableWrapper::Ptr>(prgId, wrap));
    assert(wrap->getProgressCallbacks());
    g_context->controlCenter->registerProgressCallbacks(
        wrap->getProgressCallbacks());

    return build<size_t>(prgId);
}

const char* const py::doc::deregisterProgressCallbacks =
{
    "Deregister callbacks used to monitor the progress of processes. \n\n"
    "\t :param prg: ID of the progress callbacks \n"
};

PyObject*
py::_deregisterProgressCallbacks(PyObject* self, PyObject* args, PyObject* kwds)
{
    static char*    argnames[]  = { "prg", nullptr };
    size_t          prgId       = 0;

    if (!PyArg_ParseTupleAndKeywords(args, kwds, "K", argnames, &prgId))
        return nullptr;

    assert(g_context);

    Context::CallWrapperMap::iterator const& it = g_context->callableWrappers.find(prgId);

    if (it != g_context->callableWrappers.end() &&
        it->second->getProgressCallbacks())
        g_context->controlCenter->deregisterProgressCallbacks(
            it->second->getProgressCallbacks());
    else
    {
        std::stringstream sstr;
        sstr << "Failed to find callables ID = " << prgId << ".";
        FLIRT_LOG(LOG(DEBUG) << sstr.str();)
        throw sys::Exception(sstr.str().c_str(), "Progress Callables Deregistration");
    }

    Py_RETURN_NONE;
}
