// ========================================================================= //
// Copyright 2016-2018 SUPAMONKS_STUDIO                                      //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
//                                                                           //
// ========================================================================= //
#pragma once

// declares a function with a _leading underscore in the smks::py namespace, and a docstring in the smks::py::doc namespace
#define PYFLIRT_GUARDED_FUNC_NOARGS(_FUNC_)                                                             \
    namespace smks { namespace py                                                                       \
    {                                                                                                   \
        extern PyObject* _##_FUNC_(PyObject*, PyObject*); /*private guarded implementation*/            \
                                                                                                        \
        namespace doc { extern const char* const _FUNC_; } /*declare docstring*/                        \
                                                                                                        \
        inline                                                                                          \
        PyObject*                                                                                       \
        _FUNC_(PyObject* self, PyObject* args, PyObject* kwds)                                          \
        {                                                                                               \
            PyObject* ret;                                                                              \
            try                                                                                         \
            {                                                                                           \
                if (g_context == nullptr)                                                               \
                    throw sys::Exception(                                                               \
                        "_pyflirt context not initialized.",                                            \
                        "_pyflirt '" #_FUNC_ "' Function Guard");                                       \
                return _##_FUNC_(self, args);   /*additional underscore*/                               \
            }                                                                                           \
            catch(const sys::Exception& e) { PyErr_SetString(g_pyflirtError, e.what()); return NULL; }  \
            return ret;                                                                                 \
        }                                                                                               \
    } }

// declares a function with a _leading underscore in the smks::py namespace, and a docstring in the smks::py::doc namespace
#define PYFLIRT_GUARDED_FUNC(_FUNC_)                                                                    \
    namespace smks { namespace py                                                                       \
    {                                                                                                   \
        extern PyObject* _##_FUNC_(PyObject*, PyObject*, PyObject*); /*private guarded implementation*/ \
                                                                                                        \
        namespace doc { extern const char* const _FUNC_; } /*declare docstring*/                        \
                                                                                                        \
        inline                                                                                          \
        PyObject*                                                                                       \
        _FUNC_(PyObject* self, PyObject* args, PyObject* kwds)                                          \
        {                                                                                               \
            PyObject* ret;                                                                              \
            try                                                                                         \
            {                                                                                           \
                if (g_context == nullptr)                                                               \
                    throw sys::Exception(                                                               \
                        "_pyflirt context not initialized.",                                            \
                        "_pyflirt '" #_FUNC_ "' Function Guard");                                       \
                return _##_FUNC_(self, args, kwds); /*additional underscore*/                           \
            }                                                                                           \
            catch(const sys::Exception& e) { PyErr_SetString(g_pyflirtError, e.what()); return NULL; }  \
            return ret;                                                                                 \
        }                                                                                               \
    } }

#define PYFLIRT_CREATE_ASSIGN_DECL(_SUFFIX_) \
    PYFLIRT_GUARDED_FUNC(createAssign##_SUFFIX_)

// requires the specialization of the smks::py::parseAssignArgs function
#define PYFLIRT_CREATE_ASSIGN_IMPL(_TYPE_, _SUFFIX_, _TYPENAME_)                                                     \
    const char* const py::doc::createAssign##_SUFFIX_=                                                               \
    {                                                                                                                \
        "Adds a " #_TYPENAME_ " parameter assignment node to the scene.\n"                                           \
        "- parm_name: name of the parameter.\n"                                                                      \
        "- parm_value: value of the parameter.\n"                                                                    \
        "- name: short name of the node.\n"                                                                          \
        "- parent: ID of the parent node (default = 0, adds node to the scene's root).\n"                            \
    };                                                                                                               \
                                                                                                                     \
    PyObject*                                                                                                        \
    py::_createAssign##_SUFFIX_(PyObject* self, PyObject* args, PyObject* kwds)                                      \
    {                                                                                                                \
        static char*    argnames[] = { "parm_name", "parm_value", "name", "parent", nullptr };                       \
        const char*     parmName;                                                                                    \
        _TYPE_          parmValue;                                                                                   \
        const char*     name;                                                                                        \
        unsigned int    parentId = 0;                                                                                \
                                                                                                                     \
        if (!parseAssignArgs<_TYPE_>(args, kwds, argnames, parmName, parmValue, name, parentId))                     \
            Py_RETURN_NONE;                                                                                          \
                                                                                                                     \
        assert(g_context);                                                                                           \
        const unsigned int id = g_context->controlCenter->createAssign<_TYPE_>(parmName, parmValue, name, parentId); \
                                                                                                                     \
        return build<unsigned int>(id);                                                                              \
    }

