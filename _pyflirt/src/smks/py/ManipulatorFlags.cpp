// ========================================================================= //
// Copyright 2016-2018 SUPAMONKS_STUDIO                                      //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
//                                                                           //
// ========================================================================= //
#include <smks/xchg/ManipulatorFlags.hpp>
#include <std/to_string_xchg.hpp>
#include "ManipulatorFlags.hpp"

using namespace smks;

// extern
const char* const
py::ManipulatorFlags_doc =
{
    "Manipulator mode enumeration. \n\n"
    "\t * Set of flags used to control the manipulation behavior over the "
    "scene's selection \n"
    "\t * To manipulate objects in local space, use the MANIP_OBJECT_MODE "
    "flag via bitwise union \n"
};

// extern
void
py::ManipulatorFlags_dealloc(ManipulatorFlags* self)
{
    self->ob_type->tp_free((PyObject*)self);
}

// extern
PyObject*
py::ManipulatorFlags_tp_dict()
{
    PyObject* tp_dict = PyDict_New();

    insertInDict<int>(tp_dict, "NO_MANIP"           , xchg::NO_MANIP            );
    insertInDict<int>(tp_dict, "MANIP_TRANSLATE"    , xchg::MANIP_TRANSLATE     );
    insertInDict<int>(tp_dict, "MANIP_ROTATE"       , xchg::MANIP_ROTATE        );
    insertInDict<int>(tp_dict, "MANIP_SCALE"        , xchg::MANIP_SCALE         );
    insertInDict<int>(tp_dict, "MANIP_OBJECT_MODE"  , xchg::MANIP_OBJECT_MODE   );

    return tp_dict;
}

// extern
PyObject*
py::ManipulatorFlags_new(PyTypeObject *type, PyObject *args, PyObject *kwds)
{
    ManipulatorFlags *self;

    self = reinterpret_cast<ManipulatorFlags*>(type->tp_alloc(type, 0));
    if (self != nullptr)
    { }

    return (PyObject *)self;
}

// extern
int
py::ManipulatorFlags_init(ManipulatorFlags *self, PyObject *args, PyObject *kwds)
{
    return 0;
}

// extern
const char* const
py::doc::ManipulatorFlags_str =
{
    "Return a string representing the manipulator flags. \n\n"
    "\t :param type: bitmask of manipulator flags \n"
    "\t :returns: string representing the manipulator flags \n"
};

// extern
PyObject*
py::ManipulatorFlags_str(PyObject *self, PyObject *args, PyObject *kwds)
{
    static char*    argnames[] = { "type", nullptr };
    int             tmp = 0;

    if (!PyArg_ParseTupleAndKeywords(args, kwds, "i", argnames, &tmp))
        return nullptr;

    xchg::ManipulatorFlags mode = static_cast<xchg::ManipulatorFlags>(tmp);

    return build<std::string>(std::to_string(mode));
}
