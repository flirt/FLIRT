// ========================================================================= //
// Copyright 2016-2018 SUPAMONKS_STUDIO                                      //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
//                                                                           //
// ========================================================================= //
#include <smks/xchg/ParmType.hpp>
#include <std/to_string_xchg.hpp>
#include "ParmType.hpp"
#include "ret_build.hpp"

using namespace smks;

// extern
const char* const
py::ParmType_doc =
{
    "Parameter type enumeration. \n"
};

// extern
void
py::ParmType_dealloc(ParmType* self)
{
    self->ob_type->tp_free((PyObject*)self);
}

// extern
PyObject*
py::ParmType_tp_dict()
{
    PyObject* tp_dict = PyDict_New();

    insertInDict<int>(tp_dict, "UNKNOWN"    , xchg::UNKNOWN_PARMTYPE    );
    insertInDict<int>(tp_dict, "BOOL"       , xchg::PARMTYPE_BOOL       );
    insertInDict<int>(tp_dict, "UINT"       , xchg::PARMTYPE_UINT       );
    insertInDict<int>(tp_dict, "INT"        , xchg::PARMTYPE_INT        );
    insertInDict<int>(tp_dict, "CHAR"       , xchg::PARMTYPE_CHAR       );
    insertInDict<int>(tp_dict, "SIZE"       , xchg::PARMTYPE_SIZE       );
    insertInDict<int>(tp_dict, "INT_2"      , xchg::PARMTYPE_INT_2      );
    insertInDict<int>(tp_dict, "INT_3"      , xchg::PARMTYPE_INT_3      );
    insertInDict<int>(tp_dict, "INT_4"      , xchg::PARMTYPE_INT_4      );
    insertInDict<int>(tp_dict, "FLOAT"      , xchg::PARMTYPE_FLOAT      );
    insertInDict<int>(tp_dict, "FLOAT_2"    , xchg::PARMTYPE_FLOAT_2    );
    insertInDict<int>(tp_dict, "FLOAT_4"    , xchg::PARMTYPE_FLOAT_4    );
    insertInDict<int>(tp_dict, "AFFINE_3"   , xchg::PARMTYPE_AFFINE_3   );
    insertInDict<int>(tp_dict, "STRING"     , xchg::PARMTYPE_STRING     );
    insertInDict<int>(tp_dict, "IDSET"      , xchg::PARMTYPE_IDSET      );
    insertInDict<int>(tp_dict, "DATASTREAM" , xchg::PARMTYPE_DATASTREAM );
    insertInDict<int>(tp_dict, "BOUNDINGBOX", xchg::PARMTYPE_BOUNDINGBOX);
    insertInDict<int>(tp_dict, "DATA"       , xchg::PARMTYPE_DATA       );
    insertInDict<int>(tp_dict, "OBJECT"     , xchg::PARMTYPE_OBJECT     );

    return tp_dict;
}

// extern
PyObject*
py::ParmType_new(PyTypeObject *type, PyObject *args, PyObject *kwds)
{
    ParmType *self;

    self = reinterpret_cast<ParmType*>(type->tp_alloc(type, 0));
    if (self != nullptr)
    { }

    return (PyObject *)self;
}

// extern
int
py::ParmType_init(ParmType *self, PyObject *args, PyObject *kwds)
{
    return 0;
}


// extern
const char* const
py::doc::ParmType_str =
{
    "Return a string representing the parameter type. \n\n"
    "\t :param type: parameter type \n"
    "\t :returns: string representing the parameter type \n"
};

// extern
PyObject*
py::ParmType_str(PyObject *self, PyObject *args, PyObject *kwds)
{
    static char*    argnames[] = { "type", nullptr };
    int             tmp = 0;

    if (!PyArg_ParseTupleAndKeywords(args, kwds, "i", argnames, &tmp))
        return nullptr;

    xchg::ParmType      parmType    = static_cast<xchg::ParmType>(tmp);
    const std::string&  str         = std::to_string(parmType);

    return build<std::string>(str);
}
