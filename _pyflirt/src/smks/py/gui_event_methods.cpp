// ========================================================================= //
// Copyright 2016-2018 SUPAMONKS_STUDIO                                      //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
//                                                                           //
// ========================================================================= //
#include "_pyflirt.hpp"
#include <iostream>

#include <smks/sys/Log.hpp>
#include <smks/xchg/GUIEvents.hpp>

#include "Context.hpp"
#include "ControlCenter.hpp"
#include "arg_parse.hpp"
#include "ret_build.hpp"

namespace smks { namespace py
{
    static inline
    bool
    parse(PyObject*, PyObject*, xchg::MouseEvent&);

    static inline
    bool
    parse(PyObject*, PyObject*, xchg::KeyEvent&);

    static inline
    bool
    parse(PyObject*, PyObject*, xchg::WheelEvent&);

    static inline
    bool
    parse(PyObject*, PyObject*, xchg::ResizeEvent&);
}
}

using namespace smks;

const char* const py::doc::mousePressEvent =
{
    "Handle a mouse press event. \n\n"
    "\t :param x: mouse cursor's x position at the time of the event, relative to the widget that received it \n"
    "\t :param y: mouse cursor's y position at the time of the event, relative to the widget that received it \n"
    "\t :param button: button that caused the event \n"
    "\t :param modifiers: keyboard modifier flags that existed immediately after the event occurred \n"
};

PyObject*
py::_mousePressEvent(PyObject* self, PyObject* args, PyObject* kwds)
{
    xchg::MouseEvent evt(xchg::MOUSE_PRESS_EVENT);
    if (!parse(args, kwds, evt))
        return nullptr;

    assert(g_context);
    g_context->scene->handleGUIEvent(evt);
    Py_RETURN_NONE;
}

const char* const py::doc::mouseMoveEvent =
{
    "Handle a mouse move event. \n\n"
    "\t :param x: mouse cursor's x position at the time of the event, relative to the widget that received it \n"
    "\t :param y: mouse cursor's y position at the time of the event, relative to the widget that received it \n"
    "\t :param button: button that caused the event \n"
    "\t :param modifiers: keyboard modifier flags that existed immediately after the event occurred \n"
};

PyObject*
py::_mouseMoveEvent(PyObject* self, PyObject* args, PyObject* kwds)
{
    xchg::MouseEvent evt(xchg::MOUSE_MOVE_EVENT);
    if (!parse(args, kwds, evt))
        return nullptr;

    assert(g_context);
    g_context->scene->handleGUIEvent(evt);
    Py_RETURN_NONE;
}

const char* const py::doc::mouseReleaseEvent =
{
    "Handle a mouse release event. \n\n"
    "\t :param x: mouse cursor's x position at the time of the event, relative to the widget that received it \n"
    "\t :param y: mouse cursor's y position at the time of the event, relative to the widget that received it \n"
    "\t :param button: button that caused the event \n"
    "\t :param modifiers: keyboard modifier flags that existed immediately after the event occurred \n"
};

PyObject*
py::_mouseReleaseEvent(PyObject* self, PyObject* args, PyObject* kwds)
{
    xchg::MouseEvent evt(xchg::MOUSE_RELEASE_EVENT);
    if (!parse(args, kwds, evt))
        return nullptr;

    assert(g_context);
    g_context->scene->handleGUIEvent(evt);
    Py_RETURN_NONE;
}

const char* const py::doc::mouseDoubleClickEvent =
{
    "Handle a mouse double click event. \n\n"
    "\t :param x: mouse cursor's x position at the time of the event, relative to the widget that received it \n"
    "\t :param y: mouse cursor's y position at the time of the event, relative to the widget that received it \n"
    "\t :param button: button that caused the event \n"
    "\t :param modifiers: keyboard modifier flags that existed immediately after the event occurred \n"
};

PyObject*
py::_mouseDoubleClickEvent(PyObject* self, PyObject* args, PyObject* kwds)
{
    xchg::MouseEvent evt(xchg::MOUSE_DOUBLE_CLICK_EVENT);
    if (!parse(args, kwds, evt))
        return nullptr;

    assert(g_context);
    g_context->scene->handleGUIEvent(evt);
    Py_RETURN_NONE;
}

const char* const py::doc::wheelEvent =
{
    "Handle a wheel event. \n\n"
    "\t :param x: mouse cursor's x position at the time of the event, relative to the widget that received it \n"
    "\t :param y: mouse cursor's y position at the time of the event, relative to the widget that received it \n"
    "\t :param delta: distance that the wheel is rotated, in eighths of a degree \n"
    "\t :param modifiers: keyboard modifier flags that existed immediately after the event occurred \n"
};

PyObject*
py::_wheelEvent(PyObject* self, PyObject* args, PyObject* kwds)
{
    xchg::WheelEvent evt;
    if (!parse(args, kwds, evt))
        return nullptr;

    assert(g_context);
    g_context->scene->handleGUIEvent(evt);
    Py_RETURN_NONE;
}

const char* const py::doc::keyPressEvent =
{
    "Handle a keyboard press event. \n\n"
    "\t :param key: code of the key that was pressed or released \n"
    "\t :param text: unicode text that this key generated \n"
    "\t :param modifiers: keyboard modifier flags that existed immediately after the event occurred \n"
    "\t :param auto_repeat: True if this event comes from an auto-repeating key, or False if it comes from an initial key press \n"
};

PyObject*
py::_keyPressEvent(PyObject* self, PyObject* args, PyObject* kwds)
{
    xchg::KeyEvent evt(xchg::KEY_PRESS_EVENT);
    if (!parse(args, kwds, evt))
        return nullptr;

    assert(g_context);
    g_context->scene->handleGUIEvent(evt);
    Py_RETURN_NONE;
}

const char* const py::doc::keyReleaseEvent =
{
    "Handle a keyboard release event. \n\n"
    "\t :param key: code of the key that was pressed or released \n"
    "\t :param text: unicode text that this key generated \n"
    "\t :param modifiers: keyboard modifier flags that existed immediately after the event occurred \n"
    "\t :param auto_repeat: True if this event comes from an auto-repeating key, or False if it comes from an initial key press \n"
};

PyObject*
py::_keyReleaseEvent(PyObject* self, PyObject* args, PyObject* kwds)
{
    xchg::KeyEvent evt(xchg::KEY_RELEASE_EVENT);
    if (!parse(args, kwds, evt))
        return nullptr;

    assert(g_context);
    g_context->scene->handleGUIEvent(evt);
    Py_RETURN_NONE;
}

const char* const py::doc::resizeEvent =
{
    "Handle a resize event. \n\n"
    "\t :param width: new width of the widget \n"
    "\t :param height: new height of the widget \n"
    "\t :param old_width: old width of the widget \n"
    "\t :param old_height: old height of the widget \n"
};

PyObject*
py::_resizeEvent(PyObject* self, PyObject* args, PyObject* kwds)
{
    xchg::ResizeEvent evt;
    if (!parse(args, kwds, evt))
        return nullptr;

    assert(g_context);
    g_context->scene->handleGUIEvent(evt);
    Py_RETURN_NONE;
}

const char* const py::doc::paintGL =
{
    "Handle a paint event.\n"
};

PyObject*
py::_paintGL(PyObject* self, PyObject* args)
{
    assert(g_context);
    g_context->scene->handleGUIEvent(xchg::PaintGLEvent());

    Py_RETURN_NONE;
}


// static inline
bool
py::parse(PyObject* args, PyObject* kwds, xchg::MouseEvent& evt)
{
    static char* argnames[] = { "x", "y", "button", "modifiers", nullptr };
    int x = 0;
    int y = 0;
    int button = 0;
    int modifiers = 0;

    if (!PyArg_ParseTupleAndKeywords(args, kwds, "|iiii", argnames, &x, &y, &button, &modifiers))
        return false;

    evt.x(x);
    evt.y(y);
    evt.button(static_cast<xchg::MouseButton>(button));
    evt.keyboardModifiers(modifiers);
    return true;
}

// static inline
bool
py::parse(PyObject* args, PyObject* kwds, xchg::KeyEvent& evt)
{
    static char* argnames[] = { "key", "text", "modifiers", "auto_repeat", nullptr };
    int key = 0;
    const char* text = "";
    int modifiers = 0;
    int autoRepeat = 0;

    if (!PyArg_ParseTupleAndKeywords(args, kwds, "|isii", argnames, &key, &text, &modifiers, &autoRepeat))
        return false;

    evt.key(key);
    evt.text(std::string(text));
    evt.keyboardModifiers(modifiers);
    evt.isAutoRepeat(autoRepeat != 0);
    return true;
}

// static inline
bool
py::parse(PyObject* args, PyObject* kwds, xchg::WheelEvent& evt)
{
    static char* argnames[] = { "x", "y", "delta", "modifiers", nullptr };
    int x = 0;
    int y = 0;
    int delta = 0;
    int modifiers = 0;

    if (!PyArg_ParseTupleAndKeywords(args, kwds, "|iiii", argnames, &x, &y, &delta, &modifiers))
        return false;

    evt.x(x);
    evt.y(y);
    evt.delta(delta);
    evt.keyboardModifiers(modifiers);
    return true;
}

// static inline
bool
py::parse(PyObject* args, PyObject* kwds, xchg::ResizeEvent& evt)
{
    static char* argnames[] = { "width", "height", "old_width", "old_height", nullptr };
    int width = 0;
    int height = 0;
    int oldWidth = 0;
    int oldHeight = 0;

    if (!PyArg_ParseTupleAndKeywords(args, kwds, "|iiii", argnames, &width, &height, &oldWidth, &oldHeight))
        return false;

    evt.width(width);
    evt.height(height);
    evt.oldWidth(oldWidth);
    evt.oldHeight(oldHeight);
    return true;
}
