// ========================================================================= //
// Copyright 2016-2018 SUPAMONKS_STUDIO                                      //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
//                                                                           //
// ========================================================================= //
#include <smks/xchg/ParmEvent.hpp>
#include "NodeParmObserver.hpp"

using namespace smks;

// static
py::NodeParmObserver::Ptr
py::NodeParmObserver::create(PyObject* func, unsigned int node)
{
    Ptr ptr(new NodeParmObserver(func, node));
    return ptr;
}

py::NodeParmObserver::NodeParmObserver(PyObject* func, unsigned int node):
    _func(nullptr),
    _node(node)
{
    if (func == nullptr ||
        !PyCallable_Check(func))
        throw sys::Exception(
            "Specified object is not a valid callable.",
            "Python Node Parameter Observer Constructor");

    Py_XINCREF(func);
    _func = func;
}

py::NodeParmObserver::~NodeParmObserver()
{
    Py_XDECREF(_func);
}

void
py::NodeParmObserver::handle(ClientBase&            client,
                             unsigned int           node,
                             const xchg::ParmEvent& evt)
{
    PyObject* args  = nullptr;
    PyObject* kwds  = nullptr;

    buildArgumentsAndKeywords(evt, args, kwds);

    PyObject* ret   = PyEval_CallObjectWithKeywords(_func, args, kwds);

    Py_XDECREF(args);
    Py_XDECREF(kwds);
    if (ret == nullptr)
        return;
    Py_DECREF(ret);
}

void
py::NodeParmObserver::buildArgumentsAndKeywords(
    const xchg::ParmEvent&  evt,
    PyObject*               &args,
    PyObject*               &kwds) const
{
    PyObject* info_dict = PyDict_New();

    insertInDict<unsigned int>  (info_dict, "parm",                 evt.parm());
    insertInDict<unsigned int>  (info_dict, "node",                 evt.node());
    insertInDict<bool>          (info_dict, "emitted_by_node",      evt.emittedByNode());
    insertInDict<bool>          (info_dict, "comes_from_link",      evt.comesFromLink());
    insertInDict<bool>          (info_dict, "comes_from_ancestor",  evt.comesFromAncestor());

    args = Py_BuildValue("(iO)", static_cast<int>(evt.type()), info_dict);
    Py_DECREF(info_dict);
}
