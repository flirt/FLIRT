// ========================================================================= //
// Copyright 2016-2018 SUPAMONKS_STUDIO                                      //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
//                                                                           //
// ========================================================================= //
#include <smks/xchg/SceneEvent.hpp>
#include "ClientSceneObserver.hpp"

using namespace smks;

// static
py::ClientSceneObserver::Ptr
py::ClientSceneObserver::create(PyObject* func)
{
    Ptr ptr(new ClientSceneObserver(func));
    return ptr;
}

// explicit
py::ClientSceneObserver::ClientSceneObserver(PyObject* func):
    _func(nullptr)
{
    if (func == nullptr ||
        !PyCallable_Check(func))
        throw sys::Exception(
            "Specified object is not a valid callable.",
            "Python Scene Client Observer Constructor");

    Py_XINCREF(func);
    _func = func;
}

py::ClientSceneObserver::~ClientSceneObserver()
{
    Py_XDECREF(_func);
}

void
py::ClientSceneObserver::handle(ClientBase&             client,
                                const xchg::SceneEvent& evt)
{
    PyObject* args  = nullptr;
    PyObject* kwds  = nullptr;

    buildArgumentsAndKeywords(evt, args, kwds);

    PyObject* ret   = PyEval_CallObjectWithKeywords(_func, args, kwds);

    Py_XDECREF(args);
    Py_XDECREF(kwds);
    if (ret == nullptr)
        return;
    Py_DECREF(ret);
}

void
py::ClientSceneObserver::buildArgumentsAndKeywords(
    const xchg::SceneEvent& evt,
    PyObject*               &args,
    PyObject*               &kwds) const
{
    PyObject* info_dict = PyDict_New();

    switch (evt.type())
    {
    default:
        break;
    case xchg::SceneEvent::NODE_CREATED:
    case xchg::SceneEvent::NODE_DELETED:
        insertInDict<unsigned int>(info_dict, "node",           evt.node());
        insertInDict<unsigned int>(info_dict, "parent",         evt.parent());
        break;
    case xchg::SceneEvent::PARM_CONNECTED:
    case xchg::SceneEvent::PARM_DISCONNECTED:
        insertInDict<unsigned int>(info_dict, "cnx",            evt.connection());
        insertInDict<unsigned int>(info_dict, "master",         evt.master());
        insertInDict<unsigned int>(info_dict, "master_parm",    evt.masterParm());
        insertInDict<unsigned int>(info_dict, "slave",          evt.slave());
        insertInDict<unsigned int>(info_dict, "slave_parm",     evt.slaveParm());
        break;
    case xchg::SceneEvent::NODES_LINKED:
    case xchg::SceneEvent::NODES_UNLINKED:
        insertInDict<unsigned int>(info_dict, "target",         evt.target());
        insertInDict<unsigned int>(info_dict, "source",         evt.source());
        break;
    case xchg::SceneEvent::LINK_ATTACHED:
    case xchg::SceneEvent::LINK_DETACHED:
        insertInDict<unsigned int>(info_dict, "target",         evt.target());
        insertInDict<unsigned int>(info_dict, "source",         evt.source());
        insertInDict<unsigned int>(info_dict, "parm",           evt.parm());
        break;
    }

    args = Py_BuildValue("(iO)", static_cast<int>(evt.type()), info_dict);
    Py_DECREF(info_dict);
}
