// ========================================================================= //
// Copyright 2016-2018 SUPAMONKS_STUDIO                                      //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
//                                                                           //
// ========================================================================= //
#include <smks/xchg/NodeClass.hpp>
#include <std/to_string_xchg.hpp>
#include "NodeClass.hpp"

using namespace smks;

// extern
const char* const
py::NodeClass_doc =
{
    "Node class enumeration. \n\n"
    "* Can be combined via bitwise operations. \n"
};

// extern
void
py::NodeClass_dealloc(NodeClass* self)
{
    self->ob_type->tp_free((PyObject*)self);
}

// extern
PyObject*
py::NodeClass_tp_dict()
{
    PyObject* tp_dict = PyDict_New();

    insertInDict<int>(tp_dict, "NONE"            , xchg::NONE           );
    insertInDict<int>(tp_dict, "ANY"             , xchg::ANY            );
    insertInDict<int>(tp_dict, "SCENE"           , xchg::SCENE          );
    insertInDict<int>(tp_dict, "ARCHIVE"         , xchg::ARCHIVE        );
    insertInDict<int>(tp_dict, "XFORM"           , xchg::XFORM          );
    insertInDict<int>(tp_dict, "CAMERA"          , xchg::CAMERA         );
    insertInDict<int>(tp_dict, "POINTS"          , xchg::POINTS         );
    insertInDict<int>(tp_dict, "CURVES"          , xchg::CURVES         );
    insertInDict<int>(tp_dict, "MESH"            , xchg::MESH           );
    insertInDict<int>(tp_dict, "LIGHT"           , xchg::LIGHT          );
    insertInDict<int>(tp_dict, "ASSIGN"          , xchg::ASSIGN         );
    insertInDict<int>(tp_dict, "SELECTOR"        , xchg::SELECTOR       );
    insertInDict<int>(tp_dict, "RENDER_ACTION"   , xchg::RENDER_ACTION  );
    insertInDict<int>(tp_dict, "MATERIAL"        , xchg::MATERIAL       );
    insertInDict<int>(tp_dict, "SUBSURFACE"      , xchg::SUBSURFACE     );
    insertInDict<int>(tp_dict, "TEXTURE"         , xchg::TEXTURE        );
    insertInDict<int>(tp_dict, "RENDERER"        , xchg::RENDERER       );
    insertInDict<int>(tp_dict, "TONEMAPPER"      , xchg::TONEMAPPER     );
    insertInDict<int>(tp_dict, "DENOISER"        , xchg::DENOISER       );
    insertInDict<int>(tp_dict, "FRAMEBUFFER"     , xchg::FRAMEBUFFER    );
    insertInDict<int>(tp_dict, "RENDERPASS"      , xchg::RENDERPASS     );
    insertInDict<int>(tp_dict, "INTEGRATOR"      , xchg::INTEGRATOR     );
    insertInDict<int>(tp_dict, "SAMPLER_FACTORY" , xchg::SAMPLER_FACTORY);
    insertInDict<int>(tp_dict, "PIXEL_FILTER"    , xchg::PIXEL_FILTER   );
    insertInDict<int>(tp_dict, "DRAWABLE"        , xchg::DRAWABLE       );
    insertInDict<int>(tp_dict, "SCENE_OBJECT"    , xchg::SCENE_OBJECT   );
    insertInDict<int>(tp_dict, "GRAPH_ACTION"    , xchg::GRAPH_ACTION   );
    insertInDict<int>(tp_dict, "SHADER"          , xchg::SHADER         );
    insertInDict<int>(tp_dict, "RTNODE"          , xchg::RTNODE         );

    return tp_dict;
}

// extern
PyObject*
py::NodeClass_new(PyTypeObject *type, PyObject *args, PyObject *kwds)
{
    NodeClass *self;

    self = reinterpret_cast<NodeClass*>(type->tp_alloc(type, 0));
    if (self != nullptr)
    { }

    return (PyObject *)self;
}

// extern
int
py::NodeClass_init(NodeClass *self, PyObject *args, PyObject *kwds)
{
    return 0;
}

// extern
const char* const
py::doc::NodeClass_ls =
{
    "List all node classes. \n\n"
    "\t :returns: list of integers associated with different node classes \n"
};

// extern
PyObject*
py::NodeClass_ls()
{
    PyObject* tp_dict = py::NodeClass_tp_dict();
    if (!tp_dict)
        Py_RETURN_NONE;
    return PyDict_Values(tp_dict);

    //Py_ssize_t    size    = PyDict_Size(tp_dict);
    //PyObject* ret     = PyTuple_New(size);
    //PyObject* vals    = PyDict_Values(tp_dict);
    //
    //Py_DECREF(tp_dict);
    //Py_DECREF(vals);
    //return ret;
}

// extern
const char* const
py::doc::NodeClass_str =
{
    "Return a string representing the node class. \n\n"
    "\t :param type: node class \n"
    "\t :returns: string representing the node class \n"
};

// extern
PyObject*
py::NodeClass_str(PyObject *self, PyObject *args, PyObject *kwds)
{
    static char*    argnames[] = { "type", nullptr };
    int             tmp = 0;

    if (!PyArg_ParseTupleAndKeywords(args, kwds, "i", argnames, &tmp))
        return nullptr;

    xchg::NodeClass cl = static_cast<xchg::NodeClass>(tmp);

    return build<std::string>(std::to_string(cl));
}
