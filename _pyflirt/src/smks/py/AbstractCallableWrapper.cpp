// ========================================================================= //
// Copyright 2016-2018 SUPAMONKS_STUDIO                                      //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
//                                                                           //
// ========================================================================= //
#include "AbstractCallableWrapper.hpp"
#include "NodeParmObserver.hpp"
#include "ClientSceneObserver.hpp"
#include "ProgressCallbacks.hpp"

using namespace smks;

py::AbstractCallableWrapper::Ptr
py::createAndWrapClientSceneObserver(PyObject* func)
{
    return ClientSceneObserverWrapper::create(func);
}

py::AbstractCallableWrapper::Ptr
py::createAndWrapNodeParmObserver(PyObject*     func,
                                  unsigned int  node)
{
    return NodeParmObserverWrapper::create(func, node);
}

py::AbstractCallableWrapper::Ptr
py::createAndWrapProgressCallbacks(PyObject* beginFunc,
                                   PyObject* proceedFunc,
                                   PyObject* endFunc)
{
    return ProgressCallablesWrapper::create(beginFunc, proceedFunc, endFunc);
}

// explicit
py::ClientSceneObserverWrapper::ClientSceneObserverWrapper(PyObject* func):
    _wrapped(ClientSceneObserver::create(func))
{ }

py::NodeParmObserverWrapper::NodeParmObserverWrapper(PyObject*      func,
                                                     unsigned int   node):
    _wrapped(NodeParmObserver::create(func, node))
{ }

py::ProgressCallablesWrapper::ProgressCallablesWrapper(PyObject*    beginFunc,
                                                       PyObject*    proceedFunc,
                                                       PyObject*    endFunc):
    _wrapped(ProgressCallbacks::create(beginFunc, proceedFunc, endFunc))
{ }
