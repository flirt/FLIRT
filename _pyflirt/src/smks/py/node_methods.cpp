// ========================================================================= //
// Copyright 2016-2018 SUPAMONKS_STUDIO                                      //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
//                                                                           //
// ========================================================================= //
#include "_pyflirt.hpp"
#include <iostream>

#include <smks/sys/Log.hpp>
#include <smks/AbstractClientNodeFilter.hpp>

#include "Context.hpp"
#include "ControlCenter.hpp"
#include "arg_parse.hpp"
#include "ret_build.hpp"

namespace smks { namespace py
{
    ////////////////////////////////
    // class ObserverBase<EventType>
    ////////////////////////////////
    class FilterWrap:
        public AbstractClientNodeFilter
    {
    public:
        typedef std::shared_ptr<FilterWrap> Ptr;

    private:
        PyObject* _filter;

    public:
        static inline
        Ptr
        create(PyObject* func)
        {
            Ptr ptr(new FilterWrap(func));
            return ptr;
        }

    protected:
        explicit
        FilterWrap(PyObject* func):
            _filter(nullptr)
        {
            if (func == nullptr ||
                !PyCallable_Check(func))
                throw sys::Exception(
                    "Specified object is not a valid callable.",
                    "Python Filter Creation");
            Py_XINCREF(func);
            _filter = func;
        }
    private:
        // non-copyable
        FilterWrap(const FilterWrap&);
        FilterWrap& operator=(const FilterWrap&);

    public:
        inline
        ~FilterWrap()
        {
            Py_XDECREF(_filter);
        }

        inline
        bool
        operator()(unsigned int node,
                   const ClientBase&) const
        {
            bool status = true;

            assert(_filter);
            PyObject* args  = Py_BuildValue("(I)", node);
            PyObject* kwds  = nullptr;

            PyObject* ret   = PyEval_CallObjectWithKeywords(_filter, args, kwds);
            Py_XDECREF(args);
            Py_XDECREF(kwds);

            bool ret_ok = parse<bool>(ret, status, false);
            Py_XDECREF(ret);

            if (!ret_ok)
                throw sys::Exception(
                    "Filter function either failed or did not return a boolean.",
                    "Node Filter Python Wrap");

            return status;
        }
    };
}
}

using namespace smks;


const char* const py::doc::getNodeName =
{
    "Return the name of a given node. \n\n"
    "\t :param node: ID of the node \n"
    "\t :returns: short name of the node, or None upon failure \n"
};

PyObject*
py::_getNodeName(PyObject* self, PyObject* args, PyObject* kwds)
{
    static char*    argnames[] = { "node", nullptr };
    unsigned int    id = 0;

    if (!PyArg_ParseTupleAndKeywords(args, kwds, "I", argnames, &id))
        return nullptr;

    assert(g_context);
    const char* str = g_context->controlCenter->getNodeName(id);

    if (str == nullptr ||
        strlen(str) == 0)
        Py_RETURN_NONE;

    return build<std::string>(str);
}


const char* const py::doc::getNodeFullName =
{
    "Return the full path of a given node in the scene. \n\n"
    "\t :param node: ID of the node \n"
    "\t :returns: full name of the node, or None upon failure \n"
};

PyObject*
py::_getNodeFullName(PyObject* self, PyObject* args, PyObject* kwds)
{
    static char*    argnames[] = { "node", nullptr };
    unsigned int    id = 0;

    if (!PyArg_ParseTupleAndKeywords(args, kwds, "I", argnames, &id))
        return nullptr;

    assert(g_context);
    const char* str = g_context->controlCenter->getNodeFullName(id);

    if (str == nullptr ||
        strlen(str) == 0)
        Py_RETURN_NONE;

    return build<std::string>(str);
}


const char* const py::doc::getNodeParent =
{
    "Return the parent of a given node in the scene. \n\n"
    "\t :param node: ID of the node \n"
    "\t :returns: ID of the node's parent, or 0 upon failure \n"
};

PyObject*
py::_getNodeParent(PyObject* self, PyObject* args, PyObject* kwds)
{
    static char*    argnames[] = { "node", nullptr };
    unsigned int    id = 0;

    if (!PyArg_ParseTupleAndKeywords(args, kwds, "I", argnames, &id))
        return nullptr;

    assert(g_context);
    const unsigned int parentId = g_context->controlCenter->getNodeParent(id);

    return build<unsigned int>(parentId);
}


const char* const py::doc::getNodeNumChildren =
{
    "Return the child count of a given node in the scene. \n\n"
    "\t :param node: ID of the node \n"
    "\t :returns: number of node's children \n"
};

PyObject*
py::_getNodeNumChildren(PyObject* self, PyObject* args, PyObject* kwds)
{
    static char*    argnames[] = { "node", nullptr };
    unsigned int    id = 0;

    if (!PyArg_ParseTupleAndKeywords(args, kwds, "I", argnames, &id))
        return nullptr;

    assert(g_context);
    const size_t num = g_context->controlCenter->getNodeNumChildren(id);

    return build<size_t>(num);
}


const char* const py::doc::getNodeChild =
{
    "Return the ID of a specific child from a given node in the scene. \n\n"
    "\t :param node: ID of the node \n"
    "\t :param idx: index of the child \n"
    "\t :returns: ID of the node's chosen child, or 0 upon failure \n"
};

PyObject*
py::_getNodeChild(PyObject* self, PyObject* args, PyObject* kwds)
{
    static char*    argnames[] = { "node", "idx", nullptr };
    unsigned int    id  = 0;
    size_t          idx = 0;

    if (!PyArg_ParseTupleAndKeywords(args, kwds, "In", argnames, &id, &idx))
        return nullptr;

    assert(g_context);
    const unsigned int childId = g_context->controlCenter->getNodeChild(id, idx);

    return build<unsigned int>(childId);
}


const char* const py::doc::getNodeClass =
{
    "Return the class of a given node in the scene. \n\n"
    "\t :param node: ID of the node \n"
    "\t :returns: class of the node \n"
};

PyObject*
py::_getNodeClass(PyObject* self, PyObject* args, PyObject* kwds)
{
    static char*    argnames[] = { "node", nullptr };
    unsigned int    id  = 0;

    if (!PyArg_ParseTupleAndKeywords(args, kwds, "I", argnames, &id))
        return nullptr;

    assert(g_context);
    const xchg::NodeClass cl = g_context->controlCenter->getNodeClass(id);

    return build<int>(static_cast<int>(cl));
}


const char* const py::doc::getNodeIds=
{
    "Return the IDs of a set of nodes matching the given criteria. \n\n"
    "\t :param func: callable used to filter nodes, signature: ``def func(node) -> bool`` (default None) \n"
    "\t :param regexes: list of regular expressions used to filter nodes by their fullname (default []) \n"
    "\t :param start_node: ID of the node where the scene traversal starts (default 0: uses the scene's root) \n"
    "\t :param upwards: True for an ascending traversal (default False) \n"
    "\t :returns: list of the matched nodes' IDs \n"
};

PyObject*
py::_getNodeIds(PyObject* self, PyObject* args, PyObject* kwds)
{
    static char*    argnames[]  = { "func", "regexes", "start_node", "upwards", nullptr };
    PyObject*       func        = nullptr;
    PyObject*       regexList   = nullptr;
    unsigned int    startNodeId = 0;
    int             upwards     = 0;

    if (!PyArg_ParseTupleAndKeywords(args, kwds, "|OOIi", argnames, &func, &regexList, &startNodeId, &upwards))
        return nullptr;

    AbstractClientNodeFilter::Ptr filter = nullptr;

    if (func &&
        func != Py_None)
        filter = FilterWrap::create(func);

    std::vector<std::string>    regexes;
    std::vector<const char*>    regexPtrs;

    assert(g_context);
    parse<std::vector<std::string>>(regexList, regexes, std::vector<std::string>());
    regexPtrs.reserve(regexes.size());
    for (size_t i = 0; i < regexes.size(); ++i)
        if (!regexes[i].empty())
            regexPtrs.push_back(regexes[i].c_str());

    xchg::IdSet ids;

    g_context->controlCenter->getIds(
        ids,
        false,
        filter,
        regexPtrs.size(),
        !regexPtrs.empty() ? &regexPtrs.front() : nullptr,
        startNodeId,
        upwards != 0);

    return build<xchg::IdSet>(ids);
}


const char* const py::doc::getNodeLinkSources =
{
    "Return the nodes linked to a given node. \n\n"
    "\t :param node: ID of the node \n"
    "\t :returns: list of the IDs of the nodes linked to the given node \n"
};

PyObject*
py::_getNodeLinkSources(PyObject* self, PyObject* args, PyObject* kwds)
{
    static char*    argnames[] = { "node", nullptr };
    unsigned int    id  = 0;

    if (!PyArg_ParseTupleAndKeywords(args, kwds, "I", argnames, &id))
        return nullptr;

    xchg::IdSet sources;

    assert(g_context);
    g_context->controlCenter->getNodeLinkSources(id, sources);

    return build<xchg::IdSet>(sources);
}


const char* const py::doc::getNodeLinkTargets=
{
    "Return the nodes that a given node is linked to. \n\n"
    "\t :param node: ID of the node \n"
    "\t :returns: list of the IDs of the nodes that the given node is linked to \n"
};

PyObject*
py::_getNodeLinkTargets(PyObject* self, PyObject* args, PyObject* kwds)
{
    static char*    argnames[] = { "node", nullptr };
    unsigned int    id  = 0;

    if (!PyArg_ParseTupleAndKeywords(args, kwds, "I", argnames, &id))
        return nullptr;

    xchg::IdSet targets;

    assert(g_context);
    g_context->controlCenter->getNodeLinkTargets(id, targets);

    return build<xchg::IdSet>(targets);
}


const char* const py::doc::getLinksBetweenNodes =
{
    "Return the parameters linking two given nodes. \n\n"
    "\t :param source: ID of the node the link starts from \n"
    "\t :param target: ID of the node the link ends to \n"
    "\t :returns: list of parameter IDs \n"
};

PyObject*
py::_getLinksBetweenNodes(PyObject* self, PyObject* args, PyObject* kwds)
{
    static char*    argnames[] = { "target", "source", nullptr };
    unsigned int    target  = 0;
    unsigned int    source  = 0;

    if (!PyArg_ParseTupleAndKeywords(args, kwds, "II", argnames, &target, &source))
        return nullptr;

    xchg::IdSet links;

    assert(g_context);
    g_context->controlCenter->getLinksBetweenNodes(target, source, links);

    return build<xchg::IdSet>(links);
}
