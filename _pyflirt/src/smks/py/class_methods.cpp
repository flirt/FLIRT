// ========================================================================= //
// Copyright 2016-2018 SUPAMONKS_STUDIO                                      //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
//                                                                           //
// ========================================================================= //
#include "_pyflirt.hpp"
#include <iostream>

#include <smks/sys/Log.hpp>
#include <smks/math/math.hpp>
#include <smks/xchg/NodeCode.hpp>
#include <smks/xchg/NodeCodeMap.hpp>
#include <smks/xchg/NodeCodes.hpp>
#include "ret_build.hpp"

using namespace smks;

const char* const py::doc::getClassCodes =
{
    "List all the type codes supported by a given set of node classes. \n\n"
    "\t :param cl: bitmask of node classes \n"
    "\t :returns: tuple of all supported type codes \n"
};

PyObject*
py::getClassCodes(PyObject* self, PyObject* args, PyObject* kwds)
{
    try
    {
        static char*    argnames[] = { "cl", nullptr };
        int             tmp = 0;

        if (!PyArg_ParseTupleAndKeywords(args, kwds, "i", argnames, &tmp))
            return nullptr;

        xchg::NodeClass cl = static_cast<xchg::NodeClass>(tmp);
        if (!math::isp2(cl))
        {
            throw sys::Exception(
                "Compound node class not supported.",
                "Node Class Code Getter");
            std::cout << "Compound node class not supported." << std::endl;
            return nullptr;
        }

        std::vector<std::string>    codes;
        xchg::NodeCode::Ptr         code = xchg::nodeCodes().begin();

        codes.reserve(64);
        while (code)
        {
            if ((code->nodeClasses() & cl) != 0 &&
                code->pythonExposed())
                codes.push_back(code->c_str());
            code = xchg::nodeCodes().next(code);
        }

        if (codes.empty())
            Py_RETURN_NONE;

        PyObject* ret = PyTuple_New(codes.size());
        if (!ret)
            return nullptr;

        for (size_t i = 0; i < codes.size(); ++i)
            PyTuple_SetItem(ret, i, build<std::string>(codes[i]));

        return ret;
    }
    catch(const sys::Exception& e)
    {
        PyErr_SetString(g_pyflirtError, e.what());
        return NULL;
    }
}
