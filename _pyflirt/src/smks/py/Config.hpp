// ========================================================================= //
// Copyright 2016-2018 SUPAMONKS_STUDIO                                      //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
//                                                                           //
// ========================================================================= //
#pragma once

#include <string>

namespace smks { namespace py
{
    class Config
    {
    private:
        std::string  _logFile;
        unsigned int _numThreads;

        bool _withRendering;
        bool _withViewport;

        unsigned int _screenWidth;
        unsigned int _screenHeight;

        bool _viewportAnimation;
        bool _viewportAutoPlay;

        bool _viewportOrbitCamera;
        bool _viewportCameraFocus;
        bool _viewportCameraSwitch;

        bool _viewportLightFocus;

        bool _viewportFBufferSwitch;

        bool _viewportPicking;
        bool _viewportManipulator;

        bool _viewportHUD;

        bool _viewportSnapshot;
        bool _viewportObjLoader;
        bool _viewportDebug;

    public:
        explicit
        Config(const char*);

        inline const std::string& logFile()    const { return _logFile; }
        inline unsigned int       numThreads() const { return _screenHeight; }

        inline bool withRendering() const { return _withRendering; }
        inline bool withViewport()  const { return _withViewport; }

        inline unsigned int screenWidth () const { return _screenWidth; }
        inline unsigned int screenHeight() const { return _screenHeight; }

        inline bool viewportAnimation() const { return _viewportAnimation; }
        inline bool viewportAutoPlay()  const { return _viewportAutoPlay; }

        inline bool viewportOrbitCamera()   const { return _viewportOrbitCamera; }
        inline bool viewportCameraFocus()   const { return _viewportCameraFocus; }
        inline bool viewportCameraSwitch()  const { return _viewportCameraSwitch; }

        inline bool viewportLightFocus() const { return _viewportLightFocus; }

        inline bool viewportFBufferSwitch() const { return _viewportFBufferSwitch; }

        inline bool viewportPicking()       const { return _viewportPicking; }
        inline bool viewportManipulator()   const { return _viewportManipulator; }

        inline bool viewportHUD() const { return _viewportHUD; }

        inline bool viewportSnapshot()  const { return _viewportSnapshot; }
        inline bool viewportObjLoader() const { return _viewportObjLoader; }
        inline bool viewportDebug()     const { return _viewportDebug; }

    private:
        void
        setDefaults();
    };
}
}
