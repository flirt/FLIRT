// ========================================================================= //
// Copyright 2016-2018 SUPAMONKS_STUDIO                                      //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
//                                                                           //
// ========================================================================= //
#pragma once

#include "common.hpp"
#include <smks/math/types.hpp>
#include <vector>

namespace smks { namespace py
{
    template<typename ValueType> inline
    bool
    is(PyObject*)
    {
        throw sys::Exception(
            "Unsupported type.",
            "Python Object Compatibility Check");
    }

    template<> inline
    bool
    is<bool>(PyObject* obj)
    {
        return obj &&
            PyBool_Check(obj);
    }

    template<> inline
    bool
    is<int>(PyObject* obj)
    {
        return obj &&
            PyInt_Check(obj);
    }

    template<> inline
    bool
    is<float>(PyObject* obj)
    {
        return obj &&
            PyNumber_Check(obj);
    }

    template<> inline
    bool
    is<std::string>(PyObject* obj)
    {
        return obj &&
            PyString_Check(obj);
    }

    template<> inline
    bool
    is<math::Vector2i>(PyObject* obj)
    {
        if (!obj ||
            !PyList_Check(obj) ||
            PyList_Size(obj) < 2)
            return false;

        for (size_t i = 0; i < 2; ++i)
            if (!is<int>(PyList_GetItem(obj, i)))
                return false;

        return true;
    }

    template<> inline
    bool
    is<math::Vector3i>(PyObject* obj)
    {
        if (!obj ||
            !PyList_Check(obj) ||
            PyList_Size(obj) < 3)
            return false;

        for (size_t i = 0; i < 3; ++i)
            if (!is<int>(PyList_GetItem(obj, i)))
                return false;

        return true;
    }

    template<> inline
    bool
    is<math::Vector4i>(PyObject* obj)
    {
        if (!obj ||
            !PyList_Check(obj) ||
            PyList_Size(obj) < 4)
            return false;

        for (size_t i = 0; i < 4; ++i)
            if (!is<int>(PyList_GetItem(obj, i)))
                return false;

        return true;
    }

    template<> inline
    bool
    is<math::Vector2f>(PyObject* obj)
    {
        if (!obj ||
            !PyList_Check(obj) ||
            PyList_Size(obj) < 2)
            return false;

        for (size_t i = 0; i < 2; ++i)
            if (!is<float>(PyList_GetItem(obj, i)))
                return false;

        return true;
    }

    template<> inline
    bool
    is<math::Vector4f>(PyObject* obj)
    {
        if (!obj ||
            !PyList_Check(obj) ||
            PyList_Size(obj) < 4)
            return false;

        for (size_t i = 0; i < 4; ++i)
            if (!is<float>(PyList_GetItem(obj, i)))
                return false;

        return true;
    }

    template<> inline
    bool
    is<math::Affine3f>(PyObject* obj)
    {
        if (!obj ||
            !PyList_Check(obj) ||
            PyList_Size(obj) < 16)
            return false;

        for (size_t i = 0; i < 16; ++i)
            if (!is<float>(PyList_GetItem(obj, i)))
                return false;

        return true;
    }
}
}
