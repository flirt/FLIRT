// ========================================================================= //
// Copyright 2016-2018 SUPAMONKS_STUDIO                                      //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
//                                                                           //
// ========================================================================= //
#include <smks/xchg/SceneEvent.hpp>
#include <std/to_string_xchg.hpp>
#include "SceneEvent.hpp"
#include "ret_build.hpp"

using namespace smks;

// extern
const char* const
py::SceneEvent_doc =
{
    "Scene event type enumeration. \n"
};

// extern
void
py::SceneEvent_dealloc(SceneEvent* self)
{
    self->ob_type->tp_free((PyObject*)self);
}

// extern
PyObject*
py::SceneEvent_tp_dict()
{
    PyObject* tp_dict = PyDict_New();

    insertInDict<int>(tp_dict, "UNKNOWN"            , xchg::SceneEvent::UNKNOWN             );
    insertInDict<int>(tp_dict, "NODE_CREATED"       , xchg::SceneEvent::NODE_CREATED        );
    insertInDict<int>(tp_dict, "NODE_DELETED"       , xchg::SceneEvent::NODE_DELETED        );
    insertInDict<int>(tp_dict, "PARM_CONNECTED"     , xchg::SceneEvent::PARM_CONNECTED      );
    insertInDict<int>(tp_dict, "PARM_DISCONNECTED"  , xchg::SceneEvent::PARM_DISCONNECTED   );
    insertInDict<int>(tp_dict, "NODES_LINKED"       , xchg::SceneEvent::NODES_LINKED        );
    insertInDict<int>(tp_dict, "NODES_UNLINKED"     , xchg::SceneEvent::NODES_UNLINKED      );
    insertInDict<int>(tp_dict, "LINK_ATTACHED"      , xchg::SceneEvent::LINK_ATTACHED       );
    insertInDict<int>(tp_dict, "LINK_DETACHED"      , xchg::SceneEvent::LINK_DETACHED       );

    return tp_dict;
}

// extern
PyObject*
py::SceneEvent_new(PyTypeObject *type, PyObject *args, PyObject *kwds)
{
    SceneEvent *self;

    self = reinterpret_cast<SceneEvent*>(type->tp_alloc(type, 0));
    if (self != nullptr)
    { }

    return (PyObject *)self;
}

// extern
int
py::SceneEvent_init(SceneEvent *self, PyObject *args, PyObject *kwds)
{
    return 0;
}


// extern
const char* const
py::doc::SceneEvent_str =
{
    "Return a string representing the type of the scene event. \n\n"
    "\t :param type: type of scene event \n"
    "\t :returns: string representing the type of the scene event \n"
};

// extern
PyObject*
py::SceneEvent_str(PyObject *self, PyObject *args, PyObject *kwds)
{
    static char*    argnames[] = { "type", nullptr };
    int             tmp = 0;

    if (!PyArg_ParseTupleAndKeywords(args, kwds, "i", argnames, &tmp))
        return nullptr;

    xchg::SceneEvent::Type typ = static_cast<xchg::SceneEvent::Type>(tmp);
    return build<std::string>(std::to_string(typ));
}
