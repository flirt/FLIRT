// ========================================================================= //
// Copyright 2016-2018 SUPAMONKS_STUDIO                                      //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
//                                                                           //
// ========================================================================= //
#include <sstream>
#include <json/json.h>

#include <smks/sys/Exception.hpp>
#include <smks/sys/Log.hpp>
#include <smks/math/math.hpp>
#include <smks/xchg/cfg.hpp>

#include "Config.hpp"

using namespace smks;

// explicit
py::Config::Config(const char* json)
{
    setDefaults();

    if (strlen(json) == 0)
        return;

    Json::Value  root;
    Json::Reader reader;

    if (!reader.parse(json, root))
    {
        std::stringstream sstr;
        sstr
            << "Failed to parse Json configuration: " << reader.getFormatedErrorMessages();
        throw sys::Exception(sstr.str().c_str(), "Module Device Configuration");
    }

    _logFile    = root.get(xchg::cfg::log_file,    _logFile.c_str()).asString();
    _numThreads = root.get(xchg::cfg::num_threads, _numThreads).asUInt();

    _withViewport  = root.get(xchg::cfg::with_viewport,  _withViewport).asBool();
    _withRendering = root.get(xchg::cfg::with_rendering, _withRendering).asBool();

    _screenWidth  = root.get(xchg::cfg::screen_width,  _screenWidth).asUInt();
    _screenHeight = root.get(xchg::cfg::screen_height, _screenHeight).asUInt();

    _viewportAnimation = root.get(xchg::cfg::viewport_animation, _viewportAnimation).asBool();
    _viewportAutoPlay  = root.get(xchg::cfg::viewport_auto_play, _viewportAutoPlay).asBool();

    _viewportOrbitCamera  = root.get(xchg::cfg::viewport_orbit_camera,  _viewportOrbitCamera).asBool();
    _viewportCameraFocus  = root.get(xchg::cfg::viewport_camera_focus,  _viewportCameraFocus).asBool();
    _viewportCameraSwitch = root.get(xchg::cfg::viewport_camera_switch, _viewportCameraSwitch).asBool();

    _viewportLightFocus = root.get(xchg::cfg::viewport_animation, _viewportLightFocus).asBool();

    _viewportFBufferSwitch = root.get(xchg::cfg::viewport_animation, _viewportFBufferSwitch).asBool();

    _viewportPicking     = root.get(xchg::cfg::viewport_animation, _viewportPicking).asBool();
    _viewportManipulator = root.get(xchg::cfg::viewport_animation, _viewportManipulator).asBool();

    _viewportHUD = root.get(xchg::cfg::viewport_animation, _viewportHUD).asBool();

    _viewportSnapshot  = root.get(xchg::cfg::viewport_animation, _viewportSnapshot).asBool();
    _viewportObjLoader = root.get(xchg::cfg::viewport_animation, _viewportObjLoader).asBool();
    _viewportDebug     = root.get(xchg::cfg::viewport_animation, _viewportDebug).asBool();

    if (_screenWidth == 0)
        throw sys::Exception(
            "Screen width must be positive.",
            "Module Configuration");
    if (_screenHeight == 0)
        throw sys::Exception(
            "Screen height must be positive.",
            "Module Configuration");
}

void
py::Config::setDefaults()
{
    _logFile    = "pyflirt.log";
    _numThreads = 0;
#if defined(_DEBUG)
    _numThreads = 1;
#endif // defined(_DEBUG)

    _withRendering = true;
    _withViewport  = true;

    _screenWidth  = 1280;
    _screenHeight = 1080;

    _viewportAnimation = true;
    _viewportAutoPlay  = false;

    _viewportOrbitCamera  = true;
    _viewportCameraFocus  = true;
    _viewportCameraSwitch = true;

    _viewportLightFocus = true;

    _viewportFBufferSwitch = true;

    _viewportPicking     = true;
    _viewportManipulator = true;

    _viewportHUD = true;

    _viewportSnapshot  = true;
    _viewportObjLoader = true;
    _viewportDebug     = true;
}
