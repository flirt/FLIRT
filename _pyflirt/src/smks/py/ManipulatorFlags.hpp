// ========================================================================= //
// Copyright 2016-2018 SUPAMONKS_STUDIO                                      //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
//                                                                           //
// ========================================================================= //
#pragma once

#include "common.hpp"   //<! includes <Python.h>
#include "structmember.h"

namespace smks { namespace py
{
    struct ManipulatorFlags
    {
        PyObject_HEAD
    };

    namespace doc
    {
        extern const char* const ManipulatorFlags_str;
    }

    extern const char* const    ManipulatorFlags_doc;
    extern void                 ManipulatorFlags_dealloc (ManipulatorFlags*);
    extern PyObject*            ManipulatorFlags_new     (PyTypeObject*, PyObject*, PyObject*);
    extern int                  ManipulatorFlags_init    (ManipulatorFlags*, PyObject*, PyObject*);
    extern PyObject*            ManipulatorFlags_tp_dict ();
    extern PyObject*            ManipulatorFlags_str     (PyObject*, PyObject*, PyObject*);

    static PyMemberDef          ManipulatorFlags_members [] = { { nullptr }  /* Sentinel */ };
    static PyGetSetDef          ManipulatorFlags_getsetters[] = { { nullptr }  /* Sentinel */ };

    static
    PyMethodDef
    ManipulatorFlags_methods[] =
    {
        { "str",    (PyCFunction)ManipulatorFlags_str,   METH_KEYWORDS | METH_STATIC,    doc::ManipulatorFlags_str    },
        { nullptr }  /* Sentinel */
    };

    static
    PyTypeObject
    ManipulatorFlags_Type =
    {
        PyObject_HEAD_INIT(nullptr)
        0,                                          /* ob_size */
        "_pyflirt.ManipulatorFlags",                 /* tp_name */
        sizeof(ManipulatorFlags),                    /* tp_basicsize */
        0,                                          /* tp_itemsize */
        (destructor)ManipulatorFlags_dealloc,        /* tp_dealloc */
        0,                                          /* tp_print */
        0,                                          /* tp_getattr */
        0,                                          /* tp_setattr */
        0,                                          /* tp_compare */
        0,                                          /* tp_repr */
        0,                                          /* tp_as_number */
        0,                                          /* tp_as_sequence */
        0,                                          /* tp_as_mapping */
        0,                                          /* tp_hash */
        0,                                          /* tp_call */
        0,                                          /* tp_str */
        0,                                          /* tp_getattro */
        0,                                          /* tp_setattro */
        0,                                          /* tp_as_buffer */
        Py_TPFLAGS_DEFAULT | Py_TPFLAGS_BASETYPE,   /* tp_flags */
        ManipulatorFlags_doc,                        /* tp_doc */
        0,                                          /* tp_traverse */
        0,                                          /* tp_clear */
        0,                                          /* tp_richcompare */
        0,                                          /* tp_weaklistoffset */
        0,                                          /* tp_iter */
        0,                                          /* tp_iternext */
        ManipulatorFlags_methods,                    /* tp_methods */
        ManipulatorFlags_members,                    /* tp_members */
        ManipulatorFlags_getsetters,                 /* tp_getset */
        0,                                          /* tp_base */
        0,                                          /* tp_dict */
        0,                                          /* tp_descr_get */
        0,                                          /* tp_descr_set */
        0,                                          /* tp_dictoffset */
        (initproc)ManipulatorFlags_init,             /* tp_init */
        0,                                          /* tp_alloc */
        ManipulatorFlags_new,                        /* tp_new */
    };
}
}
