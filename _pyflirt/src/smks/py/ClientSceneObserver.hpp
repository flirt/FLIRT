// ========================================================================= //
// Copyright 2016-2018 SUPAMONKS_STUDIO                                      //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
//                                                                           //
// ========================================================================= //
#pragma once

#include "_pyflirt.hpp"
#include <smks/xchg/SceneEvent.hpp>
#include <smks/AbstractClientObserver.hpp>

namespace smks { namespace py
{
    class ClientSceneObserver:
        public AbstractClientObserver
    {
    public:
        typedef std::shared_ptr<ClientSceneObserver> Ptr;
    private:
        PyObject* _func;
    public:
        static
        Ptr
        create(PyObject*);
    protected:
        explicit
        ClientSceneObserver(PyObject*);
    public:
        ~ClientSceneObserver();
    private:
        // non-copyable
        ClientSceneObserver(const ClientSceneObserver&);
        ClientSceneObserver& operator=(const ClientSceneObserver&);
    public:
        inline
        void
        handle(ClientBase&,
               const xchg::ClientEvent&)
        { }

        inline
        void
        handleSceneParmEvent(ClientBase&,
                             const xchg::ParmEvent&)
        { }

        void
        handle(ClientBase&,
               const xchg::SceneEvent&);
    private:
        void
        buildArgumentsAndKeywords(const xchg::SceneEvent&,
                                  PyObject*&,
                                  PyObject*&) const;
    };
}
}
