// ========================================================================= //
// Copyright 2016-2018 SUPAMONKS_STUDIO                                      //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
//                                                                           //
// ========================================================================= //
#include <smks/xchg/ActionState.hpp>
#include <std/to_string_xchg.hpp>
#include "ActionState.hpp"
#include "arg_parse.hpp"

using namespace smks;

// extern
const char* const
py::ActionState_doc =
{
    "Action state enumeration. \n\n"
    "While users can listen to any state, "
    "they should only set state parameters to ACTIVE or INACTIVE "
    "in order to control processes on scene nodes."
};

// extern
void
py::ActionState_dealloc(ActionState* self)
{
    self->ob_type->tp_free((PyObject*)self);
}

// extern
PyObject*
py::ActionState_tp_dict()
{
    PyObject* tp_dict = PyDict_New();

    insertInDict<int>(tp_dict, "INACTIVE",     xchg::INACTIVE);
    insertInDict<int>(tp_dict, "ACTIVE",       xchg::ACTIVE);
    insertInDict<int>(tp_dict, "ACTIVATING",   xchg::ACTIVATING);
    insertInDict<int>(tp_dict, "DEACTIVATING", xchg::DEACTIVATING);

    return tp_dict;
}

// extern
PyObject*
py::ActionState_new(PyTypeObject *type, PyObject *args, PyObject *kwds)
{
    ActionState *self;

    self = reinterpret_cast<ActionState*>(type->tp_alloc(type, 0));
    if (self != nullptr)
    { }

    return (PyObject *)self;
}

// extern
int
py::ActionState_init(ActionState *self, PyObject *args, PyObject *kwds)
{
    return 0;
}

// extern
const char* const
py::doc::ActionState_str =
{
    "Return a string representing the action state. \n\n"
    "\t :param state: action state \n"
    "\t :returns: string representing the action state \n"
};

// extern
PyObject*
py::ActionState_str(PyObject *self, PyObject *args, PyObject *kwds)
{
    static char*    argnames[]  = { "state", nullptr };
    PyObject*       objValue    = nullptr;

    if (!PyArg_ParseTupleAndKeywords(args, kwds, "O", argnames, &objValue))
        return nullptr;

    char state;

    if (!parse<char>(objValue, state, xchg::INACTIVE))
        return nullptr;
    return build<std::string>(std::to_string(state));
}
