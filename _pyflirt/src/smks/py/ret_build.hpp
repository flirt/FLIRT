// ========================================================================= //
// Copyright 2016-2018 SUPAMONKS_STUDIO                                      //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
//                                                                           //
// ========================================================================= //
#pragma once

#include "common.hpp"
#include <smks/math/types.hpp>
#include <smks/xchg/IdSet.hpp>
#include <smks/xchg/ActionState.hpp>

#include <iostream>

namespace smks { namespace py
{
    template<typename ValueType>
    PyObject*
    build(const ValueType&)
    {
        throw sys::Exception(
            "Unsupported type.",
            "Python Object From C Object");
    }

    template<> inline
    PyObject*
    build<bool>(const bool& value)
    {
        if (value)
            Py_RETURN_TRUE;
        else
            Py_RETURN_FALSE;
    }

    template<> inline
    PyObject*
    build<unsigned int>(const unsigned int& value)
    {
        return Py_BuildValue("I", value);
    }

    template<> inline
    PyObject*
    build<int>(const int& value)
    {
        return Py_BuildValue("i", value);
    }

    template<> inline
    PyObject*
    build<char>(const char& value)
    {
        return Py_BuildValue("b", value);
    }

    template<> inline
    PyObject*
    build<size_t>(const size_t& value)
    {
        return Py_BuildValue("K", value);
    }

    template<> inline
    PyObject*
    build<math::Vector2i>(const math::Vector2i& value)
    {
        return Py_BuildValue("[ii]", value.x(), value.y());
    }

    template<> inline
    PyObject*
    build<math::Vector3i>(const math::Vector3i& value)
    {
        return Py_BuildValue("[iii]", value.x(), value.y(), value.z());
    }

    template<> inline
    PyObject*
    build<math::Vector4i>(const math::Vector4i& value)
    {
        return Py_BuildValue("[iiii]", value.x(), value.y(), value.z(), value.w());
    }

    template<> inline
    PyObject*
    build<float>(const float& value)
    {
        return Py_BuildValue("f", value);
    }

    template<> inline
    PyObject*
    build<math::Vector2f>(const math::Vector2f& value)
    {
        return Py_BuildValue("[ff]", value.x(), value.y());
    }

    template<> inline
    PyObject*
    build<math::Vector4f>(const math::Vector4f& value)
    {
        return Py_BuildValue("[ffff]", value.x(), value.y(), value.z(), value.w());
    }

    template<> inline
    PyObject*
    build<math::Affine3f>(const math::Affine3f& value)
    {
        const float* data = value.matrix().data();

        return Py_BuildValue("[ffffffffffffffff]",
            data[0],    data[1],    data[2],    data[3],
            data[4],    data[5],    data[6],    data[7],
            data[8],    data[9],    data[10],   data[11],
            data[12],   data[13],   data[14],   data[15]);
    }

    template<> inline
    PyObject*
    build<std::string>(const std::string& value)
    {
        return Py_BuildValue("s", value.c_str());
    }

    template<> inline
    PyObject*
    build<xchg::IdSet>(const xchg::IdSet& value)
    {
        PyObject* ret = PyList_New(value.size());
        if (ret == nullptr)
            return nullptr;

        size_t i = 0;
        for (xchg::IdSet::const_iterator id = value.begin(); id != value.end(); ++id)
            PyList_SetItem(ret, i++, build<unsigned int>(*id));

        return ret;
    }

    template<> inline
    PyObject*
    build<std::vector<std::string>>(const std::vector<std::string>& value)
    {
        PyObject* ret = PyList_New(value.size());
        if (ret == nullptr)
            return nullptr;

        for (size_t i = 0; i < value.size(); ++i)
            PyList_SetItem(ret, i, build<std::string>(value[i]));

        return ret;
    }
}
}
