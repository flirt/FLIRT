// ========================================================================= //
// Copyright 2016-2018 SUPAMONKS_STUDIO                                      //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
//                                                                           //
// ========================================================================= //
#include "ProgressCallbacks.hpp"
#include "arg_parse.hpp"

using namespace smks;

// static
py::ProgressCallbacks::Ptr
py::ProgressCallbacks::create(PyObject* beginFunc,
                              PyObject* proceedFunc,
                              PyObject* endFunc)
{
    Ptr ptr(new ProgressCallbacks(beginFunc, proceedFunc, endFunc));
    return ptr;
}

py::ProgressCallbacks::ProgressCallbacks(PyObject*  beginFunc,
                                         PyObject*  proceedFunc,
                                         PyObject*  endFunc):
    _mutex      (),
    _beginFunc  (nullptr),
    _proceedFunc(nullptr),
    _endFunc    (nullptr)
{
    if (beginFunc == nullptr ||
        !PyCallable_Check(beginFunc))
        throw sys::Exception(
            "Specified 'begin' object is not a valid callable.",
            "Python Progress Callbacks Construction");
    if (proceedFunc == nullptr ||
        !PyCallable_Check(proceedFunc))
        throw sys::Exception(
            "Specified 'proceedFunc' object is not a valid callable.",
            "Python Progress Callbacks Construction");
    if (endFunc == nullptr ||
        !PyCallable_Check(endFunc))
        throw sys::Exception(
            "Specified 'endFunc' object is not a valid callable.",
            "Python Progress Callbacks Construction");
    Py_XINCREF(beginFunc);
    Py_XINCREF(proceedFunc);
    Py_XINCREF(endFunc);
    _beginFunc      = beginFunc;
    _proceedFunc    = proceedFunc;
    _endFunc        = endFunc;
}

py::ProgressCallbacks::~ProgressCallbacks()
{
    Py_XDECREF(_beginFunc);
    Py_XDECREF(_proceedFunc);
    Py_XDECREF(_endFunc);
}

void
py::ProgressCallbacks::begin(const char* str, size_t totalSteps)
{
    if (_beginFunc == nullptr)
        return;

    sys::sync::Lock<sys::sync::MutexSys> lock(_mutex);
    //----------------------------------
    PyObject* args = Py_BuildValue("(sK)", str, totalSteps);
    PyObject* kwds  = nullptr;
    PyObject* ret   = nullptr;
    try
    {
        ret = PyEval_CallObjectWithKeywords(_beginFunc, args, kwds);
    }
    catch (const std::exception& e)
    {
        throw sys::Exception(e.what(), "Begin Python Callback");
    }


    Py_XDECREF(args);
    Py_XDECREF(kwds);
    if (ret == nullptr)
        return;
    Py_DECREF(ret);
}

bool
py::ProgressCallbacks::proceed(size_t currentStep)
{
    if (_proceedFunc == nullptr)
        return true;    // proceed by default

    sys::sync::Lock<sys::sync::MutexSys> lock(_mutex);
    //----------------------------------
    PyObject* args = Py_BuildValue("(K)", currentStep);
    PyObject* kwds  = nullptr;
    PyObject* ret   = nullptr;
    try
    {
        ret = PyEval_CallObjectWithKeywords(_proceedFunc, args, kwds);
    }
    catch (const std::exception& e)
    {
        throw sys::Exception(e.what(), "Proceed Python Callback");
    }

    Py_XDECREF(args);
    Py_XDECREF(kwds);
    if (ret == nullptr)
        return true;
    bool b;
    parse<bool>(ret, b, true);
    Py_DECREF(ret);

    return b;
}

void
py::ProgressCallbacks::end(const char* str)
{
    if (_endFunc == nullptr)
        return;

    sys::sync::Lock<sys::sync::MutexSys> lock(_mutex);
    //----------------------------------
    PyObject* args = Py_BuildValue("(s)", str);
    PyObject* kwds  = nullptr;
    PyObject* ret   = nullptr;
    try
    {
        ret = PyEval_CallObjectWithKeywords(_endFunc, args, kwds);
    }
    catch (const std::exception& e)
    {
        throw sys::Exception(e.what(), "End Python Callback");
    }

    Py_XDECREF(args);
    Py_XDECREF(kwds);
    if (ret == nullptr)
        return;
    Py_DECREF(ret);
}
