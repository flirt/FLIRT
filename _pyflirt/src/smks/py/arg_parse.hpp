// ========================================================================= //
// Copyright 2016-2018 SUPAMONKS_STUDIO                                      //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
//                                                                           //
// ========================================================================= //
#pragma once

#include "common.hpp"
#include <smks/math/types.hpp>
#include <smks/xchg/IdSet.hpp>
#include <smks/xchg/ActionState.hpp>
#include <vector>

namespace smks { namespace py
{
    template<typename ValueType> inline
    bool
    parse(PyObject*, ValueType& ret, const ValueType& dflt)
    {
        throw sys::Exception(
            "Unsupported type.",
            "Python Object Parsing");
    }

    template<> inline
    bool
    parse<bool>(PyObject* obj, bool& ret, const bool& dflt)
    {
        ret = dflt;
        if (obj == nullptr ||
            !PyBool_Check(obj))
            return false;

        ret = PyObject_IsTrue(obj) == 1;
        return true;
    }

    template<> inline
    bool
    parse<int>(PyObject* obj, int& ret, const int& dflt)
    {
        int value = 0;

        if (obj &&
            (PyInt_Check(obj) || PyLong_Check(obj)) &&
            PyArg_Parse(obj, "i", &value) == 1)
        {
            ret = value;
            return true;
        }
        ret = dflt;
        return false;
    }

    template<> inline
    bool
    parse<char>(PyObject* obj, char& ret, const char& dflt)
    {
        int value = 0;

        if (obj &&
            (PyInt_Check(obj) || PyLong_Check(obj)) &&
            PyArg_Parse(obj, "i", &value) == 1)
        {
            ret = static_cast<char>(value);
            return true;
        }
        ret = dflt;
        return false;
    }

    template<> inline
    bool
    parse<unsigned int>(PyObject* obj, unsigned int& ret, const unsigned int& dflt)
    {
        unsigned int value = 0;

        if (obj &&
            (PyInt_Check(obj) || PyLong_Check(obj)) &&
            PyArg_Parse(obj, "I", &value) == 1)
        {
            ret = value;
            return true;
        }
        ret = dflt;
        return false;
    }

    template<> inline
    bool
    parse<size_t>(PyObject* obj, size_t& ret, const size_t& dflt)
    {
        size_t value = 0;

        if (obj &&
            (PyInt_Check(obj) || PyLong_Check(obj)) &&
            PyArg_Parse(obj, "K", &value) == 1)
        {
            ret = value;
            return true;
        }
        ret = dflt;
        return false;
    }

    template<> inline
    bool
    parse<float>(PyObject* obj, float& ret, const float& dflt)
    {
        float value = 0.0f;
        if (obj &&
            PyNumber_Check(obj) &&
            PyArg_Parse(obj, "f", &value) == 1)
        {
            ret = value;
            return true;
        }
        ret = dflt;
        return false;
    }

    template<> inline
    bool
    parse<std::string>(PyObject* obj, std::string& ret, const std::string& dflt)
    {
        const char* value = nullptr;
        if (obj && PyString_Check(obj) &&
            PyArg_Parse(obj, "s", &value) == 1)
        {
            ret = std::string(value);
            return true;
        }
        ret = dflt;
        return false;
    }

    template<> inline
    bool
    parse<std::vector<std::string>>(PyObject* obj, std::vector<std::string>& ret, const std::vector<std::string>& dflt)
    {
        ret.clear();
        if (obj)
        {
            if (PyString_Check(obj))
            {
                ret.resize(1);
                parse<std::string>(obj, ret.front(), !dflt.empty() ? dflt.front() : std::string());
                return true;
            }
            else if (PyList_Check(obj))
            {
                const size_t size = static_cast<size_t>(PyList_Size(obj));

                ret.resize(size);
                for (size_t i = 0; i < size; ++i)
                    parse<std::string>(PyList_GetItem(obj, i), ret[i], dflt.size() > i ? dflt[i] : std::string());
                return true;
            }
            else if (PyDict_Check(obj))
            {
                PyObject*       vals    = PyDict_Values(obj);
                const size_t    size    = static_cast<size_t>(PyList_Size(vals));

                ret.resize(size);
                for (size_t i = 0; i < size; ++i)
                    parse<std::string>(PyList_GetItem(vals, i), ret[i], dflt.size() > i ? dflt[i] : std::string());

                Py_DECREF(vals);
                return true;
            }
        }
        ret = dflt;
        return false;
    }

    template<> inline
    bool
    parse<math::Vector2i>(PyObject* obj, math::Vector2i& ret, const math::Vector2i& dflt)
    {
        if (obj == nullptr || !PyList_Check(obj))
        {
            ret = dflt;
            return false;
        }

        Py_ssize_t size = PyList_Size(obj);

        if (size > 0)
            parse<int>(PyList_GetItem(obj, 0), ret.x(), dflt.x());
        if (size > 1)
            parse<int>(PyList_GetItem(obj, 1), ret.y(), dflt.y());
        return true;
    }

    template<> inline
    bool
    parse<math::Vector3i>(PyObject* obj, math::Vector3i& ret, const math::Vector3i& dflt)
    {
        if (obj == nullptr || !PyList_Check(obj))
        {
            ret = dflt;
            return false;
        }

        Py_ssize_t size = PyList_Size(obj);

        if (size > 0)
            parse<int>(PyList_GetItem(obj, 0), ret.x(), dflt.x());
        if (size > 1)
            parse<int>(PyList_GetItem(obj, 1), ret.y(), dflt.y());
        if (size > 2)
            parse<int>(PyList_GetItem(obj, 2), ret.z(), dflt.z());
        return true;
    }

    template<> inline
    bool
    parse<math::Vector4i>(PyObject* obj, math::Vector4i& ret, const math::Vector4i& dflt)
    {
        if (obj == nullptr || !PyList_Check(obj))
        {
            ret = dflt;
            return false;
        }

        Py_ssize_t size = PyList_Size(obj);

        if (size > 0)
            parse<int>(PyList_GetItem(obj, 0), ret.x(), dflt.x());
        if (size > 1)
            parse<int>(PyList_GetItem(obj, 1), ret.y(), dflt.y());
        if (size > 2)
            parse<int>(PyList_GetItem(obj, 2), ret.z(), dflt.z());
        if (size > 3)
            parse<int>(PyList_GetItem(obj, 3), ret.w(), dflt.w());
        return true;
    }

    template<> inline
    bool
    parse<math::Vector2f>(PyObject* obj, math::Vector2f& ret, const math::Vector2f& dflt)
    {
        if (obj == nullptr || !PyList_Check(obj))
        {
            ret = dflt;
            return false;
        }

        Py_ssize_t size = PyList_Size(obj);

        if (size > 0)
            parse<float>(PyList_GetItem(obj, 0), ret.x(), dflt.x());
        if (size > 1)
            parse<float>(PyList_GetItem(obj, 1), ret.y(), dflt.y());
        return true;
    }

    template<> inline
    bool
    parse<math::Vector4f>(PyObject* obj, math::Vector4f& ret, const math::Vector4f& dflt)
    {
        if (obj == nullptr || !PyList_Check(obj))
        {
            ret = dflt;
            return false;
        }

        Py_ssize_t size = PyList_Size(obj);

        if (size > 0)
            parse<float>(PyList_GetItem(obj, 0), ret.x(), dflt.x());
        if (size > 1)
            parse<float>(PyList_GetItem(obj, 1), ret.y(), dflt.y());
        if (size > 2)
            parse<float>(PyList_GetItem(obj, 2), ret.z(), dflt.z());
        if (size > 3)
            parse<float>(PyList_GetItem(obj, 3), ret.w(), dflt.w());
        return true;
    }

    template<> inline
    bool
    parse<math::Affine3f>(PyObject* obj, math::Affine3f& ret, const math::Affine3f& dflt)
    {
        if (obj == nullptr || !PyList_Check(obj))
        {
            ret = dflt;
            return false;
        }

        Py_ssize_t size = PyList_Size(obj);

        for (ssize_t i = 0; i < 16; ++i)
            if (size > i)
                parse<float>(PyList_GetItem(obj, i), ret.matrix().data()[i], dflt.matrix().data()[i]);
        ret.makeAffine();
        return true;
    }

    template<> inline
    bool
    parse<xchg::IdSet>(PyObject* obj, xchg::IdSet& ret, const xchg::IdSet& dflt)
    {
        if (obj == nullptr || !PyList_Check(obj))
        {
            ret = dflt;
            return false;
        }

        Py_ssize_t size = PyList_Size(obj);

        ret.clear();
        for (ssize_t i = 0; i < size; ++i)
        {
            unsigned int id = 0;
            parse<unsigned int>(PyList_GetItem(obj, i), id, 0);
            if (id)
                ret.insert(id);
        }
        return true;
    }
}
}
