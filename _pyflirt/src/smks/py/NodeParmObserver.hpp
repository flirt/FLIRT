// ========================================================================= //
// Copyright 2016-2018 SUPAMONKS_STUDIO                                      //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
//                                                                           //
// ========================================================================= //
#pragma once

#include "_pyflirt.hpp"
#include <smks/AbstractClientNodeObserver.hpp>

namespace smks { namespace py
{
    class NodeParmObserver:
        public AbstractClientNodeObserver
    {
    public:
        typedef std::shared_ptr<NodeParmObserver> Ptr;
    private:
        PyObject*           _func;
        const unsigned int  _node;
    public:
        static
        Ptr
        create(PyObject*, unsigned int);
    protected:
        NodeParmObserver(PyObject*, unsigned int);
    public:
        ~NodeParmObserver();
    private:
        // non-copyable
        NodeParmObserver(const NodeParmObserver&);
        NodeParmObserver& operator=(const NodeParmObserver&);
    public:
        void
        handle(ClientBase&,
               unsigned int,
               const xchg::ParmEvent&);
    private:
        void
        buildArgumentsAndKeywords(const xchg::ParmEvent&, PyObject*&, PyObject*&) const;
    public:
        inline
        unsigned int
        node() const
        {
            return _node;
        }
    };
}
}
