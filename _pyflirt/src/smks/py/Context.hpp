// ========================================================================= //
// Copyright 2016-2018 SUPAMONKS_STUDIO                                      //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
//                                                                           //
// ========================================================================= //
#pragma once

#include <iostream>

#include <osgViewer/GraphicsWindow>
#include <osgGA/GUIEventHandler>

#include <smks/sys/ResourcePathFinder.hpp>
#include <smks/sys/ResourceDatabase.hpp>
#include <smks/xchg/Collection.hpp>
#include <smks/scn/Scene.hpp>
#include <smks/rndr/Client.hpp>
#include <smks/view/Client.hpp>
#include <smks/view/Animation.hpp>
#include <smks/view/AnimationControl.hpp>
#include <smks/view/AnimationTimeSwapper.hpp>
#include <smks/view/MousePicker.hpp>
#include <smks/view/CameraFocus.hpp>
#include <smks/view/CameraSwitch.hpp>
#include <smks/view/FrameBufferSwitch.hpp>
#include <smks/view/LightFocus.hpp>
#include <smks/view/hud/Handler.hpp>
#include <smks/view/Renderer.hpp>
#include <smks/view/ManipulatorControl.hpp>
#include <smks/view/SceneObjLoader.hpp>
#if defined(FLIRT_WITH_DEBUG)
#include <smks/view/TestSubSceneLoader.hpp>
#endif // defined(FLIRT_WITH_DEBUG)
#include "ControlCenter.hpp"
#include "AbstractCallableWrapper.hpp"

namespace smks
{
    namespace view
    {
        class LightFocus;
    }

    namespace py
    {
        //<! structure used to wrap all global data owned by the module
        struct Context
        {
        public:
            typedef osg::ref_ptr<osgViewer::GraphicsWindow>                     GraphicsWindowPtr;
            typedef boost::unordered_map<size_t, AbstractCallableWrapper::Ptr>  CallWrapperMap;
            typedef std::vector<osg::ref_ptr<osgGA::GUIEventHandler>>           GUIEventHandlers;
        public:
            sys::ResourcePathFinder::Ptr    pathFinder;
            sys::ResourceDatabase::Ptr      resources;
            //---
            scn::Scene::Ptr                 scene;
            //---
            GraphicsWindowPtr               graphicsWindow;
            rndr::Client::Ptr               rndrClient;
            view::Client::Ptr               viewClient;
            ControlCenter::Ptr              controlCenter;
            //---
            view::Animation::Ptr            viewAnimCallback;
            view::AnimationControl::Ptr     viewAnimCtrl;
            view::AnimationTimeSwapper::Ptr viewAnimTimeSwapper;
            view::MousePicker::Ptr          viewMousePicker;
            view::OrbitCameraHandler::Ptr   viewCamCtrl;
            view::CameraSwitch::Ptr         viewCamSwitch;
            view::FrameBufferSwitch::Ptr    viewFbufferSwitch;
            view::LightFocus::Ptr           viewLightFocus;
            view::hud::Handler::Ptr         viewHudHandler;
            view::Renderer::Ptr             viewRenderer;
            view::ManipulatorControl::Ptr   viewManipCtrl;
            view::SceneObjLoader::Ptr       viewObjLoader;
#if defined(FLIRT_WITH_DEBUG)
            view::TestSubSceneLoader::Ptr   viewTestLoader;
#endif // defined(FLIRT_WITH_DEBUG)
            //---
            size_t                          callableIdGenerator;
            CallWrapperMap                  callableWrappers;
            //---
            size_t                          progressIdGenerator;

        public:
            inline
            Context():
                pathFinder          (nullptr),
                resources           (nullptr),
                //---
                scene               (nullptr),
                //---
                graphicsWindow      (nullptr),
                rndrClient          (nullptr),
                viewClient          (nullptr),
                controlCenter       (nullptr),
                //---
                viewAnimCallback    (nullptr),
                viewAnimCtrl        (nullptr),
                viewAnimTimeSwapper (nullptr),
                viewMousePicker     (nullptr),
                viewCamCtrl         (nullptr),
                viewCamSwitch       (nullptr),
                viewFbufferSwitch   (nullptr),
                viewLightFocus      (nullptr),
                viewHudHandler      (nullptr),
                viewRenderer        (nullptr),
                viewManipCtrl       (nullptr),
                viewObjLoader       (nullptr),
#if defined(FLIRT_VIEW_DEBUG_ON)
                viewTestLoader      (nullptr),
#endif // defined(FLIRT_VIEW_DEBUG_ON)
                //---
                callableIdGenerator (0),
                callableWrappers    ()
            { }
        };
    }
}
