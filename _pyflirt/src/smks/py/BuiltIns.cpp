// ========================================================================= //
// Copyright 2016-2018 SUPAMONKS_STUDIO                                      //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
//                                                                           //
// ========================================================================= //
#include <smks/parm/BuiltIn.hpp>
#include <smks/parm/BuiltIns.hpp>
#include <smks/parm/BuiltInMap.hpp>
#include <smks/xchg/BoundingBox.hpp>
#include <smks/xchg/DataStream.hpp>
#include <smks/xchg/Data.hpp>
#include <smks/xchg/ObjectPtr.hpp>
#include <std/to_string_xchg.hpp>
#include "BuiltIns.hpp"
#include "ret_build.hpp"

namespace smks { namespace py
{
    static inline
    PyObject*
    buildInfoDict(const parm::BuiltIn& parm)
    {
        PyObject* infoDict = PyDict_New();

        // parameter ID, name, descriptive flags
        PyObject* itemId                = build<unsigned int>(parm.id());
        PyObject* itemName              = build<std::string>(parm.c_str());
        PyObject* itemNeedsScene        = build<bool>((parm.flags() & parm::NEEDS_SCENE     ) != 0);
        PyObject* itemCreatesLink       = build<bool>((parm.flags() & parm::CREATES_LINK    ) != 0);
        PyObject* itemTriggersAction    = build<bool>((parm.flags() & parm::TRIGGERS_ACTION ) != 0);
        PyObject* itemSwitchesState     = build<bool>((parm.flags() & parm::SWITCHES_STATE  ) != 0);

        PyDict_SetItemString(infoDict, "id",                itemId);
        PyDict_SetItemString(infoDict, "name",              itemName);
        PyDict_SetItemString(infoDict, "needs_scene",       itemNeedsScene);
        PyDict_SetItemString(infoDict, "creates_link",      itemCreatesLink);
        PyDict_SetItemString(infoDict, "triggers_action",   itemTriggersAction);
        PyDict_SetItemString(infoDict, "switches_state",    itemSwitchesState);
        Py_DECREF(itemId);
        Py_DECREF(itemName);
        Py_DECREF(itemNeedsScene);
        Py_DECREF(itemCreatesLink);
        Py_DECREF(itemTriggersAction);
        Py_DECREF(itemSwitchesState);

        // parameter type
        if (parm.type() != xchg::UNKNOWN_PARMTYPE)
        {
            PyObject* itemType = build<int>(static_cast<int>(parm.type()));

            PyDict_SetItemString(infoDict, "type", itemType);
            Py_DECREF(itemType);
        }

        // get default value whenever the parameter type if handled by the _pyflirt module.
        PyObject* itemDefault = nullptr;

        //----------------------------------------------
#define GET_BUILTIN_DEFAULT(_CTYPE_)                    \
        else if (parm.compatibleWith<_CTYPE_>())        \
        {                                               \
            _CTYPE_ parmValue;                          \
                                                        \
            parm.defaultValue<_CTYPE_>(parmValue);      \
            itemDefault = build<_CTYPE_>(parmValue);    \
        }
        //----------------------------------------------
        if (false) {}
        GET_BUILTIN_DEFAULT(bool)
        GET_BUILTIN_DEFAULT(unsigned int)
        GET_BUILTIN_DEFAULT(size_t)
        GET_BUILTIN_DEFAULT(char)
        GET_BUILTIN_DEFAULT(int)
        GET_BUILTIN_DEFAULT(math::Vector2i)
        GET_BUILTIN_DEFAULT(math::Vector3i)
        GET_BUILTIN_DEFAULT(math::Vector4i)
        GET_BUILTIN_DEFAULT(float)
        GET_BUILTIN_DEFAULT(math::Vector2f)
        GET_BUILTIN_DEFAULT(math::Vector4f)
        GET_BUILTIN_DEFAULT(math::Affine3f)
        GET_BUILTIN_DEFAULT(std::string)
        GET_BUILTIN_DEFAULT(xchg::IdSet)

        if (itemDefault)
        {
            PyDict_SetItemString(infoDict, "default", itemDefault);
            Py_DECREF(itemDefault);
        }

        return infoDict;
    }

    static inline
    void
    insertInDict(PyObject* dict, const parm::BuiltIn& parm)
    {
        assert(dict);

        PyObject* builtIn = buildInfoDict(parm);

        if (builtIn)
        {
            PyDict_SetItemString(dict, parm.c_str(), builtIn);
            Py_DECREF(builtIn);
        }
    }
}
}

using namespace smks;

// extern
const char* const
py::BuiltIns_doc =
{
    "Enumeration of all the built-in node parameters. \n\n"
    "Parameter data is stored in a dictionary with the following entries: \n\n"
    "* name: name of the parameter \n"
    "* id: ID of the parameter \n"
    "* type: type of the parameter \n"
    "* default: default value of the parameter \n"
};

// extern
void
py::BuiltIns_dealloc(BuiltIns* self)
{
    self->ob_type->tp_free((PyObject*)self);
}

// extern
PyObject*
py::BuiltIns_new(PyTypeObject *type, PyObject *args, PyObject *kwds)
{
    BuiltIns *self;

    self = reinterpret_cast<BuiltIns*>(type->tp_alloc(type, 0));
    if (self != nullptr)
    { }

    return (PyObject *)self;
}

// extern
int
py::BuiltIns_init(BuiltIns *self, PyObject *args, PyObject *kwds)
{
    return 0;
}

// extern
const char* const
py::doc::BuiltIns_find =
{
    "Find the built-in parameter associated with the given parameter ID. \n\n"
    "\t :param parm: ID of the parameter \n"
    "\t :returns: built-in parameter if found, else None \n"
};

// extern
PyObject*
py::BuiltIns_find(PyObject *self, PyObject *args, PyObject *kwds)
{
    static char*    argnames[]  = { "parm", nullptr };
    unsigned int    parmId      = 0;

    if (!PyArg_ParseTupleAndKeywords(args, kwds, "I", argnames, &parmId))
        return nullptr;

    parm::BuiltIn::Ptr const& builtIn = parm::builtIns().find(parmId);

    if (builtIn == nullptr)
        Py_RETURN_NONE;
    else
        return buildInfoDict(*builtIn);
}

// extern
PyObject*
py::BuiltIns_tp_dict()
{
    PyObject*           tp_dict = PyDict_New();
    parm::BuiltIn::Ptr  parm    = parm::builtIns().begin();
    while (parm)
    {
        insertInDict(tp_dict, *parm);
        parm = parm::builtIns().next(parm);
    }

    return tp_dict;
}
