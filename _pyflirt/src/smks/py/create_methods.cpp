// ========================================================================= //
// Copyright 2016-2018 SUPAMONKS_STUDIO                                      //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
//                                                                           //
// ========================================================================= //
#include "_pyflirt.hpp"
#include <iostream>
#include <std/to_string_xchg.hpp>
#include <smks/sys/Log.hpp>
#include <smks/xchg/ParmType.hpp>

#include "Context.hpp"
#include "ControlCenter.hpp"
#include "arg_parse.hpp"
#include "ret_build.hpp"

//namespace smks { namespace py
//{
//  template<typename ValueType>
//  int
//  parseAssignArgs(PyObject*, PyObject*, char**, const char* &parmName, ValueType &parmValue, const char* &name, unsigned int &parentId)
//  {
//      throw sys::Exception("Not specialized for requested value type.", "Assign Node Argument Parsing");
//  }
//
//  template<> int parseAssignArgs<bool>        (PyObject*, PyObject*, char**, const char* &parmName, bool          &parmValue, const char* &name, unsigned int &parentId);
//  template<> int parseAssignArgs<unsigned int>(PyObject*, PyObject*, char**, const char* &parmName, unsigned int  &parmValue, const char* &name, unsigned int &parentId);
//  template<> int parseAssignArgs<int>         (PyObject*, PyObject*, char**, const char* &parmName, int           &parmValue, const char* &name, unsigned int &parentId);
//  template<> int parseAssignArgs<size_t>      (PyObject*, PyObject*, char**, const char* &parmName, size_t        &parmValue, const char* &name, unsigned int &parentId);
//  template<> int parseAssignArgs<float>       (PyObject*, PyObject*, char**, const char* &parmName, float         &parmValue, const char* &name, unsigned int &parentId);
//  template<> int parseAssignArgs<std::string> (PyObject*, PyObject*, char**, const char* &parmName, std::string   &parmValue, const char* &name, unsigned int &parentId);
//}
//}

using namespace smks;


const char* const py::doc::createDataNode =
{
    "Create a generic node and add it to the scene. \n\n"
    "\t :param name: node's short name \n"
    "\t :param parent: ID of the node's parent (default 0: uses the scene's root) \n"
    "\t :returns: ID of the created node \n"
};

PyObject*
py::_createDataNode(PyObject* self, PyObject* args, PyObject* kwds)
{
    static char*    argnames[] = { "name", "parent", nullptr };
    const char*     name;
    unsigned int    parentId = 0;

    if (!PyArg_ParseTupleAndKeywords(args, kwds, "s|I", argnames, &name, &parentId))
        return nullptr;

    assert(g_context);
    const unsigned int id = g_context->controlCenter->createDataNode(name, parentId);

    return build<unsigned int>(id);
}


const char* const py::doc::destroyDataNode =
{
    "Remove the given node from the scene and destroy it. \n\n"
    "\t :param node: ID of the node \n"
};

PyObject*
py::_destroyDataNode(PyObject* self, PyObject* args, PyObject* kwds)
{
    static char*    argnames[] = { "node", nullptr };
    unsigned int    id = 0;

    if (!PyArg_ParseTupleAndKeywords(args, kwds, "I", argnames, &id))
        return nullptr;

    assert(g_context);
    g_context->controlCenter->destroyDataNode(id);

    Py_RETURN_NONE;
}


const char* const py::doc::createArchive =
{
    "Create an Alembic archive node and add it to the scene. \n\n"
    "\t :param path: path to the archive \n"
    "\t :param parent: ID of the archive's parent (default 0: uses the scene's root) \n"
    "\t :returns: ID of the created archive \n"
};

PyObject*
py::_createArchive(PyObject* self, PyObject* args, PyObject* kwds)
{
    static char*    argnames[] = { "path", "parent", nullptr };
    const char*     path;
    unsigned int    parentId = 0;

    if (!PyArg_ParseTupleAndKeywords(args, kwds, "s|I", argnames, &path, &parentId))
        return nullptr;

    assert(g_context);
    const unsigned int id = g_context->controlCenter->createArchive(path, parentId);

    return build<unsigned int>(id);
}


const char* const py::doc::createXform =
{
    "Create a transform node and add it to the scene. \n\n"
    "\t :param name: transform's short name \n"
    "\t :param parent: ID of the transform's parent (default 0: uses the scene's root) \n"
    "\t :returns: ID of the created transform \n"
};

PyObject*
py::_createXform(PyObject* self, PyObject* args, PyObject* kwds)
{
    static char*    argnames[] = { "name", "parent", nullptr };
    const char*     name;
    unsigned int    parentId = 0;

    if (!PyArg_ParseTupleAndKeywords(args, kwds, "s|I", argnames, &name, &parentId))
        return nullptr;

    assert(g_context);
    const unsigned int id = g_context->controlCenter->createXform(name, parentId);

    return build<unsigned int>(id);
}


const char* const py::doc::createCamera =
{
    "Create a camera node and add it to the scene. \n\n"
    "\t :param name: camera's short name \n"
    "\t :param parent: ID of the camera's parent (default 0: uses the scene's root) \n"
    "\t :returns: ID of the created camera \n"
};

PyObject*
py::_createCamera(PyObject* self, PyObject* args, PyObject* kwds)
{
    static char*    argnames[] = { "name", "parent", nullptr };
    const char*     name;
    unsigned int    parentId = 0;

    if (!PyArg_ParseTupleAndKeywords(args, kwds, "s|I", argnames, &name, &parentId))
        return nullptr;

    assert(g_context);
    const unsigned int id = g_context->controlCenter->createCamera(name, parentId);

    return build<unsigned int>(id);
}


const char* const py::doc::createMaterial =
{
    "Create a material node and add it to the scene. \n\n"
    "\t :param code: material's type code \n"
    "\t :param name: material's short name \n"
    "\t :param parent: ID of the material's parent (default 0: uses the scene's root) \n"
    "\t :returns: ID of the created material \n"
};

PyObject*
py::_createMaterial(PyObject* self, PyObject* args, PyObject* kwds)
{
    static char*    argnames[] = { "code", "name", "parent", nullptr };
    const char*     name;
    const char*     code;
    unsigned int    parentId = 0;

    if (!PyArg_ParseTupleAndKeywords(args, kwds, "ss|I", argnames, &code, &name, &parentId))
        return nullptr;

    assert(g_context);
    const unsigned int id = g_context->controlCenter->createMaterial(code, name, parentId);

    return build<unsigned int>(id);
}


const char* const py::doc::createSubSurface =
{
    "Create a subsurface node and add it to the scene. \n\n"
    "\t :param code: subsurface's type code \n"
    "\t :param name: subsurface's short name \n"
    "\t :param parent: ID of the subsurface's parent (default 0: uses the scene's root) \n"
    "\t :returns: ID of the created subsurface \n"
};

PyObject*
py::_createSubSurface(PyObject* self, PyObject* args, PyObject* kwds)
{
    static char*    argnames[] = { "code", "name", "parent", nullptr };
    const char*     name;
    const char*     code;
    unsigned int    parentId = 0;

    if (!PyArg_ParseTupleAndKeywords(args, kwds, "ss|I", argnames, &code, &name, &parentId))
        return nullptr;

    assert(g_context);
    const unsigned int id = g_context->controlCenter->createSubSurface(code, name, parentId);

    return build<unsigned int>(id);
}


const char* const py::doc::createTexture =
{
    "Create a texture node and add it to the scene. \n\n"
    "\t :param code: texture's type code \n"
    "\t :param name: texture's short name \n"
    "\t :param parent: ID of the texture's parent (default 0: uses the scene's root) \n"
    "\t :returns: ID of the created texture \n"
};

PyObject*
py::_createTexture(PyObject* self, PyObject* args, PyObject* kwds)
{
    static char*    argnames[] = { "code", "name", "parent", nullptr };
    const char*     name;
    const char*     code;
    unsigned int    parentId = 0;

    if (!PyArg_ParseTupleAndKeywords(args, kwds, "ss|I", argnames, &code, &name, &parentId))
        return nullptr;

    assert(g_context);
    const unsigned int id = g_context->controlCenter->createTexture(code, name, parentId);

    return build<unsigned int>(id);
}


const char* const py::doc::createSelector =
{
    "Create a selector node and add it to the scene. \n\n"
    "\t :param regex: regular expression defining the selection \n"
    "\t :param name: selector's short name \n"
    "\t :param parent: ID of the selector's parent (default 0: uses the scene's root) \n"
    "\t :returns: ID of the created selector \n"
};

PyObject*
py::_createSelector(PyObject* self, PyObject* args, PyObject* kwds)
{
    static char*    argnames[] = { "regex", "name", "parent", nullptr };
    const char*     name;
    const char*     regex;
    unsigned int    parentId = 0;

    if (!PyArg_ParseTupleAndKeywords(args, kwds, "ss|I", argnames, &regex, &name, &parentId))
        return nullptr;

    assert(g_context);
    const unsigned int id = g_context->controlCenter->createSelector(regex, name, parentId);

    return build<unsigned int>(id);
}


const char* const py::doc::createLight =
{
    "Create a light node and add it to the scene. \n\n"
    "\t :param code: light's type code \n"
    "\t :param name: light's short name \n"
    "\t :param parent: ID of the light's parent (default 0: uses the scene's root) \n"
    "\t :returns: ID of the created light \n"
};

PyObject*
py::_createLight(PyObject* self, PyObject* args, PyObject* kwds)
{
    static char*    argnames[] = { "code", "name", "parent", nullptr };
    const char*     name;
    const char*     code;
    unsigned int    parentId = 0;

    if (!PyArg_ParseTupleAndKeywords(args, kwds, "ss|I", argnames, &code, &name, &parentId))
        return nullptr;

    assert(g_context);
    const unsigned int id = g_context->controlCenter->createLight(code, name, parentId);

    return build<unsigned int>(id);
}


const char* const py::doc::createFrameBuffer =
{
    "Create a framebuffer node and add it to the scene. \n\n"
    "\t :param code: framebuffer's type code \n"
    "\t :param name: framebuffer's short name \n"
    "\t :param parent: ID of the framebuffer's parent (default 0: uses the scene's root) \n"
    "\t :returns: ID of the created framebuffer \n"
};

PyObject*
py::_createFrameBuffer(PyObject* self, PyObject* args, PyObject* kwds)
{
    static char*    argnames[] = { "code", "name", "parent", nullptr };
    const char*     name;
    const char*     code;
    unsigned int    parentId = 0;

    if (!PyArg_ParseTupleAndKeywords(args, kwds, "ss|I", argnames, &code, &name, &parentId))
        return nullptr;

    assert(g_context);
    const unsigned int id = g_context->controlCenter->createFrameBuffer(code, name, parentId);

    return build<unsigned int>(id);
}

const char* const py::doc::createDenoiser =
{
    "Create a denoiser node and add it to the scene. \n\n"
    "\t :param code: denoiser's type code \n"
    "\t :param name: denoiser's short name \n"
    "\t :param parent: ID of the denoiser's parent (default 0: uses the scene's root) \n"
    "\t :returns: ID of the created denoiser \n"
};

PyObject*
py::_createDenoiser(PyObject* self, PyObject* args, PyObject* kwds)
{
    static char*    argnames[] = { "code", "name", "parent", nullptr };
    const char*     name;
    const char*     code;
    unsigned int    parentId = 0;

    if (!PyArg_ParseTupleAndKeywords(args, kwds, "ss|I", argnames, &code, &name, &parentId))
        return nullptr;

    assert(g_context);
    const unsigned int id = g_context->controlCenter->createDenoiser(code, name, parentId);

    return build<unsigned int>(id);
}


const char* const py::doc::createToneMapper =
{
    "Create a tonemapper node and add it to the scene. \n\n"
    "\t :param code: tonemapper's type code \n"
    "\t :param name: tonemapper's short name \n"
    "\t :param parent: ID of the tonemapper's parent (default 0: uses the scene's root) \n"
    "\t :returns: ID of the created tonemapper \n"
};

PyObject*
py::_createToneMapper(PyObject* self, PyObject* args, PyObject* kwds)
{
    static char*    argnames[] = { "code", "name", "parent", nullptr };
    const char*     name;
    const char*     code;
    unsigned int    parentId = 0;

    if (!PyArg_ParseTupleAndKeywords(args, kwds, "ss|I", argnames, &code, &name, &parentId))
        return nullptr;

    assert(g_context);
    const unsigned int id = g_context->controlCenter->createToneMapper(code, name, parentId);

    return build<unsigned int>(id);
}


const char* const py::doc::createRenderer =
{
    "Create a renderer node and add it to the scene. \n\n"
    "\t :param code: renderer's type code \n"
    "\t :param name: renderer's short name \n"
    "\t :param parent: ID of the renderer's parent (default 0: uses the scene's root) \n"
    "\t :returns: ID of the created renderer \n"
};

PyObject*
py::_createRenderer(PyObject* self, PyObject* args, PyObject* kwds)
{
    static char*    argnames[] = { "code", "name", "parent", nullptr };
    const char*     name;
    const char*     code;
    unsigned int    parentId = 0;

    if (!PyArg_ParseTupleAndKeywords(args, kwds, "ss|I", argnames, &code, &name, &parentId))
        return nullptr;

    assert(g_context);
    const unsigned int id = g_context->controlCenter->createRenderer(code, name, parentId);

    return build<unsigned int>(id);
}


const char* const py::doc::createIntegrator =
{
    "Create an integrator node and add it to the scene. \n\n"
    "\t :param code: integrator's type code \n"
    "\t :param name: integrator's short name \n"
    "\t :param parent: ID of the integrator's parent (default 0: uses the scene's root) \n"
    "\t :returns: ID of the created integrator \n"
};

PyObject*
py::_createIntegrator(PyObject* self, PyObject* args, PyObject* kwds)
{
    static char*    argnames[] = { "code", "name", "parent", nullptr };
    const char*     name;
    const char*     code;
    unsigned int    parentId = 0;

    if (!PyArg_ParseTupleAndKeywords(args, kwds, "ss|I", argnames, &code, &name, &parentId))
        return nullptr;

    assert(g_context);
    const unsigned int id = g_context->controlCenter->createIntegrator(code, name, parentId);

    return build<unsigned int>(id);
}


const char* const py::doc::createSamplerFactory =
{
    "Create a sampler node and add it to the scene. \n\n"
    "\t :param code: sampler's type code \n"
    "\t :param name: sampler's short name \n"
    "\t :param parent: ID of the sampler's parent (default 0: uses the scene's root) \n"
    "\t :returns: ID of the created sampler \n"
};

PyObject*
py::_createSamplerFactory(PyObject* self, PyObject* args, PyObject* kwds)
{
    static char*    argnames[] = { "code", "name", "parent", nullptr };
    const char*     name;
    const char*     code;
    unsigned int    parentId = 0;

    if (!PyArg_ParseTupleAndKeywords(args, kwds, "ss|I", argnames, &code, &name, &parentId))
        return nullptr;

    assert(g_context);
    const unsigned int id = g_context->controlCenter->createSamplerFactory(code, name, parentId);

    return build<unsigned int>(id);
}


const char* const py::doc::createPixelFilter =
{
    "Create a pixel filter node and add it to the scene. \n\n"
    "\t :param code: filter's type code \n"
    "\t :param name: filter's short name \n"
    "\t :param parent: ID of the filter's parent (default 0: uses the scene's root) \n"
    "\t :returns: ID of the created pixel filter \n"
};

PyObject*
py::_createPixelFilter(PyObject* self, PyObject* args, PyObject* kwds)
{
    static char*    argnames[] = { "code", "name", "parent", nullptr };
    const char*     name;
    const char*     code;
    unsigned int    parentId = 0;

    if (!PyArg_ParseTupleAndKeywords(args, kwds, "ss|I", argnames, &code, &name, &parentId))
        return nullptr;

    assert(g_context);
    const unsigned int id = g_context->controlCenter->createPixelFilter(code, name, parentId);

    return build<unsigned int>(id);
}


const char* const py::doc::createRenderAction =
{
    "Create a render action node and add it to the scene. \n\n"
    "\t :param name: render action's short name \n"
    "\t :param parent: ID of the render action's parent (default 0: uses the scene's root) \n"
    "\t :returns: ID of the created render action \n"
};

PyObject*
py::_createRenderAction(PyObject* self, PyObject* args, PyObject* kwds)
{
    static char*    argnames[] = { "name", "parent", nullptr };
    const char*     name;
    unsigned int    parentId = 0;

    if (!PyArg_ParseTupleAndKeywords(args, kwds, "s|I", argnames, &name, &parentId))
        return nullptr;

    assert(g_context);
    const unsigned int id = g_context->controlCenter->createRenderAction(name, parentId);

    return build<unsigned int>(id);
}


const char* const py::doc::createShapeIdCoveragePass =
{
    "Create, and add to the scene, a render pass node "
    "triggering the computation of the shapes' (ID, coverage) pairs per pixel. \n\n"
    "\t :param name: render pass's short name \n"
    "\t :param parent: ID of the render pass's parent (default 0: uses the scene's root) \n"
    "\t :returns: ID of the created render pass \n"
};

PyObject*
py::_createShapeIdCoveragePass(PyObject* self, PyObject* args, PyObject* kwds)
{
    static char*    argnames[] = { "name", "parent", nullptr };
    const char*     name;
    unsigned int    parentId = 0;

    if (!PyArg_ParseTupleAndKeywords(args, kwds, "s|I", argnames, &name, &parentId))
        return nullptr;

    assert(g_context);
    const unsigned int id = g_context->controlCenter->createShapeIdCoveragePass(name, parentId);

    return build<unsigned int>(id);
}


const char* const py::doc::createFlatRenderPass =
{
    "Create, and add to the scene, a render pass node "
    "triggering the saving of additional data per pixel. \n\n"
    "\t :param code: render pass's type code \n"
    "\t :param name: render pass's short name \n"
    "\t :param parent: ID of the render pass's parent (default 0: uses the scene's root) \n"
    "\t :returns: ID of the created render pass \n"
};

PyObject*
py::_createFlatRenderPass(PyObject* self, PyObject* args, PyObject* kwds)
{
    static char*    argnames[] = { "code", "name", "parent", nullptr };
    const char*     code;
    const char*     name;
    unsigned int    parentId = 0;

    if (!PyArg_ParseTupleAndKeywords(args, kwds, "ss|I", argnames, &code, &name, &parentId))
        return nullptr;

    assert(g_context);
    const unsigned int id = g_context->controlCenter->createFlatRenderPass(code, name, parentId);

    return build<unsigned int>(id);
}


const char* const py::doc::createLightPathExpressionPass =
{
    "Create, and add to the scene, a render pass node "
    "triggering the per pixel accumulation of the contributions "
    "of a selected set of rays. \n\n"
    "\t :param expr: expression that describes the expected light paths for selecting rays \n"
    "\t :param name: render pass's short name \n"
    "\t :param parent: ID of the render pass's parent (default 0: uses the scene's root) \n"
    "\t :returns: ID of the created render pass \n"
};

PyObject*
py::_createLightPathExpressionPass(PyObject* self, PyObject* args, PyObject* kwds)
{
    static char*    argnames[] = { "expr", "name", "parent", nullptr };
    const char*     expr;
    const char*     name;
    unsigned int    parentId = 0;

    if (!PyArg_ParseTupleAndKeywords(args, kwds, "ss|I", argnames, &expr, &name, &parentId))
        return nullptr;

    assert(g_context);
    const unsigned int id = g_context->controlCenter->createLightPathExpressionPass(expr, name, parentId);

    return build<unsigned int>(id);
}


const char* const py::doc::createAssign =
{
    "Create a parameter assignment node and add it to the scene. "
    "Assignments get effective once configured with a node selection pattern "
    "or linked to a selector node. \n\n"
    "\t :param parm_name: assigned parameter's name \n"
    "\t :param parm_type: assigned parameter's type \n"
    "\t :param parm_value: assigned parameter's value \n"
    "\t :param name: assignment's short name \n"
    "\t :param parent: ID of the assignment's parent (default 0: uses the scene's root) \n"
    "\t :returns: ID of the created assignment node \n"
};

PyObject*
py::_createAssign(PyObject* self, PyObject* args, PyObject* kwds)
{
    static char*    argnames[]  = { "parm_name", "parm_type", "parm_value", "name", "parent", nullptr };
    const char*     parmName    = "";
    int             typ         = xchg::UNKNOWN_PARMTYPE;
    PyObject*       objValue;
    const char*     name        = "";
    unsigned int    parentId    = 0;

    if (!PyArg_ParseTupleAndKeywords(args, kwds, "siOs|I", argnames, &parmName, &typ, &objValue, &name, &parentId))
        return nullptr;

    const xchg::ParmType parmType = static_cast<xchg::ParmType>(typ);
    assert(g_context);

    //----------------------------------------------------------------------------------
#define CREATE_ASSIGN(_PARMTYPE_VALUE_, _CTYPE_, _DEFAULT_)                             \
    if (parmType == _PARMTYPE_VALUE_)                                                   \
    {                                                                                   \
        _CTYPE_ parmValue;                                                              \
        if (!parse<_CTYPE_>(objValue, parmValue, _DEFAULT_))                            \
        {                                                                               \
            std::stringstream sstr;                                                     \
            sstr << "Failed to parse '" << std::to_string(parmType) << "' value.";      \
            throw sys::Exception(sstr.str().c_str(), "Parameter Assignment Creation");  \
        }                                                                               \
        return build<unsigned int>(g_context->controlCenter->createAssign<_CTYPE_>(     \
            parmName, parmValue, name, parentId));                                      \
    }
    //----------------------------------------------------------------------------------

    CREATE_ASSIGN(xchg::PARMTYPE_BOOL,      bool,           false)
    CREATE_ASSIGN(xchg::PARMTYPE_CHAR,      char,           0)
    CREATE_ASSIGN(xchg::PARMTYPE_UINT,      unsigned int,   0)
    CREATE_ASSIGN(xchg::PARMTYPE_SIZE,      size_t,         0)
    CREATE_ASSIGN(xchg::PARMTYPE_INT,       int,            0)
    CREATE_ASSIGN(xchg::PARMTYPE_INT_2,     math::Vector2i, math::Vector2i::Zero())
    CREATE_ASSIGN(xchg::PARMTYPE_INT_3,     math::Vector3i, math::Vector3i::Zero())
    CREATE_ASSIGN(xchg::PARMTYPE_INT_4,     math::Vector4i, math::Vector4i::Zero())
    CREATE_ASSIGN(xchg::PARMTYPE_FLOAT,     float,          0.0f)
    CREATE_ASSIGN(xchg::PARMTYPE_FLOAT_2,   math::Vector2f, math::Vector2f::Zero())
    CREATE_ASSIGN(xchg::PARMTYPE_FLOAT_4,   math::Vector4f, math::Vector4f::Zero())
    CREATE_ASSIGN(xchg::PARMTYPE_AFFINE_3,  math::Affine3f, math::Affine3f::Identity())
    CREATE_ASSIGN(xchg::PARMTYPE_STRING,    std::string,    std::string())

    {
        std::stringstream sstr;
        sstr << "Unsupported parameter type '" << std::to_string(parmType) << "'.";
        throw sys::Exception(sstr.str().c_str(), "Parameter Assignment Creation");
    }
    Py_RETURN_NONE;
}


const char* const py::doc::createIdAssign =
{
    "Create an assignment node for a parameter of type ID and add it to the scene. "
    "Assignments get effective once configured with a node selection pattern "
    "or linked to a selector node. \n\n"
    "\t :param parm_name: assigned parameter's name \n"
    "\t :param parm_value: assigned parameter's value \n"
    "\t :param name: assignment's short name \n"
    "\t :param parent: ID of the assignment's parent (default 0: uses the scene's root) \n"
    "\t :returns: ID of the created assignment node \n"
};

PyObject*
py::_createIdAssign(PyObject* self, PyObject* args, PyObject* kwds)
{
    static char*    argnames[]  = { "parm_name", "parm_value", "name", "parent", nullptr };
    const char*     parmName    = "";
    unsigned int    parmValue   = 0;
    const char*     name        = "";
    unsigned int    parentId    = 0;

    if (!PyArg_ParseTupleAndKeywords(args, kwds, "sIs|I", argnames, &parmName, &parmValue, &name, &parentId))
        return nullptr;

    assert(g_context);
    const unsigned int id = g_context->controlCenter->createIdAssign(parmName, parmValue, name, parentId);
    return build<unsigned int>(id);
}
