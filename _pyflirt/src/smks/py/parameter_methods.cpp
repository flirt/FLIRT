// ========================================================================= //
// Copyright 2016-2018 SUPAMONKS_STUDIO                                      //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
//                                                                           //
// ========================================================================= //
#include "_pyflirt.hpp"
#include "arg_parse.hpp"
#include "ret_build.hpp"
#include <iostream>

#include <std/to_string_xchg.hpp>
#include <smks/sys/Log.hpp>
#include <smks/parm/BuiltIn.hpp>
#include <smks/parm/BuiltIns.hpp>
#include <smks/xchg/ParmType.hpp>
#include <smks/xchg/Data.hpp>
#include <smks/xchg/getParmType.hpp>

#include "Context.hpp"
#include "ControlCenter.hpp"
#include "ParmType.hpp"

using namespace smks;

const char* const py::doc::setNodeParameterByName =
{
    "Add a named parameter to a node, and set its value. \n\n"
    "\t :param parm_name: name of the parameter \n"
    "\t :param parm_type: type of the parameter (bounding box, "
    "data, data stream and object unsupported) \n"
    "\t :param parm_value: value of the parameter \n"
    "\t :param node: ID of the node (default 0: edits the scene's root) \n"
    "\t :returns: the ID of the created parameter \n\n"
};

PyObject*
py::_setNodeParameterByName(PyObject* self, PyObject* args, PyObject* kwds)
{
    static char*    argnames[]  = { "parm_name", "parm_type", "parm_value", "node", nullptr };
    const char*     parmName    = "";
    int             typ         = xchg::UNKNOWN_PARMTYPE;
    PyObject*       objValue    = nullptr;
    unsigned int    nodeId      = 0;

    if (!PyArg_ParseTupleAndKeywords(args, kwds, "siO|I", argnames, &parmName, &typ, &objValue, &nodeId))
        return nullptr;

    assert(g_context);
    const xchg::ParmType parmType = static_cast<xchg::ParmType>(typ);

    //-------------------------------------------------------------------------------------------------
#define SET_PARM(_PARMTYPE_VALUE_, _CTYPE_, _DEFAULT_)                                              \
    else if (parmType == _PARMTYPE_VALUE_)                                                          \
    {                                                                                               \
        _CTYPE_ parmValue;                                                                          \
                                                                                                    \
        if (!parse<_CTYPE_>(objValue, parmValue, _DEFAULT_))                                        \
        {                                                                                           \
            std::stringstream sstr;                                                                 \
            sstr                                                                                    \
                << "Failed to parse " #_CTYPE_ " value "                                            \
                << "to update parameter '" << parmName << ".";                                      \
            throw sys::Exception(sstr.str().c_str(), "Node Parameter Setter");                      \
        }                                                                                           \
        const unsigned int parmId = g_context->controlCenter                                        \
            ->setNodeParameter<_CTYPE_>(parmName, parmValue, nodeId);                               \
        return build<unsigned int>(parmId);                                                         \
    }
    //-------------------------------------------------------------------------------------------------
    if (false) {}   // used to initialize the series of 'else' statements
    SET_PARM(xchg::PARMTYPE_BOOL,       bool,               false)
    SET_PARM(xchg::PARMTYPE_INT,        int,                0)
    SET_PARM(xchg::PARMTYPE_UINT,       unsigned int,       0)
    SET_PARM(xchg::PARMTYPE_SIZE,       size_t,             0)
    SET_PARM(xchg::PARMTYPE_CHAR,       char,               0)
    SET_PARM(xchg::PARMTYPE_INT_2,      math::Vector2i,     math::Vector2i::Zero())
    SET_PARM(xchg::PARMTYPE_INT_3,      math::Vector3i,     math::Vector3i::Zero())
    SET_PARM(xchg::PARMTYPE_INT_4,      math::Vector4i,     math::Vector4i::Zero())
    SET_PARM(xchg::PARMTYPE_FLOAT,      float,              0.0f)
    SET_PARM(xchg::PARMTYPE_FLOAT_2,    math::Vector2f,     math::Vector2f::Zero())
    SET_PARM(xchg::PARMTYPE_FLOAT_4,    math::Vector4f,     math::Vector4f::Zero())
    SET_PARM(xchg::PARMTYPE_AFFINE_3,   math::Affine3f,     math::Affine3f::Identity())
    SET_PARM(xchg::PARMTYPE_STRING,     std::string,        std::string())
    SET_PARM(xchg::PARMTYPE_IDSET,      xchg::IdSet,        xchg::IdSet())

    {
        std::stringstream sstr;
        sstr
            << "Parameter type '" << std::to_string(parmType) << "' "
            << "not handled by _pyflirt module.";
        throw sys::Exception(sstr.str().c_str(), "Node Parameter Setter");
    }
    Py_RETURN_NONE;
}


const char* const py::doc::setIdNodeParameterByName =
{
    "Add a named parameter of type ID to a node, and set its value. \n\n"
    "\t :param parm_name: name of the parameter \n"
    "\t :param parm_value: value of the parameter \n"
    "\t :param node: ID of the node (default 0: edits the scene's root) \n"
    "\t :returns: the ID of the created parameter \n\n"
};

PyObject*
py::_setIdNodeParameterByName(PyObject* self, PyObject* args, PyObject* kwds)
{
    static char*    argnames[]  = { "parm_name", "parm_value", "node", nullptr };
    const char*     parmName    = "";
    unsigned int    parmValue   = 0;
    unsigned int    nodeId      = 0;

    if (!PyArg_ParseTupleAndKeywords(args, kwds, "sI|I", argnames, &parmName, &parmValue, &nodeId))
        return nullptr;
    const unsigned int parmId = g_context->controlCenter->setNodeParameterId(parmName, parmValue, nodeId);
    return build<unsigned int>(parmId);
}


const char* const py::doc::setTriggerNodeParameterByName =
{
    "Trigger a node action via a named parameter. \n\n"
    "\t :param parm_name: name of the parameter \n"
    "\t :param node: ID of the node (default 0: edits the scene's root) \n"
    "\t :returns: the ID of the parameter used as trigger \n\n"
};

PyObject*
py::_setTriggerNodeParameterByName(PyObject* self, PyObject* args, PyObject* kwds)
{
    static char*    argnames[]  = { "parm_name", "node", nullptr };
    const char*     parmName    = "";
    unsigned int    nodeId      = 0;

    if (!PyArg_ParseTupleAndKeywords(args, kwds, "s|I", argnames, &parmName, &nodeId))
        return nullptr;
    const unsigned int parmId = g_context->controlCenter->setNodeParameterTrigger(parmName, nodeId);
    return build<unsigned int>(parmId);
}


const char* const py::doc::setSwitchNodeParameterByName =
{
    "Switch a node's state via a named parameter. \n\n"
    "\t :param parm_name: name of the parameter \n"
    "\t :param parm_value: state of the switch \n"
    "\t :param node: ID of the node (default 0: edits the scene's root) \n"
    "\t :returns: the ID of the parameter used as switch \n\n"
};

PyObject*
py::_setSwitchNodeParameterByName(PyObject* self, PyObject* args, PyObject* kwds)
{
    static char*    argnames[]  = { "parm_name", "parm_value", "node", nullptr };
    const char*     parmName    = "";
    PyObject*       objValue    = nullptr;
    unsigned int    nodeId      = 0;

    if (!PyArg_ParseTupleAndKeywords(args, kwds, "sO|I", argnames, &parmName, &objValue, &nodeId))
        return nullptr;
    const bool          parmValue   = objValue && PyBool_Check(objValue) && PyObject_IsTrue(objValue) == 1;
    const unsigned int  parmId      = g_context->controlCenter->setNodeParameterSwitch(parmName, parmValue, nodeId);
    return build<unsigned int>(parmId);
}


const char* const py::doc::setNodeParameter =
{
    "Set the value of a built-in or existing parameter on a given node. \n\n"
    "\t :param parm: ID of the parameter \n"
    "\t :param parm_value: value of the parameter \n"
    "\t :param node: ID of the node (default 0: edits the scene's root) \n"
    "\t :raises: error when the parameter cannot be determined "
    "or corresponds to a builtin-in parameter of an unsupported type "
    "(bounding box, data, data stream or object) \n"
};

PyObject*
py::_setNodeParameter(PyObject* self, PyObject* args, PyObject* kwds)
{
    static char*    argnames[]  = { "parm", "parm_value", "node", nullptr };
    unsigned int    parmId      = 0;
    PyObject*       objValue    = nullptr;
    unsigned int    nodeId      = 0;

    if (!PyArg_ParseTupleAndKeywords(args, kwds, "IO|I", argnames, &parmId, &objValue, &nodeId))
        return nullptr;

    assert(g_context);

    xchg::ParameterInfo parmInfo;
    if (!g_context->controlCenter->getNodeParameterInfo(parmId, parmInfo, nodeId))
    {
        parm::BuiltIn::Ptr const& builtIn = parm::builtIns().find(parmId);

        if (!builtIn)
        {
            std::stringstream sstr;
            sstr
                << "Failed to recognize parameter with ID = " << parmId << " "
                << "on node ID = " << nodeId << ".";
            throw sys::Exception(sstr.str().c_str(), "Node Parameter Update");
        }
        parmInfo.name       = builtIn->c_str();
        parmInfo.typeinfo   = builtIn->typeinfo();
    }
    assert(parmInfo.name);
    assert(parmInfo.typeinfo);

    //-------------------------------------------------------------------------------------------------
#define UPDATE_PARM(_CTYPE_, _DEFAULT_)                                                                 \
    else if (*parmInfo.typeinfo == typeid(_CTYPE_))                                                     \
    {                                                                                                   \
        _CTYPE_ parmValue;                                                                              \
                                                                                                        \
        if (!parse<_CTYPE_>(objValue, parmValue, _DEFAULT_))                                            \
        {                                                                                               \
            std::stringstream sstr;                                                                     \
            sstr                                                                                        \
                << "Failed to parse " #_CTYPE_ " value "                                                \
                << "to update parameter '" << parmInfo.name << ".";                                     \
            throw sys::Exception(sstr.str().c_str(), "Node Parameter Update");                          \
        }                                                                                               \
        g_context->controlCenter->updateNodeParameter<_CTYPE_>(parmId, parmValue, nodeId);              \
        Py_RETURN_NONE;                                                                                 \
    }
    //-------------------------------------------------------------------------------------------------
    if (false) {}   // used to initialize the series of 'else' statements
    UPDATE_PARM(bool,               false)
    UPDATE_PARM(int,                0)
    UPDATE_PARM(unsigned int,       0)
    UPDATE_PARM(size_t,             0)
    UPDATE_PARM(char,               0)
    UPDATE_PARM(math::Vector2i,     math::Vector2i::Zero())
    UPDATE_PARM(math::Vector3i,     math::Vector3i::Zero())
    UPDATE_PARM(math::Vector4i,     math::Vector4i::Zero())
    UPDATE_PARM(float,              0.0f)
    UPDATE_PARM(math::Vector2f,     math::Vector2f::Zero())
    UPDATE_PARM(math::Vector4f,     math::Vector4f::Zero())
    UPDATE_PARM(math::Affine3f,     math::Affine3f::Identity())
    UPDATE_PARM(std::string,        std::string())
    UPDATE_PARM(xchg::IdSet,        xchg::IdSet())

    {
        std::stringstream sstr;
        sstr << "Parameter type not handled by _pyflirt module.";
        throw sys::Exception(sstr.str().c_str(), "Node Parameter Update");
    }
    Py_RETURN_NONE;
}


const char* const py::doc::getNodeParameter=
{
    "Get the value of a given node's parameter. \n\n"
    "\t :param parm: ID of the parameter \n"
    "\t :param node: ID of the node (default = 0: queries the scene's root) \n"
    "\t :returns: value of the parameter, or None if the parameter was not found \n"
};

PyObject*
py::_getNodeParameter(PyObject* self, PyObject* args, PyObject* kwds)
{
    static char*    argnames[]  = { "parm", "node", nullptr };
    unsigned int    parmId      = 0;
    unsigned int    nodeId      = 0;

    if (!PyArg_ParseTupleAndKeywords(args, kwds, "I|I", argnames, &parmId, &nodeId))
        return nullptr;

    assert(g_context);

    xchg::ParameterInfo parmInfo;
    if (!g_context->controlCenter->getNodeParameterInfo(parmId, parmInfo, nodeId))
    {
        // built-in parameter
        //-------------------
        parm::BuiltIn::Ptr const& builtIn = parm::builtIns().find(parmId);

        if (builtIn == nullptr)
            Py_RETURN_NONE;

        //--------------------------------------------------------------------------------------
#define GET_BUILTIN_VALUE(_CTYPE_)                                                              \
        else if (builtIn->compatibleWith<_CTYPE_>())                                            \
        {                                                                                       \
            _CTYPE_ parmValue;                                                                  \
            g_context->controlCenter->getNodeParameter<_CTYPE_>(*builtIn, parmValue, nodeId);   \
            return build<_CTYPE_>(parmValue);                                                   \
        }
        //--------------------------------------------------------------------------------------
        if (false) {}   // used to initialize the series of 'else' statements
        GET_BUILTIN_VALUE(bool)
        GET_BUILTIN_VALUE(unsigned int)
        GET_BUILTIN_VALUE(int)
        GET_BUILTIN_VALUE(size_t)
        GET_BUILTIN_VALUE(char)
        GET_BUILTIN_VALUE(math::Vector2i)
        GET_BUILTIN_VALUE(math::Vector3i)
        GET_BUILTIN_VALUE(math::Vector4i)
        GET_BUILTIN_VALUE(float)
        GET_BUILTIN_VALUE(math::Vector2f)
        GET_BUILTIN_VALUE(math::Vector4f)
        GET_BUILTIN_VALUE(math::Affine3f)
        GET_BUILTIN_VALUE(std::string)
        GET_BUILTIN_VALUE(xchg::IdSet)
        {
            std::stringstream sstr;
            sstr
                << "Parameter type '" << std::to_string(builtIn->type()) << "' "
                << "not handled by _pyflirt module.";
            throw sys::Exception(sstr.str().c_str(), "Node Parameter Getter");
        }
    }

    // stored parameter
    //-----------------
    assert(parmInfo.name);
    assert(parmInfo.typeinfo);
    //----------------------------------------------------------------------------------------------
#define GET_PARM_VALUE(_CTYPE_, _DEFAULT_)                                                          \
    else if (*parmInfo.typeinfo == typeid(_CTYPE_))                                                 \
    {                                                                                               \
        _CTYPE_ parmValue;                                                                          \
        g_context->controlCenter->getNodeParameter<_CTYPE_>(parmId, parmValue, _DEFAULT_, nodeId);  \
        return build<_CTYPE_>(parmValue);                                                           \
    }
    //----------------------------------------------------------------------------------------------
    if (false) {}   // used to initialize the series of 'else' statements
    GET_PARM_VALUE(bool,                false)
    GET_PARM_VALUE(unsigned int,        0)
    GET_PARM_VALUE(int,                 0)
    GET_PARM_VALUE(size_t,              0)
    GET_PARM_VALUE(char,                0)
    GET_PARM_VALUE(math::Vector2i,      math::Vector2i::Zero())
    GET_PARM_VALUE(math::Vector3i,      math::Vector3i::Zero())
    GET_PARM_VALUE(math::Vector4i,      math::Vector4i::Zero())
    GET_PARM_VALUE(float,               0.0f)
    GET_PARM_VALUE(math::Vector2f,      math::Vector2f::Zero())
    GET_PARM_VALUE(math::Vector4f,      math::Vector4f::Zero())
    GET_PARM_VALUE(math::Affine3f,      math::Affine3f::Identity())
    GET_PARM_VALUE(std::string,         std::string())
    GET_PARM_VALUE(xchg::IdSet,         xchg::IdSet())

    {
        std::stringstream sstr;
        sstr << "Parameter type not handled by _pyflirt module.";
        throw sys::Exception(sstr.str().c_str(), "Node Parameter Getter");
    }
    Py_RETURN_NONE;
}


const char* const py::doc::unsetNodeParameter=
{
    "Remove a parameter from a given node. \n\n"
    "\t :param parm: ID of the parameter \n"
    "\t :param node: ID of the node (default = 0: edits the scene's root) \n"
};

PyObject*
py::_unsetNodeParameter(PyObject* self, PyObject* args, PyObject* kwds)
{
    static char*    argnames[] = { "parm", "node", nullptr };
    unsigned int    parmId = 0;
    unsigned int    nodeId = 0;

    if (!PyArg_ParseTupleAndKeywords(args, kwds, "I|I", argnames, &parmId, &nodeId))
        return nullptr;

    assert(g_context);
    // special case if the parameter corresponds to a node ID
/*  if (g_context->controlCenter->nodeParameterExistsAsId(parmId, nodeId))
        g_context->controlCenter->unsetNodeParameterId(parmId, nodeId);
    else
        */g_context->controlCenter->unsetNodeParameter(parmId, nodeId);

    Py_RETURN_NONE;
}


const char* const py::doc::getCurrentNodeParameters=
{
    "Get a given node's current set of parameters. \n\n"
    "\t :param node: ID of the node (default = 0, queries the scene's root) \n"
    "\t :returns: list of the IDs of the parameters currently set on the node \n"
};

PyObject*
py::_getCurrentNodeParameters(PyObject* self, PyObject* args, PyObject* kwds)
{
    static char*    argnames[]  = { "node", nullptr };
    unsigned int    nodeId      = 0;

    if (!PyArg_ParseTupleAndKeywords(args, kwds, "|I", argnames, &nodeId))
        return nullptr;

    assert(g_context);
    xchg::IdSet parmIds;

    g_context->controlCenter->getCurrentNodeParameters(parmIds, nodeId);

    return build<xchg::IdSet>(parmIds);
}


const char* const py::doc::getRelevantNodeParameters=
{
    "Get all parameters the given may react to. \n\n"
    "\t :param node: ID of the node (default = 0: queries the scene's root) \n"
    "\t :returns: list of parameter IDs \n"
};

PyObject*
py::_getRelevantNodeParameters(PyObject* self, PyObject* args, PyObject* kwds)
{
    static char* argnames[] = { "node", nullptr };
    unsigned int nodeId = 0;

    if (!PyArg_ParseTupleAndKeywords(args, kwds, "|I", argnames, &nodeId))
        return nullptr;

    assert(g_context);
    xchg::IdSet parmIds;

    g_context->controlCenter->getRelevantNodeParameters(parmIds, nodeId);

    return build<xchg::IdSet>(parmIds);
}


const char* const py::doc::getNodeParameterInfo =
{
    "Retrieve information concerning the given node parameter. \n\n"
    "\t :param parm: ID of the parameter \n"
    "\t :param node: ID of the node (default = 0: queries the scene's root) \n"
    "\t :returns: parameter's information as a dictionary "
    "(entries: \"name\", \"type\", \"needs_scene\", \"creates_link\", "
     "\"triggers_action\", \"switches_state\") \n"
};

PyObject*
py::_getNodeParameterInfo(PyObject* self, PyObject* args, PyObject* kwds)
{
    static char*    argnames[]  = { "parm", "node", nullptr };
    unsigned int    parmId      = 0;
    unsigned int    nodeId      = 0;

    if (!PyArg_ParseTupleAndKeywords(args, kwds, "I|I", argnames, &parmId, &nodeId))
        return nullptr;

    assert(g_context);

    PyObject*           infoDict = PyDict_New();
    xchg::ParameterInfo parmInfo;
    if (!g_context->controlCenter->getNodeParameterInfo(parmId, parmInfo, nodeId))
        return infoDict;

    assert(parmInfo.name);
    assert(parmInfo.typeinfo);

    PyObject* itemName              = build<std::string>(parmInfo.name);
    PyObject* itemType              = build<int>(static_cast<int>(xchg::getParmType(*parmInfo.typeinfo)));
    PyObject* itemNeedsScene        = build<bool>((parmInfo.flags & parm::NEEDS_SCENE       ) != 0);
    PyObject* itemCreatesLink       = build<bool>((parmInfo.flags & parm::CREATES_LINK      ) != 0);
    PyObject* itemTriggersAction    = build<bool>((parmInfo.flags & parm::TRIGGERS_ACTION   ) != 0);
    PyObject* itemSwitchesState     = build<bool>((parmInfo.flags & parm::SWITCHES_STATE    ) != 0);

    PyDict_SetItemString(infoDict, "name",              itemName);
    PyDict_SetItemString(infoDict, "type",              itemType);
    PyDict_SetItemString(infoDict, "needs_scene",       itemNeedsScene);
    PyDict_SetItemString(infoDict, "creates_link",      itemCreatesLink);
    PyDict_SetItemString(infoDict, "triggers_action",   itemTriggersAction);
    PyDict_SetItemString(infoDict, "switches_state",    itemSwitchesState);
    Py_DECREF(itemName);
    Py_DECREF(itemType);
    Py_DECREF(itemNeedsScene);
    Py_DECREF(itemCreatesLink);
    Py_DECREF(itemTriggersAction);
    Py_DECREF(itemSwitchesState);

    return infoDict;
}


const char* const py::doc::connectNodeParameters =
{
    "Connect and synchronize two given node parameters. \n\n"
    "\t :param parm_type: type of the connected parameter \n"
    "\t :param master_parm_name: name of the parameter forcing the value \n"
    "\t :param master: ID of the master parameter's node \n"
    "\t :param slave: ID of the slave parameter's node \n"
    "\t :param slave_parm_name: name of the synchronized parameter (default master_parm_name) \n"
    "\t :param priority: priority of the connection (default 0) \n"
    "\t :returns: ID of the created connection \n"
};

PyObject*
py::_connectNodeParameters(PyObject* self, PyObject* args, PyObject* kwds)
{
    static char*    argnames[]      = { "parm_type", "master_parm_name", "master", "slave", "slave_parm_name", "priority", nullptr };
    int             typ             = xchg::UNKNOWN_PARMTYPE;
    const char*     masterParmName  = "";
    unsigned int    masterNodeId    = 0;
    unsigned int    slaveNodeId     = 0;
    const char*     slaveParmName   = "";
    int             priority        = 0;

    if (!PyArg_ParseTupleAndKeywords(args, kwds, "isII|si", argnames, &typ, &masterParmName, &masterNodeId, &slaveNodeId, &slaveParmName, &priority))
        return nullptr;

    const xchg::ParmType parmType = static_cast<xchg::ParmType>(typ);
    assert(g_context);
    assert(masterParmName);
    if (strlen(slaveParmName) == 0)
        slaveParmName = masterParmName;

    //------------------------------------------------------------------------------------------
#define CONNECT_PARM(_PARMTYPE_VALUE_, _CTYPE_)                                                 \
    else if (parmType == _PARMTYPE_VALUE_)                                                      \
    {                                                                                           \
        const unsigned int cnxId = g_context->controlCenter->connectNodeParameters<_CTYPE_>(    \
            masterNodeId, masterParmName, slaveNodeId, slaveParmName, priority);                \
        return build<unsigned int>(cnxId);                                                      \
    }
    //----------------------------------------------------------------------------------------
    if (false) {}   // used to initialize the series of 'else' statements--
    CONNECT_PARM(xchg::PARMTYPE_BOOL,       bool)
    CONNECT_PARM(xchg::PARMTYPE_CHAR,       char)
    CONNECT_PARM(xchg::PARMTYPE_UINT,       unsigned int)
    CONNECT_PARM(xchg::PARMTYPE_SIZE,       size_t)
    CONNECT_PARM(xchg::PARMTYPE_INT,        int)
    CONNECT_PARM(xchg::PARMTYPE_INT_2,      math::Vector2i)
    CONNECT_PARM(xchg::PARMTYPE_INT_3,      math::Vector3i)
    CONNECT_PARM(xchg::PARMTYPE_INT_4,      math::Vector4i)
    CONNECT_PARM(xchg::PARMTYPE_FLOAT,      float)
    CONNECT_PARM(xchg::PARMTYPE_FLOAT_2,    math::Vector2f)
    CONNECT_PARM(xchg::PARMTYPE_FLOAT_4,    math::Vector4f)
    CONNECT_PARM(xchg::PARMTYPE_AFFINE_3,   math::Affine3f)
    CONNECT_PARM(xchg::PARMTYPE_STRING,     std::string)
    CONNECT_PARM(xchg::PARMTYPE_IDSET,      xchg::IdSet)

    {
        std::stringstream sstr;
        sstr
            << "Parameter type '" << std::to_string(parmType) << "' "
            << "not handled by _pyflirt module.";
        throw sys::Exception(sstr.str().c_str(), "Parameter Connection Creation");
    }
    Py_RETURN_NONE;
}


const char* const py::doc::connectIdNodeParameters =
{
    "Connect and synchronize two given node parameters of type ID. \n\n"
    "\t :param master_parm_name: name of the parameter forcing the value \n"
    "\t :param master: ID of the master parameter's node \n"
    "\t :param slave: ID of the slave parameter's node \n"
    "\t :param slave_parm_name: name of the synchronized parameter (default master_parm_name) \n"
    "\t :param priority: priority of the connection (default 0) \n"
    "\t :returns: ID of the created connection \n"
};

PyObject*
py::_connectIdNodeParameters(PyObject* self, PyObject* args, PyObject* kwds)
{
    static char*    argnames[]      = { "master_parm_name", "master", "slave", "slave_parm_name", "priority", nullptr };
    const char*     masterParmName  = "";
    unsigned int    masterNodeId    = 0;
    unsigned int    slaveNodeId     = 0;
    const char*     slaveParmName   = "";
    int             priority        = 0;

    if (!PyArg_ParseTupleAndKeywords(args, kwds, "sII|si", argnames, &masterParmName, &masterNodeId, &slaveNodeId, &slaveParmName, &priority))
        return nullptr;

    assert(g_context);
    assert(masterParmName);
    if (strlen(slaveParmName) == 0)
        slaveParmName = masterParmName;

    const unsigned int cnxId = g_context->controlCenter->connectIdNodeParameters(
        masterNodeId, masterParmName, slaveNodeId, slaveParmName, priority);
    return build<unsigned int>(cnxId);
}


const char* const py::doc::disconnectNodeParameters =
{
    "Cut the given connection and end the synchronization "
    "of its involved parameters. \n\n"
    "\t :param cnx: ID of the connection \n"
};

PyObject*
py::_disconnectNodeParameters(PyObject* self, PyObject* args, PyObject* kwds)
{
    static char*    argnames[]  = { "cnx", nullptr };
    unsigned int    cnxId       = 0;

    if (!PyArg_ParseTupleAndKeywords(args, kwds, "I", argnames, &cnxId))
        return nullptr;

    assert(g_context);
    if (g_context->controlCenter->hasIdNodeParameterConnection(cnxId))
        g_context->controlCenter->disconnectIdNodeParameters(cnxId);
    else if (g_context->controlCenter->hasNodeParameterConnection(cnxId))
        g_context->controlCenter->disconnectNodeParameters(cnxId);
    else
    {
        std::stringstream sstr;
        sstr << "Failed to destroy node parameter connection ID = " << cnxId << ".";
        throw sys::Exception(sstr.str().c_str(), "Node Parameter Disconnection");
    }

    Py_RETURN_NONE;
}


const char* const py::doc::getConnectionInfo =
{
    "Retrieve information concerning the given parameter connection. \n\n"
    "\t :param cnx: ID of the connection \n"
    "\t :returns: connection's information as a dictionary "
    "(entries: \"master\", \"master_parm\", \"master_parm_name\", \"slave\", "
     "\"slave_parm\", \"slave_parm_name\", \"priority\") \n"
};

PyObject*
py::_getConnectionInfo(PyObject* self, PyObject* args, PyObject* kwds)
{
    static char*    argnames[]  = { "cnx", nullptr };
    unsigned int    cnxId       = 0;

    if (!PyArg_ParseTupleAndKeywords(args, kwds, "I", argnames, &cnxId))
        return nullptr;

    assert(g_context);
    scn::ParmConnectionBase::Ptr const& cnx = g_context->controlCenter->getNodeParameterConnection(cnxId);

    PyObject* dict = PyDict_New();

    if (cnx)
    {
        insertInDict<unsigned int>  (dict, "master",            cnx->inNodeId());
        insertInDict<unsigned int>  (dict, "master_parm",       cnx->inParmId());
        insertInDict<std::string>   (dict, "master_parm_name",  cnx->inParmName());
        insertInDict<unsigned int>  (dict, "slave",             cnx->outNodeId());
        insertInDict<unsigned int>  (dict, "slave_parm",        cnx->outParmId());
        insertInDict<std::string>   (dict, "slave_parm_name",   cnx->outParmName());
        insertInDict<int>           (dict, "priority",          cnx->priority());
    }
    return dict;
}


const char* const py::doc::setConnectionPriority =
{
    "Change the priority of a connection. \n\n"
    "\t :param cnx: ID of the connection \n"
    "\t :param priority: priority of the connection \n"
};

PyObject*
py::_setConnectionPriority(PyObject* self, PyObject* args, PyObject* kwds)
{
    static char*    argnames[]  = { "cnx", "priority", nullptr };
    unsigned int    cnxId       = 0;
    int             priority    = 0;

    if (!PyArg_ParseTupleAndKeywords(args, kwds, "Ii", argnames, &cnxId, &priority))
        return nullptr;

    assert(g_context);
    scn::ParmConnectionBase::Ptr const& cnx = g_context->controlCenter->getNodeParameterConnection(cnxId);\

    if (cnx)
        cnx->priority(priority);

    Py_RETURN_NONE;
}


const char* const py::doc::isNodeParameterWritable =
{
    "Check the writability of a node parameter. \n\n"
    "\t :param parm: ID of the parameter \n"
    "\t :param node: ID of the node (default = 0: queries the scene's root) \n"
    "\t :returns: True if users can edit the parameter's value \n"
};

PyObject*
py::_isNodeParameterWritable(PyObject* self, PyObject* args, PyObject* kwds)
{
    static char*    argnames[]  = { "parm", "node", nullptr };
    unsigned int    parmId      = 0;
    unsigned int    nodeId      = 0;

    if (!PyArg_ParseTupleAndKeywords(args, kwds, "I|I", argnames, &parmId, &nodeId))
        return nullptr;

    assert(g_context);
    return build<bool>(g_context->controlCenter->isNodeParameterWritable(parmId, nodeId));
}


const char* const py::doc::isNodeParameterReadable =
{
    "Check the readability of a node parameter. \n\n"
    "\t :param parm: ID of the parameter \n"
    "\t :param node: ID of the node (default = 0: queries the scene's root) \n"
    "\t :returns: True if users can query the parameter's value \n"
};

PyObject*
py::_isNodeParameterReadable(PyObject* self, PyObject* args, PyObject* kwds)
{
    static char*    argnames[]  = { "parm", "node", nullptr };
    unsigned int    parmId      = 0;
    unsigned int    nodeId      = 0;

    if (!PyArg_ParseTupleAndKeywords(args, kwds, "I|I", argnames, &parmId, &nodeId))
        return nullptr;

    assert(g_context);

    // prevents reading of parameter whose type is not properly exposed to python
    // - for built-in parameters
    parm::BuiltIn::Ptr const& parm = parm::builtIns().find(parmId);
    if (parm &&
        (parm->compatibleWith<xchg::BoundingBox>() ||
        parm->compatibleWith<xchg::DataStream>() ||
        parm->compatibleWith<xchg::Data::Ptr>() ||
        parm->compatibleWith<xchg::ObjectPtr>()))
        return build<bool>(false);
    // - for user-defined parameters
    if (g_context->controlCenter->nodeParameterExistsAs<xchg::BoundingBox>(parmId, nodeId) ||
        g_context->controlCenter->nodeParameterExistsAs<xchg::DataStream>(parmId, nodeId) ||
        g_context->controlCenter->nodeParameterExistsAs<xchg::Data::Ptr>(parmId, nodeId) ||
        g_context->controlCenter->nodeParameterExistsAs<xchg::ObjectPtr>(parmId, nodeId))
        return build<bool>(false);

    return build<bool>(g_context->controlCenter->isNodeParameterReadable(parmId, nodeId));
}
