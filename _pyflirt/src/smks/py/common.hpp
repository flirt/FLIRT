// ========================================================================= //
// Copyright 2016-2018 SUPAMONKS_STUDIO                                      //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
//                                                                           //
// ========================================================================= //
#pragma once

#include <Python.h> // must come first !
#include <smks/sys/Exception.hpp>
#include "ret_build.hpp"

namespace smks { namespace py
{
    //<! insert long value in dictionary
    //static inline
    //void
    //insertInDict(PyObject* dict, char* name, long value)
    //{
    //  PyObject* item = PyInt_FromLong(value);
    //  if (item)
    //  {
    //      PyDict_SetItemString(dict, name, item);
    //      Py_DECREF(item);
    //  }
    //}

    template<typename ValueType >static inline
    void
    insertInDict(PyObject* dict, char* name, const ValueType& value)
    {
        PyObject* item = build<ValueType>(value);
        if (item)
        {
            PyDict_SetItemString(dict, name, item);
            Py_DECREF(item);
        }
    }
}
}
