// ========================================================================= //
// Copyright 2016-2018 SUPAMONKS_STUDIO                                      //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
//                                                                           //
// ========================================================================= //
#pragma once

#include "common.hpp"   //<! includes <Python.h>
#include "_pyflirt_macros.hpp"

#include <smks/parm/ParmFlags.hpp>
#include <smks/xchg/ParmType.hpp>

namespace smks { namespace py
{
    struct Context;

    extern Context*     g_context;
    extern PyObject*    g_pyflirtError;
}
}

PYFLIRT_GUARDED_FUNC        (addIncludeDirectory)
PYFLIRT_GUARDED_FUNC_NOARGS (getScene)
PYFLIRT_GUARDED_FUNC        (createDataNode)
PYFLIRT_GUARDED_FUNC        (destroyDataNode)
PYFLIRT_GUARDED_FUNC        (getNodeName)
PYFLIRT_GUARDED_FUNC        (getNodeFullName)
PYFLIRT_GUARDED_FUNC        (getNodeParent)
PYFLIRT_GUARDED_FUNC        (getNodeNumChildren)
PYFLIRT_GUARDED_FUNC        (getNodeChild)
PYFLIRT_GUARDED_FUNC        (getNodeClass)
PYFLIRT_GUARDED_FUNC        (getNodeIds)
PYFLIRT_GUARDED_FUNC        (createArchive)
PYFLIRT_GUARDED_FUNC        (createXform)
PYFLIRT_GUARDED_FUNC        (createCamera)
PYFLIRT_GUARDED_FUNC        (createMaterial)
PYFLIRT_GUARDED_FUNC        (createSubSurface)
PYFLIRT_GUARDED_FUNC        (createTexture)
PYFLIRT_GUARDED_FUNC        (createSelector)
PYFLIRT_GUARDED_FUNC        (createLight)
PYFLIRT_GUARDED_FUNC        (createFrameBuffer)
PYFLIRT_GUARDED_FUNC        (createDenoiser)
PYFLIRT_GUARDED_FUNC        (createToneMapper)
PYFLIRT_GUARDED_FUNC        (createRenderer)
PYFLIRT_GUARDED_FUNC        (createIntegrator)
PYFLIRT_GUARDED_FUNC        (createSamplerFactory)
PYFLIRT_GUARDED_FUNC        (createPixelFilter)
PYFLIRT_GUARDED_FUNC        (createRenderAction)
PYFLIRT_GUARDED_FUNC        (createShapeIdCoveragePass)
PYFLIRT_GUARDED_FUNC        (createFlatRenderPass)
PYFLIRT_GUARDED_FUNC        (createLightPathExpressionPass)
PYFLIRT_GUARDED_FUNC        (createAssign)
PYFLIRT_GUARDED_FUNC        (createIdAssign)
PYFLIRT_GUARDED_FUNC        (setNodeParameter)
PYFLIRT_GUARDED_FUNC        (setNodeParameterByName)
PYFLIRT_GUARDED_FUNC        (setIdNodeParameterByName)
PYFLIRT_GUARDED_FUNC        (setTriggerNodeParameterByName)
PYFLIRT_GUARDED_FUNC        (setSwitchNodeParameterByName)
PYFLIRT_GUARDED_FUNC        (unsetNodeParameter)
PYFLIRT_GUARDED_FUNC        (getNodeParameter)
PYFLIRT_GUARDED_FUNC        (getCurrentNodeParameters)
PYFLIRT_GUARDED_FUNC        (getRelevantNodeParameters)
PYFLIRT_GUARDED_FUNC        (getNodeParameterInfo)
PYFLIRT_GUARDED_FUNC        (isNodeParameterWritable)
PYFLIRT_GUARDED_FUNC        (isNodeParameterReadable)
PYFLIRT_GUARDED_FUNC        (getNodeLinkSources)
PYFLIRT_GUARDED_FUNC        (getNodeLinkTargets)
PYFLIRT_GUARDED_FUNC        (getLinksBetweenNodes)
PYFLIRT_GUARDED_FUNC        (connectNodeParameters)
PYFLIRT_GUARDED_FUNC        (connectIdNodeParameters)
PYFLIRT_GUARDED_FUNC        (disconnectNodeParameters)
PYFLIRT_GUARDED_FUNC        (getConnectionInfo)
PYFLIRT_GUARDED_FUNC        (setConnectionPriority)
PYFLIRT_GUARDED_FUNC        (registerSceneObserver)
PYFLIRT_GUARDED_FUNC        (registerNodeParameterObserver)
PYFLIRT_GUARDED_FUNC        (deregisterObserver)
PYFLIRT_GUARDED_FUNC        (registerProgressCallbacks)
PYFLIRT_GUARDED_FUNC        (deregisterProgressCallbacks)

PYFLIRT_GUARDED_FUNC        (mousePressEvent)
PYFLIRT_GUARDED_FUNC        (mouseMoveEvent)
PYFLIRT_GUARDED_FUNC        (mouseReleaseEvent)
PYFLIRT_GUARDED_FUNC        (mouseDoubleClickEvent)
PYFLIRT_GUARDED_FUNC        (wheelEvent)
PYFLIRT_GUARDED_FUNC        (keyPressEvent)
PYFLIRT_GUARDED_FUNC        (keyReleaseEvent)
PYFLIRT_GUARDED_FUNC        (resizeEvent)
PYFLIRT_GUARDED_FUNC_NOARGS (paintGL)

PYFLIRT_GUARDED_FUNC_NOARGS (run)
PYFLIRT_GUARDED_FUNC_NOARGS (finalize)

namespace smks { namespace py
{
    namespace doc
    {
        extern const char* const getModuleInfo;
        extern const char* const initialize;
        extern const char* const getClassCodes;
    }
    extern PyObject* getModuleInfo(PyObject* self);
    extern PyObject* initialize   (PyObject* self, PyObject* args, PyObject* kwds);
    extern PyObject* getClassCodes(PyObject* self, PyObject* args, PyObject* kwds);

    static
    PyMethodDef
    _pyflirt_methods[] =
    {
        { "get_module_info",                    (PyCFunction)getModuleInfo,                 METH_NOARGS,   doc::getModuleInfo                 },
        { "initialize",                         (PyCFunction)initialize,                    METH_KEYWORDS, doc::initialize                    },
        { "get_class_codes",                    (PyCFunction)getClassCodes,                 METH_KEYWORDS, doc::getClassCodes                 },
        { "add_include_directory",              (PyCFunction)addIncludeDirectory,           METH_KEYWORDS, doc::addIncludeDirectory           },
        { "run",                                (PyCFunction)run,                           METH_NOARGS,   doc::run                           },
        { "finalize",                           (PyCFunction)finalize,                      METH_NOARGS,   doc::finalize                      },
        { "get_scene",                          (PyCFunction)getScene,                      METH_NOARGS,   doc::getScene                      },
        { "create_data_node",                   (PyCFunction)createDataNode,                METH_KEYWORDS, doc::createDataNode                },
        { "destroy_data_node",                  (PyCFunction)destroyDataNode,               METH_KEYWORDS, doc::destroyDataNode               },
        { "get_node_name",                      (PyCFunction)getNodeName,                   METH_KEYWORDS, doc::getNodeName                   },
        { "get_node_full_name",                 (PyCFunction)getNodeFullName,               METH_KEYWORDS, doc::getNodeFullName               },
        { "get_node_parent",                    (PyCFunction)getNodeParent,                 METH_KEYWORDS, doc::getNodeParent                 },
        { "get_node_num_children",              (PyCFunction)getNodeNumChildren,            METH_KEYWORDS, doc::getNodeNumChildren            },
        { "get_node_child",                     (PyCFunction)getNodeChild,                  METH_KEYWORDS, doc::getNodeChild                  },
        { "get_node_class",                     (PyCFunction)getNodeClass,                  METH_KEYWORDS, doc::getNodeClass                  },
        { "get_node_ids",                       (PyCFunction)getNodeIds,                    METH_KEYWORDS, doc::getNodeIds                    },
        { "create_archive",                     (PyCFunction)createArchive,                 METH_KEYWORDS, doc::createArchive                 },
        { "create_xform",                       (PyCFunction)createXform,                   METH_KEYWORDS, doc::createXform                   },
        { "create_camera",                      (PyCFunction)createCamera,                  METH_KEYWORDS, doc::createCamera                  },
        { "create_material",                    (PyCFunction)createMaterial,                METH_KEYWORDS, doc::createMaterial                },
        { "create_subsurface",                  (PyCFunction)createSubSurface,              METH_KEYWORDS, doc::createSubSurface              },
        { "create_texture",                     (PyCFunction)createTexture,                 METH_KEYWORDS, doc::createTexture                 },
        { "create_selector",                    (PyCFunction)createSelector,                METH_KEYWORDS, doc::createSelector                },
        { "create_light",                       (PyCFunction)createLight,                   METH_KEYWORDS, doc::createLight                   },
        { "create_framebuffer",                 (PyCFunction)createFrameBuffer,             METH_KEYWORDS, doc::createFrameBuffer             },
        { "create_denoiser",                    (PyCFunction)createDenoiser,                METH_KEYWORDS, doc::createDenoiser                },
        { "create_tonemapper",                  (PyCFunction)createToneMapper,              METH_KEYWORDS, doc::createToneMapper              },
        { "create_renderer",                    (PyCFunction)createRenderer,                METH_KEYWORDS, doc::createRenderer                },
        { "create_integrator",                  (PyCFunction)createIntegrator,              METH_KEYWORDS, doc::createIntegrator              },
        { "create_sampler_factory",             (PyCFunction)createSamplerFactory,          METH_KEYWORDS, doc::createSamplerFactory          },
        { "create_pixel_filter",                (PyCFunction)createPixelFilter,             METH_KEYWORDS, doc::createPixelFilter             },
        { "create_render_action",               (PyCFunction)createRenderAction,            METH_KEYWORDS, doc::createRenderAction            },
        { "create_shape_id_coverage_pass",      (PyCFunction)createShapeIdCoveragePass,     METH_KEYWORDS, doc::createShapeIdCoveragePass     },
        { "create_flat_render_pass",            (PyCFunction)createFlatRenderPass,          METH_KEYWORDS, doc::createFlatRenderPass          },
        { "create_lightpath_expression_pass",   (PyCFunction)createLightPathExpressionPass, METH_KEYWORDS, doc::createLightPathExpressionPass },
        { "create_assign",                      (PyCFunction)createAssign,                  METH_KEYWORDS, doc::createAssign                  },
        { "create_id_assign",                   (PyCFunction)createIdAssign,                METH_KEYWORDS, doc::createIdAssign                },
        { "set_node_parameter",                 (PyCFunction)setNodeParameter,              METH_KEYWORDS, doc::setNodeParameter              },
        { "set_node_parameter_by_name",         (PyCFunction)setNodeParameterByName,        METH_KEYWORDS, doc::setNodeParameterByName        },
        { "set_id_node_parameter_by_name",      (PyCFunction)setIdNodeParameterByName,      METH_KEYWORDS, doc::setIdNodeParameterByName      },
        { "set_trigger_node_parameter_by_name", (PyCFunction)setTriggerNodeParameterByName, METH_KEYWORDS, doc::setTriggerNodeParameterByName },
        { "set_switch_node_parameter_by_name",  (PyCFunction)setSwitchNodeParameterByName,  METH_KEYWORDS, doc::setSwitchNodeParameterByName  },
        { "get_node_parameter",                 (PyCFunction)getNodeParameter,              METH_KEYWORDS, doc::getNodeParameter              },
        { "unset_node_parameter",               (PyCFunction)unsetNodeParameter,            METH_KEYWORDS, doc::unsetNodeParameter            },
        { "get_current_node_parameters",        (PyCFunction)getCurrentNodeParameters,      METH_KEYWORDS, doc::getCurrentNodeParameters      },
        { "get_relevant_node_parameters",       (PyCFunction)getRelevantNodeParameters,     METH_KEYWORDS, doc::getRelevantNodeParameters     },
        { "get_node_parameter_info",            (PyCFunction)getNodeParameterInfo,          METH_KEYWORDS, doc::getNodeParameterInfo          },
        { "is_node_parameter_writable",         (PyCFunction)isNodeParameterWritable,       METH_KEYWORDS, doc::isNodeParameterWritable       },
        { "is_node_parameter_readable",         (PyCFunction)isNodeParameterReadable,       METH_KEYWORDS, doc::isNodeParameterReadable       },
        { "get_node_link_sources",              (PyCFunction)getNodeLinkSources,            METH_KEYWORDS, doc::getNodeLinkSources            },
        { "get_node_link_targets",              (PyCFunction)getNodeLinkTargets,            METH_KEYWORDS, doc::getNodeLinkTargets            },
        { "get_links_between_nodes",            (PyCFunction)getLinksBetweenNodes,          METH_KEYWORDS, doc::getLinksBetweenNodes          },
        { "connect_node_parameters",            (PyCFunction)connectNodeParameters,         METH_KEYWORDS, doc::connectNodeParameters         },
        { "connect_id_node_parameters",         (PyCFunction)connectIdNodeParameters,       METH_KEYWORDS, doc::connectIdNodeParameters       },
        { "disconnect_node_parameters",         (PyCFunction)disconnectNodeParameters,      METH_KEYWORDS, doc::disconnectNodeParameters      },
        { "get_connection_info",                (PyCFunction)getConnectionInfo,             METH_KEYWORDS, doc::getConnectionInfo             },
        { "set_connection_priority",            (PyCFunction)setConnectionPriority,         METH_KEYWORDS, doc::setConnectionPriority         },
        { "register_scene_observer",            (PyCFunction)registerSceneObserver,         METH_KEYWORDS, doc::registerSceneObserver         },
        { "register_node_parameter_observer",   (PyCFunction)registerNodeParameterObserver, METH_KEYWORDS, doc::registerNodeParameterObserver },
        { "deregister_observer",                (PyCFunction)deregisterObserver,            METH_KEYWORDS, doc::deregisterObserver            },
        { "mouse_press_event",                  (PyCFunction)mousePressEvent,               METH_KEYWORDS, doc::mousePressEvent               },
        { "mouse_move_event",                   (PyCFunction)mouseMoveEvent,                METH_KEYWORDS, doc::mouseMoveEvent                },
        { "mouse_release_event",                (PyCFunction)mouseReleaseEvent,             METH_KEYWORDS, doc::mouseReleaseEvent             },
        { "mouse_double_click_event",           (PyCFunction)mouseDoubleClickEvent,         METH_KEYWORDS, doc::mouseDoubleClickEvent         },
        { "wheel_event",                        (PyCFunction)wheelEvent,                    METH_KEYWORDS, doc::wheelEvent                    },
        { "key_press_event",                    (PyCFunction)keyPressEvent,                 METH_KEYWORDS, doc::keyPressEvent                 },
        { "key_release_event",                  (PyCFunction)keyReleaseEvent,               METH_KEYWORDS, doc::keyReleaseEvent               },
        { "resize_event",                       (PyCFunction)resizeEvent,                   METH_KEYWORDS, doc::resizeEvent                   },
        { "paint_gl",                           (PyCFunction)paintGL,                       METH_NOARGS,   doc::paintGL                       },
        { "register_progress_callbacks",        (PyCFunction)registerProgressCallbacks,     METH_KEYWORDS, doc::registerProgressCallbacks     },
        { "deregister_progress_callbacks",      (PyCFunction)deregisterProgressCallbacks,   METH_KEYWORDS, doc::deregisterProgressCallbacks   },
        { nullptr,                              nullptr,                                    0,             nullptr                            } // Sentinel
    };

    static
    const char* const
    _pyflirt_doc =
    {
        "The _pyflirt module directly exposes the entirety of the FLIRT C++ framework's API in Python, "
        "and allows users to procedurally create, edit, preview, and render its 3D scenes."
    };
}
}

