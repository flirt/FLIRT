// ========================================================================= //
// Copyright 2016-2018 SUPAMONKS_STUDIO                                      //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
//                                                                           //
// ========================================================================= //
#pragma once

#include <smks/ClientBase.hpp>
#include <smks/ClientBindingBase.hpp>

namespace smks
{
    namespace scn
    {
        class TreeNode;
        class Scene;
    }
    namespace py
    {
        class ControlCenter:
            public ClientBase
        {
        public:
            typedef std::shared_ptr<ControlCenter>  Ptr;
            typedef std::weak_ptr<ControlCenter>    WPtr;
        private:
            typedef std::shared_ptr<scn::TreeNode>  TreeNodePtr;
            typedef std::weak_ptr<scn::TreeNode>    TreeNodeWPtr;

        private:
            struct MouseEventCallback;
            struct WheelEventCallback;
            struct ResizeEventCallback;

        public:
            static
            Ptr
            create();

            static
            void
            destroy(Ptr&);

        private:
            // non-copyable
            ControlCenter(const ControlCenter&);
            ControlCenter& operator=(const ControlCenter&);

        protected:
            ControlCenter();

            inline
            ClientBindingBase::Ptr
            createBinding(TreeNodeWPtr const&)
            {  return nullptr; }

            inline
            void
            destroyBinding(ClientBindingBase::Ptr const&)
            { }

            inline
            void
            registerGUIEventCallbacks(scn::Scene&)
            { }

            inline
            void
            deregisterGUIEventCallbacks(scn::Scene&)
            { }
        };
    }
}
