// ========================================================================= //
// Copyright 2016-2018 SUPAMONKS_STUDIO                                      //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
//                                                                           //
// ========================================================================= //
#include "_pyflirt.hpp"
#include <iostream>
#include <json/json.h>

#include <smks/sys/Log.hpp>

#if defined(FLIRT_LOG_ENABLED)
INITIALIZE_NULL_EASYLOGGINGPP;
#endif // defined(FLIRT_LOG_ENABLED)

#include <smks/sys/Exception.hpp>

#include <smks/sys/initialize_asset.hpp>
#include <smks/sys/initialize_scn.hpp>
#include <smks/sys/initialize_rndr_client.hpp>
#include <smks/sys/initialize_view_client.hpp>
#include <smks/sys/initialize_view_device_ctrl.hpp>
#if defined(FLIRT_WITH_DEBUG)
#include <smks/sys/initialize_view_debug.hpp>
#endif // defined(FLIRT_WITH_DEBUG)

#include <smks/sys/version.hpp>
#include <smks/sys/compile_info.hpp>
#include <smks/sys/ResourcePathFinder.hpp>
#include <smks/sys/ResourceDatabase.hpp>

#include <smks/view/AbstractDevice.hpp>

#include "Config.hpp"
#include "Context.hpp"
#include "ControlCenter.hpp"
#include "ret_build.hpp"

using namespace smks;

const char* const py::doc::getModuleInfo =
{
    "Get module information. \n\n"
    "\t :returns: dictionary containing module information \n"
};

PyObject*
py::getModuleInfo(PyObject* self)
{
    PyObject* dict = PyDict_New();

    insertInDict<int>           (dict, "major_version", smks::sys::FLIRT_MAJOR);
    insertInDict<int>           (dict, "minor_version", smks::sys::FLIRT_MINOR);
    insertInDict<int>           (dict, "tweak_version", smks::sys::FLIRT_TWEAK);
    insertInDict<std::string>   (dict, "date",          smks::sys::BUILD_DATE);
    insertInDict<std::string>   (dict, "repository",    smks::sys::BUILD_REPO);
    insertInDict<std::string>   (dict, "commit",        smks::sys::BUILD_COMMIT);
    insertInDict<std::string>   (dict, "compiler",      smks::sys::BUILD_COMPILER);

    return dict;
}


const char* const py::doc::initialize =
{
    "Allocate resources for storing the module's content, starting its execution. \n\n"
    "\t :param cfg: configuration settings in JSON format \n\n"
};

PyObject*
py::initialize(PyObject* self, PyObject* args, PyObject* kwds)
{
    static char* argnames[] = { "cfg", nullptr };
    const char*  jsonCfg    = "";

    if (!PyArg_ParseTupleAndKeywords(args, kwds, "|s", argnames, &jsonCfg))
        return nullptr;

    try
    {
    Config cfg(jsonCfg);

#if defined(FLIRT_LOG_ENABLED)
    el::Helpers::setStorage(smks::sys::sharedLoggingRepository());
    el::Loggers::reconfigureAllLoggers(el::Level::Global, el::ConfigurationType::ToStandardOutput, "false");
    el::Loggers::reconfigureAllLoggers(el::Level::Global, el::ConfigurationType::Format,           "%datetime [%fbase:%line]: %msg");
    el::Loggers::reconfigureAllLoggers(el::Level::Global, el::ConfigurationType::ToFile,           "true");
    el::Loggers::reconfigureAllLoggers(el::Level::Global, el::ConfigurationType::Filename,         cfg.logFile());
    LOG(DEBUG) << "module configuration"
        << "\n\t- #threads = " << cfg.numThreads()
        << "\n\t- log file = " << cfg.logFile()
        << "\n\t- with rendering = " << cfg.withRendering()
        << "\n\t- with viewport = " << cfg.withViewport()
        << "\n\t- screen width = " << cfg.screenWidth()
        << "\n\t- screen height = " << cfg.screenHeight();
#endif // defined(FLIRT_LOG_ENABLED)

    sys::initializeSmksAsset();
    sys::initializeSmksScn(cfg.numThreads());
    sys::initializeSmksRndrClient();
    sys::initializeSmksViewClient();
    sys::initializeSmksViewDeviceCtrl();
#if defined(FLIRT_WITH_DEBUG)
    sys::initializeViewDebug();
#endif // defined(FLIRT_WITH_DEBUG)

    if (g_context)
        Py_RETURN_NONE;
    assert(!g_context);
    g_context = new Context();

    g_context->pathFinder   = sys::ResourcePathFinder::create();
    g_context->resources    = sys::ResourceDatabase::create("resources");

    g_context->resources->setSharedPathFinder(g_context->pathFinder);

    g_context->scene = scn::Scene::create(jsonCfg);
    g_context->scene->setSharedResourceDatabase(g_context->resources);

    g_context->controlCenter = ControlCenter::create();
    g_context->controlCenter->setSharedResourceDatabase(g_context->resources);
    g_context->controlCenter->setScene(g_context->scene);

    if (cfg.withRendering())
    {
        g_context->rndrClient = rndr::Client::create(jsonCfg);
        g_context->rndrClient->setSharedResourceDatabase(g_context->resources);
        g_context->rndrClient->setScene(g_context->scene);
    }
    if (cfg.withViewport())
    {
        g_context->graphicsWindow = new osgViewer::GraphicsWindowEmbedded(
            0, 0, cfg.screenWidth(), cfg.screenHeight());

        g_context->viewClient = view::Client::create(jsonCfg, g_context->graphicsWindow);
        g_context->viewClient->setSharedResourceDatabase(g_context->resources);
        g_context->viewClient->setScene(g_context->scene);

        view::AbstractDevice* viewDevice = g_context->viewClient->device();

        // animation
        if (cfg.viewportAnimation())
        {
            g_context->viewAnimCallback    = view::Animation::create(g_context->viewClient);
            g_context->viewAnimCtrl        = view::AnimationControl::create();
            g_context->viewAnimTimeSwapper = view::AnimationTimeSwapper::create();

            if (cfg.viewportAutoPlay())
                g_context->viewAnimCallback->play();
        }

        // camera handler
        if (cfg.viewportCameraFocus())
            g_context->viewCamCtrl = view::CameraFocus::create(viewDevice);
        else if (cfg.viewportOrbitCamera())
            g_context->viewCamCtrl = view::OrbitCameraHandler::create(viewDevice);

        if (cfg.viewportCameraSwitch())
            g_context->viewCamSwitch = view::CameraSwitch::create(g_context->viewClient);

        // light focus
        if (cfg.viewportLightFocus())
            g_context->viewLightFocus = view::LightFocus::create();

        // framebuffer switch
        if (cfg.viewportCameraFocus())
            g_context->viewFbufferSwitch = view::FrameBufferSwitch::create(g_context->viewClient);

        // picking and manipulation
        if (cfg.viewportPicking())
            g_context->viewMousePicker = view::MousePicker::create();
        if (cfg.viewportManipulator())
            g_context->viewManipCtrl   = view::ManipulatorControl::create();

        // head-up display
        if (cfg.viewportHUD())
            g_context->viewHudHandler = view::hud::Handler::create();

        // render trigger
        if (cfg.viewportSnapshot() &&
            g_context->rndrClient)
            g_context->viewRenderer = view::Renderer::create();

        // batched object initialization
        if (cfg.viewportObjLoader())
            g_context->viewObjLoader = view::SceneObjLoader::create();

#if defined(FLIRT_WITH_DEBUG)
        if (cfg.viewportDebug())
            g_context->viewTestLoader = view::TestSubSceneLoader::create();
#endif // defined(FLIRT_WITH_DEBUG)


        if (g_context->viewAnimTimeSwapper)
            g_context->viewAnimTimeSwapper->detectionRangeY(0, 25);
        if (g_context->viewHudHandler)
            g_context->viewHudHandler->timelineRangeY(osg::Vec2f(0.0f, 25.0f));
        //if (g_context->viewRenderer)
        //  g_context->viewRenderer->configurationFile(...);

        if (viewDevice)
        {
            if (g_context->viewAnimCallback)
                g_context->viewClient->root()->addUpdateCallback(g_context->viewAnimCallback.get());

            if (g_context->viewAnimCtrl)
                viewDevice->addEventHandler(g_context->viewAnimCtrl.get());
            if (g_context->viewAnimTimeSwapper)
                viewDevice->addEventHandler(g_context->viewAnimTimeSwapper.get());
            if (g_context->viewMousePicker)
                viewDevice->addEventHandler(g_context->viewMousePicker.get());
            if (g_context->viewCamCtrl)
                viewDevice->addEventHandler(g_context->viewCamCtrl.get());
            if (g_context->viewCamSwitch)
                viewDevice->addEventHandler(g_context->viewCamSwitch.get());
            if (g_context->viewFbufferSwitch)
                viewDevice->addEventHandler(g_context->viewFbufferSwitch.get());
            if (g_context->viewLightFocus)
                viewDevice->addEventHandler(g_context->viewLightFocus.get());
            if (g_context->viewHudHandler)
                viewDevice->addEventHandler(g_context->viewHudHandler.get());
            if (g_context->viewRenderer)
                viewDevice->addEventHandler(g_context->viewRenderer.get());
            if (g_context->viewManipCtrl)
                viewDevice->addEventHandler(g_context->viewManipCtrl.get());
            if (g_context->viewObjLoader)
                viewDevice->addEventHandler(g_context->viewObjLoader.get());

#if defined(FLIRT_WITH_DEBUG)
            if (g_context->viewTestLoader)
                viewDevice->addEventHandler(g_context->viewTestLoader.get());
#endif // defined(FLIRT_WITH_DEBUG)
            }
        }
    }
    catch(const sys::Exception& e)
    {
        PyErr_SetString(g_pyflirtError, e.what());
        return NULL;
    }

    Py_RETURN_NONE;
}


const char* const py::doc::finalize =
{
    "Release the resources allocated by the module, ending its execution. \n"
};

PyObject*
py::_finalize(PyObject* self, PyObject* args)
{
    assert(g_context);

    g_context->graphicsWindow           = nullptr;
    view::AbstractDevice* viewDevice    = g_context->viewClient
        ? g_context->viewClient->device()
        : nullptr;

    if (viewDevice)
    {
#if defined(FLIRT_WITH_DEBUG)
        if (g_context->viewTestLoader)
            viewDevice->removeEventHandler(g_context->viewTestLoader.get());
#endif // defined(FLIRT_WITH_DEBUG)

        if (g_context->viewObjLoader)
            viewDevice->removeEventHandler(g_context->viewObjLoader.get());
        if (g_context->viewManipCtrl)
            viewDevice->removeEventHandler(g_context->viewManipCtrl.get());
        if (g_context->viewRenderer)
            viewDevice->removeEventHandler(g_context->viewRenderer.get());
        if (g_context->viewHudHandler)
            viewDevice->removeEventHandler(g_context->viewHudHandler.get());
        if (g_context->viewLightFocus)
            viewDevice->removeEventHandler(g_context->viewLightFocus.get());
        if (g_context->viewFbufferSwitch)
            viewDevice->removeEventHandler(g_context->viewFbufferSwitch.get());
        if (g_context->viewCamSwitch)
            viewDevice->removeEventHandler(g_context->viewCamSwitch.get());
        if (g_context->viewCamCtrl)
            viewDevice->removeEventHandler(g_context->viewCamCtrl.get());
        if (g_context->viewMousePicker)
            viewDevice->removeEventHandler(g_context->viewMousePicker.get());
        if (g_context->viewAnimTimeSwapper)
            viewDevice->removeEventHandler(g_context->viewAnimTimeSwapper.get());
        if (g_context->viewAnimCtrl)
            viewDevice->removeEventHandler(g_context->viewAnimCtrl.get());

        if (g_context->viewAnimCallback)
            g_context->viewClient->root()->removeUpdateCallback(g_context->viewAnimCallback.get());
    }
#if defined(FLIRT_WITH_DEBUG)
    view::TestSubSceneLoader::destroy(g_context->viewTestLoader);
#endif // defined(FLIRT_WITH_DEBUG)
    view::SceneObjLoader::destroy(g_context->viewObjLoader);
    view::ManipulatorControl::destroy(g_context->viewManipCtrl);
    view::Renderer::destroy(g_context->viewRenderer);
    view::hud::Handler::destroy(g_context->viewHudHandler);
    view::LightFocus::destroy(g_context->viewLightFocus);
    view::FrameBufferSwitch::destroy(g_context->viewClient, g_context->viewFbufferSwitch);
    view::CameraSwitch::destroy(g_context->viewClient, g_context->viewCamSwitch);

    {
        view::CameraFocus::Ptr ptr = dynamic_cast<view::CameraFocus*>(g_context->viewCamCtrl.get());
        if (ptr)
            view::CameraFocus::destroy(viewDevice, ptr);
    }
    {
        view::OrbitCameraHandler::Ptr ptr = dynamic_cast<view::OrbitCameraHandler*>(g_context->viewCamCtrl.get());
        if (ptr)
            view::OrbitCameraHandler::destroy(viewDevice, ptr);
    }
    g_context->viewCamCtrl = nullptr;

    view::MousePicker::destroy(g_context->viewMousePicker);
    view::AnimationTimeSwapper::destroy(g_context->viewAnimTimeSwapper);
    view::AnimationControl::destroy(g_context->viewAnimCtrl);
    view::Animation::destroy(g_context->viewClient, g_context->viewAnimCallback);

    if (g_context->viewClient)
        view::Client::destroy(g_context->viewClient);
    if (g_context->rndrClient)
        rndr::Client::destroy(g_context->rndrClient);
    ControlCenter::destroy(g_context->controlCenter);
    scn::Scene::destroy(g_context->scene);

    delete g_context;
    g_context = nullptr;

#if defined(FLIRT_WITH_DEBUG)
    sys::finalizeViewDebug();
#endif // defined(FLIRT_WITH_DEBUG)
    sys::finalizeSmksRndrClient();
    sys::finalizeSmksViewClient();
    sys::finalizeSmksViewDeviceCtrl();
    sys::finalizeSmksScn();
    sys::finalizeSmksAsset();

#if defined(FLIRT_LOG_ENABLED)
    LOG(DEBUG) << "finalized module";
    el::Helpers::setStorage(nullptr);
#endif // defined(FLIRT_LOG_ENABLED)

    Py_RETURN_NONE;
}


const char* const py::doc::getScene =
{
    "Get the ID of the scene's root. \n\n"
    "\t :returns: ID of the scene's root \n"
};

PyObject*
py::_getScene(PyObject* self, PyObject* args)
{
    assert(g_context);
    const unsigned int id = g_context->scene->id();

    return build<unsigned int>(id);
}


const char* const py::doc::run =
{
    "Execute a main frame loop. \n\n"
    "\t :returns: 0 upon success, positive status code otherwise \n"
};

PyObject*
py::_run(PyObject* self, PyObject* args)
{
    assert(g_context);
    int ret = 0;

    osgViewer::Viewer* viewDevice = g_context->viewClient
        ? g_context->viewClient->device()
        : nullptr;

    if (viewDevice)
    {
        viewDevice->setDone(false);
        ret = viewDevice->run();
    }

    return build<int>(ret);
}



const char* const py::doc::addIncludeDirectory =
{
    "Add a directory path to the module's resource managers. \n\n"
    "\t :param path: include directory path \n"
    "\t :returns: True upon success"
};

PyObject*
py::_addIncludeDirectory(PyObject* self, PyObject* args, PyObject* kwds)
{
    static char* argnames[] = { "path", nullptr };

    const char* path;
    if (!PyArg_ParseTupleAndKeywords(args, kwds, "s", argnames, &path))
        return nullptr;

    assert(g_context);
    const bool ok = g_context->pathFinder->addIncludeDirectory(path);

    return build<bool>(ok);
}

